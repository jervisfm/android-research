package com.google.tts;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

@Deprecated
public class TTS
{
  private static final int MIN_VER = 10;
  private InitListener cb = null;
  private Context ctx;
  private ITTS itts;
  private ITTSCallback ittscallback;
  private ServiceConnection serviceConnection;
  private boolean showInstaller = false;
  private SpeechCompletedListener speechCompletedCallback = null;
  private boolean started = false;
  private int version = -1;
  private TTSVersionAlert versionAlert = null;

  public TTS(Context paramContext, InitListener paramInitListener, TTSVersionAlert paramTTSVersionAlert)
  {
    this.showInstaller = true;
    this.versionAlert = paramTTSVersionAlert;
    this.ctx = paramContext;
    this.cb = paramInitListener;
    if (dataFilesCheck())
      initTts();
  }

  public TTS(Context paramContext, InitListener paramInitListener, boolean paramBoolean)
  {
    this.showInstaller = paramBoolean;
    this.ctx = paramContext;
    this.cb = paramInitListener;
    if (dataFilesCheck())
      initTts();
  }

  private boolean dataFilesCheck()
  {
    if (!ConfigurationManager.allFilesExist())
      if (!this.showInstaller)
        return false;
    try
    {
      Context localContext = this.ctx.createPackageContext("com.google.tts", 3);
      Intent localIntent = new Intent(localContext, localContext.getClassLoader().loadClass("com.google.tts.ConfigurationManager"));
      this.ctx.startActivity(localIntent);
      return false;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      localNameNotFoundException.printStackTrace();
      return true;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      while (true)
        localClassNotFoundException.printStackTrace();
    }
  }

  private void initTts()
  {
    this.started = false;
    this.serviceConnection = new ServiceConnection()
    {
      public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
      {
        TTS.this.itts = ITTS.Stub.asInterface(paramAnonymousIBinder);
        try
        {
          TTS.this.version = TTS.this.itts.getVersion();
          if (TTS.this.version < 10)
          {
            if (!TTS.this.showInstaller)
              return;
            if (TTS.this.versionAlert != null)
            {
              TTS.this.versionAlert.show();
              return;
            }
            new TTSVersionAlert(TTS.this.ctx, null, null, null).show();
            return;
          }
        }
        catch (RemoteException localRemoteException)
        {
          TTS.this.initTts();
          return;
        }
        TTS.this.ittscallback = new ITTSCallback.Stub()
        {
          public void markReached(String paramAnonymous2String)
            throws RemoteException
          {
            if (TTS.this.speechCompletedCallback != null)
              TTS.this.speechCompletedCallback.onSpeechCompleted();
          }
        };
        TTS.this.itts.registerCallback(TTS.this.ittscallback);
        TTS.this.started = true;
        if (TTS.this.cb != null)
          TTS.this.cb.onInit(TTS.this.version);
      }

      public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
      {
        TTS.this.itts = null;
        TTS.this.cb = null;
        TTS.this.started = false;
      }
    };
    Intent localIntent = new Intent("android.intent.action.USE_TTS");
    localIntent.addCategory("android.intent.category.TTS");
    if ((!this.ctx.bindService(localIntent, this.serviceConnection, 1)) && (this.showInstaller))
    {
      if (this.versionAlert != null)
        this.versionAlert.show();
    }
    else
      return;
    new TTSVersionAlert(this.ctx, null, null, null).show();
  }

  public static boolean isInstalled(Context paramContext)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    Intent localIntent = new Intent("android.intent.action.USE_TTS");
    localIntent.addCategory("android.intent.category.TTS");
    return localPackageManager.resolveService(localIntent, 0) != null;
  }

  public void addSpeech(String paramString1, String paramString2)
  {
    if (!this.started)
      return;
    try
    {
      this.itts.addSpeechFile(paramString1, paramString2);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      this.started = false;
      initTts();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      this.started = false;
      initTts();
    }
  }

  public void addSpeech(String paramString1, String paramString2, int paramInt)
  {
    if (!this.started)
      return;
    try
    {
      this.itts.addSpeech(paramString1, paramString2, paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      this.started = false;
      initTts();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      this.started = false;
      initTts();
    }
  }

  public int getVersion()
  {
    return this.version;
  }

  public boolean isSpeaking()
  {
    if (!this.started)
      return false;
    try
    {
      boolean bool = this.itts.isSpeaking();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
      return false;
    }
    catch (NullPointerException localNullPointerException)
    {
      this.started = false;
      initTts();
      return false;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      this.started = false;
      initTts();
    }
    return false;
  }

  public void playEarcon(TTSEarcon paramTTSEarcon, int paramInt, String[] paramArrayOfString)
  {
    playEarcon(paramTTSEarcon.name(), paramInt, paramArrayOfString);
  }

  public void playEarcon(String paramString, int paramInt, String[] paramArrayOfString)
  {
    if (!this.started)
      return;
    try
    {
      this.itts.playEarcon(paramString, paramInt, paramArrayOfString);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      this.started = false;
      initTts();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      this.started = false;
      initTts();
    }
  }

  public void setEngine(TTSEngine paramTTSEngine)
  {
    if (!this.started)
      return;
    try
    {
      this.itts.setEngine(paramTTSEngine.toString());
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
    }
  }

  public void setEngine(String paramString)
  {
    if (!this.started)
      return;
    try
    {
      this.itts.setEngine(paramString);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
    }
  }

  public void setLanguage(String paramString)
  {
    if (!this.started)
      return;
    try
    {
      this.itts.setLanguage(paramString);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
    }
  }

  public void setOnSpeechCompletedListener(SpeechCompletedListener paramSpeechCompletedListener)
  {
    this.speechCompletedCallback = paramSpeechCompletedListener;
  }

  public void setSpeechRate(int paramInt)
  {
    if (!this.started)
      return;
    try
    {
      this.itts.setSpeechRate(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
    }
  }

  public void showVersionAlert()
  {
    if (!this.started)
      return;
    if (this.versionAlert != null)
    {
      this.versionAlert.show();
      return;
    }
    new TTSVersionAlert(this.ctx, null, null, null).show();
  }

  public void shutdown()
  {
    try
    {
      this.ctx.unbindService(this.serviceConnection);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
    }
  }

  public void speak(String paramString, int paramInt, String[] paramArrayOfString)
  {
    Log.i("TTS received: ", paramString);
    if (!this.started)
      return;
    try
    {
      this.itts.speak(paramString, paramInt, paramArrayOfString);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      Log.e("TTS", "RemoteException error.");
      this.started = false;
      initTts();
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      Log.e("TTS", "NullPointerException error.");
      this.started = false;
      initTts();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      Log.e("TTS", "IllegalStateException error.");
      this.started = false;
      initTts();
    }
  }

  public void stop()
  {
    if (!this.started)
      return;
    try
    {
      this.itts.stop();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      this.started = false;
      initTts();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      this.started = false;
      initTts();
    }
  }

  public boolean synthesizeToFile(String paramString1, String[] paramArrayOfString, String paramString2)
  {
    if (!this.started)
      return false;
    try
    {
      boolean bool = this.itts.synthesizeToFile(paramString1, paramArrayOfString, paramString2);
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      this.started = false;
      initTts();
      return false;
    }
    catch (NullPointerException localNullPointerException)
    {
      this.started = false;
      initTts();
      return false;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      this.started = false;
      initTts();
    }
    return false;
  }

  public static abstract interface InitListener
  {
    public abstract void onInit(int paramInt);
  }

  public static abstract interface SpeechCompletedListener
  {
    public abstract void onSpeechCompleted();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.TTS
 * JD-Core Version:    0.6.2
 */