package com.google.tts;

public enum TTSEarcon
{
  static
  {
    CANCEL = new TTSEarcon("CANCEL", 3);
    TTSEarcon[] arrayOfTTSEarcon = new TTSEarcon[4];
    arrayOfTTSEarcon[0] = SILENCE;
    arrayOfTTSEarcon[1] = TICK;
    arrayOfTTSEarcon[2] = TOCK;
    arrayOfTTSEarcon[3] = CANCEL;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.TTSEarcon
 * JD-Core Version:    0.6.2
 */