package com.google.tts;

public enum TTSEngine
{
  static
  {
    ESPEAK = new TTSEngine("ESPEAK", 3);
    PICO = new TTSEngine("PICO", 4);
    TTSEngine[] arrayOfTTSEngine = new TTSEngine[5];
    arrayOfTTSEngine[0] = PRERECORDED_ONLY;
    arrayOfTTSEngine[1] = PRERECORDED_WITH_TTS;
    arrayOfTTSEngine[2] = TTS_ONLY;
    arrayOfTTSEngine[3] = ESPEAK;
    arrayOfTTSEngine[4] = PICO;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.TTSEngine
 * JD-Core Version:    0.6.2
 */