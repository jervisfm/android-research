package com.google.tts;

public enum TTSParams
{
  static
  {
    VOICE_FEMALE = new TTSParams("VOICE_FEMALE", 1);
    VOICE_ROBOT = new TTSParams("VOICE_ROBOT", 2);
    TTSParams[] arrayOfTTSParams = new TTSParams[3];
    arrayOfTTSParams[0] = VOICE_MALE;
    arrayOfTTSParams[1] = VOICE_FEMALE;
    arrayOfTTSParams[2] = VOICE_ROBOT;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.TTSParams
 * JD-Core Version:    0.6.2
 */