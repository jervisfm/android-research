package com.google.tts;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import javax.xml.parsers.FactoryConfigurationError;

public class TTSService extends Service
  implements MediaPlayer.OnCompletionListener
{
  private static final String ACTION = "android.intent.action.USE_TTS";
  private static final String BETA_ACTION = "com.google.intent.action.START_TTS_SERVICE_BETA";
  private static final String BETA_CATEGORY = "com.google.intent.category.TTS_BETA";
  private static final String CATEGORY = "android.intent.category.TTS";
  private static final int DEFAULT_STREAM_TYPE = 3;
  private static final String DEFAULT_SYNTH = "com.svox.pico";
  private static final int MAX_FILENAME_LENGTH = 250;
  private static final int MAX_SPEECH_ITEM_CHAR_LENGTH = 4000;
  private static final String PKGNAME = "android.tts";
  protected static final String SERVICE_TAG = "TtsService";
  private static final int SPEECHQUEUELOCK_TIMEOUT = 5000;
  static final HashMap<String, String> langRegionToLocale = new HashMap();
  private static SynthProxyBeta sNativeSynth = null;
  private String currentSpeechEngineSOFile = "";
  private boolean deprecatedKeepBlockingFlag = false;
  private final ITtsBeta.Stub mBinder = new ITtsBeta.Stub()
  {
    public void addEarcon(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3, int paramAnonymousInt)
    {
      TTSService.this.mSelf.addEarcon(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3, paramAnonymousInt);
    }

    public void addEarconFile(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3)
    {
      TTSService.this.mSelf.addEarcon(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3);
    }

    public void addSpeech(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3, int paramAnonymousInt)
    {
      TTSService.this.mSelf.addSpeech(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3, paramAnonymousInt);
    }

    public void addSpeechFile(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3)
    {
      TTSService.this.mSelf.addSpeech(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3);
    }

    public boolean areDefaultsEnforced()
    {
      return TTSService.this.mSelf.isDefaultEnforced();
    }

    public String getDefaultEngine()
    {
      return TTSService.this.mSelf.getDefaultEngine();
    }

    public String[] getLanguage()
    {
      return TTSService.this.mSelf.getLanguage();
    }

    public int isLanguageAvailable(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3, String[] paramAnonymousArrayOfString)
    {
      for (int i = 0; ; i += 2)
      {
        if (i >= -1 + paramAnonymousArrayOfString.length);
        while (true)
        {
          return TTSService.this.mSelf.isLanguageAvailable(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3);
          String str = paramAnonymousArrayOfString[i];
          if ((str == null) || (!str.equals("engine")))
            break;
          TTSService.this.mSelf.setEngine(paramAnonymousArrayOfString[(i + 1)]);
        }
      }
    }

    public boolean isSpeaking()
    {
      return (TTSService.access$2(TTSService.this).mIsSpeaking) && (TTSService.this.mSpeechQueue.size() < 1);
    }

    public int playEarcon(String paramAnonymousString1, String paramAnonymousString2, int paramAnonymousInt, String[] paramAnonymousArrayOfString)
    {
      ArrayList localArrayList = new ArrayList();
      if (paramAnonymousArrayOfString != null)
        localArrayList = new ArrayList(Arrays.asList(paramAnonymousArrayOfString));
      return TTSService.this.mSelf.playEarcon(paramAnonymousString1, paramAnonymousString2, paramAnonymousInt, localArrayList);
    }

    public int playSilence(String paramAnonymousString, long paramAnonymousLong, int paramAnonymousInt, String[] paramAnonymousArrayOfString)
    {
      ArrayList localArrayList = new ArrayList();
      if (paramAnonymousArrayOfString != null)
        localArrayList = new ArrayList(Arrays.asList(paramAnonymousArrayOfString));
      return TTSService.this.mSelf.playSilence(paramAnonymousString, paramAnonymousLong, paramAnonymousInt, localArrayList);
    }

    public int registerCallback(String paramAnonymousString, ITtsCallbackBeta paramAnonymousITtsCallbackBeta)
    {
      if (paramAnonymousITtsCallbackBeta != null)
      {
        TTSService.this.mCallbacks.register(paramAnonymousITtsCallbackBeta);
        TTSService.this.mCallbacksMap.put(paramAnonymousString, paramAnonymousITtsCallbackBeta);
        return 0;
      }
      return -1;
    }

    public int setEngineByPackageName(String paramAnonymousString)
    {
      return TTSService.this.mSelf.setEngine(paramAnonymousString);
    }

    public int setLanguage(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3, String paramAnonymousString4)
    {
      return TTSService.this.mSelf.setLanguage(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3, paramAnonymousString4);
    }

    public int setPitch(String paramAnonymousString, int paramAnonymousInt)
    {
      return TTSService.this.mSelf.setPitch(paramAnonymousString, paramAnonymousInt);
    }

    public int setSpeechRate(String paramAnonymousString, int paramAnonymousInt)
    {
      return TTSService.this.mSelf.setSpeechRate(paramAnonymousString, paramAnonymousInt);
    }

    public int speak(String paramAnonymousString1, String paramAnonymousString2, int paramAnonymousInt, String[] paramAnonymousArrayOfString)
    {
      ArrayList localArrayList = new ArrayList();
      if (paramAnonymousArrayOfString != null)
        localArrayList = new ArrayList(Arrays.asList(paramAnonymousArrayOfString));
      return TTSService.this.mSelf.speak(paramAnonymousString1, paramAnonymousString2, paramAnonymousInt, localArrayList);
    }

    public int stop(String paramAnonymousString)
    {
      return TTSService.this.mSelf.stop(paramAnonymousString);
    }

    public boolean synthesizeToFile(String paramAnonymousString1, String paramAnonymousString2, String[] paramAnonymousArrayOfString, String paramAnonymousString3)
    {
      ArrayList localArrayList = new ArrayList();
      if (paramAnonymousArrayOfString != null)
        localArrayList = new ArrayList(Arrays.asList(paramAnonymousArrayOfString));
      return TTSService.this.mSelf.synthesizeToFile(paramAnonymousString1, paramAnonymousString2, localArrayList, paramAnonymousString3);
    }

    public int unregisterCallback(String paramAnonymousString, ITtsCallbackBeta paramAnonymousITtsCallbackBeta)
    {
      if (paramAnonymousITtsCallbackBeta != null)
      {
        TTSService.this.mCallbacksMap.remove(paramAnonymousString);
        TTSService.this.mCallbacks.unregister(paramAnonymousITtsCallbackBeta);
        return 0;
      }
      return -1;
    }
  };
  private final ITTS.Stub mBinderOld = new ITTS.Stub()
  {
    public void addEarcon(String paramAnonymousString1, String paramAnonymousString2, int paramAnonymousInt)
    {
      TTSService.this.mSelf.addEarcon("DEPRECATED", paramAnonymousString1, paramAnonymousString2, paramAnonymousInt);
    }

    public void addEarconFile(String paramAnonymousString1, String paramAnonymousString2)
    {
      TTSService.this.mSelf.addEarcon("DEPRECATED", paramAnonymousString1, paramAnonymousString2);
    }

    public void addSpeech(String paramAnonymousString1, String paramAnonymousString2, int paramAnonymousInt)
    {
      TTSService.this.mSelf.addSpeech("DEPRECATED", paramAnonymousString1, paramAnonymousString2, paramAnonymousInt);
    }

    public void addSpeechFile(String paramAnonymousString1, String paramAnonymousString2)
    {
      TTSService.this.mSelf.addSpeech("DEPRECATED", paramAnonymousString1, paramAnonymousString2);
    }

    public int getVersion()
    {
      Object localObject = new PackageInfo();
      try
      {
        PackageInfo localPackageInfo = TTSService.this.mSelf.getPackageManager().getPackageInfo(TTSService.this.mSelf.getPackageName(), 0);
        localObject = localPackageInfo;
        return ((PackageInfo)localObject).versionCode;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        while (true)
          localNameNotFoundException.printStackTrace();
      }
    }

    public boolean isSpeaking()
    {
      return (TTSService.access$2(TTSService.this).mIsSpeaking) && (TTSService.this.mSpeechQueue.size() < 1);
    }

    public void playEarcon(String paramAnonymousString, int paramAnonymousInt, String[] paramAnonymousArrayOfString)
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add("utteranceId");
      localArrayList.add("DEPRECATED");
      TTSService.this.mSelf.playEarcon("DEPRECATED", paramAnonymousString, paramAnonymousInt, localArrayList);
    }

    public void registerCallback(ITTSCallback paramAnonymousITTSCallback)
    {
      if (paramAnonymousITTSCallback != null)
        TTSService.this.mCallbacksOld.register(paramAnonymousITTSCallback);
    }

    public void setEngine(String paramAnonymousString)
    {
      if ((!paramAnonymousString.equals(TTSEngine.TTS_ONLY.toString())) && (!paramAnonymousString.equals(TTSEngine.PRERECORDED_WITH_TTS.toString())) && (!paramAnonymousString.equals(TTSEngine.PRERECORDED_ONLY.toString())))
      {
        if (paramAnonymousString.equals(TTSEngine.ESPEAK.toString()))
          TTSService.this.mSelf.setEngine("/data/data/com.google.tts/lib/libespeakengine.so");
      }
      else
        return;
      if (paramAnonymousString.equals(TTSEngine.PICO.toString()))
      {
        TTSService.this.mSelf.setEngine("/system/lib/libttspico.so");
        return;
      }
      TTSService.this.mSelf.setEngine(paramAnonymousString);
    }

    public void setLanguage(String paramAnonymousString)
    {
      if (paramAnonymousString.length() == 3)
      {
        new Locale(paramAnonymousString);
        TTSService.this.mSelf.setLanguage("DEPRECATED", paramAnonymousString, "", "");
        return;
      }
      if (paramAnonymousString.length() == 7)
      {
        new Locale(paramAnonymousString.substring(0, 3), paramAnonymousString.substring(4, 7));
        TTSService.this.mSelf.setLanguage("DEPRECATED", paramAnonymousString.substring(0, 3), paramAnonymousString.substring(4, 7), "");
        return;
      }
      String str1 = (String)TTSService.langRegionToLocale.get(paramAnonymousString);
      String str2 = "";
      String str3 = "";
      String str4 = "";
      if (str1 == null)
      {
        Log.e("TtsService", "Error: " + paramAnonymousString + " not supported.");
        return;
      }
      if (str1.length() > 2)
        str2 = str1.substring(0, 3);
      if (str1.length() > 6)
        str3 = str1.substring(4, 7);
      if (str1.length() > 8)
        str4 = str1.substring(8);
      new Locale(str2, str3, str4);
      TTSService.this.mSelf.setLanguage("DEPRECATED", str2, str3, str4);
    }

    public void setSpeechRate(int paramAnonymousInt)
    {
      TTSService.this.mSelf.setSpeechRate("DEPRECATED", paramAnonymousInt);
    }

    public void speak(String paramAnonymousString, int paramAnonymousInt, String[] paramAnonymousArrayOfString)
    {
      ArrayList localArrayList = new ArrayList();
      if ((paramAnonymousArrayOfString != null) && (paramAnonymousString.length() == 1))
      {
        if (paramAnonymousArrayOfString[0].equals(TTSParams.VOICE_FEMALE.toString()))
          paramAnonymousString = paramAnonymousString + "[fem]";
        if (paramAnonymousArrayOfString[0].equals(TTSParams.VOICE_ROBOT.toString()))
          paramAnonymousString = paramAnonymousString + "[robot]";
      }
      localArrayList.add("utteranceId");
      localArrayList.add("DEPRECATED");
      TTSService.this.mSelf.speak("DEPRECATED", paramAnonymousString, paramAnonymousInt, localArrayList);
    }

    public void stop()
    {
      TTSService.this.mSelf.stopAll("DEPRECATED");
    }

    public boolean synthesizeToFile(String paramAnonymousString1, String[] paramAnonymousArrayOfString, String paramAnonymousString2)
    {
      ArrayList localArrayList = new ArrayList();
      if (paramAnonymousArrayOfString != null)
        localArrayList = new ArrayList(Arrays.asList(paramAnonymousArrayOfString));
      boolean bool = TTSService.this.mSelf.synthesizeToFile("DEPRECATED", paramAnonymousString1, localArrayList, paramAnonymousString2);
      if (bool)
        TTSService.this.deprecatedKeepBlockingFlag = true;
      while (true)
      {
        if (!TTSService.this.deprecatedKeepBlockingFlag)
          return bool;
        try
        {
          Thread.sleep(500L);
        }
        catch (InterruptedException localInterruptedException)
        {
          localInterruptedException.printStackTrace();
        }
      }
    }

    public void unregisterCallback(ITTSCallback paramAnonymousITTSCallback)
    {
      if (paramAnonymousITTSCallback != null)
        TTSService.this.mCallbacksOld.unregister(paramAnonymousITTSCallback);
    }
  };
  private final RemoteCallbackList<ITtsCallbackBeta> mCallbacks = new RemoteCallbackList();
  private HashMap<String, ITtsCallbackBeta> mCallbacksMap;
  private final RemoteCallbackList<ITTSCallback> mCallbacksOld = new RemoteCallbackList();
  private SpeechItem mCurrentSpeechItem;
  private HashMap<String, SoundResource> mEarcons;
  private boolean mIsSpeaking;
  private HashMap<SpeechItem, Boolean> mKillList;
  private MediaPlayer mPlayer;
  private TTSService mSelf;
  private ArrayList<SpeechItem> mSpeechQueue;
  private boolean mSynthBusy;
  private HashMap<String, SoundResource> mUtterances;
  private final ReentrantLock speechQueueLock = new ReentrantLock();
  boolean synthThreadBusy = false;
  private final ReentrantLock synthesizerLock = new ReentrantLock();

  static
  {
    langRegionToLocale.put("af", "afr");
    langRegionToLocale.put("bs", "bos");
    langRegionToLocale.put("zh-rHK", "yue");
    langRegionToLocale.put("zh", "cmn");
    langRegionToLocale.put("hr", "hrv");
    langRegionToLocale.put("cz", "ces");
    langRegionToLocale.put("cs", "ces");
    langRegionToLocale.put("nl", "nld");
    langRegionToLocale.put("en", "eng");
    langRegionToLocale.put("en-rUS", "eng-USA");
    langRegionToLocale.put("en-rGB", "eng-GBR");
    langRegionToLocale.put("eo", "epo");
    langRegionToLocale.put("fi", "fin");
    langRegionToLocale.put("fr", "fra");
    langRegionToLocale.put("fr-rFR", "fra-FRA");
    langRegionToLocale.put("de", "deu");
    langRegionToLocale.put("de-rDE", "deu-DEU");
    langRegionToLocale.put("el", "ell");
    langRegionToLocale.put("hi", "hin");
    langRegionToLocale.put("hu", "hun");
    langRegionToLocale.put("is", "isl");
    langRegionToLocale.put("id", "ind");
    langRegionToLocale.put("it", "ita");
    langRegionToLocale.put("it-rIT", "ita-ITA");
    langRegionToLocale.put("ku", "kur");
    langRegionToLocale.put("la", "lat");
    langRegionToLocale.put("mk", "mkd");
    langRegionToLocale.put("no", "nor");
    langRegionToLocale.put("pl", "pol");
    langRegionToLocale.put("pt", "por");
    langRegionToLocale.put("ro", "ron");
    langRegionToLocale.put("ru", "rus");
    langRegionToLocale.put("sr", "srp");
    langRegionToLocale.put("sk", "slk");
    langRegionToLocale.put("es", "spa");
    langRegionToLocale.put("es-rES", "spa-ESP");
    langRegionToLocale.put("es-rMX", "spa-MEX");
    langRegionToLocale.put("sw", "swa");
    langRegionToLocale.put("sv", "swe");
    langRegionToLocale.put("ta", "tam");
    langRegionToLocale.put("tr", "tur");
    langRegionToLocale.put("vi", "vie");
    langRegionToLocale.put("cy", "cym");
  }

  private void addEarcon(String paramString1, String paramString2, String paramString3)
  {
    this.mEarcons.put(paramString2, new SoundResource(paramString3));
  }

  private void addEarcon(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    this.mEarcons.put(paramString2, new SoundResource(paramString3, paramInt));
  }

  private void addSpeech(String paramString1, String paramString2, String paramString3)
  {
    this.mUtterances.put(paramString2, new SoundResource(paramString3));
  }

  private void addSpeech(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    this.mUtterances.put(paramString2, new SoundResource(paramString3, paramInt));
  }

  private void broadcastTtsQueueProcessingCompleted()
  {
    sendBroadcast(new Intent("android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED"));
  }

  private void cleanUpPlayer()
  {
    if (this.mPlayer != null)
    {
      this.mPlayer.release();
      this.mPlayer = null;
    }
  }

  // ERROR //
  private void dispatchUtteranceCompletedCallback(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 269	com/google/tts/TTSService:mCallbacksOld	Landroid/os/RemoteCallbackList;
    //   4: invokevirtual 490	android/os/RemoteCallbackList:beginBroadcast	()I
    //   7: istore_3
    //   8: iconst_0
    //   9: istore 4
    //   11: iload 4
    //   13: iload_3
    //   14: if_icmplt +29 -> 43
    //   17: aload_0
    //   18: getfield 269	com/google/tts/TTSService:mCallbacksOld	Landroid/os/RemoteCallbackList;
    //   21: invokevirtual 493	android/os/RemoteCallbackList:finishBroadcast	()V
    //   24: aload_0
    //   25: getfield 302	com/google/tts/TTSService:mCallbacksMap	Ljava/util/HashMap;
    //   28: aload_2
    //   29: invokevirtual 497	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   32: checkcast 499	com/google/tts/ITtsCallbackBeta
    //   35: astore 7
    //   37: aload 7
    //   39: ifnonnull +33 -> 72
    //   42: return
    //   43: aload_0
    //   44: getfield 269	com/google/tts/TTSService:mCallbacksOld	Landroid/os/RemoteCallbackList;
    //   47: iload 4
    //   49: invokevirtual 503	android/os/RemoteCallbackList:getBroadcastItem	(I)Landroid/os/IInterface;
    //   52: checkcast 505	com/google/tts/ITTSCallback
    //   55: ldc_w 278
    //   58: invokeinterface 508 2 0
    //   63: iinc 4 1
    //   66: goto -55 -> 11
    //   69: astore 6
    //   71: return
    //   72: ldc 35
    //   74: ldc_w 510
    //   77: invokestatic 516	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   80: pop
    //   81: aload_0
    //   82: getfield 267	com/google/tts/TTSService:mCallbacks	Landroid/os/RemoteCallbackList;
    //   85: invokevirtual 490	android/os/RemoteCallbackList:beginBroadcast	()I
    //   88: istore 9
    //   90: aload 7
    //   92: aload_1
    //   93: invokeinterface 519 2 0
    //   98: aload_0
    //   99: getfield 267	com/google/tts/TTSService:mCallbacks	Landroid/os/RemoteCallbackList;
    //   102: invokevirtual 493	android/os/RemoteCallbackList:finishBroadcast	()V
    //   105: ldc 35
    //   107: new 521	java/lang/StringBuilder
    //   110: dup
    //   111: ldc_w 523
    //   114: invokespecial 524	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   117: iload 9
    //   119: invokevirtual 528	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   122: invokevirtual 531	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   125: invokestatic 516	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   128: pop
    //   129: return
    //   130: astore 10
    //   132: goto -34 -> 98
    //   135: astore 5
    //   137: goto -74 -> 63
    //
    // Exception table:
    //   from	to	target	type
    //   17	24	69	java/lang/IllegalStateException
    //   90	98	130	android/os/RemoteException
    //   43	63	135	android/os/RemoteException
  }

  private String getDefaultCountry()
  {
    return PreferenceManager.getDefaultSharedPreferences(this).getString("tts_default_country", Locale.getDefault().getISO3Country());
  }

  private String getDefaultEngine()
  {
    String str = PreferenceManager.getDefaultSharedPreferences(this).getString("tts_default_synth", "com.svox.pico");
    if (str == null)
      str = "com.svox.pico";
    return str;
  }

  private String getDefaultLanguage()
  {
    return PreferenceManager.getDefaultSharedPreferences(this).getString("tts_default_lang", Locale.getDefault().getISO3Language());
  }

  private String getDefaultLocVariant()
  {
    return PreferenceManager.getDefaultSharedPreferences(this).getString("tts_default_variant", Locale.getDefault().getVariant());
  }

  private int getDefaultPitch()
  {
    return 100;
  }

  private int getDefaultRate()
  {
    return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("rate_pref", "100"));
  }

  private String[] getLanguage()
  {
    try
    {
      String[] arrayOfString = sNativeSynth.getLanguage();
      return arrayOfString;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  private SoundResource getSoundResource(SpeechItem paramSpeechItem)
  {
    String str = paramSpeechItem.mText;
    int i = paramSpeechItem.mType;
    SoundResource localSoundResource = null;
    if (i != 2)
    {
      if (paramSpeechItem.mType == 1)
        localSoundResource = (SoundResource)this.mEarcons.get(str);
    }
    else
      return localSoundResource;
    return (SoundResource)this.mUtterances.get(str);
  }

  private int getStreamTypeFromParams(ArrayList<String> paramArrayList)
  {
    int i = 3;
    if (paramArrayList == null)
      return i;
    int j = 0;
    while (true)
    {
      if (j >= -1 + paramArrayList.size())
        return i;
      String str = (String)paramArrayList.get(j);
      int k;
      if ((str != null) && (str.equals("streamType")))
        k = j + 1;
      try
      {
        int m = Integer.parseInt((String)paramArrayList.get(k));
        i = m;
        j += 2;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        while (true)
          i = 3;
      }
    }
  }

  private boolean isDefaultEnforced()
  {
    return PreferenceManager.getDefaultSharedPreferences(this).getInt("toggle_use_default_tts_settings", 0) == 1;
  }

  private int isLanguageAvailable(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      int i = sNativeSynth.isLanguageAvailable(paramString1, paramString2, paramString3);
      return i;
    }
    catch (NullPointerException localNullPointerException)
    {
    }
    return -2;
  }

  private int killAllUtterances()
  {
    int i = -1;
    boolean bool = false;
    try
    {
      bool = this.speechQueueLock.tryLock(5000L, TimeUnit.MILLISECONDS);
      if (bool)
      {
        this.mSpeechQueue.clear();
        if (this.mCurrentSpeechItem != null)
        {
          i = sNativeSynth.stop();
          this.mKillList.put(this.mCurrentSpeechItem, Boolean.valueOf(true));
          this.mIsSpeaking = false;
          if ((this.mCurrentSpeechItem.mType == 3) && (this.mCurrentSpeechItem.mFilename != null))
          {
            File localFile = new File(this.mCurrentSpeechItem.mFilename);
            Log.v("TtsService", "Leaving behind " + this.mCurrentSpeechItem.mFilename);
            if (localFile.exists())
            {
              Log.v("TtsService", "About to delete " + this.mCurrentSpeechItem.mFilename);
              if (localFile.delete())
                Log.v("TtsService", "file successfully deleted");
            }
          }
          this.mCurrentSpeechItem = null;
        }
        label189: if (bool)
          this.speechQueueLock.unlock();
        return i;
      }
      Log.e("TtsService", "TTS killAllUtterances(): queue locked longer than expected");
      i = -1;
    }
    catch (InterruptedException localInterruptedException)
    {
      Log.e("TtsService", "TTS killAllUtterances(): tryLock interrupted");
      i = -1;
    }
    finally
    {
      break label189;
    }
  }

  private int playEarcon(String paramString1, String paramString2, int paramInt, ArrayList<String> paramArrayList)
  {
    if (paramInt == 0)
      stop(paramString1);
    while (true)
    {
      this.mSpeechQueue.add(new SpeechItem(paramString1, paramString2, paramArrayList, 1));
      if (!this.mIsSpeaking)
        processSpeechQueue();
      return 0;
      if (paramInt == 2)
        stopAll(paramString1);
    }
  }

  private int playSilence(String paramString, long paramLong, int paramInt, ArrayList<String> paramArrayList)
  {
    if (paramInt == 0)
      stop(paramString);
    this.mSpeechQueue.add(new SpeechItem(paramString, paramLong, paramArrayList));
    if (!this.mIsSpeaking)
      processSpeechQueue();
    return 0;
  }

  // ERROR //
  private void processSpeechQueue()
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield 396	com/google/tts/TTSService:mSynthBusy	Z
    //   8: ifeq +6 -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_0
    //   17: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   20: ldc2_w 622
    //   23: getstatic 629	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   26: invokevirtual 633	java/util/concurrent/locks/ReentrantLock:tryLock	(JLjava/util/concurrent/TimeUnit;)Z
    //   29: istore_1
    //   30: iload_1
    //   31: ifne +29 -> 60
    //   34: ldc 35
    //   36: ldc_w 690
    //   39: invokestatic 675	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   42: pop
    //   43: iload_1
    //   44: ifeq +462 -> 506
    //   47: aload_0
    //   48: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   51: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   54: return
    //   55: astore_2
    //   56: aload_0
    //   57: monitorexit
    //   58: aload_2
    //   59: athrow
    //   60: aload_0
    //   61: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   64: invokevirtual 599	java/util/ArrayList:size	()I
    //   67: iconst_1
    //   68: if_icmpge +50 -> 118
    //   71: aload_0
    //   72: iconst_0
    //   73: putfield 446	com/google/tts/TTSService:mIsSpeaking	Z
    //   76: aload_0
    //   77: getfield 405	com/google/tts/TTSService:mKillList	Ljava/util/HashMap;
    //   80: invokevirtual 691	java/util/HashMap:clear	()V
    //   83: aload_0
    //   84: invokespecial 693	com/google/tts/TTSService:broadcastTtsQueueProcessingCompleted	()V
    //   87: goto -44 -> 43
    //   90: astore 4
    //   92: ldc 35
    //   94: ldc_w 695
    //   97: invokestatic 675	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   100: pop
    //   101: aload 4
    //   103: invokevirtual 698	java/lang/InterruptedException:printStackTrace	()V
    //   106: iload_1
    //   107: ifeq +399 -> 506
    //   110: aload_0
    //   111: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   114: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   117: return
    //   118: aload_0
    //   119: aload_0
    //   120: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   123: iconst_0
    //   124: invokevirtual 602	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   127: checkcast 584	com/google/tts/TTSService$SpeechItem
    //   130: putfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   133: aload_0
    //   134: iconst_1
    //   135: putfield 446	com/google/tts/TTSService:mIsSpeaking	Z
    //   138: aload_0
    //   139: aload_0
    //   140: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   143: invokespecial 700	com/google/tts/TTSService:getSoundResource	(Lcom/google/tts/TTSService$SpeechItem;)Lcom/google/tts/TTSService$SoundResource;
    //   146: astore 7
    //   148: ldc 35
    //   150: new 521	java/lang/StringBuilder
    //   153: dup
    //   154: ldc_w 702
    //   157: invokespecial 524	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   160: aload_0
    //   161: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   164: getfield 587	com/google/tts/TTSService$SpeechItem:mText	Ljava/lang/String;
    //   167: invokevirtual 657	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   170: invokevirtual 531	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   173: invokestatic 516	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   176: pop
    //   177: aload 7
    //   179: ifnonnull +111 -> 290
    //   182: aload_0
    //   183: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   186: getfield 590	com/google/tts/TTSService$SpeechItem:mType	I
    //   189: ifne +54 -> 243
    //   192: aload_0
    //   193: aload_0
    //   194: aload_0
    //   195: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   198: invokespecial 706	com/google/tts/TTSService:splitCurrentTextIfNeeded	(Lcom/google/tts/TTSService$SpeechItem;)Lcom/google/tts/TTSService$SpeechItem;
    //   201: putfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   204: aload_0
    //   205: aload_0
    //   206: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   209: invokespecial 710	com/google/tts/TTSService:speakInternalOnly	(Lcom/google/tts/TTSService$SpeechItem;)V
    //   212: aload_0
    //   213: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   216: invokevirtual 599	java/util/ArrayList:size	()I
    //   219: ifle +12 -> 231
    //   222: aload_0
    //   223: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   226: iconst_0
    //   227: invokevirtual 713	java/util/ArrayList:remove	(I)Ljava/lang/Object;
    //   230: pop
    //   231: iload_1
    //   232: ifeq +274 -> 506
    //   235: aload_0
    //   236: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   239: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   242: return
    //   243: aload_0
    //   244: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   247: getfield 590	com/google/tts/TTSService$SpeechItem:mType	I
    //   250: iconst_3
    //   251: if_icmpne +28 -> 279
    //   254: aload_0
    //   255: aload_0
    //   256: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   259: invokespecial 716	com/google/tts/TTSService:synthToFileInternalOnly	(Lcom/google/tts/TTSService$SpeechItem;)V
    //   262: goto -50 -> 212
    //   265: astore_3
    //   266: iload_1
    //   267: ifeq +10 -> 277
    //   270: aload_0
    //   271: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   274: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   277: aload_3
    //   278: athrow
    //   279: aload_0
    //   280: aload_0
    //   281: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   284: invokespecial 719	com/google/tts/TTSService:silence	(Lcom/google/tts/TTSService$SpeechItem;)V
    //   287: goto -75 -> 212
    //   290: aload_0
    //   291: invokespecial 721	com/google/tts/TTSService:cleanUpPlayer	()V
    //   294: aload 7
    //   296: getfield 724	com/google/tts/TTSService$SoundResource:mSourcePackageName	Ljava/lang/String;
    //   299: ldc 32
    //   301: if_acmpne +47 -> 348
    //   304: aload_0
    //   305: aload_0
    //   306: aload 7
    //   308: getfield 727	com/google/tts/TTSService$SoundResource:mResId	I
    //   311: invokestatic 731	android/media/MediaPlayer:create	(Landroid/content/Context;I)Landroid/media/MediaPlayer;
    //   314: putfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   317: aload_0
    //   318: getfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   321: ifnonnull +120 -> 441
    //   324: aload_0
    //   325: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   328: invokevirtual 636	java/util/ArrayList:clear	()V
    //   331: aload_0
    //   332: iconst_0
    //   333: putfield 446	com/google/tts/TTSService:mIsSpeaking	Z
    //   336: iload_1
    //   337: ifeq +169 -> 506
    //   340: aload_0
    //   341: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   344: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   347: return
    //   348: aload 7
    //   350: getfield 724	com/google/tts/TTSService$SoundResource:mSourcePackageName	Ljava/lang/String;
    //   353: astore 10
    //   355: aload 10
    //   357: ifnull +65 -> 422
    //   360: aload_0
    //   361: aload 7
    //   363: getfield 724	com/google/tts/TTSService$SoundResource:mSourcePackageName	Ljava/lang/String;
    //   366: iconst_0
    //   367: invokevirtual 735	com/google/tts/TTSService:createPackageContext	(Ljava/lang/String;I)Landroid/content/Context;
    //   370: astore 14
    //   372: aload_0
    //   373: aload 14
    //   375: aload 7
    //   377: getfield 727	com/google/tts/TTSService$SoundResource:mResId	I
    //   380: invokestatic 731	android/media/MediaPlayer:create	(Landroid/content/Context;I)Landroid/media/MediaPlayer;
    //   383: putfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   386: goto -69 -> 317
    //   389: astore 12
    //   391: aload 12
    //   393: invokevirtual 736	android/content/pm/PackageManager$NameNotFoundException:printStackTrace	()V
    //   396: aload_0
    //   397: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   400: iconst_0
    //   401: invokevirtual 713	java/util/ArrayList:remove	(I)Ljava/lang/Object;
    //   404: pop
    //   405: aload_0
    //   406: iconst_0
    //   407: putfield 446	com/google/tts/TTSService:mIsSpeaking	Z
    //   410: iload_1
    //   411: ifeq +95 -> 506
    //   414: aload_0
    //   415: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   418: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   421: return
    //   422: aload_0
    //   423: aload_0
    //   424: aload 7
    //   426: getfield 737	com/google/tts/TTSService$SoundResource:mFilename	Ljava/lang/String;
    //   429: invokestatic 743	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   432: invokestatic 746	android/media/MediaPlayer:create	(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;
    //   435: putfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   438: goto -121 -> 317
    //   441: aload_0
    //   442: getfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   445: aload_0
    //   446: invokevirtual 750	android/media/MediaPlayer:setOnCompletionListener	(Landroid/media/MediaPlayer$OnCompletionListener;)V
    //   449: aload_0
    //   450: getfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   453: aload_0
    //   454: aload_0
    //   455: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   458: getfield 753	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
    //   461: invokespecial 755	com/google/tts/TTSService:getStreamTypeFromParams	(Ljava/util/ArrayList;)I
    //   464: invokevirtual 759	android/media/MediaPlayer:setAudioStreamType	(I)V
    //   467: aload_0
    //   468: getfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   471: invokevirtual 762	android/media/MediaPlayer:start	()V
    //   474: goto -262 -> 212
    //   477: astore 11
    //   479: aload_0
    //   480: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   483: invokevirtual 636	java/util/ArrayList:clear	()V
    //   486: aload_0
    //   487: iconst_0
    //   488: putfield 446	com/google/tts/TTSService:mIsSpeaking	Z
    //   491: aload_0
    //   492: invokespecial 721	com/google/tts/TTSService:cleanUpPlayer	()V
    //   495: iload_1
    //   496: ifeq +10 -> 506
    //   499: aload_0
    //   500: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   503: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   506: return
    //
    // Exception table:
    //   from	to	target	type
    //   4	13	55	finally
    //   14	16	55	finally
    //   56	58	55	finally
    //   16	30	90	java/lang/InterruptedException
    //   34	43	90	java/lang/InterruptedException
    //   60	87	90	java/lang/InterruptedException
    //   118	177	90	java/lang/InterruptedException
    //   182	212	90	java/lang/InterruptedException
    //   212	231	90	java/lang/InterruptedException
    //   243	262	90	java/lang/InterruptedException
    //   279	287	90	java/lang/InterruptedException
    //   290	317	90	java/lang/InterruptedException
    //   317	336	90	java/lang/InterruptedException
    //   348	355	90	java/lang/InterruptedException
    //   360	372	90	java/lang/InterruptedException
    //   372	386	90	java/lang/InterruptedException
    //   391	410	90	java/lang/InterruptedException
    //   422	438	90	java/lang/InterruptedException
    //   441	449	90	java/lang/InterruptedException
    //   449	474	90	java/lang/InterruptedException
    //   479	495	90	java/lang/InterruptedException
    //   16	30	265	finally
    //   34	43	265	finally
    //   60	87	265	finally
    //   92	106	265	finally
    //   118	177	265	finally
    //   182	212	265	finally
    //   212	231	265	finally
    //   243	262	265	finally
    //   279	287	265	finally
    //   290	317	265	finally
    //   317	336	265	finally
    //   348	355	265	finally
    //   360	372	265	finally
    //   372	386	265	finally
    //   391	410	265	finally
    //   422	438	265	finally
    //   441	449	265	finally
    //   449	474	265	finally
    //   479	495	265	finally
    //   360	372	389	android/content/pm/PackageManager$NameNotFoundException
    //   449	474	477	java/lang/IllegalStateException
  }

  private void setDefaultSettings()
  {
    setLanguage("", getDefaultLanguage(), getDefaultCountry(), getDefaultLocVariant());
    setSpeechRate("", getDefaultRate());
  }

  private int setEngine(String paramString)
  {
    if (isDefaultEnforced())
      paramString = getDefaultEngine();
    int i = 4;
    try
    {
      int m = Integer.parseInt(Build.VERSION.SDK);
      i = m;
      if ((i < 4) && (paramString.equals("com.svox.pico")))
        paramString = "com.google.tts";
      if (paramString.equals("com.svox.pico"))
        if (i < 5)
        {
          str2 = "/data/data/com.google.tts/lib/libttspico_4.so";
          if (!this.currentSpeechEngineSOFile.equals(str2))
            break label459;
          return 0;
        }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      String str2;
      while (true)
      {
        Log.e("TtsService", "Unable to parse SDK version: " + Build.VERSION.SDK);
        continue;
        str2 = "/data/data/com.google.tts/lib/libttspico.so";
        continue;
        Intent localIntent = new Intent("android.intent.action.START_TTS_ENGINE");
        ResolveInfo[] arrayOfResolveInfo1 = new ResolveInfo[0];
        ResolveInfo[] arrayOfResolveInfo2 = (ResolveInfo[])getPackageManager().queryIntentActivities(localIntent, 0).toArray(arrayOfResolveInfo1);
        ActivityInfo localActivityInfo;
        for (int j = 0; ; j++)
        {
          int k = arrayOfResolveInfo2.length;
          localActivityInfo = null;
          if (j >= k);
          while (true)
          {
            if (localActivityInfo != null)
              break label238;
            Log.e("TtsService", "Invalid TTS Engine Package: " + paramString);
            return -1;
            if (!arrayOfResolveInfo2[j].activityInfo.packageName.equals(paramString))
              break;
            localActivityInfo = arrayOfResolveInfo2[j].activityInfo;
          }
        }
        label238: if (i < 5)
          i = 4;
        String str1 = (localActivityInfo.name.replace(new StringBuilder(String.valueOf(localActivityInfo.packageName)).append(".").toString(), "") + "_" + i + ".so").toLowerCase();
        str2 = "/data/data/" + localActivityInfo.packageName + "/lib/libtts" + str1;
        if (!new File(str2).exists())
        {
          String str3 = (localActivityInfo.name.replace(new StringBuilder(String.valueOf(localActivityInfo.packageName)).append(".").toString(), "") + ".so").toLowerCase();
          str2 = "/data/data/" + localActivityInfo.packageName + "/lib/libtts" + str3;
        }
      }
      label459: if (!new File(str2).exists())
      {
        Log.e("TtsService", "Invalid TTS Binary: " + str2);
        return -1;
      }
      if (sNativeSynth != null)
      {
        sNativeSynth.stop();
        sNativeSynth.shutdown();
        sNativeSynth = null;
      }
      sNativeSynth = new SynthProxyBeta(str2);
      this.currentSpeechEngineSOFile = str2;
    }
    return 0;
  }

  private int setLanguage(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Log.v("TtsService", "TtsService.setLanguage(" + paramString2 + ", " + paramString3 + ", " + paramString4 + ")");
    try
    {
      if (isDefaultEnforced())
        return sNativeSynth.setLanguage(getDefaultLanguage(), getDefaultCountry(), getDefaultLocVariant());
      int i = sNativeSynth.setLanguage(paramString2, paramString3, paramString4);
      return i;
    }
    catch (NullPointerException localNullPointerException)
    {
    }
    return -1;
  }

  private int setPitch(String paramString, int paramInt)
  {
    try
    {
      int i = sNativeSynth.setPitch(paramInt);
      return i;
    }
    catch (NullPointerException localNullPointerException)
    {
    }
    return -1;
  }

  private int setSpeechRate(String paramString, int paramInt)
  {
    try
    {
      if (isDefaultEnforced())
        return sNativeSynth.setSpeechRate(getDefaultRate());
      int i = sNativeSynth.setSpeechRate(paramInt);
      return i;
    }
    catch (NullPointerException localNullPointerException)
    {
    }
    return -1;
  }

  private void silence(final SpeechItem paramSpeechItem)
  {
    Thread localThread = new Thread(new Runnable()
    {
      public void run()
      {
        String str = "";
        int i;
        if (paramSpeechItem.mParams != null)
          i = 0;
        while (true)
        {
          if (i >= -1 + paramSpeechItem.mParams.size());
          try
          {
            Thread.sleep(paramSpeechItem.mDuration);
            return;
            if (((String)paramSpeechItem.mParams.get(i)).equals("utteranceId"))
              str = (String)paramSpeechItem.mParams.get(i + 1);
            i += 2;
          }
          catch (InterruptedException localInterruptedException)
          {
            localInterruptedException.printStackTrace();
            return;
          }
          finally
          {
            if (str.length() > 0)
              TTSService.this.dispatchUtteranceCompletedCallback(str, paramSpeechItem.mCallingApp);
            TTSService.this.processSpeechQueue();
          }
        }
      }
    });
    localThread.setPriority(1);
    localThread.start();
  }

  private int speak(String paramString1, String paramString2, int paramInt, ArrayList<String> paramArrayList)
  {
    Log.v("TtsService", "TTS service received " + paramString2);
    if (paramInt == 0)
      stop(paramString1);
    while (true)
    {
      this.mSpeechQueue.add(new SpeechItem(paramString1, paramString2, paramArrayList, 0));
      if (!this.mIsSpeaking)
        processSpeechQueue();
      return 0;
      if (paramInt == 2)
        stopAll(paramString1);
    }
  }

  private void speakInternalOnly(final SpeechItem paramSpeechItem)
  {
    Log.e("TtsService", "Creating synth thread for: " + paramSpeechItem.mText);
    Thread localThread = new Thread(new Runnable()
    {
      // ERROR //
      public void run()
      {
        // Byte code:
        //   0: iconst_0
        //   1: istore_1
        //   2: ldc 28
        //   4: astore_2
        //   5: aload_0
        //   6: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   9: invokestatic 34	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   12: invokevirtual 40	java/util/concurrent/locks/ReentrantLock:tryLock	()Z
        //   15: istore_1
        //   16: iload_1
        //   17: ifne +94 -> 111
        //   20: aload_0
        //   21: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   24: iconst_1
        //   25: invokestatic 44	com/google/tts/TTSService:access$29	(Lcom/google/tts/TTSService;Z)V
        //   28: ldc2_w 45
        //   31: invokestatic 52	java/lang/Thread:sleep	(J)V
        //   34: new 48	java/lang/Thread
        //   37: dup
        //   38: new 2	com/google/tts/TTSService$1SynthThread
        //   41: dup
        //   42: aload_0
        //   43: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   46: aload_0
        //   47: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   50: invokespecial 54	com/google/tts/TTSService$1SynthThread:<init>	(Lcom/google/tts/TTSService;Lcom/google/tts/TTSService$SpeechItem;)V
        //   53: invokespecial 57	java/lang/Thread:<init>	(Ljava/lang/Runnable;)V
        //   56: invokevirtual 60	java/lang/Thread:start	()V
        //   59: aload_0
        //   60: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   63: iconst_0
        //   64: invokestatic 44	com/google/tts/TTSService:access$29	(Lcom/google/tts/TTSService;Z)V
        //   67: aload_2
        //   68: invokevirtual 66	java/lang/String:length	()I
        //   71: ifle +18 -> 89
        //   74: aload_0
        //   75: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   78: aload_2
        //   79: aload_0
        //   80: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   83: getfield 72	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
        //   86: invokestatic 76	com/google/tts/TTSService:access$26	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;)V
        //   89: iload_1
        //   90: ifeq +20 -> 110
        //   93: aload_0
        //   94: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   97: invokestatic 34	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   100: invokevirtual 79	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //   103: aload_0
        //   104: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   107: invokestatic 83	com/google/tts/TTSService:access$27	(Lcom/google/tts/TTSService;)V
        //   110: return
        //   111: iconst_3
        //   112: istore 6
        //   114: ldc 28
        //   116: astore 7
        //   118: ldc 28
        //   120: astore 8
        //   122: ldc 28
        //   124: astore 9
        //   126: ldc 28
        //   128: astore 10
        //   130: ldc 28
        //   132: astore 11
        //   134: ldc 28
        //   136: astore 12
        //   138: aload_0
        //   139: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   142: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   145: ifnull +23 -> 168
        //   148: iconst_0
        //   149: istore 13
        //   151: iload 13
        //   153: iconst_m1
        //   154: aload_0
        //   155: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   158: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   161: invokevirtual 92	java/util/ArrayList:size	()I
        //   164: iadd
        //   165: if_icmplt +168 -> 333
        //   168: aload_0
        //   169: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   172: invokestatic 96	com/google/tts/TTSService:access$30	(Lcom/google/tts/TTSService;)Ljava/util/HashMap;
        //   175: aload_0
        //   176: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   179: invokevirtual 102	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //   182: ifnonnull +107 -> 289
        //   185: aload 11
        //   187: invokevirtual 66	java/lang/String:length	()I
        //   190: ifle +439 -> 629
        //   193: aload_0
        //   194: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   197: aload 11
        //   199: invokestatic 106	com/google/tts/TTSService:access$15	(Lcom/google/tts/TTSService;Ljava/lang/String;)I
        //   202: pop
        //   203: aload 7
        //   205: invokevirtual 66	java/lang/String:length	()I
        //   208: ifle +498 -> 706
        //   211: aload_0
        //   212: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   215: ldc 28
        //   217: aload 7
        //   219: aload 8
        //   221: aload 9
        //   223: invokestatic 110	com/google/tts/TTSService:access$18	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //   226: pop
        //   227: aload 10
        //   229: invokevirtual 66	java/lang/String:length	()I
        //   232: ifle +554 -> 786
        //   235: aload_0
        //   236: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   239: ldc 28
        //   241: aload 10
        //   243: invokestatic 116	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //   246: invokestatic 120	com/google/tts/TTSService:access$13	(Lcom/google/tts/TTSService;Ljava/lang/String;I)I
        //   249: pop
        //   250: aload 12
        //   252: invokevirtual 66	java/lang/String:length	()I
        //   255: ifle +551 -> 806
        //   258: aload_0
        //   259: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   262: ldc 28
        //   264: aload 12
        //   266: invokestatic 116	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //   269: invokestatic 123	com/google/tts/TTSService:access$14	(Lcom/google/tts/TTSService;Ljava/lang/String;I)I
        //   272: pop
        //   273: invokestatic 127	com/google/tts/TTSService:access$36	()Lcom/google/tts/SynthProxyBeta;
        //   276: aload_0
        //   277: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   280: getfield 130	com/google/tts/TTSService$SpeechItem:mText	Ljava/lang/String;
        //   283: iload 6
        //   285: invokevirtual 136	com/google/tts/SynthProxyBeta:speak	(Ljava/lang/String;I)I
        //   288: pop
        //   289: aload_2
        //   290: invokevirtual 66	java/lang/String:length	()I
        //   293: ifle +18 -> 311
        //   296: aload_0
        //   297: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   300: aload_2
        //   301: aload_0
        //   302: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   305: getfield 72	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
        //   308: invokestatic 76	com/google/tts/TTSService:access$26	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;)V
        //   311: iload_1
        //   312: ifeq -202 -> 110
        //   315: aload_0
        //   316: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   319: invokestatic 34	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   322: invokevirtual 79	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //   325: aload_0
        //   326: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   329: invokestatic 83	com/google/tts/TTSService:access$27	(Lcom/google/tts/TTSService;)V
        //   332: return
        //   333: aload_0
        //   334: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   337: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   340: iload 13
        //   342: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   345: checkcast 62	java/lang/String
        //   348: astore 25
        //   350: aload 25
        //   352: ifnull +487 -> 839
        //   355: aload 25
        //   357: ldc 141
        //   359: invokevirtual 145	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   362: ifeq +25 -> 387
        //   365: aload_0
        //   366: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   369: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   372: iload 13
        //   374: iconst_1
        //   375: iadd
        //   376: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   379: checkcast 62	java/lang/String
        //   382: astore 10
        //   384: goto +455 -> 839
        //   387: aload 25
        //   389: ldc 147
        //   391: invokevirtual 145	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   394: ifeq +25 -> 419
        //   397: aload_0
        //   398: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   401: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   404: iload 13
        //   406: iconst_1
        //   407: iadd
        //   408: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   411: checkcast 62	java/lang/String
        //   414: astore 7
        //   416: goto +423 -> 839
        //   419: aload 25
        //   421: ldc 149
        //   423: invokevirtual 145	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   426: ifeq +25 -> 451
        //   429: aload_0
        //   430: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   433: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   436: iload 13
        //   438: iconst_1
        //   439: iadd
        //   440: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   443: checkcast 62	java/lang/String
        //   446: astore 8
        //   448: goto +391 -> 839
        //   451: aload 25
        //   453: ldc 151
        //   455: invokevirtual 145	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   458: ifeq +25 -> 483
        //   461: aload_0
        //   462: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   465: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   468: iload 13
        //   470: iconst_1
        //   471: iadd
        //   472: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   475: checkcast 62	java/lang/String
        //   478: astore 9
        //   480: goto +359 -> 839
        //   483: aload 25
        //   485: ldc 153
        //   487: invokevirtual 145	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   490: ifeq +24 -> 514
        //   493: aload_0
        //   494: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   497: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   500: iload 13
        //   502: iconst_1
        //   503: iadd
        //   504: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   507: checkcast 62	java/lang/String
        //   510: astore_2
        //   511: goto +328 -> 839
        //   514: aload 25
        //   516: ldc 155
        //   518: invokevirtual 145	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   521: istore 26
        //   523: iload 26
        //   525: ifeq +40 -> 565
        //   528: aload_0
        //   529: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   532: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   535: iload 13
        //   537: iconst_1
        //   538: iadd
        //   539: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   542: checkcast 62	java/lang/String
        //   545: invokestatic 116	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //   548: istore 28
        //   550: iload 28
        //   552: istore 6
        //   554: goto +285 -> 839
        //   557: astore 27
        //   559: iconst_3
        //   560: istore 6
        //   562: goto +277 -> 839
        //   565: aload 25
        //   567: ldc 157
        //   569: invokevirtual 145	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   572: ifeq +25 -> 597
        //   575: aload_0
        //   576: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   579: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   582: iload 13
        //   584: iconst_1
        //   585: iadd
        //   586: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   589: checkcast 62	java/lang/String
        //   592: astore 11
        //   594: goto +245 -> 839
        //   597: aload 25
        //   599: ldc 159
        //   601: invokevirtual 145	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   604: ifeq +235 -> 839
        //   607: aload_0
        //   608: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   611: getfield 87	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   614: iload 13
        //   616: iconst_1
        //   617: iadd
        //   618: invokevirtual 139	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   621: checkcast 62	java/lang/String
        //   624: astore 12
        //   626: goto +213 -> 839
        //   629: aload_0
        //   630: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   633: aload_0
        //   634: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   637: invokestatic 163	com/google/tts/TTSService:access$20	(Lcom/google/tts/TTSService;)Ljava/lang/String;
        //   640: invokestatic 106	com/google/tts/TTSService:access$15	(Lcom/google/tts/TTSService;Ljava/lang/String;)I
        //   643: pop
        //   644: goto -441 -> 203
        //   647: astore 4
        //   649: ldc 165
        //   651: ldc 167
        //   653: invokestatic 173	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //   656: pop
        //   657: aload 4
        //   659: invokevirtual 176	java/lang/InterruptedException:printStackTrace	()V
        //   662: aload_2
        //   663: invokevirtual 66	java/lang/String:length	()I
        //   666: ifle +18 -> 684
        //   669: aload_0
        //   670: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   673: aload_2
        //   674: aload_0
        //   675: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   678: getfield 72	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
        //   681: invokestatic 76	com/google/tts/TTSService:access$26	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;)V
        //   684: iload_1
        //   685: ifeq -575 -> 110
        //   688: aload_0
        //   689: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   692: invokestatic 34	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   695: invokevirtual 79	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //   698: aload_0
        //   699: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   702: invokestatic 83	com/google/tts/TTSService:access$27	(Lcom/google/tts/TTSService;)V
        //   705: return
        //   706: aload_0
        //   707: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   710: ldc 28
        //   712: aload_0
        //   713: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   716: invokestatic 179	com/google/tts/TTSService:access$31	(Lcom/google/tts/TTSService;)Ljava/lang/String;
        //   719: aload_0
        //   720: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   723: invokestatic 182	com/google/tts/TTSService:access$32	(Lcom/google/tts/TTSService;)Ljava/lang/String;
        //   726: aload_0
        //   727: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   730: invokestatic 185	com/google/tts/TTSService:access$33	(Lcom/google/tts/TTSService;)Ljava/lang/String;
        //   733: invokestatic 110	com/google/tts/TTSService:access$18	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //   736: pop
        //   737: goto -510 -> 227
        //   740: astore_3
        //   741: aload_2
        //   742: invokevirtual 66	java/lang/String:length	()I
        //   745: ifle +18 -> 763
        //   748: aload_0
        //   749: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   752: aload_2
        //   753: aload_0
        //   754: getfield 16	com/google/tts/TTSService$1SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   757: getfield 72	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
        //   760: invokestatic 76	com/google/tts/TTSService:access$26	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;)V
        //   763: iload_1
        //   764: ifeq +20 -> 784
        //   767: aload_0
        //   768: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   771: invokestatic 34	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   774: invokevirtual 79	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //   777: aload_0
        //   778: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   781: invokestatic 83	com/google/tts/TTSService:access$27	(Lcom/google/tts/TTSService;)V
        //   784: aload_3
        //   785: athrow
        //   786: aload_0
        //   787: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   790: ldc 28
        //   792: aload_0
        //   793: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   796: invokestatic 189	com/google/tts/TTSService:access$34	(Lcom/google/tts/TTSService;)I
        //   799: invokestatic 120	com/google/tts/TTSService:access$13	(Lcom/google/tts/TTSService;Ljava/lang/String;I)I
        //   802: pop
        //   803: goto -553 -> 250
        //   806: aload_0
        //   807: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   810: ldc 28
        //   812: aload_0
        //   813: getfield 14	com/google/tts/TTSService$1SynthThread:this$0	Lcom/google/tts/TTSService;
        //   816: invokestatic 192	com/google/tts/TTSService:access$35	(Lcom/google/tts/TTSService;)I
        //   819: invokestatic 123	com/google/tts/TTSService:access$14	(Lcom/google/tts/TTSService;Ljava/lang/String;I)I
        //   822: pop
        //   823: goto -550 -> 273
        //   826: astore 18
        //   828: ldc 165
        //   830: ldc 194
        //   832: invokestatic 197	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //   835: pop
        //   836: goto -547 -> 289
        //   839: iinc 13 2
        //   842: goto -691 -> 151
        //
        // Exception table:
        //   from	to	target	type
        //   528	550	557	java/lang/NumberFormatException
        //   5	16	647	java/lang/InterruptedException
        //   20	67	647	java/lang/InterruptedException
        //   138	148	647	java/lang/InterruptedException
        //   151	168	647	java/lang/InterruptedException
        //   168	203	647	java/lang/InterruptedException
        //   203	227	647	java/lang/InterruptedException
        //   227	250	647	java/lang/InterruptedException
        //   250	273	647	java/lang/InterruptedException
        //   273	289	647	java/lang/InterruptedException
        //   333	350	647	java/lang/InterruptedException
        //   355	384	647	java/lang/InterruptedException
        //   387	416	647	java/lang/InterruptedException
        //   419	448	647	java/lang/InterruptedException
        //   451	480	647	java/lang/InterruptedException
        //   483	511	647	java/lang/InterruptedException
        //   514	523	647	java/lang/InterruptedException
        //   528	550	647	java/lang/InterruptedException
        //   565	594	647	java/lang/InterruptedException
        //   597	626	647	java/lang/InterruptedException
        //   629	644	647	java/lang/InterruptedException
        //   706	737	647	java/lang/InterruptedException
        //   786	803	647	java/lang/InterruptedException
        //   806	823	647	java/lang/InterruptedException
        //   828	836	647	java/lang/InterruptedException
        //   5	16	740	finally
        //   20	67	740	finally
        //   138	148	740	finally
        //   151	168	740	finally
        //   168	203	740	finally
        //   203	227	740	finally
        //   227	250	740	finally
        //   250	273	740	finally
        //   273	289	740	finally
        //   333	350	740	finally
        //   355	384	740	finally
        //   387	416	740	finally
        //   419	448	740	finally
        //   451	480	740	finally
        //   483	511	740	finally
        //   514	523	740	finally
        //   528	550	740	finally
        //   565	594	740	finally
        //   597	626	740	finally
        //   629	644	740	finally
        //   649	662	740	finally
        //   706	737	740	finally
        //   786	803	740	finally
        //   806	823	740	finally
        //   828	836	740	finally
        //   273	289	826	java/lang/NullPointerException
      }
    });
    localThread.setPriority(10);
    localThread.start();
  }

  private SpeechItem splitCurrentTextIfNeeded(SpeechItem paramSpeechItem)
  {
    if (paramSpeechItem.mText.length() < 4000)
      return paramSpeechItem;
    String str = paramSpeechItem.mCallingApp;
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    int j = 4000 - 1;
    if (j >= paramSpeechItem.mText.length())
    {
      localArrayList.add(new SpeechItem(str, paramSpeechItem.mText.substring(i), null, 0));
      this.mSpeechQueue.remove(0);
    }
    for (int k = -1 + localArrayList.size(); ; k--)
    {
      if (k < 0)
      {
        return (SpeechItem)this.mSpeechQueue.get(0);
        localArrayList.add(new SpeechItem(str, paramSpeechItem.mText.substring(i, j), null, 0));
        i = j;
        j = -1 + (i + 4000);
        break;
      }
      this.mSpeechQueue.add(0, (SpeechItem)localArrayList.get(k));
    }
  }

  // ERROR //
  private int stop(String paramString)
  {
    // Byte code:
    //   0: iconst_m1
    //   1: istore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: aload_0
    //   5: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   8: ldc2_w 622
    //   11: getstatic 629	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   14: invokevirtual 633	java/util/concurrent/locks/ReentrantLock:tryLock	(JLjava/util/concurrent/TimeUnit;)Z
    //   17: istore_3
    //   18: iload_3
    //   19: ifeq +166 -> 185
    //   22: ldc 35
    //   24: ldc_w 891
    //   27: invokestatic 894	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   30: pop
    //   31: iconst_m1
    //   32: aload_0
    //   33: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   36: invokevirtual 599	java/util/ArrayList:size	()I
    //   39: iadd
    //   40: istore 8
    //   42: iload 8
    //   44: iconst_m1
    //   45: if_icmpgt +105 -> 150
    //   48: aload_0
    //   49: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   52: ifnull +189 -> 241
    //   55: aload_0
    //   56: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   59: getfield 878	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
    //   62: aload_1
    //   63: invokevirtual 610	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   66: istore 10
    //   68: iload 10
    //   70: ifeq +171 -> 241
    //   73: getstatic 261	com/google/tts/TTSService:sNativeSynth	Lcom/google/tts/SynthProxyBeta;
    //   76: invokevirtual 640	com/google/tts/SynthProxyBeta:stop	()I
    //   79: istore 15
    //   81: iload 15
    //   83: istore_2
    //   84: aload_0
    //   85: getfield 405	com/google/tts/TTSService:mKillList	Ljava/util/HashMap;
    //   88: aload_0
    //   89: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   92: iconst_1
    //   93: invokestatic 646	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   96: invokevirtual 93	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   99: pop
    //   100: aload_0
    //   101: getfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   104: astore 13
    //   106: aload 13
    //   108: ifnull +10 -> 118
    //   111: aload_0
    //   112: getfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   115: invokevirtual 896	android/media/MediaPlayer:stop	()V
    //   118: aload_0
    //   119: iconst_0
    //   120: putfield 446	com/google/tts/TTSService:mIsSpeaking	Z
    //   123: aload_0
    //   124: aconst_null
    //   125: putfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   128: ldc 35
    //   130: ldc_w 898
    //   133: invokestatic 894	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   136: pop
    //   137: iload_3
    //   138: ifeq +10 -> 148
    //   141: aload_0
    //   142: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   145: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   148: iload_2
    //   149: ireturn
    //   150: aload_0
    //   151: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   154: iload 8
    //   156: invokevirtual 602	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   159: checkcast 584	com/google/tts/TTSService$SpeechItem
    //   162: getfield 878	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
    //   165: aload_1
    //   166: invokevirtual 610	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   169: ifeq +59 -> 228
    //   172: aload_0
    //   173: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   176: iload 8
    //   178: invokevirtual 713	java/util/ArrayList:remove	(I)Ljava/lang/Object;
    //   181: pop
    //   182: goto +46 -> 228
    //   185: ldc 35
    //   187: ldc_w 900
    //   190: invokestatic 675	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   193: pop
    //   194: iconst_m1
    //   195: istore_2
    //   196: goto -59 -> 137
    //   199: astore 5
    //   201: ldc 35
    //   203: ldc_w 902
    //   206: invokestatic 675	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   209: pop
    //   210: aload 5
    //   212: invokevirtual 698	java/lang/InterruptedException:printStackTrace	()V
    //   215: goto -78 -> 137
    //   218: astore 4
    //   220: goto -83 -> 137
    //   223: astore 14
    //   225: goto -107 -> 118
    //   228: iinc 8 255
    //   231: goto -189 -> 42
    //   234: astore 11
    //   236: iconst_m1
    //   237: istore_2
    //   238: goto -154 -> 84
    //   241: iconst_0
    //   242: istore_2
    //   243: goto -115 -> 128
    //
    // Exception table:
    //   from	to	target	type
    //   4	18	199	java/lang/InterruptedException
    //   22	42	199	java/lang/InterruptedException
    //   48	68	199	java/lang/InterruptedException
    //   73	81	199	java/lang/InterruptedException
    //   84	106	199	java/lang/InterruptedException
    //   111	118	199	java/lang/InterruptedException
    //   118	128	199	java/lang/InterruptedException
    //   128	137	199	java/lang/InterruptedException
    //   150	182	199	java/lang/InterruptedException
    //   185	194	199	java/lang/InterruptedException
    //   4	18	218	finally
    //   22	42	218	finally
    //   48	68	218	finally
    //   73	81	218	finally
    //   84	106	218	finally
    //   111	118	218	finally
    //   118	128	218	finally
    //   128	137	218	finally
    //   150	182	218	finally
    //   185	194	218	finally
    //   201	215	218	finally
    //   111	118	223	java/lang/IllegalStateException
    //   73	81	234	java/lang/NullPointerException
  }

  // ERROR //
  private int stopAll(String paramString)
  {
    // Byte code:
    //   0: iconst_m1
    //   1: istore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: aload_0
    //   5: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   8: ldc2_w 622
    //   11: getstatic 629	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   14: invokevirtual 633	java/util/concurrent/locks/ReentrantLock:tryLock	(JLjava/util/concurrent/TimeUnit;)Z
    //   17: istore_3
    //   18: iload_3
    //   19: ifeq +165 -> 184
    //   22: iconst_m1
    //   23: aload_0
    //   24: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   27: invokevirtual 599	java/util/ArrayList:size	()I
    //   30: iadd
    //   31: istore 7
    //   33: iload 7
    //   35: iconst_m1
    //   36: if_icmpgt +116 -> 152
    //   39: aload_0
    //   40: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   43: ifnull +197 -> 240
    //   46: aload_0
    //   47: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   50: getfield 590	com/google/tts/TTSService$SpeechItem:mType	I
    //   53: iconst_3
    //   54: if_icmpne +21 -> 75
    //   57: aload_0
    //   58: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   61: getfield 878	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
    //   64: aload_1
    //   65: invokevirtual 610	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   68: istore 14
    //   70: iload 14
    //   72: ifeq +168 -> 240
    //   75: getstatic 261	com/google/tts/TTSService:sNativeSynth	Lcom/google/tts/SynthProxyBeta;
    //   78: invokevirtual 640	com/google/tts/SynthProxyBeta:stop	()I
    //   81: istore 13
    //   83: iload 13
    //   85: istore_2
    //   86: aload_0
    //   87: getfield 405	com/google/tts/TTSService:mKillList	Ljava/util/HashMap;
    //   90: aload_0
    //   91: getfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   94: iconst_1
    //   95: invokestatic 646	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   98: invokevirtual 93	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   101: pop
    //   102: aload_0
    //   103: getfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   106: astore 11
    //   108: aload 11
    //   110: ifnull +10 -> 120
    //   113: aload_0
    //   114: getfield 478	com/google/tts/TTSService:mPlayer	Landroid/media/MediaPlayer;
    //   117: invokevirtual 896	android/media/MediaPlayer:stop	()V
    //   120: aload_0
    //   121: iconst_0
    //   122: putfield 446	com/google/tts/TTSService:mIsSpeaking	Z
    //   125: aload_0
    //   126: aconst_null
    //   127: putfield 638	com/google/tts/TTSService:mCurrentSpeechItem	Lcom/google/tts/TTSService$SpeechItem;
    //   130: ldc 35
    //   132: ldc_w 904
    //   135: invokestatic 894	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   138: pop
    //   139: iload_3
    //   140: ifeq +10 -> 150
    //   143: aload_0
    //   144: getfield 274	com/google/tts/TTSService:speechQueueLock	Ljava/util/concurrent/locks/ReentrantLock;
    //   147: invokevirtual 670	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   150: iload_2
    //   151: ireturn
    //   152: aload_0
    //   153: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   156: iload 7
    //   158: invokevirtual 602	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   161: checkcast 584	com/google/tts/TTSService$SpeechItem
    //   164: getfield 590	com/google/tts/TTSService$SpeechItem:mType	I
    //   167: iconst_3
    //   168: if_icmpeq +59 -> 227
    //   171: aload_0
    //   172: getfield 450	com/google/tts/TTSService:mSpeechQueue	Ljava/util/ArrayList;
    //   175: iload 7
    //   177: invokevirtual 713	java/util/ArrayList:remove	(I)Ljava/lang/Object;
    //   180: pop
    //   181: goto +46 -> 227
    //   184: ldc 35
    //   186: ldc_w 906
    //   189: invokestatic 675	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   192: pop
    //   193: iconst_m1
    //   194: istore_2
    //   195: goto -56 -> 139
    //   198: astore 5
    //   200: ldc 35
    //   202: ldc_w 908
    //   205: invokestatic 675	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   208: pop
    //   209: aload 5
    //   211: invokevirtual 698	java/lang/InterruptedException:printStackTrace	()V
    //   214: goto -75 -> 139
    //   217: astore 4
    //   219: goto -80 -> 139
    //   222: astore 12
    //   224: goto -104 -> 120
    //   227: iinc 7 255
    //   230: goto -197 -> 33
    //   233: astore 9
    //   235: iconst_m1
    //   236: istore_2
    //   237: goto -151 -> 86
    //   240: iconst_0
    //   241: istore_2
    //   242: goto -112 -> 130
    //
    // Exception table:
    //   from	to	target	type
    //   4	18	198	java/lang/InterruptedException
    //   22	33	198	java/lang/InterruptedException
    //   39	70	198	java/lang/InterruptedException
    //   75	83	198	java/lang/InterruptedException
    //   86	108	198	java/lang/InterruptedException
    //   113	120	198	java/lang/InterruptedException
    //   120	130	198	java/lang/InterruptedException
    //   130	139	198	java/lang/InterruptedException
    //   152	181	198	java/lang/InterruptedException
    //   184	193	198	java/lang/InterruptedException
    //   4	18	217	finally
    //   22	33	217	finally
    //   39	70	217	finally
    //   75	83	217	finally
    //   86	108	217	finally
    //   113	120	217	finally
    //   120	130	217	finally
    //   130	139	217	finally
    //   152	181	217	finally
    //   184	193	217	finally
    //   200	214	217	finally
    //   113	120	222	java/lang/IllegalStateException
    //   75	83	233	java/lang/NullPointerException
  }

  private void synthToFileInternalOnly(final SpeechItem paramSpeechItem)
  {
    Thread localThread = new Thread(new Runnable()
    {
      // ERROR //
      public void run()
      {
        // Byte code:
        //   0: iconst_0
        //   1: istore_1
        //   2: ldc 26
        //   4: astore_2
        //   5: ldc 28
        //   7: new 30	java/lang/StringBuilder
        //   10: dup
        //   11: ldc 32
        //   13: invokespecial 35	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
        //   16: aload_0
        //   17: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   20: getfield 41	com/google/tts/TTSService$SpeechItem:mFilename	Ljava/lang/String;
        //   23: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   26: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   29: invokestatic 55	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //   32: pop
        //   33: aload_0
        //   34: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   37: invokestatic 61	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   40: invokevirtual 67	java/util/concurrent/locks/ReentrantLock:tryLock	()Z
        //   43: istore_1
        //   44: iload_1
        //   45: ifne +247 -> 292
        //   48: aload_0
        //   49: monitorenter
        //   50: aload_0
        //   51: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   54: iconst_1
        //   55: invokestatic 71	com/google/tts/TTSService:access$29	(Lcom/google/tts/TTSService;Z)V
        //   58: aload_0
        //   59: monitorexit
        //   60: ldc2_w 72
        //   63: invokestatic 79	java/lang/Thread:sleep	(J)V
        //   66: new 75	java/lang/Thread
        //   69: dup
        //   70: new 2	com/google/tts/TTSService$2SynthThread
        //   73: dup
        //   74: aload_0
        //   75: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   78: aload_0
        //   79: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   82: invokespecial 81	com/google/tts/TTSService$2SynthThread:<init>	(Lcom/google/tts/TTSService;Lcom/google/tts/TTSService$SpeechItem;)V
        //   85: invokespecial 84	java/lang/Thread:<init>	(Ljava/lang/Runnable;)V
        //   88: invokevirtual 87	java/lang/Thread:start	()V
        //   91: aload_0
        //   92: monitorenter
        //   93: aload_0
        //   94: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   97: iconst_0
        //   98: invokestatic 71	com/google/tts/TTSService:access$29	(Lcom/google/tts/TTSService;Z)V
        //   101: aload_0
        //   102: monitorexit
        //   103: aload_2
        //   104: invokevirtual 93	java/lang/String:length	()I
        //   107: ifle +18 -> 125
        //   110: aload_0
        //   111: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   114: aload_2
        //   115: aload_0
        //   116: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   119: getfield 96	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
        //   122: invokestatic 100	com/google/tts/TTSService:access$26	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;)V
        //   125: iload_1
        //   126: ifeq +20 -> 146
        //   129: aload_0
        //   130: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   133: invokestatic 61	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   136: invokevirtual 103	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //   139: aload_0
        //   140: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   143: invokestatic 107	com/google/tts/TTSService:access$27	(Lcom/google/tts/TTSService;)V
        //   146: aload_0
        //   147: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   150: iconst_0
        //   151: invokestatic 110	com/google/tts/TTSService:access$24	(Lcom/google/tts/TTSService;Z)V
        //   154: return
        //   155: astore 7
        //   157: aload_0
        //   158: monitorexit
        //   159: aload 7
        //   161: athrow
        //   162: astore 5
        //   164: ldc 28
        //   166: ldc 112
        //   168: invokestatic 115	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //   171: pop
        //   172: aload 5
        //   174: invokevirtual 118	java/lang/InterruptedException:printStackTrace	()V
        //   177: aload_2
        //   178: invokevirtual 93	java/lang/String:length	()I
        //   181: ifle +18 -> 199
        //   184: aload_0
        //   185: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   188: aload_2
        //   189: aload_0
        //   190: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   193: getfield 96	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
        //   196: invokestatic 100	com/google/tts/TTSService:access$26	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;)V
        //   199: iload_1
        //   200: ifeq +20 -> 220
        //   203: aload_0
        //   204: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   207: invokestatic 61	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   210: invokevirtual 103	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //   213: aload_0
        //   214: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   217: invokestatic 107	com/google/tts/TTSService:access$27	(Lcom/google/tts/TTSService;)V
        //   220: aload_0
        //   221: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   224: iconst_0
        //   225: invokestatic 110	com/google/tts/TTSService:access$24	(Lcom/google/tts/TTSService;Z)V
        //   228: return
        //   229: astore 8
        //   231: aload_0
        //   232: monitorexit
        //   233: aload 8
        //   235: athrow
        //   236: astore 4
        //   238: aload_2
        //   239: invokevirtual 93	java/lang/String:length	()I
        //   242: ifle +18 -> 260
        //   245: aload_0
        //   246: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   249: aload_2
        //   250: aload_0
        //   251: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   254: getfield 96	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
        //   257: invokestatic 100	com/google/tts/TTSService:access$26	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;)V
        //   260: iload_1
        //   261: ifeq +20 -> 281
        //   264: aload_0
        //   265: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   268: invokestatic 61	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   271: invokevirtual 103	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //   274: aload_0
        //   275: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   278: invokestatic 107	com/google/tts/TTSService:access$27	(Lcom/google/tts/TTSService;)V
        //   281: aload_0
        //   282: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   285: iconst_0
        //   286: invokestatic 110	com/google/tts/TTSService:access$24	(Lcom/google/tts/TTSService;Z)V
        //   289: aload 4
        //   291: athrow
        //   292: ldc 26
        //   294: astore 9
        //   296: ldc 26
        //   298: astore 10
        //   300: ldc 26
        //   302: astore 11
        //   304: ldc 26
        //   306: astore 12
        //   308: ldc 26
        //   310: astore 13
        //   312: ldc 26
        //   314: astore 14
        //   316: aload_0
        //   317: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   320: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   323: ifnull +23 -> 346
        //   326: iconst_0
        //   327: istore 15
        //   329: iload 15
        //   331: iconst_m1
        //   332: aload_0
        //   333: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   336: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   339: invokevirtual 127	java/util/ArrayList:size	()I
        //   342: iadd
        //   343: if_icmplt +181 -> 524
        //   346: aload_0
        //   347: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   350: invokestatic 131	com/google/tts/TTSService:access$30	(Lcom/google/tts/TTSService;)Ljava/util/HashMap;
        //   353: aload_0
        //   354: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   357: invokevirtual 137	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //   360: ifnonnull +112 -> 472
        //   363: aload 13
        //   365: invokevirtual 93	java/lang/String:length	()I
        //   368: ifle +401 -> 769
        //   371: aload_0
        //   372: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   375: aload 13
        //   377: invokestatic 141	com/google/tts/TTSService:access$15	(Lcom/google/tts/TTSService;Ljava/lang/String;)I
        //   380: pop
        //   381: aload 9
        //   383: invokevirtual 93	java/lang/String:length	()I
        //   386: ifle +401 -> 787
        //   389: aload_0
        //   390: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   393: ldc 26
        //   395: aload 9
        //   397: aload 10
        //   399: aload 11
        //   401: invokestatic 145	com/google/tts/TTSService:access$18	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //   404: pop
        //   405: aload 12
        //   407: invokevirtual 93	java/lang/String:length	()I
        //   410: ifle +411 -> 821
        //   413: aload_0
        //   414: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   417: ldc 26
        //   419: aload 12
        //   421: invokestatic 151	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //   424: invokestatic 155	com/google/tts/TTSService:access$13	(Lcom/google/tts/TTSService;Ljava/lang/String;I)I
        //   427: pop
        //   428: aload 14
        //   430: invokevirtual 93	java/lang/String:length	()I
        //   433: ifle +408 -> 841
        //   436: aload_0
        //   437: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   440: ldc 26
        //   442: aload 14
        //   444: invokestatic 151	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //   447: invokestatic 158	com/google/tts/TTSService:access$14	(Lcom/google/tts/TTSService;Ljava/lang/String;I)I
        //   450: pop
        //   451: invokestatic 162	com/google/tts/TTSService:access$36	()Lcom/google/tts/SynthProxyBeta;
        //   454: aload_0
        //   455: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   458: getfield 165	com/google/tts/TTSService$SpeechItem:mText	Ljava/lang/String;
        //   461: aload_0
        //   462: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   465: getfield 41	com/google/tts/TTSService$SpeechItem:mFilename	Ljava/lang/String;
        //   468: invokevirtual 170	com/google/tts/SynthProxyBeta:synthesizeToFile	(Ljava/lang/String;Ljava/lang/String;)I
        //   471: pop
        //   472: aload_2
        //   473: invokevirtual 93	java/lang/String:length	()I
        //   476: ifle +18 -> 494
        //   479: aload_0
        //   480: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   483: aload_2
        //   484: aload_0
        //   485: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   488: getfield 96	com/google/tts/TTSService$SpeechItem:mCallingApp	Ljava/lang/String;
        //   491: invokestatic 100	com/google/tts/TTSService:access$26	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;)V
        //   494: iload_1
        //   495: ifeq +20 -> 515
        //   498: aload_0
        //   499: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   502: invokestatic 61	com/google/tts/TTSService:access$28	(Lcom/google/tts/TTSService;)Ljava/util/concurrent/locks/ReentrantLock;
        //   505: invokevirtual 103	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //   508: aload_0
        //   509: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   512: invokestatic 107	com/google/tts/TTSService:access$27	(Lcom/google/tts/TTSService;)V
        //   515: aload_0
        //   516: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   519: iconst_0
        //   520: invokestatic 110	com/google/tts/TTSService:access$24	(Lcom/google/tts/TTSService;Z)V
        //   523: return
        //   524: aload_0
        //   525: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   528: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   531: iload 15
        //   533: invokevirtual 173	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   536: checkcast 89	java/lang/String
        //   539: astore 27
        //   541: aload 27
        //   543: ifnull +331 -> 874
        //   546: aload 27
        //   548: ldc 175
        //   550: invokevirtual 179	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   553: ifeq +25 -> 578
        //   556: aload_0
        //   557: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   560: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   563: iload 15
        //   565: iconst_1
        //   566: iadd
        //   567: invokevirtual 173	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   570: checkcast 89	java/lang/String
        //   573: astore 12
        //   575: goto +299 -> 874
        //   578: aload 27
        //   580: ldc 181
        //   582: invokevirtual 179	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   585: ifeq +25 -> 610
        //   588: aload_0
        //   589: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   592: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   595: iload 15
        //   597: iconst_1
        //   598: iadd
        //   599: invokevirtual 173	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   602: checkcast 89	java/lang/String
        //   605: astore 9
        //   607: goto +267 -> 874
        //   610: aload 27
        //   612: ldc 183
        //   614: invokevirtual 179	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   617: ifeq +25 -> 642
        //   620: aload_0
        //   621: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   624: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   627: iload 15
        //   629: iconst_1
        //   630: iadd
        //   631: invokevirtual 173	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   634: checkcast 89	java/lang/String
        //   637: astore 10
        //   639: goto +235 -> 874
        //   642: aload 27
        //   644: ldc 185
        //   646: invokevirtual 179	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   649: ifeq +25 -> 674
        //   652: aload_0
        //   653: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   656: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   659: iload 15
        //   661: iconst_1
        //   662: iadd
        //   663: invokevirtual 173	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   666: checkcast 89	java/lang/String
        //   669: astore 11
        //   671: goto +203 -> 874
        //   674: aload 27
        //   676: ldc 187
        //   678: invokevirtual 179	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   681: ifeq +24 -> 705
        //   684: aload_0
        //   685: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   688: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   691: iload 15
        //   693: iconst_1
        //   694: iadd
        //   695: invokevirtual 173	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   698: checkcast 89	java/lang/String
        //   701: astore_2
        //   702: goto +172 -> 874
        //   705: aload 27
        //   707: ldc 189
        //   709: invokevirtual 179	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   712: ifeq +25 -> 737
        //   715: aload_0
        //   716: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   719: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   722: iload 15
        //   724: iconst_1
        //   725: iadd
        //   726: invokevirtual 173	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   729: checkcast 89	java/lang/String
        //   732: astore 13
        //   734: goto +140 -> 874
        //   737: aload 27
        //   739: ldc 191
        //   741: invokevirtual 179	java/lang/String:equals	(Ljava/lang/Object;)Z
        //   744: ifeq +130 -> 874
        //   747: aload_0
        //   748: getfield 16	com/google/tts/TTSService$2SynthThread:val$speechItem	Lcom/google/tts/TTSService$SpeechItem;
        //   751: getfield 122	com/google/tts/TTSService$SpeechItem:mParams	Ljava/util/ArrayList;
        //   754: iload 15
        //   756: iconst_1
        //   757: iadd
        //   758: invokevirtual 173	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   761: checkcast 89	java/lang/String
        //   764: astore 14
        //   766: goto +108 -> 874
        //   769: aload_0
        //   770: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   773: aload_0
        //   774: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   777: invokestatic 195	com/google/tts/TTSService:access$20	(Lcom/google/tts/TTSService;)Ljava/lang/String;
        //   780: invokestatic 141	com/google/tts/TTSService:access$15	(Lcom/google/tts/TTSService;Ljava/lang/String;)I
        //   783: pop
        //   784: goto -403 -> 381
        //   787: aload_0
        //   788: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   791: ldc 26
        //   793: aload_0
        //   794: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   797: invokestatic 198	com/google/tts/TTSService:access$31	(Lcom/google/tts/TTSService;)Ljava/lang/String;
        //   800: aload_0
        //   801: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   804: invokestatic 201	com/google/tts/TTSService:access$32	(Lcom/google/tts/TTSService;)Ljava/lang/String;
        //   807: aload_0
        //   808: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   811: invokestatic 204	com/google/tts/TTSService:access$33	(Lcom/google/tts/TTSService;)Ljava/lang/String;
        //   814: invokestatic 145	com/google/tts/TTSService:access$18	(Lcom/google/tts/TTSService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //   817: pop
        //   818: goto -413 -> 405
        //   821: aload_0
        //   822: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   825: ldc 26
        //   827: aload_0
        //   828: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   831: invokestatic 208	com/google/tts/TTSService:access$34	(Lcom/google/tts/TTSService;)I
        //   834: invokestatic 155	com/google/tts/TTSService:access$13	(Lcom/google/tts/TTSService;Ljava/lang/String;I)I
        //   837: pop
        //   838: goto -410 -> 428
        //   841: aload_0
        //   842: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   845: ldc 26
        //   847: aload_0
        //   848: getfield 14	com/google/tts/TTSService$2SynthThread:this$0	Lcom/google/tts/TTSService;
        //   851: invokestatic 211	com/google/tts/TTSService:access$35	(Lcom/google/tts/TTSService;)I
        //   854: invokestatic 158	com/google/tts/TTSService:access$14	(Lcom/google/tts/TTSService;Ljava/lang/String;I)I
        //   857: pop
        //   858: goto -407 -> 451
        //   861: astore 20
        //   863: ldc 28
        //   865: ldc 213
        //   867: invokestatic 216	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //   870: pop
        //   871: goto -399 -> 472
        //   874: iinc 15 2
        //   877: goto -548 -> 329
        //
        // Exception table:
        //   from	to	target	type
        //   50	60	155	finally
        //   157	159	155	finally
        //   33	44	162	java/lang/InterruptedException
        //   48	50	162	java/lang/InterruptedException
        //   60	93	162	java/lang/InterruptedException
        //   159	162	162	java/lang/InterruptedException
        //   233	236	162	java/lang/InterruptedException
        //   316	326	162	java/lang/InterruptedException
        //   329	346	162	java/lang/InterruptedException
        //   346	381	162	java/lang/InterruptedException
        //   381	405	162	java/lang/InterruptedException
        //   405	428	162	java/lang/InterruptedException
        //   428	451	162	java/lang/InterruptedException
        //   451	472	162	java/lang/InterruptedException
        //   524	541	162	java/lang/InterruptedException
        //   546	575	162	java/lang/InterruptedException
        //   578	607	162	java/lang/InterruptedException
        //   610	639	162	java/lang/InterruptedException
        //   642	671	162	java/lang/InterruptedException
        //   674	702	162	java/lang/InterruptedException
        //   705	734	162	java/lang/InterruptedException
        //   737	766	162	java/lang/InterruptedException
        //   769	784	162	java/lang/InterruptedException
        //   787	818	162	java/lang/InterruptedException
        //   821	838	162	java/lang/InterruptedException
        //   841	858	162	java/lang/InterruptedException
        //   863	871	162	java/lang/InterruptedException
        //   93	103	229	finally
        //   231	233	229	finally
        //   33	44	236	finally
        //   48	50	236	finally
        //   60	93	236	finally
        //   159	162	236	finally
        //   164	177	236	finally
        //   233	236	236	finally
        //   316	326	236	finally
        //   329	346	236	finally
        //   346	381	236	finally
        //   381	405	236	finally
        //   405	428	236	finally
        //   428	451	236	finally
        //   451	472	236	finally
        //   524	541	236	finally
        //   546	575	236	finally
        //   578	607	236	finally
        //   610	639	236	finally
        //   642	671	236	finally
        //   674	702	236	finally
        //   705	734	236	finally
        //   737	766	236	finally
        //   769	784	236	finally
        //   787	818	236	finally
        //   821	838	236	finally
        //   841	858	236	finally
        //   863	871	236	finally
        //   451	472	861	java/lang/NullPointerException
      }
    });
    localThread.setPriority(10);
    localThread.start();
  }

  private boolean synthesizeToFile(String paramString1, String paramString2, ArrayList<String> paramArrayList, String paramString3)
  {
    if (paramString3.length() > 250);
    while (paramString2.length() >= 4000)
      return false;
    File localFile;
    try
    {
      localFile = new File(paramString3);
      if (localFile.exists())
      {
        Log.v("TtsService", "File " + paramString3 + " exists, deleting.");
        localFile.delete();
      }
      if (!localFile.createNewFile())
      {
        Log.e("TtsService", "Unable to synthesize to file: can't create " + paramString3);
        return false;
      }
    }
    catch (IOException localIOException)
    {
      Log.e("TtsService", "Can't create " + paramString3 + " due to exception " + localIOException);
      return false;
    }
    localFile.delete();
    this.mSpeechQueue.add(new SpeechItem(paramString1, paramString2, paramArrayList, 3, paramString3));
    if (!this.mIsSpeaking)
      processSpeechQueue();
    return true;
  }

  public IBinder onBind(Intent paramIntent)
  {
    Iterator localIterator2;
    Iterator localIterator1;
    if ("android.intent.action.USE_TTS".equals(paramIntent.getAction()))
    {
      localIterator2 = paramIntent.getCategories().iterator();
      if (localIterator2.hasNext());
    }
    else if ("com.google.intent.action.START_TTS_SERVICE_BETA".equals(paramIntent.getAction()))
    {
      localIterator1 = paramIntent.getCategories().iterator();
    }
    do
      if (!localIterator1.hasNext())
      {
        return null;
        if (!((String)localIterator2.next()).equals("android.intent.category.TTS"))
          break;
        return this.mBinderOld;
      }
    while (!((String)localIterator1.next()).equals("com.google.intent.category.TTS_BETA"));
    return this.mBinder;
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    SpeechItem localSpeechItem = this.mCurrentSpeechItem;
    String str1;
    ArrayList localArrayList;
    String str2;
    if (localSpeechItem != null)
    {
      str1 = localSpeechItem.mCallingApp;
      localArrayList = localSpeechItem.mParams;
      str2 = "";
      if (localArrayList == null);
    }
    for (int i = 0; ; i += 2)
    {
      if (i >= -1 + localArrayList.size())
      {
        if (str2.length() > 0)
          dispatchUtteranceCompletedCallback(str2, str1);
        processSpeechQueue();
        return;
      }
      if (((String)localArrayList.get(i)).equals("utteranceId"))
        str2 = (String)localArrayList.get(i + 1);
    }
  }

  public void onCreate()
  {
    super.onCreate();
    Log.v("TtsService", "TtsService.onCreate()");
    this.currentSpeechEngineSOFile = "";
    String str1 = PreferenceManager.getDefaultSharedPreferences(this).getString("tts_default_synth", "com.svox.pico");
    if (setEngine(str1) != 0)
    {
      Log.e("TtsService", "Unable to start up with " + str1 + ". Falling back to the default TTS engine.");
      setEngine("com.svox.pico");
    }
    this.mSelf = this;
    this.mIsSpeaking = false;
    this.mSynthBusy = false;
    this.mEarcons = new HashMap();
    this.mUtterances = new HashMap();
    this.mCallbacksMap = new HashMap();
    this.mSpeechQueue = new ArrayList();
    this.mPlayer = null;
    this.mCurrentSpeechItem = null;
    this.mKillList = new HashMap();
    setDefaultSettings();
    InputStream localInputStream = getResources().openRawResource(2131034282);
    try
    {
      Properties localProperties = new Properties();
      localProperties.load(localInputStream);
      Enumeration localEnumeration = localProperties.keys();
      while (true)
      {
        boolean bool = localEnumeration.hasMoreElements();
        if (!bool)
        {
          this.mUtterances.put("[tock]", new SoundResource("android.tts", 2131034290));
          this.mUtterances.put("[slnc]", new SoundResource("android.tts", 2131034281));
          this.mEarcons.put("[tock]", new SoundResource("android.tts", 2131034290));
          this.mEarcons.put("[slnc]", new SoundResource("android.tts", 2131034281));
          Log.e("TtsService", "onCreate completed.");
          return;
        }
        String str2 = localEnumeration.nextElement().toString();
        String str3 = "com.google.tts:raw/" + localProperties.getProperty(str2);
        TypedValue localTypedValue = new TypedValue();
        getResources().getValue(str3, localTypedValue, false);
        this.mUtterances.put(str2, new SoundResource("android.tts", localTypedValue.resourceId));
      }
    }
    catch (FactoryConfigurationError localFactoryConfigurationError)
    {
      while (true)
        localFactoryConfigurationError.printStackTrace();
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      while (true)
        localIllegalArgumentException.printStackTrace();
    }
    catch (SecurityException localSecurityException)
    {
      while (true)
        localSecurityException.printStackTrace();
    }
  }

  public void onDestroy()
  {
    super.onDestroy();
    killAllUtterances();
    cleanUpPlayer();
    if (sNativeSynth != null)
      sNativeSynth.shutdown();
    sNativeSynth = null;
    this.mCallbacks.kill();
    this.mCallbacksOld.kill();
    Log.v("TtsService", "onDestroy() completed");
  }

  private static class SoundResource
  {
    public String mFilename = null;
    public int mResId = -1;
    public String mSourcePackageName = null;

    public SoundResource(String paramString)
    {
      this.mSourcePackageName = null;
      this.mResId = -1;
      this.mFilename = paramString;
    }

    public SoundResource(String paramString, int paramInt)
    {
      this.mSourcePackageName = paramString;
      this.mResId = paramInt;
      this.mFilename = null;
    }
  }

  private static class SpeechItem
  {
    public static final int EARCON = 1;
    public static final int SILENCE = 2;
    public static final int TEXT = 0;
    public static final int TEXT_TO_FILE = 3;
    public String mCallingApp = "";
    public long mDuration = 0L;
    public String mFilename = null;
    public ArrayList<String> mParams = null;
    public String mText = "";
    public int mType = 0;

    public SpeechItem(String paramString, long paramLong, ArrayList<String> paramArrayList)
    {
      this.mDuration = paramLong;
      this.mParams = paramArrayList;
      this.mType = 2;
      this.mCallingApp = paramString;
    }

    public SpeechItem(String paramString1, String paramString2, ArrayList<String> paramArrayList, int paramInt)
    {
      this.mText = paramString2;
      this.mParams = paramArrayList;
      this.mType = paramInt;
      this.mCallingApp = paramString1;
    }

    public SpeechItem(String paramString1, String paramString2, ArrayList<String> paramArrayList, int paramInt, String paramString3)
    {
      this.mText = paramString2;
      this.mParams = paramArrayList;
      this.mType = paramInt;
      this.mFilename = paramString3;
      this.mCallingApp = paramString1;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.TTSService
 * JD-Core Version:    0.6.2
 */