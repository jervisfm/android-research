package com.google.tts;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;

public class TTSVersionAlert extends AlertDialog.Builder
{
  private static final String INSTALL_TTS = "Install the TTS";
  private static final String MARKET_URI = "market://search?q=pname:com.google.tts";
  private static final String NO_TTS = "This application can talk using the text-to-speech (TTS) library. Please install the TTS.";
  private static final String QUIT = "Do not install the TTS";
  private Activity parent;

  public TTSVersionAlert(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    super(paramContext);
    this.parent = ((Activity)paramContext);
    DialogInterface.OnClickListener local1;
    DialogInterface.OnClickListener local2;
    if (paramString1 != null)
    {
      setMessage(paramString1);
      local1 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.google.tts"));
          TTSVersionAlert.this.parent.startActivity(localIntent);
        }
      };
      local2 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
        }
      };
      if (paramString2 == null)
        break label80;
      setPositiveButton(paramString2, local1);
    }
    while (true)
    {
      if (paramString3 == null)
        break label92;
      setNegativeButton(paramString3, local2);
      return;
      setMessage("This application can talk using the text-to-speech (TTS) library. Please install the TTS.");
      break;
      label80: setPositiveButton("Install the TTS", local1);
    }
    label92: setNegativeButton("Do not install the TTS", local2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.TTSVersionAlert
 * JD-Core Version:    0.6.2
 */