package com.google.tts;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class TextToSpeechSettings extends PreferenceActivity
  implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener, TextToSpeechBeta.OnInitListener
{
  private static final String DEFAULT_COUNTRY_VAL = "USA";
  private static final String DEFAULT_LANG_VAL = "eng";
  private static final String DEFAULT_VARIANT_VAL = "";
  private static final String FALLBACK_TTS_DEFAULT_SYNTH = "com.svox.pico";
  private static final int GET_SAMPLE_TEXT = 1983;
  private static final String KEY_PLUGIN_ENABLED_PREFIX = "ENABLED_";
  private static final String KEY_PLUGIN_SETTINGS_PREFIX = "SETTINGS_";
  private static final String KEY_TTS_DEFAULT_COUNTRY = "tts_default_country";
  private static final String KEY_TTS_DEFAULT_LANG = "tts_default_lang";
  private static final String KEY_TTS_DEFAULT_RATE = "tts_default_rate";
  private static final String KEY_TTS_DEFAULT_SYNTH = "tts_default_synth";
  private static final String KEY_TTS_DEFAULT_VARIANT = "tts_default_variant";
  private static final String KEY_TTS_ENABLED_PLUGINS = "tts_enabled_plugins";
  private static final String KEY_TTS_INSTALL_DATA = "tts_install_data";
  private static final String KEY_TTS_PLAY_EXAMPLE = "tts_play_example";
  private static final String KEY_TTS_USE_DEFAULT = "toggle_use_default_tts_settings";
  private static final String LOCALE_DELIMITER = "-";
  private static final String SYSTEM_TTS = "com.svox.pico";
  private static final String TAG = "TextToSpeechSettings";
  private static final int VOICE_DATA_INTEGRITY_CHECK = 1977;
  private String mDefaultCountry = null;
  private String mDefaultEng = "";
  private String mDefaultLanguage = null;
  private ListPreference mDefaultLocPref = null;
  private String mDefaultLocVariant = null;
  private int mDefaultRate = 100;
  private ListPreference mDefaultRatePref = null;
  private ListPreference mDefaultSynthPref = null;
  private int mDemoStringIndex = 0;
  private String[] mDemoStrings;
  private boolean mEnableDemo = false;
  private Preference mInstallData = null;
  private Preference mPlayExample = null;
  private TextToSpeechBeta mTts = null;
  private boolean mTtsStarted = false;
  private CheckBoxPreference mUseDefaultPref = null;
  private boolean mVoicesMissing = false;
  private SharedPreferences prefs;
  private SharedPreferences.Editor prefsEditor;

  private void addEngineSpecificSettings()
  {
    PreferenceGroup localPreferenceGroup = (PreferenceGroup)findPreference("tts_engines_section");
    Intent localIntent = new Intent("android.intent.action.START_TTS_ENGINE");
    ResolveInfo[] arrayOfResolveInfo1 = new ResolveInfo[0];
    PackageManager localPackageManager = getPackageManager();
    ResolveInfo[] arrayOfResolveInfo2 = (ResolveInfo[])localPackageManager.queryIntentActivities(localIntent, 0).toArray(arrayOfResolveInfo1);
    for (int i = 0; ; i++)
    {
      if (i >= arrayOfResolveInfo2.length)
        return;
      final String str = arrayOfResolveInfo2[i].activityInfo.packageName;
      if (!arrayOfResolveInfo2[i].activityInfo.packageName.equals("com.svox.pico"))
      {
        CheckBoxPreference localCheckBoxPreference = new CheckBoxPreference(this);
        localCheckBoxPreference.setKey("ENABLED_" + str);
        localCheckBoxPreference.setTitle(arrayOfResolveInfo2[i].loadLabel(localPackageManager));
        localPreferenceGroup.addPreference(localCheckBoxPreference);
      }
      if (pluginHasSettings(str))
      {
        Preference localPreference = new Preference(this);
        localPreference.setKey("SETTINGS_" + str);
        localPreference.setTitle(arrayOfResolveInfo2[i].loadLabel(localPackageManager));
        Resources localResources = getResources();
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = arrayOfResolveInfo2[i].loadLabel(localPackageManager);
        localPreference.setSummary(localResources.getString(2131165212, arrayOfObject));
        localPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
        {
          public boolean onPreferenceClick(Preference paramAnonymousPreference)
          {
            Intent localIntent = new Intent();
            localIntent.setClassName(str, str + ".EngineSettings");
            TextToSpeechSettings.this.startActivity(localIntent);
            return true;
          }
        });
        localPreferenceGroup.addPreference(localPreference);
      }
    }
  }

  private void checkVoiceData()
  {
    PackageManager localPackageManager = getPackageManager();
    Intent localIntent = new Intent();
    localIntent.setAction("android.speech.tts.engine.CHECK_TTS_DATA");
    List localList = localPackageManager.queryIntentActivities(localIntent, 0);
    for (int i = 0; ; i++)
    {
      if (i >= localList.size())
        return;
      ActivityInfo localActivityInfo = ((ResolveInfo)localList.get(i)).activityInfo;
      if (this.mDefaultEng.equals(localActivityInfo.packageName))
      {
        localIntent.setClassName(this.mDefaultEng, localActivityInfo.name);
        startActivityForResult(localIntent, 1977);
      }
    }
  }

  private void getSampleText()
  {
    PackageManager localPackageManager = getPackageManager();
    Intent localIntent = new Intent();
    localIntent.setAction("android.speech.tts.engine.GET_SAMPLE_TEXT");
    localIntent.putExtra("language", this.mDefaultLanguage);
    localIntent.putExtra("country", this.mDefaultCountry);
    localIntent.putExtra("variant", this.mDefaultLocVariant);
    List localList = localPackageManager.queryIntentActivities(localIntent, 0);
    for (int i = 0; ; i++)
    {
      if (i >= localList.size())
      {
        if ((this.mDefaultEng.equals("com.svox.pico")) && (Integer.parseInt(Build.VERSION.SDK) < 8) && (Integer.parseInt(Build.VERSION.SDK) > 3))
        {
          localIntent.setClassName("com.google.tts", "com.google.tts.GetSampleText");
          startActivityForResult(localIntent, 1983);
        }
        return;
      }
      ActivityInfo localActivityInfo = ((ResolveInfo)localList.get(i)).activityInfo;
      if (this.mDefaultEng.equals(localActivityInfo.packageName))
      {
        localIntent.setClassName(this.mDefaultEng, localActivityInfo.name);
        startActivityForResult(localIntent, 1983);
      }
    }
  }

  private boolean hasLangPref()
  {
    String str = this.prefs.getString("tts_default_lang", null);
    if ((str == null) || (str.length() < 1));
    while ((this.prefs.getString("tts_default_country", null) == null) || (this.prefs.getString("tts_default_variant", null) == null))
      return false;
    return true;
  }

  private void initClickers()
  {
    this.mPlayExample = findPreference("tts_play_example");
    this.mPlayExample.setOnPreferenceClickListener(this);
    this.mInstallData = findPreference("tts_install_data");
    this.mInstallData.setOnPreferenceClickListener(this);
  }

  private void initDefaultLang()
  {
    if (!hasLangPref())
    {
      if (!isCurrentLocSupported())
        break label129;
      useCurrentLocAsDefault();
    }
    while (true)
    {
      this.mDefaultLanguage = this.prefs.getString("tts_default_lang", "eng");
      this.mDefaultCountry = this.prefs.getString("tts_default_country", "USA");
      this.mDefaultLocVariant = this.prefs.getString("tts_default_variant", "");
      this.mDemoStringIndex = this.mDefaultLocPref.findIndexOfValue(this.mDefaultLanguage + "-" + this.mDefaultCountry);
      if (this.mDemoStringIndex > -1)
        this.mDefaultLocPref.setValueIndex(this.mDemoStringIndex);
      return;
      label129: useSupportedLocAsDefault();
    }
  }

  private void initDefaultSettings()
  {
    getContentResolver();
    this.mUseDefaultPref = ((CheckBoxPreference)findPreference("toggle_use_default_tts_settings"));
    int i = this.prefs.getInt("toggle_use_default_tts_settings", 0);
    CheckBoxPreference localCheckBoxPreference = this.mUseDefaultPref;
    if (i == 1);
    for (boolean bool = true; ; bool = false)
    {
      localCheckBoxPreference.setChecked(bool);
      this.mUseDefaultPref.setOnPreferenceChangeListener(this);
      this.mDefaultSynthPref = ((ListPreference)findPreference("tts_default_synth"));
      loadEngines();
      this.mDefaultSynthPref.setOnPreferenceChangeListener(this);
      this.mDefaultEng = this.prefs.getString("tts_default_synth", "com.svox.pico");
      this.mDefaultRatePref = ((ListPreference)findPreference("tts_default_rate"));
      this.mDefaultRate = this.prefs.getInt("tts_default_rate", this.mDefaultRate);
      this.mDefaultRatePref.setValue(String.valueOf(this.mDefaultRate));
      this.mDefaultRatePref.setOnPreferenceChangeListener(this);
      this.mDefaultLocPref = ((ListPreference)findPreference("tts_default_lang"));
      initDefaultLang();
      this.mDefaultLocPref.setOnPreferenceChangeListener(this);
      return;
    }
  }

  private void installVoiceData()
  {
    PackageManager localPackageManager = getPackageManager();
    Intent localIntent = new Intent();
    localIntent.setAction("android.speech.tts.engine.INSTALL_TTS_DATA");
    localIntent.addFlags(268435456);
    List localList = localPackageManager.queryIntentActivities(localIntent, 0);
    for (int i = 0; ; i++)
    {
      if (i >= localList.size())
        return;
      ActivityInfo localActivityInfo = ((ResolveInfo)localList.get(i)).activityInfo;
      if (this.mDefaultEng.equals(localActivityInfo.packageName))
      {
        localIntent.setClassName(this.mDefaultEng, localActivityInfo.name);
        startActivity(localIntent);
      }
    }
  }

  private boolean isCurrentLocSupported()
  {
    String str = Locale.getDefault().getISO3Language() + "-" + Locale.getDefault().getISO3Country();
    return this.mDefaultLocPref.findIndexOfValue(str) > -1;
  }

  private void loadEngines()
  {
    this.mDefaultSynthPref = ((ListPreference)findPreference("tts_default_synth"));
    Intent localIntent = new Intent("android.intent.action.START_TTS_ENGINE");
    ResolveInfo[] arrayOfResolveInfo1 = new ResolveInfo[0];
    PackageManager localPackageManager = getPackageManager();
    ResolveInfo[] arrayOfResolveInfo2 = (ResolveInfo[])localPackageManager.queryIntentActivities(localIntent, 0).toArray(arrayOfResolveInfo1);
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    String str1 = "";
    int i = 0;
    if (i >= arrayOfResolveInfo2.length)
    {
      this.prefsEditor.putString("tts_enabled_plugins", str1);
      this.prefsEditor.commit();
      CharSequence[] arrayOfCharSequence1 = new CharSequence[localArrayList1.size()];
      CharSequence[] arrayOfCharSequence2 = new CharSequence[localArrayList2.size()];
      this.mDefaultSynthPref.setEntries((CharSequence[])localArrayList1.toArray(arrayOfCharSequence1));
      this.mDefaultSynthPref.setEntryValues((CharSequence[])localArrayList2.toArray(arrayOfCharSequence2));
      String str3 = this.prefs.getString("tts_default_synth", "com.svox.pico");
      int j = this.mDefaultSynthPref.findIndexOfValue(str3);
      if (j == -1)
        j = this.mDefaultSynthPref.findIndexOfValue("com.svox.pico");
      this.mDefaultSynthPref.setValueIndex(j);
      return;
    }
    String str2 = arrayOfResolveInfo2[i].activityInfo.packageName;
    if (str2.equals("com.svox.pico"))
    {
      localArrayList1.add(arrayOfResolveInfo2[i].loadLabel(localPackageManager));
      localArrayList2.add(str2);
    }
    while (true)
    {
      i++;
      break;
      CheckBoxPreference localCheckBoxPreference = (CheckBoxPreference)findPreference("ENABLED_" + str2);
      if ((localCheckBoxPreference != null) && (localCheckBoxPreference.isChecked()))
      {
        localArrayList1.add(arrayOfResolveInfo2[i].loadLabel(localPackageManager));
        localArrayList2.add(str2);
        str1 = str1 + str2 + " ";
      }
    }
  }

  private void parseLocaleInfo(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString, "-");
    this.mDefaultLanguage = "";
    this.mDefaultCountry = "";
    this.mDefaultLocVariant = "";
    if (localStringTokenizer.hasMoreTokens())
      this.mDefaultLanguage = localStringTokenizer.nextToken().trim();
    if (localStringTokenizer.hasMoreTokens())
      this.mDefaultCountry = localStringTokenizer.nextToken().trim();
    if (localStringTokenizer.hasMoreTokens())
      this.mDefaultLocVariant = localStringTokenizer.nextToken().trim();
  }

  private boolean pluginHasSettings(String paramString)
  {
    PackageManager localPackageManager = getPackageManager();
    Intent localIntent = new Intent();
    localIntent.setClassName(paramString, paramString + ".EngineSettings");
    return localPackageManager.resolveActivity(localIntent, 65536) != null;
  }

  private void updateWidgetState()
  {
    this.mPlayExample.setEnabled(this.mEnableDemo);
    this.mUseDefaultPref.setEnabled(this.mEnableDemo);
    this.mDefaultRatePref.setEnabled(this.mEnableDemo);
    this.mDefaultLocPref.setEnabled(this.mEnableDemo);
    this.mInstallData.setEnabled(this.mVoicesMissing);
  }

  private void useCurrentLocAsDefault()
  {
    Locale localLocale = Locale.getDefault();
    this.prefsEditor.putString("tts_default_lang", localLocale.getISO3Language());
    this.prefsEditor.putString("tts_default_country", localLocale.getISO3Country());
    this.prefsEditor.putString("tts_default_variant", localLocale.getVariant());
    this.prefsEditor.commit();
  }

  private void useSupportedLocAsDefault()
  {
    this.prefsEditor.putString("tts_default_lang", "eng");
    this.prefsEditor.putString("tts_default_country", "USA");
    this.prefsEditor.putString("tts_default_variant", "");
    this.prefsEditor.commit();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 1977)
      if (paramIntent == null)
      {
        this.mEnableDemo = false;
        this.mVoicesMissing = false;
        updateWidgetState();
      }
    label178: label617: String str1;
    label798: 
    do
    {
      do
      {
        return;
        ArrayList localArrayList1 = paramIntent.getStringArrayListExtra("availableVoices");
        ArrayList localArrayList2 = paramIntent.getStringArrayListExtra("unavailableVoices");
        if ((this.mDefaultEng.equals("com.svox.pico")) && (Integer.parseInt(Build.VERSION.SDK) < 8) && (Integer.parseInt(Build.VERSION.SDK) > 3))
        {
          localArrayList1 = new ArrayList();
          localArrayList2 = new ArrayList();
          if (paramInt2 >= 0)
            break label178;
          localArrayList2.add("deu-DEU");
          localArrayList2.add("eng-GBR");
          localArrayList2.add("eng-USA");
          localArrayList2.add("spa-ESP");
          localArrayList2.add("fra-FRA");
          localArrayList2.add("ita-ITA");
        }
        while ((localArrayList1 == null) || (localArrayList2 == null))
        {
          this.mEnableDemo = false;
          this.mVoicesMissing = false;
          updateWidgetState();
          return;
          localArrayList1.add("deu-DEU");
          localArrayList1.add("eng-GBR");
          localArrayList1.add("eng-USA");
          localArrayList1.add("spa-ESP");
          localArrayList1.add("fra-FRA");
          localArrayList1.add("ita-ITA");
        }
        CharSequence[] arrayOfCharSequence1;
        CharSequence[] arrayOfCharSequence2;
        int i;
        String str2;
        int j;
        if (localArrayList1.size() > 0)
        {
          if (this.mTts == null)
            this.mTts = new TextToSpeechBeta(this, this);
          ListPreference localListPreference = (ListPreference)findPreference("tts_default_lang");
          arrayOfCharSequence1 = new CharSequence[localArrayList1.size()];
          arrayOfCharSequence2 = new CharSequence[localArrayList1.size()];
          i = -1;
          str2 = this.mDefaultLanguage;
          if (this.mDefaultCountry.length() > 0)
            str2 = str2 + "-" + this.mDefaultCountry;
          if (this.mDefaultLocVariant.length() > 0)
            str2 = str2 + "-" + this.mDefaultLocVariant;
          j = 0;
          if (j >= localArrayList1.size())
          {
            localListPreference.setEntries(arrayOfCharSequence1);
            localListPreference.setEntryValues(arrayOfCharSequence2);
            if (i > -1)
              localListPreference.setValueIndex(i);
            this.mEnableDemo = true;
            if (this.mTts.setLanguage(new Locale(this.mDefaultLanguage, this.mDefaultCountry, this.mDefaultLocVariant)) < 0)
            {
              Locale localLocale2 = Locale.getDefault();
              this.mDefaultLanguage = localLocale2.getISO3Language();
              this.mDefaultCountry = localLocale2.getISO3Country();
              this.mDefaultLocVariant = localLocale2.getVariant();
              if (this.mTts.setLanguage(new Locale(this.mDefaultLanguage, this.mDefaultCountry, this.mDefaultLocVariant)) < 0)
              {
                parseLocaleInfo(localListPreference.getEntryValues()[0].toString());
                this.mTts.setLanguage(new Locale(this.mDefaultLanguage, this.mDefaultCountry, this.mDefaultLocVariant));
              }
              this.prefsEditor.putString("tts_default_lang", this.mDefaultLanguage);
              this.prefsEditor.putString("tts_default_country", this.mDefaultCountry);
              this.prefsEditor.putString("tts_default_variant", this.mDefaultLocVariant);
              this.prefsEditor.commit();
            }
            if (localArrayList2.size() <= 0)
              break label798;
          }
        }
        for (this.mVoicesMissing = true; ; this.mVoicesMissing = false)
        {
          updateWidgetState();
          return;
          String[] arrayOfString = ((String)localArrayList1.get(j)).split("-");
          Locale localLocale1;
          if (arrayOfString.length == 1)
            localLocale1 = new Locale(arrayOfString[0]);
          while (true)
          {
            if (localLocale1 != null)
            {
              arrayOfCharSequence1[j] = localLocale1.getDisplayName();
              arrayOfCharSequence2[j] = ((CharSequence)localArrayList1.get(j));
              if (arrayOfCharSequence2[j].equals(str2))
                i = j;
            }
            j++;
            break;
            if (arrayOfString.length == 2)
            {
              localLocale1 = new Locale(arrayOfString[0], arrayOfString[1]);
            }
            else
            {
              int k = arrayOfString.length;
              localLocale1 = null;
              if (k == 3)
                localLocale1 = new Locale(arrayOfString[0], arrayOfString[1], arrayOfString[2]);
            }
          }
          this.mEnableDemo = false;
          break label617;
        }
      }
      while (paramInt1 != 1983);
      if (paramInt2 != 0)
        break;
      str1 = getString(2131165205);
      if ((paramIntent != null) && (paramIntent.getStringExtra("sampleText") != null))
        str1 = paramIntent.getStringExtra("sampleText");
    }
    while (this.mTts == null);
    this.mTts.speak(str1, 0, null);
    return;
    Log.e("TextToSpeechSettings", "Did not have a sample string for the requested language");
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
    this.prefsEditor = this.prefs.edit();
    addPreferencesFromResource(2130968577);
    addEngineSpecificSettings();
    this.mDemoStrings = getResources().getStringArray(2131099652);
    setVolumeControlStream(3);
    this.mEnableDemo = false;
    this.mTtsStarted = false;
    Locale localLocale = Locale.getDefault();
    this.mDefaultLanguage = localLocale.getISO3Language();
    this.mDefaultCountry = localLocale.getISO3Country();
    this.mDefaultLocVariant = localLocale.getVariant();
    this.mTts = new TextToSpeechBeta(this, this);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    if (this.mTts != null)
      this.mTts.shutdown();
  }

  public void onInit(int paramInt1, int paramInt2)
  {
    if (paramInt1 == 0)
    {
      this.mEnableDemo = true;
      if (this.mDefaultLanguage == null)
        this.mDefaultLanguage = Locale.getDefault().getISO3Language();
      if (this.mDefaultCountry == null)
        this.mDefaultCountry = Locale.getDefault().getISO3Country();
      if (this.mDefaultLocVariant == null)
        this.mDefaultLocVariant = new String();
      initDefaultSettings();
      this.mTts.setLanguage(new Locale(this.mDefaultLanguage, this.mDefaultCountry, this.mDefaultLocVariant));
      this.mTts.setSpeechRate(this.mDefaultRate / 100.0F);
      this.mTts.setEngineByPackageNameExtended(this.mDefaultEng);
      initClickers();
      updateWidgetState();
      checkVoiceData();
      this.mTtsStarted = true;
      Log.v("TextToSpeechSettings", "TTS engine for settings screen initialized.");
    }
    while (true)
    {
      updateWidgetState();
      return;
      Log.v("TextToSpeechSettings", "TTS engine for settings screen failed to initialize successfully.");
      this.mEnableDemo = false;
    }
  }

  protected void onPause()
  {
    super.onPause();
    if ((this.mDefaultRatePref != null) && (this.mDefaultRatePref.getDialog() != null))
      this.mDefaultRatePref.getDialog().dismiss();
    if ((this.mDefaultLocPref != null) && (this.mDefaultLocPref.getDialog() != null))
      this.mDefaultLocPref.getDialog().dismiss();
    if ((this.mDefaultSynthPref != null) && (this.mDefaultSynthPref.getDialog() != null))
      this.mDefaultSynthPref.getDialog().dismiss();
  }

  public boolean onPreferenceChange(Preference paramPreference, Object paramObject)
  {
    if ("toggle_use_default_tts_settings".equals(paramPreference.getKey()))
    {
      boolean bool = ((Boolean)paramObject).booleanValue();
      int j = 0;
      if (bool)
        j = 1;
      this.prefsEditor.putInt("toggle_use_default_tts_settings", j);
      this.prefsEditor.commit();
      Log.i("TextToSpeechSettings", "TTS use default settings is " + paramObject.toString());
    }
    do
    {
      return true;
      if ("tts_default_rate".equals(paramPreference.getKey()))
      {
        this.mDefaultRate = Integer.parseInt((String)paramObject);
        try
        {
          this.prefsEditor.putInt("tts_default_rate", this.mDefaultRate);
          this.prefsEditor.commit();
          if (this.mTts != null)
            this.mTts.setSpeechRate(this.mDefaultRate / 100.0F);
          Log.i("TextToSpeechSettings", "TTS default rate is " + this.mDefaultRate);
          return true;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          Log.e("TextToSpeechSettings", "could not persist default TTS rate setting", localNumberFormatException);
          return true;
        }
      }
      if ("tts_default_lang".equals(paramPreference.getKey()))
      {
        getContentResolver();
        parseLocaleInfo((String)paramObject);
        this.prefsEditor.putString("tts_default_lang", this.mDefaultLanguage);
        this.prefsEditor.putString("tts_default_country", this.mDefaultCountry);
        this.prefsEditor.putString("tts_default_variant", this.mDefaultLocVariant);
        this.prefsEditor.commit();
        Log.v("TextToSpeechSettings", "TTS default lang/country/variant set to " + this.mDefaultLanguage + "/" + this.mDefaultCountry + "/" + this.mDefaultLocVariant);
        if (this.mTts != null)
          this.mTts.setLanguage(new Locale(this.mDefaultLanguage, this.mDefaultCountry, this.mDefaultLocVariant));
        int i = this.mDefaultLocPref.findIndexOfValue((String)paramObject);
        Log.v("Settings", " selected is " + i);
        if (i > -1);
        while (true)
        {
          this.mDemoStringIndex = i;
          return true;
          i = 0;
        }
      }
    }
    while (!"tts_default_synth".equals(paramPreference.getKey()));
    this.mDefaultEng = paramObject.toString();
    this.prefsEditor.putString("tts_default_synth", this.mDefaultEng);
    this.prefsEditor.commit();
    if (this.mTts != null)
    {
      this.mTts.setEngineByPackageNameExtended(this.mDefaultEng);
      this.mEnableDemo = false;
      this.mVoicesMissing = false;
      updateWidgetState();
      checkVoiceData();
    }
    Log.v("Settings", "The default synth is: " + paramObject.toString());
    return true;
  }

  public boolean onPreferenceClick(Preference paramPreference)
  {
    if (paramPreference == this.mPlayExample)
    {
      getSampleText();
      return true;
    }
    if (paramPreference == this.mInstallData)
    {
      installVoiceData();
      finish();
      return true;
    }
    return false;
  }

  public boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference)
  {
    if ((paramPreference instanceof CheckBoxPreference))
    {
      final CheckBoxPreference localCheckBoxPreference = (CheckBoxPreference)paramPreference;
      if (!localCheckBoxPreference.getKey().equals("toggle_use_default_tts_settings"))
      {
        if (localCheckBoxPreference.isChecked())
        {
          localCheckBoxPreference.setChecked(false);
          AlertDialog.Builder localBuilder = new AlertDialog.Builder(this).setTitle(17039380).setIcon(17301543);
          Object[] arrayOfObject = new Object[1];
          arrayOfObject[0] = localCheckBoxPreference.getTitle();
          localBuilder.setMessage(getString(2131165210, arrayOfObject)).setCancelable(true).setPositiveButton(17039370, new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              localCheckBoxPreference.setChecked(true);
              TextToSpeechSettings.this.loadEngines();
            }
          }).setNegativeButton(17039360, new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
            }
          }).create().show();
          return true;
        }
        loadEngines();
        return true;
      }
    }
    return false;
  }

  protected void onStart()
  {
    super.onStart();
    if (this.mTtsStarted)
    {
      initClickers();
      updateWidgetState();
      checkVoiceData();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.TextToSpeechSettings
 * JD-Core Version:    0.6.2
 */