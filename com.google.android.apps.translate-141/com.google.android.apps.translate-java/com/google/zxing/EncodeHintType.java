package com.google.zxing;

public final class EncodeHintType
{
  public static final EncodeHintType CHARACTER_SET = new EncodeHintType();
  public static final EncodeHintType ERROR_CORRECTION = new EncodeHintType();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.zxing.EncodeHintType
 * JD-Core Version:    0.6.2
 */