package com.google.zxing;

public abstract interface ResultPointCallback
{
  public abstract void foundPossibleResultPoint(ResultPoint paramResultPoint);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.zxing.ResultPointCallback
 * JD-Core Version:    0.6.2
 */