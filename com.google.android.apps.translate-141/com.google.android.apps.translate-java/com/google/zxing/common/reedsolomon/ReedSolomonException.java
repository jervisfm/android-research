package com.google.zxing.common.reedsolomon;

public final class ReedSolomonException extends Exception
{
  public ReedSolomonException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.zxing.common.reedsolomon.ReedSolomonException
 * JD-Core Version:    0.6.2
 */