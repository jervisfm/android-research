package com.x.google.common;

public abstract interface Clock
{
  public static final long INVALID_TIME = -9223372036854775808L;

  public abstract long currentTimeMillis();

  public abstract long relativeTimeMillis();

  public abstract long uptimeMillis();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.Clock
 * JD-Core Version:    0.6.2
 */