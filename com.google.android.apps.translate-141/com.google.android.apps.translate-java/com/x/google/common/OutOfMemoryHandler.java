package com.x.google.common;

public abstract interface OutOfMemoryHandler
{
  public abstract void handleOutOfMemory(boolean paramBoolean);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.OutOfMemoryHandler
 * JD-Core Version:    0.6.2
 */