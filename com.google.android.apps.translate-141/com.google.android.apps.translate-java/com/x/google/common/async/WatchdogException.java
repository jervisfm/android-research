package com.x.google.common.async;

import java.io.IOException;

public class WatchdogException extends IOException
{
  public WatchdogException()
  {
  }

  public WatchdogException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.async.WatchdogException
 * JD-Core Version:    0.6.2
 */