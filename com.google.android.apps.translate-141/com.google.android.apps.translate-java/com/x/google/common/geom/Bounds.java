package com.x.google.common.geom;

public abstract interface Bounds
{
  public abstract int getBottom();

  public abstract int getHeight();

  public abstract int getLeft();

  public abstract int getRight();

  public abstract int getTop();

  public abstract int getWidth();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.geom.Bounds
 * JD-Core Version:    0.6.2
 */