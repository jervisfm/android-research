package com.x.google.common.io;

public abstract class BaseHttpConnectionFactory extends BaseConnectionFactory
  implements HttpConnectionFactory
{
  protected static final String NETWORK_AVAILABLE_PREF_NAME = "HttpWorks";

  protected BaseHttpConnectionFactory()
  {
    super("HttpWorks");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.io.BaseHttpConnectionFactory
 * JD-Core Version:    0.6.2
 */