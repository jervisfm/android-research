package com.x.google.common.io;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract interface GoogleTcpConnection
{
  public abstract void close()
    throws IOException;

  public abstract void notifyTimeout();

  public abstract DataInputStream openDataInputStream()
    throws IOException;

  public abstract DataOutputStream openDataOutputStream()
    throws IOException;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.io.GoogleTcpConnection
 * JD-Core Version:    0.6.2
 */