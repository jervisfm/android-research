package com.x.google.common.task;

public abstract interface RunnableWithParams
{
  public abstract void run(int paramInt, Object[] paramArrayOfObject);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.task.RunnableWithParams
 * JD-Core Version:    0.6.2
 */