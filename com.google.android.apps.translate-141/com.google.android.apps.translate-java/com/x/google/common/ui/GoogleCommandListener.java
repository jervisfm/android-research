package com.x.google.common.ui;

public abstract interface GoogleCommandListener
{
  public abstract void commandAction(GoogleCommand paramGoogleCommand, Object paramObject);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.ui.GoogleCommandListener
 * JD-Core Version:    0.6.2
 */