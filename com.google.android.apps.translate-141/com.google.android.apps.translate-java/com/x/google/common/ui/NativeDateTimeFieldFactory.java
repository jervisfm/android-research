package com.x.google.common.ui;

public abstract interface NativeDateTimeFieldFactory
{
  public abstract NativeDateTimeField createNativeDateTimeField(GoogleCommandListener paramGoogleCommandListener, String paramString, GoogleCommand paramGoogleCommand1, GoogleCommand paramGoogleCommand2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.ui.NativeDateTimeFieldFactory
 * JD-Core Version:    0.6.2
 */