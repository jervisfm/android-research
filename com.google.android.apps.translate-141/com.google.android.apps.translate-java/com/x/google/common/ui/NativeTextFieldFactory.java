package com.x.google.common.ui;

public abstract interface NativeTextFieldFactory
{
  public abstract NativeTextField createNativeTextField(GoogleCommandListener paramGoogleCommandListener, String paramString, GoogleCommand paramGoogleCommand1, GoogleCommand paramGoogleCommand2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.ui.NativeTextFieldFactory
 * JD-Core Version:    0.6.2
 */