package com.x.google.common.ui;

public abstract interface RepaintListener
{
  public abstract void repaint();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.ui.RepaintListener
 * JD-Core Version:    0.6.2
 */