package com.x.google.common.util;

public abstract interface Comparator
{
  public abstract int compare(Object paramObject1, Object paramObject2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.util.Comparator
 * JD-Core Version:    0.6.2
 */