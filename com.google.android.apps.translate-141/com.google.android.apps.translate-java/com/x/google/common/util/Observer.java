package com.x.google.common.util;

public abstract interface Observer
{
  public abstract void update(Observable paramObservable, Object paramObject);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.common.util.Observer
 * JD-Core Version:    0.6.2
 */