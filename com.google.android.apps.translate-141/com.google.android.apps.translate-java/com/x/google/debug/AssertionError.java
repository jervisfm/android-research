package com.x.google.debug;

public final class AssertionError extends RuntimeException
{
  public AssertionError()
  {
  }

  public AssertionError(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.debug.AssertionError
 * JD-Core Version:    0.6.2
 */