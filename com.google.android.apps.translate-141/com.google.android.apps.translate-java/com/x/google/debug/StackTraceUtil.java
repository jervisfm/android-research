package com.x.google.debug;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class StackTraceUtil
{
  private static PrintStream err;

  public static String getCallStack(Throwable paramThrowable)
  {
    try
    {
      PrintStream localPrintStream = System.err;
      ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
      err = new PrintStream(localByteArrayOutputStream);
      paramThrowable.printStackTrace();
      String str = localByteArrayOutputStream.toString();
      err = localPrintStream;
      return str;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.debug.StackTraceUtil
 * JD-Core Version:    0.6.2
 */