package com.x.google.masf;

import java.io.IOException;
import java.io.InputStream;

public abstract interface InputStreamProvider
{
  public abstract void dispose();

  public abstract InputStream getInputStream()
    throws IOException;

  public abstract int getStreamLength()
    throws IOException;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.masf.InputStreamProvider
 * JD-Core Version:    0.6.2
 */