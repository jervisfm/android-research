package com.x.google.masf.services;

public abstract interface AnalyticsImpression
{
  public static final int UTM_CAMPAIGN = 2;
  public static final int UTM_CREATIVE = 3;
  public static final int UTM_SIZE = 4;
  public static final int UTM_SOURCE = 1;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.masf.services.AnalyticsImpression
 * JD-Core Version:    0.6.2
 */