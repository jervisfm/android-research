package com.x.google.masf.services;

public abstract interface DummyStubbyServerRequest
{
  public static final int AN_INT = 1;
  public static final int A_STRING = 2;
  public static final int CLIENT_INFO = 257;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.masf.services.DummyStubbyServerRequest
 * JD-Core Version:    0.6.2
 */