package com.x.google.masf.services;

public abstract interface LogDataProto
{
  public static final int CLIENT_INFO = 1;
  public static final int ENTRY = 2;
  public static final int ENTRY_LOG_TEXT = 5;
  public static final int ENTRY_SOURCE = 4;
  public static final int ENTRY_TIMESTAMP = 3;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.masf.services.LogDataProto
 * JD-Core Version:    0.6.2
 */