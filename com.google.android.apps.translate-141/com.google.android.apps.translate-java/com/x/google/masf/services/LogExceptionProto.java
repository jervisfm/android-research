package com.x.google.masf.services;

public abstract interface LogExceptionProto
{
  public static final int CLIENT_INFO = 1;
  public static final int CLIENT_TIMESTAMP = 2;
  public static final int EXCEPTION_CLASS_NAME = 21;
  public static final int EXCEPTION_MESSAGE = 22;
  public static final int EXCEPTION_STACKTRACE = 23;
  public static final int SOURCE = 20;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.masf.services.LogExceptionProto
 * JD-Core Version:    0.6.2
 */