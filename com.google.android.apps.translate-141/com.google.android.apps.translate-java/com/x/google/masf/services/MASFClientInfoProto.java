package com.x.google.masf.services;

public abstract interface MASFClientInfoProto
{
  public static final int APP_NAME = 1;
  public static final int APP_VERSION = 2;
  public static final int CLIENT_LOCALE = 5;
  public static final int COOKIE = 7;
  public static final int DIST_CHANNEL = 4;
  public static final int PLATFORM = 3;
  public static final int REQUEST_IP = 6;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.x.google.masf.services.MASFClientInfoProto
 * JD-Core Version:    0.6.2
 */