.class public Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ActionBarSpinnerAdapter.java"

# interfaces
.implements Landroid/widget/SpinnerAdapter;
.implements Landroid/app/ActionBar$OnNavigationListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Landroid/widget/SpinnerAdapter;",
        "Landroid/app/ActionBar$OnNavigationListener;"
    }
.end annotation


# static fields
.field public static final CONVERSATION_FRAGMENT_TAG:Ljava/lang/String; = "conversation"

.field public static final CONVERSATION_ITEM_POSITION:I = 0x1

.field public static final FAVORITE_FRAGMENT_TAG:Ljava/lang/String; = "favorite"

.field public static final FAVORITE_ITEM_POSITION:I = 0x3

.field public static final HISTORY_FRAGMENT_TAG:Ljava/lang/String; = "history"

.field public static final HISTORY_ITEM_POSITION:I = 0x2

.field private static final SHOW_SMS:Z = false

.field public static final SMS_PICKER_FRAGMENT_TAG:Ljava/lang/String; = "sms"

.field public static final SMS_PICKER_ITEM_POSITION:I = 0x4

.field private static final TAG:Ljava/lang/String; = "ActionBarSpinnerAdapter"

.field public static final TRANSATE_FRAGMENT_TAG:Ljava/lang/String; = "translate"

.field public static final TRANSLATE_ITEM_POSITION:I

.field public static final USE_HISTORY_STACK:Z


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mConversationFragment:Lcom/google/android/apps/translate/conversation/ConversationFragment;

.field private mConversationItem:Ljava/lang/String;

.field private mCurrentFragment:Landroid/app/Fragment;

.field private mFavoriteFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

.field private mFavoriteItem:Ljava/lang/String;

.field private mHistoryFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

.field private mHistoryItem:Ljava/lang/String;

.field private mSelectedPosition:I

.field private mSmsPickerFragment:Lcom/google/android/apps/translate/SmsPickerFragment;

.field private mSmsPickerItem:Ljava/lang/String;

.field private mTabMenuMode:Z

.field private mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragment;

.field private mTranslateFragmentWrapper:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

.field private mTranslateItem:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;IIZLcom/google/android/apps/translate/translation/TranslateFragmentWrapper;)V
    .locals 3
    .parameter "context"
    .parameter "resource"
    .parameter "textViewResourceId"
    .parameter "tabMenuMode"
    .parameter "translateFragmentWrapper"

    .prologue
    const/4 v2, 0x0

    .line 82
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 50
    iput-boolean v2, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTabMenuMode:Z

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSelectedPosition:I

    .line 83
    const-string v0, "ActionBarSpinnerAdapter"

    const-string v1, "ActionBarSpinnerAdapter"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    check-cast p1, Landroid/app/Activity;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    .line 85
    iput-object p5, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateFragmentWrapper:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/translate/R$string;->btn_translate:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateItem:Ljava/lang/String;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/translate/R$string;->menu_conversation_mode:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mConversationItem:Ljava/lang/String;

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/translate/R$string;->label_history:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mHistoryItem:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/translate/R$string;->label_favorites:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mFavoriteItem:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/translate/R$string;->title_sms_translation:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSmsPickerItem:Ljava/lang/String;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->clear()V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateItem:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->add(Ljava/lang/Object;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mConversationItem:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->add(Ljava/lang/Object;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mHistoryItem:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->add(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mFavoriteItem:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->add(Ljava/lang/Object;)V

    .line 101
    invoke-virtual {p0, p4}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->setTabMenuMode(Z)V

    .line 102
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->onNavigationItemSelected(IJ)Z

    .line 103
    return-void
.end method

.method private postProcessPreviousFragment(II)V
    .locals 1
    .parameter "oldPosition"
    .parameter "newPosition"

    .prologue
    const/4 v0, 0x0

    .line 140
    packed-switch p1, :pswitch_data_0

    .line 166
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 144
    :pswitch_1
    if-eq p1, p2, :cond_0

    .line 146
    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mConversationFragment:Lcom/google/android/apps/translate/conversation/ConversationFragment;

    goto :goto_0

    .line 150
    :pswitch_2
    if-eq p1, p2, :cond_0

    .line 152
    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mHistoryFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

    goto :goto_0

    .line 156
    :pswitch_3
    if-eq p1, p2, :cond_0

    .line 158
    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mFavoriteFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

    goto :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public static removeActionBarTitle(Landroid/app/ActionBar;)V
    .locals 3
    .parameter "actionBar"

    .prologue
    const/4 v2, 0x0

    .line 401
    const-string v0, "ActionBarSpinnerAdapter"

    const-string v1, "removeActionBarTitle"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 403
    invoke-virtual {p0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 404
    invoke-virtual {p0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 405
    return-void
.end method

.method public static setActionBarTitle(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 5
    .parameter "activity"
    .parameter "title"

    .prologue
    .line 386
    const-string v3, "ActionBarSpinnerAdapter"

    const-string v4, "setActionBarTitle"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 388
    .local v0, actionBar:Landroid/app/ActionBar;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 389
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 390
    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 392
    .local v1, inflator:Landroid/view/LayoutInflater;
    sget v3, Lcom/google/android/apps/translate/R$layout;->action_bar_spinner_item:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 393
    .local v2, textView:Landroid/widget/TextView;
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 394
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 395
    return-void
.end method

.method private startConversationFragment(Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 3
    .parameter "translateEntry"

    .prologue
    .line 238
    const-string v0, "ActionBarSpinnerAdapter"

    const-string v1, "startConversationFragment"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mConversationFragment:Lcom/google/android/apps/translate/conversation/ConversationFragment;

    if-nez v0, :cond_0

    .line 240
    new-instance v0, Lcom/google/android/apps/translate/conversation/ConversationFragment;

    invoke-direct {v0, p1}, Lcom/google/android/apps/translate/conversation/ConversationFragment;-><init>(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mConversationFragment:Lcom/google/android/apps/translate/conversation/ConversationFragment;

    .line 242
    :cond_0
    sget v0, Lcom/google/android/apps/translate/R$id;->fragment_container:I

    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mConversationFragment:Lcom/google/android/apps/translate/conversation/ConversationFragment;

    const-string v2, "conversation"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->switchFragment(ILandroid/app/Fragment;Ljava/lang/String;)V

    .line 243
    return-void
.end method

.method private startFavoriteFragment()V
    .locals 3

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mFavoriteFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

    if-nez v0, :cond_0

    .line 254
    new-instance v0, Lcom/google/android/apps/translate/history/HistoryFragment;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/translate/history/HistoryFragment;-><init>(ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mFavoriteFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

    .line 256
    :cond_0
    sget v0, Lcom/google/android/apps/translate/R$id;->fragment_container:I

    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mFavoriteFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

    const-string v2, "favorite"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->switchFragment(ILandroid/app/Fragment;Ljava/lang/String;)V

    .line 257
    return-void
.end method

.method private startHistoryFragment()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mHistoryFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

    if-nez v0, :cond_0

    .line 247
    new-instance v0, Lcom/google/android/apps/translate/history/HistoryFragment;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/translate/history/HistoryFragment;-><init>(ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mHistoryFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

    .line 249
    :cond_0
    sget v0, Lcom/google/android/apps/translate/R$id;->fragment_container:I

    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mHistoryFragment:Lcom/google/android/apps/translate/history/HistoryFragment;

    const-string v2, "history"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->switchFragment(ILandroid/app/Fragment;Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method private startSmsPickerFragment()V
    .locals 3

    .prologue
    .line 260
    const-string v0, "ActionBarSpinnerAdapter"

    const-string v1, "startSmsPickerFragment"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSmsPickerFragment:Lcom/google/android/apps/translate/SmsPickerFragment;

    if-nez v0, :cond_0

    .line 262
    new-instance v0, Lcom/google/android/apps/translate/SmsPickerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/translate/SmsPickerFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSmsPickerFragment:Lcom/google/android/apps/translate/SmsPickerFragment;

    .line 264
    :cond_0
    sget v0, Lcom/google/android/apps/translate/R$id;->fragment_container:I

    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSmsPickerFragment:Lcom/google/android/apps/translate/SmsPickerFragment;

    const-string v2, "sms"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->switchFragment(ILandroid/app/Fragment;Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method private startTranslateFragment()V
    .locals 3

    .prologue
    .line 230
    const-string v0, "ActionBarSpinnerAdapter"

    const-string v1, "startTranslateFragment"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragment;

    if-nez v0, :cond_0

    .line 232
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateFragment;

    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateFragmentWrapper:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/translate/translation/TranslateFragment;-><init>(Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragment;

    .line 234
    :cond_0
    sget v0, Lcom/google/android/apps/translate/R$id;->fragment_container:I

    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragment;

    const-string v2, "translate"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->switchFragment(ILandroid/app/Fragment;Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method private switchFragment(ILandroid/app/Fragment;Ljava/lang/String;)V
    .locals 3
    .parameter "fragmentContainerResourceId"
    .parameter "fragment"
    .parameter "fragTag"

    .prologue
    .line 222
    iget-object v2, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 223
    .local v0, fragmentManager:Landroid/app/FragmentManager;
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 224
    .local v1, transaction:Landroid/app/FragmentTransaction;
    invoke-virtual {v1, p1, p2, p3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 225
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 226
    iput-object p2, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    .line 227
    return-void
.end method


# virtual methods
.method public getCurrentFragment()Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    return-object v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v4, 0x0

    .line 185
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 186
    .local v2, view:Landroid/view/View;
    sget v3, Lcom/google/android/apps/translate/R$id;->spinner_item_text_view:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 189
    .local v1, textView:Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v0

    .line 190
    .local v0, padding:I
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 191
    sget v3, Lcom/google/android/apps/translate/R$drawable;->bg_action_bar:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 193
    packed-switch p1, :pswitch_data_0

    .line 217
    :goto_0
    return-object v2

    .line 195
    :pswitch_0
    sget v3, Lcom/google/android/apps/translate/R$drawable;->spinner_icon_translate_bw:I

    invoke-virtual {v1, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    .line 199
    :pswitch_1
    sget v3, Lcom/google/android/apps/translate/R$drawable;->spinner_icon_conversation_bw:I

    invoke-virtual {v1, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    .line 203
    :pswitch_2
    sget v3, Lcom/google/android/apps/translate/R$drawable;->spinner_icon_history_bw:I

    invoke-virtual {v1, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    .line 207
    :pswitch_3
    sget v3, Lcom/google/android/apps/translate/R$drawable;->spinner_icon_starred_bw:I

    invoke-virtual {v1, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    .line 211
    :pswitch_4
    sget v3, Lcom/google/android/apps/translate/R$drawable;->spinner_icon_sms_translation_bw:I

    invoke-virtual {v1, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getSelectedNavigationIndex()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 299
    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    if-eqz v1, :cond_0

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/google/android/apps/translate/translation/TranslateFragment;

    if-eqz v1, :cond_1

    .line 314
    :cond_0
    :goto_0
    return v0

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/google/android/apps/translate/conversation/ConversationFragment;

    if-eqz v1, :cond_2

    .line 303
    const/4 v0, 0x1

    goto :goto_0

    .line 304
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/google/android/apps/translate/history/HistoryFragment;

    if-eqz v1, :cond_4

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/google/android/apps/translate/history/HistoryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/HistoryFragment;->isHistoryMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 306
    const/4 v0, 0x2

    goto :goto_0

    .line 308
    :cond_3
    const/4 v0, 0x3

    goto :goto_0

    .line 310
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/google/android/apps/translate/SmsPickerFragment;

    if-eqz v1, :cond_0

    .line 311
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public getTranslateFragment()Lcom/google/android/apps/translate/translation/TranslateFragment;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragment;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTabMenuMode:Z

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 180
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public isTabMenuMode()Z
    .locals 1

    .prologue
    .line 379
    iget-boolean v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTabMenuMode:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 2
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 321
    const-string v0, "ActionBarSpinnerAdapter"

    const-string v1, "onActivityResult"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSelectedPosition:I

    packed-switch v0, :pswitch_data_0

    .line 332
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 325
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 328
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 322
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/google/android/apps/translate/editor/PreImeAutoCompleteTextView$OnKeyPreImeListener;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mCurrentFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/google/android/apps/translate/editor/PreImeAutoCompleteTextView$OnKeyPreImeListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/translate/editor/PreImeAutoCompleteTextView$OnKeyPreImeListener;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    const/4 v0, 0x1

    .line 285
    :goto_0
    return v0

    .line 273
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    .line 285
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onNavigationItemSelected(IJ)Z
    .locals 4
    .parameter "position"
    .parameter "itemId"

    .prologue
    const/4 v1, 0x1

    .line 107
    const-string v2, "ActionBarSpinnerAdapter"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNavigationItemSelected itemPosition="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " itemId="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " item="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSelectedPosition:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->postProcessPreviousFragment(II)V

    .line 110
    iget v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSelectedPosition:I

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 136
    :goto_0
    return v0

    .line 113
    :cond_0
    iput p1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mSelectedPosition:I

    .line 116
    packed-switch p1, :pswitch_data_0

    .line 136
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->startTranslateFragment()V

    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateFragment;->getHelper()Lcom/google/android/apps/translate/translation/TranslateHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getInitialTranslationEntry()Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->startConversationFragment(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    move v0, v1

    .line 123
    goto :goto_0

    .line 125
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->startHistoryFragment()V

    move v0, v1

    .line 126
    goto :goto_0

    .line 128
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->startFavoriteFragment()V

    move v0, v1

    .line 129
    goto :goto_0

    .line 131
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->startSmsPickerFragment()V

    move v0, v1

    .line 132
    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setTabMenuMode(Z)V
    .locals 3
    .parameter "tabMenuMode"

    .prologue
    .line 339
    iput-boolean p1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTabMenuMode:Z

    .line 341
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v2, p0}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->setActionBarTitle(Landroid/app/Activity;Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;)V

    .line 342
    iget-object v1, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/translate/R$id;->tab_menu_fake_list:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 344
    .local v0, fakeView:Landroid/widget/LinearLayout;
    if-eqz p1, :cond_0

    const/4 v1, 0x4

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 345
    return-void

    .line 344
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public setTabWidth()V
    .locals 4

    .prologue
    .line 351
    const-string v2, "ActionBarSpinnerAdapter"

    const-string v3, "setTabWidth"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    iget-boolean v2, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mTabMenuMode:Z

    if-nez v2, :cond_0

    .line 353
    const-string v2, "ActionBarSpinnerAdapter"

    const-string v3, "setTabWidth BYE!"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 357
    .local v1, rootView:Landroid/view/View;
    sget v2, Lcom/google/android/apps/translate/R$id;->tab_menu_fragment_container:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 359
    .local v0, containerView:Landroid/widget/LinearLayout;
    new-instance v2, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/apps/translate/ActionBarSpinnerAdapter$1;-><init>(Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;Landroid/view/View;Landroid/widget/LinearLayout;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
