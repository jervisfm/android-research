.class Lcom/google/android/apps/translate/HomeActivity$4;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/HomeActivity;->showEulaDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/HomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/HomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 330
    iput-object p1, p0, Lcom/google/android/apps/translate/HomeActivity$4;->this$0:Lcom/google/android/apps/translate/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .parameter "dialog"
    .parameter "id"

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity$4;->this$0:Lcom/google/android/apps/translate/HomeActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Profile;->setEulaAccepted(Landroid/content/Context;Z)V

    .line 335
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->useFragments()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 344
    return-void

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity$4;->this$0:Lcom/google/android/apps/translate/HomeActivity;

    #calls: Lcom/google/android/apps/translate/HomeActivity;->startTranslateActivity()V
    invoke-static {v0}, Lcom/google/android/apps/translate/HomeActivity;->access$200(Lcom/google/android/apps/translate/HomeActivity;)V

    goto :goto_0
.end method
