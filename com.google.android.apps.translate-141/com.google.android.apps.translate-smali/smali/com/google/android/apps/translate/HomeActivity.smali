.class public Lcom/google/android/apps/translate/HomeActivity;
.super Landroid/app/Activity;
.source "HomeActivity.java"


# static fields
.field private static final BULLET_MARK:Ljava/lang/String; = "\u2022"

.field private static final EULA_TEST:Z = false

.field private static final TAG:Ljava/lang/String; = "HomeActivity"


# instance fields
.field private mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

.field private mHandler:Landroid/os/Handler;

.field private mInitRun:Z

.field private final mOnVoiceSupportedLanguagesResult:Landroid/content/BroadcastReceiver;

.field private mShowEula:Z

.field private mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

.field private mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mHandler:Landroid/os/Handler;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mShowEula:Z

    .line 56
    new-instance v0, Lcom/google/android/apps/translate/HomeActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/HomeActivity$1;-><init>(Lcom/google/android/apps/translate/HomeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mOnVoiceSupportedLanguagesResult:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/HomeActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/HomeActivity;->reportCsiMetrics()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/translate/HomeActivity;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/apps/translate/HomeActivity;->mInitRun:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/HomeActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/HomeActivity;->startTranslateActivity()V

    return-void
.end method

.method private checkVersion()V
    .locals 6

    .prologue
    .line 159
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->getCurrentVersionCode(Landroid/content/Context;)I

    move-result v1

    .line 160
    .local v1, versionCodeInSettings:I
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getVersionCode(Landroid/content/Context;)I

    move-result v0

    .line 161
    .local v0, versionCode:I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "version code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 164
    if-eq v1, v0, :cond_1

    .line 165
    const-string v2, "version code changed (from %d to %d)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    const/16 v2, 0xa

    if-gt v1, v2, :cond_0

    .line 172
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->resetFirstRunFlags(Landroid/content/Context;)V

    .line 174
    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Profile;->setCurrentVersionCode(Landroid/content/Context;I)V

    .line 176
    :cond_1
    return-void
.end method

.method public static getWelcomeText(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .parameter "context"

    .prologue
    .line 292
    sget v0, Lcom/google/android/apps/translate/R$string;->welcome_text:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "\u2022"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "\u2022"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "\u2022"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "\u2022"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private reportCsiMetrics()V
    .locals 5

    .prologue
    .line 204
    const-string v1, "HomeActivity"

    const-string v2, "reportCsiMetrics"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/translate/HomeActivity;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "init"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/CsiTimer;->end([Ljava/lang/String;)V

    .line 207
    iget-object v1, p0, Lcom/google/android/apps/translate/HomeActivity;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/translate/CsiTimer;->report(Landroid/content/Context;)V

    .line 209
    new-instance v1, Lcom/google/android/apps/translate/CsiTimer;

    const-string v2, "home"

    invoke-direct {v1, v2}, Lcom/google/android/apps/translate/CsiTimer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/HomeActivity;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/translate/HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/TranslateApplication;

    .line 213
    .local v0, app:Lcom/google/android/apps/translate/TranslateApplication;
    invoke-virtual {v0}, Lcom/google/android/apps/translate/TranslateApplication;->endAndReportAppCsiTimer()V

    .line 214
    return-void
.end method

.method private declared-synchronized showEulaDialog()V
    .locals 8

    .prologue
    .line 300
    monitor-enter p0

    :try_start_0
    const-string v6, "layout_inflater"

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/HomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 302
    .local v3, inflater:Landroid/view/LayoutInflater;
    sget v6, Lcom/google/android/apps/translate/R$layout;->eula:I

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 305
    .local v2, dialogContentView:Landroid/view/View;
    sget v6, Lcom/google/android/apps/translate/R$id;->welcome_text:I

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 306
    .local v5, welcomeText:Landroid/widget/TextView;
    invoke-static {p0}, Lcom/google/android/apps/translate/HomeActivity;->getWelcomeText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    sget v6, Lcom/google/android/apps/translate/R$id;->tos_link:I

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 310
    .local v4, tosLink:Landroid/widget/TextView;
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 312
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 313
    .local v1, builder:Landroid/app/AlertDialog$Builder;
    sget v6, Lcom/google/android/apps/translate/R$string;->app_name:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 314
    sget v6, Lcom/google/android/apps/translate/R$drawable;->icon:I

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 315
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 319
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 321
    new-instance v6, Lcom/google/android/apps/translate/HomeActivity$3;

    invoke-direct {v6, p0}, Lcom/google/android/apps/translate/HomeActivity$3;-><init>(Lcom/google/android/apps/translate/HomeActivity;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 328
    sget v6, Lcom/google/android/apps/translate/R$string;->btn_accept:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/translate/HomeActivity$4;

    invoke-direct {v7, p0}, Lcom/google/android/apps/translate/HomeActivity$4;-><init>(Lcom/google/android/apps/translate/HomeActivity;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 347
    sget v6, Lcom/google/android/apps/translate/R$string;->btn_decline:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/translate/HomeActivity$5;

    invoke-direct {v7, p0}, Lcom/google/android/apps/translate/HomeActivity$5;-><init>(Lcom/google/android/apps/translate/HomeActivity;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 356
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 357
    .local v0, alert:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    monitor-exit p0

    return-void

    .line 300
    .end local v0           #alert:Landroid/app/AlertDialog;
    .end local v1           #builder:Landroid/app/AlertDialog$Builder;
    .end local v2           #dialogContentView:Landroid/view/View;
    .end local v3           #inflater:Landroid/view/LayoutInflater;
    .end local v4           #tosLink:Landroid/widget/TextView;
    .end local v5           #welcomeText:Landroid/widget/TextView;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method private startTranslateActivity()V
    .locals 2

    .prologue
    .line 134
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->setActionBarTitle(Landroid/app/Activity;Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/translate/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Util;->openTranslateActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/translate/HomeActivity;->finish()V

    .line 137
    return-void
.end method

.method private stopCsiTimers()V
    .locals 2

    .prologue
    .line 188
    const-string v0, "HomeActivity"

    const-string v1, "stopCsiTimers"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/translate/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mInitRun:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mShowEula:Z

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/translate/HomeActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/HomeActivity$2;-><init>(Lcom/google/android/apps/translate/HomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 201
    :cond_0
    return-void
.end method


# virtual methods
.method clearEulaAcceptBit()V
    .locals 0

    .prologue
    .line 364
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->clearEulaAccepted(Landroid/content/Context;)V

    .line 365
    return-void
.end method

.method public getTranslateFragmentWrapper()Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 237
    const-string v0, "HomeActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->useFragments()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;->onActivityResult(IILandroid/content/Intent;)V

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .parameter "newConfig"

    .prologue
    .line 284
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 288
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    .line 272
    const-string v0, "HomeActivity"

    const-string v1, "onContextItemSelected"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x1

    .line 94
    const-string v1, "HomeActivity"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iput-boolean v5, p0, Lcom/google/android/apps/translate/HomeActivity;->mInitRun:Z

    .line 96
    new-instance v1, Lcom/google/android/apps/translate/CsiTimer;

    const-string v2, "home"

    invoke-direct {v1, v2}, Lcom/google/android/apps/translate/CsiTimer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/HomeActivity;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/translate/HomeActivity;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "init"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/CsiTimer;->begin([Ljava/lang/String;)V

    .line 99
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-direct {p0}, Lcom/google/android/apps/translate/HomeActivity;->checkVersion()V

    .line 108
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/HomeActivity;->setVolumeControlStream(I)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/translate/HomeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/apps/translate/R$layout;->home_activity:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 111
    .local v0, rootView:Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/HomeActivity;->setContentView(Landroid/view/View;)V

    .line 113
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->useFragments()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    new-instance v1, Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;->getHelper()Lcom/google/android/apps/translate/translation/TranslateHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    .line 116
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->isEulaAccepted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    iput-boolean v5, p0, Lcom/google/android/apps/translate/HomeActivity;->mShowEula:Z

    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/translate/HomeActivity;->showEulaDialog()V

    .line 131
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/HomeActivity;->stopCsiTimers()V

    goto :goto_0

    .line 123
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->isEulaAccepted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 124
    iput-boolean v5, p0, Lcom/google/android/apps/translate/HomeActivity;->mShowEula:Z

    .line 125
    invoke-direct {p0}, Lcom/google/android/apps/translate/HomeActivity;->showEulaDialog()V

    goto :goto_0

    .line 127
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/translate/HomeActivity;->stopCsiTimers()V

    .line 128
    invoke-direct {p0}, Lcom/google/android/apps/translate/HomeActivity;->startTranslateActivity()V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    .line 278
    const-string v0, "HomeActivity"

    const-string v1, "onCreateContextMenu"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 280
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 251
    const-string v0, "HomeActivity"

    const-string v1, "onCreateOptionsMenu"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 231
    const-string v0, "HomeActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 233
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 141
    const-string v0, "HomeActivity"

    const-string v1, "onKeyDown"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->useFragments()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x1

    .line 147
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    .line 263
    const-string v0, "HomeActivity"

    const-string v1, "onOptionsItemSelected"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    const/4 v0, 0x1

    .line 267
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 218
    const-string v0, "HomeActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onPause()V

    .line 225
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mInitRun:Z

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/translate/HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/TranslateApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/TranslateApplication;->resetAppCsiTimer()V

    .line 227
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 257
    const-string v0, "HomeActivity"

    const-string v1, "onPrepareOptionsMenu"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 180
    const-string v0, "HomeActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/translate/HomeActivity;->mTranslateFragment:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;->onResume()V

    .line 184
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 185
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 152
    const-string v0, "HomeActivity"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 155
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/Translate;->setAcceptLanguage(Ljava/lang/String;)V

    .line 156
    return-void
.end method
