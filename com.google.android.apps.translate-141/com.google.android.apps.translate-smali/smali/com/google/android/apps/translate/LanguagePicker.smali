.class public Lcom/google/android/apps/translate/LanguagePicker;
.super Ljava/lang/Object;
.source "LanguagePicker.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;,
        Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;,
        Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;
    }
.end annotation


# static fields
.field private static final NO_FONT_SUPPORT_LANGUAGES:[Ljava/lang/String; = null

.field private static final SHOW_ALL_LANGUAGES_FOR_CONVERSATION:Z = false

.field private static final TAG:Ljava/lang/String; = "LanguagePicker"

.field private static final USE_CONV_LANG_PROFILE:Z

.field private static mUnsupportedLanguages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mAndroidSupportedSourceLanguageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation
.end field

.field private mAndroidSupportedTargetLanguageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation
.end field

.field private mConvSupportedLangList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation
.end field

.field private mIsConversationMode:Z

.field private mLanguages:Lcom/google/android/apps/translate/Languages;

.field private final mOnLanguagePairSelectedListener:Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;

.field private mSourceItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSourceLanguage:Lcom/google/android/apps/translate/Language;

.field private final mSourceSpinner:Landroid/widget/Spinner;

.field private mTargetItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetLanguage:Lcom/google/android/apps/translate/Language;

.field private final mTargetSpinner:Landroid/widget/Spinner;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 37
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    sput-object v4, Lcom/google/android/apps/translate/LanguagePicker;->mUnsupportedLanguages:Ljava/util/HashSet;

    .line 41
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "hi"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "fa"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "th"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "yi"

    aput-object v6, v4, v5

    sput-object v4, Lcom/google/android/apps/translate/LanguagePicker;->NO_FONT_SUPPORT_LANGUAGES:[Ljava/lang/String;

    .line 46
    sget-object v0, Lcom/google/android/apps/translate/LanguagePicker;->NO_FONT_SUPPORT_LANGUAGES:[Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 47
    .local v2, l:Ljava/lang/String;
    sget-object v4, Lcom/google/android/apps/translate/LanguagePicker;->mUnsupportedLanguages:Ljava/util/HashSet;

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 46
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    .end local v2           #l:Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/widget/Spinner;Landroid/widget/Spinner;Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;)V
    .locals 2
    .parameter "activity"
    .parameter "sourceSpinner"
    .parameter "targetSpinner"
    .parameter "onLanguagePairSelectedListener"

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mIsConversationMode:Z

    .line 91
    iput-object p1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    .line 94
    invoke-static {}, Lcom/google/android/apps/translate/LanguagesFactory;->get()Lcom/google/android/apps/translate/LanguagesFactory;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/translate/LanguagesFactory;->getLanguages(Landroid/content/Context;Ljava/util/Locale;)Lcom/google/android/apps/translate/Languages;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageList(Lcom/google/android/apps/translate/Languages;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Languages;->getDefaultFromLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Languages;->getDefaultToLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/translate/LanguagePicker;->updateCurrentLanguagePair(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 99
    iput-object p2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    .line 100
    iput-object p3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 104
    iput-object p4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mOnLanguagePairSelectedListener:Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/LanguagePicker;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private static filterConversationLanguages(Lcom/google/android/apps/translate/VoiceInputHelper;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .parameter "voiceInputHelper"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/translate/VoiceInputHelper;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    .local p1, langList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 316
    .local v0, filteredList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/Language;

    .line 317
    .local v2, lang:Lcom/google/android/apps/translate/Language;
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/VoiceInputHelper;->isConversationLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/translate/Util;->languageShortNameToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/translate/tts/NetworkTts;->isLanguageAvailable(Ljava/util/Locale;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 320
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    .end local v2           #lang:Lcom/google/android/apps/translate/Language;
    :cond_1
    return-object v0
.end method

.method private generateDropDownItems(ZLjava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .parameter "selectLanguage"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    .local p2, supportList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    .local p3, recentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 140
    .local v2, list:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;>;"
    if-eqz p1, :cond_0

    .line 141
    new-instance v3, Lcom/google/android/apps/translate/Language;

    const-string v4, "select"

    iget-object v5, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    sget v6, Lcom/google/android/apps/translate/R$string;->hint_language_selector:I

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/translate/Language;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .local v3, selectedLang:Lcom/google/android/apps/translate/Language;
    new-instance v4, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    sget-object v5, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;->SELECT:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;

    invoke-direct {v4, v3, v5}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    .end local v3           #selectedLang:Lcom/google/android/apps/translate/Language;
    :cond_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/Language;

    .line 147
    .local v1, language:Lcom/google/android/apps/translate/Language;
    new-instance v4, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    sget-object v5, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;->RECENTLY_USED:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;

    invoke-direct {v4, v1, v5}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    .end local v1           #language:Lcom/google/android/apps/translate/Language;
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/Language;

    .line 150
    .restart local v1       #language:Lcom/google/android/apps/translate/Language;
    new-instance v4, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    sget-object v5, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;->NORMAL:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;

    invoke-direct {v4, v1, v5}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    .end local v1           #language:Lcom/google/android/apps/translate/Language;
    :cond_2
    return-object v2
.end method

.method private getAndroidSupportedLanguages(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545
    .local p1, allLanguages:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 546
    .local v2, result:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/Language;

    .line 547
    .local v1, l:Lcom/google/android/apps/translate/Language;
    sget-object v3, Lcom/google/android/apps/translate/LanguagePicker;->mUnsupportedLanguages:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 548
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 551
    .end local v1           #l:Lcom/google/android/apps/translate/Language;
    :cond_1
    return-object v2
.end method

.method private getRecentLeftConversationLanguage(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361
    .local p1, rightRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v2, leftRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Profile;->getRecentFromLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/Language;

    .line 371
    .local v1, lang:Lcom/google/android/apps/translate/Language;
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mConvSupportedLangList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 372
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    const/4 v4, 0x0

    invoke-static {p1, v3, v1, v4}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;->getToLanguageGivenFromLanguage(Ljava/util/List;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 377
    .end local v1           #lang:Lcom/google/android/apps/translate/Language;
    :cond_1
    return-object v2
.end method

.method private getRecentRightConversationLanguage()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 390
    .local v2, rightRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Profile;->getRecentToLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/Language;

    .line 391
    .local v1, lang:Lcom/google/android/apps/translate/Language;
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mConvSupportedLangList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 392
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 396
    .end local v1           #lang:Lcom/google/android/apps/translate/Language;
    :cond_1
    return-object v2
.end method

.method private setLanguagePairFromSpinners()V
    .locals 4

    .prologue
    .line 331
    const-string v2, "LanguagePicker"

    const-string v3, "setLanguagePairFromSpinners"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;->getLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v0

    .line 333
    .local v0, source:Lcom/google/android/apps/translate/Language;
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;->getLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v1

    .line 334
    .local v1, target:Lcom/google/android/apps/translate/Language;
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/translate/LanguagePicker;->setLanguagePairToSpinners(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 335
    return-void
.end method

.method private updateCurrentLanguagePair(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 0
    .parameter "sourceLanguage"
    .parameter "targetLanguage"

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    .line 125
    iput-object p2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    .line 126
    return-void
.end method

.method private updateLanguageSpinner(Landroid/widget/Spinner;Ljava/util/List;Lcom/google/android/apps/translate/Language;)V
    .locals 2
    .parameter "spinner"
    .parameter
    .parameter "selectedLanguage"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/Spinner;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;",
            ">;",
            "Lcom/google/android/apps/translate/Language;",
            ")V"
        }
    .end annotation

    .prologue
    .line 422
    .local p2, items:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;>;"
    const/4 v0, 0x0

    .local v0, pos:I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 423
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;->getLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 425
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    .end local v0           #pos:I
    :cond_1
    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 426
    invoke-virtual {p1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;->notifyDataSetChanged()V

    .line 427
    return-void

    .line 422
    .restart local v0       #pos:I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private updateLanguageSpinners()V
    .locals 5

    .prologue
    .line 400
    iget-boolean v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mIsConversationMode:Z

    if-eqz v2, :cond_0

    .line 401
    invoke-direct {p0}, Lcom/google/android/apps/translate/LanguagePicker;->getRecentRightConversationLanguage()Ljava/util/List;

    move-result-object v1

    .line 402
    .local v1, rightRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-direct {p0, v1}, Lcom/google/android/apps/translate/LanguagePicker;->getRecentLeftConversationLanguage(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 403
    .local v0, leftRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/translate/LanguagePicker;->updateRecentDropDownItems(Ljava/util/List;Ljava/util/List;)V

    .line 404
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;->notifyDataSetChanged()V

    .line 405
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageSpinner(Landroid/widget/Spinner;Ljava/util/List;Lcom/google/android/apps/translate/Language;)V

    .line 406
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/translate/LanguagePicker;->updateRecentDropDownItems(Ljava/util/List;Ljava/util/List;)V

    .line 415
    .end local v0           #leftRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    .end local v1           #rightRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;->notifyDataSetChanged()V

    .line 416
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageSpinner(Landroid/widget/Spinner;Ljava/util/List;Lcom/google/android/apps/translate/Language;)V

    .line 417
    return-void

    .line 408
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Profile;->getRecentFromLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/translate/LanguagePicker;->updateRecentDropDownItems(Ljava/util/List;Ljava/util/List;)V

    .line 410
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;->notifyDataSetChanged()V

    .line 411
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageSpinner(Landroid/widget/Spinner;Ljava/util/List;Lcom/google/android/apps/translate/Language;)V

    .line 412
    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Profile;->getRecentToLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/translate/LanguagePicker;->updateRecentDropDownItems(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method private updateRecentDropDownItems(Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, items:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;>;"
    .local p2, recentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    const/4 v8, 0x0

    .line 160
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 161
    .local v5, temps:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;>;"
    const/4 v1, 0x0

    .line 162
    .local v1, autoSelected:Z
    const/4 v0, 0x0

    .line 163
    .local v0, autoItem:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;
    iget-object v6, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v6}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 170
    const/4 v1, 0x1

    .line 173
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;->getSpec()Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;->SELECT:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;

    if-ne v6, v7, :cond_1

    .line 174
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/translate/Language;

    .line 178
    .local v4, language:Lcom/google/android/apps/translate/Language;
    new-instance v6, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    sget-object v7, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;->RECENTLY_USED:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;

    invoke-direct {v6, v4, v7}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 181
    .end local v4           #language:Lcom/google/android/apps/translate/Language;
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;

    .line 182
    .local v3, item:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;
    invoke-virtual {v3}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;->getSpec()Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;->NORMAL:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem$LanguageSpec;

    if-ne v6, v7, :cond_3

    .line 183
    if-eqz v1, :cond_4

    invoke-virtual {v3}, Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;->getLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 184
    move-object v0, v3

    goto :goto_1

    .line 186
    :cond_4
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 190
    .end local v3           #item:Lcom/google/android/apps/translate/LanguagePicker$LanguageDropDownItem;
    :cond_5
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 191
    if-eqz v0, :cond_6

    .line 192
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_6
    invoke-interface {p1, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 195
    return-void
.end method


# virtual methods
.method public getFromLanguage()Lcom/google/android/apps/translate/Language;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method public getToLanguage()Lcom/google/android/apps/translate/Language;
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter "arg1"
    .parameter "arg2"
    .parameter "arg3"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x1

    .line 527
    const-string v0, "LanguagePicker"

    const-string v1, "onItemSelected"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    invoke-direct {p0}, Lcom/google/android/apps/translate/LanguagePicker;->setLanguagePairFromSpinners()V

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mOnLanguagePairSelectedListener:Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mOnLanguagePairSelectedListener:Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;

    iget-object v1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-interface {v0, v1, v2, v3, v3}, Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;->onLanguagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 534
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 539
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public setLanguagePairToSpinners(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 3
    .parameter "source"
    .parameter "target"

    .prologue
    .line 342
    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translate/Util;->isSelectLanguage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translate/Util;->isSelectLanguage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 344
    iget-boolean v1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mIsConversationMode:Z

    if-eqz v1, :cond_1

    .line 348
    iget-object v1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;->getFromLanguageGivenToLanguage(Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v0

    .line 350
    .local v0, convertedSource:Lcom/google/android/apps/translate/Language;
    iget-object v1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v0, p2}, Lcom/google/android/apps/translate/Profile;->setLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 356
    .end local v0           #convertedSource:Lcom/google/android/apps/translate/Language;
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/translate/LanguagePicker;->updateCurrentLanguagePair(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 357
    invoke-direct {p0}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageSpinners()V

    .line 358
    return-void

    .line 353
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/translate/Profile;->setLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    goto :goto_0
.end method

.method public setupLanguageSpinners()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 203
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Profile;->getLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)[Lcom/google/android/apps/translate/Language;

    move-result-object v0

    .line 204
    .local v0, languagePair:[Lcom/google/android/apps/translate/Language;
    array-length v3, v0

    if-le v3, v5, :cond_0

    aget-object v3, v0, v6

    if-eqz v3, :cond_0

    aget-object v3, v0, v5

    if-eqz v3, :cond_0

    .line 205
    aget-object v3, v0, v6

    aget-object v4, v0, v5

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/translate/LanguagePicker;->updateCurrentLanguagePair(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 209
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mAndroidSupportedSourceLanguageList:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-static {v4, v5}, Lcom/google/android/apps/translate/Profile;->getRecentFromLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v6, v3, v4}, Lcom/google/android/apps/translate/LanguagePicker;->generateDropDownItems(ZLjava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    .line 213
    new-instance v1, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;

    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    sget v4, Lcom/google/android/apps/translate/R$layout;->language_spinner:I

    iget-object v5, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    invoke-direct {v1, p0, v3, v4, v5}, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;-><init>(Lcom/google/android/apps/translate/LanguagePicker;Landroid/content/Context;ILjava/util/List;)V

    .line 215
    .local v1, sourceAdapter:Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 216
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageSpinner(Landroid/widget/Spinner;Ljava/util/List;Lcom/google/android/apps/translate/Language;)V

    .line 219
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mAndroidSupportedTargetLanguageList:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-static {v4, v5}, Lcom/google/android/apps/translate/Profile;->getRecentToLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v6, v3, v4}, Lcom/google/android/apps/translate/LanguagePicker;->generateDropDownItems(ZLjava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    .line 223
    new-instance v2, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;

    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    sget v4, Lcom/google/android/apps/translate/R$layout;->language_spinner:I

    iget-object v5, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;-><init>(Lcom/google/android/apps/translate/LanguagePicker;Landroid/content/Context;ILjava/util/List;)V

    .line 225
    .local v2, targetAdapter:Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 226
    iget-object v3, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageSpinner(Landroid/widget/Spinner;Ljava/util/List;Lcom/google/android/apps/translate/Language;)V

    .line 227
    return-void
.end method

.method public setupLanguageSpinnersForConversationMode(Lcom/google/android/apps/translate/VoiceInputHelper;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 11
    .parameter "voiceInputHelper"
    .parameter "left"
    .parameter "right"

    .prologue
    .line 236
    const-string v8, "LanguagePicker"

    const-string v9, "setupLanguageSpinnersForConversationMode"

    invoke-static {v8, v9}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mIsConversationMode:Z

    .line 239
    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mAndroidSupportedTargetLanguageList:Ljava/util/List;

    invoke-static {p1, v8}, Lcom/google/android/apps/translate/LanguagePicker;->filterConversationLanguages(Lcom/google/android/apps/translate/VoiceInputHelper;Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mConvSupportedLangList:Ljava/util/List;

    .line 243
    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    iget-object v9, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-static {v8, v9}, Lcom/google/android/apps/translate/Profile;->getLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)[Lcom/google/android/apps/translate/Language;

    move-result-object v0

    .line 247
    .local v0, languagePair:[Lcom/google/android/apps/translate/Language;
    const/4 v2, 0x1

    .line 248
    .local v2, leftSelect:Z
    const/4 v5, 0x1

    .line 249
    .local v5, rightSelect:Z
    if-eqz p2, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mConvSupportedLangList:Ljava/util/List;

    invoke-interface {v8, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 250
    const/4 v2, 0x0

    .line 267
    :cond_0
    :goto_0
    if-eqz p3, :cond_3

    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mConvSupportedLangList:Ljava/util/List;

    invoke-interface {v8, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 268
    const/4 v5, 0x0

    .line 285
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/translate/LanguagePicker;->getRecentRightConversationLanguage()Ljava/util/List;

    move-result-object v4

    .line 286
    .local v4, rightRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-direct {p0, v4}, Lcom/google/android/apps/translate/LanguagePicker;->getRecentLeftConversationLanguage(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 287
    .local v1, leftRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/translate/LanguagePicker;->updateCurrentLanguagePair(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 288
    invoke-static {p1, v1}, Lcom/google/android/apps/translate/LanguagePicker;->filterConversationLanguages(Lcom/google/android/apps/translate/VoiceInputHelper;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 292
    .local v3, recentLangList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mConvSupportedLangList:Ljava/util/List;

    invoke-direct {p0, v2, v8, v3}, Lcom/google/android/apps/translate/LanguagePicker;->generateDropDownItems(ZLjava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    .line 293
    new-instance v6, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;

    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    sget v9, Lcom/google/android/apps/translate/R$layout;->language_spinner:I

    iget-object v10, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    invoke-direct {v6, p0, v8, v9, v10}, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;-><init>(Lcom/google/android/apps/translate/LanguagePicker;Landroid/content/Context;ILjava/util/List;)V

    .line 295
    .local v6, sourceAdapter:Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;
    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 296
    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceItems:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/apps/translate/LanguagePicker;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageSpinner(Landroid/widget/Spinner;Ljava/util/List;Lcom/google/android/apps/translate/Language;)V

    .line 298
    invoke-static {p1, v4}, Lcom/google/android/apps/translate/LanguagePicker;->filterConversationLanguages(Lcom/google/android/apps/translate/VoiceInputHelper;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 301
    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mConvSupportedLangList:Ljava/util/List;

    invoke-direct {p0, v5, v8, v3}, Lcom/google/android/apps/translate/LanguagePicker;->generateDropDownItems(ZLjava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    .line 302
    new-instance v7, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;

    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    sget v9, Lcom/google/android/apps/translate/R$layout;->language_spinner:I

    iget-object v10, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    invoke-direct {v7, p0, v8, v9, v10}, Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;-><init>(Lcom/google/android/apps/translate/LanguagePicker;Landroid/content/Context;ILjava/util/List;)V

    .line 304
    .local v7, targetAdapter:Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;
    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 305
    iget-object v8, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetItems:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/apps/translate/LanguagePicker;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageSpinner(Landroid/widget/Spinner;Ljava/util/List;Lcom/google/android/apps/translate/Language;)V

    .line 306
    return-void

    .line 261
    .end local v1           #leftRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    .end local v3           #recentLangList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    .end local v4           #rightRecentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    .end local v6           #sourceAdapter:Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;
    .end local v7           #targetAdapter:Lcom/google/android/apps/translate/LanguagePicker$LanguageAdapter;
    :cond_2
    if-eqz v2, :cond_0

    .line 262
    new-instance p2, Lcom/google/android/apps/translate/Language;

    .end local p2
    const-string v8, "select"

    iget-object v9, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    sget v10, Lcom/google/android/apps/translate/R$string;->hint_language_selector:I

    invoke-virtual {v9, v10}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p2, v8, v9}, Lcom/google/android/apps/translate/Language;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local p2
    goto :goto_0

    .line 279
    :cond_3
    if-eqz v5, :cond_1

    .line 280
    new-instance p3, Lcom/google/android/apps/translate/Language;

    .end local p3
    const-string v8, "select"

    iget-object v9, p0, Lcom/google/android/apps/translate/LanguagePicker;->mActivity:Landroid/app/Activity;

    sget v10, Lcom/google/android/apps/translate/R$string;->hint_language_selector:I

    invoke-virtual {v9, v10}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p3, v8, v9}, Lcom/google/android/apps/translate/Language;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local p3
    goto :goto_1
.end method

.method public updateLanguageList(Lcom/google/android/apps/translate/Languages;)V
    .locals 2
    .parameter "languages"

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Languages;->getSupportedFromLanguages()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mAndroidSupportedSourceLanguageList:Ljava/util/List;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/translate/LanguagePicker;->mLanguages:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Languages;->getSupportedToLanguages()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/LanguagePicker;->mAndroidSupportedTargetLanguageList:Ljava/util/List;

    .line 118
    return-void
.end method
