.class public Lcom/google/android/apps/translate/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# static fields
.field public static final DEBUG:I = 0x8

.field private static final DEBUG_ALL:Z = false

.field public static final ERROR:I = 0x1

.field public static final INFO:I = 0x4

.field public static final NONE:I = 0x0

.field public static final T:Z = false

.field public static final V:Z = false

.field public static final VERBOSE:I = 0x10

.field public static final WARNING:I = 0x2

.field private static sLastTimes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static sLevel:I

.field private static sTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    sput v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    .line 27
    const-string v0, "Logger"

    sput-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"

    .prologue
    .line 97
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 127
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 130
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 155
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"

    .prologue
    .line 75
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 175
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 176
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 195
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 196
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"

    .prologue
    .line 115
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 145
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 148
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 167
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"

    .prologue
    .line 87
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 187
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 188
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 207
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 208
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"

    .prologue
    .line 103
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 133
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 136
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 159
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"

    .prologue
    .line 79
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 179
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 180
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 199
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 200
    :cond_0
    return-void
.end method

.method public static isDebug()Z
    .locals 1

    .prologue
    .line 211
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isError()Z
    .locals 1

    .prologue
    .line 223
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInfo()Z
    .locals 1

    .prologue
    .line 215
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWarning()Z
    .locals 1

    .prologue
    .line 219
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setLogLevel(I)V
    .locals 1
    .parameter "level"

    .prologue
    .line 44
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    .line 49
    sparse-switch p0, :sswitch_data_0

    .line 61
    :goto_0
    return-void

    .line 51
    :sswitch_0
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    or-int/lit8 v0, v0, 0x10

    sput v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    .line 53
    :sswitch_1
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    or-int/lit8 v0, v0, 0x8

    sput v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    .line 55
    :sswitch_2
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    or-int/lit8 v0, v0, 0x4

    sput v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    .line 57
    :sswitch_3
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    or-int/lit8 v0, v0, 0x2

    sput v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    .line 59
    :sswitch_4
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    or-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    goto :goto_0

    .line 49
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_3
        0x4 -> :sswitch_2
        0x8 -> :sswitch_1
        0x10 -> :sswitch_0
    .end sparse-switch
.end method

.method public static setTag(Ljava/lang/String;)V
    .locals 0
    .parameter "tag"

    .prologue
    .line 67
    if-eqz p0, :cond_0

    sput-object p0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    .line 68
    :cond_0
    return-void
.end method

.method public static declared-synchronized t(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 243
    const-class v0, Lcom/google/android/apps/translate/Logger;

    monitor-enter v0

    monitor-exit v0

    return-void
.end method

.method public static v(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"

    .prologue
    .line 91
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 121
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 124
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 151
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"

    .prologue
    .line 71
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 171
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 172
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 191
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 192
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"

    .prologue
    .line 109
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "obj"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 139
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 142
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 163
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"

    .prologue
    .line 83
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "tag"
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 183
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 184
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "msg"
    .parameter "cause"

    .prologue
    .line 203
    sget v0, Lcom/google/android/apps/translate/Logger;->sLevel:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/Logger;->sTag:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 204
    :cond_0
    return-void
.end method
