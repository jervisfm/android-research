.class public Lcom/google/android/apps/translate/Profile;
.super Ljava/lang/Object;
.source "Profile.java"


# static fields
.field public static final DEFAULT_CAMERA_FRONTEND_URL:Ljava/lang/String; = "http://www.google.com"

.field private static final DEFAULT_EN_FROM:Ljava/lang/String; = "en"

.field private static final DEFAULT_EN_TO:Ljava/lang/String; = "es"

.field private static final DEFAULT_OTHER_FROM:Ljava/lang/String; = "en"

.field private static final EMPTY_ENTRY:Ljava/lang/String; = ""

.field public static final KEY_CONVERSATION_LANGUAGE_LEFT:Ljava/lang/String; = "key_convlang_left"

.field public static final KEY_CONVERSATION_LANGUAGE_RIGHT:Ljava/lang/String; = "key_convlang_right"

.field public static final KEY_CURRENT_VERSION_CODE:Ljava/lang/String; = "key_version_code"

.field public static final KEY_DEBUG_CAMERA_FRONTEND_URL:Ljava/lang/String; = "key_debug_camera_input_frontend_url"

.field public static final KEY_ENABLE_CAMERA_LOGGING:Ljava/lang/String; = "key_enable_camera_logging"

.field public static final KEY_ENABLE_CONVERSATION_CONFIRM:Ljava/lang/String; = "key_enable_conversation_confirm"

.field public static final KEY_ENABLE_DUAL_MODE:Ljava/lang/String; = "key_enable_dual_mode"

.field public static final KEY_ENABLE_INSTANT_TRANSLATION:Ljava/lang/String; = "key_enable_instant_translation"

.field public static final KEY_ENABLE_OFFLINE_TRANSLATE:Ljava/lang/String; = "key_enable_offline_translate_mode"

.field public static final KEY_ENABLE_STREAMING_CONVERSATION:Ljava/lang/String; = "key_enable_streaming_conversation"

.field public static final KEY_EULA_ACCEPTED:Ljava/lang/String; = "key_eula_accepted"

.field public static final KEY_FAVORITE_ORDER:Ljava/lang/String; = "key_favorite_order"

.field public static final KEY_HANDWRITING_LANGUAGE_DEFVALUE_PREFIX:Ljava/lang/String; = "default_handwriting_language_"

.field public static final KEY_HISTORY_ORDER:Ljava/lang/String; = "key_history_order"

.field public static final KEY_HOME_FIRST_RUN:Ljava/lang/String; = "key_home_first_run"

.field public static final KEY_INPUT_TEXT:Ljava/lang/String; = "key_input_text"

.field public static final KEY_LANGUAGE_FROM:Ljava/lang/String; = "key_language_from"

.field public static final KEY_LANGUAGE_LIST_WITH_LOCALE:Ljava/lang/String; = "key_language_list_with_locale"

.field public static final KEY_LANGUAGE_TO:Ljava/lang/String; = "key_language_to"

.field public static final KEY_LAST_TRANSLATION_FROM_LANG:Ljava/lang/String; = "key_last_translation_from_lang"

.field public static final KEY_LAST_TRANSLATION_INPUT:Ljava/lang/String; = "key_last_translation_input"

.field public static final KEY_LAST_TRANSLATION_IS_AUTO:Ljava/lang/String; = "key_last_translation_is_auto"

.field public static final KEY_LAST_TRANSLATION_OUTPUT:Ljava/lang/String; = "key_last_translation_output"

.field public static final KEY_LAST_TRANSLATION_TO_LANG:Ljava/lang/String; = "key_last_translation_to_lang"

.field public static final KEY_PREFER_NETWORK_TTS:Ljava/lang/String; = "key_prefer_network_tts"

.field public static final KEY_RECENT_CONVERSATION_LANGUAGE_LEFT:Ljava/lang/String; = "key_recent_convlang_left"

.field public static final KEY_RECENT_CONVERSATION_LANGUAGE_RIGHT:Ljava/lang/String; = "key_recent_convlang_right"

.field public static final KEY_RECENT_LANGUAGE_FROM:Ljava/lang/String; = "key_recent_language_from"

.field public static final KEY_RECENT_LANGUAGE_TO:Ljava/lang/String; = "key_recent_language_to"

.field public static final KEY_SHOW_CAMERA_LOGGING_DIALOG:Ljava/lang/String; = "key_show_camera_logging_dialog"

.field public static final KEY_SUPPORTED_VOICE_INPUT_LANGS:Ljava/lang/String; = "key_supported_voice_input_langs"

.field public static final KEY_TRANSLATE_SETTING:Ljava/lang/String; = "key_title_settings"

.field private static final LANGUAGE_SEPARATOR:Ljava/lang/String; = "\t"

.field private static final LOGMSG_CANNOT_GET_DEFAULT_PREFVALUE:Ljava/lang/String; = "Failed to get the default preference value for key="

.field private static final MAX_RECENT_PAIRS:I = 0x3

.field private static final PAIR_SEPARATOR:Ljava/lang/String; = "\t"

.field private static final TAG:Ljava/lang/String; = "Profile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addRecentLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V
    .locals 10
    .parameter "context"
    .parameter "lang"
    .parameter "key"

    .prologue
    .line 159
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, ""

    invoke-interface {v8, p2, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "\t"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 166
    .local v6, oldLangs:[Ljava/lang/String;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 168
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    .local v7, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const/4 v4, 0x1

    .line 171
    .local v4, numRecent:I
    move-object v0, v6

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v5, v0, v2

    .line 172
    .local v5, oldLang:Ljava/lang/String;
    const/4 v8, 0x3

    if-lt v4, v8, :cond_3

    .line 180
    .end local v5           #oldLang:Ljava/lang/String;
    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, p2, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 181
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 175
    .restart local v5       #oldLang:Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 176
    const-string v8, "\t"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    add-int/lit8 v4, v4, 0x1

    .line 171
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static clearEulaAccepted(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 325
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_eula_accepted"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 329
    return-void
.end method

.method public static getCameraInputFrontendUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .parameter "context"

    .prologue
    .line 605
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 607
    .local v1, url:Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 608
    const-string v0, "http://www.google.com"

    .line 613
    .local v0, pref:Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCameraInputFrontendUrl pref_url="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 614
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "http://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 615
    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 617
    :cond_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 610
    .end local v0           #pref:Ljava/lang/String;
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "key_debug_camera_input_frontend_url"

    const-string v4, "http://www.google.com"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0       #pref:Ljava/lang/String;
    goto :goto_0
.end method

.method public static getCameraInputFrontendUrls(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 591
    .local v3, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->getFrontendUrls(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v5

    .line 592
    .local v5, urls:[Ljava/lang/String;
    const-string v6, "http://www.google.com"

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 593
    if-eqz v5, :cond_0

    .line 594
    move-object v0, v5

    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 595
    .local v4, url:Ljava/lang/String;
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 594
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 598
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v4           #url:Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method public static getCameraLogging(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 430
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_enable_camera_logging"

    sget v2, Lcom/google/android/apps/translate/R$string;->default_enable_camera_logging:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getConversationInputConfirm(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 405
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_enable_conversation_confirm"

    sget v2, Lcom/google/android/apps/translate/R$string;->default_enable_conversation_confirm:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getConversationLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)[Lcom/google/android/apps/translate/Language;
    .locals 6
    .parameter "context"
    .parameter "languageList"

    .prologue
    .line 143
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 144
    .local v1, pref:Landroid/content/SharedPreferences;
    const-string v4, "key_convlang_left"

    invoke-static {p0, p1}, Lcom/google/android/apps/translate/Profile;->getDefaultFromLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, left:Ljava/lang/String;
    const-string v4, "key_convlang_right"

    invoke-static {p0, p1}, Lcom/google/android/apps/translate/Profile;->getDefaultToLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, right:Ljava/lang/String;
    const/4 v4, 0x2

    new-array v2, v4, [Lcom/google/android/apps/translate/Language;

    .line 150
    .local v2, results:[Lcom/google/android/apps/translate/Language;
    const/4 v4, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v5

    aput-object v5, v2, v4

    .line 151
    const/4 v4, 0x1

    invoke-virtual {p1, v3}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v5

    aput-object v5, v2, v4

    .line 153
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "profile conversation from: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", to: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 155
    return-object v2
.end method

.method public static getCurrentVersionCode(Landroid/content/Context;)I
    .locals 3
    .parameter "context"

    .prologue
    .line 371
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_version_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static getDefaultFromLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/lang/String;
    .locals 1
    .parameter "context"
    .parameter "languageList"

    .prologue
    .line 258
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/Profile;->isEnglishLocale(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "en"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "en"

    goto :goto_0
.end method

.method private static getDefaultToLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/lang/String;
    .locals 3
    .parameter "context"
    .parameter "languageList"

    .prologue
    .line 264
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 268
    .local v0, locale:Ljava/util/Locale;
    invoke-static {v0}, Lcom/google/android/apps/translate/Profile;->isEnglishLocale(Ljava/util/Locale;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 269
    const-string v1, "es"

    .line 272
    :cond_0
    :goto_0
    return-object v1

    .line 271
    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->getLanguageShortNameByLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 272
    .local v1, shortName:Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v1, "es"

    goto :goto_0
.end method

.method public static getDualMode(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 396
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_enable_dual_mode"

    sget v2, Lcom/google/android/apps/translate/R$string;->default_enable_dual_mode:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getFavoriteOrder(Landroid/content/Context;)I
    .locals 3
    .parameter "context"

    .prologue
    .line 508
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_favorite_order"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getFromShortLangNameFromHandwritingLangShortName(Lcom/google/android/apps/translate/Languages;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "languageList"
    .parameter "handwritingShortLangName"

    .prologue
    .line 662
    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 663
    invoke-static {p0}, Lcom/google/android/apps/translate/Languages;->getDefaultChineseFromLanguage(Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/Language;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object p1

    .line 665
    .end local p1
    :cond_0
    return-object p1
.end method

.method private static getFrontendUrls(Landroid/content/Context;)[Ljava/lang/String;
    .locals 2
    .parameter "context"

    .prologue
    .line 423
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$array;->frontend_domains:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHandwritingLangShortNameFromFromShortLangName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "fromShortLangName"

    .prologue
    .line 673
    const-string v0, "zh-CN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "zh-TW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 675
    :cond_0
    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p0

    .line 677
    .end local p0
    :cond_1
    return-object p0
.end method

.method private static getHandwritingLanguageDefaultValueKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "shortLangName"

    .prologue
    .line 681
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "default_handwriting_language_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHistoryOrder(Landroid/content/Context;)I
    .locals 3
    .parameter "context"

    .prologue
    .line 488
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_history_order"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getInputText(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "context"

    .prologue
    .line 536
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_input_text"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstantTranslation(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 414
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_enable_instant_translation"

    sget v2, Lcom/google/android/apps/translate/R$string;->default_enable_instant_translation:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getLanguageList(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .parameter "context"

    .prologue
    .line 298
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Profile;->getLanguageList(Landroid/content/Context;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLanguageList(Landroid/content/Context;Ljava/util/Locale;)Ljava/lang/String;
    .locals 3
    .parameter "context"
    .parameter "locale"

    .prologue
    .line 305
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key_language_list_with_locale_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->getLanguageShortNameByLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 307
    .local v0, key:Ljava/lang/String;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)[Lcom/google/android/apps/translate/Language;
    .locals 6
    .parameter "context"
    .parameter "languageList"

    .prologue
    .line 124
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 125
    .local v1, pref:Landroid/content/SharedPreferences;
    const-string v4, "key_language_from"

    invoke-static {p0, p1}, Lcom/google/android/apps/translate/Profile;->getDefaultFromLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, from:Ljava/lang/String;
    const-string v4, "key_language_to"

    invoke-static {p0, p1}, Lcom/google/android/apps/translate/Profile;->getDefaultToLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 130
    .local v3, to:Ljava/lang/String;
    const/4 v4, 0x2

    new-array v2, v4, [Lcom/google/android/apps/translate/Language;

    .line 131
    .local v2, results:[Lcom/google/android/apps/translate/Language;
    const/4 v4, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v5

    aput-object v5, v2, v4

    .line 132
    const/4 v4, 0x1

    invoke-virtual {p1, v3}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v5

    aput-object v5, v2, v4

    .line 134
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "profile from: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", to: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 136
    return-object v2
.end method

.method public static getLastTranslation(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 9
    .parameter "context"
    .parameter "languages"

    .prologue
    const/4 v8, 0x0

    .line 688
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move-object v7, v8

    .line 707
    :cond_1
    :goto_0
    return-object v7

    .line 691
    :cond_2
    const-string v0, "Profile"

    const-string v1, "getLastTranslation"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 693
    .local v6, p:Landroid/content/SharedPreferences;
    const-string v0, "key_last_translation_from_lang"

    const-string v1, ""

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "key_last_translation_to_lang"

    const-string v2, ""

    invoke-interface {v6, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "key_last_translation_input"

    const-string v3, ""

    invoke-interface {v6, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "key_last_translation_output"

    const-string v4, ""

    invoke-interface {v6, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "key_last_translation_is_auto"

    const/4 v5, 0x0

    invoke-interface {v6, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->build(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v7

    .line 700
    .local v7, translation:Lcom/google/android/apps/translate/translation/TranslateEntry;
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasInputText()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v7}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasSourceAndTargetLanguages()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    move-object v7, v8

    .line 707
    goto :goto_0
.end method

.method public static getOfflineTranslate(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 387
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_enable_offline_translate_mode"

    sget v2, Lcom/google/android/apps/translate/R$string;->default_enable_offline_translate_mode:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getPreferNetworkTts(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 477
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_prefer_network_tts"

    sget v2, Lcom/google/android/apps/translate/R$string;->default_prefer_network_tts:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static getRecentConversationLanguages(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;
    .locals 9
    .parameter "context"
    .parameter "recentKey"
    .parameter "languages"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/translate/Languages;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, ""

    invoke-interface {v7, p1, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 241
    .local v6, result:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 242
    .local v4, list:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    const-string v7, "\t"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v5, v0, v1

    .line 243
    .local v5, name:Ljava/lang/String;
    const/4 v2, 0x0

    .line 244
    .local v2, lang:Lcom/google/android/apps/translate/Language;
    const-string v7, "key_recent_convlang_left"

    if-ne p1, v7, :cond_2

    .line 245
    invoke-virtual {p2, v5}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    .line 249
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 250
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 246
    :cond_2
    const-string v7, "key_recent_convlang_right"

    if-ne p1, v7, :cond_0

    .line 247
    invoke-virtual {p2, v5}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    goto :goto_1

    .line 253
    .end local v2           #lang:Lcom/google/android/apps/translate/Language;
    .end local v5           #name:Ljava/lang/String;
    :cond_3
    return-object v4
.end method

.method public static getRecentConversationLeftLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;
    .locals 1
    .parameter "context"
    .parameter "languages"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/translate/Languages;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    const-string v0, "key_recent_convlang_left"

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/translate/Profile;->getRecentConversationLanguages(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getRecentConversationRightLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;
    .locals 1
    .parameter "context"
    .parameter "languages"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/translate/Languages;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    const-string v0, "key_recent_convlang_right"

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/translate/Profile;->getRecentConversationLanguages(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getRecentFromLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;
    .locals 1
    .parameter "context"
    .parameter "languages"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/translate/Languages;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    const-string v0, "key_recent_language_from"

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/translate/Profile;->getRecentLanguages(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static getRecentLanguages(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;
    .locals 9
    .parameter "context"
    .parameter "recentKey"
    .parameter "languages"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/translate/Languages;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, ""

    invoke-interface {v7, p1, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 222
    .local v6, result:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 223
    .local v4, list:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    const-string v7, "\t"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v5, v0, v1

    .line 224
    .local v5, name:Ljava/lang/String;
    const/4 v2, 0x0

    .line 225
    .local v2, lang:Lcom/google/android/apps/translate/Language;
    const-string v7, "key_recent_language_from"

    if-ne p1, v7, :cond_2

    .line 226
    invoke-virtual {p2, v5}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    .line 230
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 231
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 227
    :cond_2
    const-string v7, "key_recent_language_to"

    if-ne p1, v7, :cond_0

    .line 228
    invoke-virtual {p2, v5}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    goto :goto_1

    .line 234
    .end local v2           #lang:Lcom/google/android/apps/translate/Language;
    .end local v5           #name:Ljava/lang/String;
    :cond_3
    return-object v4
.end method

.method public static getRecentToLanguages(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;
    .locals 1
    .parameter "context"
    .parameter "languages"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/translate/Languages;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    const-string v0, "key_recent_language_to"

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/translate/Profile;->getRecentLanguages(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getShowCameraLoggingDialog(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 449
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_show_camera_logging_dialog"

    sget v2, Lcom/google/android/apps/translate/R$string;->default_show_camera_logging_dialog:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getStreamingConversation(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 468
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_enable_streaming_conversation"

    sget v2, Lcom/google/android/apps/translate/R$string;->default_enable_streaming_conversation:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getSupportedVoiceLanguages(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 4
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 577
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "key_supported_voice_input_langs"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 579
    .local v0, languagesString:Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 580
    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translate/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 582
    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0
.end method

.method public static getVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "context"
    .parameter "lang"

    .prologue
    .line 554
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static isEnglishLocale(Ljava/util/Locale;)Z
    .locals 2
    .parameter "locale"

    .prologue
    .line 279
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isEulaAccepted(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 335
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_eula_accepted"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isHandwritingSupported(Landroid/content/Context;Lcom/google/android/apps/translate/Language;)Z
    .locals 7
    .parameter "context"
    .parameter "lang"

    .prologue
    const/4 v4, 0x0

    .line 635
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->isHandwritingSupported()Z

    move-result v5

    if-nez v5, :cond_1

    .line 654
    :cond_0
    :goto_0
    return v4

    .line 639
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/translate/Profile;->getHandwritingLangShortNameFromFromShortLangName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 643
    .local v3, handwritingShortLangName:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/translate/Profile;->getHandwritingLanguageDefaultValueKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 645
    .local v0, defValueKey:Ljava/lang/String;
    :try_start_0
    const-class v5, Lcom/google/android/apps/translate/R$string;

    invoke-virtual {v5, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 646
    .local v2, field:Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    goto :goto_0

    .line 647
    .end local v2           #field:Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 648
    .local v1, e:Ljava/lang/SecurityException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to get the default preference value for key="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 651
    .end local v1           #e:Ljava/lang/SecurityException;
    :catch_1
    move-exception v1

    .line 652
    .local v1, e:Ljava/lang/IllegalArgumentException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to get the default preference value for key="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 649
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v5

    goto :goto_0
.end method

.method public static isHomeFirstRun(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 343
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_home_first_run"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static resetFirstRunFlags(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    .line 379
    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Profile;->setHomeFirstRun(Landroid/content/Context;Z)Z

    .line 380
    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Profile;->setEulaAccepted(Landroid/content/Context;Z)V

    .line 381
    return-void
.end method

.method public static setCameraInputFrontendUrl(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .parameter "context"
    .parameter "url"

    .prologue
    .line 625
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_debug_camera_input_frontend_url"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 629
    return-void
.end method

.method public static setCameraLogging(Landroid/content/Context;Z)V
    .locals 2
    .parameter "context"
    .parameter "enableLogging"

    .prologue
    .line 439
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_enable_camera_logging"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 443
    return-void
.end method

.method public static setConversationLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 3
    .parameter "context"
    .parameter "left"
    .parameter "right"

    .prologue
    .line 104
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 118
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 109
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_1

    .line 110
    const-string v1, "key_convlang_left"

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 111
    const-string v1, "key_recent_convlang_left"

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/translate/Profile;->addRecentLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    .line 113
    :cond_1
    if-eqz p2, :cond_2

    .line 114
    const-string v1, "key_convlang_right"

    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 115
    const-string v1, "key_recent_convlang_right"

    invoke-static {p0, p2, v1}, Lcom/google/android/apps/translate/Profile;->addRecentLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    .line 117
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static setCurrentVersionCode(Landroid/content/Context;I)V
    .locals 2
    .parameter "context"
    .parameter "versionCode"

    .prologue
    .line 361
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_version_code"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 365
    return-void
.end method

.method public static setEulaAccepted(Landroid/content/Context;Z)V
    .locals 2
    .parameter "context"
    .parameter "accepted"

    .prologue
    .line 315
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_eula_accepted"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 319
    return-void
.end method

.method public static setFavoriteOrder(Landroid/content/Context;I)V
    .locals 2
    .parameter "context"
    .parameter "order"

    .prologue
    .line 516
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_favorite_order"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 520
    return-void
.end method

.method public static setHistoryOrder(Landroid/content/Context;I)V
    .locals 2
    .parameter "context"
    .parameter "order"

    .prologue
    .line 496
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_history_order"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 500
    return-void
.end method

.method public static setHomeFirstRun(Landroid/content/Context;Z)Z
    .locals 2
    .parameter "context"
    .parameter "firstRun"

    .prologue
    .line 351
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_home_first_run"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public static setInputText(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .parameter "context"
    .parameter "text"

    .prologue
    .line 526
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_input_text"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 530
    return-void
.end method

.method public static setLanguageList(Landroid/content/Context;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 3
    .parameter "context"
    .parameter "languageList"
    .parameter "locale"

    .prologue
    .line 286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key_language_list_with_locale_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/apps/translate/Util;->getLanguageShortNameByLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 288
    .local v0, key:Ljava/lang/String;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 292
    return-void
.end method

.method public static setLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 3
    .parameter "context"
    .parameter "from"
    .parameter "to"

    .prologue
    .line 87
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 92
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v1, "key_language_from"

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 93
    const-string v1, "key_language_to"

    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 94
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 96
    const-string v1, "key_recent_language_from"

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/translate/Profile;->addRecentLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    .line 97
    const-string v1, "key_recent_language_to"

    invoke-static {p0, p2, v1}, Lcom/google/android/apps/translate/Profile;->addRecentLanguage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setLastTranslation(Landroid/content/Context;Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 7
    .parameter "context"
    .parameter "translation"

    .prologue
    .line 714
    const-string v5, "Profile"

    const-string v6, "setLastTranslation"

    invoke-static {v5, v6}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    if-nez p0, :cond_0

    .line 741
    :goto_0
    return-void

    .line 718
    :cond_0
    const-string v1, ""

    .line 719
    .local v1, inputText:Ljava/lang/String;
    const-string v3, ""

    .line 720
    .local v3, outputText:Ljava/lang/String;
    const-string v0, ""

    .line 721
    .local v0, fromLang:Ljava/lang/String;
    const-string v4, ""

    .line 722
    .local v4, toLang:Ljava/lang/String;
    const/4 v2, 0x0

    .line 723
    .local v2, isAuto:Z
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasInputText()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasSourceAndTargetLanguages()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 726
    iget-object v1, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    .line 727
    iget-object v3, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    .line 728
    iget-object v5, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v0

    .line 729
    iget-object v5, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v4

    .line 730
    iget-boolean v2, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->isAutoLanguage:Z

    .line 733
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "key_last_translation_input"

    invoke-interface {v5, v6, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "key_last_translation_output"

    invoke-interface {v5, v6, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "key_last_translation_from_lang"

    invoke-interface {v5, v6, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "key_last_translation_to_lang"

    invoke-interface {v5, v6, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "key_last_translation_is_auto"

    invoke-interface {v5, v6, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static setShowCameraLoggingDialog(Landroid/content/Context;Z)V
    .locals 2
    .parameter "context"
    .parameter "showDialog"

    .prologue
    .line 458
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "key_show_camera_logging_dialog"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 462
    return-void
.end method

.method public static setSupportedVoiceLanguages(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 562
    .local p1, languages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 563
    .local v2, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 564
    .local v1, language:Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 567
    .end local v1           #language:Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "key_supported_voice_input_langs"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 571
    return-void
.end method

.method public static setVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "context"
    .parameter "lang"
    .parameter "voiceLang"

    .prologue
    .line 544
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 548
    return-void
.end method
