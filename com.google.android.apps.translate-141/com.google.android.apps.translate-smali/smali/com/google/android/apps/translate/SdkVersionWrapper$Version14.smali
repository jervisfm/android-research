.class Lcom/google/android/apps/translate/SdkVersionWrapper$Version14;
.super Lcom/google/android/apps/translate/SdkVersionWrapper$Version11;
.source "SdkVersionWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/SdkVersionWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Version14"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/SdkVersionWrapper$Version11;-><init>(Lcom/google/android/apps/translate/SdkVersionWrapper$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/translate/SdkVersionWrapper$1;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/android/apps/translate/SdkVersionWrapper$Version14;-><init>()V

    return-void
.end method


# virtual methods
.method public sendFeedback(Landroid/app/Activity;Z)V
    .locals 3
    .parameter "activity"
    .parameter "requestScreenshot"

    .prologue
    .line 275
    if-nez p2, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 277
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/translate/SdkVersionWrapper$Version11;->sendFeedback(Landroid/app/Activity;Z)V

    .line 299
    :goto_0
    return-void

    .line 281
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.BUG_REPORT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 283
    .local v1, intent:Landroid/content/Intent;
    new-instance v0, Lcom/google/android/apps/translate/SdkVersionWrapper$Version14$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/SdkVersionWrapper$Version14$1;-><init>(Lcom/google/android/apps/translate/SdkVersionWrapper$Version14;)V

    .line 298
    .local v0, conn:Landroid/content/ServiceConnection;
    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method public setHomeButton(Landroid/app/Activity;Z)V
    .locals 2
    .parameter "activity"
    .parameter "enable"

    .prologue
    const/4 v1, 0x0

    .line 263
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 268
    .local v0, actionBar:Landroid/app/ActionBar;
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 269
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 271
    return-void
.end method

.method public useExternalFontsForUnsupportedLanguages()Z
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return v0
.end method
