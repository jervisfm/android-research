.class Lcom/google/android/apps/translate/SettingsActivity$9;
.super Ljava/lang/Object;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/SettingsActivity;->setCameraInputFrontendUrlOnClickListener(Landroid/app/Dialog;Landroid/widget/ListView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/SettingsActivity;

.field final synthetic val$dialog:Landroid/app/Dialog;

.field final synthetic val$listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/SettingsActivity;Landroid/widget/ListView;Landroid/app/Dialog;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 384
    iput-object p1, p0, Lcom/google/android/apps/translate/SettingsActivity$9;->this$0:Lcom/google/android/apps/translate/SettingsActivity;

    iput-object p2, p0, Lcom/google/android/apps/translate/SettingsActivity$9;->val$listView:Landroid/widget/ListView;

    iput-object p3, p0, Lcom/google/android/apps/translate/SettingsActivity$9;->val$dialog:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 387
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/google/android/apps/translate/SettingsActivity$9;->this$0:Lcom/google/android/apps/translate/SettingsActivity;

    iget-object v0, p0, Lcom/google/android/apps/translate/SettingsActivity$9;->val$listView:Landroid/widget/ListView;

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/translate/Profile;->setCameraInputFrontendUrl(Landroid/content/Context;Ljava/lang/String;)V

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/translate/SettingsActivity$9;->this$0:Lcom/google/android/apps/translate/SettingsActivity;

    #calls: Lcom/google/android/apps/translate/SettingsActivity;->setupCameraInputDebugPreferences()V
    invoke-static {v0}, Lcom/google/android/apps/translate/SettingsActivity;->access$600(Lcom/google/android/apps/translate/SettingsActivity;)V

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/translate/SettingsActivity$9;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 393
    return-void
.end method
