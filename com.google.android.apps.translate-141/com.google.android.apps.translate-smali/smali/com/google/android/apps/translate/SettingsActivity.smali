.class public Lcom/google/android/apps/translate/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SettingsActivity.java"


# static fields
.field private static final LANGUAGE_PREFIX:Ljava/lang/String; = "lang_"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.google.android.apps.translate"

.field private static final STRING_TYPE:Ljava/lang/String; = "string"

.field public static final SUPPORT_VOICE_LANG_PREF:Z = true

.field private static final TAG:Ljava/lang/String; = "SettingsActivity"


# instance fields
.field mLanguageList:Lcom/google/android/apps/translate/Languages;

.field private final mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

.field mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/apps/translate/SettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/SettingsActivity$1;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/SettingsActivity;->mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/SettingsActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->refreshLanguageList()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/SettingsActivity;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/SettingsActivity;->showVoiceInputLanguagesListDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/SettingsActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->setupVoiceInputPreferences()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/SettingsActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->openAboutDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/SettingsActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->openOfflineModelManagement()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/SettingsActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->showCameraInputFrontendUrlListDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/translate/SettingsActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->setupCameraInputDebugPreferences()V

    return-void
.end method

.method public static getAboutInfoText(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .parameter "context"

    .prologue
    .line 415
    sget v0, Lcom/google/android/apps/translate/R$string;->about_info:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getVersionCode(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getVoiceInputLangaugesFromStrings(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, voiceInputLanguages:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 199
    .local v3, languages:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 200
    .local v2, languageString:Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/google/android/apps/translate/SettingsActivity;->getVoiceInputLanguageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 201
    .local v1, langName:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 205
    new-instance v4, Lcom/google/android/apps/translate/Language;

    invoke-direct {v4, v2, v1}, Lcom/google/android/apps/translate/Language;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 207
    .end local v1           #langName:Ljava/lang/String;
    .end local v2           #languageString:Ljava/lang/String;
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 208
    return-object v3
.end method

.method private getVoiceInputLanguageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter "shortName"

    .prologue
    .line 224
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/SettingsActivity;->getVoiceInputLanguageNameFromResourceFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, langName:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 243
    .end local v0           #langName:Ljava/lang/String;
    :goto_0
    return-object v0

    .line 229
    .restart local v0       #langName:Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/translate/VoiceInputHelper;->isDogfoodVoiceInputLanguage(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 230
    invoke-static {p1}, Lcom/google/android/apps/translate/VoiceInputHelper;->getVoiceInputLanguageNameFromDogfoodName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 232
    .local v2, nonDogfoodLang:Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/google/android/apps/translate/SettingsActivity;->getVoiceInputLanguageNameFromResourceFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 233
    .local v1, langNameToBePublic:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 235
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 239
    goto :goto_0

    .line 242
    .end local v1           #langNameToBePublic:Ljava/lang/String;
    .end local v2           #nonDogfoodLang:Ljava/lang/String;
    :cond_2
    const-string v3, "SettingsActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unrecognized voice input language: ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v0, ""

    goto :goto_0
.end method

.method private getVoiceInputLanguageNameFromResourceFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .parameter "shortName"

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/translate/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 215
    .local v2, resources:Landroid/content/res/Resources;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "lang_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2d

    const/16 v6, 0x5f

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, resName:Ljava/lang/String;
    const-string v3, "string"

    const-string v4, "com.google.android.apps.translate"

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 217
    .local v0, resId:I
    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 220
    :goto_0
    return-object v3

    :cond_0
    const-string v3, ""

    goto :goto_0
.end method

.method private openAboutDialog()V
    .locals 6

    .prologue
    .line 423
    invoke-static {p0}, Lcom/google/android/apps/translate/SettingsActivity;->getAboutInfoText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 425
    .local v0, aboutInfoText:Ljava/lang/String;
    sget v3, Lcom/google/android/apps/translate/R$layout;->about:I

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 428
    .local v1, aboutView:Landroid/view/View;
    sget v3, Lcom/google/android/apps/translate/R$id;->about_info:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 432
    sget v3, Lcom/google/android/apps/translate/R$id;->feedback:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 433
    .local v2, feedback:Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<u>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/google/android/apps/translate/R$string;->label_send_feedback:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</u>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 436
    new-instance v3, Lcom/google/android/apps/translate/SettingsActivity$10;

    invoke-direct {v3, p0}, Lcom/google/android/apps/translate/SettingsActivity$10;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 448
    sget v3, Lcom/google/android/apps/translate/R$id;->tos_link:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 450
    sget v3, Lcom/google/android/apps/translate/R$id;->privacy_link:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 454
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/translate/R$string;->app_name:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    sget v4, Lcom/google/android/apps/translate/R$drawable;->icon:I

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/translate/SettingsActivity$11;

    invoke-direct {v5, p0}, Lcom/google/android/apps/translate/SettingsActivity$11;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 467
    return-void
.end method

.method private openOfflineModelManagement()V
    .locals 2

    .prologue
    .line 410
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 411
    return-void
.end method

.method private refreshLanguageList()V
    .locals 1

    .prologue
    .line 470
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getLanguageListFromProfile(Landroid/content/Context;)Lcom/google/android/apps/translate/Languages;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/SettingsActivity;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 471
    return-void
.end method

.method private setCameraInputFrontendUrlChecked(Landroid/widget/ListView;Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .parameter "listView"
    .parameter
    .parameter "pref"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 400
    .local p2, urls:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 401
    .local v1, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 402
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 403
    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 407
    :cond_0
    return-void

    .line 401
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setCameraInputFrontendUrlOnClickListener(Landroid/app/Dialog;Landroid/widget/ListView;)V
    .locals 1
    .parameter "dialog"
    .parameter "listView"

    .prologue
    .line 384
    new-instance v0, Lcom/google/android/apps/translate/SettingsActivity$9;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/apps/translate/SettingsActivity$9;-><init>(Lcom/google/android/apps/translate/SettingsActivity;Landroid/widget/ListView;Landroid/app/Dialog;)V

    invoke-virtual {p2, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 395
    return-void
.end method

.method private setCameraInputFrontendUrlsListView(Landroid/widget/ListView;Ljava/util/List;)V
    .locals 4
    .parameter "listView"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 368
    .local p2, urls:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Landroid/widget/ArrayAdapter;

    sget v2, Lcom/google/android/apps/translate/R$layout;->debug_camera_input_frontend_url:I

    sget v3, Lcom/google/android/apps/translate/R$id;->debug_camera_input_frontend_url_text:I

    invoke-direct {v0, p0, v2, v3, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 372
    .local v0, listAdapter:Landroid/widget/ListAdapter;
    invoke-virtual {p0}, Lcom/google/android/apps/translate/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1080012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 374
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 377
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 378
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->getCameraInputFrontendUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 379
    .local v1, pref:Ljava/lang/String;
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/translate/SettingsActivity;->setCameraInputFrontendUrlChecked(Landroid/widget/ListView;Ljava/util/List;Ljava/lang/String;)V

    .line 380
    return-void
.end method

.method private setVoiceInputLanguageChecked(Landroid/widget/ListView;Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .parameter "listView"
    .parameter
    .parameter "preference"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 265
    .local p2, voiceInputLanguages:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 266
    .local v1, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 267
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 269
    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 273
    :cond_0
    return-void

    .line 266
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setVoiceInputLanguageOnClickListener(Landroid/app/Dialog;Landroid/widget/ListView;Ljava/lang/String;)V
    .locals 1
    .parameter "dialog"
    .parameter "listView"
    .parameter "language"

    .prologue
    .line 277
    new-instance v0, Lcom/google/android/apps/translate/SettingsActivity$4;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/google/android/apps/translate/SettingsActivity$4;-><init>(Lcom/google/android/apps/translate/SettingsActivity;Ljava/lang/String;Landroid/widget/ListView;Landroid/app/Dialog;)V

    invoke-virtual {p2, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 288
    return-void
.end method

.method private setVoiceInputLanguagesListView(Landroid/widget/ListView;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .parameter "listView"
    .parameter "language"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p3, voiceInputLanguages:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    new-instance v0, Landroid/widget/ArrayAdapter;

    sget v2, Lcom/google/android/apps/translate/R$layout;->voice_input_language:I

    sget v3, Lcom/google/android/apps/translate/R$id;->text_language_name:I

    invoke-direct {v0, p0, v2, v3, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 252
    .local v0, listAdapter:Landroid/widget/ListAdapter;
    invoke-virtual {p0}, Lcom/google/android/apps/translate/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1080012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 254
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 257
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 258
    invoke-static {p0, p2}, Lcom/google/android/apps/translate/Profile;->getVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 259
    .local v1, preference:Ljava/lang/String;
    invoke-direct {p0, p1, p3, v1}, Lcom/google/android/apps/translate/SettingsActivity;->setVoiceInputLanguageChecked(Landroid/widget/ListView;Ljava/util/List;Ljava/lang/String;)V

    .line 260
    return-void
.end method

.method private setupCameraInputDebugPreferences()V
    .locals 3

    .prologue
    .line 315
    const-string v2, "key_debug_camera_frontend_url"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 316
    .local v1, pref:Landroid/preference/Preference;
    new-instance v2, Lcom/google/android/apps/translate/SettingsActivity$7;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translate/SettingsActivity$7;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 324
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->getCameraInputFrontendUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, current:Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 327
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_0
    const-string v2, "Use default frontend URL"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setupPreferenceListeners()V
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/translate/SettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$string;->key_settings_about:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/translate/SettingsActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/SettingsActivity$5;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 302
    invoke-static {p0}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->isOfflineTranslateSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/translate/SettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$string;->key_manage_offline_model:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/translate/SettingsActivity$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/SettingsActivity$6;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 312
    :cond_0
    return-void
.end method

.method private setupVoiceInputPreferences()V
    .locals 10

    .prologue
    .line 112
    sget v8, Lcom/google/android/apps/translate/R$string;->key_settings_voice_input:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/translate/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/apps/translate/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceGroup;

    .line 114
    .local v7, voiceInputSettings:Landroid/preference/PreferenceGroup;
    invoke-virtual {v7}, Landroid/preference/PreferenceGroup;->removeAll()V

    .line 115
    new-instance v2, Landroid/preference/PreferenceCategory;

    invoke-direct {v2, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 116
    .local v2, localeSettings:Landroid/preference/PreferenceCategory;
    sget v8, Lcom/google/android/apps/translate/R$string;->title_voice_input_locale_settings:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/translate/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 117
    invoke-virtual {v7, v2}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 119
    iget-object v8, p0, Lcom/google/android/apps/translate/SettingsActivity;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    iget-object v9, p0, Lcom/google/android/apps/translate/SettingsActivity;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/translate/VoiceInputHelper;->getTranslateLanguageList(Lcom/google/android/apps/translate/Languages;)Ljava/util/List;

    move-result-object v5

    .line 120
    .local v5, transLangs:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 121
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/translate/Language;

    .line 122
    .local v4, transLang:Lcom/google/android/apps/translate/Language;
    iget-object v8, p0, Lcom/google/android/apps/translate/SettingsActivity;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/translate/VoiceInputHelper;->getVoiceInputLanguageList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 125
    .local v6, voiceInputLangs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/translate/SettingsActivity;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    invoke-virtual {v8, v4}, Lcom/google/android/apps/translate/VoiceInputHelper;->isVoiceInputAvailable(Lcom/google/android/apps/translate/Language;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 128
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 129
    .local v3, preference:Landroid/preference/Preference;
    invoke-virtual {v4}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 130
    invoke-virtual {v4}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 131
    new-instance v8, Lcom/google/android/apps/translate/SettingsActivity$2;

    invoke-direct {v8, p0}, Lcom/google/android/apps/translate/SettingsActivity$2;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 139
    invoke-virtual {v4}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/google/android/apps/translate/Profile;->getVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, current:Ljava/lang/String;
    const-string v8, ""

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 142
    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/SettingsActivity;->getVoiceInputLanguageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 147
    :goto_1
    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 144
    :cond_1
    sget v8, Lcom/google/android/apps/translate/R$string;->summary_voice_input_locale_default:I

    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_1

    .line 152
    .end local v0           #current:Ljava/lang/String;
    .end local v3           #preference:Landroid/preference/Preference;
    .end local v4           #transLang:Lcom/google/android/apps/translate/Language;
    .end local v6           #voiceInputLangs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {v7}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v8

    if-nez v8, :cond_3

    .line 153
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    .line 155
    :cond_3
    return-void
.end method

.method private showCameraInputFrontendUrlListDialog()V
    .locals 7

    .prologue
    .line 337
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->getCameraInputFrontendUrls(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    .line 340
    .local v3, urls:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez v3, :cond_0

    .line 365
    :goto_0
    return-void

    .line 345
    :cond_0
    sget v4, Lcom/google/android/apps/translate/R$layout;->debug_camera_frontend_urls:I

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 346
    .local v1, dialogView:Landroid/view/View;
    sget v4, Lcom/google/android/apps/translate/R$id;->debug_camera_input_frontend_urls:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 348
    .local v2, urlListView:Landroid/widget/ListView;
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/translate/SettingsActivity;->setCameraInputFrontendUrlsListView(Landroid/widget/ListView;Ljava/util/List;)V

    .line 351
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "[DEBUG] Set Camera Input Frontend URL"

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x104

    new-instance v6, Lcom/google/android/apps/translate/SettingsActivity$8;

    invoke-direct {v6, p0}, Lcom/google/android/apps/translate/SettingsActivity$8;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 363
    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/translate/SettingsActivity;->setCameraInputFrontendUrlOnClickListener(Landroid/app/Dialog;Landroid/widget/ListView;)V

    .line 364
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private showVoiceInputLanguagesListDialog(Ljava/lang/String;)V
    .locals 7
    .parameter "language"

    .prologue
    .line 163
    iget-object v4, p0, Lcom/google/android/apps/translate/SettingsActivity;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    invoke-virtual {v4, p1}, Lcom/google/android/apps/translate/VoiceInputHelper;->getVoiceInputLanguageList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 167
    .local v3, voiceInputLanguages:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    sget v4, Lcom/google/android/apps/translate/R$layout;->change_languages:I

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 174
    .local v1, dialogView:Landroid/view/View;
    sget v4, Lcom/google/android/apps/translate/R$id;->voice_input_languages:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 175
    .local v2, langListView:Landroid/widget/ListView;
    invoke-direct {p0, v3}, Lcom/google/android/apps/translate/SettingsActivity;->getVoiceInputLangaugesFromStrings(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v2, p1, v4}, Lcom/google/android/apps/translate/SettingsActivity;->setVoiceInputLanguagesListView(Landroid/widget/ListView;Ljava/lang/String;Ljava/util/List;)V

    .line 179
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget v5, Lcom/google/android/apps/translate/R$string;->title_voice_input_languages:I

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x108009b

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x104

    new-instance v6, Lcom/google/android/apps/translate/SettingsActivity$3;

    invoke-direct {v6, p0}, Lcom/google/android/apps/translate/SettingsActivity$3;-><init>(Lcom/google/android/apps/translate/SettingsActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 192
    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-direct {p0, v0, v2, p1}, Lcom/google/android/apps/translate/SettingsActivity;->setVoiceInputLanguageOnClickListener(Landroid/app/Dialog;Landroid/widget/ListView;Ljava/lang/String;)V

    .line 193
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/translate/SettingsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/TranslateApplication;

    .line 60
    .local v0, app:Lcom/google/android/apps/translate/TranslateApplication;
    invoke-virtual {v0}, Lcom/google/android/apps/translate/TranslateApplication;->getVoiceInputHelper()Lcom/google/android/apps/translate/VoiceInputHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/translate/SettingsActivity;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->refreshLanguageList()V

    .line 63
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->beforeSetContentView(Landroid/app/Activity;)V

    .line 64
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    sget v3, Lcom/google/android/apps/translate/R$xml;->settings_activity:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/SettingsActivity;->addPreferencesFromResource(I)V

    .line 66
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->afterSetContentView(Landroid/app/Activity;)V

    .line 69
    invoke-static {p0}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->isOfflineTranslateSupported(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 70
    const-string v3, "key_root_settings"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    .line 71
    .local v1, mainPref:Landroid/preference/PreferenceScreen;
    const-string v3, "key_offline_translate_settings"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 72
    .local v2, pref:Landroid/preference/Preference;
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 76
    .end local v1           #mainPref:Landroid/preference/PreferenceScreen;
    .end local v2           #pref:Landroid/preference/Preference;
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/unveil/textinput/TextInputView;->isSupported(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 77
    const-string v3, "key_title_settings"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 78
    .local v1, mainPref:Landroid/preference/PreferenceCategory;
    const-string v3, "key_enable_camera_logging"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 79
    .restart local v2       #pref:Landroid/preference/Preference;
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 83
    .end local v1           #mainPref:Landroid/preference/PreferenceCategory;
    .end local v2           #pref:Landroid/preference/Preference;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->setupVoiceInputPreferences()V

    .line 91
    invoke-virtual {v0}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild()Z

    move-result v3

    if-nez v3, :cond_2

    .line 92
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->setupCameraInputDebugPreferences()V

    .line 100
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/SettingsActivity;->setupPreferenceListeners()V

    .line 101
    iget-object v3, p0, Lcom/google/android/apps/translate/SettingsActivity;->mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/translate/SettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 103
    return-void

    .line 95
    :cond_2
    const-string v3, "key_title_settings"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 96
    .restart local v1       #mainPref:Landroid/preference/PreferenceCategory;
    const-string v3, "key_debug_camera_frontend_url"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 97
    .restart local v2       #pref:Landroid/preference/Preference;
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/translate/SettingsActivity;->mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/SettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 108
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 109
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 475
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 481
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 477
    :pswitch_0
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->openHomeActivity(Landroid/app/Activity;)V

    .line 478
    const/4 v0, 0x1

    goto :goto_0

    .line 475
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
