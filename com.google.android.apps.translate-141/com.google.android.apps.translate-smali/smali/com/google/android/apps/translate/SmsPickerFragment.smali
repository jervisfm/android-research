.class public Lcom/google/android/apps/translate/SmsPickerFragment;
.super Landroid/app/ListFragment;
.source "SmsPickerFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SmsPickerFragment"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 26
    new-instance v0, Lcom/google/android/apps/translate/SmsPickerHelper;

    invoke-direct {v0}, Lcom/google/android/apps/translate/SmsPickerHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    .line 31
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 49
    const-string v0, "SmsPickerFragment"

    const-string v1, "onActivityCreated"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/translate/SmsPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mActivity:Landroid/app/Activity;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    iget-object v1, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/SmsPickerHelper;->onCreate(Landroid/app/Activity;)V

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/translate/SmsPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/translate/SmsPickerHelper;->init(Landroid/widget/LinearLayout;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/SmsPickerHelper;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/SmsPickerFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/translate/SmsPickerFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 56
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 35
    const-string v0, "SmsPickerFragment"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/SmsPickerFragment;->setHasOptionsMenu(Z)V

    .line 38
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .parameter "menu"
    .parameter "inflater"

    .prologue
    .line 81
    const-string v0, "SmsPickerFragment"

    const-string v1, "onCreateOptionsMenu"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/translate/SmsPickerHelper;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/ListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 44
    sget v0, Lcom/google/android/apps/translate/R$layout;->sms_picker_activity:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v7, 0x0

    .line 67
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->SMS:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->UNKNOWN:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationInputMethod(Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 71
    iget-object v6, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/translate/SmsPickerHelper;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v6, v0, v7, v7, v1}, Lcom/google/android/apps/translate/translation/TranslateFragment;->startTranslateFragment(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Z)V

    .line 77
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    .line 97
    const-string v0, "SmsPickerFragment"

    const-string v1, "onOptionsItemSelected"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/SmsPickerHelper;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 102
    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .parameter "menu"

    .prologue
    .line 90
    const-string v0, "SmsPickerFragment"

    const-string v1, "onPrepareOptionsMenu"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mSmsPickerHelper:Lcom/google/android/apps/translate/SmsPickerHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/SmsPickerHelper;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 92
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 93
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 60
    const-string v0, "SmsPickerFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 62
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/SmsPickerFragment;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->setHomeButton(Landroid/app/Activity;Z)V

    .line 63
    return-void
.end method
