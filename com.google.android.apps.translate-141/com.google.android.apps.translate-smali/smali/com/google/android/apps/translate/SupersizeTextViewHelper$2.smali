.class Lcom/google/android/apps/translate/SupersizeTextViewHelper$2;
.super Ljava/lang/Object;
.source "SupersizeTextViewHelper.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/SupersizeTextViewHelper;->setupZoomSupport()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

.field final synthetic val$scaleDetector:Landroid/view/ScaleGestureDetector;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$2;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    iput-object p2, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$2;->val$scaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "v"
    .parameter "event"

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$2;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    #getter for: Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->access$400(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$2;->val$scaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 311
    const/4 v0, 0x1

    return v0
.end method
