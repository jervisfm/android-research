.class Lcom/google/android/apps/translate/SupersizeTextViewHelper$3;
.super Ljava/lang/Object;
.source "SupersizeTextViewHelper.java"

# interfaces
.implements Landroid/widget/ZoomButtonsController$OnZoomListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/SupersizeTextViewHelper;->setupZoomButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)V
    .locals 0
    .parameter

    .prologue
    .line 324
    iput-object p1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$3;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVisibilityChanged(Z)V
    .locals 2
    .parameter "visible"

    .prologue
    .line 327
    if-nez p1, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$3;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    #getter for: Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->access$400(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$3;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    #getter for: Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTriggerZoomButtonsListener:Landroid/view/View$OnTouchListener;
    invoke-static {v1}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->access$500(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)Landroid/view/View$OnTouchListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 330
    :cond_0
    return-void
.end method

.method public onZoom(Z)V
    .locals 2
    .parameter "zoomIn"

    .prologue
    .line 334
    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$3;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    if-eqz p1, :cond_0

    const v0, 0x3f8ccccd

    :goto_0
    #calls: Lcom/google/android/apps/translate/SupersizeTextViewHelper;->zoomSupersizeText(F)V
    invoke-static {v1, v0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->access$200(Lcom/google/android/apps/translate/SupersizeTextViewHelper;F)V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$3;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    #calls: Lcom/google/android/apps/translate/SupersizeTextViewHelper;->updateZoomButtonsEnabled()V
    invoke-static {v0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->access$100(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)V

    .line 336
    return-void

    .line 334
    :cond_0
    const v0, 0x3f666666

    goto :goto_0
.end method
