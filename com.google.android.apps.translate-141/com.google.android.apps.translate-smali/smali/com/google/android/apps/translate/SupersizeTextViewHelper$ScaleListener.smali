.class Lcom/google/android/apps/translate/SupersizeTextViewHelper$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "SupersizeTextViewHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/SupersizeTextViewHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)V
    .locals 0
    .parameter

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$ScaleListener;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;Lcom/google/android/apps/translate/SupersizeTextViewHelper$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 260
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/SupersizeTextViewHelper$ScaleListener;-><init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .parameter "detector"

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$ScaleListener;->this$0:Lcom/google/android/apps/translate/SupersizeTextViewHelper;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    #calls: Lcom/google/android/apps/translate/SupersizeTextViewHelper;->zoomSupersizeText(F)V
    invoke-static {v0, v1}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->access$200(Lcom/google/android/apps/translate/SupersizeTextViewHelper;F)V

    .line 264
    const/4 v0, 0x1

    return v0
.end method
