.class public Lcom/google/android/apps/translate/SupersizeTextViewHelper;
.super Ljava/lang/Object;
.source "SupersizeTextViewHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/SupersizeTextViewHelper$ScaleListener;,
        Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;
    }
.end annotation


# static fields
.field private static final DEFAULT_FONT_SIZE:I = 0x3c

.field private static final SCALE_MIN_FONT_SIZE:I = 0xf

.field private static final ZOOM_IN_SCALE_FACTOR:F = 1.1f

.field private static final ZOOM_OUT_SCALE_FACTOR:F = 0.9f

.field private static final ZOOM_SPEED:I = 0x64


# instance fields
.field private mCalculatedFont:F

.field private final mContext:Landroid/content/Context;

.field private mMaxFontSizeInPixel:I

.field private mScaleFactor:F

.field private final mTextView:Landroid/widget/TextView;

.field private final mTriggerZoomButtonsListener:Landroid/view/View$OnTouchListener;

.field private final mViewHeight:I

.field private final mViewWidth:I

.field private mZoomController:Landroid/widget/ZoomButtonsController;

.field private mZoomMaxScale:F

.field private mZoomMinScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;II)V
    .locals 3
    .parameter "context"
    .parameter "textView"
    .parameter "widgetWidth"
    .parameter "widgetHeight"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/high16 v0, 0x4270

    iput v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mCalculatedFont:F

    .line 60
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    .line 63
    const/16 v0, 0x3c

    iput v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mMaxFontSizeInPixel:I

    .line 71
    new-instance v0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper$1;-><init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTriggerZoomButtonsListener:Landroid/view/View$OnTouchListener;

    .line 112
    invoke-static {p1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mContext:Landroid/content/Context;

    .line 113
    invoke-static {p2}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    .line 114
    if-lez p3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/translate/Preconditions;->checkArguments(Z)V

    .line 115
    if-lez p4, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/google/android/apps/translate/Preconditions;->checkArguments(Z)V

    .line 117
    iput p3, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mViewWidth:I

    .line 118
    iput p4, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mViewHeight:I

    .line 121
    iget v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mViewWidth:I

    iput v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mMaxFontSizeInPixel:I

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->enableTextScrolling(Landroid/widget/TextView;Z)V

    .line 127
    invoke-direct {p0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->setupZoomSupport()V

    .line 128
    return-void

    :cond_1
    move v0, v2

    .line 114
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)Landroid/widget/ZoomButtonsController;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->updateZoomButtonsEnabled()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/SupersizeTextViewHelper;F)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->zoomSupersizeText(F)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)Landroid/view/View$OnTouchListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTriggerZoomButtonsListener:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method private static calcTextLayoutInfo(Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;Ljava/lang/String;ILandroid/text/TextPaint;I)V
    .locals 8
    .parameter "resultLayout"
    .parameter "text"
    .parameter "viewWidth"
    .parameter "testPaint"
    .parameter "tryFontSize"

    .prologue
    const/4 v7, 0x0

    .line 237
    if-lez p4, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/apps/translate/Preconditions;->checkArguments(Z)V

    .line 238
    int-to-float v1, p4

    invoke-virtual {p3, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 240
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f80

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p3

    move v3, p2

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 243
    .local v0, layout:Landroid/text/StaticLayout;
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;->requiredHeight:I

    .line 244
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;->requiredLines:I

    .line 245
    return-void

    .end local v0           #layout:Landroid/text/StaticLayout;
    :cond_0
    move v1, v7

    .line 237
    goto :goto_0
.end method

.method private static createTestPaint(Landroid/graphics/Typeface;I)Landroid/text/TextPaint;
    .locals 2
    .parameter "textTypeface"
    .parameter "fontSize"

    .prologue
    .line 217
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    .line 218
    .local v0, testPaint:Landroid/text/TextPaint;
    invoke-virtual {v0, p0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 219
    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 220
    return-object v0
.end method

.method private static enableTextScrolling(Landroid/widget/TextView;Z)V
    .locals 1
    .parameter "textView"
    .parameter "enable"

    .prologue
    .line 289
    if-eqz p1, :cond_0

    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 290
    return-void

    .line 289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFontSizeTooBig(Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;)Z
    .locals 2
    .parameter "requirement"

    .prologue
    .line 256
    iget v0, p1, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;->requiredHeight:I

    iget v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mViewHeight:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pickFontInRect()Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;
    .locals 8

    .prologue
    const/16 v7, 0x3c

    .line 181
    new-instance v3, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;

    const/4 v6, 0x1

    invoke-direct {v3, v7, v6, v7}, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;-><init>(III)V

    .line 182
    .local v3, requirement:Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;
    iget-object v6, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    iget v7, v3, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;->fontSize:I

    invoke-static {v6, v7}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->createTestPaint(Landroid/graphics/Typeface;I)Landroid/text/TextPaint;

    move-result-object v4

    .line 183
    .local v4, testPaint:Landroid/text/TextPaint;
    iget-object v6, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 188
    .local v5, text:Ljava/lang/String;
    iget v1, v3, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;->fontSize:I

    .line 189
    .local v1, lowFontSize:I
    iget v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mViewWidth:I

    .line 190
    .local v0, highFontSize:I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 193
    sub-int v6, v0, v1

    add-int/lit8 v6, v6, 0x1

    div-int/lit8 v6, v6, 0x2

    add-int v2, v1, v6

    .line 194
    .local v2, midFontSize:I
    iget v6, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mViewWidth:I

    invoke-static {v3, v5, v6, v4, v2}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->calcTextLayoutInfo(Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;Ljava/lang/String;ILandroid/text/TextPaint;I)V

    .line 195
    invoke-direct {p0, v3}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->isFontSizeTooBig(Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 196
    add-int/lit8 v0, v2, -0x1

    goto :goto_0

    .line 198
    :cond_0
    move v1, v2

    goto :goto_0

    .line 203
    .end local v2           #midFontSize:I
    :cond_1
    iput v1, v3, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;->fontSize:I

    .line 204
    return-object v3
.end method

.method private setupZoomButtons()V
    .locals 3

    .prologue
    .line 321
    new-instance v0, Landroid/widget/ZoomButtonsController;

    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    invoke-direct {v0, v1}, Landroid/widget/ZoomButtonsController;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/widget/ZoomButtonsController;->setZoomSpeed(J)V

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setAutoDismissed(Z)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    new-instance v1, Lcom/google/android/apps/translate/SupersizeTextViewHelper$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper$3;-><init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;)V

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setOnZoomListener(Landroid/widget/ZoomButtonsController$OnZoomListener;)V

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTriggerZoomButtonsListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 339
    return-void
.end method

.method private setupZoomSupport()V
    .locals 4

    .prologue
    .line 297
    invoke-static {}, Lcom/google/android/apps/translate/Util;->getSdkVersion()I

    move-result v1

    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    .line 298
    invoke-direct {p0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->setupZoomButtons()V

    .line 314
    :goto_0
    return-void

    .line 302
    :cond_0
    new-instance v0, Landroid/view/ScaleGestureDetector;

    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/apps/translate/SupersizeTextViewHelper$ScaleListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/translate/SupersizeTextViewHelper$ScaleListener;-><init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;Lcom/google/android/apps/translate/SupersizeTextViewHelper$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 305
    .local v0, scaleDetector:Landroid/view/ScaleGestureDetector;
    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/apps/translate/SupersizeTextViewHelper$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper$2;-><init>(Lcom/google/android/apps/translate/SupersizeTextViewHelper;Landroid/view/ScaleGestureDetector;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private updateZoomButtonsEnabled()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    invoke-static {v0}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    iget-object v3, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    iget v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    iget v4, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomMaxScale:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ZoomButtonsController;->setZoomInEnabled(Z)V

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    iget v3, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    iget v4, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomMinScale:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setZoomOutEnabled(Z)V

    .line 348
    return-void

    :cond_0
    move v0, v2

    .line 346
    goto :goto_0

    :cond_1
    move v1, v2

    .line 347
    goto :goto_1
.end method

.method private zoomSupersizeText(F)V
    .locals 4
    .parameter "scaleFactor"

    .prologue
    .line 275
    iget v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    .line 278
    iget v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    iget v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomMaxScale:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomMinScale:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mCalculatedFont:F

    iget v3, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 281
    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    .line 136
    :cond_0
    return-void
.end method

.method public resizeText(Ljava/lang/String;[Lcom/google/android/apps/translate/Language;)V
    .locals 4
    .parameter "text"
    .parameter "textPossibleLanguages"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 147
    invoke-static {p2}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    array-length v1, p2

    if-lez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-static {p1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    invoke-direct {p0}, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->pickFontInRect()Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;

    move-result-object v0

    .line 155
    .local v0, textLayoutInfo:Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;
    iget v1, v0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;->requiredLines:I

    if-ne v1, v2, :cond_0

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 160
    :cond_0
    iget v1, v0, Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;->fontSize:I

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mCalculatedFont:F

    .line 163
    const/high16 v1, 0x3f80

    iput v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mScaleFactor:F

    .line 164
    const/high16 v1, 0x4170

    iget v2, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mCalculatedFont:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomMinScale:F

    .line 165
    iget v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mMaxFontSizeInPixel:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mCalculatedFont:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mZoomMaxScale:F

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mCalculatedFont:F

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/translate/SupersizeTextViewHelper;->mTextView:Landroid/widget/TextView;

    sget-object v2, Lcom/google/android/apps/translate/Constants$AppearanceType;->UNCHANGED:Lcom/google/android/apps/translate/Constants$AppearanceType;

    invoke-static {v1, p1, p2, v2, v3}, Lcom/google/android/apps/translate/Util;->setTextAndFont(Landroid/widget/TextView;Ljava/lang/String;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;Z)V

    .line 170
    return-void

    .end local v0           #textLayoutInfo:Lcom/google/android/apps/translate/SupersizeTextViewHelper$TextLayoutInfo;
    :cond_1
    move v1, v3

    .line 148
    goto :goto_0
.end method
