.class public Lcom/google/android/apps/translate/Translate$TranslateResult;
.super Ljava/lang/Object;
.source "Translate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/Translate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TranslateResult"
.end annotation


# static fields
.field private static final DETECTED_SRCLANG:I = 0x5

.field private static final DICTIONARY_RESULT:I = 0x2

.field private static final RESULT_PART_NUM:I = 0x6

.field private static final SPELL_CORRECTION:I = 0x4

.field private static final SRC_TRANSLITERATION:I = 0x3

.field private static final TRANSLATE_TEXT:I = 0x0

.field private static final TRG_TRANSLITERATION:I = 0x1


# instance fields
.field private final mParts:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/translate/history/Entry;)V
    .locals 1
    .parameter "translateEntry"

    .prologue
    .line 200
    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/Entry;->getOutputText()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "resultText"

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Ljava/lang/String;Z)V

    .line 171
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 6
    .parameter "resultText"
    .parameter "hasFromLangCode"

    .prologue
    const/4 v2, 0x1

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    const-string v3, "Translate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resultText="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->splitTranslateResult(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, parts:[Ljava/lang/String;
    if-eqz p2, :cond_0

    array-length v3, v1

    if-le v3, v2, :cond_0

    .line 189
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    .line 190
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 191
    iget-object v3, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v1, v4

    aput-object v4, v3, v0

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    .end local v0           #i:I
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    .line 196
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    array-length v3, v3

    const/4 v4, 0x6

    if-gt v3, v4, :cond_2

    :goto_1
    invoke-static {v2}, Lcom/google/android/apps/translate/Preconditions;->checkArguments(Z)V

    .line 197
    return-void

    .line 196
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static appendTranslateResultLangIdsFieldFromLandIdJson(Ljava/lang/StringBuilder;Lorg/json/JSONObject;)V
    .locals 6
    .parameter "srcDetectedLang"
    .parameter "langIdJson"

    .prologue
    .line 276
    if-nez p1, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    :try_start_0
    const-string v4, "srclangs"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 286
    .local v3, srcLangsJson:Lorg/json/JSONArray;
    if-eqz v3, :cond_0

    .line 289
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 290
    .local v0, cnt:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v0, :cond_0

    .line 292
    :try_start_1
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 297
    add-int/lit8 v4, v0, -0x1

    if-ge v2, v4, :cond_2

    .line 298
    const/16 v4, 0x2c

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 290
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 282
    .end local v0           #cnt:I
    .end local v2           #i:I
    .end local v3           #srcLangsJson:Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 283
    .local v1, e:Lorg/json/JSONException;
    const-string v4, "Failed to parse language detection result. "

    invoke-static {v4, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 293
    .end local v1           #e:Lorg/json/JSONException;
    .restart local v0       #cnt:I
    .restart local v2       #i:I
    .restart local v3       #srcLangsJson:Lorg/json/JSONArray;
    :catch_1
    move-exception v1

    .line 294
    .restart local v1       #e:Lorg/json/JSONException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to parse language detection result. position="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private final getPart(I)Ljava/lang/String;
    .locals 1
    .parameter "partIndex"

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private final hasPart(I)Z
    .locals 1
    .parameter "partIndex"

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getDetectedSrcShortLangName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter "currentSrcLangShortName"

    .prologue
    const/4 v5, 0x5

    .line 216
    invoke-direct {p0, v5}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getPart(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 217
    new-instance v3, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v4, 0x2c

    invoke-direct {v3, v4}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 218
    .local v3, split:Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-direct {p0, v5}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getPart(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 219
    const/4 v0, 0x0

    .line 220
    .local v0, firstLang:Ljava/lang/String;
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 221
    .local v2, lang:Ljava/lang/String;
    if-nez v0, :cond_1

    .line 222
    move-object v0, v2

    .line 229
    :cond_1
    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 230
    const-string v0, ""

    .line 235
    .end local v0           #firstLang:Ljava/lang/String;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #lang:Ljava/lang/String;
    .end local v3           #split:Landroid/text/TextUtils$SimpleStringSplitter;
    :cond_2
    :goto_0
    return-object v0

    .line 233
    .restart local v0       #firstLang:Ljava/lang/String;
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #split:Landroid/text/TextUtils$SimpleStringSplitter;
    :cond_3
    if-nez v0, :cond_2

    const-string v0, ""

    goto :goto_0

    .line 235
    .end local v0           #firstLang:Ljava/lang/String;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #split:Landroid/text/TextUtils$SimpleStringSplitter;
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public final getDictionaryResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getPart(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSpellCorrection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getPart(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSrcTransliteration()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getPart(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTranslateText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getPart(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTrgTransliteration()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getPart(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hasDetectedSrcShorLangName()Z
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->hasPart(I)Z

    move-result v0

    return v0
.end method

.method public final hasDictionaryResult()Z
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->hasPart(I)Z

    move-result v0

    return v0
.end method

.method public final hasSrcTransliteration()Z
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->hasPart(I)Z

    move-result v0

    return v0
.end method

.method public final hasTranslateText()Z
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->hasPart(I)Z

    move-result v0

    return v0
.end method

.method public final hasTrgTransliteration()Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->hasPart(I)Z

    move-result v0

    return v0
.end method

.method public stripOnMemoryAttributes()Z
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    .line 256
    iget-object v2, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    array-length v0, v2

    .line 257
    .local v0, len:I
    const/4 v1, 0x0

    .line 258
    .local v1, stripped:Z
    if-le v0, v4, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 259
    iget-object v2, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    .line 260
    const/4 v1, 0x1

    .line 262
    :cond_0
    if-le v0, v5, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 263
    iget-object v2, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v5

    .line 264
    const/4 v1, 0x1

    .line 266
    :cond_1
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 271
    const-string v0, "\t"

    iget-object v1, p0, Lcom/google/android/apps/translate/Translate$TranslateResult;->mParts:[Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
