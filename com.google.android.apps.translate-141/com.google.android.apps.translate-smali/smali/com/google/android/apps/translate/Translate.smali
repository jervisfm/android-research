.class public Lcom/google/android/apps/translate/Translate;
.super Ljava/lang/Object;
.source "Translate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/Translate$TranslateResult;
    }
.end annotation


# static fields
.field public static final ALPHA_LANG_PARAM:Ljava/lang/String; = "al"

.field private static final ALPHA_VALUE:Ljava/lang/String; = "1"

.field private static final CB_VALUE:Ljava/lang/String; = "1"

.field private static final DICTIONARY_FIELD:Ljava/lang/String; = "dict"

.field private static final DICTIONARY_SYNONYMS:Ljava/lang/String; = "terms"

.field private static final DICTIONARY_TYPE:Ljava/lang/String; = "pos"

.field private static final ENCODING:Ljava/lang/String; = "UTF-8"

.field public static final ERROR_EMPTY:I = -0x1

.field public static final ERROR_EMPTY_WITH_LANGID:I = -0x4

.field public static final ERROR_INPUT_TEXT_UTF8_ENCODING:I = -0x65

.field public static final ERROR_JSON_PARSING:I = -0x12d

.field public static final ERROR_MSG_PREFIX:Ljava/lang/String; = " E"

.field public static final ERROR_NETWORK:I = -0x2

.field public static final ERROR_NETWORK_HTTP:I = -0xc9

.field public static final ERROR_NONE:I = 0x0

.field private static final FIND_LANG_VAR:Ljava/lang/String; = "&hl="

.field public static final INPUT_ENCODING_PARAM:Ljava/lang/String; = "ie"

.field private static final INPUT_ENCODING_VAR:Ljava/lang/String; = "&ie="

.field private static final LANGID_FIELD:Ljava/lang/String; = "ld_result"

.field private static final LANGID_SEPARATOR:C = ','

.field private static final LANGID_SRCLANGS_SUB_FIELD:Ljava/lang/String; = "srclangs"

.field public static final LOCALE_LANG_PARAM:Ljava/lang/String; = "hl"

.field private static final LOCALE_LANG_VAR:Ljava/lang/String; = "&hl="

.field public static final NAME_SEPARATOR:Ljava/lang/String; = ":"

.field public static final OTF_DEFAULT_PARAM:Ljava/lang/String; = "&otf=1"

.field public static final OTF_INSTANT_PARAM:Ljava/lang/String; = "&otf=2"

.field public static final OTF_NEW_INSTANT_PARAM:Ljava/lang/String; = "&otf=3"

.field public static final OTF_PARAM:Ljava/lang/String; = "otf"

.field public static final OUTPUT_ENCODING_PARAM:Ljava/lang/String; = "oe"

.field private static final OUTPUT_ENCODING_VAR:Ljava/lang/String; = "&oe="

.field private static final SOURCE_LANGUAGE_FIELD:Ljava/lang/String; = "src"

.field public static final SOURCE_LANG_PARAM:Ljava/lang/String; = "sl"

.field private static final SOURCE_LANG_VAR:Ljava/lang/String; = "&sl="

.field private static final SPELLCORRECTION_FIELD:Ljava/lang/String; = "spell"

.field private static final SPELLCORRECTION_SUB_FIELD:Ljava/lang/String; = "spell_html_res"

.field private static final SRC_TRANSLITERATION_FIELD:Ljava/lang/String; = "src_translit"

.field private static final TAG:Ljava/lang/String; = "Translate"

.field public static final TARGET_LANG_PARAM:Ljava/lang/String; = "tl"

.field private static final TARGET_LANG_VAR:Ljava/lang/String; = "&tl="

.field private static final TEXT_VAR:Ljava/lang/String; = "&text="

.field public static final TRANSLATE_HOST:Ljava/lang/String; = "translate.google.com"

.field public static final TRANSLATE_PAGE_PARAM:Ljava/lang/String; = "u"

.field public static final TRANSLATE_PAGE_PATH:Ljava/lang/String; = "translate"

.field public static final TRANSLATE_QUERY_DELIM_PATTERN:Ljava/lang/String; = "\\|"

.field public static final TRANSLATE_QUERY_PARAM:Ljava/lang/String; = "q"

.field public static final TRANSLATE_SCHEME:Ljava/lang/String; = "http"

.field public static final TRANSLATE_WEB_MIMETYPE:Ljava/lang/String; = "text/html"

.field private static final TRANSLATION_DATA_FIELD:Ljava/lang/String; = "sentences"

.field private static final TRANSLATION_FIELD:Ljava/lang/String; = "trans"

.field private static final TRG_TRANSLITERATION_FIELD:Ljava/lang/String; = "translit"

.field private static final URL_LANGUAGE:Ljava/lang/String; = "http://translate.google.com/translate_a/l?client=at&cb=1&alpha=1"

.field private static final URL_TRANSLATE:Ljava/lang/String; = "http://translate.google.com/translate_a/t?client=at&sc=1&v=2.0"

.field private static mAcceptLanguage:Ljava/lang/String;

.field private static mUserAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/translate/Translate;->mUserAgent:Ljava/lang/String;

    .line 144
    const-string v0, "en"

    sput-object v0, Lcom/google/android/apps/translate/Translate;->mAcceptLanguage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    return-void
.end method

.method private static extractLanguages(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .parameter "raw"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 511
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 512
    .local v3, languages:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 513
    const-string v4, "1("

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, ")"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 540
    :cond_0
    :goto_0
    return-object v3

    .line 516
    :cond_1
    const-string v4, "1"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 521
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    const/16 v4, 0x7b

    const/16 v5, 0x5b

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x7d

    const/16 v6, 0x5d

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x3a

    const/16 v6, 0x2c

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 525
    .local v2, jsonArray:Lorg/json/JSONArray;
    invoke-static {v2}, Lcom/google/android/apps/translate/Translate;->verifyJsonArrayText(Lorg/json/JSONArray;)V

    .line 528
    const-string v4, "sl"

    const-string v5, "sl"

    const/4 v6, 0x0

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/translate/Translate;->getSubArrayBykeyName(Lorg/json/JSONArray;Ljava/lang/String;Z)Lorg/json/JSONArray;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lcom/google/android/apps/translate/Translate;->parseJsonLanguageList(Ljava/lang/String;Lorg/json/JSONArray;Ljava/util/List;)V

    .line 530
    const-string v4, "tl"

    const-string v5, "tl"

    const/4 v6, 0x0

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/translate/Translate;->getSubArrayBykeyName(Lorg/json/JSONArray;Ljava/lang/String;Z)Lorg/json/JSONArray;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lcom/google/android/apps/translate/Translate;->parseJsonLanguageList(Ljava/lang/String;Lorg/json/JSONArray;Ljava/util/List;)V

    .line 534
    const-string v4, "al"

    const/4 v5, 0x1

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/translate/Translate;->getSubArrayBykeyName(Lorg/json/JSONArray;Ljava/lang/String;Z)Lorg/json/JSONArray;

    move-result-object v0

    .line 536
    .local v0, alphaArray:Lorg/json/JSONArray;
    if-eqz v0, :cond_0

    .line 537
    const-string v4, "al"

    invoke-static {v4, v0, v3}, Lcom/google/android/apps/translate/Translate;->parseJsonLanguageList(Ljava/lang/String;Lorg/json/JSONArray;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 541
    .end local v0           #alphaArray:Lorg/json/JSONArray;
    .end local v2           #jsonArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 542
    .local v1, ex:Ljava/lang/Exception;
    const-string v4, "TranslateService"

    const-string v5, "JSON error."

    invoke-static {v4, v5, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 543
    throw v1
.end method

.method private static extractSuggestions(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 768
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 770
    .local v5, suggestions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 771
    .local v2, jsonArray:Lorg/json/JSONArray;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v3

    .line 772
    .local v3, list:Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 773
    .local v4, size:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 774
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 773
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 776
    .end local v1           #i:I
    .end local v2           #jsonArray:Lorg/json/JSONArray;
    .end local v3           #list:Lorg/json/JSONArray;
    .end local v4           #size:I
    :catch_0
    move-exception v0

    .line 777
    .local v0, e:Lorg/json/JSONException;
    const-string v6, "TranslateService"

    const-string v7, "JSON error."

    invoke-static {v6, v7, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 778
    throw v0

    .line 780
    .end local v0           #e:Lorg/json/JSONException;
    .restart local v1       #i:I
    .restart local v2       #jsonArray:Lorg/json/JSONArray;
    .restart local v3       #list:Lorg/json/JSONArray;
    .restart local v4       #size:I
    :cond_0
    return-object v5
.end method

.method private static fetchUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 444
    sget-object v2, Lcom/google/android/apps/translate/Translate;->mUserAgent:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 445
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "No user-agent"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 448
    :cond_0
    sget-object v2, Lcom/google/android/apps/translate/Translate;->mUserAgent:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->newHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v3, p0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 450
    .local v0, response:Lorg/apache/http/HttpResponse;
    const-string v1, ""

    .line 451
    .local v1, result:Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 452
    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->readStringFromHttpResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v1

    .line 455
    :cond_1
    return-object v1
.end method

.method private static fetchUrl1(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 460
    const/4 v1, 0x0

    .line 461
    .local v1, is:Ljava/io/InputStream;
    const-string v4, ""

    .line 463
    .local v4, result:Ljava/lang/String;
    :try_start_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 464
    .local v0, client:Lorg/apache/http/client/HttpClient;
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 465
    .local v2, request:Lorg/apache/http/client/methods/HttpGet;
    invoke-interface {v0, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 466
    .local v3, response:Lorg/apache/http/HttpResponse;
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 467
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    .line 468
    .local v5, statusLine:Lorg/apache/http/StatusLine;
    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_1

    .line 469
    invoke-static {v1}, Lcom/google/android/apps/translate/Translate;->toString(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 474
    if-eqz v1, :cond_0

    .line 475
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 478
    :cond_0
    return-object v4

    .line 471
    :cond_1
    :try_start_1
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474
    .end local v0           #client:Lorg/apache/http/client/HttpClient;
    .end local v2           #request:Lorg/apache/http/client/methods/HttpGet;
    .end local v3           #response:Lorg/apache/http/HttpResponse;
    .end local v5           #statusLine:Lorg/apache/http/StatusLine;
    :catchall_0
    move-exception v6

    if-eqz v1, :cond_2

    .line 475
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v6
.end method

.method private static formatOfflineOutput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter "input"
    .parameter "sourceLanguage"
    .parameter "output"

    .prologue
    .line 801
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 802
    .local v3, sentences:Lorg/json/JSONObject;
    const-string v4, "trans"

    invoke-virtual {v3, v4, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 803
    const-string v4, "src_translit"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 804
    const-string v4, "translit"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 805
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 806
    .local v0, array:Lorg/json/JSONArray;
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 808
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 809
    .local v2, json:Lorg/json/JSONObject;
    const-string v4, "sentences"

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 810
    const-string v4, "src"

    invoke-virtual {v2, v4, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 813
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/google/android/apps/translate/Translate;->formatTranslation(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 818
    .end local v0           #array:Lorg/json/JSONArray;
    .end local v2           #json:Lorg/json/JSONObject;
    .end local v3           #sentences:Lorg/json/JSONObject;
    :goto_0
    return-object v4

    .line 814
    :catch_0
    move-exception v1

    .line 815
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 816
    const-string v4, "Exception in formatOfflineOutput."

    invoke-static {v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 818
    const-string v4, ""

    goto :goto_0
.end method

.method private static formatTranslation(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 25
    .parameter "sourceLanguage"
    .parameter "raw"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 641
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 642
    if-eqz p1, :cond_0

    const-string v22, ""

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 643
    :cond_0
    const-string v22, ""

    .line 739
    :goto_0
    return-object v22

    .line 644
    :cond_1
    const-string v22, "\""

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    const-string v22, "\""

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 646
    const/16 v22, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const-string v23, "\\n"

    const-string v24, "\n"

    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v22

    goto :goto_0

    .line 648
    :cond_2
    const-string v22, "Translate"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "formatTranslation raw="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    new-instance v11, Lorg/json/JSONObject;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 651
    .local v11, json:Lorg/json/JSONObject;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 652
    .local v18, translation:Ljava/lang/StringBuilder;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 653
    .local v15, srcTransliteration:Ljava/lang/StringBuilder;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 654
    .local v20, trgTransliteration:Ljava/lang/StringBuilder;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 655
    .local v12, spellCorrection:Ljava/lang/StringBuilder;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 659
    .local v14, srcDetectedLang:Ljava/lang/StringBuilder;
    const-string v22, "sentences"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 660
    const-string v22, "sentences"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 661
    .local v3, count:I
    const/4 v9, 0x0

    .local v9, i:I
    :goto_1
    if-ge v9, v3, :cond_3

    .line 662
    const-string v22, "sentences"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v19

    .line 664
    .local v19, translationJson:Lorg/json/JSONObject;
    const-string v22, "trans"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    const-string v22, "src_translit"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 667
    const-string v22, "translit"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 671
    .end local v3           #count:I
    .end local v9           #i:I
    .end local v19           #translationJson:Lorg/json/JSONObject;
    :cond_3
    const-string v22, "spell"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 672
    const-string v22, "spell"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 673
    .local v13, spellCorrectionJson:Lorg/json/JSONObject;
    if-eqz v13, :cond_4

    .line 674
    const-string v22, "spell_html_res"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 678
    .end local v13           #spellCorrectionJson:Lorg/json/JSONObject;
    :cond_4
    const-string v22, "ld_result"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 679
    const-string v22, "ld_result"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v14, v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->appendTranslateResultLangIdsFieldFromLandIdJson(Ljava/lang/StringBuilder;Lorg/json/JSONObject;)V

    .line 684
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 685
    const-string v22, "src"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 688
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 689
    .local v4, detailedTranslation:Ljava/lang/StringBuilder;
    const-string v22, "\t"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 691
    const-string v22, "\t"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 693
    const-string v22, "\t"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    :try_start_0
    const-string v22, "dict"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 700
    const-string v22, "dict"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 701
    .local v7, dictionaryJson:Lorg/json/JSONArray;
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    .line 702
    .local v8, dictionaryLength:I
    if-lez v8, :cond_a

    .line 703
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 704
    .local v5, dictionary:Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    .restart local v9       #i:I
    :goto_2
    if-ge v9, v8, :cond_9

    .line 705
    invoke-virtual {v7, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 707
    .local v6, dictionaryEntry:Lorg/json/JSONObject;
    const-string v22, "pos"

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 709
    .local v21, type:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_7

    .line 710
    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":\n"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 712
    :cond_7
    const-string v22, "terms"

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v16

    .line 714
    .local v16, synonyms:Lorg/json/JSONArray;
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONArray;->length()I

    move-result v17

    .line 718
    .local v17, synonymsLength:I
    const/4 v10, 0x1

    .local v10, j:I
    :goto_3
    move/from16 v0, v17

    if-gt v10, v0, :cond_8

    .line 719
    const-string v22, " "

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ". "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    add-int/lit8 v23, v10, -0x1

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\n"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 718
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 723
    :cond_8
    const-string v22, "\n"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 704
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 725
    .end local v6           #dictionaryEntry:Lorg/json/JSONObject;
    .end local v10           #j:I
    .end local v16           #synonyms:Lorg/json/JSONArray;
    .end local v17           #synonymsLength:I
    .end local v21           #type:Ljava/lang/String;
    :cond_9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 732
    .end local v5           #dictionary:Ljava/lang/StringBuilder;
    .end local v7           #dictionaryJson:Lorg/json/JSONArray;
    .end local v8           #dictionaryLength:I
    .end local v9           #i:I
    :cond_a
    :goto_4
    const-string v22, "\t"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 733
    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 734
    const-string v22, "\t"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 735
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 736
    const-string v22, "\t"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 737
    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 739
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 728
    :catch_0
    move-exception v22

    goto :goto_4
.end method

.method private static generateErrorResult(I)Ljava/lang/String;
    .locals 1
    .parameter "errorCode"

    .prologue
    .line 413
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static generateLanguageTuple(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "prefix"
    .parameter "shortName"
    .parameter "longName"

    .prologue
    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 623
    .local v0, out:Ljava/lang/StringBuilder;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getResultCode(Ljava/lang/String;)I
    .locals 7
    .parameter "result"

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v6, 0x1

    .line 417
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v3

    .line 431
    :cond_0
    :goto_0
    return v2

    .line 423
    :cond_1
    const-string v4, "\t"

    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 424
    .local v1, results:[Ljava/lang/String;
    array-length v4, v1

    if-le v4, v6, :cond_3

    aget-object v4, v1, v6

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 425
    new-instance v0, Lcom/google/android/apps/translate/Translate$TranslateResult;

    invoke-direct {v0, p0, v6}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Ljava/lang/String;Z)V

    .line 426
    .local v0, r:Lcom/google/android/apps/translate/Translate$TranslateResult;
    invoke-virtual {v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->hasDetectedSrcShorLangName()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 427
    const/4 v2, -0x4

    goto :goto_0

    :cond_2
    move v2, v3

    .line 429
    goto :goto_0

    .line 431
    .end local v0           #r:Lcom/google/android/apps/translate/Translate$TranslateResult;
    :cond_3
    array-length v3, v1

    if-ne v3, v6, :cond_0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method private static getSubArrayBykeyName(Lorg/json/JSONArray;Ljava/lang/String;Z)Lorg/json/JSONArray;
    .locals 6
    .parameter "jsonArray"
    .parameter "keyName"
    .parameter "optional"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 561
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 562
    .local v2, length:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 563
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 564
    add-int/lit8 v3, v2, -0x1

    if-lt v0, v3, :cond_0

    .line 565
    new-instance v3, Lorg/json/JSONException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SubArray for : \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\" does not exist."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 568
    :cond_0
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v3}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v3

    .line 583
    :goto_1
    return-object v3

    .line 562
    :cond_1
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 572
    :cond_2
    if-nez p2, :cond_4

    .line 573
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 574
    .local v1, keys:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_3

    .line 575
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    add-int/lit8 v0, v0, 0x2

    goto :goto_2

    .line 579
    :cond_3
    new-instance v3, Lorg/json/JSONException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot find \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\". All the key list: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 583
    .end local v1           #keys:Ljava/lang/StringBuilder;
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static languages(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .parameter "baseLanguage"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 331
    invoke-static {p0}, Lcom/google/android/apps/translate/Translate;->retrieveLanguages(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static offlineTranslate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "text"
    .parameter "from"
    .parameter "to"
    .parameter "extra"

    .prologue
    .line 794
    invoke-static {p1, p2, p0}, Lcom/google/android/apps/translate/offline/TranslateOfflineUtil;->translate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/translate/Translate;->formatOfflineOutput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static parseJsonLanguageList(Ljava/lang/String;Lorg/json/JSONArray;Ljava/util/List;)V
    .locals 4
    .parameter "param"
    .parameter "jsonArray"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/json/JSONArray;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 614
    .local p2, languages:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    .line 615
    .local v1, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 616
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3}, Lcom/google/android/apps/translate/Translate;->generateLanguageTuple(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 615
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 619
    :cond_0
    return-void
.end method

.method private static prepareTranslateUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "text"
    .parameter "from"
    .parameter "to"
    .parameter "extra"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 490
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://translate.google.com/translate_a/t?client=at&sc=1&v=2.0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 491
    .local v0, url:Ljava/lang/StringBuilder;
    const-string v1, "&sl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    const-string v1, "&tl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    const-string v1, "&hl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/Translate;->mAcceptLanguage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    const-string v1, "&ie="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 495
    const-string v1, "&oe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    const-string v1, "&text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {p0, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    if-eqz p3, :cond_0

    const-string v1, ""

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 499
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static retrieveLanguages(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .parameter "baseLanguage"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 335
    const-string v1, ""

    .line 337
    .local v1, result:Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://translate.google.com/translate_a/l?client=at&cb=1&alpha=1"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 338
    .local v2, url:Ljava/lang/StringBuilder;
    const-string v3, "&hl="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    const-string v3, "&ie="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    const-string v3, "&oe="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    const-string v3, "Translate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "url="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/translate/Translate;->fetchUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 343
    invoke-static {v1}, Lcom/google/android/apps/translate/Translate;->extractLanguages(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 344
    .end local v2           #url:Ljava/lang/StringBuilder;
    :catch_0
    move-exception v0

    .line 345
    .local v0, ex:Ljava/lang/Exception;
    const-string v3, "TranslateService"

    const-string v4, "Error during languages retrieval."

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 346
    throw v0
.end method

.method private static retrieveTranslation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 10
    .parameter "text"
    .parameter "from"
    .parameter "to"
    .parameter "extra"
    .parameter "dualMode"

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 361
    const-string v3, ""

    .line 362
    .local v3, result:Ljava/lang/String;
    new-instance v5, Lcom/google/android/apps/translate/CsiTimer;

    const-string v7, "trs"

    invoke-direct {v5, v7}, Lcom/google/android/apps/translate/CsiTimer;-><init>(Ljava/lang/String;)V

    .line 363
    .local v5, translateTimer:Lcom/google/android/apps/translate/CsiTimer;
    new-array v7, v8, [Ljava/lang/String;

    const-string v8, "t"

    aput-object v8, v7, v9

    invoke-virtual {v5, v7}, Lcom/google/android/apps/translate/CsiTimer;->begin([Ljava/lang/String;)V

    .line 366
    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/translate/Translate;->prepareTranslateUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 367
    .local v6, url:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 368
    const-string v7, "Translate"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "retrieveTranslation url="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    :cond_0
    :try_start_1
    invoke-static {v6}, Lcom/google/android/apps/translate/Translate;->fetchUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    .line 388
    if-eqz p4, :cond_1

    .line 390
    :try_start_2
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 392
    .local v2, json:Lorg/json/JSONObject;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 393
    .local v4, sentence:Lorg/json/JSONObject;
    const-string v7, "trans"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p1, p2, p0}, Lcom/google/android/apps/translate/offline/TranslateOfflineUtil;->translate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 395
    const-string v7, "src_translit"

    const-string v8, ""

    invoke-virtual {v4, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 396
    const-string v7, "translit"

    const-string v8, ""

    invoke-virtual {v4, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 397
    const-string v7, "sentences"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    invoke-virtual {v7, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 398
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    .line 401
    .end local v2           #json:Lorg/json/JSONObject;
    .end local v4           #sentence:Lorg/json/JSONObject;
    :cond_1
    invoke-static {p1, v3}, Lcom/google/android/apps/translate/Translate;->formatTranslation(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 403
    .local v1, formatedResult:Ljava/lang/String;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "t"

    aput-object v9, v7, v8

    invoke-virtual {v5, v7}, Lcom/google/android/apps/translate/CsiTimer;->end([Ljava/lang/String;)V

    .line 404
    sget-object v7, Lcom/google/android/apps/translate/Translate;->mUserAgent:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/translate/Util;->newHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/apps/translate/CsiTimer;->report(Lorg/apache/http/client/HttpClient;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    .line 408
    .end local v1           #formatedResult:Ljava/lang/String;
    .end local v6           #url:Ljava/lang/String;
    :goto_0
    return-object v1

    .line 370
    :catch_0
    move-exception v0

    .line 371
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    const-string v7, "TranslateService"

    const-string v8, "Error during input text encoding."

    invoke-static {v7, v8, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 372
    const/16 v7, -0x65

    invoke-static {v7}, Lcom/google/android/apps/translate/Translate;->generateErrorResult(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 377
    .end local v0           #ex:Ljava/io/UnsupportedEncodingException;
    .restart local v6       #url:Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 378
    .local v0, ex:Lorg/apache/http/client/ClientProtocolException;
    const-string v7, "TranslateService"

    const-string v8, "Error during translation retrieval (http error)."

    invoke-static {v7, v8, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 380
    const/16 v7, -0xc9

    invoke-static {v7}, Lcom/google/android/apps/translate/Translate;->generateErrorResult(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 381
    .end local v0           #ex:Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v0

    .line 382
    .local v0, ex:Ljava/io/IOException;
    const-string v7, "TranslateService"

    const-string v8, "Error during translation retrieval (networkerror)."

    invoke-static {v7, v8, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 384
    const/4 v7, -0x2

    invoke-static {v7}, Lcom/google/android/apps/translate/Translate;->generateErrorResult(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 406
    .end local v0           #ex:Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 407
    .local v0, ex:Lorg/json/JSONException;
    const-string v7, "TranslateService"

    const-string v8, "Error during translation retrieval."

    invoke-static {v7, v8, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 408
    const/16 v7, -0x12d

    invoke-static {v7}, Lcom/google/android/apps/translate/Translate;->generateErrorResult(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static setAcceptLanguage(Ljava/lang/String;)V
    .locals 0
    .parameter "acceptLanguage"

    .prologue
    .line 439
    sput-object p0, Lcom/google/android/apps/translate/Translate;->mAcceptLanguage:Ljava/lang/String;

    .line 440
    return-void
.end method

.method public static setUserAgent(Ljava/lang/String;)V
    .locals 0
    .parameter "userAgent"

    .prologue
    .line 435
    sput-object p0, Lcom/google/android/apps/translate/Translate;->mUserAgent:Ljava/lang/String;

    .line 436
    return-void
.end method

.method private static toString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .parameter "inputStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 751
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 754
    .local v1, outputBuilder:Ljava/lang/StringBuilder;
    if-eqz p0, :cond_0

    .line 755
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    const-string v5, "UTF-8"

    invoke-direct {v4, p0, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 757
    .local v2, reader:Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, string:Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 758
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 761
    .end local v2           #reader:Ljava/io/BufferedReader;
    .end local v3           #string:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 762
    .local v0, ex:Ljava/lang/Exception;
    const-string v4, "TranslateService"

    const-string v5, "Error reading translation stream."

    invoke-static {v4, v5, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 764
    .end local v0           #ex:Ljava/lang/Exception;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static translate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .parameter "text"
    .parameter "from"
    .parameter "to"
    .parameter "extra"
    .parameter "dualMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 319
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/translate/Translate;->retrieveTranslation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static verifyJsonArrayText(Lorg/json/JSONArray;)V
    .locals 5
    .parameter "jsonArray"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 594
    const-string v3, "sl"

    invoke-static {p0, v3, v4}, Lcom/google/android/apps/translate/Translate;->getSubArrayBykeyName(Lorg/json/JSONArray;Ljava/lang/String;Z)Lorg/json/JSONArray;

    move-result-object v1

    .line 595
    .local v1, slArray:Lorg/json/JSONArray;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_0

    .line 596
    new-instance v3, Lorg/json/JSONException;

    const-string v4, "Incorrect number of pairs of param: sl"

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 599
    :cond_0
    const-string v3, "tl"

    invoke-static {p0, v3, v4}, Lcom/google/android/apps/translate/Translate;->getSubArrayBykeyName(Lorg/json/JSONArray;Ljava/lang/String;Z)Lorg/json/JSONArray;

    move-result-object v2

    .line 600
    .local v2, tlArray:Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    .line 601
    new-instance v3, Lorg/json/JSONException;

    const-string v4, "Incorrect number of pairs of param: tl"

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 606
    :cond_1
    const-string v3, "al"

    const/4 v4, 0x1

    invoke-static {p0, v3, v4}, Lcom/google/android/apps/translate/Translate;->getSubArrayBykeyName(Lorg/json/JSONArray;Ljava/lang/String;Z)Lorg/json/JSONArray;

    move-result-object v0

    .line 607
    .local v0, alArray:Lorg/json/JSONArray;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_2

    .line 608
    new-instance v3, Lorg/json/JSONException;

    const-string v4, "Incorrect number of pairs of param: al"

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 610
    :cond_2
    return-void
.end method
