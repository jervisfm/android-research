.class Lcom/google/android/apps/translate/TranslateApplication$3;
.super Landroid/content/BroadcastReceiver;
.source "TranslateApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/TranslateApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/TranslateApplication;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/TranslateApplication;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/apps/translate/TranslateApplication$3;->this$0:Lcom/google/android/apps/translate/TranslateApplication;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 62
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/TranslateApplication$3;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v4

    .line 63
    .local v4, result:Landroid/os/Bundle;
    const-string v6, "android.speech.extra.SUPPORTED_LANGUAGES"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 65
    .local v2, languages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v2, :cond_0

    .line 66
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 67
    .local v1, lang:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "voicelang="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #lang:Ljava/lang/String;
    :cond_0
    const-string v6, "android.speech.extra.LANGUAGE_PREFERENCE"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, preference:Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 73
    invoke-static {p1, v2}, Lcom/google/android/apps/translate/Profile;->setSupportedVoiceLanguages(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 75
    :cond_1
    if-eqz v3, :cond_2

    .line 76
    invoke-static {v3}, Lcom/google/android/apps/translate/VoiceInputHelper;->getShortLanguageNameFromVoiceInputLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 78
    .local v5, shortLangName:Ljava/lang/String;
    if-eqz v5, :cond_2

    invoke-static {p1, v5}, Lcom/google/android/apps/translate/Profile;->getVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 81
    invoke-static {p1, v5, v3}, Lcom/google/android/apps/translate/Profile;->setVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v6, "zh-TW"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "zh-CN"

    invoke-static {p1, v6}, Lcom/google/android/apps/translate/Profile;->getVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 87
    const-string v6, "zh-CN"

    invoke-static {p1, v6, v3}, Lcom/google/android/apps/translate/Profile;->setVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .end local v5           #shortLangName:Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 90
    .restart local v5       #shortLangName:Ljava/lang/String;
    :cond_3
    const-string v6, "zh-CN"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "zh-TW"

    invoke-static {p1, v6}, Lcom/google/android/apps/translate/Profile;->getVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 93
    const-string v6, "zh-TW"

    invoke-static {p1, v6, v3}, Lcom/google/android/apps/translate/Profile;->setVoiceInputLanguage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
