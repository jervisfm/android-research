.class public Lcom/google/android/apps/translate/TranslateApplication;
.super Landroid/app/Application;
.source "TranslateApplication.java"

# interfaces
.implements Lcom/google/android/apps/translate/tts/DonutTtsCallback;


# instance fields
.field private mConvShowIntroMessage:Z

.field private mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

.field private mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

.field private final mOnTtsChanged:Landroid/content/BroadcastReceiver;

.field private final mOnVoiceSupportedLanguagesResult:Landroid/content/BroadcastReceiver;

.field private mPackageName:Ljava/lang/String;

.field private mPreferences:Landroid/content/SharedPreferences;

.field final mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mShowIntroMessage:Z

.field mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

.field private mTts:Lcom/google/android/apps/translate/tts/MyTts;

.field private mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 27
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 33
    iput-boolean v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mShowIntroMessage:Z

    .line 34
    iput-boolean v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mConvShowIntroMessage:Z

    .line 38
    new-instance v0, Lcom/google/android/apps/translate/TranslateApplication$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/TranslateApplication$1;-><init>(Lcom/google/android/apps/translate/TranslateApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 48
    new-instance v0, Lcom/google/android/apps/translate/TranslateApplication$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/TranslateApplication$2;-><init>(Lcom/google/android/apps/translate/TranslateApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mOnTtsChanged:Landroid/content/BroadcastReceiver;

    .line 58
    new-instance v0, Lcom/google/android/apps/translate/TranslateApplication$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/TranslateApplication$3;-><init>(Lcom/google/android/apps/translate/TranslateApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mOnVoiceSupportedLanguagesResult:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/TranslateApplication;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/translate/TranslateApplication;->reloadSupportedVoiceInputLanguages()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/TranslateApplication;)Lcom/google/android/apps/translate/tts/MyTts;
    .locals 1
    .parameter "x0"

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    return-object v0
.end method

.method private initializeInstantTranslation()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-direct {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    .line 188
    return-void
.end method

.method private initializeTranslateManager()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/apps/translate/TranslateManagerImpl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/TranslateManagerImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    invoke-interface {v0}, Lcom/google/android/apps/translate/TranslateManager;->initialize()V

    .line 178
    return-void
.end method

.method private initializeTts()V
    .locals 1

    .prologue
    .line 181
    const-string v0, "initialize TTS"

    invoke-static {v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 182
    new-instance v0, Lcom/google/android/apps/translate/tts/MyTts;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/tts/MyTts;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/tts/MyTts;->setCallback(Lcom/google/android/apps/translate/tts/DonutTtsCallback;)V

    .line 184
    return-void
.end method

.method public static isReleaseBuild(Landroid/content/Context;)Z
    .locals 1
    .parameter "context"

    .prologue
    .line 261
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isReleaseBuild(Ljava/lang/String;)Z
    .locals 1
    .parameter "pkgName"

    .prologue
    .line 251
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.apps.translate"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reloadSupportedVoiceInputLanguages()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/VoiceInputHelper;->reloadVoiceInputLanguageMap()V

    .line 173
    return-void
.end method


# virtual methods
.method public endAndReportAppCsiTimer()V
    .locals 4

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "init"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/CsiTimer;->end([Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/CsiTimer;->report(Landroid/content/Context;)V

    .line 214
    new-instance v0, Lcom/google/android/apps/translate/CsiTimer;

    const-string v1, "app"

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/CsiTimer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    .line 215
    return-void
.end method

.method public getConvShowIntroMessage()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 239
    iget-boolean v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mConvShowIntroMessage:Z

    if-eqz v1, :cond_0

    .line 241
    iput-boolean v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mConvShowIntroMessage:Z

    .line 242
    const/4 v0, 0x1

    .line 244
    :cond_0
    return v0
.end method

.method public getInstantTranslateHandler()Lcom/google/android/apps/translate/editor/InstantTranslateHandler;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    return-object v0
.end method

.method public getMyTts()Lcom/google/android/apps/translate/tts/MyTts;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    return-object v0
.end method

.method public getShowIntroMessage()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 229
    iget-boolean v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mShowIntroMessage:Z

    if-eqz v1, :cond_0

    .line 231
    iput-boolean v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mShowIntroMessage:Z

    .line 232
    const/4 v0, 0x1

    .line 234
    :cond_0
    return v0
.end method

.method public getTranslateManager()Lcom/google/android/apps/translate/TranslateManager;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    return-object v0
.end method

.method public getVoiceInputHelper()Lcom/google/android/apps/translate/VoiceInputHelper;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    return-object v0
.end method

.method public isReleaseBuild()Z
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mPackageName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .parameter "newConfig"

    .prologue
    .line 145
    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 149
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->getLanguageList(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getLanguagesFromServerAsync(Landroid/content/Context;)V

    .line 152
    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 104
    new-instance v1, Lcom/google/android/apps/translate/CsiTimer;

    const-string v2, "app"

    invoke-direct {v1, v2}, Lcom/google/android/apps/translate/CsiTimer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    .line 105
    iget-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "init"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/CsiTimer;->begin([Ljava/lang/String;)V

    .line 107
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mOnVoiceSupportedLanguagesResult:Landroid/content/BroadcastReceiver;

    invoke-static {p0, v1}, Lcom/google/android/apps/translate/VoiceInputHelper;->getSupportedLanguages(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 112
    new-instance v1, Lcom/google/android/apps/translate/VoiceInputHelper;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/VoiceInputHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/translate/TranslateApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mPackageName:Ljava/lang/String;

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild()Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/apps/translate/Logger;->setLogLevel(I)V

    .line 119
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/TranslateApplication;->initializeTranslateManager()V

    .line 120
    invoke-direct {p0}, Lcom/google/android/apps/translate/TranslateApplication;->initializeTts()V

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/translate/TranslateApplication;->initializeInstantTranslation()V

    .line 123
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mPreferences:Landroid/content/SharedPreferences;

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mPreferences:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/apps/translate/TranslateApplication;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/translate/TranslateApplication;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/translate/TranslateApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/ExternalFonts;->initialize(Landroid/content/res/AssetManager;Landroid/content/Context;)V

    .line 129
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->generateUserAgentName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translate/Translate;->setUserAgent(Ljava/lang/String;)V

    .line 132
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getLanguagesFromServerAsync(Landroid/content/Context;)V

    .line 136
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 137
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 138
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 139
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mOnTtsChanged:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/translate/TranslateApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 141
    return-void
.end method

.method public onInit(Z)V
    .locals 0
    .parameter "succeed"

    .prologue
    .line 225
    return-void
.end method

.method public onTerminate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 156
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mPreferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/apps/translate/TranslateApplication;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mOnTtsChanged:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/TranslateApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    invoke-interface {v0}, Lcom/google/android/apps/translate/TranslateManager;->deinitialize()V

    .line 162
    iput-object v2, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    if-eqz v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/tts/MyTts;->shutdown()V

    .line 167
    iput-object v2, p0, Lcom/google/android/apps/translate/TranslateApplication;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    .line 169
    :cond_1
    return-void
.end method

.method public resetAppCsiTimer()V
    .locals 2

    .prologue
    .line 219
    new-instance v0, Lcom/google/android/apps/translate/CsiTimer;

    const-string v1, "app"

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/CsiTimer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/TranslateApplication;->mCsiTimer:Lcom/google/android/apps/translate/CsiTimer;

    .line 220
    return-void
.end method
