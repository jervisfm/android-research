.class public Lcom/google/android/apps/translate/TranslateBaseActivity;
.super Landroid/app/Activity;
.source "TranslateBaseActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static afterSetContentView(Landroid/app/Activity;)V
    .locals 4
    .parameter "activity"

    .prologue
    .line 62
    invoke-static {}, Lcom/google/android/apps/translate/Util;->isHoneycombCompatible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x7

    sget v3, Lcom/google/android/apps/translate/R$layout;->translate_title:I

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFeatureInt(II)V

    .line 66
    sget v1, Lcom/google/android/apps/translate/R$id;->translate_title:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 67
    .local v0, translateTitleText:Landroid/widget/TextView;
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->getTitle(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    .end local v0           #translateTitleText:Landroid/widget/TextView;
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->setActionBarTitle(Landroid/app/Activity;Lcom/google/android/apps/translate/ActionBarSpinnerAdapter;)V

    goto :goto_0
.end method

.method public static beforeSetContentView(Landroid/app/Activity;)V
    .locals 1
    .parameter "activity"

    .prologue
    .line 52
    invoke-static {}, Lcom/google/android/apps/translate/Util;->isHoneycombCompatible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    .line 55
    :cond_0
    return-void
.end method

.method public static getTitle(Landroid/app/Activity;)Ljava/lang/String;
    .locals 1
    .parameter "activity"

    .prologue
    .line 77
    instance-of v0, p0, Lcom/google/android/apps/translate/SettingsActivity;

    if-eqz v0, :cond_0

    .line 78
    sget v0, Lcom/google/android/apps/translate/R$string;->menu_settings:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 79
    :cond_0
    instance-of v0, p0, Lcom/google/android/apps/translate/conversation/ConversationActivity;

    if-eqz v0, :cond_1

    .line 80
    sget v0, Lcom/google/android/apps/translate/R$string;->menu_conversation_mode:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_1
    instance-of v0, p0, Lcom/google/android/apps/translate/history/HistoryActivity;

    if-eqz v0, :cond_3

    move-object v0, p0

    .line 82
    check-cast v0, Lcom/google/android/apps/translate/history/HistoryActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/HistoryActivity;->isHistoryMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    sget v0, Lcom/google/android/apps/translate/R$string;->label_history:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_2
    sget v0, Lcom/google/android/apps/translate/R$string;->label_favorites:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_3
    instance-of v0, p0, Lcom/google/android/apps/translate/SmsPickerActivity;

    if-eqz v0, :cond_4

    .line 88
    sget v0, Lcom/google/android/apps/translate/R$string;->menu_translate_sms:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_4
    instance-of v0, p0, Lcom/google/android/apps/translate/HomeActivity;

    if-nez v0, :cond_5

    instance-of v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;

    if-eqz v0, :cond_6

    .line 91
    :cond_5
    sget v0, Lcom/google/android/apps/translate/R$string;->btn_translate:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_6
    const-string v0, ""

    goto :goto_0
.end method

.method public static getTitle(Landroid/app/Activity;Landroid/app/Fragment;)Ljava/lang/String;
    .locals 1
    .parameter "activity"
    .parameter "frag"

    .prologue
    .line 100
    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    .line 101
    instance-of v0, p1, Lcom/google/android/apps/translate/conversation/ConversationFragment;

    if-eqz v0, :cond_0

    .line 102
    sget v0, Lcom/google/android/apps/translate/R$string;->menu_conversation_mode:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 115
    .end local p1
    :goto_0
    return-object v0

    .line 103
    .restart local p1
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/translate/history/HistoryFragment;

    if-eqz v0, :cond_2

    .line 104
    check-cast p1, Lcom/google/android/apps/translate/history/HistoryFragment;

    .end local p1
    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/HistoryFragment;->isHistoryMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    sget v0, Lcom/google/android/apps/translate/R$string;->label_history:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 107
    :cond_1
    sget v0, Lcom/google/android/apps/translate/R$string;->label_favorites:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109
    .restart local p1
    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/translate/SmsPickerFragment;

    if-eqz v0, :cond_3

    .line 110
    sget v0, Lcom/google/android/apps/translate/R$string;->menu_translate_sms:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 111
    :cond_3
    instance-of v0, p1, Lcom/google/android/apps/translate/translation/TranslateFragment;

    if-eqz v0, :cond_4

    .line 112
    sget v0, Lcom/google/android/apps/translate/R$string;->btn_translate:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 115
    :cond_4
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->getTitle(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 120
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 126
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 122
    :pswitch_0
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->openHomeActivity(Landroid/app/Activity;)V

    .line 123
    const/4 v0, 0x1

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public setContentView(I)V
    .locals 0
    .parameter "layoutResID"

    .prologue
    .line 28
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->beforeSetContentView(Landroid/app/Activity;)V

    .line 29
    invoke-super {p0, p1}, Landroid/app/Activity;->setContentView(I)V

    .line 30
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->afterSetContentView(Landroid/app/Activity;)V

    .line 31
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 0
    .parameter "view"

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->beforeSetContentView(Landroid/app/Activity;)V

    .line 36
    invoke-super {p0, p1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    .line 37
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->afterSetContentView(Landroid/app/Activity;)V

    .line 38
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .parameter "view"
    .parameter "params"

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->beforeSetContentView(Landroid/app/Activity;)V

    .line 43
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 44
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateBaseActivity;->afterSetContentView(Landroid/app/Activity;)V

    .line 45
    return-void
.end method
