.class Lcom/google/android/apps/translate/TranslateService$1;
.super Lcom/google/android/apps/translate/ITranslate$Stub;
.source "TranslateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/TranslateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/TranslateService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/TranslateService;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/translate/TranslateService$1;->this$0:Lcom/google/android/apps/translate/TranslateService;

    invoke-direct {p0}, Lcom/google/android/apps/translate/ITranslate$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public translate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "text"
    .parameter "from"
    .parameter "to"
    .parameter "extra"

    .prologue
    .line 46
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "from="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 47
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "to="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 48
    iget-object v2, p0, Lcom/google/android/apps/translate/TranslateService$1;->this$0:Lcom/google/android/apps/translate/TranslateService;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/TranslateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 51
    .local v0, context:Landroid/content/Context;
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    const-string v2, "TranslateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Profile.getOfflineTranslate="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translate/TranslateService$1;->this$0:Lcom/google/android/apps/translate/TranslateService;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/TranslateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/translate/Profile;->getOfflineTranslate(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v2, "TranslateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isNetworkAvailable="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v2, "TranslateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ProfilesAdapter.hasLocalModelFiles="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translate/TranslateService$1;->this$0:Lcom/google/android/apps/translate/TranslateService;

    #getter for: Lcom/google/android/apps/translate/TranslateService;->localProfiles:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/apps/translate/TranslateService;->access$000(Lcom/google/android/apps/translate/TranslateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-static {v4, p2, p3}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->hasLocalModelFiles(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->isOfflineTranslateSupported(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Lcom/google/android/apps/translate/Profile;->getOfflineTranslate(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/translate/TranslateService$1;->this$0:Lcom/google/android/apps/translate/TranslateService;

    #getter for: Lcom/google/android/apps/translate/TranslateService;->localProfiles:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/apps/translate/TranslateService;->access$000(Lcom/google/android/apps/translate/TranslateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v2, p2, p3}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->hasLocalModelFiles(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 62
    const-string v2, "TranslateService"

    const-string v3, "Translate.offlineTranslate"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/apps/translate/Translate;->offlineTranslate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 75
    .end local v0           #context:Landroid/content/Context;
    :goto_0
    return-object v2

    .line 64
    .restart local v0       #context:Landroid/content/Context;
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->isOfflineTranslateSupported(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v0}, Lcom/google/android/apps/translate/Profile;->getDualMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/translate/TranslateService$1;->this$0:Lcom/google/android/apps/translate/TranslateService;

    #getter for: Lcom/google/android/apps/translate/TranslateService;->localProfiles:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/apps/translate/TranslateService;->access$000(Lcom/google/android/apps/translate/TranslateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v2, p2, p3}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->hasLocalModelFiles(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 67
    const-string v2, "TranslateService"

    const-string v3, "Translate.translate DUAL MODE"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v2, 0x1

    invoke-static {p1, p2, p3, p4, v2}, Lcom/google/android/apps/translate/Translate;->translate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 70
    :cond_3
    const-string v2, "TranslateService"

    const-string v3, "Translate.translate ONLINE"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const/4 v2, 0x0

    invoke-static {p1, p2, p3, p4, v2}, Lcom/google/android/apps/translate/Translate;->translate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 73
    .end local v0           #context:Landroid/content/Context;
    :catch_0
    move-exception v1

    .line 74
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "TranslateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to perform translation: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v2, 0x0

    goto :goto_0
.end method
