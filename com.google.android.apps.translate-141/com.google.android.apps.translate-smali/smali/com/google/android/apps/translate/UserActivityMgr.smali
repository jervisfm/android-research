.class public Lcom/google/android/apps/translate/UserActivityMgr;
.super Ljava/lang/Object;
.source "UserActivityMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;,
        Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;,
        Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;,
        Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    }
.end annotation


# static fields
.field public static final DEBUG:Z = false

.field private static final INPUT_METHOD_VAR:Ljava/lang/String; = "&inputm="

.field private static final INPUT_SOURCE_VAR:Ljava/lang/String; = "&source="

.field private static final INTERVAL_COUNT_SEPARATOR:Ljava/lang/String; = ","

.field private static final INTERVAL_COUNT_TAG_VALUE_SEPARATOR:Ljava/lang/String; = "."

.field private static final INTERVAL_COUNT_VAR:Ljava/lang/String; = "&ic="

.field private static final PREV_SOURCE_LANG_VAR:Ljava/lang/String; = "&psl="

.field private static final PREV_TARGET_LANG_VAR:Ljava/lang/String; = "&ptl="

.field private static final SWAP_VAR:Ljava/lang/String; = "&swap="

.field private static final TAG:Ljava/lang/String; = "UserActivityMgr"

.field private static final VALUE_NOT_SET:Ljava/lang/String;

.field private static sIcTagToImpressionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static sInstance:Lcom/google/android/apps/translate/UserActivityMgr;


# instance fields
.field private mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/translate/UserActivityMgr;->VALUE_NOT_SET:Ljava/lang/String;

    .line 16
    invoke-static {}, Lcom/google/android/apps/translate/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translate/UserActivityMgr;->sIcTagToImpressionMap:Ljava/util/Map;

    .line 34
    new-instance v0, Lcom/google/android/apps/translate/UserActivityMgr;

    invoke-direct {v0}, Lcom/google/android/apps/translate/UserActivityMgr;-><init>()V

    sput-object v0, Lcom/google/android/apps/translate/UserActivityMgr;->sInstance:Lcom/google/android/apps/translate/UserActivityMgr;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    new-instance v0, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    invoke-direct {v0}, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    .line 302
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr;->VALUE_NOT_SET:Ljava/lang/String;

    return-object v0
.end method

.method public static get()Lcom/google/android/apps/translate/UserActivityMgr;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr;->sInstance:Lcom/google/android/apps/translate/UserActivityMgr;

    return-object v0
.end method

.method public static setLanguageChanges(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 4
    .parameter "mFromLanguage"
    .parameter "mToLanguage"
    .parameter "language1"
    .parameter "language2"

    .prologue
    .line 306
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 307
    invoke-static {p0}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    invoke-static {p1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    invoke-static {p2}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    invoke-static {p3}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    :cond_0
    invoke-virtual {p2, p0}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p3, p1}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    :cond_1
    const/4 v0, 0x1

    .line 313
    .local v0, isChanged:Z
    :goto_0
    if-eqz v0, :cond_4

    .line 314
    invoke-virtual {p0, p2}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 315
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->SL_CHANGE:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {p0}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;Ljava/lang/String;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 318
    :cond_2
    invoke-virtual {p1, p3}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 319
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->TL_CHANGE:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;Ljava/lang/String;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 322
    :cond_3
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->UNKNOWN:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationInputMethod(Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 325
    :cond_4
    return-void

    .line 312
    .end local v0           #isChanged:Z
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setLanguageChangesForConversation(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 0
    .parameter "mFromLanguage"
    .parameter "mToLanguage"
    .parameter "language1"
    .parameter "language2"

    .prologue
    .line 332
    return-void
.end method


# virtual methods
.method public declared-synchronized getCurrTranslationExtraParams()Ljava/lang/String;
    .locals 10

    .prologue
    .line 233
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    .local v4, subUrl:Ljava/lang/StringBuilder;
    iget-object v7, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v7, v7, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->inputm:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    sget-object v8, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->UNKNOWN:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    if-eq v7, v8, :cond_0

    .line 237
    const-string v7, "&inputm="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v8, v8, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->inputm:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    invoke-virtual {v8}, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->ordinal()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 242
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v3, v7, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->source:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    .line 243
    .local v3, source:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;
    sget-object v7, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->UNKNOWN:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    if-eq v3, v7, :cond_1

    .line 244
    invoke-virtual {v3}, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->paramValue()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/translate/UserActivityMgr;->VALUE_NOT_SET:Ljava/lang/String;

    if-ne v7, v8, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v6, v7, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->sourceCgiParamData:Ljava/lang/String;

    .line 246
    .local v6, value:Ljava/lang/String;
    :goto_0
    sget-object v7, Lcom/google/android/apps/translate/UserActivityMgr;->VALUE_NOT_SET:Ljava/lang/String;

    if-eq v6, v7, :cond_1

    .line 247
    invoke-virtual {v3}, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->paramName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .end local v6           #value:Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 252
    .local v2, intervalCountValue:Ljava/lang/StringBuilder;
    iget-object v7, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v7, v7, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->intervalCountMap:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    .line 253
    .local v5, tag:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;
    iget-object v7, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v7, v7, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->intervalCountMap:Ljava/util/Map;

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 254
    .local v0, c:I
    if-eqz v0, :cond_2

    .line 255
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_3

    .line 256
    const-string v7, ","

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :cond_3
    invoke-virtual {v5}, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->tagName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string v7, "."

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 233
    .end local v0           #c:I
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #intervalCountValue:Ljava/lang/StringBuilder;
    .end local v3           #source:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;
    .end local v4           #subUrl:Ljava/lang/StringBuilder;
    .end local v5           #tag:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 244
    .restart local v3       #source:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;
    .restart local v4       #subUrl:Ljava/lang/StringBuilder;
    :cond_4
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->paramValue()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 263
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #intervalCountValue:Ljava/lang/StringBuilder;
    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_6

    .line 264
    const-string v7, "&ic="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 265
    iget-object v7, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v7, v7, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->intervalCountMap:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->clear()V

    .line 271
    :cond_6
    const-string v7, "UserActivityMgr"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getCurrTranslationExtraParams subUrl="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    monitor-exit p0

    return-object v7
.end method

.method public declared-synchronized getIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;)I
    .locals 1
    .parameter "tag"

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v0, v0, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->intervalCountMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v0, v0, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->intervalCountMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTranslationSource()Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;
    .locals 1

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v0, v0, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->source:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;
    .locals 4
    .parameter "tag"
    .parameter "inc"

    .prologue
    .line 189
    monitor-enter p0

    move v0, p2

    .line 190
    .local v0, newValue:I
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v1, v1, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->intervalCountMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v1, v1, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->intervalCountMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v1, v1, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->intervalCountMap:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    const-string v1, "UserActivityMgr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setTranslationInputMethod mTranslation.intervalCountMap[\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\']="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    monitor-exit p0

    return-object p0

    .line 189
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized prepareNewTranslation()V
    .locals 2

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    const-string v0, "UserActivityMgr"

    const-string v1, "prepareNewTranslation"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    invoke-direct {v0}, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setImpression(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;
    .locals 3
    .parameter "tag"
    .parameter "shown"

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr;->sIcTagToImpressionMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr;->sIcTagToImpressionMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 207
    .local v0, prevShown:Z
    :goto_0
    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr;->sIcTagToImpressionMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    .line 209
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 211
    :goto_1
    monitor-exit p0

    return-object v1

    .line 205
    .end local v0           #prevShown:Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .restart local v0       #prevShown:Z
    :cond_1
    move-object v1, p0

    .line 211
    goto :goto_1

    .line 205
    .end local v0           #prevShown:Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setTranslationInputMethod(Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;)Lcom/google/android/apps/translate/UserActivityMgr;
    .locals 3
    .parameter "method"

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-static {p1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iput-object p1, v0, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->inputm:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    .line 162
    const-string v0, "UserActivityMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTranslationInputMethod mTranslation.inputm="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v2, v2, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->inputm:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    monitor-exit p0

    return-object p0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;
    .locals 1
    .parameter "source"

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr;->VALUE_NOT_SET:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;Ljava/lang/String;)Lcom/google/android/apps/translate/UserActivityMgr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;Ljava/lang/String;)Lcom/google/android/apps/translate/UserActivityMgr;
    .locals 3
    .parameter "source"
    .parameter "data"

    .prologue
    .line 170
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iput-object p1, v0, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->source:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iput-object p2, v0, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->sourceCgiParamData:Ljava/lang/String;

    .line 172
    const-string v0, "UserActivityMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTranslationInputMethod mTranslation.source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v2, v2, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->source:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v0, "UserActivityMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTranslationInputMethod mTranslation.sourceCgiParamData="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/UserActivityMgr;->mTranslation:Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;

    iget-object v2, v2, Lcom/google/android/apps/translate/UserActivityMgr$TranslationActivity;->sourceCgiParamData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    monitor-exit p0

    return-object p0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
