.class public Lcom/google/android/apps/translate/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field public static final HTTP_REQUEST_TIMEOUT_MILLIS:I = 0x4e20

.field public static final LANGUAGE_PAIR_TEXT_SEPARATOR:Ljava/lang/String; = " \u00bb "

.field private static final MIN_LANGUAGE_COUNT:I = 0x32

.field private static final NETWORK_BUFFER_SIZE:I = 0x200

.field private static final TAG:Ljava/lang/String; = "Util"

.field private static final USER_AGENT:Ljava/lang/String; = "AndroidTranslate"

.field private static mCameraLoggingConfirmed:Z

.field private static sHttpClient:Lorg/apache/http/client/HttpClient;

.field private static final sSdkVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/translate/Util;->sSdkVersion:I

    .line 86
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/translate/Util;->mCameraLoggingConfirmed:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static applyStylesToTextRange(Ljava/lang/CharSequence;Ljava/lang/String;[Landroid/text/style/CharacterStyle;[Landroid/text/style/CharacterStyle;[Landroid/text/style/CharacterStyle;)Ljava/lang/CharSequence;
    .locals 12
    .parameter "text"
    .parameter "placeholder"
    .parameter "cs"
    .parameter "prevPartTextCS"
    .parameter "afterPartTextCS"

    .prologue
    .line 838
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 839
    .local v2, holderLen:I
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 842
    .local v8, textStr:Ljava/lang/String;
    invoke-virtual {v8, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    add-int v6, v9, v2

    .line 843
    .local v6, rangeStart:I
    invoke-virtual {v8, p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    .line 845
    .local v5, rangeEnd:I
    if-ltz v6, :cond_2

    if-ltz v5, :cond_2

    .line 846
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 848
    .local v1, builder:Landroid/text/SpannableStringBuilder;
    move-object v0, p2

    .local v0, arr$:[Landroid/text/style/CharacterStyle;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v7, v0, v3

    .line 849
    .local v7, s:Landroid/text/style/CharacterStyle;
    const/16 v9, 0x21

    invoke-virtual {v1, v7, v6, v5, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 848
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 853
    .end local v7           #s:Landroid/text/style/CharacterStyle;
    :cond_0
    add-int v9, v5, v2

    invoke-virtual {v1, v5, v9}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 854
    sub-int v9, v6, v2

    invoke-virtual {v1, v9, v6}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 857
    if-eqz p3, :cond_1

    sub-int v9, v6, v2

    if-lez v9, :cond_1

    .line 858
    move-object v0, p3

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v7, v0, v3

    .line 859
    .restart local v7       #s:Landroid/text/style/CharacterStyle;
    const/4 v9, 0x0

    sub-int v10, v6, v2

    const/16 v11, 0x21

    invoke-virtual {v1, v7, v9, v10, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 858
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 865
    .end local v7           #s:Landroid/text/style/CharacterStyle;
    :cond_1
    if-eqz p4, :cond_3

    add-int v9, v5, v2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v9, v10, :cond_3

    .line 866
    move-object/from16 v0, p4

    array-length v4, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_3

    aget-object v7, v0, v3

    .line 867
    .restart local v7       #s:Landroid/text/style/CharacterStyle;
    sub-int v9, v5, v2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    mul-int/lit8 v11, v2, 0x2

    sub-int/2addr v10, v11

    const/16 v11, 0x21

    invoke-virtual {v1, v7, v9, v10, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 866
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0           #arr$:[Landroid/text/style/CharacterStyle;
    .end local v1           #builder:Landroid/text/SpannableStringBuilder;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v7           #s:Landroid/text/style/CharacterStyle;
    :cond_2
    move-object v1, p0

    .line 878
    :cond_3
    return-object v1
.end method

.method public static copyToClipBoard(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V
    .locals 3
    .parameter "activity"
    .parameter "text"
    .parameter "chipPart"

    .prologue
    .line 1140
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->copyToClipBoard(Landroid/content/Context;Ljava/lang/String;)V

    .line 1141
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->INPUT_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_SRC_COPY_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    :goto_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 1144
    return-void

    .line 1141
    :cond_0
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_TRG_COPY_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    goto :goto_0
.end method

.method private static detectAndSetFonts(Landroid/text/Spannable;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;)V
    .locals 11
    .parameter "text"
    .parameter "langs"
    .parameter "type"

    .prologue
    .line 739
    const/4 v3, 0x0

    .line 740
    .local v3, langsToCheck:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/google/android/apps/translate/Language;>;"
    if-eqz p1, :cond_1

    array-length v8, p1

    if-lez v8, :cond_1

    .line 741
    new-instance v3, Ljava/util/LinkedList;

    .end local v3           #langsToCheck:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/google/android/apps/translate/Language;>;"
    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 742
    .restart local v3       #langsToCheck:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/google/android/apps/translate/Language;>;"
    move-object v1, p1

    .local v1, arr$:[Lcom/google/android/apps/translate/Language;
    array-length v5, v1

    .local v5, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v4, v1, v2

    .line 743
    .local v4, language:Lcom/google/android/apps/translate/Language;
    invoke-static {v4}, Lcom/google/android/apps/translate/ExternalFonts;->isUsingExternalFont(Lcom/google/android/apps/translate/Language;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 745
    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 742
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 750
    .end local v1           #arr$:[Lcom/google/android/apps/translate/Language;
    .end local v2           #i$:I
    .end local v4           #language:Lcom/google/android/apps/translate/Language;
    .end local v5           #len$:I
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 752
    :cond_2
    const/4 v8, 0x0

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v9

    const-class v10, Lcom/google/android/apps/translate/ExtTypefaceSpan;

    invoke-interface {p0, v8, v9, v10}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/android/apps/translate/ExtTypefaceSpan;

    .line 753
    .local v7, spans:[Lcom/google/android/apps/translate/ExtTypefaceSpan;
    if-eqz v7, :cond_4

    .line 754
    move-object v1, v7

    .local v1, arr$:[Lcom/google/android/apps/translate/ExtTypefaceSpan;
    array-length v5, v1

    .restart local v5       #len$:I
    const/4 v2, 0x0

    .restart local v2       #i$:I
    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v6, v1, v2

    .local v6, span:Lcom/google/android/apps/translate/ExtTypefaceSpan;
    invoke-interface {p0, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 759
    .end local v1           #arr$:[Lcom/google/android/apps/translate/ExtTypefaceSpan;
    .end local v2           #i$:I
    .end local v5           #len$:I
    .end local v6           #span:Lcom/google/android/apps/translate/ExtTypefaceSpan;
    .end local v7           #spans:[Lcom/google/android/apps/translate/ExtTypefaceSpan;
    :cond_3
    new-instance v0, Lcom/google/android/apps/translate/Util$3;

    invoke-direct {v0, p2}, Lcom/google/android/apps/translate/Util$3;-><init>(Lcom/google/android/apps/translate/Constants$AppearanceType;)V

    .line 805
    .local v0, applier:Lcom/google/android/apps/translate/LanguageDetector$LanguageSpanApplier;
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/translate/Language;

    .line 806
    .restart local v4       #language:Lcom/google/android/apps/translate/Language;
    invoke-virtual {v0, p0, v4}, Lcom/google/android/apps/translate/LanguageDetector$LanguageSpanApplier;->applyLanguageSpans(Landroid/text/Spannable;Lcom/google/android/apps/translate/Language;)V

    goto :goto_2

    .line 808
    .end local v0           #applier:Lcom/google/android/apps/translate/LanguageDetector$LanguageSpanApplier;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #language:Lcom/google/android/apps/translate/Language;
    :cond_4
    return-void
.end method

.method public static detectAndSetFonts(Landroid/widget/TextView;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;)V
    .locals 4
    .parameter "view"
    .parameter "langs"
    .parameter "type"

    .prologue
    .line 708
    const/4 v2, 0x0

    .line 710
    .local v2, text:Landroid/text/Spannable;
    :try_start_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/text/Spannable;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 719
    invoke-static {v2, p1, p2}, Lcom/google/android/apps/translate/Util;->detectAndSetFonts(Landroid/text/Spannable;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;)V

    .line 720
    :goto_0
    return-void

    .line 711
    :catch_0
    move-exception v1

    .line 715
    .local v1, e:Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public static determineSupportedLanguages(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .parameter "baseLanguage"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 986
    const/4 v1, 0x0

    .line 989
    .local v1, languageList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/translate/Translate;->languages(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 994
    :goto_0
    return-object v1

    .line 990
    :catch_0
    move-exception v0

    .line 991
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "Util"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not initialize Languages: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static filterMatches(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .parameter "prefixFilter"
    .parameter "text"

    .prologue
    .line 659
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 660
    .local v1, pos:I
    packed-switch v1, :pswitch_data_0

    .line 666
    invoke-static {}, Ljava/text/BreakIterator;->getWordInstance()Ljava/text/BreakIterator;

    move-result-object v0

    .line 667
    .local v0, bi:Ljava/text/BreakIterator;
    invoke-virtual {v0, p1}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 668
    invoke-virtual {v0, v1}, Ljava/text/BreakIterator;->isBoundary(I)Z

    move-result v2

    .end local v0           #bi:Ljava/text/BreakIterator;
    :goto_0
    return v2

    .line 662
    :pswitch_0
    const/4 v2, 0x0

    goto :goto_0

    .line 664
    :pswitch_1
    const/4 v2, 0x1

    goto :goto_0

    .line 660
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static findLanguagePosition(Ljava/util/List;Lcom/google/android/apps/translate/Language;)I
    .locals 3
    .parameter
    .parameter "lang"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;",
            "Lcom/google/android/apps/translate/Language;",
            ")I"
        }
    .end annotation

    .prologue
    .line 214
    .local p0, languageList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 215
    .local v1, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 216
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    .end local v0           #i:I
    :goto_1
    return v0

    .line 215
    .restart local v0       #i:I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static formatTimeStampString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 1
    .parameter "context"
    .parameter "when"

    .prologue
    .line 886
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/translate/Util;->formatTimeStampString(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatTimeStampString(Landroid/content/Context;JZ)Ljava/lang/String;
    .locals 5
    .parameter "context"
    .parameter "when"
    .parameter "fullFormat"

    .prologue
    .line 894
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 895
    .local v2, then:Landroid/text/format/Time;
    invoke-virtual {v2, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 896
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 897
    .local v1, now:Landroid/text/format/Time;
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 900
    const v0, 0x80b00

    .line 905
    .local v0, format_flags:I
    iget v3, v2, Landroid/text/format/Time;->year:I

    iget v4, v1, Landroid/text/format/Time;->year:I

    if-eq v3, v4, :cond_1

    .line 906
    or-int/lit8 v0, v0, 0x14

    .line 918
    :goto_0
    if-eqz p3, :cond_0

    .line 919
    or-int/lit8 v0, v0, 0x11

    .line 922
    :cond_0
    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 907
    :cond_1
    iget v3, v2, Landroid/text/format/Time;->yearDay:I

    iget v4, v1, Landroid/text/format/Time;->yearDay:I

    if-eq v3, v4, :cond_2

    .line 909
    or-int/lit8 v0, v0, 0x10

    goto :goto_0

    .line 912
    :cond_2
    or-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static generateAlphaLanguagesFromList(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1034
    .local p0, languages:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "al"

    invoke-static {p0, v1}, Lcom/google/android/apps/translate/Util;->generateLanguagesFromList(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1036
    .local v0, alphaLanguages:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    const-string v1, "Util"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " alpha-languages supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    return-object v0
.end method

.method public static generateFromLanguagesFromList(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1005
    .local p0, languages:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "sl"

    invoke-static {p0, v1}, Lcom/google/android/apps/translate/Util;->generateLanguagesFromList(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1007
    .local v0, fromLanguages:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    const-string v1, "Util"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from-languages supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    return-object v0
.end method

.method public static generateLanguagePairText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "from"
    .parameter "to"

    .prologue
    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \u00bb "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static generateLanguagesFromList(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .parameter
    .parameter "tag"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1043
    .local p0, languageList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v3

    .line 1045
    .local v3, languages:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1046
    .local v0, each:Ljava/lang/String;
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1047
    .local v2, item:[Ljava/lang/String;
    array-length v4, v2

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 1049
    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1050
    new-instance v4, Lcom/google/android/apps/translate/Language;

    const/4 v5, 0x1

    aget-object v5, v2, v5

    const/4 v6, 0x2

    aget-object v6, v2, v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/translate/Language;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1054
    .end local v0           #each:Ljava/lang/String;
    .end local v2           #item:[Ljava/lang/String;
    :cond_1
    return-object v3
.end method

.method public static generateLongPairText(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)Ljava/lang/String;
    .locals 2
    .parameter "from"
    .parameter "to"

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Util;->generateLanguagePairText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static generateToLanguagesFromList(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/Language;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1020
    .local p0, languages:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "tl"

    invoke-static {p0, v1}, Lcom/google/android/apps/translate/Util;->generateLanguagesFromList(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1022
    .local v0, toLanguages:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/Language;>;"
    const-string v1, "Util"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to-languages supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    return-object v0
.end method

.method public static generateUserAgentName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .parameter "context"

    .prologue
    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AndroidTranslate/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHttp(Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 2
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 629
    invoke-static {}, Lcom/google/android/apps/translate/Util;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, p0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 2

    .prologue
    .line 566
    const-class v1, Lcom/google/android/apps/translate/Util;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/translate/Util;->sHttpClient:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 567
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->newHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translate/Util;->sHttpClient:Lorg/apache/http/client/HttpClient;

    .line 569
    :cond_0
    sget-object v0, Lcom/google/android/apps/translate/Util;->sHttpClient:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 566
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getInputSuggestions(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 17
    .parameter "context"
    .parameter "fromLang"
    .parameter "toLang"
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    const/16 v15, 0x14

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v15, v1}, Lcom/google/android/apps/translate/history/FavoriteDb;->getAllByATime(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 462
    .local v7, favoriteEntries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    const/16 v15, 0x14

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v15, v1}, Lcom/google/android/apps/translate/history/HistoryDb;->getAllByATime(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 466
    .local v8, historyEntries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_1

    const/4 v13, 0x1

    .line 467
    .local v13, matchFromLang:Z
    :goto_0
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 468
    .local v5, entryKeys:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 469
    .local v3, entries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    const/4 v15, 0x2

    new-array v6, v15, [Ljava/util/List;

    const/4 v15, 0x0

    aput-object v7, v6, v15

    const/4 v15, 0x1

    aput-object v8, v6, v15

    .line 471
    .local v6, entrySets:[Ljava/util/List;
    move-object v2, v6

    .local v2, arr$:[Ljava/util/List;
    array-length v12, v2

    .local v12, len$:I
    const/4 v9, 0x0

    .local v9, i$:I
    move v10, v9

    .end local v9           #i$:I
    .local v10, i$:I
    :goto_1
    if-ge v10, v12, :cond_5

    aget-object v14, v2, v10

    .line 472
    .local v14, set:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    if-eqz v14, :cond_0

    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 471
    .end local v10           #i$:I
    :cond_0
    add-int/lit8 v9, v10, 0x1

    .restart local v9       #i$:I
    move v10, v9

    .end local v9           #i$:I
    .restart local v10       #i$:I
    goto :goto_1

    .line 466
    .end local v2           #arr$:[Ljava/util/List;
    .end local v3           #entries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    .end local v5           #entryKeys:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v6           #entrySets:[Ljava/util/List;
    .end local v10           #i$:I
    .end local v12           #len$:I
    .end local v13           #matchFromLang:Z
    .end local v14           #set:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :cond_1
    const/4 v13, 0x0

    goto :goto_0

    .line 475
    .restart local v2       #arr$:[Ljava/util/List;
    .restart local v3       #entries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    .restart local v5       #entryKeys:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v6       #entrySets:[Ljava/util/List;
    .restart local v10       #i$:I
    .restart local v12       #len$:I
    .restart local v13       #matchFromLang:Z
    .restart local v14       #set:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :cond_2
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .end local v10           #i$:I
    .local v9, i$:Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/translate/history/Entry;

    .line 476
    .local v4, entry:Lcom/google/android/apps/translate/history/Entry;
    invoke-virtual {v4}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v11

    .line 477
    .local v11, key:Ljava/lang/String;
    invoke-virtual {v5, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_3

    if-eqz v13, :cond_4

    invoke-virtual {v4}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguageShortName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 479
    :cond_4
    invoke-virtual {v5, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 480
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 484
    .end local v4           #entry:Lcom/google/android/apps/translate/history/Entry;
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v11           #key:Ljava/lang/String;
    .end local v14           #set:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    .restart local v10       #i$:I
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x14

    move/from16 v0, v16

    if-gt v15, v0, :cond_6

    .end local v3           #entries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :goto_3
    return-object v3

    .restart local v3       #entries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :cond_6
    const/4 v15, 0x0

    const/16 v16, 0x14

    move/from16 v0, v16

    invoke-interface {v3, v15, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    goto :goto_3
.end method

.method public static getLanguageListFromProfile(Landroid/content/Context;)Lcom/google/android/apps/translate/Languages;
    .locals 4
    .parameter "context"

    .prologue
    .line 673
    invoke-static {p0}, Lcom/google/android/apps/translate/Profile;->getLanguageList(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 675
    .local v0, languageList:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 676
    new-instance v1, Lcom/google/android/apps/translate/Languages;

    invoke-direct {v1, v0}, Lcom/google/android/apps/translate/Languages;-><init>(Ljava/lang/String;)V

    .line 678
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/translate/Languages;

    invoke-static {}, Lcom/google/android/apps/translate/Languages;->getDefaultFromLanguages()Ljava/util/List;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/translate/Languages;->getDefaultToLanguages()Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/translate/Languages;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method public static getLanguageShortNameByLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->getLanguageShortNameByLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLanguageShortNameByLocale(Ljava/util/Locale;)Ljava/lang/String;
    .locals 2
    .parameter "locale"

    .prologue
    .line 436
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "locale: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 437
    sget-object v0, Ljava/util/Locale;->TAIWAN:Ljava/util/Locale;

    invoke-virtual {v0, p0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    const-string v0, "locale is TAIWAN"

    invoke-static {v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 439
    const-string v0, "zh-TW"

    .line 444
    :goto_0
    return-object v0

    .line 440
    :cond_0
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v0, p0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 441
    const-string v0, "locale is CHINA"

    invoke-static {v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 442
    const-string v0, "zh-CN"

    goto :goto_0

    .line 444
    :cond_1
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getLanguagesFromServer(Landroid/content/Context;)Z
    .locals 1
    .parameter "context"

    .prologue
    .line 930
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Util;->getLanguagesFromServer(Landroid/content/Context;Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public static getLanguagesFromServer(Landroid/content/Context;Ljava/util/Locale;)Z
    .locals 5
    .parameter "context"
    .parameter "locale"

    .prologue
    .line 957
    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->getLanguageShortNameByLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/translate/Util;->determineSupportedLanguages(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 959
    .local v0, languageList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 960
    new-instance v2, Lcom/google/android/apps/translate/Languages;

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->generateFromLanguagesFromList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->generateToLanguagesFromList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/translate/Languages;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 965
    .local v2, newList:Lcom/google/android/apps/translate/Languages;
    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->isNewLanguageListQualified(Lcom/google/android/apps/translate/Languages;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 966
    invoke-static {v0}, Lcom/google/android/apps/translate/Languages;->setAlphaLanguages(Ljava/util/List;)V

    .line 968
    invoke-virtual {v2}, Lcom/google/android/apps/translate/Languages;->dumpLanguages()Ljava/lang/String;

    move-result-object v1

    .line 969
    .local v1, newDump:Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/google/android/apps/translate/Profile;->getLanguageList(Landroid/content/Context;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 970
    invoke-static {p0, v1, p1}, Lcom/google/android/apps/translate/Profile;->setLanguageList(Landroid/content/Context;Ljava/lang/String;Ljava/util/Locale;)V

    .line 971
    const/4 v3, 0x1

    .line 975
    .end local v1           #newDump:Ljava/lang/String;
    .end local v2           #newList:Lcom/google/android/apps/translate/Languages;
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getLanguagesFromServerAsync(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 939
    new-instance v0, Lcom/google/android/apps/translate/Util$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/Util$4;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Util$4;->start()V

    .line 948
    return-void
.end method

.method public static getLocalizedStringId(Landroid/content/Context;ILcom/google/android/apps/translate/Language;)I
    .locals 8
    .parameter "context"
    .parameter "baseId"
    .parameter "language"

    .prologue
    const/16 v6, 0x5f

    const/4 v7, 0x0

    .line 1074
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1075
    .local v3, resources:Landroid/content/res/Resources;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1076
    .local v2, prefix:Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2d

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 1078
    .local v1, languagePostfix:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v7, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1079
    .local v0, id:I
    if-nez v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-le v4, v5, :cond_0

    .line 1081
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v7, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1083
    :cond_0
    if-nez v0, :cond_1

    .end local p1
    :goto_0
    return p1

    .restart local p1
    :cond_1
    move p1, v0

    goto :goto_0
.end method

.method public static getResponseInputStream(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    .locals 5
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 515
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 516
    .local v1, entity:Lorg/apache/http/HttpEntity;
    if-eqz v1, :cond_1

    .line 517
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 518
    .local v2, input:Ljava/io/InputStream;
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    .line 519
    .local v0, encodingHeader:Lorg/apache/http/Header;
    if-eqz v0, :cond_0

    const-string v3, "gzip"

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 520
    new-instance v3, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v3, v2}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v2, v3

    .line 524
    .end local v0           #encodingHeader:Lorg/apache/http/Header;
    .end local v2           #input:Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getSdkVersion()I
    .locals 1

    .prologue
    .line 650
    sget v0, Lcom/google/android/apps/translate/Util;->sSdkVersion:I

    return v0
.end method

.method public static getVersionCode(Landroid/content/Context;)I
    .locals 5
    .parameter "context"

    .prologue
    const/4 v1, 0x0

    .line 637
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v1, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 642
    :goto_0
    return v1

    .line 640
    :catch_0
    move-exception v0

    .line 641
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "Util"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package not found: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getVersionName(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .parameter "context"

    .prologue
    .line 320
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :goto_0
    return-object v1

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    const-string v1, "Util"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Package not found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :cond_0
    const-string v1, "0.0.0"

    goto :goto_0
.end method

.method public static hasSmsSupport(Landroid/content/Context;)Z
    .locals 1
    .parameter "context"

    .prologue
    .line 273
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->hasFeatureTelephony(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static hasSpeechRecognizer(Landroid/content/Context;)Z
    .locals 4
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    .line 225
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static hideSoftKeyboard(Landroid/app/Activity;Landroid/os/IBinder;)V
    .locals 2
    .parameter "activity"
    .parameter "windowToken"

    .prologue
    .line 354
    const-string v0, "Util"

    const-string v1, "hideSoftInputFromWindow"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 357
    return-void
.end method

.method public static isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z
    .locals 1
    .parameter "language"

    .prologue
    .line 492
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isAutoDetectLanguage(Ljava/lang/String;)Z
    .locals 1
    .parameter "shortName"

    .prologue
    .line 499
    const-string v0, "auto"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isFloatingActivity(Landroid/app/Activity;)Z
    .locals 4
    .parameter "activity"

    .prologue
    const/4 v1, 0x0

    .line 1150
    if-nez p0, :cond_1

    .line 1158
    :cond_0
    :goto_0
    return v1

    .line 1157
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1158
    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isHoneycombCompatible()Z
    .locals 2

    .prologue
    .line 235
    invoke-static {}, Lcom/google/android/apps/translate/Util;->getSdkVersion()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNetworkAvailable(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    .line 1169
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1171
    .local v1, connectivityManager:Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1172
    .local v0, activeNetworkInfo:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isNewLanguageListQualified(Lcom/google/android/apps/translate/Languages;)Z
    .locals 2
    .parameter "newList"

    .prologue
    const/16 v1, 0x32

    .line 1058
    invoke-virtual {p0}, Lcom/google/android/apps/translate/Languages;->getSupportedFromLanguages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/translate/Languages;->getSupportedToLanguages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1060
    const/4 v0, 0x1

    .line 1062
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSelectLanguage(Ljava/lang/String;)Z
    .locals 1
    .parameter "shortName"

    .prologue
    .line 507
    const-string v0, "select"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isStarredTranslation(Landroid/app/Activity;Lcom/google/android/apps/translate/history/Entry;)Z
    .locals 1
    .parameter "activity"
    .parameter "translationEntry"

    .prologue
    .line 347
    invoke-static {p0, p1}, Lcom/google/android/apps/translate/history/FavoriteDb;->contains(Landroid/content/Context;Lcom/google/android/apps/translate/history/Entry;)Z

    move-result v0

    return v0
.end method

.method public static languageShortNameToLocale(Ljava/lang/String;)Ljava/util/Locale;
    .locals 4
    .parameter "shortName"

    .prologue
    .line 417
    const/16 v1, 0x2d

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 418
    .local v0, pos:I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/Locale;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static newHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;
    .locals 6
    .parameter "userAgent"

    .prologue
    .line 580
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 581
    .local v0, client:Lorg/apache/http/client/HttpClient;
    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    .line 584
    .local v2, params:Lorg/apache/http/params/HttpParams;
    const/16 v3, 0x4e20

    invoke-static {v2, v3}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 587
    const-string v3, "http.connection-manager.factory-object"

    new-instance v4, Lcom/google/android/apps/translate/Util$2;

    invoke-direct {v4}, Lcom/google/android/apps/translate/Util$2;-><init>()V

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 596
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 597
    .local v1, defaultHeaders:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Accept-Charset"

    const-string v5, "UTF-8"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Accept-Encoding"

    const-string v5, "gzip"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    const-string v3, "http.default-headers"

    invoke-interface {v2, v3, v1}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 602
    invoke-static {v0, p0}, Lcom/google/android/apps/translate/Util;->setUserAgentToHttpClient(Lorg/apache/http/client/HttpClient;Ljava/lang/String;)V

    .line 603
    return-object v0
.end method

.method public static openConversationActivity(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 3
    .parameter "activity"
    .parameter "translateEntry"

    .prologue
    .line 256
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/translate/conversation/ConversationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 257
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasInputText()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    const-string v1, "key_current_translation"

    iget-object v2, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v1, :cond_1

    .line 261
    const-string v1, "key_language_from"

    iget-object v2, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 263
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v1, :cond_2

    .line 264
    const-string v1, "key_language_to"

    iget-object v2, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 266
    :cond_2
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 267
    return-void
.end method

.method public static openFavoriteActivity(Landroid/app/Activity;)V
    .locals 3
    .parameter "activity"

    .prologue
    .line 309
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/translate/history/HistoryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 310
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "history"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 311
    const-string v1, "flush_on_pause"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 312
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 313
    return-void
.end method

.method public static openHistoryActivity(Landroid/app/Activity;)V
    .locals 3
    .parameter "activity"

    .prologue
    const/4 v2, 0x1

    .line 242
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/translate/history/HistoryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "history"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 244
    const-string v1, "flush_on_pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 245
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 246
    return-void
.end method

.method public static openHomeActivity(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 397
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/translate/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 398
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 399
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 400
    return-void
.end method

.method public static openSmsTranslationActivity(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 280
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/translate/SmsPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xaa

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 282
    return-void
.end method

.method public static openSupersizeTextActivity(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V
    .locals 5
    .parameter "activity"
    .parameter "translateEntry"
    .parameter "triggeredByGesture"

    .prologue
    .line 289
    if-nez p1, :cond_0

    .line 303
    :goto_0
    return-void

    .line 292
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/translate/SupersizeTextActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 293
    .local v0, intent:Landroid/content/Intent;
    new-instance v1, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v2, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Ljava/lang/String;)V

    .line 295
    .local v1, results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    const-string v2, "key_translate_text"

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    const-string v2, "key_text_input"

    iget-object v3, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    const-string v2, "key_language_to"

    iget-object v3, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 298
    const-string v2, "key_language_from"

    iget-object v3, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 299
    const-string v2, "key_supersize_by_gesture"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 300
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 301
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_TRG_SUPERSIZE_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    goto :goto_0
.end method

.method public static openTranslateActivity(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 2
    .parameter "activity"
    .parameter "intent"

    .prologue
    .line 380
    const-class v0, Lcom/google/android/apps/translate/translation/TranslateActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 383
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/translate/HomeActivity;

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->setNoTransitionAnimation(Landroid/content/Intent;)V

    .line 385
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 386
    return-void
.end method

.method public static openTranslateActivity(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "activity"
    .parameter "inputText"
    .parameter "outputText"
    .parameter "from"
    .parameter "to"

    .prologue
    .line 364
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/translate/translation/TranslateActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 365
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "key_text_input"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    const-string v1, "key_text_output"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 367
    const-string v1, "key_language_from"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 368
    const-string v1, "key_language_to"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/translate/HomeActivity;

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->setNoTransitionAnimation(Landroid/content/Intent;)V

    .line 373
    :cond_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 374
    return-void
.end method

.method public static readStringFromHttpResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 7
    .parameter "response"

    .prologue
    const/4 v5, 0x0

    .line 531
    const/4 v2, 0x0

    .line 533
    .local v2, input:Ljava/io/InputStream;
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getResponseInputStream(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v2

    .line 534
    if-eqz v2, :cond_3

    .line 535
    new-instance v3, Ljava/io/InputStreamReader;

    const-string v6, "UTF-8"

    invoke-direct {v3, v2, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 536
    .local v3, reader:Ljava/io/InputStreamReader;
    const/16 v6, 0x200

    new-array v0, v6, [C

    .line 537
    .local v0, buf:[C
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 539
    .local v1, builder:Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStreamReader;->read([C)I

    move-result v4

    .local v4, size:I
    const/4 v6, -0x1

    if-le v4, v6, :cond_1

    .line 540
    const/4 v6, 0x0

    invoke-virtual {v1, v0, v6, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 544
    .end local v0           #buf:[C
    .end local v1           #builder:Ljava/lang/StringBuilder;
    .end local v3           #reader:Ljava/io/InputStreamReader;
    .end local v4           #size:I
    :catch_0
    move-exception v6

    .line 549
    if-eqz v2, :cond_0

    .line 551
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 557
    :cond_0
    :goto_1
    return-object v5

    .line 542
    .restart local v0       #buf:[C
    .restart local v1       #builder:Ljava/lang/StringBuilder;
    .restart local v3       #reader:Ljava/io/InputStreamReader;
    .restart local v4       #size:I
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v5

    .line 549
    :cond_2
    if-eqz v2, :cond_0

    .line 551
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 552
    :catch_1
    move-exception v6

    goto :goto_1

    .line 549
    .end local v0           #buf:[C
    .end local v1           #builder:Ljava/lang/StringBuilder;
    .end local v3           #reader:Ljava/io/InputStreamReader;
    .end local v4           #size:I
    :cond_3
    if-eqz v2, :cond_0

    .line 551
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 552
    :catch_2
    move-exception v6

    goto :goto_1

    .line 546
    :catch_3
    move-exception v6

    .line 549
    if-eqz v2, :cond_0

    .line 551
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 552
    :catch_4
    move-exception v6

    goto :goto_1

    .line 549
    :catchall_0
    move-exception v5

    if-eqz v2, :cond_4

    .line 551
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 554
    :cond_4
    :goto_2
    throw v5

    .line 552
    :catch_5
    move-exception v6

    goto :goto_1

    :catch_6
    move-exception v6

    goto :goto_2
.end method

.method public static searchTextOnWeb(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V
    .locals 5
    .parameter "activity"
    .parameter "queryText"
    .parameter "chipPart"

    .prologue
    .line 1117
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1118
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1119
    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1121
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1122
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v3

    sget-object v2, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->INPUT_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    if-ne p2, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_SRC_SEARCH_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    :goto_0
    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 1129
    :goto_1
    return-void

    .line 1122
    :cond_0
    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_TRG_SEARCH_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1125
    :catch_0
    move-exception v0

    .line 1126
    .local v0, e:Landroid/content/ActivityNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/google/android/apps/translate/R$string;->msg_error_intent_web_search:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;)V

    .line 1127
    sget v2, Lcom/google/android/apps/translate/R$string;->msg_error_intent_web_search:I

    invoke-static {p0, v2}, Lcom/google/android/apps/translate/Util;->showLongToastMessage(Landroid/app/Activity;I)V

    goto :goto_1
.end method

.method private static setNoTransitionAnimation(Landroid/content/Intent;)V
    .locals 2
    .parameter "intent"

    .prologue
    .line 389
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->setIntentNoAnimation(Landroid/content/Intent;)V

    .line 390
    const-string v0, "key_no_transition_animation"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 391
    return-void
.end method

.method public static setTextAndFont(Landroid/widget/TextView;Ljava/lang/String;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;Z)V
    .locals 2
    .parameter "view"
    .parameter "text"
    .parameter "langs"
    .parameter "type"
    .parameter "parseHtml"

    .prologue
    .line 693
    if-eqz p4, :cond_0

    new-instance v0, Landroid/text/SpannableString;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 695
    .local v0, ss:Landroid/text/SpannableString;
    :goto_0
    invoke-static {v0, p2, p3}, Lcom/google/android/apps/translate/Util;->detectAndSetFonts(Landroid/text/Spannable;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;)V

    .line 696
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 697
    return-void

    .line 693
    .end local v0           #ss:Landroid/text/SpannableString;
    :cond_0
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static setUserAgentToHttpClient(Lorg/apache/http/client/HttpClient;Ljava/lang/String;)V
    .locals 3
    .parameter "client"
    .parameter "userAgent"

    .prologue
    .line 613
    invoke-interface {p0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 616
    .local v0, params:Lorg/apache/http/params/HttpParams;
    if-nez p1, :cond_0

    .line 617
    invoke-static {v0}, Lorg/apache/http/params/HttpProtocolParams;->getUserAgent(Lorg/apache/http/params/HttpParams;)Ljava/lang/String;

    move-result-object p1

    .line 619
    :cond_0
    const-string v1, "gzip"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 620
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (gzip)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 622
    :cond_1
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 623
    return-void
.end method

.method public static showLongToastMessage(Landroid/app/Activity;I)V
    .locals 1
    .parameter "activity"
    .parameter "resId"

    .prologue
    .line 158
    invoke-virtual {p0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Util;->showLongToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    .line 159
    return-void
.end method

.method public static showLongToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V
    .locals 1
    .parameter "activity"
    .parameter "message"

    .prologue
    .line 115
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/translate/Util;->showToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;I)V

    .line 116
    return-void
.end method

.method public static showNetworkTtsError(Landroid/app/Activity;I)V
    .locals 1
    .parameter "activity"
    .parameter "error"

    .prologue
    .line 1093
    packed-switch p1, :pswitch_data_0

    .line 1105
    :goto_0
    return-void

    .line 1095
    :pswitch_0
    sget v0, Lcom/google/android/apps/translate/R$string;->msg_network_tts_error:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Util;->showLongToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1100
    :pswitch_1
    sget v0, Lcom/google/android/apps/translate/R$string;->msg_network_tts_too_long:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Util;->showLongToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1093
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static showShortToastMessage(Landroid/app/Activity;I)V
    .locals 1
    .parameter "activity"
    .parameter "resId"

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    .line 106
    return-void
.end method

.method public static showShortToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V
    .locals 1
    .parameter "activity"
    .parameter "message"

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/translate/Util;->showToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;I)V

    .line 96
    return-void
.end method

.method private static showToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;I)V
    .locals 1
    .parameter "activity"
    .parameter "message"
    .parameter "duration"

    .prologue
    .line 163
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    :goto_0
    return-void

    .line 166
    :cond_0
    new-instance v0, Lcom/google/android/apps/translate/Util$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/translate/Util$1;-><init>(Landroid/app/Activity;Ljava/lang/CharSequence;I)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static showTranslationErrorToastMessage(Landroid/app/Activity;I)V
    .locals 2
    .parameter "activity"
    .parameter "errorCode"

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v0, msg:Ljava/lang/StringBuilder;
    sparse-switch p1, :sswitch_data_0

    .line 143
    sget v1, Lcom/google/android/apps/translate/R$string;->msg_translation_error:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const-string v1, " E"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 148
    :goto_0
    invoke-static {p0, v0}, Lcom/google/android/apps/translate/Util;->showLongToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    .line 149
    :sswitch_0
    return-void

    .line 132
    :sswitch_1
    sget v1, Lcom/google/android/apps/translate/R$string;->msg_network_error:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 135
    :sswitch_2
    sget v1, Lcom/google/android/apps/translate/R$string;->msg_network_error:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    const-string v1, " E"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 126
    nop

    :sswitch_data_0
    .sparse-switch
        -0xc9 -> :sswitch_2
        -0x2 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method public static splitTranslateResult(Lcom/google/android/apps/translate/history/Entry;)[Ljava/lang/String;
    .locals 3
    .parameter "entry"

    .prologue
    .line 193
    if-nez p0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/history/Entry;->getOutputText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->splitTranslateResult(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static splitTranslateResult(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .parameter "resultText"

    .prologue
    .line 202
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "\t"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static toggleStarredTranslation(Landroid/app/Activity;ZLcom/google/android/apps/translate/history/Entry;)V
    .locals 0
    .parameter "activity"
    .parameter "isStarred"
    .parameter "translationEntry"

    .prologue
    .line 336
    if-eqz p1, :cond_0

    .line 337
    invoke-static {p0, p2}, Lcom/google/android/apps/translate/history/FavoriteDb;->add(Landroid/content/Context;Lcom/google/android/apps/translate/history/Entry;)V

    .line 341
    :goto_0
    return-void

    .line 339
    :cond_0
    invoke-static {p0, p2}, Lcom/google/android/apps/translate/history/FavoriteDb;->remove(Landroid/content/Context;Lcom/google/android/apps/translate/history/Entry;)V

    goto :goto_0
.end method
