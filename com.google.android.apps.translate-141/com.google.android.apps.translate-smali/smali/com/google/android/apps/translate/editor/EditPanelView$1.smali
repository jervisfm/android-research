.class Lcom/google/android/apps/translate/editor/EditPanelView$1;
.super Ljava/lang/Object;
.source "EditPanelView.java"

# interfaces
.implements Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/editor/EditPanelView;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/editor/EditPanelView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/editor/EditPanelView;)V
    .locals 0
    .parameter

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$1;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, suggestEntries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    const/4 v2, 0x0

    .line 314
    const-string v3, "EditPanelView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "filter entries.size()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView$1;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;
    invoke-static {v3}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$200(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/TextSlot;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/translate/translation/TranslateEntry;->normalizeInputText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, inputText:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object p1, v2

    .line 336
    .end local p1           #suggestEntries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    :cond_0
    :goto_0
    return-object p1

    .line 321
    .restart local p1       #suggestEntries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 324
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 325
    .local v1, suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getType()Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_DETECTED_SRCLANG:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    if-ne v3, v4, :cond_2

    .line 326
    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getSourceText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object p1, v2

    .line 327
    goto :goto_0

    .line 330
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object p1, v2

    .line 332
    goto :goto_0
.end method
