.class Lcom/google/android/apps/translate/editor/EditPanelView$3;
.super Ljava/lang/Object;
.source "EditPanelView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/editor/EditPanelView;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/editor/EditPanelView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/editor/EditPanelView;)V
    .locals 0
    .parameter

    .prologue
    .line 353
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$3;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 4
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const-string v1, "EditPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mListView.setOnItemClickListener position="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 361
    .local v0, suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getHtmlInputText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$3;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$400(Lcom/google/android/apps/translate/editor/EditPanelView;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getHtmlInputText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Util;->showLongToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    .line 368
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 365
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$3;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$400(Lcom/google/android/apps/translate/editor/EditPanelView;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Util;->showLongToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
