.class Lcom/google/android/apps/translate/editor/EditPanelView$4;
.super Ljava/lang/Object;
.source "EditPanelView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/editor/EditPanelView;->onSuggestSelected(Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

.field final synthetic val$inputText:Ljava/lang/String;

.field final synthetic val$suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/editor/EditPanelView;Ljava/lang/String;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 707
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    iput-object p2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->val$inputText:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->val$suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;
    invoke-static {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$500(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/InputMethodView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->cancel()V

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;
    invoke-static {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$600(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->val$inputText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v2, v3}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->update(Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Z)V

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->val$suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getType()Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_DETECTED_SRCLANG:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    if-ne v0, v1, :cond_0

    .line 713
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;
    invoke-static {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$800(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;
    invoke-static {v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$700(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/Languages;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->val$suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->val$suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getSourceText()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView$Callback;->onSourceLanguageChangeRequested(Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    .line 720
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$400(Lcom/google/android/apps/translate/editor/EditPanelView;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->val$suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->logSuggestionClick(Landroid/content/Context;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;Z)V

    .line 721
    return-void

    .line 718
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->this$0:Lcom/google/android/apps/translate/editor/EditPanelView;

    #getter for: Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;
    invoke-static {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->access$200(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/TextSlot;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$4;->val$inputText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->replaceTextWithSelection(Ljava/lang/String;)V

    goto :goto_0
.end method
