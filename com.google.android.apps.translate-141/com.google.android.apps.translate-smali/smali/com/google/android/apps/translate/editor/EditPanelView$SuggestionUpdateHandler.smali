.class Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;
.super Landroid/os/Handler;
.source "EditPanelView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/editor/EditPanelView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SuggestionUpdateHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;
    }
.end annotation


# static fields
.field private static final MSG_UPDATE_SUGGESTIONS:I = 0x0

.field private static final UPDATE_SUGGESTIONS_MILLIS:J = 0x1f4L


# instance fields
.field private mIsConversationMode:Z

.field private mSourceLanguage:Lcom/google/android/apps/translate/Language;

.field private mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

.field private mUserExplicitlySetSourceLanguage:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mUserExplicitlySetSourceLanguage:Z

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mIsConversationMode:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/translate/editor/EditPanelView$1;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;-><init>()V

    return-void
.end method

.method private filterSuggestions(Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V
    .locals 4
    .parameter "inputText"
    .parameter "spellCorrection"
    .parameter "detectedSrcShortLangName"

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    if-eqz v2, :cond_0

    .line 150
    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v2, p2, p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->setSpellCorrection(Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Z

    move-result v0

    .line 155
    .local v0, spellCorrectionAdded:Z
    :goto_0
    if-nez v0, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mIsConversationMode:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mUserExplicitlySetSourceLanguage:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v2, p3, p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->setDetectedSrcShortLangName(Ljava/lang/String;Ljava/lang/String;)Z

    .line 164
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->notifyDataSetChanged()V

    .line 165
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 166
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 167
    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mUserExplicitlySetSourceLanguage:Z

    .line 170
    .end local v0           #spellCorrectionAdded:Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 150
    goto :goto_0

    .line 162
    .restart local v0       #spellCorrectionAdded:Z
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->setDetectedSrcShortLangName(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 133
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 140
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;

    .line 136
    .local v0, data:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;
    iget-object v1, v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;->mInputText:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;->mSpellCorrection:Landroid/text/SpannableStringBuilder;

    iget-object v3, v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;->mDetectedSrcShortLangName:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->filterSuggestions(Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    .line 145
    .end local v0           #data:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;
    :cond_0
    return-void

    .line 133
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setIsConversationMode(Z)V
    .locals 0
    .parameter "isConversationMode"

    .prologue
    .line 173
    iput-boolean p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mIsConversationMode:Z

    .line 174
    return-void
.end method

.method public setSourceLanguage(Lcom/google/android/apps/translate/Language;)V
    .locals 0
    .parameter "lang"

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    .line 178
    return-void
.end method

.method public setSuggestAdapter(Lcom/google/android/apps/translate/editor/SuggestAdapter;)V
    .locals 0
    .parameter "suggestAdapter"

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    .line 186
    return-void
.end method

.method public setUserExplicitlySetSourceLanguage(Z)V
    .locals 0
    .parameter "userExplicitlySetSourceLanguage"

    .prologue
    .line 181
    iput-boolean p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->mUserExplicitlySetSourceLanguage:Z

    .line 182
    return-void
.end method

.method public update(Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Z)V
    .locals 5
    .parameter "inputText"
    .parameter "spellCorrection"
    .parameter "detectedSrcShortLangName"
    .parameter "updateImmediately"

    .prologue
    const/4 v4, 0x0

    .line 198
    const-string v1, "EditPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "update text="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-static {p1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->normalizeInputText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 200
    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->removeMessages(I)V

    .line 201
    if-nez p4, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 202
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->filterSuggestions(Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    .line 211
    :goto_0
    return-void

    .line 204
    :cond_1
    new-instance v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;-><init>(Lcom/google/android/apps/translate/editor/EditPanelView$1;)V

    .line 205
    .local v0, data:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;
    iput-object p1, v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;->mInputText:Ljava/lang/String;

    .line 206
    iput-object p2, v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;->mSpellCorrection:Landroid/text/SpannableStringBuilder;

    .line 207
    iput-object p3, v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler$SuggestionUpdateData;->mDetectedSrcShortLangName:Ljava/lang/String;

    .line 208
    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method
