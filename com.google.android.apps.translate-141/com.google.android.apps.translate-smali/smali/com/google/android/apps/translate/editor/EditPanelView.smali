.class public Lcom/google/android/apps/translate/editor/EditPanelView;
.super Landroid/widget/LinearLayout;
.source "EditPanelView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;
.implements Landroid/view/View$OnKeyListener;
.implements Lcom/google/android/apps/translate/editor/PreImeAutoCompleteTextView$OnKeyPreImeListener;
.implements Lcom/google/android/apps/translate/editor/InstantTranslateTextView$Callback;
.implements Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;
.implements Landroid/text/TextWatcher;
.implements Lcom/google/android/apps/translate/editor/SlotView$TouchEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/editor/EditPanelView$7;,
        Lcom/google/android/apps/translate/editor/EditPanelView$Callback;,
        Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;
    }
.end annotation


# static fields
.field public static final ALWAYS_SHOW_CLEAR_BUTTON_IN_EDITMODE:Z = true

.field private static final SELECT_TEXT_WHEN_EDIT_MODE_STARTS:Z = true

.field private static final TAG:Ljava/lang/String; = "EditPanelView"

.field private static final TRANSLATE_AFTER_VOICE_INPUT:Z = true


# instance fields
.field private mAccept:Landroid/widget/ImageView;

.field private mAcceptWrapper:Landroid/view/View;

.field private mActivity:Landroid/app/Activity;

.field private mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

.field private mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

.field private mClearButton:Landroid/widget/ImageButton;

.field private mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

.field private mControlPanelWrapper:Landroid/view/View;

.field private mEditMode:Z

.field private mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

.field private mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

.field private mInstantTranslationDivider:Landroid/view/View;

.field private mInternalEditMode:Z

.field private mIsConversationMode:Z

.field private mLanguageList:Lcom/google/android/apps/translate/Languages;

.field private mLatestTranslationSource:Ljava/lang/String;

.field private mLatestTranslationTarget:Ljava/lang/String;

.field private mListView:Landroid/widget/ListView;

.field private mLongPressed:Z

.field private mPostEditInitialText:Ljava/lang/String;

.field private mPostEditInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

.field private mSourceLanguage:Lcom/google/android/apps/translate/Language;

.field private mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

.field private mSuggestFilter:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

.field private mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

.field private mTargetLanguage:Lcom/google/android/apps/translate/Language;

.field private mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

.field private mTranslateProgressBar:Landroid/widget/ProgressBar;

.field private mVoiceLocale:Ljava/lang/String;

.field private final sMaxLines:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter "context"
    .parameter "attr"

    .prologue
    const/4 v1, 0x0

    .line 243
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditMode:Z

    .line 100
    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInternalEditMode:Z

    .line 101
    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    .line 111
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mPostEditInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    .line 115
    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLongPressed:Z

    .line 116
    new-instance v0, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;-><init>(Lcom/google/android/apps/translate/editor/EditPanelView$1;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    .line 244
    const-string v0, "EditPanelView"

    const-string v1, "EditPanelView"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$integer;->InputMaxLines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->sMaxLines:I

    .line 246
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/translate/editor/EditPanelView;)Landroid/widget/ImageView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAccept:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/TextSlot;
    .locals 1
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/editor/EditPanelView;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/editor/EditPanelView;->onSuggestSelected(Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/editor/EditPanelView;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/InputMethodView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/Languages;
    .locals 1
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/translate/editor/EditPanelView;)Lcom/google/android/apps/translate/editor/EditPanelView$Callback;
    .locals 1
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/translate/editor/EditPanelView;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslateProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private declared-synchronized clearTexts()V
    .locals 2

    .prologue
    .line 428
    monitor-enter p0

    :try_start_0
    const-string v0, "EditPanelView"

    const-string v1, "clearTexts"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->setText(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    monitor-exit p0

    return-void

    .line 428
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private handleTapOnInputTextBox()V
    .locals 2

    .prologue
    .line 980
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 981
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/translate/editor/EditPanelView$Callback;->onEditModeStart(Lcom/google/android/apps/translate/editor/EditPanelView;Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 998
    :goto_0
    return-void

    .line 983
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->hasInputMethodShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 984
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    goto :goto_0

    .line 986
    :cond_1
    sget-object v0, Lcom/google/android/apps/translate/editor/EditPanelView$7;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->getCurrentInputMethod()Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 988
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    goto :goto_0

    .line 991
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->cancel()V

    goto :goto_0

    .line 986
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onSuggestSelected(Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;)V
    .locals 4
    .parameter "suggestEntry"

    .prologue
    .line 705
    invoke-virtual {p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v0

    .line 706
    .local v0, inputText:Ljava/lang/String;
    const-string v1, "EditPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSuggestSelected inputText="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/translate/editor/EditPanelView$4;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/apps/translate/editor/EditPanelView$4;-><init>(Lcom/google/android/apps/translate/editor/EditPanelView;Ljava/lang/String;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 723
    return-void
.end method

.method private updateImeSuggestions()V
    .locals 4

    .prologue
    .line 405
    const-string v1, "EditPanelView"

    const-string v2, "updateImeSuggestions"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 407
    .local v0, deviceLang:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 409
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/TextSlot;->getInputType()I

    move-result v2

    const v3, -0x10001

    and-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/TextSlot;->setInputType(I)V

    .line 417
    :goto_0
    return-void

    .line 414
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/TextSlot;->getInputType()I

    move-result v2

    const/high16 v3, 0x1

    or-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/TextSlot;->setInputType(I)V

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .parameter "s"

    .prologue
    const/4 v4, 0x0

    .line 889
    const-string v1, "EditPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "afterTextChanged text="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->isInternalEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 904
    :goto_0
    return-void

    .line 895
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInternalEditMode:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 899
    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->enableEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 901
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 902
    .local v0, text:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v4, v4, v2}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->update(Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Z)V

    .line 903
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->updateButtons(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    .prologue
    .line 923
    return-void
.end method

.method public disableEditMode(ZLcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V
    .locals 4
    .parameter "hideTranslation"
    .parameter "event"

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 559
    const-string v0, "EditPanelView"

    const-string v3, "disableEditMode"

    invoke-static {v0, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->disableSuggestions()Z

    .line 561
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    iget v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->sMaxLines:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/translate/editor/TextSlot;->setMaxLines(I)V

    .line 562
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/translate/editor/TextSlot;->setIsTextEditor(Z)V

    .line 563
    iput-boolean v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditMode:Z

    .line 564
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 565
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setVisibility(I)V

    .line 566
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->isStopped()Z

    move-result v0

    if-nez v0, :cond_0

    .line 567
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->INSTANT_TRANSLATION_SHOWN:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->setImpression(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 570
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->stop()V

    .line 572
    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    if-eqz v0, :cond_1

    .line 573
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanelWrapper:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 574
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAcceptWrapper:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 575
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mClearButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 578
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslationDivider:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 579
    return-void

    :cond_2
    move v0, v2

    .line 565
    goto :goto_0
.end method

.method public disableSuggestions()Z
    .locals 2

    .prologue
    .line 682
    const-string v0, "EditPanelView"

    const-string v1, "disableSuggestions"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 686
    const/4 v0, 0x1

    .line 688
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enableEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V
    .locals 4
    .parameter "inputMethod"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 582
    const-string v0, "EditPanelView"

    const-string v1, "enableEditMode"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    iput-boolean v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditMode:Z

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->setMaxLines(I)V

    .line 585
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/translate/editor/TextSlot;->setIsTextEditor(Z)V

    .line 586
    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/editor/EditPanelView;->initInstantTranslation(Z)V

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslationDivider:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanelWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAcceptWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mClearButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 592
    invoke-static {p1}, Lcom/google/android/apps/translate/editor/InputMethodView;->logInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 593
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mPostEditInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    .line 594
    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    if-eqz v0, :cond_1

    .line 595
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->CONV:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 599
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->hideTranslationLoading()V

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->updateButtons(Ljava/lang/String;)V

    .line 601
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->isStopped()Z

    move-result v0

    if-nez v0, :cond_0

    .line 602
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->INSTANT_TRANSLATION_SHOWN:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->setImpression(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 605
    :cond_0
    return-void

    .line 597
    :cond_1
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->EDIT:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    goto :goto_0
.end method

.method public enableSuggestions()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 670
    const-string v1, "EditPanelView"

    const-string v2, "enableSuggestions"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_1

    .line 672
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-nez v1, :cond_0

    .line 673
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 675
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 676
    const/4 v0, 0x1

    .line 678
    :cond_1
    return v0
.end method

.method public endInternalEdit()V
    .locals 1

    .prologue
    .line 884
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInternalEditMode:Z

    .line 885
    return-void
.end method

.method public getSourceLanguage()Lcom/google/android/apps/translate/Language;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method public getSuggestTextViewCount()I
    .locals 6

    .prologue
    .line 769
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/InputMethodView;->getEditingAreaViewHeight()I

    move-result v0

    .line 770
    .local v0, editingAreaHeight:I
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 771
    .local v1, r:Landroid/content/res/Resources;
    sget v3, Lcom/google/android/apps/translate/R$dimen;->suggest_text_box_height:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 772
    .local v2, suggestTextViewHeight:I
    if-nez v2, :cond_0

    .line 773
    const/4 v3, 0x0

    .line 777
    :goto_0
    return v3

    .line 775
    :cond_0
    const-string v3, "EditPanelView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSuggestTextViewCount editingAreaHeight="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const-string v3, "EditPanelView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSuggestTextViewCount suggestTextViewHeight="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    div-int v3, v0, v2

    goto :goto_0
.end method

.method public getTargetLanguage()Lcom/google/android/apps/translate/Language;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method public getTranslationText(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "srcText"

    .prologue
    .line 794
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLatestTranslationSource:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 795
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    const-string v0, "EditPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTranslationText: Use translation result in handwriting response. translation=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLatestTranslationTarget:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLatestTranslationTarget:Ljava/lang/String;

    .line 805
    :goto_0
    return-object v0

    .line 801
    :cond_1
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 802
    const-string v0, "EditPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTranslationText: Try to use instant translation result. translation=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->getTranslationText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->getTranslationText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hasInputMethodShown()Z
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->hasInputMethodShown()Z

    move-result v0

    return v0
.end method

.method public hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z
    .locals 2
    .parameter "event"

    .prologue
    .line 550
    const-string v0, "EditPanelView"

    const-string v1, "hideCurrentInputMethod"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    move-result v0

    return v0
.end method

.method public hideTranslationLoading()V
    .locals 2

    .prologue
    .line 738
    const-string v0, "EditPanelView"

    const-string v1, "hideTranslationLoading"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/editor/EditPanelView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/editor/EditPanelView$6;-><init>(Lcom/google/android/apps/translate/editor/EditPanelView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 747
    return-void
.end method

.method public init(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Z)V
    .locals 11
    .parameter "activity"
    .parameter "languages"
    .parameter "fromLanguage"
    .parameter "toLanguage"
    .parameter "locale"
    .parameter "isConversationMode"

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    .line 283
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->setUserExplicitlySetSourceLanguage(Z)V

    .line 287
    iput-object p3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    .line 288
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->setSourceLanguage(Lcom/google/android/apps/translate/Language;)V

    .line 289
    iput-object p4, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    .line 290
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mVoiceLocale:Ljava/lang/String;

    .line 291
    iput-object p2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 292
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-static {v1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    move/from16 v0, p6

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    .line 294
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    iget-boolean v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->setIsConversationMode(Z)V

    .line 296
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/TranslateApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/TranslateApplication;->getInstantTranslateHandler()Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    .line 298
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/TextSlot;->setInstantTranslateHandler(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)V

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->getCurrentInputMethod()Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    move-result-object v10

    .line 301
    .local v10, im:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->initInstantTranslation(Z)V

    .line 302
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v5, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v6, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    iget-object v7, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

    iget-object v8, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mVoiceLocale:Ljava/lang/String;

    move-object v9, p0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/translate/editor/InputMethodView;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/editor/TextSlot;Lcom/google/android/apps/translate/asreditor/AsrResultEditor;Ljava/lang/String;Lcom/google/android/apps/translate/editor/EditPanelView;)V

    .line 304
    iget-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    if-nez v1, :cond_0

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v1, v10}, Lcom/google/android/apps/translate/editor/InputMethodView;->restartInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 309
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->updateImeSuggestions()V

    .line 311
    new-instance v1, Lcom/google/android/apps/translate/editor/EditPanelView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/editor/EditPanelView$1;-><init>(Lcom/google/android/apps/translate/editor/EditPanelView;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestFilter:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

    .line 339
    new-instance v1, Lcom/google/android/apps/translate/editor/SuggestAdapter;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v5, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v6, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestFilter:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

    iget-object v7, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/translate/editor/SuggestAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;Landroid/widget/EditText;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    .line 341
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->setSuggestAdapter(Lcom/google/android/apps/translate/editor/SuggestAdapter;)V

    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->disableSuggestions()Z

    .line 343
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_1

    .line 344
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/google/android/apps/translate/editor/EditPanelView$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translate/editor/EditPanelView$2;-><init>(Lcom/google/android/apps/translate/editor/EditPanelView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 353
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/google/android/apps/translate/editor/EditPanelView$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translate/editor/EditPanelView$3;-><init>(Lcom/google/android/apps/translate/editor/EditPanelView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 378
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->updateButtons(Ljava/lang/String;)V

    .line 379
    return-void

    .line 283
    .end local v10           #im:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public initInstantTranslation(Z)V
    .locals 4
    .parameter "enable"

    .prologue
    .line 385
    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditMode:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/translate/Profile;->getInstantTranslation(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->forceInstantTranslation()V

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setVisibility(I)V

    .line 393
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->invalidate()V

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->invalidate()V

    .line 395
    return-void

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->stop()V

    goto :goto_0
.end method

.method public isEditMode()Z
    .locals 1

    .prologue
    .line 555
    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditMode:Z

    return v0
.end method

.method public onAccept(Z)V
    .locals 3
    .parameter "doTranslate"

    .prologue
    .line 465
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 467
    .local v0, inputText:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/TextSlot;->setSelection(I)V

    .line 468
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mPostEditInitialText:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mPostEditInitialText:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 470
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->CONV_EDIT:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 472
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 473
    const/4 p1, 0x0

    .line 475
    :cond_1
    if-eqz p1, :cond_2

    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->showTranslationLoading()V

    .line 479
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->getId()I

    move-result v2

    invoke-interface {v1, v2, p1}, Lcom/google/android/apps/translate/editor/EditPanelView$Callback;->onAccept(IZ)V

    .line 480
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1001
    const-string v0, "EditPanelView"

    const-string v1, "onActivityResult"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    if-eqz v0, :cond_0

    .line 1003
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/translate/editor/InputMethodView;->onActivityResult(IILandroid/content/Intent;)V

    .line 1005
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 435
    const-string v2, "EditPanelView"

    const-string v3, "onClick"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 437
    .local v0, id:I
    sget v2, Lcom/google/android/apps/translate/R$id;->btn_confirm_view_accept:I

    if-ne v0, v2, :cond_1

    .line 438
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v3, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->ACCEPT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 439
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    sget v2, Lcom/google/android/apps/translate/R$id;->btn_confirm_view_clear:I

    if-ne v0, v2, :cond_0

    .line 441
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/TextSlot;->isEmpty()Z

    move-result v1

    .line 442
    .local v1, isTextEmpty:Z
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->clearTexts()V

    .line 443
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    if-eqz v2, :cond_2

    .line 444
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->reset()V

    .line 446
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->onClearText()V

    .line 447
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->cancel()V

    .line 448
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->hasInputMethodShown()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v1, :cond_0

    .line 450
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "newConfig"

    .prologue
    .line 756
    invoke-static {}, Lcom/google/android/apps/translate/Util;->getSdkVersion()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 757
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    if-eqz v0, :cond_1

    .line 760
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 762
    :cond_1
    return-void
.end method

.method public onConfirm()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 457
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->INSTANT_TRANSLATION_CLICKED:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->ACCEPT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 461
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    .line 462
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->onDestroy()V

    .line 547
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "e"

    .prologue
    .line 964
    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "e"

    .prologue
    .line 969
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "e"

    .prologue
    .line 934
    const/4 v0, 0x0

    return v0
.end method

.method public onEditCompleted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "srcText"
    .parameter "trgText"

    .prologue
    .line 786
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLatestTranslationSource:Ljava/lang/String;

    .line 787
    iput-object p2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLatestTranslationTarget:Ljava/lang/String;

    .line 788
    return-void
.end method

.method public onEditPanelHeightChanged(I)V
    .locals 1
    .parameter "actionBarHeight"

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView;->onEditPanelHeightChanged(I)V

    .line 782
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    const/4 v3, 0x1

    .line 484
    const-string v0, "EditPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEditorAction actionId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    if-eqz p3, :cond_0

    .line 486
    const-string v0, "EditPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEditorAction event.getAction()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq v0, v3, :cond_2

    :cond_1
    const/4 v0, 0x6

    if-ne p2, v0, :cond_3

    .line 490
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->ACCEPT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 491
    invoke-virtual {p0, v3}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    .line 493
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 250
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 251
    sget v0, Lcom/google/android/apps/translate/R$id;->msg_confirm_content:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/asreditor/AsrResultEditor;->getEditorField()Lcom/google/android/apps/translate/editor/TextSlot;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    .line 254
    sget v0, Lcom/google/android/apps/translate/R$id;->btn_confirm_view_accept:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAccept:Landroid/widget/ImageView;

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAccept:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    sget v0, Lcom/google/android/apps/translate/R$id;->frame_accept:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAcceptWrapper:Landroid/view/View;

    .line 257
    sget v0, Lcom/google/android/apps/translate/R$id;->btn_confirm_view_clear:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mClearButton:Landroid/widget/ImageButton;

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mClearButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    sget v0, Lcom/google/android/apps/translate/R$id;->instant_translation_divider:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInstantTranslationDivider:Landroid/view/View;

    .line 260
    sget v0, Lcom/google/android/apps/translate/R$id;->edit_panel_instant_translate:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setCallback(Lcom/google/android/apps/translate/editor/InstantTranslateTextView$Callback;)V

    .line 262
    sget v0, Lcom/google/android/apps/translate/R$id;->translation_loading:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslateProgressBar:Landroid/widget/ProgressBar;

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->setCursorVisible(Z)V

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/editor/TextSlot;->setKeyPreImeListener(Lcom/google/android/apps/translate/editor/PreImeAutoCompleteTextView$OnKeyPreImeListener;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/editor/TextSlot;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/editor/TextSlot;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 268
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .parameter "e1"
    .parameter "e2"
    .parameter "velocityX"
    .parameter "velocityY"

    .prologue
    .line 939
    const/4 v0, 0x0

    return v0
.end method

.method public onInputMethodReady(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V
    .locals 2
    .parameter "inputMethod"

    .prologue
    .line 841
    const-string v0, "EditPanelView"

    const-string v1, "onInputMethodReady"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->disableSuggestions()Z

    .line 843
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->invalidate()V

    .line 844
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

    invoke-interface {v0, p1}, Lcom/google/android/apps/translate/editor/EditPanelView$Callback;->onEditModeReady(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 845
    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLongPressed:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->CAMERA:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->HANDWRITING:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne p1, v0, :cond_1

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->selectAll()V

    .line 855
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->enableSuggestions()Z

    .line 856
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mPostEditInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne v0, v1, :cond_2

    .line 858
    invoke-static {p1}, Lcom/google/android/apps/translate/editor/InputMethodView;->logInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 859
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mPostEditInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    .line 861
    :cond_2
    return-void
.end method

.method public onInputMethodStart(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)Z
    .locals 2
    .parameter "inputMethod"

    .prologue
    .line 830
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 831
    const-string v0, "EditPanelView"

    const-string v1, "onInputMethodStart NOT-EDIT"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/translate/editor/EditPanelView$Callback;->onEditModeStart(Lcom/google/android/apps/translate/editor/EditPanelView;Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 833
    const/4 v0, 0x1

    .line 837
    :goto_0
    return v0

    .line 835
    :cond_0
    const-string v0, "EditPanelView"

    const-string v1, "onInputMethodStart EDIT"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->disableSuggestions()Z

    .line 837
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    .line 865
    const-string v1, "EditPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKey KeyEvent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    if-eqz p3, :cond_1

    const/16 v1, 0x42

    if-ne p2, v1, :cond_1

    .line 867
    const-string v1, "EditPanelView"

    const-string v2, "onKey ENTER"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 869
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->ACCEPT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 870
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    .line 876
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 5
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 636
    const-string v1, "EditPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyPreIme keyCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    if-eqz p2, :cond_5

    const/4 v1, 0x4

    if-ne p1, v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 638
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_4

    .line 639
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->hasInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 640
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 641
    iget-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    if-eqz v1, :cond_1

    .line 666
    :cond_0
    :goto_0
    return v0

    .line 646
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 648
    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    goto :goto_0

    .line 651
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 652
    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    goto :goto_0

    .line 658
    :cond_3
    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    .line 659
    const-string v1, "EditPanelView"

    const-string v2, "onKeyPreIme ACCEPT"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 662
    :cond_4
    const-string v1, "EditPanelView"

    const-string v2, "onKeyPreIme FORWARD_BACK"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 666
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/translate/editor/InputMethodView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLongPause(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V
    .locals 3
    .parameter "inputText"
    .parameter "targetText"
    .parameter "spellCorrection"
    .parameter "detectedSrcShortLangName"

    .prologue
    .line 528
    const-string v0, "EditPanelView"

    const-string v1, "onLongPause"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, p4, v2}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->update(Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Z)V

    .line 534
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1
    .parameter "e"

    .prologue
    .line 944
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLongPressed:Z

    .line 945
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->handleTapOnInputTextBox()V

    .line 946
    return-void
.end method

.method public onNonStreamingVoiceResult(Ljava/lang/String;)V
    .locals 4
    .parameter "recognitionResult"

    .prologue
    .line 612
    const-string v1, "EditPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNonStreamingVoiceResult recognitionResult="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    if-eqz p1, :cond_0

    .line 614
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/translate/editor/TextSlot;->replaceSelectedText(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 615
    .local v0, newText:Ljava/lang/String;
    iget-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    if-nez v1, :cond_1

    .line 616
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->ACCEPT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 617
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->onAccept(Z)V

    .line 624
    .end local v0           #newText:Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 619
    .restart local v0       #newText:Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mPostEditInitialText:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mIsConversationMode:Z

    if-eqz v1, :cond_0

    .line 620
    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mPostEditInitialText:Ljava/lang/String;

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->onPause()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->PAUSE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->disableEditMode(ZLcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .parameter "e1"
    .parameter "e2"
    .parameter "distanceX"
    .parameter "distanceY"

    .prologue
    .line 950
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .parameter "e"

    .prologue
    .line 955
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "e"

    .prologue
    const/4 v0, 0x0

    .line 974
    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLongPressed:Z

    .line 975
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->handleTapOnInputTextBox()V

    .line 976
    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "e"

    .prologue
    .line 959
    const/4 v0, 0x0

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    .prologue
    .line 810
    if-gtz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getCurrentInputMethod()Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->HANDWRITING:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne v0, v1, :cond_0

    .line 813
    const-string v0, "EditPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forcibly switch to keyboard. edit_panel_height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " input_method_panel_height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 827
    :cond_0
    return-void
.end method

.method public onSourceTextDone(Landroid/text/SpannableStringBuilder;)V
    .locals 0
    .parameter "text"

    .prologue
    .line 518
    return-void
.end method

.method public onSourceTextUpdate(Landroid/text/SpannableStringBuilder;)V
    .locals 0
    .parameter "voiceInputText"

    .prologue
    .line 498
    return-void
.end method

.method public onTargetTextDone(Landroid/text/SpannableStringBuilder;)V
    .locals 1
    .parameter "text"

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setText(Ljava/lang/CharSequence;)V

    .line 523
    return-void
.end method

.method public onTargetTextUpdate(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V
    .locals 4
    .parameter "inputText"
    .parameter "targetText"
    .parameter "spellCorrection"
    .parameter "detectedSrcShortLangName"

    .prologue
    const/4 v3, 0x0

    .line 506
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestUpdateHandler:Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3, p4, v3}, Lcom/google/android/apps/translate/editor/EditPanelView$SuggestionUpdateHandler;->update(Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Z)V

    .line 511
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 512
    .local v0, oldText:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mTranslate:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->scrollToEdge(Ljava/lang/String;Landroid/widget/EditText;Z)V

    .line 514
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    .prologue
    .line 926
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 927
    const-string v0, "EditPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTextChanged s="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " start="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " before="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :cond_0
    return-void
.end method

.method public onTranslationDone()V
    .locals 2

    .prologue
    .line 750
    const-string v0, "EditPanelView"

    const-string v1, "onTranslationDone"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->hideTranslationLoading()V

    .line 752
    return-void
.end method

.method public resetSuggestions()V
    .locals 2

    .prologue
    .line 697
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 699
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mSuggestAdapter:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 702
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/google/android/apps/translate/editor/EditPanelView$Callback;)V
    .locals 2
    .parameter "callback"

    .prologue
    .line 423
    const-string v0, "EditPanelView"

    const-string v1, "setCallback"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

    .line 425
    return-void
.end method

.method public setInputMethodView(Lcom/google/android/apps/translate/editor/InputMethodView;)V
    .locals 2
    .parameter "view"

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$id;->input_method_view_wrapper:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mControlPanelWrapper:Landroid/view/View;

    .line 274
    return-void
.end method

.method public setLanguageList(Lcom/google/android/apps/translate/Languages;)V
    .locals 0
    .parameter "list"

    .prologue
    .line 401
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 402
    return-void
.end method

.method public setListView(Landroid/widget/ListView;)V
    .locals 0
    .parameter "listView"

    .prologue
    .line 277
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mListView:Landroid/widget/ListView;

    .line 278
    return-void
.end method

.method public showTranslationLoading()V
    .locals 2

    .prologue
    .line 726
    const-string v0, "EditPanelView"

    const-string v1, "showTranslationLoading"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/editor/EditPanelView$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/editor/EditPanelView$5;-><init>(Lcom/google/android/apps/translate/editor/EditPanelView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 735
    return-void
.end method

.method public startInternalEdit()V
    .locals 1

    .prologue
    .line 880
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mInternalEditMode:Z

    .line 881
    return-void
.end method

.method public updateButtons(Ljava/lang/String;)V
    .locals 6
    .parameter "text"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 912
    const-string v3, "EditPanelView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateButtons text="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    .line 917
    .local v0, enableButtons:Z
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mClearButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mCallback:Lcom/google/android/apps/translate/editor/EditPanelView$Callback;

    invoke-interface {v4}, Lcom/google/android/apps/translate/editor/EditPanelView$Callback;->hasSomethingToClear()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 919
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/EditPanelView;->mAccept:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 920
    return-void

    .end local v0           #enableButtons:Z
    :cond_2
    move v0, v2

    .line 913
    goto :goto_0
.end method
