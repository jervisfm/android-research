.class public Lcom/google/android/apps/translate/editor/InputMethodView;
.super Landroid/widget/LinearLayout;
.source "InputMethodView.java"

# interfaces
.implements Lcom/google/android/apps/translate/VoiceInput$UiListener;
.implements Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/editor/InputMethodView$17;,
        Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;,
        Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;
    }
.end annotation


# static fields
.field private static final ENABLE_CAMERA_FOR_AUTO_DETECT:Z = false

.field private static final HIDE_INPUT_SELECTOR:Z = true

.field private static final SHOW_KEYBOARD:Z = false

.field public static final SLIDE_ANIMATION_MILLISECONDS:I = 0x96

.field private static final TAG:Ljava/lang/String; = "InputMethodView"

.field private static final USE_STREAMING_VOICE_INPUT:Z


# instance fields
.field private mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

.field private mActivity:Landroid/app/Activity;

.field private mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

.field private mCameraBtn:Landroid/widget/ImageButton;

.field private mCameraHeight:I

.field private mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

.field private mCameraInputViews:[Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

.field private mCameraSupported:Z

.field private mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

.field private mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

.field private mHandwritingBtn:Landroid/widget/ImageButton;

.field private mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

.field private mIconCount:I

.field private mInputMethodPlaceholder:Landroid/widget/LinearLayout;

.field private mInputSelector:Landroid/view/View;

.field private mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

.field private mKeyboardBtn:Landroid/widget/ImageButton;

.field private mLoadingImage:Z

.field private mMicBtn:Landroid/widget/ImageButton;

.field private mPreviousSourceLanguage:Lcom/google/android/apps/translate/Language;

.field private mPreviousTargetLanguage:Lcom/google/android/apps/translate/Language;

.field private mSourceLanguage:Lcom/google/android/apps/translate/Language;

.field private mStartedInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

.field private mStreamingView:Lcom/google/android/apps/translate/RecognitionView;

.field private mTargetLanguage:Lcom/google/android/apps/translate/Language;

.field private mTranslateView:Landroid/view/View;

.field private mVoiceInput:Lcom/google/android/apps/translate/VoiceInput;

.field private mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

.field private mVoiceLocale:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 133
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    .line 67
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mStartedInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    .line 84
    iput v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    .line 88
    iput-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    .line 102
    iput-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    .line 103
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputViews:[Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    .line 104
    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraSupported:Z

    .line 106
    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mLoadingImage:Z

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/editor/InputMethodView;)Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/editor/InputMethodView;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/editor/InputMethodView;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/editor/InputMethodView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraHeight:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/editor/InputMethodView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideSoftwareKeyboard()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/editor/InputMethodView;)Lcom/google/android/apps/translate/editor/EditPanelView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/translate/editor/InputMethodView;)Lcom/google/android/apps/translate/handwriting/HandwritingInputView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/translate/editor/InputMethodView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->render()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/translate/editor/InputMethodView;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView;->showInputSelector(Z)V

    return-void
.end method

.method private changeCameraLanguage()V
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->changeSourceLanguage(Ljava/lang/String;)Z

    .line 442
    :goto_0
    return-void

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->changeSourceLanguage(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private clearSelection()V
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mKeyboardBtn:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/translate/R$drawable;->input_keyboard:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mMicBtn:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/translate/R$drawable;->input_voice:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingBtn:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/translate/R$drawable;->input_handwriting:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 539
    return-void
.end method

.method private getCameraInputUserAgent()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 355
    const-string v2, ""

    .line 357
    .local v2, versionName:Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 359
    .local v1, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 363
    .end local v1           #packageInfo:Landroid/content/pm/PackageInfo;
    :goto_0
    const/4 v3, 0x0

    check-cast v3, Ljava/util/Locale;

    const-string v4, "GoogleGoggles-AndroidTranslateTextSearch/%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "InputMethodView"

    const-string v4, "Unable to retrieve version code from manifest."

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private hideInputSelector(Z)V
    .locals 2
    .parameter "animate"

    .prologue
    .line 293
    const-string v0, "InputMethodView"

    const-string v1, "hideInputSelector"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    if-eqz p1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputSelector:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->slideDown(Landroid/view/View;)V

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputSelector:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private hideSoftwareKeyboard()V
    .locals 3

    .prologue
    .line 529
    const-string v1, "InputMethodView"

    const-string v2, "hideSoftInputFromWindow"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 532
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 533
    return-void
.end method

.method private initializeCameraInputView(Landroid/view/LayoutInflater;)V
    .locals 7
    .parameter "inflater"

    .prologue
    const/4 v4, 0x0

    .line 332
    const-string v3, "InputMethodView"

    const-string v5, "initializeCameraInputView"

    invoke-static {v3, v5}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getRootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 335
    .local v0, container:Landroid/widget/FrameLayout;
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v1, v3, Landroid/content/res/Configuration;->orientation:I

    .line 336
    .local v1, currentOrientation:I
    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    const/4 v2, 0x1

    .line 337
    .local v2, index:I
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputViews:[Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    aget-object v3, v3, v2

    if-nez v3, :cond_0

    .line 338
    iget-object v5, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputViews:[Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    sget v3, Lcom/google/android/apps/translate/R$layout;->camera_text_input_view:I

    const/4 v6, 0x0

    invoke-virtual {p1, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    aput-object v3, v5, v2

    .line 341
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputViews:[Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    aget-object v3, v3, v2

    iget-object v5, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    if-eq v3, v5, :cond_2

    .line 342
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    if-eqz v3, :cond_1

    .line 343
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 345
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputViews:[Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    aget-object v3, v3, v2

    iput-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    .line 346
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v0, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 349
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->setListener(Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;)V

    .line 350
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    sget-object v4, Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;->TRANSLATE:Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->setClientType(Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;)V

    .line 351
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getCameraInputUserAgent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->setUserAgent(Ljava/lang/String;)V

    .line 352
    return-void

    .end local v2           #index:I
    :cond_3
    move v2, v4

    .line 336
    goto :goto_0
.end method

.method private initializeHandwritingInputView(Landroid/view/LayoutInflater;)V
    .locals 3
    .parameter "inflater"

    .prologue
    .line 302
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    if-nez v1, :cond_0

    .line 303
    new-instance v0, Lcom/google/research/handwriting/gui/CandidateViewHandler;

    invoke-direct {v0, p1}, Lcom/google/research/handwriting/gui/CandidateViewHandler;-><init>(Landroid/view/LayoutInflater;)V

    .line 304
    .local v0, candidateViewHandler:Lcom/google/research/handwriting/gui/CandidateViewHandler;
    sget v1, Lcom/google/android/apps/translate/R$layout;->handwriting_input_view:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    iput-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1, v0, v2, p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->initialize(Lcom/google/research/handwriting/gui/CandidateViewHandler;Landroid/widget/EditText;Lcom/google/android/apps/translate/editor/InputMethodView;)V

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setCallback(Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;)V

    .line 312
    .end local v0           #candidateViewHandler:Lcom/google/research/handwriting/gui/CandidateViewHandler;
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->initializeEditText(Landroid/widget/EditText;)V

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setVisibility(I)V

    goto :goto_0
.end method

.method private initializeVoiceInputView(Landroid/view/LayoutInflater;)V
    .locals 1
    .parameter "inflater"

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/TranslateApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/TranslateApplication;->getVoiceInputHelper()Lcom/google/android/apps/translate/VoiceInputHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    .line 329
    return-void
.end method

.method static logInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V
    .locals 3
    .parameter "inputMethod"

    .prologue
    .line 627
    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$17;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 641
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->UNKNOWN:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    .line 644
    .local v0, im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    :goto_0
    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->UNKNOWN:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    if-eq v0, v1, :cond_0

    .line 645
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationInputMethod(Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 647
    :cond_0
    return-void

    .line 629
    .end local v0           #im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->SPEECH:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    .line 630
    .restart local v0       #im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    goto :goto_0

    .line 632
    .end local v0           #im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->HANDWRITING:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    .line 633
    .restart local v0       #im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    goto :goto_0

    .line 635
    .end local v0           #im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->VIRTUAL_KEYBOARD:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    .line 636
    .restart local v0       #im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    goto :goto_0

    .line 638
    .end local v0           #im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->CAMERA:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    .line 639
    .restart local v0       #im:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;
    goto :goto_0

    .line 627
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private render()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 371
    const-string v3, "InputMethodView"

    const-string v4, "render"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputMethodPlaceholder:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 374
    sget-object v3, Lcom/google/android/apps/translate/editor/InputMethodView$17;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 426
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 376
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputMethodPlaceholder:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 377
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    const/4 v3, -0x2

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 378
    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 379
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputMethodPlaceholder:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 380
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputMethodPlaceholder:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->invalidate()V

    goto :goto_0

    .line 395
    .end local v1           #params:Landroid/view/ViewGroup$LayoutParams;
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Profile;->isHandwritingSupported(Landroid/content/Context;Lcom/google/android/apps/translate/Language;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 396
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputMethodPlaceholder:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 397
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 398
    .restart local v1       #params:Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 400
    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 401
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 402
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->invalidate()V

    .line 403
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->slideUpInputMethodView()V

    goto :goto_0

    .line 407
    .end local v1           #params:Landroid/view/ViewGroup$LayoutParams;
    :pswitch_3
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mStartedInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    sget-object v4, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->CAMERA:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne v3, v4, :cond_1

    .line 408
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->restartCameraView()V

    goto :goto_0

    .line 410
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 412
    .local v0, inflater:Landroid/view/LayoutInflater;
    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->initializeCameraInputView(Landroid/view/LayoutInflater;)V

    .line 413
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->setCameraViewHeight()V

    .line 414
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/google/android/apps/translate/Profile;->getCameraLogging(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->setImageLogging(Z)V

    .line 415
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 416
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/google/android/apps/translate/Profile;->getCameraInputFrontendUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 417
    .local v2, url:Ljava/lang/String;
    const-string v3, "InputMethodView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "camera input frontend url="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->setFrontendUrl(Ljava/lang/String;)V

    .line 420
    .end local v2           #url:Ljava/lang/String;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->startCameraInput()V

    goto/16 :goto_0

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private restartCameraView()V
    .locals 3

    .prologue
    .line 452
    const-string v0, "InputMethodView"

    const-string v1, "restartCameraView"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->PAUSE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 454
    new-instance v0, Lcom/google/android/apps/translate/editor/InputMethodView$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$6;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 460
    return-void
.end method

.method private setCameraViewHeight()V
    .locals 5

    .prologue
    .line 510
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraHeight:I

    .line 512
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v1, v2, v3

    .line 513
    .local v1, translateViewHeight:I
    const-string v2, "InputMethodView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "translateViewHeight="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 515
    .local v0, params:Landroid/view/ViewGroup$LayoutParams;
    iget v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraHeight:I

    sub-int v2, v1, v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 516
    const-string v2, "InputMethodView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CAMERA mCameraHeight="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const-string v2, "InputMethodView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CAMERA params.height="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 519
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 520
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    iget v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraHeight:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->setImeControlsHeight(I)V

    .line 521
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->invalidate()V

    .line 522
    const-string v2, "InputMethodView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "params.height"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    const-string v2, "InputMethodView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "params.width"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    const-string v2, "InputMethodView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mCameraInputView.getHeight()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v4}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const-string v2, "InputMethodView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mCameraInputView.getWidth()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v4}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    return-void
.end method

.method public static setSoftwareKeyboardAvailable(Landroid/app/Activity;Z)V
    .locals 4
    .parameter "activity"
    .parameter "available"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 607
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 609
    .local v0, m:Landroid/view/inputmethod/InputMethodManager;
    if-nez v0, :cond_0

    .line 623
    :goto_0
    return-void

    .line 612
    :cond_0
    if-eqz p1, :cond_1

    .line 613
    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    goto :goto_0

    .line 621
    :cond_1
    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    goto :goto_0
.end method

.method private declared-synchronized showCameraLoggingDialog(Landroid/app/Activity;Z)V
    .locals 9
    .parameter "activity"
    .parameter "forceShow"

    .prologue
    .line 1015
    monitor-enter p0

    if-eqz p2, :cond_1

    .line 1016
    const/4 v7, 0x1

    :try_start_0
    invoke-static {p1, v7}, Lcom/google/android/apps/translate/Profile;->setShowCameraLoggingDialog(Landroid/content/Context;Z)V

    .line 1022
    :cond_0
    const-string v7, "layout_inflater"

    invoke-virtual {p1, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 1024
    .local v4, inflater:Landroid/view/LayoutInflater;
    sget v7, Lcom/google/android/apps/translate/R$layout;->camera_logging_dialog:I

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1026
    .local v3, dialogContentView:Landroid/view/View;
    sget v7, Lcom/google/android/apps/translate/R$id;->camera_logging_checkbox:I

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 1029
    .local v2, cb:Landroid/widget/CheckBox;
    invoke-static {p1}, Lcom/google/android/apps/translate/Profile;->getCameraLogging(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1030
    new-instance v7, Lcom/google/android/apps/translate/editor/InputMethodView$14;

    invoke-direct {v7, p0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView$14;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;Landroid/app/Activity;)V

    invoke-virtual {v2, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1037
    sget v7, Lcom/google/android/apps/translate/R$string;->label_ok:I

    invoke-virtual {p1, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1038
    .local v6, okBtnLabel:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    sget v7, Lcom/google/android/apps/translate/R$string;->label_camera_input:I

    invoke-virtual {p1, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1040
    .local v5, msg:Ljava/lang/StringBuilder;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1041
    .local v1, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1045
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 1050
    new-instance v7, Lcom/google/android/apps/translate/editor/InputMethodView$15;

    invoke-direct {v7, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$15;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1058
    new-instance v7, Lcom/google/android/apps/translate/editor/InputMethodView$16;

    invoke-direct {v7, p0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView$16;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;Landroid/app/Activity;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1066
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1067
    .local v0, alert:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1068
    .end local v0           #alert:Landroid/app/AlertDialog;
    .end local v1           #builder:Landroid/app/AlertDialog$Builder;
    .end local v2           #cb:Landroid/widget/CheckBox;
    .end local v3           #dialogContentView:Landroid/view/View;
    .end local v4           #inflater:Landroid/view/LayoutInflater;
    .end local v5           #msg:Ljava/lang/StringBuilder;
    .end local v6           #okBtnLabel:Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return-void

    .line 1018
    :cond_1
    :try_start_1
    invoke-static {p1}, Lcom/google/android/apps/translate/Profile;->getShowCameraLoggingDialog(Landroid/content/Context;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    if-nez v7, :cond_0

    goto :goto_0

    .line 1015
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method private showInputSelector(Z)V
    .locals 2
    .parameter "animate"

    .prologue
    .line 275
    const-string v0, "InputMethodView"

    const-string v1, "showInputSelector"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    if-nez v0, :cond_1

    .line 278
    if-eqz p1, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputSelector:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->slideDown(Landroid/view/View;)V

    .line 290
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputSelector:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 284
    :cond_1
    if-eqz p1, :cond_2

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputSelector:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->slideUp(Landroid/view/View;)V

    goto :goto_0

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputSelector:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private slideDown(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    const/4 v4, 0x0

    .line 863
    const-string v2, "InputMethodView"

    const-string v3, "slideDown"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 888
    :goto_0
    return-void

    .line 868
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getHeight()I

    move-result v0

    .line 869
    .local v0, height:I
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v0

    invoke-direct {v1, v4, v4, v4, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 870
    .local v1, slideDown:Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 871
    new-instance v2, Lcom/google/android/apps/translate/editor/InputMethodView$13;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView$13;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 887
    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private slideDownInputMethodView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 803
    const-string v2, "InputMethodView"

    const-string v3, "slideDownInputMethodView"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getHeight()I

    move-result v0

    .line 805
    .local v0, height:I
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v0

    invoke-direct {v1, v4, v4, v4, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 806
    .local v1, slideDown:Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 807
    new-instance v2, Lcom/google/android/apps/translate/editor/InputMethodView$11;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$11;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 830
    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 831
    return-void
.end method

.method private slideUp(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    const/4 v4, 0x0

    .line 835
    const-string v2, "InputMethodView"

    const-string v3, "slideUp"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 860
    :goto_0
    return-void

    .line 840
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 841
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 842
    .local v0, height:I
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v0

    invoke-direct {v1, v4, v4, v2, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 843
    .local v1, slideUp:Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 844
    new-instance v2, Lcom/google/android/apps/translate/editor/InputMethodView$12;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$12;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 859
    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private slideUpInputMethodView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 771
    const-string v2, "InputMethodView"

    const-string v3, "slideUpInputMethodView"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getHeight()I

    move-result v0

    .line 773
    .local v0, height:I
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v0

    invoke-direct {v1, v4, v4, v2, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 774
    .local v1, slideUp:Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 775
    new-instance v2, Lcom/google/android/apps/translate/editor/InputMethodView$10;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$10;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 799
    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 800
    return-void
.end method

.method private startCameraInput()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->startInput(Landroid/view/inputmethod/EditorInfo;Ljava/lang/String;)Z

    .line 434
    :goto_0
    return-void

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->startInput(Landroid/view/inputmethod/EditorInfo;Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 912
    const-string v0, "InputMethodView"

    const-string v1, "cancel"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->clearSelection()V

    .line 914
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$17;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 932
    :goto_0
    :pswitch_0
    return-void

    .line 916
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->clearLimboState()V

    .line 917
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->resetStrokes(Z)V

    goto :goto_0

    .line 920
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mVoiceInput:Lcom/google/android/apps/translate/VoiceInput;

    if-eqz v0, :cond_0

    .line 921
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mVoiceInput:Lcom/google/android/apps/translate/VoiceInput;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/VoiceInput;->cancel()V

    .line 923
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->showInputSelector(Z)V

    goto :goto_0

    .line 914
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getCurrentInputMethod()Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    return-object v0
.end method

.method public getEditingAreaViewHeight()I
    .locals 4

    .prologue
    .line 463
    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$17;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 481
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    :cond_0
    :goto_0
    return v0

    .line 465
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v0, v2, 0x2

    .line 467
    .local v0, maxHeight:I
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 472
    .local v1, translateViewHeight:I
    if-lt v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    .line 475
    .end local v0           #maxHeight:I
    .end local v1           #translateViewHeight:I
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v0, v2, 0x2

    goto :goto_0

    .line 463
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public hasInputMethodShown()Z
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z
    .locals 6
    .parameter "event"

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 547
    const-string v3, "InputMethodView"

    const-string v4, "hideCurrentInputMethod"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    if-nez v3, :cond_0

    .line 603
    :goto_0
    return v2

    .line 551
    :cond_0
    sget-object v3, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    iput-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mStartedInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    .line 552
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/translate/editor/TextSlot;->setIsTextEditor(Z)V

    .line 553
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->cancel()V

    .line 554
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    if-eqz v3, :cond_1

    .line 555
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->commitSourceText()V

    .line 557
    :cond_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 558
    .local v0, hidden:Ljava/lang/Boolean;
    sget-object v3, Lcom/google/android/apps/translate/editor/InputMethodView$17;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    iget-object v4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 597
    invoke-direct {p0, v5}, Lcom/google/android/apps/translate/editor/InputMethodView;->showInputSelector(Z)V

    .line 599
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 602
    :goto_1
    :pswitch_0
    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    iput-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    .line 603
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto :goto_0

    .line 565
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->onEditCompleted(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V

    .line 566
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->slideDownInputMethodView()V

    goto :goto_1

    .line 569
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->finishInput()V

    .line 570
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 571
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 572
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 573
    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 575
    invoke-direct {p0, v5}, Lcom/google/android/apps/translate/editor/InputMethodView;->showInputSelector(Z)V

    goto :goto_1

    .line 579
    .end local v1           #params:Landroid/view/ViewGroup$LayoutParams;
    :pswitch_3
    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->setSoftwareKeyboardAvailable(Landroid/app/Activity;Z)V

    .line 580
    new-instance v2, Lcom/google/android/apps/translate/editor/InputMethodView$8;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$8;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->post(Ljava/lang/Runnable;)Z

    .line 592
    invoke-direct {p0, v5}, Lcom/google/android/apps/translate/editor/InputMethodView;->showInputSelector(Z)V

    goto :goto_1

    .line 558
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public init(Landroid/app/Activity;Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/editor/TextSlot;Lcom/google/android/apps/translate/asreditor/AsrResultEditor;Ljava/lang/String;Lcom/google/android/apps/translate/editor/EditPanelView;)V
    .locals 9
    .parameter "activity"
    .parameter "instantTranslateHandler"
    .parameter "sourceLanguage"
    .parameter "targetLanguage"
    .parameter "editorField"
    .parameter "asrResultEditor"
    .parameter "locale"
    .parameter "editPanel"

    .prologue
    .line 148
    const-string v6, "InputMethodView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "init from="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p4}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    .line 151
    iput-object p2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    .line 152
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mPreviousSourceLanguage:Lcom/google/android/apps/translate/Language;

    .line 153
    iput-object p3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    .line 154
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mPreviousTargetLanguage:Lcom/google/android/apps/translate/Language;

    .line 155
    iput-object p4, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    .line 156
    iput-object p5, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    .line 157
    iput-object p6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

    .line 158
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mVoiceLocale:Ljava/lang/String;

    .line 159
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

    .line 160
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_selector:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputSelector:Landroid/view/View;

    .line 161
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_placeholder:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mInputMethodPlaceholder:Landroid/widget/LinearLayout;

    .line 162
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 165
    .local v3, inflater:Landroid/view/LayoutInflater;
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_keyboard_button:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mKeyboardBtn:Landroid/widget/ImageButton;

    .line 166
    const/4 v6, 0x0

    iput v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    .line 178
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mKeyboardBtn:Landroid/widget/ImageButton;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 181
    invoke-direct {p0, v3}, Lcom/google/android/apps/translate/editor/InputMethodView;->initializeVoiceInputView(Landroid/view/LayoutInflater;)V

    .line 182
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_mic_button:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mMicBtn:Landroid/widget/ImageButton;

    .line 183
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    iget-object v7, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/translate/VoiceInputHelper;->isVoiceInputAvailable(Lcom/google/android/apps/translate/Language;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 184
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mMicBtn:Landroid/widget/ImageButton;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 185
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mMicBtn:Landroid/widget/ImageButton;

    new-instance v7, Lcom/google/android/apps/translate/editor/InputMethodView$2;

    invoke-direct {v7, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$2;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    .line 198
    :goto_0
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_handwriting_button:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingBtn:Landroid/widget/ImageButton;

    .line 199
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    iget-object v7, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v6, v7}, Lcom/google/android/apps/translate/Profile;->isHandwritingSupported(Landroid/content/Context;Lcom/google/android/apps/translate/Language;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 200
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingBtn:Landroid/widget/ImageButton;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 201
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingBtn:Landroid/widget/ImageButton;

    new-instance v7, Lcom/google/android/apps/translate/editor/InputMethodView$3;

    invoke-direct {v7, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$3;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    iget v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    .line 214
    :goto_1
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_camera_button:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraBtn:Landroid/widget/ImageButton;

    .line 215
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraSupported:Z

    .line 216
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-static {v6}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->isSupported(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 217
    invoke-direct {p0, v3}, Lcom/google/android/apps/translate/editor/InputMethodView;->initializeCameraInputView(Landroid/view/LayoutInflater;)V

    .line 218
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    if-eqz v6, :cond_0

    .line 222
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v6}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->getSupportedLanguages()[Ljava/lang/String;

    move-result-object v1

    .local v1, arr$:[Ljava/lang/String;
    array-length v5, v1

    .local v5, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_2
    if-ge v2, v5, :cond_0

    aget-object v4, v1, v2

    .line 223
    .local v4, lang:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v7}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 224
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraSupported:Z

    .line 231
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v4           #lang:Ljava/lang/String;
    .end local v5           #len$:I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getRootView()Landroid/view/View;

    move-result-object v6

    sget v7, Lcom/google/android/apps/translate/R$id;->translate_app_container:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    .line 232
    iget-boolean v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraSupported:Z

    if-eqz v6, :cond_5

    .line 233
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraBtn:Landroid/widget/ImageButton;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 234
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraBtn:Landroid/widget/ImageButton;

    new-instance v7, Lcom/google/android/apps/translate/editor/InputMethodView$4;

    invoke-direct {v7, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$4;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    iget v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mIconCount:I

    .line 247
    :goto_3
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getCurrentInputMethod()Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-eq v6, v7, :cond_6

    .line 250
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideInputSelector(Z)V

    .line 254
    :goto_4
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_divider_1:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 255
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_divider_2:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 256
    sget v6, Lcom/google/android/apps/translate/R$id;->input_method_divider_3:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 260
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    if-eqz v6, :cond_1

    .line 261
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTranslateView:Landroid/view/View;

    new-instance v7, Lcom/google/android/apps/translate/editor/InputMethodView$5;

    invoke-direct {v7, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$5;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 272
    :cond_1
    return-void

    .line 195
    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mMicBtn:Landroid/widget/ImageButton;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 211
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingBtn:Landroid/widget/ImageButton;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 222
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v4       #lang:Ljava/lang/String;
    .restart local v5       #len$:I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 244
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v4           #lang:Ljava/lang/String;
    .end local v5           #len$:I
    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraBtn:Landroid/widget/ImageButton;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3

    .line 252
    :cond_6
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/google/android/apps/translate/editor/InputMethodView;->showInputSelector(Z)V

    goto :goto_4
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1113
    const-string v1, "InputMethodView"

    const-string v2, "onActivityResult"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    packed-switch p1, :pswitch_data_0

    .line 1124
    :goto_0
    return-void

    .line 1116
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getCurrentInputMethod()Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->CAMERA:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne v1, v2, :cond_0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    if-eqz v1, :cond_0

    .line 1118
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1119
    .local v0, selectedImage:Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->loadImage(Landroid/net/Uri;)V

    .line 1121
    .end local v0           #selectedImage:Landroid/net/Uri;
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mLoadingImage:Z

    goto :goto_0

    .line 1114
    nop

    :pswitch_data_0
    .packed-switch 0xb6
        :pswitch_0
    .end packed-switch
.end method

.method public onCameraError()V
    .locals 3

    .prologue
    .line 1005
    const-string v0, "InputMethodView"

    const-string v1, "Failed to open camera!"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/translate/R$string;->text_input_cannot_open_camera:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1008
    return-void
.end method

.method public onCancelVoice()V
    .locals 0

    .prologue
    .line 905
    return-void
.end method

.method public onClearText()V
    .locals 2

    .prologue
    .line 962
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$17;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 972
    :goto_0
    return-void

    .line 964
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->onClearText()V

    goto :goto_0

    .line 962
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "newConfig"

    .prologue
    .line 954
    invoke-static {}, Lcom/google/android/apps/translate/Util;->getSdkVersion()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 955
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 958
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->render()V

    .line 959
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 948
    const-string v0, "InputMethodView"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->PAUSE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 950
    return-void
.end method

.method public onEditPanelHeightChanged(I)V
    .locals 3
    .parameter "actionBarHeight"

    .prologue
    .line 486
    new-instance v0, Lcom/google/android/apps/translate/editor/InputMethodView$7;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView$7;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;I)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 506
    return-void
.end method

.method public onError()V
    .locals 0

    .prologue
    .line 909
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1072
    if-nez p2, :cond_1

    .line 1094
    :cond_0
    :goto_0
    return v0

    .line 1075
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1077
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getCurrentInputMethod()Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->CAMERA:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne v2, v3, :cond_0

    .line 1078
    const-string v0, "InputMethodView"

    const-string v2, "onKeyPreIme CAMERA!"

    invoke-static {v0, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 1080
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1081
    :cond_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 1082
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraInputView:Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/unveil/textinput/compatible/TextInputView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1084
    goto :goto_0

    .line 1088
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->getCurrentInputMethod()Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->CAMERA:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne v2, v3, :cond_0

    .line 1089
    const-string v0, "InputMethodView"

    const-string v2, "onKeyPreIme FOCUS!"

    invoke-static {v0, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1091
    goto :goto_0

    .line 1075
    nop

    :sswitch_data_0
    .sparse-switch
        0x1b -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyboardButton()V
    .locals 1

    .prologue
    .line 990
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 991
    return-void
.end method

.method public onLoadImage()V
    .locals 3

    .prologue
    .line 1099
    const-string v1, "InputMethodView"

    const-string v2, "onLoadImage"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1102
    .local v0, intent:Landroid/content/Intent;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mLoadingImage:Z

    .line 1103
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    const/16 v2, 0xb6

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1104
    return-void
.end method

.method public onLoadImageError()V
    .locals 2

    .prologue
    .line 1108
    const-string v0, "InputMethodView"

    const-string v1, "onLoadImageError"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/translate/R$string;->text_input_cannot_load_image:I

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;I)V

    .line 1110
    return-void
.end method

.method public onNetworkError(I)V
    .locals 3
    .parameter "statusCode"

    .prologue
    .line 998
    const-string v0, "InputMethodView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Camera input failed due to network error. status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/translate/R$string;->msg_network_error:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1001
    return-void
.end method

.method public onPause()Z
    .locals 2

    .prologue
    .line 939
    const-string v0, "InputMethodView"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mLoadingImage:Z

    if-eqz v0, :cond_0

    .line 941
    const/4 v0, 0x1

    .line 944
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTextSelected(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .parameter "text"
    .parameter "alternatives"

    .prologue
    .line 979
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 983
    :goto_0
    return-void

    .line 982
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->replaceSelectedText(Ljava/lang/String;Z)Ljava/lang/String;

    goto :goto_0
.end method

.method public onVoiceResults(Ljava/util/List;ZZ)V
    .locals 0
    .parameter
    .parameter "canceled"
    .parameter "finished"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 898
    .local p1, recognitionResults:Ljava/util/List;,"Ljava/util/List<Ljava/lang/CharSequence;>;"
    return-void
.end method

.method public restartInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V
    .locals 2
    .parameter "inputMethod"

    .prologue
    .line 650
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$17;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 661
    :goto_0
    return-void

    .line 654
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    goto :goto_0

    .line 650
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V
    .locals 6
    .parameter "inputMethod"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 664
    const-string v1, "InputMethodView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startInputMethod: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/translate/editor/EditPanelView;->onInputMethodStart(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 768
    :goto_0
    return-void

    .line 671
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/translate/editor/TextSlot;->setIsTextEditor(Z)V

    .line 672
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-eq v1, p1, :cond_1

    .line 673
    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->SWITCH:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 674
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    .line 676
    :cond_1
    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$17;->$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod:[I

    invoke-virtual {p1}, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 760
    invoke-direct {p0, v5}, Lcom/google/android/apps/translate/editor/InputMethodView;->showInputSelector(Z)V

    .line 761
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->onInputMethodReady(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 764
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mStartedInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-eq v1, p1, :cond_3

    .line 765
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditorField:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->beginEditing()V

    .line 767
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    iput-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mStartedInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    goto :goto_0

    .line 679
    :pswitch_0
    invoke-direct {p0, v4}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideInputSelector(Z)V

    .line 683
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Profile;->isHandwritingSupported(Landroid/content/Context;Lcom/google/android/apps/translate/Language;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 684
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mStartedInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->HANDWRITING:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mPreviousSourceLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mPreviousSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mPreviousTargetLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    if-ne v1, v2, :cond_5

    .line 688
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 690
    .local v0, inflater:Landroid/view/LayoutInflater;
    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/editor/InputMethodView;->initializeHandwritingInputView(Landroid/view/LayoutInflater;)V

    .line 691
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setSourceAndTargetLanguages(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 693
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->render()V

    goto :goto_1

    .line 695
    .end local v0           #inflater:Landroid/view/LayoutInflater;
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mHandwritingInputView:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setSourceAndTargetLanguages(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    goto :goto_1

    .line 699
    :cond_6
    const-string v1, "InputMethodView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handwriting not supported for language="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/Language;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    goto :goto_1

    .line 713
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->onInputMethodReady(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 714
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translate/VoiceInputHelper;->startVoiceInput(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;)V

    .line 716
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->render()V

    goto/16 :goto_1

    .line 719
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/translate/editor/InputMethodView;->showCameraLoggingDialog(Landroid/app/Activity;Z)V

    .line 721
    invoke-direct {p0, v4}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideInputSelector(Z)V

    .line 725
    iget-boolean v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mCameraSupported:Z

    if-eqz v1, :cond_8

    .line 726
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mStartedInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->CAMERA:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    if-ne v1, v2, :cond_7

    .line 727
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mPreviousSourceLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mPreviousSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    if-eq v1, v2, :cond_2

    .line 729
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->changeCameraLanguage()V

    goto/16 :goto_1

    .line 732
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->render()V

    .line 733
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mEditPanel:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActiveInputMethod:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->onInputMethodReady(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    goto/16 :goto_1

    .line 736
    :cond_8
    const-string v1, "InputMethodView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Camera not supported for language="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/Language;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    goto/16 :goto_1

    .line 743
    :pswitch_3
    invoke-direct {p0, v4}, Lcom/google/android/apps/translate/editor/InputMethodView;->hideInputSelector(Z)V

    .line 745
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mKeyboardBtn:Landroid/widget/ImageButton;

    sget v2, Lcom/google/android/apps/translate/R$drawable;->input_keyboard_selected:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 746
    invoke-direct {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->render()V

    .line 747
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/InputMethodView;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v5}, Lcom/google/android/apps/translate/editor/InputMethodView;->setSoftwareKeyboardAvailable(Landroid/app/Activity;Z)V

    .line 749
    new-instance v1, Lcom/google/android/apps/translate/editor/InputMethodView$9;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/editor/InputMethodView$9;-><init>(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/apps/translate/editor/InputMethodView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 756
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InputMethodView;->invalidate()V

    goto/16 :goto_1

    .line 676
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
