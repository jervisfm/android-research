.class public Lcom/google/android/apps/translate/editor/InstantTranslateHandler;
.super Ljava/lang/Object;
.source "InstantTranslateHandler.java"

# interfaces
.implements Lcom/google/android/apps/translate/VoiceInput$UiListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;,
        Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;,
        Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TextUpdateHandler;,
        Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;
    }
.end annotation


# static fields
.field private static final PLACEHOLDER:Ljava/lang/String; = "\u2026"

.field private static final TAG:Ljava/lang/String; = "InstantTranslateHandler"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCallback:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;

.field private mDetectedSrcShortLangName:Ljava/lang/String;

.field private mIsStopped:Z

.field private mSourceLanguage:Lcom/google/android/apps/translate/Language;

.field private mSpellCorrection:Landroid/text/SpannableStringBuilder;

.field private mSrcHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;

.field private mSrcTextComposing:Landroid/text/SpannableStringBuilder;

.field private mTargetLanguage:Lcom/google/android/apps/translate/Language;

.field private mTranslateComposing:Landroid/text/SpannableStringBuilder;

.field private mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

.field private mTranslateThread:Ljava/lang/Thread;

.field private mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateComposing:Landroid/text/SpannableStringBuilder;

    .line 35
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcTextComposing:Landroid/text/SpannableStringBuilder;

    .line 36
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSpellCorrection:Landroid/text/SpannableStringBuilder;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mDetectedSrcShortLangName:Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mIsStopped:Z

    .line 41
    new-instance v0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;-><init>(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Lcom/google/android/apps/translate/editor/InstantTranslateHandler$1;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;

    .line 43
    new-instance v0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$1;-><init>(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateThread:Ljava/lang/Thread;

    .line 167
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Lcom/google/android/apps/translate/Language;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;)Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->doTextTranslateSync(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Landroid/text/SpannableStringBuilder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSpellCorrection:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSpellCorrection:Landroid/text/SpannableStringBuilder;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mDetectedSrcShortLangName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mDetectedSrcShortLangName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mCallback:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Landroid/text/SpannableStringBuilder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcTextComposing:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->setTranslateText(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Landroid/text/SpannableStringBuilder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateComposing:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateComposing:Landroid/text/SpannableStringBuilder;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;)Lcom/google/android/apps/translate/Language;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method private doTextTranslateSync(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V
    .locals 12
    .parameter "from"
    .parameter "to"
    .parameter "text"

    .prologue
    const/4 v11, 0x1

    .line 326
    iget-boolean v7, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mIsStopped:Z

    if-eqz v7, :cond_0

    .line 368
    :goto_0
    return-void

    .line 329
    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 331
    iget-object v7, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    const-string v8, ""

    const-string v9, ""

    const-string v10, ""

    invoke-virtual {v7, p3, v8, v9, v10}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;->handleTranslationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 339
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    monitor-enter v8

    .line 340
    :try_start_0
    const-string v7, "InstantTranslateHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doTextTranslateSync from="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " text="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v7, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    invoke-interface {v7, p1, p2}, Lcom/google/android/apps/translate/TranslateManager;->setLanguagePair(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 342
    iget-object v7, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    const/4 v9, 0x1

    invoke-interface {v7, v9}, Lcom/google/android/apps/translate/TranslateManager;->setInstantTransalte(Z)V

    .line 343
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 344
    .local v4, start:J
    iget-object v7, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    invoke-interface {v7, p3}, Lcom/google/android/apps/translate/TranslateManager;->doTranslate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 345
    .local v3, result:Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 346
    .local v0, end:J
    sub-long v9, v0, v4

    .line 347
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    invoke-static {v3}, Lcom/google/android/apps/translate/Translate;->getResultCode(Ljava/lang/String;)I

    move-result v2

    .line 353
    .local v2, error:I
    sparse-switch v2, :sswitch_data_0

    .line 365
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "translation error happened error="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 347
    .end local v0           #end:J
    .end local v2           #error:I
    .end local v3           #result:Ljava/lang/String;
    .end local v4           #start:J
    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 356
    .restart local v0       #end:J
    .restart local v2       #error:I
    .restart local v3       #result:Ljava/lang/String;
    .restart local v4       #start:J
    :sswitch_0
    new-instance v6, Lcom/google/android/apps/translate/Translate$TranslateResult;

    invoke-direct {v6, v3, v11}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Ljava/lang/String;Z)V

    .line 357
    .local v6, transalteResult:Lcom/google/android/apps/translate/Translate$TranslateResult;
    iget-object v7, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getSpellCorrection()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getDetectedSrcShortLangName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, p3, v8, v9, v10}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;->handleTranslationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v7, "translated text: "

    invoke-virtual {v6}, Lcom/google/android/apps/translate/Translate$TranslateResult;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 353
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_0
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method static equalsExceptForDots(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .parameter "s1"
    .parameter "s2"

    .prologue
    .line 393
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v0, v2, v3

    .line 394
    .local v0, lenDiff:I
    if-nez v0, :cond_0

    .line 395
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 401
    .end local p0
    :goto_0
    return v2

    .line 396
    .restart local p0
    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 397
    new-instance v1, Ljava/lang/StringBuilder;

    if-lez v0, :cond_1

    move-object v2, p1

    :goto_1
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 398
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string v2, "\u2026"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    if-lez v0, :cond_2

    .end local p0
    :goto_2
    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0

    .end local v1           #sb:Ljava/lang/StringBuilder;
    .restart local p0
    :cond_1
    move-object v2, p0

    .line 397
    goto :goto_1

    .restart local v1       #sb:Ljava/lang/StringBuilder;
    :cond_2
    move-object p0, p1

    .line 399
    goto :goto_2

    .line 401
    .end local v1           #sb:Ljava/lang/StringBuilder;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setTranslateText(Z)V
    .locals 2
    .parameter "setEllipsis"

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$2;-><init>(Lcom/google/android/apps/translate/editor/InstantTranslateHandler;Z)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 283
    return-void
.end method


# virtual methods
.method public commitSourceText()V
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mIsStopped:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    if-nez v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;->commit()V

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;->commit()V

    goto :goto_0
.end method

.method public getTranslationText(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "srcText"

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcTextComposing:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateComposing:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public init(Landroid/app/Activity;Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 2
    .parameter "activity"
    .parameter "callback"
    .parameter "sourceLanguage"
    .parameter "targetLanguage"

    .prologue
    .line 61
    const-string v0, "InstantTranslateHandler"

    const-string v1, "InstantTranslateHandler#init"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iput-object p2, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mCallback:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;

    .line 63
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mActivity:Landroid/app/Activity;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/TranslateApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/TranslateApplication;->getTranslateManager()Lcom/google/android/apps/translate/TranslateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    .line 66
    iput-object p3, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    .line 67
    iput-object p4, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mIsStopped:Z

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->reset()V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    const-string v0, "InstantTranslateHandler"

    const-string v1, "InstantTranslateHandler#init => mTranslateThread.start()"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 76
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;->updateSourceText()V

    .line 77
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;->init()V

    goto :goto_0
.end method

.method public isStopped()Z
    .locals 1

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mIsStopped:Z

    return v0
.end method

.method public onCancelVoice()V
    .locals 0

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->commitSourceText()V

    .line 319
    return-void
.end method

.method public onError()V
    .locals 0

    .prologue
    .line 323
    return-void
.end method

.method public onVoiceResults(Ljava/util/List;ZZ)V
    .locals 0
    .parameter
    .parameter "canceled"
    .parameter "finished"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 304
    .local p1, recognitionResults:Ljava/util/List;,"Ljava/util/List<Ljava/lang/CharSequence;>;"
    return-void
.end method

.method public declared-synchronized reset()V
    .locals 1

    .prologue
    .line 286
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcTextComposing:Landroid/text/SpannableStringBuilder;

    .line 287
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTranslateComposing:Landroid/text/SpannableStringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    monitor-exit p0

    return-void

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mIsStopped:Z

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;->stop()V

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mTrgHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$TargetTextUpdateHandler;->stop()V

    .line 389
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->reset()V

    .line 390
    return-void
.end method

.method public updateSourceText(Ljava/lang/CharSequence;)V
    .locals 1
    .parameter "newSourceText"

    .prologue
    .line 312
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcTextComposing:Landroid/text/SpannableStringBuilder;

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->mSrcHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler$SourceTextUpdateHandler;->updateSourceText()V

    .line 314
    return-void
.end method
