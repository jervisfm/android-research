.class Lcom/google/android/apps/translate/editor/SuggestAdapter$1;
.super Ljava/lang/Object;
.source "SuggestAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/editor/SuggestAdapter;->getHistorySuggestions(Ljava/lang/CharSequence;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/translate/history/Entry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/editor/SuggestAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$1;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/history/Entry;)I
    .locals 8
    .parameter "e1"
    .parameter "e2"

    .prologue
    const-wide/16 v6, 0x0

    .line 124
    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/Entry;->getAccessedTime()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getAccessedTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 125
    .local v0, d:J
    cmp-long v2, v0, v6

    if-nez v2, :cond_0

    .line 126
    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/Entry;->getCreatedTime()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getCreatedTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 128
    :cond_0
    cmp-long v2, v0, v6

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 130
    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    int-to-long v0, v2

    .line 132
    :cond_1
    cmp-long v2, v0, v6

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 121
    check-cast p1, Lcom/google/android/apps/translate/history/Entry;

    .end local p1
    check-cast p2, Lcom/google/android/apps/translate/history/Entry;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/translate/editor/SuggestAdapter$1;->compare(Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/history/Entry;)I

    move-result v0

    return v0
.end method
