.class public Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
.super Ljava/lang/Object;
.source "SuggestAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/editor/SuggestAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SuggestEntry"
.end annotation


# instance fields
.field private mEntry:Lcom/google/android/apps/translate/history/Entry;

.field private mHtmlInputText:Ljava/lang/String;

.field private mSourceText:Ljava/lang/String;

.field private mType:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "entry"
    .parameter "type"
    .parameter "htmlInputText"
    .parameter "sourceText"

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->mEntry:Lcom/google/android/apps/translate/history/Entry;

    .line 67
    iput-object p2, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->mType:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    .line 68
    iput-object p3, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->mHtmlInputText:Ljava/lang/String;

    .line 69
    invoke-static {p4}, Lcom/google/android/apps/translate/translation/TranslateEntry;->normalizeInputText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->mSourceText:Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method public getEntry()Lcom/google/android/apps/translate/history/Entry;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->mEntry:Lcom/google/android/apps/translate/history/Entry;

    return-object v0
.end method

.method public getHtmlInputText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->mHtmlInputText:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->mSourceText:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->mType:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    return-object v0
.end method
