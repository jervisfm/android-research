.class Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;
.super Landroid/widget/Filter;
.source "SuggestAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/editor/SuggestAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuggestFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/translate/editor/SuggestAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/translate/editor/SuggestAdapter;Lcom/google/android/apps/translate/editor/SuggestAdapter$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;-><init>(Lcom/google/android/apps/translate/editor/SuggestAdapter;)V

    return-void
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .parameter "resultValue"

    .prologue
    .line 207
    check-cast p1, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .end local p1
    invoke-virtual {p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 12
    .parameter "constraint"

    .prologue
    .line 146
    const-string v9, "SuggestAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "performFiltering constraint="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v6, 0x0

    .line 148
    .local v6, spellCrrectionShown:Z
    const/4 v0, 0x0

    .line 149
    .local v0, detectedLanguageShown:Z
    const/4 v4, 0x0

    .line 150
    .local v4, results:Landroid/widget/Filter$FilterResults;
    if-eqz p1, :cond_4

    .line 152
    :try_start_0
    iget-object v9, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    #calls: Lcom/google/android/apps/translate/editor/SuggestAdapter;->getHistorySuggestions(Ljava/lang/CharSequence;)Ljava/util/List;
    invoke-static {v9, p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$000(Lcom/google/android/apps/translate/editor/SuggestAdapter;Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v8

    .line 153
    .local v8, suggestList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    iget-object v9, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    #getter for: Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-static {v9}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$100(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 154
    const/4 v1, 0x0

    .line 155
    .local v1, duplicateFound:Z
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 156
    .local v7, suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-virtual {v7}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    #getter for: Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-static {v10}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$100(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 158
    const/4 v1, 0x1

    .line 162
    .end local v7           #suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    :cond_1
    if-nez v1, :cond_2

    .line 163
    iget-object v9, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    #getter for: Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-static {v9}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$100(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    const/4 v6, 0x1

    .line 171
    .end local v1           #duplicateFound:Z
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_2
    :goto_0
    iget-object v9, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    #getter for: Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestFilter:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;
    invoke-static {v9}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$300(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 172
    iget-object v9, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    #getter for: Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestFilter:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;
    invoke-static {v9}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$300(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

    move-result-object v9

    invoke-interface {v9, v8}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;->filter(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 174
    :cond_3
    if-eqz v8, :cond_4

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 175
    new-instance v5, Landroid/widget/Filter$FilterResults;

    invoke-direct {v5}, Landroid/widget/Filter$FilterResults;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    .end local v4           #results:Landroid/widget/Filter$FilterResults;
    .local v5, results:Landroid/widget/Filter$FilterResults;
    :try_start_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    iput v9, v5, Landroid/widget/Filter$FilterResults;->count:I

    .line 177
    iput-object v8, v5, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v4, v5

    .line 185
    .end local v5           #results:Landroid/widget/Filter$FilterResults;
    .end local v8           #suggestList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    .restart local v4       #results:Landroid/widget/Filter$FilterResults;
    :cond_4
    :goto_1
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->SPELL_CORRECTION_SHOWN_IN_EDIT_MODE:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v9, v10, v6}, Lcom/google/android/apps/translate/UserActivityMgr;->setImpression(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 187
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->LANGID_SHOWN_IN_EDIT_MODE:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v9, v10, v0}, Lcom/google/android/apps/translate/UserActivityMgr;->setImpression(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 189
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v10

    sget-object v11, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->HISTORY_SHOWN_IN_EDIT_MODE:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    if-eqz v4, :cond_6

    iget v9, v4, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v9, :cond_6

    const/4 v9, 0x1

    :goto_2
    invoke-virtual {v10, v11, v9}, Lcom/google/android/apps/translate/UserActivityMgr;->setImpression(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 192
    return-object v4

    .line 166
    .restart local v8       #suggestList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    :cond_5
    :try_start_2
    iget-object v9, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    #getter for: Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-static {v9}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$200(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 167
    iget-object v9, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    #getter for: Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-static {v9}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$200(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 168
    const/4 v0, 0x1

    goto :goto_0

    .line 179
    .end local v8           #suggestList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    :catch_0
    move-exception v2

    .line 182
    .local v2, e:Ljava/lang/Exception;
    :goto_3
    const-string v9, "SuggestAdapter"

    const-string v10, "Cannot get suggestions"

    invoke-static {v9, v10, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 189
    .end local v2           #e:Ljava/lang/Exception;
    :cond_6
    const/4 v9, 0x0

    goto :goto_2

    .line 179
    .end local v4           #results:Landroid/widget/Filter$FilterResults;
    .restart local v5       #results:Landroid/widget/Filter$FilterResults;
    .restart local v8       #suggestList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    :catch_1
    move-exception v2

    move-object v4, v5

    .end local v5           #results:Landroid/widget/Filter$FilterResults;
    .restart local v4       #results:Landroid/widget/Filter$FilterResults;
    goto :goto_3
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3
    .parameter "constraint"
    .parameter "results"

    .prologue
    .line 198
    const-string v0, "SuggestAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in publish result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    if-eqz p2, :cond_0

    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_0

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    :goto_0
    #setter for: Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestList:Ljava/util/List;
    invoke-static {v1, v0}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->access$402(Lcom/google/android/apps/translate/editor/SuggestAdapter;Ljava/util/List;)Ljava/util/List;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;->this$0:Lcom/google/android/apps/translate/editor/SuggestAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->notifyDataSetChanged()V

    .line 203
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
