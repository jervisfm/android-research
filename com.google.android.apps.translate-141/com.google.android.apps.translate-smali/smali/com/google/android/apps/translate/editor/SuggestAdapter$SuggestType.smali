.class public final enum Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;
.super Ljava/lang/Enum;
.source "SuggestAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/editor/SuggestAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SuggestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

.field public static final enum HISTORY:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

.field public static final enum TWS_DETECTED_SRCLANG:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

.field public static final enum TWS_SPELL_CORRECTION:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    const-string v1, "HISTORY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->HISTORY:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    .line 95
    new-instance v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    const-string v1, "TWS_SPELL_CORRECTION"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_SPELL_CORRECTION:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    .line 97
    new-instance v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    const-string v1, "TWS_DETECTED_SRCLANG"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_DETECTED_SRCLANG:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    .line 91
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    sget-object v1, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->HISTORY:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_SPELL_CORRECTION:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_DETECTED_SRCLANG:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->$VALUES:[Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;
    .locals 1
    .parameter "name"

    .prologue
    .line 91
    const-class v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->$VALUES:[Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    invoke-virtual {v0}, [Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    return-object v0
.end method
