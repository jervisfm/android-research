.class public Lcom/google/android/apps/translate/editor/SuggestAdapter;
.super Landroid/widget/BaseAdapter;
.source "SuggestAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/editor/SuggestAdapter$2;,
        Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;,
        Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;,
        Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;,
        Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SuggestAdapter"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mEditorField:Landroid/widget/EditText;

.field private mLanguageList:Lcom/google/android/apps/translate/Languages;

.field private mSourceLanguage:Lcom/google/android/apps/translate/Language;

.field private mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

.field private mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

.field private mSuggestFilter:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

.field private mSuggestList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetLanguage:Lcom/google/android/apps/translate/Language;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;Landroid/widget/EditText;)V
    .locals 1
    .parameter "activity"
    .parameter "languages"
    .parameter "from"
    .parameter "to"
    .parameter "filter"
    .parameter "editorField"

    .prologue
    .line 108
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 109
    invoke-static {p1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mActivity:Landroid/app/Activity;

    .line 110
    invoke-static {p3}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/Language;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    .line 111
    invoke-static {p4}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/Language;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    .line 112
    invoke-static {p2}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/Languages;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 113
    invoke-static {p5}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestFilter:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

    .line 114
    invoke-static {p6}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mEditorField:Landroid/widget/EditText;

    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/editor/SuggestAdapter;Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getHistorySuggestions(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestFilter:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/translate/editor/SuggestAdapter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestList:Ljava/util/List;

    return-object p1
.end method

.method public static getDetectedLanguageSuggestEntry(Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    .locals 6
    .parameter "languageList"
    .parameter "from"
    .parameter "to"
    .parameter "detectedSrcShortLangName"
    .parameter "inputText"

    .prologue
    const/4 v2, 0x0

    .line 351
    const-string v3, "SuggestAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDetectedLanguageSuggestEntry "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-object v2

    .line 356
    :cond_1
    invoke-virtual {p0, p3}, Lcom/google/android/apps/translate/Languages;->getFromLanguageLongName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 357
    .local v0, longName:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 362
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 363
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string v2, "<b><i>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    const-string v2, "</i></b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    new-instance v2, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    new-instance v3, Lcom/google/android/apps/translate/history/Entry;

    const-string v4, ""

    invoke-direct {v3, p1, p2, p3, v4}, Lcom/google/android/apps/translate/history/Entry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_DETECTED_SRCLANG:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, p4}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;-><init>(Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getHistorySuggestions(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 8
    .parameter "constraint"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v4, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/translate/Util;->getInputSuggestions(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 121
    .local v2, list:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    new-instance v4, Lcom/google/android/apps/translate/editor/SuggestAdapter$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/translate/editor/SuggestAdapter$1;-><init>(Lcom/google/android/apps/translate/editor/SuggestAdapter;)V

    invoke-static {v2, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 135
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 136
    .local v3, suggestList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/history/Entry;

    .line 137
    .local v0, entry:Lcom/google/android/apps/translate/history/Entry;
    new-instance v4, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    sget-object v5, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->HISTORY:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v0, v5, v6, v7}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;-><init>(Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_0
    return-object v3
.end method

.method public static getSpellCorrectionSuggestEntry(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    .locals 5
    .parameter "from"
    .parameter "to"
    .parameter "spellCorrection"
    .parameter "inputText"

    .prologue
    .line 317
    const-string v1, "SuggestAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSpellCorrectionSuggestEntry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    const/4 v1, 0x0

    .line 322
    :goto_0
    return-object v1

    .line 321
    :cond_0
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, spellCorrectionText:Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    new-instance v2, Lcom/google/android/apps/translate/history/Entry;

    const-string v3, ""

    invoke-direct {v2, p0, p1, v0, v3}, Lcom/google/android/apps/translate/history/Entry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_SPELL_CORRECTION:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, p3}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;-><init>(Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static logSuggestionClick(Landroid/content/Context;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;Z)V
    .locals 4
    .parameter "context"
    .parameter "suggestEntry"
    .parameter "editMode"

    .prologue
    .line 378
    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->SUGGEST:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    .line 379
    .local v1, reqSrc:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->HISTORY_CLICKED_IN_EDIT_MODE:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    .line 380
    .local v0, icTag:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;
    sget-object v2, Lcom/google/android/apps/translate/editor/SuggestAdapter$2;->$SwitchMap$com$google$android$apps$translate$editor$SuggestAdapter$SuggestType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getType()Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 402
    invoke-virtual {p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getType()Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 404
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "New suggestion type ignored."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 382
    :pswitch_0
    if-eqz p2, :cond_1

    .line 383
    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->TWS_SPELL_CORRECTION_IN_EDIT_MODE:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    .line 384
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->SPELL_CORRECTION_CLICKED_IN_EDIT_MODE:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    .line 408
    :cond_0
    :goto_0
    :pswitch_1
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 409
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 410
    return-void

    .line 386
    :cond_1
    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->TWS_SPELL_CORRECTION_ON_CHIP_VIEW:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    .line 387
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->SPELL_CORRECTION_CLICKED_ON_CHIP_VIEW:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    .line 389
    goto :goto_0

    .line 391
    :pswitch_2
    if-eqz p2, :cond_2

    .line 392
    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->TWS_LANGID_IN_EDIT_MODE:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    .line 393
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->LANGID_CLICKED_IN_EDIT_MODE:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    goto :goto_0

    .line 395
    :cond_2
    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->TWS_LANGID_ON_CHIP_VIEW:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    .line 396
    sget-object v0, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->LANGID_CLICKED_ON_CHIP_VIEW:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    .line 398
    goto :goto_0

    .line 380
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 213
    new-instance v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter;-><init>(Lcom/google/android/apps/translate/editor/SuggestAdapter;Lcom/google/android/apps/translate/editor/SuggestAdapter$1;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSuggestList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 228
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 233
    const-string v13, "SuggestAdapter"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "adapter getview called at position: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    if-eqz p2, :cond_1

    move-object/from16 v4, p2

    .line 236
    .local v4, itemView:Landroid/view/View;
    :goto_0
    sget v13, Lcom/google/android/apps/translate/R$id;->text:I

    invoke-virtual {v4, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;

    .line 238
    .local v9, suggestView:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 239
    .local v6, suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    invoke-virtual {v6}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v1

    .line 240
    .local v1, entry:Lcom/google/android/apps/translate/history/Entry;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguageShortName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    .line 242
    .local v2, from:Lcom/google/android/apps/translate/Language;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getToLanguageShortName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v10

    .line 245
    .local v10, to:Lcom/google/android/apps/translate/Language;
    sget v13, Lcom/google/android/apps/translate/R$id;->ic_suggest_type:I

    invoke-virtual {v4, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 246
    .local v7, suggestTypeIconView:Landroid/view/View;
    sget v13, Lcom/google/android/apps/translate/R$id;->suggest_type:I

    invoke-virtual {v4, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 247
    .local v8, suggestTypeView:Landroid/widget/TextView;
    sget-object v13, Lcom/google/android/apps/translate/editor/SuggestAdapter$2;->$SwitchMap$com$google$android$apps$translate$editor$SuggestAdapter$SuggestType:[I

    invoke-virtual {v6}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getType()Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 262
    const/16 v13, 0x8

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 263
    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Landroid/view/View;->setVisibility(I)V

    .line 264
    sget-object v13, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v9, v13}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 268
    :goto_1
    invoke-virtual {v9}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 269
    .local v5, oldText:Ljava/lang/String;
    invoke-virtual {v6}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getHtmlInputText()Ljava/lang/String;

    move-result-object v3

    .line 270
    .local v3, htmlText:Ljava/lang/String;
    if-nez v3, :cond_2

    invoke-virtual {v6}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v13

    :goto_2
    const/4 v14, 0x2

    new-array v15, v14, [Lcom/google/android/apps/translate/Language;

    const/4 v14, 0x0

    aput-object v2, v15, v14

    const/4 v14, 0x1

    aput-object v10, v15, v14

    sget-object v16, Lcom/google/android/apps/translate/Constants$AppearanceType;->UNCHANGED:Lcom/google/android/apps/translate/Constants$AppearanceType;

    if-eqz v3, :cond_3

    const/4 v14, 0x1

    :goto_3
    move-object/from16 v0, v16

    invoke-static {v9, v13, v15, v0, v14}, Lcom/google/android/apps/translate/Util;->setTextAndFont(Landroid/widget/TextView;Ljava/lang/String;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;Z)V

    .line 277
    invoke-virtual {v6}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getType()Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    move-result-object v13

    sget-object v14, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->TWS_SPELL_CORRECTION:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    if-ne v13, v14, :cond_0

    .line 278
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mEditorField:Landroid/widget/EditText;

    const/4 v14, 0x1

    invoke-virtual {v9, v5, v13, v14}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->scrollToEdge(Ljava/lang/String;Landroid/widget/EditText;Z)V

    .line 281
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getTranslation()Ljava/lang/String;

    move-result-object v11

    .line 282
    .local v11, translation:Ljava/lang/String;
    sget v13, Lcom/google/android/apps/translate/R$id;->translation:I

    invoke-virtual {v4, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 285
    .local v12, translationView:Landroid/view/View;
    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    .line 293
    return-object v4

    .line 234
    .end local v1           #entry:Lcom/google/android/apps/translate/history/Entry;
    .end local v2           #from:Lcom/google/android/apps/translate/Language;
    .end local v3           #htmlText:Ljava/lang/String;
    .end local v4           #itemView:Landroid/view/View;
    .end local v5           #oldText:Ljava/lang/String;
    .end local v6           #suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    .end local v7           #suggestTypeIconView:Landroid/view/View;
    .end local v8           #suggestTypeView:Landroid/widget/TextView;
    .end local v9           #suggestView:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;
    .end local v10           #to:Lcom/google/android/apps/translate/Language;
    .end local v11           #translation:Ljava/lang/String;
    .end local v12           #translationView:Landroid/view/View;
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mActivity:Landroid/app/Activity;

    sget v14, Lcom/google/android/apps/translate/R$layout;->suggest_text:I

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    goto/16 :goto_0

    .line 249
    .restart local v1       #entry:Lcom/google/android/apps/translate/history/Entry;
    .restart local v2       #from:Lcom/google/android/apps/translate/Language;
    .restart local v4       #itemView:Landroid/view/View;
    .restart local v6       #suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    .restart local v7       #suggestTypeIconView:Landroid/view/View;
    .restart local v8       #suggestTypeView:Landroid/widget/TextView;
    .restart local v9       #suggestView:Lcom/google/android/apps/translate/editor/InstantTranslateTextView;
    .restart local v10       #to:Lcom/google/android/apps/translate/Language;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mActivity:Landroid/app/Activity;

    sget v14, Lcom/google/android/apps/translate/R$string;->label_translate_from:I

    invoke-virtual {v13, v14}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 251
    const/4 v13, 0x4

    invoke-virtual {v7, v13}, Landroid/view/View;->setVisibility(I)V

    .line 252
    sget-object v13, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v9, v13}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1

    .line 255
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mActivity:Landroid/app/Activity;

    sget v14, Lcom/google/android/apps/translate/R$string;->msg_did_you_mean:I

    invoke-virtual {v13, v14}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    const/4 v13, 0x4

    invoke-virtual {v7, v13}, Landroid/view/View;->setVisibility(I)V

    .line 258
    sget-object v13, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v9, v13}, Lcom/google/android/apps/translate/editor/InstantTranslateTextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_1

    .restart local v3       #htmlText:Ljava/lang/String;
    .restart local v5       #oldText:Ljava/lang/String;
    :cond_2
    move-object v13, v3

    .line 270
    goto :goto_2

    :cond_3
    const/4 v14, 0x0

    goto :goto_3

    .line 247
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setDetectedSrcShortLangName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .parameter "detectedSrcShortLangName"
    .parameter "inputText"

    .prologue
    .line 336
    const-string v0, "SuggestAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDetectedSrcShortLangName "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v0, v1, v2, p1, p2}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getDetectedLanguageSuggestEntry(Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSpellCorrection(Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Z
    .locals 3
    .parameter "spellCorrection"
    .parameter "inpuText"

    .prologue
    .line 303
    const-string v0, "SuggestAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSpellCorrection "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v1, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getSpellCorrectionSuggestEntry(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/translate/editor/SuggestAdapter;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
