.class Lcom/google/android/apps/translate/handwriting/HandwritingInputView$1;
.super Landroid/os/Handler;
.source "HandwritingInputView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/handwriting/HandwritingInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)V
    .locals 0
    .parameter

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$1;->this$0:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    const/4 v2, 0x0

    .line 228
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 230
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$1;->this$0:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    #getter for: Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHintText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->access$700(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 233
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 234
    .local v1, hint:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x1

    .line 235
    .local v0, hasHint:Z
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$1;->this$0:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    #getter for: Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHintText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->access$700(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$1;->this$0:Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    #getter for: Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHintText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->access$700(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v0, :cond_3

    .end local v1           #hint:Ljava/lang/String;
    :goto_3
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .end local v0           #hasHint:Z
    .restart local v1       #hint:Ljava/lang/String;
    :cond_1
    move v0, v2

    .line 234
    goto :goto_1

    .line 235
    .restart local v0       #hasHint:Z
    :cond_2
    const/16 v2, 0x8

    goto :goto_2

    .line 236
    :cond_3
    const-string v1, ""

    goto :goto_3

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
