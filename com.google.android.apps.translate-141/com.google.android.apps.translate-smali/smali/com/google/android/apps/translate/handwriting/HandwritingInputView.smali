.class public Lcom/google/android/apps/translate/handwriting/HandwritingInputView;
.super Landroid/widget/LinearLayout;
.source "HandwritingInputView.java"

# interfaces
.implements Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;
.implements Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;
.implements Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/google/research/handwriting/gui/UIHandlerCallback;
.implements Lcom/google/android/apps/translate/handwriting/EditTextCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;,
        Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateRecognizerHandler;,
        Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateInputConnection;
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY_TEXTBOX:Ljava/lang/String; = "textbox"

.field private static final CLIENT_NAME:Ljava/lang/String; = "atrans"

.field private static final HANDWRITING_API_RECO_URL:Ljava/lang/String; = "/request"

.field private static final HANDWRITING_API_SERVER_NAME:Ljava/lang/String; = "http://inputtools.google.com"

.field private static final MAX_STROKE_WIDTH_CHAR_BY_CHAR:F = 4.0f

.field private static final MAX_STROKE_WIDTH_WORD_BY_WORD:F = 4.0f

.field private static final MIN_STROKE_WIDTH_CHAR_BY_CHAR:F = 4.0f

.field private static final MIN_STROKE_WIDTH_WORD_BY_WORD:F = 4.0f

.field private static final MSG_HINT:I = 0x1

.field private static final SPACE_KEY_LABEL_FOR_PENDING_REQUESTS:Ljava/lang/String; = "..."

.field private static final SUPPORT_BOOKKEEPER:Z = false

.field private static final TAG:Ljava/lang/String; = null

.field private static final TALK_TO_TRANSLATE_FRONTEND:Z = true

.field private static final TRANSLATE_API_RECO_URL:Ljava/lang/String; = "/translate_a/hw"

.field private static final TRANSLATE_API_SERVER_NAME:Ljava/lang/String; = "http://translate.google.com"

.field private static final USE_ZH_WORD_HACK:Z


# instance fields
.field private mCallback:Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;

.field private mCandidateContainerView:Landroid/view/View;

.field private mCandidateViewHandler:Lcom/google/research/handwriting/gui/CandidateViewHandler;

.field private final mCodeDelete:I

.field private final mCodeKeyboard:I

.field private final mCodeSpace:I

.field private mContext:Landroid/content/Context;

.field private mContinuousWritingHandler:Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

.field private mContinuousWritingManager:Lcom/google/research/handwriting/gui/ContinuousWritingManager;

.field private mCursorSelectionEnd:I

.field private mCursorSelectionStart:I

.field private mEditText:Landroid/widget/EditText;

.field private mFullScreenHorizontal:Z

.field private mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

.field private mHintText:Landroid/widget/TextView;

.field private mIcLock:Ljava/lang/Object;

.field private mInputConnection:Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateInputConnection;

.field private mInputMethodView:Lcom/google/android/apps/translate/editor/InputMethodView;

.field private mKeyboard:Landroid/inputmethodservice/Keyboard;

.field private mKeyboardView:Landroid/inputmethodservice/KeyboardView;

.field private mLocale:Ljava/lang/String;

.field private mMessageHandler:Landroid/os/Handler;

.field private final mPostContextSize:I

.field private final mPreContextSize:I

.field private mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

.field private final mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

.field private mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

.field private mShouldShowTopCandidateOnSpaceKey:Z

.field private mShowHint:Z

.field private mSourceLanguage:Lcom/google/android/apps/translate/Language;

.field private mSpaceKey:Landroid/inputmethodservice/Keyboard$Key;

.field private mTargetLanguage:Lcom/google/android/apps/translate/Language;

.field private mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

.field private final mUiSettings:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 185
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 188
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 94
    new-instance v0, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;

    invoke-direct {v0}, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUiSettings:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;

    .line 96
    new-instance v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-direct {v0}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    .line 102
    new-instance v0, Lcom/google/research/handwriting/gui/ContinuousWritingManager;

    invoke-direct {v0}, Lcom/google/research/handwriting/gui/ContinuousWritingManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingManager:Lcom/google/research/handwriting/gui/ContinuousWritingManager;

    .line 103
    new-instance v0, Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

    invoke-direct {v0}, Lcom/google/research/handwriting/gui/ContinuousWritingHandler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingHandler:Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

    .line 106
    iput-boolean v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShowHint:Z

    .line 107
    iput-boolean v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mFullScreenHorizontal:Z

    .line 108
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mIcLock:Ljava/lang/Object;

    .line 110
    iput-boolean v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShouldShowTopCandidateOnSpaceKey:Z

    .line 122
    iput v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionStart:I

    .line 123
    iput v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionEnd:I

    .line 189
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "HandwritingInputView constructor"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iput-object p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContext:Landroid/content/Context;

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$integer;->button_input_method_keyboard:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCodeKeyboard:I

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$integer;->button_space:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCodeSpace:I

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$integer;->button_delete:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCodeDelete:I

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$integer;->handwriting_pre_context_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mPreContextSize:I

    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/translate/R$integer;->handwriting_post_context_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mPostContextSize:I

    .line 198
    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setWillNotDraw(Z)V

    .line 200
    new-instance v0, Lcom/google/research/handwriting/gui/UIHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingHandler:Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingManager:Lcom/google/research/handwriting/gui/ContinuousWritingManager;

    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mIcLock:Ljava/lang/Object;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/research/handwriting/gui/UIHandler;-><init>(Lcom/google/research/handwriting/gui/UIHandlerCallback;Lcom/google/research/handwriting/gui/ContinuousWritingHandler;Lcom/google/research/handwriting/gui/ContinuousWritingManager;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0, p0}, Lcom/google/research/handwriting/gui/UIHandler;->setHandwritingImeView(Landroid/view/View;)V

    .line 203
    new-instance v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateRecognizerHandler;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateRecognizerHandler;-><init>(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/RecognizerHandler;->setHIHandler(Lcom/google/research/handwriting/gui/UIHandler;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    invoke-virtual {v0, v4}, Lcom/google/research/handwriting/gui/RecognizerHandler;->setAutoSelect(Z)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/RecognizerHandler;->setAutoSelectMilli(I)V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/RecognizerHandler;->setApplicationContext(Landroid/content/Context;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    invoke-virtual {v0, v4}, Lcom/google/research/handwriting/gui/RecognizerHandler;->setShowDebugInformation(Z)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0, v5}, Lcom/google/research/handwriting/gui/UIHandler;->setAutoSpace(Z)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0, v4}, Lcom/google/research/handwriting/gui/UIHandler;->setUseBookkeeper(Z)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUiSettings:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;

    iput-boolean v4, v0, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;->useBackGesture:Z

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUiSettings:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;

    iput-boolean v4, v0, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;->useSpaceGesture:Z

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUiSettings:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/UIHandler;->setRecognizerSettings(Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognizerUISettings;)V

    .line 216
    invoke-direct {p0, v5}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setRecognizer(Z)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/RecognizerHandler;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    sget v0, Lcom/google/android/apps/translate/R$id;->busyDisplay:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/UIHandler;->setRecognizerHandler(Lcom/google/research/handwriting/gui/RecognizerHandler;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/UIHandler;->setHandwritingRecognizer(Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;)V

    .line 225
    new-instance v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$1;-><init>(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mMessageHandler:Landroid/os/Handler;

    .line 243
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)Landroid/widget/EditText;
    .locals 1
    .parameter "x0"

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)Lcom/google/research/handwriting/gui/UIHandler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 62
    iput p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionStart:I

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 62
    iput p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionEnd:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;ILandroid/view/inputmethod/InputConnection;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setCursor(ILandroid/view/inputmethod/InputConnection;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHintText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;ZZ)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setHintAndCursor(ZZ)V

    return-void
.end method

.method private clearLimboStateAndClearText()V
    .locals 2

    .prologue
    .line 779
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "clearLimboStateAndClearText"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearComposingText()V

    .line 783
    :cond_0
    return-void
.end method

.method private initializeCursor()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 345
    sget-object v2, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v3, "initializeCursor"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mIcLock:Ljava/lang/Object;

    monitor-enter v3

    .line 347
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getImeCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 348
    .local v1, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v1, :cond_0

    .line 349
    new-instance v2, Landroid/view/inputmethod/ExtractedTextRequest;

    invoke-direct {v2}, Landroid/view/inputmethod/ExtractedTextRequest;-><init>()V

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, Landroid/view/inputmethod/InputConnection;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;

    move-result-object v0

    .line 350
    .local v0, exText:Landroid/view/inputmethod/ExtractedText;
    if-eqz v0, :cond_2

    .line 354
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getCursorSelectionStart()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 357
    iget-object v2, v0, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setCursor(ILandroid/view/inputmethod/InputConnection;)V

    .line 368
    .end local v0           #exText:Landroid/view/inputmethod/ExtractedText;
    :cond_0
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getCursorSelectionStart()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 371
    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setCursor(ILandroid/view/inputmethod/InputConnection;)V

    .line 373
    :cond_1
    return-void

    .line 360
    .restart local v0       #exText:Landroid/view/inputmethod/ExtractedText;
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getCursorSelectionStart()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 364
    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setCursor(ILandroid/view/inputmethod/InputConnection;)V

    goto :goto_0

    .line 368
    .end local v0           #exText:Landroid/view/inputmethod/ExtractedText;
    .end local v1           #ic:Landroid/view/inputmethod/InputConnection;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private isCharacterByCharacterRecognitionLanguage(Ljava/lang/String;)Z
    .locals 1
    .parameter "sourceLangShortName"

    .prologue
    .line 468
    const-string v0, "ja"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "zh-CN"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "zh-TW"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ko"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setCursor(ILandroid/view/inputmethod/InputConnection;)V
    .locals 2
    .parameter "pos"
    .parameter "ic"

    .prologue
    .line 376
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "setCursor"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iput p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionStart:I

    .line 378
    iput p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionEnd:I

    .line 379
    if-eqz p2, :cond_0

    .line 380
    invoke-interface {p2, p1, p1}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    .line 382
    :cond_0
    return-void
.end method

.method private setHintAndCursor(ZZ)V
    .locals 6
    .parameter "initializeBookkeeper"
    .parameter "showHint"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 321
    sget-object v3, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setHintAndCursor initializeBookkeeper="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " showHint="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    if-eqz v3, :cond_0

    .line 325
    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 327
    .local v0, hasInputText:Z
    :goto_0
    if-eqz p2, :cond_2

    .line 328
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/translate/R$string;->hint_handwriting_input_text:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setHint(Ljava/lang/String;)V

    .line 335
    :goto_1
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 339
    invoke-direct {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->initializeCursor()V

    .line 342
    .end local v0           #hasInputText:Z
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 325
    goto :goto_0

    .line 332
    .restart local v0       #hasInputText:Z
    :cond_2
    const-string v1, ""

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setHint(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private setKeyboardView()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 290
    sget-object v5, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v6, "setKeyboardView"

    invoke-static {v5, v6}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    sget v5, Lcom/google/android/apps/translate/R$id;->handwriting_view_buttons:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/inputmethodservice/KeyboardView;

    iput-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    .line 292
    new-instance v5, Landroid/inputmethodservice/Keyboard;

    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/google/android/apps/translate/R$xml;->handwriting_view_buttons:I

    invoke-direct {v5, v6, v7}, Landroid/inputmethodservice/Keyboard;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    .line 293
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    iget-object v6, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    invoke-virtual {v5, v6}, Landroid/inputmethodservice/KeyboardView;->setKeyboard(Landroid/inputmethodservice/Keyboard;)V

    .line 295
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    invoke-virtual {v5, v8}, Landroid/inputmethodservice/KeyboardView;->setPreviewEnabled(Z)V

    .line 296
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    invoke-virtual {v5, p0}, Landroid/inputmethodservice/KeyboardView;->setOnKeyboardActionListener(Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;)V

    .line 297
    const/4 v4, 0x0

    .line 298
    .local v4, keyIndex:I
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/translate/R$integer;->button_space:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 299
    .local v3, keyCodeForButtonSpace:I
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    invoke-virtual {v5}, Landroid/inputmethodservice/Keyboard;->getKeys()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/inputmethodservice/Keyboard$Key;

    .line 300
    .local v2, key:Landroid/inputmethodservice/Keyboard$Key;
    iget-object v0, v2, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    .line 301
    .local v0, codes:[I
    array-length v5, v0

    if-lez v5, :cond_1

    aget v5, v0, v8

    if-ne v5, v3, :cond_1

    .line 302
    iput-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSpaceKey:Landroid/inputmethodservice/Keyboard$Key;

    .line 307
    .end local v0           #codes:[I
    .end local v2           #key:Landroid/inputmethodservice/Keyboard$Key;
    :cond_0
    return-void

    .line 305
    .restart local v0       #codes:[I
    .restart local v2       #key:Landroid/inputmethodservice/Keyboard$Key;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 306
    goto :goto_0
.end method

.method private setRecognizer(Z)V
    .locals 4
    .parameter "useTranslateApi"

    .prologue
    const/4 v3, 0x0

    .line 250
    if-eqz p1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->useTranslateApi:Z

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    const-string v2, "http://translate.google.com"

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->server:Ljava/lang/String;

    .line 253
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    const-string v2, "/translate_a/hw"

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->recoPath:Ljava/lang/String;

    .line 259
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iput v3, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->verbosity:I

    .line 260
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    const-string v2, "atrans"

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->clientName:Ljava/lang/String;

    .line 262
    invoke-static {}, Lcom/google/research/handwriting/networkrecognizer/HandwritingHttpClient;->getNewHttpClient()Lcom/google/research/handwriting/networkrecognizer/HandwritingHttpClient;

    move-result-object v0

    .line 263
    .local v0, httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translate/Util;->generateUserAgentName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Util;->setUserAgentToHttpClient(Lorg/apache/http/client/HttpClient;Ljava/lang/String;)V

    .line 264
    new-instance v1, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    invoke-direct {v1, v2}, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;-><init>(Lcom/google/research/handwriting/gui/RecognizerClient;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    .line 265
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    new-instance v2, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;

    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-direct {v2, v0, v3}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;)V

    invoke-virtual {v1, v2}, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;->setRecognizer(Lcom/google/research/handwriting/base/HandwritingRecognizer;)V

    .line 266
    return-void

    .line 255
    .end local v0           #httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iput-boolean v3, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->useTranslateApi:Z

    .line 256
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    const-string v2, "http://inputtools.google.com"

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->server:Ljava/lang/String;

    .line 257
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    const-string v2, "/request"

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->recoPath:Ljava/lang/String;

    goto :goto_0
.end method

.method private updateSpaceKeyLabel(Ljava/lang/CharSequence;)V
    .locals 3
    .parameter "candidate"

    .prologue
    const/4 v2, 0x0

    .line 679
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "updateSpaceKeyLabel"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSpaceKey:Landroid/inputmethodservice/Keyboard$Key;

    iput-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    .line 682
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSpaceKey:Landroid/inputmethodservice/Keyboard$Key;

    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/translate/R$drawable;->sym_keyboard_space:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    .line 687
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView;->invalidateAllKeys()V

    .line 688
    return-void

    .line 684
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSpaceKey:Landroid/inputmethodservice/Keyboard$Key;

    iput-object p1, v0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSpaceKey:Landroid/inputmethodservice/Keyboard$Key;

    iput-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public cancelStroke()V
    .locals 0

    .prologue
    .line 751
    return-void
.end method

.method public clearLimboState()V
    .locals 2

    .prologue
    .line 786
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "clearLimboState"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearComposingText()V

    .line 788
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->invalidate()V

    .line 789
    return-void
.end method

.method public deleteText()V
    .locals 3

    .prologue
    .line 517
    sget-object v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v2, "deleteText"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 519
    .local v0, e:Landroid/text/Editable;
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    if-gez v1, :cond_1

    .line 520
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 521
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 528
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/UIHandler;->clearTranslatedText()V

    .line 529
    return-void

    .line 523
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 524
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    .line 525
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    .line 526
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0
.end method

.method public getCursorSelectionEnd()I
    .locals 1

    .prologue
    .line 702
    iget v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionEnd:I

    return v0
.end method

.method public getCursorSelectionStart()I
    .locals 1

    .prologue
    .line 697
    iget v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionStart:I

    return v0
.end method

.method public getImeCurrentInputConnection()Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mInputConnection:Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateInputConnection;

    return-object v0
.end method

.method public getRecognizerLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceTextToTranslate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initText(Ljava/lang/String;)V
    .locals 3
    .parameter "initText"

    .prologue
    .line 640
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "initText"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/UIHandler;->commitText(Ljava/lang/CharSequence;)V

    .line 646
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mIcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 647
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getImeCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setCursor(ILandroid/view/inputmethod/InputConnection;)V

    .line 648
    monitor-exit v1

    .line 649
    return-void

    .line 648
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public initialize(Lcom/google/research/handwriting/gui/CandidateViewHandler;Landroid/widget/EditText;Lcom/google/android/apps/translate/editor/InputMethodView;)V
    .locals 4
    .parameter "candidateViewHandler"
    .parameter "editText"
    .parameter "inputMethodView"

    .prologue
    const/4 v3, 0x1

    .line 594
    sget-object v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v2, "initialize"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    iput-object p2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    .line 597
    iput-object p3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mInputMethodView:Lcom/google/android/apps/translate/editor/InputMethodView;

    .line 598
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    check-cast v1, Lcom/google/android/apps/translate/handwriting/HandwritingTextEdit;

    invoke-interface {v1, p0}, Lcom/google/android/apps/translate/handwriting/HandwritingTextEdit;->setCallback(Lcom/google/android/apps/translate/handwriting/EditTextCallback;)V

    .line 600
    new-instance v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateInputConnection;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateInputConnection;-><init>(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;Landroid/view/View;Z)V

    iput-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mInputConnection:Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateInputConnection;

    .line 602
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$2;-><init>(Lcom/google/android/apps/translate/handwriting/HandwritingInputView;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 619
    iput-object p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateViewHandler:Lcom/google/research/handwriting/gui/CandidateViewHandler;

    .line 620
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateViewHandler:Lcom/google/research/handwriting/gui/CandidateViewHandler;

    invoke-virtual {v1, p0}, Lcom/google/research/handwriting/gui/CandidateViewHandler;->setListener(Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;)V

    .line 621
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateViewHandler:Lcom/google/research/handwriting/gui/CandidateViewHandler;

    invoke-virtual {v1, v3}, Lcom/google/research/handwriting/gui/CandidateViewHandler;->setCompletionListLocationBelowCandidates(Z)V

    .line 622
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateViewHandler:Lcom/google/research/handwriting/gui/CandidateViewHandler;

    invoke-virtual {v1, v2}, Lcom/google/research/handwriting/gui/UIHandler;->setCandidateViewHandler(Lcom/google/research/handwriting/gui/CandidateViewHandler;)V

    .line 624
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateViewHandler:Lcom/google/research/handwriting/gui/CandidateViewHandler;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/CandidateViewHandler;->getContainerView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateContainerView:Landroid/view/View;

    .line 625
    sget v1, Lcom/google/android/apps/translate/R$id;->handwriting_candidate_view:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 626
    .local v0, layout:Landroid/widget/LinearLayout;
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateContainerView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 627
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateContainerView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$drawable;->handwriting_extra_row_bg_tile:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 629
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingHandler:Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mIcLock:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/google/research/handwriting/gui/ContinuousWritingHandler;->setIclock(Ljava/lang/Object;)V

    .line 630
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingHandler:Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mInputConnection:Lcom/google/android/apps/translate/handwriting/HandwritingInputView$TranslateInputConnection;

    invoke-virtual {v1, v2}, Lcom/google/research/handwriting/gui/ContinuousWritingHandler;->setCurrentInputConnection(Landroid/view/inputmethod/InputConnection;)V

    .line 631
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingHandler:Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v1, v2}, Lcom/google/research/handwriting/gui/ContinuousWritingHandler;->setUIHandler(Lcom/google/research/handwriting/gui/UIHandler;)V

    .line 632
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    invoke-virtual {v1, v3}, Lcom/google/research/handwriting/gui/RecognizerHandler;->onInitialized(Z)V

    .line 633
    return-void
.end method

.method public initializeEditText(Landroid/widget/EditText;)V
    .locals 0
    .parameter "editText"

    .prologue
    .line 636
    iput-object p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    .line 637
    return-void
.end method

.method public onClearText()V
    .locals 2

    .prologue
    .line 811
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "onClearText"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/UIHandler;->onKeyDelete()V

    .line 815
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->resetStrokes(Z)V

    .line 816
    return-void
.end method

.method public onCommitText(Ljava/lang/CharSequence;)V
    .locals 3
    .parameter "text"

    .prologue
    .line 579
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "onCommitText"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/UIHandler;->updateTranslatedText()V

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCallback:Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v2}, Lcom/google/research/handwriting/gui/UIHandler;->getTranslatedText()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;->onEditCompleted(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .parameter "canvas"

    .prologue
    .line 386
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 387
    return-void
.end method

.method public onEditCompleted(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V
    .locals 4
    .parameter "event"

    .prologue
    const/4 v1, 0x0

    .line 848
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v2, "onEditCompleted"

    invoke-static {v0, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/UIHandler;->updateTranslatedText()V

    .line 850
    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->ACCEPT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/research/handwriting/gui/UIHandler;->finishInput(Z)V

    .line 851
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCallback:Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v3}, Lcom/google/research/handwriting/gui/UIHandler;->getTranslatedText()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;->onEditCompleted(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mTranslatedText is... "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v3}, Lcom/google/research/handwriting/gui/UIHandler;->getTranslatedText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->resetStrokes(Z)V

    .line 857
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/UIHandler;->setHandwritingOverlayView(Lcom/google/research/handwriting/gui/HandwritingOverlayView;)V

    .line 858
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setVisibility(I)V

    .line 859
    return-void

    :cond_0
    move v0, v1

    .line 850
    goto :goto_0
.end method

.method public onEditStarted()V
    .locals 4

    .prologue
    .line 822
    sget-object v2, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v3, "onEditStarted"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    if-eqz v2, :cond_0

    .line 826
    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v2, v3}, Lcom/google/research/handwriting/gui/UIHandler;->setHandwritingOverlayView(Lcom/google/research/handwriting/gui/HandwritingOverlayView;)V

    .line 828
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    invoke-virtual {v2}, Lcom/google/research/handwriting/gui/RecognizerHandler;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 831
    sget v2, Lcom/google/android/apps/translate/R$id;->busyDisplay:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 833
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getWidth()I

    move-result v1

    .line 834
    .local v1, w:I
    invoke-virtual {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->getHeight()I

    move-result v0

    .line 835
    .local v0, h:I
    if-lez v1, :cond_2

    if-lez v0, :cond_2

    .line 837
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->onSizeChanged(II)V

    .line 842
    :cond_2
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 270
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 272
    sget v0, Lcom/google/android/apps/translate/R$id;->handwriting_overlay:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v0, p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->setHandwritingOverlayListener(Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/RecognizerHandler;->setHandwritingOverlayView(Lcom/google/research/handwriting/gui/HandwritingOverlayView;)V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerHandler:Lcom/google/research/handwriting/gui/RecognizerHandler;

    sget v1, Lcom/google/android/apps/translate/R$id;->busyDisplay:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/RecognizerHandler;->setBusyDisplay(Landroid/view/View;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/UIHandler;->setHandwritingOverlayView(Lcom/google/research/handwriting/gui/HandwritingOverlayView;)V

    .line 279
    invoke-direct {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setKeyboardView()V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingManager:Lcom/google/research/handwriting/gui/ContinuousWritingManager;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/ContinuousWritingManager;->setRecognizer(Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;)V

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingManager:Lcom/google/research/handwriting/gui/ContinuousWritingManager;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingHandler:Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/ContinuousWritingManager;->setCallback(Lcom/google/research/handwriting/gui/ContinuousWritingManager$ContinuousWritingCallbacks;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContinuousWritingHandler:Lcom/google/research/handwriting/gui/ContinuousWritingHandler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/ContinuousWritingHandler;->setHandwritingOverlayView(Lcom/google/research/handwriting/gui/HandwritingOverlayView;)V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;->initialize()V

    .line 286
    sget v0, Lcom/google/android/apps/translate/R$id;->handwriting_hint_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHintText:Landroid/widget/TextView;

    .line 287
    return-void
.end method

.method public onKey(I[I)V
    .locals 4
    .parameter "primaryCode"
    .parameter "keyCodes"

    .prologue
    .line 476
    sget-object v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKey primaryCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 478
    .local v0, e:Landroid/text/Editable;
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 479
    sget-object v1, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->SWITCH:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->onEditCompleted(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V

    .line 498
    :goto_0
    return-void

    .line 482
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/UIHandler;->cancelAutoSelect()V

    .line 483
    if-ltz p1, :cond_1

    .line 484
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v1, p1}, Lcom/google/research/handwriting/gui/UIHandler;->onKeyNormal(I)V

    .line 495
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v2}, Lcom/google/research/handwriting/gui/UIHandler;->getPreContext()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;->setPreContext(Ljava/lang/String;)V

    .line 496
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    iget-object v2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v2}, Lcom/google/research/handwriting/gui/UIHandler;->getPostContext()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;->setPostContext(Ljava/lang/String;)V

    .line 497
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/UIHandler;->clearResults()V

    goto :goto_0

    .line 485
    :cond_1
    iget v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCodeDelete:I

    if-ne p1, v1, :cond_2

    .line 486
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/UIHandler;->onKeyDelete()V

    goto :goto_1

    .line 487
    :cond_2
    iget v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCodeKeyboard:I

    if-ne p1, v1, :cond_3

    .line 488
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->resetStrokes(Z)V

    .line 489
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/research/handwriting/gui/UIHandler;->setHandwritingOverlayView(Lcom/google/research/handwriting/gui/HandwritingOverlayView;)V

    .line 490
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setVisibility(I)V

    .line 491
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mInputMethodView:Lcom/google/android/apps/translate/editor/InputMethodView;

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->KEYBOARD:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/InputMethodView;->startInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    goto :goto_1

    .line 493
    :cond_3
    int-to-char v1, p1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->sendKeyChar(C)V

    goto :goto_1
.end method

.method public onPenDown(FFJF)V
    .locals 6
    .parameter "x"
    .parameter "y"
    .parameter "time"
    .parameter "pressure"

    .prologue
    const/4 v1, 0x0

    .line 731
    iget-boolean v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShowHint:Z

    if-eqz v0, :cond_0

    .line 732
    iput-boolean v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShowHint:Z

    .line 734
    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setHint(Ljava/lang/String;)V

    .line 735
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    check-cast v0, Lcom/google/android/apps/translate/handwriting/HandwritingTextEdit;

    invoke-interface {v0, v1}, Lcom/google/android/apps/translate/handwriting/HandwritingTextEdit;->setIsTextEditor(Z)V

    .line 736
    iget-boolean v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShouldShowTopCandidateOnSpaceKey:Z

    if-eqz v0, :cond_1

    .line 737
    const-string v0, "..."

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->updateSpaceKeyLabel(Ljava/lang/CharSequence;)V

    .line 739
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/research/handwriting/gui/UIHandler;->onPenDown(FFJF)V

    .line 740
    return-void
.end method

.method public onPenMove(FFJF)V
    .locals 6
    .parameter "x"
    .parameter "y"
    .parameter "time"
    .parameter "pressure"

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/research/handwriting/gui/UIHandler;->onPenMove(FFJF)V

    .line 745
    return-void
.end method

.method public onPenUp(FFJF)V
    .locals 6
    .parameter "x"
    .parameter "y"
    .parameter "time"
    .parameter "pressure"

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/research/handwriting/gui/UIHandler;->onPenUp(FFJF)V

    .line 756
    return-void
.end method

.method public onPickSuggestion(ILjava/lang/CharSequence;Z)V
    .locals 2
    .parameter "index"
    .parameter "selectedString"
    .parameter "completion"

    .prologue
    .line 588
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "onPickSuggestion"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/research/handwriting/gui/UIHandler;->onPickSuggestion(ILjava/lang/CharSequence;Z)V

    .line 590
    return-void
.end method

.method public onPress(I)V
    .locals 0
    .parameter "primaryCode"

    .prologue
    .line 533
    return-void
.end method

.method public onRelease(I)V
    .locals 0
    .parameter "primaryCode"

    .prologue
    .line 537
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 775
    const-string v0, "textbox"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->initText(Ljava/lang/String;)V

    .line 776
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 771
    const-string v0, "textbox"

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    return-void
.end method

.method public onSelectionChanged(II)V
    .locals 2
    .parameter "selStart"
    .parameter "selEnd"

    .prologue
    .line 793
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "onSelectionChanged"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    if-nez v0, :cond_0

    .line 807
    :goto_0
    return-void

    .line 799
    :cond_0
    iput p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionStart:I

    .line 800
    iput p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCursorSelectionEnd:I

    .line 801
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/UIHandler;->getJustModifiedComposingRegion()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 802
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/UIHandler;->setJustModifiedComposingRegion(Z)V

    goto :goto_0

    .line 806
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0, p1, p2}, Lcom/google/research/handwriting/gui/UIHandler;->onUpdateSelection(II)V

    goto :goto_0
.end method

.method public onSizeChanged(II)V
    .locals 4
    .parameter "width"
    .parameter "height"

    .prologue
    .line 712
    sget-object v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSizeChanged width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    sget-object v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSizeChanged height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    if-eqz v1, :cond_0

    .line 715
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v1, p1, p2}, Lcom/google/research/handwriting/gui/UIHandler;->onSizeChanged(II)V

    .line 718
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->clearLimboStateAndClearText()V

    .line 719
    invoke-direct {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setKeyboardView()V

    .line 721
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    if-eqz v1, :cond_1

    .line 723
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 725
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 727
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return-void
.end method

.method public onText(Ljava/lang/CharSequence;)V
    .locals 0
    .parameter "text"

    .prologue
    .line 541
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .parameter "v"
    .parameter "event"

    .prologue
    const/4 v2, 0x0

    .line 394
    sget-object v3, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v4, "onTouch"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    if-nez v3, :cond_0

    .line 412
    :goto_0
    :pswitch_0
    return v2

    .line 399
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v0, v3

    .line 400
    .local v0, x:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v1, v3

    .line 401
    .local v1, y:I
    sget-object v3, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onTouch: ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 412
    :pswitch_1
    const/4 v2, 0x1

    goto :goto_0

    .line 402
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public resetStrokes(Z)V
    .locals 3
    .parameter "clearAllText"

    .prologue
    const/4 v2, 0x1

    .line 310
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "resetStrokes"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/UIHandler;->clearRecognizer()V

    .line 312
    if-eqz p1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    check-cast v0, Lcom/google/android/apps/translate/handwriting/HandwritingTextEdit;

    invoke-interface {v0, v2}, Lcom/google/android/apps/translate/handwriting/HandwritingTextEdit;->setIsTextEditor(Z)V

    .line 316
    iget-boolean v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShowHint:Z

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setHintAndCursor(ZZ)V

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/UIHandler;->clearResults()V

    .line 318
    return-void
.end method

.method public sendKeyChar(C)V
    .locals 4
    .parameter "primaryCode"

    .prologue
    .line 502
    sget-object v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v2, "sendKeyChar"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 504
    .local v0, e:Landroid/text/Editable;
    iget v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCodeSpace:I

    if-ne p1, v1, :cond_1

    .line 505
    sget-object v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v2, "sendKeyChar codeSpace"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    if-ltz v1, :cond_0

    .line 507
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    const-string v2, " "

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 509
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/UIHandler;->clearTranslatedText()V

    .line 513
    :goto_0
    return-void

    .line 511
    :cond_1
    sget-object v1, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendKeyChar ignoring primaryCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setCallback(Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;)V
    .locals 0
    .parameter "callback"

    .prologue
    .line 567
    iput-object p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCallback:Lcom/google/android/apps/translate/handwriting/HandwritingInputView$HandwritingInputViewCallback;

    .line 568
    return-void
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 762
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mMessageHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mMessageHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 763
    return-void
.end method

.method public setImeCandidatesViewShown(Z)V
    .locals 2
    .parameter "shown"

    .prologue
    .line 670
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "setImeCandidatesViewShown"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    iget-boolean v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mFullScreenHorizontal:Z

    if-eqz v0, :cond_0

    .line 676
    :goto_0
    return-void

    .line 675
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mCandidateContainerView:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public setSourceAndTargetLanguages(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 8
    .parameter "sourceLanguage"
    .parameter "targetLanguage"

    .prologue
    const/high16 v7, 0x4080

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 416
    sget-object v5, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v6, "setSourceAndTargetLanguages"

    invoke-static {v5, v6}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    if-eq v5, p1, :cond_5

    move v1, v3

    .line 419
    .local v1, srcLangChanged:Z
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    if-eq v5, p2, :cond_6

    move v2, v3

    .line 420
    .local v2, trgLangChanged:Z
    :goto_1
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    .line 425
    :cond_0
    sget-object v5, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v6, "setSourceAndTargetLanguages: language changed."

    invoke-static {v5, v6}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    invoke-direct {p0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->clearLimboStateAndClearText()V

    .line 428
    invoke-virtual {p0, v4}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->resetStrokes(Z)V

    .line 430
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mUIHandler:Lcom/google/research/handwriting/gui/UIHandler;

    invoke-virtual {v5}, Lcom/google/research/handwriting/gui/UIHandler;->clearTranslatedText()V

    .line 433
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    .line 434
    iput-object p2, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    .line 435
    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v0

    .line 436
    .local v0, sourceLangShortName:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-boolean v5, v5, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->useTranslateApi:Z

    if-eqz v5, :cond_2

    .line 437
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizerSettings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v6, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mTargetLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->targetLanguage:Ljava/lang/String;

    .line 439
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/translate/Profile;->getHandwritingLangShortNameFromFromShortLangName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mLocale:Ljava/lang/String;

    .line 442
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    if-eqz v5, :cond_4

    .line 443
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 444
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mLocale:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 445
    const-string v5, "zh-words"

    iput-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mLocale:Ljava/lang/String;

    .line 446
    invoke-direct {p0, v4}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setRecognizer(Z)V

    .line 451
    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    iget-object v6, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mLocale:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;->setLanguage(Ljava/lang/String;)V

    .line 453
    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->isCharacterByCharacterRecognitionLanguage(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 454
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v5, v7}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->setMinStrokeWidth(F)V

    .line 455
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v5, v7}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->setMaxStrokeWidth(F)V

    .line 460
    :goto_3
    iget-boolean v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShowHint:Z

    invoke-direct {p0, v3, v5}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setHintAndCursor(ZZ)V

    .line 463
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mSourceLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/research/handwriting/gui/ImeUtils;->isLanguageWithSpaces(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_9

    :goto_4
    iput-boolean v3, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShouldShowTopCandidateOnSpaceKey:Z

    .line 465
    return-void

    .end local v0           #sourceLangShortName:Ljava/lang/String;
    .end local v1           #srcLangChanged:Z
    .end local v2           #trgLangChanged:Z
    :cond_5
    move v1, v4

    .line 418
    goto/16 :goto_0

    .restart local v1       #srcLangChanged:Z
    :cond_6
    move v2, v4

    .line 419
    goto/16 :goto_1

    .line 448
    .restart local v0       #sourceLangShortName:Ljava/lang/String;
    .restart local v2       #trgLangChanged:Z
    :cond_7
    invoke-direct {p0, v3}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->setRecognizer(Z)V

    goto :goto_2

    .line 457
    :cond_8
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v5, v7}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->setMinStrokeWidth(F)V

    .line 458
    iget-object v5, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mHandwritingOverlayView:Lcom/google/research/handwriting/gui/HandwritingOverlayView;

    invoke-virtual {v5, v7}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->setMaxStrokeWidth(F)V

    goto :goto_3

    :cond_9
    move v3, v4

    .line 463
    goto :goto_4
.end method

.method public setSuggestedWords(Lcom/google/research/handwriting/gui/SuggestedWords;)V
    .locals 2
    .parameter "suggestions"

    .prologue
    .line 653
    sget-object v0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->TAG:Ljava/lang/String;

    const-string v1, "setSuggestedWords"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    iget-boolean v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mShouldShowTopCandidateOnSpaceKey:Z

    if-nez v0, :cond_0

    .line 666
    :goto_0
    return-void

    .line 657
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/research/handwriting/gui/SuggestedWords;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 658
    iget-object v0, p0, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->mRecognizer:Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer;->hasPendingRequests()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 659
    const-string v0, "..."

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->updateSpaceKeyLabel(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 661
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/research/handwriting/gui/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->updateSpaceKeyLabel(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 664
    :cond_2
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/handwriting/HandwritingInputView;->updateSpaceKeyLabel(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public swipeDown()V
    .locals 0

    .prologue
    .line 545
    return-void
.end method

.method public swipeLeft()V
    .locals 0

    .prologue
    .line 549
    return-void
.end method

.method public swipeRight()V
    .locals 0

    .prologue
    .line 553
    return-void
.end method

.method public swipeUp()V
    .locals 0

    .prologue
    .line 557
    return-void
.end method
