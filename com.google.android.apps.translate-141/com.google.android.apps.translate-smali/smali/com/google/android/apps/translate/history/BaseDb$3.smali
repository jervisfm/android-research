.class Lcom/google/android/apps/translate/history/BaseDb$3;
.super Ljava/lang/Thread;
.source "BaseDb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/history/BaseDb;->saveIntoDbAsync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/history/BaseDb;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/history/BaseDb;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"

    .prologue
    .line 547
    iput-object p1, p0, Lcom/google/android/apps/translate/history/BaseDb$3;->this$0:Lcom/google/android/apps/translate/history/BaseDb;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private iterate()Z
    .locals 5

    .prologue
    .line 557
    const-wide/16 v1, 0x1388

    :try_start_0
    invoke-static {v1, v2}, Lcom/google/android/apps/translate/history/BaseDb$3;->sleep(J)V

    .line 558
    iget-object v2, p0, Lcom/google/android/apps/translate/history/BaseDb$3;->this$0:Lcom/google/android/apps/translate/history/BaseDb;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    const/4 v0, 0x0

    .line 560
    .local v0, totalWaitedTimeMs:I
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb$3;->this$0:Lcom/google/android/apps/translate/history/BaseDb;

    iget-boolean v1, v1, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z

    if-nez v1, :cond_0

    const v1, 0x186a0

    if-ge v0, v1, :cond_0

    .line 561
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb$3;->this$0:Lcom/google/android/apps/translate/history/BaseDb;

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v3, v4}, Ljava/lang/Object;->wait(J)V

    .line 562
    add-int/lit16 v0, v0, 0x1388

    goto :goto_0

    .line 565
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb$3;->this$0:Lcom/google/android/apps/translate/history/BaseDb;

    iget-boolean v1, v1, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z

    if-nez v1, :cond_1

    .line 567
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb$3;->this$0:Lcom/google/android/apps/translate/history/BaseDb;

    const/4 v3, 0x0

    iput-object v3, v1, Lcom/google/android/apps/translate/history/BaseDb;->mFlusherThread:Ljava/lang/Thread;

    .line 568
    const/4 v1, 0x0

    monitor-exit v2

    .line 575
    .end local v0           #totalWaitedTimeMs:I
    :goto_1
    return v1

    .line 570
    .restart local v0       #totalWaitedTimeMs:I
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 571
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb$3;->this$0:Lcom/google/android/apps/translate/history/BaseDb;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/BaseDb;->saveIntoDb()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 575
    .end local v0           #totalWaitedTimeMs:I
    :goto_2
    const/4 v1, 0x1

    goto :goto_1

    .line 570
    .restart local v0       #totalWaitedTimeMs:I
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 572
    .end local v0           #totalWaitedTimeMs:I
    :catch_0
    move-exception v1

    goto :goto_2
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 550
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/history/BaseDb$3;->iterate()Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    return-void
.end method
