.class public Lcom/google/android/apps/translate/history/BaseDb;
.super Ljava/lang/Object;
.source "BaseDb.java"


# static fields
.field private static final BUFFER_SIZE:I = 0xc800

.field static final COLLATOR:Ljava/text/Collator; = null

.field private static final DEBUG:Z = false

.field private static final EMPTY_ENTRY:Lcom/google/android/apps/translate/history/Entry; = null

.field private static final ENTRY_ATIME_COMPARATOR:Ljava/util/Comparator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation
.end field

.field private static final ENTRY_COMPARATOR:Ljava/util/Comparator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation
.end field

.field private static final FLUSHER_DELAY_MS:I = 0x1388

.field private static final FLUSHER_TIMEOUT_MS:I = 0x186a0

.field private static final TAG:Ljava/lang/String; = "BaseDb"

.field private static final VERSION:I = 0x1


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDbName:Ljava/lang/String;

.field private final mEntries:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation
.end field

.field mFlusherThread:Ljava/lang/Thread;

.field private mIsOnMemoryDb:Z

.field private mLastModifiedTimeMs:J

.field private mLoadedInMemory:Z

.field private final mLruEntries:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mMaxCount:I

.field mMemoryDirty:Z

.field private final mOpenedCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/apps/translate/history/Entry;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/translate/history/Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/translate/history/BaseDb;->EMPTY_ENTRY:Lcom/google/android/apps/translate/history/Entry;

    .line 44
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translate/history/BaseDb;->COLLATOR:Ljava/text/Collator;

    .line 46
    sget-object v0, Lcom/google/android/apps/translate/history/BaseDb;->COLLATOR:Ljava/text/Collator;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 55
    new-instance v0, Lcom/google/android/apps/translate/history/BaseDb$1;

    invoke-direct {v0}, Lcom/google/android/apps/translate/history/BaseDb$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/translate/history/BaseDb;->ENTRY_COMPARATOR:Ljava/util/Comparator;

    .line 87
    new-instance v0, Lcom/google/android/apps/translate/history/BaseDb$2;

    invoke-direct {v0}, Lcom/google/android/apps/translate/history/BaseDb$2;-><init>()V

    sput-object v0, Lcom/google/android/apps/translate/history/BaseDb;->ENTRY_ATIME_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IZ)V
    .locals 2
    .parameter "context"
    .parameter "dbName"
    .parameter "maxCount"
    .parameter "isOnMemoryDb"

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, Lcom/google/android/apps/translate/history/BaseDb;->ENTRY_COMPARATOR:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    .line 126
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, Lcom/google/android/apps/translate/history/BaseDb;->ENTRY_ATIME_COMPARATOR:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    .line 132
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mOpenedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mIsOnMemoryDb:Z

    .line 148
    iput-object p1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mContext:Landroid/content/Context;

    .line 149
    invoke-static {p2}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    .line 150
    iput p3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMaxCount:I

    .line 151
    iput-boolean p4, p0, Lcom/google/android/apps/translate/history/BaseDb;->mIsOnMemoryDb:Z

    .line 152
    return-void
.end method

.method private dumpEntry(Ljava/lang/String;Lcom/google/android/apps/translate/history/Entry;)V
    .locals 5
    .parameter "tag"
    .parameter "entry"

    .prologue
    .line 473
    const-string v0, "BaseDb"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DbName       ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const-string v0, "BaseDb"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " InputText    ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v0, "BaseDb"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " OutputText   ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getOutputText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const-string v0, "BaseDb"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AccessedTime ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/history/BaseDb;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getAccessedTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/translate/Util;->formatTimeStampString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const-string v0, "BaseDb"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CreatedTime  ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/history/BaseDb;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getCreatedTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/translate/Util;->formatTimeStampString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method private getFilteredEntries(ILjava/lang/String;Ljava/util/Collection;)Ljava/util/ArrayList;
    .locals 4
    .parameter "count"
    .parameter "prefixFilter"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    .local p3, entries:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/android/apps/translate/history/Entry;>;"
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 278
    .local v2, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/translate/history/Entry;>;"
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/history/Entry;

    .line 279
    .local v0, entry:Lcom/google/android/apps/translate/history/Entry;
    if-nez p1, :cond_2

    .line 291
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_1
    return-object v2

    .line 282
    .restart local v0       #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/google/android/apps/translate/Util;->filterMatches(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/Entry;->getTranslation()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/google/android/apps/translate/Util;->filterMatches(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 284
    :cond_3
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    add-int/lit8 p1, p1, -0x1

    goto :goto_0
.end method

.method private declared-synchronized loadIntoMemory()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 416
    monitor-enter p0

    :try_start_0
    iget-boolean v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLoadedInMemory:Z

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mIsOnMemoryDb:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_1

    .line 470
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 423
    :cond_1
    const/4 v4, 0x0

    .line 425
    .local v4, stream:Ljava/io/DataInputStream;
    const/4 v7, 0x0

    :try_start_1
    iput-boolean v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z

    .line 426
    iget-object v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v7}, Ljava/util/TreeMap;->clear()V

    .line 427
    iget-object v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 428
    .local v3, file:Ljava/io/File;
    new-instance v5, Ljava/io/DataInputStream;

    new-instance v7, Ljava/io/BufferedInputStream;

    iget-object v8, p0, Lcom/google/android/apps/translate/history/BaseDb;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v8

    const v9, 0xc800

    invoke-direct {v7, v8, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {v5, v7}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 431
    .end local v4           #stream:Ljava/io/DataInputStream;
    .local v5, stream:Ljava/io/DataInputStream;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLastModifiedTimeMs:J

    .line 432
    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    .line 433
    .local v6, version:I
    if-ne v6, v10, :cond_5

    .line 434
    const/4 v0, 0x0

    .local v0, count:I
    :goto_1
    iget v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMaxCount:I

    if-ge v0, v7, :cond_2

    .line 435
    invoke-static {v5}, Lcom/google/android/apps/translate/history/Entry;->readData(Ljava/io/DataInputStream;)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v2

    .line 436
    .local v2, entry:Lcom/google/android/apps/translate/history/Entry;
    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguageShortName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    move-result v7

    if-eqz v7, :cond_4

    .line 457
    .end local v0           #count:I
    .end local v2           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_2
    :goto_2
    if-eqz v5, :cond_7

    .line 459
    :try_start_3
    invoke-virtual {v5}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-object v4, v5

    .line 466
    .end local v3           #file:Ljava/io/File;
    .end local v5           #stream:Ljava/io/DataInputStream;
    .end local v6           #version:I
    .restart local v4       #stream:Ljava/io/DataInputStream;
    :cond_3
    :goto_3
    const/4 v7, 0x1

    :try_start_4
    iput-boolean v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLoadedInMemory:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 416
    .end local v4           #stream:Ljava/io/DataInputStream;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 443
    .restart local v0       #count:I
    .restart local v2       #entry:Lcom/google/android/apps/translate/history/Entry;
    .restart local v3       #file:Ljava/io/File;
    .restart local v5       #stream:Ljava/io/DataInputStream;
    .restart local v6       #version:I
    :cond_4
    :try_start_5
    iget-object v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v7, v2, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    iget-object v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v7, v2, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 446
    .end local v0           #count:I
    .end local v2           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_5
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isWarning()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 447
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Upgrading database from version "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", which will destroy all old data"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/google/android/apps/translate/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    goto :goto_2

    .line 450
    .end local v6           #version:I
    :catch_0
    move-exception v1

    move-object v4, v5

    .line 451
    .end local v3           #file:Ljava/io/File;
    .end local v5           #stream:Ljava/io/DataInputStream;
    .local v1, e:Ljava/io/FileNotFoundException;
    .restart local v4       #stream:Ljava/io/DataInputStream;
    :goto_4
    :try_start_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLastModifiedTimeMs:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 457
    if-eqz v4, :cond_3

    .line 459
    :try_start_7
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_3

    .line 460
    :catch_1
    move-exception v7

    goto :goto_3

    .end local v1           #e:Ljava/io/FileNotFoundException;
    .end local v4           #stream:Ljava/io/DataInputStream;
    .restart local v3       #file:Ljava/io/File;
    .restart local v5       #stream:Ljava/io/DataInputStream;
    .restart local v6       #version:I
    :catch_2
    move-exception v7

    move-object v4, v5

    .line 462
    .end local v5           #stream:Ljava/io/DataInputStream;
    .restart local v4       #stream:Ljava/io/DataInputStream;
    goto :goto_3

    .line 452
    .end local v3           #file:Ljava/io/File;
    .end local v6           #version:I
    :catch_3
    move-exception v1

    .line 455
    .local v1, e:Ljava/io/IOException;
    :goto_5
    :try_start_8
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "loadIntoMemory failed from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 457
    if-eqz v4, :cond_3

    .line 459
    :try_start_9
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto :goto_3

    .line 460
    :catch_4
    move-exception v7

    goto :goto_3

    .line 457
    .end local v1           #e:Ljava/io/IOException;
    :catchall_1
    move-exception v7

    :goto_6
    if-eqz v4, :cond_6

    .line 459
    :try_start_a
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 462
    :cond_6
    :goto_7
    :try_start_b
    throw v7
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 460
    :catch_5
    move-exception v8

    goto :goto_7

    .line 457
    .end local v4           #stream:Ljava/io/DataInputStream;
    .restart local v3       #file:Ljava/io/File;
    .restart local v5       #stream:Ljava/io/DataInputStream;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5           #stream:Ljava/io/DataInputStream;
    .restart local v4       #stream:Ljava/io/DataInputStream;
    goto :goto_6

    .line 452
    .end local v4           #stream:Ljava/io/DataInputStream;
    .restart local v5       #stream:Ljava/io/DataInputStream;
    :catch_6
    move-exception v1

    move-object v4, v5

    .end local v5           #stream:Ljava/io/DataInputStream;
    .restart local v4       #stream:Ljava/io/DataInputStream;
    goto :goto_5

    .line 450
    .end local v3           #file:Ljava/io/File;
    :catch_7
    move-exception v1

    goto :goto_4

    .end local v4           #stream:Ljava/io/DataInputStream;
    .restart local v3       #file:Ljava/io/File;
    .restart local v5       #stream:Ljava/io/DataInputStream;
    .restart local v6       #version:I
    :cond_7
    move-object v4, v5

    .end local v5           #stream:Ljava/io/DataInputStream;
    .restart local v4       #stream:Ljava/io/DataInputStream;
    goto/16 :goto_3
.end method

.method private onModified()V
    .locals 2

    .prologue
    .line 355
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLastModifiedTimeMs:J

    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z

    .line 357
    return-void
.end method

.method private declared-synchronized saveIntoDbAsync()V
    .locals 3

    .prologue
    .line 542
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mIsOnMemoryDb:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 582
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 546
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mFlusherThread:Ljava/lang/Thread;

    if-nez v0, :cond_2

    .line 547
    new-instance v0, Lcom/google/android/apps/translate/history/BaseDb$3;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-Flusher"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/translate/history/BaseDb$3;-><init>(Lcom/google/android/apps/translate/history/BaseDb;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mFlusherThread:Ljava/lang/Thread;

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mFlusherThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 542
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 580
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized add(Lcom/google/android/apps/translate/history/Entry;)V
    .locals 4
    .parameter "entry"

    .prologue
    .line 338
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v3, p1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/history/Entry;

    .line 339
    .local v1, oldEntry:Lcom/google/android/apps/translate/history/Entry;
    if-nez v1, :cond_1

    .line 340
    iget-object v3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->size()I

    move-result v0

    .line 341
    .local v0, newCount:I
    iget v3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMaxCount:I

    if-le v0, v3, :cond_0

    .line 343
    iget-object v3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->lastKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/translate/history/Entry;

    .line 344
    .local v2, oldest:Lcom/google/android/apps/translate/history/Entry;
    iget-object v3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v3, v2}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    iget-object v3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v3, v2}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    .end local v0           #newCount:I
    .end local v2           #oldest:Lcom/google/android/apps/translate/history/Entry;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v3, p1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    invoke-direct {p0}, Lcom/google/android/apps/translate/history/BaseDb;->onModified()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    monitor-exit p0

    return-void

    .line 348
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v3, v1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 338
    .end local v1           #oldEntry:Lcom/google/android/apps/translate/history/Entry;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public close(Z)V
    .locals 1
    .parameter "syncFlush"

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/history/BaseDb;->flush(Z)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mOpenedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 176
    return-void
.end method

.method public declared-synchronized exists(Lcom/google/android/apps/translate/history/Entry;)Z
    .locals 1
    .parameter "entry"

    .prologue
    .line 391
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized exists(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .parameter "fromLanguage"
    .parameter "toLanguage"
    .parameter "inputText"

    .prologue
    .line 399
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    new-instance v1, Lcom/google/android/apps/translate/history/Entry;

    const-string v2, ""

    invoke-direct {v1, p1, p2, p3, v2}, Lcom/google/android/apps/translate/history/Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public flush(Z)V
    .locals 0
    .parameter "sync"

    .prologue
    .line 196
    if-eqz p1, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/translate/history/BaseDb;->saveIntoDb()V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/translate/history/BaseDb;->saveIntoDbAsync()V

    goto :goto_0
.end method

.method public declared-synchronized get(Lcom/google/android/apps/translate/history/Entry;)Lcom/google/android/apps/translate/history/Entry;
    .locals 1
    .parameter "entry"

    .prologue
    .line 225
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/history/Entry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/translate/history/Entry;
    .locals 3
    .parameter "fromLanguage"
    .parameter "toLanguage"
    .parameter "inputText"

    .prologue
    .line 233
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    new-instance v1, Lcom/google/android/apps/translate/history/Entry;

    const-string v2, ""

    invoke-direct {v1, p1, p2, p3, v2}, Lcom/google/android/apps/translate/history/Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/history/Entry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAll()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAll(I)Ljava/util/List;
    .locals 2
    .parameter "count"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/history/BaseDb;->getAll()Ljava/util/List;

    move-result-object v0

    .line 252
    .local v0, result:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lt p1, v1, :cond_0

    .end local v0           #result:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :goto_0
    monitor-exit p0

    return-object v0

    .restart local v0       #result:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v0, v1, p1}, Ljava/util/List;->subList(II)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 251
    .end local v0           #result:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getAll(ILjava/lang/String;)Ljava/util/List;
    .locals 1
    .parameter "count"
    .parameter "prefixFilter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/history/BaseDb;->getAll(I)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 269
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/translate/history/BaseDb;->getFilteredEntries(ILjava/lang/String;Ljava/util/Collection;)Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAllByATime()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/history/BaseDb;->getAllByATime(I)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAllByATime(I)Ljava/util/List;
    .locals 3
    .parameter "count"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 311
    .local v0, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/translate/history/Entry;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt p1, v2, :cond_0

    .end local v0           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/translate/history/Entry;>;"
    :goto_0
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .restart local v0       #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/translate/history/Entry;>;"
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v2, p1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 310
    .end local v0           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/translate/history/Entry;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getAllByATime(ILjava/lang/String;)Ljava/util/List;
    .locals 1
    .parameter "count"
    .parameter "prefixFilter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translate/history/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/history/BaseDb;->getAllByATime(I)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 328
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/translate/history/BaseDb;->getAllByATime()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/translate/history/BaseDb;->getFilteredEntries(ILjava/lang/String;Ljava/util/Collection;)Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLastModifiedTime()J
    .locals 4

    .prologue
    .line 407
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLastModifiedTimeMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLastModifiedTimeMs:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRawCount()I
    .locals 1

    .prologue
    .line 383
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isOpened()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mOpenedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public open()Lcom/google/android/apps/translate/history/BaseDb;
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/google/android/apps/translate/history/BaseDb;->loadIntoMemory()V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mOpenedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 163
    return-object p0
.end method

.method public declared-synchronized remove(Lcom/google/android/apps/translate/history/Entry;)V
    .locals 2
    .parameter "entry"

    .prologue
    .line 363
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/translate/history/Entry;

    move-object p1, v0

    .line 364
    if-eqz p1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    invoke-direct {p0}, Lcom/google/android/apps/translate/history/BaseDb;->onModified()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    :cond_0
    monitor-exit p0

    return-void

    .line 363
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized removeAll()V
    .locals 1

    .prologue
    .line 374
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 376
    invoke-direct {p0}, Lcom/google/android/apps/translate/history/BaseDb;->onModified()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    monitor-exit p0

    return-void

    .line 374
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method saveIntoDb()V
    .locals 11

    .prologue
    .line 489
    monitor-enter p0

    .line 490
    :try_start_0
    iget-boolean v6, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/apps/translate/history/BaseDb;->mIsOnMemoryDb:Z

    if-eqz v6, :cond_1

    .line 491
    :cond_0
    monitor-exit p0

    .line 536
    :goto_0
    return-void

    .line 498
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v6}, Ljava/util/TreeMap;->size()I

    move-result v3

    .line 499
    .local v3, numEntries:I
    iget-object v6, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v6}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v6

    new-array v7, v3, [Lcom/google/android/apps/translate/history/Entry;

    invoke-interface {v6, v7}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/translate/history/Entry;

    .line 500
    .local v1, entryLruList:[Lcom/google/android/apps/translate/history/Entry;
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z

    .line 501
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    iget-object v7, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    monitor-enter v7

    .line 505
    const/4 v4, 0x0

    .line 508
    .local v4, stream:Ljava/io/DataOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/DataOutputStream;

    new-instance v6, Ljava/io/BufferedOutputStream;

    iget-object v8, p0, Lcom/google/android/apps/translate/history/BaseDb;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v8

    const v9, 0xc800

    invoke-direct {v6, v8, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    invoke-direct {v5, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 511
    .end local v4           #stream:Ljava/io/DataOutputStream;
    .local v5, stream:Ljava/io/DataOutputStream;
    const/4 v6, 0x1

    :try_start_2
    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 512
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 513
    aget-object v6, v1, v2

    invoke-virtual {v6, v5}, Lcom/google/android/apps/translate/history/Entry;->writeData(Ljava/io/DataOutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 512
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 501
    .end local v1           #entryLruList:[Lcom/google/android/apps/translate/history/Entry;
    .end local v2           #i:I
    .end local v3           #numEntries:I
    .end local v5           #stream:Ljava/io/DataOutputStream;
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    .line 517
    .restart local v1       #entryLruList:[Lcom/google/android/apps/translate/history/Entry;
    .restart local v2       #i:I
    .restart local v3       #numEntries:I
    .restart local v5       #stream:Ljava/io/DataOutputStream;
    :cond_2
    :try_start_4
    sget-object v6, Lcom/google/android/apps/translate/history/BaseDb;->EMPTY_ENTRY:Lcom/google/android/apps/translate/history/Entry;

    invoke-virtual {v6, v5}, Lcom/google/android/apps/translate/history/Entry;->writeData(Ljava/io/DataOutputStream;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 519
    if-eqz v5, :cond_3

    .line 521
    :try_start_5
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_3
    :goto_2
    move-object v4, v5

    .line 532
    .end local v2           #i:I
    .end local v5           #stream:Ljava/io/DataOutputStream;
    .restart local v4       #stream:Ljava/io/DataOutputStream;
    :goto_3
    :try_start_6
    monitor-exit v7

    goto :goto_0

    :catchall_1
    move-exception v6

    :goto_4
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 522
    .end local v4           #stream:Ljava/io/DataOutputStream;
    .restart local v2       #i:I
    .restart local v5       #stream:Ljava/io/DataOutputStream;
    :catch_0
    move-exception v0

    .line 523
    .local v0, e:Ljava/io/IOException;
    :try_start_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveIntoDb failed into "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 524
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 528
    :catch_1
    move-exception v0

    move-object v4, v5

    .line 529
    .end local v2           #i:I
    .end local v5           #stream:Ljava/io/DataOutputStream;
    .restart local v4       #stream:Ljava/io/DataOutputStream;
    :goto_5
    :try_start_8
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveIntoDb failed into "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 530
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_3

    .line 519
    .end local v0           #e:Ljava/io/IOException;
    :catchall_2
    move-exception v6

    :goto_6
    if-eqz v4, :cond_4

    .line 521
    :try_start_9
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 525
    :cond_4
    :goto_7
    :try_start_a
    throw v6

    .line 528
    :catch_2
    move-exception v0

    goto :goto_5

    .line 522
    :catch_3
    move-exception v0

    .line 523
    .restart local v0       #e:Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveIntoDb failed into "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/translate/history/BaseDb;->mDbName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 524
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/apps/translate/history/BaseDb;->mMemoryDirty:Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_7

    .line 532
    .end local v0           #e:Ljava/io/IOException;
    .end local v4           #stream:Ljava/io/DataOutputStream;
    .restart local v2       #i:I
    .restart local v5       #stream:Ljava/io/DataOutputStream;
    :catchall_3
    move-exception v6

    move-object v4, v5

    .end local v5           #stream:Ljava/io/DataOutputStream;
    .restart local v4       #stream:Ljava/io/DataOutputStream;
    goto :goto_4

    .line 519
    .end local v2           #i:I
    .end local v4           #stream:Ljava/io/DataOutputStream;
    .restart local v5       #stream:Ljava/io/DataOutputStream;
    :catchall_4
    move-exception v6

    move-object v4, v5

    .end local v5           #stream:Ljava/io/DataOutputStream;
    .restart local v4       #stream:Ljava/io/DataOutputStream;
    goto :goto_6
.end method

.method public declared-synchronized updateLastAccessed(Lcom/google/android/apps/translate/history/Entry;)V
    .locals 3
    .parameter "entry"

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mEntries:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/translate/history/Entry;

    move-object p1, v0

    .line 212
    if-eqz p1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/translate/history/Entry;->setAccessedTime(J)V

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/translate/history/BaseDb;->mLruEntries:Ljava/util/TreeMap;

    invoke-virtual {v1, p1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    invoke-direct {p0}, Lcom/google/android/apps/translate/history/BaseDb;->onModified()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :cond_0
    monitor-exit p0

    return-void

    .line 211
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
