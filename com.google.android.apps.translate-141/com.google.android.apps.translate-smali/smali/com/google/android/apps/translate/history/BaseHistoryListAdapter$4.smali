.class Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;
.super Ljava/lang/Thread;
.source "BaseHistoryListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->loadListInBackground(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

.field final synthetic val$dbChanged:Z

.field final synthetic val$filter:Ljava/lang/String;

.field final synthetic val$force:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;Ljava/lang/String;ZZ)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 212
    iput-object p1, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->this$0:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    iput-object p2, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->val$filter:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->val$force:Z

    iput-boolean p4, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->val$dbChanged:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 215
    iget-object v2, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->this$0:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    iget-object v3, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->val$filter:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->fetchEntriesFromDb(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 216
    .local v0, historyEntries:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/translate/history/HistoryEntry;>;"
    iget-object v2, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->this$0:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    iput-boolean v1, v2, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->mIsLoadingList:Z

    .line 217
    iget-boolean v2, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->val$force:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->val$dbChanged:Z

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 218
    .local v1, scrollToTop:Z
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$4;->this$0:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->refreshHistoryList(Ljava/util/List;Z)V

    .line 219
    return-void
.end method
