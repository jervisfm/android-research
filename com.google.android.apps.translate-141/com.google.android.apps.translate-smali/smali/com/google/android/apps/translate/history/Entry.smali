.class public Lcom/google/android/apps/translate/history/Entry;
.super Ljava/lang/Object;
.source "Entry.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final KEY_DELIMITER:Ljava/lang/String; = "\\t"

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mAccessedTime:J

.field private mCreatedTime:J

.field private final mInputText:Ljava/lang/String;

.field private final mLanguageFrom:Ljava/lang/String;

.field private final mLanguageTo:Ljava/lang/String;

.field private final mOutputText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "from"
    .parameter "to"
    .parameter "inputText"
    .parameter "outputText"

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    .line 49
    invoke-static {p2}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    .line 50
    invoke-static {p3}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    .line 51
    invoke-static {p4}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    .line 52
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/translate/history/Entry;)V
    .locals 2
    .parameter "entry"

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    .line 60
    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    .line 61
    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    .line 62
    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    .line 63
    iget-wide v0, p1, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    .line 64
    iget-wide v0, p1, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    .line 65
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/translate/history/Entry;Ljava/lang/String;)V
    .locals 2
    .parameter "entry"
    .parameter "outputText"

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    .line 72
    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    .line 73
    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    .line 75
    iget-wide v0, p1, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    .line 76
    iget-wide v0, p1, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    .line 77
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "fromShortName"
    .parameter "toShortName"
    .parameter "inputText"
    .parameter "outputText"

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    .line 38
    invoke-static {p2}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    .line 39
    invoke-static {p3}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    .line 40
    invoke-static {p4}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    .line 41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    iput-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    .line 42
    return-void
.end method

.method public static getDbKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "from"
    .parameter "to"
    .parameter "inputText"

    .prologue
    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/google/android/apps/translate/Util;->generateLanguagePairText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readData(Ljava/io/DataInputStream;)Lcom/google/android/apps/translate/history/Entry;
    .locals 8
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v2

    .line 157
    .local v2, from:Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v5

    .line 158
    .local v5, to:Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    .line 159
    .local v3, inputText:Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    .line 160
    .local v4, outputText:Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/translate/history/Entry;

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/google/android/apps/translate/history/Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    .local v1, entry:Lcom/google/android/apps/translate/history/Entry;
    :try_start_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/translate/history/Entry;->setCreatedTime(J)V

    .line 163
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/translate/history/Entry;->setAccessedTime(J)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    return-object v1

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, e:Ljava/lang/IllegalArgumentException;
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Failed to read time from stream"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public static readKey(Ljava/io/DataInputStream;)Lcom/google/android/apps/translate/history/Entry;
    .locals 5
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, from:Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v2

    .line 176
    .local v2, to:Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, inputText:Ljava/lang/String;
    new-instance v3, Lcom/google/android/apps/translate/history/Entry;

    const-string v4, ""

    invoke-direct {v3, v0, v2, v1, v4}, Lcom/google/android/apps/translate/history/Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method


# virtual methods
.method public getAccessedTime()J
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    return-wide v0
.end method

.method public getCreatedTime()J
    .locals 2

    .prologue
    .line 109
    iget-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    return-wide v0
.end method

.method public getDbKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/translate/history/Entry;->getDbKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryWithoutOnMemoryAttributes()Lcom/google/android/apps/translate/history/Entry;
    .locals 3

    .prologue
    .line 198
    new-instance v0, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Ljava/lang/String;Z)V

    .line 199
    .local v0, result:Lcom/google/android/apps/translate/Translate$TranslateResult;
    invoke-virtual {v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->stripOnMemoryAttributes()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    new-instance v1, Lcom/google/android/apps/translate/history/Entry;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/translate/history/Entry;-><init>(Lcom/google/android/apps/translate/history/Entry;Ljava/lang/String;)V

    move-object p0, v1

    .line 202
    .end local p0
    :cond_0
    return-object p0
.end method

.method public getFromLanguage(Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/Language;
    .locals 1
    .parameter "languages"

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v0

    return-object v0
.end method

.method public getFromLanguageShortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    return-object v0
.end method

.method public getInputText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    return-object v0
.end method

.method public getToLanguage(Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/Language;
    .locals 1
    .parameter "languages"

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v0

    return-object v0
.end method

.method public getToLanguageShortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslation()Ljava/lang/String;
    .locals 3

    .prologue
    .line 104
    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 105
    .local v0, separator:I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    goto :goto_0
.end method

.method public isSameTranslation(Lcom/google/android/apps/translate/history/Entry;)Z
    .locals 2
    .parameter "entry"

    .prologue
    .line 146
    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAccessedTime(J)V
    .locals 2
    .parameter "accessedTime"

    .prologue
    .line 122
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/translate/Preconditions;->checkArguments(Z)V

    .line 123
    iput-wide p1, p0, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    .line 124
    return-void

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCreatedTime(J)V
    .locals 2
    .parameter "createdTime"

    .prologue
    .line 113
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/translate/Preconditions;->checkArguments(Z)V

    .line 114
    iput-wide p1, p0, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    .line 115
    return-void

    .line 113
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeData(Ljava/io/DataOutputStream;)V
    .locals 2
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageFrom:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mLanguageTo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mInputText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/translate/history/Entry;->mOutputText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 188
    iget-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mCreatedTime:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 189
    iget-wide v0, p0, Lcom/google/android/apps/translate/history/Entry;->mAccessedTime:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 190
    return-void
.end method
