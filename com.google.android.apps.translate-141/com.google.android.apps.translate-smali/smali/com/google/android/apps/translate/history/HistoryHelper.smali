.class public Lcom/google/android/apps/translate/history/HistoryHelper;
.super Lcom/google/android/apps/translate/translation/BaseTranslateHelper;
.source "HistoryHelper.java"

# interfaces
.implements Lcom/google/android/apps/translate/editor/PreImeAutoCompleteTextView$OnKeyPreImeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/history/HistoryHelper$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HistoryHelper"


# instance fields
.field private mIsHistoryMode:Z

.field private mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mIsHistoryMode:Z

    return-void
.end method


# virtual methods
.method public getListAdapter()Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    return-object v0
.end method

.method public loadDatabaseFile(Z)V
    .locals 3
    .parameter "isHistory"

    .prologue
    const/4 v2, 0x1

    .line 116
    iput-boolean p1, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mIsHistoryMode:Z

    .line 117
    if-eqz p1, :cond_0

    .line 118
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->HISTORY_VIEWS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 119
    new-instance v0, Lcom/google/android/apps/translate/history/HistoryListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    sget-object v2, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$HistoryDisplayMode;->HISTORY:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$HistoryDisplayMode;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/translate/history/HistoryListAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$HistoryDisplayMode;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    .line 124
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->FAVORITES_VIEWS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 122
    new-instance v0, Lcom/google/android/apps/translate/history/HistoryListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    sget-object v2, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$HistoryDisplayMode;->FAVORITE:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$HistoryDisplayMode;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/translate/history/HistoryListAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/apps/translate/history/BaseHistoryListAdapter$HistoryDisplayMode;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)Z
    .locals 2
    .parameter "newConfig"

    .prologue
    .line 248
    const-string v0, "HistoryHelper"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->onConfigurationChanged(Landroid/content/res/Configuration;)Z

    move-result v0

    .line 252
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 11
    .parameter "item"

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 136
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v2

    check-cast v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 137
    .local v2, mi:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget v3, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    .line 139
    .local v3, position:I
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 140
    .local v1, id:I
    sget v8, Lcom/google/android/apps/translate/R$id;->context_menu_translate_input_text:I

    if-ne v1, v8, :cond_1

    .line 141
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getEntry(I)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 142
    .local v0, entry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    iget-boolean v8, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mIsHistoryMode:Z

    invoke-virtual {v6, v0, v8}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->openTranslateActivity(Lcom/google/android/apps/translate/history/Entry;Z)V

    .line 143
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v6

    sget-object v8, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_SRC_TRANSLATE_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v6, v8, v7}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    move v6, v7

    .line 198
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_0
    :goto_0
    return v6

    .line 146
    :cond_1
    sget v8, Lcom/google/android/apps/translate/R$id;->context_menu_translate_translation:I

    if-ne v1, v8, :cond_2

    .line 147
    iget-object v8, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v8, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getEntry(I)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 148
    .restart local v0       #entry:Lcom/google/android/apps/translate/history/Entry;
    new-instance v5, Lcom/google/android/apps/translate/history/Entry;

    iget-object v8, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/translate/history/Entry;->getToLanguage(Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/Language;

    move-result-object v8

    invoke-virtual {p0, v8, v6}, Lcom/google/android/apps/translate/history/HistoryHelper;->getFromLanguageGivenToLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguage(Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/Language;

    move-result-object v9

    invoke-virtual {p0, v9, v6}, Lcom/google/android/apps/translate/history/HistoryHelper;->getToLanguageGivenFromLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/Entry;->getTranslation()Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-direct {v5, v8, v6, v9, v10}, Lcom/google/android/apps/translate/history/Entry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .local v5, reverseEntry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    iget-boolean v8, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mIsHistoryMode:Z

    invoke-virtual {v6, v5, v8}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->openTranslateActivity(Lcom/google/android/apps/translate/history/Entry;Z)V

    .line 154
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v6

    sget-object v8, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_TRG_TRANSLATE_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v6, v8, v7}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    move v6, v7

    .line 156
    goto :goto_0

    .line 157
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    .end local v5           #reverseEntry:Lcom/google/android/apps/translate/history/Entry;
    :cond_2
    sget v8, Lcom/google/android/apps/translate/R$id;->context_menu_copy_input_text:I

    if-ne v1, v8, :cond_3

    .line 158
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    iget-object v8, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v8, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getEntry(I)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->INPUT_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v6, v8, v9}, Lcom/google/android/apps/translate/Util;->copyToClipBoard(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    .line 160
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    sget v8, Lcom/google/android/apps/translate/R$string;->toast_message_copy_input_text:I

    invoke-static {v6, v8}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;I)V

    .line 161
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->notifyDataSetChanged()V

    move v6, v7

    .line 162
    goto :goto_0

    .line 163
    :cond_3
    sget v8, Lcom/google/android/apps/translate/R$id;->context_menu_copy:I

    if-ne v1, v8, :cond_4

    .line 164
    new-instance v4, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getEntry(I)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Lcom/google/android/apps/translate/history/Entry;)V

    .line 166
    .local v4, results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v6, v8, v9}, Lcom/google/android/apps/translate/Util;->copyToClipBoard(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    .line 167
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    sget v8, Lcom/google/android/apps/translate/R$string;->toast_message_copy:I

    invoke-static {v6, v8}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;I)V

    .line 168
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->notifyDataSetChanged()V

    move v6, v7

    .line 169
    goto/16 :goto_0

    .line 170
    .end local v4           #results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    :cond_4
    sget v8, Lcom/google/android/apps/translate/R$id;->context_menu_remove:I

    if-ne v1, v8, :cond_5

    .line 171
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->removeHistoryEntry(I)V

    move v6, v7

    .line 172
    goto/16 :goto_0

    .line 173
    :cond_5
    sget v8, Lcom/google/android/apps/translate/R$id;->context_menu_share:I

    if-ne v1, v8, :cond_7

    .line 174
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getEntry(I)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 175
    .restart local v0       #entry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v6, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    instance-of v6, v6, Lcom/google/android/apps/translate/translation/ChipView;

    if-eqz v6, :cond_6

    .line 176
    iget-object v8, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    iget-object v9, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v6, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    check-cast v6, Lcom/google/android/apps/translate/translation/ChipView;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/translation/ChipView;->getSelectedChipPart()Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    move-result-object v6

    invoke-static {v8, v9, v0, v6}, Lcom/google/android/apps/translate/translation/TranslateHelper;->performChipActionShare(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    .line 183
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->notifyDataSetChanged()V

    move v6, v7

    .line 184
    goto/16 :goto_0

    .line 180
    :cond_6
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    iget-object v8, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    sget-object v9, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->NONE:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v6, v8, v0, v9}, Lcom/google/android/apps/translate/translation/TranslateHelper;->performChipActionShare(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    goto :goto_1

    .line 185
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_7
    sget v8, Lcom/google/android/apps/translate/R$id;->context_menu_search_input_text:I

    if-ne v1, v8, :cond_8

    .line 186
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getEntry(I)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 187
    .restart local v0       #entry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->INPUT_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v6, v8, v9}, Lcom/google/android/apps/translate/Util;->searchTextOnWeb(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    .line 188
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->notifyDataSetChanged()V

    move v6, v7

    .line 189
    goto/16 :goto_0

    .line 190
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_8
    sget v8, Lcom/google/android/apps/translate/R$id;->context_menu_search_translation:I

    if-ne v1, v8, :cond_0

    .line 191
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getEntry(I)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 192
    .restart local v0       #entry:Lcom/google/android/apps/translate/history/Entry;
    new-instance v4, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getEntry(I)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Lcom/google/android/apps/translate/history/Entry;)V

    .line 194
    .restart local v4       #results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v6, v8, v9}, Lcom/google/android/apps/translate/Util;->searchTextOnWeb(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    .line 195
    iget-object v6, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->notifyDataSetChanged()V

    move v6, v7

    .line 196
    goto/16 :goto_0
.end method

.method public onCreate(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 51
    const-string v0, "HistoryHelper"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-static {}, Lcom/google/android/apps/translate/LanguagesFactory;->get()Lcom/google/android/apps/translate/LanguagesFactory;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/translate/LanguagesFactory;->getLanguages(Landroid/content/Context;Ljava/util/Locale;)Lcom/google/android/apps/translate/Languages;

    move-result-object v0

    invoke-super {p0, p1, v0}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;->onCreate(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;)V

    .line 55
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    .line 202
    iget-object v4, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    sget v5, Lcom/google/android/apps/translate/R$menu;->history_activity_context_menu:I

    invoke-virtual {v4, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    move-object v2, p3

    .line 203
    check-cast v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 204
    .local v2, mi:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget v3, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    .line 205
    .local v3, position:I
    iget-object v4, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getHistoryEntryItem(I)Lcom/google/android/apps/translate/history/HistoryEntry;

    move-result-object v4

    iget-object v1, v4, Lcom/google/android/apps/translate/history/HistoryEntry;->entry:Lcom/google/android/apps/translate/history/Entry;

    .line 206
    .local v1, entry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v4, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    instance-of v4, v4, Lcom/google/android/apps/translate/translation/ChipView;

    if-eqz v4, :cond_0

    .line 207
    iget-object v0, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/translate/translation/ChipView;

    .line 208
    .local v0, chipView:Lcom/google/android/apps/translate/translation/ChipView;
    sget-object v4, Lcom/google/android/apps/translate/history/HistoryHelper$1;->$SwitchMap$com$google$android$apps$translate$translation$ChipView$ChipPart:[I

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/ChipView;->getSelectedChipPart()Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 245
    .end local v0           #chipView:Lcom/google/android/apps/translate/translation/ChipView;
    :goto_0
    return-void

    .line 210
    .restart local v0       #chipView:Lcom/google/android/apps/translate/translation/ChipView;
    :pswitch_0
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_translate_translation:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 211
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_share:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 212
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_copy:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 213
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_search_translation:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 214
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_copy_input_text:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 215
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_search_input_text:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 216
    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_0

    .line 219
    :pswitch_1
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_translate_translation:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 220
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_remove:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 221
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_copy:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 222
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_search_translation:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 223
    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_0

    .line 226
    :pswitch_2
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_translate_input_text:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 227
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_remove:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 228
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_copy_input_text:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 229
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_search_input_text:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 230
    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getTranslation()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_0

    .line 239
    .end local v0           #chipView:Lcom/google/android/apps/translate/translation/ChipView;
    :cond_0
    sget v4, Lcom/google/android/apps/translate/R$id;->context_menu_share:I

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 243
    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 1
    .parameter "menu"
    .parameter "inflater"

    .prologue
    .line 92
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 93
    sget v0, Lcom/google/android/apps/translate/R$menu;->history_activity_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->onDestroy()V

    .line 83
    :cond_0
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 260
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    .prologue
    .line 86
    iget-object v1, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->getHistoryEntryItem(I)Lcom/google/android/apps/translate/history/HistoryEntry;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/translate/history/HistoryEntry;->entry:Lcom/google/android/apps/translate/history/Entry;

    .line 87
    .local v0, entry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v1, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mIsHistoryMode:Z

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->openTranslateActivity(Lcom/google/android/apps/translate/history/Entry;Z)V

    .line 88
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 104
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 105
    .local v0, id:I
    sget v2, Lcom/google/android/apps/translate/R$id;->menu_feedback:I

    if-ne v0, v2, :cond_0

    .line 106
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->sendFeedback(Landroid/app/Activity;Z)V

    .line 109
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;->onPause()V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->onPause()V

    .line 67
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .parameter "menu"

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 101
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 71
    const-string v0, "HistoryHelper"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-super {p0}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;->onResume()V

    .line 73
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->setHomeButton(Landroid/app/Activity;Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->onResume()V

    .line 77
    :cond_0
    return-void
.end method

.method public setFlushOnPause(Z)V
    .locals 1
    .parameter "flushOnPause"

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->setFlushOnPause(Z)V

    .line 133
    return-void
.end method

.method public setListView(Landroid/widget/ListView;)V
    .locals 2
    .parameter "listView"

    .prologue
    .line 127
    const-string v0, "HistoryHelper"

    const-string v1, "setListView"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/translate/history/HistoryHelper;->mListAdapter:Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/history/BaseHistoryListAdapter;->setListView(Landroid/widget/ListView;)V

    .line 129
    return-void
.end method
