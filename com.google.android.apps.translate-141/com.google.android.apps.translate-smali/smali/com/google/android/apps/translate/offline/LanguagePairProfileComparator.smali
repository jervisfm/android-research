.class public Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;
.super Ljava/lang/Object;
.source "LanguagePairProfileComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;",
        ">;"
    }
.end annotation


# instance fields
.field private mLanguageList:Lcom/google/android/apps/translate/Languages;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/translate/Languages;)V
    .locals 0
    .parameter "languageList"

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 24
    return-void
.end method

.method private getLanguageNameWithEnglishMovedToEnd(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "lang"

    .prologue
    .line 43
    const-string v0, "en"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const-string v0, "zzz"

    .line 47
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->getLanguageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;)I
    .locals 7
    .parameter "p1"
    .parameter "p2"

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->getFromLangCode()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->getLanguageNameWithEnglishMovedToEnd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, fromLang1:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->getToLangCode()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->getLanguageNameWithEnglishMovedToEnd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 30
    .local v3, toLang1:Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->getFromLangCode()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->getLanguageNameWithEnglishMovedToEnd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 31
    .local v1, fromLang2:Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->getToLangCode()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->getLanguageNameWithEnglishMovedToEnd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 33
    .local v4, toLang2:Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    .line 34
    .local v5, toLangCompare:I
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 35
    .local v2, fromLangCompare:I
    if-nez v2, :cond_0

    .line 38
    .end local v5           #toLangCompare:I
    :goto_0
    return v5

    .restart local v5       #toLangCompare:I
    :cond_0
    move v5, v2

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    .end local p1
    check-cast p2, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->compare(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;)I

    move-result v0

    return v0
.end method

.method public getLanguageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "lang"

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    if-nez v1, :cond_1

    move-object v0, p1

    .line 62
    :cond_0
    :goto_0
    return-object v0

    .line 58
    :cond_1
    const-string v1, "zh"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 59
    const-string p1, "zh-CN"

    .line 61
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/translate/Languages;->getFromLanguageLongName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, longName:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, p1

    goto :goto_0
.end method
