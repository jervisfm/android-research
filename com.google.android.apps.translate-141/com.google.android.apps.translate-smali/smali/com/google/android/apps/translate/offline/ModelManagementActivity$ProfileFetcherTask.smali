.class Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;
.super Landroid/os/AsyncTask;
.source "ModelManagementActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/offline/ModelManagementActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProfileFetcherTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/translate/offline/ModelManagementActivity;Lcom/google/android/apps/translate/offline/ModelManagementActivity$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;-><init>(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    check-cast p1, [Ljava/lang/String;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 1
    .parameter "id"

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #calls: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->fetchProfiles()V
    invoke-static {v0}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$200(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)V

    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 64
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 7
    .parameter "result"

    .prologue
    .line 83
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$300(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$300(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->getLanguageProfiles()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$300(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->getLanguagePairProfiles()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 86
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->notifyDataSetChanged()V

    .line 87
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->localProfiles:Landroid/content/SharedPreferences;
    invoke-static {v5}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$500(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->setLocalProfiles(Landroid/content/SharedPreferences;)V

    .line 88
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    invoke-static {v5}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$300(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->getCommonProfile()Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->setCommonProfile(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;)V

    .line 89
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$300(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->getLanguageProfiles()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    .line 90
    .local v3, p:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->addLanguageProfile(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;)V

    goto :goto_0

    .line 92
    .end local v3           #p:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    :cond_0
    invoke-static {}, Lcom/google/android/apps/translate/LanguagesFactory;->get()Lcom/google/android/apps/translate/LanguagesFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/translate/LanguagesFactory;->getLanguages(Landroid/content/Context;Ljava/util/Locale;)Lcom/google/android/apps/translate/Languages;

    move-result-object v2

    .line 94
    .local v2, languageList:Lcom/google/android/apps/translate/Languages;
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$300(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->getLanguagePairProfiles()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    .line 95
    .local v3, p:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 97
    .end local v3           #p:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    :cond_1
    new-instance v0, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;

    invoke-direct {v0, v2}, Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;-><init>(Lcom/google/android/apps/translate/Languages;)V

    .line 99
    .local v0, comparator:Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->sort(Ljava/util/Comparator;)V

    .line 100
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->setLanguagePairProfileComparator(Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;)V

    .line 104
    .end local v0           #comparator:Lcom/google/android/apps/translate/offline/LanguagePairProfileComparator;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #languageList:Lcom/google/android/apps/translate/Languages;
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$100(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    .line 106
    iget-object v4, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    #getter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    invoke-static {v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;->notifyDataSetChanged()V

    .line 107
    return-void

    .line 102
    :cond_2
    const-string v4, "profilesFetcher"

    const-string v5, "No profiles"

    invoke-static {v4, v5}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    iget-object v1, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->this$0:Lcom/google/android/apps/translate/offline/ModelManagementActivity;

    const-string v2, "Please wait..."

    const-string v3, "Retrieving profiles ..."

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v1

    #setter for: Lcom/google/android/apps/translate/offline/ModelManagementActivity;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->access$102(Lcom/google/android/apps/translate/offline/ModelManagementActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 71
    return-void
.end method
