.class public Lcom/google/android/apps/translate/offline/ModelManagementActivity;
.super Landroid/app/ListActivity;
.source "ModelManagementActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/offline/ModelManagementActivity$1;,
        Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;
    }
.end annotation


# static fields
.field private static final OFFLINE_TRANSLATE_SUPPORTED:Z


# instance fields
.field private backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

.field private localProfiles:Landroid/content/SharedPreferences;

.field private profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;

.field private progressDialog:Landroid/app/ProgressDialog;

.field private final usbMode:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->usbMode:Z

    .line 64
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/translate/offline/ModelManagementActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->progressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->fetchProfiles()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Lcom/google/android/apps/translate/offline/ProfilesAdapter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/offline/ModelManagementActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->localProfiles:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private fetchProfiles()V
    .locals 3

    .prologue
    .line 116
    const-string v1, "http://dl.google.com/translate/offline/profiles.txt"

    invoke-static {}, Lcom/google/android/apps/translate/offline/LocalFileNameUtil;->getLocalProfilesFile()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/offline/ModelDownloader;->downloadFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    const-string v1, "profileFetcher"

    const-string v2, "Download profile error"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :goto_0
    return-void

    .line 125
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/translate/offline/LocalFileNameUtil;->getLocalProfilesFile()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->backendProfiles:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    :try_end_0
    .catch Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    const-string v1, "localModelProfiles"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->localProfiles:Landroid/content/SharedPreferences;

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, e:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static isOfflineTranslateSupported(Landroid/content/Context;)Z
    .locals 1
    .parameter "context"

    .prologue
    .line 142
    invoke-static {p0}, Lcom/google/android/apps/translate/TranslateApplication;->isReleaseBuild(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 53
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    sget v0, Lcom/google/android/apps/translate/R$layout;->offline_model_management:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->setContentView(I)V

    .line 55
    new-instance v0, Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    sget v2, Lcom/google/android/apps/translate/R$layout;->offline_model_adaptor_row:I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/offline/ProfilesAdapter;-><init>(Landroid/content/Context;ILjava/util/List;Landroid/content/SharedPreferences;Z)V

    iput-object v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->profilesAdapter:Lcom/google/android/apps/translate/offline/ProfilesAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/offline/ModelManagementActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 60
    new-instance v0, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;-><init>(Lcom/google/android/apps/translate/offline/ModelManagementActivity;Lcom/google/android/apps/translate/offline/ModelManagementActivity$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/offline/ModelManagementActivity$ProfileFetcherTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 61
    return-void
.end method
