.class public Lcom/google/android/apps/translate/translation/ChipView;
.super Landroid/widget/LinearLayout;
.source "ChipView.java"

# interfaces
.implements Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/translation/ChipView$7;,
        Lcom/google/android/apps/translate/translation/ChipView$StarOnClickListener;,
        Lcom/google/android/apps/translate/translation/ChipView$NetworkTtsCallback;,
        Lcom/google/android/apps/translate/translation/ChipView$ChipViewTag;,
        Lcom/google/android/apps/translate/translation/ChipView$ChipPart;,
        Lcom/google/android/apps/translate/translation/ChipView$ViewState;
    }
.end annotation


# static fields
.field private static final ADD_LITTLE_STAR_TO_UNSELECTED_CHIP:Z = true

.field private static final DEBUG:Z = false

.field private static final EXPANDING_ANIMATION_DELAY_MILLIS:I = 0x32

.field private static final LOWER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final PADDING_CHIP_RESOURCE_ID_LIST:Ljava/util/Set; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHONETIC_FONT:Lcom/google/android/apps/translate/ExternalFonts; = null

.field private static final PHONETIC_SPAN:Lcom/google/android/apps/translate/ExtTypefaceSpan; = null

.field private static final SCROLLING_ANIMATION_DELAY_MILLIS:I = 0x32

.field private static final SCROLLING_ANIMATION_MILLIS:I = 0x12c

.field private static final SCROLLING_OVERRUN_PX:I = 0x0

.field private static final SUGGEST_CHIP_RESOURCE_ID_LIST:Ljava/util/Set; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ChipView"

.field private static final UNSELECT_ON_TOUCH:Z = true

.field private static final UPPER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccessTimeStampView:Landroid/widget/TextView;

.field private mActivity:Landroid/app/Activity;

.field private mAnimateScroll:Z

.field private mApp:Lcom/google/android/apps/translate/TranslateApplication;

.field private mChip:Landroid/widget/LinearLayout;

.field private mChipDivider:Landroid/view/View;

.field private mCreateTimeStampView:Landroid/widget/TextView;

.field private mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

.field private mDictDivider:Landroid/view/View;

.field private mFromLanguage:Lcom/google/android/apps/translate/Language;

.field private mFromLanguageText:Landroid/widget/TextView;

.field private mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

.field private mInputText:Landroid/widget/TextView;

.field private mInputTextChip:Landroid/widget/LinearLayout;

.field private mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

.field private mIsExpanded:Z

.field private mItemEntry:Lcom/google/android/apps/translate/history/HistoryEntry;

.field private mLanguageList:Lcom/google/android/apps/translate/Languages;

.field private mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mLittleStarBtn1:Landroid/widget/ImageButton;

.field private mLittleStarBtn2:Landroid/widget/ImageButton;

.field private mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

.field private mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

.field private mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

.field private mSrcTransliteration:Landroid/widget/TextView;

.field private mSrcTts:Landroid/view/View;

.field private mSrcTtsWrapper:Landroid/view/View;

.field private mSuggestTypeView:Landroid/widget/TextView;

.field private mSuggestView:Landroid/widget/TextView;

.field private mToLanguage:Lcom/google/android/apps/translate/Language;

.field private mToLanguageText:Landroid/widget/TextView;

.field private mTouchEventViewId:I

.field private mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

.field private mTranslatedText:Landroid/widget/TextView;

.field private mTranslationChip:Landroid/widget/LinearLayout;

.field private mTrgTransliteration:Landroid/widget/TextView;

.field private mTrgTts:Landroid/view/View;

.field private mTrgTtsWrapper:Landroid/view/View;

.field private mTts:Lcom/google/android/apps/translate/tts/MyTts;

.field private mTtsSpinningWheelHeight:I

.field private mTtsSpinningWheelWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/apps/translate/ExternalFonts;

    const-string v1, "ipa.ttf"

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/ExternalFonts;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/translate/translation/ChipView;->PHONETIC_FONT:Lcom/google/android/apps/translate/ExternalFonts;

    .line 66
    new-instance v0, Lcom/google/android/apps/translate/ExtTypefaceSpan;

    sget-object v1, Lcom/google/android/apps/translate/translation/ChipView;->PHONETIC_FONT:Lcom/google/android/apps/translate/ExternalFonts;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/ExternalFonts;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/ExtTypefaceSpan;-><init>(Landroid/graphics/Typeface;)V

    sput-object v0, Lcom/google/android/apps/translate/translation/ChipView;->PHONETIC_SPAN:Lcom/google/android/apps/translate/ExtTypefaceSpan;

    .line 134
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/translate/translation/ChipView;->UPPER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    .line 135
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/translate/translation/ChipView;->SUGGEST_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    .line 136
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/translate/translation/ChipView;->LOWER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    .line 137
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/translate/translation/ChipView;->PADDING_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    .line 144
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->UPPER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    sget v1, Lcom/google/android/apps/translate/R$id;->mvh_input_text:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 146
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->SUGGEST_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    sget v1, Lcom/google/android/apps/translate/R$id;->mvh_suggest_text:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 148
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->LOWER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    sget v1, Lcom/google/android/apps/translate/R$id;->mvh_translated_text:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 150
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->PADDING_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    sget v1, Lcom/google/android/apps/translate/R$id;->chip_divider:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attr"

    .prologue
    const/4 v0, 0x0

    .line 154
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 120
    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mAnimateScroll:Z

    .line 138
    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mIsExpanded:Z

    .line 155
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/translation/ChipView;)Lcom/google/android/apps/translate/tts/MyTts;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/translation/ChipView;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslationChip:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguageText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguageText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mChipDivider:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTtsWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTtsWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/ImageButton;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn1:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/ImageButton;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn2:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/translate/translation/ChipView;)Lcom/google/android/apps/translate/translation/OutputPanelView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTransliteration:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTts:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTransliteration:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/translate/translation/ChipView;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mIsExpanded:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/google/android/apps/translate/translation/ChipView;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mIsExpanded:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/google/android/apps/translate/translation/ChipView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/ChipView;->initializeOutputPanelView()V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/translate/translation/ChipView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/ChipView;->setSpeakerIcons()V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/translate/translation/ChipView;)Lcom/google/android/apps/translate/history/HistoryListAdapter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/translate/translation/ChipView;Lcom/google/android/apps/translate/translation/ChipView$ViewState;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/translation/ChipView;->renderOutputView(Lcom/google/android/apps/translate/translation/ChipView$ViewState;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/ListView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/translate/translation/ChipView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTtsSpinningWheelWidth:I

    return v0
.end method

.method static synthetic access$2900(Lcom/google/android/apps/translate/translation/ChipView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTtsSpinningWheelHeight:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/google/android/apps/translate/translation/ChipView;)Lcom/google/android/apps/translate/history/HistoryEntry;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mItemEntry:Lcom/google/android/apps/translate/history/HistoryEntry;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/apps/translate/translation/ChipView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/ChipView;->getPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/translate/translation/ChipView;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/translation/ChipView;->setStar(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTts:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/translation/ChipView;)Ljava/lang/CharSequence;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/ChipView;->getTranslationText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mChip:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslatedText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/translate/translation/ChipView;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputTextChip:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private canSpeak(Ljava/lang/String;)Z
    .locals 4
    .parameter "shortName"

    .prologue
    .line 845
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    if-nez v1, :cond_0

    .line 846
    const/4 v0, 0x0

    .line 851
    :goto_0
    return v0

    .line 849
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->languageShortNameToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/tts/MyTts;->isLanguageAvailable(Ljava/util/Locale;)Z

    move-result v0

    .line 850
    .local v0, isAvailable:Z
    const-string v1, "ChipView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is TTS available: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", return: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getPosition()I
    .locals 2

    .prologue
    .line 658
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/ChipView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translate/translation/ChipView$ChipViewTag;

    .line 660
    .local v0, tag:Lcom/google/android/apps/translate/translation/ChipView$ChipViewTag;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/google/android/apps/translate/translation/ChipView$ChipViewTag;->position:I

    goto :goto_0
.end method

.method private getTranslationText()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 836
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-nez v1, :cond_0

    .line 837
    const-string v1, ""

    .line 841
    :goto_0
    return-object v1

    .line 839
    :cond_0
    new-instance v0, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v1, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Ljava/lang/String;)V

    .line 841
    .local v0, results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    invoke-virtual {v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private initializeOutputPanelView()V
    .locals 3

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v0, p0, p0, v1, v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->init(Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;Lcom/google/android/apps/translate/translation/ChipView;Lcom/google/android/apps/translate/TranslateManager;Lcom/google/android/apps/translate/Languages;)V

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/translation/OutputPanelView;->postBackCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/translation/OutputPanelView;->setResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 481
    return-void
.end method

.method private renderOutputView(Lcom/google/android/apps/translate/translation/ChipView$ViewState;)V
    .locals 2
    .parameter "viewState"

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/ChipView$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/translate/translation/ChipView$3;-><init>(Lcom/google/android/apps/translate/translation/ChipView;Lcom/google/android/apps/translate/translation/ChipView$ViewState;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 475
    return-void
.end method

.method private selectAndOpenContextMenu()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 531
    iget-boolean v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mIsExpanded:Z

    if-eqz v0, :cond_2

    .line 532
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->UPPER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    iget v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputTextChip:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputTextChip:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->openContextMenu(Landroid/view/View;)V

    .line 547
    :cond_0
    :goto_0
    return-void

    .line 535
    :cond_1
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->LOWER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    iget v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/ChipView;->getTranslationText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslationChip:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslationChip:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->openContextMenu(Landroid/view/View;)V

    goto :goto_0

    .line 542
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->setSelected(Z)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->openContextMenu(Landroid/view/View;)V

    goto :goto_0
.end method

.method private selectChip(I)V
    .locals 9
    .parameter "position"

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 550
    const-string v2, "ChipView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectChip position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    if-nez v2, :cond_4

    .line 553
    sget-object v2, Lcom/google/android/apps/translate/translation/ChipView;->SUGGEST_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    iget v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 554
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    if-eqz v2, :cond_1

    .line 555
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->logSuggestionClick(Landroid/content/Context;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;Z)V

    .line 556
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/translation/InputPanel;->copyToInputTextBox(Ljava/lang/String;)V

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    if-eqz v2, :cond_0

    .line 558
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    .line 560
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->logSuggestionClick(Landroid/content/Context;Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;Z)V

    .line 561
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getSourceText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/translate/translation/InputPanel;->onSourceLanguageChangeRequested(Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    goto :goto_0

    .line 564
    :cond_2
    sget-object v2, Lcom/google/android/apps/translate/translation/ChipView;->UPPER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    iget v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 565
    const-string v2, "ChipView"

    const-string v3, "selectChip TOUCHTOUCH SRC"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v2, :cond_0

    .line 567
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->CHIPVIEW_SRCTEXT:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 569
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    sget-object v3, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->INPUT_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/translation/InputPanel;->copyToInputTextBox(Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    goto :goto_0

    .line 571
    :cond_3
    sget-object v2, Lcom/google/android/apps/translate/translation/ChipView;->LOWER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    iget v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 572
    const-string v2, "ChipView"

    const-string v3, "selectChip TOUCHTOUCH TRG"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v2, :cond_0

    .line 574
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->CHIPVIEW_TRGTEXT:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 576
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    sget-object v3, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/translation/InputPanel;->copyToInputTextBox(Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    goto :goto_0

    .line 581
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->getSelectedHistoryItemPosition()I

    move-result v0

    .line 582
    .local v0, currentPosition:I
    if-ne v0, p1, :cond_5

    .line 585
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2, v6}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->setSelectedHistoryItemPosition(I)V

    .line 587
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2, v8}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->setSelectedHistoryItem(Lcom/google/android/apps/translate/translation/ChipView;)V

    .line 588
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->notifyDataSetChanged()V

    .line 589
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputTextChip:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 590
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslationChip:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 591
    invoke-virtual {p0, v5}, Lcom/google/android/apps/translate/translation/ChipView;->setSelected(Z)V

    goto/16 :goto_0

    .line 594
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->isHistoryMode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 595
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->HISTORY_VIEW_ITEM_EXPANSIONS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v2, v3, v7}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 603
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->getSelectedHistoryItem()Lcom/google/android/apps/translate/translation/ChipView;

    move-result-object v1

    .line 604
    .local v1, curretnSelectedView:Lcom/google/android/apps/translate/translation/ChipView;
    if-eqz v1, :cond_6

    .line 605
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2, v6}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->setSelectedHistoryItemPosition(I)V

    .line 607
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2, v8}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->setSelectedHistoryItem(Lcom/google/android/apps/translate/translation/ChipView;)V

    .line 608
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->notifyDataSetChanged()V

    .line 610
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mAnimateScroll:Z

    if-eqz v2, :cond_8

    .line 612
    new-instance v2, Lcom/google/android/apps/translate/translation/ChipView$4;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/translate/translation/ChipView$4;-><init>(Lcom/google/android/apps/translate/translation/ChipView;I)V

    const-wide/16 v3, 0x64

    invoke-virtual {p0, v2, v3, v4}, Lcom/google/android/apps/translate/translation/ChipView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 598
    .end local v1           #curretnSelectedView:Lcom/google/android/apps/translate/translation/ChipView;
    :cond_7
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->FAVORITES_VIEW_ITEM_EXPANSIONS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v2, v3, v7}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    goto :goto_1

    .line 643
    .restart local v1       #curretnSelectedView:Lcom/google/android/apps/translate/translation/ChipView;
    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/translate/translation/ChipView$5;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/translate/translation/ChipView$5;-><init>(Lcom/google/android/apps/translate/translation/ChipView;I)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method private setSpeakerIcons()V
    .locals 4

    .prologue
    .line 812
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-nez v2, :cond_0

    .line 813
    const-string v2, "no translation or no result, do not set speaker icon"

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 830
    :goto_0
    return-void

    .line 817
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v2, v2, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->canSpeak(Ljava/lang/String;)Z

    move-result v0

    .line 820
    .local v0, canSpeakSrc:Z
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/ChipView;->getTranslationText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v2, v2, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->canSpeak(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    .line 822
    .local v1, canSpeakTrg:Z
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/translate/translation/ChipView$6;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/translate/translation/ChipView$6;-><init>(Lcom/google/android/apps/translate/translation/ChipView;ZZ)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 820
    .end local v1           #canSpeakTrg:Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private setStar(Z)V
    .locals 2
    .parameter "isFavorite"

    .prologue
    .line 387
    const-string v0, "ChipView"

    const-string v1, "setStar"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn1:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/translate/R$drawable;->ic_star_active:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 390
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn2:Landroid/widget/ImageButton;

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/translate/R$drawable;->ic_star_active:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 392
    return-void

    .line 388
    :cond_0
    sget v0, Lcom/google/android/apps/translate/R$drawable;->ic_star_inactive:I

    goto :goto_0

    .line 390
    :cond_1
    sget v0, Lcom/google/android/apps/translate/R$drawable;->ic_star_inactive:I

    goto :goto_1
.end method

.method private setTransliteration(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 5
    .parameter "textView"
    .parameter "transliteration"

    .prologue
    .line 802
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 803
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 809
    :goto_0
    return-void

    .line 805
    :cond_0
    sget-object v0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p1, p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 806
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    sget-object v1, Lcom/google/android/apps/translate/translation/ChipView;->PHONETIC_SPAN:Lcom/google/android/apps/translate/ExtTypefaceSpan;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method


# virtual methods
.method public getOutputView()Lcom/google/android/apps/translate/translation/OutputPanelView;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    return-object v0
.end method

.method public getSelectedChipPart()Lcom/google/android/apps/translate/translation/ChipView$ChipPart;
    .locals 2

    .prologue
    .line 518
    iget-boolean v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mIsExpanded:Z

    if-eqz v0, :cond_2

    .line 519
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->UPPER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    iget v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->INPUT_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    .line 527
    :goto_0
    return-object v0

    .line 521
    :cond_0
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->LOWER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    iget v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 522
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    goto :goto_0

    .line 523
    :cond_1
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView;->SUGGEST_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    iget v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 524
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->SUGGESTION:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    goto :goto_0

    .line 527
    :cond_2
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->ENTIRE_CHIP:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    goto :goto_0
.end method

.method public getSelectedViewId()I
    .locals 1

    .prologue
    .line 514
    iget v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    return v0
.end method

.method public initParameters(Landroid/app/Activity;Landroid/widget/ListView;Lcom/google/android/apps/translate/history/HistoryListAdapter;Lcom/google/android/apps/translate/TranslateManager;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/translation/InputPanel;Z)V
    .locals 10
    .parameter "activity"
    .parameter "listView"
    .parameter "listAdapter"
    .parameter "translateManager"
    .parameter "from"
    .parameter "to"
    .parameter "inputPanel"
    .parameter "animateScroll"

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    .line 175
    iput-object p2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListView:Landroid/widget/ListView;

    .line 176
    iput-object p3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    .line 177
    iput-object p4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    .line 178
    iput-object p5, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    .line 179
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    .line 180
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/translate/TranslateApplication;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mApp:Lcom/google/android/apps/translate/TranslateApplication;

    .line 181
    sget v6, Lcom/google/android/apps/translate/R$id;->panel_output:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/translate/translation/OutputPanelView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    .line 182
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/google/android/apps/translate/translation/OutputPanelView;->setVisibility(I)V

    .line 183
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_chip:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mChip:Landroid/widget/LinearLayout;

    .line 184
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_input_text_wrapper:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputTextChip:Landroid/widget/LinearLayout;

    .line 185
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_translation_text_wrapper:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslationChip:Landroid/widget/LinearLayout;

    .line 186
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    .line 187
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    invoke-static {v6}, Lcom/google/android/apps/translate/Util;->getLanguageListFromProfile(Landroid/content/Context;)Lcom/google/android/apps/translate/Languages;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 188
    sget v6, Lcom/google/android/apps/translate/R$id;->chip_divider:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mChipDivider:Landroid/view/View;

    .line 189
    sget v6, Lcom/google/android/apps/translate/R$id;->dictionary_divider:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mDictDivider:Landroid/view/View;

    .line 191
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_input_text:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputText:Landroid/widget/TextView;

    .line 192
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_translated_text:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslatedText:Landroid/widget/TextView;

    .line 193
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_my_lang:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguageText:Landroid/widget/TextView;

    .line 194
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_their_lang:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguageText:Landroid/widget/TextView;

    .line 195
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_src_transliteration:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTransliteration:Landroid/widget/TextView;

    .line 196
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_trg_transliteration:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTransliteration:Landroid/widget/TextView;

    .line 197
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_suggest_type:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestTypeView:Landroid/widget/TextView;

    .line 198
    sget v6, Lcom/google/android/apps/translate/R$id;->mvh_suggest_text:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestView:Landroid/widget/TextView;

    .line 199
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestTypeView:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 200
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestView:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v6, :cond_0

    .line 205
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mApp:Lcom/google/android/apps/translate/TranslateApplication;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/TranslateApplication;->getInstantTranslateHandler()Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    .line 206
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v9, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v6, v7, p0, v8, v9}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 209
    :cond_0
    sget-object v6, Lcom/google/android/apps/translate/translation/ChipView;->UPPER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 210
    .local v2, id:I
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 211
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 213
    .end local v2           #id:I
    :cond_1
    sget-object v6, Lcom/google/android/apps/translate/translation/ChipView;->SUGGEST_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 214
    .restart local v2       #id:I
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 215
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 217
    .end local v2           #id:I
    :cond_2
    sget-object v6, Lcom/google/android/apps/translate/translation/ChipView;->LOWER_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 218
    .restart local v2       #id:I
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 219
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 221
    .end local v2           #id:I
    :cond_3
    sget-object v6, Lcom/google/android/apps/translate/translation/ChipView;->PADDING_CHIP_RESOURCE_ID_LIST:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 222
    .restart local v2       #id:I
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 223
    invoke-virtual {p0, v2}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 225
    .end local v2           #id:I
    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mChip:Landroid/widget/LinearLayout;

    invoke-virtual {v6, p0}, Landroid/widget/LinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 226
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mChip:Landroid/widget/LinearLayout;

    invoke-virtual {v6, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mAnimateScroll:Z

    .line 233
    sget v6, Lcom/google/android/apps/translate/R$id;->btn_little_star_button1:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn1:Landroid/widget/ImageButton;

    .line 234
    sget v6, Lcom/google/android/apps/translate/R$id;->btn_little_star_button2:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn2:Landroid/widget/ImageButton;

    .line 236
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn1:Landroid/widget/ImageButton;

    new-instance v7, Lcom/google/android/apps/translate/translation/ChipView$StarOnClickListener;

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn1:Landroid/widget/ImageButton;

    invoke-direct {v7, p0, v8}, Lcom/google/android/apps/translate/translation/ChipView$StarOnClickListener;-><init>(Lcom/google/android/apps/translate/translation/ChipView;Landroid/view/View;)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn2:Landroid/widget/ImageButton;

    new-instance v7, Lcom/google/android/apps/translate/translation/ChipView$StarOnClickListener;

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLittleStarBtn2:Landroid/widget/ImageButton;

    invoke-direct {v7, p0, v8}, Lcom/google/android/apps/translate/translation/ChipView$StarOnClickListener;-><init>(Lcom/google/android/apps/translate/translation/ChipView;Landroid/view/View;)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    const/16 v6, 0x8

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->setVisibility(I)V

    .line 250
    sget v6, Lcom/google/android/apps/translate/R$id;->frame_src_tts_speak:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTtsWrapper:Landroid/view/View;

    .line 251
    sget v6, Lcom/google/android/apps/translate/R$id;->frame_trg_tts_speak:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/translate/translation/ChipView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTtsWrapper:Landroid/view/View;

    .line 252
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTtsWrapper:Landroid/view/View;

    sget v7, Lcom/google/android/apps/translate/R$id;->frame_tts_speak:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTts:Landroid/view/View;

    .line 253
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTtsWrapper:Landroid/view/View;

    sget v7, Lcom/google/android/apps/translate/R$id;->frame_tts_speak:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTts:Landroid/view/View;

    .line 254
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTts:Landroid/view/View;

    sget v7, Lcom/google/android/apps/translate/R$id;->btn_translate_tts_speak:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 255
    .local v4, srcTtsSpeakBtn:Landroid/view/View;
    new-instance v6, Lcom/google/android/apps/translate/translation/ChipView$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/translate/translation/ChipView$1;-><init>(Lcom/google/android/apps/translate/translation/ChipView;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTts:Landroid/view/View;

    sget v7, Lcom/google/android/apps/translate/R$id;->btn_translate_tts_speak:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 271
    .local v5, trgTtsSpeakBtn:Landroid/view/View;
    new-instance v6, Lcom/google/android/apps/translate/translation/ChipView$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/translate/translation/ChipView$2;-><init>(Lcom/google/android/apps/translate/translation/ChipView;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mApp:Lcom/google/android/apps/translate/TranslateApplication;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/TranslateApplication;->getMyTts()Lcom/google/android/apps/translate/tts/MyTts;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTts:Lcom/google/android/apps/translate/tts/MyTts;

    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/ChipView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/translate/R$drawable;->tts_speak:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 290
    .local v3, speakerDrawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTtsSpinningWheelWidth:I

    .line 291
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTtsSpinningWheelHeight:I

    .line 293
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 711
    const-string v0, "ChipView"

    const-string v1, "onClick"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    .line 713
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/ChipView;->getPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/translation/ChipView;->selectChip(I)V

    .line 714
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 682
    const-string v0, "ChipView"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->onDestroy()V

    .line 686
    :cond_0
    return-void
.end method

.method public onFavoriteChanged(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V
    .locals 1
    .parameter "translateEntry"
    .parameter "isFavorite"

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    .line 677
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/translate/translation/InputPanel;->onFavoriteChanged(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V

    .line 679
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .parameter "v"

    .prologue
    .line 703
    const-string v0, "ChipView"

    const-string v1, "onLongClick"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTouchEventViewId:I

    .line 705
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/ChipView;->selectAndOpenContextMenu()V

    .line 706
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPause(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V
    .locals 0
    .parameter "inputText"
    .parameter "targetText"
    .parameter "spellCorrection"
    .parameter "detectedSrcShortLangName"

    .prologue
    .line 799
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->onPause()V

    .line 692
    :cond_0
    return-void
.end method

.method public onSourceTextDone(Landroid/text/SpannableStringBuilder;)V
    .locals 0
    .parameter "inputText"

    .prologue
    .line 788
    return-void
.end method

.method public onSourceTextUpdate(Landroid/text/SpannableStringBuilder;)V
    .locals 0
    .parameter "inputText"

    .prologue
    .line 720
    return-void
.end method

.method public onTargetTextDone(Landroid/text/SpannableStringBuilder;)V
    .locals 0
    .parameter "targetText"

    .prologue
    .line 793
    return-void
.end method

.method public onTargetTextUpdate(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V
    .locals 11
    .parameter "inputText"
    .parameter "targetText"
    .parameter "spellCorrection"
    .parameter "detectedSrcShortLangName"

    .prologue
    const/16 v10, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 728
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslatedText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {p2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 783
    :goto_0
    return-void

    .line 733
    :cond_0
    const/4 v2, 0x0

    .line 734
    .local v2, spellCrrectionShown:Z
    const/4 v0, 0x0

    .line 735
    .local v0, detectedLanguageShown:Z
    const/4 v3, 0x0

    .line 736
    .local v3, suggestEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v7, p3, v8}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getSpellCorrectionSuggestEntry(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 738
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    if-nez v4, :cond_2

    .line 739
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v8, p4, v9}, Lcom/google/android/apps/translate/editor/SuggestAdapter;->getDetectedLanguageSuggestEntry(Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 742
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    if-eqz v4, :cond_1

    .line 743
    const/4 v0, 0x1

    .line 745
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 751
    :goto_1
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v4

    sget-object v7, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->SPELL_CORRECTION_SHOWN_ON_CHIP_VIEW:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v4, v7, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->setImpression(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 753
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v4

    sget-object v7, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->LANGID_SHOWN_ON_CHIP_VIEW:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v4, v7, v0}, Lcom/google/android/apps/translate/UserActivityMgr;->setImpression(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 756
    if-nez v3, :cond_3

    .line 757
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestTypeView:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 758
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestView:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 747
    :cond_2
    const/4 v2, 0x1

    .line 748
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    goto :goto_1

    .line 760
    :cond_3
    sget-object v4, Lcom/google/android/apps/translate/translation/ChipView$7;->$SwitchMap$com$google$android$apps$translate$editor$SuggestAdapter$SuggestType:[I

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getType()Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestType;->ordinal()I

    move-result v7

    aget v4, v4, v7

    packed-switch v4, :pswitch_data_0

    .line 770
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestTypeView:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 774
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getHtmlInputText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "   "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 775
    .local v1, htmlText:Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestView:Landroid/widget/TextView;

    if-nez v1, :cond_4

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;->getEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v4

    :goto_3
    const/4 v8, 0x2

    new-array v8, v8, [Lcom/google/android/apps/translate/Language;

    iget-object v9, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    aput-object v9, v8, v6

    iget-object v9, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    aput-object v9, v8, v5

    sget-object v9, Lcom/google/android/apps/translate/Constants$AppearanceType;->UNCHANGED:Lcom/google/android/apps/translate/Constants$AppearanceType;

    if-eqz v1, :cond_5

    :goto_4
    invoke-static {v7, v4, v8, v9, v5}, Lcom/google/android/apps/translate/Util;->setTextAndFont(Landroid/widget/TextView;Ljava/lang/String;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;Z)V

    .line 781
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestView:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 762
    .end local v1           #htmlText:Ljava/lang/String;
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestTypeView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    sget v8, Lcom/google/android/apps/translate/R$string;->label_translate_from:I

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 765
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestTypeView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    sget v8, Lcom/google/android/apps/translate/R$string;->msg_did_you_mean:I

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .restart local v1       #htmlText:Ljava/lang/String;
    :cond_4
    move-object v4, v1

    .line 775
    goto :goto_3

    :cond_5
    move v5, v6

    goto :goto_4

    .line 760
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTranslationDone(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V
    .locals 1
    .parameter "translateEntry"
    .parameter "isFavorite"

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/translate/translation/InputPanel;->onTranslationDone(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V

    .line 672
    :cond_0
    return-void
.end method

.method public onTranslationFailed(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V
    .locals 1
    .parameter "from"
    .parameter "to"
    .parameter "inputText"

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/InputPanel;->onTranslationFailed(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    .line 699
    :cond_0
    return-void
.end method

.method public render(Lcom/google/android/apps/translate/history/HistoryEntry;)V
    .locals 2
    .parameter "historyEntry"

    .prologue
    const/4 v1, 0x0

    .line 296
    if-eqz p1, :cond_0

    .line 297
    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/translation/ChipView;->setVisibility(I)V

    .line 298
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/android/apps/translate/translation/ChipView;->render(Lcom/google/android/apps/translate/history/HistoryEntry;II)V

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/ChipView;->setVisibility(I)V

    goto :goto_0
.end method

.method public render(Lcom/google/android/apps/translate/history/HistoryEntry;II)V
    .locals 11
    .parameter "itemEntry"
    .parameter "position"
    .parameter "entries"

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/16 v0, 0x8

    const/4 v5, 0x0

    .line 309
    const-string v1, "ChipView"

    const-string v2, "render"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    iget-object v7, p1, Lcom/google/android/apps/translate/history/HistoryEntry;->entry:Lcom/google/android/apps/translate/history/Entry;

    .line 311
    .local v7, entry:Lcom/google/android/apps/translate/history/Entry;
    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/HistoryEntry;->isNullEntry()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v7, :cond_1

    .line 312
    :cond_0
    const-string v1, "ChipView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "render NULL position="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguageText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguageText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTransliteration:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTransliteration:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/ChipView;->setVisibility(I)V

    .line 384
    :goto_0
    return-void

    .line 320
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/ChipView;->getVisibility()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 321
    invoke-virtual {p0, v5}, Lcom/google/android/apps/translate/translation/ChipView;->setVisibility(I)V

    .line 323
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mItemEntry:Lcom/google/android/apps/translate/history/HistoryEntry;

    if-eqz v1, :cond_3

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mItemEntry:Lcom/google/android/apps/translate/history/HistoryEntry;

    iget-object v1, v1, Lcom/google/android/apps/translate/history/HistoryEntry;->entry:Lcom/google/android/apps/translate/history/Entry;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getDbKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/translate/history/HistoryEntry;->entry:Lcom/google/android/apps/translate/history/Entry;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/Entry;->getDbKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mItemEntry:Lcom/google/android/apps/translate/history/HistoryEntry;

    iget-boolean v1, v1, Lcom/google/android/apps/translate/history/HistoryEntry;->isFavorite:Z

    iget-boolean v2, p1, Lcom/google/android/apps/translate/history/HistoryEntry;->isFavorite:Z

    if-ne v1, v2, :cond_3

    .line 326
    const-string v0, "ChipView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "render REUSE position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    new-instance v0, Lcom/google/android/apps/translate/history/HistoryEntry;

    invoke-direct {v0, p1}, Lcom/google/android/apps/translate/history/HistoryEntry;-><init>(Lcom/google/android/apps/translate/history/HistoryEntry;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mItemEntry:Lcom/google/android/apps/translate/history/HistoryEntry;

    .line 371
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    if-nez v0, :cond_7

    .line 372
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ViewState;->SELECTED_EXPANDED:Lcom/google/android/apps/translate/translation/ChipView$ViewState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/translation/ChipView;->renderOutputView(Lcom/google/android/apps/translate/translation/ChipView$ViewState;)V

    goto :goto_0

    .line 329
    :cond_3
    const-string v1, "ChipView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "render NEW position="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    new-instance v1, Lcom/google/android/apps/translate/history/HistoryEntry;

    invoke-direct {v1, p1}, Lcom/google/android/apps/translate/history/HistoryEntry;-><init>(Lcom/google/android/apps/translate/history/HistoryEntry;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mItemEntry:Lcom/google/android/apps/translate/history/HistoryEntry;

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v7}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguageShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    .line 333
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v7}, Lcom/google/android/apps/translate/history/Entry;->getToLanguageShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    .line 334
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    if-eqz v1, :cond_4

    .line 335
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1, v2, p0, v3, v4}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/editor/InstantTranslateHandler$InstantTranslateListner;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 338
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputText:Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v2

    new-array v3, v9, [Lcom/google/android/apps/translate/Language;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    aput-object v4, v3, v5

    sget-object v4, Lcom/google/android/apps/translate/Constants$AppearanceType;->UNCHANGED:Lcom/google/android/apps/translate/Constants$AppearanceType;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/translate/Util;->setTextAndFont(Landroid/widget/TextView;Ljava/lang/String;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;Z)V

    .line 341
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestTypeView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSuggestView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 343
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    if-eqz v1, :cond_5

    .line 344
    iput-object v10, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSpellCorrectionEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 345
    iput-object v10, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcLangDetectEntry:Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;

    .line 346
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInstantTranslateHandler:Lcom/google/android/apps/translate/editor/InstantTranslateHandler;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mInputText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/InstantTranslateHandler;->updateSourceText(Ljava/lang/CharSequence;)V

    .line 349
    :cond_5
    new-instance v8, Lcom/google/android/apps/translate/Translate$TranslateResult;

    invoke-direct {v8, v7}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Lcom/google/android/apps/translate/history/Entry;)V

    .line 351
    .local v8, results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTranslatedText:Landroid/widget/TextView;

    invoke-virtual {v8}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/apps/translate/Language;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    aput-object v4, v3, v9

    sget-object v4, Lcom/google/android/apps/translate/Constants$AppearanceType;->UNCHANGED:Lcom/google/android/apps/translate/Constants$AppearanceType;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/translate/Util;->setTextAndFont(Landroid/widget/TextView;Ljava/lang/String;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;Z)V

    .line 357
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguageText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguageText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mSrcTransliteration:Landroid/widget/TextView;

    invoke-virtual {v8}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getSrcTransliteration()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/translate/translation/ChipView;->setTransliteration(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mTrgTransliteration:Landroid/widget/TextView;

    invoke-virtual {v8}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTrgTransliteration()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/translate/translation/ChipView;->setTransliteration(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 361
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mDictDivider:Landroid/view/View;

    invoke-virtual {v8}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getDictionaryResult()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 364
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/ChipView;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/ChipView;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v7}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/google/android/apps/translate/history/Entry;->getOutputText()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mItemEntry:Lcom/google/android/apps/translate/history/HistoryEntry;

    iget-boolean v0, v0, Lcom/google/android/apps/translate/history/HistoryEntry;->isFavorite:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/translation/ChipView;->setStar(Z)V

    goto/16 :goto_1

    :cond_6
    move v0, v5

    .line 361
    goto :goto_2

    .line 374
    .end local v8           #results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/ChipView;->mListAdapter:Lcom/google/android/apps/translate/history/HistoryListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/HistoryListAdapter;->getSelectedHistoryItemPosition()I

    move-result v6

    .line 375
    .local v6, currentPosition:I
    if-ne v6, p2, :cond_8

    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ViewState;->SELECTED_EXPANDED:Lcom/google/android/apps/translate/translation/ChipView$ViewState;

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/translation/ChipView;->renderOutputView(Lcom/google/android/apps/translate/translation/ChipView$ViewState;)V

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ViewState;->UNSELECTED:Lcom/google/android/apps/translate/translation/ChipView$ViewState;

    goto :goto_3
.end method
