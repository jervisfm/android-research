.class Lcom/google/android/apps/translate/translation/InputPanel$8;
.super Ljava/lang/Object;
.source "InputPanel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/translation/InputPanel;->setTranslationResult(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/translation/InputPanel;

.field final synthetic val$setInputText:Z

.field final synthetic val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/translation/InputPanel;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 871
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    iput-object p2, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iput-boolean p3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$setInputText:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 875
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    #getter for: Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;
    invoke-static {v3}, Lcom/google/android/apps/translate/translation/InputPanel;->access$500(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/editor/EditPanelView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/EditPanelView;->hideTranslationLoading()V

    .line 876
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v3, :cond_4

    .line 877
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/translation/TranslateEntry;->isInvalid()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 878
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasSourceAndTargetLanguages()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 879
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v4, v4, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v5, v5, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v3, v4, v5, v6, v6}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 885
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasInputText()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v3, v3, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    :goto_0
    invoke-virtual {v4, v3}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputText(Ljava/lang/String;)V

    .line 888
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    #calls: Lcom/google/android/apps/translate/translation/InputPanel;->setResultView(Z)V
    invoke-static {v3, v6}, Lcom/google/android/apps/translate/translation/InputPanel;->access$1000(Lcom/google/android/apps/translate/translation/InputPanel;Z)V

    .line 908
    :goto_1
    return-void

    .line 885
    :cond_1
    const-string v3, ""

    goto :goto_0

    .line 890
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    #calls: Lcom/google/android/apps/translate/translation/InputPanel;->setResultView(Z)V
    invoke-static {v3, v5}, Lcom/google/android/apps/translate/translation/InputPanel;->access$1000(Lcom/google/android/apps/translate/translation/InputPanel;Z)V

    .line 891
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 892
    .local v0, entry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    #getter for: Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/google/android/apps/translate/translation/InputPanel;->access$000(Lcom/google/android/apps/translate/translation/InputPanel;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/translate/Util;->isStarredTranslation(Landroid/app/Activity;Lcom/google/android/apps/translate/history/Entry;)Z

    move-result v2

    .line 893
    .local v2, isFavorite:Z
    new-instance v1, Lcom/google/android/apps/translate/history/HistoryEntry;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/translate/history/HistoryEntry;-><init>(Lcom/google/android/apps/translate/history/Entry;Z)V

    .line 894
    .local v1, historyEntry:Lcom/google/android/apps/translate/history/HistoryEntry;
    iget-boolean v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->val$setInputText:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    #getter for: Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;
    invoke-static {v3}, Lcom/google/android/apps/translate/translation/InputPanel;->access$900(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/editor/TextSlot;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/TextSlot;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 900
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v4, v1, Lcom/google/android/apps/translate/history/HistoryEntry;->entry:Lcom/google/android/apps/translate/history/Entry;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputText(Ljava/lang/String;)V

    .line 902
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    #getter for: Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;
    invoke-static {v3}, Lcom/google/android/apps/translate/translation/InputPanel;->access$1400(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/translation/ChipView;

    move-result-object v3

    invoke-virtual {v3, v1, v6, v5}, Lcom/google/android/apps/translate/translation/ChipView;->render(Lcom/google/android/apps/translate/history/HistoryEntry;II)V

    .line 903
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    #getter for: Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;
    invoke-static {v3}, Lcom/google/android/apps/translate/translation/InputPanel;->access$500(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/editor/EditPanelView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/translation/InputPanel;->getInputText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/translate/editor/EditPanelView;->updateButtons(Ljava/lang/String;)V

    goto :goto_1

    .line 906
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    .end local v1           #historyEntry:Lcom/google/android/apps/translate/history/HistoryEntry;
    .end local v2           #isFavorite:Z
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel$8;->this$0:Lcom/google/android/apps/translate/translation/InputPanel;

    #calls: Lcom/google/android/apps/translate/translation/InputPanel;->setResultView(Z)V
    invoke-static {v3, v6}, Lcom/google/android/apps/translate/translation/InputPanel;->access$1000(Lcom/google/android/apps/translate/translation/InputPanel;Z)V

    goto :goto_1
.end method
