.class public Lcom/google/android/apps/translate/translation/InputPanel;
.super Ljava/lang/Object;
.source "InputPanel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;
.implements Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;
.implements Lcom/google/android/apps/translate/editor/PreImeAutoCompleteTextView$OnKeyPreImeListener;
.implements Lcom/google/android/apps/translate/editor/EditPanelView$Callback;


# static fields
.field private static final ALWAYS_SHOW_INTRO:Z = true

.field private static final CLEAR_OR_APPEND_TEXT_AFTER_TRANSLATION:Z = false

.field private static final CLEAR_TEXT_AFTER_TRANSLATION:Z = false

.field private static final ENABLE_CHINESE_AUTO_SWAP:Z = false

.field private static final ENABLE_LOCAL_AUTO_SWAP:Z = false

.field private static final SELECT_TEXT_AFTER_TRANSLATION:Z = true

.field private static final TAG:Ljava/lang/String; = "InputPanel"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

.field private mBtnClearInput:Landroid/widget/ImageButton;

.field private mChipView:Lcom/google/android/apps/translate/translation/ChipView;

.field private mChipWrapper:Landroid/view/View;

.field private mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

.field private mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

.field private mControlPanelWrapper:Landroid/view/View;

.field private mEnableLanguageDetector:Z

.field private mFromLanguage:Lcom/google/android/apps/translate/Language;

.field private mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

.field private mIntroMessageView:Landroid/view/View;

.field private mLanguageList:Lcom/google/android/apps/translate/Languages;

.field private mLanguagePanel:Landroid/view/View;

.field private mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

.field private mListView:Landroid/widget/ListView;

.field private mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

.field private mPanelView:Landroid/view/View;

.field private mPrevTextLength:I

.field private mSelectionEnd:I

.field private mSelectionSaved:Z

.field private mSelectionStart:I

.field private mSelectionText:Ljava/lang/String;

.field private mShowIntro:Z

.field private mTitleView:Lcom/google/android/apps/translate/home/TitleView;

.field private mToLanguage:Lcom/google/android/apps/translate/Language;

.field private mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

.field private mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

.field private mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Landroid/view/View;Lcom/google/android/apps/translate/translation/TranslateHelper;)V
    .locals 14
    .parameter "activity"
    .parameter "languages"
    .parameter "rootView"
    .parameter "callback"

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPrevTextLength:I

    .line 106
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mEnableLanguageDetector:Z

    .line 110
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mShowIntro:Z

    .line 119
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionSaved:Z

    .line 135
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    .line 136
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 137
    sget v1, Lcom/google/android/apps/translate/R$id;->panel_input:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    .line 138
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-static {v1}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    invoke-static {}, Lcom/google/android/apps/translate/Util;->isHoneycombCompatible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/translate/R$id;->fragments_translate_title_bar:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/home/TitleView;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    .line 143
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->translate_title_bar:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/translate/home/TitleView;

    .line 145
    .local v13, titleViewInFragment:Lcom/google/android/apps/translate/home/TitleView;
    const/16 v1, 0x8

    invoke-virtual {v13, v1}, Lcom/google/android/apps/translate/home/TitleView;->setVisibility(I)V

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/google/android/apps/translate/translation/TranslateActivity;

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->translate_title_bar:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/home/TitleView;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    .line 154
    .end local v13           #titleViewInFragment:Lcom/google/android/apps/translate/home/TitleView;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->language_selection_panel:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePanel:Landroid/view/View;

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePanel:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/home/TitleView;->setLanguagePanel(Landroid/view/View;)V

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->input_panel_intro:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mIntroMessageView:Landroid/view/View;

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->history_list_wrapper:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipWrapper:Landroid/view/View;

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/translate/TranslateApplication;

    .line 159
    .local v10, app:Lcom/google/android/apps/translate/TranslateApplication;
    invoke-virtual {v10}, Lcom/google/android/apps/translate/TranslateApplication;->getTranslateManager()Lcom/google/android/apps/translate/TranslateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->input_panel_chip_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/translation/ChipView;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    .line 162
    invoke-virtual {v10}, Lcom/google/android/apps/translate/TranslateApplication;->getVoiceInputHelper()Lcom/google/android/apps/translate/VoiceInputHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->suggest_list:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mListView:Landroid/widget/ListView;

    .line 166
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/translate/R$id;->input_method_view_wrapper:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mControlPanelWrapper:Landroid/view/View;

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mControlPanelWrapper:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->control_panel:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/editor/InputMethodView;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->input_panel_input_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/editor/EditPanelView;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->startInternalEdit()V

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/translate/editor/EditPanelView;->setCallback(Lcom/google/android/apps/translate/editor/EditPanelView$Callback;)V

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mControlPanel:Lcom/google/android/apps/translate/editor/InputMethodView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->setInputMethodView(Lcom/google/android/apps/translate/editor/InputMethodView;)V

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->setListView(Landroid/widget/ListView;)V

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    sget v2, Lcom/google/android/apps/translate/R$id;->msg_confirm_content:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

    .line 176
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mAsrResultEditor:Lcom/google/android/apps/translate/asreditor/AsrResultEditor;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/asreditor/AsrResultEditor;->getEditorField()Lcom/google/android/apps/translate/editor/TextSlot;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/TextSlot;->setTouchEventCallback(Lcom/google/android/apps/translate/editor/SlotView$TouchEventListener;)V

    .line 180
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    sget v2, Lcom/google/android/apps/translate/R$id;->btn_confirm_view_clear:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mBtnClearInput:Landroid/widget/ImageButton;

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->btn_swap:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mBtnClearInput:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->spinner_my_lang:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Spinner;

    .line 187
    .local v11, spinner1:Landroid/widget/Spinner;
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->spinner_their_lang:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Spinner;

    .line 188
    .local v12, spinner2:Landroid/widget/Spinner;
    new-instance v1, Lcom/google/android/apps/translate/LanguagePicker;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2, v11, v12, p0}, Lcom/google/android/apps/translate/LanguagePicker;-><init>(Landroid/app/Activity;Landroid/widget/Spinner;Landroid/widget/Spinner;Lcom/google/android/apps/translate/LanguagePicker$OnLanguagePairSelectedListener;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/LanguagePicker;->setupLanguageSpinners()V

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/LanguagePicker;->getFromLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/LanguagePicker;->getToLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    iget-object v6, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    const/4 v9, 0x0

    move-object v8, p0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/translate/translation/ChipView;->initParameters(Landroid/app/Activity;Landroid/widget/ListView;Lcom/google/android/apps/translate/history/HistoryListAdapter;Lcom/google/android/apps/translate/TranslateManager;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/translation/InputPanel;Z)V

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/ChipView;->getOutputView()Lcom/google/android/apps/translate/translation/OutputPanelView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v1, p0, v2, v3, v4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->init(Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;Lcom/google/android/apps/translate/translation/ChipView;Lcom/google/android/apps/translate/TranslateManager;Lcom/google/android/apps/translate/Languages;)V

    .line 196
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v6, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/translate/VoiceInputHelper;->getAsrLocale(Landroid/content/Context;Lcom/google/android/apps/translate/Language;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/translate/editor/EditPanelView;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Z)V

    .line 198
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->requestFocus()Z

    .line 200
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mShowIntro:Z

    .line 205
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/translate/translation/InputPanel;->setResultView(Z)V

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/home/TitleView;->checkTitleShow()V

    .line 207
    return-void

    .line 151
    .end local v10           #app:Lcom/google/android/apps/translate/TranslateApplication;
    .end local v11           #spinner1:Landroid/widget/Spinner;
    .end local v12           #spinner2:Landroid/widget/Spinner;
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/translate/R$id;->translate_title_bar:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/home/TitleView;

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/translation/InputPanel;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/Languages;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/translate/translation/InputPanel;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->setResultView(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/translate/translation/InputPanel;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/home/TitleView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/translate/translation/InputPanel;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePanel:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/translation/ChipView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/Language;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/Language;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/VoiceInputHelper;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/editor/EditPanelView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/translate/translation/InputPanel;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputBoxIcons(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/translate/translation/InputPanel;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputTextHint()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/translate/translation/InputPanel;Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->endEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/editor/TextSlot;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    return-object v0
.end method

.method private doTranslate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter "text"
    .parameter "translatedText"

    .prologue
    .line 440
    const-string v0, "InputPanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doTranslate input_text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Util;->hideSoftKeyboard(Landroid/app/Activity;Landroid/os/IBinder;)V

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->selectAll()V

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->doTranslate(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/VoiceInputHelper;->getAsrLocale(Landroid/content/Context;Lcom/google/android/apps/translate/Language;)Ljava/lang/String;

    move-result-object v5

    .line 451
    .local v5, locale:Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/translate/editor/EditPanelView;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Z)V

    .line 453
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->INIT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/translation/InputPanel;->endEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V

    .line 454
    return-void
.end method

.method private endEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V
    .locals 4
    .parameter "event"

    .prologue
    .line 809
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mShowIntro:Z

    .line 811
    const-string v0, "InputPanel"

    const-string v1, "endEditMode"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$6;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel$6;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/InputPanel$7;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 833
    return-void
.end method

.method private internalSetInputText(Ljava/lang/String;)V
    .locals 3
    .parameter "text"

    .prologue
    .line 489
    const-string v0, "InputPanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "internalSetInputText text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel$2;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 507
    return-void
.end method

.method private isCompositionText(Landroid/text/Spanned;II)Z
    .locals 6
    .parameter "s"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 619
    const-class v5, Ljava/lang/Object;

    invoke-interface {p1, p2, p3, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    .line 620
    .local v4, spans:[Ljava/lang/Object;
    move-object v0, v4

    .local v0, arr$:[Ljava/lang/Object;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 621
    .local v3, span:Ljava/lang/Object;
    invoke-interface {p1, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    if-ne v5, p2, :cond_0

    invoke-interface {p1, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    if-ne v5, p3, :cond_0

    invoke-interface {p1, v3}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v5

    and-int/lit16 v5, v5, 0x100

    if-eqz v5, :cond_0

    .line 623
    const/4 v5, 0x1

    .line 626
    .end local v3           #span:Ljava/lang/Object;
    :goto_1
    return v5

    .line 620
    .restart local v3       #span:Ljava/lang/Object;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 626
    .end local v3           #span:Ljava/lang/Object;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private notifyLanguageChanges(Z)V
    .locals 10
    .parameter "triggerTranslate"

    .prologue
    const/4 v6, 0x0

    .line 380
    const-string v0, "InputPanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "languagePairChanged triggerTranslate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    aput-object v2, v1, v6

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    aput-object v3, v1, v2

    sget-object v2, Lcom/google/android/apps/translate/Constants$AppearanceType;->INPUT_PANEL:Lcom/google/android/apps/translate/Constants$AppearanceType;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/translate/Util;->detectAndSetFonts(Landroid/widget/TextView;[Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Constants$AppearanceType;)V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/translate/TranslateManager;->setLanguagePair(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v9, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v5, v8, v9}, Lcom/google/android/apps/translate/VoiceInputHelper;->getAsrLocale(Landroid/content/Context;Lcom/google/android/apps/translate/Language;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/translate/editor/EditPanelView;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Z)V

    .line 394
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputTextHint()V

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->resetSuggestions()V

    .line 424
    :goto_0
    return-void

    .line 397
    :cond_0
    if-eqz p1, :cond_2

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->getCurrentTranslation()Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v7

    .line 399
    .local v7, currentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;
    if-eqz v7, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v1, v7, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v1, v7, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 402
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mOutputView:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->getInputText()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->doTranslate(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    .end local v7           #currentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/InputPanel$1;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private declared-synchronized restoreSelection()V
    .locals 3

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionSaved:Z

    if-eqz v0, :cond_1

    .line 264
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionSaved:Z

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionText:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionText:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    iget v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionStart:I

    iget v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionEnd:I

    if-ge v0, v1, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->setSelected(Z)V

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    iget v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionStart:I

    iget v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionEnd:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/editor/TextSlot;->setSelection(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :cond_1
    monitor-exit p0

    return-void

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setInputBoxIcons(Z)V
    .locals 4
    .parameter "redrawList"

    .prologue
    const/4 v2, 0x0

    .line 527
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    .line 530
    .local v1, v:Landroid/view/View;
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    .line 532
    .local v0, hasInputText:Z
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v3

    if-nez v3, :cond_1

    .line 533
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mBtnClearInput:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 534
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mBtnClearInput:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 542
    :goto_1
    return-void

    .end local v0           #hasInputText:Z
    :cond_0
    move v0, v2

    .line 530
    goto :goto_0

    .line 537
    .restart local v0       #hasInputText:Z
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mBtnClearInput:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method

.method private setInputTextHint()V
    .locals 3

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    if-nez v0, :cond_0

    .line 437
    :goto_0
    return-void

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 433
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/translate/R$string;->hint_input_text_auto:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/translate/editor/TextSlot;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private setPreviousResult()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 776
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 778
    .local v0, translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateEntry;->isInvalid()Z

    move-result v1

    if-nez v1, :cond_1

    .line 779
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/translate/translation/InputPanel;->setResultView(Z)V

    .line 781
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v1, v2, v2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setInputPanel(ZZ)V

    .line 786
    :goto_1
    return-void

    .line 776
    .end local v0           #translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getCurrentTranslation()Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    goto :goto_0

    .line 784
    .restart local v0       #translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/translate/translation/InputPanel;->setResultView(Z)V

    goto :goto_1
.end method

.method private setResultView(Z)V
    .locals 3
    .parameter "hasResult"

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 913
    if-eqz p1, :cond_0

    .line 914
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mIntroMessageView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 915
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 924
    :goto_0
    return-void

    .line 917
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipWrapper:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 918
    iget-boolean v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mShowIntro:Z

    if-eqz v0, :cond_1

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mIntroMessageView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 921
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mIntroMessageView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private startEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V
    .locals 2
    .parameter "inputMethod"

    .prologue
    .line 789
    const-string v0, "InputPanel"

    const-string v1, "startEditMode"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mShowIntro:Z

    .line 791
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPrevTextLength:I

    .line 792
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel$5;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 804
    return-void
.end method

.method private swapLanguagePair(Z)V
    .locals 4
    .parameter "manualSwap"

    .prologue
    const/4 v3, 0x0

    .line 355
    const-string v0, "InputPanel"

    const-string v1, "swapLanguagePair"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/translate/R$string;->msg_error_swap_language:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    .line 368
    :goto_0
    return-void

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getFromLanguageGivenToLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getToLanguageGivenFromLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .parameter "s"

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/editor/EditPanelView;->afterTextChanged(Landroid/text/Editable;)V

    .line 603
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/translate/editor/EditPanelView;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 610
    :cond_0
    return-void
.end method

.method public clearInputText()V
    .locals 2

    .prologue
    .line 548
    const-string v0, "InputPanel"

    const-string v1, "clearInputText"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/InputPanel$3;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 556
    return-void
.end method

.method declared-synchronized copyToInputTextBox(Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V
    .locals 6
    .parameter "chipPart"

    .prologue
    .line 1023
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-nez v2, :cond_1

    const/4 v1, 0x0

    .line 1025
    .local v1, translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->isInvalid()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 1047
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 1023
    .end local v1           #translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getCurrentTranslation()Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    goto :goto_0

    .line 1028
    .restart local v1       #translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_2
    sget-object v2, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    if-ne p1, v2, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/translate/history/Entry;->getTranslation()Ljava/lang/String;

    move-result-object v0

    .line 1030
    .local v0, text:Ljava/lang/String;
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1031
    sget-object v2, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    if-ne p1, v2, :cond_4

    .line 1032
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    iget-object v3, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getFromLanguageGivenToLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    iget-object v4, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getToLanguageGivenFromLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 1045
    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1023
    .end local v0           #text:Ljava/lang/String;
    .end local v1           #translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1028
    .restart local v1       #translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_3
    :try_start_2
    iget-object v0, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    goto :goto_2

    .line 1040
    .restart local v0       #text:Ljava/lang/String;
    :cond_4
    iget-object v2, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v3, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method declared-synchronized copyToInputTextBox(Ljava/lang/String;)V
    .locals 5
    .parameter "text"

    .prologue
    .line 1050
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getCurrentTranslation()Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    .line 1051
    .local v0, entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    if-eqz v0, :cond_0

    .line 1052
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1053
    iget-object v1, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 1056
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1059
    :cond_0
    monitor-exit p0

    return-void

    .line 1050
    .end local v0           #entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getContextMenuTargetView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    return-object v0
.end method

.method public getFromLanguage()Lcom/google/android/apps/translate/Language;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method public getInputText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedChipView()Lcom/google/android/apps/translate/translation/ChipView$ChipPart;
    .locals 1

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    if-eqz v0, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/ChipView;->getSelectedChipPart()Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    move-result-object v0

    .line 1003
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->NONE:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    goto :goto_0
.end method

.method public getToLanguage()Lcom/google/android/apps/translate/Language;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    return-object v0
.end method

.method public hasSomethingToClear()Z
    .locals 2

    .prologue
    .line 994
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 996
    .local v0, translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasInputText()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 994
    .end local v0           #translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getCurrentTranslation()Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    goto :goto_0

    .line 996
    .restart local v0       #translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mPanelView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 842
    return-void
.end method

.method public isInEditingMode()Z
    .locals 1

    .prologue
    .line 837
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method declared-synchronized languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V
    .locals 5
    .parameter "from"
    .parameter "to"
    .parameter "triggerTranslate"
    .parameter "manual"

    .prologue
    const/4 v0, 0x0

    .line 569
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p2, v2}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 573
    .local v0, isChanged:Z
    :cond_2
    if-eqz v0, :cond_4

    .line 574
    const-string v2, "InputPanel"

    const-string v3, "languagePairSelected CHANGED!"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    const/4 v3, 0x0

    invoke-virtual {v2, p2, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getFromLanguageGivenToLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    .line 578
    .local v1, toLangAsFrom:Lcom/google/android/apps/translate/Language;
    invoke-virtual {p1, v1}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p1}, Lcom/google/android/apps/translate/Languages;->isChinese(Lcom/google/android/apps/translate/Language;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 580
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/translate/Language;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 581
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getToLanguageGivenFromLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object p2

    .line 586
    :cond_3
    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    .line 587
    iput-object p2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    .line 588
    invoke-direct {p0, p3}, Lcom/google/android/apps/translate/translation/InputPanel;->notifyLanguageChanges(Z)V

    .line 589
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/translate/LanguagePicker;->setLanguagePairToSpinners(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 591
    if-nez p4, :cond_4

    .line 593
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    invoke-static {p1, p2}, Lcom/google/android/apps/translate/Util;->generateLongPairText(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 596
    .end local v1           #toLangAsFrom:Lcom/google/android/apps/translate/Language;
    :cond_4
    monitor-exit p0

    return-void

    .line 583
    .restart local v1       #toLangAsFrom:Lcom/google/android/apps/translate/Language;
    :cond_5
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getFromLanguageGivenToLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    goto :goto_0

    .line 569
    .end local v0           #isChanged:Z
    .end local v1           #toLangAsFrom:Lcom/google/android/apps/translate/Language;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public onAccept(IZ)V
    .locals 4
    .parameter "resourceId"
    .parameter "doTranslate"

    .prologue
    .line 764
    const-string v2, "InputPanel"

    const-string v3, "onAccept"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 766
    .local v0, inputText:Ljava/lang/String;
    if-eqz p2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 767
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->getTranslationText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 768
    .local v1, translatedText:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    .end local v1           #translatedText:Ljava/lang/String;
    :goto_0
    return-void

    .line 770
    :cond_0
    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->ACCEPT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-direct {p0, v2}, Lcom/google/android/apps/translate/translation/InputPanel;->endEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V

    .line 771
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->setPreviousResult()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v3, -0x1

    .line 706
    const-string v1, "InputPanel"

    const-string v2, "onActivityResult"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v1, :cond_0

    .line 708
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/apps/translate/editor/EditPanelView;->onActivityResult(IILandroid/content/Intent;)V

    .line 710
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 746
    :cond_1
    :goto_0
    return-void

    .line 712
    :sswitch_0
    if-ne p2, v3, :cond_1

    if-eqz p3, :cond_1

    .line 716
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 717
    .local v0, sms:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 718
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->SMS:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->UNKNOWN:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationInputMethod(Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 722
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;)V

    goto :goto_0

    .line 726
    .end local v0           #sms:Ljava/lang/String;
    :sswitch_1
    if-ne p2, v3, :cond_1

    if-eqz p3, :cond_1

    .line 730
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->restoreSelection()V

    .line 732
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v1

    if-nez v1, :cond_2

    .line 734
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;->NONE:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->enableEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 737
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    sget-object v2, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->SWITCH:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->hideCurrentInputMethod(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)Z

    .line 738
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-static {p3}, Lcom/google/android/apps/translate/VoiceInputHelper;->getRecognitionResult(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->onNonStreamingVoiceResult(Ljava/lang/String;)V

    goto :goto_0

    .line 710
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0xaa -> :sswitch_0
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 313
    const-string v1, "InputPanel"

    const-string v2, "onClick"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 315
    .local v0, id:I
    sget v1, Lcom/google/android/apps/translate/R$id;->btn_swap:I

    if-ne v0, v1, :cond_1

    .line 316
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;->UNKNOWN:Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationInputMethod(Lcom/google/android/apps/translate/UserActivityMgr$InputMethod;)Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->SWAP:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 319
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/translate/translation/InputPanel;->swapLanguagePair(Z)V

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    sget v1, Lcom/google/android/apps/translate/R$id;->btn_confirm_view_clear:I

    if-ne v0, v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 322
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/translate/editor/EditPanelView;->onClick(Landroid/view/View;)V

    .line 323
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputTextHint()V

    goto :goto_0

    .line 325
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 330
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/editor/EditPanelView;->updateButtons(Ljava/lang/String;)V

    goto :goto_0

    .line 328
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->clearInputText()V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)Z
    .locals 4
    .parameter "newConfig"

    .prologue
    const/4 v0, 0x1

    .line 849
    const-string v1, "InputPanel"

    const-string v2, "onConfigurationChanged"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_2

    .line 852
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v1, :cond_1

    .line 853
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/translate/editor/EditPanelView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 854
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    if-eqz v1, :cond_1

    .line 855
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    const/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translate/home/TitleView;->fixEditingViewVisibility(Lcom/google/android/apps/translate/editor/EditPanelView;I)V

    .line 860
    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 970
    const-string v0, "InputPanel"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    if-eqz v0, :cond_0

    .line 972
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/ChipView;->onDestroy()V

    .line 974
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    if-eqz v0, :cond_1

    .line 975
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/editor/TextSlot;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 977
    :cond_1
    return-void
.end method

.method public onEditModeReady(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V
    .locals 3
    .parameter "inputMethod"

    .prologue
    .line 987
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_0

    .line 988
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/home/TitleView;->fixEditingViewVisibility(Lcom/google/android/apps/translate/editor/EditPanelView;I)V

    .line 990
    :cond_0
    return-void
.end method

.method public onEditModeStart(Lcom/google/android/apps/translate/editor/EditPanelView;Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V
    .locals 2
    .parameter "editPanelView"
    .parameter "inputMethod"

    .prologue
    .line 981
    const-string v0, "InputPanel"

    const-string v1, "onEditModeStart"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    invoke-direct {p0, p2}, Lcom/google/android/apps/translate/translation/InputPanel;->startEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 983
    return-void
.end method

.method public onFavoriteChanged(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V
    .locals 0
    .parameter "translateEntry"
    .parameter "isFavorite"

    .prologue
    .line 967
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/translate/editor/EditPanelView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 702
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLanguagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V
    .locals 2
    .parameter "language1"
    .parameter "language2"
    .parameter "triggerTranslate"
    .parameter "manual"

    .prologue
    .line 344
    const-string v0, "InputPanel"

    const-string v1, "onLanguagePairSelected"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/translate/UserActivityMgr;->setLanguageChanges(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 348
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 349
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->onPause()V

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mChipView:Lcom/google/android/apps/translate/translation/ChipView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/ChipView;->onPause()V

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getSelectionStart()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->getSelectionEnd()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 284
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionSaved:Z

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionStart:I

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getSelectionEnd()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionEnd:I

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/TextSlot;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mSelectionText:Ljava/lang/String;

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/editor/TextSlot;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->setSelection(I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/editor/TextSlot;->setIsTextEditor(Z)V

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/home/TitleView;->showTitleBar()V

    .line 298
    return-void
.end method

.method public onResume(Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 10
    .parameter "translateEntry"

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 218
    const-string v0, "InputPanel"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mControlPanelWrapper:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mControlPanelWrapper:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/LanguagePicker;->setupLanguageSpinners()V

    .line 225
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    if-eqz v0, :cond_3

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/translate/translation/TranslateEntry;->getFromLanguageForLanguagePicker(Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/Language;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p0, v0, v1, v6, v6}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 235
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/home/TitleView;->showTitleBar()V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->showSearchButtons()V

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 238
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/translation/InputPanel;->startEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethod;)V

    .line 239
    const-string v0, "InputPanel"

    const-string v1, "onResume ==> startEditMode"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/translate/editor/TextSlot;->setIsTextEditor(Z)V

    .line 257
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->restoreSelection()V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->endInternalEdit()V

    .line 259
    return-void

    .line 230
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    if-nez v0, :cond_1

    .line 231
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/LanguagePicker;->getFromLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/LanguagePicker;->getToLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v6, v6}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    goto :goto_0

    .line 241
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mVoiceInputHelper:Lcom/google/android/apps/translate/VoiceInputHelper;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mFromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v5, v7, v8}, Lcom/google/android/apps/translate/VoiceInputHelper;->getAsrLocale(Landroid/content/Context;Lcom/google/android/apps/translate/Language;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/translate/editor/EditPanelView;->init(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Z)V

    .line 243
    sget-object v0, Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;->INIT:Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/translation/InputPanel;->endEditMode(Lcom/google/android/apps/translate/editor/InputMethodView$InputMethodEvent;)V

    .line 246
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 247
    const-string v0, "InputPanel"

    const-string v1, "onResume ==> setTranslationResult"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-object v0, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 249
    iget-object v0, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 251
    :cond_6
    invoke-virtual {p0, p1, v9}, Lcom/google/android/apps/translate/translation/InputPanel;->setTranslationResult(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V

    goto :goto_1
.end method

.method public onSourceLanguageChangeRequested(Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V
    .locals 6
    .parameter "newSrcLang"
    .parameter "inputText"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1063
    const-string v3, "InputPanel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSourceLanguageChangeRequested newSrcLang="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " inputText="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/editor/EditPanelView;->isEditMode()Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1066
    .local v0, doTranslate:Z
    :goto_0
    if-eqz v0, :cond_2

    .line 1069
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/translate/translation/TranslateEntry;->normalizeInputText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1071
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p0, p1, v1, v2, v2}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 1072
    invoke-virtual {p0, p2}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;)V

    .line 1079
    :goto_1
    return-void

    .end local v0           #doTranslate:Z
    :cond_0
    move v0, v2

    .line 1065
    goto :goto_0

    .line 1074
    .restart local v0       #doTranslate:Z
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p0, p1, v3, v1, v2}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    goto :goto_1

    .line 1077
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mToLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p0, p1, v1, v2, v2}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    goto :goto_1
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 210
    const-string v0, "InputPanel"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mInputEditText:Lcom/google/android/apps/translate/editor/TextSlot;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/translate/editor/TextSlot;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTitleView:Lcom/google/android/apps/translate/home/TitleView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/home/TitleView;->checkTitleShow()V

    .line 215
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    .prologue
    .line 631
    invoke-static {}, Lcom/google/android/apps/translate/Logger;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632
    const-string v0, "InputPanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTextChanged s="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " start="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " before="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputBoxIcons(Z)V

    .line 636
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputTextHint()V

    .line 675
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_1

    .line 676
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/translate/editor/EditPanelView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 680
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mEnableLanguageDetector:Z

    .line 681
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .parameter "v"
    .parameter "event"

    .prologue
    .line 685
    const-string v1, "InputPanel"

    const-string v2, "onTouch"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 687
    .local v0, id:I
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mIntroMessageView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 694
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public onTranslationDone()V
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->onTranslationDone()V

    .line 846
    return-void
.end method

.method public onTranslationDone(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V
    .locals 3
    .parameter "translateEntry"
    .parameter "isFavorite"

    .prologue
    .line 928
    const-string v0, "InputPanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTranslationDone mInputPanel.getInputText()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 931
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->showSearchButtons()V

    .line 932
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/InputPanel;->getInputText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 933
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$9;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/InputPanel$9;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 963
    :goto_0
    return-void

    .line 952
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$10;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/InputPanel$10;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onTranslationFailed(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V
    .locals 7
    .parameter "from"
    .parameter "to"
    .parameter "inputText"

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/editor/EditPanelView;->hideTranslationLoading()V

    .line 1013
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->build(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 1020
    return-void
.end method

.method public setInputText(Ljava/lang/String;)V
    .locals 3
    .parameter "text"

    .prologue
    .line 481
    const-string v0, "InputPanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInputText text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mEnableLanguageDetector:Z

    .line 485
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->internalSetInputText(Ljava/lang/String;)V

    .line 486
    return-void
.end method

.method public setLanguageList(Lcom/google/android/apps/translate/Languages;)V
    .locals 2
    .parameter "list"

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguagePicker:Lcom/google/android/apps/translate/LanguagePicker;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/LanguagePicker;->updateLanguageList(Lcom/google/android/apps/translate/Languages;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mConfirmView:Lcom/google/android/apps/translate/editor/EditPanelView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/editor/EditPanelView;->setLanguageList(Lcom/google/android/apps/translate/Languages;)V

    .line 309
    :cond_0
    return-void
.end method

.method public setTextAndDoTranslate(Ljava/lang/String;)V
    .locals 3
    .parameter "text"

    .prologue
    .line 522
    const-string v0, "InputPanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTextAndDoTranslate text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    return-void
.end method

.method public setTextAndDoTranslate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "text"
    .parameter "translatedText"

    .prologue
    .line 513
    const-string v0, "InputPanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTextAndDoTranslate text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " translatedText="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputText(Ljava/lang/String;)V

    .line 515
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/translate/translation/InputPanel;->doTranslate(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    return-void
.end method

.method setTranslationResult(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V
    .locals 2
    .parameter "translateEntry"
    .parameter "setInputText"

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/translate/translation/InputPanel$8;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 910
    return-void
.end method

.method public showSearchButtons()V
    .locals 2

    .prologue
    .line 753
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/InputPanel;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/InputPanel$4;-><init>(Lcom/google/android/apps/translate/translation/InputPanel;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 760
    return-void
.end method
