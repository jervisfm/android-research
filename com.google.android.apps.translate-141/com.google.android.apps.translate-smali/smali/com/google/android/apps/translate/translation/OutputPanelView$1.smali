.class Lcom/google/android/apps/translate/translation/OutputPanelView$1;
.super Ljava/lang/Object;
.source "OutputPanelView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/translation/OutputPanelView;->initializeInBackground()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/translation/OutputPanelView;)V
    .locals 0
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 128
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #calls: Lcom/google/android/apps/translate/translation/OutputPanelView;->openFavoriteDb()Lcom/google/android/apps/translate/history/BaseDb;
    invoke-static {v5}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$100(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/history/BaseDb;

    move-result-object v5

    #setter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;
    invoke-static {v4, v5}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$002(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/history/BaseDb;)Lcom/google/android/apps/translate/history/BaseDb;

    .line 131
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #calls: Lcom/google/android/apps/translate/translation/OutputPanelView;->openHistoryDb()Lcom/google/android/apps/translate/history/BaseDb;
    invoke-static {v5}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$300(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/history/BaseDb;

    move-result-object v5

    #setter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;
    invoke-static {v4, v5}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$202(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/history/BaseDb;)Lcom/google/android/apps/translate/history/BaseDb;

    .line 134
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mInitLock:Landroid/os/ConditionVariable;
    invoke-static {v4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$400(Lcom/google/android/apps/translate/translation/OutputPanelView;)Landroid/os/ConditionVariable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/ConditionVariable;->open()V

    .line 136
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;
    invoke-static {v4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$500(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 138
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;
    invoke-static {v4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$500(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    .line 139
    .local v0, entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    iget-object v1, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    .line 140
    .local v1, from:Lcom/google/android/apps/translate/Language;
    iget-object v3, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    .line 141
    .local v3, to:Lcom/google/android/apps/translate/Language;
    iget-object v4, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 142
    iget-object v2, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    .line 143
    .local v2, inputText:Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v3, v2, v5}, Lcom/google/android/apps/translate/translation/OutputPanelView;->doTranslate(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    .end local v2           #inputText:Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->showTranslateResultView()V

    .line 151
    .end local v0           #entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    .end local v1           #from:Lcom/google/android/apps/translate/Language;
    .end local v3           #to:Lcom/google/android/apps/translate/Language;
    :cond_0
    return-void

    .line 145
    .restart local v0       #entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    .restart local v1       #from:Lcom/google/android/apps/translate/Language;
    .restart local v3       #to:Lcom/google/android/apps/translate/Language;
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;
    invoke-static {v5}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$200(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/history/BaseDb;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;
    invoke-static {v6}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$500(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v6

    const/4 v7, 0x0

    #calls: Lcom/google/android/apps/translate/translation/OutputPanelView;->addToDb(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)Z
    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$600(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)Z

    .line 146
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    invoke-virtual {v4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->showTranslatingMessage()V

    .line 147
    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$1;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #calls: Lcom/google/android/apps/translate/translation/OutputPanelView;->setTranslateResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    invoke-static {v4, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$700(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    goto :goto_0
.end method
