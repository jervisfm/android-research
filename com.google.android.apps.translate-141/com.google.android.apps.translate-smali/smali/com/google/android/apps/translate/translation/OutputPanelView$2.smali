.class Lcom/google/android/apps/translate/translation/OutputPanelView$2;
.super Ljava/lang/Object;
.source "OutputPanelView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/translation/OutputPanelView;->translateInBackground(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

.field final synthetic val$from:Lcom/google/android/apps/translate/Language;

.field final synthetic val$text:Ljava/lang/String;

.field final synthetic val$to:Lcom/google/android/apps/translate/Language;

.field final synthetic val$translatedText:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iput-object p2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$from:Lcom/google/android/apps/translate/Language;

    iput-object p3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$translatedText:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$to:Lcom/google/android/apps/translate/Language;

    iput-object p5, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$text:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v12, -0x4

    .line 173
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mInitLock:Landroid/os/ConditionVariable;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$400(Lcom/google/android/apps/translate/translation/OutputPanelView;)Landroid/os/ConditionVariable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->block()V

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$from:Lcom/google/android/apps/translate/Language;

    .line 176
    .local v1, sourceLanguage:Lcom/google/android/apps/translate/Language;
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$from:Lcom/google/android/apps/translate/Language;

    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v5

    .line 178
    .local v5, isAuto:Z
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$translatedText:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 179
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$to:Lcom/google/android/apps/translate/Language;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$text:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$translatedText:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 181
    .local v0, resultEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #calls: Lcom/google/android/apps/translate/translation/OutputPanelView;->setTranslateResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    invoke-static {v2, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$700(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 191
    .end local v0           #resultEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$800(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/TranslateManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$from:Lcom/google/android/apps/translate/Language;

    iget-object v11, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$to:Lcom/google/android/apps/translate/Language;

    invoke-interface {v2, v3, v11}, Lcom/google/android/apps/translate/TranslateManager;->setLanguagePair(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    .line 192
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$800(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/TranslateManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$text:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/translate/TranslateManager;->doTranslate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 198
    .local v8, result:Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/apps/translate/Translate;->getResultCode(Ljava/lang/String;)I

    move-result v7

    .line 201
    .local v7, error:I
    if-eqz v7, :cond_1

    if-ne v7, v12, :cond_6

    .line 203
    :cond_1
    move-object v4, v8

    .line 206
    .local v4, translatedResult:Ljava/lang/String;
    const-string v2, "\t"

    invoke-virtual {v8, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 207
    .local v6, delimiter:I
    if-ltz v6, :cond_3

    .line 208
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$900(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/Languages;

    move-result-object v2

    invoke-virtual {v8, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    .line 210
    if-nez v1, :cond_2

    .line 211
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$from:Lcom/google/android/apps/translate/Language;

    .line 213
    :cond_2
    add-int/lit8 v2, v6, 0x1

    invoke-virtual {v8, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 217
    :cond_3
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$to:Lcom/google/android/apps/translate/Language;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$text:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 219
    .restart local v0       #resultEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->postBackCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 220
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->setResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 222
    const-string v2, "OutputPanelView"

    const-string v3, "Saves to history..."

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$200(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/history/BaseDb;

    move-result-object v11

    if-ne v7, v12, :cond_5

    move v2, v9

    :goto_0
    #calls: Lcom/google/android/apps/translate/translation/OutputPanelView;->addToDb(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)Z
    invoke-static {v3, v11, v0, v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$600(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)Z

    .line 240
    .end local v4           #translatedResult:Ljava/lang/String;
    .end local v6           #delimiter:I
    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$1100(Lcom/google/android/apps/translate/translation/OutputPanelView;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/translate/translation/OutputPanelView$2$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/translate/translation/OutputPanelView$2$1;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView$2;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 246
    return-void

    .restart local v4       #translatedResult:Ljava/lang/String;
    .restart local v6       #delimiter:I
    :cond_5
    move v2, v10

    .line 223
    goto :goto_0

    .line 225
    .end local v0           #resultEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    .end local v4           #translatedResult:Ljava/lang/String;
    .end local v6           #delimiter:I
    :cond_6
    const-string v2, "OutputPanelView"

    const-string v3, "Gets result from history if possible..."

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/translation/OutputPanelView;->postBackCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 227
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$from:Lcom/google/android/apps/translate/Language;

    iget-object v10, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$to:Lcom/google/android/apps/translate/Language;

    iget-object v11, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$text:Ljava/lang/String;

    #calls: Lcom/google/android/apps/translate/translation/OutputPanelView;->tryLoadingFromHistory(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    invoke-static {v2, v3, v10, v11}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$1000(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    .line 228
    .restart local v0       #resultEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    if-nez v0, :cond_7

    .line 229
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$1100(Lcom/google/android/apps/translate/translation/OutputPanelView;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v7}, Lcom/google/android/apps/translate/Util;->showTranslationErrorToastMessage(Landroid/app/Activity;I)V

    .line 230
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$1200(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$1200(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$from:Lcom/google/android/apps/translate/Language;

    iget-object v9, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$to:Lcom/google/android/apps/translate/Language;

    iget-object v10, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->val$text:Ljava/lang/String;

    invoke-interface {v2, v3, v9, v10}, Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;->onTranslationFailed(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    goto :goto_1

    .line 234
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$1200(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 235
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    #getter for: Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;
    invoke-static {v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->access$1200(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    move-result-object v2

    invoke-interface {v2, v0, v9}, Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;->onTranslationDone(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V

    goto :goto_1
.end method
