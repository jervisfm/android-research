.class Lcom/google/android/apps/translate/translation/OutputPanelView$6;
.super Ljava/lang/Object;
.source "OutputPanelView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/translate/translation/OutputPanelView;->showTranslationAlpha(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

.field final synthetic val$from:Lcom/google/android/apps/translate/Language;

.field final synthetic val$to:Lcom/google/android/apps/translate/Language;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 456
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$6;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    iput-object p2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$6;->val$from:Lcom/google/android/apps/translate/Language;

    iput-object p3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$6;->val$to:Lcom/google/android/apps/translate/Language;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 460
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$6;->this$0:Lcom/google/android/apps/translate/translation/OutputPanelView;

    sget v2, Lcom/google/android/apps/translate/R$id;->text_translation_alpha:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 461
    .local v0, tv:Landroid/widget/TextView;
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$6;->val$from:Lcom/google/android/apps/translate/Language;

    invoke-static {v1}, Lcom/google/android/apps/translate/Languages;->isAlphaLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView$6;->val$to:Lcom/google/android/apps/translate/Language;

    invoke-static {v1}, Lcom/google/android/apps/translate/Languages;->isAlphaLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 462
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 466
    :goto_0
    return-void

    .line 464
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
