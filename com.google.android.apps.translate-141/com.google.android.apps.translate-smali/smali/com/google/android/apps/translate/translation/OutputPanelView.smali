.class public Lcom/google/android/apps/translate/translation/OutputPanelView;
.super Landroid/widget/LinearLayout;
.source "OutputPanelView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;
    }
.end annotation


# static fields
.field public static final SHOW_TRANSLATED_TEXT:Z = false

.field private static final TAG:Ljava/lang/String; = "OutputPanelView"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

.field private mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

.field private mDictionaryTextView:Landroid/widget/TextView;

.field private mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

.field private mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

.field private mInitLock:Landroid/os/ConditionVariable;

.field private mLanguageList:Lcom/google/android/apps/translate/Languages;

.field private mMessageTextView:Landroid/widget/TextView;

.field private mOutputTextViewPanelView:Landroid/widget/LinearLayout;

.field private mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mInitLock:Landroid/os/ConditionVariable;

    .line 68
    check-cast p1, Landroid/app/Activity;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    .line 69
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/history/BaseDb;)Lcom/google/android/apps/translate/history/BaseDb;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/history/BaseDb;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->openFavoriteDb()Lcom/google/android/apps/translate/history/BaseDb;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/OutputPanelView;->tryLoadingFromHistory(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/translate/translation/OutputPanelView;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->showTranslationAlpha(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/translate/translation/OutputPanelView;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->setDictionaryResult(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/translate/translation/OutputPanelView;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mDictionaryTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/translate/translation/OutputPanelView;->translateInBackground(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/translate/translation/OutputPanelView;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mOutputTextViewPanelView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/history/BaseDb;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/history/BaseDb;)Lcom/google/android/apps/translate/history/BaseDb;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/history/BaseDb;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->openHistoryDb()Lcom/google/android/apps/translate/history/BaseDb;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/translation/OutputPanelView;)Landroid/os/ConditionVariable;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mInitLock:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/translation/TranslateEntry;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/OutputPanelView;->addToDb(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/translation/OutputPanelView;->setTranslateResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/TranslateManager;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/translate/translation/OutputPanelView;)Lcom/google/android/apps/translate/Languages;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    return-object v0
.end method

.method private addToDb(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)Z
    .locals 3
    .parameter "db"
    .parameter "translateEntry"
    .parameter "skipDbWrite"

    .prologue
    const/4 v0, 0x1

    .line 276
    const-string v1, "OutputPanelView"

    const-string v2, "addToDb"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/BaseDb;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 280
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/translate/translation/OutputPanelView;->isStarred(Lcom/google/android/apps/translate/translation/TranslateEntry;)Z

    move-result v2

    invoke-interface {v1, p2, v2}, Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;->onTranslationDone(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V

    .line 285
    :cond_0
    if-nez p3, :cond_1

    .line 286
    invoke-virtual {p2}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/translate/history/Entry;->getEntryWithoutOnMemoryAttributes()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/translate/history/BaseDb;->add(Lcom/google/android/apps/translate/history/Entry;)V

    .line 287
    invoke-virtual {p1, v0}, Lcom/google/android/apps/translate/history/BaseDb;->flush(Z)V

    .line 292
    :cond_1
    :goto_0
    return v0

    .line 291
    :cond_2
    const-string v0, "OutputPanelView"

    const-string v1, "database not opened!"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private closeFavoriteDb()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/history/BaseDb;->close(Z)V

    .line 257
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

    .line 259
    :cond_0
    return-void
.end method

.method private closeHistoryDb()V
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/history/BaseDb;->close(Z)V

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    .line 317
    :cond_0
    return-void
.end method

.method private flushFavoriteDb()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/history/BaseDb;->flush(Z)V

    .line 265
    :cond_0
    return-void
.end method

.method private flushHistoryDb()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/history/BaseDb;->flush(Z)V

    .line 323
    :cond_0
    return-void
.end method

.method private getFromDb(Lcom/google/android/apps/translate/history/BaseDb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/translate/history/Entry;
    .locals 1
    .parameter "db"
    .parameter "from"
    .parameter "to"
    .parameter "inputText"

    .prologue
    .line 296
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/BaseDb;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {p1, p2, p3, p4}, Lcom/google/android/apps/translate/history/BaseDb;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 299
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideTranslationAlpha()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/OutputPanelView$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/OutputPanelView$5;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 453
    return-void
.end method

.method private initializeInBackground()V
    .locals 2

    .prologue
    .line 121
    const-string v0, "OutputPanelView"

    const-string v1, "initializeInBackground"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->showTranslatingMessage()V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mInitLock:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 124
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/apps/translate/translation/OutputPanelView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/OutputPanelView$1;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 153
    return-void
.end method

.method private isInDb(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;)Z
    .locals 3
    .parameter "db"
    .parameter "translation"

    .prologue
    .line 268
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/BaseDb;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p2, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/translate/history/BaseDb;->exists(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 272
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openFavoriteDb()Lcom/google/android/apps/translate/history/BaseDb;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/translate/history/FavoriteDb;->open(Landroid/content/Context;)Lcom/google/android/apps/translate/history/BaseDb;

    move-result-object v0

    return-object v0
.end method

.method private openHistoryDb()Lcom/google/android/apps/translate/history/BaseDb;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/translate/history/HistoryDb;->open(Landroid/content/Context;)Lcom/google/android/apps/translate/history/BaseDb;

    move-result-object v0

    return-object v0
.end method

.method private setDictionaryResult(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 2
    .parameter "word"
    .parameter "result"
    .parameter "fromLanguage"
    .parameter "toLanguage"

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/OutputPanelView$7;

    invoke-direct {v1, p0, p2, p4, p3}, Lcom/google/android/apps/translate/translation/OutputPanelView$7;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;Ljava/lang/String;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 493
    return-void
.end method

.method private setTranslateResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/OutputPanelView$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/translate/translation/OutputPanelView$4;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 443
    return-void
.end method

.method private showTranslationAlpha(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 2
    .parameter "from"
    .parameter "to"

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/OutputPanelView$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/translate/translation/OutputPanelView$6;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 468
    return-void
.end method

.method private translateInBackground(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter "from"
    .parameter "to"
    .parameter "text"
    .parameter "translatedText"

    .prologue
    .line 157
    const-string v0, "OutputPanelView"

    const-string v1, "translateInBackground"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    const-string v0, "no text, do not translate"

    invoke-static {v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 248
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->showTranslatingMessage()V

    .line 168
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/google/android/apps/translate/translation/OutputPanelView$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/OutputPanelView$2;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private tryLoadingFromHistory(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 9
    .parameter "from"
    .parameter "to"
    .parameter "text"

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 337
    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v5

    .line 338
    .local v5, isAutoLanguage:Z
    if-eqz v5, :cond_0

    .line 339
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v2, p3}, Lcom/google/android/apps/translate/history/FavoriteDb;->getAllByATime(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 340
    .local v7, list:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v6, v0

    .line 341
    .local v6, entry:Lcom/google/android/apps/translate/history/Entry;
    :goto_0
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getToLanguageShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 343
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguageShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object p1

    .line 358
    .end local v6           #entry:Lcom/google/android/apps/translate/history/Entry;
    .end local v7           #list:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :cond_0
    :goto_1
    invoke-static {p1}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3, p3}, Lcom/google/android/apps/translate/translation/OutputPanelView;->getFromDb(Lcom/google/android/apps/translate/history/BaseDb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v6

    .line 361
    .restart local v6       #entry:Lcom/google/android/apps/translate/history/Entry;
    if-eqz v6, :cond_5

    .line 363
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getOutputText()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 365
    .local v0, resultEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->postBackCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 367
    const-string v1, "Loaded from favorites"

    invoke-static {v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 368
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->setResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    invoke-direct {p0, v1, v0, v8}, Lcom/google/android/apps/translate/translation/OutputPanelView;->addToDb(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)Z

    .line 388
    .end local v0           #resultEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    .end local v6           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_1
    :goto_2
    return-object v0

    .line 340
    .restart local v7       #list:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :cond_2
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/history/Entry;

    move-object v6, v1

    goto :goto_0

    .line 346
    .restart local v6       #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v2, p3}, Lcom/google/android/apps/translate/history/HistoryDb;->getAllByATime(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 347
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v6, v0

    .line 348
    :goto_3
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getToLanguageShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguageShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object p1

    goto :goto_1

    .line 347
    :cond_4
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/history/Entry;

    move-object v6, v1

    goto :goto_3

    .line 373
    .end local v7           #list:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translate/history/Entry;>;"
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3, p3}, Lcom/google/android/apps/translate/translation/OutputPanelView;->getFromDb(Lcom/google/android/apps/translate/history/BaseDb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/translate/history/Entry;

    move-result-object v6

    .line 374
    if-eqz v6, :cond_1

    .line 376
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mHistoryDb:Lcom/google/android/apps/translate/history/BaseDb;

    invoke-direct {p0, v1, v6}, Lcom/google/android/apps/translate/translation/OutputPanelView;->updateLastAccessed(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/history/Entry;)V

    .line 379
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getOutputText()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 381
    .restart local v0       #resultEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->postBackCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 383
    const-string v1, "Loaded from history"

    invoke-static {v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->setResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    goto :goto_2
.end method

.method private updateLastAccessed(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/history/Entry;)V
    .locals 1
    .parameter "db"
    .parameter "entry"

    .prologue
    .line 303
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/translate/history/BaseDb;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {p1, p2}, Lcom/google/android/apps/translate/history/BaseDb;->updateLastAccessed(Lcom/google/android/apps/translate/history/Entry;)V

    .line 306
    :cond_0
    return-void
.end method


# virtual methods
.method public copyTranslationText()V
    .locals 4

    .prologue
    .line 558
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->isInvalid()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 561
    :cond_1
    new-instance v0, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Lcom/google/android/apps/translate/history/Entry;)V

    .line 563
    .local v0, results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/translate/Util;->copyToClipBoard(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    .line 564
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/translate/R$string;->toast_message_copy:I

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method public doTranslate(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter "from"
    .parameter "to"
    .parameter "text"
    .parameter "translatedText"

    .prologue
    .line 497
    const-string v0, "OutputPanelView"

    const-string v1, "doTranslate"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 510
    :goto_0
    return-void

    .line 502
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    new-instance v0, Lcom/google/android/apps/translate/translation/OutputPanelView$8;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/OutputPanelView$8;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getCurrentTranslation()Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    return-object v0
.end method

.method public hideTranslateResultView()V
    .locals 2

    .prologue
    .line 531
    const-string v0, "OutputPanelView"

    const-string v1, "hideTranslateResultView"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/OutputPanelView$10;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/OutputPanelView$10;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 540
    return-void
.end method

.method public init(Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;Lcom/google/android/apps/translate/translation/ChipView;Lcom/google/android/apps/translate/TranslateManager;Lcom/google/android/apps/translate/Languages;)V
    .locals 2
    .parameter "callback"
    .parameter "chipView"
    .parameter "translateManager"
    .parameter "languageList"

    .prologue
    .line 76
    const-string v0, "OutputPanelView"

    const-string v1, "init"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCallback:Lcom/google/android/apps/translate/translation/OutputPanelView$OutputPanelViewCallback;

    .line 82
    iput-object p3, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    .line 83
    iput-object p4, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mTranslateManager:Lcom/google/android/apps/translate/TranslateManager;

    invoke-static {v0}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-static {v0}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget v0, Lcom/google/android/apps/translate/R$id;->output_textview_panel:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mOutputTextViewPanelView:Landroid/widget/LinearLayout;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mOutputTextViewPanelView:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/google/android/apps/translate/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget v0, Lcom/google/android/apps/translate/R$id;->text_translation_panel_message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mMessageTextView:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mMessageTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    sget v0, Lcom/google/android/apps/translate/R$id;->text_dict:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mDictionaryTextView:Landroid/widget/TextView;

    .line 96
    sget v0, Lcom/google/android/apps/translate/R$id;->btn_supersize:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    sget v0, Lcom/google/android/apps/translate/R$id;->btn_share:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    sget v0, Lcom/google/android/apps/translate/R$id;->btn_copy:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->initializeInBackground()V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->showTranslateResultView()V

    goto :goto_0
.end method

.method public isStarred(Lcom/google/android/apps/translate/translation/TranslateEntry;)Z
    .locals 1
    .parameter "translation"

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mFavoriteDb:Lcom/google/android/apps/translate/history/BaseDb;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/translate/translation/OutputPanelView;->isInDb(Lcom/google/android/apps/translate/history/BaseDb;Lcom/google/android/apps/translate/translation/TranslateEntry;)Z

    move-result v0

    return v0
.end method

.method public isTranslateResultViewHidden()Z
    .locals 2

    .prologue
    .line 527
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 544
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 545
    .local v0, id:I
    sget v1, Lcom/google/android/apps/translate/R$id;->btn_copy:I

    if-ne v0, v1, :cond_1

    .line 546
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->copyTranslationText()V

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    sget v1, Lcom/google/android/apps/translate/R$id;->btn_share:I

    if-ne v0, v1, :cond_2

    .line 548
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->shareTranslatedText()V

    goto :goto_0

    .line 549
    :cond_2
    sget v1, Lcom/google/android/apps/translate/R$id;->btn_supersize:I

    if-ne v0, v1, :cond_0

    .line 550
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->showSupersizeTranslatText()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 589
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->closeFavoriteDb()V

    .line 590
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->closeHistoryDb()V

    .line 591
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 594
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->flushHistoryDb()V

    .line 595
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/OutputPanelView;->flushFavoriteDb()V

    .line 596
    return-void
.end method

.method public postBackCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 2
    .parameter "resultEntry"

    .prologue
    .line 392
    const-string v0, "OutputPanelView"

    const-string v1, "postBackCurrentTranslation"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/OutputPanelView$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/translate/translation/OutputPanelView$3;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 399
    return-void
.end method

.method public refreshLanguageList(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V
    .locals 6
    .parameter "newFrom"
    .parameter "newTo"

    .prologue
    .line 112
    invoke-static {}, Lcom/google/android/apps/translate/LanguagesFactory;->get()Lcom/google/android/apps/translate/LanguagesFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/translate/LanguagesFactory;->getLanguages(Landroid/content/Context;Ljava/util/Locale;)Lcom/google/android/apps/translate/Languages;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 114
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v3, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v4, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-boolean v5, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->isAutoLanguage:Z

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    .line 118
    :cond_0
    return-void
.end method

.method public setResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 326
    const-string v0, "OutputPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setResult result.outputText="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/translation/OutputPanelView;->setTranslateResult(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 328
    return-void
.end method

.method public shareTranslatedText()V
    .locals 4

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateEntry;->isInvalid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 584
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->performChipActionShare(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    goto :goto_0
.end method

.method public showSupersizeTranslatText()V
    .locals 3

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-nez v0, :cond_0

    .line 575
    :goto_0
    return-void

    .line 574
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/translate/Util;->openSupersizeTextActivity(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V

    goto :goto_0
.end method

.method public showTranslateResultView()V
    .locals 2

    .prologue
    .line 513
    const-string v0, "OutputPanelView"

    const-string v1, "showTranslateResultView"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/OutputPanelView;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/OutputPanelView$9;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/OutputPanelView$9;-><init>(Lcom/google/android/apps/translate/translation/OutputPanelView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 524
    return-void
.end method

.method public showTranslatingMessage()V
    .locals 0

    .prologue
    .line 403
    return-void
.end method
