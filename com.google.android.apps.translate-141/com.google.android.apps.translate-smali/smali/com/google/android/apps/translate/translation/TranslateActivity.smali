.class public Lcom/google/android/apps/translate/translation/TranslateActivity;
.super Landroid/app/Activity;
.source "TranslateActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TranslateActivity"


# instance fields
.field private mLanguageList:Lcom/google/android/apps/translate/Languages;

.field private mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

.field private mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-direct {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/translation/TranslateActivity;)Lcom/google/android/apps/translate/translation/TranslateHelper;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->finish()V

    .line 162
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onActivityResult(IILandroid/content/Intent;)V

    .line 144
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "newConfig"

    .prologue
    .line 134
    const-string v0, "TranslateActivity"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onConfigurationChanged(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    :cond_0
    return v1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    .line 38
    const-string v1, "TranslateActivity"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->getLanguageListFromProfile(Landroid/content/Context;)Lcom/google/android/apps/translate/Languages;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/TranslateActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/apps/translate/R$layout;->translate_activity:I

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 43
    .local v0, rootView:Landroid/widget/FrameLayout;
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v1, p0, v2, v0, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onCreate(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Landroid/view/View;Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;)V

    .line 45
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/TranslateActivity;->setContentView(Landroid/view/View;)V

    .line 47
    new-instance v1, Lcom/google/android/apps/translate/translation/TranslateActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/translation/TranslateActivity$1;-><init>(Lcom/google/android/apps/translate/translation/TranslateActivity;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/translate/translation/TranslateActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getContextMenuTargetView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/translate/translation/TranslateActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 56
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    .line 120
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 122
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/TranslateActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x1

    .line 98
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 85
    const-string v0, "TranslateActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onDestroy()V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mOnLanguagesChanged:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/TranslateActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 91
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 166
    const-string v0, "TranslateActivity"

    const-string v1, "onKeyDown"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x1

    .line 170
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter "intent"

    .prologue
    .line 60
    const-string v0, "TranslateActivity"

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onNewIntent(Landroid/content/Intent;)V

    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 63
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onPause()V

    .line 156
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 103
    invoke-static {p1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const/4 v0, 0x1

    .line 106
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onResume()V

    .line 150
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 67
    const-string v0, "TranslateActivity"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onStart()V

    .line 72
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 76
    const-string v0, "TranslateActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateActivity;->mTranslateHelper:Lcom/google/android/apps/translate/translation/TranslateHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->onStop()V

    .line 81
    :cond_0
    return-void
.end method
