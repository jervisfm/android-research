.class public Lcom/google/android/apps/translate/translation/TranslateEntry;
.super Ljava/lang/Object;
.source "TranslateEntry.java"


# instance fields
.field public final fromLanguage:Lcom/google/android/apps/translate/Language;

.field public final inputText:Ljava/lang/String;

.field public final isAutoLanguage:Z

.field public final outputText:Ljava/lang/String;

.field public final toLanguage:Lcom/google/android/apps/translate/Language;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .parameter "fromLanguage"
    .parameter "toLanguage"
    .parameter "inputText"
    .parameter "outputText"
    .parameter "isAutoLanguage"

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    .line 31
    invoke-static {p3}, Lcom/google/android/apps/translate/translation/TranslateEntry;->normalizeInputText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    .line 33
    iput-boolean p5, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->isAutoLanguage:Z

    .line 34
    return-void
.end method

.method public static build(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 7
    .parameter "from"
    .parameter "to"
    .parameter "text"
    .parameter "translation"
    .parameter "isAutoLang"
    .parameter "languageList"

    .prologue
    const/4 v6, 0x0

    .line 103
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 104
    invoke-virtual {p5, p0}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    .line 105
    .local v1, fromLanguage:Lcom/google/android/apps/translate/Language;
    invoke-virtual {p5, p1}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    .line 107
    .local v2, toLanguage:Lcom/google/android/apps/translate/Language;
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 108
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-nez p3, :cond_0

    const-string v4, ""

    :goto_0
    move-object v3, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 114
    .local v0, entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasInputText()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 118
    .end local v0           #entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    .end local v1           #fromLanguage:Lcom/google/android/apps/translate/Language;
    .end local v2           #toLanguage:Lcom/google/android/apps/translate/Language;
    :goto_1
    return-object v0

    .restart local v1       #fromLanguage:Lcom/google/android/apps/translate/Language;
    .restart local v2       #toLanguage:Lcom/google/android/apps/translate/Language;
    :cond_0
    move-object v4, p3

    .line 108
    goto :goto_0

    .restart local v0       #entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    :cond_1
    move-object v0, v6

    .line 114
    goto :goto_1

    .end local v0           #entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    .end local v1           #fromLanguage:Lcom/google/android/apps/translate/Language;
    .end local v2           #toLanguage:Lcom/google/android/apps/translate/Language;
    :cond_2
    move-object v0, v6

    .line 118
    goto :goto_1
.end method

.method public static normalizeInputText(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "inputText"

    .prologue
    .line 81
    if-nez p0, :cond_0

    .line 82
    const-string v0, ""

    .line 84
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getFromLanguageForLanguagePicker(Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/Language;
    .locals 1
    .parameter "languageList"

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->isAutoLanguage:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 126
    const-string v0, "auto"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v0

    .line 128
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    goto :goto_0
.end method

.method public hasInputText()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSourceAndTargetLanguages()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInvalid()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toNewEntry()Lcom/google/android/apps/translate/history/Entry;
    .locals 5

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/apps/translate/history/Entry;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/translate/history/Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
