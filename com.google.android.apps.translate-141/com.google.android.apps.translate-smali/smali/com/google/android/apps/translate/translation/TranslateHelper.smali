.class public Lcom/google/android/apps/translate/translation/TranslateHelper;
.super Lcom/google/android/apps/translate/translation/BaseTranslateHelper;
.source "TranslateHelper.java"

# interfaces
.implements Lcom/google/android/apps/translate/editor/PreImeAutoCompleteTextView$OnKeyPreImeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/translation/TranslateHelper$6;,
        Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;
    }
.end annotation


# static fields
.field private static final DEFAULT_BROWSER_ACTIVITY:Ljava/lang/String; = "com.android.browser.BrowserActivity"

.field private static final DEFAULT_BROWSER_PACKAGE:Ljava/lang/String; = "com.android.browser"

.field private static final TAG:Ljava/lang/String; = "TranslateHelper"


# instance fields
.field private mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

.field private mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

.field private mTranslateFragmentWrapper:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

.field private mTranslatePanel:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;-><init>()V

    .line 64
    const-string v0, "TranslateHelper"

    const-string v1, "TranslateHelper"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method static synthetic access$000(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Lcom/google/android/apps/translate/Languages;Ljava/util/Locale;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->sendTranslateResult(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Lcom/google/android/apps/translate/Languages;Ljava/util/Locale;)V

    return-void
.end method

.method static synthetic access$100(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Languages;Ljava/util/Locale;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 54
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getLocalizedLanguageName(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Languages;Ljava/util/Locale;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/translation/TranslateHelper;)Lcom/google/android/apps/translate/translation/InputPanel;
    .locals 1
    .parameter "x0"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/translate/translation/TranslateHelper;Lcom/google/android/apps/translate/translation/InputPanel;)Lcom/google/android/apps/translate/translation/InputPanel;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/translation/TranslateHelper;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mTranslatePanel:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/translate/translation/TranslateHelper;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 1
    .parameter "x0"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    return-object v0
.end method

.method public static getAutoTranslationMessage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "context"
    .parameter "to"
    .parameter "toLanguageLong"

    .prologue
    .line 642
    sget v0, Lcom/google/android/apps/translate/R$string;->label_automatic_translation:I

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/translate/Util;->getLocalizedStringId(Landroid/content/Context;ILcom/google/android/apps/translate/Language;)I

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getLocalizedLanguageName(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Languages;Ljava/util/Locale;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V
    .locals 6
    .parameter "activity"
    .parameter "lang"
    .parameter "languages"
    .parameter "locale"
    .parameter "callback"

    .prologue
    .line 658
    const-string v4, "TranslateHelper"

    const-string v5, "sendTranslateResult"

    invoke-static {v4, v5}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    if-eqz p1, :cond_2

    .line 660
    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v3

    .line 661
    .local v3, shortName:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v0

    .line 663
    .local v0, localizedLangName:Ljava/lang/String;
    const/4 v2, 0x0

    .line 664
    .local v2, refreshLocalLangList:Z
    if-eqz p2, :cond_0

    .line 665
    invoke-virtual {p2, v3}, Lcom/google/android/apps/translate/Languages;->getToLanguageLongName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 666
    .local v1, longName:Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 667
    move-object v0, v1

    .line 673
    .end local v1           #longName:Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 674
    invoke-static {p0, p1, p3, p4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getLocalizedLanguageNameAfterLoadLanguages(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Ljava/util/Locale;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V

    .line 677
    :cond_1
    invoke-interface {p4, p1, p3, v0}, Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;->onLanguageNameReceived(Lcom/google/android/apps/translate/Language;Ljava/util/Locale;Ljava/lang/String;)V

    .line 679
    .end local v0           #localizedLangName:Ljava/lang/String;
    .end local v2           #refreshLocalLangList:Z
    .end local v3           #shortName:Ljava/lang/String;
    :cond_2
    return-void

    .line 669
    .restart local v0       #localizedLangName:Ljava/lang/String;
    .restart local v1       #longName:Ljava/lang/String;
    .restart local v2       #refreshLocalLangList:Z
    .restart local v3       #shortName:Ljava/lang/String;
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static getLocalizedLanguageName(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V
    .locals 4
    .parameter "activity"
    .parameter "lang"
    .parameter "callback"

    .prologue
    .line 504
    const-string v2, "TranslateHelper"

    const-string v3, "getLocalizedLanguageName"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    if-eqz p1, :cond_0

    .line 506
    invoke-virtual {p1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->languageShortNameToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 507
    .local v1, locale:Ljava/util/Locale;
    invoke-static {}, Lcom/google/android/apps/translate/LanguagesFactory;->get()Lcom/google/android/apps/translate/LanguagesFactory;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p0, v1, v3}, Lcom/google/android/apps/translate/LanguagesFactory;->getLanguages(Landroid/content/Context;Ljava/util/Locale;Z)Lcom/google/android/apps/translate/Languages;

    move-result-object v0

    .line 509
    .local v0, languages:Lcom/google/android/apps/translate/Languages;
    if-nez v0, :cond_1

    .line 510
    invoke-static {p0, p1, v1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getLocalizedLanguageNameAfterLoadLanguages(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Ljava/util/Locale;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V

    .line 515
    .end local v0           #languages:Lcom/google/android/apps/translate/Languages;
    .end local v1           #locale:Ljava/util/Locale;
    :cond_0
    :goto_0
    return-void

    .line 512
    .restart local v0       #languages:Lcom/google/android/apps/translate/Languages;
    .restart local v1       #locale:Ljava/util/Locale;
    :cond_1
    invoke-static {p0, p1, v0, v1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getLocalizedLanguageName(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Languages;Ljava/util/Locale;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V

    goto :goto_0
.end method

.method private static getLocalizedLanguageNameAfterLoadLanguages(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Ljava/util/Locale;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V
    .locals 9
    .parameter "activity"
    .parameter "lang"
    .parameter "locale"
    .parameter "callback"

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 558
    const-string v0, "TranslateHelper"

    const-string v1, "getLocalizedLanguageNameAfterLoadLanguages"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    new-array v7, v3, [Z

    aput-boolean v2, v7, v2

    .line 560
    .local v7, canceled:[Z
    const/4 v1, 0x0

    sget v0, Lcom/google/android/apps/translate/R$string;->msg_loading:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/translate/translation/TranslateHelper$3;

    invoke-direct {v5, v7}, Lcom/google/android/apps/translate/translation/TranslateHelper$3;-><init>([Z)V

    move-object v0, p0

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v4

    .line 569
    .local v4, dialog:Landroid/app/ProgressDialog;
    const-string v8, ""

    .line 570
    .local v8, langName:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateHelper$4;

    move-object v1, p0

    move-object v2, p2

    move-object v3, v7

    move-object v5, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/translate/translation/TranslateHelper$4;-><init>(Landroid/app/Activity;Ljava/util/Locale;[ZLandroid/app/ProgressDialog;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/translation/TranslateHelper$LocalizedLanguageCallback;)V

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper$4;->start()V

    .line 587
    return-void
.end method

.method public static getOriginalTextMessage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "context"
    .parameter "to"
    .parameter "fromLanguageLong"

    .prologue
    .line 650
    sget v0, Lcom/google/android/apps/translate/R$string;->label_original_text:I

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/translate/Util;->getLocalizedStringId(Landroid/content/Context;ILcom/google/android/apps/translate/Language;)I

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getTranslateEntryByAction(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 4
    .parameter "activity"
    .parameter "intent"
    .parameter "languageList"

    .prologue
    .line 154
    const-string v2, "TranslateHelper"

    const-string v3, "getTranslateEntryByAction"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, action:Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->parseNativeIntent(Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    .line 157
    .local v1, entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    move-object v2, v1

    .line 184
    :goto_0
    return-object v2

    .line 161
    :cond_1
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 162
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->parseTranslateUrl(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    .line 163
    if-eqz v1, :cond_2

    .line 164
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->ACTION_VIEW:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    :cond_2
    move-object v2, v1

    .line 167
    goto :goto_0

    .line 168
    :cond_3
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 169
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->parseSentData(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_4

    .line 171
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->ACTION_SEND:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    :cond_4
    move-object v2, v1

    .line 174
    goto :goto_0

    .line 175
    :cond_5
    const-string v2, "android.intent.action.SEARCH"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 176
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->parseSearchData(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    .line 177
    if-eqz v1, :cond_6

    .line 178
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;->ACTION_SEARCH:Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/UserActivityMgr;->setTranslationSource(Lcom/google/android/apps/translate/UserActivityMgr$RequestSource;)Lcom/google/android/apps/translate/UserActivityMgr;

    :cond_6
    move-object v2, v1

    .line 181
    goto :goto_0

    .line 184
    :cond_7
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getWebTranslateUri(Lcom/google/android/apps/translate/Languages;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 5
    .parameter "languageList"
    .parameter "systemLang"
    .parameter "inputText"

    .prologue
    .line 365
    invoke-static {p2}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p2}, Landroid/webkit/URLUtil;->isHttpUrl(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p2}, Landroid/webkit/URLUtil;->isHttpsUrl(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 367
    :cond_0
    const/4 v2, 0x0

    .line 386
    :goto_0
    return-object v2

    .line 369
    :cond_1
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 370
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v2, "http"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "translate.google.com"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "translate"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "sl"

    const-string v4, "auto"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "u"

    invoke-virtual {v2, v3, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 376
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 378
    const-string v2, "hl"

    invoke-virtual {v0, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 379
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    .line 380
    .local v1, toLanguage:Lcom/google/android/apps/translate/Language;
    if-eqz v1, :cond_2

    .line 382
    const-string v2, "tl"

    invoke-virtual {v1}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 386
    .end local v1           #toLanguage:Lcom/google/android/apps/translate/Language;
    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method static onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 3
    .parameter "activity"
    .parameter "menu"
    .parameter "inflater"

    .prologue
    const/4 v2, 0x0

    .line 397
    const-string v0, "TranslateHelper"

    const-string v1, "onCreateOptionsMenu"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    sget v0, Lcom/google/android/apps/translate/R$menu;->translate_activity_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 399
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->useFragments()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    sget v0, Lcom/google/android/apps/translate/R$id;->menu_conversation_mode:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 402
    sget v0, Lcom/google/android/apps/translate/R$id;->menu_history:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 403
    sget v0, Lcom/google/android/apps/translate/R$id;->menu_favorite:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 405
    :cond_0
    sget v0, Lcom/google/android/apps/translate/R$id;->menu_sms_translation:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/translate/Util;->hasSmsSupport(Landroid/content/Context;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 406
    return v2
.end method

.method static onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 410
    const-string v0, "TranslateHelper"

    const-string v1, "onPrepareOptionsMenu"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const/4 v0, 0x0

    return v0
.end method

.method private static parseCgiParams(Landroid/net/Uri;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 6
    .parameter "uri"
    .parameter "languageList"

    .prologue
    .line 235
    const-string v2, "sl"

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, sl:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 237
    const-string v0, "auto"

    .line 239
    :cond_0
    const-string v2, "tl"

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 240
    .local v1, tl:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 241
    invoke-static {}, Lcom/google/android/apps/translate/Util;->getLanguageShortNameByLocale()Ljava/lang/String;

    move-result-object v1

    .line 243
    :cond_1
    const-string v2, "q"

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Ljava/lang/String;)Z

    move-result v4

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->build(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v2

    return-object v2
.end method

.method private static parseFragmentParams(Landroid/net/Uri;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 8
    .parameter "uri"
    .parameter "languageList"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x3

    const/4 v4, 0x0

    .line 253
    const-string v0, "TranslateHelper"

    const-string v1, "parseFragmentParams"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v6

    .line 255
    .local v6, fragment:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    const-string v0, "\\|"

    invoke-virtual {v6, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v7

    .line 257
    .local v7, params:[Ljava/lang/String;
    array-length v0, v7

    if-ne v0, v2, :cond_0

    .line 258
    aget-object v0, v7, v4

    const/4 v1, 0x1

    aget-object v1, v7, v1

    const/4 v2, 0x2

    aget-object v2, v7, v2

    aget-object v4, v7, v4

    invoke-static {v4}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Ljava/lang/String;)Z

    move-result v4

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->build(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v3

    .line 267
    .end local v7           #params:[Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method private static parseNativeIntent(Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 6
    .parameter "intent"
    .parameter "languageList"

    .prologue
    .line 189
    const-string v1, "key_language_from"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, sl:Ljava/lang/String;
    const-string v1, "key_language_to"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "key_text_input"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "key_text_output"

    invoke-virtual {p0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Ljava/lang/String;)Z

    move-result v4

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->build(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    return-object v1
.end method

.method private static parseSearchData(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 3
    .parameter "activity"
    .parameter "intent"
    .parameter "languageList"

    .prologue
    .line 327
    const-string v1, "TranslateHelper"

    const-string v2, "parseSearchData"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SEARCH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 329
    :cond_0
    const/4 v1, 0x0

    .line 336
    :goto_0
    return-object v1

    .line 332
    :cond_1
    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 333
    .local v0, inputText:Ljava/lang/String;
    if-nez v0, :cond_2

    .line 334
    const-string v1, "user_query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 336
    :cond_2
    invoke-static {p0, v0, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->prepareForAutoTranslation(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    goto :goto_0
.end method

.method private static parseSentData(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 3
    .parameter "activity"
    .parameter "intent"
    .parameter "languageList"

    .prologue
    .line 309
    const-string v1, "TranslateHelper"

    const-string v2, "parseSentData"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 311
    :cond_0
    const/4 v1, 0x0

    .line 315
    :goto_0
    return-object v1

    .line 314
    :cond_1
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, inputText:Ljava/lang/String;
    invoke-static {p0, v0, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->prepareForAutoTranslation(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    goto :goto_0
.end method

.method private static parseTranslateUrl(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 5
    .parameter "activity"
    .parameter "intent"
    .parameter "languageList"

    .prologue
    const/4 v2, 0x0

    .line 210
    const-string v3, "TranslateHelper"

    const-string v4, "parseTranslateUrl"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object v0, v2

    .line 230
    :cond_1
    :goto_0
    return-object v0

    .line 217
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 218
    .local v1, uri:Landroid/net/Uri;
    invoke-static {v1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->parseCgiParams(Landroid/net/Uri;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    .line 219
    .local v0, translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    if-nez v0, :cond_3

    .line 220
    invoke-static {v1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->parseFragmentParams(Landroid/net/Uri;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    .line 223
    :cond_3
    if-nez v0, :cond_1

    .line 226
    const-string v3, "TranslateHelper"

    const-string v4, "parseTranslateUrl fall back to Translate web app."

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->startWebTranslate(Landroid/app/Activity;Landroid/net/Uri;)V

    move-object v0, v2

    .line 230
    goto :goto_0
.end method

.method public static performChipActionShare(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V
    .locals 8
    .parameter "activity"
    .parameter "languages"
    .parameter "entry"
    .parameter "chipPart"

    .prologue
    const/4 v7, 0x1

    .line 935
    new-instance v6, Lcom/google/android/apps/translate/Translate$TranslateResult;

    invoke-direct {v6, p2}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Lcom/google/android/apps/translate/history/Entry;)V

    .line 936
    .local v6, results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguageShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/translate/Languages;->getFromLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getToLanguageShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/translate/Languages;->getToLanguageByShortName(Ljava/lang/String;)Lcom/google/android/apps/translate/Language;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getOutputText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/apps/translate/history/Entry;->getFromLanguageShortName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Ljava/lang/String;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 942
    .local v0, translateEntry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    sget-object v1, Lcom/google/android/apps/translate/translation/TranslateHelper$6;->$SwitchMap$com$google$android$apps$translate$translation$ChipView$ChipPart:[I

    invoke-virtual {p3}, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 956
    invoke-static {p0, v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->sendTranslateResult(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 957
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_TRG_SHARE_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v1, v2, v7}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 961
    :goto_0
    return-void

    .line 945
    :pswitch_0
    iget-object v1, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->sendText(Landroid/app/Activity;Ljava/lang/String;)V

    .line 946
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_SRC_SHARE_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v1, v2, v7}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    goto :goto_0

    .line 950
    :pswitch_1
    invoke-virtual {v6}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->sendText(Landroid/app/Activity;Ljava/lang/String;)V

    .line 951
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_TRG_SHARE_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v1, v2, v7}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    goto :goto_0

    .line 942
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static prepareForAutoTranslation(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 8
    .parameter "activity"
    .parameter "inputText"
    .parameter "languageList"

    .prologue
    const/4 v1, 0x0

    .line 341
    const-string v0, "TranslateHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepareForAutoTranslation inputText="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    invoke-static {p1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->normalizeInputText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 343
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    :goto_0
    return-object v1

    .line 349
    :cond_0
    invoke-static {}, Lcom/google/android/apps/translate/Util;->getLanguageShortNameByLocale()Ljava/lang/String;

    move-result-object v6

    .line 350
    .local v6, systemLang:Ljava/lang/String;
    invoke-static {p2, v6, p1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getWebTranslateUri(Lcom/google/android/apps/translate/Languages;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 351
    .local v7, uri:Landroid/net/Uri;
    if-eqz v7, :cond_1

    .line 352
    const-string v0, "TranslateHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepareForAutoTranslation uri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-static {p0, v7}, Lcom/google/android/apps/translate/translation/TranslateHelper;->startWebTranslate(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_0

    .line 359
    :cond_1
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    const/4 v5, 0x0

    move-object v2, v1

    move-object v3, p1

    move-object v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v1, v0

    goto :goto_0
.end method

.method public static readIntentData(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 4
    .parameter "activity"
    .parameter "intent"
    .parameter "languageList"

    .prologue
    const/4 v1, 0x0

    .line 135
    const-string v2, "TranslateHelper"

    const-string v3, "readIntentData"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    if-nez p1, :cond_0

    .line 137
    const-string v2, "no intent, finish activity"

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;)V

    move-object v0, v1

    .line 149
    :goto_0
    return-object v0

    .line 141
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getTranslateEntryByAction(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    .line 142
    .local v0, entry:Lcom/google/android/apps/translate/translation/TranslateEntry;
    if-nez v0, :cond_1

    .line 143
    const-string v2, "no input text or from/to languages"

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;)V

    move-object v0, v1

    .line 144
    goto :goto_0

    .line 148
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v2, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/translate/Profile;->setLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;)V

    goto :goto_0
.end method

.method public static sendText(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3
    .parameter "activity"
    .parameter "text"

    .prologue
    .line 682
    const-string v1, "TranslateHelper"

    const-string v2, "sendText"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    if-eqz p1, :cond_0

    .line 684
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 685
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 687
    sget v1, Lcom/google/android/apps/translate/R$string;->title_send_chooser:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 690
    .end local v0           #intent:Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static sendTranslateResult(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 4
    .parameter "activity"
    .parameter "translateEntry"

    .prologue
    .line 481
    const-string v2, "TranslateHelper"

    const-string v3, "sendTranslateResult"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    if-eqz p1, :cond_0

    .line 483
    iget-object v2, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->languageShortNameToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 485
    .local v1, locale:Ljava/util/Locale;
    invoke-static {}, Lcom/google/android/apps/translate/LanguagesFactory;->get()Lcom/google/android/apps/translate/LanguagesFactory;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p0, v1, v3}, Lcom/google/android/apps/translate/LanguagesFactory;->getLanguages(Landroid/content/Context;Ljava/util/Locale;Z)Lcom/google/android/apps/translate/Languages;

    move-result-object v0

    .line 487
    .local v0, languages:Lcom/google/android/apps/translate/Languages;
    if-nez v0, :cond_1

    .line 488
    invoke-static {p0, p1, v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->sendTranslateResultAfterLoadLanguages(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Ljava/util/Locale;)V

    .line 493
    .end local v0           #languages:Lcom/google/android/apps/translate/Languages;
    .end local v1           #locale:Ljava/util/Locale;
    :cond_0
    :goto_0
    return-void

    .line 490
    .restart local v0       #languages:Lcom/google/android/apps/translate/Languages;
    .restart local v1       #locale:Ljava/util/Locale;
    :cond_1
    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->sendTranslateResult(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Lcom/google/android/apps/translate/Languages;Ljava/util/Locale;)V

    goto :goto_0
.end method

.method private static sendTranslateResult(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Lcom/google/android/apps/translate/Languages;Ljava/util/Locale;)V
    .locals 14
    .parameter "activity"
    .parameter "translateEntry"
    .parameter "languages"
    .parameter "locale"

    .prologue
    .line 591
    const-string v11, "TranslateHelper"

    const-string v12, "sendTranslateResult"

    invoke-static {v11, v12}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    if-eqz p1, :cond_1

    .line 593
    iget-object v11, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v11}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    .line 594
    .local v2, fromLanguage:Ljava/lang/String;
    iget-object v11, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v11}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v9

    .line 595
    .local v9, toLanguage:Ljava/lang/String;
    iget-object v11, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v11}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v3

    .line 596
    .local v3, fromLanguageLong:Ljava/lang/String;
    iget-object v11, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v11}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v10

    .line 598
    .local v10, toLanguageLong:Ljava/lang/String;
    const/4 v6, 0x0

    .line 599
    .local v6, refreshLocalLangList:Z
    if-eqz p2, :cond_0

    .line 600
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/translate/Languages;->getToLanguageLongName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 601
    .local v5, longName:Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 602
    move-object v3, v5

    .line 607
    :goto_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Lcom/google/android/apps/translate/Languages;->getToLanguageLongName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 608
    if-eqz v5, :cond_3

    .line 609
    move-object v10, v5

    .line 615
    .end local v5           #longName:Ljava/lang/String;
    :cond_0
    :goto_1
    if-eqz v6, :cond_4

    .line 616
    move-object/from16 v0, p3

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->sendTranslateResultAfterLoadLanguages(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Ljava/util/Locale;)V

    .line 637
    .end local v2           #fromLanguage:Ljava/lang/String;
    .end local v3           #fromLanguageLong:Ljava/lang/String;
    .end local v6           #refreshLocalLangList:Z
    .end local v9           #toLanguage:Ljava/lang/String;
    .end local v10           #toLanguageLong:Ljava/lang/String;
    :cond_1
    :goto_2
    return-void

    .line 604
    .restart local v2       #fromLanguage:Ljava/lang/String;
    .restart local v3       #fromLanguageLong:Ljava/lang/String;
    .restart local v5       #longName:Ljava/lang/String;
    .restart local v6       #refreshLocalLangList:Z
    .restart local v9       #toLanguage:Ljava/lang/String;
    .restart local v10       #toLanguageLong:Ljava/lang/String;
    :cond_2
    const/4 v6, 0x1

    goto :goto_0

    .line 611
    :cond_3
    const/4 v6, 0x1

    goto :goto_1

    .line 620
    .end local v5           #longName:Ljava/lang/String;
    :cond_4
    new-instance v7, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v11, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->outputText:Ljava/lang/String;

    invoke-direct {v7, v11}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Ljava/lang/String;)V

    .line 623
    .local v7, results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    iget-object v8, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    .line 624
    .local v8, to:Lcom/google/android/apps/translate/Language;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0, v8, v10}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getAutoTranslationMessage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {p0, v8, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getOriginalTextMessage(Landroid/content/Context;Lcom/google/android/apps/translate/Language;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 629
    .local v1, content:Ljava/lang/String;
    new-instance v4, Landroid/content/Intent;

    const-string v11, "android.intent.action.SEND"

    invoke-direct {v4, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 630
    .local v4, intent:Landroid/content/Intent;
    const-string v11, "text/plain"

    invoke-virtual {v4, v11}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 631
    const-string v11, "android.intent.extra.SUBJECT"

    sget v12, Lcom/google/android/apps/translate/R$string;->msg_send_translation_subject:I

    iget-object v13, p1, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-static {p0, v12, v13}, Lcom/google/android/apps/translate/Util;->getLocalizedStringId(Landroid/content/Context;ILcom/google/android/apps/translate/Language;)I

    move-result v12

    invoke-virtual {p0, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 633
    const-string v11, "android.intent.extra.TEXT"

    invoke-virtual {v4, v11, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 634
    sget v11, Lcom/google/android/apps/translate/R$string;->title_send_chooser:I

    invoke-virtual {p0, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {p0, v11}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method private static sendTranslateResultAfterLoadLanguages(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;Ljava/util/Locale;)V
    .locals 7
    .parameter "activity"
    .parameter "translateEntry"
    .parameter "locale"

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 519
    const-string v0, "TranslateHelper"

    const-string v1, "sendTranslateResultAfterLoadLanguages"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    new-array v6, v3, [Z

    aput-boolean v2, v6, v2

    .line 521
    .local v6, canceled:[Z
    const/4 v1, 0x0

    sget v0, Lcom/google/android/apps/translate/R$string;->msg_loading:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/translate/translation/TranslateHelper$1;

    invoke-direct {v5, v6}, Lcom/google/android/apps/translate/translation/TranslateHelper$1;-><init>([Z)V

    move-object v0, p0

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v4

    .line 530
    .local v4, dialog:Landroid/app/ProgressDialog;
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateHelper$2;

    move-object v1, p0

    move-object v2, p2

    move-object v3, v6

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateHelper$2;-><init>(Landroid/app/Activity;Ljava/util/Locale;[ZLandroid/app/ProgressDialog;Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateHelper$2;->start()V

    .line 547
    return-void
.end method

.method private static startWebTranslate(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 10
    .parameter "activity"
    .parameter "uri"

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 271
    const-string v4, "TranslateHelper"

    const-string v5, "startWebTranslate"

    invoke-static {v4, v5}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 273
    .local v3, webTranslateIntent:Landroid/content/Intent;
    const-string v4, "android.intent.category.DEFAULT"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    const-string v4, "android.intent.category.BROWSABLE"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    const-string v4, "text/html"

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 278
    .local v0, defaultBrowserIntent:Landroid/content/Intent;
    const-string v4, "com.android.browser"

    const-string v5, "com.android.browser.BrowserActivity"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    new-array v6, v9, [Landroid/content/Intent;

    aput-object v0, v6, v8

    const/high16 v7, 0x1

    invoke-virtual {v4, v5, v6, v3, v7}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 286
    .local v2, info:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 287
    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    :cond_0
    :try_start_0
    invoke-virtual {p0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    :goto_0
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->WEB_TRANSLATION_REQUESTS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v4, v5, v9}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 298
    return-void

    .line 292
    :catch_0
    move-exception v1

    .line 293
    .local v1, e:Landroid/content/ActivityNotFoundException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget v5, Lcom/google/android/apps/translate/R$string;->msg_error_intent_web_translate:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;)V

    .line 294
    sget v4, Lcom/google/android/apps/translate/R$string;->msg_error_intent_web_translate:I

    invoke-static {p0, v4}, Lcom/google/android/apps/translate/Util;->showLongToastMessage(Landroid/app/Activity;I)V

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 4

    .prologue
    .line 778
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "key_no_transition_animation"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 780
    .local v0, noAnim:Z
    if-eqz v0, :cond_0

    .line 781
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->removeTransitionAnimation(Landroid/app/Activity;)V

    .line 783
    :cond_0
    return-void
.end method

.method public getContextMenuTargetView()Landroid/view/View;
    .locals 1

    .prologue
    .line 996
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->getContextMenuTargetView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method getCurrentTranslation()Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    return-object v0
.end method

.method public getInitialTranslationEntry()Lcom/google/android/apps/translate/translation/TranslateEntry;
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 448
    const/4 v7, 0x0

    .line 449
    .local v7, useTextInInputTextBox:Z
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasInputText()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/TranslateEntry;->hasSourceAndTargetLanguages()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-nez v0, :cond_1

    const-string v6, ""

    .line 453
    .local v6, normalizedText:Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v0, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    .line 455
    .end local v6           #normalizedText:Ljava/lang/String;
    :cond_0
    if-eqz v7, :cond_2

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v0, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getToLanguageGivenFromLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    .line 461
    .local v1, from:Lcom/google/android/apps/translate/Language;
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v2, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    .line 462
    .local v2, to:Lcom/google/android/apps/translate/Language;
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v3, v0, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    .line 477
    .local v3, inputText:Ljava/lang/String;
    :goto_1
    new-instance v0, Lcom/google/android/apps/translate/translation/TranslateEntry;

    const-string v4, ""

    invoke-static {v1}, Lcom/google/android/apps/translate/Util;->isAutoDetectLanguage(Lcom/google/android/apps/translate/Language;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;-><init>(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    .line 451
    .end local v1           #from:Lcom/google/android/apps/translate/Language;
    .end local v2           #to:Lcom/google/android/apps/translate/Language;
    .end local v3           #inputText:Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->getInputText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/translation/TranslateEntry;->normalizeInputText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 468
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-nez v0, :cond_3

    .line 469
    const/4 v1, 0x0

    .line 470
    .restart local v1       #from:Lcom/google/android/apps/translate/Language;
    const/4 v2, 0x0

    .line 475
    .restart local v2       #to:Lcom/google/android/apps/translate/Language;
    :goto_2
    const-string v3, ""

    .restart local v3       #inputText:Ljava/lang/String;
    goto :goto_1

    .line 472
    .end local v1           #from:Lcom/google/android/apps/translate/Language;
    .end local v2           #to:Lcom/google/android/apps/translate/Language;
    .end local v3           #inputText:Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->getFromLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getToLanguageGivenFromLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v1

    .line 473
    .restart local v1       #from:Lcom/google/android/apps/translate/Language;
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->getToLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v2

    .restart local v2       #to:Lcom/google/android/apps/translate/Language;
    goto :goto_2
.end method

.method public getInputText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 786
    const-string v1, "TranslateHelper"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getInputText mInputPanel is null? "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->getInputText()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 786
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 787
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public isInEditingMode()Z
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->isInEditingMode()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 743
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-nez p3, :cond_1

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 747
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    .line 748
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/translate/translation/InputPanel;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)Z
    .locals 3
    .parameter "newConfig"

    .prologue
    const/4 v2, 0x0

    .line 847
    const-string v0, "TranslateHelper"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-nez v0, :cond_1

    .line 861
    :cond_0
    :goto_0
    return v2

    .line 851
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 853
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->onConfigurationChanged(Landroid/content/res/Configuration;)Z

    goto :goto_0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .parameter "item"

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 865
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 866
    .local v1, id:I
    sget v6, Lcom/google/android/apps/translate/R$id;->context_menu_translate_input_text:I

    if-ne v1, v6, :cond_0

    .line 867
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/translate/translation/InputPanel;->setLanguageList(Lcom/google/android/apps/translate/Languages;)V

    .line 868
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v7, v7, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v8, v8, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {v6, v7, v8, v5, v5}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 873
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v6, v6, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_SRC_TRANSLATE_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v5, v6, v4}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    .line 922
    :goto_0
    return v4

    .line 877
    :cond_0
    sget v6, Lcom/google/android/apps/translate/R$id;->context_menu_translate_translation:I

    if-ne v1, v6, :cond_1

    .line 878
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/translate/translation/InputPanel;->setLanguageList(Lcom/google/android/apps/translate/Languages;)V

    .line 879
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getTranslation()Ljava/lang/String;

    move-result-object v2

    .line 880
    .local v2, inputText:Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v6, v2}, Lcom/google/android/apps/translate/translation/InputPanel;->setInputText(Ljava/lang/String;)V

    .line 881
    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v7, v7, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p0, v7, v5}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getFromLanguageGivenToLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v8, v8, Lcom/google/android/apps/translate/translation/TranslateEntry;->fromLanguage:Lcom/google/android/apps/translate/Language;

    invoke-virtual {p0, v8, v5}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getToLanguageGivenFromLanguage(Lcom/google/android/apps/translate/Language;Z)Lcom/google/android/apps/translate/Language;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v5, v5}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    .line 886
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    const-string v6, ""

    invoke-virtual {v5, v2, v6}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    invoke-static {}, Lcom/google/android/apps/translate/UserActivityMgr;->get()Lcom/google/android/apps/translate/UserActivityMgr;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;->CHIPVIEW_TRG_TRANSLATE_CLICKS:Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;

    invoke-virtual {v5, v6, v4}, Lcom/google/android/apps/translate/UserActivityMgr;->incrementIntervalCount(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;I)Lcom/google/android/apps/translate/UserActivityMgr;

    goto :goto_0

    .line 890
    .end local v2           #inputText:Ljava/lang/String;
    :cond_1
    sget v6, Lcom/google/android/apps/translate/R$id;->context_menu_copy_input_text:I

    if-ne v1, v6, :cond_2

    .line 891
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v6}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->INPUT_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/translate/Util;->copyToClipBoard(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    .line 893
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    sget v6, Lcom/google/android/apps/translate/R$string;->toast_message_copy_input_text:I

    invoke-static {v5, v6}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;I)V

    goto :goto_0

    .line 895
    :cond_2
    sget v6, Lcom/google/android/apps/translate/R$id;->context_menu_copy:I

    if-ne v1, v6, :cond_3

    .line 896
    new-instance v3, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Lcom/google/android/apps/translate/history/Entry;)V

    .line 898
    .local v3, results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/translate/Util;->copyToClipBoard(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    .line 899
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    sget v6, Lcom/google/android/apps/translate/R$string;->toast_message_copy:I

    invoke-static {v5, v6}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;I)V

    goto :goto_0

    .line 901
    .end local v3           #results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    :cond_3
    sget v6, Lcom/google/android/apps/translate/R$id;->context_menu_share:I

    if-ne v1, v6, :cond_5

    .line 902
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 903
    .local v0, entry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v5, :cond_4

    .line 904
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v7, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v7}, Lcom/google/android/apps/translate/translation/InputPanel;->getSelectedChipView()Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    move-result-object v7

    invoke-static {v5, v6, v0, v7}, Lcom/google/android/apps/translate/translation/TranslateHelper;->performChipActionShare(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    goto/16 :goto_0

    .line 907
    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    sget-object v7, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->NONE:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v5, v6, v0, v7}, Lcom/google/android/apps/translate/translation/TranslateHelper;->performChipActionShare(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Lcom/google/android/apps/translate/history/Entry;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    goto/16 :goto_0

    .line 911
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_5
    sget v6, Lcom/google/android/apps/translate/R$id;->context_menu_search_input_text:I

    if-ne v1, v6, :cond_6

    .line 912
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 913
    .restart local v0       #entry:Lcom/google/android/apps/translate/history/Entry;
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/Entry;->getInputText()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->INPUT_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/translate/Util;->searchTextOnWeb(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    goto/16 :goto_0

    .line 915
    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    :cond_6
    sget v6, Lcom/google/android/apps/translate/R$id;->context_menu_search_translation:I

    if-ne v1, v6, :cond_7

    .line 916
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 917
    .restart local v0       #entry:Lcom/google/android/apps/translate/history/Entry;
    new-instance v3, Lcom/google/android/apps/translate/Translate$TranslateResult;

    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v5}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/android/apps/translate/Translate$TranslateResult;-><init>(Lcom/google/android/apps/translate/history/Entry;)V

    .line 919
    .restart local v3       #results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    iget-object v5, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/Translate$TranslateResult;->getTranslateText()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->TRANSLATION_TEXT:Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/translate/Util;->searchTextOnWeb(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/translate/translation/ChipView$ChipPart;)V

    goto/16 :goto_0

    .end local v0           #entry:Lcom/google/android/apps/translate/history/Entry;
    .end local v3           #results:Lcom/google/android/apps/translate/Translate$TranslateResult;
    :cond_7
    move v4, v5

    .line 922
    goto/16 :goto_0
.end method

.method public onCreate(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Landroid/view/View;Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;)V
    .locals 5
    .parameter "activity"
    .parameter "languages"
    .parameter "view"
    .parameter "translateFragmentWrapper"

    .prologue
    const/4 v4, 0x1

    .line 74
    const-string v1, "TranslateHelper"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;->onCreate(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;)V

    .line 76
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setVolumeControlStream(I)V

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 78
    iput-object p3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mTranslatePanel:Landroid/view/View;

    .line 79
    iput-object p4, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mTranslateFragmentWrapper:Lcom/google/android/apps/translate/translation/TranslateFragmentWrapper;

    .line 80
    const/4 v0, 0x0

    .line 81
    .local v0, doTranslate:Z
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-nez v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->readIntentData(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v1, :cond_0

    .line 88
    const/4 v0, 0x1

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-nez v1, :cond_1

    .line 92
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-static {v1, v2}, Lcom/google/android/apps/translate/Profile;->getLastTranslation(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    .line 94
    :cond_1
    if-eqz v0, :cond_2

    .line 95
    invoke-virtual {p0, v4, v4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setInputPanel(ZZ)V

    .line 103
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translate/TranslateApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/TranslateApplication;->resetAppCsiTimer()V

    .line 104
    return-void

    .line 97
    :cond_2
    new-instance v1, Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mTranslatePanel:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/google/android/apps/translate/translation/InputPanel;-><init>(Landroid/app/Activity;Lcom/google/android/apps/translate/Languages;Landroid/view/View;Lcom/google/android/apps/translate/translation/TranslateHelper;)V

    iput-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    .line 964
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    sget v2, Lcom/google/android/apps/translate/R$menu;->history_activity_context_menu:I

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 965
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v1, :cond_0

    .line 966
    sget-object v1, Lcom/google/android/apps/translate/translation/TranslateHelper$6;->$SwitchMap$com$google$android$apps$translate$translation$ChipView$ChipPart:[I

    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/translation/InputPanel;->getSelectedChipView()Lcom/google/android/apps/translate/translation/ChipView$ChipPart;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/translate/translation/ChipView$ChipPart;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 992
    :cond_0
    invoke-interface {p1}, Landroid/view/ContextMenu;->clear()V

    .line 993
    :goto_0
    return-void

    .line 968
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v1, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 969
    sget v1, Lcom/google/android/apps/translate/R$id;->context_menu_translate_translation:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 970
    sget v1, Lcom/google/android/apps/translate/R$id;->context_menu_remove:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 971
    sget v1, Lcom/google/android/apps/translate/R$id;->context_menu_copy:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 972
    sget v1, Lcom/google/android/apps/translate/R$id;->context_menu_search_translation:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 973
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v1, v1, Lcom/google/android/apps/translate/translation/TranslateEntry;->inputText:Ljava/lang/String;

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_0

    .line 978
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->isInvalid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 979
    sget v1, Lcom/google/android/apps/translate/R$id;->context_menu_translate_input_text:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 980
    sget v1, Lcom/google/android/apps/translate/R$id;->context_menu_remove:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 981
    sget v1, Lcom/google/android/apps/translate/R$id;->context_menu_copy_input_text:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 982
    sget v1, Lcom/google/android/apps/translate/R$id;->context_menu_search_input_text:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 983
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/translate/translation/TranslateEntry;->toNewEntry()Lcom/google/android/apps/translate/history/Entry;

    move-result-object v0

    .line 984
    .local v0, entry:Lcom/google/android/apps/translate/history/Entry;
    invoke-virtual {v0}, Lcom/google/android/apps/translate/history/Entry;->getTranslation()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_0

    .line 966
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 390
    const-string v0, "TranslateHelper"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->onDestroy()V

    .line 394
    :cond_0
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x0

    .line 1017
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/translate/translation/InputPanel;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1018
    const/4 v0, 0x1

    .line 1025
    :cond_0
    :goto_0
    return v0

    .line 1020
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/apps/translate/Util;->isFloatingActivity(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public onLanguagesChanged()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 807
    const-string v3, "TranslateHelper"

    const-string v4, "onLanguagesChanged"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    if-nez v3, :cond_1

    .line 809
    const-string v3, "TranslateHelper"

    const-string v4, "onLanguagesChanged not changed!"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    :cond_0
    :goto_0
    return-void

    .line 815
    :cond_1
    invoke-static {}, Lcom/google/android/apps/translate/LanguagesFactory;->get()Lcom/google/android/apps/translate/LanguagesFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/translate/LanguagesFactory;->getLanguages(Landroid/content/Context;Ljava/util/Locale;)Lcom/google/android/apps/translate/Languages;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    .line 816
    const/4 v0, 0x0

    .line 817
    .local v0, newFrom:Lcom/google/android/apps/translate/Language;
    const/4 v1, 0x0

    .line 818
    .local v1, newTo:Lcom/google/android/apps/translate/Language;
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/translation/TranslateEntry;->isInvalid()Z

    move-result v3

    if-nez v3, :cond_2

    .line 820
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/translate/translation/TranslateEntry;->getFromLanguageForLanguagePicker(Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/Language;

    move-result-object v0

    .line 821
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    iget-object v1, v3, Lcom/google/android/apps/translate/translation/TranslateEntry;->toLanguage:Lcom/google/android/apps/translate/Language;

    .line 823
    :cond_2
    if-eqz v0, :cond_3

    if-nez v1, :cond_4

    .line 824
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Profile;->getLanguagePair(Landroid/content/Context;Lcom/google/android/apps/translate/Languages;)[Lcom/google/android/apps/translate/Language;

    move-result-object v2

    .line 825
    .local v2, pair:[Lcom/google/android/apps/translate/Language;
    aget-object v3, v2, v6

    if-eqz v3, :cond_5

    aget-object v3, v2, v7

    if-eqz v3, :cond_5

    .line 826
    aget-object v0, v2, v6

    .line 827
    aget-object v1, v2, v7

    .line 833
    .end local v2           #pair:[Lcom/google/android/apps/translate/Language;
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v3, :cond_0

    .line 834
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/translate/translation/InputPanel;->setLanguageList(Lcom/google/android/apps/translate/Languages;)V

    .line 835
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v3, v0, v1, v6, v6}, Lcom/google/android/apps/translate/translation/InputPanel;->languagePairSelected(Lcom/google/android/apps/translate/Language;Lcom/google/android/apps/translate/Language;ZZ)V

    goto :goto_0

    .line 829
    .restart local v2       #pair:[Lcom/google/android/apps/translate/Language;
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/Languages;->getDefaultFromLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v0

    .line 830
    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-virtual {v3}, Lcom/google/android/apps/translate/Languages;->getDefaultToLanguage()Lcom/google/android/apps/translate/Language;

    move-result-object v1

    goto :goto_1
.end method

.method onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .parameter "intent"

    .prologue
    const/4 v2, 0x1

    .line 115
    const-string v0, "TranslateHelper"

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mLanguageList:Lcom/google/android/apps/translate/Languages;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/translate/translation/TranslateHelper;->readIntentData(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/apps/translate/Languages;)Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setTranslateEntry(Lcom/google/android/apps/translate/translation/TranslateEntry;ZZZ)V

    .line 117
    return-void
.end method

.method onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 415
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 416
    .local v0, id:I
    sget v2, Lcom/google/android/apps/translate/R$id;->menu_history:I

    if-ne v0, v2, :cond_0

    .line 417
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->openHistoryActivity(Landroid/app/Activity;)V

    .line 437
    :goto_0
    return v1

    .line 419
    :cond_0
    sget v2, Lcom/google/android/apps/translate/R$id;->menu_favorite:I

    if-ne v0, v2, :cond_1

    .line 420
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->openFavoriteActivity(Landroid/app/Activity;)V

    goto :goto_0

    .line 422
    :cond_1
    sget v2, Lcom/google/android/apps/translate/R$id;->menu_conversation_mode:I

    if-ne v0, v2, :cond_2

    .line 423
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/google/android/apps/translate/translation/TranslateHelper;->getInitialTranslationEntry()Lcom/google/android/apps/translate/translation/TranslateEntry;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Util;->openConversationActivity(Landroid/app/Activity;Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    goto :goto_0

    .line 425
    :cond_2
    sget v2, Lcom/google/android/apps/translate/R$id;->menu_sms_translation:I

    if-ne v0, v2, :cond_4

    .line 426
    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/apps/translate/Util;->openSmsTranslationActivity(Landroid/app/Activity;)V

    .line 437
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 427
    :cond_4
    sget v2, Lcom/google/android/apps/translate/R$id;->menu_feedback:I

    if-ne v0, v2, :cond_5

    .line 428
    invoke-static {}, Lcom/google/android/apps/translate/SdkVersionWrapper;->getWrapper()Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/translate/SdkVersionWrapper$WrapperBase;->sendFeedback(Landroid/app/Activity;Z)V

    goto :goto_0

    .line 430
    :cond_5
    sget v2, Lcom/google/android/apps/translate/R$id;->menu_settings:I

    if-ne v0, v2, :cond_6

    .line 431
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    const-class v5, Lcom/google/android/apps/translate/SettingsActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 433
    :cond_6
    const v2, 0x102002c

    if-ne v0, v2, :cond_3

    .line 434
    iget-object v2, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->openHomeActivity(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 770
    invoke-super {p0}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;->onPause()V

    .line 771
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    .line 772
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->onPause()V

    .line 774
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 761
    const-string v0, "TranslateHelper"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    invoke-super {p0}, Lcom/google/android/apps/translate/translation/BaseTranslateHelper;->onResume()V

    .line 763
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translate/translation/InputPanel;->onResume(Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 766
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/Translate;->setAcceptLanguage(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/translation/InputPanel;->onStart()V

    .line 125
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Profile;->setLastTranslation(Landroid/content/Context;Lcom/google/android/apps/translate/translation/TranslateEntry;)V

    .line 131
    :cond_0
    return-void
.end method

.method public setCurrentTranslation(Lcom/google/android/apps/translate/translation/TranslateEntry;)V
    .locals 4
    .parameter "translateEntry"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 698
    const-string v0, "TranslateHelper"

    const-string v1, "setCurrentTranslation"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    invoke-virtual {p0, p1, v2, v2, v3}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setTranslateEntry(Lcom/google/android/apps/translate/translation/TranslateEntry;ZZZ)V

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    iget-object v1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/translate/translation/InputPanel;->setTranslationResult(Lcom/google/android/apps/translate/translation/TranslateEntry;Z)V

    .line 701
    return-void
.end method

.method setInputPanel(ZZ)V
    .locals 2
    .parameter "doTranslate"
    .parameter "setInputText"

    .prologue
    .line 704
    const-string v0, "TranslateHelper"

    const-string v1, "setInputPanel"

    invoke-static {v0, v1}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/translate/translation/TranslateHelper$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/translate/translation/TranslateHelper$5;-><init>(Lcom/google/android/apps/translate/translation/TranslateHelper;ZZ)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 737
    return-void
.end method

.method public setLanguageList(Lcom/google/android/apps/translate/Languages;)V
    .locals 1
    .parameter "languages"

    .prologue
    .line 795
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->setLanguageList(Lcom/google/android/apps/translate/Languages;)V

    .line 796
    return-void
.end method

.method public setTextAndDoTranslate(Ljava/lang/String;)V
    .locals 1
    .parameter "inputText"

    .prologue
    .line 843
    iget-object v0, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mInputPanel:Lcom/google/android/apps/translate/translation/InputPanel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/translation/InputPanel;->setTextAndDoTranslate(Ljava/lang/String;)V

    .line 844
    return-void
.end method

.method declared-synchronized setTranslateEntry(Lcom/google/android/apps/translate/translation/TranslateEntry;ZZZ)V
    .locals 1
    .parameter "translateEntry"
    .parameter "updateInputPanel"
    .parameter "doTranslate"
    .parameter "setInputText"

    .prologue
    .line 1001
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/translate/translation/TranslateHelper;->mCurrentTranslation:Lcom/google/android/apps/translate/translation/TranslateEntry;

    .line 1002
    if-eqz p2, :cond_0

    .line 1003
    invoke-virtual {p0, p3, p4}, Lcom/google/android/apps/translate/translation/TranslateHelper;->setInputPanel(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1005
    :cond_0
    monitor-exit p0

    return-void

    .line 1001
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
