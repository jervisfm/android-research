.class public Lcom/google/android/apps/translate/tts/DonutTtsWrapper;
.super Ljava/lang/Object;
.source "DonutTtsWrapper.java"


# static fields
.field private static final ESPEAK_ENGINE_PACKAGE:Ljava/lang/String; = "com.marvin.espeak"

#the value of this static final field might be set in the static constructor
.field private static final NEW_PLATFORM:Z = false

.field private static final PICO_ENGINE_PACKAGE:Ljava/lang/String; = "com.svox.pico"

.field private static final PICO_LANG_SET:Ljava/util/HashSet; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TTS_EXTENDED_PACKAGE:Ljava/lang/String; = "com.google.tts"


# instance fields
.field private mCallback:Lcom/google/android/apps/translate/tts/DonutTtsCallback;

.field private final mEspeakEngine:Ljava/lang/String;

.field private final mIsEspeakEngineInstalled:Z

.field private final mIsTtsExtendedInstalled:Z

.field private mTts:Lcom/google/tts/TextToSpeechBeta;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x8

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->NEW_PLATFORM:Z

    .line 27
    invoke-static {}, Lcom/google/android/apps/translate/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->PICO_LANG_SET:Ljava/util/HashSet;

    .line 30
    invoke-static {}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->setPicoLanguagesMap()V

    .line 42
    :try_start_0
    const-string v1, "android.speech.tts.TextToSpeech"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    return-void

    .line 25
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/translate/tts/DonutTtsCallback;)V
    .locals 2
    .parameter "context"
    .parameter "callback"

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mCallback:Lcom/google/android/apps/translate/tts/DonutTtsCallback;

    .line 56
    new-instance v0, Lcom/google/tts/TextToSpeechBeta;

    new-instance v1, Lcom/google/android/apps/translate/tts/DonutTtsWrapper$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper$1;-><init>(Lcom/google/android/apps/translate/tts/DonutTtsWrapper;)V

    invoke-direct {v0, p1, v1}, Lcom/google/tts/TextToSpeechBeta;-><init>(Landroid/content/Context;Lcom/google/tts/TextToSpeechBeta$OnInitListener;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    .line 64
    sget-boolean v0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->NEW_PLATFORM:Z

    if-eqz v0, :cond_0

    const-string v0, "com.marvin.espeak"

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mEspeakEngine:Ljava/lang/String;

    .line 65
    invoke-static {p1}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isTtsExtendedInstalled(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mIsTtsExtendedInstalled:Z

    .line 66
    invoke-static {p1}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isEspeakEngineInstalled(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mIsEspeakEngineInstalled:Z

    .line 67
    return-void

    .line 64
    :cond_0
    const-string v0, "com.google.tts"

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/tts/DonutTtsWrapper;)Lcom/google/android/apps/translate/tts/DonutTtsCallback;
    .locals 1
    .parameter "x0"

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mCallback:Lcom/google/android/apps/translate/tts/DonutTtsCallback;

    return-object v0
.end method

.method public static checkAvailable()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public static isEspeakEngineInstalled(Landroid/content/Context;)Z
    .locals 5
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    .line 187
    sget-boolean v3, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->NEW_PLATFORM:Z

    if-nez v3, :cond_0

    .line 195
    :goto_0
    return v2

    .line 189
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 191
    .local v1, pm:Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.marvin.espeak"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    const/4 v2, 0x1

    goto :goto_0

    .line 192
    :catch_0
    move-exception v0

    .line 193
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private isLanguageAvailable(Ljava/util/Locale;Ljava/lang/String;)Z
    .locals 2
    .parameter "locale"
    .parameter "engine"

    .prologue
    .line 168
    iget-object v1, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v1, p2}, Lcom/google/tts/TextToSpeechBeta;->setEngineByPackageNameExtended(Ljava/lang/String;)I

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v1, p1}, Lcom/google/tts/TextToSpeechBeta;->setLanguage(Ljava/util/Locale;)I

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v1, p1}, Lcom/google/tts/TextToSpeechBeta;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v0

    .line 171
    .local v0, languageSupport:I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isPicoSupportedLanguage(Ljava/lang/String;)Z
    .locals 1
    .parameter "language"

    .prologue
    .line 164
    sget-object v0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->PICO_LANG_SET:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isTtsExtendedInstalled(Landroid/content/Context;)Z
    .locals 1
    .parameter "context"

    .prologue
    .line 179
    sget-boolean v0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->NEW_PLATFORM:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/tts/TextToSpeechBeta;->isInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setPicoLanguagesMap()V
    .locals 5

    .prologue
    .line 158
    sget-object v0, Lcom/google/android/apps/translate/Constants;->PICO_SUPPORTED_LANGUAGES:[Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 159
    .local v2, l:Ljava/lang/String;
    sget-object v4, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->PICO_LANG_SET:Ljava/util/HashSet;

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 158
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 161
    .end local v2           #l:Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public isLanguageAvailable(Ljava/util/Locale;)Z
    .locals 4
    .parameter "locale"

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 128
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    if-nez v3, :cond_1

    .line 154
    :cond_0
    :goto_0
    return v1

    .line 130
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mIsTtsExtendedInstalled:Z

    if-nez v3, :cond_2

    sget-boolean v3, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->NEW_PLATFORM:Z

    if-eqz v3, :cond_7

    .line 131
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v3}, Lcom/google/tts/TextToSpeechBeta;->getDefaultEngineExtended()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, defaultEngine:Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 134
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v3}, Lcom/google/tts/TextToSpeechBeta;->areDefaultsEnforcedExtended()Z

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    .line 135
    goto :goto_0

    .line 136
    :cond_3
    const-string v3, "com.svox.pico"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mEspeakEngine:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isLanguageAvailable(Ljava/util/Locale;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    .line 140
    goto :goto_0

    .line 144
    :cond_4
    const-string v3, "com.svox.pico"

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isLanguageAvailable(Ljava/util/Locale;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    .line 145
    goto :goto_0

    .line 146
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mIsTtsExtendedInstalled:Z

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mIsEspeakEngineInstalled:Z

    if-eqz v2, :cond_0

    .line 147
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mEspeakEngine:Ljava/lang/String;

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isLanguageAvailable(Ljava/util/Locale;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 149
    .end local v0           #defaultEngine:Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isPicoSupportedLanguage(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 151
    goto :goto_0
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v0}, Lcom/google/tts/TextToSpeechBeta;->shutdown()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    .line 77
    :cond_0
    return-void
.end method

.method public speak(Ljava/util/Locale;Ljava/lang/String;I)V
    .locals 5
    .parameter "locale"
    .parameter "text"
    .parameter "queueMode"

    .prologue
    const/4 v4, 0x0

    .line 83
    iget-boolean v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mIsTtsExtendedInstalled:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->NEW_PLATFORM:Z

    if-eqz v2, :cond_5

    .line 84
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v2}, Lcom/google/tts/TextToSpeechBeta;->getDefaultEngineExtended()Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, defaultEngine:Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 86
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v2}, Lcom/google/tts/TextToSpeechBeta;->areDefaultsEnforcedExtended()Z

    move-result v1

    .line 90
    .local v1, defaultFocred:Z
    if-nez v1, :cond_1

    const-string v2, "com.svox.pico"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mEspeakEngine:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 93
    :cond_1
    if-nez v1, :cond_2

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isLanguageAvailable(Ljava/util/Locale;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 94
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v2, v0}, Lcom/google/tts/TextToSpeechBeta;->setEngineByPackageNameExtended(Ljava/lang/String;)I

    .line 95
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v2, p1}, Lcom/google/tts/TextToSpeechBeta;->setLanguage(Ljava/util/Locale;)I

    .line 96
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v2, p2, p3, v4}, Lcom/google/tts/TextToSpeechBeta;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 115
    .end local v0           #defaultEngine:Ljava/lang/String;
    .end local v1           #defaultFocred:Z
    :cond_3
    :goto_0
    return-void

    .line 103
    .restart local v0       #defaultEngine:Ljava/lang/String;
    :cond_4
    const-string v2, "com.svox.pico"

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isLanguageAvailable(Ljava/util/Locale;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 104
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    const-string v3, "com.svox.pico"

    invoke-virtual {v2, v3}, Lcom/google/tts/TextToSpeechBeta;->setEngineByPackageNameExtended(Ljava/lang/String;)I

    .line 113
    .end local v0           #defaultEngine:Ljava/lang/String;
    :cond_5
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v2, p1}, Lcom/google/tts/TextToSpeechBeta;->setLanguage(Ljava/util/Locale;)I

    .line 114
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v2, p2, p3, v4}, Lcom/google/tts/TextToSpeechBeta;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_0

    .line 105
    .restart local v0       #defaultEngine:Ljava/lang/String;
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mIsTtsExtendedInstalled:Z

    if-nez v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mIsEspeakEngineInstalled:Z

    if-eqz v2, :cond_3

    .line 106
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    iget-object v3, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mEspeakEngine:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/tts/TextToSpeechBeta;->setEngineByPackageNameExtended(Ljava/lang/String;)I

    goto :goto_1
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->mTts:Lcom/google/tts/TextToSpeechBeta;

    invoke-virtual {v0}, Lcom/google/tts/TextToSpeechBeta;->stop()I

    .line 122
    return-void
.end method
