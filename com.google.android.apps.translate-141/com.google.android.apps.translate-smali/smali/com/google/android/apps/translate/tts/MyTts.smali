.class public Lcom/google/android/apps/translate/tts/MyTts;
.super Ljava/lang/Object;
.source "MyTts.java"

# interfaces
.implements Lcom/google/android/apps/translate/tts/DonutTtsCallback;


# static fields
.field private static final TAG:Ljava/lang/String; = "MyTts"

.field private static sHasDonutTts:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDonutCallback:Lcom/google/android/apps/translate/tts/DonutTtsCallback;

.field private mDonutTts:Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

.field private mNetworkTts:Lcom/google/android/apps/translate/tts/NetworkTts;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->checkAvailable()V

    .line 37
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/translate/tts/MyTts;->sHasDonutTts:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    .local v0, t:Ljava/lang/Throwable;
    :goto_0
    return-void

    .line 38
    .end local v0           #t:Ljava/lang/Throwable;
    :catch_0
    move-exception v0

    .line 39
    .restart local v0       #t:Ljava/lang/Throwable;
    const/4 v1, 0x0

    sput-boolean v1, Lcom/google/android/apps/translate/tts/MyTts;->sHasDonutTts:Z

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/translate/tts/MyTts;->init()V

    .line 49
    return-void
.end method

.method public static getTtsSpeakingMessage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "context"
    .parameter "longLangName"

    .prologue
    .line 109
    sget v0, Lcom/google/android/apps/translate/R$string;->msg_speaking:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isNativeTtsAvailable(Ljava/util/Locale;)Z
    .locals 1
    .parameter "locale"

    .prologue
    .line 136
    sget-boolean v0, Lcom/google/android/apps/translate/tts/MyTts;->sHasDonutTts:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutTts:Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->isLanguageAvailable(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public init()V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/apps/translate/tts/NetworkTts;

    invoke-direct {v0}, Lcom/google/android/apps/translate/tts/NetworkTts;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mNetworkTts:Lcom/google/android/apps/translate/tts/NetworkTts;

    .line 54
    sget-boolean v0, Lcom/google/android/apps/translate/tts/MyTts;->sHasDonutTts:Z

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "new Donut TTS"

    invoke-static {v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 56
    new-instance v0, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

    iget-object v1, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;-><init>(Landroid/content/Context;Lcom/google/android/apps/translate/tts/DonutTtsCallback;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutTts:Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

    .line 58
    :cond_0
    return-void
.end method

.method public isLanguageAvailable(Ljava/util/Locale;)Z
    .locals 2
    .parameter "locale"

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/translate/Profile;->getPreferNetworkTts(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mNetworkTts:Lcom/google/android/apps/translate/tts/NetworkTts;

    invoke-static {p1}, Lcom/google/android/apps/translate/tts/NetworkTts;->isLanguageAvailable(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    .line 132
    :goto_0
    return v0

    .line 131
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "is language available (Donut) for locale: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;)V

    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/tts/MyTts;->isNativeTtsAvailable(Ljava/util/Locale;)Z

    move-result v0

    goto :goto_0
.end method

.method public onInit(Z)V
    .locals 1
    .parameter "succeed"

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutCallback:Lcom/google/android/apps/translate/tts/DonutTtsCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutCallback:Lcom/google/android/apps/translate/tts/DonutTtsCallback;

    invoke-interface {v0, p1}, Lcom/google/android/apps/translate/tts/DonutTtsCallback;->onInit(Z)V

    .line 142
    :cond_0
    return-void
.end method

.method public prefetch(Ljava/util/Locale;Ljava/lang/String;)V
    .locals 2
    .parameter "locale"
    .parameter "text"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/translate/Profile;->getPreferNetworkTts(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mNetworkTts:Lcom/google/android/apps/translate/tts/NetworkTts;

    iget-object v1, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/translate/tts/NetworkTts;->prefetch(Landroid/content/Context;Ljava/util/Locale;Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/google/android/apps/translate/tts/DonutTtsCallback;)V
    .locals 0
    .parameter "donutCallback"

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutCallback:Lcom/google/android/apps/translate/tts/DonutTtsCallback;

    .line 74
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 64
    sget-boolean v0, Lcom/google/android/apps/translate/tts/MyTts;->sHasDonutTts:Z

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutTts:Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutTts:Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->shutdown()V

    .line 67
    :cond_0
    return-void
.end method

.method public speak(Landroid/app/Activity;Lcom/google/android/apps/translate/Language;Ljava/lang/String;Lcom/google/android/apps/translate/tts/NetworkTts$Callback;)V
    .locals 4
    .parameter "activity"
    .parameter "lang"
    .parameter "text"
    .parameter "c"

    .prologue
    .line 81
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 82
    .local v0, am:Landroid/media/AudioManager;
    if-eqz v0, :cond_2

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    if-nez v2, :cond_2

    if-eqz p1, :cond_2

    .line 84
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/translate/R$string;->msg_tts_volume_off:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    .line 92
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/translate/Util;->languageShortNameToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 94
    .local v1, locale:Ljava/util/Locale;
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/translate/Profile;->getPreferNetworkTts(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v1}, Lcom/google/android/apps/translate/tts/NetworkTts;->isLanguageAvailable(Ljava/util/Locale;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 95
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutTts:Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutTts:Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->stop()V

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/MyTts;->mNetworkTts:Lcom/google/android/apps/translate/tts/NetworkTts;

    iget-object v3, p0, Lcom/google/android/apps/translate/tts/MyTts;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v1, p3, p4}, Lcom/google/android/apps/translate/tts/NetworkTts;->speak(Landroid/content/Context;Ljava/util/Locale;Ljava/lang/String;Lcom/google/android/apps/translate/tts/NetworkTts$Callback;)V

    .line 105
    :cond_1
    :goto_1
    return-void

    .line 88
    .end local v1           #locale:Ljava/util/Locale;
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/translate/Language;->getLongName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/translate/tts/MyTts;->getTtsSpeakingMessage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/translate/Util;->showShortToastMessage(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 101
    .restart local v1       #locale:Ljava/util/Locale;
    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/apps/translate/tts/MyTts;->isNativeTtsAvailable(Ljava/util/Locale;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 102
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/MyTts;->mNetworkTts:Lcom/google/android/apps/translate/tts/NetworkTts;

    invoke-virtual {v2}, Lcom/google/android/apps/translate/tts/NetworkTts;->stop()V

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/MyTts;->mDonutTts:Lcom/google/android/apps/translate/tts/DonutTtsWrapper;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, p3, v3}, Lcom/google/android/apps/translate/tts/DonutTtsWrapper;->speak(Ljava/util/Locale;Ljava/lang/String;I)V

    goto :goto_1
.end method
