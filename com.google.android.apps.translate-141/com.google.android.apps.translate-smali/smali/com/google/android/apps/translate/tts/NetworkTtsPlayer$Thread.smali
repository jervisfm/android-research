.class final Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;
.super Ljava/lang/Thread;
.source "NetworkTtsPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Thread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)V
    .locals 1
    .parameter

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    .line 214
    const-string v0, "NetworkTtsPlayer"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 215
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 220
    :goto_0
    const/4 v1, 0x0

    .line 222
    .local v1, cmd:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #getter for: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$000(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Ljava/util/LinkedList;

    move-result-object v3

    monitor-enter v3

    .line 223
    :try_start_0
    const-string v2, "NetworkTtsPlayer"

    const-string v4, "RemoveFirst"

    invoke-static {v2, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #getter for: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$000(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;

    move-object v1, v0

    .line 225
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    iget v2, v1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->code:I

    packed-switch v2, :pswitch_data_0

    .line 247
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #getter for: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$000(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Ljava/util/LinkedList;

    move-result-object v3

    monitor-enter v3

    .line 248
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #getter for: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$000(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 253
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    const/4 v4, 0x0

    #setter for: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mThread:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;
    invoke-static {v2, v4}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$402(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;)Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

    .line 254
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #calls: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->releaseWakeLock()V
    invoke-static {v2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$500(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)V

    .line 255
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 225
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 229
    :pswitch_0
    const-string v2, "NetworkTtsPlayer"

    const-string v3, "PLAY"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-string v2, "internalPlayStart"

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->t(Ljava/lang/String;)V

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #calls: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->startSound(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$100(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V

    .line 232
    const-string v2, "internalPlayEnd"

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->t(Ljava/lang/String;)V

    .line 233
    const-string v2, "playEnd"

    invoke-static {v2}, Lcom/google/android/apps/translate/Logger;->t(Ljava/lang/String;)V

    goto :goto_1

    .line 236
    :pswitch_1
    const-string v2, "NetworkTtsPlayer"

    const-string v3, "STOP"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #getter for: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$200(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #getter for: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$200(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #getter for: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$200(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_1

    .line 242
    :pswitch_2
    const-string v2, "NetworkTtsPlayer"

    const-string v3, "PREFETCH"

    invoke-static {v2, v3}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->this$0:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;

    #calls: Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->prefetchSound(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->access$300(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V

    goto :goto_1

    .line 257
    :cond_1
    :try_start_3
    monitor-exit v3

    goto/16 :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 227
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
