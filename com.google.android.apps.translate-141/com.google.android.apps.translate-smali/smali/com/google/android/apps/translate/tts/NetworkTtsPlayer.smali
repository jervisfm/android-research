.class public Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;
.super Ljava/lang/Object;
.source "NetworkTtsPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$1;,
        Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;,
        Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;,
        Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException;
    }
.end annotation


# static fields
.field private static final NETWORK_BUFFER:[B = null

.field private static final PLAY:I = 0x1

.field private static final PREFETCH:I = 0x3

.field private static final STOP:I = 0x2

.field private static final TAG:Ljava/lang/String; = "NetworkTtsPlayer"

.field private static final TTS_AUDIO_FILE:Ljava/lang/String; = "tts.dat"

.field public static final TTS_TOO_LONG:I = 0x1

.field public static final TTS_UNAVAILABLE:I = 0x0

.field private static final USER_AGENT:Ljava/lang/String; = "AndroidTranslate"


# instance fields
.field private mCmdQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;",
            ">;"
        }
    .end annotation
.end field

.field private mLastUri:Landroid/net/Uri;

.field private mPlayer:Landroid/media/MediaPlayer;

.field private mPrefetchedUri:Landroid/net/Uri;

.field private mPrefetchingPlayer:Landroid/media/MediaPlayer;

.field private mState:I

.field private mThread:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0x200

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->NETWORK_BUFFER:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mState:I

    .line 78
    invoke-static {}, Lcom/google/android/apps/translate/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;

    .line 212
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Ljava/util/LinkedList;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->startSound(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)Landroid/media/MediaPlayer;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->prefetchSound(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;)Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mThread:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->releaseWakeLock()V

    return-void
.end method

.method private acquireWakeLock()V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 366
    :cond_0
    return-void
.end method

.method private enqueueLocked(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V
    .locals 1
    .parameter "cmd"

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mThread:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

    if-nez v0, :cond_0

    .line 333
    invoke-direct {p0}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->acquireWakeLock()V

    .line 334
    new-instance v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

    invoke-direct {v0, p0}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;-><init>(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;)V

    iput-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mThread:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mThread:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

    invoke-virtual {v0}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;->start()V

    .line 337
    :cond_0
    return-void
.end method

.method private prefetchSound(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V
    .locals 5
    .parameter "cmd"

    .prologue
    .line 150
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mLastUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mLastUri:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchingPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchedUri:Landroid/net/Uri;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchedUri:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 170
    :cond_1
    :goto_0
    return-void

    .line 159
    :cond_2
    :try_start_0
    iget-object v2, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->context:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->prepareNewStream(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    move-result-object v1

    .line 160
    .local v1, player:Landroid/media/MediaPlayer;
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchingPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_3

    .line 161
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchingPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 164
    :cond_3
    iput-object v1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchingPlayer:Landroid/media/MediaPlayer;

    .line 165
    iget-object v2, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    iput-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchedUri:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 167
    .end local v1           #player:Landroid/media/MediaPlayer;
    :catch_0
    move-exception v0

    .line 168
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "NetworkTtsPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error prefetching sound for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/translate/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private prepareNewStream(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;
    .locals 2
    .parameter "context"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    .line 174
    .local v0, player:Landroid/media/MediaPlayer;
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 177
    const-string v1, "streamingStart"

    invoke-static {v1}, Lcom/google/android/apps/translate/Logger;->t(Ljava/lang/String;)V

    .line 178
    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->streamFromServer(Landroid/content/Context;Landroid/media/MediaPlayer;Landroid/net/Uri;)V

    .line 179
    const-string v1, "streamingEnd"

    invoke-static {v1}, Lcom/google/android/apps/translate/Logger;->t(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 182
    return-object v0
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 372
    :cond_0
    return-void
.end method

.method private startSound(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V
    .locals 8
    .parameter "cmd"

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 82
    :try_start_0
    const-string v3, "NetworkTtsPlayer"

    const-string v4, "Starting playback"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_2

    .line 86
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mLastUri:Landroid/net/Uri;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mLastUri:Landroid/net/Uri;

    iget-object v4, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v3

    if-eqz v3, :cond_1

    .line 88
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 89
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->start()V

    .line 90
    const-string v3, "NetworkTtsPlayer"

    const-string v4, "Cache hit"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 95
    .local v0, e:Ljava/lang/Exception;
    :try_start_2
    const-string v3, "NetworkTtsPlayer"

    const-string v4, "Cache replay error"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->release()V

    .line 99
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    .line 103
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchingPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchedUri:Landroid/net/Uri;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchedUri:Landroid/net/Uri;

    iget-object v4, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 105
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_3

    .line 106
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->release()V

    .line 107
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    .line 110
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchingPlayer:Landroid/media/MediaPlayer;

    iput-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    .line 111
    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchedUri:Landroid/net/Uri;

    iput-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mLastUri:Landroid/net/Uri;

    .line 112
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchingPlayer:Landroid/media/MediaPlayer;

    .line 113
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPrefetchedUri:Landroid/net/Uri;

    .line 114
    const-string v3, "NetworkTtsPlayer"

    const-string v4, "Prefetch hit"

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-direct {p0, p1}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->startSound(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V
    :try_end_2
    .catch Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 133
    :catch_1
    move-exception v0

    .line 134
    .local v0, e:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException;
    const-string v3, "NetworkTtsPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TTS not found for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/translate/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 135
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    if-eqz v3, :cond_0

    .line 138
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    invoke-interface {v3, v7}, Lcom/google/android/apps/translate/tts/NetworkTts$Callback;->onError(I)V

    goto :goto_0

    .line 119
    .end local v0           #e:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException;
    :cond_4
    :try_start_3
    new-instance v2, Lcom/google/android/apps/translate/CsiTimer;

    const-string v3, "nts"

    invoke-direct {v2, v3}, Lcom/google/android/apps/translate/CsiTimer;-><init>(Ljava/lang/String;)V

    .line 120
    .local v2, ttsRetrivalTimer:Lcom/google/android/apps/translate/CsiTimer;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "t"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/CsiTimer;->begin([Ljava/lang/String;)V

    .line 122
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    if-eqz v3, :cond_5

    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    invoke-interface {v3}, Lcom/google/android/apps/translate/tts/NetworkTts$Callback;->onPrepare()V

    .line 123
    :cond_5
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->context:Landroid/content/Context;

    iget-object v4, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->prepareNewStream(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    move-result-object v1

    .line 124
    .local v1, player:Landroid/media/MediaPlayer;
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    if-eqz v3, :cond_6

    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    invoke-interface {v3}, Lcom/google/android/apps/translate/tts/NetworkTts$Callback;->onReady()V

    .line 125
    :cond_6
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "t"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/CsiTimer;->end([Ljava/lang/String;)V

    .line 126
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->context:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translate/CsiTimer;->report(Landroid/content/Context;)V

    .line 128
    const-string v3, "NetworkTtsPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Playing "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/translate/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 130
    iput-object v1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mPlayer:Landroid/media/MediaPlayer;

    .line 131
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    iput-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mLastUri:Landroid/net/Uri;
    :try_end_3
    .catch Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 141
    .end local v1           #player:Landroid/media/MediaPlayer;
    .end local v2           #ttsRetrivalTimer:Lcom/google/android/apps/translate/CsiTimer;
    :catch_2
    move-exception v0

    .line 142
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "NetworkTtsPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error loading sound for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/translate/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    if-eqz v3, :cond_0

    .line 144
    iget-object v3, p1, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    invoke-interface {v3, v6}, Lcom/google/android/apps/translate/tts/NetworkTts$Callback;->onError(I)V

    goto/16 :goto_0
.end method

.method private streamFromServer(Landroid/content/Context;Landroid/media/MediaPlayer;Landroid/net/Uri;)V
    .locals 10
    .parameter "context"
    .parameter "player"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 188
    new-instance v3, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    const-string v7, "tts.dat"

    invoke-direct {v3, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 189
    .local v3, ttsFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 191
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 194
    .local v5, ttsOutput:Ljava/io/FileOutputStream;
    const-string v6, "AndroidTranslate"

    invoke-static {v6}, Lcom/google/android/apps/translate/Util;->newHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v6

    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 195
    .local v1, response:Lorg/apache/http/HttpResponse;
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    const/16 v7, 0x194

    if-ne v6, v7, :cond_0

    .line 196
    new-instance v6, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException;

    invoke-direct {v6}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$TtsNotFoundException;-><init>()V

    throw v6

    .line 198
    :cond_0
    invoke-static {v1}, Lcom/google/android/apps/translate/Util;->getResponseInputStream(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    .line 199
    .local v0, input:Ljava/io/InputStream;
    if-eqz v0, :cond_1

    .line 201
    :goto_0
    sget-object v6, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->NETWORK_BUFFER:[B

    invoke-virtual {v0, v6}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, size:I
    const/4 v6, -0x1

    if-le v2, v6, :cond_1

    .line 202
    sget-object v6, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->NETWORK_BUFFER:[B

    invoke-virtual {v5, v6, v9, v2}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_0

    .line 205
    .end local v2           #size:I
    :cond_1
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 208
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 209
    .local v4, ttsInput:Ljava/io/FileInputStream;
    invoke-virtual {v4}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {p2, v6}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 210
    return-void
.end method


# virtual methods
.method public play(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1
    .parameter "context"
    .parameter "uri"

    .prologue
    .line 291
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->play(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/apps/translate/tts/NetworkTts$Callback;)V

    .line 292
    return-void
.end method

.method public play(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/apps/translate/tts/NetworkTts$Callback;)V
    .locals 3
    .parameter "context"
    .parameter "uri"
    .parameter "callback"

    .prologue
    const/4 v2, 0x1

    .line 273
    const-string v1, "playStart"

    invoke-static {v1}, Lcom/google/android/apps/translate/Logger;->t(Ljava/lang/String;)V

    .line 274
    new-instance v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;-><init>(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$1;)V

    .line 275
    .local v0, cmd:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;
    iput v2, v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->code:I

    .line 276
    iput-object p1, v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->context:Landroid/content/Context;

    .line 277
    iput-object p2, v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    .line 278
    iput-object p3, v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->callback:Lcom/google/android/apps/translate/tts/NetworkTts$Callback;

    .line 279
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;

    monitor-enter v2

    .line 280
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->enqueueLocked(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V

    .line 281
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mState:I

    .line 282
    monitor-exit v2

    .line 283
    return-void

    .line 282
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public prefetch(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3
    .parameter "context"
    .parameter "uri"

    .prologue
    .line 302
    new-instance v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;-><init>(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$1;)V

    .line 303
    .local v0, cmd:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;
    const/4 v1, 0x3

    iput v1, v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->code:I

    .line 304
    iput-object p1, v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->context:Landroid/content/Context;

    .line 305
    iput-object p2, v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->uri:Landroid/net/Uri;

    .line 306
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;

    monitor-enter v2

    .line 307
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->enqueueLocked(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V

    .line 308
    monitor-exit v2

    .line 309
    return-void

    .line 308
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setUsesWakeLock(Landroid/content/Context;)V
    .locals 4
    .parameter "context"

    .prologue
    .line 352
    iget-object v1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mThread:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

    if-eqz v1, :cond_1

    .line 355
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assertion failed mWakeLock="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mThread="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mThread:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 358
    :cond_1
    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 359
    .local v0, pm:Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string v2, "NetworkTtsPlayer"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 360
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 315
    iget-object v2, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mCmdQueue:Ljava/util/LinkedList;

    monitor-enter v2

    .line 318
    :try_start_0
    iget v1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mState:I

    if-eq v1, v3, :cond_0

    .line 319
    new-instance v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;-><init>(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$1;)V

    .line 320
    .local v0, cmd:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;->code:I

    .line 321
    invoke-direct {p0, v0}, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->enqueueLocked(Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;)V

    .line 322
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/translate/tts/NetworkTtsPlayer;->mState:I

    .line 324
    .end local v0           #cmd:Lcom/google/android/apps/translate/tts/NetworkTtsPlayer$Command;
    :cond_0
    monitor-exit v2

    .line 325
    return-void

    .line 324
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
