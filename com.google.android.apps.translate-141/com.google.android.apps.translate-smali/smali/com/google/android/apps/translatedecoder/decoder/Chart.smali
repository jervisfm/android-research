.class public Lcom/google/android/apps/translatedecoder/decoder/Chart;
.super Ljava/lang/Object;
.source "Chart.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private final latticeNodes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;",
            ">;>;"
        }
    .end annotation
.end field

.field private final nodesForEndPosition:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;",
            ">;"
        }
    .end annotation
.end field

.field private final pruner:Lcom/google/android/apps/translatedecoder/decoder/BeamPruner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/translatedecoder/decoder/Chart;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/translatedecoder/decoder/Chart;-><init>(Lcom/google/android/apps/translatedecoder/decoder/BeamPruner;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/translatedecoder/decoder/BeamPruner;)V
    .locals 1
    .parameter "pruner"

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->latticeNodes:Ljava/util/Map;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->nodesForEndPosition:Ljava/util/Map;

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->pruner:Lcom/google/android/apps/translatedecoder/decoder/BeamPruner;

    .line 43
    return-void
.end method

.method private printLatticeNodes(Ljava/util/List;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, nodes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .local v2, sb:Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;

    .line 89
    .local v1, node:Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;
    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;->viterbiEdge()Lcom/google/android/apps/translatedecoder/decoder/LatticeEdge;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 90
    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;->viterbiEdge()Lcom/google/android/apps/translatedecoder/decoder/LatticeEdge;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/translatedecoder/decoder/LatticeEdge;->insideViterbiCost()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 92
    :cond_0
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 94
    .end local v1           #node:Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;
    :cond_1
    sget-object v3, Lcom/google/android/apps/translatedecoder/decoder/Chart;->logger:Ljava/util/logging/Logger;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 95
    return-void
.end method


# virtual methods
.method public finishProcess(I)V
    .locals 5
    .parameter "endPos"

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processed endPos= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->nodesForEndPosition:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translatedecoder/decoder/Chart;->hasNode(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->pruner:Lcom/google/android/apps/translatedecoder/decoder/BeamPruner;

    if-eqz v0, :cond_2

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->latticeNodes:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->pruner:Lcom/google/android/apps/translatedecoder/decoder/BeamPruner;

    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->latticeNodes:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/translatedecoder/decoder/BeamPruner;->sortAndPruneList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    :cond_2
    sget-object v0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->logger:Ljava/util/logging/Logger;

    invoke-virtual {v0}, Ljava/util/logging/Logger;->getLevel()Ljava/util/logging/Level;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-ne v0, v1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->latticeNodes:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/apps/translatedecoder/decoder/Chart;->printLatticeNodes(Ljava/util/List;)V

    goto :goto_0
.end method

.method public getNode(I[I)Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;
    .locals 4
    .parameter "endPos"
    .parameter "stateWords"

    .prologue
    .line 47
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->nodesForEndPosition:Ljava/util/Map;

    invoke-static {p2, p1}, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;->getSignature([II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;

    .line 48
    .local v0, node:Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;
    if-nez v0, :cond_1

    .line 49
    new-instance v0, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;

    .end local v0           #node:Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;
    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;-><init>(I[I)V

    .line 50
    .restart local v0       #node:Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->nodesForEndPosition:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;->getSignature()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->latticeNodes:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 52
    .local v1, nodes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;>;"
    if-nez v1, :cond_0

    .line 53
    new-instance v1, Ljava/util/ArrayList;

    .end local v1           #nodes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .restart local v1       #nodes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;>;"
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->latticeNodes:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    .end local v1           #nodes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;>;"
    :cond_1
    return-object v0
.end method

.method public hasNode(I)Z
    .locals 3
    .parameter "endPos"

    .prologue
    .line 62
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->latticeNodes:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 63
    .local v0, nodes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public nodes(I)Ljava/util/List;
    .locals 2
    .parameter "endPos"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/decoder/Chart;->latticeNodes:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
