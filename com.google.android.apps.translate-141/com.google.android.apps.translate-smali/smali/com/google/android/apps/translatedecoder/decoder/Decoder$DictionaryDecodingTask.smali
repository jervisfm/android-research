.class Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;
.super Ljava/lang/Object;
.source "Decoder.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translatedecoder/decoder/Decoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DictionaryDecodingTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;",
        ">;>;"
    }
.end annotation


# instance fields
.field private decoder:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

.field private inputSent:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/translatedecoder/decoder/Decoder;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/translatedecoder/decoder/Decoder;Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "decoder"
    .parameter "inputSent"

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;->this$0:Lcom/google/android/apps/translatedecoder/decoder/Decoder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput-object p2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;->decoder:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

    .line 210
    iput-object p3, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;->inputSent:Ljava/lang/String;

    .line 211
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;->call()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;->decoder:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;->inputSent:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->decoding(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 215
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;>;"
    return-object v0
.end method
