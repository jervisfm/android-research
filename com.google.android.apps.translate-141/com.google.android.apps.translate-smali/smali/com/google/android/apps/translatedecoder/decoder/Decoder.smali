.class public Lcom/google/android/apps/translatedecoder/decoder/Decoder;
.super Ljava/lang/Object;
.source "Decoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;
    }
.end annotation


# static fields
.field private static final LM_UNK_STRING:Ljava/lang/String; = "<UNK>"

.field private static final kNumDefaultThreadsInExecutor:I = 0x4

.field private static final logger:Ljava/util/logging/Logger;

.field private static final mExecutor:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final config:Lcom/google/android/apps/translatedecoder/util/Config;

.field private dictDecoder:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

.field private final lmFeature:Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;

.field private final lmSymbolConverter:Lcom/google/android/apps/translatedecoder/util/LmSymbolConverter;

.field private final ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

.field private rapidRespTbl:Lcom/google/android/apps/translatedecoder/rapidresp/RapidResponseTbl;

.field private final tm:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

.field private final tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 57
    const-class v0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/translatedecoder/util/Config;Lcom/google/android/apps/translatedecoder/util/SymbolTable;Lcom/google/android/apps/translatedecoder/util/SymbolTable;)V
    .locals 12
    .parameter "config"
    .parameter "ptSymbol"
    .parameter "lmSymbol"

    .prologue
    const/4 v2, 0x0

    const-wide/16 v10, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->dictDecoder:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    .line 64
    const/4 v9, 0x0

    .line 65
    .local v9, simulateProdlm:Z
    const/4 v8, -0x1

    .line 67
    .local v8, lmUnkId:I
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->convertSymbol()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 68
    if-nez p3, :cond_0

    .line 69
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "convertSymbol is set true, but lmSymbol is null"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 72
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->simulateProdlm()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    const-string v1, "<UNK>"

    invoke-virtual {p3, v1}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->hasWord(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 74
    const/4 v9, 0x1

    .line 75
    const-string v1, "<UNK>"

    invoke-virtual {p3, v1}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->getId(Ljava/lang/String;)I

    move-result v8

    .line 80
    :cond_1
    :goto_0
    new-instance v1, Lcom/google/android/apps/translatedecoder/util/LmSymbolConverter;

    invoke-direct {v1, p2, p3, v9, v8}, Lcom/google/android/apps/translatedecoder/util/LmSymbolConverter;-><init>(Lcom/google/android/apps/translatedecoder/util/SymbolTable;Lcom/google/android/apps/translatedecoder/util/SymbolTable;ZI)V

    iput-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmSymbolConverter:Lcom/google/android/apps/translatedecoder/util/LmSymbolConverter;

    .line 86
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->tmFile()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tm:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    .line 88
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->oovTmCost()D

    move-result-wide v1

    cmpl-double v1, v1, v10

    if-lez v1, :cond_2

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tm:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->oovTmCost()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->setOovCost(D)V

    .line 91
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->maxPhraseLength()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->maxPhraseLength()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tm:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->maxPhraseLen()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 92
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tm:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->maxPhraseLength()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->setMaxPhraseLen(I)V

    .line 94
    :cond_3
    sget-object v1, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    const-string v2, "Finished reading pt!"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->rapidRespFile()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 103
    new-instance v0, Lcom/google/android/apps/translatedecoder/pt/HashMapBasedPt;

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->maxPhraseLength()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->oovTmCost()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->rapidRespFile()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translatedecoder/pt/HashMapBasedPt;-><init>(IDLjava/lang/String;Lcom/google/android/apps/translatedecoder/util/SymbolTable;)V

    .line 105
    .local v0, rapidPT:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;
    new-instance v1, Lcom/google/android/apps/translatedecoder/rapidresp/RapidResponseTbl;

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->dominateCost()D

    move-result-wide v2

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/translatedecoder/rapidresp/RapidResponseTbl;-><init>(Lcom/google/android/apps/translatedecoder/pt/PhraseTable;D)V

    iput-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->rapidRespTbl:Lcom/google/android/apps/translatedecoder/rapidresp/RapidResponseTbl;

    .line 108
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->maxPhraseLength()I

    move-result v1

    if-lez v1, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->maxPhraseLength()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->maxPhraseLen()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 109
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->maxPhraseLength()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->setMaxPhraseLen(I)V

    .line 111
    :cond_4
    sget-object v1, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "actuall maxPhraseLength="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tm:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    invoke-virtual {v3}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->maxPhraseLen()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 112
    sget-object v1, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    const-string v2, "Finished reading rapid response phrase table!"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 115
    .end local v0           #rapidPT:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->lmFile()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translatedecoder/lm/LanguageModel;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/lm/LanguageModel;

    move-result-object v7

    .line 116
    .local v7, lm:Lcom/google/android/apps/translatedecoder/lm/LanguageModel;
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->maxNumCachedNgrams()I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/google/android/apps/translatedecoder/lm/LanguageModel;->setMaxNumCachedNgrams(I)V

    .line 119
    if-eqz v9, :cond_6

    .line 120
    invoke-virtual {v7, v8}, Lcom/google/android/apps/translatedecoder/lm/LanguageModel;->setUnkId(I)V

    .line 121
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/google/android/apps/translatedecoder/lm/LanguageModel;->setSimulateProdlm(Z)V

    .line 124
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->oovLmCost()D

    move-result-wide v1

    cmpl-double v1, v1, v10

    if-lez v1, :cond_7

    .line 125
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->oovLmCost()D

    move-result-wide v1

    invoke-virtual {v7, v1, v2}, Lcom/google/android/apps/translatedecoder/lm/LanguageModel;->setOovCost(D)V

    .line 127
    :cond_7
    invoke-virtual {v7}, Lcom/google/android/apps/translatedecoder/lm/LanguageModel;->lmOrder()I

    move-result v6

    .line 128
    .local v6, acturalLmOrder:I
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->lmOrder()I

    move-result v1

    if-lez v1, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->lmOrder()I

    move-result v1

    invoke-virtual {v7}, Lcom/google/android/apps/translatedecoder/lm/LanguageModel;->lmOrder()I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 129
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->lmOrder()I

    move-result v6

    .line 130
    invoke-virtual {v7, v6}, Lcom/google/android/apps/translatedecoder/lm/LanguageModel;->setLmOrder(I)V

    .line 132
    :cond_8
    new-instance v1, Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmSymbolConverter:Lcom/google/android/apps/translatedecoder/util/LmSymbolConverter;

    invoke-direct {v1, v6, v7, v2}, Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;-><init>(ILcom/google/android/apps/translatedecoder/lm/LanguageModel;Lcom/google/android/apps/translatedecoder/util/LmSymbolConverter;)V

    iput-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmFeature:Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmFeature:Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;

    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->relativeLmWeight()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;->setRelativeLmWeight(D)V

    .line 134
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->ngramQueriedFile()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmFeature:Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;->setRememberNgrams(Lcom/google/android/apps/translatedecoder/util/SymbolTable;)V

    .line 137
    :cond_9
    sget-object v1, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    const-string v2, "Finished reading lm!"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->preprocDataFile()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    .line 140
    sget-object v1, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    const-string v2, "Finished reading tokenizer data!"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 142
    new-instance v1, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;-><init>(Lcom/google/android/apps/translatedecoder/util/Config;Lcom/google/android/apps/translatedecoder/util/SymbolTable;)V

    iput-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->dictDecoder:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

    .line 143
    return-void

    .line 77
    .end local v6           #acturalLmOrder:I
    .end local v7           #lm:Lcom/google/android/apps/translatedecoder/lm/LanguageModel;
    :cond_a
    sget-object v1, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    const-string v2, "lm symbol tbl does not have <UNK>"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 82
    :cond_b
    iput-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmSymbolConverter:Lcom/google/android/apps/translatedecoder/util/LmSymbolConverter;

    goto/16 :goto_1
.end method

.method public static main([Ljava/lang/String;)V
    .locals 9
    .parameter "args"

    .prologue
    const/4 v8, 0x1

    .line 288
    array-length v6, p0

    if-ge v6, v8, :cond_0

    .line 289
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "Usage: java Decoder configFile [--option_key=option_value]+"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 290
    invoke-static {v8}, Ljava/lang/System;->exit(I)V

    .line 294
    :cond_0
    new-instance v0, Lcom/google/android/apps/translatedecoder/util/Config;

    const/4 v6, 0x0

    aget-object v6, p0, v6

    invoke-direct {v0, v6}, Lcom/google/android/apps/translatedecoder/util/Config;-><init>(Ljava/lang/String;)V

    .line 298
    .local v0, config:Lcom/google/android/apps/translatedecoder/util/Config;
    array-length v6, p0

    add-int/lit8 v6, v6, -0x1

    new-array v2, v6, [Ljava/lang/String;

    .line 299
    .local v2, flagsArray:[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    array-length v6, v2

    if-ge v3, v6, :cond_1

    .line 300
    add-int/lit8 v6, v3, 0x1

    aget-object v6, p0, v6

    aput-object v6, v2, v3

    .line 299
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 302
    :cond_1
    invoke-virtual {v0, v2}, Lcom/google/android/apps/translatedecoder/util/Config;->setParametersFromArgs([Ljava/lang/String;)V

    .line 305
    new-instance v5, Lcom/google/android/apps/translatedecoder/util/HashMapBasedSymbol;

    invoke-direct {v5}, Lcom/google/android/apps/translatedecoder/util/HashMapBasedSymbol;-><init>()V

    .line 306
    .local v5, ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;
    new-instance v4, Lcom/google/android/apps/translatedecoder/util/HashMapBasedSymbol;

    invoke-direct {v4}, Lcom/google/android/apps/translatedecoder/util/HashMapBasedSymbol;-><init>()V

    .line 307
    .local v4, lmSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;
    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/util/Config;->readSymbolFromFile()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 308
    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/util/Config;->ptSymbolTblFile()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    move-result-object v5

    .line 309
    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/util/Config;->lmSymbolTblFile()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    move-result-object v4

    .line 311
    :cond_2
    new-instance v1, Lcom/google/android/apps/translatedecoder/decoder/Decoder;

    invoke-direct {v1, v0, v5, v4}, Lcom/google/android/apps/translatedecoder/decoder/Decoder;-><init>(Lcom/google/android/apps/translatedecoder/util/Config;Lcom/google/android/apps/translatedecoder/util/SymbolTable;Lcom/google/android/apps/translatedecoder/util/SymbolTable;)V

    .line 312
    .local v1, decoder:Lcom/google/android/apps/translatedecoder/decoder/Decoder;
    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/util/Config;->inputFile()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/util/Config;->outputFile()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->batchDecoding(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->shutDown()V

    .line 314
    return-void
.end method


# virtual methods
.method public batchDecoding(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .parameter "inputFile"
    .parameter "outputFIle"

    .prologue
    .line 260
    :try_start_0
    new-instance v3, Ljava/io/DataInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 261
    .local v3, in:Ljava/io/DataInputStream;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 262
    .local v0, br:Ljava/io/BufferedReader;
    new-instance v5, Ljava/io/DataOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 263
    .local v5, out:Ljava/io/DataOutputStream;
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v6, Ljava/io/OutputStreamWriter;

    invoke-direct {v6, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 266
    .local v1, bw:Ljava/io/BufferedWriter;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .local v4, line:Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 267
    sget-object v6, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Decoding: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 268
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->decoding(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 273
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .end local v3           #in:Ljava/io/DataInputStream;
    .end local v4           #line:Ljava/lang/String;
    .end local v5           #out:Ljava/io/DataOutputStream;
    :catch_0
    move-exception v2

    .line 274
    .local v2, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 278
    .end local v2           #e:Ljava/io/FileNotFoundException;
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v6}, Lcom/google/android/apps/translatedecoder/util/Config;->ngramQueriedFile()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 279
    iget-object v6, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmFeature:Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;

    iget-object v7, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v7}, Lcom/google/android/apps/translatedecoder/util/Config;->ngramQueriedFile()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;->saveNgramsQueried(Ljava/lang/String;)V

    .line 281
    :cond_0
    return-void

    .line 270
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v1       #bw:Ljava/io/BufferedWriter;
    .restart local v3       #in:Ljava/io/DataInputStream;
    .restart local v4       #line:Ljava/lang/String;
    .restart local v5       #out:Ljava/io/DataOutputStream;
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V

    .line 271
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    .line 272
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 275
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .end local v3           #in:Ljava/io/DataInputStream;
    .end local v4           #line:Ljava/lang/String;
    .end local v5           #out:Ljava/io/DataOutputStream;
    :catch_1
    move-exception v2

    .line 276
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public decode(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .parameter "sentence"

    .prologue
    .line 220
    sget-object v9, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v10, Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;

    iget-object v11, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->dictDecoder:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

    invoke-direct {v10, p0, v11, p1}, Lcom/google/android/apps/translatedecoder/decoder/Decoder$DictionaryDecodingTask;-><init>(Lcom/google/android/apps/translatedecoder/decoder/Decoder;Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;Ljava/lang/String;)V

    invoke-interface {v9, v10}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v2

    .line 222
    .local v2, dictDecoderTask:Ljava/util/concurrent/Future;,"Ljava/util/concurrent/Future<Ljava/util/List<Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;>;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->decoding(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 223
    .local v8, translation:Ljava/lang/String;
    const/4 v3, 0x0

    .line 225
    .local v3, entries:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;>;"
    const-wide/16 v9, 0x1f4

    :try_start_0
    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v9, v10, v11}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/util/List;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    .line 233
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    .local v1, detailedTranslation:Ljava/lang/StringBuilder;
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 235
    const/4 v9, 0x0

    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;

    invoke-virtual {v9}, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->tgtWords()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    const-string v9, "\t"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    const/4 v7, 0x1

    .line 238
    .local v7, j:I
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;

    .line 239
    .local v4, entry:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;
    const-string v9, " "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ". "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->tgtWords()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    add-int/lit8 v7, v7, 0x1

    .line 245
    const-string v9, "\n"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 226
    .end local v1           #detailedTranslation:Ljava/lang/StringBuilder;
    .end local v4           #entry:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v7           #j:I
    :catch_0
    move-exception v5

    .line 227
    .local v5, ex:Ljava/lang/InterruptedException;
    sget-object v9, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Dictionary Decoding was exceptional! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    goto :goto_0

    .line 228
    .end local v5           #ex:Ljava/lang/InterruptedException;
    :catch_1
    move-exception v5

    .line 229
    .local v5, ex:Ljava/util/concurrent/ExecutionException;
    sget-object v9, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Dictionary Decoding was exceptional! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/util/concurrent/ExecutionException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 230
    .end local v5           #ex:Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v5

    .line 231
    .local v5, ex:Ljava/util/concurrent/TimeoutException;
    sget-object v9, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->logger:Ljava/util/logging/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Dictionary Decoding timed out! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/util/concurrent/TimeoutException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 248
    .end local v5           #ex:Ljava/util/concurrent/TimeoutException;
    .restart local v1       #detailedTranslation:Ljava/lang/StringBuilder;
    :cond_0
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method

.method public decoding(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "sentence"

    .prologue
    .line 180
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/util/Config;->runPreprocess()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v3}, Lcom/google/android/apps/translatedecoder/util/Config;->srcLang()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->tokenizeWithJoin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, tokenized:Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->addWords(Ljava/lang/String;)[I

    move-result-object v0

    .line 183
    .local v0, inputWords:[I
    invoke-virtual {p0, v0}, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->decoding([I)Ljava/lang/String;

    move-result-object v2

    .line 185
    .end local v0           #inputWords:[I
    .end local v1           #tokenized:Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->addWords(Ljava/lang/String;)[I

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->decoding([I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public decoding([I)Ljava/lang/String;
    .locals 8
    .parameter "inputWords"

    .prologue
    .line 194
    new-instance v0, Lcom/google/android/apps/translatedecoder/decoder/Search;

    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tm:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->rapidRespTbl:Lcom/google/android/apps/translatedecoder/rapidresp/RapidResponseTbl;

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmFeature:Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;

    iget-object v4, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    iget-object v5, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/translatedecoder/decoder/Search;-><init>(Lcom/google/android/apps/translatedecoder/pt/PhraseTable;Lcom/google/android/apps/translatedecoder/rapidresp/RapidResponseTbl;Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;Lcom/google/android/apps/translatedecoder/util/Config;Lcom/google/android/apps/translatedecoder/util/SymbolTable;[I)V

    .line 195
    .local v0, searcher:Lcom/google/android/apps/translatedecoder/decoder/Search;
    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/decoder/Search;->search()Lcom/google/android/apps/translatedecoder/decoder/Lattice;

    move-result-object v7

    .line 196
    .local v7, lattice:Lcom/google/android/apps/translatedecoder/decoder/Lattice;
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/util/Config;->runPostprocess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/util/Config;->tgtLang()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v3}, Lcom/google/android/apps/translatedecoder/util/Config;->printBracket()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v7, v3, v4}, Lcom/google/android/apps/translatedecoder/decoder/Lattice;->extractViterbi(ZLcom/google/android/apps/translatedecoder/util/SymbolTable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->deTokenize(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 200
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/util/Config;->printBracket()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v7, v1, v2}, Lcom/google/android/apps/translatedecoder/decoder/Lattice;->extractViterbi(ZLcom/google/android/apps/translatedecoder/util/SymbolTable;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public decodingWithStatInfo(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;
    .locals 12
    .parameter "rawInput"

    .prologue
    .line 150
    new-instance v9, Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;

    invoke-direct {v9}, Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;-><init>()V

    .line 151
    .local v9, res:Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;
    invoke-virtual {v9, p1}, Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;->setRawInput(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 153
    .local v10, tokenizedInput:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/util/Config;->runPreprocess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/util/Config;->srcLang()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->tokenizeWithJoin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 156
    :cond_0
    invoke-virtual {v9, v10}, Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;->setTokenizedInput(Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v1, v10}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->addWords(Ljava/lang/String;)[I

    move-result-object v6

    .line 160
    .local v6, inputWords:[I
    new-instance v0, Lcom/google/android/apps/translatedecoder/decoder/Search;

    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tm:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->rapidRespTbl:Lcom/google/android/apps/translatedecoder/rapidresp/RapidResponseTbl;

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->lmFeature:Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;

    iget-object v4, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    iget-object v5, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/translatedecoder/decoder/Search;-><init>(Lcom/google/android/apps/translatedecoder/pt/PhraseTable;Lcom/google/android/apps/translatedecoder/rapidresp/RapidResponseTbl;Lcom/google/android/apps/translatedecoder/lm/LanguageModelFeature;Lcom/google/android/apps/translatedecoder/util/Config;Lcom/google/android/apps/translatedecoder/util/SymbolTable;[I)V

    .line 161
    .local v0, searcher:Lcom/google/android/apps/translatedecoder/decoder/Search;
    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/decoder/Search;->search()Lcom/google/android/apps/translatedecoder/decoder/Lattice;

    move-result-object v8

    .line 162
    .local v8, lattice:Lcom/google/android/apps/translatedecoder/decoder/Lattice;
    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/decoder/Lattice;->goalNode()Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/decoder/LatticeNode;->insideViterbiCost()D

    move-result-wide v1

    invoke-virtual {v9, v1, v2}, Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;->setCost(D)V

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/util/Config;->printBracket()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v8, v1, v2}, Lcom/google/android/apps/translatedecoder/decoder/Lattice;->extractViterbi(ZLcom/google/android/apps/translatedecoder/util/SymbolTable;)Ljava/lang/String;

    move-result-object v11

    .line 164
    .local v11, tokenizedOutput:Ljava/lang/String;
    invoke-virtual {v9, v11}, Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;->setTokenizedOutput(Ljava/lang/String;)V

    .line 166
    move-object v7, v11

    .line 167
    .local v7, deTokenizedOutput:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/util/Config;->runPostprocess()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/util/Config;->tgtLang()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v11}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->deTokenize(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 170
    :cond_1
    invoke-virtual {v9, v7}, Lcom/google/android/apps/translatedecoder/decoder/DecoderOutput;->setDetokenizedOutput(Ljava/lang/String;)V

    .line 172
    return-object v9
.end method

.method public shutDown()V
    .locals 1

    .prologue
    .line 284
    sget-object v0, Lcom/google/android/apps/translatedecoder/decoder/Decoder;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 285
    return-void
.end method
