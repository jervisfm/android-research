.class public Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;
.super Ljava/lang/Object;
.source "DictionaryDecoder.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private final config:Lcom/google/android/apps/translatedecoder/util/Config;

.field private final phraseTbl:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

.field private final ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

.field private final tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/translatedecoder/util/Config;Lcom/google/android/apps/translatedecoder/util/SymbolTable;)V
    .locals 2
    .parameter "config"
    .parameter "ptSymbol"

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    .line 32
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->tmFile()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->phraseTbl:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    .line 33
    sget-object v0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->logger:Ljava/util/logging/Logger;

    const-string v1, "Finished reading phraseTbl!"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, Lcom/google/android/apps/translatedecoder/util/Config;->preprocDataFile()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    .line 36
    sget-object v0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->logger:Ljava/util/logging/Logger;

    const-string v1, "Finished reading tokenizer data!"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 37
    return-void
.end method


# virtual methods
.method public decoding(Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .parameter "sentence"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/util/Config;->runPreprocess()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v3}, Lcom/google/android/apps/translatedecoder/util/Config;->srcLang()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->tokenizeWithJoin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->addWords(Ljava/lang/String;)[I

    move-result-object v7

    .line 48
    .local v7, inputWords:[I
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->phraseTbl:Lcom/google/android/apps/translatedecoder/pt/PhraseTable;

    invoke-virtual {v1, v7}, Lcom/google/android/apps/translatedecoder/pt/PhraseTable;->getPhrases([I)Ljava/util/List;

    move-result-object v9

    .line 49
    .local v9, pairs:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/pt/PhrasePair;>;"
    if-eqz v9, :cond_0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_3

    .line 50
    :cond_0
    const/4 v10, 0x0

    .line 73
    :cond_1
    return-object v10

    .line 44
    .end local v7           #inputWords:[I
    .end local v9           #pairs:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/pt/PhrasePair;>;"
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->addWords(Ljava/lang/String;)[I

    move-result-object v7

    goto :goto_0

    .line 52
    .restart local v7       #inputWords:[I
    .restart local v9       #pairs:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/pt/PhrasePair;>;"
    :cond_3
    const/4 v10, 0x0

    .line 53
    .local v10, res:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;

    .line 54
    .local v8, pair:Lcom/google/android/apps/translatedecoder/pt/PhrasePair;
    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->dictInfo()[I

    move-result-object v1

    if-eqz v1, :cond_4

    .line 55
    if-nez v10, :cond_5

    .line 56
    new-instance v10, Ljava/util/ArrayList;

    .end local v10           #res:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .restart local v10       #res:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;>;"
    :cond_5
    const/4 v0, 0x0

    .line 59
    .local v0, entry:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/util/Config;->runPostprocess()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 60
    new-instance v0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;

    .end local v0           #entry:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->srcWords()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->getWords([I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->config:Lcom/google/android/apps/translatedecoder/util/Config;

    invoke-virtual {v3}, Lcom/google/android/apps/translatedecoder/util/Config;->tgtLang()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->tgtWords()[I

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->getWords([I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->deTokenize(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->dictInfo()[I

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->getWords([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->cost()D

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    .line 70
    .restart local v0       #entry:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;
    :goto_2
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 65
    :cond_6
    new-instance v0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;

    .end local v0           #entry:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->srcWords()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->getWords([I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->tgtWords()[I

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->getWords([I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryDecoder;->ptSymbol:Lcom/google/android/apps/translatedecoder/util/SymbolTable;

    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->dictInfo()[I

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/translatedecoder/util/SymbolTable;->getWords([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->cost()D

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    .restart local v0       #entry:Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;
    goto :goto_2
.end method
