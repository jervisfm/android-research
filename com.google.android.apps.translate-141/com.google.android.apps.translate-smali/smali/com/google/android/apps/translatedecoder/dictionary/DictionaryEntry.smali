.class public Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;
.super Ljava/lang/Object;
.source "DictionaryEntry.java"


# instance fields
.field private final cost:D

.field private final srcWords:Ljava/lang/String;

.field private final tag:Ljava/lang/String;

.field private final tgtWords:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
    .locals 0
    .parameter "srcWords"
    .parameter "tgtWords"
    .parameter "tag"
    .parameter "cost"

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->srcWords:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->tgtWords:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->tag:Ljava/lang/String;

    .line 24
    iput-wide p4, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->cost:D

    .line 25
    return-void
.end method


# virtual methods
.method public cost()D
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->cost:D

    return-wide v0
.end method

.method public srcWords()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->srcWords:Ljava/lang/String;

    return-object v0
.end method

.method public tag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public tgtWords()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/dictionary/DictionaryEntry;->tgtWords:Ljava/lang/String;

    return-object v0
.end method
