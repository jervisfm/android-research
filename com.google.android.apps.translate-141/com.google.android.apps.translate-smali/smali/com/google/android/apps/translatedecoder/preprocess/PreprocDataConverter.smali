.class public Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;
.super Ljava/lang/Object;
.source "PreprocDataConverter.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field preprocAbbrData:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field preprocMapData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static handleSpace(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "in"

    .prologue
    .line 134
    const-string v0, "^\\s+$"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const-string v0, " "

    .line 137
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 10
    .parameter "args"

    .prologue
    const/4 v8, 0x1

    .line 142
    array-length v6, p0

    if-gtz v6, :cond_0

    .line 143
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "Usage: blaze run PreprocDataConverter -- --charMapFile=file --charMapOutFile=file--preprocDataFile=file --preprocDataOutFile=file --mmapFormat=flag--removeDiacritics=flag"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 147
    invoke-static {v8}, Ljava/lang/System;->exit(I)V

    .line 149
    :cond_0
    new-instance v2, Lcom/google/android/apps/translatedecoder/util/ConfigParser;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;-><init>([Ljava/lang/String;)V

    .line 150
    .local v2, config:Lcom/google/android/apps/translatedecoder/util/ConfigParser;
    const/4 v6, 0x4

    new-array v4, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "charMapFile"

    aput-object v7, v4, v6

    const-string v6, "charMapOutFile"

    aput-object v6, v4, v8

    const/4 v6, 0x2

    const-string v7, "preprocDataFile"

    aput-object v7, v4, v6

    const/4 v6, 0x3

    const-string v7, "preprocDataOutFile"

    aput-object v7, v4, v6

    .line 152
    .local v4, requiredProperties:[Ljava/lang/String;
    invoke-virtual {v2, v4}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->checkRequiredProperties([Ljava/lang/String;)V

    .line 155
    const-string v6, "charMapFile"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->readCharMapTbl(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 157
    .local v0, charMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;

    new-instance v6, Ljava/lang/Boolean;

    const-string v7, "removeDiacritics"

    const-string v8, "false"

    invoke-virtual {v2, v7, v8}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/Boolean;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    new-instance v7, Lcom/google/android/apps/translatedecoder/util/ArrayBasedStringMap;

    invoke-direct {v7, v0}, Lcom/google/android/apps/translatedecoder/util/ArrayBasedStringMap;-><init>(Ljava/util/Map;)V

    invoke-direct {v1, v6, v7}, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;-><init>(ZLcom/google/android/apps/translatedecoder/util/StringMap;)V

    .line 160
    .local v1, charNormalizer:Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;
    const-string v6, "charMapOutFile"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/Boolean;

    const-string v8, "mmapFormat"

    const-string v9, "true"

    invoke-virtual {v2, v8, v9}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/Boolean;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;->writeToFile(Ljava/lang/String;Z)V

    .line 164
    new-instance v3, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;

    invoke-direct {v3}, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;-><init>()V

    .line 165
    .local v3, converter:Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;
    const-string v6, "preprocDataFile"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6, v1}, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->readPreprocData(Ljava/lang/String;Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;)V

    .line 166
    new-instance v5, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    new-instance v6, Lcom/google/android/apps/translatedecoder/util/ArrayBasedStringSet;

    iget-object v7, v3, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->preprocAbbrData:Ljava/util/Set;

    invoke-direct {v6, v7}, Lcom/google/android/apps/translatedecoder/util/ArrayBasedStringSet;-><init>(Ljava/util/Set;)V

    new-instance v7, Lcom/google/android/apps/translatedecoder/util/ArrayBasedStringMap;

    iget-object v8, v3, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->preprocMapData:Ljava/util/Map;

    invoke-direct {v7, v8}, Lcom/google/android/apps/translatedecoder/util/ArrayBasedStringMap;-><init>(Ljava/util/Map;)V

    invoke-direct {v5, v6, v7, v1}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;-><init>(Lcom/google/android/apps/translatedecoder/util/StringSet;Lcom/google/android/apps/translatedecoder/util/StringMap;Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;)V

    .line 168
    .local v5, token:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
    const-string v6, "preprocDataOutFile"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/Boolean;

    const-string v8, "mmapFormat"

    const-string v9, "true"

    invoke-virtual {v2, v8, v9}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/Boolean;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->writeToFile(Ljava/lang/String;Z)V

    .line 170
    return-void
.end method

.method public static readCharMapTbl(Ljava/lang/String;)Ljava/util/Map;
    .locals 14
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x1

    .line 91
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 92
    .local v1, charMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Ljava/io/DataInputStream;

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v9}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 93
    .local v3, in:Ljava/io/DataInputStream;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    const-string v10, "utf8"

    invoke-direct {v9, v3, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 95
    .local v0, br:Ljava/io/BufferedReader;
    const-string v9, "\\|\\s+(.{1,4})\\s+\\|\\s+(.{1,8})\\s+\\|\\s+(.{1,4})\\s+\\|\\s+(.{1,8})\\s+\\|\\s+\\|"

    invoke-static {v9}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 98
    .local v7, pattern:Ljava/util/regex/Pattern;
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .local v4, line:Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 99
    invoke-static {v4}, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 100
    invoke-virtual {v7, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 101
    .local v5, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v9

    if-ne v9, v13, :cond_3

    .line 102
    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->handleSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 103
    .local v6, original:Ljava/lang/String;
    const/4 v9, 0x3

    invoke-virtual {v5, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->handleSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 104
    .local v8, replace:Ljava/lang/String;
    const-string v9, "^\\s+$"

    invoke-virtual {v8, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x4

    invoke-virtual {v5, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "^\\s*0\\s*$"

    invoke-virtual {v9, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 106
    move-object v8, v6

    .line 108
    :cond_1
    invoke-interface {v1, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-gt v9, v13, :cond_2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v13, :cond_0

    .line 111
    :cond_2
    sget-object v9, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->logger:Ljava/util/logging/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "A human-perceivable char has more than four machine chars (utf16)! Theline is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/logging/Logger;->severe(Ljava/lang/String;)V

    .line 113
    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/System;->exit(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #charMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3           #in:Ljava/io/DataInputStream;
    .end local v4           #line:Ljava/lang/String;
    .end local v5           #matcher:Ljava/util/regex/Matcher;
    .end local v6           #original:Ljava/lang/String;
    .end local v7           #pattern:Ljava/util/regex/Pattern;
    .end local v8           #replace:Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 124
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 125
    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    .line 126
    const/4 v1, 0x0

    .end local v2           #e:Ljava/lang/Exception;
    :goto_1
    return-object v1

    .line 116
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v1       #charMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v3       #in:Ljava/io/DataInputStream;
    .restart local v4       #line:Ljava/lang/String;
    .restart local v5       #matcher:Ljava/util/regex/Matcher;
    .restart local v7       #pattern:Ljava/util/regex/Pattern;
    :cond_3
    :try_start_1
    sget-object v9, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->logger:Ljava/util/logging/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "No match with the regular expression! The line is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ". The file"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/logging/Logger;->severe(Ljava/lang/String;)V

    .line 118
    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_0

    .line 121
    .end local v5           #matcher:Ljava/util/regex/Matcher;
    :cond_4
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public readPreprocData(Ljava/lang/String;Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;)V
    .locals 12
    .parameter "fileName"
    .parameter "charNormalizer"

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    .line 53
    :try_start_0
    sget-object v7, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->logger:Ljava/util/logging/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Read preproc data from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 54
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->preprocAbbrData:Ljava/util/Set;

    .line 55
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->preprocMapData:Ljava/util/Map;

    .line 56
    new-instance v3, Ljava/io/DataInputStream;

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v7}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 57
    .local v3, in:Ljava/io/DataInputStream;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-direct {v7, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 58
    .local v1, br:Ljava/io/BufferedReader;
    const-string v7, "^([a-zA-Z]+)\\s+[aA][bB][bB][rR][eE][vV]\\s+(.+)$"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 59
    .local v0, abbrPattern:Ljava/util/regex/Pattern;
    const-string v7, "^([a-zA-Z]+)\\s+word_map\\s+([^\\s]+)\\s+(.+)$"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 62
    .local v5, mapPattern:Ljava/util/regex/Pattern;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .local v4, line:Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 63
    invoke-virtual {p2, v4}, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;->normalizeChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 64
    invoke-virtual {v0, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 65
    .local v6, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v7

    if-ne v7, v10, :cond_1

    .line 66
    iget-object v7, p0, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->preprocAbbrData:Ljava/util/Set;

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v6, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->getAbbrSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 81
    .end local v0           #abbrPattern:Ljava/util/regex/Pattern;
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v3           #in:Ljava/io/DataInputStream;
    .end local v4           #line:Ljava/lang/String;
    .end local v5           #mapPattern:Ljava/util/regex/Pattern;
    .end local v6           #matcher:Ljava/util/regex/Matcher;
    :catch_0
    move-exception v2

    .line 82
    .local v2, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 86
    .end local v2           #e:Ljava/io/FileNotFoundException;
    :goto_1
    return-void

    .line 68
    .restart local v0       #abbrPattern:Ljava/util/regex/Pattern;
    .restart local v1       #br:Ljava/io/BufferedReader;
    .restart local v3       #in:Ljava/io/DataInputStream;
    .restart local v4       #line:Ljava/lang/String;
    .restart local v5       #mapPattern:Ljava/util/regex/Pattern;
    .restart local v6       #matcher:Ljava/util/regex/Matcher;
    :cond_1
    :try_start_1
    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 69
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v7

    if-ne v7, v11, :cond_0

    .line 70
    iget-object v7, p0, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->preprocMapData:Ljava/util/Map;

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v6, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->getMapSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {v6, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 83
    .end local v0           #abbrPattern:Ljava/util/regex/Pattern;
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v3           #in:Ljava/io/DataInputStream;
    .end local v4           #line:Ljava/lang/String;
    .end local v5           #mapPattern:Ljava/util/regex/Pattern;
    .end local v6           #matcher:Ljava/util/regex/Matcher;
    :catch_1
    move-exception v2

    .line 84
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 78
    .end local v2           #e:Ljava/io/IOException;
    .restart local v0       #abbrPattern:Ljava/util/regex/Pattern;
    .restart local v1       #br:Ljava/io/BufferedReader;
    .restart local v3       #in:Ljava/io/DataInputStream;
    .restart local v4       #line:Ljava/lang/String;
    .restart local v5       #mapPattern:Ljava/util/regex/Pattern;
    :cond_2
    :try_start_2
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V

    .line 79
    sget-object v7, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->logger:Ljava/util/logging/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "abbrSize="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->preprocAbbrData:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 80
    sget-object v7, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->logger:Ljava/util/logging/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mapSize="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/translatedecoder/preprocess/PreprocDataConverter;->preprocMapData:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1
.end method
