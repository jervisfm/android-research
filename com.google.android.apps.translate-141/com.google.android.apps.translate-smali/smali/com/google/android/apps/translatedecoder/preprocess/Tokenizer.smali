.class public Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
.super Ljava/lang/Object;
.source "Tokenizer.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final logger:Ljava/util/logging/Logger; = null

.field private static final serialVersionUID:J = -0x2fb7c5ecfa704443L


# instance fields
.field private final charNormalizer:Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;

.field private final handleSpecialPuncts:Z

.field private final preprocAbbrData:Lcom/google/android/apps/translatedecoder/util/StringSet;

.field private final preprocMapData:Lcom/google/android/apps/translatedecoder/util/StringMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/translatedecoder/util/StringSet;Lcom/google/android/apps/translatedecoder/util/StringMap;Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;)V
    .locals 1
    .parameter "preprocAbbrData"
    .parameter "preprocMapData"
    .parameter "charNormalizer"

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->handleSpecialPuncts:Z

    .line 64
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->preprocAbbrData:Lcom/google/android/apps/translatedecoder/util/StringSet;

    .line 65
    iput-object p2, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->preprocMapData:Lcom/google/android/apps/translatedecoder/util/StringMap;

    .line 66
    iput-object p3, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->charNormalizer:Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;

    .line 67
    return-void
.end method

.method public static getAbbrSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "lang"
    .parameter "abbr"

    .prologue
    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMapSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "lang"
    .parameter "orignal"

    .prologue
    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isCJKT(Ljava/lang/String;)Z
    .locals 2
    .parameter "fullLangName"

    .prologue
    .line 371
    const-string v0, "chinese"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zh"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "japanese"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ja"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "korean"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ko"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "thai"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "th"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 13
    .parameter "args"

    .prologue
    const/4 v12, 0x1

    .line 378
    array-length v10, p0

    if-gtz v10, :cond_0

    .line 379
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "Usage: java Tokenizer --inputFile=file --outputFile=file --language=language --preprocDataFile=file"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 382
    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    .line 385
    :cond_0
    new-instance v2, Lcom/google/android/apps/translatedecoder/util/ConfigParser;

    invoke-direct {v2, p0}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;-><init>([Ljava/lang/String;)V

    .line 387
    .local v2, config:Lcom/google/android/apps/translatedecoder/util/ConfigParser;
    const/4 v10, 0x4

    new-array v8, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "inputFile"

    aput-object v11, v8, v10

    const-string v10, "outputFile"

    aput-object v10, v8, v12

    const/4 v10, 0x2

    const-string v11, "language"

    aput-object v11, v8, v10

    const/4 v10, 0x3

    const-string v11, "preprocDataFile"

    aput-object v11, v8, v10

    .line 389
    .local v8, requiredProperties:[Ljava/lang/String;
    invoke-virtual {v2, v8}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->checkRequiredProperties([Ljava/lang/String;)V

    .line 391
    const-string v10, "preprocDataFile"

    invoke-virtual {v2, v10}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    move-result-object v9

    .line 394
    .local v9, tokenizer:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
    :try_start_0
    new-instance v4, Ljava/io/DataInputStream;

    new-instance v10, Ljava/io/FileInputStream;

    const-string v11, "inputFile"

    invoke-virtual {v2, v11}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v10}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 396
    .local v4, in:Ljava/io/DataInputStream;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-direct {v10, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 397
    .local v0, br:Ljava/io/BufferedReader;
    new-instance v6, Ljava/io/DataOutputStream;

    new-instance v10, Ljava/io/FileOutputStream;

    const-string v11, "outputFile"

    invoke-virtual {v2, v11}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v10}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 399
    .local v6, out:Ljava/io/DataOutputStream;
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v10, Ljava/io/OutputStreamWriter;

    invoke-direct {v10, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 402
    .local v1, bw:Ljava/io/BufferedWriter;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, line:Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 403
    sget-object v10, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->logger:Ljava/util/logging/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Process: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 404
    const-string v10, "language"

    invoke-virtual {v2, v10}, Lcom/google/android/apps/translatedecoder/util/ConfigParser;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->tokenizeWithJoin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 405
    .local v7, outLine:Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 410
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .end local v4           #in:Ljava/io/DataInputStream;
    .end local v5           #line:Ljava/lang/String;
    .end local v6           #out:Ljava/io/DataOutputStream;
    .end local v7           #outLine:Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 411
    .local v3, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 415
    .end local v3           #e:Ljava/io/FileNotFoundException;
    :goto_1
    return-void

    .line 407
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v1       #bw:Ljava/io/BufferedWriter;
    .restart local v4       #in:Ljava/io/DataInputStream;
    .restart local v5       #line:Ljava/lang/String;
    .restart local v6       #out:Ljava/io/DataOutputStream;
    :cond_1
    :try_start_1
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    .line 408
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    .line 409
    invoke-virtual {v6}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 412
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .end local v4           #in:Ljava/io/DataInputStream;
    .end local v5           #line:Ljava/lang/String;
    .end local v6           #out:Ljava/io/DataOutputStream;
    :catch_1
    move-exception v3

    .line 413
    .local v3, e:Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
    .locals 4
    .parameter "buf"

    .prologue
    .line 364
    invoke-static {p0}, Lcom/google/android/apps/translatedecoder/util/StringSet;->readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/util/StringSet;

    move-result-object v1

    .line 365
    .local v1, preprocAbbrData:Lcom/google/android/apps/translatedecoder/util/StringSet;
    invoke-static {p0}, Lcom/google/android/apps/translatedecoder/util/StringMap;->readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/util/StringMap;

    move-result-object v2

    .line 366
    .local v2, preprocMapData:Lcom/google/android/apps/translatedecoder/util/StringMap;
    invoke-static {p0}, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;->readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;

    move-result-object v0

    .line 367
    .local v0, charNormalizer:Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;
    new-instance v3, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    invoke-direct {v3, v1, v2, v0}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;-><init>(Lcom/google/android/apps/translatedecoder/util/StringSet;Lcom/google/android/apps/translatedecoder/util/StringMap;Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;)V

    return-object v3
.end method

.method public static readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
    .locals 10
    .parameter "file"

    .prologue
    .line 338
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v1, p0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 339
    .local v0, channel:Ljava/nio/channels/FileChannel;
    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    long-to-int v4, v4

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v6

    .line 340
    .local v6, buf:Ljava/nio/ByteBuffer;
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    invoke-static {}, Lcom/google/android/apps/translatedecoder/util/MemMapUtil;->mmapFileSignature()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 341
    invoke-static {v6}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    move-result-object v9

    .line 342
    .local v9, res:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 353
    .end local v0           #channel:Ljava/nio/channels/FileChannel;
    .end local v6           #buf:Ljava/nio/ByteBuffer;
    .end local v9           #res:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
    :goto_0
    return-object v9

    .line 345
    .restart local v0       #channel:Ljava/nio/channels/FileChannel;
    .restart local v6       #buf:Ljava/nio/ByteBuffer;
    :cond_0
    new-instance v8, Ljava/io/ObjectInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 346
    .local v8, in:Ljava/io/ObjectInputStream;
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;

    .line 347
    .restart local v9       #res:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->close()V

    .line 348
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 351
    .end local v0           #channel:Ljava/nio/channels/FileChannel;
    .end local v6           #buf:Ljava/nio/ByteBuffer;
    .end local v8           #in:Ljava/io/ObjectInputStream;
    .end local v9           #res:Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;
    :catch_0
    move-exception v7

    .line 352
    .local v7, ex:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 353
    const/4 v9, 0x0

    goto :goto_0
.end method


# virtual methods
.method public deTokenize(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .parameter "lang"
    .parameter "sentence"

    .prologue
    .line 253
    invoke-static {p1}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->isCJKT(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 254
    const-string v5, "\\s+"

    const-string v6, ""

    invoke-virtual {p2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 277
    :goto_0
    return-object v5

    .line 257
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 258
    .local v4, res:Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 259
    .local v3, previousIsSpace:Z
    const/4 v2, 0x0

    .local v2, offset:I
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_3

    .line 260
    invoke-virtual {p2, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    .line 261
    .local v1, codepoint:I
    new-instance v0, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([C)V

    .line 264
    .local v0, c:Ljava/lang/String;
    if-eqz v3, :cond_1

    const-string v5, "^\\p{Punct}$"

    invoke-virtual {v0, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 265
    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_1
    const-string v5, "^\\p{Space}$"

    invoke-virtual {v0, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 270
    const/4 v3, 0x1

    .line 275
    :goto_2
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 276
    goto :goto_1

    .line 272
    :cond_2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    const/4 v3, 0x0

    goto :goto_2

    .line 277
    .end local v0           #c:Ljava/lang/String;
    .end local v1           #codepoint:I
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public getWordMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 4
    .parameter "lang"
    .parameter "token"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 290
    .local p3, tokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->preprocMapData:Lcom/google/android/apps/translatedecoder/util/StringMap;

    invoke-static {p1, p2}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->getMapSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/translatedecoder/util/StringMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, res:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 292
    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 294
    const-string v2, "\\s+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, words:[Ljava/lang/String;
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 296
    const/4 v2, 0x1

    .line 298
    .end local v1           #words:[Ljava/lang/String;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isAbbrev(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .parameter "lang"
    .parameter "token"

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->preprocAbbrData:Lcom/google/android/apps/translatedecoder/util/StringSet;

    invoke-static {p1, p2}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->getAbbrSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translatedecoder/util/StringSet;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isNumber(Ljava/lang/String;)Z
    .locals 1
    .parameter "token"

    .prologue
    .line 245
    const-string v0, "-*[\\d]+\\.[\\d]+"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSpecialPunct(Ljava/lang/String;)Z
    .locals 1
    .parameter "token"

    .prologue
    .line 241
    const-string v0, "&apos;"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "&quot;"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public replaceSpecialPuncts(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "in"

    .prologue
    .line 236
    const-string v1, "&apos;"

    const-string v2, " &apos; "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, in2:Ljava/lang/String;
    const-string v1, "&quot;"

    const-string v2, " &quot; "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public tokenize(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 17
    .parameter "lang"
    .parameter "in"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->isCJKT(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 106
    invoke-virtual/range {p0 .. p2}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->tokenizeForCJK(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 187
    :cond_0
    return-object v10

    .line 110
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->charNormalizer:Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;->normalizeChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "\\s+"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 113
    .local v14, words1:[Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v10, res:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .local v12, tem:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    move-object v2, v14

    .local v2, arr$:[Ljava/lang/String;
    array-length v6, v2

    .local v6, len$:I
    const/4 v5, 0x0

    .local v5, i$:I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v13, v2, v5

    .line 116
    .local v13, word:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->isAbbrev(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_2

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->isNumber(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_2

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->isSpecialPunct(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 118
    :cond_2
    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_3
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 119
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v12}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->getWordMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 121
    invoke-interface {v10, v12}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 124
    :cond_5
    const/4 v11, 0x0

    .line 127
    .local v11, sb:Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    .line 128
    .local v9, prev:C
    const/4 v7, 0x0

    .line 129
    .local v7, next:I
    const/4 v8, 0x0

    .local v8, offset:I
    :goto_2
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    if-ge v8, v15, :cond_10

    .line 130
    invoke-virtual {v13, v8}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    .line 131
    .local v4, codepoint:I
    new-instance v3, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v15

    invoke-direct {v3, v15}, Ljava/lang/String;-><init>([C)V

    .line 132
    .local v3, c:Ljava/lang/String;
    const-string v15, "^\\p{Punct}$"

    invoke-virtual {v3, v15}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 135
    const-string v15, "\'"

    invoke-virtual {v3, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v15

    if-nez v15, :cond_c

    .line 136
    if-eqz v11, :cond_7

    .line 137
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v11, v15}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v9

    .line 141
    :goto_3
    invoke-static {v4}, Ljava/lang/Character;->charCount(I)I

    move-result v15

    add-int/2addr v15, v8

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 142
    invoke-static {v4}, Ljava/lang/Character;->charCount(I)I

    move-result v15

    add-int/2addr v15, v8

    invoke-virtual {v13, v15}, Ljava/lang/String;->codePointAt(I)I

    move-result v7

    .line 146
    :goto_4
    const/16 v15, 0x6c

    if-eq v9, v15, :cond_6

    const/16 v15, 0x4c

    if-eq v9, v15, :cond_6

    const/16 v15, 0x64

    if-eq v9, v15, :cond_6

    const/16 v15, 0x44

    if-ne v9, v15, :cond_9

    .line 148
    :cond_6
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v10, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    const/4 v11, 0x0

    .line 180
    :goto_5
    invoke-static {v4}, Ljava/lang/Character;->charCount(I)I

    move-result v15

    add-int/2addr v8, v15

    .line 181
    goto :goto_2

    .line 139
    :cond_7
    const/4 v9, 0x0

    goto :goto_3

    .line 144
    :cond_8
    const/4 v7, 0x0

    goto :goto_4

    .line 152
    :cond_9
    const/16 v15, 0x73

    if-ne v7, v15, :cond_a

    .line 154
    if-eqz v11, :cond_a

    .line 156
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v10, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    const/4 v11, 0x0

    .line 160
    :cond_a
    if-nez v11, :cond_b

    .line 161
    new-instance v11, Ljava/lang/StringBuilder;

    .end local v11           #sb:Ljava/lang/StringBuilder;
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 163
    .restart local v11       #sb:Ljava/lang/StringBuilder;
    :cond_b
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 166
    :cond_c
    if-eqz v11, :cond_d

    .line 168
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v10, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_d
    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    const/4 v11, 0x0

    goto :goto_5

    .line 175
    :cond_e
    if-nez v11, :cond_f

    .line 176
    new-instance v11, Ljava/lang/StringBuilder;

    .end local v11           #sb:Ljava/lang/StringBuilder;
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    .restart local v11       #sb:Ljava/lang/StringBuilder;
    :cond_f
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 182
    .end local v3           #c:Ljava/lang/String;
    .end local v4           #codepoint:I
    :cond_10
    if-eqz v11, :cond_3

    .line 183
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v10, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public tokenizeForCJK(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .parameter "lang"
    .parameter "in"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v10, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->charNormalizer:Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;

    invoke-virtual {v10, p2}, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;->normalizeChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "\\s+"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 199
    .local v9, words:[Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v7, tokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, v9

    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_0
    if-ge v3, v4, :cond_5

    aget-object v8, v0, v3

    .line 201
    .local v8, word:Ljava/lang/String;
    const/4 v6, 0x0

    .line 204
    .local v6, token:Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, offset:I
    :goto_1
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v5, v10, :cond_3

    .line 205
    invoke-virtual {v8, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    .line 206
    .local v2, codepoint:I
    invoke-static {v2}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, c:Ljava/lang/String;
    const/16 v10, 0x7f

    if-le v2, v10, :cond_1

    .line 208
    if-eqz v6, :cond_0

    .line 210
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    :cond_0
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    const/4 v6, 0x0

    .line 221
    :goto_2
    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v10

    add-int/2addr v5, v10

    .line 222
    goto :goto_1

    .line 216
    :cond_1
    if-nez v6, :cond_2

    .line 217
    new-instance v6, Ljava/lang/StringBuilder;

    .end local v6           #token:Ljava/lang/StringBuilder;
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .restart local v6       #token:Ljava/lang/StringBuilder;
    :cond_2
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 223
    .end local v1           #c:Ljava/lang/String;
    .end local v2           #codepoint:I
    :cond_3
    if-eqz v6, :cond_4

    .line 224
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 227
    .end local v5           #offset:I
    .end local v6           #token:Ljava/lang/StringBuilder;
    .end local v8           #word:Ljava/lang/String;
    :cond_5
    return-object v7
.end method

.method public tokenizeWithJoin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "lang"
    .parameter "in"

    .prologue
    .line 79
    const/4 v2, 0x0

    .line 81
    .local v2, words:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, p2}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->replaceSpecialPuncts(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->tokenize(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .local v1, sb:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 87
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 89
    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public writeToByteBuffer(Ljava/nio/ByteBuffer;)V
    .locals 1
    .parameter "buf"

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->preprocAbbrData:Lcom/google/android/apps/translatedecoder/util/StringSet;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translatedecoder/util/StringSet;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->preprocMapData:Lcom/google/android/apps/translatedecoder/util/StringMap;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translatedecoder/util/StringMap;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->charNormalizer:Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translatedecoder/preprocess/CharNormalizer;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 361
    return-void
.end method

.method public writeToFile(Ljava/lang/String;)V
    .locals 1
    .parameter "file"

    .prologue
    .line 311
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->writeToFile(Ljava/lang/String;Z)V

    .line 312
    return-void
.end method

.method public writeToFile(Ljava/lang/String;Z)V
    .locals 10
    .parameter "file"
    .parameter "mmapFormat"

    .prologue
    .line 316
    if-eqz p2, :cond_0

    .line 317
    :try_start_0
    new-instance v8, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v8, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    .local v8, fout:Ljava/io/RandomAccessFile;
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 319
    .local v0, channel:Ljava/nio/channels/FileChannel;
    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x5f5e100

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v6

    .line 321
    .local v6, buf:Ljava/nio/ByteBuffer;
    invoke-static {}, Lcom/google/android/apps/translatedecoder/util/MemMapUtil;->mmapFileSignature()I

    move-result v1

    invoke-virtual {v6, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 322
    invoke-virtual {p0, v6}, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 323
    sget-object v1, Lcom/google/android/apps/translatedecoder/preprocess/Tokenizer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Final buffer size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 324
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;

    .line 325
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 334
    .end local v0           #channel:Ljava/nio/channels/FileChannel;
    .end local v6           #buf:Ljava/nio/ByteBuffer;
    .end local v8           #fout:Ljava/io/RandomAccessFile;
    :goto_0
    return-void

    .line 327
    :cond_0
    new-instance v9, Ljava/io/ObjectOutputStream;

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v9, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 328
    .local v9, out:Ljava/io/ObjectOutputStream;
    invoke-virtual {v9, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 329
    invoke-virtual {v9}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 331
    .end local v9           #out:Ljava/io/ObjectOutputStream;
    :catch_0
    move-exception v7

    .line 332
    .local v7, ex:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
