.class public Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;
.super Ljava/lang/Object;
.source "CommonProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    invoke-direct {v0}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    return-object v0
.end method

.method public setConfigFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;
    .locals 1
    .parameter "configFile"

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->configFile:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->access$002(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 27
    return-object p0
.end method

.method public setPreprocFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;
    .locals 1
    .parameter "preprocFile"

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->preprocFile:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->access$102(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 32
    return-object p0
.end method

.method public setVersion(I)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;
    .locals 1
    .parameter "version"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->version:I
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->access$202(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;I)I

    .line 37
    return-object p0
.end method
