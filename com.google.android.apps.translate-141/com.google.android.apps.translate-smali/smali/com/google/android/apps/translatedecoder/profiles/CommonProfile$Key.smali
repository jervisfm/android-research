.class final enum Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;
.super Ljava/lang/Enum;
.source "CommonProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Key"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

.field public static final enum CONFIG:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

.field public static final enum PREPROC:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

.field public static final enum VERSION:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    const-string v1, "CONFIG"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->CONFIG:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    const-string v1, "PREPROC"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->PREPROC:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    const-string v1, "VERSION"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->CONFIG:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->PREPROC:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->$VALUES:[Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;
    .locals 1
    .parameter "name"

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->$VALUES:[Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    invoke-virtual {v0}, [Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    return-object v0
.end method
