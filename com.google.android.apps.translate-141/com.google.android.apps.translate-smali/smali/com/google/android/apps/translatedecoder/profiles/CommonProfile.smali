.class public Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
.super Ljava/lang/Object;
.source "CommonProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;,
        Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;
    }
.end annotation


# instance fields
.field private configFile:Ljava/lang/String;

.field private preprocFile:Ljava/lang/String;

.field private version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->configFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->preprocFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput p1, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->version:I

    return p1
.end method

.method public static newBuilder()Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;-><init>()V

    return-object v0
.end method

.method public static readFrom(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    .locals 3
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    .line 59
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->newBuilder()Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->CONFIG:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->setConfigFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->PREPROC:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->setPreprocFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->setVersion(I)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Builder;->build()Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, e:Lorg/json/JSONException;
    new-instance v1, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    const-string v2, "Invalid format found when reading profile."

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public getConfigFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->configFile:Ljava/lang/String;

    return-object v0
.end method

.method public getJsonObject()Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 51
    .local v0, json:Lorg/json/JSONObject;
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->CONFIG:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->configFile:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 52
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->PREPROC:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->preprocFile:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 53
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->version:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 54
    return-object v0
.end method

.method public getPreprocFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->preprocFile:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->version:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 84
    :goto_0
    return-object v1

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, e:Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "configFile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->configFile:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
