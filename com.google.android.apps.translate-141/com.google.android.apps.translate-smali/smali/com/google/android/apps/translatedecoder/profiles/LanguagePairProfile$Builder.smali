.class public Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;
.super Ljava/lang/Object;
.source "LanguagePairProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    invoke-direct {v0}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    return-object v0
.end method

.method public setConfigFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;
    .locals 1
    .parameter "configFile"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->configFile:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->access$202(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public setFromLangCode(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;
    .locals 1
    .parameter "fromLangCode"

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->fromLangCode:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->access$002(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 30
    return-object p0
.end method

.method public setPtSymbolFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;
    .locals 1
    .parameter "ptSymbolFile"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->ptSymbolFile:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->access$302(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public setTmFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;
    .locals 1
    .parameter "tmFile"

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->tmFile:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->access$402(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 50
    return-object p0
.end method

.method public setToLangCode(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;
    .locals 1
    .parameter "toLangCode"

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->toLangCode:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->access$102(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public setVersion(I)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;
    .locals 1
    .parameter "version"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->version:I
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->access$502(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;I)I

    .line 55
    return-object p0
.end method
