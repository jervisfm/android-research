.class final enum Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;
.super Ljava/lang/Enum;
.source "LanguagePairProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Key"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

.field public static final enum CONFIG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

.field public static final enum FROMLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

.field public static final enum PTSYMBOL:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

.field public static final enum TM:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

.field public static final enum TOLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

.field public static final enum VERSION:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    const-string v1, "FROMLANG"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->FROMLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    const-string v1, "TOLANG"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->TOLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    const-string v1, "CONFIG"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->CONFIG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    const-string v1, "PTSYMBOL"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->PTSYMBOL:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    const-string v1, "TM"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->TM:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    const-string v1, "VERSION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->FROMLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->TOLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->CONFIG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->PTSYMBOL:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->TM:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->$VALUES:[Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;
    .locals 1
    .parameter "name"

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->$VALUES:[Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v0}, [Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    return-object v0
.end method
