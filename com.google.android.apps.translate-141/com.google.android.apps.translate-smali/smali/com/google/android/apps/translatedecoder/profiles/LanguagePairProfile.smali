.class public Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
.super Ljava/lang/Object;
.source "LanguagePairProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;,
        Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;
    }
.end annotation


# instance fields
.field private configFile:Ljava/lang/String;

.field private fromLangCode:Ljava/lang/String;

.field private ptSymbolFile:Ljava/lang/String;

.field private tmFile:Ljava/lang/String;

.field private toLangCode:Ljava/lang/String;

.field private version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->fromLangCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->toLangCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->configFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->ptSymbolFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->tmFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->version:I

    return p1
.end method

.method public static newBuilder()Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;-><init>()V

    return-object v0
.end method

.method public static readFrom(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    .locals 3
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    .line 80
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->newBuilder()Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->FROMLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->setFromLangCode(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->TOLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->setToLangCode(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->CONFIG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->setConfigFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->PTSYMBOL:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->setPtSymbolFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->TM:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->setTmFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->setVersion(I)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Builder;->build()Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, e:Lorg/json/JSONException;
    new-instance v1, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    const-string v2, "Invalid format found when reading profile."

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public getConfigFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->configFile:Ljava/lang/String;

    return-object v0
.end method

.method public getFromLangCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->fromLangCode:Ljava/lang/String;

    return-object v0
.end method

.method public getJsonObject()Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 69
    .local v0, json:Lorg/json/JSONObject;
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->FROMLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->fromLangCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->TOLANG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->toLangCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 71
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->CONFIG:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->configFile:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->PTSYMBOL:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->ptSymbolFile:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 73
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->TM:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->tmFile:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 74
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->version:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 75
    return-object v0
.end method

.method public getPtSymbolFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->ptSymbolFile:Ljava/lang/String;

    return-object v0
.end method

.method public getTmFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->tmFile:Ljava/lang/String;

    return-object v0
.end method

.method public getToLangCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->toLangCode:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->version:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 119
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 121
    :goto_0
    return-object v1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, e:Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fromLangCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->fromLangCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " toLangCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->toLangCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
