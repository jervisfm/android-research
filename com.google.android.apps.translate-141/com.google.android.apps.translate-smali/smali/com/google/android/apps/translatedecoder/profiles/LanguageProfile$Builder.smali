.class public Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;
.super Ljava/lang/Object;
.source "LanguageProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    invoke-direct {v0}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    return-object v0
.end method

.method public setLangCode(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;
    .locals 1
    .parameter "langCode"

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->langCode:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->access$002(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 28
    return-object p0
.end method

.method public setLmFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;
    .locals 1
    .parameter "lmFile"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->lmFile:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->access$202(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 38
    return-object p0
.end method

.method public setLmSymbolFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;
    .locals 1
    .parameter "lmSymbolFile"

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->lmSymbolFile:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->access$102(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 33
    return-object p0
.end method

.method public setVersion(I)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;
    .locals 1
    .parameter "version"

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->version:I
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->access$302(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;I)I

    .line 43
    return-object p0
.end method
