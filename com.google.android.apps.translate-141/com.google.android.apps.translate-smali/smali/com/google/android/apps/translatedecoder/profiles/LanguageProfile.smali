.class public Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
.super Ljava/lang/Object;
.source "LanguageProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;,
        Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;
    }
.end annotation


# instance fields
.field private langCode:Ljava/lang/String;

.field private lmFile:Ljava/lang/String;

.field private lmSymbolFile:Ljava/lang/String;

.field private version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->langCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->lmSymbolFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->lmFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 13
    iput p1, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->version:I

    return p1
.end method

.method public static newBuilder()Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;-><init>()V

    return-object v0
.end method

.method public static readFrom(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    .locals 3
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    .line 66
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->newBuilder()Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->LANG:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->setLangCode(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->LMSYMBOL:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->setLmSymbolFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->LM:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->setLmFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;

    invoke-virtual {v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->setVersion(I)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Builder;->build()Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, e:Lorg/json/JSONException;
    new-instance v1, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    const-string v2, "Invalid format found when reading profile."

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public getJsonObject()Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 57
    .local v0, json:Lorg/json/JSONObject;
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->LANG:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->langCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 58
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->LMSYMBOL:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->lmSymbolFile:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 59
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->LM:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->lmFile:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;

    invoke-virtual {v1}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile$Key;->name()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->version:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 61
    return-object v0
.end method

.method public getLangCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->langCode:Ljava/lang/String;

    return-object v0
.end method

.method public getLmFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->lmFile:Ljava/lang/String;

    return-object v0
.end method

.method public getLmSymbolFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->lmSymbolFile:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->version:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 94
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 96
    :goto_0
    return-object v1

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, e:Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "langCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->langCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
