.class public Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
.super Ljava/lang/Object;
.source "OfflineDataProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private profile:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    invoke-direct {v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->profile:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    return-void
.end method


# virtual methods
.method public addLanguagePairProfiles(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    .locals 1
    .parameter "languagePairProfile"

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->profile:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    #getter for: Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languagePairProfiles:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->access$200(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 48
    return-object p0
.end method

.method public addLanguageProfiles(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    .locals 1
    .parameter "languageProfile"

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->profile:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    #getter for: Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languageProfiles:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->access$100(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    return-object p0
.end method

.method public build()Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->profile:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    return-object v0
.end method

.method public setCommonProfile(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    .locals 1
    .parameter "commonProfile"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->profile:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    #setter for: Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    invoke-static {v0, p1}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->access$002(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    .line 38
    return-object p0
.end method
