.class final enum Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;
.super Ljava/lang/Enum;
.source "OfflineDataProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Key"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

.field public static final enum COMMON:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

.field public static final enum LANG:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

.field public static final enum LANGPAIR:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

.field public static final enum VERSION:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    const-string v1, "COMMON"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->COMMON:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    const-string v1, "LANG"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->LANG:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    const-string v1, "LANGPAIR"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->LANGPAIR:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    const-string v1, "VERSION"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->COMMON:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->LANG:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->LANGPAIR:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->VERSION:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->$VALUES:[Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;
    .locals 1
    .parameter "name"

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->$VALUES:[Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    invoke-virtual {v0}, [Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    return-object v0
.end method
