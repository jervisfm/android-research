.class public Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
.super Ljava/lang/Object;
.source "OfflineDataProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;,
        Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;
    }
.end annotation


# static fields
.field private static final BUFFSIZE:I = 0x400

.field public static final JSON_INDENT:I = 0x2


# instance fields
.field private commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

.field private languagePairProfiles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;",
            ">;"
        }
    .end annotation
.end field

.field private languageProfiles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languageProfiles:Ljava/util/Set;

    .line 26
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languagePairProfiles:Ljava/util/Set;

    .line 33
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;)Ljava/util/Set;
    .locals 1
    .parameter "x0"

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languageProfiles:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;)Ljava/util/Set;
    .locals 1
    .parameter "x0"

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languagePairProfiles:Ljava/util/Set;

    return-object v0
.end method

.method public static getProfileString(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;)Ljava/lang/String;
    .locals 4
    .parameter "profile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    .line 181
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    .line 182
    .local v1, json:Lorg/json/JSONObject;
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 183
    .end local v1           #json:Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 184
    .local v0, e:Lorg/json/JSONException;
    new-instance v2, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    const-string v3, "Invalid format found when writing profile."

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static newBuilder()Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;-><init>()V

    return-object v0
.end method

.method public static readFrom(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    .locals 9
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    .line 139
    :try_start_0
    sget-object v7, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->COMMON:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    invoke-virtual {v7}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->readFrom(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 143
    .local v1, commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    invoke-static {}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->newBuilder()Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->setCommonProfile(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;

    move-result-object v0

    .line 148
    .local v0, builder:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    :try_start_1
    sget-object v7, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->LANG:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    invoke-virtual {v7}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 149
    .local v4, jsonArray:Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 151
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->readFrom(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    move-result-object v6

    .line 152
    .local v6, langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    invoke-virtual {v0, v6}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->addLanguageProfiles(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 149
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 140
    .end local v0           #builder:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    .end local v1           #commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    .end local v3           #i:I
    .end local v4           #jsonArray:Lorg/json/JSONArray;
    .end local v6           #langProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    :catch_0
    move-exception v2

    .line 141
    .local v2, e:Lorg/json/JSONException;
    new-instance v7, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    const-string v8, "Missing common profile."

    invoke-direct {v7, v8, v2}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 154
    .end local v2           #e:Lorg/json/JSONException;
    .restart local v0       #builder:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    .restart local v1       #commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    :catch_1
    move-exception v2

    .line 155
    .restart local v2       #e:Lorg/json/JSONException;
    new-instance v7, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    const-string v8, "Missing language profile."

    invoke-direct {v7, v8, v2}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 159
    .end local v2           #e:Lorg/json/JSONException;
    .restart local v3       #i:I
    .restart local v4       #jsonArray:Lorg/json/JSONArray;
    :cond_0
    :try_start_2
    sget-object v7, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->LANGPAIR:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    invoke-virtual {v7}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 160
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 162
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->readFrom(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    move-result-object v5

    .line 163
    .local v5, langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    invoke-virtual {v0, v5}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->addLanguagePairProfiles(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 160
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 165
    .end local v5           #langPairProfile:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    :catch_2
    move-exception v2

    .line 166
    .restart local v2       #e:Lorg/json/JSONException;
    new-instance v7, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    const-string v8, "Missing language pair profile."

    invoke-direct {v7, v8, v2}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 168
    .end local v2           #e:Lorg/json/JSONException;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;->build()Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;

    move-result-object v7

    return-object v7
.end method

.method public static readFromFile(Ljava/lang/String;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    .locals 6
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {p0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->readTextFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 102
    .local v2, jsonString:Ljava/lang/String;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 103
    .local v1, json:Lorg/json/JSONObject;
    invoke-static {v1}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->readFrom(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 104
    .end local v1           #json:Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 105
    .local v0, e:Lorg/json/JSONException;
    new-instance v3, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid format found when reading profile. jsonString="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method private static readTextFrom(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 9
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x400

    .line 112
    const/4 v3, 0x0

    .line 113
    .local v3, offset:I
    new-array v0, v6, [B

    .line 115
    .local v0, buffer:[B
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .local v4, sb:Ljava/lang/StringBuilder;
    :cond_0
    const/16 v6, 0x400

    :try_start_0
    invoke-virtual {p0, v0, v3, v6}, Ljava/io/DataInputStream;->read([BII)I

    move-result v5

    .local v5, sz:I
    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 118
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v5, :cond_0

    .line 119
    aget-byte v6, v0, v2

    int-to-char v6, v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 122
    .end local v2           #i:I
    .end local v5           #sz:I
    :catch_0
    move-exception v1

    .line 123
    .local v1, e:Ljava/io/IOException;
    new-instance v6, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "I/O error happened when reading profile. offset="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v1}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 126
    .end local v1           #e:Ljava/io/IOException;
    .restart local v5       #sz:I
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static readTextFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    .line 80
    const/4 v1, 0x0

    .line 82
    .local v1, in:Ljava/io/DataInputStream;
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 83
    .end local v1           #in:Ljava/io/DataInputStream;
    .local v2, in:Ljava/io/DataInputStream;
    :try_start_1
    invoke-static {v2}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->readTextFrom(Ljava/io/DataInputStream;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v3

    .line 88
    if-eqz v2, :cond_0

    .line 89
    :try_start_2
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 92
    :cond_0
    return-object v3

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, e:Ljava/io/IOException;
    new-instance v3, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "I/O error happened when closing profile. file="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 84
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #in:Ljava/io/DataInputStream;
    .restart local v1       #in:Ljava/io/DataInputStream;
    :catch_1
    move-exception v0

    .line 85
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_0
    :try_start_3
    new-instance v3, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File not found. file="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 87
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v3

    .line 88
    :goto_1
    if-eqz v1, :cond_1

    .line 89
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 92
    :cond_1
    throw v3

    .line 91
    :catch_2
    move-exception v0

    .line 92
    .local v0, e:Ljava/io/IOException;
    new-instance v3, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "I/O error happened when closing profile. file="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 87
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #in:Ljava/io/DataInputStream;
    .restart local v2       #in:Ljava/io/DataInputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2           #in:Ljava/io/DataInputStream;
    .restart local v1       #in:Ljava/io/DataInputStream;
    goto :goto_1

    .line 84
    .end local v1           #in:Ljava/io/DataInputStream;
    .restart local v2       #in:Ljava/io/DataInputStream;
    :catch_3
    move-exception v0

    move-object v1, v2

    .end local v2           #in:Ljava/io/DataInputStream;
    .restart local v1       #in:Ljava/io/DataInputStream;
    goto :goto_0
.end method

.method public static writeTo(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;Ljava/io/DataOutputStream;)V
    .locals 3
    .parameter "profile"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;
        }
    .end annotation

    .prologue
    .line 198
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->getProfileString(Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;

    const-string v2, "I/O error happened when writing profile"

    invoke-direct {v1, v2}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfileException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public getCommonProfile()Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    return-object v0
.end method

.method public getJsonObject()Lorg/json/JSONObject;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 61
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 62
    .local v1, json:Lorg/json/JSONObject;
    sget-object v5, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->COMMON:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    invoke-virtual {v5}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->commonProfile:Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;

    invoke-virtual {v6}, Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 64
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 65
    .local v3, langProfiles:Lorg/json/JSONArray;
    iget-object v5, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languageProfiles:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;

    .line 66
    .local v4, p:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    invoke-virtual {v4}, Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 68
    .end local v4           #p:Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    :cond_0
    sget-object v5, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->LANG:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    invoke-virtual {v5}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 71
    .local v2, langPairProfiles:Lorg/json/JSONArray;
    iget-object v5, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languagePairProfiles:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;

    .line 72
    .local v4, p:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    invoke-virtual {v4}, Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 74
    .end local v4           #p:Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    :cond_1
    sget-object v5, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->LANGPAIR:Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;

    invoke-virtual {v5}, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 76
    return-object v1
.end method

.method public getLanguagePairProfiles()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languagePairProfiles:Ljava/util/Set;

    return-object v0
.end method

.method public getLanguageProfiles()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;->languageProfiles:Ljava/util/Set;

    return-object v0
.end method
