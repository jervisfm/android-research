.class public Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;
.super Ljava/lang/Object;
.source "TargetPhrasesContainer.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x57b4a9a953c0e42L


# instance fields
.field private final data:Lcom/google/android/apps/translatedecoder/util/BitData;

.field private final dictTagWordId:I

.field private final quantizer:Lcom/google/android/apps/translatedecoder/succinct/Quantizer;

.field private final valLen:I

.field private final wordIdLen:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/translatedecoder/util/BitData;IILcom/google/android/apps/translatedecoder/succinct/Quantizer;I)V
    .locals 0
    .parameter "data"
    .parameter "wordIdLen"
    .parameter "valLen"
    .parameter "quantizer"
    .parameter "dictTagWordId"

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->data:Lcom/google/android/apps/translatedecoder/util/BitData;

    .line 64
    iput p2, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->wordIdLen:I

    .line 65
    iput p3, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->valLen:I

    .line 66
    iput-object p4, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->quantizer:Lcom/google/android/apps/translatedecoder/succinct/Quantizer;

    .line 67
    iput p5, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->dictTagWordId:I

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/util/BitSet;IILcom/google/android/apps/translatedecoder/succinct/Quantizer;)V
    .locals 6
    .parameter "data"
    .parameter "wordIdLen"
    .parameter "valLen"
    .parameter "quantizer"

    .prologue
    .line 77
    new-instance v1, Lcom/google/android/apps/translatedecoder/util/BitSetBasedBitData;

    invoke-direct {v1, p1}, Lcom/google/android/apps/translatedecoder/util/BitSetBasedBitData;-><init>(Ljava/util/BitSet;)V

    const/4 v5, -0x1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;-><init>(Lcom/google/android/apps/translatedecoder/util/BitData;IILcom/google/android/apps/translatedecoder/succinct/Quantizer;I)V

    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/util/BitSet;IILcom/google/android/apps/translatedecoder/succinct/Quantizer;I)V
    .locals 6
    .parameter "data"
    .parameter "wordIdLen"
    .parameter "valLen"
    .parameter "quantizer"
    .parameter "dictTagWordId"

    .prologue
    .line 72
    new-instance v1, Lcom/google/android/apps/translatedecoder/util/BitSetBasedBitData;

    invoke-direct {v1, p1}, Lcom/google/android/apps/translatedecoder/util/BitSetBasedBitData;-><init>(Ljava/util/BitSet;)V

    move-object v0, p0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;-><init>(Lcom/google/android/apps/translatedecoder/util/BitData;IILcom/google/android/apps/translatedecoder/succinct/Quantizer;I)V

    .line 73
    return-void
.end method

.method private addTargetPhrase(Lcom/google/android/apps/translatedecoder/util/BitData;I[IIZ)I
    .locals 7
    .parameter "data"
    .parameter "bitOffSet"
    .parameter "words"
    .parameter "value"
    .parameter "lastPhrase"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 107
    move v1, p2

    .line 108
    .local v1, res:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v3, p3

    if-ge v0, v3, :cond_1

    .line 109
    aget v3, p3, v0

    iget v6, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->wordIdLen:I

    invoke-static {v3, p1, v1, v6}, Lcom/google/android/apps/translatedecoder/util/BitsUtil;->intToBitSet(ILcom/google/android/apps/translatedecoder/util/BitData;II)V

    .line 110
    iget v3, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->wordIdLen:I

    add-int/2addr v1, v3

    .line 111
    add-int/lit8 v2, v1, 0x1

    .end local v1           #res:I
    .local v2, res:I
    array-length v3, p3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    move v3, v4

    :goto_1
    invoke-virtual {p1, v1, v3}, Lcom/google/android/apps/translatedecoder/util/BitData;->set(IZ)V

    .line 108
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2           #res:I
    .restart local v1       #res:I
    goto :goto_0

    .end local v1           #res:I
    .restart local v2       #res:I
    :cond_0
    move v3, v5

    .line 111
    goto :goto_1

    .line 113
    .end local v2           #res:I
    .restart local v1       #res:I
    :cond_1
    iget v3, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->valLen:I

    invoke-static {p4, p1, v1, v3}, Lcom/google/android/apps/translatedecoder/util/BitsUtil;->intToBitSet(ILcom/google/android/apps/translatedecoder/util/BitData;II)V

    .line 114
    iget v3, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->valLen:I

    add-int/2addr v1, v3

    .line 115
    add-int/lit8 v2, v1, 0x1

    .end local v1           #res:I
    .restart local v2       #res:I
    if-nez p5, :cond_2

    :goto_2
    invoke-virtual {p1, v1, v4}, Lcom/google/android/apps/translatedecoder/util/BitData;->set(IZ)V

    .line 116
    return v2

    :cond_2
    move v4, v5

    .line 115
    goto :goto_2
.end method

.method private static findDictInfoStartPos(I[I)I
    .locals 2
    .parameter "dictTagWordId"
    .parameter "tgtWords"

    .prologue
    .line 190
    const/4 v0, 0x0

    .line 191
    .local v0, dictInfoStartPos:I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 192
    aget v1, p1, v0

    if-ne v1, p0, :cond_0

    .line 196
    .end local v0           #dictInfoStartPos:I
    :goto_1
    return v0

    .line 191
    .restart local v0       #dictInfoStartPos:I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;
    .locals 6
    .parameter "buf"

    .prologue
    .line 178
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 179
    .local v2, wordIdLen:I
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    .line 180
    .local v3, valLen:I
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 181
    .local v5, dictTagWordId:I
    invoke-static {p0}, Lcom/google/android/apps/translatedecoder/succinct/Quantizer;->readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/succinct/Quantizer;

    move-result-object v4

    .line 182
    .local v4, quantizer:Lcom/google/android/apps/translatedecoder/succinct/Quantizer;
    invoke-static {p0}, Lcom/google/android/apps/translatedecoder/util/BitData;->readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/util/BitData;

    move-result-object v1

    .line 184
    .local v1, data:Lcom/google/android/apps/translatedecoder/util/BitData;
    new-instance v0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;-><init>(Lcom/google/android/apps/translatedecoder/util/BitData;IILcom/google/android/apps/translatedecoder/succinct/Quantizer;I)V

    return-object v0
.end method


# virtual methods
.method public addTargetPhrases(Ljava/util/List;I)I
    .locals 9
    .parameter
    .parameter "byteOffset"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translatedecoder/pt/PhrasePair;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, phrases:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/pt/PhrasePair;>;"
    mul-int/lit8 v2, p2, 0x8

    .line 92
    .local v2, bitOffset:I
    const/4 v6, 0x0

    .local v6, j:I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 93
    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->data:Lcom/google/android/apps/translatedecoder/util/BitData;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;

    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->tgtWords()[I

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->quantizer:Lcom/google/android/apps/translatedecoder/succinct/Quantizer;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;

    invoke-virtual {v0}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;->cost()D

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Lcom/google/android/apps/translatedecoder/succinct/Quantizer;->getUnit(D)I

    move-result v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt v6, v0, :cond_0

    const/4 v5, 0x1

    :goto_1
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->addTargetPhrase(Lcom/google/android/apps/translatedecoder/util/BitData;I[IIZ)I

    move-result v2

    .line 92
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 93
    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 97
    :cond_1
    :goto_2
    rem-int/lit8 v0, v2, 0x8

    if-eqz v0, :cond_2

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->data:Lcom/google/android/apps/translatedecoder/util/BitData;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/translatedecoder/util/BitData;->set(I)V

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 102
    :cond_2
    div-int/lit8 v0, v2, 0x8

    return v0
.end method

.method public convertToPhrases([II)Ljava/util/List;
    .locals 14
    .parameter "srcWords"
    .parameter "byteOffSet"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/translatedecoder/pt/PhrasePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v11, res:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/translatedecoder/pt/PhrasePair;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v12, targetWords:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    mul-int/lit8 v9, p2, 0x8

    .line 130
    .local v9, pos:I
    const/4 v7, 0x1

    .line 131
    .local v7, hasMorePhrasess:Z
    :goto_0
    if-eqz v7, :cond_4

    .line 133
    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 135
    const/4 v8, 0x1

    .line 136
    .local v8, hasMoreWords:Z
    :goto_1
    if-eqz v8, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->data:Lcom/google/android/apps/translatedecoder/util/BitData;

    iget v1, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->wordIdLen:I

    invoke-static {v0, v9, v1}, Lcom/google/android/apps/translatedecoder/util/BitsUtil;->bitSetToInt(Lcom/google/android/apps/translatedecoder/util/BitData;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    iget v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->wordIdLen:I

    add-int/2addr v9, v0

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->data:Lcom/google/android/apps/translatedecoder/util/BitData;

    add-int/lit8 v10, v9, 0x1

    .end local v9           #pos:I
    .local v10, pos:I
    invoke-virtual {v0, v9}, Lcom/google/android/apps/translatedecoder/util/BitData;->get(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 140
    const/4 v8, 0x0

    move v9, v10

    .end local v10           #pos:I
    .restart local v9       #pos:I
    goto :goto_1

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->quantizer:Lcom/google/android/apps/translatedecoder/succinct/Quantizer;

    iget-object v1, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->data:Lcom/google/android/apps/translatedecoder/util/BitData;

    iget v2, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->valLen:I

    invoke-static {v1, v9, v2}, Lcom/google/android/apps/translatedecoder/util/BitsUtil;->bitSetToInt(Lcom/google/android/apps/translatedecoder/util/BitData;II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/translatedecoder/succinct/Quantizer;->getValue(I)D

    move-result-wide v4

    .line 145
    .local v4, cost:D
    iget v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->valLen:I

    add-int/2addr v9, v0

    .line 146
    iget v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->dictTagWordId:I

    if-ltz v0, :cond_3

    .line 147
    invoke-static {v12}, Lcom/google/android/apps/translatedecoder/util/Utils;->listToArray(Ljava/util/List;)[I

    move-result-object v13

    .line 149
    .local v13, tgtWords:[I
    iget v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->dictTagWordId:I

    invoke-static {v0, v13}, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->findDictInfoStartPos(I[I)I

    move-result v6

    .line 150
    .local v6, dictInfoStartPos:I
    const/4 v0, -0x1

    if-ne v6, v0, :cond_2

    .line 151
    new-instance v0, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;

    invoke-direct {v0, p1, v13, v4, v5}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;-><init>([I[ID)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    .end local v6           #dictInfoStartPos:I
    .end local v13           #tgtWords:[I
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->data:Lcom/google/android/apps/translatedecoder/util/BitData;

    add-int/lit8 v10, v9, 0x1

    .end local v9           #pos:I
    .restart local v10       #pos:I
    invoke-virtual {v0, v9}, Lcom/google/android/apps/translatedecoder/util/BitData;->get(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    const/4 v7, 0x0

    :cond_1
    move v9, v10

    .line 165
    .end local v10           #pos:I
    .restart local v9       #pos:I
    goto :goto_0

    .line 153
    .restart local v6       #dictInfoStartPos:I
    .restart local v13       #tgtWords:[I
    :cond_2
    new-instance v0, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;

    const/4 v1, 0x0

    invoke-static {v13, v1, v6}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v2

    add-int/lit8 v1, v6, 0x1

    array-length v3, v13

    invoke-static {v13, v1, v3}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v3

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;-><init>([I[I[ID)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 160
    .end local v6           #dictInfoStartPos:I
    .end local v13           #tgtWords:[I
    :cond_3
    new-instance v0, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;

    invoke-static {v12}, Lcom/google/android/apps/translatedecoder/util/Utils;->listToArray(Ljava/util/List;)[I

    move-result-object v1

    invoke-direct {v0, p1, v1, v4, v5}, Lcom/google/android/apps/translatedecoder/pt/PhrasePair;-><init>([I[ID)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 166
    .end local v4           #cost:D
    .end local v8           #hasMoreWords:Z
    :cond_4
    return-object v11

    .end local v9           #pos:I
    .restart local v8       #hasMoreWords:Z
    .restart local v10       #pos:I
    :cond_5
    move v9, v10

    .end local v10           #pos:I
    .restart local v9       #pos:I
    goto :goto_1
.end method

.method public writeToByteBuffer(Ljava/nio/ByteBuffer;)V
    .locals 1
    .parameter "buf"

    .prologue
    .line 170
    iget v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->wordIdLen:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 171
    iget v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->valLen:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 172
    iget v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->dictTagWordId:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->quantizer:Lcom/google/android/apps/translatedecoder/succinct/Quantizer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translatedecoder/succinct/Quantizer;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/translatedecoder/pt/TargetPhrasesContainer;->data:Lcom/google/android/apps/translatedecoder/util/BitData;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/translatedecoder/util/BitData;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 175
    return-void
.end method
