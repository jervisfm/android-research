.class public abstract Lcom/google/android/apps/translatedecoder/util/BitData;
.super Ljava/lang/Object;
.source "BitData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final logger:Ljava/util/logging/Logger; = null

.field private static final serialVersionUID:J = 0x3ae176ad111e5747L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/translatedecoder/util/BitData;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/translatedecoder/util/BitData;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromByteBuffer(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/util/BitData;
    .locals 4
    .parameter "buf"

    .prologue
    .line 41
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 42
    .local v0, classId:I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 43
    invoke-static {p0}, Lcom/google/android/apps/translatedecoder/util/ByteBufferBasedBitData;->readFromByteBufferHelper(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/translatedecoder/util/BitData;

    move-result-object v1

    return-object v1

    .line 45
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown class id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public abstract clear()V
.end method

.method public abstract get(I)Z
.end method

.method public abstract length()I
.end method

.method public abstract nextSetBit(I)I
.end method

.method public set(I)V
    .locals 1
    .parameter "bitIndex"

    .prologue
    .line 29
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/translatedecoder/util/BitData;->set(IZ)V

    .line 30
    return-void
.end method

.method public abstract set(IZ)V
.end method

.method public abstract size()I
.end method

.method public abstract writeToByteBuffer(Ljava/nio/ByteBuffer;)V
.end method
