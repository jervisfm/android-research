.class public abstract Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;
.super Lcom/google/android/apps/unveil/PreviewLoopingActivity;
.source "GogglesPreviewLoopingActivity.java"


# static fields
.field private static final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# instance fields
.field protected application:Lcom/google/android/apps/unveil/UnveilContext;

.field private locationProvider:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;

.field protected sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    sput-object v0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected disableSensors()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->locationProvider:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->off()V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;->off()V

    .line 102
    return-void
.end method

.method protected enableSensors()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->locationProvider:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->on()V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;->on()V

    .line 97
    return-void
.end method

.method public getSettings()Lcom/google/android/apps/unveil/UnveilSettings;
    .locals 3

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 55
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "UnveilSettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "UnveilSettings"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/UnveilSettings;

    .line 58
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected initializeActivity(I)V
    .locals 4
    .parameter "contentView"

    .prologue
    .line 37
    sget-object v1, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "initializeActivity"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    iget-object v1, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->application:Lcom/google/android/apps/unveil/UnveilContext;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/android/apps/unveil/UnveilContext;->setContinuous(Z)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->getSettings()Lcom/google/android/apps/unveil/UnveilSettings;

    move-result-object v0

    .line 43
    .local v0, settings:Lcom/google/android/apps/unveil/UnveilSettings;
    if-eqz v0, :cond_0

    .line 44
    iget-object v1, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->application:Lcom/google/android/apps/unveil/UnveilContext;

    invoke-interface {v1, v0}, Lcom/google/android/apps/unveil/UnveilContext;->setSettings(Lcom/google/android/apps/unveil/UnveilSettings;)V

    .line 47
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->initializeActivity(I)V

    .line 49
    iget-object v1, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->application:Lcom/google/android/apps/unveil/UnveilContext;

    invoke-interface {v1}, Lcom/google/android/apps/unveil/UnveilContext;->getLocationProvider()Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->locationProvider:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;

    .line 50
    iget-object v1, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->application:Lcom/google/android/apps/unveil/UnveilContext;

    invoke-interface {v1}, Lcom/google/android/apps/unveil/UnveilContext;->getSensorProvider()Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    .line 51
    return-void
.end method

.method protected maybeCreateLooper(Landroid/graphics/Matrix;)V
    .locals 3
    .parameter "matrix"

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->maybeCreateLooper(Landroid/graphics/Matrix;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->glCameraPreview:Lcom/google/android/apps/unveil/env/gl/GLCameraPreview;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->application:Lcom/google/android/apps/unveil/UnveilContext;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/UnveilContext;->getSettings()Lcom/google/android/apps/unveil/UnveilSettings;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/unveil/UnveilSettings;->useGLES2Overlay:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    iget-object v1, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->glCameraPreview:Lcom/google/android/apps/unveil/env/gl/GLCameraPreview;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/env/gl/GLCameraPreview;->getFrameLoader()Lcom/google/android/apps/unveil/nonstop/FrameProcessor;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->addPreviewProcessor(Lcom/google/android/apps/unveil/nonstop/FrameProcessor;I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/UnveilContext;

    iput-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->application:Lcom/google/android/apps/unveil/UnveilContext;

    .line 33
    return-void
.end method

.method protected declared-synchronized pauseLooping()V
    .locals 1

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->isRunning()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 83
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 81
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->disableSensors()V

    .line 82
    invoke-super {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->pauseLooping()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized resumeLooping()V
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->isRunning()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 90
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/GogglesPreviewLoopingActivity;->enableSensors()V

    .line 91
    invoke-super {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->resumeLooping()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
