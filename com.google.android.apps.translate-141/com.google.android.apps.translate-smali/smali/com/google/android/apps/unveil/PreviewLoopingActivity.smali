.class public abstract Lcom/google/android/apps/unveil/PreviewLoopingActivity;
.super Landroid/app/Activity;
.source "PreviewLoopingActivity.java"

# interfaces
.implements Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
.implements Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;


# static fields
.field private static final LOOPER_FPS:I = 0x1e

.field private static final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# instance fields
.field protected cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

.field private debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

.field protected glCameraPreview:Lcom/google/android/apps/unveil/env/gl/GLCameraPreview;

.field private glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

.field private isActivityRunning:Z

.field private needsStartCameraPreview:Z

.field private onResumePending:Z

.field private previewDisplayedSize:Lcom/google/android/apps/unveil/env/Size;

.field protected previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

.field private wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    sput-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onResumePending:Z

    return-void
.end method

.method private onResumeInternal()V
    .locals 3

    .prologue
    .line 229
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "onResumeInternal()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCamera()V

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/ui/GlOverlay;->onResume()V

    .line 241
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onLooperRestart()V

    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->isActivityRunning:Z

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->requestLayout()V

    .line 248
    return-void
.end method


# virtual methods
.method protected abstract addFrameProcessors(Lcom/google/android/apps/unveil/nonstop/PreviewLooper;)V
.end method

.method public addGlRenderCallback(Lcom/google/android/apps/unveil/ui/GlOverlay$RenderCallback;)V
    .locals 1
    .parameter "renderCallback"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/ui/GlOverlay;->addRenderCallback(Lcom/google/android/apps/unveil/ui/GlOverlay$RenderCallback;)V

    .line 119
    :cond_0
    return-void
.end method

.method public addRenderCallback(Lcom/google/android/apps/unveil/nonstop/DebugView$RenderCallback;)V
    .locals 1
    .parameter "callback"

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/nonstop/DebugView;->addCallback(Lcom/google/android/apps/unveil/nonstop/DebugView$RenderCallback;)V

    .line 128
    return-void
.end method

.method protected addViewAsCameraOverlay(Landroid/view/View;I)V
    .locals 1
    .parameter "view"
    .parameter "index"

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->addView(Landroid/view/View;I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->requestLayout()V

    .line 108
    return-void
.end method

.method public getRotationAngleBetweenCameraAndDisplay()I
    .locals 3

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->getRequestedOrientation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 142
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled orientation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->getRequestedOrientation()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :pswitch_0
    const/4 v0, 0x0

    .line 139
    :goto_0
    return v0

    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected initializeActivity(I)V
    .locals 5
    .parameter "contentView"

    .prologue
    const/16 v4, 0x400

    const/4 v3, 0x1

    .line 68
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "initializeActivity"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    invoke-virtual {p0, v3}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->requestWindowFeature(I)Z

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 74
    invoke-virtual {p0, p1}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->setContentView(I)V

    .line 76
    sget v0, Lcom/google/android/apps/unveil/R$id;->nonstop_debug_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/nonstop/DebugView;

    iput-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    .line 84
    sget v0, Lcom/google/android/apps/unveil/R$id;->camera_preview:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/sensors/CameraManager;

    iput-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->registerListener(Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;)V

    .line 87
    sget v0, Lcom/google/android/apps/unveil/R$id;->gl_camera_preview_overlay:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/env/gl/GLCameraPreview;

    iput-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glCameraPreview:Lcom/google/android/apps/unveil/env/gl/GLCameraPreview;

    .line 89
    sget v0, Lcom/google/android/apps/unveil/R$id;->camera_wrapper_layout:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    iput-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->setCameraManager(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->setCameraLayoutHandler(Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->getRotationAngleBetweenCameraAndDisplay()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->setRotation(I)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getMirrored()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->setMirrored(Z)V

    .line 96
    sget v0, Lcom/google/android/apps/unveil/R$id;->gl_overlay:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/ui/GlOverlay;

    iput-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/unveil/ui/GlOverlay;->setZOrderMediaOverlay(Z)V

    .line 102
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->needsStartCameraPreview:Z

    .line 103
    return-void
.end method

.method protected declared-synchronized isLoopingPaused()Z
    .locals 1

    .prologue
    .line 365
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->isRunning()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected maybeCreateLooper(Landroid/graphics/Matrix;)V
    .locals 7
    .parameter "frameToCanvas"

    .prologue
    const/4 v6, 0x0

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 180
    :cond_0
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Initializing PreviewLooper."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    new-instance v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->getRotationAngleBetweenCameraAndDisplay()I

    move-result v2

    const/high16 v3, 0x41f0

    const/4 v4, 0x2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;IFILandroid/graphics/Matrix;)V

    iput-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    new-instance v1, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;

    iget-object v2, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    iget-object v5, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;Lcom/google/android/apps/unveil/nonstop/DebugView;Lcom/google/android/apps/unveil/ui/GlOverlay;)V

    invoke-virtual {v0, v1, v6}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->addPreviewProcessor(Lcom/google/android/apps/unveil/nonstop/FrameProcessor;I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->addFrameProcessors(Lcom/google/android/apps/unveil/nonstop/PreviewLooper;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/nonstop/DebugView;->setCallback(Lcom/google/android/apps/unveil/nonstop/PreviewLooper;)V

    goto :goto_0
.end method

.method public onCameraAcquisitionError()V
    .locals 3

    .prologue
    .line 340
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Failed to acquire camera."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    return-void
.end method

.method public onCameraConnected()V
    .locals 3

    .prologue
    .line 331
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Camera connected, requesting final layout before starting preview."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->requestLayout()V

    .line 336
    return-void
.end method

.method public onCameraFlashControlError()V
    .locals 3

    .prologue
    .line 345
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Failed to apply camera flash setting."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 346
    return-void
.end method

.method public onCameraLayoutComplete()V
    .locals 3

    .prologue
    .line 361
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Camera is measured successfully"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    return-void
.end method

.method public onCameraLayoutFinished(Lcom/google/android/apps/unveil/env/Size;Landroid/graphics/Matrix;)V
    .locals 1
    .parameter "previewDisplayedSize"
    .parameter "frameToCanvas"

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->isActivityRunning:Z

    if-nez v0, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewDisplayedSize:Lcom/google/android/apps/unveil/env/Size;

    .line 155
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->needsStartCameraPreview:Z

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->startPreview()V

    .line 157
    invoke-virtual {p0, p2}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->maybeCreateLooper(Landroid/graphics/Matrix;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->resumeLooping()V

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->needsStartCameraPreview:Z

    goto :goto_0
.end method

.method public declared-synchronized onCameraPreviewSizeChanged()V
    .locals 5

    .prologue
    .line 355
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Camera preview size changed to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->wrappingLayout:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    monitor-exit p0

    return-void

    .line 355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCameraQualityError()V
    .locals 3

    .prologue
    .line 350
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Failed to apply camera quality settings."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 64
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 271
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "onDestroy"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    .line 273
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Shutting down preview frame processing."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->shutdown()V

    .line 277
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 278
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    .line 283
    packed-switch p1, :pswitch_data_0

    .line 305
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    .line 286
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/unveil/nonstop/DebugView;->cycleDebugMode(Z)V

    .line 287
    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    if-eqz v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/ui/GlOverlay;->requestRender()V

    goto :goto_0

    .line 296
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/nonstop/DebugView;->cycleDebugMode(Z)V

    .line 297
    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    if-eqz v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/ui/GlOverlay;->requestRender()V

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    .line 312
    packed-switch p1, :pswitch_data_0

    .line 326
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :pswitch_0
    return v0

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onLooperRestart()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->isActivityRunning:Z

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/ui/GlOverlay;->onPause()V

    .line 258
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->pauseLooping()V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isPreviewing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->needsStartCameraPreview:Z

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->forceReleaseCamera()V

    .line 266
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 267
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 217
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->hasWindowFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "onResume. onResumePending=true"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onResumePending:Z

    .line 226
    :goto_0
    return-void

    .line 222
    :cond_0
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "onResume. onResumePending=false"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onResumeInternal()V

    .line 224
    iput-boolean v3, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onResumePending:Z

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 6
    .parameter "hasFocus"

    .prologue
    const/4 v5, 0x0

    .line 208
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "onWindowFocusChanged.hasFocus=%B, .onResumePending=%B"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onResumePending:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onResumePending:Z

    if-eqz v0, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onResumeInternal()V

    .line 211
    iput-boolean v5, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onResumePending:Z

    .line 213
    :cond_0
    return-void
.end method

.method protected declared-synchronized pauseLooping()V
    .locals 3

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->isRunning()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 374
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 372
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Attempting to pause looping."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseLoop()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized resumeLooping()V
    .locals 3

    .prologue
    .line 377
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->isRunning()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 380
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->onLooperRestart()V

    .line 382
    sget-object v0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Attempting to resume looping."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    iget-object v1, p0, Lcom/google/android/apps/unveil/PreviewLoopingActivity;->previewDisplayedSize:Lcom/google/android/apps/unveil/env/Size;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->startLoop(Lcom/google/android/apps/unveil/env/Size;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 377
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
