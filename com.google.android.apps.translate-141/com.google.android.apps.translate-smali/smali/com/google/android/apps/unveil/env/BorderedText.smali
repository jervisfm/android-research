.class public Lcom/google/android/apps/unveil/env/BorderedText;
.super Ljava/lang/Object;
.source "BorderedText.java"


# instance fields
.field private final exteriorPaint:Landroid/graphics/Paint;

.field private final interiorPaint:Landroid/graphics/Paint;

.field private positions:[F

.field private final textSize:F

.field private widths:[F


# direct methods
.method public constructor <init>(F)V
    .locals 2
    .parameter "textSize"

    .prologue
    .line 33
    const/4 v0, -0x1

    const/high16 v1, -0x100

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/unveil/env/BorderedText;-><init>(IIF)V

    .line 34
    return-void
.end method

.method public constructor <init>(IIF)V
    .locals 4
    .parameter "interiorColor"
    .parameter "exteriorColor"
    .parameter "textSize"

    .prologue
    const/16 v3, 0xff

    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 52
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 60
    iput p3, p0, Lcom/google/android/apps/unveil/env/BorderedText;->textSize:F

    .line 61
    return-void
.end method


# virtual methods
.method public drawText(Landroid/graphics/Canvas;FFLjava/lang/String;)V
    .locals 4
    .parameter "canvas"
    .parameter "posX"
    .parameter "posY"
    .parameter "text"

    .prologue
    .line 64
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->widths:[F

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->widths:[F

    array-length v2, v2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 65
    :cond_0
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v2

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->widths:[F

    .line 66
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->positions:[F

    .line 69
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/apps/unveil/env/BorderedText;->widths:[F

    invoke-virtual {v2, p4, v3}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 70
    move v1, p2

    .line 71
    .local v1, lastPosX:F
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->widths:[F

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->positions:[F

    mul-int/lit8 v3, v0, 0x2

    aput v1, v2, v3

    .line 73
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->positions:[F

    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    aput p3, v2, v3

    .line 74
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->widths:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->positions:[F

    iget-object v3, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4, v2, v3}, Landroid/graphics/Canvas;->drawPosText(Ljava/lang/String;[FLandroid/graphics/Paint;)V

    .line 78
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/BorderedText;->positions:[F

    iget-object v3, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4, v2, v3}, Landroid/graphics/Canvas;->drawPosText(Ljava/lang/String;[FLandroid/graphics/Paint;)V

    .line 79
    return-void
.end method

.method public getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V
    .locals 1
    .parameter "line"
    .parameter "index"
    .parameter "count"
    .parameter "lineBounds"

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 101
    return-void
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->textSize:F

    return v0
.end method

.method public setAlpha(I)V
    .locals 1
    .parameter "alpha"

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 96
    return-void
.end method

.method public setExteriorColor(I)V
    .locals 1
    .parameter "color"

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    return-void
.end method

.method public setInteriorColor(I)V
    .locals 1
    .parameter "color"

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    return-void
.end method

.method public setTextAlign(Landroid/graphics/Paint$Align;)V
    .locals 1
    .parameter "align"

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->interiorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/BorderedText;->exteriorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 106
    return-void
.end method
