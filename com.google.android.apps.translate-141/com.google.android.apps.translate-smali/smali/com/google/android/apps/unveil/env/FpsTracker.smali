.class public Lcom/google/android/apps/unveil/env/FpsTracker;
.super Ljava/lang/Object;
.source "FpsTracker.java"


# static fields
.field public static final DEFAULT_CIRCULAR_QUEUE_SIZE:I = 0x28

.field private static final MAX_SUPPORTED_FPS:I = 0x28


# instance fields
.field private final circularQueue:[I

.field private currIndex:I

.field final fpsBuckets:[I

.field private fpsString:Ljava/lang/String;

.field private lastFrameTimestamp:J

.field private numFrames:I

.field private totalTime:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x28

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/env/FpsTracker;-><init>(I)V

    .line 34
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .parameter "queueSize"

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/16 v0, 0x29

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsBuckets:[I

    .line 26
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->lastFrameTimestamp:J

    .line 27
    iput v2, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->numFrames:I

    .line 28
    iput v2, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->totalTime:I

    .line 29
    iput v2, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->currIndex:I

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsString:Ljava/lang/String;

    .line 37
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->circularQueue:[I

    .line 38
    return-void
.end method

.method private handleDelta(I)V
    .locals 7
    .parameter "delta"

    .prologue
    const/16 v6, 0x3e8

    const/16 v5, 0x28

    .line 63
    if-gtz p1, :cond_0

    .line 90
    :goto_0
    return-void

    .line 68
    :cond_0
    iget v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->numFrames:I

    iget-object v4, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->circularQueue:[I

    array-length v4, v4

    if-le v3, v4, :cond_1

    .line 69
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->circularQueue:[I

    iget v4, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->currIndex:I

    aget v2, v3, v4

    .line 70
    .local v2, oldDelta:I
    div-int v1, v6, v2

    .line 71
    .local v1, fpsForOldFrame:I
    if-gt v1, v5, :cond_2

    .line 72
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsBuckets:[I

    aget v4, v3, v1

    add-int/lit8 v4, v4, -0x1

    aput v4, v3, v1

    .line 76
    :goto_1
    iget v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->totalTime:I

    sub-int/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->totalTime:I

    .line 80
    .end local v1           #fpsForOldFrame:I
    .end local v2           #oldDelta:I
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->circularQueue:[I

    iget v4, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->currIndex:I

    aput p1, v3, v4

    .line 81
    div-int v0, v6, p1

    .line 82
    .local v0, fpsForFrame:I
    if-gt v0, v5, :cond_3

    .line 83
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsBuckets:[I

    aget v4, v3, v0

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, v0

    .line 87
    :goto_2
    iget v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->totalTime:I

    add-int/2addr v3, p1

    iput v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->totalTime:I

    .line 89
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsString:Ljava/lang/String;

    goto :goto_0

    .line 74
    .end local v0           #fpsForFrame:I
    .restart local v1       #fpsForOldFrame:I
    .restart local v2       #oldDelta:I
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsBuckets:[I

    aget v4, v3, v5

    add-int/lit8 v4, v4, -0x1

    aput v4, v3, v5

    goto :goto_1

    .line 85
    .end local v1           #fpsForOldFrame:I
    .end local v2           #oldDelta:I
    .restart local v0       #fpsForFrame:I
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsBuckets:[I

    aget v4, v3, v5

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, v5

    goto :goto_2
.end method


# virtual methods
.method public getFpsString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 99
    iget-object v6, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsString:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 100
    iget-object v6, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsString:Ljava/lang/String;

    .line 134
    :goto_0
    return-object v6

    .line 103
    :cond_0
    iget v6, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->numFrames:I

    iget-object v7, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->circularQueue:[I

    array-length v7, v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 104
    .local v3, numValidFrames:I
    if-nez v3, :cond_1

    .line 105
    const-string v6, ""

    goto :goto_0

    .line 109
    :cond_1
    const/4 v6, 0x5

    new-array v5, v6, [Ljava/lang/Integer;

    .line 110
    .local v5, quartiles:[Ljava/lang/Integer;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_2

    .line 111
    const/4 v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    .line 110
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 114
    :cond_2
    const/4 v0, 0x0

    .line 115
    .local v0, countSoFar:I
    const/4 v4, 0x0

    .line 116
    .local v4, quartileIndex:I
    const/4 v2, 0x0

    :goto_2
    const/16 v6, 0x28

    if-gt v2, v6, :cond_5

    .line 117
    iget-object v6, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsBuckets:[I

    aget v6, v6, v2

    if-nez v6, :cond_4

    .line 116
    :cond_3
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 121
    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsBuckets:[I

    aget v6, v6, v2

    add-int/2addr v0, v6

    .line 122
    mul-int v6, v4, v3

    div-int/lit8 v6, v6, 0x4

    if-lt v0, v6, :cond_3

    .line 123
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    .line 124
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 128
    :cond_5
    const/high16 v6, 0x447a

    iget v7, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->totalTime:I

    int-to-float v7, v7

    int-to-float v8, v3

    div-float/2addr v7, v8

    div-float v1, v6, v7

    .line 130
    .local v1, fps:F
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x2

    invoke-static {v1, v7}, Lcom/google/android/apps/unveil/env/NumberUtils;->format(FI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-static {v7, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->currIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->circularQueue:[I

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsString:Ljava/lang/String;

    .line 134
    iget-object v6, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->fpsString:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public tick()Z
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/unveil/env/FpsTracker;->tick(J)Z

    move-result v0

    return v0
.end method

.method public tick(J)Z
    .locals 2
    .parameter "currTimestamp"

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->numFrames:I

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->circularQueue:[I

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->currIndex:I

    .line 48
    iget v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->numFrames:I

    if-lez v0, :cond_0

    .line 49
    iget-wide v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->lastFrameTimestamp:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/env/FpsTracker;->handleDelta(I)V

    .line 52
    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->lastFrameTimestamp:J

    .line 53
    iget v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->numFrames:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->numFrames:I

    .line 55
    iget v0, p0, Lcom/google/android/apps/unveil/env/FpsTracker;->currIndex:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
