.class Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;
.super Ljava/lang/Object;
.source "RegionQuadTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/unveil/env/RegionQuadTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Link"
.end annotation


# instance fields
.field private intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/unveil/env/RegionQuadTree",
            "<TT;>;"
        }
    .end annotation
.end field

.field private leafNodeItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/unveil/env/RegionQuadTree$Item",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/unveil/env/RegionQuadTree",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

.field final synthetic this$0:Lcom/google/android/apps/unveil/env/RegionQuadTree;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/unveil/env/RegionQuadTree;Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;Lcom/google/android/apps/unveil/env/RegionQuadTree;)V
    .locals 0
    .parameter
    .parameter "quadrant"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;",
            "Lcom/google/android/apps/unveil/env/RegionQuadTree",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 357
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    .local p3, parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>;"
    iput-object p1, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->this$0:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358
    iput-object p2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    .line 359
    iput-object p3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    .line 360
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->addItem(Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V

    return-void
.end method

.method private addItem(Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/unveil/env/RegionQuadTree$Item",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 369
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    .local p1, newItem:Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;,"Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #calls: Lcom/google/android/apps/unveil/env/RegionQuadTree;->addItem(Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$100(Lcom/google/android/apps/unveil/env/RegionQuadTree;Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V

    .line 386
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 372
    invoke-direct {p0}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->splitLeaf()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #calls: Lcom/google/android/apps/unveil/env/RegionQuadTree;->addItem(Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$100(Lcom/google/android/apps/unveil/env/RegionQuadTree;Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V

    goto :goto_0

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 383
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private canSplit()Z
    .locals 3

    .prologue
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    const/4 v0, 0x1

    .line 519
    iget-object v1, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #getter for: Lcom/google/android/apps/unveil/env/RegionQuadTree;->width:I
    invoke-static {v1}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$600(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v1

    if-le v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #getter for: Lcom/google/android/apps/unveil/env/RegionQuadTree;->height:I
    invoke-static {v1}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$700(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getHeight()I
    .locals 3

    .prologue
    .line 495
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    sget-object v0, Lcom/google/android/apps/unveil/env/RegionQuadTree$1;->$SwitchMap$com$google$android$apps$unveil$env$RegionQuadTree$Quadrant:[I

    iget-object v1, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 509
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown quadrant: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 499
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #getter for: Lcom/google/android/apps/unveil/env/RegionQuadTree;->height:I
    invoke-static {v0}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$700(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 506
    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #getter for: Lcom/google/android/apps/unveil/env/RegionQuadTree;->height:I
    invoke-static {v0}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$700(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 495
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getMinX()I
    .locals 3

    .prologue
    .line 433
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    sget-object v0, Lcom/google/android/apps/unveil/env/RegionQuadTree$1;->$SwitchMap$com$google$android$apps$unveil$env$RegionQuadTree$Quadrant:[I

    iget-object v1, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 445
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown quadrant: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 437
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #calls: Lcom/google/android/apps/unveil/env/RegionQuadTree;->getCenterX()I
    invoke-static {v0}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$200(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v0

    .line 442
    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #getter for: Lcom/google/android/apps/unveil/env/RegionQuadTree;->xOrigin:I
    invoke-static {v0}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$300(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v0

    goto :goto_0

    .line 433
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private getMinY()I
    .locals 3

    .prologue
    .line 453
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    sget-object v0, Lcom/google/android/apps/unveil/env/RegionQuadTree$1;->$SwitchMap$com$google$android$apps$unveil$env$RegionQuadTree$Quadrant:[I

    iget-object v1, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 465
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown quadrant: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 457
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #getter for: Lcom/google/android/apps/unveil/env/RegionQuadTree;->yOrigin:I
    invoke-static {v0}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$400(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v0

    .line 462
    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #calls: Lcom/google/android/apps/unveil/env/RegionQuadTree;->getCenterY()I
    invoke-static {v0}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$500(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v0

    goto :goto_0

    .line 453
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getWidth()I
    .locals 3

    .prologue
    .line 473
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    sget-object v0, Lcom/google/android/apps/unveil/env/RegionQuadTree$1;->$SwitchMap$com$google$android$apps$unveil$env$RegionQuadTree$Quadrant:[I

    iget-object v1, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 487
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown quadrant: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->quadrant:Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Quadrant;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 479
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #getter for: Lcom/google/android/apps/unveil/env/RegionQuadTree;->width:I
    invoke-static {v0}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$600(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    .line 484
    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->parent:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #getter for: Lcom/google/android/apps/unveil/env/RegionQuadTree;->width:I
    invoke-static {v0}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$600(Lcom/google/android/apps/unveil/env/RegionQuadTree;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 473
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private splitLeaf()Z
    .locals 7

    .prologue
    .line 531
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    invoke-direct {p0}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->canSplit()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 532
    new-instance v2, Lcom/google/android/apps/unveil/env/RegionQuadTree;

    invoke-direct {p0}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->getMinX()I

    move-result v3

    invoke-direct {p0}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->getMinY()I

    move-result v4

    invoke-direct {p0}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->getWidth()I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->getHeight()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/apps/unveil/env/RegionQuadTree;-><init>(IIII)V

    iput-object v2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    .line 533
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;

    .line 534
    .local v1, item:Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;,"Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;"
    iget-object v2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    #calls: Lcom/google/android/apps/unveil/env/RegionQuadTree;->addItem(Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->access$100(Lcom/google/android/apps/unveil/env/RegionQuadTree;Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;)V

    goto :goto_0

    .line 536
    .end local v1           #item:Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;,"Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;"
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    .line 537
    const/4 v2, 0x1

    .line 539
    .end local v0           #i$:Ljava/util/Iterator;
    :goto_1
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method getItemsInRegion(IIIILjava/util/List;)Ljava/util/List;
    .locals 4
    .parameter "minX"
    .parameter "minY"
    .parameter "maxX"
    .parameter "maxY"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/env/RegionQuadTree",
            "<TT;>;>;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/env/RegionQuadTree$Item",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    .local p5, treesToSearch:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>;>;"
    const/4 v2, 0x0

    .line 411
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 413
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 414
    .local v2, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;>;"
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;

    .line 415
    .local v1, item:Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;,"Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;"
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;->inRegion(IIII)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 416
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 420
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #item:Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;,"Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;"
    .end local v2           #items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;>;"
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    if-eqz v3, :cond_2

    .line 421
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    invoke-interface {p5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 425
    :cond_2
    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 545
    .local p0, this:Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;,"Lcom/google/android/apps/unveil/env/RegionQuadTree<TT;>.Link;"
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    if-eqz v3, :cond_0

    .line 546
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->intermediateNode:Lcom/google/android/apps/unveil/env/RegionQuadTree;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/env/RegionQuadTree;->toString()Ljava/lang/String;

    move-result-object v3

    .line 557
    :goto_0
    return-object v3

    .line 547
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 549
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v3, "( "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    iget-object v3, p0, Lcom/google/android/apps/unveil/env/RegionQuadTree$Link;->leafNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;

    .line 551
    .local v2, item:Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;,"Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;"
    invoke-virtual {v2}, Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 554
    .end local v2           #item:Lcom/google/android/apps/unveil/env/RegionQuadTree$Item;,"Lcom/google/android/apps/unveil/env/RegionQuadTree$Item<TT;>;"
    :cond_1
    const/16 v3, 0x29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 555
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 557
    .end local v0           #builder:Ljava/lang/StringBuilder;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_2
    const-string v3, "<NIL>"

    goto :goto_0
.end method
