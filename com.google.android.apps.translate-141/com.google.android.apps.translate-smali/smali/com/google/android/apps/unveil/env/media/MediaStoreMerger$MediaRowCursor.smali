.class Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;
.super Ljava/lang/Object;
.source "MediaStoreMerger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/unveil/env/media/MediaStoreMerger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaRowCursor"
.end annotation


# instance fields
.field private final cursor:Landroid/database/Cursor;

.field private row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

.field private final storageUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/net/Uri;Landroid/database/Cursor;)V
    .locals 0
    .parameter "storageUri"
    .parameter "cursor"

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput-object p1, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->storageUri:Landroid/net/Uri;

    .line 151
    iput-object p2, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->cursor:Landroid/database/Cursor;

    .line 152
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri;Landroid/database/Cursor;Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;-><init>(Landroid/net/Uri;Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;)Landroid/database/Cursor;
    .locals 1
    .parameter "x0"

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->cursor:Landroid/database/Cursor;

    return-object v0
.end method

.method private readUntilValid(Z)Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;
    .locals 6
    .parameter "cursorHasData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/unveil/env/media/InvalidUriException;
        }
    .end annotation

    .prologue
    .line 189
    :goto_0
    if-eqz p1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->cursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->storageUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;->fromCursor(Landroid/database/Cursor;Landroid/net/Uri;)Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

    move-result-object v0

    .line 191
    .local v0, row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;
    iget-object v1, v0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;->path:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;->path:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;->getFileLength()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 199
    .end local v0           #row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;
    :goto_1
    return-object v0

    .line 195
    .restart local v0       #row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;
    :cond_0
    invoke-static {}, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger;->access$000()Lcom/google/android/apps/unveil/env/UnveilLogger;

    move-result-object v1

    const-string v2, "skipping invalid MediaStore row=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;->getUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    iget-object v1, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result p1

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result p1

    goto :goto_0

    .line 199
    .end local v0           #row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public get()Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

    return-object v0
.end method

.method public moveToFirst()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/unveil/env/media/InvalidUriException;
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->readUntilValid(Z)Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToNext()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/unveil/env/media/InvalidUriException;
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->readUntilValid(Z)Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRowCursor;->row:Lcom/google/android/apps/unveil/env/media/MediaStoreMerger$MediaRow;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
