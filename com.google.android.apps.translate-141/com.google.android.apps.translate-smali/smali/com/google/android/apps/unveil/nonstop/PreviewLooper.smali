.class public Lcom/google/android/apps/unveil/nonstop/PreviewLooper;
.super Ljava/lang/Object;
.source "PreviewLooper.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Lcom/google/android/apps/unveil/nonstop/TimestampedFrame$BufferSink;


# static fields
.field private static final FRAME_SLOP_TIME_MS:J = 0x5L

.field private static final LARGE_TEXT_SIZE:I = 0x14

.field private static final SMALL_TEXT_SIZE:I = 0x10

.field private static final TEXT_BUFFER_SIZE:I = 0x4


# instance fields
.field private activeProcessor:I

.field private allPreviewProcessors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/nonstop/FrameProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

.field private delayedRequestPending:Z

.field private final emptyLines:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private firstRun:Z

.field private final frameRequestTimer:Lcom/google/android/apps/unveil/env/Stopwatch;

.field private final frameToCanvas:Landroid/graphics/Matrix;

.field private final handler:Landroid/os/Handler;

.field private final interFrameDelayMs:J

.field private final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

.field private numPreviewFrames:I

.field private final paint:Landroid/graphics/Paint;

.field private volatile pauseRequested:Z

.field private final previewFrameSize:Lcom/google/android/apps/unveil/env/Size;

.field private final processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

.field private rotation:I

.field private volatile running:Z

.field private final uiThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/unveil/sensors/CameraManager;IFI)V
    .locals 6
    .parameter "cameraManager"
    .parameter "rotation"
    .parameter "targetFps"
    .parameter "numThreads"

    .prologue
    .line 133
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;IFILandroid/graphics/Matrix;)V

    .line 134
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/unveil/sensors/CameraManager;IFILandroid/graphics/Matrix;)V
    .locals 8
    .parameter "cameraManager"
    .parameter "rotation"
    .parameter "targetFps"
    .parameter "numThreads"
    .parameter "frameToCanvas"

    .prologue
    const/4 v7, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v5, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v5}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    .line 69
    iput-boolean v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->delayedRequestPending:Z

    .line 78
    const/4 v5, -0x1

    iput v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    .line 83
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    .line 84
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->emptyLines:Ljava/util/Vector;

    .line 94
    iput-object p1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    .line 95
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->uiThread:Ljava/lang/Thread;

    .line 97
    iput p2, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->rotation:I

    .line 98
    iput-object p5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->frameToCanvas:Landroid/graphics/Matrix;

    .line 100
    new-instance v5, Lcom/google/android/apps/unveil/env/Stopwatch;

    invoke-direct {v5}, Lcom/google/android/apps/unveil/env/Stopwatch;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->frameRequestTimer:Lcom/google/android/apps/unveil/env/Stopwatch;

    .line 102
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->firstRun:Z

    .line 103
    iput-boolean v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->running:Z

    .line 105
    iput-boolean v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->delayedRequestPending:Z

    .line 106
    const/high16 v5, 0x447a

    div-float/2addr v5, p3

    float-to-long v5, v5

    iput-wide v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->interFrameDelayMs:J

    .line 108
    invoke-virtual {p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->previewFrameSize:Lcom/google/android/apps/unveil/env/Size;

    .line 110
    new-array v5, p4, [Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    iput-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    .line 112
    iput p2, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->rotation:I

    .line 114
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->handler:Landroid/os/Handler;

    .line 116
    const/4 v0, 0x0

    .line 117
    .local v0, lastChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    add-int/lit8 v2, p4, -0x1

    .local v2, level:I
    move-object v1, v0

    .end local v0           #lastChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    .local v1, lastChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    :goto_0
    if-lez v2, :cond_0

    .line 121
    rsub-int/lit8 v5, v2, 0x5

    const/4 v6, 0x5

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 122
    .local v4, priority:I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ProcessingThread "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 123
    .local v3, name:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/unveil/nonstop/ProcessingThread;

    invoke-direct {v0, v3, v4, v1}, Lcom/google/android/apps/unveil/nonstop/ProcessingThread;-><init>(Ljava/lang/String;ILcom/google/android/apps/unveil/nonstop/ProcessingChain;)V

    .line 124
    .end local v1           #lastChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    .restart local v0       #lastChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    iget-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    aput-object v0, v5, v2

    .line 117
    add-int/lit8 v2, v2, -0x1

    move-object v1, v0

    .end local v0           #lastChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    .restart local v1       #lastChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    goto :goto_0

    .line 126
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #priority:I
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    new-instance v6, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    invoke-direct {v6, v1}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;-><init>(Lcom/google/android/apps/unveil/nonstop/ProcessingChain;)V

    aput-object v6, v5, v7

    .line 127
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/unveil/nonstop/PreviewLooper;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->delayedRequestPending:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/unveil/nonstop/PreviewLooper;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->requestFrameDelayed()V

    return-void
.end method

.method private initAllProcessors(Lcom/google/android/apps/unveil/env/Size;Landroid/graphics/Matrix;)V
    .locals 4
    .parameter "viewSize"
    .parameter "frameToCanvas"

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->waitUntilNonUiThreadsNotProcessing()V

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;

    .line 328
    .local v1, processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    iget-object v2, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->previewFrameSize:Lcom/google/android/apps/unveil/env/Size;

    iget v3, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->rotation:I

    invoke-virtual {v1, v2, p1, v3, p2}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->init(Lcom/google/android/apps/unveil/env/Size;Lcom/google/android/apps/unveil/env/Size;ILandroid/graphics/Matrix;)V

    goto :goto_0

    .line 330
    .end local v1           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :cond_0
    return-void
.end method

.method private pauseAllProcessors()V
    .locals 3

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->waitUntilNonUiThreadsNotProcessing()V

    .line 341
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;

    .line 342
    .local v1, processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    invoke-virtual {v1}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->pause()V

    goto :goto_0

    .line 344
    .end local v1           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :cond_0
    return-void
.end method

.method private pauseIfRequested()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 237
    iget-boolean v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseRequested:Z

    if-nez v1, :cond_0

    .line 246
    :goto_0
    return v0

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 242
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->waitUntilNonUiThreadsNotProcessing()V

    .line 243
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->runQueuedRunnables()V

    .line 244
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseAllProcessors()V

    .line 245
    iput-boolean v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->running:Z

    .line 246
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private declared-synchronized requestFrameDelayed()V
    .locals 6

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->delayedRequestPending:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->running:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_1

    .line 186
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 166
    :cond_1
    const/4 v4, 0x1

    :try_start_1
    iput-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->delayedRequestPending:Z

    .line 168
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->frameRequestTimer:Lcom/google/android/apps/unveil/env/Stopwatch;

    invoke-virtual {v4}, Lcom/google/android/apps/unveil/env/Stopwatch;->getElapsedMilliseconds()J

    move-result-wide v0

    .line 169
    .local v0, timeSinceLastFrameRequest:J
    iget-wide v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->interFrameDelayMs:J

    sub-long v2, v4, v0

    .line 171
    .local v2, timeTillNextFrameRequest:J
    const-wide/16 v4, 0x5

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    .line 174
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->handler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/apps/unveil/nonstop/PreviewLooper$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper$1;-><init>(Lcom/google/android/apps/unveil/nonstop/PreviewLooper;)V

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    .end local v0           #timeSinceLastFrameRequest:J
    .end local v2           #timeTillNextFrameRequest:J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 183
    .restart local v0       #timeSinceLastFrameRequest:J
    .restart local v2       #timeTillNextFrameRequest:J
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->requestNextFrame()V

    .line 184
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->delayedRequestPending:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private requestNextFrame()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->frameRequestTimer:Lcom/google/android/apps/unveil/env/Stopwatch;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/env/Stopwatch;->reset()V

    .line 194
    return-void
.end method

.method private runQueuedRunnables()V
    .locals 7

    .prologue
    .line 255
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 256
    .local v4, runnables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Runnable;>;"
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    .local v0, arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v5, v0, v1

    .line 257
    .local v5, thread:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    invoke-virtual {v5, v4}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;->pollRunnables(Ljava/util/Collection;)V

    .line 256
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    .end local v5           #thread:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Runnable;

    .line 261
    .local v3, runnable:Ljava/lang/Runnable;
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 263
    .end local v3           #runnable:Ljava/lang/Runnable;
    :cond_1
    return-void
.end method

.method private startAllProcessors()V
    .locals 3

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->waitUntilNonUiThreadsNotProcessing()V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;

    .line 335
    .local v1, processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    invoke-virtual {v1}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->start()V

    goto :goto_0

    .line 337
    .end local v1           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :cond_0
    return-void
.end method

.method public static supportNonstopFrameProcessing(Landroid/content/Context;)Z
    .locals 2
    .parameter "context"

    .prologue
    .line 486
    const-string v1, "activity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 487
    .local v0, limits:I
    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private waitUntilNonUiThreadsNotProcessing()V
    .locals 2

    .prologue
    .line 369
    const/4 v0, 0x1

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 370
    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;->blockUntilReadyForFrame()V

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 372
    :cond_0
    return-void
.end method


# virtual methods
.method public addPreviewProcessor(Lcom/google/android/apps/unveil/nonstop/FrameProcessor;I)V
    .locals 2
    .parameter "handler"
    .parameter "level"

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    aget-object v1, v0, p2

    monitor-enter v1

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;->addProcessor(Lcom/google/android/apps/unveil/nonstop/FrameProcessor;)V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->allPreviewProcessors:Ljava/util/List;

    .line 140
    monitor-exit v1

    .line 141
    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public changeMode(Z)Z
    .locals 11
    .parameter "up"

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    .line 376
    .local v4, numProcessors:I
    iget v10, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    if-eqz p1, :cond_1

    move v7, v8

    :goto_0
    add-int/2addr v7, v10

    iput v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    .line 378
    add-int/lit8 v3, v4, 0x2

    .line 379
    .local v3, numChoices:I
    iget v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    add-int/lit8 v7, v7, 0x1

    add-int/2addr v7, v3

    rem-int/2addr v7, v3

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    .line 381
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v6

    .line 382
    .local v6, processors:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/nonstop/FrameProcessor;>;"
    iget v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    if-ne v7, v4, :cond_2

    move v0, v8

    .line 383
    .local v0, allActive:Z
    :goto_1
    const/4 v1, 0x0

    .local v1, i:I
    :goto_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_4

    .line 384
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;

    .line 385
    .local v5, processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    if-nez v0, :cond_0

    iget v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    if-ne v1, v7, :cond_3

    :cond_0
    move v2, v8

    .line 386
    .local v2, isActive:Z
    :goto_3
    invoke-virtual {v5, v2}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->setDebugActive(Z)V

    .line 383
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 376
    .end local v0           #allActive:Z
    .end local v1           #i:I
    .end local v2           #isActive:Z
    .end local v3           #numChoices:I
    .end local v5           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    .end local v6           #processors:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/nonstop/FrameProcessor;>;"
    :cond_1
    const/4 v7, -0x1

    goto :goto_0

    .restart local v3       #numChoices:I
    .restart local v6       #processors:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/nonstop/FrameProcessor;>;"
    :cond_2
    move v0, v9

    .line 382
    goto :goto_1

    .restart local v0       #allActive:Z
    .restart local v1       #i:I
    .restart local v5       #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :cond_3
    move v2, v9

    .line 385
    goto :goto_3

    .line 389
    .end local v5           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :cond_4
    iget v7, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    if-ltz v7, :cond_5

    :goto_4
    return v8

    :cond_5
    move v8, v9

    goto :goto_4
.end method

.method public drawDebug(Landroid/graphics/Canvas;)V
    .locals 23
    .parameter "canvas"

    .prologue
    .line 393
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    move/from16 v19, v0

    if-gez v19, :cond_1

    .line 475
    :cond_0
    return-void

    .line 397
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v13

    .line 398
    .local v13, processors:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/nonstop/FrameProcessor;>;"
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v11

    .line 401
    .local v11, numProcessors:I
    const/4 v6, 0x0

    .local v6, i:I
    :goto_0
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v6, v0, :cond_5

    .line 402
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-eq v0, v11, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ne v6, v0, :cond_4

    :cond_2
    const/4 v7, 0x1

    .line 403
    .local v7, isActive:Z
    :goto_1
    if-eqz v7, :cond_3

    .line 404
    invoke-interface {v13, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;

    .line 405
    .local v12, processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    monitor-enter v12

    .line 406
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->drawDebug(Landroid/graphics/Canvas;)V

    .line 407
    monitor-exit v12

    .line 401
    .end local v12           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 402
    .end local v7           #isActive:Z
    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    .line 407
    .restart local v7       #isActive:Z
    .restart local v12       #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :catchall_0
    move-exception v19

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v19

    .line 411
    .end local v7           #isActive:Z
    .end local v12           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ne v0, v11, :cond_7

    const/4 v5, 0x1

    .line 414
    .local v5, allActive:Z
    :goto_2
    const/16 v17, 0x0

    .line 415
    .local v17, xPos:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    .line 416
    .local v16, startingYPos:I
    const/4 v6, 0x0

    :goto_3
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v6, v0, :cond_0

    .line 418
    invoke-interface {v13, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;

    .line 420
    .restart local v12       #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    if-nez v5, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->activeProcessor:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ne v6, v0, :cond_8

    :cond_6
    const/4 v7, 0x1

    .line 423
    .restart local v7       #isActive:Z
    :goto_4
    if-eqz v7, :cond_9

    .line 424
    monitor-enter v12

    .line 425
    :try_start_1
    invoke-virtual {v12}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->getDebugText()Ljava/util/Vector;

    move-result-object v9

    .line 426
    .local v9, lines:Ljava/util/Vector;,"Ljava/util/Vector<Ljava/lang/String;>;"
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 431
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v15

    .line 435
    .local v15, shadedWidth:I
    if-eqz v5, :cond_a

    const/16 v19, 0x2

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v20

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 440
    .local v10, numLines:I
    :goto_6
    if-eqz v7, :cond_c

    const/16 v19, 0x1

    :goto_7
    add-int v19, v19, v10

    mul-int/lit8 v19, v19, 0x14

    add-int/lit8 v14, v19, 0x1c

    .line 443
    .local v14, shadedHeight:I
    sub-int v16, v16, v14

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    const/high16 v20, -0x100

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setColor(I)V

    .line 446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    const/16 v20, 0x64

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 448
    move/from16 v18, v16

    .line 449
    .local v18, yPos:I
    new-instance v19, Landroid/graphics/Rect;

    const/16 v20, 0x0

    add-int/lit8 v21, v15, 0x0

    add-int v22, v18, v14

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v18

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    const/16 v20, 0xff

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    if-eqz v7, :cond_d

    const v19, -0xff0001

    :goto_8
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    const/high16 v20, 0x41a0

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 457
    add-int/lit8 v18, v18, 0x18

    .line 458
    invoke-virtual {v12}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->getHeaderText()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 460
    if-eqz v7, :cond_f

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    const/16 v20, -0x1

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setColor(I)V

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    const/high16 v20, 0x4180

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 465
    add-int/lit8 v18, v18, 0x14

    .line 466
    invoke-virtual {v12}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->getTimeStats()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 467
    const/4 v8, 0x0

    .local v8, lineNum:I
    :goto_9
    if-ge v8, v10, :cond_e

    .line 468
    add-int/lit8 v18, v18, 0x14

    .line 469
    invoke-virtual {v9, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    const/16 v20, 0x0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->paint:Landroid/graphics/Paint;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 467
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    .line 411
    .end local v5           #allActive:Z
    .end local v7           #isActive:Z
    .end local v8           #lineNum:I
    .end local v9           #lines:Ljava/util/Vector;,"Ljava/util/Vector<Ljava/lang/String;>;"
    .end local v10           #numLines:I
    .end local v12           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    .end local v14           #shadedHeight:I
    .end local v15           #shadedWidth:I
    .end local v16           #startingYPos:I
    .end local v17           #xPos:I
    .end local v18           #yPos:I
    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 420
    .restart local v5       #allActive:Z
    .restart local v12       #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    .restart local v16       #startingYPos:I
    .restart local v17       #xPos:I
    :cond_8
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 426
    .restart local v7       #isActive:Z
    :catchall_1
    move-exception v19

    :try_start_2
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v19

    .line 428
    :cond_9
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->emptyLines:Ljava/util/Vector;

    .restart local v9       #lines:Ljava/util/Vector;,"Ljava/util/Vector<Ljava/lang/String;>;"
    goto/16 :goto_5

    .line 435
    .restart local v15       #shadedWidth:I
    :cond_a
    if-eqz v7, :cond_b

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v10

    goto/16 :goto_6

    :cond_b
    const/4 v10, 0x0

    goto/16 :goto_6

    .line 440
    .restart local v10       #numLines:I
    :cond_c
    const/16 v19, 0x0

    goto/16 :goto_7

    .line 455
    .restart local v14       #shadedHeight:I
    .restart local v18       #yPos:I
    :cond_d
    const v19, -0xffff01

    goto/16 :goto_8

    .line 472
    .restart local v8       #lineNum:I
    :cond_e
    add-int/lit8 v18, v18, 0x4

    .line 416
    .end local v8           #lineNum:I
    :cond_f
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_3
.end method

.method public getAllProcessors()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/nonstop/FrameProcessor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->allPreviewProcessors:Ljava/util/List;

    if-nez v4, :cond_0

    .line 148
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->allPreviewProcessors:Ljava/util/List;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    .local v0, arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 151
    .local v3, thread:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->allPreviewProcessors:Ljava/util/List;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;->getProcessors()Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 150
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 154
    .end local v0           #arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #thread:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->allPreviewProcessors:Ljava/util/List;

    return-object v4
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 478
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->running:Z

    return v0
.end method

.method public declared-synchronized onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 8
    .parameter "rawData"
    .parameter "camera"

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->running:Z

    if-nez v1, :cond_1

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Dropping frame due to pause event."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 205
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseIfRequested()Z

    move-result v1

    if-nez v1, :cond_0

    .line 211
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->runQueuedRunnables()V

    .line 213
    iget v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->numPreviewFrames:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->numPreviewFrames:I

    .line 216
    new-instance v0, Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;

    iget-object v2, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->previewFrameSize:Lcom/google/android/apps/unveil/env/Size;

    iget v3, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->numPreviewFrames:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget v6, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->rotation:I

    move-object v1, p1

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;-><init>([BLcom/google/android/apps/unveil/env/Size;IJILcom/google/android/apps/unveil/nonstop/TimestampedFrame$BufferSink;)V

    .line 225
    .local v0, previewFrame:Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;
    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;->addReference()V

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;->sendFrame(Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;)V

    .line 233
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->requestFrameDelayed()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 198
    .end local v0           #previewFrame:Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized pauseLoop()V
    .locals 7

    .prologue
    .line 301
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "pauseLoop"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    iget-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->running:Z

    if-nez v4, :cond_1

    .line 303
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "Pausing a PreviewLooper that was already paused."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 307
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    .local v0, arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 308
    .local v3, processingChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    invoke-virtual {v3}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;->stopSoon()V

    .line 307
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 311
    .end local v3           #processingChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    :cond_2
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseRequested:Z

    .line 315
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->uiThread:Ljava/lang/Thread;

    if-ne v4, v5, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseIfRequested()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 301
    .end local v0           #arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public returnBuffer([B)V
    .locals 1
    .parameter "rawData"

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->addPreviewBuffer([B)V

    .line 252
    return-void
.end method

.method public declared-synchronized shutdown()V
    .locals 8

    .prologue
    .line 351
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "shutdown"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseLoop()V

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    .local v0, arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 356
    .local v3, processingChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    invoke-virtual {v3}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;->shutdown()V

    .line 355
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 359
    .end local v3           #processingChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;

    .line 360
    .local v4, processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    invoke-virtual {v4}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 351
    .end local v0           #arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #len$:I
    .end local v4           #processor:Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 362
    .restart local v0       #arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #len$:I
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized startLoop(Lcom/google/android/apps/unveil/env/Size;)V
    .locals 8
    .parameter "viewSize"

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "startLoop with size %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    iget-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->running:Z

    if-eqz v4, :cond_0

    .line 271
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "Starting a PreviewLooper that was already started, just resetting preview callback."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 273
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/unveil/sensors/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 274
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->requestNextFrame()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :goto_0
    monitor-exit p0

    return-void

    .line 277
    :cond_0
    const/4 v4, 0x1

    :try_start_1
    iput-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->running:Z

    .line 278
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseRequested:Z

    .line 280
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->frameRequestTimer:Lcom/google/android/apps/unveil/env/Stopwatch;

    invoke-virtual {v4}, Lcom/google/android/apps/unveil/env/Stopwatch;->start()V

    .line 281
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->delayedRequestPending:Z

    .line 283
    const/4 v4, 0x0

    iput v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->numPreviewFrames:I

    .line 285
    iget-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->firstRun:Z

    if-eqz v4, :cond_1

    .line 286
    iget-object v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->frameToCanvas:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->initAllProcessors(Lcom/google/android/apps/unveil/env/Size;Landroid/graphics/Matrix;)V

    .line 288
    :cond_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->firstRun:Z

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->processingChains:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;

    .local v0, arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 291
    .local v3, processingChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    invoke-virtual {v3}, Lcom/google/android/apps/unveil/nonstop/ProcessingChain;->start()V

    .line 290
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 293
    .end local v3           #processingChain:Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->startAllProcessors()V

    .line 294
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->requestNextFrame()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 269
    .end local v0           #arr$:[Lcom/google/android/apps/unveil/nonstop/ProcessingChain;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method
