.class Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1;
.super Ljava/lang/Object;
.source "SpeechEngine.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/unveil/nonstop/SpeechEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/unveil/nonstop/SpeechEngine;


# direct methods
.method constructor <init>(Lcom/google/android/apps/unveil/nonstop/SpeechEngine;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1;->this$0:Lcom/google/android/apps/unveil/nonstop/SpeechEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 4
    .parameter "status"

    .prologue
    const/4 v2, 0x0

    .line 85
    invoke-static {}, Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->access$000()Lcom/google/android/apps/unveil/env/UnveilLogger;

    move-result-object v0

    const-string v1, "onInit()"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1;->this$0:Lcom/google/android/apps/unveil/nonstop/SpeechEngine;

    monitor-enter v1

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1;->this$0:Lcom/google/android/apps/unveil/nonstop/SpeechEngine;

    #getter for: Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->textToSpeech:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->access$100(Lcom/google/android/apps/unveil/nonstop/SpeechEngine;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    if-nez v0, :cond_0

    .line 89
    monitor-exit v1

    .line 108
    :goto_0
    return-void

    .line 92
    :cond_0
    if-nez p1, :cond_1

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1;->this$0:Lcom/google/android/apps/unveil/nonstop/SpeechEngine;

    #getter for: Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->textToSpeech:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->access$100(Lcom/google/android/apps/unveil/nonstop/SpeechEngine;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1$1;-><init>(Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1;)V

    invoke-virtual {v0, v2}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/SpeechEngine$1;->this$0:Lcom/google/android/apps/unveil/nonstop/SpeechEngine;

    const/4 v2, 0x1

    #setter for: Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->isTTSready:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->access$302(Lcom/google/android/apps/unveil/nonstop/SpeechEngine;Z)Z

    .line 103
    invoke-static {}, Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->access$000()Lcom/google/android/apps/unveil/env/UnveilLogger;

    move-result-object v0

    const-string v2, "TextToSpeech is ready"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 105
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/android/apps/unveil/nonstop/SpeechEngine;->access$000()Lcom/google/android/apps/unveil/env/UnveilLogger;

    move-result-object v0

    const-string v2, "TTS failed to initialize"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
