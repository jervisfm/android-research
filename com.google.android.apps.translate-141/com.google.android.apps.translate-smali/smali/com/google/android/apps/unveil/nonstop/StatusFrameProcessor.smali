.class public Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;
.super Lcom/google/android/apps/unveil/nonstop/FrameProcessor;
.source "StatusFrameProcessor.java"


# static fields
.field private static final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# instance fields
.field private final cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

.field private final debugText:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

.field private fpsTracker:Lcom/google/android/apps/unveil/env/FpsTracker;

.field private final glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

.field private listener:Lcom/google/android/apps/unveil/sensors/UnveilSensor$Listener;

.field private final sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

.field private sensorValues:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    sput-object v0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/unveil/sensors/CameraManager;Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;Lcom/google/android/apps/unveil/nonstop/DebugView;Lcom/google/android/apps/unveil/ui/GlOverlay;)V
    .locals 1
    .parameter "cameraManager"
    .parameter "sensorProvider"
    .parameter "debugView"
    .parameter "glDebugView"

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/unveil/nonstop/FrameProcessor;-><init>()V

    .line 31
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugText:Ljava/util/Vector;

    .line 36
    new-instance v0, Lcom/google/android/apps/unveil/env/FpsTracker;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/FpsTracker;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->fpsTracker:Lcom/google/android/apps/unveil/env/FpsTracker;

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    .line 43
    iput-object p3, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    .line 44
    iput-object p4, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    .line 45
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;[F)[F
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 23
    iput-object p1, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorValues:[F

    return-object p1
.end method


# virtual methods
.method public declared-synchronized getDebugText()Ljava/util/Vector;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugText:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugText:Ljava/util/Vector;

    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->fpsTracker:Lcom/google/android/apps/unveil/env/FpsTracker;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/env/FpsTracker;->getFpsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugText:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Screen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->getViewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Preview: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->getPreviewFrameSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugText:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Camera state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getStateName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorValues:[F

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugText:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "azimuth: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorValues:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pitch: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorValues:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", roll: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorValues:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugText:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;->getOrientationSensor()Lcom/google/android/apps/unveil/sensors/UnveilSensor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->listener:Lcom/google/android/apps/unveil/sensors/UnveilSensor$Listener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/sensors/UnveilSensor;->unregisterListener(Lcom/google/android/apps/unveil/sensors/UnveilSensor$Listener;)Z

    .line 65
    :cond_0
    return-void
.end method

.method protected declared-synchronized onProcessFrame(Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;)V
    .locals 3
    .parameter "frame"

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->fpsTracker:Lcom/google/android/apps/unveil/env/FpsTracker;

    invoke-virtual {p1}, Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;->getTimestamp()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/FpsTracker;->tick(J)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;->shouldShowVerbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    sget-object v0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->fpsTracker:Lcom/google/android/apps/unveil/env/FpsTracker;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/env/FpsTracker;->getFpsString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/DebugView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/DebugView;->invalidate()V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->glDebugView:Lcom/google/android/apps/unveil/ui/GlOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/ui/GlOverlay;->requestRender()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :cond_1
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor$1;-><init>(Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;)V

    iput-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->listener:Lcom/google/android/apps/unveil/sensors/UnveilSensor$Listener;

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->sensorProvider:Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;->getOrientationSensor()Lcom/google/android/apps/unveil/sensors/UnveilSensor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;->listener:Lcom/google/android/apps/unveil/sensors/UnveilSensor$Listener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/sensors/UnveilSensor;->registerListener(Lcom/google/android/apps/unveil/sensors/UnveilSensor$Listener;)Z

    .line 58
    :cond_0
    return-void
.end method
