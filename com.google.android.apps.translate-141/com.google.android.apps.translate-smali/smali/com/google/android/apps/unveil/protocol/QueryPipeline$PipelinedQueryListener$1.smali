.class Lcom/google/android/apps/unveil/protocol/QueryPipeline$PipelinedQueryListener$1;
.super Ljava/lang/Object;
.source "QueryPipeline.java"

# interfaces
.implements Lcom/google/android/apps/unveil/history/SearchHistoryProvider$DeleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/unveil/protocol/QueryPipeline$PipelinedQueryListener;->onQueryResponse(Lcom/google/android/apps/unveil/protocol/QueryResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/unveil/protocol/QueryPipeline$PipelinedQueryListener;

.field final synthetic val$queryResponse:Lcom/google/android/apps/unveil/protocol/QueryResponse;


# direct methods
.method constructor <init>(Lcom/google/android/apps/unveil/protocol/QueryPipeline$PipelinedQueryListener;Lcom/google/android/apps/unveil/protocol/QueryResponse;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/apps/unveil/protocol/QueryPipeline$PipelinedQueryListener$1;->this$1:Lcom/google/android/apps/unveil/protocol/QueryPipeline$PipelinedQueryListener;

    iput-object p2, p0, Lcom/google/android/apps/unveil/protocol/QueryPipeline$PipelinedQueryListener$1;->val$queryResponse:Lcom/google/android/apps/unveil/protocol/QueryResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 3

    .prologue
    .line 114
    invoke-static {}, Lcom/google/android/apps/unveil/protocol/QueryPipeline;->access$000()Lcom/google/android/apps/unveil/env/UnveilLogger;

    move-result-object v0

    const-string v1, "Error while silently deleting query saved for later."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    return-void
.end method

.method public onResult()V
    .locals 5

    .prologue
    .line 119
    invoke-static {}, Lcom/google/android/apps/unveil/protocol/QueryPipeline;->access$000()Lcom/google/android/apps/unveil/env/UnveilLogger;

    move-result-object v0

    const-string v1, "Moment %s silently deleted."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/unveil/protocol/QueryPipeline$PipelinedQueryListener$1;->val$queryResponse:Lcom/google/android/apps/unveil/protocol/QueryResponse;

    invoke-virtual {v4}, Lcom/google/android/apps/unveil/protocol/QueryResponse;->getMomentId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    return-void
.end method
