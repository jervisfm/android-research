.class public Lcom/google/android/apps/unveil/protocol/TraceTracker;
.super Ljava/lang/Object;
.source "TraceTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/unveil/protocol/TraceTracker$1;,
        Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    }
.end annotation


# static fields
.field static final MAX_EVICT_NOTICES:I = 0x4

.field private static final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# instance fields
.field private final cookieFetcher:Lcom/google/android/apps/unveil/protocol/TracingCookieFetcher;

.field private final currentActions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;",
            ">;"
        }
    .end annotation
.end field

.field private final networkInfoProvider:Lcom/google/android/apps/unveil/network/NetworkInfoProvider;

.field private traceActionNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "TraceTracker"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/unveil/network/NetworkInfoProvider;Lcom/google/android/apps/unveil/protocol/TracingCookieFetcher;)V
    .locals 1
    .parameter "networkInfoProvider"
    .parameter "cookieFetcher"

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->networkInfoProvider:Lcom/google/android/apps/unveil/network/NetworkInfoProvider;

    .line 105
    iput-object p2, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->cookieFetcher:Lcom/google/android/apps/unveil/protocol/TracingCookieFetcher;

    .line 106
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    .line 107
    return-void
.end method

.method static synthetic access$1100()Lcom/google/android/apps/unveil/env/UnveilLogger;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-object v0
.end method

.method private addAction(ILcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)V
    .locals 6
    .parameter "actionNumber"
    .parameter "actionData"

    .prologue
    .line 405
    iget-object v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v1

    .line 406
    :try_start_0
    sget-object v0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Adding action %d with tracing cookie %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    #getter for: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->tracingCookie:Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$900(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    monitor-exit v1

    .line 409
    return-void

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private addFinishedTraceActions(Lcom/google/goggles/TracingProtos$TraceRequest$Builder;)V
    .locals 4
    .parameter "traceRequest"

    .prologue
    .line 358
    iget-object v3, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v3

    .line 359
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    .line 360
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->isEnded()Z
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$400(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 361
    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->populate(Lcom/google/goggles/TracingProtos$TraceRequest$Builder;)V

    goto :goto_0

    .line 364
    .end local v0           #actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    return-void
.end method

.method private clearFinishedTraceActions()V
    .locals 6

    .prologue
    .line 374
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 375
    .local v1, finishedTraceActionNumbers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v5

    .line 378
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 379
    .local v3, traceActionNumber:Ljava/lang/Integer;
    iget-object v4, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->isEnded()Z
    invoke-static {v4}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$400(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 380
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 386
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #traceActionNumber:Ljava/lang/Integer;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 383
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 384
    .local v0, finishedTraceActionNumber:Ljava/lang/Integer;
    iget-object v4, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 386
    .end local v0           #finishedTraceActionNumber:Ljava/lang/Integer;
    :cond_2
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387
    return-void
.end method

.method private getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    .locals 3
    .parameter "actionNumber"

    .prologue
    .line 395
    iget-object v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v1

    .line 396
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    monitor-exit v1

    return-object v0

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized incrementCurrentActionNumber()V
    .locals 3

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Incrementing current action number."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    iget v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->traceActionNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->traceActionNumber:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    monitor-exit p0

    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized addDurationInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;II)V
    .locals 6
    .parameter "intervalName"
    .parameter "durationMs"
    .parameter "actionNumber"

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p3}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 212
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_0

    .line 213
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "TraceAction %d has already been completed, cannot add duration interval %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :goto_0
    monitor-exit p0

    return-void

    .line 218
    :cond_0
    :try_start_1
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->addDurationInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 211
    .end local v0           #actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized addPoint(ILcom/google/goggles/TracingProtos$PointVariable$Type;)V
    .locals 6
    .parameter "actionNumber"
    .parameter "pointName"

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 243
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_0

    .line 244
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Tried to add point of type %s to action %d but that action is not currently open."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :goto_0
    monitor-exit p0

    return-void

    .line 249
    :cond_0
    :try_start_1
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->addPoint(Lcom/google/goggles/TracingProtos$PointVariable$Type;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$200(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;Lcom/google/goggles/TracingProtos$PointVariable$Type;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 242
    .end local v0           #actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized addPoint(Lcom/google/goggles/TracingProtos$PointVariable$Type;)V
    .locals 1
    .parameter "pointName"

    .prologue
    .line 238
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->addPoint(ILcom/google/goggles/TracingProtos$PointVariable$Type;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    monitor-exit p0

    return-void

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public addProcessorStatus(ILcom/google/goggles/TracingProtos$ProcessorStatus;)V
    .locals 6
    .parameter "actionNumber"
    .parameter "processorStatus"

    .prologue
    .line 253
    if-nez p2, :cond_0

    .line 265
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 258
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_1

    .line 259
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Tried to add processor status to action %d but that action is not currently open."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 264
    :cond_1
    invoke-virtual {v0, p2}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->addProcessorStatus(Lcom/google/goggles/TracingProtos$ProcessorStatus;)V

    goto :goto_0
.end method

.method public addRawInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;JJI)V
    .locals 6
    .parameter "intervalName"
    .parameter "startMs"
    .parameter "endMs"
    .parameter "actionNumber"

    .prologue
    .line 224
    invoke-direct {p0, p6}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 225
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_0

    .line 226
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "TraceAction %d has already been completed, cannot add duration interval %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 232
    :goto_0
    return-void

    :cond_0
    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 231
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->addRawInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;JJ)V

    goto :goto_0
.end method

.method public declared-synchronized beginInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)I
    .locals 2
    .parameter "intervalName"

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v0

    .line 146
    .local v0, currentActionNumber:I
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->beginInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    monitor-exit p0

    return v0

    .line 145
    .end local v0           #currentActionNumber:I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public beginInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;I)V
    .locals 5
    .parameter "intervalName"
    .parameter "actionNumber"

    .prologue
    .line 154
    invoke-direct {p0, p2}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 155
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_0

    .line 156
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "[%s]: null TraceAction in beginInterval!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    :goto_0
    return-void

    .line 159
    :cond_0
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->startInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$000(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V

    goto :goto_0
.end method

.method public declared-synchronized beginTraceAction(I)V
    .locals 4
    .parameter "actionNumber"

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    :goto_0
    monitor-exit p0

    return-void

    .line 276
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    iget-object v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->cookieFetcher:Lcom/google/android/apps/unveil/protocol/TracingCookieFetcher;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/protocol/TracingCookieFetcher;->getFreshCookie()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->networkInfoProvider:Lcom/google/android/apps/unveil/network/NetworkInfoProvider;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/network/NetworkInfoProvider;->getNetworkInfo()Lcom/google/goggles/NetworkInfoProtos$NetworkInfo;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;-><init>(ILjava/lang/String;Lcom/google/goggles/NetworkInfoProtos$NetworkInfo;Lcom/google/android/apps/unveil/protocol/TraceTracker$1;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->addAction(ILcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public clearAll()V
    .locals 2

    .prologue
    .line 442
    iget-object v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v1

    .line 443
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 444
    monitor-exit v1

    .line 445
    return-void

    .line 444
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized deleteTraceAction(I)V
    .locals 4
    .parameter "actionNumber"

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 312
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 313
    sget-object v0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "key was not in actions list!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 319
    :goto_0
    monitor-exit p0

    return-void

    .line 317
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 311
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized endInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V
    .locals 1
    .parameter "intervalName"

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->endInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    monitor-exit p0

    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized endInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;I)V
    .locals 6
    .parameter "intervalName"
    .parameter "actionNumber"

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 189
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_0

    .line 190
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "TraceAction %d has already been completed, cannot end interval %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    :goto_0
    monitor-exit p0

    return-void

    .line 194
    :cond_0
    :try_start_1
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->endIntervalNow(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$100(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 188
    .end local v0           #actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public endIntervalDelayed(Lcom/google/goggles/TracingProtos$SpanVariable$Type;IJ)V
    .locals 6
    .parameter "intervalName"
    .parameter "actionNumber"
    .parameter "intervalEndedTimestamp"

    .prologue
    .line 199
    invoke-direct {p0, p2}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 200
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_0

    .line 201
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "TraceAction %d has already been completed, cannot end interval %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-virtual {v0, p1, p3, p4}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->endIntervalAtTime(Lcom/google/goggles/TracingProtos$SpanVariable$Type;J)V

    goto :goto_0
.end method

.method public declared-synchronized getCurrentActionNumber()I
    .locals 1

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->traceActionNumber:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLatestActionString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    iget v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->traceActionNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    iget v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->traceActionNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getTracingCookieForAction(I)Ljava/lang/String;
    .locals 4
    .parameter "actionNumber"

    .prologue
    .line 337
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 338
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_0

    .line 339
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Null action in getTracingCookieForAction."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 340
    const/4 v1, 0x0

    .line 343
    :goto_0
    return-object v1

    :cond_0
    #getter for: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->tracingCookie:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$900(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getTracingCookieForCurrentAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getTracingCookieForAction(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasPendingActions()Z
    .locals 4

    .prologue
    .line 452
    iget-object v3, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v3

    .line 453
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    .line 454
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->isEnded()Z
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$400(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 455
    const/4 v2, 0x1

    monitor-exit v3

    .line 459
    .end local v0           #actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    :goto_0
    return v2

    .line 458
    :cond_1
    monitor-exit v3

    .line 459
    const/4 v2, 0x0

    goto :goto_0

    .line 458
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public populateRequest(Lcom/google/goggles/TracingProtos$TraceRequest$Builder;)V
    .locals 2
    .parameter "traceRequest"

    .prologue
    .line 423
    iget-object v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v1

    .line 424
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->tryToEndAllExcept(Ljava/util/List;)V

    .line 425
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->addFinishedTraceActions(Lcom/google/goggles/TracingProtos$TraceRequest$Builder;)V

    .line 426
    invoke-direct {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->clearFinishedTraceActions()V

    .line 427
    monitor-exit v1

    .line 428
    return-void

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public populateRequestContinuous(Lcom/google/goggles/TracingProtos$TraceRequest$Builder;)V
    .locals 2
    .parameter "traceRequest"

    .prologue
    .line 435
    iget-object v1, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    monitor-enter v1

    .line 436
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->addFinishedTraceActions(Lcom/google/goggles/TracingProtos$TraceRequest$Builder;)V

    .line 437
    invoke-direct {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->clearFinishedTraceActions()V

    .line 438
    monitor-exit v1

    .line 439
    return-void

    .line 438
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setTrackingId(Ljava/lang/String;I)V
    .locals 6
    .parameter "trackingId"
    .parameter "actionNumber"

    .prologue
    .line 466
    invoke-direct {p0, p2}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 467
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-nez v0, :cond_0

    .line 468
    sget-object v1, Lcom/google/android/apps/unveil/protocol/TraceTracker;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Null action in setTracingId for number %d."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 472
    :goto_0
    return-void

    .line 471
    :cond_0
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->setTrackingId(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$1000(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized startNewTraceAction()V
    .locals 1

    .prologue
    .line 326
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->tryToEndTraceAction(I)V

    .line 329
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->incrementCurrentActionNumber()V

    .line 330
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->beginTraceAction(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    monitor-exit p0

    return-void

    .line 326
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .local v0, s:Ljava/lang/StringBuilder;
    const-string v1, "TraceTracker ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string v1, "traceActionNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->traceActionNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v1, "currentActions("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "),"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public declared-synchronized tryToEndAllExcept(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 299
    .local p1, excluding:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/unveil/protocol/TraceTracker;->currentActions:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    .line 300
    .local v0, a:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    #getter for: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->actionNumber:I
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$800(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 301
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->isEnded()Z
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$400(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Z

    move-result v2

    if-nez v2, :cond_1

    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->shouldEnd()Z
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$500(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 302
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->end()V
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$600(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 299
    .end local v0           #a:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 304
    .restart local v0       #a:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_1
    :try_start_1
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$708(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 308
    .end local v0           #a:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized tryToEndTraceAction(I)V
    .locals 2
    .parameter "actionNumber"

    .prologue
    .line 285
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getActionData(I)Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;

    move-result-object v0

    .line 286
    .local v0, actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    if-eqz v0, :cond_0

    .line 287
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->isEnded()Z
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$400(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Z

    move-result v1

    if-nez v1, :cond_1

    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->shouldEnd()Z
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$500(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 288
    #calls: Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->end()V
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$600(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 290
    :cond_1
    :try_start_1
    invoke-static {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;->access$704(Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 285
    .end local v0           #actionData:Lcom/google/android/apps/unveil/protocol/TraceTracker$ActionData;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
