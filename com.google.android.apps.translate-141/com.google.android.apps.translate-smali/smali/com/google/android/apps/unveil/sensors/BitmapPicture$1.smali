.class Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;
.super Ljava/lang/Object;
.source "BitmapPicture.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/unveil/sensors/BitmapPicture;-><init>([BIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

.field final synthetic val$height:I

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/unveil/sensors/BitmapPicture;II)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    iput p2, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->val$width:I

    iput p3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->val$height:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 58
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    monitor-enter v4

    .line 60
    :try_start_0
    iget v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->val$width:I

    iget v5, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->val$height:I

    mul-int/2addr v3, v5

    mul-int/lit8 v3, v3, 0x2

    new-array v1, v3, [B

    .line 61
    .local v1, rgbArray:[B
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    #getter for: Lcom/google/android/apps/unveil/sensors/BitmapPicture;->cachedYuvData:[B
    invoke-static {v3}, Lcom/google/android/apps/unveil/sensors/BitmapPicture;->access$000(Lcom/google/android/apps/unveil/sensors/BitmapPicture;)[B

    move-result-object v3

    if-eqz v3, :cond_0

    .line 62
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    #getter for: Lcom/google/android/apps/unveil/sensors/BitmapPicture;->cachedYuvData:[B
    invoke-static {v3}, Lcom/google/android/apps/unveil/sensors/BitmapPicture;->access$000(Lcom/google/android/apps/unveil/sensors/BitmapPicture;)[B

    move-result-object v3

    iget v5, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->val$width:I

    iget v6, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->val$height:I

    invoke-static {v3, v1, v5, v6}, Lcom/google/android/apps/unveil/env/ImageUtils;->convertYUV420SPToRGB565([B[BII)V

    .line 63
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 64
    .local v2, rgbBuffer:Ljava/nio/ByteBuffer;
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    iget v5, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->val$width:I

    iget v6, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->val$height:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    #setter for: Lcom/google/android/apps/unveil/sensors/BitmapPicture;->cachedBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3, v5}, Lcom/google/android/apps/unveil/sensors/BitmapPicture;->access$102(Lcom/google/android/apps/unveil/sensors/BitmapPicture;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 65
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    #getter for: Lcom/google/android/apps/unveil/sensors/BitmapPicture;->cachedBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/google/android/apps/unveil/sensors/BitmapPicture;->access$100(Lcom/google/android/apps/unveil/sensors/BitmapPicture;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v2           #rgbBuffer:Ljava/nio/ByteBuffer;
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    const/4 v5, 0x1

    #setter for: Lcom/google/android/apps/unveil/sensors/BitmapPicture;->cachedBitmapReady:Z
    invoke-static {v3, v5}, Lcom/google/android/apps/unveil/sensors/BitmapPicture;->access$302(Lcom/google/android/apps/unveil/sensors/BitmapPicture;Z)Z

    .line 72
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 74
    .end local v1           #rgbArray:[B
    :goto_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, e:Ljava/lang/OutOfMemoryError;
    :try_start_2
    invoke-static {}, Lcom/google/android/apps/unveil/sensors/BitmapPicture;->access$200()Lcom/google/android/apps/unveil/env/UnveilLogger;

    move-result-object v3

    const-string v5, "Out of memory when creating bitmap picture."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 71
    :try_start_3
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    const/4 v5, 0x1

    #setter for: Lcom/google/android/apps/unveil/sensors/BitmapPicture;->cachedBitmapReady:Z
    invoke-static {v3, v5}, Lcom/google/android/apps/unveil/sensors/BitmapPicture;->access$302(Lcom/google/android/apps/unveil/sensors/BitmapPicture;Z)Z

    .line 72
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    goto :goto_0

    .line 74
    .end local v0           #e:Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 71
    :catchall_1
    move-exception v3

    :try_start_4
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    const/4 v6, 0x1

    #setter for: Lcom/google/android/apps/unveil/sensors/BitmapPicture;->cachedBitmapReady:Z
    invoke-static {v5, v6}, Lcom/google/android/apps/unveil/sensors/BitmapPicture;->access$302(Lcom/google/android/apps/unveil/sensors/BitmapPicture;Z)Z

    .line 72
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/BitmapPicture$1;->this$0:Lcom/google/android/apps/unveil/sensors/BitmapPicture;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method
