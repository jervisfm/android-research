.class Lcom/google/android/apps/unveil/sensors/CameraManager$10;
.super Ljava/lang/Object;
.source "CameraManager.java"

# interfaces
.implements Landroid/hardware/Camera$ShutterCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/unveil/sensors/CameraManager;->takePicture(Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V
    .locals 0
    .parameter

    .prologue
    .line 556
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$10;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShutter()V
    .locals 5

    .prologue
    .line 559
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$10;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    monitor-enter v3

    .line 560
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$10;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    const/4 v4, 0x4

    #setter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I
    invoke-static {v2, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$002(Lcom/google/android/apps/unveil/sensors/CameraManager;I)I

    .line 561
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$10;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #getter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->shutterListeners:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$100(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;

    .line 562
    .local v0, cb:Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;
    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;->onShutter()V

    goto :goto_0

    .line 565
    .end local v0           #cb:Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 564
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$10;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #getter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->shutterListeners:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$100(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 565
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 566
    return-void
.end method
