.class Lcom/google/android/apps/unveil/sensors/CameraManager$11;
.super Ljava/lang/Object;
.source "CameraManager.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/unveil/sensors/CameraManager;->focus(Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V
    .locals 0
    .parameter

    .prologue
    .line 660
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 7
    .parameter "success"
    .parameter "camera"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 663
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    monitor-enter v3

    .line 664
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    const/4 v4, 0x2

    #setter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I
    invoke-static {v2, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$002(Lcom/google/android/apps/unveil/sensors/CameraManager;I)I

    .line 666
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #calls: Lcom/google/android/apps/unveil/sensors/CameraManager;->maybeSetPendingCameraParameters()V
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$200(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    .line 668
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #getter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$300(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;

    .line 669
    .local v0, cb:Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;
    invoke-interface {v0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;->onFocus(Z)V

    goto :goto_0

    .line 673
    .end local v0           #cb:Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 671
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #getter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$300(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 672
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    const/4 v4, 0x0

    #setter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I
    invoke-static {v2, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$002(Lcom/google/android/apps/unveil/sensors/CameraManager;I)I

    .line 673
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 674
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #getter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseRequiredAfterFocus:Z
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$400(Lcom/google/android/apps/unveil/sensors/CameraManager;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 675
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #setter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseRequiredAfterFocus:Z
    invoke-static {v2, v5}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$402(Lcom/google/android/apps/unveil/sensors/CameraManager;Z)Z

    .line 676
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #calls: Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseCamera()V
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$500(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    .line 684
    :cond_1
    :goto_1
    return-void

    .line 677
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #getter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->snapRequiredAfterFocus:Z
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$600(Lcom/google/android/apps/unveil/sensors/CameraManager;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 678
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #setter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->snapRequiredAfterFocus:Z
    invoke-static {v2, v5}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$602(Lcom/google/android/apps/unveil/sensors/CameraManager;Z)Z

    .line 679
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v2, v6, v6}, Lcom/google/android/apps/unveil/sensors/CameraManager;->takePicture(Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;)V

    goto :goto_1

    .line 680
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #getter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$700(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 681
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #getter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$700(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setFlashMode(Ljava/lang/String;)V

    .line 682
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;->this$0:Lcom/google/android/apps/unveil/sensors/CameraManager;

    #setter for: Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;
    invoke-static {v2, v6}, Lcom/google/android/apps/unveil/sensors/CameraManager;->access$702(Lcom/google/android/apps/unveil/sensors/CameraManager;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1
.end method
