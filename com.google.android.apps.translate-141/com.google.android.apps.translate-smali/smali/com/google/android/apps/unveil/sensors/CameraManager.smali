.class public Lcom/google/android/apps/unveil/sensors/CameraManager;
.super Landroid/view/SurfaceView;
.source "CameraManager.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/hardware/Camera$PictureCallback;
.implements Lcom/google/android/apps/unveil/sensors/CameraProvider$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;,
        Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;,
        Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;,
        Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;,
        Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;
    }
.end annotation


# static fields
.field public static final FLASH_MODE_UNSUPPORTED:Ljava/lang/String; = "unsupported"

.field public static final RATIO_SLOP:F = 0.1f

.field private static final STATE_FOCUSED:I = 0x2

.field private static final STATE_FOCUSING:I = 0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_SNAPPED:I = 0x4

.field private static final STATE_SNAPPING:I = 0x3

.field private static final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

.field private static specialCases:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/unveil/env/Size;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile acquireCameraOnVisibilityChange:Z

.field private acquisitionPending:Z

.field private camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

.field private cameraAcquirer:Lcom/google/android/apps/unveil/sensors/CameraProvider;

.field private cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

.field private cameraParamsProvider:Lcom/google/android/apps/unveil/env/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/unveil/env/Provider",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private cameraRotation:I

.field private cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/unveil/env/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile currentDisplayRotation:I

.field private volatile currentOrientation:I

.field private currentQualitySetting:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

.field private currentZoomLevel:I

.field private deviceFullScreenDisplaySize:Lcom/google/android/apps/unveil/env/Size;

.field private final executor:Ljava/util/concurrent/Executor;

.field private flashMode:Ljava/lang/String;

.field private volatile flashSettingAfterFocus:Ljava/lang/String;

.field private final focusListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;",
            ">;"
        }
    .end annotation
.end field

.field private forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

.field private isContinuousFocusSupported:Ljava/lang/Boolean;

.field private isContinuousProvider:Lcom/google/android/apps/unveil/env/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/unveil/env/Provider",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isFlashSupported:Ljava/lang/Boolean;

.field private isFocusSupported:Ljava/lang/Boolean;

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private maxZoomLevel:I

.field private volatile naturalOrientation:I

.field private volatile pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

.field private volatile pendingStartPreview:Z

.field private final pictureListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;",
            ">;"
        }
    .end annotation
.end field

.field private previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

.field private final previewFetcherLock:Ljava/lang/Object;

.field private volatile previewing:Z

.field private volatile releaseRequiredAfterFocus:Z

.field private sensorProviderProvider:Lcom/google/android/apps/unveil/env/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/unveil/env/Provider",
            "<",
            "Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;",
            ">;"
        }
    .end annotation
.end field

.field private shouldBatchSettingOfParameters:Z

.field private final shutterListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;",
            ">;"
        }
    .end annotation
.end field

.field private volatile snapRequiredAfterFocus:Z

.field private volatile state:I

.field private textureView:Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;

.field private zoomSupported:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    sput-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 249
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;

    .line 79
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;->NORMAL_QUALITY:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentQualitySetting:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    .line 97
    iput v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    .line 101
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    .line 102
    const-string v0, "off"

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashMode:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shutterListeners:Ljava/util/List;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pictureListeners:Ljava/util/List;

    .line 107
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    .line 108
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCameraOnVisibilityChange:Z

    .line 118
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shouldBatchSettingOfParameters:Z

    .line 123
    new-instance v0, Lcom/google/android/apps/unveil/sensors/CameraManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$1;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->executor:Ljava/util/concurrent/Executor;

    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcherLock:Ljava/lang/Object;

    .line 156
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    .line 250
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->init(Landroid/content/Context;)V

    .line 251
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 254
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;

    .line 79
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;->NORMAL_QUALITY:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentQualitySetting:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    .line 97
    iput v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    .line 101
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    .line 102
    const-string v0, "off"

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashMode:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shutterListeners:Ljava/util/List;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pictureListeners:Ljava/util/List;

    .line 107
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    .line 108
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCameraOnVisibilityChange:Z

    .line 118
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shouldBatchSettingOfParameters:Z

    .line 123
    new-instance v0, Lcom/google/android/apps/unveil/sensors/CameraManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$1;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->executor:Ljava/util/concurrent/Executor;

    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcherLock:Ljava/lang/Object;

    .line 156
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    .line 255
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->init(Landroid/content/Context;)V

    .line 256
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 259
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;

    .line 79
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;->NORMAL_QUALITY:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentQualitySetting:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    .line 97
    iput v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    .line 101
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    .line 102
    const-string v0, "off"

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashMode:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shutterListeners:Ljava/util/List;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pictureListeners:Ljava/util/List;

    .line 107
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    .line 108
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCameraOnVisibilityChange:Z

    .line 118
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shouldBatchSettingOfParameters:Z

    .line 123
    new-instance v0, Lcom/google/android/apps/unveil/sensors/CameraManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$1;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->executor:Ljava/util/concurrent/Executor;

    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcherLock:Ljava/lang/Object;

    .line 156
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    .line 260
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->init(Landroid/content/Context;)V

    .line 261
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/unveil/UnveilContext;)V
    .locals 3
    .parameter "context"
    .parameter "unveilContext"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 264
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;

    .line 79
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;->NORMAL_QUALITY:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentQualitySetting:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    .line 97
    iput v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    .line 101
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    .line 102
    const-string v0, "off"

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashMode:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shutterListeners:Ljava/util/List;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pictureListeners:Ljava/util/List;

    .line 107
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    .line 108
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCameraOnVisibilityChange:Z

    .line 118
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shouldBatchSettingOfParameters:Z

    .line 123
    new-instance v0, Lcom/google/android/apps/unveil/sensors/CameraManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$1;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->executor:Ljava/util/concurrent/Executor;

    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcherLock:Ljava/lang/Object;

    .line 156
    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    .line 265
    invoke-direct {p0, v2, p2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->init(Landroid/content/Context;Lcom/google/android/apps/unveil/UnveilContext;)V

    .line 266
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/unveil/sensors/CameraManager;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shutterListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/unveil/sensors/CameraManager;)Lcom/google/android/apps/unveil/sensors/CameraProvider;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraAcquirer:Lcom/google/android/apps/unveil/sensors/CameraProvider;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/unveil/sensors/CameraManager;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->maybeSetPendingCameraParameters()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/unveil/sensors/CameraManager;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseRequiredAfterFocus:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/unveil/sensors/CameraManager;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseRequiredAfterFocus:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/unveil/sensors/CameraManager;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseCamera()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/unveil/sensors/CameraManager;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->snapRequiredAfterFocus:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/unveil/sensors/CameraManager;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->snapRequiredAfterFocus:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/unveil/sensors/CameraManager;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/unveil/sensors/CameraManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/unveil/sensors/CameraManager;)Lcom/google/android/apps/unveil/env/Provider;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/unveil/sensors/CameraManager;)Lcom/google/android/apps/unveil/env/Provider;
    .locals 1
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParamsProvider:Lcom/google/android/apps/unveil/env/Provider;

    return-object v0
.end method

.method private configureCameraAndStartPreview(II)V
    .locals 8
    .parameter "width"
    .parameter "height"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 934
    invoke-direct {p0, v7}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setBatchCameraParameters(Z)V

    .line 937
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setOptimalPreviewSize(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 946
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentQualitySetting:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    invoke-direct {p0, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setPictureQuality(Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;)V

    .line 947
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->queryFlashSupport()V

    .line 950
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->textureView:Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;

    if-eqz v3, :cond_0

    .line 951
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->textureView:Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;

    invoke-interface {v3, v4}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->setPreviewTexture(Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;)V

    .line 956
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->correctDisplayOrientation()V

    .line 959
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashMode:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setFlashMode(Ljava/lang/String;)V

    .line 960
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->queryFocusSupport()V

    .line 961
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->enableAutoFocus()V

    .line 962
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;

    .line 963
    .local v2, l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    invoke-interface {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;->onCameraPreviewSizeChanged()V

    goto :goto_1

    .line 938
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    :catch_0
    move-exception v0

    .line 941
    .local v0, e:Ljava/lang/RuntimeException;
    sget-object v3, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v4, "Failed to set optimal preview size."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 943
    invoke-direct {p0, v6}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setBatchCameraParameters(Z)V

    .line 974
    .end local v0           #e:Ljava/lang/RuntimeException;
    :goto_2
    return-void

    .line 953
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    .line 966
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_1
    invoke-direct {p0, v6}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setBatchCameraParameters(Z)V

    .line 968
    sget-object v3, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v4, "Starting preview!"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 969
    iput-boolean v7, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    .line 970
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-interface {v3}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->startPreview()V

    .line 972
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->queryZoomSupport()V

    .line 973
    iput v6, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentZoomLevel:I

    goto :goto_2
.end method

.method private configureSurfaceHolder()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-class v1, Lcom/google/android/apps/unveil/sensors/proxies/camera/RealCamera;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-class v1, Lcom/google/android/apps/unveil/sensors/proxies/camera/FelixCamera;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 367
    :cond_1
    return-void
.end method

.method private correctDisplayOrientation()V
    .locals 2

    .prologue
    .line 992
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    .line 993
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->correctDisplayOrientationForFroyo()V

    .line 997
    :goto_0
    return-void

    .line 995
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraToDisplayRotation(Landroid/content/Context;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->setDisplayOrientation(I)V

    goto :goto_0
.end method

.method private correctDisplayOrientationForFroyo()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1005
    iput v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    .line 1007
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentDisplayRotation:I

    packed-switch v0, :pswitch_data_0

    .line 1021
    :goto_0
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->naturalOrientation:I

    if-ne v0, v2, :cond_0

    .line 1022
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    add-int/lit8 v0, v0, 0x5a

    iput v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    .line 1023
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    const/16 v1, 0x168

    if-ne v0, v1, :cond_0

    .line 1024
    iput v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    .line 1027
    :cond_0
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Rotating camera %d degrees"

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1028
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    iget v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->setDisplayOrientation(I)V

    .line 1029
    return-void

    .line 1009
    :pswitch_0
    iput v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    goto :goto_0

    .line 1012
    :pswitch_1
    const/16 v0, 0x10e

    iput v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    goto :goto_0

    .line 1015
    :pswitch_2
    const/16 v0, 0xb4

    iput v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    goto :goto_0

    .line 1018
    :pswitch_3
    const/16 v0, 0x5a

    iput v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    goto :goto_0

    .line 1007
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private enableAutoFocus()V
    .locals 7

    .prologue
    .line 764
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v2, :cond_1

    .line 777
    :cond_0
    :goto_0
    return-void

    .line 768
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 769
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v1

    .line 770
    .local v1, params:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    const-string v2, "focus-mode"

    const-string v3, "auto"

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setCameraParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 773
    :catch_0
    move-exception v0

    .line 774
    .local v0, e:Ljava/lang/RuntimeException;
    sget-object v2, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v3, "Unable to set focus mode to: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "auto"

    aput-object v6, v4, v5

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static findOptimalSize(Ljava/util/List;IIZI)Lcom/google/android/apps/unveil/env/Size;
    .locals 19
    .parameter
    .parameter "targetWidth"
    .parameter "targetHeight"
    .parameter "requireAspectRatio"
    .parameter "maxPixels"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/env/Size;",
            ">;IIZI)",
            "Lcom/google/android/apps/unveil/env/Size;"
        }
    .end annotation

    .prologue
    .line 1545
    .local p0, sizes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/env/Size;>;"
    sget-object v14, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-virtual {v14}, Lcom/google/android/apps/unveil/env/UnveilLogger;->shouldShowVerbose()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1547
    sget-object v14, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v15, "findOptimalSize([%s], %d, %d, %b, %d)"

    const/16 v16, 0x5

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/unveil/env/Size;->sizeListToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x3

    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x4

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1551
    :cond_0
    move/from16 v0, p1

    int-to-float v14, v0

    move/from16 v0, p2

    int-to-float v15, v0

    div-float v12, v14, v15

    .line 1552
    .local v12, targetAspectRatio:F
    const v14, 0x3dcccccd

    mul-float v4, v12, v14

    .line 1553
    .local v4, allowedRatioDiff:F
    if-eqz p3, :cond_1

    .line 1554
    sget-object v14, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v15, "Target ratio is %f, allowed slop of %f"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1558
    :cond_1
    mul-int v13, p1, p2

    .line 1560
    .local v13, targetPixelCount:I
    const v5, 0x7fffffff

    .line 1561
    .local v5, bestPixelCountDiff:I
    const/4 v6, 0x0

    .line 1562
    .local v6, bestSize:Lcom/google/android/apps/unveil/env/Size;
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/unveil/env/Size;

    .line 1563
    .local v11, size:Lcom/google/android/apps/unveil/env/Size;
    if-lez p4, :cond_3

    iget v14, v11, Lcom/google/android/apps/unveil/env/Size;->width:I

    iget v15, v11, Lcom/google/android/apps/unveil/env/Size;->height:I

    mul-int/2addr v14, v15

    move/from16 v0, p4

    if-gt v14, v0, :cond_2

    .line 1566
    :cond_3
    iget v14, v11, Lcom/google/android/apps/unveil/env/Size;->width:I

    int-to-float v14, v14

    iget v15, v11, Lcom/google/android/apps/unveil/env/Size;->height:I

    int-to-float v15, v15

    div-float v9, v14, v15

    .line 1567
    .local v9, potentialAspectRatio:F
    sub-float v14, v12, v9

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v10

    .line 1570
    .local v10, ratioDiff:F
    if-eqz p3, :cond_4

    cmpg-float v14, v10, v4

    if-gtz v14, :cond_2

    .line 1571
    :cond_4
    iget v14, v11, Lcom/google/android/apps/unveil/env/Size;->width:I

    iget v15, v11, Lcom/google/android/apps/unveil/env/Size;->height:I

    mul-int/2addr v14, v15

    sub-int/2addr v14, v13

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v8

    .line 1572
    .local v8, pixelCountDiff:I
    if-ge v8, v5, :cond_2

    .line 1573
    move v5, v8

    .line 1574
    move-object v6, v11

    goto :goto_0

    .line 1579
    .end local v8           #pixelCountDiff:I
    .end local v9           #potentialAspectRatio:F
    .end local v10           #ratioDiff:F
    .end local v11           #size:Lcom/google/android/apps/unveil/env/Size;
    :cond_5
    if-nez v6, :cond_6

    if-eqz p3, :cond_6

    .line 1580
    sget-object v14, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v15, "Couldn\'t find size that meets aspect ratio requirement, trying without."

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1581
    const/4 v14, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p4

    invoke-static {v0, v1, v2, v14, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->findOptimalSize(Ljava/util/List;IIZI)Lcom/google/android/apps/unveil/env/Size;

    move-result-object v6

    .line 1587
    .end local v6           #bestSize:Lcom/google/android/apps/unveil/env/Size;
    :goto_1
    return-object v6

    .line 1582
    .restart local v6       #bestSize:Lcom/google/android/apps/unveil/env/Size;
    :cond_6
    if-nez v6, :cond_7

    .line 1583
    sget-object v14, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v15, "No optimal size!"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1586
    :cond_7
    sget-object v14, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v15, "Optimal size is %s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v6}, Lcom/google/android/apps/unveil/env/Size;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private declared-synchronized getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    .locals 3

    .prologue
    .line 1300
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v0, :cond_0

    .line 1301
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    if-eqz v0, :cond_1

    .line 1304
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "getParameters returning deferred value set while taking a picture!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1305
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 1311
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1307
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->getParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static getCameraToDisplayRotation(Landroid/content/Context;)I
    .locals 12
    .parameter "context"

    .prologue
    const/4 v9, 0x2

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 1704
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x8

    if-gt v7, v8, :cond_0

    .line 1705
    sget-object v7, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v8, "getCameraToDisplayRotation() called on API <= 8!"

    new-array v9, v6, [Ljava/lang/Object;

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1741
    :goto_0
    return v6

    .line 1709
    :cond_0
    const-string v7, "window"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 1710
    .local v5, wm:Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 1711
    .local v2, displayRotation:I
    new-instance v3, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v3}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1712
    .local v3, info:Landroid/hardware/Camera$CameraInfo;
    invoke-static {v6, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1714
    const/4 v1, 0x0

    .line 1715
    .local v1, degrees:I
    packed-switch v2, :pswitch_data_0

    .line 1723
    :goto_1
    iget v7, v3, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v7, v11, :cond_1

    .line 1724
    iget v7, v3, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v7, v1

    rem-int/lit16 v0, v7, 0x168

    .line 1725
    .local v0, adjustedRotation:I
    rsub-int v7, v0, 0x168

    rem-int/lit16 v0, v7, 0x168

    .line 1740
    :goto_2
    sget-object v7, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v8, "getDisplayRotation() %d, %d"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v11

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v6, v0

    .line 1741
    goto :goto_0

    .line 1716
    .end local v0           #adjustedRotation:I
    :pswitch_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1717
    :pswitch_1
    const/16 v1, 0x5a

    goto :goto_1

    .line 1718
    :pswitch_2
    const/16 v1, 0xb4

    goto :goto_1

    .line 1719
    :pswitch_3
    const/16 v1, 0x10e

    goto :goto_1

    .line 1727
    :cond_1
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xa

    if-le v7, v8, :cond_2

    .line 1728
    iget v7, v3, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v7, v1

    add-int/lit16 v7, v7, 0x168

    rem-int/lit16 v0, v7, 0x168

    .restart local v0       #adjustedRotation:I
    goto :goto_2

    .line 1734
    .end local v0           #adjustedRotation:I
    :cond_2
    invoke-static {p0}, Lcom/google/android/apps/unveil/env/Viewport;->computeNaturalOrientation(Landroid/content/Context;)I

    move-result v7

    if-ne v7, v9, :cond_3

    move v4, v6

    .line 1737
    .local v4, orientationGuess:I
    :goto_3
    sub-int v7, v4, v1

    add-int/lit16 v7, v7, 0x168

    rem-int/lit16 v0, v7, 0x168

    .restart local v0       #adjustedRotation:I
    goto :goto_2

    .line 1734
    .end local v0           #adjustedRotation:I
    .end local v4           #orientationGuess:I
    :cond_3
    const/16 v4, 0x5a

    goto :goto_3

    .line 1715
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getSpecialCasePreviewSize()Lcom/google/android/apps/unveil/env/Size;
    .locals 8

    .prologue
    const/16 v7, 0x280

    const/16 v6, 0x1e0

    .line 1402
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 1403
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    .line 1408
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    const-string v2, "SPH-M900"

    new-instance v3, Lcom/google/android/apps/unveil/env/Size;

    invoke-direct {v3, v7, v6}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1413
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    const-string v2, "DROIDX"

    new-instance v3, Lcom/google/android/apps/unveil/env/Size;

    const/16 v4, 0x320

    const/16 v5, 0x1c0

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1417
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    const-string v2, "XT720"

    new-instance v3, Lcom/google/android/apps/unveil/env/Size;

    const/16 v4, 0x350

    invoke-direct {v3, v4, v6}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1422
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    const-string v2, "Nexus S"

    new-instance v3, Lcom/google/android/apps/unveil/env/Size;

    invoke-direct {v3, v7, v6}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    const-string v2, "Droid"

    new-instance v3, Lcom/google/android/apps/unveil/env/Size;

    invoke-direct {v3, v7, v6}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1430
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    const-string v2, "SGH-T999"

    new-instance v3, Lcom/google/android/apps/unveil/env/Size;

    invoke-direct {v3, v7, v6}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1433
    :cond_0
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1434
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->specialCases:Ljava/util/HashMap;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/env/Size;

    .line 1435
    .local v0, specialCaseSize:Lcom/google/android/apps/unveil/env/Size;
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Special case: using %s for preview size on %s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1440
    .end local v0           #specialCaseSize:Lcom/google/android/apps/unveil/env/Size;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 269
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/unveil/UnveilContext;

    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/UnveilContext;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->init(Landroid/content/Context;Lcom/google/android/apps/unveil/UnveilContext;)V

    .line 274
    :goto_0
    return-void

    .line 272
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->init(Landroid/content/Context;Lcom/google/android/apps/unveil/UnveilContext;)V

    goto :goto_0
.end method

.method private init(Landroid/content/Context;Lcom/google/android/apps/unveil/UnveilContext;)V
    .locals 3
    .parameter "context"
    .parameter "unveilContext"

    .prologue
    .line 277
    if-eqz p2, :cond_0

    .line 278
    new-instance v1, Lcom/google/android/apps/unveil/sensors/CameraManager$2;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/unveil/sensors/CameraManager$2;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;Lcom/google/android/apps/unveil/UnveilContext;)V

    iput-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->sensorProviderProvider:Lcom/google/android/apps/unveil/env/Provider;

    .line 284
    new-instance v1, Lcom/google/android/apps/unveil/sensors/CameraManager$3;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/unveil/sensors/CameraManager$3;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;Lcom/google/android/apps/unveil/UnveilContext;)V

    iput-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousProvider:Lcom/google/android/apps/unveil/env/Provider;

    .line 290
    new-instance v1, Lcom/google/android/apps/unveil/sensors/CameraManager$4;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/unveil/sensors/CameraManager$4;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;Lcom/google/android/apps/unveil/UnveilContext;)V

    iput-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    .line 296
    new-instance v1, Lcom/google/android/apps/unveil/sensors/CameraManager$5;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/unveil/sensors/CameraManager$5;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;Lcom/google/android/apps/unveil/UnveilContext;)V

    iput-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParamsProvider:Lcom/google/android/apps/unveil/env/Provider;

    .line 304
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 305
    .local v0, surfaceHolder:Landroid/view/SurfaceHolder;
    invoke-interface {v0}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    .line 306
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 308
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->configureSurfaceHolder()V

    .line 310
    if-eqz p1, :cond_1

    .line 311
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->updateCurrentRotationAndOrientation(I)Z

    .line 314
    :cond_1
    new-instance v1, Lcom/google/android/apps/unveil/sensors/CameraProvider;

    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->executor:Ljava/util/concurrent/Executor;

    invoke-direct {v1, v2, p0}, Lcom/google/android/apps/unveil/sensors/CameraProvider;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/apps/unveil/sensors/CameraProvider$Listener;)V

    iput-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraAcquirer:Lcom/google/android/apps/unveil/sensors/CameraProvider;

    .line 315
    return-void
.end method

.method public static isFrontFacingCamera(Landroid/content/Context;)Z
    .locals 7
    .parameter "context"

    .prologue
    const/4 v3, 0x0

    .line 1748
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x8

    if-gt v4, v5, :cond_1

    .line 1749
    sget-object v4, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "isFrontFacingCamera() called on API <= 8!"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1761
    :cond_0
    :goto_0
    return v3

    .line 1752
    :cond_1
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    .line 1753
    .local v0, count:I
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1755
    .local v2, info:Landroid/hardware/Camera$CameraInfo;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 1756
    invoke-static {v1, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1757
    iget v4, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-eqz v4, :cond_0

    .line 1755
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1761
    :cond_2
    if-lez v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isTakingPicture()Z
    .locals 2

    .prologue
    .line 1385
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized maybeSetPendingCameraParameters()V
    .locals 3

    .prologue
    .line 1343
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v0, :cond_1

    .line 1344
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "No camera yet to set parameters on."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1382
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1348
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    if-eqz v0, :cond_0

    .line 1352
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shouldBatchSettingOfParameters:Z

    if-eqz v0, :cond_2

    .line 1354
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "maybeSetPendingCameraParameters() directly called while cache is active."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1360
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 1362
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isTakingPicture()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1363
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Taking picture, aborting setParameters."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1343
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1367
    :cond_3
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1368
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Focusing, aborting setParameters."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1372
    :cond_4
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Setting camera parameters."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1373
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    invoke-interface {v0, v1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->setParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V

    .line 1374
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 1381
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private queryFlashSupport()V
    .locals 10

    .prologue
    .line 1078
    iget-object v8, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFlashSupported:Ljava/lang/Boolean;

    if-eqz v8, :cond_1

    .line 1100
    :cond_0
    :goto_0
    return-void

    .line 1082
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v8

    const-string v9, "flash-mode-values"

    invoke-interface {v8, v9}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1084
    .local v2, flashModesAsString:Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1088
    const/4 v7, 0x0

    .line 1089
    .local v7, torch:Z
    const/4 v6, 0x0

    .line 1090
    .local v6, off:Z
    const-string v8, ","

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1091
    .local v1, flashModes:[Ljava/lang/String;
    move-object v0, v1

    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v5, v0, v3

    .line 1092
    .local v5, mode:Ljava/lang/String;
    const-string v8, "torch"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1093
    const/4 v7, 0x1

    .line 1091
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1094
    :cond_3
    const-string v8, "off"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1095
    const/4 v6, 0x1

    goto :goto_2

    .line 1099
    .end local v5           #mode:Ljava/lang/String;
    :cond_4
    if-eqz v6, :cond_5

    if-eqz v7, :cond_5

    const/4 v8, 0x1

    :goto_3
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFlashSupported:Ljava/lang/Boolean;

    goto :goto_0

    :cond_5
    const/4 v8, 0x0

    goto :goto_3
.end method

.method private queryFocusSupport()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1108
    iget-object v6, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported:Ljava/lang/Boolean;

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported:Ljava/lang/Boolean;

    if-eqz v6, :cond_1

    .line 1126
    :cond_0
    return-void

    .line 1112
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v6

    const-string v7, "focus-mode-values"

    invoke-interface {v6, v7}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1114
    .local v2, focusModesAsString:Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1118
    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1119
    .local v1, focusModes:[Ljava/lang/String;
    move-object v0, v1

    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 1120
    .local v5, mode:Ljava/lang/String;
    const-string v6, "auto"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1121
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported:Ljava/lang/Boolean;

    .line 1119
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1122
    :cond_3
    const-string v6, "continuous-picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1123
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method private queryZoomSupport()V
    .locals 2

    .prologue
    .line 977
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v0

    .line 979
    .local v0, parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->isSmoothZoomSupported()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->zoomSupported:Z

    .line 981
    iget-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->zoomSupported:Z

    if-eqz v1, :cond_0

    .line 982
    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->getMaxZoom()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->maxZoomLevel:I

    .line 984
    :cond_0
    return-void
.end method

.method private declared-synchronized releaseCamera()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1069
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    if-ne v0, v1, :cond_0

    .line 1070
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Deferring camera release until after focus"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1071
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseRequiredAfterFocus:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1075
    :goto_0
    monitor-exit p0

    return-void

    .line 1074
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->forceReleaseCamera()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1069
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private roundOffOrientation(I)I
    .locals 2
    .parameter "cameraOrientation"

    .prologue
    .line 1612
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 1615
    add-int/lit8 v0, p1, 0x5a

    .line 1619
    .local v0, orientation:I
    :goto_0
    return v0

    .line 1617
    .end local v0           #orientation:I
    :cond_0
    const/4 v0, 0x0

    .restart local v0       #orientation:I
    goto :goto_0
.end method

.method private declared-synchronized setBatchCameraParameters(Z)V
    .locals 1
    .parameter "shouldBatch"

    .prologue
    .line 1334
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shouldBatchSettingOfParameters:Z

    .line 1336
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shouldBatchSettingOfParameters:Z

    if-nez v0, :cond_0

    .line 1338
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->maybeSetPendingCameraParameters()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1340
    :cond_0
    monitor-exit p0

    return-void

    .line 1334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setCameraParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V
    .locals 1
    .parameter "parameters"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 1324
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingSetParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 1325
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 1327
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shouldBatchSettingOfParameters:Z

    if-nez v0, :cond_0

    .line 1328
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->maybeSetPendingCameraParameters()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1330
    :cond_0
    monitor-exit p0

    return-void

    .line 1324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setOptimalPictureSize(II)V
    .locals 13
    .parameter "width"
    .parameter "height"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1506
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v1

    .line 1508
    .local v1, parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v5, :cond_0

    if-nez v1, :cond_1

    .line 1533
    :cond_0
    :goto_0
    return-void

    .line 1513
    :cond_1
    :try_start_0
    invoke-interface {v1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v4

    .line 1514
    .local v4, sizes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/env/Size;>;"
    if-nez v4, :cond_2

    .line 1515
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Failed to find picture sizes in camera parameters."

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1525
    .end local v4           #sizes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/env/Size;>;"
    :catch_0
    move-exception v0

    .line 1526
    .local v0, e:Ljava/lang/RuntimeException;
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "No suitable picture size available, forcing %dx%d"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1527
    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->setPictureSize(II)V

    .line 1529
    .end local v0           #e:Ljava/lang/RuntimeException;
    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setCameraParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V

    .line 1531
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPictureSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    .line 1532
    .local v2, pictureSize:Lcom/google/android/apps/unveil/env/Size;
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Set picture size to %dx%d"

    new-array v7, v12, [Ljava/lang/Object;

    iget v8, v2, Lcom/google/android/apps/unveil/env/Size;->width:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    iget v8, v2, Lcom/google/android/apps/unveil/env/Size;->height:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1518
    .end local v2           #pictureSize:Lcom/google/android/apps/unveil/env/Size;
    .restart local v4       #sizes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/env/Size;>;"
    :cond_2
    const/4 v5, 0x0

    const/4 v6, 0x0

    :try_start_1
    invoke-static {v4, p1, p2, v5, v6}, Lcom/google/android/apps/unveil/sensors/CameraManager;->findOptimalSize(Ljava/util/List;IIZI)Lcom/google/android/apps/unveil/env/Size;

    move-result-object v3

    .line 1519
    .local v3, size:Lcom/google/android/apps/unveil/env/Size;
    if-nez v3, :cond_3

    .line 1520
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Could not find a suitable picture size."

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1523
    :cond_3
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Attempting to set optimal picture size %dx%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget v9, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1524
    iget v5, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    iget v6, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    invoke-interface {v1, v5, v6}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->setPictureSize(II)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private setOptimalPreviewSize(II)V
    .locals 10
    .parameter "width"
    .parameter "height"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1481
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v0

    .line 1483
    .local v0, parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v3, :cond_0

    if-nez v0, :cond_1

    .line 1503
    :cond_0
    :goto_0
    return-void

    .line 1487
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    if-eqz v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    .line 1491
    .local v2, size:Lcom/google/android/apps/unveil/env/Size;
    :goto_1
    if-nez v2, :cond_3

    .line 1492
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Could not find a suitable preview size."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1487
    .end local v2           #size:Lcom/google/android/apps/unveil/env/Size;
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getOptimalPreviewSize(II)Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    goto :goto_1

    .line 1495
    .restart local v2       #size:Lcom/google/android/apps/unveil/env/Size;
    :cond_3
    sget-object v4, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "Attempting to set optimal preview size %dx%d %s"

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/Object;

    iget v3, v2, Lcom/google/android/apps/unveil/env/Size;->width:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    iget v3, v2, Lcom/google/android/apps/unveil/env/Size;->height:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v8

    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    if-nez v3, :cond_4

    const-string v3, ""

    :goto_2
    aput-object v3, v6, v9

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1497
    iget v3, v2, Lcom/google/android/apps/unveil/env/Size;->width:I

    iget v4, v2, Lcom/google/android/apps/unveil/env/Size;->height:I

    invoke-interface {v0, v3, v4}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->setPreviewSize(II)V

    .line 1499
    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setCameraParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V

    .line 1501
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v1

    .line 1502
    .local v1, previewSize:Lcom/google/android/apps/unveil/env/Size;
    sget-object v3, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v4, "Set preview size to %dx%d"

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, v1, Lcom/google/android/apps/unveil/env/Size;->width:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget v6, v1, Lcom/google/android/apps/unveil/env/Size;->height:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1495
    .end local v1           #previewSize:Lcom/google/android/apps/unveil/env/Size;
    :cond_4
    const-string v3, "based on forced preview size"

    goto :goto_2
.end method

.method private setPictureQuality(Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;)V
    .locals 8
    .parameter "quality"

    .prologue
    .line 710
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v4, :cond_1

    .line 730
    :cond_0
    :goto_0
    return-void

    .line 714
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v3

    .line 719
    .local v3, params:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    const-string v4, "jpeg-quality"

    iget v5, p1, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;->jpegQuality:I

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->set(Ljava/lang/String;I)V

    .line 722
    :try_start_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setCameraParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V

    .line 723
    iget v4, p1, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;->desiredWidth:I

    iget v5, p1, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;->desiredHeight:I

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setOptimalPictureSize(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 724
    :catch_0
    move-exception v0

    .line 725
    .local v0, e:Ljava/lang/RuntimeException;
    sget-object v4, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "Unable to set quality to: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 726
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;

    .line 727
    .local v2, l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    invoke-interface {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;->onCameraQualityError()V

    goto :goto_1
.end method

.method private updateCurrentRotationAndOrientation(I)Z
    .locals 3
    .parameter "newOrientation"

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/unveil/env/Viewport;->computeNaturalOrientation(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->naturalOrientation:I

    .line 402
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 404
    .local v0, display:Landroid/view/Display;
    iget v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentDisplayRotation:I

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentOrientation:I

    if-eq v1, p1, :cond_1

    .line 405
    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentDisplayRotation:I

    .line 406
    iput p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentOrientation:I

    .line 407
    const/4 v1, 0x1

    .line 409
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized acquireCamera()V
    .locals 4

    .prologue
    .line 818
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "acquireCamera"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 820
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquisitionPending:Z

    if-nez v0, :cond_1

    .line 821
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "CameraManager is requesting a camera to manage."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 822
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquisitionPending:Z

    .line 825
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 826
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraAcquirer:Lcom/google/android/apps/unveil/sensors/CameraProvider;

    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParamsProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v1}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/apps/unveil/sensors/CameraProvider;->acquireCamera(Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 842
    :goto_0
    monitor-exit p0

    return-void

    .line 829
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/unveil/sensors/CameraManager$12;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$12;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 818
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 837
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v0, :cond_2

    .line 838
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "CameraManager is already waiting for a pending camera request."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 840
    :cond_2
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "CameraManager already has a camera, ignoring call to acquireCamera."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public addPreviewBuffer([B)V
    .locals 4
    .parameter "previewBuffer"

    .prologue
    .line 1277
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcherLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1278
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    if-eqz v0, :cond_0

    .line 1279
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/sensors/PreviewFetcher;->addPreviewBuffer([B)V

    .line 1283
    :goto_0
    monitor-exit v1

    .line 1284
    return-void

    .line 1281
    :cond_0
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Tried to give buffer to null PreviewFetcher"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1283
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearCachedCameraParams()V
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 441
    return-void
.end method

.method public enableContinuousFocus(Z)V
    .locals 7
    .parameter "enabled"

    .prologue
    .line 780
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v2, :cond_1

    .line 800
    :cond_0
    :goto_0
    return-void

    .line 784
    :cond_1
    if-nez p1, :cond_2

    .line 785
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->enableAutoFocus()V

    goto :goto_0

    .line 789
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 790
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v1

    .line 791
    .local v1, params:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    const-string v2, "focus-mode"

    const-string v3, "continuous-picture"

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setCameraParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 795
    :catch_0
    move-exception v0

    .line 796
    .local v0, e:Ljava/lang/RuntimeException;
    sget-object v2, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v3, "Unable to set focus mode to: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "continuous-picture"

    aput-object v6, v4, v5

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public declared-synchronized focus(Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;)V
    .locals 7
    .parameter "focusCb"

    .prologue
    const/4 v6, 0x1

    .line 636
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_1

    .line 696
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 640
    :cond_1
    :try_start_1
    iget v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 641
    sget-object v4, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "Focus just finished and callbacks are being invoked, ignoring focus request!"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 636
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 645
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isTakingPicture()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 646
    sget-object v4, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "A picture is being taken now, ignoring focus request!"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 652
    :cond_3
    if-eqz p1, :cond_4

    .line 653
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 655
    :cond_4
    iget v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    if-eq v4, v6, :cond_0

    .line 658
    const/4 v4, 0x1

    iput v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    .line 660
    new-instance v0, Lcom/google/android/apps/unveil/sensors/CameraManager$11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$11;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 688
    .local v0, autoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;
    :try_start_3
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-interface {v4, v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 689
    :catch_0
    move-exception v2

    .line 690
    .local v2, e:Ljava/lang/RuntimeException;
    const/4 v4, 0x0

    :try_start_4
    iput v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    .line 691
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;

    .line 692
    .local v1, cb:Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;->onFocus(Z)V

    goto :goto_1

    .line 694
    .end local v1           #cb:Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;
    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->focusListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized forceReleaseCamera()V
    .locals 3

    .prologue
    .line 1044
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v0, :cond_2

    .line 1045
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1046
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Attempting cancelAutoFocus call."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1047
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->cancelAutoFocus()V

    .line 1049
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1050
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    if-eqz v0, :cond_1

    .line 1051
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->stopPreview()V

    .line 1054
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraAcquirer:Lcom/google/android/apps/unveil/sensors/CameraProvider;

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/sensors/CameraProvider;->releaseCamera(Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;)V

    .line 1055
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    .line 1057
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 1060
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    .line 1061
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    .line 1062
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseRequiredAfterFocus:Z

    .line 1063
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->snapRequiredAfterFocus:Z

    .line 1064
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    .line 1065
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1066
    monitor-exit p0

    return-void

    .line 1044
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCameraRotation()I
    .locals 1

    .prologue
    .line 1036
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraRotation:I

    return v0
.end method

.method public getExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public getExpectedFlashMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFlashSupported:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFlashSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 808
    :cond_0
    const-string v0, "unsupported"

    .line 810
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashMode:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMirrored()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1775
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x8

    if-gt v1, v4, :cond_1

    .line 1776
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "getMirrored() called on API <= 8!"

    new-array v4, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1789
    :cond_0
    :goto_0
    return v3

    .line 1781
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v1}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-class v4, Lcom/google/android/apps/unveil/sensors/proxies/camera/RealCamera;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v1}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-class v4, Lcom/google/android/apps/unveil/sensors/proxies/camera/FelixCamera;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1787
    :cond_2
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1788
    .local v0, ci:Landroid/hardware/Camera$CameraInfo;
    invoke-static {v3, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1789
    iget v1, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v1, v2, :cond_3

    move v1, v2

    :goto_1
    move v3, v1

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1
.end method

.method public getOptimalPreviewSize(II)Lcom/google/android/apps/unveil/env/Size;
    .locals 5
    .parameter "width"
    .parameter "height"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 1444
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCamera()V

    .line 1446
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v0

    .line 1448
    .local v0, parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v3, :cond_0

    if-nez v0, :cond_2

    .line 1449
    :cond_0
    const/4 v1, 0x0

    .line 1469
    :cond_1
    :goto_0
    return-object v1

    .line 1455
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Lcom/google/android/apps/unveil/sensors/proxies/camera/RealCamera;

    if-ne v3, v4, :cond_3

    .line 1456
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getSpecialCasePreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v1

    .line 1457
    .local v1, size:Lcom/google/android/apps/unveil/env/Size;
    if-nez v1, :cond_1

    .line 1464
    .end local v1           #size:Lcom/google/android/apps/unveil/env/Size;
    :cond_3
    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    .line 1465
    .local v2, sizes:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/env/Size;>;"
    if-nez v2, :cond_4

    .line 1466
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Failed to find preview sizes in camera parameters."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1469
    :cond_4
    const/4 v4, 0x1

    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v3}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    const v3, 0x63d80

    :goto_1
    invoke-static {v2, p1, p2, v4, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->findOptimalSize(Ljava/util/List;IIZI)Lcom/google/android/apps/unveil/env/Size;

    move-result-object v1

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getPictureQuality()Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentQualitySetting:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    return-object v0
.end method

.method public getPictureSize()Lcom/google/android/apps/unveil/env/Size;
    .locals 2

    .prologue
    .line 1149
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v0

    .line 1150
    .local v0, params:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    if-nez v0, :cond_0

    .line 1151
    const/4 v1, 0x0

    .line 1153
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->getPictureSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v1

    goto :goto_0
.end method

.method public getPreviewSize()Lcom/google/android/apps/unveil/env/Size;
    .locals 2

    .prologue
    .line 1137
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    if-eqz v1, :cond_0

    .line 1138
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    .line 1145
    :goto_0
    return-object v1

    .line 1141
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v0

    .line 1142
    .local v0, params:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    if-nez v0, :cond_1

    .line 1143
    const/4 v1, 0x0

    goto :goto_0

    .line 1145
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v1

    goto :goto_0
.end method

.method public getStateName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1623
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    packed-switch v0, :pswitch_data_0

    .line 1640
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 1625
    :pswitch_0
    const-string v0, "FOCUSED"

    goto :goto_0

    .line 1628
    :pswitch_1
    const-string v0, "FOCUSING"

    goto :goto_0

    .line 1631
    :pswitch_2
    const-string v0, "IDLE"

    goto :goto_0

    .line 1634
    :pswitch_3
    const-string v0, "SNAPPED"

    goto :goto_0

    .line 1637
    :pswitch_4
    const-string v0, "SNAPPING"

    goto :goto_0

    .line 1623
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public init(Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/unveil/env/Provider",
            "<",
            "Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;",
            ">;",
            "Lcom/google/android/apps/unveil/env/Provider",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/apps/unveil/env/Provider",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/unveil/env/Provider",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, sensorProviderProvider:Lcom/google/android/apps/unveil/env/Provider;,"Lcom/google/android/apps/unveil/env/Provider<Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;>;"
    .local p2, isContinuousProvider:Lcom/google/android/apps/unveil/env/Provider;,"Lcom/google/android/apps/unveil/env/Provider<Ljava/lang/Boolean;>;"
    .local p3, cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;,"Lcom/google/android/apps/unveil/env/Provider<Ljava/lang/String;>;"
    .local p4, cameraParamsProvider:Lcom/google/android/apps/unveil/env/Provider;,"Lcom/google/android/apps/unveil/env/Provider<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->sensorProviderProvider:Lcom/google/android/apps/unveil/env/Provider;

    .line 325
    iput-object p2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousProvider:Lcom/google/android/apps/unveil/env/Provider;

    .line 326
    iput-object p3, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraTypeProvider:Lcom/google/android/apps/unveil/env/Provider;

    .line 327
    iput-object p4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParamsProvider:Lcom/google/android/apps/unveil/env/Provider;

    .line 329
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->configureSurfaceHolder()V

    .line 330
    return-void
.end method

.method public initDefaults()V
    .locals 4

    .prologue
    .line 337
    new-instance v0, Lcom/google/android/apps/unveil/sensors/CameraManager$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$6;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    new-instance v1, Lcom/google/android/apps/unveil/sensors/CameraManager$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$7;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    new-instance v2, Lcom/google/android/apps/unveil/sensors/CameraManager$8;

    invoke-direct {v2, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$8;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    new-instance v3, Lcom/google/android/apps/unveil/sensors/CameraManager$9;

    invoke-direct {v3, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$9;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->init(Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;)V

    .line 359
    return-void
.end method

.method public declared-synchronized isCameraConnected()Z
    .locals 1

    .prologue
    .line 1690
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isContinuousFocusSupported()Z
    .locals 1

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public isFlashSupported()Z
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFlashSupported:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFlashSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public isFocusSupported()Z
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public isFocusing()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1389
    iget v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreviewing()Z
    .locals 1

    .prologue
    .line 845
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    return v0
.end method

.method public declared-synchronized onCameraAcquired(Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;)V
    .locals 5
    .parameter "camera"

    .prologue
    .line 1658
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v3, "onCameraAcquired"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1659
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquisitionPending:Z

    .line 1660
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    .line 1662
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;

    .line 1663
    .local v1, l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    invoke-interface {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;->onCameraConnected()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1658
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1666
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->maybeSetPendingCameraParameters()V

    .line 1668
    iget-boolean v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    if-eqz v2, :cond_1

    .line 1669
    sget-object v2, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v3, "Starting preview!"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1670
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->startPreview()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1672
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized onCameraAcquisitionError()V
    .locals 3

    .prologue
    .line 1646
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquisitionPending:Z

    .line 1651
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;

    .line 1652
    .local v1, l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    invoke-interface {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;->onCameraAcquisitionError()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1646
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1654
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .parameter "newConfig"

    .prologue
    .line 381
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 382
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->updateCurrentRotationAndOrientation(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    if-eqz v0, :cond_0

    .line 384
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->stopPreview()V

    .line 385
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->correctDisplayOrientation()V

    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->startPreview()V

    .line 389
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 486
    invoke-super/range {p0 .. p5}, Landroid/view/SurfaceView;->onLayout(ZIIII)V

    .line 488
    sget-object v2, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v3, "onLayout: %b, %d, %d, %d, %d"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;

    .line 490
    .local v1, listener:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    invoke-interface {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;->onCameraLayoutComplete()V

    goto :goto_0

    .line 492
    .end local v1           #listener:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    :cond_0
    return-void
.end method

.method public declared-synchronized onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 5
    .parameter "data"
    .parameter "camera"

    .prologue
    .line 578
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->maybeSetPendingCameraParameters()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    if-nez p2, :cond_1

    .line 600
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 584
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pictureListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 586
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->sensorProviderProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v4}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-virtual {v4}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;->getRoundedDeviceOrientation()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->roundOffOrientation(I)I

    move-result v3

    .line 588
    .local v3, orientation:I
    invoke-static {p1, v3}, Lcom/google/android/apps/unveil/env/PictureFactory;->createJpeg([BI)Lcom/google/android/apps/unveil/env/Picture;

    move-result-object v2

    .line 589
    .local v2, jpeg:Lcom/google/android/apps/unveil/env/Picture;
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pictureListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;

    .line 590
    .local v0, cb:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;
    invoke-interface {v0, v2}, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;->onPictureTaken(Lcom/google/android/apps/unveil/env/Picture;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 578
    .end local v0           #cb:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #jpeg:Lcom/google/android/apps/unveil/env/Picture;
    .end local v3           #orientation:I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 592
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #jpeg:Lcom/google/android/apps/unveil/env/Picture;
    .restart local v3       #orientation:I
    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pictureListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 595
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #jpeg:Lcom/google/android/apps/unveil/env/Picture;
    .end local v3           #orientation:I
    :cond_3
    const/4 v4, 0x0

    iput v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    .line 597
    iget-boolean v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    if-eqz v4, :cond_0

    .line 598
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->startPreview()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 5
    .parameter "visibility"

    .prologue
    .line 496
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onWindowVisibilityChanged(I)V

    .line 497
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "onWindowVisibilityChanged: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 498
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCameraOnVisibilityChange:Z

    if-eqz v0, :cond_0

    .line 499
    if-nez p1, :cond_1

    .line 500
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCamera()V

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 502
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseCamera()V

    goto :goto_0
.end method

.method public registerListener(Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1288
    return-void
.end method

.method public requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 4
    .parameter "previewCb"

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v0, :cond_0

    .line 1252
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "No camera, can\'t comply with frame request."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1273
    :goto_0
    return-void

    .line 1256
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcherLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1257
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    iget-object v0, v0, Lcom/google/android/apps/unveil/sensors/PreviewFetcher;->previewCallback:Landroid/hardware/Camera$PreviewCallback;

    if-eq v0, p1, :cond_4

    .line 1259
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    if-eqz v0, :cond_2

    .line 1260
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/PreviewFetcher;->stop()V

    .line 1261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    .line 1264
    :cond_2
    if-nez p1, :cond_3

    .line 1265
    monitor-exit v1

    goto :goto_0

    .line 1271
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1268
    :cond_3
    :try_start_1
    new-instance v0, Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v3

    invoke-direct {v0, v2, p1, v3}, Lcom/google/android/apps/unveil/sensors/PreviewFetcher;-><init>(Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;Landroid/hardware/Camera$PreviewCallback;Lcom/google/android/apps/unveil/env/Size;)V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    .line 1269
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/PreviewFetcher;->start()V

    .line 1271
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1272
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewFetcher:Lcom/google/android/apps/unveil/sensors/PreviewFetcher;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/PreviewFetcher;->requestFrame()V

    goto :goto_0
.end method

.method public requestPictureQuality(Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;)V
    .locals 0
    .parameter "quality"

    .prologue
    .line 705
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentQualitySetting:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    .line 706
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setPictureQuality(Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;)V

    .line 707
    return-void
.end method

.method public setAcquireCameraOnVisibilityChange(Z)V
    .locals 0
    .parameter "enabled"

    .prologue
    .line 516
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCameraOnVisibilityChange:Z

    .line 517
    return-void
.end method

.method public setCameraProxy(Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;)V
    .locals 2
    .parameter "cam"

    .prologue
    .line 427
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->cameraParameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    .line 431
    const/16 v0, 0x280

    const/16 v1, 0x1e0

    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->configureCameraAndStartPreview(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    :goto_0
    return-void

    .line 432
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setFlashMode(Ljava/lang/String;)V
    .locals 8
    .parameter "flashMode"

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 737
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v4, :cond_0

    if-eqz p1, :cond_0

    const-string v4, "off"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "torch"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 761
    :cond_0
    :goto_0
    return-void

    .line 742
    :cond_1
    iget v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    if-ne v4, v6, :cond_2

    .line 743
    sget-object v4, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "Deferring flash setting until focus complete."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 744
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    goto :goto_0

    .line 748
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFlashSupported:Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFlashSupported:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 749
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashMode:Ljava/lang/String;

    .line 750
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;

    move-result-object v3

    .line 751
    .local v3, params:Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    const-string v4, "flash-mode"

    invoke-interface {v3, v4, p1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    :try_start_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setCameraParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 754
    :catch_0
    move-exception v0

    .line 755
    .local v0, e:Ljava/lang/RuntimeException;
    sget-object v4, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "Unable to set flash mode to: %s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 756
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;

    .line 757
    .local v2, l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    invoke-interface {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;->onCameraFlashControlError()V

    goto :goto_1
.end method

.method public setForcedPreviewSize(Lcom/google/android/apps/unveil/env/Size;)V
    .locals 0
    .parameter "size"

    .prologue
    .line 1686
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->forcedPreviewSize:Lcom/google/android/apps/unveil/env/Size;

    .line 1687
    return-void
.end method

.method public declared-synchronized setFullScreenDisplaySize(Lcom/google/android/apps/unveil/env/Size;)V
    .locals 1
    .parameter "deviceFullScreenDisplaySize"

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->deviceFullScreenDisplaySize:Lcom/google/android/apps/unveil/env/Size;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    monitor-exit p0

    return-void

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setZoomLevel(I)V
    .locals 5
    .parameter "zoomLevel"

    .prologue
    .line 603
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isPreviewing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 604
    :cond_0
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Too early to zoom!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 608
    :cond_2
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->zoomSupported:Z

    if-nez v0, :cond_3

    .line 609
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Zooming not supported!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 603
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 613
    :cond_3
    :try_start_2
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->maxZoomLevel:I

    if-le p1, v0, :cond_4

    .line 614
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Zoom is too great! %d requested, max is %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->maxZoomLevel:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 618
    :cond_4
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentZoomLevel:I

    if-eq p1, v0, :cond_1

    .line 619
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Setting zoom level to %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-interface {v0, p1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->startSmoothZoom(I)V

    .line 621
    iput p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentZoomLevel:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized startPreview()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 849
    monitor-enter p0

    :try_start_0
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Starting preview!"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 851
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCamera()V

    .line 852
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v5, :cond_2

    .line 853
    iget-boolean v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquisitionPending:Z

    if-eqz v5, :cond_1

    .line 854
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Deferring startPreview due to acquisitionPending."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 855
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 910
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 857
    :cond_1
    :try_start_1
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Failed to acquire camera, can\'t start preview"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 849
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 862
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isTakingPicture()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 863
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Deferring startPreview due to picture taking."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 864
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    goto :goto_0

    .line 866
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusing()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 867
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Deferring startPreview due to focusing."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 868
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    goto :goto_0

    .line 872
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    if-eqz v5, :cond_5

    .line 873
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Already previewing."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 874
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    goto :goto_0

    .line 878
    :cond_5
    const/4 v4, 0x0

    .line 879
    .local v4, viewWidth:I
    const/4 v3, 0x0

    .line 880
    .local v3, viewHeight:I
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->deviceFullScreenDisplaySize:Lcom/google/android/apps/unveil/env/Size;

    if-nez v5, :cond_7

    .line 881
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getWidth()I

    move-result v4

    .line 882
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getHeight()I

    move-result v3

    .line 883
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Full display size is null, falling back to CameraManager view size of %dx%d."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 891
    :goto_1
    if-eqz v4, :cond_6

    if-nez v3, :cond_8

    .line 892
    :cond_6
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Too early to preview, no device size or CameraManager View size known yet."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 893
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    goto/16 :goto_0

    .line 886
    :cond_7
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->deviceFullScreenDisplaySize:Lcom/google/android/apps/unveil/env/Size;

    iget v4, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    .line 887
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->deviceFullScreenDisplaySize:Lcom/google/android/apps/unveil/env/Size;

    iget v3, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    .line 888
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Using full display size of %dx%d to pick preview size."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 898
    :cond_8
    :try_start_3
    iget v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->currentOrientation:I

    if-ne v5, v10, :cond_9

    .line 899
    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->configureCameraAndStartPreview(II)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 903
    :catch_0
    move-exception v0

    .line 904
    .local v0, e:Ljava/io/IOException;
    :try_start_4
    sget-object v5, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Unable to acquire/configure camera hardware."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 905
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseCamera()V

    .line 906
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;

    .line 907
    .local v2, l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    invoke-interface {v2}, Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;->onCameraAcquisitionError()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 901
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #l:Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;
    :cond_9
    :try_start_5
    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->configureCameraAndStartPreview(II)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0
.end method

.method public declared-synchronized stopPreview()V
    .locals 3

    .prologue
    .line 913
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v0, :cond_0

    .line 914
    sget-object v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Can\'t stop preview on a null camera."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 924
    :goto_0
    monitor-exit p0

    return-void

    .line 917
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->stopPreview()V

    .line 918
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pendingStartPreview:Z

    .line 919
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    .line 920
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseRequiredAfterFocus:Z

    .line 921
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->snapRequiredAfterFocus:Z

    .line 922
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->flashSettingAfterFocus:Ljava/lang/String;

    .line 923
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 913
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 6
    .parameter "surfaceHolder"
    .parameter "format"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v5, 0x0

    .line 456
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "surfaceChanged: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p3, p4}, Lcom/google/android/apps/unveil/env/Size;->dimensionsAsString(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 458
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCamera()V

    .line 460
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    if-nez v1, :cond_0

    .line 461
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Failed to acquire camera before setting preview display"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 481
    :goto_0
    return-void

    .line 466
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->textureView:Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;

    if-eqz v1, :cond_1

    .line 467
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->textureView:Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;

    invoke-interface {v1, v2}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->setPreviewTexture(Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 471
    :catch_0
    move-exception v0

    .line 472
    .local v0, e:Ljava/io/IOException;
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Failed to set preview display"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 469
    .end local v0           #e:Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 479
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseCamera()V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .parameter "surfaceHolder"

    .prologue
    .line 446
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .parameter "surfaceHolder"

    .prologue
    .line 450
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->releaseCamera()V

    .line 451
    return-void
.end method

.method public declared-synchronized takePicture(Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;)V
    .locals 4
    .parameter "shutterCb"
    .parameter "pictureCb"

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 529
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 571
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 533
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isTakingPicture()Z

    move-result v1

    if-nez v1, :cond_0

    .line 538
    if-eqz p1, :cond_2

    .line 539
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->shutterListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541
    :cond_2
    if-eqz p2, :cond_3

    .line 542
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->pictureListeners:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544
    :cond_3
    iget v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    if-eq v1, v3, :cond_0

    .line 547
    iget v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 548
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->snapRequiredAfterFocus:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 529
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 551
    :cond_5
    const/4 v1, 0x3

    :try_start_2
    iput v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->state:I

    .line 553
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Before taking picture via hardware camera."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->time(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 555
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->previewing:Z

    .line 556
    new-instance v0, Lcom/google/android/apps/unveil/sensors/CameraManager$10;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager$10;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    .line 569
    .local v0, scb:Landroid/hardware/Camera$ShutterCallback;
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2, p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    .line 570
    sget-object v1, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "After taking picture via hardware camera."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->time(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public takePictureFromFrame(Lcom/google/android/apps/unveil/env/Picture;Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;)V
    .locals 2
    .parameter "picture"
    .parameter "cb"

    .prologue
    .line 1594
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->sensorProviderProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v1}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;->getRoundedDeviceOrientation()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->roundOffOrientation(I)I

    move-result v0

    .line 1596
    .local v0, orientation:I
    invoke-virtual {p1, v0}, Lcom/google/android/apps/unveil/env/Picture;->setOrientation(I)V

    .line 1597
    invoke-interface {p2, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;->onPictureTaken(Lcom/google/android/apps/unveil/env/Picture;)V

    .line 1598
    return-void
.end method

.method public takePictureFromFrame(Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;)V
    .locals 4
    .parameter "frame"
    .parameter "cb"

    .prologue
    .line 1604
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->sensorProviderProvider:Lcom/google/android/apps/unveil/env/Provider;

    invoke-interface {v1}, Lcom/google/android/apps/unveil/env/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;->getRoundedDeviceOrientation()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->roundOffOrientation(I)I

    move-result v0

    .line 1606
    .local v0, orientation:I
    invoke-virtual {p1}, Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;->getRawData()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;->getHeight()I

    move-result v3

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/unveil/env/PictureFactory;->createBitmap([BIII)Lcom/google/android/apps/unveil/env/Picture;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/google/android/apps/unveil/sensors/CameraManager;->takePictureFromFrame(Lcom/google/android/apps/unveil/env/Picture;Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;)V

    .line 1608
    return-void
.end method

.method public transformToPictureCoordinates(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 2
    .parameter "rect"

    .prologue
    .line 1166
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getHeight()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->transformToPictureCoordinatesInner(Landroid/graphics/Rect;II)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method transformToPictureCoordinatesInner(Landroid/graphics/Rect;II)Landroid/graphics/Rect;
    .locals 27
    .parameter "rect"
    .parameter "viewWidth"
    .parameter "viewHeight"

    .prologue
    .line 1172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/sensors/CameraManager;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    .line 1173
    const/16 v22, 0x0

    .line 1247
    :goto_0
    return-object v22

    .line 1176
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v16

    .line 1177
    .local v16, previewSize:Lcom/google/android/apps/unveil/env/Size;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPictureSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v4

    .line 1179
    .local v4, cameraSize:Lcom/google/android/apps/unveil/env/Size;
    const/16 v20, 0x0

    .line 1180
    .local v20, unwarpedRect:Landroid/graphics/Rect;
    if-eqz p1, :cond_1

    .line 1184
    new-instance v20, Landroid/graphics/Rect;

    .end local v20           #unwarpedRect:Landroid/graphics/Rect;
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1185
    .restart local v20       #unwarpedRect:Landroid/graphics/Rect;
    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v6, v22, v23

    .line 1186
    .local v6, horizontalScale:F
    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v21, v22, v23

    .line 1187
    .local v21, verticalScale:F
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v6

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1188
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v6

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1189
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v21

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1190
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v21

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1195
    .end local v6           #horizontalScale:F
    .end local v21           #verticalScale:F
    :cond_1
    iget v11, v4, Lcom/google/android/apps/unveil/env/Size;->width:I

    .line 1196
    .local v11, pictureMajorAxis:I
    iget v12, v4, Lcom/google/android/apps/unveil/env/Size;->height:I

    .line 1197
    .local v12, pictureMinorAxis:I
    move-object/from16 v0, v16

    iget v14, v0, Lcom/google/android/apps/unveil/env/Size;->width:I

    .line 1198
    .local v14, previewMajorAxis:I
    move-object/from16 v0, v16

    iget v15, v0, Lcom/google/android/apps/unveil/env/Size;->height:I

    .line 1200
    .local v15, previewMinorAxis:I
    sget-object v22, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v23, "pictureMajorAxis: %d pictureMinorAxis %d"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-virtual/range {v22 .. v24}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1201
    sget-object v22, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v23, "previewMajorAxis: %d previewMinorAxis: %d"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-virtual/range {v22 .. v24}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1203
    int-to-float v0, v11

    move/from16 v22, v0

    int-to-float v0, v14

    move/from16 v23, v0

    div-float v22, v22, v23

    int-to-float v0, v12

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    div-float v23, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->min(FF)F

    move-result v18

    .line 1206
    .local v18, scaleFactor:F
    const/high16 v22, 0x3f80

    cmpl-float v22, v18, v22

    if-lez v22, :cond_3

    .line 1209
    sget-object v22, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v23, "scale up factor: %f"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-virtual/range {v22 .. v24}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1211
    if-nez p1, :cond_2

    .line 1212
    int-to-float v0, v14

    move/from16 v22, v0

    mul-float v22, v22, v18

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 1213
    .local v8, newMajorAxis:I
    int-to-float v0, v15

    move/from16 v22, v0

    mul-float v22, v22, v18

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 1239
    .local v9, newMinorAxis:I
    :goto_1
    sget-object v22, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v23, "newMajorAxis: %d newMinorAxis: %d"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-virtual/range {v22 .. v24}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1241
    sub-int v22, v11, v8

    div-int/lit8 v7, v22, 0x2

    .line 1242
    .local v7, left:I
    sub-int v22, v12, v9

    div-int/lit8 v19, v22, 0x2

    .line 1243
    .local v19, top:I
    add-int v17, v7, v8

    .line 1244
    .local v17, right:I
    add-int v3, v19, v9

    .line 1245
    .local v3, bottom:I
    sget-object v22, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v23, "left: %d top: %d right: %d bottom: %d"

    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-virtual/range {v22 .. v24}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1247
    new-instance v22, Landroid/graphics/Rect;

    move-object/from16 v0, v22

    move/from16 v1, v19

    move/from16 v2, v17

    invoke-direct {v0, v7, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/16 :goto_0

    .line 1216
    .end local v3           #bottom:I
    .end local v7           #left:I
    .end local v8           #newMajorAxis:I
    .end local v9           #newMinorAxis:I
    .end local v17           #right:I
    .end local v19           #top:I
    :cond_2
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v18

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 1217
    .restart local v8       #newMajorAxis:I
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->height()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v18

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v9

    .restart local v9       #newMinorAxis:I
    goto/16 :goto_1

    .line 1222
    .end local v8           #newMajorAxis:I
    .end local v9           #newMinorAxis:I
    :cond_3
    int-to-float v0, v11

    move/from16 v22, v0

    int-to-float v0, v12

    move/from16 v23, v0

    div-float v10, v22, v23

    .line 1225
    .local v10, pictureAspectRatio:F
    if-nez p1, :cond_4

    .line 1226
    int-to-float v0, v14

    move/from16 v22, v0

    int-to-float v0, v15

    move/from16 v23, v0

    div-float v13, v22, v23

    .line 1227
    .local v13, previewAspectRatio:F
    div-float v18, v13, v10

    .line 1233
    .end local v13           #previewAspectRatio:F
    :goto_2
    sget-object v22, Lcom/google/android/apps/unveil/sensors/CameraManager;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v23, "fix aspect ratio factor: %f"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-virtual/range {v22 .. v24}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1235
    move v8, v11

    .line 1236
    .restart local v8       #newMajorAxis:I
    int-to-float v0, v12

    move/from16 v22, v0

    div-float v22, v22, v18

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v9

    .restart local v9       #newMinorAxis:I
    goto/16 :goto_1

    .line 1229
    .end local v8           #newMajorAxis:I
    .end local v9           #newMinorAxis:I
    :cond_4
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->height()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v5, v22, v23

    .line 1230
    .local v5, cropRectAspectRatio:F
    div-float v18, v5, v10

    goto :goto_2
.end method

.method public unregisterListener(Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1293
    return-void
.end method

.method public useTextureView(Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;)V
    .locals 0
    .parameter "view"

    .prologue
    .line 1679
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/CameraManager;->textureView:Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;

    .line 1680
    return-void
.end method
