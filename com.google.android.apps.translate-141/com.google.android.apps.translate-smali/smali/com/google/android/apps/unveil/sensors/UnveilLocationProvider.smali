.class public Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;
.super Ljava/lang/Object;
.source "UnveilLocationProvider.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    }
.end annotation


# static fields
.field private static final MAX_CACHED_LOCATION_AGE_MILLIS:J = 0xea60L

.field private static final TAG:Ljava/lang/String; = "UnveilLocationListener"


# instance fields
.field private final application:Lcom/google/android/apps/unveil/UnveilContext;

.field private final context:Landroid/content/Context;

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private location:Landroid/location/Location;

.field private final locationManager:Landroid/location/LocationManager;

.field private final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/unveil/UnveilContext;Landroid/location/LocationManager;Landroid/content/Context;)V
    .locals 1
    .parameter "application"
    .parameter "locationManager"
    .parameter "context"

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->application:Lcom/google/android/apps/unveil/UnveilContext;

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->locationManager:Landroid/location/LocationManager;

    .line 37
    iput-object p3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->context:Landroid/content/Context;

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->listeners:Ljava/util/Set;

    .line 39
    return-void
.end method

.method private eraseAllCachedLocationsAndDisable()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 169
    iput-object v3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    .line 170
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;

    .line 171
    .local v1, listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    invoke-interface {v1, p0, v3}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;->onLocationChanged(Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;Landroid/location/Location;)V

    goto :goto_0

    .line 173
    .end local v1           #listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->off()V

    .line 174
    return-void
.end method

.method private googleLocationDisabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 43
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/unveil/sensors/GoogleLocationSettingHelper;->isAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/unveil/sensors/GoogleLocationSettingHelper;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->googleLocationDisabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->eraseAllCachedLocationsAndDisable()V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    return-object v0
.end method

.method public getLocationCache(F)Lcom/google/android/apps/unveil/sensors/LocationCache;
    .locals 2
    .parameter "evictionMeters"

    .prologue
    .line 200
    new-instance v0, Lcom/google/android/apps/unveil/sensors/LocationCache;

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->application:Lcom/google/android/apps/unveil/UnveilContext;

    invoke-interface {v1}, Lcom/google/android/apps/unveil/UnveilContext;->getLatLngEncrypter()Lcom/google/android/apps/unveil/network/LatLngEncrypter;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/unveil/sensors/LocationCache;-><init>(Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;Lcom/google/android/apps/unveil/network/LatLngEncrypter;F)V

    return-object v0
.end method

.method public off()V
    .locals 3

    .prologue
    .line 139
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->locationManager:Landroid/location/LocationManager;

    invoke-virtual {v2, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;

    .line 141
    .local v1, listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    invoke-interface {v1, p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;->onLocationDisabled(Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;)V

    goto :goto_0

    .line 143
    .end local v1           #listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    :cond_0
    return-void
.end method

.method public on()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->googleLocationDisabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Google location not available, not requesting location."

    new-array v2, v14, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->eraseAllCachedLocationsAndDisable()V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const/4 v7, 0x0

    .line 62
    .local v7, haveGpsProvider:Z
    const/4 v8, 0x0

    .line 65
    .local v8, haveNetworkProvider:Z
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    const/4 v7, 0x1

    .line 76
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 78
    const/4 v8, 0x1

    .line 86
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->listeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;

    .line 87
    .local v10, listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    invoke-interface {v10, p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;->onLocationEnabled(Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;)V

    goto :goto_3

    .line 69
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v10           #listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "GPS location provider disabled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 71
    :catch_0
    move-exception v6

    .line 72
    .local v6, e:Ljava/lang/IllegalArgumentException;
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "No GPS location provider; are you in the emulator?"

    new-array v2, v14, [Ljava/lang/Object;

    invoke-virtual {v0, v6, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 80
    .end local v6           #e:Ljava/lang/IllegalArgumentException;
    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Wireless network location provider disabled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 82
    :catch_1
    move-exception v6

    .line 83
    .restart local v6       #e:Ljava/lang/IllegalArgumentException;
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "No network location provider; are you in the emulator?"

    new-array v2, v14, [Ljava/lang/Object;

    invoke-virtual {v0, v6, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 92
    .end local v6           #e:Ljava/lang/IllegalArgumentException;
    .restart local v9       #i$:Ljava/util/Iterator;
    :cond_4
    const/4 v12, 0x0

    .line 93
    .local v12, prevGpsLocation:Landroid/location/Location;
    if-eqz v7, :cond_5

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v12

    .line 96
    :cond_5
    const/4 v13, 0x0

    .line 97
    .local v13, prevNetworkLocation:Landroid/location/Location;
    if-eqz v8, :cond_6

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v13

    .line 103
    :cond_6
    if-eqz v12, :cond_a

    if-eqz v13, :cond_a

    .line 105
    invoke-virtual {v12}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {v13}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_9

    .line 106
    iput-object v12, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    .line 117
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->application:Lcom/google/android/apps/unveil/UnveilContext;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/UnveilContext;->getMockLocation()Landroid/location/Location;

    move-result-object v11

    .line 118
    .local v11, mockLocation:Landroid/location/Location;
    if-eqz v11, :cond_8

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    if-nez v0, :cond_c

    .line 120
    iput-object v11, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    .line 126
    :cond_8
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 128
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_d

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    goto/16 :goto_0

    .line 108
    .end local v11           #mockLocation:Landroid/location/Location;
    :cond_9
    iput-object v13, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    goto :goto_4

    .line 111
    :cond_a
    if-eqz v12, :cond_b

    .line 112
    iput-object v12, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    goto :goto_4

    .line 113
    :cond_b
    if-eqz v13, :cond_7

    .line 114
    iput-object v13, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    goto :goto_4

    .line 122
    .restart local v11       #mockLocation:Landroid/location/Location;
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    invoke-virtual {v0, v11}, Landroid/location/Location;->set(Landroid/location/Location;)V

    goto :goto_5

    .line 131
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->listeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;

    .line 132
    .restart local v10       #listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    invoke-interface {v10, p0, v0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;->onLocationChanged(Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;Landroid/location/Location;)V

    goto :goto_6
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 6
    .parameter "changedLocation"

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->googleLocationDisabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 148
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v4, "Got a new location, but location is disabled, turning off."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->eraseAllCachedLocationsAndDisable()V

    .line 166
    :cond_0
    return-void

    .line 152
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    if-nez v3, :cond_3

    .line 153
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    .line 158
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->application:Lcom/google/android/apps/unveil/UnveilContext;

    invoke-interface {v3}, Lcom/google/android/apps/unveil/UnveilContext;->getMockLocation()Landroid/location/Location;

    move-result-object v2

    .line 159
    .local v2, mockLocation:Landroid/location/Location;
    if-eqz v2, :cond_2

    .line 160
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    invoke-virtual {v3, v2}, Landroid/location/Location;->set(Landroid/location/Location;)V

    .line 163
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->listeners:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;

    .line 164
    .local v1, listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    invoke-interface {v1, p0, v3}, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;->onLocationChanged(Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;Landroid/location/Location;)V

    goto :goto_1

    .line 155
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;
    .end local v2           #mockLocation:Landroid/location/Location;
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->location:Landroid/location/Location;

    invoke-virtual {v3, p1}, Landroid/location/Location;->set(Landroid/location/Location;)V

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .parameter "provider"

    .prologue
    .line 185
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .parameter "provider"

    .prologue
    .line 187
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    .prologue
    .line 189
    return-void
.end method

.method public registerListener(Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;)Z
    .locals 1
    .parameter "listener"

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public unregisterListener(Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider$Listener;)Z
    .locals 1
    .parameter "listener"

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/UnveilLocationProvider;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
