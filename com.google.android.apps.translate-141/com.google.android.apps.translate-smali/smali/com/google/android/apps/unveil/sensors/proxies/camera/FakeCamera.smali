.class public abstract Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;
.super Ljava/lang/Object;
.source "FakeCamera.java"

# interfaces
.implements Lcom/google/android/apps/unveil/sensors/proxies/camera/CameraProxy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$5;,
        Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;,
        Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;
    }
.end annotation


# static fields
.field private static final FRAME_DELAY_FOR_FAILURE_MS:I = 0x3e8

.field public static final LOCKSTEP_KEY:Ljava/lang/String; = "lockstep_callbacks"

.field private static final MIN_FRAME_DELAY_MS:I = 0xa

.field public static final SKIP_RENDERING_KEY:Ljava/lang/String; = "skip_rendering"

.field protected static camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;


# instance fields
.field private final bufferQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private callback:Landroid/hardware/Camera$PreviewCallback;

.field private volatile callbackRunning:Z

.field private callbackType:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

.field private cameraSurfaceHolder:Landroid/view/SurfaceHolder;

.field private final extraSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private focusCallback:Landroid/hardware/Camera$AutoFocusCallback;

.field private final handler:Landroid/os/Handler;

.field private lastFrameData:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;

.field private localRgbBuffer:[I

.field private final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

.field private final paint:Landroid/graphics/Paint;

.field private parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

.field private previewActive:Z

.field private previewHeight:I

.field private previewWidth:I

.field private renderRotation:I

.field protected resources:Landroid/content/res/Resources;

.field private final skipRendering:Z

.field private final waitForCallbacks:Z


# direct methods
.method protected constructor <init>(Landroid/os/Handler;Ljava/util/Map;Landroid/content/res/Resources;)V
    .locals 2
    .parameter "handler"
    .parameter
    .parameter "resources"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, extraParams:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    .line 74
    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackRunning:Z

    .line 116
    iput-object p2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->extraSettings:Ljava/util/Map;

    .line 117
    iput-object p3, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->resources:Landroid/content/res/Resources;

    .line 119
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->paint:Landroid/graphics/Paint;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 122
    new-instance v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->bufferQueue:Ljava/util/List;

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callback:Landroid/hardware/Camera$PreviewCallback;

    .line 126
    sget-object v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;->ONESHOT:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackType:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    .line 128
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->handler:Landroid/os/Handler;

    .line 130
    const-string v0, "lockstep_callbacks"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->getExtraValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->waitForCallbacks:Z

    .line 131
    const-string v0, "skip_rendering"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->getExtraValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->skipRendering:Z

    .line 132
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;)Landroid/hardware/Camera$AutoFocusCallback;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->focusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->frameLoop()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;)Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->lastFrameData:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;)Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackRunning:Z

    return p1
.end method

.method private drawFrame(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;)Z
    .locals 11
    .parameter "frame"

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 418
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->cameraSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v1, :cond_0

    .line 441
    :goto_0
    return v2

    .line 423
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->cameraSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 424
    .local v0, canvas:Landroid/graphics/Canvas;
    if-nez v0, :cond_1

    .line 425
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v3, "couldn\'t get canvas!"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 429
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->frameSize:Lcom/google/android/apps/unveil/env/Size;

    new-instance v3, Lcom/google/android/apps/unveil/env/Size;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->getDisplayOrientation()I

    move-result v4

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/unveil/env/ImageUtils;->getTransformationMatrix(Lcom/google/android/apps/unveil/env/Size;Lcom/google/android/apps/unveil/env/Size;I)Landroid/graphics/Matrix;

    move-result-object v10

    .line 433
    .local v10, matrix:Landroid/graphics/Matrix;
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 434
    invoke-virtual {v0, v10}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 435
    invoke-virtual {p1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->getRgbData()[I

    move-result-object v1

    iget-object v3, p1, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->frameSize:Lcom/google/android/apps/unveil/env/Size;

    iget v3, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    iget-object v4, p1, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->frameSize:Lcom/google/android/apps/unveil/env/Size;

    iget v6, v4, Lcom/google/android/apps/unveil/env/Size;->width:I

    iget-object v4, p1, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->frameSize:Lcom/google/android/apps/unveil/env/Size;

    iget v7, v4, Lcom/google/android/apps/unveil/env/Size;->height:I

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 437
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 439
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->cameraSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    move v2, v8

    .line 441
    goto :goto_0
.end method

.method private frameLoop()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 358
    const/16 v7, 0x3e8

    iget-object v8, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    const-string v9, "preview-frame-rate"

    invoke-virtual {v8, v9}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;->getInt(Ljava/lang/String;)I

    move-result v8

    div-int/2addr v7, v8

    int-to-long v5, v7

    .line 360
    .local v5, timePerFrame:J
    new-instance v3, Lcom/google/android/apps/unveil/env/Stopwatch;

    invoke-direct {v3}, Lcom/google/android/apps/unveil/env/Stopwatch;-><init>()V

    .line 361
    .local v3, frameTimer:Lcom/google/android/apps/unveil/env/Stopwatch;
    invoke-virtual {v3}, Lcom/google/android/apps/unveil/env/Stopwatch;->start()V

    .line 363
    :goto_0
    iget-boolean v7, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewActive:Z

    if-eqz v7, :cond_1

    .line 364
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->maybeWaitForCallbacks()V

    .line 366
    invoke-virtual {v3}, Lcom/google/android/apps/unveil/env/Stopwatch;->reset()V

    .line 367
    invoke-direct {p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->renderFrameAndCallBack()Z

    move-result v4

    .line 370
    .local v4, renderSuccessful:Z
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/apps/unveil/env/Stopwatch;->getElapsedMilliseconds()J

    move-result-wide v1

    .line 371
    .local v1, frameTime:J
    if-nez v4, :cond_0

    .line 372
    iget-object v7, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v8, "renderFrameAndCallBack() was unsuccessful. Sleeping for a while."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    const-wide/16 v7, 0x3e8

    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 377
    .end local v1           #frameTime:J
    :catch_0
    move-exception v0

    .line 378
    .local v0, e:Ljava/lang/InterruptedException;
    iget-object v7, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v8, "Problem sleeping."

    new-array v9, v11, [Ljava/lang/Object;

    invoke-virtual {v7, v0, v8, v9}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 375
    .end local v0           #e:Ljava/lang/InterruptedException;
    .restart local v1       #frameTime:J
    :cond_0
    const-wide/16 v7, 0xa

    sub-long v9, v5, v1

    :try_start_1
    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 381
    .end local v1           #frameTime:J
    .end local v4           #renderSuccessful:Z
    :cond_1
    return-void
.end method

.method private handleCallback(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;)V
    .locals 8
    .parameter "frame"

    .prologue
    const/4 v7, 0x0

    .line 472
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callback:Landroid/hardware/Camera$PreviewCallback;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackRunning:Z

    if-eqz v4, :cond_1

    .line 527
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callback:Landroid/hardware/Camera$PreviewCallback;

    .line 477
    .local v3, tmpCallback:Landroid/hardware/Camera$PreviewCallback;
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackRunning:Z

    .line 479
    const/4 v2, 0x0

    .line 482
    .local v2, frameBytesYUV:[B
    sget-object v4, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$5;->$SwitchMap$com$google$android$apps$unveil$sensors$proxies$camera$FakeCamera$CallbackType:[I

    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackType:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    invoke-virtual {v5}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 506
    :goto_1
    #getter for: Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->yuvData:[B
    invoke-static {p1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->access$400(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;)[B

    move-result-object v4

    if-nez v4, :cond_3

    .line 508
    #getter for: Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->rgbData:[I
    invoke-static {p1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->access$300(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;)[I

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewWidth:I

    iget v6, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewHeight:I

    invoke-static {v4, v2, v5, v6}, Lcom/google/android/apps/unveil/env/ImageUtils;->convertARGB8888ToYUV420SP([I[BII)V

    .line 516
    :goto_2
    move-object v1, v2

    .line 517
    .local v1, dataBuffer:[B
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->handler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$4;

    invoke-direct {v5, p0, v3, v1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$4;-><init>(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;Landroid/hardware/Camera$PreviewCallback;[B)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 484
    .end local v1           #dataBuffer:[B
    :pswitch_0
    iget-object v5, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->bufferQueue:Ljava/util/List;

    monitor-enter v5

    .line 485
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->bufferQueue:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 486
    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->bufferQueue:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, [B

    move-object v2, v0

    .line 492
    monitor-exit v5

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 489
    :cond_2
    const/4 v4, 0x0

    :try_start_1
    iput-boolean v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackRunning:Z

    .line 490
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 496
    :pswitch_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callback:Landroid/hardware/Camera$PreviewCallback;

    .line 502
    :pswitch_2
    iget v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewWidth:I

    iget v5, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewHeight:I

    invoke-static {v4, v5}, Lcom/google/android/apps/unveil/env/ImageUtils;->getYUVByteSize(II)I

    move-result v4

    new-array v2, v4, [B

    goto :goto_1

    .line 512
    :cond_3
    #getter for: Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->yuvData:[B
    invoke-static {p1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;->access$400(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;)[B

    move-result-object v4

    array-length v5, v2

    invoke-static {v4, v7, v2, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    .line 482
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private maybeWaitForCallbacks()V
    .locals 4

    .prologue
    .line 445
    iget-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->waitForCallbacks:Z

    if-nez v1, :cond_0

    .line 464
    :goto_0
    return-void

    .line 449
    :cond_0
    monitor-enter p0

    .line 451
    :goto_1
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewActive:Z

    if-eqz v1, :cond_3

    .line 453
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callback:Landroid/hardware/Camera$PreviewCallback;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackRunning:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackType:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    sget-object v2, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;->WITH_BUFFER:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->bufferQueue:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 455
    :cond_1
    monitor-exit p0

    goto :goto_0

    .line 463
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 458
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 459
    :catch_0
    move-exception v0

    .line 460
    .local v0, e:Ljava/lang/Exception;
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Exception!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 463
    .end local v0           #e:Ljava/lang/Exception;
    :cond_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized renderFrameAndCallBack()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 384
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 409
    :goto_0
    monitor-exit p0

    return v1

    .line 389
    :cond_0
    :try_start_1
    iget v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewWidth:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewHeight:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_2

    .line 391
    :cond_1
    const-wide/16 v2, 0x3e8

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 392
    :catch_0
    move-exception v0

    .line 393
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 384
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 398
    :cond_2
    :try_start_4
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->localRgbBuffer:[I

    if-nez v2, :cond_3

    .line 399
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->localRgbBuffer:[I

    .line 403
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->generateFrame()Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->lastFrameData:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;

    .line 405
    iget-boolean v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->skipRendering:Z

    if-eqz v2, :cond_4

    const/4 v1, 0x1

    .line 407
    .local v1, renderSucessful:Z
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->lastFrameData:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;

    invoke-direct {p0, v2}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->handleCallback(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;)V

    goto :goto_0

    .line 405
    .end local v1           #renderSucessful:Z
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->lastFrameData:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;

    invoke-direct {p0, v2}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->drawFrame(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized addCallbackBuffer([B)V
    .locals 2
    .parameter "buffer"

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->bufferQueue:Ljava/util/List;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 137
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->bufferQueue:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 140
    monitor-exit p0

    return-void

    .line 138
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 136
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    .locals 1
    .parameter "autoFocusCallback"

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->focusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    .line 147
    new-instance v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$1;-><init>(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;)V

    .line 158
    .local v0, thread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 159
    return-void
.end method

.method public cancelAutoFocus()V
    .locals 0

    .prologue
    .line 540
    return-void
.end method

.method protected abstract generateFrame()Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$RawFrame;
.end method

.method protected getDisplayOrientation()I
    .locals 1

    .prologue
    .line 548
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->renderRotation:I

    return v0
.end method

.method protected getExtraValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "key"
    .parameter "defaulValue"

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->extraSettings:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->extraSettings:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, p2

    .line 555
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->extraSettings:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method protected getHeight()I
    .locals 1

    .prologue
    .line 534
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewHeight:I

    return v0
.end method

.method public getParameters()Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;
    .locals 2

    .prologue
    .line 163
    new-instance v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    invoke-direct {v0, v1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;-><init>(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;)V

    return-object v0
.end method

.method protected getWidth()I
    .locals 1

    .prologue
    .line 530
    iget v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewWidth:I

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewActive:Z

    .line 169
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->camera:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;

    .line 170
    return-void
.end method

.method public setDisplayOrientation(I)V
    .locals 0
    .parameter "degrees"

    .prologue
    .line 544
    iput p1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->renderRotation:I

    .line 545
    return-void
.end method

.method public declared-synchronized setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 1
    .parameter "previewCallback"

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;->ONESHOT:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackType:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    .line 175
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callback:Landroid/hardware/Camera$PreviewCallback;

    .line 176
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    monitor-exit p0

    return-void

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setParameters(Lcom/google/android/apps/unveil/sensors/proxies/camera/ParametersProxy;)V
    .locals 5
    .parameter "params"

    .prologue
    .line 181
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    check-cast p1, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    .end local p1
    invoke-direct {v0, p1}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;-><init>(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;)V

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/unveil/env/Size;->height:I

    iput v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewHeight:I

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/unveil/env/Size;->width:I

    iput v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewWidth:I

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Setting picture size to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->parameters:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;

    invoke-virtual {v4}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeParameters;->getPictureSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->v(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    monitor-exit p0

    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 1
    .parameter "previewCallback"

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;->REPEATING:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackType:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    .line 191
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callback:Landroid/hardware/Camera$PreviewCallback;

    .line 192
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    monitor-exit p0

    return-void

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 1
    .parameter "previewCallback"

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callback:Landroid/hardware/Camera$PreviewCallback;

    .line 210
    if-eqz p1, :cond_0

    .line 211
    sget-object v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;->WITH_BUFFER:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackType:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    .line 215
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    monitor-exit p0

    return-void

    .line 213
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;->ONESHOT:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;

    iput-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->callbackType:Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$CallbackType;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .parameter "cameraSurfaceHolder"

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->cameraSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 198
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 199
    return-void
.end method

.method public setPreviewTexture(Lcom/google/android/apps/unveil/env/PlatformVersionUtils$TextureView;)V
    .locals 2
    .parameter "cameraSurfaceTexture"

    .prologue
    .line 203
    new-instance v0, Ljava/lang/NoSuchMethodError;

    const-string v1, "FakeCamera doesn\'t support setPreviewTexture"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchMethodError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V
    .locals 3
    .parameter "listener"

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "FakeCamera.setZoomChangeListener(): zooming not supported!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 566
    return-void
.end method

.method public startPreview()V
    .locals 4

    .prologue
    .line 220
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewActive:Z

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Starting preview loop."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    new-instance v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$2;-><init>(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;)V

    .line 226
    .local v0, thread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 227
    return-void
.end method

.method public startSmoothZoom(I)V
    .locals 3
    .parameter "value"

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "FakeCamera.startSmoothZoom(): zooming not supported!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 561
    return-void
.end method

.method public declared-synchronized stopPreview()V
    .locals 1

    .prologue
    .line 231
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;->previewActive:Z

    .line 232
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    monitor-exit p0

    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    .locals 1
    .parameter "shutterCallback"
    .parameter "object"
    .parameter "callback"

    .prologue
    .line 241
    new-instance v0, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$3;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera$3;-><init>(Lcom/google/android/apps/unveil/sensors/proxies/camera/FakeCamera;Landroid/hardware/Camera$PictureCallback;)V

    .line 283
    .local v0, thread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 284
    return-void
.end method
