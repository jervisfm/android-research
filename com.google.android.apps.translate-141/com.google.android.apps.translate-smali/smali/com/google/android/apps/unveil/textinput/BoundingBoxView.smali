.class public Lcom/google/android/apps/unveil/textinput/BoundingBoxView;
.super Landroid/view/View;
.source "BoundingBoxView.java"


# static fields
.field private static final HIGHLIGHT_RADIUS_DIP:F = 5.0f

.field private static final SELECTED_COLOR:I

.field private static final UNSELECTED_COLOR:I

.field private static logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# instance fields
.field private final highlightRadius:F

.field private final paint:Landroid/graphics/Paint;

.field private queryPictureSize:Lcom/google/android/apps/unveil/env/Size;

.field private textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

.field private wordRect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xff

    .line 29
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    sput-object v0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    .line 31
    const/16 v0, 0x4b

    const/4 v1, 0x0

    const/16 v2, 0xa0

    const/16 v3, 0xd2

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->SELECTED_COLOR:I

    .line 32
    const/16 v0, 0x28

    invoke-static {v0, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->UNSELECTED_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->wordRect:Landroid/graphics/RectF;

    .line 48
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->paint:Landroid/graphics/Paint;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    const/high16 v0, 0x40a0

    invoke-static {v0, p1}, Lcom/google/android/apps/unveil/env/PixelUtils;->dipToPix(FLandroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->highlightRadius:F

    .line 52
    return-void
.end method

.method private drawWords(Landroid/graphics/Canvas;Ljava/util/List;Z)V
    .locals 9
    .parameter "canvas"
    .parameter
    .parameter "selected"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p2, words:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;>;"
    iget-object v4, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->paint:Landroid/graphics/Paint;

    if-eqz p3, :cond_0

    sget v3, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->SELECTED_COLOR:I

    :goto_0
    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 85
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;

    .line 86
    .local v2, word:Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;
    invoke-virtual {v2}, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->getBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    .line 87
    .local v0, box:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->wordRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getX()I

    move-result v6

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getY()I

    move-result v7

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    int-to-float v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 89
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->wordRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->highlightRadius:F

    iget v5, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->highlightRadius:F

    iget-object v6, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 84
    .end local v0           #box:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #word:Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;
    :cond_0
    sget v3, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->UNSELECTED_COLOR:I

    goto :goto_0

    .line 91
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .parameter "canvas"

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 80
    :goto_0
    monitor-exit p0

    return-void

    .line 74
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->queryPictureSize:Lcom/google/android/apps/unveil/env/Size;

    iget v1, v1, Lcom/google/android/apps/unveil/env/Size;->width:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->queryPictureSize:Lcom/google/android/apps/unveil/env/Size;

    iget v2, v2, Lcom/google/android/apps/unveil/env/Size;->height:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextMasker;->getSelectedWords()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->drawWords(Landroid/graphics/Canvas;Ljava/util/List;Z)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextMasker;->getUnselectedWords()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->drawWords(Landroid/graphics/Canvas;Ljava/util/List;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setQueryPictureSize(Lcom/google/android/apps/unveil/env/Size;)V
    .locals 0
    .parameter "queryPictureSize"

    .prologue
    .line 55
    invoke-static {p1}, Lcom/google/android/apps/unveil/env/Check;->checkNotNull(Ljava/lang/Object;)V

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->queryPictureSize:Lcom/google/android/apps/unveil/env/Size;

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->invalidate()V

    .line 59
    return-void
.end method

.method public setTextMasker(Lcom/google/android/apps/unveil/textinput/TextMasker;)V
    .locals 0
    .parameter "textMasker"

    .prologue
    .line 62
    invoke-static {p1}, Lcom/google/android/apps/unveil/env/Check;->checkNotNull(Ljava/lang/Object;)V

    .line 64
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->invalidate()V

    .line 66
    return-void
.end method
