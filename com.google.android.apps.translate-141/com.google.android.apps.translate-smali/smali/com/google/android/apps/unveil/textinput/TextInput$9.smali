.class Lcom/google/android/apps/unveil/textinput/TextInput$9;
.super Ljava/lang/Object;
.source "TextInput.java"

# interfaces
.implements Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/unveil/textinput/TextInput;->snap()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/unveil/textinput/TextInput;


# direct methods
.method constructor <init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V
    .locals 0
    .parameter

    .prologue
    .line 658
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput$9;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocus(Z)V
    .locals 5
    .parameter "success"

    .prologue
    .line 661
    invoke-static {}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$800()Lcom/google/android/apps/unveil/env/UnveilLogger;

    move-result-object v1

    const-string v2, "Camera focus completed. %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    const-string v0, "Succeed"

    :goto_0
    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput$9;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #calls: Lcom/google/android/apps/unveil/textinput/TextInput;->showRecentFrame()V
    invoke-static {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$900(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput$9;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;
    invoke-static {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1000(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseLoop()V

    .line 664
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput$9;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;
    invoke-static {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$600(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->onPreviewFrozen()V

    .line 665
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput$9;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput$9;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;
    invoke-static {v1}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1100(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    move-result-object v1

    #calls: Lcom/google/android/apps/unveil/textinput/TextInput;->asyncCaptureAndSendFrame(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1200(Lcom/google/android/apps/unveil/textinput/TextInput;Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V

    .line 666
    return-void

    .line 661
    :cond_0
    const-string v0, "Failed"

    goto :goto_0
.end method
