.class Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;
.super Lcom/google/android/apps/unveil/nonstop/ImageBlurProcessor;
.source "TextInput.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/unveil/textinput/TextInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoFocusProcessor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/unveil/textinput/TextInput;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/unveil/textinput/TextInput;Lcom/google/android/apps/unveil/sensors/CameraManager;)V
    .locals 0
    .parameter
    .parameter "cameraManager"

    .prologue
    .line 1116
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    .line 1117
    invoke-direct {p0, p2}, Lcom/google/android/apps/unveil/nonstop/ImageBlurProcessor;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    .line 1118
    return-void
.end method


# virtual methods
.method protected onProcessFrame(Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;)V
    .locals 1
    .parameter "frame"

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;
    invoke-static {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1400(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1155
    :cond_0
    :goto_0
    return-void

    .line 1146
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;
    invoke-static {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1400(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1150
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocus:Z
    invoke-static {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1500(Lcom/google/android/apps/unveil/textinput/TextInput;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1154
    invoke-super {p0, p1}, Lcom/google/android/apps/unveil/nonstop/ImageBlurProcessor;->onProcessFrame(Lcom/google/android/apps/unveil/nonstop/TimestampedFrame;)V

    goto :goto_0
.end method

.method public shouldFocus()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1121
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;
    invoke-static {v1}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1400(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1137
    :cond_0
    :goto_0
    return v0

    .line 1124
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;
    invoke-static {v1}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1400(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1128
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;->this$0:Lcom/google/android/apps/unveil/textinput/TextInput;

    #getter for: Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocus:Z
    invoke-static {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->access$1500(Lcom/google/android/apps/unveil/textinput/TextInput;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1130
    const/4 v0, 0x1

    goto :goto_0

    .line 1137
    :cond_2
    invoke-super {p0}, Lcom/google/android/apps/unveil/nonstop/ImageBlurProcessor;->isLastFrameBlurred()Z

    move-result v0

    goto :goto_0
.end method
