.class public Lcom/google/android/apps/unveil/textinput/TextInput;
.super Landroid/widget/FrameLayout;
.source "TextInput.java"

# interfaces
.implements Lcom/google/android/apps/unveil/textinput/SmudgeView$Listener;
.implements Lcom/google/android/apps/unveil/textinput/TextQueryListener$Listener;
.implements Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;,
        Lcom/google/android/apps/unveil/textinput/TextInput$RecentFrame;,
        Lcom/google/android/apps/unveil/textinput/TextInput$Listener;,
        Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;
    }
.end annotation


# static fields
.field private static final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field private autoFocus:Z

.field private autoFocusProcessor:Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;

.field private blockingCaptureLatch:Ljava/util/concurrent/CountDownLatch;

.field private boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

.field private cameraLayoutComplete:Z

.field private cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

.field private capturedToCroppedMatrix:Landroid/graphics/Matrix;

.field private clientType:Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;

.field private connector:Lcom/google/android/apps/unveil/network/AbstractConnector;

.field private debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

.field private gridView:Landroid/view/View;

.field private imageLogging:Z

.field private imageToSend:Lcom/google/android/apps/unveil/env/Picture;

.field private volatile isSnapping:Z

.field private listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

.field private previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

.field private queryId:J

.field private queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

.field private queryManager:Lcom/google/android/apps/unveil/protocol/QueryManager;

.field private recentFrame:Lcom/google/android/apps/unveil/textinput/TextInput$RecentFrame;

.field private smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

.field private sourceLanguage:Ljava/lang/String;

.field private final supportedLanguages:[Ljava/lang/String;

.field private textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

.field private textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

.field private traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

.field private uiHandler:Landroid/os/Handler;

.field private userAgent:Ljava/lang/String;

.field private zoomableContainer:Lcom/google/android/apps/unveil/textinput/ZoomableContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    sput-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v4, 0x0

    .line 180
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    sget-object v1, Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;->FELIX_TEXT:Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->clientType:Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;

    .line 146
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocus:Z

    .line 147
    iput-boolean v4, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageLogging:Z

    .line 154
    iput-boolean v4, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraLayoutComplete:Z

    .line 162
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->blockingCaptureLatch:Ljava/util/concurrent/CountDownLatch;

    .line 181
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/unveil/textinput/R$array;->text_input_languages:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->supportedLanguages:[Ljava/lang/String;

    .line 182
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/unveil/textinput/R$string;->text_input_default_user_agent:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->userAgent:Ljava/lang/String;

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/unveil/textinput/R$string;->text_input_frontend_url:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, frontendUrl:Ljava/lang/String;
    sget-object v1, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Frontend is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->setConnector(Landroid/content/Context;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/unveil/textinput/TextInput;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->userAgent:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/nonstop/PreviewLooper;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/textinput/TextQueryListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/unveil/textinput/TextInput;Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->asyncCaptureAndSendFrame(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/textinput/ZoomableContainer;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->zoomableContainer:Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/sensors/CameraManager;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/unveil/textinput/TextInput;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocus:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/unveil/textinput/TextInput;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->startCameraPreviewAndLooper()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/textinput/SmudgeView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/unveil/textinput/TextInput;Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->blockingCaptureAndSendFrame(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/protocol/TraceTracker;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/unveil/textinput/TextInput;)Lcom/google/android/apps/unveil/textinput/TextInput$Listener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/unveil/textinput/TextInput;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->blockingCaptureLatch:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic access$800()Lcom/google/android/apps/unveil/env/UnveilLogger;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/unveil/textinput/TextInput;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->showRecentFrame()V

    return-void
.end method

.method private asyncCaptureAndSendFrame(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V
    .locals 3
    .parameter "queryListener"

    .prologue
    .line 439
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->blockingCaptureLatch:Ljava/util/concurrent/CountDownLatch;

    .line 440
    new-instance v0, Lcom/google/android/apps/unveil/textinput/TextInput$7;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput$7;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getExecutor()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->uiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/textinput/TextInput$7;->execute(Ljava/util/concurrent/Executor;Landroid/os/Handler;)V

    .line 450
    return-void
.end method

.method private blockingCaptureAndSendFrame(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V
    .locals 10
    .parameter "queryListener"

    .prologue
    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 472
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 473
    .local v1, capturedPicture:Ljava/util/concurrent/atomic/AtomicReference;,"Ljava/util/concurrent/atomic/AtomicReference<Lcom/google/android/apps/unveil/env/Picture;>;"
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v5, v6, v6}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 474
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    new-instance v6, Lcom/google/android/apps/unveil/textinput/TextInput$8;

    invoke-direct {v6, p0, v1}, Lcom/google/android/apps/unveil/textinput/TextInput$8;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-virtual {v5, v9, v6}, Lcom/google/android/apps/unveil/sensors/CameraManager;->takePicture(Lcom/google/android/apps/unveil/sensors/CameraManager$ShutterCallback;Lcom/google/android/apps/unveil/sensors/CameraManager$PictureCallback;)V

    .line 486
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->blockingCaptureLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 491
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->allocateBitmapAndMatriciesForCapture()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 492
    .local v2, croppedBitmap:Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    .line 524
    :cond_0
    :goto_1
    return-void

    .line 487
    .end local v2           #croppedBitmap:Landroid/graphics/Bitmap;
    :catch_0
    move-exception v3

    .line 488
    .local v3, e:Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 500
    .end local v3           #e:Ljava/lang/InterruptedException;
    .restart local v2       #croppedBitmap:Landroid/graphics/Bitmap;
    :cond_1
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 501
    .local v0, canvas:Landroid/graphics/Canvas;
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/unveil/env/Picture;

    invoke-virtual {v5}, Lcom/google/android/apps/unveil/env/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->capturedToCroppedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v5, v6, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 503
    const/16 v5, 0x5a

    invoke-static {v2, v5}, Lcom/google/android/apps/unveil/env/PictureFactory;->createBitmap(Landroid/graphics/Bitmap;I)Lcom/google/android/apps/unveil/env/Picture;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    .line 504
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    sget-object v6, Lcom/google/goggles/TracingProtos$SpanVariable$Type;->IMAGE_REENCODE:Lcom/google/goggles/TracingProtos$SpanVariable$Type;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->endInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V

    .line 505
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    sget-object v6, Lcom/google/goggles/TracingProtos$SpanVariable$Type;->ACQUIRE_TO_IMAGE:Lcom/google/goggles/TracingProtos$SpanVariable$Type;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->endInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V

    .line 507
    move-object v4, p1

    .line 508
    .local v4, listener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    invoke-virtual {v5}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/unveil/textinput/TextQueryListener;->setPictureSize(Lcom/google/android/apps/unveil/env/Size;)V

    .line 510
    new-instance v5, Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    invoke-direct {v5}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->addMsSinceEpoch(Ljava/lang/Long;)Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    invoke-virtual {v6}, Lcom/google/android/apps/unveil/env/Picture;->getJpegData()[B

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    invoke-virtual {v7}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->addImageData([BLcom/google/android/apps/unveil/env/Size;)Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->sourceLanguage:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->addSourceLanguage(Ljava/lang/String;)Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageLogging:Z

    invoke-virtual {v5, v6}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->setCanLogImage(Z)Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    .line 516
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    sget-object v6, Lcom/google/android/apps/unveil/protocol/QueryResponseFactory$QueryType;->FELIX_TEXT_IMAGE:Lcom/google/android/apps/unveil/protocol/QueryResponseFactory$QueryType;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->setQueryType(Lcom/google/android/apps/unveil/protocol/QueryResponseFactory$QueryType;)V

    .line 517
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    iget-object v6, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->clientType:Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;

    iget-object v6, v6, Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;->source:Lcom/google/goggles/GogglesProtos$GogglesRequest$Source;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->setSource(Lcom/google/goggles/GogglesProtos$GogglesRequest$Source;)V

    .line 518
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryManager:Lcom/google/android/apps/unveil/protocol/QueryManager;

    iget-object v6, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    iget-object v7, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    invoke-virtual {v7}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->buildGogglesRequestBuilder()Lcom/google/goggles/GogglesProtos$GogglesRequest$Builder;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v4, v8}, Lcom/google/android/apps/unveil/protocol/QueryManager;->sendQuery(Lcom/google/android/apps/unveil/protocol/QueryBuilder;Lcom/google/goggles/GogglesProtos$GogglesRequest$Builder;Lcom/google/android/apps/unveil/protocol/QueryListener;Z)I

    .line 519
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    iget-object v6, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    invoke-virtual {v6}, Lcom/google/android/apps/unveil/env/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v5, v6, v9}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setImageToDisplay(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 523
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v5}, Lcom/google/android/apps/unveil/sensors/CameraManager;->forceReleaseCamera()V

    goto/16 :goto_1
.end method

.method private calculatePreviewTranslation()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 430
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 431
    .local v0, location:[I
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getLocationInWindow([I)V

    .line 432
    aget v1, v0, v4

    .line 433
    .local v1, topOfCamera:I
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->getLocationInWindow([I)V

    .line 434
    aget v2, v0, v4

    .line 435
    .local v2, topOfVisibleArea:I
    sub-int v3, v2, v1

    return v3
.end method

.method public static findViewByType(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;
    .locals 4
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 263
    .local p1, clazz:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    invoke-static {p0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByTypeInner(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v0

    .line 264
    .local v0, ret:Landroid/view/View;
    if-nez v0, :cond_0

    .line 265
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can not find expected view of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Did you forget to define it in the xml layout?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :cond_0
    return-object v0
.end method

.method private static findViewByTypeInner(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;
    .locals 6
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .local p1, clazz:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v5, 0x0

    .line 272
    instance-of v4, p0, Landroid/view/ViewGroup;

    if-nez v4, :cond_1

    .line 273
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 291
    .end local p0
    :goto_0
    return-object p0

    .restart local p0
    :cond_0
    move-object p0, v5

    .line 276
    goto :goto_0

    :cond_1
    move-object v4, p0

    .line 278
    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 279
    .local v2, n:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    if-ge v1, v2, :cond_4

    move-object v4, p0

    .line 280
    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 281
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object p0, v0

    .line 282
    goto :goto_0

    .line 284
    :cond_2
    instance-of v4, v0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_3

    .line 285
    invoke-static {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByTypeInner(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v3

    .line 286
    .local v3, ret:Landroid/view/View;
    if-eqz v3, :cond_3

    move-object p0, v3

    .line 287
    goto :goto_0

    .line 279
    .end local v3           #ret:Landroid/view/View;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0           #child:Landroid/view/View;
    :cond_4
    move-object p0, v5

    .line 291
    goto :goto_0
.end method

.method private declared-synchronized load(Lcom/google/android/apps/unveil/env/Picture;)V
    .locals 7
    .parameter "picture"

    .prologue
    .line 708
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->gridView:Landroid/view/View;

    const/high16 v1, 0x3f80

    const/4 v2, 0x0

    const-wide/16 v3, 0x258

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/unveil/textinput/AnimationHelper;->alpha(Landroid/view/View;FFJLandroid/view/animation/Interpolator;Ljava/lang/Runnable;)V

    .line 712
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->clearSmudges()V

    .line 713
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {p1}, Lcom/google/android/apps/unveil/env/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setImageToDisplay(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 715
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    const/high16 v1, 0x3f80

    const/high16 v2, 0x3f80

    const-wide/16 v3, 0x258

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/unveil/textinput/AnimationHelper;->alpha(Landroid/view/View;FFJLandroid/view/animation/Interpolator;Ljava/lang/Runnable;)V

    .line 718
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryId:J

    .line 719
    new-instance v0, Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    iget-wide v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryId:J

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/apps/unveil/textinput/TextQueryListener$Listener;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/unveil/textinput/TextQueryListener;-><init>(Lcom/google/android/apps/unveil/textinput/BoundingBoxView;Lcom/google/android/apps/unveil/textinput/TextMasker;J[Lcom/google/android/apps/unveil/textinput/TextQueryListener$Listener;)V

    .line 721
    .local v0, listener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/TextQueryListener;->setPictureSize(Lcom/google/android/apps/unveil/env/Size;)V

    .line 722
    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->setQueryListener(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V

    .line 724
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseLoop()V

    .line 725
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->forceReleaseCamera()V

    .line 727
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setAcceptSmudges(Z)V

    .line 728
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/textinput/TextMasker;->clearWords()V

    .line 729
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    sget-object v2, Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;->NONE:Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/textinput/TextMasker;->setSmudge(Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;)V

    .line 730
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->setVisibility(I)V

    .line 731
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->zoomableContainer:Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/textinput/ZoomableContainer;->reset()V

    .line 733
    new-instance v1, Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    invoke-direct {v1}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->addMsSinceEpoch(Ljava/lang/Long;)Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    invoke-virtual {v2}, Lcom/google/android/apps/unveil/env/Picture;->getJpegData()[B

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->addImageData([BLcom/google/android/apps/unveil/env/Size;)Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->sourceLanguage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->addSourceLanguage(Ljava/lang/String;)Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageLogging:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->setCanLogImage(Z)Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    .line 739
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    sget-object v2, Lcom/google/android/apps/unveil/protocol/QueryResponseFactory$QueryType;->FELIX_TEXT_IMAGE:Lcom/google/android/apps/unveil/protocol/QueryResponseFactory$QueryType;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->setQueryType(Lcom/google/android/apps/unveil/protocol/QueryResponseFactory$QueryType;)V

    .line 740
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->clientType:Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;

    iget-object v2, v2, Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;->source:Lcom/google/goggles/GogglesProtos$GogglesRequest$Source;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->setSource(Lcom/google/goggles/GogglesProtos$GogglesRequest$Source;)V

    .line 741
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryManager:Lcom/google/android/apps/unveil/protocol/QueryManager;

    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/protocol/QueryBuilder;->buildGogglesRequestBuilder()Lcom/google/goggles/GogglesProtos$GogglesRequest$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/google/android/apps/unveil/protocol/QueryManager;->sendQuery(Lcom/google/android/apps/unveil/protocol/QueryBuilder;Lcom/google/goggles/GogglesProtos$GogglesRequest$Builder;Lcom/google/android/apps/unveil/protocol/QueryListener;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742
    monitor-exit p0

    return-void

    .line 708
    .end local v0           #listener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private maybeSendTraces()V
    .locals 6

    .prologue
    .line 998
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->hasPendingActions()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1020
    :goto_0
    return-void

    .line 1001
    :cond_0
    invoke-static {}, Lcom/google/goggles/TracingProtos$TraceRequest;->newBuilder()Lcom/google/goggles/TracingProtos$TraceRequest$Builder;

    move-result-object v0

    .line 1002
    .local v0, traceRequestBuilder:Lcom/google/goggles/TracingProtos$TraceRequest$Builder;
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->populateRequest(Lcom/google/goggles/TracingProtos$TraceRequest$Builder;)V

    .line 1004
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->connector:Lcom/google/android/apps/unveil/network/AbstractConnector;

    invoke-virtual {v0}, Lcom/google/goggles/TracingProtos$TraceRequest$Builder;->build()Lcom/google/goggles/TracingProtos$TraceRequest;

    move-result-object v2

    const-class v3, Lcom/google/goggles/TracingProtos$TraceResponse;

    new-instance v4, Lcom/google/android/apps/unveil/textinput/TextInput$12;

    invoke-direct {v4, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$12;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    const-string v5, ""

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/unveil/network/AbstractConnector;->sendRequest(Lcom/google/protobuf/GeneratedMessageLite;Ljava/lang/Class;Lcom/google/android/apps/unveil/network/AbstractConnector$ResponseHandler;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private maybeTriggerFocus(Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;)V
    .locals 3
    .parameter "callback"

    .prologue
    const/4 v2, 0x0

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocusProcessor:Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocusProcessor:Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;->shouldFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Trigger focus before taking picture."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->focus(Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;)V

    .line 465
    :goto_0
    return-void

    .line 462
    :cond_0
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Already in focus, no focus necessary."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 463
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;->onFocus(Z)V

    goto :goto_0
.end method

.method private newQueryManager()Lcom/google/android/apps/unveil/protocol/QueryManager;
    .locals 7

    .prologue
    .line 984
    new-instance v0, Lcom/google/android/apps/unveil/protocol/QueryManager;

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    new-instance v2, Lcom/google/android/apps/unveil/protocol/QueryManagerParams;

    new-instance v3, Lcom/google/android/apps/unveil/textinput/TextInput$11;

    invoke-direct {v3, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$11;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/unveil/protocol/QueryManagerParams;-><init>(Lcom/google/android/apps/unveil/env/Provider;)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->connector:Lcom/google/android/apps/unveil/network/AbstractConnector;

    new-instance v5, Lcom/google/android/apps/unveil/env/Viewport;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/unveil/env/Viewport;->computeNaturalOrientation(Landroid/content/Context;)I

    move-result v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/unveil/env/Viewport;-><init>(I)V

    invoke-static {v5}, Lcom/google/android/apps/unveil/env/Providers;->staticProvider(Ljava/lang/Object;)Lcom/google/android/apps/unveil/env/Provider;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/unveil/env/Providers;->staticProvider(Ljava/lang/Object;)Lcom/google/android/apps/unveil/env/Provider;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/unveil/protocol/QueryManager;-><init>(Lcom/google/android/apps/unveil/protocol/TraceTracker;Lcom/google/android/apps/unveil/protocol/QueryManagerParams;Ljava/util/List;Lcom/google/android/apps/unveil/network/AbstractConnector;Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;)V

    return-object v0
.end method

.method private declared-synchronized restart()Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 305
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    move v7, v8

    .line 307
    .local v7, previewAlreadyRunning:Z
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->uiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/unveil/textinput/TextInput$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$5;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 320
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->setQueryListener(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    .line 322
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setAcceptSmudges(Z)V

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->getAlphaValue()F

    move-result v1

    const/4 v2, 0x0

    const-wide/16 v3, 0x190

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/apps/unveil/textinput/TextInput$6;

    invoke-direct {v6, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$6;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/unveil/textinput/AnimationHelper;->alpha(Landroid/view/View;FFJLandroid/view/animation/Interpolator;Ljava/lang/Runnable;)V

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->gridView:Landroid/view/View;

    const/4 v1, 0x0

    const/high16 v2, 0x3f80

    const-wide/16 v3, 0x190

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/unveil/textinput/AnimationHelper;->alpha(Landroid/view/View;FFJLandroid/view/animation/Interpolator;Ljava/lang/Runnable;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextMasker;->clearWords()V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    sget-object v1, Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;->NONE:Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/TextMasker;->setSmudge(Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->zoomableContainer:Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/ZoomableContainer;->reset()V

    .line 340
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->isSnapping:Z

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->onRestart()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 343
    if-nez v7, :cond_2

    move v0, v8

    :goto_1
    monitor-exit p0

    return v0

    .end local v7           #previewAlreadyRunning:Z
    :cond_1
    move v7, v9

    .line 305
    goto :goto_0

    .restart local v7       #previewAlreadyRunning:Z
    :cond_2
    move v0, v9

    .line 343
    goto :goto_1

    .line 305
    .end local v7           #previewAlreadyRunning:Z
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setConnector(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "context"
    .parameter "frontendUrl"

    .prologue
    .line 189
    new-instance v0, Lcom/google/android/apps/unveil/textinput/TextInput$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$1;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    invoke-static {p1}, Lcom/google/android/apps/unveil/textinput/InstallationIdHelper;->getInstallationId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/apps/unveil/network/HttpClientConnector;->newHttpClientConnector(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/unveil/env/Provider;Ljava/lang/String;)Lcom/google/android/apps/unveil/network/HttpClientConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->connector:Lcom/google/android/apps/unveil/network/AbstractConnector;

    .line 199
    new-instance v0, Lcom/google/android/apps/unveil/protocol/TraceTracker;

    new-instance v1, Lcom/google/android/apps/unveil/textinput/TextInput$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput$2;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/apps/unveil/protocol/TracingCookieFetcher;

    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->connector:Lcom/google/android/apps/unveil/network/AbstractConnector;

    invoke-direct {v2, v3}, Lcom/google/android/apps/unveil/protocol/TracingCookieFetcher;-><init>(Lcom/google/android/apps/unveil/network/AbstractConnector;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/unveil/protocol/TraceTracker;-><init>(Lcom/google/android/apps/unveil/network/NetworkInfoProvider;Lcom/google/android/apps/unveil/protocol/TracingCookieFetcher;)V

    iput-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    .line 205
    invoke-direct {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->newQueryManager()Lcom/google/android/apps/unveil/protocol/QueryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryManager:Lcom/google/android/apps/unveil/protocol/QueryManager;

    .line 206
    return-void
.end method

.method private setQueryListener(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V
    .locals 2
    .parameter "newListener"

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/TextQueryListener;->cancel(Z)V

    .line 298
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    .line 299
    return-void
.end method

.method private showRecentFrame()V
    .locals 12

    .prologue
    const/16 v3, 0x10e

    const/high16 v1, 0x3f80

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 673
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 674
    .local v10, rotationMatrix:Landroid/graphics/Matrix;
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->recentFrame:Lcom/google/android/apps/unveil/textinput/TextInput$RecentFrame;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextInput$RecentFrame;->get()Lcom/google/android/apps/unveil/env/Picture;

    move-result-object v9

    .line 675
    .local v9, p:Lcom/google/android/apps/unveil/env/Picture;
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraToDisplayRotation(Landroid/content/Context;)I

    move-result v8

    .line 676
    .local v8, displayRotation:I
    if-eqz v9, :cond_3

    .line 677
    const/16 v0, 0x5a

    if-eq v8, v0, :cond_0

    if-ne v8, v3, :cond_2

    .line 678
    :cond_0
    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/unveil/env/Size;->width:I

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/unveil/env/Size;->height:I

    div-int/lit8 v2, v2, 0x2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v7, v0

    .line 679
    .local v7, axisPoint:F
    int-to-float v0, v8

    invoke-virtual {v10, v0, v7, v7}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 680
    if-ne v8, v3, :cond_1

    .line 683
    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/unveil/env/Size;->width:I

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/unveil/env/Size;->height:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {v10, v4, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/unveil/env/Size;->height:I

    int-to-float v2, v2

    div-float v11, v0, v2

    .line 686
    .local v11, scaleFactor:F
    invoke-virtual {v10, v11, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 692
    .end local v7           #axisPoint:F
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->calculatePreviewTranslation()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v10, v4, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 693
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2, v10}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setImageToDisplay(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 694
    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->recycle()V

    .line 699
    .end local v11           #scaleFactor:F
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    const-wide/16 v3, 0x258

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    move v2, v1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/unveil/textinput/AnimationHelper;->alpha(Landroid/view/View;FFJLandroid/view/animation/Interpolator;Ljava/lang/Runnable;)V

    .line 701
    return-void

    .line 688
    :cond_2
    int-to-float v0, v8

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/unveil/env/Size;->width:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v3

    iget v3, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v10, v0, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 689
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/unveil/env/Size;->width:I

    int-to-float v2, v2

    div-float v11, v0, v2

    .line 690
    .restart local v11       #scaleFactor:F
    invoke-virtual {v10, v11, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_0

    .line 696
    .end local v11           #scaleFactor:F
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v0, v6, v6}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setImageToDisplay(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    goto :goto_1
.end method

.method private declared-synchronized startCameraPreviewAndLooper()V
    .locals 9

    .prologue
    .line 951
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraLayoutComplete:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isCameraConnected()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 981
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 956
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->startPreview()V

    .line 958
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-nez v3, :cond_4

    .line 959
    new-instance v3, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    iget-object v4, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraToDisplayRotation(Landroid/content/Context;)I

    move-result v5

    const/high16 v6, 0x41f0

    const/4 v7, 0x3

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;IFI)V

    iput-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    .line 962
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    iget-object v4, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->recentFrame:Lcom/google/android/apps/unveil/textinput/TextInput$RecentFrame;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->addPreviewProcessor(Lcom/google/android/apps/unveil/nonstop/FrameProcessor;I)V

    .line 963
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    iget-object v4, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocusProcessor:Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->addPreviewProcessor(Lcom/google/android/apps/unveil/nonstop/FrameProcessor;I)V

    .line 965
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    new-instance v4, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;

    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    new-instance v6, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;-><init>(Landroid/content/Context;)V

    iget-object v7, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    const/4 v8, 0x0

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/apps/unveil/nonstop/StatusFrameProcessor;-><init>(Lcom/google/android/apps/unveil/sensors/CameraManager;Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;Lcom/google/android/apps/unveil/nonstop/DebugView;Lcom/google/android/apps/unveil/ui/GlOverlay;)V

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->addPreviewProcessor(Lcom/google/android/apps/unveil/nonstop/FrameProcessor;I)V

    .line 968
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    iget-object v4, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/unveil/nonstop/DebugView;->setCallback(Lcom/google/android/apps/unveil/nonstop/PreviewLooper;)V

    .line 973
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v0

    .line 974
    .local v0, previewSize:Lcom/google/android/apps/unveil/env/Size;
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraToDisplayRotation(Landroid/content/Context;)I

    move-result v2

    .line 975
    .local v2, rotation:I
    const/16 v3, 0x5a

    if-eq v2, v3, :cond_2

    const/16 v3, 0x10e

    if-ne v2, v3, :cond_3

    .line 977
    :cond_2
    new-instance v1, Lcom/google/android/apps/unveil/env/Size;

    iget v3, v0, Lcom/google/android/apps/unveil/env/Size;->height:I

    iget v4, v0, Lcom/google/android/apps/unveil/env/Size;->width:I

    invoke-direct {v1, v3, v4}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    .end local v0           #previewSize:Lcom/google/android/apps/unveil/env/Size;
    .local v1, previewSize:Lcom/google/android/apps/unveil/env/Size;
    move-object v0, v1

    .line 979
    .end local v1           #previewSize:Lcom/google/android/apps/unveil/env/Size;
    .restart local v0       #previewSize:Lcom/google/android/apps/unveil/env/Size;
    :cond_3
    sget-object v3, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preview size is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 980
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->startLoop(Lcom/google/android/apps/unveil/env/Size;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 951
    .end local v0           #previewSize:Lcom/google/android/apps/unveil/env/Size;
    .end local v2           #rotation:I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 970
    :cond_4
    :try_start_2
    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v3}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseLoop()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method protected allocateBitmapAndMatriciesForCapture()Landroid/graphics/Bitmap;
    .locals 23

    .prologue
    .line 354
    new-instance v9, Lcom/google/android/apps/unveil/env/Size;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->getWidth()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->getHeight()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v9, v0, v1}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    .line 355
    .local v9, displayedSize:Lcom/google/android/apps/unveil/env/Size;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPictureSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v5

    .line 356
    .local v5, capturedImageSize:Lcom/google/android/apps/unveil/env/Size;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getPreviewSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v3

    .line 357
    .local v3, cameraPreviewSize:Lcom/google/android/apps/unveil/env/Size;
    sget-object v18, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v19, "allocateBitmapsForCapture:displayedSize(%s), capturedImageSize(%s), cameraPreviewSize(%s), cameraManager(%d x %d)"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v9, v20, v21

    const/16 v21, 0x1

    aput-object v5, v20, v21

    const/16 v21, 0x2

    aput-object v3, v20, v21

    const/16 v21, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getWidth()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getHeight()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    if-eqz v5, :cond_0

    if-nez v3, :cond_1

    .line 365
    :cond_0
    sget-object v18, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v19, "Failed to get imageSize or previewSize"

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    const/4 v7, 0x0

    .line 424
    :goto_0
    return-object v7

    .line 369
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getCameraToDisplayRotation(Landroid/content/Context;)I

    move-result v8

    .line 370
    .local v8, displayRotation:I
    const/16 v18, 0x5a

    move/from16 v0, v18

    if-eq v8, v0, :cond_2

    const/16 v18, 0x10e

    move/from16 v0, v18

    if-ne v8, v0, :cond_3

    .line 372
    :cond_2
    new-instance v6, Lcom/google/android/apps/unveil/env/Size;

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v18, v0

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v6, v0, v1}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    .line 373
    .end local v5           #capturedImageSize:Lcom/google/android/apps/unveil/env/Size;
    .local v6, capturedImageSize:Lcom/google/android/apps/unveil/env/Size;
    new-instance v4, Lcom/google/android/apps/unveil/env/Size;

    iget v0, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v18, v0

    iget v0, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    .end local v3           #cameraPreviewSize:Lcom/google/android/apps/unveil/env/Size;
    .local v4, cameraPreviewSize:Lcom/google/android/apps/unveil/env/Size;
    move-object v3, v4

    .end local v4           #cameraPreviewSize:Lcom/google/android/apps/unveil/env/Size;
    .restart local v3       #cameraPreviewSize:Lcom/google/android/apps/unveil/env/Size;
    move-object v5, v6

    .line 379
    .end local v6           #capturedImageSize:Lcom/google/android/apps/unveil/env/Size;
    .restart local v5       #capturedImageSize:Lcom/google/android/apps/unveil/env/Size;
    :cond_3
    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    iget v0, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v18, v18, v19

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    iget v0, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(FF)F

    move-result v12

    .line 382
    .local v12, previewToPictureScale:F
    new-instance v14, Lcom/google/android/apps/unveil/env/Size;

    iget v0, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, v12

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    iget v0, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v12

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v14, v0, v1}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    .line 388
    .local v14, upscaledPreview:Lcom/google/android/apps/unveil/env/Size;
    invoke-static {v14, v9}, Lcom/google/android/apps/unveil/env/ImageUtils;->generateUndistortTransformer(Lcom/google/android/apps/unveil/env/Size;Lcom/google/android/apps/unveil/env/Size;)Landroid/graphics/Matrix;

    move-result-object v15

    .line 390
    .local v15, upscaledToDisplayedTransformer:Landroid/graphics/Matrix;
    const/16 v18, 0x9

    move/from16 v0, v18

    new-array v11, v0, [F

    .line 391
    .local v11, matrixValues:[F
    invoke-virtual {v15, v11}, Landroid/graphics/Matrix;->getValues([F)V

    .line 392
    const/16 v18, 0x0

    aget v16, v11, v18

    .line 393
    .local v16, xScale:F
    const/16 v18, 0x4

    aget v17, v11, v18

    .line 396
    .local v17, yScale:F
    iget v0, v14, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v18, v18, v16

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    iget v0, v14, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v19, v19, v17

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    sget-object v20, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v18 .. v20}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 400
    .local v7, croppedBitmap:Landroid/graphics/Bitmap;
    new-instance v18, Landroid/graphics/Matrix;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Matrix;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/unveil/textinput/TextInput;->capturedToCroppedMatrix:Landroid/graphics/Matrix;

    .line 401
    const/16 v18, 0x5a

    move/from16 v0, v18

    if-eq v8, v0, :cond_4

    const/16 v18, 0x10e

    move/from16 v0, v18

    if-ne v8, v0, :cond_6

    .line 402
    :cond_4
    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v18, v0

    div-int/lit8 v18, v18, 0x2

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v19, v0

    div-int/lit8 v19, v19, 0x2

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v18

    move/from16 v0, v18

    int-to-float v2, v0

    .line 403
    .local v2, axisPoint:F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->capturedToCroppedMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    int-to-float v0, v8

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 404
    const/16 v18, 0x10e

    move/from16 v0, v18

    if-ne v8, v0, :cond_5

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->capturedToCroppedMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v20, v0

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v21, v0

    sub-int v20, v20, v21

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 416
    .end local v2           #axisPoint:F
    :cond_5
    :goto_1
    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getWidth()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v18, v18, v19

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(FF)F

    move-result v10

    .line 419
    .local v10, displayedToPictureScale:F
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->calculatePreviewTranslation()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, v10

    move/from16 v0, v18

    float-to-int v13, v0

    .line 420
    .local v13, shift:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->capturedToCroppedMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v19, v0

    iget v0, v14, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    neg-int v0, v0

    move/from16 v19, v0

    div-int/lit8 v19, v19, 0x2

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v20, v0

    iget v0, v14, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v21, v0

    sub-int v20, v20, v21

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    sub-int v20, v20, v13

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_0

    .line 410
    .end local v10           #displayedToPictureScale:F
    .end local v13           #shift:I
    :cond_6
    const/16 v18, 0xb4

    move/from16 v0, v18

    if-ne v8, v0, :cond_5

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->capturedToCroppedMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    const/high16 v19, 0x4334

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    iget v0, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v21, v0

    div-int/lit8 v21, v21, 0x2

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    invoke-virtual/range {v18 .. v21}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    goto/16 :goto_1
.end method

.method public declared-synchronized changeSourceLanguage(Ljava/lang/String;)Z
    .locals 2
    .parameter "language"

    .prologue
    const/4 v0, 0x0

    .line 781
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->sourceLanguage:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v1, :cond_1

    .line 794
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 784
    :cond_1
    :try_start_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->sourceLanguage:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 787
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->sourceLanguage:Ljava/lang/String;

    .line 789
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textQuery:Lcom/google/android/apps/unveil/protocol/QueryBuilder;

    if-eqz v1, :cond_0

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->load(Lcom/google/android/apps/unveil/env/Picture;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794
    const/4 v0, 0x1

    goto :goto_0

    .line 781
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cycleDebugMode(Z)V
    .locals 1
    .parameter "cycleUp"

    .prologue
    .line 749
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/nonstop/DebugView;->cycleDebugMode(Z)V

    .line 751
    return-void
.end method

.method public finishInput()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 569
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "finish input explicitly."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 570
    iput-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageToSend:Lcom/google/android/apps/unveil/env/Picture;

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    .line 573
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->pauseLoop()V

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->blockingCaptureLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 577
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->stopPreview()V

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->forceReleaseCamera()V

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->getCurrentActionNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->tryToEndTraceAction(I)V

    .line 581
    invoke-direct {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->maybeSendTraces()V

    .line 585
    invoke-virtual {p0, v4}, Lcom/google/android/apps/unveil/textinput/TextInput;->setVisibility(I)V

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setVisibility(I)V

    .line 592
    iput-boolean v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraLayoutComplete:Z

    .line 593
    iput-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    .line 594
    return-void
.end method

.method public getSupportedLanguages()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->supportedLanguages:[Ljava/lang/String;

    return-object v0
.end method

.method public gotResult(Ljava/lang/String;)V
    .locals 4
    .parameter "language"

    .prologue
    .line 876
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "got result: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 877
    return-void
.end method

.method public isFrozen()Z
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->previewLooper:Lcom/google/android/apps/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/nonstop/PreviewLooper;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized loadImage(Lcom/google/android/apps/unveil/env/Picture;)V
    .locals 22
    .parameter "picture"

    .prologue
    .line 802
    monitor-enter p0

    :try_start_0
    new-instance v7, Lcom/google/android/apps/unveil/env/Size;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->getWidth()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->getHeight()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v7, v0, v1}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    .line 803
    .local v7, displayedSize:Lcom/google/android/apps/unveil/env/Size;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/unveil/env/Picture;->getSize()Lcom/google/android/apps/unveil/env/Size;

    move-result-object v10

    .line 804
    .local v10, pictureSize:Lcom/google/android/apps/unveil/env/Size;
    iget v0, v10, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    iget v0, v7, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v15, v17, v18

    .line 805
    .local v15, xRatio:F
    iget v0, v10, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    iget v0, v7, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v16, v17, v18

    .line 806
    .local v16, yRatio:F
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 807
    .local v4, biggerRatio:F
    const/high16 v17, 0x3f80

    div-float v11, v17, v4

    .line 808
    .local v11, scaleFactor:F
    iget v0, v10, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v11

    move/from16 v0, v17

    float-to-int v13, v0

    .line 809
    .local v13, scaledPictureWidth:I
    iget v0, v10, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v11

    move/from16 v0, v17

    float-to-int v12, v0

    .line 812
    .local v12, scaledPictureHeight:I
    iget v0, v7, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v17, v0

    iget v0, v7, Lcom/google/android/apps/unveil/env/Size;->height:I

    move/from16 v18, v0

    sget-object v19, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v17 .. v19}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 814
    .local v5, bitmap:Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 815
    .local v6, canvas:Landroid/graphics/Canvas;
    iget v0, v7, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v17, v0

    sub-int v17, v17, v13

    div-int/lit8 v8, v17, 0x2

    .line 816
    .local v8, left:I
    iget v0, v7, Lcom/google/android/apps/unveil/env/Size;->width:I

    move/from16 v17, v0

    sub-int v17, v17, v12

    div-int/lit8 v14, v17, 0x2

    .line 817
    .local v14, top:I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/unveil/env/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v17

    const/16 v18, 0x0

    new-instance v19, Landroid/graphics/Rect;

    add-int v20, v8, v13

    add-int v21, v14, v12

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v8, v14, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 819
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v5, v0}, Lcom/google/android/apps/unveil/env/PictureFactory;->createBitmap(Landroid/graphics/Bitmap;I)Lcom/google/android/apps/unveil/env/Picture;

    move-result-object v9

    .line 820
    .local v9, p:Lcom/google/android/apps/unveil/env/Picture;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/apps/unveil/textinput/TextInput;->load(Lcom/google/android/apps/unveil/env/Picture;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 821
    monitor-exit p0

    return-void

    .line 802
    .end local v4           #biggerRatio:F
    .end local v5           #bitmap:Landroid/graphics/Bitmap;
    .end local v6           #canvas:Landroid/graphics/Canvas;
    .end local v7           #displayedSize:Lcom/google/android/apps/unveil/env/Size;
    .end local v8           #left:I
    .end local v9           #p:Lcom/google/android/apps/unveil/env/Picture;
    .end local v10           #pictureSize:Lcom/google/android/apps/unveil/env/Size;
    .end local v11           #scaleFactor:F
    .end local v12           #scaledPictureHeight:I
    .end local v13           #scaledPictureWidth:I
    .end local v14           #top:I
    .end local v15           #xRatio:F
    .end local v16           #yRatio:F
    :catchall_0
    move-exception v17

    monitor-exit p0

    throw v17
.end method

.method public noResults()V
    .locals 3

    .prologue
    .line 869
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "no results."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 870
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    sget-object v1, Lcom/google/goggles/TracingProtos$SpanVariable$Type;->START_TO_RENDERED:Lcom/google/goggles/TracingProtos$SpanVariable$Type;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->endInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V

    .line 871
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->noResults()V

    .line 872
    return-void
.end method

.method public onCameraAcquisitionError()V
    .locals 3

    .prologue
    .line 901
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Failed to acquire camera."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 902
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->onCameraError()V

    .line 903
    return-void
.end method

.method public onCameraConnected()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 894
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Camera connected"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 895
    invoke-virtual {p0, v3}, Lcom/google/android/apps/unveil/textinput/TextInput;->setVisibility(I)V

    .line 896
    invoke-direct {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->startCameraPreviewAndLooper()V

    .line 897
    return-void
.end method

.method public onCameraFlashControlError()V
    .locals 3

    .prologue
    .line 907
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Failed to apply camera flash setting."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 908
    return-void
.end method

.method public onCameraLayoutComplete()V
    .locals 4

    .prologue
    .line 923
    sget-object v1, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "Camera layout completed."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 926
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraLayoutComplete:Z

    .line 927
    invoke-direct {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->startCameraPreviewAndLooper()V

    .line 932
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->zoomableContainer:Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/textinput/ZoomableContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 933
    .local v0, lp:Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 934
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->zoomableContainer:Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/unveil/textinput/ZoomableContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 938
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->zoomableContainer:Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextInput$10;

    invoke-direct {v2, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$10;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/unveil/textinput/ZoomableContainer;->post(Ljava/lang/Runnable;)Z

    .line 944
    return-void
.end method

.method public declared-synchronized onCameraPreviewSizeChanged()V
    .locals 3

    .prologue
    .line 917
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Camera preview size changed."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 918
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocus:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->setAutoFocus(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 919
    monitor-exit p0

    return-void

    .line 917
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCameraQualityError()V
    .locals 3

    .prologue
    .line 912
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "Failed to apply camera quality settings."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 913
    return-void
.end method

.method protected onFinishInflate()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 210
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 212
    .local v1, context:Landroid/content/Context;
    const-class v2, Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-static {p0, v2}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByType(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/sensors/CameraManager;

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    .line 214
    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setAcquireCameraOnVisibilityChange(Z)V

    .line 215
    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->registerListener(Lcom/google/android/apps/unveil/sensors/CameraManager$Listener;)V

    .line 216
    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    sget-object v3, Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;->HIGH_QUALITY:Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/unveil/sensors/CameraManager;->requestPictureQuality(Lcom/google/android/apps/unveil/sensors/CameraManager$PictureQuality;)V

    .line 217
    const-class v2, Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    invoke-static {p0, v2}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByType(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->zoomableContainer:Lcom/google/android/apps/unveil/textinput/ZoomableContainer;

    .line 218
    const-class v2, Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-static {p0, v2}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByType(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/textinput/SmudgeView;

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    .line 219
    const-class v2, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    invoke-static {p0, v2}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByType(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    .line 220
    const-class v2, Lcom/google/android/apps/unveil/textinput/GridOverlayView;

    invoke-static {p0, v2}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByType(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->gridView:Landroid/view/View;

    .line 221
    const-class v2, Lcom/google/android/apps/unveil/nonstop/DebugView;

    invoke-static {p0, v2}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByType(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/unveil/nonstop/DebugView;

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->debugView:Lcom/google/android/apps/unveil/nonstop/DebugView;

    .line 222
    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-direct {v2, v1}, Lcom/google/android/apps/unveil/textinput/TextMasker;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->setTextMasker(Lcom/google/android/apps/unveil/textinput/TextMasker;)V

    .line 224
    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setListener(Lcom/google/android/apps/unveil/textinput/SmudgeView$Listener;)V

    .line 225
    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setAcceptSmudges(Z)V

    .line 231
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->uiHandler:Landroid/os/Handler;

    .line 232
    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->audioManager:Landroid/media/AudioManager;

    .line 233
    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextInput$RecentFrame;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/unveil/textinput/TextInput$RecentFrame;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;Lcom/google/android/apps/unveil/textinput/TextInput$1;)V

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->recentFrame:Lcom/google/android/apps/unveil/textinput/TextInput$RecentFrame;

    .line 234
    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;

    iget-object v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;Lcom/google/android/apps/unveil/sensors/CameraManager;)V

    iput-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocusProcessor:Lcom/google/android/apps/unveil/textinput/TextInput$AutoFocusProcessor;

    .line 236
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 237
    .local v0, application:Landroid/content/Context;
    instance-of v2, v0, Lcom/google/android/apps/unveil/UnveilContext;

    if-nez v2, :cond_0

    .line 238
    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    new-instance v3, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;

    invoke-direct {v3, v1}, Lcom/google/android/apps/unveil/sensors/UnveilSensorProvider;-><init>(Landroid/content/Context;)V

    invoke-static {v3}, Lcom/google/android/apps/unveil/env/Providers;->staticProvider(Ljava/lang/Object;)Lcom/google/android/apps/unveil/env/Provider;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/unveil/env/Providers;->staticProvider(Ljava/lang/Object;)Lcom/google/android/apps/unveil/env/Provider;

    move-result-object v4

    const-class v5, Lcom/google/android/apps/unveil/sensors/proxies/camera/RealCamera;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/unveil/env/Providers;->staticProvider(Ljava/lang/Object;)Lcom/google/android/apps/unveil/env/Provider;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/unveil/textinput/TextInput$3;

    invoke-direct {v6, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$3;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/unveil/sensors/CameraManager;->init(Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;Lcom/google/android/apps/unveil/env/Provider;)V

    .line 249
    :cond_0
    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextInput$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$4;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/unveil/textinput/TextInput;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/google/android/apps/unveil/textinput/TextInput;->setVisibility(I)V

    .line 258
    return-void
.end method

.method public onNetworkError(I)V
    .locals 5
    .parameter "statusCode"

    .prologue
    .line 888
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on network error: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 889
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->onNetworkError(I)V

    .line 890
    return-void
.end method

.method public onResult()V
    .locals 3

    .prologue
    .line 881
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on result."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 882
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    sget-object v1, Lcom/google/goggles/TracingProtos$SpanVariable$Type;->START_TO_RENDERED:Lcom/google/goggles/TracingProtos$SpanVariable$Type;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->endInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)V

    .line 883
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/textinput/TextMasker;->getSelectedWordsAsString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->onResult(Ljava/lang/String;[Ljava/lang/String;)V

    .line 884
    return-void
.end method

.method public onSmudgeComplete(Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;)V
    .locals 5
    .parameter "smudge"

    .prologue
    const/4 v4, 0x0

    .line 845
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on smudge complete."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 846
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    iget-wide v0, v0, Lcom/google/android/apps/unveil/textinput/TextQueryListener;->queryId:J

    iget-wide v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextQueryListener;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 851
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextMasker;->setSmudge(Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;)V

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextQueryListener;->textResponseRecieved()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 854
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->setVisibility(I)V

    .line 855
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->invalidate()V

    .line 856
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v1}, Lcom/google/android/apps/unveil/textinput/TextMasker;->getSelectedWordsAsString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->onResult(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 858
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    iget-wide v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryId:J

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->onSearching(J)V

    goto :goto_0
.end method

.method public onSmudgeProgress(Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;Landroid/graphics/Rect;)V
    .locals 5
    .parameter "smudge"
    .parameter "dirtyRect"

    .prologue
    const/4 v4, 0x0

    .line 831
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on smudge progress."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 832
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    iget-wide v0, v0, Lcom/google/android/apps/unveil/textinput/TextQueryListener;->queryId:J

    iget-wide v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryListener:Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextQueryListener;->textResponseRecieved()Z

    move-result v0

    if-nez v0, :cond_1

    .line 841
    :cond_0
    :goto_0
    return-void

    .line 838
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextMasker;->setSmudge(Lcom/google/android/apps/unveil/textinput/SmudgeView$Smudge;)V

    .line 839
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->setVisibility(I)V

    .line 840
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/unveil/textinput/BoundingBoxView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public onSmudgeStarted()V
    .locals 3

    .prologue
    .line 825
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on smudge started."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->clearSmudges()V

    .line 827
    return-void
.end method

.method public onZoom()V
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/textinput/TextInput$Listener;->onZoom()V

    .line 865
    return-void
.end method

.method public setAutoFocus(Z)V
    .locals 3
    .parameter "enabled"

    .prologue
    const/4 v2, 0x0

    .line 603
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->autoFocus:Z

    .line 605
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setFocusable(Z)V

    .line 606
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isFocusSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 607
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "camera does not support focus."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 618
    :goto_0
    return-void

    .line 611
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isContinuousFocusSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->enableContinuousFocus(Z)V

    .line 613
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "camera supports continuous focus."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 617
    :cond_1
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "use image blurriness based auto focus."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setClientType(Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;)V
    .locals 0
    .parameter "clientType"

    .prologue
    .line 534
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->clientType:Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;

    .line 535
    return-void
.end method

.method public setFrontendUrl(Ljava/lang/String;)V
    .locals 1
    .parameter "frontendUrl"

    .prologue
    .line 765
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->setConnector(Landroid/content/Context;Ljava/lang/String;)V

    .line 766
    return-void
.end method

.method public setImageLogging(Z)V
    .locals 0
    .parameter "enabled"

    .prologue
    .line 625
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->imageLogging:Z

    .line 626
    return-void
.end method

.method public setListener(Lcom/google/android/apps/unveil/textinput/TextInput$Listener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 527
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->listener:Lcom/google/android/apps/unveil/textinput/TextInput$Listener;

    .line 528
    return-void
.end method

.method public setUserAgent(Ljava/lang/String;)V
    .locals 0
    .parameter "userAgent"

    .prologue
    .line 541
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->userAgent:Ljava/lang/String;

    .line 542
    return-void
.end method

.method public declared-synchronized snap()V
    .locals 7

    .prologue
    .line 641
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->isSnapping:Z

    if-eqz v0, :cond_0

    .line 642
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "You already snapped, ignoring duplicate snap request."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669
    :goto_0
    monitor-exit p0

    return-void

    .line 645
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->isSnapping:Z

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    sget-object v1, Lcom/google/goggles/TracingProtos$PointVariable$Type;->USER:Lcom/google/goggles/TracingProtos$PointVariable$Type;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->addPoint(Lcom/google/goggles/TracingProtos$PointVariable$Type;)V

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    sget-object v1, Lcom/google/goggles/TracingProtos$SpanVariable$Type;->START_TO_RENDERED:Lcom/google/goggles/TracingProtos$SpanVariable$Type;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->beginInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)I

    .line 649
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    sget-object v1, Lcom/google/goggles/TracingProtos$SpanVariable$Type;->ACQUIRE_TO_IMAGE:Lcom/google/goggles/TracingProtos$SpanVariable$Type;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->beginInterval(Lcom/google/goggles/TracingProtos$SpanVariable$Type;)I

    .line 650
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->gridView:Landroid/view/View;

    const/high16 v1, 0x3f80

    const/4 v2, 0x0

    const-wide/16 v3, 0x258

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/unveil/textinput/AnimationHelper;->alpha(Landroid/view/View;FFJLandroid/view/animation/Interpolator;Ljava/lang/Runnable;)V

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->clearSmudges()V

    .line 655
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryId:J

    .line 656
    new-instance v0, Lcom/google/android/apps/unveil/textinput/TextQueryListener;

    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->boundingBoxView:Lcom/google/android/apps/unveil/textinput/BoundingBoxView;

    iget-object v2, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->textMasker:Lcom/google/android/apps/unveil/textinput/TextMasker;

    iget-wide v3, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->queryId:J

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/apps/unveil/textinput/TextQueryListener$Listener;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/unveil/textinput/TextQueryListener;-><init>(Lcom/google/android/apps/unveil/textinput/BoundingBoxView;Lcom/google/android/apps/unveil/textinput/TextMasker;J[Lcom/google/android/apps/unveil/textinput/TextQueryListener$Listener;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->setQueryListener(Lcom/google/android/apps/unveil/textinput/TextQueryListener;)V

    .line 658
    new-instance v0, Lcom/google/android/apps/unveil/textinput/TextInput$9;

    invoke-direct {v0, p0}, Lcom/google/android/apps/unveil/textinput/TextInput$9;-><init>(Lcom/google/android/apps/unveil/textinput/TextInput;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->maybeTriggerFocus(Lcom/google/android/apps/unveil/sensors/CameraManager$FocusCallback;)V

    .line 668
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->smudgeView:Lcom/google/android/apps/unveil/textinput/SmudgeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/SmudgeView;->setAcceptSmudges(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 641
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public startInput(Ljava/lang/String;)Z
    .locals 5
    .parameter "language"

    .prologue
    const/4 v4, 0x0

    .line 552
    sget-object v1, Lcom/google/android/apps/unveil/textinput/TextInput;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "start input explicitly, language %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "auto-detect"

    :goto_0
    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setVisibility(I)V

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/sensors/CameraManager;->acquireCamera()V

    .line 558
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->sourceLanguage:Ljava/lang/String;

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->startNewTraceAction()V

    .line 560
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInput;->traceTracker:Lcom/google/android/apps/unveil/protocol/TraceTracker;

    sget-object v1, Lcom/google/goggles/TracingProtos$PointVariable$Type;->CAMERA_PREVIEW_START:Lcom/google/goggles/TracingProtos$PointVariable$Type;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/protocol/TraceTracker;->addPoint(Lcom/google/goggles/TracingProtos$PointVariable$Type;)V

    .line 561
    invoke-direct {p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->restart()Z

    move-result v0

    return v0

    :cond_0
    move-object v0, p1

    .line 552
    goto :goto_0
.end method
