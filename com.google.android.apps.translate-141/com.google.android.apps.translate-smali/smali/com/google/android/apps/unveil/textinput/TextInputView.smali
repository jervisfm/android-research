.class public Lcom/google/android/apps/unveil/textinput/TextInputView;
.super Landroid/widget/FrameLayout;
.source "TextInputView.java"

# interfaces
.implements Lcom/google/android/apps/unveil/textinput/TextInput$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;
    }
.end annotation


# static fields
.field private static final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# instance fields
.field private customHeight:Ljava/lang/String;

.field private instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

.field private keyboardButton:Landroid/view/View;

.field private listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

.field private loadButton:Landroid/view/View;

.field private progressBar:Landroid/view/View;

.field private retryButton:Landroid/view/View;

.field private snapButton:Landroid/view/View;

.field private sourceLanguage:Ljava/lang/String;

.field private textInput:Lcom/google/android/apps/unveil/textinput/TextInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    sput-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    invoke-virtual {p0, p1}, Lcom/google/android/apps/unveil/textinput/TextInputView;->createView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/unveil/textinput/TextInputView;->addView(Landroid/view/View;)V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/unveil/textinput/TextInputView;)Lcom/google/android/apps/unveil/textinput/TextInput;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/unveil/textinput/TextInputView;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->sourceLanguage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/unveil/textinput/TextInputView;)Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

    return-object v0
.end method

.method public static final isSupported(Landroid/content/Context;)Z
    .locals 7
    .parameter "context"

    .prologue
    const/4 v3, 0x0

    .line 75
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x9

    if-ge v4, v5, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v3

    .line 78
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 80
    .local v2, pm:Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_0

    const-string v4, "android.hardware.camera"

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 84
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v4

    if-eqz v4, :cond_0

    .line 91
    :try_start_0
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 92
    .local v1, info:Landroid/hardware/Camera$CameraInfo;
    const/4 v4, 0x0

    invoke-static {v4, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    const/4 v3, 0x1

    goto :goto_0

    .line 93
    .end local v1           #info:Landroid/hardware/Camera$CameraInfo;
    :catch_0
    move-exception v0

    .line 94
    .local v0, e:Ljava/lang/RuntimeException;
    sget-object v4, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v5, "This device claims it has a camera, but attempted getCameraInfo failed"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private makeSizeSpec(Ljava/lang/String;I)I
    .locals 8
    .parameter "param"
    .parameter "suggestedSpec"

    .prologue
    const/high16 v7, 0x4000

    const/high16 v6, -0x8000

    const/4 v5, 0x0

    .line 285
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 286
    .local v0, mode:I
    if-ne v0, v7, :cond_1

    .line 308
    .end local p2
    :cond_0
    :goto_0
    return p2

    .line 289
    .restart local p2
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 290
    .local v2, size:I
    move v1, v2

    .line 291
    .local v1, pixels:I
    const-string v3, "px"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 292
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 304
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    if-ne v0, v6, :cond_0

    if-le v2, v1, :cond_0

    .line 306
    :cond_3
    invoke-static {v7, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_0

    .line 293
    :cond_4
    const-string v3, "dp"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 294
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInputView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/unveil/env/PixelUtils;->dipToPix(FLandroid/content/Context;)I

    move-result v1

    goto :goto_1

    .line 296
    :cond_5
    const-string v3, "in"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 297
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInputView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/unveil/env/PixelUtils;->inchToPix(FLandroid/content/Context;)I

    move-result v1

    goto :goto_1

    .line 299
    :cond_6
    const-string v3, "%"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 300
    if-ne v0, v6, :cond_2

    .line 301
    int-to-float v3, v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    mul-float/2addr v3, v4

    const/high16 v4, 0x42c8

    div-float/2addr v3, v4

    float-to-int v1, v3

    goto :goto_1
.end method


# virtual methods
.method public changeSourceLanguage(Ljava/lang/String;)Z
    .locals 1
    .parameter "language"

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->changeSourceLanguage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected createView(Landroid/content/Context;)Landroid/view/View;
    .locals 2
    .parameter "context"

    .prologue
    .line 106
    sget v0, Lcom/google/android/apps/unveil/textinput/R$layout;->text_input_view:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public cycleDebugMode(Z)V
    .locals 1
    .parameter "cycleUp"

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->cycleDebugMode(Z)V

    .line 239
    return-void
.end method

.method public finishInput()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->finishInput()V

    .line 198
    return-void
.end method

.method public getSupportedLanguages()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->getSupportedLanguages()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isFrozen()Z
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->isFrozen()Z

    move-result v0

    return v0
.end method

.method public loadImage(Landroid/net/Uri;)V
    .locals 11
    .parameter "imageUri"

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 264
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInputView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 265
    .local v0, context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 266
    .local v4, resolver:Landroid/content/ContentResolver;
    invoke-static {v4, p1}, Lcom/google/android/apps/unveil/env/media/MediaStoreMergerAdapter;->getImage(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/google/android/apps/unveil/env/media/ImageLoader$Image;

    move-result-object v2

    .line 267
    .local v2, image:Lcom/google/android/apps/unveil/env/media/ImageLoader$Image;
    if-nez v2, :cond_0

    .line 268
    sget-object v5, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Loaded null image for %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 269
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

    invoke-interface {v5}, Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;->onLoadImageError()V

    .line 282
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #image:Lcom/google/android/apps/unveil/env/media/ImageLoader$Image;
    .end local v4           #resolver:Landroid/content/ContentResolver;
    :goto_0
    return-void

    .line 272
    .restart local v0       #context:Landroid/content/Context;
    .restart local v2       #image:Lcom/google/android/apps/unveil/env/media/ImageLoader$Image;
    .restart local v4       #resolver:Landroid/content/ContentResolver;
    :cond_0
    iget v5, v2, Lcom/google/android/apps/unveil/env/media/ImageLoader$Image;->orientation:I

    invoke-static {v0, p1, v5}, Lcom/google/android/apps/unveil/env/PictureFactory;->loadAndDownsample(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/apps/unveil/env/Picture;

    move-result-object v3

    .line 274
    .local v3, picture:Lcom/google/android/apps/unveil/env/Picture;
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/unveil/textinput/TextInput;->loadImage(Lcom/google/android/apps/unveil/env/Picture;)V
    :try_end_0
    .catch Lcom/google/android/apps/unveil/env/media/InvalidUriException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/unveil/env/PictureLoadingException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 275
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #image:Lcom/google/android/apps/unveil/env/media/ImageLoader$Image;
    .end local v3           #picture:Lcom/google/android/apps/unveil/env/Picture;
    .end local v4           #resolver:Landroid/content/ContentResolver;
    :catch_0
    move-exception v1

    .line 276
    .local v1, e:Lcom/google/android/apps/unveil/env/media/InvalidUriException;
    sget-object v5, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Failed to load image, invalid uri: %s"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object p1, v7, v9

    invoke-virtual {v5, v1, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

    invoke-interface {v5}, Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;->onLoadImageError()V

    goto :goto_0

    .line 278
    .end local v1           #e:Lcom/google/android/apps/unveil/env/media/InvalidUriException;
    :catch_1
    move-exception v1

    .line 279
    .local v1, e:Lcom/google/android/apps/unveil/env/PictureLoadingException;
    sget-object v5, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v6, "Failed to load image!"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-virtual {v5, v1, v6, v7}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 280
    iget-object v5, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

    invoke-interface {v5}, Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;->onLoadImageError()V

    goto :goto_0
.end method

.method public noResults()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 385
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "no results"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->setVisibility(I)V

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    sget-object v1, Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;->NO_TEXT:Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->show(Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;)V

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->sendAccessibilityEvent(I)V

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->retryButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->snapButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->progressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 392
    return-void
.end method

.method public onCameraError()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

    invoke-interface {v0}, Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;->onCameraError()V

    .line 409
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 113
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/textinput/TextInputView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 116
    .local v0, context:Landroid/content/Context;
    const-class v1, Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-static {p0, v1}, Lcom/google/android/apps/unveil/textinput/TextInput;->findViewByType(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/textinput/TextInput;

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/unveil/textinput/TextInput;->setListener(Lcom/google/android/apps/unveil/textinput/TextInput$Listener;)V

    .line 119
    sget v1, Lcom/google/android/apps/unveil/textinput/R$id;->instructions:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/unveil/textinput/TextInputView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/unveil/textinput/InstructionsView;

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    .line 120
    sget v1, Lcom/google/android/apps/unveil/textinput/R$id;->progress_bar:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/unveil/textinput/TextInputView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->progressBar:Landroid/view/View;

    .line 122
    sget v1, Lcom/google/android/apps/unveil/textinput/R$id;->snapshot:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/unveil/textinput/TextInputView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->snapButton:Landroid/view/View;

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->snapButton:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextInputView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/unveil/textinput/TextInputView$1;-><init>(Lcom/google/android/apps/unveil/textinput/TextInputView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    sget v1, Lcom/google/android/apps/unveil/textinput/R$id;->retry:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/unveil/textinput/TextInputView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->retryButton:Landroid/view/View;

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->retryButton:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextInputView$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/unveil/textinput/TextInputView$2;-><init>(Lcom/google/android/apps/unveil/textinput/TextInputView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    sget v1, Lcom/google/android/apps/unveil/textinput/R$id;->keyboard:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/unveil/textinput/TextInputView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->keyboardButton:Landroid/view/View;

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->keyboardButton:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextInputView$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/unveil/textinput/TextInputView$3;-><init>(Lcom/google/android/apps/unveil/textinput/TextInputView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    sget v1, Lcom/google/android/apps/unveil/textinput/R$id;->load:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/unveil/textinput/TextInputView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->loadButton:Landroid/view/View;

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->loadButton:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/unveil/textinput/TextInputView$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/unveil/textinput/TextInputView$4;-><init>(Lcom/google/android/apps/unveil/textinput/TextInputView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    sget-object v1, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v2, "text input view is created."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 322
    const/16 v0, 0x1b

    if-ne p1, v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0}, Lcom/google/android/apps/unveil/textinput/TextInput;->snap()V

    .line 324
    const/4 v0, 0x1

    .line 326
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 331
    const/16 v0, 0x1b

    if-ne p1, v0, :cond_0

    .line 332
    const/4 v0, 0x1

    .line 334
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->customHeight:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->customHeight:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/unveil/textinput/TextInputView;->makeSizeSpec(Ljava/lang/String;I)I

    move-result p2

    .line 315
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "param.height is %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->customHeight:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 318
    return-void
.end method

.method public onNetworkError(I)V
    .locals 5
    .parameter "statusCode"

    .prologue
    .line 396
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "network error: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;->onNetworkError(I)V

    .line 398
    return-void
.end method

.method public onPictureTaken()V
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->setVisibility(I)V

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    sget-object v1, Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;->USE_FINGER_TO_HIGHLIGHT:Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->show(Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->sendAccessibilityEvent(I)V

    .line 364
    return-void
.end method

.method public onPreviewFrozen()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 351
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on preview frozen"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    sget-object v1, Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;->HOLD_STEADY:Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->show(Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->sendAccessibilityEvent(I)V

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->retryButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->snapButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 357
    return-void
.end method

.method public onRestart()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 339
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on restart"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->setVisibility(I)V

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    sget-object v1, Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;->TAP_TO_READ:Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->show(Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;)V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->sendAccessibilityEvent(I)V

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->retryButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->snapButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->progressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 346
    return-void
.end method

.method public onResult(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .parameter "text"
    .parameter "alternatives"

    .prologue
    const/4 v3, 0x0

    .line 375
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on result \'%s\'"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;->onTextSelected(Ljava/lang/String;[Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->retryButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->snapButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->progressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->setVisibility(I)V

    .line 381
    return-void
.end method

.method public onSearching(J)V
    .locals 5
    .parameter "queryId"

    .prologue
    const/4 v4, 0x0

    .line 368
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "on searching with query id %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->setVisibility(I)V

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->progressBar:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 371
    return-void
.end method

.method public onZoom()V
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->setVisibility(I)V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->instructions:Lcom/google/android/apps/unveil/textinput/InstructionsView;

    sget-object v1, Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;->USE_TWO_FINGERS_TO_PAN:Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/unveil/textinput/InstructionsView;->show(Lcom/google/android/apps/unveil/textinput/InstructionsView$Instruction;)V

    .line 404
    return-void
.end method

.method public setAutoFocus(Z)V
    .locals 1
    .parameter "enabled"

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->setAutoFocus(Z)V

    .line 218
    return-void
.end method

.method public setClientType(Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;)V
    .locals 1
    .parameter "clientType"

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->setClientType(Lcom/google/android/apps/unveil/textinput/TextInput$ClientType;)V

    .line 173
    return-void
.end method

.method public setFrontendUrl(Ljava/lang/String;)V
    .locals 1
    .parameter "frontendUrl"

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->setFrontendUrl(Ljava/lang/String;)V

    .line 253
    return-void
.end method

.method public setImageLogging(Z)V
    .locals 1
    .parameter "enabled"

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->setImageLogging(Z)V

    .line 225
    return-void
.end method

.method public setListener(Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;)V
    .locals 3
    .parameter "listener"

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "set listener"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->listener:Lcom/google/android/apps/unveil/textinput/TextInputView$Listener;

    .line 164
    return-void
.end method

.method public setUserAgent(Ljava/lang/String;)V
    .locals 1
    .parameter "userAgent"

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/textinput/TextInput;->setUserAgent(Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public setViewHeight(Ljava/lang/String;)V
    .locals 0
    .parameter "height"

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->customHeight:Ljava/lang/String;

    .line 211
    return-void
.end method

.method public startInput(Landroid/view/inputmethod/EditorInfo;Ljava/lang/String;)Z
    .locals 3
    .parameter "info"
    .parameter "language"

    .prologue
    .line 188
    sget-object v0, Lcom/google/android/apps/unveil/textinput/TextInputView;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v1, "start input with editor info"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/unveil/env/UnveilLogger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    iput-object p2, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->sourceLanguage:Ljava/lang/String;

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/TextInputView;->textInput:Lcom/google/android/apps/unveil/textinput/TextInput;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/unveil/textinput/TextInput;->startInput(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
