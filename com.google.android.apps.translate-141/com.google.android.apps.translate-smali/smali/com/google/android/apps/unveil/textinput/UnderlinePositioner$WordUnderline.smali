.class public Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;
.super Ljava/lang/Object;
.source "UnderlinePositioner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/unveil/textinput/UnderlinePositioner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WordUnderline"
.end annotation


# instance fields
.field private final left:Landroid/graphics/Point;

.field private final right:Landroid/graphics/Point;

.field private final wordBox:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;


# direct methods
.method constructor <init>(Landroid/graphics/Point;Landroid/graphics/Point;Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)V
    .locals 0
    .parameter "left"
    .parameter "right"
    .parameter "wordBox"

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->left:Landroid/graphics/Point;

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->right:Landroid/graphics/Point;

    .line 29
    iput-object p3, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->wordBox:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 30
    return-void
.end method


# virtual methods
.method draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 8
    .parameter "canvas"
    .parameter "paint"

    .prologue
    const/high16 v7, 0x4000

    .line 40
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v6

    .line 41
    .local v6, width:F
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->left:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    div-float v1, v6, v7

    add-float/2addr v1, v0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->left:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    sub-float v2, v0, v6

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->left:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    div-float v3, v6, v7

    add-float/2addr v3, v0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->left:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->left:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->left:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->right:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->right:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->right:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    div-float v1, v6, v7

    sub-float v1, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->right:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->right:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    div-float v3, v6, v7

    sub-float v3, v0, v3

    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->right:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    sub-float v4, v0, v6

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 44
    return-void
.end method

.method public getBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/unveil/textinput/UnderlinePositioner$WordUnderline;->wordBox:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object v0
.end method
