.class public Lcom/google/android/apps/unveil/tracking/VisionGyro;
.super Ljava/lang/Object;
.source "VisionGyro.java"


# instance fields
.field private nativeVisionGyro:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "clientvision"

    invoke-static {v0}, Lcom/google/android/apps/unveil/env/ResourceUtils;->loadNativeLibrary(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/tracking/VisionGyro;->constructNative()V

    .line 8
    return-void
.end method


# virtual methods
.method protected native constructNative()V
.end method

.method public declared-synchronized destroy()V
    .locals 1

    .prologue
    .line 11
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/tracking/VisionGyro;->destroyNative()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    monitor-exit p0

    return-void

    .line 11
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected native destroyNative()V
.end method

.method public declared-synchronized getLastTransform(Z)[F
    .locals 2
    .parameter "centerCoordinateSystem"

    .prologue
    .line 23
    monitor-enter p0

    const/16 v1, 0x9

    :try_start_0
    new-array v0, v1, [F

    .line 24
    .local v0, matrix:[F
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/unveil/tracking/VisionGyro;->obtainVgTransformNative([FZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    monitor-exit p0

    return-object v0

    .line 23
    .end local v0           #matrix:[F
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized nextFrame([BII)[F
    .locals 2
    .parameter "frameData"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 15
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/unveil/tracking/VisionGyro;->nextFrameNative([BII)V

    .line 17
    const/16 v1, 0x9

    new-array v0, v1, [F

    .line 18
    .local v0, matrix:[F
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/unveil/tracking/VisionGyro;->obtainVgTransformNative([FZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19
    monitor-exit p0

    return-object v0

    .line 15
    .end local v0           #matrix:[F
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected native nextFrameNative([BII)V
.end method

.method protected native obtainVgTransformNative([FZ)V
.end method
