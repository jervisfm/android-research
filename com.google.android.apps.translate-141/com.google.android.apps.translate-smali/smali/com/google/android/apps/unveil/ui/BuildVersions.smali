.class public Lcom/google/android/apps/unveil/ui/BuildVersions;
.super Ljava/lang/Object;
.source "BuildVersions.java"


# static fields
.field private static final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    sput-object v0, Lcom/google/android/apps/unveil/ui/BuildVersions;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static populate(Landroid/widget/TextView;)V
    .locals 11
    .parameter "buildVersion"

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 29
    const/4 v5, -0x1

    .line 30
    .local v5, versionCode:I
    const-wide/16 v3, 0x0

    .line 32
    .local v3, timestamp:J
    :try_start_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 33
    .local v0, context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget v5, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 36
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    iget-object v2, v6, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 37
    .local v2, sourceDir:Ljava/lang/String;
    new-instance v6, Ljava/util/zip/ZipFile;

    invoke-direct {v6, v2}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V

    const-string v7, "classes.dex"

    invoke-virtual {v6, v7}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getTime()J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v3

    .line 43
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #sourceDir:Ljava/lang/String;
    :goto_0
    const-string v6, "Build Version: %d (%s)"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    const-string v8, "M/d h:mm a"

    invoke-static {v8, v3, v4}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    return-void

    .line 38
    :catch_0
    move-exception v1

    .line 39
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v6, Lcom/google/android/apps/unveil/ui/BuildVersions;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v7, "Unable to load version code: %s"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v1, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v1

    .line 41
    .local v1, e:Ljava/io/IOException;
    sget-object v6, Lcom/google/android/apps/unveil/ui/BuildVersions;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v7, "Unable to calculate timestamp: %s"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v1, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
