.class public Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;
.super Landroid/widget/FrameLayout;
.source "CameraWrappingLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$1;,
        Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;,
        Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;,
        Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;
    }
.end annotation


# instance fields
.field alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

.field private cameraLayoutHandler:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;

.field private cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

.field private frameToCanvas:Landroid/graphics/Matrix;

.field private final logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

.field private mirrored:Z

.field private rotation:I

.field private scaleType:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 24
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    .line 56
    sget-object v0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;->TOP:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    .line 58
    sget-object v0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;->FIT:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->scaleType:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->mirrored:Z

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    .line 56
    sget-object v0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;->TOP:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    .line 58
    sget-object v0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;->FIT:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->scaleType:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->mirrored:Z

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    new-instance v0, Lcom/google/android/apps/unveil/env/UnveilLogger;

    invoke-direct {v0}, Lcom/google/android/apps/unveil/env/UnveilLogger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    .line 56
    sget-object v0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;->TOP:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    .line 58
    sget-object v0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;->FIT:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    iput-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->scaleType:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->mirrored:Z

    .line 79
    return-void
.end method

.method static getDisplayedSize(Lcom/google/android/apps/unveil/env/Size;Lcom/google/android/apps/unveil/env/Size;ILcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;)Lcom/google/android/apps/unveil/env/Size;
    .locals 6
    .parameter "maxViewSize"
    .parameter "previewSize"
    .parameter "rotation"
    .parameter "scaleType"

    .prologue
    .line 115
    sget-object v4, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;->FILL:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    if-ne p3, v4, :cond_0

    .line 135
    .end local p0
    :goto_0
    return-object p0

    .line 119
    .restart local p0
    :cond_0
    const/16 v4, 0x5a

    if-eq p2, v4, :cond_1

    const/16 v4, 0x10e

    if-ne p2, v4, :cond_2

    :cond_1
    iget v4, p1, Lcom/google/android/apps/unveil/env/Size;->height:I

    int-to-float v4, v4

    iget v5, p1, Lcom/google/android/apps/unveil/env/Size;->width:I

    int-to-float v5, v5

    div-float v0, v4, v5

    .line 123
    .local v0, frameAspectRatio:F
    :goto_1
    iget v4, p0, Lcom/google/android/apps/unveil/env/Size;->width:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/apps/unveil/env/Size;->height:I

    int-to-float v5, v5

    div-float v3, v4, v5

    .line 127
    .local v3, viewAspectRatio:F
    cmpl-float v4, v0, v3

    if-lez v4, :cond_3

    .line 128
    iget v2, p0, Lcom/google/android/apps/unveil/env/Size;->width:I

    .line 129
    .local v2, newWidth:I
    iget v4, p0, Lcom/google/android/apps/unveil/env/Size;->width:I

    int-to-float v4, v4

    div-float/2addr v4, v0

    float-to-int v1, v4

    .line 135
    .local v1, newHeight:I
    :goto_2
    new-instance p0, Lcom/google/android/apps/unveil/env/Size;

    .end local p0
    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    goto :goto_0

    .line 119
    .end local v0           #frameAspectRatio:F
    .end local v1           #newHeight:I
    .end local v2           #newWidth:I
    .end local v3           #viewAspectRatio:F
    .restart local p0
    :cond_2
    iget v4, p1, Lcom/google/android/apps/unveil/env/Size;->width:I

    int-to-float v4, v4

    iget v5, p1, Lcom/google/android/apps/unveil/env/Size;->height:I

    int-to-float v5, v5

    div-float v0, v4, v5

    goto :goto_1

    .line 131
    .restart local v0       #frameAspectRatio:F
    .restart local v3       #viewAspectRatio:F
    :cond_3
    iget v4, p0, Lcom/google/android/apps/unveil/env/Size;->height:I

    int-to-float v4, v4

    mul-float/2addr v4, v0

    float-to-int v2, v4

    .line 132
    .restart local v2       #newWidth:I
    iget v1, p0, Lcom/google/android/apps/unveil/env/Size;->height:I

    .restart local v1       #newHeight:I
    goto :goto_2
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .parameter "newConfig"

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/unveil/sensors/CameraManager;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 142
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 13
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 147
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v9, "onLayout: %b, %d, %d, %d, %d"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    if-nez p1, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v8}, Lcom/google/android/apps/unveil/sensors/CameraManager;->isPreviewing()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 151
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v9, "Already previewing or no layout needed, but recursing anyway."

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    :cond_0
    new-instance v5, Lcom/google/android/apps/unveil/env/Size;

    sub-int v8, p4, p2

    sub-int v9, p5, p3

    invoke-direct {v5, v8, v9}, Lcom/google/android/apps/unveil/env/Size;-><init>(II)V

    .line 156
    .local v5, fullViewSize:Lcom/google/android/apps/unveil/env/Size;
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v9, "Full view size: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/unveil/env/UnveilLogger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    const/4 v7, 0x0

    .line 158
    .local v7, previewSize:Lcom/google/android/apps/unveil/env/Size;
    iget-object v9, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    monitor-enter v9

    .line 161
    :try_start_0
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    invoke-virtual {v8, v5}, Lcom/google/android/apps/unveil/sensors/CameraManager;->setFullScreenDisplaySize(Lcom/google/android/apps/unveil/env/Size;)V

    .line 162
    iget v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->rotation:I

    const/16 v10, 0x5a

    if-eq v8, v10, :cond_1

    iget v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->rotation:I

    const/16 v10, 0x10e

    if-ne v8, v10, :cond_3

    .line 163
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    iget v10, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    iget v11, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    invoke-virtual {v8, v10, v11}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getOptimalPreviewSize(II)Lcom/google/android/apps/unveil/env/Size;

    move-result-object v7

    .line 167
    :goto_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    if-nez v7, :cond_4

    .line 170
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v9, "Preview size was null!"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/unveil/env/UnveilLogger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 227
    :cond_2
    :goto_1
    return-void

    .line 165
    :cond_3
    :try_start_1
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    iget v10, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    iget v11, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    invoke-virtual {v8, v10, v11}, Lcom/google/android/apps/unveil/sensors/CameraManager;->getOptimalPreviewSize(II)Lcom/google/android/apps/unveil/env/Size;

    move-result-object v7

    goto :goto_0

    .line 167
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 175
    :cond_4
    iget v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->rotation:I

    iget-object v9, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->scaleType:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    invoke-static {v5, v7, v8, v9}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->getDisplayedSize(Lcom/google/android/apps/unveil/env/Size;Lcom/google/android/apps/unveil/env/Size;ILcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;)Lcom/google/android/apps/unveil/env/Size;

    move-result-object v3

    .line 176
    .local v3, embeddedViewSize:Lcom/google/android/apps/unveil/env/Size;
    iget v8, v5, Lcom/google/android/apps/unveil/env/Size;->width:I

    iget v9, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    sub-int/2addr v8, v9

    div-int/lit8 v0, v8, 0x2

    .line 177
    .local v0, bufferX:I
    iget v8, v5, Lcom/google/android/apps/unveil/env/Size;->height:I

    iget v9, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    sub-int/2addr v8, v9

    div-int/lit8 v1, v8, 0x2

    .line 179
    .local v1, bufferY:I
    iget v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->rotation:I

    invoke-static {v7, v3, v8}, Lcom/google/android/apps/unveil/env/ImageUtils;->getTransformationMatrix(Lcom/google/android/apps/unveil/env/Size;Lcom/google/android/apps/unveil/env/Size;I)Landroid/graphics/Matrix;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->frameToCanvas:Landroid/graphics/Matrix;

    .line 181
    iget-boolean v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->mirrored:Z

    if-eqz v8, :cond_5

    .line 183
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 184
    .local v4, flip:Landroid/graphics/Matrix;
    const/high16 v8, -0x4080

    const/high16 v9, 0x3f80

    invoke-virtual {v4, v8, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 185
    iget v8, v7, Lcom/google/android/apps/unveil/env/Size;->width:I

    int-to-float v8, v8

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 186
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->frameToCanvas:Landroid/graphics/Matrix;

    invoke-virtual {v8, v4}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 189
    .end local v4           #flip:Landroid/graphics/Matrix;
    :cond_5
    sget-object v8, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$1;->$SwitchMap$com$google$android$apps$unveil$ui$CameraWrappingLayout$Alignment:[I

    iget-object v9, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 197
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v9, "Error: unsupported alignment %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v12}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;->name()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    :pswitch_0
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->frameToCanvas:Landroid/graphics/Matrix;

    int-to-float v9, v0

    int-to-float v10, v1

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 204
    :goto_2
    const/4 v6, 0x0

    .local v6, i:I
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->getChildCount()I

    move-result v8

    if-ge v6, v8, :cond_6

    .line 205
    invoke-virtual {p0, v6}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 206
    .local v2, child:Landroid/view/View;
    sget-object v8, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$1;->$SwitchMap$com$google$android$apps$unveil$ui$CameraWrappingLayout$Alignment:[I

    iget-object v9, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v9}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    .line 215
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->logger:Lcom/google/android/apps/unveil/env/UnveilLogger;

    const-string v9, "Error: unsupported alignment %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v12}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;->name()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/unveil/env/UnveilLogger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    :pswitch_1
    iget v8, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    add-int/2addr v8, v0

    iget v9, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    add-int/2addr v9, v1

    invoke-virtual {v2, v0, v1, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 204
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 191
    .end local v2           #child:Landroid/view/View;
    .end local v6           #i:I
    :pswitch_2
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->frameToCanvas:Landroid/graphics/Matrix;

    int-to-float v9, v0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2

    .line 194
    :pswitch_3
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->frameToCanvas:Landroid/graphics/Matrix;

    int-to-float v9, v0

    mul-int/lit8 v10, v1, 0x2

    int-to-float v10, v10

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2

    .line 208
    .restart local v2       #child:Landroid/view/View;
    .restart local v6       #i:I
    :pswitch_4
    const/4 v8, 0x0

    iget v9, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    add-int/2addr v9, v0

    iget v10, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    invoke-virtual {v2, v0, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 211
    :pswitch_5
    mul-int/lit8 v8, v1, 0x2

    iget v9, v3, Lcom/google/android/apps/unveil/env/Size;->width:I

    add-int/2addr v9, v0

    mul-int/lit8 v10, v1, 0x2

    iget v11, v3, Lcom/google/android/apps/unveil/env/Size;->height:I

    add-int/2addr v10, v11

    invoke-virtual {v2, v0, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 224
    .end local v2           #child:Landroid/view/View;
    :cond_6
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraLayoutHandler:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;

    if-eqz v8, :cond_2

    .line 225
    iget-object v8, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraLayoutHandler:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;

    iget-object v9, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->frameToCanvas:Landroid/graphics/Matrix;

    invoke-interface {v8, v3, v9}, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;->onCameraLayoutFinished(Lcom/google/android/apps/unveil/env/Size;Landroid/graphics/Matrix;)V

    goto/16 :goto_1

    .line 189
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch

    .line 206
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method

.method public setAlignment(Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;)V
    .locals 0
    .parameter "alignment"

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->alignment:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$Alignment;

    .line 107
    return-void
.end method

.method public setCameraLayoutHandler(Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;)V
    .locals 0
    .parameter "cameraLayoutHandler"

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraLayoutHandler:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;

    .line 95
    return-void
.end method

.method public setCameraManager(Lcom/google/android/apps/unveil/sensors/CameraManager;)V
    .locals 0
    .parameter "manager"

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->cameraManager:Lcom/google/android/apps/unveil/sensors/CameraManager;

    .line 91
    return-void
.end method

.method public setMirrored(Z)V
    .locals 0
    .parameter "mirrored"

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->mirrored:Z

    .line 103
    return-void
.end method

.method public setRotation(I)V
    .locals 0
    .parameter "rotation"

    .prologue
    .line 98
    iput p1, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->rotation:I

    .line 99
    return-void
.end method

.method public setScaleType(Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;)V
    .locals 0
    .parameter "scaleType"

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/unveil/ui/CameraWrappingLayout;->scaleType:Lcom/google/android/apps/unveil/ui/CameraWrappingLayout$ScaleType;

    .line 111
    return-void
.end method
