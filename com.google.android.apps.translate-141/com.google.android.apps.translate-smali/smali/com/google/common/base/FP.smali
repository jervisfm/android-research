.class public final Lcom/google/common/base/FP;
.super Ljava/lang/Object;
.source "FP.java"


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fingerprint(Ljava/lang/String;)J
    .locals 2
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 26
    sget-object v0, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/FP;->fingerprint([B)J

    move-result-wide v0

    return-wide v0
.end method

.method static fingerprint([B)J
    .locals 4
    .parameter "value"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 32
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/google/common/base/FP;->hash32([BI)I

    move-result v0

    .line 33
    .local v0, hi:I
    const v2, 0x18eb8

    invoke-static {p0, v2}, Lcom/google/common/base/FP;->hash32([BI)I

    move-result v1

    .line 34
    .local v1, lo:I
    invoke-static {v0, v1}, Lcom/google/common/base/Hash;->combineFingerprints(II)J

    move-result-wide v2

    return-wide v2
.end method

.method static hash32([BI)I
    .locals 7
    .parameter "value"
    .parameter "seed"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 39
    const/4 v5, 0x0

    .line 40
    .local v5, offset:I
    array-length v4, p0

    .line 41
    .local v4, length:I
    const v0, -0x61c88647

    .line 42
    .local v0, a:I
    const v1, -0x61c88647

    .line 43
    .local v1, b:I
    move v2, p1

    .line 46
    .local v2, c:I
    move v3, v4

    .local v3, keylen:I
    :goto_0
    const/16 v6, 0xc

    if-lt v3, v6, :cond_0

    .line 47
    invoke-static {p0, v5}, Lcom/google/common/base/FP;->word32At([BI)I

    move-result v6

    add-int/2addr v0, v6

    .line 48
    add-int/lit8 v6, v5, 0x4

    invoke-static {p0, v6}, Lcom/google/common/base/FP;->word32At([BI)I

    move-result v6

    add-int/2addr v1, v6

    .line 49
    add-int/lit8 v6, v5, 0x8

    invoke-static {p0, v6}, Lcom/google/common/base/FP;->word32At([BI)I

    move-result v6

    add-int/2addr v2, v6

    .line 52
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    ushr-int/lit8 v6, v2, 0xd

    xor-int/2addr v0, v6

    .line 53
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v6, v0, 0x8

    xor-int/2addr v1, v6

    .line 54
    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    ushr-int/lit8 v6, v1, 0xd

    xor-int/2addr v2, v6

    .line 55
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    ushr-int/lit8 v6, v2, 0xc

    xor-int/2addr v0, v6

    .line 56
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v6, v0, 0x10

    xor-int/2addr v1, v6

    .line 57
    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    ushr-int/lit8 v6, v1, 0x5

    xor-int/2addr v2, v6

    .line 58
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    ushr-int/lit8 v6, v2, 0x3

    xor-int/2addr v0, v6

    .line 59
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v6, v0, 0xa

    xor-int/2addr v1, v6

    .line 60
    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    ushr-int/lit8 v6, v1, 0xf

    xor-int/2addr v2, v6

    .line 46
    add-int/lit8 v3, v3, -0xc

    add-int/lit8 v5, v5, 0xc

    goto :goto_0

    .line 63
    :cond_0
    add-int/2addr v2, v4

    .line 64
    packed-switch v3, :pswitch_data_0

    .line 94
    :goto_1
    invoke-static {v0, v1, v2}, Lcom/google/common/base/Hash;->mix32(III)I

    move-result v6

    return v6

    .line 66
    :pswitch_0
    add-int/lit8 v6, v5, 0xa

    aget-byte v6, p0, v6

    shl-int/lit8 v6, v6, 0x18

    add-int/2addr v2, v6

    .line 68
    :pswitch_1
    add-int/lit8 v6, v5, 0x9

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    add-int/2addr v2, v6

    .line 70
    :pswitch_2
    add-int/lit8 v6, v5, 0x8

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    add-int/2addr v2, v6

    .line 73
    :pswitch_3
    add-int/lit8 v6, v5, 0x4

    invoke-static {p0, v6}, Lcom/google/common/base/FP;->word32At([BI)I

    move-result v6

    add-int/2addr v1, v6

    .line 74
    invoke-static {p0, v5}, Lcom/google/common/base/FP;->word32At([BI)I

    move-result v6

    add-int/2addr v0, v6

    .line 75
    goto :goto_1

    .line 77
    :pswitch_4
    add-int/lit8 v6, v5, 0x6

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    add-int/2addr v1, v6

    .line 79
    :pswitch_5
    add-int/lit8 v6, v5, 0x5

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    add-int/2addr v1, v6

    .line 81
    :pswitch_6
    add-int/lit8 v6, v5, 0x4

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    add-int/2addr v1, v6

    .line 83
    :pswitch_7
    invoke-static {p0, v5}, Lcom/google/common/base/FP;->word32At([BI)I

    move-result v6

    add-int/2addr v0, v6

    .line 84
    goto :goto_1

    .line 86
    :pswitch_8
    add-int/lit8 v6, v5, 0x2

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    add-int/2addr v0, v6

    .line 88
    :pswitch_9
    add-int/lit8 v6, v5, 0x1

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    add-int/2addr v0, v6

    .line 90
    :pswitch_a
    add-int/lit8 v6, v5, 0x0

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    add-int/2addr v0, v6

    goto :goto_1

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static word32At([BI)I
    .locals 2
    .parameter "bytes"
    .parameter "offset"

    .prologue
    .line 101
    add-int/lit8 v0, p1, 0x0

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method
