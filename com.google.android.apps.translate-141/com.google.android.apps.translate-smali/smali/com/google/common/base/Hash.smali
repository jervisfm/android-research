.class public final Lcom/google/common/base/Hash;
.super Ljava/lang/Object;
.source "Hash.java"


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation


# static fields
.field private static final CONSTANT32:I = -0x61c88647

.field static final CONSTANT64:J = -0x1f73e299748a907eL

.field private static final SEED32:I = 0x12b9b0a1

.field static final SEED64:J = 0x2b992ddfa23249d6L


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static combineFingerprints(II)J
    .locals 6
    .parameter "hi"
    .parameter "lo"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 686
    if-nez p0, :cond_1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 688
    :cond_0
    const v0, 0x130f9bef

    xor-int/2addr p0, v0

    .line 689
    const v0, -0x6b5f56d8

    xor-int/2addr p1, v0

    .line 691
    :cond_1
    int-to-long v0, p0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p1

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public static fingerprint(Ljava/lang/String;)J
    .locals 3
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 255
    sget-object v1, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 256
    .local v0, temp:[B
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Lcom/google/common/base/Hash;->fingerprint([BII)J

    move-result-wide v1

    return-wide v1
.end method

.method public static fingerprint([B)J
    .locals 2
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 661
    if-nez p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/google/common/base/Hash;->fingerprint([BII)J

    move-result-wide v0

    return-wide v0

    :cond_0
    array-length v0, p0

    goto :goto_0
.end method

.method static fingerprint([BII)J
    .locals 4
    .parameter "value"
    .parameter "offset"
    .parameter "length"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 679
    const/4 v2, 0x0

    invoke-static {p0, p1, p2, v2}, Lcom/google/common/base/Hash;->hash32([BIII)I

    move-result v0

    .line 680
    .local v0, hi:I
    const v2, 0x18eb8

    invoke-static {p0, p1, p2, v2}, Lcom/google/common/base/Hash;->hash32([BIII)I

    move-result v1

    .line 681
    .local v1, lo:I
    invoke-static {v0, v1}, Lcom/google/common/base/Hash;->combineFingerprints(II)J

    move-result-wide v2

    return-wide v2
.end method

.method public static fprint96(Ljava/lang/String;)[B
    .locals 1
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 277
    sget-object v0, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Hash;->fprint96([B)[B

    move-result-object v0

    return-object v0
.end method

.method public static fprint96([B)[B
    .locals 5
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 295
    :try_start_0
    const-string v3, "SHA-1"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 299
    .local v2, md:Ljava/security/MessageDigest;
    invoke-virtual {v2, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 300
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 301
    .local v1, hash:[B
    const/4 v3, 0x0

    const/16 v4, 0xc

    invoke-static {v1, v3, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    return-object v3

    .line 296
    .end local v1           #hash:[B
    .end local v2           #md:Ljava/security/MessageDigest;
    :catch_0
    move-exception v0

    .line 297
    .local v0, e:Ljava/security/NoSuchAlgorithmException;
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "SHA-1 should be available in the JVM."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static fprint96AsKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/16 v4, 0xc

    .line 340
    sget-object v3, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Hash;->fprint96([B)[B

    move-result-object v1

    .line 341
    .local v1, fp96:[B
    new-array v0, v4, [C

    .line 342
    .local v0, chars:[C
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 343
    aget-byte v3, v1, v2

    and-int/lit16 v3, v3, 0xff

    int-to-char v3, v3

    aput-char v3, v0, v2

    .line 342
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 345
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    return-object v3
.end method

.method public static fprint96AsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .parameter "str"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const-wide v10, 0xffffffffL

    .line 319
    invoke-static {p0}, Lcom/google/common/base/Hash;->fprint96(Ljava/lang/String;)[B

    move-result-object v7

    .line 320
    .local v7, value:[B
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v6

    .line 321
    .local v6, ib:Ljava/nio/IntBuffer;
    invoke-virtual {v6}, Ljava/nio/IntBuffer;->get()I

    move-result v8

    int-to-long v8, v8

    and-long v0, v8, v10

    .line 322
    .local v0, group0:J
    invoke-virtual {v6}, Ljava/nio/IntBuffer;->get()I

    move-result v8

    int-to-long v8, v8

    and-long v2, v8, v10

    .line 323
    .local v2, group1:J
    invoke-virtual {v6}, Ljava/nio/IntBuffer;->get()I

    move-result v8

    int-to-long v8, v8

    and-long v4, v8, v10

    .line 324
    .local v4, group2:J
    const-string v8, "%08X_%08X_%08X"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public static fprint96KeyModShardFromKey(Ljava/lang/String;I)I
    .locals 11
    .parameter "key"
    .parameter "numShards"

    .prologue
    const/16 v10, 0x20

    const-wide v8, 0xffffffffL

    .line 376
    if-lez p1, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 378
    invoke-static {p0}, Lcom/google/common/base/Hash;->keyToFprint96(Ljava/lang/String;)[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v3

    .line 379
    .local v3, ib:Ljava/nio/IntBuffer;
    int-to-long v6, p1

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    .line 385
    .local v4, mod:Ljava/math/BigInteger;
    invoke-virtual {v3}, Ljava/nio/IntBuffer;->get()I

    move-result v6

    int-to-long v6, v6

    and-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    .line 386
    .local v0, group0:Ljava/math/BigInteger;
    invoke-virtual {v3}, Ljava/nio/IntBuffer;->get()I

    move-result v6

    int-to-long v6, v6

    and-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    .line 387
    .local v1, group1:Ljava/math/BigInteger;
    invoke-virtual {v3}, Ljava/nio/IntBuffer;->get()I

    move-result v6

    int-to-long v6, v6

    and-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    .line 388
    .local v2, group2:Ljava/math/BigInteger;
    invoke-virtual {v0, v10}, Ljava/math/BigInteger;->shiftLeft(I)Ljava/math/BigInteger;

    move-result-object v0

    .line 389
    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->or(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    .line 390
    .local v5, remainder:Ljava/math/BigInteger;
    invoke-virtual {v5, v10}, Ljava/math/BigInteger;->shiftLeft(I)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/math/BigInteger;->or(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigInteger;->intValue()I

    move-result v6

    return v6

    .line 376
    .end local v0           #group0:Ljava/math/BigInteger;
    .end local v1           #group1:Ljava/math/BigInteger;
    .end local v2           #group2:Ljava/math/BigInteger;
    .end local v3           #ib:Ljava/nio/IntBuffer;
    .end local v4           #mod:Ljava/math/BigInteger;
    .end local v5           #remainder:Ljava/math/BigInteger;
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static hash32(I)I
    .locals 2
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 51
    const v0, -0x61c88647

    const v1, 0x12b9b0a1

    invoke-static {p0, v0, v1}, Lcom/google/common/base/Hash;->mix32(III)I

    move-result v0

    return v0
.end method

.method public static hash32(II)I
    .locals 1
    .parameter "value"
    .parameter "seed"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 64
    const v0, -0x61c88647

    invoke-static {p0, v0, p1}, Lcom/google/common/base/Hash;->mix32(III)I

    move-result v0

    return v0
.end method

.method public static hash32(J)I
    .locals 6
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76
    const-wide v2, -0x1f73e299748a907eL

    const-wide v4, 0x2b992ddfa23249d6L

    move-wide v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/common/base/Hash;->mix64(JJJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static hash32(JJ)I
    .locals 6
    .parameter "value"
    .parameter "seed"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 90
    const-wide v2, -0x1f73e299748a907eL

    move-wide v0, p0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/common/base/Hash;->mix64(JJJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static hash32(Ljava/lang/String;)I
    .locals 1
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 140
    const v0, 0x12b9b0a1

    invoke-static {p0, v0}, Lcom/google/common/base/Hash;->hash32(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static hash32(Ljava/lang/String;I)I
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "seed"
    .end parameter
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 170
    if-nez p0, :cond_0

    .line 171
    const/4 v0, 0x0

    invoke-static {v0, v1, v1, p1}, Lcom/google/common/base/Hash;->hash32([BIII)I

    move-result v0

    .line 173
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/base/Hash;->hash32([BI)I

    move-result v0

    goto :goto_0
.end method

.method public static hash32(Ljava/lang/String;Ljava/nio/charset/Charset;)I
    .locals 1
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "charset"
    .end parameter
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 128
    const v0, 0x12b9b0a1

    invoke-static {p0, p1, v0}, Lcom/google/common/base/Hash;->hash32(Ljava/lang/String;Ljava/nio/charset/Charset;I)I

    move-result v0

    return v0
.end method

.method static hash32(Ljava/lang/String;Ljava/nio/charset/Charset;I)I
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "charset"
    .end parameter
    .parameter "seed"
    .end parameter
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    if-nez p0, :cond_0

    .line 155
    const/4 v0, 0x0

    invoke-static {v0, v1, v1, p2}, Lcom/google/common/base/Hash;->hash32([BIII)I

    move-result v0

    .line 157
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/base/Hash;->hash32([BI)I

    move-result v0

    goto :goto_0
.end method

.method public static hash32([B)I
    .locals 2
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 402
    if-nez p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/google/common/base/Hash;->hash32([BII)I

    move-result v0

    return v0

    :cond_0
    array-length v0, p0

    goto :goto_0
.end method

.method static hash32([BI)I
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "seed"
    .end parameter
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 414
    if-nez p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {p0, v1, v0, p1}, Lcom/google/common/base/Hash;->hash32([BIII)I

    move-result v0

    return v0

    :cond_0
    array-length v0, p0

    goto :goto_0
.end method

.method static hash32([BII)I
    .locals 1
    .parameter "value"
    .parameter "offset"
    .parameter "length"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 427
    const v0, 0x12b9b0a1

    invoke-static {p0, p1, p2, v0}, Lcom/google/common/base/Hash;->hash32([BIII)I

    move-result v0

    return v0
.end method

.method private static hash32([BIII)I
    .locals 5
    .parameter "value"
    .parameter "offset"
    .parameter "length"
    .parameter "seed"

    .prologue
    .line 440
    const v0, -0x61c88647

    .line 441
    .local v0, a:I
    move v1, v0

    .line 442
    .local v1, b:I
    move v2, p3

    .line 445
    .local v2, c:I
    move v3, p2

    .local v3, keylen:I
    :goto_0
    const/16 v4, 0xc

    if-lt v3, v4, :cond_0

    .line 446
    invoke-static {p0, p1}, Lcom/google/common/base/Hash;->word32At([BI)I

    move-result v4

    add-int/2addr v0, v4

    .line 447
    add-int/lit8 v4, p1, 0x4

    invoke-static {p0, v4}, Lcom/google/common/base/Hash;->word32At([BI)I

    move-result v4

    add-int/2addr v1, v4

    .line 448
    add-int/lit8 v4, p1, 0x8

    invoke-static {p0, v4}, Lcom/google/common/base/Hash;->word32At([BI)I

    move-result v4

    add-int/2addr v2, v4

    .line 451
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    ushr-int/lit8 v4, v2, 0xd

    xor-int/2addr v0, v4

    .line 452
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v4, v0, 0x8

    xor-int/2addr v1, v4

    .line 453
    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    ushr-int/lit8 v4, v1, 0xd

    xor-int/2addr v2, v4

    .line 454
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    ushr-int/lit8 v4, v2, 0xc

    xor-int/2addr v0, v4

    .line 455
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v4, v0, 0x10

    xor-int/2addr v1, v4

    .line 456
    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    ushr-int/lit8 v4, v1, 0x5

    xor-int/2addr v2, v4

    .line 457
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    ushr-int/lit8 v4, v2, 0x3

    xor-int/2addr v0, v4

    .line 458
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v4, v0, 0xa

    xor-int/2addr v1, v4

    .line 459
    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    ushr-int/lit8 v4, v1, 0xf

    xor-int/2addr v2, v4

    .line 445
    add-int/lit8 v3, v3, -0xc

    add-int/lit8 p1, p1, 0xc

    goto :goto_0

    .line 463
    :cond_0
    add-int/2addr v2, p2

    .line 464
    packed-switch v3, :pswitch_data_0

    .line 493
    :goto_1
    invoke-static {v0, v1, v2}, Lcom/google/common/base/Hash;->mix32(III)I

    move-result v4

    return v4

    .line 466
    :pswitch_0
    add-int/lit8 v4, p1, 0xa

    aget-byte v4, p0, v4

    shl-int/lit8 v4, v4, 0x18

    add-int/2addr v2, v4

    .line 468
    :pswitch_1
    add-int/lit8 v4, p1, 0x9

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    add-int/2addr v2, v4

    .line 470
    :pswitch_2
    add-int/lit8 v4, p1, 0x8

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    add-int/2addr v2, v4

    .line 473
    :pswitch_3
    add-int/lit8 v4, p1, 0x4

    invoke-static {p0, v4}, Lcom/google/common/base/Hash;->word32At([BI)I

    move-result v4

    add-int/2addr v1, v4

    .line 474
    invoke-static {p0, p1}, Lcom/google/common/base/Hash;->word32At([BI)I

    move-result v4

    add-int/2addr v0, v4

    .line 475
    goto :goto_1

    .line 477
    :pswitch_4
    add-int/lit8 v4, p1, 0x6

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    add-int/2addr v1, v4

    .line 479
    :pswitch_5
    add-int/lit8 v4, p1, 0x5

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    add-int/2addr v1, v4

    .line 481
    :pswitch_6
    add-int/lit8 v4, p1, 0x4

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v1, v4

    .line 483
    :pswitch_7
    invoke-static {p0, p1}, Lcom/google/common/base/Hash;->word32At([BI)I

    move-result v4

    add-int/2addr v0, v4

    .line 484
    goto :goto_1

    .line 486
    :pswitch_8
    add-int/lit8 v4, p1, 0x2

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    add-int/2addr v0, v4

    .line 488
    :pswitch_9
    add-int/lit8 v4, p1, 0x1

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    add-int/2addr v0, v4

    .line 490
    :pswitch_a
    add-int/lit8 v4, p1, 0x0

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v0, v4

    goto :goto_1

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static hash64(J)J
    .locals 6
    .parameter "value"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 102
    const-wide v2, -0x1f73e299748a907eL

    const-wide v4, 0x2b992ddfa23249d6L

    move-wide v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/common/base/Hash;->mix64(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hash64(JJ)J
    .locals 6
    .parameter "value"
    .parameter "seed"
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 115
    const-wide v2, -0x1f73e299748a907eL

    move-wide v0, p0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/common/base/Hash;->mix64(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hash64(Ljava/lang/String;)J
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 198
    const-wide v0, 0x2b992ddfa23249d6L

    invoke-static {p0, v0, v1}, Lcom/google/common/base/Hash;->hash64(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hash64(Ljava/lang/String;J)J
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "seed"
    .end parameter
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 228
    if-nez p0, :cond_0

    .line 229
    const/4 v0, 0x0

    invoke-static {v0, v1, v1, p1, p2}, Lcom/google/common/base/Hash;->hash64([BIIJ)J

    move-result-wide v0

    .line 231
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/common/base/Hash;->hash64([BJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static hash64(Ljava/lang/String;Ljava/nio/charset/Charset;)J
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "charset"
    .end parameter
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 186
    const-wide v0, 0x2b992ddfa23249d6L

    invoke-static {p0, p1, v0, v1}, Lcom/google/common/base/Hash;->hash64(Ljava/lang/String;Ljava/nio/charset/Charset;J)J

    move-result-wide v0

    return-wide v0
.end method

.method static hash64(Ljava/lang/String;Ljava/nio/charset/Charset;J)J
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "charset"
    .end parameter
    .parameter "seed"
    .end parameter
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 211
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    if-nez p0, :cond_0

    .line 213
    const/4 v0, 0x0

    invoke-static {v0, v1, v1, p2, p3}, Lcom/google/common/base/Hash;->hash64([BIIJ)J

    move-result-wide v0

    .line 215
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/google/common/base/Hash;->hash64([BJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static hash64([B)J
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .annotation runtime Lcom/google/common/annotations/Strongly;
        contact = "java-core-libraries-team"
        date = "2012-09-01"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 505
    if-nez p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/google/common/base/Hash;->hash64([BII)J

    move-result-wide v0

    return-wide v0

    :cond_0
    array-length v0, p0

    goto :goto_0
.end method

.method static hash64([BII)J
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "offset"
    .end parameter
    .parameter "length"
    .end parameter
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 517
    const-wide v0, 0x2b992ddfa23249d6L

    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/common/base/Hash;->hash64([BIIJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static hash64([BIIJ)J
    .locals 11
    .parameter "value"
    .parameter "offset"
    .parameter "length"
    .parameter "seed"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 543
    const-wide v0, -0x1f73e299748a907eL

    .line 544
    .local v0, a:J
    move-wide v2, v0

    .line 545
    .local v2, b:J
    move-wide v4, p3

    .line 548
    .local v4, c:J
    move v6, p2

    .local v6, keylen:I
    :goto_0
    const/16 v7, 0x18

    if-lt v6, v7, :cond_0

    .line 549
    invoke-static {p0, p1}, Lcom/google/common/base/Hash;->word64At([BI)J

    move-result-wide v7

    add-long/2addr v0, v7

    .line 550
    add-int/lit8 v7, p1, 0x8

    invoke-static {p0, v7}, Lcom/google/common/base/Hash;->word64At([BI)J

    move-result-wide v7

    add-long/2addr v2, v7

    .line 551
    add-int/lit8 v7, p1, 0x10

    invoke-static {p0, v7}, Lcom/google/common/base/Hash;->word64At([BI)J

    move-result-wide v7

    add-long/2addr v4, v7

    .line 554
    sub-long/2addr v0, v2

    sub-long/2addr v0, v4

    const/16 v7, 0x2b

    ushr-long v7, v4, v7

    xor-long/2addr v0, v7

    .line 555
    sub-long/2addr v2, v4

    sub-long/2addr v2, v0

    const/16 v7, 0x9

    shl-long v7, v0, v7

    xor-long/2addr v2, v7

    .line 556
    sub-long/2addr v4, v0

    sub-long/2addr v4, v2

    const/16 v7, 0x8

    ushr-long v7, v2, v7

    xor-long/2addr v4, v7

    .line 557
    sub-long/2addr v0, v2

    sub-long/2addr v0, v4

    const/16 v7, 0x26

    ushr-long v7, v4, v7

    xor-long/2addr v0, v7

    .line 558
    sub-long/2addr v2, v4

    sub-long/2addr v2, v0

    const/16 v7, 0x17

    shl-long v7, v0, v7

    xor-long/2addr v2, v7

    .line 559
    sub-long/2addr v4, v0

    sub-long/2addr v4, v2

    const/4 v7, 0x5

    ushr-long v7, v2, v7

    xor-long/2addr v4, v7

    .line 560
    sub-long/2addr v0, v2

    sub-long/2addr v0, v4

    const/16 v7, 0x23

    ushr-long v7, v4, v7

    xor-long/2addr v0, v7

    .line 561
    sub-long/2addr v2, v4

    sub-long/2addr v2, v0

    const/16 v7, 0x31

    shl-long v7, v0, v7

    xor-long/2addr v2, v7

    .line 562
    sub-long/2addr v4, v0

    sub-long/2addr v4, v2

    const/16 v7, 0xb

    ushr-long v7, v2, v7

    xor-long/2addr v4, v7

    .line 563
    sub-long/2addr v0, v2

    sub-long/2addr v0, v4

    const/16 v7, 0xc

    ushr-long v7, v4, v7

    xor-long/2addr v0, v7

    .line 564
    sub-long/2addr v2, v4

    sub-long/2addr v2, v0

    const/16 v7, 0x12

    shl-long v7, v0, v7

    xor-long/2addr v2, v7

    .line 565
    sub-long/2addr v4, v0

    sub-long/2addr v4, v2

    const/16 v7, 0x16

    ushr-long v7, v2, v7

    xor-long/2addr v4, v7

    .line 548
    add-int/lit8 v6, v6, -0x18

    add-int/lit8 p1, p1, 0x18

    goto :goto_0

    .line 568
    :cond_0
    int-to-long v7, p2

    add-long/2addr v4, v7

    .line 569
    packed-switch v6, :pswitch_data_0

    .line 622
    :goto_1
    invoke-static/range {v0 .. v5}, Lcom/google/common/base/Hash;->mix64(JJJ)J

    move-result-wide v7

    return-wide v7

    .line 571
    :pswitch_0
    add-int/lit8 v7, p1, 0x16

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const/16 v9, 0x38

    shl-long/2addr v7, v9

    add-long/2addr v4, v7

    .line 573
    :pswitch_1
    add-int/lit8 v7, p1, 0x15

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x30

    shl-long/2addr v7, v9

    add-long/2addr v4, v7

    .line 575
    :pswitch_2
    add-int/lit8 v7, p1, 0x14

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x28

    shl-long/2addr v7, v9

    add-long/2addr v4, v7

    .line 577
    :pswitch_3
    add-int/lit8 v7, p1, 0x13

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x20

    shl-long/2addr v7, v9

    add-long/2addr v4, v7

    .line 579
    :pswitch_4
    add-int/lit8 v7, p1, 0x12

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x18

    shl-long/2addr v7, v9

    add-long/2addr v4, v7

    .line 581
    :pswitch_5
    add-int/lit8 v7, p1, 0x11

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x10

    shl-long/2addr v7, v9

    add-long/2addr v4, v7

    .line 583
    :pswitch_6
    add-int/lit8 v7, p1, 0x10

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x8

    shl-long/2addr v7, v9

    add-long/2addr v4, v7

    .line 586
    :pswitch_7
    add-int/lit8 v7, p1, 0x8

    invoke-static {p0, v7}, Lcom/google/common/base/Hash;->word64At([BI)J

    move-result-wide v7

    add-long/2addr v2, v7

    .line 587
    invoke-static {p0, p1}, Lcom/google/common/base/Hash;->word64At([BI)J

    move-result-wide v7

    add-long/2addr v0, v7

    .line 588
    goto :goto_1

    .line 590
    :pswitch_8
    add-int/lit8 v7, p1, 0xe

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x30

    shl-long/2addr v7, v9

    add-long/2addr v2, v7

    .line 592
    :pswitch_9
    add-int/lit8 v7, p1, 0xd

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x28

    shl-long/2addr v7, v9

    add-long/2addr v2, v7

    .line 594
    :pswitch_a
    add-int/lit8 v7, p1, 0xc

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x20

    shl-long/2addr v7, v9

    add-long/2addr v2, v7

    .line 596
    :pswitch_b
    add-int/lit8 v7, p1, 0xb

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x18

    shl-long/2addr v7, v9

    add-long/2addr v2, v7

    .line 598
    :pswitch_c
    add-int/lit8 v7, p1, 0xa

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x10

    shl-long/2addr v7, v9

    add-long/2addr v2, v7

    .line 600
    :pswitch_d
    add-int/lit8 v7, p1, 0x9

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x8

    shl-long/2addr v7, v9

    add-long/2addr v2, v7

    .line 602
    :pswitch_e
    add-int/lit8 v7, p1, 0x8

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    add-long/2addr v2, v7

    .line 604
    :pswitch_f
    invoke-static {p0, p1}, Lcom/google/common/base/Hash;->word64At([BI)J

    move-result-wide v7

    add-long/2addr v0, v7

    .line 605
    goto/16 :goto_1

    .line 607
    :pswitch_10
    add-int/lit8 v7, p1, 0x6

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x30

    shl-long/2addr v7, v9

    add-long/2addr v0, v7

    .line 609
    :pswitch_11
    add-int/lit8 v7, p1, 0x5

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x28

    shl-long/2addr v7, v9

    add-long/2addr v0, v7

    .line 611
    :pswitch_12
    add-int/lit8 v7, p1, 0x4

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x20

    shl-long/2addr v7, v9

    add-long/2addr v0, v7

    .line 613
    :pswitch_13
    add-int/lit8 v7, p1, 0x3

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x18

    shl-long/2addr v7, v9

    add-long/2addr v0, v7

    .line 615
    :pswitch_14
    add-int/lit8 v7, p1, 0x2

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x10

    shl-long/2addr v7, v9

    add-long/2addr v0, v7

    .line 617
    :pswitch_15
    add-int/lit8 v7, p1, 0x1

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    const/16 v9, 0x8

    shl-long/2addr v7, v9

    add-long/2addr v0, v7

    .line 619
    :pswitch_16
    add-int/lit8 v7, p1, 0x0

    aget-byte v7, p0, v7

    int-to-long v7, v7

    const-wide/16 v9, 0xff

    and-long/2addr v7, v9

    add-long/2addr v0, v7

    goto/16 :goto_1

    .line 569
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static hash64([BJ)J
    .locals 2
    .parameter "value"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "seed"
    .end parameter
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 529
    if-nez p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {p0, v1, v0, p1, p2}, Lcom/google/common/base/Hash;->hash64([BIIJ)J

    move-result-wide v0

    return-wide v0

    :cond_0
    array-length v0, p0

    goto :goto_0
.end method

.method static keyToFprint96(Ljava/lang/String;)[B
    .locals 4
    .parameter "key"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/16 v3, 0xc

    .line 358
    new-array v0, v3, [B

    .line 359
    .local v0, fprint96:[B
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 360
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 359
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 362
    :cond_0
    return-object v0
.end method

.method static mix32(III)I
    .locals 1
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 699
    sub-int/2addr p0, p1

    sub-int/2addr p0, p2

    ushr-int/lit8 v0, p2, 0xd

    xor-int/2addr p0, v0

    .line 700
    sub-int/2addr p1, p2

    sub-int/2addr p1, p0

    shl-int/lit8 v0, p0, 0x8

    xor-int/2addr p1, v0

    .line 701
    sub-int/2addr p2, p0

    sub-int/2addr p2, p1

    ushr-int/lit8 v0, p1, 0xd

    xor-int/2addr p2, v0

    .line 702
    sub-int/2addr p0, p1

    sub-int/2addr p0, p2

    ushr-int/lit8 v0, p2, 0xc

    xor-int/2addr p0, v0

    .line 703
    sub-int/2addr p1, p2

    sub-int/2addr p1, p0

    shl-int/lit8 v0, p0, 0x10

    xor-int/2addr p1, v0

    .line 704
    sub-int/2addr p2, p0

    sub-int/2addr p2, p1

    ushr-int/lit8 v0, p1, 0x5

    xor-int/2addr p2, v0

    .line 705
    sub-int/2addr p0, p1

    sub-int/2addr p0, p2

    ushr-int/lit8 v0, p2, 0x3

    xor-int/2addr p0, v0

    .line 706
    sub-int/2addr p1, p2

    sub-int/2addr p1, p0

    shl-int/lit8 v0, p0, 0xa

    xor-int/2addr p1, v0

    .line 707
    sub-int/2addr p2, p0

    sub-int/2addr p2, p1

    ushr-int/lit8 v0, p1, 0xf

    xor-int/2addr p2, v0

    .line 708
    return p2
.end method

.method static mix64(JJJ)J
    .locals 2
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 716
    sub-long/2addr p0, p2

    sub-long/2addr p0, p4

    const/16 v0, 0x2b

    ushr-long v0, p4, v0

    xor-long/2addr p0, v0

    .line 717
    sub-long/2addr p2, p4

    sub-long/2addr p2, p0

    const/16 v0, 0x9

    shl-long v0, p0, v0

    xor-long/2addr p2, v0

    .line 718
    sub-long/2addr p4, p0

    sub-long/2addr p4, p2

    const/16 v0, 0x8

    ushr-long v0, p2, v0

    xor-long/2addr p4, v0

    .line 719
    sub-long/2addr p0, p2

    sub-long/2addr p0, p4

    const/16 v0, 0x26

    ushr-long v0, p4, v0

    xor-long/2addr p0, v0

    .line 720
    sub-long/2addr p2, p4

    sub-long/2addr p2, p0

    const/16 v0, 0x17

    shl-long v0, p0, v0

    xor-long/2addr p2, v0

    .line 721
    sub-long/2addr p4, p0

    sub-long/2addr p4, p2

    const/4 v0, 0x5

    ushr-long v0, p2, v0

    xor-long/2addr p4, v0

    .line 722
    sub-long/2addr p0, p2

    sub-long/2addr p0, p4

    const/16 v0, 0x23

    ushr-long v0, p4, v0

    xor-long/2addr p0, v0

    .line 723
    sub-long/2addr p2, p4

    sub-long/2addr p2, p0

    const/16 v0, 0x31

    shl-long v0, p0, v0

    xor-long/2addr p2, v0

    .line 724
    sub-long/2addr p4, p0

    sub-long/2addr p4, p2

    const/16 v0, 0xb

    ushr-long v0, p2, v0

    xor-long/2addr p4, v0

    .line 725
    sub-long/2addr p0, p2

    sub-long/2addr p0, p4

    const/16 v0, 0xc

    ushr-long v0, p4, v0

    xor-long/2addr p0, v0

    .line 726
    sub-long/2addr p2, p4

    sub-long/2addr p2, p0

    const/16 v0, 0x12

    shl-long v0, p0, v0

    xor-long/2addr p2, v0

    .line 727
    sub-long/2addr p4, p0

    sub-long/2addr p4, p2

    const/16 v0, 0x16

    ushr-long v0, p2, v0

    xor-long/2addr p4, v0

    .line 728
    return-wide p4
.end method

.method private static word32At([BI)I
    .locals 2
    .parameter "bytes"
    .parameter "offset"

    .prologue
    .line 632
    add-int/lit8 v0, p1, 0x0

    aget-byte v0, p0, v0

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x18

    add-int/2addr v0, v1

    return v0
.end method

.method private static word64At([BI)J
    .locals 7
    .parameter "bytes"
    .parameter "offset"

    .prologue
    const-wide/16 v5, 0xff

    .line 639
    add-int/lit8 v0, p1, 0x0

    aget-byte v0, p0, v0

    int-to-long v0, v0

    and-long/2addr v0, v5

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    int-to-long v2, v2

    and-long/2addr v2, v5

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    int-to-long v2, v2

    and-long/2addr v2, v5

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x3

    aget-byte v2, p0, v2

    int-to-long v2, v2

    and-long/2addr v2, v5

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x4

    aget-byte v2, p0, v2

    int-to-long v2, v2

    and-long/2addr v2, v5

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x5

    aget-byte v2, p0, v2

    int-to-long v2, v2

    and-long/2addr v2, v5

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x6

    aget-byte v2, p0, v2

    int-to-long v2, v2

    and-long/2addr v2, v5

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x7

    aget-byte v2, p0, v2

    int-to-long v2, v2

    and-long/2addr v2, v5

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method
