.class public final Lcom/google/common/base/Log2;
.super Ljava/lang/Object;
.source "Log2.java"


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final LOG2_USE_JAVA_LOG_PROP:Ljava/lang/String; = "google.log2UseJavaLog"

.field private static defaultLog:Lcom/google/common/logging/Logger;

.field private static systemErrLog:Lcom/google/common/logging/Logger;

.field public static useJavaLogging:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 71
    sput-object v2, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    .line 72
    sput-object v2, Lcom/google/common/base/Log2;->systemErrLog:Lcom/google/common/logging/Logger;

    .line 77
    const/4 v2, 0x0

    sput-boolean v2, Lcom/google/common/base/Log2;->useJavaLogging:Z

    .line 85
    const-string v2, "google.log2UseJavaLog"

    invoke-static {v2}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/google/common/base/Log2;->useJavaLogging:Z

    .line 86
    sget-boolean v2, Lcom/google/common/base/Log2;->useJavaLogging:Z

    if-eqz v2, :cond_0

    .line 87
    new-instance v2, Lcom/google/common/base/Log2Logger;

    invoke-direct {v2}, Lcom/google/common/base/Log2Logger;-><init>()V

    sput-object v2, Lcom/google/common/base/Log2;->systemErrLog:Lcom/google/common/logging/Logger;

    .line 88
    sget-object v2, Lcom/google/common/base/Log2;->systemErrLog:Lcom/google/common/logging/Logger;

    sput-object v2, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    .line 90
    :cond_0
    sget-object v2, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    if-nez v2, :cond_1

    .line 92
    :try_start_0
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-direct {v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 94
    .local v0, bw:Ljava/io/BufferedWriter;
    new-instance v2, Lcom/google/common/base/LogWriter;

    invoke-direct {v2, v0}, Lcom/google/common/base/LogWriter;-><init>(Ljava/io/Writer;)V

    sput-object v2, Lcom/google/common/base/Log2;->systemErrLog:Lcom/google/common/logging/Logger;

    .line 95
    sget-object v2, Lcom/google/common/base/Log2;->systemErrLog:Lcom/google/common/logging/Logger;

    sput-object v2, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_1
    return-void

    .line 96
    :catch_0
    move-exception v1

    .line 97
    .local v1, t:Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Log2: could not initialize  LogWriter object!"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultLog()Lcom/google/common/logging/Logger;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    return-object v0
.end method

.method public static getExceptionTrace(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 5
    .parameter "t"

    .prologue
    .line 206
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 207
    .local v2, sw:Ljava/io/StringWriter;
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 208
    .local v0, pw:Ljava/io/PrintWriter;
    instance-of v3, p0, Ljava/sql/SQLException;

    if-eqz v3, :cond_1

    move-object v1, p0

    .line 209
    check-cast v1, Ljava/sql/SQLException;

    .line 211
    .local v1, sqlx:Ljava/sql/SQLException;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SQLException: errorCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/sql/SQLException;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sqlState="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/sql/SQLException;->getSQLState()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 213
    invoke-virtual {v1, v0}, Ljava/sql/SQLException;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 215
    invoke-virtual {v1}, Ljava/sql/SQLException;->getNextException()Ljava/sql/SQLException;

    move-result-object v1

    .line 216
    if-nez v1, :cond_0

    .line 224
    .end local v1           #sqlx:Ljava/sql/SQLException;
    :goto_1
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 218
    .restart local v1       #sqlx:Ljava/sql/SQLException;
    :cond_0
    const-string v3, "chained to:"

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 221
    .end local v1           #sqlx:Ljava/sql/SQLException;
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    goto :goto_1
.end method

.method public static getThreshold()I
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0}, Lcom/google/common/logging/Logger;->getThreshold()I

    move-result v0

    return v0
.end method

.method public static logDebug(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 158
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0, p0}, Lcom/google/common/logging/Logger;->logDebug(Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public static logError(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 190
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0, p0}, Lcom/google/common/logging/Logger;->logError(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public static logEvent(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 165
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0, p0}, Lcom/google/common/logging/Logger;->logEvent(Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public static logException(Ljava/lang/Throwable;)V
    .locals 1
    .parameter "t"

    .prologue
    .line 172
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0, p0}, Lcom/google/common/logging/Logger;->logException(Ljava/lang/Throwable;)V

    .line 173
    return-void
.end method

.method public static logException(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1
    .parameter "t"
    .parameter "msg"

    .prologue
    .line 179
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0, p0, p1}, Lcom/google/common/logging/Logger;->logException(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public static logSevereException(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1
    .parameter "t"
    .parameter "msg"

    .prologue
    .line 183
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0, p0, p1}, Lcom/google/common/logging/Logger;->logSevereException(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public static setDefaultLog(Lcom/google/common/logging/Logger;)V
    .locals 3
    .parameter "log"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 114
    const-class v1, Lcom/google/common/base/Log2;

    monitor-enter v1

    .line 115
    :try_start_0
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    sget-object v2, Lcom/google/common/base/Log2;->systemErrLog:Lcom/google/common/logging/Logger;

    if-eq v0, v2, :cond_0

    .line 116
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0}, Lcom/google/common/logging/Logger;->close()V

    .line 118
    :cond_0
    if-eqz p0, :cond_1

    .line 119
    sput-object p0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    .line 122
    :goto_0
    monitor-exit v1

    .line 123
    return-void

    .line 121
    :cond_1
    sget-object v0, Lcom/google/common/base/Log2;->systemErrLog:Lcom/google/common/logging/Logger;

    sput-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    goto :goto_0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setThreadTag(Ljava/lang/String;)V
    .locals 1
    .parameter "s"

    .prologue
    .line 199
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0, p0}, Lcom/google/common/logging/Logger;->setThreadTag(Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method public static setThreshold(I)V
    .locals 1
    .parameter "level"

    .prologue
    .line 144
    sget-object v0, Lcom/google/common/base/Log2;->defaultLog:Lcom/google/common/logging/Logger;

    invoke-interface {v0, p0}, Lcom/google/common/logging/Logger;->setThreshold(I)V

    .line 145
    return-void
.end method
