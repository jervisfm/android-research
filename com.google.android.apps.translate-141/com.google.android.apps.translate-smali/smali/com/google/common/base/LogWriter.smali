.class Lcom/google/common/base/LogWriter;
.super Ljava/lang/Object;
.source "LogWriter.java"

# interfaces
.implements Lcom/google/common/logging/Logger;


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/base/LogWriter$LoggedError;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final DEFAULT_THRESHOLD:I = 0x0

.field private static final LEVEL_IDS:Ljava/lang/String; = "DIX"


# instance fields
.field dateFormatter:Ljava/text/SimpleDateFormat;

.field threadTagMap:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private threshold:I

.field writer:Ljava/io/Writer;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/google/common/base/LogWriter;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 61
    iput-object v0, p0, Lcom/google/common/base/LogWriter;->writer:Ljava/io/Writer;

    .line 230
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/common/base/LogWriter;->threadTagMap:Ljava/lang/ThreadLocal;

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 2
    .parameter "writer"

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/google/common/base/LogWriter;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 61
    iput-object v0, p0, Lcom/google/common/base/LogWriter;->writer:Ljava/io/Writer;

    .line 230
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/common/base/LogWriter;->threadTagMap:Ljava/lang/ThreadLocal;

    .line 73
    iput-object p1, p0, Lcom/google/common/base/LogWriter;->writer:Ljava/io/Writer;

    .line 74
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyMMdd HH:mm:ss.SSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/common/base/LogWriter;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/LogWriter;->threshold:I

    .line 76
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 252
    :try_start_0
    iget-object v0, p0, Lcom/google/common/base/LogWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 253
    iget-object v0, p0, Lcom/google/common/base/LogWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :goto_0
    return-void

    .line 254
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getThreadTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/common/base/LogWriter;->threadTagMap:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getThreshold()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/common/base/LogWriter;->threshold:I

    return v0
.end method

.method public logDebug(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/common/base/LogWriter;->write(ILjava/lang/String;)V

    .line 108
    return-void
.end method

.method public logError(Ljava/lang/String;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 171
    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/common/base/LogWriter$LoggedError;

    invoke-direct {v2}, Lcom/google/common/base/LogWriter$LoggedError;-><init>()V

    invoke-static {v2}, Lcom/google/common/base/Log2;->getExceptionTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/common/base/LogWriter;->write(ILjava/lang/String;)V

    .line 172
    return-void
.end method

.method public logEvent(Ljava/lang/String;)V
    .locals 1
    .parameter "msg"

    .prologue
    .line 115
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/common/base/LogWriter;->write(ILjava/lang/String;)V

    .line 116
    return-void
.end method

.method public logException(Ljava/lang/Throwable;)V
    .locals 1
    .parameter "t"

    .prologue
    .line 137
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/google/common/base/LogWriter;->logException(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public logException(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4
    .parameter "t"
    .parameter "msg"

    .prologue
    .line 142
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, errorMessage:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 144
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 146
    :cond_0
    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/google/common/base/Log2;->getExceptionTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/common/base/LogWriter;->write(ILjava/lang/String;)V

    .line 147
    return-void
.end method

.method public logSevereException(Ljava/lang/Throwable;)V
    .locals 1
    .parameter "t"

    .prologue
    .line 151
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/google/common/base/LogWriter;->logSevereException(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public logSevereException(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4
    .parameter "t"
    .parameter "msg"

    .prologue
    .line 156
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, errorMessage:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 158
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 160
    :cond_0
    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/google/common/base/Log2;->getExceptionTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/common/base/LogWriter;->write(ILjava/lang/String;)V

    .line 161
    return-void
.end method

.method public logTimedEvent(Ljava/lang/String;JJ)V
    .locals 4
    .parameter "msg"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 125
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sub-long v2, p4, p2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms.: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/common/base/LogWriter;->write(ILjava/lang/String;)V

    .line 126
    return-void
.end method

.method public setErrorEmail(Ljava/lang/String;)V
    .locals 0
    .parameter "emailAddr"

    .prologue
    .line 133
    return-void
.end method

.method public setThreadTag(Ljava/lang/String;)V
    .locals 1
    .parameter "s"

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/common/base/LogWriter;->threadTagMap:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 240
    return-void
.end method

.method public setThreshold(I)V
    .locals 3
    .parameter "level"

    .prologue
    .line 85
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RotatingLog#setThreshold(int) : invalid threshold value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_1
    iput p1, p0, Lcom/google/common/base/LogWriter;->threshold:I

    .line 90
    return-void
.end method

.method protected declared-synchronized write(ILjava/lang/String;)V
    .locals 9
    .parameter "level"
    .parameter "msg"

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget v6, p0, Lcom/google/common/base/LogWriter;->threshold:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge p1, v6, :cond_0

    .line 228
    :goto_0
    monitor-exit p0

    return-void

    .line 193
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/common/base/LogWriter;->getThreadTag()Ljava/lang/String;

    move-result-object v4

    .line 194
    .local v4, threadTag:Ljava/lang/String;
    if-nez v4, :cond_2

    .line 195
    const-string v4, ""

    .line 203
    :goto_1
    const-string v6, "DIX"

    invoke-virtual {v6, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 204
    .local v0, charPrefix:C
    new-instance v3, Ljava/lang/StringBuffer;

    const/16 v6, 0x3e8

    invoke-direct {v3, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 206
    .local v3, sb:Ljava/lang/StringBuffer;
    const/16 v6, 0xa

    invoke-static {v6}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 207
    .local v5, token:Ljava/lang/String;
    iget-object v6, p0, Lcom/google/common/base/LogWriter;->dateFormatter:Ljava/text/SimpleDateFormat;

    if-eqz v6, :cond_1

    .line 208
    iget-object v6, p0, Lcom/google/common/base/LogWriter;->dateFormatter:Ljava/text/SimpleDateFormat;

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 209
    const/16 v6, 0x3a

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 211
    :cond_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 212
    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 213
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 214
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 215
    const-string v6, "\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    const/16 v0, 0x20

    goto :goto_2

    .line 197
    .end local v0           #charPrefix:C
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #sb:Ljava/lang/StringBuffer;
    .end local v5           #token:Ljava/lang/String;
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    goto :goto_1

    .line 220
    .restart local v0       #charPrefix:C
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #sb:Ljava/lang/StringBuffer;
    :cond_3
    :try_start_2
    iget-object v6, p0, Lcom/google/common/base/LogWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 221
    iget-object v6, p0, Lcom/google/common/base/LogWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v6}, Ljava/io/Writer;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 222
    :catch_0
    move-exception v2

    .line 223
    .local v2, ioE:Ljava/io/IOException;
    :try_start_3
    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LogWriter#write(int, String) : error in writing to log!\n Exception thrown: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\nlog entry: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 191
    .end local v0           #charPrefix:C
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #ioE:Ljava/io/IOException;
    .end local v3           #sb:Ljava/lang/StringBuffer;
    .end local v4           #threadTag:Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method
