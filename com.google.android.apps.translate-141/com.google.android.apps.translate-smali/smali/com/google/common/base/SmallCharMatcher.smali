.class final Lcom/google/common/base/SmallCharMatcher;
.super Lcom/google/common/base/CharMatcher$FastMatcher;
.source "SmallCharMatcher.java"


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
    emulated = true
.end annotation


# static fields
.field static final MAX_SIZE:I = 0x3f

.field static final MAX_TABLE_SIZE:I = 0x80


# instance fields
.field private final containsZero:Z

.field final filter:J

.field private final reprobe:Z

.field private final table:[C


# direct methods
.method private constructor <init>([CJZZLjava/lang/String;)V
    .locals 0
    .parameter "table"
    .parameter "filter"
    .parameter "containsZero"
    .parameter "reprobe"
    .parameter "description"

    .prologue
    .line 43
    invoke-direct {p0, p6}, Lcom/google/common/base/CharMatcher$FastMatcher;-><init>(Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/google/common/base/SmallCharMatcher;->table:[C

    .line 45
    iput-wide p2, p0, Lcom/google/common/base/SmallCharMatcher;->filter:J

    .line 46
    iput-boolean p4, p0, Lcom/google/common/base/SmallCharMatcher;->containsZero:Z

    .line 47
    iput-boolean p5, p0, Lcom/google/common/base/SmallCharMatcher;->reprobe:Z

    .line 48
    return-void
.end method

.method static buildTable(I[CZ)[C
    .locals 7
    .parameter "modulus"
    .parameter "charArray"
    .parameter "reprobe"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 56
    new-array v5, p0, [C

    .line 57
    .local v5, table:[C
    move-object v0, p1

    .local v0, arr$:[C
    array-length v4, v0

    .local v4, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v4, :cond_1

    aget-char v1, v0, v2

    .line 58
    .local v1, c:C
    rem-int v3, v1, p0

    .line 59
    .local v3, index:I
    if-gez v3, :cond_0

    .line 60
    add-int/2addr v3, p0

    .line 62
    :cond_0
    aget-char v6, v5, v3

    if-eqz v6, :cond_2

    if-nez p2, :cond_2

    .line 63
    const/4 v5, 0x0

    .line 71
    .end local v1           #c:C
    .end local v3           #index:I
    .end local v5           #table:[C
    :cond_1
    return-object v5

    .line 64
    .restart local v1       #c:C
    .restart local v3       #index:I
    .restart local v5       #table:[C
    :cond_2
    if-eqz p2, :cond_3

    .line 65
    :goto_1
    aget-char v6, v5, v3

    if-eqz v6, :cond_3

    .line 66
    add-int/lit8 v6, v3, 0x1

    rem-int v3, v6, p0

    goto :goto_1

    .line 69
    :cond_3
    aput-char v1, v5, v3

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private checkFilter(I)Z
    .locals 4
    .parameter "c"

    .prologue
    const-wide/16 v2, 0x1

    .line 51
    iget-wide v0, p0, Lcom/google/common/base/SmallCharMatcher;->filter:J

    shr-long/2addr v0, p1

    and-long/2addr v0, v2

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static from(Ljava/util/BitSet;Ljava/lang/String;)Lcom/google/common/base/CharMatcher;
    .locals 5
    .parameter "chars"
    .parameter "description"
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.util.BitSet"
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Ljava/util/BitSet;->cardinality()I

    move-result v4

    new-array v1, v4, [C

    .line 78
    .local v1, charArray:[C
    const/4 v2, 0x0

    .local v2, i:I
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    .local v0, c:I
    move v3, v2

    .end local v2           #i:I
    .local v3, i:I
    :goto_0
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 79
    add-int/lit8 v2, v3, 0x1

    .end local v3           #i:I
    .restart local v2       #i:I
    int-to-char v4, v0

    aput-char v4, v1, v3

    .line 78
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v4}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v3, v2

    .end local v2           #i:I
    .restart local v3       #i:I
    goto :goto_0

    .line 81
    :cond_0
    invoke-static {v1, p1}, Lcom/google/common/base/SmallCharMatcher;->from([CLjava/lang/String;)Lcom/google/common/base/CharMatcher;

    move-result-object v4

    return-object v4
.end method

.method static from([CLjava/lang/String;)Lcom/google/common/base/CharMatcher;
    .locals 15
    .parameter "chars"
    .parameter "description"

    .prologue
    .line 85
    array-length v12, p0

    .line 86
    .local v12, size:I
    const/4 v0, 0x0

    aget-char v0, p0, v0

    if-nez v0, :cond_0

    const/4 v4, 0x1

    .line 87
    .local v4, containsZero:Z
    :goto_0
    const/4 v5, 0x0

    .line 90
    .local v5, reprobe:Z
    const-wide/16 v2, 0x0

    .line 91
    .local v2, filter:J
    move-object v7, p0

    .local v7, arr$:[C
    array-length v11, v7

    .local v11, len$:I
    const/4 v10, 0x0

    .local v10, i$:I
    :goto_1
    if-ge v10, v11, :cond_1

    aget-char v8, v7, v10

    .line 92
    .local v8, c:C
    const-wide/16 v13, 0x1

    shl-long/2addr v13, v8

    or-long/2addr v2, v13

    .line 91
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 86
    .end local v2           #filter:J
    .end local v4           #containsZero:Z
    .end local v5           #reprobe:Z
    .end local v7           #arr$:[C
    .end local v8           #c:C
    .end local v10           #i$:I
    .end local v11           #len$:I
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 94
    .restart local v2       #filter:J
    .restart local v4       #containsZero:Z
    .restart local v5       #reprobe:Z
    .restart local v7       #arr$:[C
    .restart local v10       #i$:I
    .restart local v11       #len$:I
    :cond_1
    const/4 v1, 0x0

    .line 95
    .local v1, table:[C
    move v9, v12

    .local v9, i:I
    :goto_2
    if-nez v1, :cond_2

    const/16 v0, 0x80

    if-ge v9, v0, :cond_2

    .line 96
    const/4 v0, 0x0

    invoke-static {v9, p0, v0}, Lcom/google/common/base/SmallCharMatcher;->buildTable(I[CZ)[C

    move-result-object v1

    .line 95
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 99
    :cond_2
    if-nez v1, :cond_3

    .line 100
    const/16 v0, 0x80

    const/4 v6, 0x1

    invoke-static {v0, p0, v6}, Lcom/google/common/base/SmallCharMatcher;->buildTable(I[CZ)[C

    move-result-object v1

    .line 101
    const/4 v5, 0x1

    .line 103
    :cond_3
    new-instance v0, Lcom/google/common/base/SmallCharMatcher;

    move-object/from16 v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/common/base/SmallCharMatcher;-><init>([CJZZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public matches(C)Z
    .locals 4
    .parameter "c"

    .prologue
    const/4 v1, 0x0

    .line 108
    if-nez p1, :cond_1

    .line 109
    iget-boolean v1, p0, Lcom/google/common/base/SmallCharMatcher;->containsZero:Z

    .line 128
    :cond_0
    :goto_0
    return v1

    .line 111
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/common/base/SmallCharMatcher;->checkFilter(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    iget-object v2, p0, Lcom/google/common/base/SmallCharMatcher;->table:[C

    array-length v2, v2

    rem-int v0, p1, v2

    .line 115
    .local v0, index:I
    if-gez v0, :cond_2

    .line 116
    iget-object v2, p0, Lcom/google/common/base/SmallCharMatcher;->table:[C

    array-length v2, v2

    add-int/2addr v0, v2

    .line 120
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/google/common/base/SmallCharMatcher;->table:[C

    aget-char v2, v2, v0

    if-eqz v2, :cond_0

    .line 122
    iget-object v2, p0, Lcom/google/common/base/SmallCharMatcher;->table:[C

    aget-char v2, v2, v0

    if-ne v2, p1, :cond_3

    .line 123
    const/4 v1, 0x1

    goto :goto_0

    .line 124
    :cond_3
    iget-boolean v2, p0, Lcom/google/common/base/SmallCharMatcher;->reprobe:Z

    if-eqz v2, :cond_0

    .line 126
    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/google/common/base/SmallCharMatcher;->table:[C

    array-length v3, v3

    rem-int v0, v2, v3

    goto :goto_1
.end method

.method setBits(Ljava/util/BitSet;)V
    .locals 5
    .parameter "bitSet"
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.util.BitSet"
    .end annotation

    .prologue
    .line 136
    iget-boolean v4, p0, Lcom/google/common/base/SmallCharMatcher;->containsZero:Z

    if-eqz v4, :cond_0

    .line 137
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/util/BitSet;->set(I)V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/SmallCharMatcher;->table:[C

    .local v0, arr$:[C
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-char v1, v0, v2

    .line 140
    .local v1, c:C
    if-eqz v1, :cond_1

    .line 141
    invoke-virtual {p1, v1}, Ljava/util/BitSet;->set(I)V

    .line 139
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 144
    .end local v1           #c:C
    :cond_2
    return-void
.end method
