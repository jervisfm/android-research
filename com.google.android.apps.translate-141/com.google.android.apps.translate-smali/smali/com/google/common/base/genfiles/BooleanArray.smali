.class public final Lcom/google/common/base/genfiles/BooleanArray;
.super Ljava/lang/Object;
.source "BooleanArray.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private length:I

.field private list:[Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    .line 33
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "capacity"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-array v0, p1, [Z

    iput-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    .line 46
    return-void
.end method

.method private constructor <init>([ZI)V
    .locals 0
    .parameter "array"
    .parameter "arrayLength"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    .line 65
    iput p2, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    .line 66
    return-void
.end method

.method public constructor <init>([ZII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 51
    new-array v0, p3, [Z

    iput-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    .line 52
    iput p3, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    .line 53
    iget-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    return-void

    :cond_0
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public static varargs newInstance([Z)Lcom/google/common/base/genfiles/BooleanArray;
    .locals 2
    .parameter "array"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 87
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    new-instance v0, Lcom/google/common/base/genfiles/BooleanArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/google/common/base/genfiles/BooleanArray;-><init>([ZI)V

    return-object v0
.end method


# virtual methods
.method public add(Z)V
    .locals 3
    .parameter "x"

    .prologue
    .line 113
    iget v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    iget-object v1, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 114
    iget v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/BooleanArray;->ensureCapacity(I)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    iget v1, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    aput-boolean p1, v0, v1

    .line 117
    return-void
.end method

.method public add([ZII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    .line 121
    iget v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 122
    iget v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    add-int/2addr v0, p3

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/BooleanArray;->ensureCapacity(I)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    iget v1, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 125
    iget v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    .line 126
    return-void
.end method

.method public addArray(Lcom/google/common/base/genfiles/BooleanArray;)V
    .locals 3
    .parameter "other"

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/google/common/base/genfiles/BooleanArray;->rep()[Z

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/common/base/genfiles/BooleanArray;->size()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/common/base/genfiles/BooleanArray;->add([ZII)V

    .line 131
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    .line 136
    return-void
.end method

.method public ensureCapacity(I)V
    .locals 5
    .parameter "n"

    .prologue
    const/4 v4, 0x0

    .line 140
    iget-object v2, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    array-length v2, v2

    if-ge v2, p1, :cond_1

    .line 141
    iget-object v2, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    array-length v2, v2

    mul-int/lit8 v1, v2, 0x2

    .line 142
    .local v1, newSize:I
    if-ge v1, p1, :cond_0

    .line 143
    move v1, p1

    .line 145
    :cond_0
    new-array v0, v1, [Z

    .line 146
    .local v0, copy:[Z
    iget-object v2, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    iget v3, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    iput-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    .line 149
    .end local v0           #copy:[Z
    .end local v1           #newSize:I
    :cond_1
    return-void
.end method

.method public get(I)Z
    .locals 1
    .parameter "i"

    .prologue
    .line 100
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 101
    iget-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    aget-boolean v0, v0, p1

    return v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rep()[Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    return-object v0
.end method

.method public set(IZ)V
    .locals 1
    .parameter "i"
    .parameter "x"

    .prologue
    .line 107
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 108
    iget-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    aput-boolean p2, v0, p1

    .line 109
    return-void

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    return v0
.end method

.method public toArray()[Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 168
    iget v1, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    new-array v0, v1, [Z

    .line 169
    .local v0, copy:[Z
    iget-object v1, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    iget v2, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 170
    return-object v0
.end method

.method public trimToSize()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    array-length v0, v0

    iget v1, p0, Lcom/google/common/base/genfiles/BooleanArray;->length:I

    if-eq v0, v1, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/google/common/base/genfiles/BooleanArray;->toArray()[Z

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/base/genfiles/BooleanArray;->list:[Z

    .line 164
    :cond_0
    return-void
.end method
