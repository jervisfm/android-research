.class public final Lcom/google/common/base/genfiles/ByteArray;
.super Ljava/lang/Object;
.source "ByteArray.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private length:I

.field private list:[B


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    .line 32
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "capacity"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    .line 45
    return-void
.end method

.method private constructor <init>([BI)V
    .locals 0
    .parameter "array"
    .parameter "arrayLength"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    .line 64
    iput p2, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    .line 65
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 50
    new-array v0, p3, [B

    iput-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    .line 51
    iput p3, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    .line 52
    iget-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53
    return-void

    :cond_0
    move v0, v1

    .line 49
    goto :goto_0
.end method


# virtual methods
.method public add(B)V
    .locals 3
    .parameter "x"

    .prologue
    .line 76
    iget v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    iget-object v1, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 77
    iget v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/ByteArray;->ensureCapacity(I)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    iget v1, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    aput-byte p1, v0, v1

    .line 80
    return-void
.end method

.method public add([BII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    .line 84
    iget v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 85
    iget v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    add-int/2addr v0, p3

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/ByteArray;->ensureCapacity(I)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    iget v1, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    iget v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    .line 89
    return-void
.end method

.method public final addUTF(Ljava/lang/String;)V
    .locals 6
    .parameter "str"

    .prologue
    .line 148
    iget v1, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    .line 149
    .local v1, originalLength:I
    sget-object v3, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 150
    .local v2, utf8Bytes:[B
    iget v3, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    array-length v4, v2

    add-int/2addr v3, v4

    invoke-virtual {p0, v3}, Lcom/google/common/base/genfiles/ByteArray;->resize(I)V

    .line 151
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 152
    iget-object v3, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    add-int v4, v1, v0

    aget-byte v5, v2, v0

    aput-byte v5, v3, v4

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    .line 94
    return-void
.end method

.method public copy([BIII)V
    .locals 4
    .parameter "dest"
    .parameter "len"
    .parameter "srcPos"
    .parameter "destPos"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    if-ltz p3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 137
    if-ltz p4, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 138
    if-ltz p2, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 139
    add-int v0, p3, p2

    iget v3, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    if-gt v0, v3, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 140
    add-int v0, p4, p2

    array-length v3, p1

    if-gt v0, v3, :cond_4

    :goto_4
    invoke-static {v1}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 141
    iget-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    invoke-static {v0, p3, p1, p4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    return-void

    :cond_0
    move v0, v2

    .line 136
    goto :goto_0

    :cond_1
    move v0, v2

    .line 137
    goto :goto_1

    :cond_2
    move v0, v2

    .line 138
    goto :goto_2

    :cond_3
    move v0, v2

    .line 139
    goto :goto_3

    :cond_4
    move v1, v2

    .line 140
    goto :goto_4
.end method

.method public ensureCapacity(I)V
    .locals 5
    .parameter "n"

    .prologue
    const/4 v4, 0x0

    .line 98
    iget-object v2, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    array-length v2, v2

    if-ge v2, p1, :cond_1

    .line 99
    iget-object v2, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    array-length v2, v2

    mul-int/lit8 v1, v2, 0x2

    .line 100
    .local v1, newSize:I
    if-ge v1, p1, :cond_0

    .line 101
    move v1, p1

    .line 103
    :cond_0
    new-array v0, v1, [B

    .line 104
    .local v0, copy:[B
    iget-object v2, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    iget v3, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    iput-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    .line 107
    .end local v0           #copy:[B
    .end local v1           #newSize:I
    :cond_1
    return-void
.end method

.method public rep()[B
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    return-object v0
.end method

.method public resize(I)V
    .locals 1
    .parameter "n"

    .prologue
    .line 122
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 123
    invoke-virtual {p0, p1}, Lcom/google/common/base/genfiles/ByteArray;->ensureCapacity(I)V

    .line 124
    iput p1, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    .line 125
    return-void

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    return v0
.end method

.method public toArray()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 129
    iget v1, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    new-array v0, v1, [B

    .line 130
    .local v0, copy:[B
    iget-object v1, p0, Lcom/google/common/base/genfiles/ByteArray;->list:[B

    iget v2, p0, Lcom/google/common/base/genfiles/ByteArray;->length:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131
    return-object v0
.end method
