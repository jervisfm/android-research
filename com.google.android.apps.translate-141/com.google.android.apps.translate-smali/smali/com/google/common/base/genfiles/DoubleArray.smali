.class public final Lcom/google/common/base/genfiles/DoubleArray;
.super Ljava/lang/Object;
.source "DoubleArray.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private length:I

.field private list:[D


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    .line 33
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "capacity"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-array v0, p1, [D

    iput-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    .line 46
    return-void
.end method

.method private constructor <init>([DI)V
    .locals 0
    .parameter "array"
    .parameter "arrayLength"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    .line 65
    iput p2, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    .line 66
    return-void
.end method

.method public constructor <init>([DII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 51
    new-array v0, p3, [D

    iput-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    .line 52
    iput p3, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    .line 53
    iget-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    return-void

    :cond_0
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public static varargs newInstance([D)Lcom/google/common/base/genfiles/DoubleArray;
    .locals 2
    .parameter "array"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 87
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    new-instance v0, Lcom/google/common/base/genfiles/DoubleArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/google/common/base/genfiles/DoubleArray;-><init>([DI)V

    return-object v0
.end method


# virtual methods
.method public add(D)V
    .locals 3
    .parameter "x"

    .prologue
    .line 113
    iget v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    iget-object v1, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 114
    iget v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/DoubleArray;->ensureCapacity(I)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    iget v1, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    aput-wide p1, v0, v1

    .line 117
    return-void
.end method

.method public add([DII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    .line 121
    iget v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 122
    iget v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    add-int/2addr v0, p3

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/DoubleArray;->ensureCapacity(I)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    iget v1, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 125
    iget v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    .line 126
    return-void
.end method

.method public addArray(Lcom/google/common/base/genfiles/DoubleArray;)V
    .locals 3
    .parameter "other"

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/google/common/base/genfiles/DoubleArray;->rep()[D

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/common/base/genfiles/DoubleArray;->size()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/common/base/genfiles/DoubleArray;->add([DII)V

    .line 131
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    .line 136
    return-void
.end method

.method public copy([DIII)V
    .locals 4
    .parameter "dest"
    .parameter "len"
    .parameter "srcPos"
    .parameter "destPos"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184
    if-ltz p3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 185
    if-ltz p4, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 186
    if-ltz p2, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 187
    add-int v0, p3, p2

    iget v3, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    if-gt v0, v3, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 188
    add-int v0, p4, p2

    array-length v3, p1

    if-gt v0, v3, :cond_4

    :goto_4
    invoke-static {v1}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 189
    iget-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    invoke-static {v0, p3, p1, p4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 190
    return-void

    :cond_0
    move v0, v2

    .line 184
    goto :goto_0

    :cond_1
    move v0, v2

    .line 185
    goto :goto_1

    :cond_2
    move v0, v2

    .line 186
    goto :goto_2

    :cond_3
    move v0, v2

    .line 187
    goto :goto_3

    :cond_4
    move v1, v2

    .line 188
    goto :goto_4
.end method

.method public ensureCapacity(I)V
    .locals 5
    .parameter "n"

    .prologue
    const/4 v4, 0x0

    .line 140
    iget-object v2, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    array-length v2, v2

    if-ge v2, p1, :cond_1

    .line 141
    iget-object v2, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    array-length v2, v2

    mul-int/lit8 v1, v2, 0x2

    .line 142
    .local v1, newSize:I
    if-ge v1, p1, :cond_0

    .line 143
    move v1, p1

    .line 145
    :cond_0
    new-array v0, v1, [D

    .line 146
    .local v0, copy:[D
    iget-object v2, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    iget v3, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    iput-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    .line 149
    .end local v0           #copy:[D
    .end local v1           #newSize:I
    :cond_1
    return-void
.end method

.method public get(I)D
    .locals 2
    .parameter "i"

    .prologue
    .line 100
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 101
    iget-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    aget-wide v0, v0, p1

    return-wide v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rep()[D
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    return-object v0
.end method

.method public set(ID)V
    .locals 1
    .parameter "i"
    .parameter "x"

    .prologue
    .line 107
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 108
    iget-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    aput-wide p2, v0, p1

    .line 109
    return-void

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    return v0
.end method

.method public subArray(II)[D
    .locals 5
    .parameter "start"
    .parameter "len"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 175
    if-ltz p1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 176
    add-int v1, p1, p2

    iget v4, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    if-gt v1, v4, :cond_1

    :goto_1
    invoke-static {v2}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 177
    new-array v0, p2, [D

    .line 178
    .local v0, copy:[D
    iget-object v1, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    invoke-static {v1, p1, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 179
    return-object v0

    .end local v0           #copy:[D
    :cond_0
    move v1, v3

    .line 175
    goto :goto_0

    :cond_1
    move v2, v3

    .line 176
    goto :goto_1
.end method

.method public toArray()[D
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 168
    iget v1, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    new-array v0, v1, [D

    .line 169
    .local v0, copy:[D
    iget-object v1, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    iget v2, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 170
    return-object v0
.end method

.method public trimToSize()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    array-length v0, v0

    iget v1, p0, Lcom/google/common/base/genfiles/DoubleArray;->length:I

    if-eq v0, v1, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/google/common/base/genfiles/DoubleArray;->toArray()[D

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/base/genfiles/DoubleArray;->list:[D

    .line 164
    :cond_0
    return-void
.end method
