.class public final Lcom/google/common/base/genfiles/FloatArray;
.super Ljava/lang/Object;
.source "FloatArray.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private length:I

.field private list:[F


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    .line 33
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "capacity"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-array v0, p1, [F

    iput-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    .line 46
    return-void
.end method

.method private constructor <init>([FI)V
    .locals 0
    .parameter "array"
    .parameter "arrayLength"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    .line 65
    iput p2, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    .line 66
    return-void
.end method

.method public constructor <init>([FII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 51
    new-array v0, p3, [F

    iput-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    .line 52
    iput p3, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    .line 53
    iget-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    return-void

    :cond_0
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public static varargs newInstance([F)Lcom/google/common/base/genfiles/FloatArray;
    .locals 2
    .parameter "array"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 87
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    new-instance v0, Lcom/google/common/base/genfiles/FloatArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/google/common/base/genfiles/FloatArray;-><init>([FI)V

    return-object v0
.end method


# virtual methods
.method public add(F)V
    .locals 3
    .parameter "x"

    .prologue
    .line 106
    iget v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    iget-object v1, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 107
    iget v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/FloatArray;->ensureCapacity(I)V

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    iget v1, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    aput p1, v0, v1

    .line 110
    return-void
.end method

.method public add([FII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    .line 114
    iget v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 115
    iget v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    add-int/2addr v0, p3

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/FloatArray;->ensureCapacity(I)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    iget v1, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    iget v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    .line 119
    return-void
.end method

.method public addArray(Lcom/google/common/base/genfiles/FloatArray;)V
    .locals 3
    .parameter "other"

    .prologue
    .line 123
    invoke-virtual {p1}, Lcom/google/common/base/genfiles/FloatArray;->rep()[F

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/common/base/genfiles/FloatArray;->size()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/common/base/genfiles/FloatArray;->add([FII)V

    .line 124
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    .line 129
    return-void
.end method

.method public ensureCapacity(I)V
    .locals 5
    .parameter "n"

    .prologue
    const/4 v4, 0x0

    .line 133
    iget-object v2, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    array-length v2, v2

    if-ge v2, p1, :cond_1

    .line 134
    iget-object v2, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    array-length v2, v2

    mul-int/lit8 v1, v2, 0x2

    .line 135
    .local v1, newSize:I
    if-ge v1, p1, :cond_0

    .line 136
    move v1, p1

    .line 138
    :cond_0
    new-array v0, v1, [F

    .line 139
    .local v0, copy:[F
    iget-object v2, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    iget v3, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    iput-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    .line 142
    .end local v0           #copy:[F
    .end local v1           #newSize:I
    :cond_1
    return-void
.end method

.method public get(I)F
    .locals 1
    .parameter "i"

    .prologue
    .line 100
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 101
    iget-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    aget v0, v0, p1

    return v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rep()[F
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    return v0
.end method

.method public subArray(II)[F
    .locals 5
    .parameter "start"
    .parameter "len"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 168
    if-ltz p1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 169
    add-int v1, p1, p2

    iget v4, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    if-gt v1, v4, :cond_1

    :goto_1
    invoke-static {v2}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 170
    new-array v0, p2, [F

    .line 171
    .local v0, copy:[F
    iget-object v1, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    invoke-static {v1, p1, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    return-object v0

    .end local v0           #copy:[F
    :cond_0
    move v1, v3

    .line 168
    goto :goto_0

    :cond_1
    move v2, v3

    .line 169
    goto :goto_1
.end method

.method public toArray()[F
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 161
    iget v1, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    new-array v0, v1, [F

    .line 162
    .local v0, copy:[F
    iget-object v1, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    iget v2, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 163
    return-object v0
.end method

.method public trimToSize()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    array-length v0, v0

    iget v1, p0, Lcom/google/common/base/genfiles/FloatArray;->length:I

    if-eq v0, v1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/google/common/base/genfiles/FloatArray;->toArray()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/base/genfiles/FloatArray;->list:[F

    .line 157
    :cond_0
    return-void
.end method
