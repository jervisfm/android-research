.class public final Lcom/google/common/base/genfiles/IntArray;
.super Ljava/lang/Object;
.source "IntArray.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private length:I

.field private list:[I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    .line 33
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "capacity"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    .line 46
    return-void
.end method

.method private constructor <init>([II)V
    .locals 0
    .parameter "array"
    .parameter "arrayLength"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    .line 65
    iput p2, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    .line 66
    return-void
.end method

.method public constructor <init>([III)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 51
    new-array v0, p3, [I

    iput-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    .line 52
    iput p3, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    .line 53
    iget-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    return-void

    :cond_0
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public static varargs newInstance([I)Lcom/google/common/base/genfiles/IntArray;
    .locals 2
    .parameter "array"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 87
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    new-instance v0, Lcom/google/common/base/genfiles/IntArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/google/common/base/genfiles/IntArray;-><init>([II)V

    return-object v0
.end method


# virtual methods
.method public add(I)V
    .locals 3
    .parameter "x"

    .prologue
    .line 113
    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    iget-object v1, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 114
    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/IntArray;->ensureCapacity(I)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    iget v1, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    aput p1, v0, v1

    .line 117
    return-void
.end method

.method public add([III)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    .line 121
    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 122
    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    add-int/2addr v0, p3

    invoke-virtual {p0, v0}, Lcom/google/common/base/genfiles/IntArray;->ensureCapacity(I)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    iget v1, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 125
    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    .line 126
    return-void
.end method

.method public addArray(Lcom/google/common/base/genfiles/IntArray;)V
    .locals 3
    .parameter "other"

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/google/common/base/genfiles/IntArray;->rep()[I

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/common/base/genfiles/IntArray;->size()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/common/base/genfiles/IntArray;->add([III)V

    .line 131
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    .line 136
    return-void
.end method

.method public ensureCapacity(I)V
    .locals 5
    .parameter "n"

    .prologue
    const/4 v4, 0x0

    .line 168
    iget-object v2, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    array-length v2, v2

    if-ge v2, p1, :cond_1

    .line 169
    iget-object v2, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    array-length v2, v2

    mul-int/lit8 v1, v2, 0x2

    .line 170
    .local v1, newSize:I
    if-ge v1, p1, :cond_0

    .line 171
    move v1, p1

    .line 173
    :cond_0
    new-array v0, v1, [I

    .line 174
    .local v0, copy:[I
    iget-object v2, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    iget v3, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 175
    iput-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    .line 177
    .end local v0           #copy:[I
    .end local v1           #newSize:I
    :cond_1
    return-void
.end method

.method public get(I)I
    .locals 1
    .parameter "i"

    .prologue
    .line 100
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 101
    iget-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    aget v0, v0, p1

    return v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public indexOf(I)I
    .locals 2
    .parameter "element"

    .prologue
    .line 158
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget v1, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    if-ge v0, v1, :cond_1

    .line 159
    iget-object v1, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 163
    .end local v0           #i:I
    :goto_1
    return v0

    .line 158
    .restart local v0       #i:I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public removeFast(I)V
    .locals 3
    .parameter "i"

    .prologue
    .line 149
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 150
    iget-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    iget-object v1, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    iget v2, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    aput v1, v0, p1

    .line 151
    invoke-virtual {p0}, Lcom/google/common/base/genfiles/IntArray;->removeLast()V

    .line 152
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeLast()V
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 143
    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    .line 144
    return-void

    .line 140
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rep()[I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    return-object v0
.end method

.method public set(II)V
    .locals 1
    .parameter "i"
    .parameter "x"

    .prologue
    .line 107
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 108
    iget-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    aput p2, v0, p1

    .line 109
    return-void

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    return v0
.end method

.method public subArray(II)[I
    .locals 5
    .parameter "start"
    .parameter "len"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 203
    if-ltz p1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 204
    add-int v1, p1, p2

    iget v4, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    if-gt v1, v4, :cond_1

    :goto_1
    invoke-static {v2}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 205
    new-array v0, p2, [I

    .line 206
    .local v0, copy:[I
    iget-object v1, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    invoke-static {v1, p1, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    return-object v0

    .end local v0           #copy:[I
    :cond_0
    move v1, v3

    .line 203
    goto :goto_0

    :cond_1
    move v2, v3

    .line 204
    goto :goto_1
.end method

.method public toArray()[I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 196
    iget v1, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    new-array v0, v1, [I

    .line 197
    .local v0, copy:[I
    iget-object v1, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    iget v2, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    return-object v0
.end method

.method public trimToSize()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    array-length v0, v0

    iget v1, p0, Lcom/google/common/base/genfiles/IntArray;->length:I

    if-eq v0, v1, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/google/common/base/genfiles/IntArray;->toArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/base/genfiles/IntArray;->list:[I

    .line 192
    :cond_0
    return-void
.end method
