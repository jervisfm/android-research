.class public final Lcom/google/common/base/genfiles/LongArray;
.super Ljava/lang/Object;
.source "LongArray.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GoogleInternal;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private length:I

.field private list:[J


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 35
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "capacity"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-array v0, p1, [J

    iput-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 48
    return-void
.end method

.method private constructor <init>([JI)V
    .locals 0
    .parameter "array"
    .parameter "arrayLength"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 67
    iput p2, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 68
    return-void
.end method

.method public constructor <init>([JII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 53
    new-array v0, p3, [J

    iput-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 54
    iput p3, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 55
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    return-void

    :cond_0
    move v0, v1

    .line 52
    goto :goto_0
.end method

.method private ensureCapacity(I)V
    .locals 5
    .parameter "n"

    .prologue
    const/4 v4, 0x0

    .line 199
    iget-object v2, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    array-length v2, v2

    if-ge v2, p1, :cond_1

    .line 200
    iget-object v2, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    array-length v2, v2

    mul-int/lit8 v1, v2, 0x2

    .line 201
    .local v1, newSize:I
    if-ge v1, p1, :cond_0

    .line 202
    move v1, p1

    .line 204
    :cond_0
    new-array v0, v1, [J

    .line 205
    .local v0, copy:[J
    iget-object v2, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    iget v3, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 206
    iput-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 208
    .end local v0           #copy:[J
    .end local v1           #newSize:I
    :cond_1
    return-void
.end method

.method public static varargs newInstance([J)Lcom/google/common/base/genfiles/LongArray;
    .locals 2
    .parameter "array"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 89
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    new-instance v0, Lcom/google/common/base/genfiles/LongArray;

    array-length v1, p0

    invoke-direct {v0, p0, v1}, Lcom/google/common/base/genfiles/LongArray;-><init>([JI)V

    return-object v0
.end method

.method static newInstance([JI)Lcom/google/common/base/genfiles/LongArray;
    .locals 1
    .parameter "array"
    .parameter "length"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 116
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    if-ltz p1, :cond_0

    array-length v0, p0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 118
    new-instance v0, Lcom/google/common/base/genfiles/LongArray;

    invoke-direct {v0, p0, p1}, Lcom/google/common/base/genfiles/LongArray;-><init>([JI)V

    return-object v0

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(J)V
    .locals 3
    .parameter "x"

    .prologue
    .line 142
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    iget-object v1, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 143
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/common/base/genfiles/LongArray;->ensureCapacity(I)V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    iget v1, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    aput-wide p1, v0, v1

    .line 146
    return-void
.end method

.method add([JII)V
    .locals 2
    .parameter "source"
    .parameter "start"
    .parameter "num"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 151
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 152
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    add-int/2addr v0, p3

    invoke-direct {p0, v0}, Lcom/google/common/base/genfiles/LongArray;->ensureCapacity(I)V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    iget v1, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 156
    return-void
.end method

.method public addArray(Lcom/google/common/base/genfiles/LongArray;)V
    .locals 3
    .parameter "other"

    .prologue
    .line 160
    invoke-virtual {p1}, Lcom/google/common/base/genfiles/LongArray;->rep()[J

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/common/base/genfiles/LongArray;->size()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/common/base/genfiles/LongArray;->add([JII)V

    .line 161
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 166
    return-void
.end method

.method copy([JIII)V
    .locals 4
    .parameter "dest"
    .parameter "len"
    .parameter "srcPos"
    .parameter "destPos"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 268
    if-ltz p3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 269
    if-ltz p4, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 270
    if-ltz p2, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 271
    add-int v0, p3, p2

    iget v3, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    if-gt v0, v3, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 272
    add-int v0, p4, p2

    array-length v3, p1

    if-gt v0, v3, :cond_4

    :goto_4
    invoke-static {v1}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 273
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    invoke-static {v0, p3, p1, p4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 274
    return-void

    :cond_0
    move v0, v2

    .line 268
    goto :goto_0

    :cond_1
    move v0, v2

    .line 269
    goto :goto_1

    :cond_2
    move v0, v2

    .line 270
    goto :goto_2

    :cond_3
    move v0, v2

    .line 271
    goto :goto_3

    :cond_4
    move v1, v2

    .line 272
    goto :goto_4
.end method

.method public get(I)J
    .locals 2
    .parameter "i"

    .prologue
    .line 129
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 130
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    aget-wide v0, v0, p1

    return-wide v0

    .line 129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public indexOf(J)I
    .locals 3
    .parameter "element"

    .prologue
    .line 189
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget v1, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    if-ge v0, v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    aget-wide v1, v1, v0

    cmp-long v1, v1, p1

    if-nez v1, :cond_0

    .line 194
    .end local v0           #i:I
    :goto_1
    return v0

    .line 189
    .restart local v0       #i:I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public removeFast(I)V
    .locals 3
    .parameter "i"

    .prologue
    .line 180
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 181
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    iget-object v1, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    iget v2, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    add-int/lit8 v2, v2, -0x1

    aget-wide v1, v1, v2

    aput-wide v1, v0, p1

    .line 182
    invoke-virtual {p0}, Lcom/google/common/base/genfiles/LongArray;->removeLast()V

    .line 183
    return-void

    .line 180
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method removeLast()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 171
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 174
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 175
    return-void

    .line 171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rep()[J
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    return-object v0
.end method

.method resize(I)V
    .locals 1
    .parameter "n"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 224
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 225
    invoke-direct {p0, p1}, Lcom/google/common/base/genfiles/LongArray;->ensureCapacity(I)V

    .line 226
    iput p1, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 227
    return-void

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public set(IJ)V
    .locals 1
    .parameter "i"
    .parameter "x"

    .prologue
    .line 136
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    aput-wide p2, v0, p1

    .line 138
    return-void

    .line 136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    return v0
.end method

.method subArray(II)[J
    .locals 5
    .parameter "start"
    .parameter "len"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 258
    if-ltz p1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 259
    add-int v1, p1, p2

    iget v4, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    if-gt v1, v4, :cond_1

    :goto_1
    invoke-static {v2}, Lcom/google/common/base/X;->assertTrue(Z)V

    .line 260
    new-array v0, p2, [J

    .line 261
    .local v0, copy:[J
    iget-object v1, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    invoke-static {v1, p1, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 262
    return-object v0

    .end local v0           #copy:[J
    :cond_0
    move v1, v3

    .line 258
    goto :goto_0

    :cond_1
    move v2, v3

    .line 259
    goto :goto_1
.end method

.method swap(Lcom/google/common/base/genfiles/LongArray;)V
    .locals 3
    .parameter "other"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 240
    iget v0, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 241
    .local v0, tmpLength:I
    iget v2, p1, Lcom/google/common/base/genfiles/LongArray;->length:I

    iput v2, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 242
    iput v0, p1, Lcom/google/common/base/genfiles/LongArray;->length:I

    .line 243
    iget-object v1, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 244
    .local v1, tmpList:[J
    iget-object v2, p1, Lcom/google/common/base/genfiles/LongArray;->list:[J

    iput-object v2, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 245
    iput-object v1, p1, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 246
    return-void
.end method

.method public toArray()[J
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    iget v1, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    new-array v0, v1, [J

    .line 251
    .local v0, copy:[J
    iget-object v1, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    iget v2, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 252
    return-object v0
.end method

.method public trimToSize()V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    array-length v0, v0

    iget v1, p0, Lcom/google/common/base/genfiles/LongArray;->length:I

    if-eq v0, v1, :cond_0

    .line 232
    invoke-virtual {p0}, Lcom/google/common/base/genfiles/LongArray;->toArray()[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/base/genfiles/LongArray;->list:[J

    .line 234
    :cond_0
    return-void
.end method
