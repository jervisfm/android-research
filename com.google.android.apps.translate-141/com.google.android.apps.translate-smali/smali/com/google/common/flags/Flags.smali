.class public Lcom/google/common/flags/Flags;
.super Ljava/lang/Object;
.source "Flags.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/flags/Flags$FlagInfoImpl;,
        Lcom/google/common/flags/Flags$FlagMapHolder;,
        Lcom/google/common/flags/Flags$ParseState;
    }
.end annotation


# static fields
.field private static final BLANK_LINE_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final CLASS_LOADER:Ljava/lang/String; = "com.google.common.flags.classLoader"

.field private static final COMMENT_LINE_PATTERN:Ljava/util/regex/Pattern; = null

.field public static final DISABLE_CHECKING:Ljava/lang/String; = "com.google.common.flags.disableStateChecking"

.field private static final DISABLE_EXIT:Ljava/lang/String; = "com.google.common.flags.noExit"

.field private static final EMPTY_STRING_ARRAY:[Ljava/lang/String; = null

.field private static final FLAG_FILE_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final FLAG_NAME_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final FLAG_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final FLAG_RESOURCE_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final HELPSHORT_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final HELP_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final UNDEF_OK_FLAG_NAME:Ljava/lang/String; = "undefok"

.field private static final XML_HELP_PATTERN:Ljava/util/regex/Pattern;

.field private static final cachedMainClassName:Ljava/lang/String;

.field private static final completionHooks:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static flagClassLoader:Ljava/lang/ClassLoader;

.field private static final logger:Ljava/util/logging/Logger;

.field private static outputStream:Ljava/io/PrintStream;

.field private static parseStackTrace:Ljava/lang/Throwable;

.field private static parseState:Lcom/google/common/flags/Flags$ParseState;

.field private static final preferredClasses:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static undefOkSupported:Z

.field private static usagePrefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 86
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    sput-object v4, Lcom/google/common/flags/Flags;->completionHooks:Ljava/util/Collection;

    .line 89
    const-class v4, Lcom/google/common/flags/Flags;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->logger:Ljava/util/logging/Logger;

    .line 100
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sput-object v4, Lcom/google/common/flags/Flags;->outputStream:Ljava/io/PrintStream;

    .line 127
    new-array v4, v5, [Ljava/lang/String;

    sput-object v4, Lcom/google/common/flags/Flags;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    .line 137
    sget-object v4, Lcom/google/common/flags/Flags$ParseState;->NOT_STARTED:Lcom/google/common/flags/Flags$ParseState;

    sput-object v4, Lcom/google/common/flags/Flags;->parseState:Lcom/google/common/flags/Flags$ParseState;

    .line 150
    sput-object v6, Lcom/google/common/flags/Flags;->usagePrefix:Ljava/lang/String;

    .line 155
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    sput-object v4, Lcom/google/common/flags/Flags;->preferredClasses:Ljava/util/Set;

    .line 161
    sput-object v6, Lcom/google/common/flags/Flags;->flagClassLoader:Ljava/lang/ClassLoader;

    .line 173
    sput-boolean v5, Lcom/google/common/flags/Flags;->undefOkSupported:Z

    .line 189
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/flags/Flags;->getMainClassNameFromStackTrace([Ljava/lang/StackTraceElement;)Ljava/lang/String;

    move-result-object v2

    .line 192
    .local v2, mainClassName:Ljava/lang/String;
    if-nez v2, :cond_1

    .line 195
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/StackTraceElement;

    .line 196
    .local v3, stack:[Ljava/lang/StackTraceElement;
    invoke-static {v3}, Lcom/google/common/flags/Flags;->getMainClassNameFromStackTrace([Ljava/lang/StackTraceElement;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/AccessControlException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 197
    if-eqz v2, :cond_0

    .line 206
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #stack:[Ljava/lang/StackTraceElement;
    :cond_1
    :goto_0
    sput-object v2, Lcom/google/common/flags/Flags;->cachedMainClassName:Ljava/lang/String;

    .line 678
    const-string v4, "-+help(=true)?"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->HELP_PATTERN:Ljava/util/regex/Pattern;

    .line 681
    const-string v4, "-+helpshort(=true)?"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->HELPSHORT_PATTERN:Ljava/util/regex/Pattern;

    .line 684
    const-string v4, "-+helpxml(=true)?"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->XML_HELP_PATTERN:Ljava/util/regex/Pattern;

    .line 714
    const-string v4, "-+([^=]*)(?:=(.*))?"

    const/16 v5, 0x28

    invoke-static {v4, v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->FLAG_PATTERN:Ljava/util/regex/Pattern;

    .line 852
    const-string v4, "-+flagfile=(.+)"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->FLAG_FILE_PATTERN:Ljava/util/regex/Pattern;

    .line 856
    const-string v4, "-+flagresource=(.+)"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->FLAG_RESOURCE_PATTERN:Ljava/util/regex/Pattern;

    .line 944
    const-string v4, "\\s*"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->BLANK_LINE_PATTERN:Ljava/util/regex/Pattern;

    .line 946
    const-string v4, "\\s*#.*"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->COMMENT_LINE_PATTERN:Ljava/util/regex/Pattern;

    .line 1383
    const-string v4, "\\w[_\\w]*"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, Lcom/google/common/flags/Flags;->FLAG_NAME_PATTERN:Ljava/util/regex/Pattern;

    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 203
    .local v0, e:Ljava/security/AccessControlException;
    sget-object v4, Lcom/google/common/flags/Flags;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v6, "Unable to calculate main class name"

    invoke-virtual {v4, v5, v6, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/Map;
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcom/google/common/flags/Flags;->loadFlagManifests()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static addPreferredClass(Ljava/lang/Class;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1513
    .local p0, clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/flags/Flags;->addPreferredClass(Ljava/lang/String;)V

    .line 1514
    return-void
.end method

.method public static addPreferredClass(Ljava/lang/String;)V
    .locals 1
    .parameter "className"

    .prologue
    .line 1503
    sget-object v0, Lcom/google/common/flags/Flags;->preferredClasses:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1504
    return-void
.end method

.method public static allFlags()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/common/flags/FlagInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1806
    invoke-static {}, Lcom/google/common/flags/Flags;->allFlagsInternal()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private static allFlagsInternal()Ljava/util/Collection;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/common/flags/Flags$FlagInfoImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1767
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1772
    .local v0, allNames:Ljava/util/Map;,"Ljava/util/Map<Lcom/google/common/flags/FlagDescription;Ljava/util/Set<Ljava/lang/String;>;>;"
    invoke-static {}, Lcom/google/common/flags/Flags;->expandedFlagMap()Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1773
    .local v3, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1774
    .local v8, key:Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Set;

    .line 1775
    .local v5, flags:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    .line 1778
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/flags/FlagDescription;

    .line 1779
    .local v1, desc:Lcom/google/common/flags/FlagDescription;
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    .line 1780
    .local v4, flagNames:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v4, :cond_1

    .line 1781
    new-instance v4, Ljava/util/TreeSet;

    .end local v4           #flagNames:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/TreeSet;-><init>()V

    .line 1782
    .restart local v4       #flagNames:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1784
    :cond_1
    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1788
    .end local v1           #desc:Lcom/google/common/flags/FlagDescription;
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;>;"
    .end local v4           #flagNames:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5           #flags:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    .end local v8           #key:Ljava/lang/String;
    :cond_2
    new-instance v10, Ljava/util/TreeSet;

    invoke-direct {v10}, Ljava/util/TreeSet;-><init>()V

    .line 1790
    .local v10, result:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/Flags$FlagInfoImpl;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1791
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/google/common/flags/FlagDescription;Ljava/util/Set<Ljava/lang/String;>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/flags/FlagDescription;

    .line 1792
    .restart local v1       #desc:Lcom/google/common/flags/FlagDescription;
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Collection;

    invoke-direct {v9, v11}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1793
    .local v9, names:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Lcom/google/common/flags/Flags$FlagInfoImpl;

    invoke-direct {v7, v9, v1}, Lcom/google/common/flags/Flags$FlagInfoImpl;-><init>(Ljava/util/List;Lcom/google/common/flags/FlagDescription;)V

    .line 1794
    .local v7, info:Lcom/google/common/flags/Flags$FlagInfoImpl;
    invoke-interface {v10, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1796
    .end local v1           #desc:Lcom/google/common/flags/FlagDescription;
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/google/common/flags/FlagDescription;Ljava/util/Set<Ljava/lang/String;>;>;"
    .end local v7           #info:Lcom/google/common/flags/Flags$FlagInfoImpl;
    .end local v9           #names:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    return-object v10
.end method

.method private static appendExternalFlags(Ljava/io/BufferedReader;Ljava/util/List;)V
    .locals 3
    .parameter "reader"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/BufferedReader;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/common/flags/InvalidFlagSyntaxException;,
            Lcom/google/common/flags/ExternalFlagsLoadException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 963
    .local p1, collatedArgs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 964
    .local v1, line:Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_4

    .line 965
    sget-object v2, Lcom/google/common/flags/Flags;->FLAG_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 966
    sget-object v2, Lcom/google/common/flags/Flags;->FLAG_FILE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/common/flags/Flags;->FLAG_RESOURCE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 968
    :cond_0
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 969
    .local v0, fileFlag:[Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/flags/Flags;->loadExternalFlags([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 977
    .end local v0           #fileFlag:[Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-virtual {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 971
    :cond_2
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 973
    :cond_3
    sget-object v2, Lcom/google/common/flags/Flags;->BLANK_LINE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/common/flags/Flags;->COMMENT_LINE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_1

    .line 975
    new-instance v2, Lcom/google/common/flags/InvalidFlagSyntaxException;

    invoke-direct {v2, v1}, Lcom/google/common/flags/InvalidFlagSyntaxException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 979
    :cond_4
    return-void
.end method

.method private static appendExternalFlagsFromFile(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter "file"
    .parameter "arg"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/common/flags/InvalidFlagSyntaxException;,
            Ljava/io/IOException;,
            Lcom/google/common/flags/ExternalFlagsLoadException;
        }
    .end annotation

    .prologue
    .line 931
    .local p0, collatedArgs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 933
    .local v1, reader:Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 934
    .end local v1           #reader:Ljava/io/BufferedReader;
    .local v2, reader:Ljava/io/BufferedReader;
    :try_start_1
    invoke-static {v2, p0}, Lcom/google/common/flags/Flags;->appendExternalFlags(Ljava/io/BufferedReader;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 938
    if-eqz v2, :cond_0

    .line 939
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 942
    :cond_0
    return-void

    .line 935
    .end local v2           #reader:Ljava/io/BufferedReader;
    .restart local v1       #reader:Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 936
    .local v0, exc:Ljava/io/FileNotFoundException;
    :goto_0
    :try_start_2
    new-instance v3, Lcom/google/common/flags/ExternalFlagsLoadException;

    invoke-direct {v3, p2}, Lcom/google/common/flags/ExternalFlagsLoadException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 938
    .end local v0           #exc:Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v3

    :goto_1
    if-eqz v1, :cond_1

    .line 939
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    :cond_1
    throw v3

    .line 938
    .end local v1           #reader:Ljava/io/BufferedReader;
    .restart local v2       #reader:Ljava/io/BufferedReader;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2           #reader:Ljava/io/BufferedReader;
    .restart local v1       #reader:Ljava/io/BufferedReader;
    goto :goto_1

    .line 935
    .end local v1           #reader:Ljava/io/BufferedReader;
    .restart local v2       #reader:Ljava/io/BufferedReader;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2           #reader:Ljava/io/BufferedReader;
    .restart local v1       #reader:Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method private static appendExternalFlagsFromResource(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter "resource"
    .parameter "arg"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/common/flags/ExternalFlagsLoadException;,
            Lcom/google/common/flags/InvalidFlagSyntaxException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 907
    .local p0, collatedArgs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-class v3, Lcom/google/common/flags/Flags;

    invoke-virtual {v3, p1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 909
    .local v2, resourceStream:Ljava/io/InputStream;
    if-nez v2, :cond_0

    .line 912
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 914
    .local v0, contextClassLoader:Ljava/lang/ClassLoader;
    if-eqz v0, :cond_0

    .line 915
    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 918
    .end local v0           #contextClassLoader:Ljava/lang/ClassLoader;
    :cond_0
    if-nez v2, :cond_1

    .line 919
    new-instance v3, Lcom/google/common/flags/ExternalFlagsLoadException;

    invoke-direct {v3, p2}, Lcom/google/common/flags/ExternalFlagsLoadException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 922
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 923
    .local v1, reader:Ljava/io/BufferedReader;
    invoke-static {v1, p0}, Lcom/google/common/flags/Flags;->appendExternalFlags(Ljava/io/BufferedReader;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 925
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 927
    return-void

    .line 925
    .end local v1           #reader:Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v3
.end method

.method static canonicalFlagMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/flags/FlagDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->canonicalFlagMap:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 243
    invoke-static {}, Lcom/google/common/flags/Flags;->initMaps()V

    .line 245
    :cond_0
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->canonicalFlagMap:Ljava/util/Map;

    return-object v0
.end method

.method public static clearFlagData()V
    .locals 2

    .prologue
    .line 338
    sget-object v0, Lcom/google/common/flags/Flags;->parseState:Lcom/google/common/flags/Flags$ParseState;

    sget-object v1, Lcom/google/common/flags/Flags$ParseState;->DONE:Lcom/google/common/flags/Flags$ParseState;

    if-eq v0, v1, :cond_0

    .line 339
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Flag parsing must be completed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_0
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->manuallyRegisteredFlags:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 342
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->canonicalFlagMap:Ljava/util/Map;

    .line 343
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->expandedFlagMap:Ljava/util/Map;

    .line 344
    const/4 v0, 0x0

    sput-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->whitelistedPrefixes:Ljava/util/Collection;

    .line 345
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->longNameMap:Ljava/util/Map;

    .line 346
    return-void
.end method

.method static clearFlagMapsForTesting()V
    .locals 1

    .prologue
    .line 315
    invoke-static {}, Lcom/google/common/flags/Flags;->reallyClearFlagMapsForTesting()V

    .line 316
    invoke-static {}, Lcom/google/common/flags/Flags;->loadFlagManifests()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->longNameMap:Ljava/util/Map;

    .line 317
    return-void
.end method

.method static createCanonicalFlagMap(Ljava/util/Collection;Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/common/flags/FlagDescription;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/flags/FlagDescription;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/flags/FlagDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 539
    .local p0, flags:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/common/flags/FlagDescription;>;"
    .local p1, expandedFlagMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 542
    .local v0, canonicalMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/flags/FlagDescription;

    .line 543
    .local v1, flag:Lcom/google/common/flags/FlagDescription;
    invoke-static {v1, p1}, Lcom/google/common/flags/Flags;->getBestFlagName(Lcom/google/common/flags/FlagDescription;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 546
    .end local v1           #flag:Lcom/google/common/flags/FlagDescription;
    :cond_0
    return-object v0
.end method

.method public static disableStateCheckingForTest()V
    .locals 3

    .prologue
    .line 1579
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v0

    .line 1580
    .local v0, props:Ljava/util/Properties;
    const-string v1, "com.google.common.flags.disableStateChecking"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 1581
    return-void
.end method

.method public static disableUndefOkSupport()V
    .locals 1

    .prologue
    .line 1856
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/common/flags/Flags;->undefOkSupported:Z

    .line 1857
    return-void
.end method

.method public static enableStateCheckingForTest()V
    .locals 3

    .prologue
    .line 1589
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v0

    .line 1590
    .local v0, props:Ljava/util/Properties;
    const-string v1, "com.google.common.flags.disableStateChecking"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 1591
    return-void
.end method

.method public static enableUndefOkSupport()V
    .locals 1

    .prologue
    .line 1849
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/common/flags/Flags;->undefOkSupported:Z

    .line 1850
    return-void
.end method

.method private static exitUnlessDisabled(I)V
    .locals 1
    .parameter "status"

    .prologue
    .line 708
    const-string v0, "com.google.common.flags.noExit"

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 709
    invoke-static {p0}, Ljava/lang/System;->exit(I)V

    .line 711
    :cond_0
    return-void
.end method

.method static expandFlagMap(Ljava/util/Collection;)Ljava/util/Map;
    .locals 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/common/flags/FlagDescription;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/flags/FlagDescription;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p0, flags:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/common/flags/FlagDescription;>;"
    const/4 v14, 0x1

    .line 501
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 504
    .local v1, allNames:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/common/flags/FlagDescription;

    .line 505
    .local v5, flag:Lcom/google/common/flags/FlagDescription;
    invoke-static {v5}, Lcom/google/common/flags/Flags;->getAllNamesForFlag(Lcom/google/common/flags/FlagDescription;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 506
    .local v0, alias:Ljava/lang/String;
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    .line 507
    .local v4, dups:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    if-nez v4, :cond_1

    .line 508
    new-instance v4, Ljava/util/HashSet;

    .end local v4           #dups:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .restart local v4       #dups:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    :cond_1
    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 515
    .end local v0           #alias:Ljava/lang/String;
    .end local v4           #dups:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    .end local v5           #flag:Lcom/google/common/flags/FlagDescription;
    .end local v7           #i$:Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 516
    .local v8, key:Ljava/lang/String;
    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    .line 517
    .restart local v4       #dups:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v9

    if-le v9, v14, :cond_3

    .line 518
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 519
    .local v2, b:Ljava/lang/StringBuilder;
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7       #i$:Ljava/util/Iterator;
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/common/flags/FlagDescription;

    .line 520
    .local v3, d:Lcom/google/common/flags/FlagDescription;
    invoke-virtual {v3}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 521
    const/16 v9, 0x2c

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 523
    .end local v3           #d:Lcom/google/common/flags/FlagDescription;
    :cond_4
    sget-object v9, Lcom/google/common/flags/Flags;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v11, "Flag {0} is not a unique short form because {1} flags use it: {2}"

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v8, v12, v13

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v14

    const/4 v13, 0x2

    aput-object v2, v12, v13

    invoke-virtual {v9, v10, v11, v12}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 529
    .end local v2           #b:Ljava/lang/StringBuilder;
    .end local v4           #dups:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v8           #key:Ljava/lang/String;
    :cond_5
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    return-object v9
.end method

.method private static expandedFlagMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/flags/FlagDescription;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 253
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->expandedFlagMap:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 254
    invoke-static {}, Lcom/google/common/flags/Flags;->initMaps()V

    .line 256
    :cond_0
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->expandedFlagMap:Ljava/util/Map;

    return-object v0
.end method

.method public static exposedFlags()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/common/flags/FlagInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1819
    invoke-static {}, Lcom/google/common/flags/Flags;->allFlagsInternal()Ljava/util/Collection;

    move-result-object v0

    .line 1820
    .local v0, allFlags:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/common/flags/Flags$FlagInfoImpl;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/google/common/flags/Flags$FlagInfoImpl;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1821
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/flags/Flags$FlagInfoImpl;

    .line 1822
    .local v1, flag:Lcom/google/common/flags/Flags$FlagInfoImpl;
    iget-object v3, v1, Lcom/google/common/flags/Flags$FlagInfoImpl;->desc:Lcom/google/common/flags/FlagDescription;

    invoke-virtual {v3}, Lcom/google/common/flags/FlagDescription;->getDocLevel()Lcom/google/common/flags/DocLevel;

    move-result-object v3

    sget-object v4, Lcom/google/common/flags/DocLevel;->PUBLIC:Lcom/google/common/flags/DocLevel;

    if-ne v3, v4, :cond_1

    iget-object v3, v1, Lcom/google/common/flags/Flags$FlagInfoImpl;->desc:Lcom/google/common/flags/FlagDescription;

    invoke-static {v3}, Lcom/google/common/flags/Flags;->whitelistAllowsAliasing(Lcom/google/common/flags/FlagDescription;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1824
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1826
    .end local v1           #flag:Lcom/google/common/flags/Flags$FlagInfoImpl;
    :cond_2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v3

    return-object v3
.end method

.method static flag(Lcom/google/common/flags/FlagDescription;)Lcom/google/common/flags/Flag;
    .locals 7
    .parameter "desc"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/flags/FlagDescription;",
            ")",
            "Lcom/google/common/flags/Flag",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1040
    invoke-static {}, Lcom/google/common/flags/Flags;->manuallyRegisteredFlags()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/common/flags/Flag;

    .line 1041
    .local v3, flag:Lcom/google/common/flags/Flag;,"Lcom/google/common/flags/Flag<*>;"
    if-eqz v3, :cond_0

    .line 1050
    .end local v3           #flag:Lcom/google/common/flags/Flag;,"Lcom/google/common/flags/Flag<*>;"
    :goto_0
    return-object v3

    .line 1047
    .restart local v3       #flag:Lcom/google/common/flags/Flag;,"Lcom/google/common/flags/Flag<*>;"
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getContainerClassName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/flags/Flags;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1048
    .local v0, containerClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getSimpleFieldName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 1049
    .local v2, field:Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 1050
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/common/flags/Flag;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_4

    move-object v3, v4

    goto :goto_0

    .line 1051
    .end local v0           #containerClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #field:Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 1052
    .local v1, ex:Ljava/lang/ClassNotFoundException;
    new-instance v4, Ljava/lang/LinkageError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Class for flag field "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getFullyQualifiedFieldName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " present in manifest, absent at runtime: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/LinkageError;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1056
    .end local v1           #ex:Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v1

    .line 1057
    .local v1, ex:Ljava/lang/NoSuchFieldException;
    new-instance v4, Ljava/lang/LinkageError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Flag field "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getFullyQualifiedFieldName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " present in manifest, absent at runtime: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/NoSuchFieldException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/LinkageError;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1060
    .end local v1           #ex:Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v1

    .line 1061
    .local v1, ex:Ljava/lang/IllegalAccessException;
    new-instance v4, Ljava/lang/LinkageError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to get flag field "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getFullyQualifiedFieldName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/LinkageError;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1064
    .end local v1           #ex:Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 1066
    .local v1, ex:Ljava/lang/NullPointerException;
    new-instance v4, Ljava/lang/LinkageError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Forgot to make the flag static? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getFullyQualifiedFieldName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/LinkageError;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1068
    .end local v1           #ex:Ljava/lang/NullPointerException;
    :catch_4
    move-exception v1

    .line 1069
    .local v1, ex:Ljava/lang/ClassCastException;
    new-instance v4, Ljava/lang/LinkageError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot convert field "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getSimpleFieldName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to a Flag in order to resolve "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getFullyQualifiedFieldName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/ClassCastException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/LinkageError;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private static formatFlag(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter "printStream"
    .parameter "flagName"
    .parameter "flagDescription"
    .parameter "defaultString"

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x46

    .line 1110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1111
    .local v1, b:Ljava/lang/StringBuilder;
    const-string v3, "   --"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1112
    if-eqz p3, :cond_0

    .line 1113
    const-string v3, "; default: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1116
    :cond_0
    const/16 v0, 0x46

    .line 1117
    .local v0, LINE_LIMIT:I
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-le v3, v5, :cond_2

    .line 1118
    const-string v3, " "

    invoke-virtual {v1, v3, v5}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;I)I

    move-result v2

    .line 1119
    .local v2, space:I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    const-string v3, " "

    invoke-virtual {v1, v3, v5}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 1120
    :cond_1
    const/16 v3, 0xa

    if-ge v2, v3, :cond_4

    .line 1124
    .end local v2           #space:I
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 1125
    :cond_3
    return-void

    .line 1121
    .restart local v2       #space:I
    :cond_4
    invoke-virtual {v1, v6, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1122
    add-int/lit8 v3, v2, 0x1

    const-string v4, "      "

    invoke-virtual {v1, v6, v3, v4}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getAllFlags()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1673
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    .line 1674
    .local v4, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {}, Lcom/google/common/flags/Flags;->canonicalFlagMap()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1675
    .local v1, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1676
    .local v3, key:Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/flags/FlagDescription;

    .line 1677
    .local v0, desc:Lcom/google/common/flags/FlagDescription;
    const/16 v5, 0x2e

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-gez v5, :cond_0

    .line 1680
    invoke-static {v0}, Lcom/google/common/flags/Flags;->flag(Lcom/google/common/flags/FlagDescription;)Lcom/google/common/flags/Flag;

    move-result-object v5

    iget-object v5, v5, Lcom/google/common/flags/Flag;->value:Ljava/lang/Object;

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1682
    .end local v0           #desc:Lcom/google/common/flags/FlagDescription;
    .end local v1           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    .end local v3           #key:Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method static getAllNamesForFlag(Lcom/google/common/flags/FlagDescription;)Ljava/util/List;
    .locals 3
    .parameter "flag"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/flags/FlagDescription;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 431
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 435
    .local v0, aliases:Ljava/util/LinkedHashSet;,"Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/google/common/flags/Flags;->whitelistAllowsAliasing(Lcom/google/common/flags/FlagDescription;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 437
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getAltName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getAltName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 442
    :cond_0
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getShortFlagName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 445
    :cond_1
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 449
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->isShortFlagNameSpecified()Z

    move-result v1

    if-nez v1, :cond_2

    .line 450
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getFullyQualifiedFieldName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 454
    :cond_2
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "java.lang.Boolean"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 455
    invoke-static {v0}, Lcom/google/common/flags/Flags;->getNoPrefixedAliases(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 458
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method public static getAllowedFlags()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1490
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->whitelistedPrefixes:Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1491
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    sget-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->whitelistedPrefixes:Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private static getAsString(Lcom/google/common/flags/Flag;)Ljava/lang/String;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/flags/Flag",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1310
    .local p0, flag:Lcom/google/common/flags/Flag;,"Lcom/google/common/flags/Flag<*>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/google/common/flags/Flag;->parsableStringValue()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1312
    :goto_0
    return-object v1

    .line 1311
    :catch_0
    move-exception v0

    .line 1312
    .local v0, uoe:Ljava/lang/UnsupportedOperationException;
    invoke-virtual {p0}, Lcom/google/common/flags/Flag;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/flags/Flags;->valueToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getBestFlag(Ljava/lang/String;)Lcom/google/common/flags/FlagDescription;
    .locals 7
    .parameter "flag"

    .prologue
    const/4 v4, 0x0

    .line 260
    invoke-static {}, Lcom/google/common/flags/Flags;->expandedFlagMap()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 261
    .local v0, allDesc:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    if-nez v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-object v4

    .line 264
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 265
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/common/flags/FlagDescription;

    goto :goto_0

    .line 267
    :cond_2
    const/4 v3, 0x0

    .line 268
    .local v3, result:Lcom/google/common/flags/FlagDescription;
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/flags/FlagDescription;

    .line 269
    .local v1, desc:Lcom/google/common/flags/FlagDescription;
    sget-object v5, Lcom/google/common/flags/Flags;->preferredClasses:Ljava/util/Set;

    invoke-virtual {v1}, Lcom/google/common/flags/FlagDescription;->getContainerClassName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 270
    if-nez v3, :cond_0

    .line 273
    move-object v3, v1

    goto :goto_1

    .end local v1           #desc:Lcom/google/common/flags/FlagDescription;
    :cond_4
    move-object v4, v3

    .line 276
    goto :goto_0
.end method

.method private static getBestFlagName(Lcom/google/common/flags/FlagDescription;Ljava/util/Map;)Ljava/lang/String;
    .locals 5
    .parameter "flag"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/flags/FlagDescription;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/flags/FlagDescription;",
            ">;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 559
    .local p1, expandedFlagMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;>;"
    invoke-static {p0}, Lcom/google/common/flags/Flags;->getAllNamesForFlag(Lcom/google/common/flags/FlagDescription;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 561
    .local v0, alias:Ljava/lang/String;
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 563
    .local v1, descs:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 564
    return-object v0

    .line 568
    .end local v0           #alias:Ljava/lang/String;
    .end local v1           #descs:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "Must be one unique name for flag?!"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3
.end method

.method private static getDefaultAsString(Lcom/google/common/flags/Flag;)Ljava/lang/String;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/flags/Flag",
            "<TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1293
    .local p0, flag:Lcom/google/common/flags/Flag;,"Lcom/google/common/flags/Flag<TT;>;"
    invoke-virtual {p0}, Lcom/google/common/flags/Flag;->getDefault()Ljava/lang/Object;

    move-result-object v0

    .line 1294
    .local v0, defaultValue:Ljava/lang/Object;,"TT;"
    if-nez v0, :cond_0

    .line 1295
    const/4 v2, 0x0

    .line 1300
    :goto_0
    return-object v2

    .line 1298
    :cond_0
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/common/flags/Flag;->parsableStringValue(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1299
    :catch_0
    move-exception v1

    .line 1300
    .local v1, uoe:Ljava/lang/UnsupportedOperationException;
    invoke-static {v0}, Lcom/google/common/flags/Flags;->valueToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static getFlagManifestClassLoader()Ljava/lang/ClassLoader;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 398
    const-string v3, "com.google.common.flags.classLoader"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400
    .local v0, className:Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 402
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ClassLoader;

    .line 404
    .local v2, loader:Ljava/lang/ClassLoader;
    sget-object v3, Lcom/google/common/flags/Flags;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "User specified classloader: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 418
    .end local v2           #loader:Ljava/lang/ClassLoader;
    :goto_0
    return-object v2

    .line 406
    :catch_0
    move-exception v1

    .line 407
    .local v1, e:Ljava/lang/InstantiationException;
    sget-object v3, Lcom/google/common/flags/Flags;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "Couldn\'t instantiate ClassLoader \'%s\'"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 418
    .end local v1           #e:Ljava/lang/InstantiationException;
    :cond_0
    :goto_1
    const-class v3, Lcom/google/common/flags/Flags;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    goto :goto_0

    .line 409
    :catch_1
    move-exception v1

    .line 410
    .local v1, e:Ljava/lang/IllegalAccessException;
    sget-object v3, Lcom/google/common/flags/Flags;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "Couldn\'t instantiate ClassLoader \'%s\'"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 412
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 413
    .local v1, e:Ljava/lang/ClassNotFoundException;
    sget-object v3, Lcom/google/common/flags/Flags;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "Couldn\'t instantiate ClassLoader \'%s\'"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static getMainClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1263
    sget-object v0, Lcom/google/common/flags/Flags;->cachedMainClassName:Ljava/lang/String;

    return-object v0
.end method

.method private static getMainClassNameFromStackTrace([Ljava/lang/StackTraceElement;)Ljava/lang/String;
    .locals 3
    .parameter "stack"

    .prologue
    .line 215
    array-length v2, p0

    if-lez v2, :cond_1

    .line 216
    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    aget-object v1, p0, v2

    .line 217
    .local v1, stackRoot:Ljava/lang/StackTraceElement;
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, methodName:Ljava/lang/String;
    const-string v2, "main"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "<clinit>"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 219
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 222
    .end local v0           #methodName:Ljava/lang/String;
    .end local v1           #stackRoot:Ljava/lang/StackTraceElement;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getNoPrefixedAliases(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 462
    .local p0, names:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 463
    .local v2, noPrefixedAliases:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 464
    .local v1, name:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 466
    .end local v1           #name:Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method private static getProgramName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1365
    const-string v1, "unknown"

    .line 1366
    .local v1, progname:Ljava/lang/String;
    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    .line 1367
    .local v0, frames:[Ljava/lang/StackTraceElement;
    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    .line 1368
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 1370
    :cond_0
    return-object v1
.end method

.method private static getStackTraceAsString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2
    .parameter "t"

    .prologue
    .line 1833
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1834
    .local v0, stringWriter:Ljava/io/StringWriter;
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 1835
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getUnknownFlags(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 840
    .local p0, unrecognizedFlags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p1, acceptableUnrecognizedFlags:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 841
    .local v2, unknownFlags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 842
    .local v0, flagName:Ljava/lang/String;
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "no"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 845
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 848
    .end local v0           #flagName:Ljava/lang/String;
    :cond_2
    return-object v2
.end method

.method public static getUsagePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1186
    sget-object v0, Lcom/google/common/flags/Flags;->usagePrefix:Ljava/lang/String;

    return-object v0
.end method

.method private static initMaps()V
    .locals 2

    .prologue
    .line 354
    invoke-static {}, Lcom/google/common/flags/Flags;->longNameMap()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 355
    .local v0, allflags:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/common/flags/FlagDescription;>;"
    invoke-static {v0}, Lcom/google/common/flags/Flags;->expandFlagMap(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    sput-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->expandedFlagMap:Ljava/util/Map;

    .line 356
    sget-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->expandedFlagMap:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/google/common/flags/Flags;->createCanonicalFlagMap(Ljava/util/Collection;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    sput-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->canonicalFlagMap:Ljava/util/Map;

    .line 358
    return-void
.end method

.method private static isAllowedUndefOkFlag(Ljava/lang/String;)Z
    .locals 1
    .parameter "flagName"

    .prologue
    .line 832
    invoke-static {}, Lcom/google/common/flags/Flags;->isUndefOkSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "undefok"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isBooleanFlag(Lcom/google/common/flags/FlagDescription;)Z
    .locals 2
    .parameter "desc"

    .prologue
    .line 1023
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.Boolean"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isLegitimateFlag(Lcom/google/common/flags/FlagDescription;)Z
    .locals 1
    .parameter "description"

    .prologue
    .line 1028
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUndefOkSupported()Z
    .locals 1

    .prologue
    .line 1842
    sget-boolean v0, Lcom/google/common/flags/Flags;->undefOkSupported:Z

    return v0
.end method

.method private static loadClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 1085
    :goto_0
    :try_start_0
    sget-object v2, Lcom/google/common/flags/Flags;->flagClassLoader:Ljava/lang/ClassLoader;

    if-eqz v2, :cond_0

    .line 1086
    const/4 v2, 0x1

    sget-object v3, Lcom/google/common/flags/Flags;->flagClassLoader:Ljava/lang/ClassLoader;

    invoke-static {p0, v2, v3}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    .line 1088
    :goto_1
    return-object v2

    :cond_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_1

    .line 1090
    :catch_0
    move-exception v0

    .line 1091
    .local v0, e:Ljava/lang/ClassNotFoundException;
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 1092
    .local v1, idx:I
    if-gez v1, :cond_1

    .line 1093
    throw v0

    .line 1097
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "$"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1099
    goto :goto_0
.end method

.method private static loadExternalFlags([Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/common/flags/InvalidFlagSyntaxException;,
            Lcom/google/common/flags/ExternalFlagsLoadException;
        }
    .end annotation

    .prologue
    .line 879
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 881
    .local v2, collatedArgs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    move-object v1, p0

    .local v1, arr$:[Ljava/lang/String;
    array-length v7, v1

    .local v7, len$:I
    const/4 v6, 0x0

    .local v6, i$:I
    :goto_0
    if-ge v6, v7, :cond_2

    aget-object v0, v1, v6

    .line 883
    .local v0, arg:Ljava/lang/String;
    :try_start_0
    sget-object v10, Lcom/google/common/flags/Flags;->FLAG_FILE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 884
    .local v5, fileMatcher:Ljava/util/regex/Matcher;
    sget-object v10, Lcom/google/common/flags/Flags;->FLAG_RESOURCE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 886
    .local v9, resourceMatcher:Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 888
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 889
    .local v4, file:Ljava/lang/String;
    invoke-static {v2, v4, v0}, Lcom/google/common/flags/Flags;->appendExternalFlagsFromFile(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    .end local v4           #file:Ljava/lang/String;
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 890
    :cond_0
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 892
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 893
    .local v8, resource:Ljava/lang/String;
    invoke-static {v2, v8, v0}, Lcom/google/common/flags/Flags;->appendExternalFlagsFromResource(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 898
    .end local v5           #fileMatcher:Ljava/util/regex/Matcher;
    .end local v8           #resource:Ljava/lang/String;
    .end local v9           #resourceMatcher:Ljava/util/regex/Matcher;
    :catch_0
    move-exception v3

    .line 899
    .local v3, exc:Ljava/io/IOException;
    new-instance v10, Lcom/google/common/flags/ExternalFlagsLoadException;

    invoke-direct {v10, v0}, Lcom/google/common/flags/ExternalFlagsLoadException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 896
    .end local v3           #exc:Ljava/io/IOException;
    .restart local v5       #fileMatcher:Ljava/util/regex/Matcher;
    .restart local v9       #resourceMatcher:Ljava/util/regex/Matcher;
    :cond_1
    :try_start_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 902
    .end local v0           #arg:Ljava/lang/String;
    .end local v5           #fileMatcher:Ljava/util/regex/Matcher;
    .end local v9           #resourceMatcher:Ljava/util/regex/Matcher;
    :cond_2
    sget-object v10, Lcom/google/common/flags/Flags;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    invoke-interface {v2, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    return-object v10
.end method

.method private static loadFlagManifests()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/flags/FlagDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 368
    .local v3, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    invoke-static {}, Lcom/google/common/flags/Flags;->getFlagManifestClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    .line 371
    .local v4, myLoader:Ljava/lang/ClassLoader;
    :try_start_0
    const-string v6, "flags.xml"

    invoke-virtual {v4, v6}, Ljava/lang/ClassLoader;->getResources(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v1

    .line 372
    .local v1, flagURLs:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/net/URL;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 373
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/URL;

    .line 374
    .local v5, url:Ljava/net/URL;
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-virtual {v5}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    .local v2, is:Ljava/io/InputStream;
    :try_start_1
    invoke-static {v2, v3}, Lcom/google/common/flags/XmlSupport;->fromXml(Ljava/io/InputStream;Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lcom/google/common/flags/MalformedFlagDescriptionException; {:try_start_1 .. :try_end_1} :catch_1

    .line 381
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 384
    .end local v1           #flagURLs:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/net/URL;>;"
    .end local v2           #is:Ljava/io/InputStream;
    .end local v5           #url:Ljava/net/URL;
    :catch_0
    move-exception v0

    .line 385
    .local v0, ex:Ljava/io/IOException;
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 377
    .end local v0           #ex:Ljava/io/IOException;
    .restart local v1       #flagURLs:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/net/URL;>;"
    .restart local v2       #is:Ljava/io/InputStream;
    .restart local v5       #url:Ljava/net/URL;
    :catch_1
    move-exception v0

    .line 378
    .local v0, ex:Lcom/google/common/flags/MalformedFlagDescriptionException;
    :try_start_3
    new-instance v6, Lcom/google/common/flags/MalformedFlagDescriptionException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to parse "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lcom/google/common/flags/MalformedFlagDescriptionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 381
    .end local v0           #ex:Lcom/google/common/flags/MalformedFlagDescriptionException;
    :catchall_0
    move-exception v6

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v6
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 387
    .end local v2           #is:Ljava/io/InputStream;
    .end local v5           #url:Ljava/net/URL;
    :cond_0
    return-object v3
.end method

.method private static longNameMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/flags/FlagDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->longNameMap:Ljava/util/Map;

    return-object v0
.end method

.method private static manuallyRegisteredFlags()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/flags/Flag",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 285
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->manuallyRegisteredFlags:Ljava/util/Map;

    return-object v0
.end method

.method private static maybeSyslogOnStart([Ljava/lang/String;)V
    .locals 24
    .parameter "args"

    .prologue
    .line 579
    :try_start_0
    invoke-static {}, Lcom/google/common/flags/Flags;->getMainClassName()Ljava/lang/String;

    move-result-object v13

    .line 580
    .local v13, mainClassName:Ljava/lang/String;
    if-nez v13, :cond_1

    .line 638
    .end local v13           #mainClassName:Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 584
    .restart local v13       #mainClassName:Ljava/lang/String;
    :cond_1
    invoke-static {}, Ljava/net/InetAddress;->getLocalHost()Ljava/net/InetAddress;

    move-result-object v2

    .line 585
    .local v2, addr:Ljava/net/InetAddress;
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v9

    .line 586
    .local v9, hostname:Ljava/lang/String;
    const-string v19, ".corp.google.com"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 591
    const-string v19, "com.sun.security.auth.module.UnixSystem"

    invoke-static/range {v19 .. v19}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    .line 592
    .local v6, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {v6}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v8

    .line 593
    .local v8, clsObj:Ljava/lang/Object;
    const-string v19, "getUid"

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 594
    .local v7, clsMeth:Ljava/lang/reflect/Method;
    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v7, v8, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    .line 595
    .local v16, result:Ljava/lang/Object;
    move-object/from16 v0, v16

    check-cast v0, Ljava/lang/Long;

    move-object/from16 v18, v0

    .line 598
    .local v18, uid:Ljava/lang/Long;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " log_path:\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " language:\"java\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " tool_type:\"cmdline\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " uid:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " host_name:\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " log_timestamp:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    const-wide/16 v22, 0x3e8

    div-long v20, v20, v22

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " logger:\"logger_java\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 611
    .local v17, toolLogProtoStr:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 612
    .local v4, argvStr:Ljava/lang/StringBuilder;
    move-object/from16 v5, p0

    .local v5, arr$:[Ljava/lang/String;
    array-length v11, v5

    .local v11, len$:I
    const/4 v10, 0x0

    .local v10, i$:I
    :goto_1
    if-ge v10, v11, :cond_2

    aget-object v3, v5, v10

    .line 613
    .local v3, argv:Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " argv:\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 618
    .end local v3           #argv:Ljava/lang/String;
    :cond_2
    const-string v12, "/usr/lib/crudd/log_usage"

    .line 620
    .local v12, loggerPath:Ljava/lang/String;
    new-instance v19, Ljava/io/File;

    const-string v20, "/usr/lib/crudd/log_usage"

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 621
    new-instance v15, Ljava/lang/ProcessBuilder;

    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "/usr/lib/crudd/log_usage"

    aput-object v21, v19, v20

    const/16 v20, 0x1

    const-string v21, "--tool_log_proto"

    aput-object v21, v19, v20

    const/16 v20, 0x2

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v19

    invoke-direct {v15, v0}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    .line 631
    .local v15, pb:Ljava/lang/ProcessBuilder;
    :goto_2
    invoke-virtual {v15}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v14

    .line 632
    .local v14, p:Ljava/lang/Process;
    invoke-virtual {v14}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    .line 633
    invoke-virtual {v14}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/io/OutputStream;->close()V

    .line 634
    invoke-virtual {v14}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 635
    .end local v2           #addr:Ljava/net/InetAddress;
    .end local v4           #argvStr:Ljava/lang/StringBuilder;
    .end local v5           #arr$:[Ljava/lang/String;
    .end local v6           #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v7           #clsMeth:Ljava/lang/reflect/Method;
    .end local v8           #clsObj:Ljava/lang/Object;
    .end local v9           #hostname:Ljava/lang/String;
    .end local v10           #i$:I
    .end local v11           #len$:I
    .end local v12           #loggerPath:Ljava/lang/String;
    .end local v13           #mainClassName:Ljava/lang/String;
    .end local v14           #p:Ljava/lang/Process;
    .end local v15           #pb:Ljava/lang/ProcessBuilder;
    .end local v16           #result:Ljava/lang/Object;
    .end local v17           #toolLogProtoStr:Ljava/lang/String;
    .end local v18           #uid:Ljava/lang/Long;
    :catch_0
    move-exception v19

    goto/16 :goto_0

    .line 627
    .restart local v2       #addr:Ljava/net/InetAddress;
    .restart local v4       #argvStr:Ljava/lang/StringBuilder;
    .restart local v5       #arr$:[Ljava/lang/String;
    .restart local v6       #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .restart local v7       #clsMeth:Ljava/lang/reflect/Method;
    .restart local v8       #clsObj:Ljava/lang/Object;
    .restart local v9       #hostname:Ljava/lang/String;
    .restart local v10       #i$:I
    .restart local v11       #len$:I
    .restart local v12       #loggerPath:Ljava/lang/String;
    .restart local v13       #mainClassName:Ljava/lang/String;
    .restart local v16       #result:Ljava/lang/Object;
    .restart local v17       #toolLogProtoStr:Ljava/lang/String;
    .restart local v18       #uid:Ljava/lang/Long;
    :cond_3
    new-instance v15, Ljava/lang/ProcessBuilder;

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "/usr/bin/logger"

    aput-object v21, v19, v20

    const/16 v20, 0x1

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "ToolLogProto: logjam_tag=tattler_initgoogle "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v19

    invoke-direct {v15, v0}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v15       #pb:Ljava/lang/ProcessBuilder;
    goto :goto_2
.end method

.method public static parse([Ljava/lang/String;)[Ljava/lang/String;
    .locals 10
    .parameter "args"

    .prologue
    .line 655
    const/4 v6, 0x0

    .line 657
    .local v6, result:[Ljava/lang/String;
    invoke-static {p0}, Lcom/google/common/flags/Flags;->maybeSyslogOnStart([Ljava/lang/String;)V

    .line 659
    :try_start_0
    sget-object v7, Lcom/google/common/flags/Flags$ParseState;->IN_PROGRESS:Lcom/google/common/flags/Flags$ParseState;

    invoke-static {v7}, Lcom/google/common/flags/Flags;->setParseState(Lcom/google/common/flags/Flags$ParseState;)V

    .line 660
    invoke-static {p0}, Lcom/google/common/flags/Flags;->parseInternal([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 661
    const-class v8, Lcom/google/common/flags/Flags;

    monitor-enter v8
    :try_end_0
    .catch Lcom/google/common/flags/FlagException; {:try_start_0 .. :try_end_0} :catch_0

    .line 662
    :try_start_1
    sget-object v7, Lcom/google/common/flags/Flags$ParseState;->DONE:Lcom/google/common/flags/Flags$ParseState;

    invoke-static {v7}, Lcom/google/common/flags/Flags;->setParseState(Lcom/google/common/flags/Flags$ParseState;)V

    .line 663
    sget-object v7, Lcom/google/common/flags/Flags;->completionHooks:Ljava/util/Collection;

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Runnable;

    invoke-interface {v7, v9}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Runnable;

    .line 664
    .local v1, completionHooks:[Ljava/lang/Runnable;
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 670
    move-object v0, v1

    .local v0, arr$:[Ljava/lang/Runnable;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 671
    .local v5, r:Ljava/lang/Runnable;
    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    .line 670
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 664
    .end local v0           #arr$:[Ljava/lang/Runnable;
    .end local v1           #completionHooks:[Ljava/lang/Runnable;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v5           #r:Ljava/lang/Runnable;
    :catchall_0
    move-exception v7

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v7
    :try_end_3
    .catch Lcom/google/common/flags/FlagException; {:try_start_3 .. :try_end_3} :catch_0

    .line 665
    :catch_0
    move-exception v2

    .line 666
    .local v2, ex:Lcom/google/common/flags/FlagException;
    new-instance v7, Lcom/google/common/flags/InvalidFlagsException;

    invoke-virtual {v2}, Lcom/google/common/flags/FlagException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v2}, Lcom/google/common/flags/InvalidFlagsException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 674
    .end local v2           #ex:Lcom/google/common/flags/FlagException;
    .restart local v0       #arr$:[Ljava/lang/Runnable;
    .restart local v1       #completionHooks:[Ljava/lang/Runnable;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    :cond_0
    return-object v6
.end method

.method private static parseInternal([Ljava/lang/String;)[Ljava/lang/String;
    .locals 21
    .parameter "commandLineArgs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/common/flags/AmbiguousFlagException;,
            Lcom/google/common/flags/ExternalFlagsLoadException;,
            Lcom/google/common/flags/InvalidFlagSyntaxException;,
            Lcom/google/common/flags/InvalidFlagValueException;,
            Lcom/google/common/flags/UnrecognizedFlagException;
        }
    .end annotation

    .prologue
    .line 734
    invoke-static {}, Lcom/google/common/flags/Flags;->initMaps()V

    .line 735
    invoke-static/range {p0 .. p0}, Lcom/google/common/flags/Flags;->loadExternalFlags([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 737
    .local v3, args:[Ljava/lang/String;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 738
    .local v14, nonFlagArgs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 739
    .local v16, unrecognizedFlags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 740
    .local v1, acceptableUnrecognizedFlags:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v10, 0x0

    .local v10, i:I
    :goto_0
    array-length v0, v3

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v10, v0, :cond_f

    .line 741
    aget-object v2, v3, v10

    .line 743
    .local v2, arg:Ljava/lang/String;
    const-string v18, "-"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 744
    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 740
    :cond_0
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 748
    :cond_1
    const-string v18, "--"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 749
    :goto_2
    add-int/lit8 v10, v10, 0x1

    array-length v0, v3

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v10, v0, :cond_f

    .line 750
    aget-object v18, v3, v10

    move-object/from16 v0, v18

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 755
    :cond_2
    invoke-static {v2}, Lcom/google/common/flags/Flags;->printRequestedHelp(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 756
    move-object/from16 v0, v16

    invoke-static {v0, v1}, Lcom/google/common/flags/Flags;->getUnknownFlags(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v15

    .line 758
    .local v15, unknownFlags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_3

    .line 759
    sget-object v18, Lcom/google/common/flags/Flags;->outputStream:Ljava/io/PrintStream;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Unrecognized flags: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 761
    :cond_3
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_4

    const/16 v18, 0x0

    :goto_3
    invoke-static/range {v18 .. v18}, Lcom/google/common/flags/Flags;->exitUnlessDisabled(I)V

    .line 762
    sget-object v18, Lcom/google/common/flags/Flags;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    .line 826
    .end local v2           #arg:Ljava/lang/String;
    :goto_4
    return-object v18

    .line 761
    .restart local v2       #arg:Ljava/lang/String;
    :cond_4
    const/16 v18, 0x1

    goto :goto_3

    .line 765
    .end local v15           #unknownFlags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    sget-object v18, Lcom/google/common/flags/Flags;->FLAG_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    .line 766
    .local v13, m:Ljava/util/regex/Matcher;
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->matches()Z

    move-result v18

    if-nez v18, :cond_6

    .line 767
    new-instance v18, Lcom/google/common/flags/InvalidFlagSyntaxException;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/google/common/flags/InvalidFlagSyntaxException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 769
    :cond_6
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    .line 770
    .local v7, flagName:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 771
    new-instance v18, Lcom/google/common/flags/InvalidFlagSyntaxException;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/google/common/flags/InvalidFlagSyntaxException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 773
    :cond_7
    const/16 v18, 0x2

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v17

    .line 774
    .local v17, valueString:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/common/flags/Flags;->getBestFlag(Ljava/lang/String;)Lcom/google/common/flags/FlagDescription;

    move-result-object v5

    .line 775
    .local v5, description:Lcom/google/common/flags/FlagDescription;
    const/4 v9, 0x0

    .line 777
    .local v9, guessingNextArgIsValue:Z
    if-nez v17, :cond_d

    .line 778
    invoke-static {v5}, Lcom/google/common/flags/Flags;->isBooleanFlag(Lcom/google/common/flags/FlagDescription;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 779
    invoke-virtual {v5, v7}, Lcom/google/common/flags/FlagDescription;->isPositiveFormOfName(Ljava/lang/String;)Z

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v17

    .line 781
    invoke-virtual {v5}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v7

    .line 800
    :cond_8
    :goto_5
    invoke-static {v7}, Lcom/google/common/flags/Flags;->isAllowedUndefOkFlag(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 802
    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .local v4, arr$:[Ljava/lang/String;
    array-length v12, v4

    .local v12, len$:I
    const/4 v11, 0x0

    .local v11, i$:I
    :goto_6
    if-ge v11, v12, :cond_0

    aget-object v8, v4, v11

    .line 803
    .local v8, fn:Ljava/lang/String;
    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 802
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 782
    .end local v4           #arr$:[Ljava/lang/String;
    .end local v8           #fn:Ljava/lang/String;
    .end local v11           #i$:I
    .end local v12           #len$:I
    :cond_9
    invoke-static {v5}, Lcom/google/common/flags/Flags;->isLegitimateFlag(Lcom/google/common/flags/FlagDescription;)Z

    move-result v18

    if-nez v18, :cond_a

    invoke-static {v7}, Lcom/google/common/flags/Flags;->isAllowedUndefOkFlag(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 784
    :cond_a
    array-length v0, v3

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-ne v10, v0, :cond_b

    .line 785
    new-instance v18, Lcom/google/common/flags/InvalidFlagSyntaxException;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " is missing a value"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lcom/google/common/flags/InvalidFlagSyntaxException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 787
    :cond_b
    add-int/lit8 v10, v10, 0x1

    aget-object v17, v3, v10

    .line 788
    const/4 v9, 0x1

    goto :goto_5

    .line 790
    :cond_c
    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 793
    :cond_d
    invoke-static {v5}, Lcom/google/common/flags/Flags;->isBooleanFlag(Lcom/google/common/flags/FlagDescription;)Z

    move-result v18

    if-eqz v18, :cond_8

    invoke-virtual {v5, v7}, Lcom/google/common/flags/FlagDescription;->isPositiveFormOfName(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_8

    .line 795
    new-instance v18, Lcom/google/common/flags/InvalidFlagSyntaxException;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/google/common/flags/InvalidFlagSyntaxException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 810
    :cond_e
    :try_start_0
    move-object/from16 v0, v17

    invoke-static {v7, v0}, Lcom/google/common/flags/Flags;->processFlag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/common/flags/UnrecognizedFlagException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 811
    :catch_0
    move-exception v6

    .line 812
    .local v6, e:Lcom/google/common/flags/UnrecognizedFlagException;
    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 813
    if-eqz v9, :cond_0

    .line 814
    add-int/lit8 v10, v10, -0x1

    goto/16 :goto_1

    .line 819
    .end local v2           #arg:Ljava/lang/String;
    .end local v5           #description:Lcom/google/common/flags/FlagDescription;
    .end local v6           #e:Lcom/google/common/flags/UnrecognizedFlagException;
    .end local v7           #flagName:Ljava/lang/String;
    .end local v9           #guessingNextArgIsValue:Z
    .end local v13           #m:Ljava/util/regex/Matcher;
    .end local v17           #valueString:Ljava/lang/String;
    :cond_f
    move-object/from16 v0, v16

    invoke-static {v0, v1}, Lcom/google/common/flags/Flags;->getUnknownFlags(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v15

    .line 821
    .restart local v15       #unknownFlags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    if-lez v18, :cond_10

    .line 822
    new-instance v18, Lcom/google/common/flags/UnrecognizedFlagException;

    move-object/from16 v0, v18

    invoke-direct {v0, v15}, Lcom/google/common/flags/UnrecognizedFlagException;-><init>(Ljava/util/List;)V

    throw v18

    .line 826
    :cond_10
    sget-object v18, Lcom/google/common/flags/Flags;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-interface {v14, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [Ljava/lang/String;

    goto/16 :goto_4
.end method

.method private static printRequestedHelp(Ljava/lang/String;)Z
    .locals 4
    .parameter "arg"

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 688
    sget-object v2, Lcom/google/common/flags/Flags;->HELP_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 689
    sget-object v2, Lcom/google/common/flags/Flags;->outputStream:Ljava/io/PrintStream;

    sget-object v3, Lcom/google/common/flags/Flags;->usagePrefix:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/google/common/flags/Flags;->usage(Ljava/io/PrintStream;ZLjava/lang/String;)V

    .line 698
    :goto_0
    return v0

    .line 691
    :cond_0
    sget-object v2, Lcom/google/common/flags/Flags;->HELPSHORT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 692
    sget-object v1, Lcom/google/common/flags/Flags;->outputStream:Ljava/io/PrintStream;

    sget-object v2, Lcom/google/common/flags/Flags;->usagePrefix:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/common/flags/Flags;->usage(Ljava/io/PrintStream;ZLjava/lang/String;)V

    goto :goto_0

    .line 694
    :cond_1
    sget-object v2, Lcom/google/common/flags/Flags;->XML_HELP_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 695
    new-instance v1, Ljava/io/PrintWriter;

    sget-object v2, Lcom/google/common/flags/Flags;->outputStream:Ljava/io/PrintStream;

    invoke-direct {v1, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v1}, Lcom/google/common/flags/Flags;->xmlUsage(Ljava/io/PrintWriter;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 698
    goto :goto_0
.end method

.method public static processFlag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter "flagName"
    .parameter "flagValue"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/common/flags/UnrecognizedFlagException;,
            Lcom/google/common/flags/InvalidFlagValueException;,
            Lcom/google/common/flags/AmbiguousFlagException;
        }
    .end annotation

    .prologue
    .line 990
    invoke-static {p0}, Lcom/google/common/flags/Flags;->getBestFlag(Ljava/lang/String;)Lcom/google/common/flags/FlagDescription;

    move-result-object v1

    .line 991
    .local v1, desc:Lcom/google/common/flags/FlagDescription;
    if-nez v1, :cond_2

    .line 992
    invoke-static {}, Lcom/google/common/flags/Flags;->expandedFlagMap()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 993
    .local v0, allDesc:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 994
    :cond_0
    new-instance v4, Lcom/google/common/flags/UnrecognizedFlagException;

    invoke-direct {v4, p0}, Lcom/google/common/flags/UnrecognizedFlagException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 997
    :cond_1
    new-instance v4, Lcom/google/common/flags/AmbiguousFlagException;

    invoke-direct {v4, p0, v0}, Lcom/google/common/flags/AmbiguousFlagException;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    throw v4

    .line 1001
    .end local v0           #allDesc:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/common/flags/FlagDescription;>;"
    :cond_2
    :try_start_0
    invoke-static {v1}, Lcom/google/common/flags/Flags;->flag(Lcom/google/common/flags/FlagDescription;)Lcom/google/common/flags/Flag;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/common/flags/Flag;->setFromString(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/common/flags/InvalidFlagValueException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/common/flags/IllegalFlagStateException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1019
    return-void

    .line 1002
    :catch_0
    move-exception v2

    .line 1003
    .local v2, e:Ljava/lang/NullPointerException;
    new-instance v3, Lcom/google/common/flags/InvalidFlagValueException;

    const-string v4, "NullPointerException"

    invoke-direct {v3, v4}, Lcom/google/common/flags/InvalidFlagValueException;-><init>(Ljava/lang/String;)V

    .line 1006
    .local v3, newException:Lcom/google/common/flags/InvalidFlagValueException;
    invoke-virtual {v1}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/common/flags/InvalidFlagValueException;->flagName:Ljava/lang/String;

    .line 1007
    const-string v4, "null"

    iput-object v4, v3, Lcom/google/common/flags/InvalidFlagValueException;->flagValue:Ljava/lang/String;

    .line 1008
    throw v3

    .line 1009
    .end local v2           #e:Ljava/lang/NullPointerException;
    .end local v3           #newException:Lcom/google/common/flags/InvalidFlagValueException;
    :catch_1
    move-exception v2

    .line 1011
    .local v2, e:Lcom/google/common/flags/InvalidFlagValueException;
    invoke-virtual {v1}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/common/flags/InvalidFlagValueException;->flagName:Ljava/lang/String;

    .line 1012
    iput-object p1, v2, Lcom/google/common/flags/InvalidFlagValueException;->flagValue:Ljava/lang/String;

    .line 1013
    throw v2

    .line 1014
    .end local v2           #e:Lcom/google/common/flags/InvalidFlagValueException;
    :catch_2
    move-exception v2

    .line 1016
    .local v2, e:Lcom/google/common/flags/IllegalFlagStateException;
    invoke-virtual {v1}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/common/flags/IllegalFlagStateException;->flagName:Ljava/lang/String;

    .line 1017
    throw v2
.end method

.method static reallyClearFlagMapsForTesting()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 321
    sget-object v0, Lcom/google/common/flags/Flags$ParseState;->NOT_STARTED:Lcom/google/common/flags/Flags$ParseState;

    sput-object v0, Lcom/google/common/flags/Flags;->parseState:Lcom/google/common/flags/Flags$ParseState;

    .line 322
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->longNameMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 323
    sput-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->canonicalFlagMap:Ljava/util/Map;

    .line 324
    sput-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->expandedFlagMap:Ljava/util/Map;

    .line 325
    sget-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->manuallyRegisteredFlags:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 326
    sput-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->whitelistedPrefixes:Ljava/util/Collection;

    .line 327
    invoke-static {}, Lcom/google/common/flags/Flags;->disableUndefOkSupport()V

    .line 328
    return-void
.end method

.method public static registerCompletionHook(Ljava/lang/Runnable;)V
    .locals 4
    .parameter "callback"

    .prologue
    .line 1533
    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 1535
    :cond_0
    const-class v2, Lcom/google/common/flags/Flags;

    monitor-enter v2

    .line 1536
    :try_start_0
    sget-object v1, Lcom/google/common/flags/Flags;->parseState:Lcom/google/common/flags/Flags$ParseState;

    sget-object v3, Lcom/google/common/flags/Flags$ParseState;->DONE:Lcom/google/common/flags/Flags$ParseState;

    if-ne v1, v3, :cond_2

    const/4 v0, 0x1

    .line 1537
    .local v0, parseDone:Z
    :goto_0
    sget-object v1, Lcom/google/common/flags/Flags;->completionHooks:Ljava/util/Collection;

    invoke-interface {v1, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1538
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1539
    if-eqz v0, :cond_1

    .line 1540
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 1542
    :cond_1
    return-void

    .line 1536
    .end local v0           #parseDone:Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1538
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static declared-synchronized registerFlag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/flags/DocLevel;Ljava/lang/String;Lcom/google/common/flags/Flag;)V
    .locals 6
    .parameter "containerClassName"
    .parameter "simpleName"
    .parameter "doc"
    .parameter "altName"
    .parameter "doclevel"
    .parameter "type"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/common/flags/DocLevel;",
            "Ljava/lang/String;",
            "Lcom/google/common/flags/Flag",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1417
    .local p6, flag:Lcom/google/common/flags/Flag;,"Lcom/google/common/flags/Flag<*>;"
    const-class v3, Lcom/google/common/flags/Flags;

    monitor-enter v3

    :try_start_0
    invoke-static {p1, p0}, Lcom/google/common/flags/FlagDescription;->createManuallyRegisteredFlag(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/flags/FlagDescription$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/common/flags/FlagDescription$Builder;->doc(Ljava/lang/String;)Lcom/google/common/flags/FlagDescription$Builder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/common/flags/FlagDescription$Builder;->altName(Ljava/lang/String;)Lcom/google/common/flags/FlagDescription$Builder;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/common/flags/FlagDescription$Builder;->docLevel(Lcom/google/common/flags/DocLevel;)Lcom/google/common/flags/FlagDescription$Builder;

    move-result-object v2

    invoke-virtual {v2, p5}, Lcom/google/common/flags/FlagDescription$Builder;->type(Ljava/lang/String;)Lcom/google/common/flags/FlagDescription$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/flags/FlagDescription$Builder;->build()Lcom/google/common/flags/FlagDescription;

    move-result-object v0

    .line 1424
    .local v0, description:Lcom/google/common/flags/FlagDescription;
    if-nez p6, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1417
    .end local v0           #description:Lcom/google/common/flags/FlagDescription;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 1428
    .restart local v0       #description:Lcom/google/common/flags/FlagDescription;
    :cond_0
    if-eqz p3, :cond_1

    :try_start_1
    sget-object v2, Lcom/google/common/flags/Flags;->FLAG_NAME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1429
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v4, "Alternate flag name must contain only alphanumeric characters, hyphens, and underscores."

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1434
    :cond_1
    invoke-static {}, Lcom/google/common/flags/Flags;->longNameMap()Ljava/util/Map;

    move-result-object v1

    .line 1436
    .local v1, longNameMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/common/flags/Flags;->stateCheckingDisabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1438
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Duplicate flag "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1442
    :cond_2
    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1443
    invoke-static {}, Lcom/google/common/flags/Flags;->manuallyRegisteredFlags()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1445
    const/4 v2, 0x0

    sput-object v2, Lcom/google/common/flags/Flags$FlagMapHolder;->canonicalFlagMap:Ljava/util/Map;

    .line 1446
    const/4 v2, 0x0

    sput-object v2, Lcom/google/common/flags/Flags$FlagMapHolder;->expandedFlagMap:Ljava/util/Map;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447
    monitor-exit v3

    return-void
.end method

.method public static resetAllFlagsForTest()V
    .locals 3

    .prologue
    .line 1601
    invoke-static {}, Lcom/google/common/flags/Flags;->longNameMap()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/flags/FlagDescription;

    .line 1603
    .local v0, desc:Lcom/google/common/flags/FlagDescription;
    :try_start_0
    invoke-static {v0}, Lcom/google/common/flags/Flags;->flag(Lcom/google/common/flags/FlagDescription;)Lcom/google/common/flags/Flag;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/flags/Flag;->resetForTest()V
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1604
    :catch_0
    move-exception v2

    goto :goto_0

    .line 1609
    .end local v0           #desc:Lcom/google/common/flags/FlagDescription;
    :cond_0
    sget-object v2, Lcom/google/common/flags/Flags$ParseState;->NOT_STARTED:Lcom/google/common/flags/Flags$ParseState;

    invoke-static {v2}, Lcom/google/common/flags/Flags;->setParseState(Lcom/google/common/flags/Flags$ParseState;)V

    .line 1610
    return-void
.end method

.method public static resetFlagForTest(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter "member"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1640
    .local p0, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "^FLAG_"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/flags/Flags;->verifyAndResetFlag(Ljava/lang/String;)V

    .line 1643
    sget-object v0, Lcom/google/common/flags/Flags$ParseState;->NOT_STARTED:Lcom/google/common/flags/Flags$ParseState;

    invoke-static {v0}, Lcom/google/common/flags/Flags;->setParseState(Lcom/google/common/flags/Flags$ParseState;)V

    .line 1644
    return-void
.end method

.method static resetOutputStreamForTesting()V
    .locals 1

    .prologue
    .line 108
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sput-object v0, Lcom/google/common/flags/Flags;->outputStream:Ljava/io/PrintStream;

    .line 109
    return-void
.end method

.method public static resetPreferredClasses()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1521
    sget-object v0, Lcom/google/common/flags/Flags;->preferredClasses:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1522
    return-void
.end method

.method public static varargs resetSomeFlagsForTest([Ljava/lang/String;)V
    .locals 5
    .parameter "flagNames"

    .prologue
    .line 1622
    move-object v0, p0

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 1623
    .local v1, flagName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/flags/Flags;->verifyAndResetFlag(Ljava/lang/String;)V

    .line 1622
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1626
    .end local v1           #flagName:Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/google/common/flags/Flags$ParseState;->NOT_STARTED:Lcom/google/common/flags/Flags$ParseState;

    invoke-static {v4}, Lcom/google/common/flags/Flags;->setParseState(Lcom/google/common/flags/Flags$ParseState;)V

    .line 1627
    return-void
.end method

.method public static setAllowedFlags(Ljava/util/Collection;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, allowedPrefixes:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 1475
    if-nez p0, :cond_0

    .line 1476
    sput-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->whitelistedPrefixes:Ljava/util/Collection;

    .line 1481
    :goto_0
    sput-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->expandedFlagMap:Ljava/util/Map;

    .line 1482
    sput-object v1, Lcom/google/common/flags/Flags$FlagMapHolder;->canonicalFlagMap:Ljava/util/Map;

    .line 1483
    return-void

    .line 1478
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, p0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/common/flags/Flags$FlagMapHolder;->whitelistedPrefixes:Ljava/util/Collection;

    goto :goto_0
.end method

.method public static setFlagClassLoader(Ljava/lang/ClassLoader;)V
    .locals 0
    .parameter "loader"

    .prologue
    .line 234
    sput-object p0, Lcom/google/common/flags/Flags;->flagClassLoader:Ljava/lang/ClassLoader;

    .line 235
    return-void
.end method

.method static setOutputStreamForTesting(Ljava/io/PrintStream;)V
    .locals 0
    .parameter "stream"

    .prologue
    .line 104
    sput-object p0, Lcom/google/common/flags/Flags;->outputStream:Ljava/io/PrintStream;

    .line 105
    return-void
.end method

.method private static declared-synchronized setParseState(Lcom/google/common/flags/Flags$ParseState;)V
    .locals 4
    .parameter "newState"

    .prologue
    .line 1550
    const-class v1, Lcom/google/common/flags/Flags;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/common/flags/Flags$ParseState;->IN_PROGRESS:Lcom/google/common/flags/Flags$ParseState;

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/google/common/flags/Flags;->parseState:Lcom/google/common/flags/Flags$ParseState;

    sget-object v2, Lcom/google/common/flags/Flags$ParseState;->NOT_STARTED:Lcom/google/common/flags/Flags$ParseState;

    if-eq v0, v2, :cond_0

    invoke-static {}, Lcom/google/common/flags/Flags;->stateCheckingDisabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1553
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot call parse more than once.  Here is the stacktrace of the previous call:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/common/flags/Flags;->parseStackTrace:Ljava/lang/Throwable;

    invoke-static {v3}, Lcom/google/common/flags/Flags;->getStackTraceAsString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1550
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1557
    :cond_0
    :try_start_1
    sput-object p0, Lcom/google/common/flags/Flags;->parseState:Lcom/google/common/flags/Flags$ParseState;

    .line 1558
    sget-object v0, Lcom/google/common/flags/Flags$ParseState;->DONE:Lcom/google/common/flags/Flags$ParseState;

    if-ne p0, v0, :cond_1

    .line 1559
    new-instance v0, Ljava/lang/Throwable;

    const-string v2, "The stack trace associated with the PREVIOUS call to parse"

    invoke-direct {v0, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/common/flags/Flags;->parseStackTrace:Ljava/lang/Throwable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1564
    :goto_0
    monitor-exit v1

    return-void

    .line 1562
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    sput-object v0, Lcom/google/common/flags/Flags;->parseStackTrace:Ljava/lang/Throwable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static setUsagePrefix(Ljava/lang/String;)V
    .locals 0
    .parameter "usgPrefix"

    .prologue
    .line 1175
    sput-object p0, Lcom/google/common/flags/Flags;->usagePrefix:Ljava/lang/String;

    .line 1176
    return-void
.end method

.method private static simpleType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "typeClassName"

    .prologue
    .line 1374
    const-string v0, "java.lang.Integer"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, "int"

    .line 1380
    .end local p0
    :cond_0
    :goto_0
    return-object p0

    .line 1375
    .restart local p0
    :cond_1
    const-string v0, "java.lang.Long"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p0, "long"

    goto :goto_0

    .line 1376
    :cond_2
    const-string v0, "java.lang.Float"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string p0, "float"

    goto :goto_0

    .line 1377
    :cond_3
    const-string v0, "java.lang.Double"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string p0, "double"

    goto :goto_0

    .line 1378
    :cond_4
    const-string v0, "java.lang.String"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string p0, "string"

    goto :goto_0

    .line 1379
    :cond_5
    const-string v0, "java.lang.Boolean"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "boolean"

    goto :goto_0
.end method

.method static sortFlags(Z)Ljava/util/Set;
    .locals 6
    .parameter "shortForm"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/flags/FlagDescription;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1134
    invoke-static {}, Lcom/google/common/flags/Flags;->getMainClassName()Ljava/lang/String;

    move-result-object v4

    .line 1135
    .local v4, mainClassName:Ljava/lang/String;
    new-instance v2, Ljava/util/TreeSet;

    new-instance v5, Lcom/google/common/flags/Flags$1;

    invoke-direct {v5}, Lcom/google/common/flags/Flags$1;-><init>()V

    invoke-direct {v2, v5}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 1150
    .local v2, entries:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;>;"
    invoke-static {}, Lcom/google/common/flags/Flags;->canonicalFlagMap()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1151
    .local v1, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/flags/FlagDescription;

    .line 1153
    .local v0, descr:Lcom/google/common/flags/FlagDescription;
    invoke-static {v0}, Lcom/google/common/flags/Flags;->whitelistAllowsAliasing(Lcom/google/common/flags/FlagDescription;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1156
    if-eqz p0, :cond_1

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getContainerClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1160
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1162
    .end local v0           #descr:Lcom/google/common/flags/FlagDescription;
    .end local v1           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    :cond_2
    return-object v2
.end method

.method public static stateCheckingDisabled()Z
    .locals 1

    .prologue
    .line 1570
    const-string v0, "com.google.common.flags.disableStateChecking"

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static usage(Ljava/io/PrintStream;)V
    .locals 2
    .parameter "printStream"

    .prologue
    .line 1196
    const/4 v0, 0x0

    sget-object v1, Lcom/google/common/flags/Flags;->usagePrefix:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/common/flags/Flags;->usage(Ljava/io/PrintStream;ZLjava/lang/String;)V

    .line 1197
    return-void
.end method

.method public static usage(Ljava/io/PrintStream;Ljava/lang/String;)V
    .locals 1
    .parameter "printStream"
    .parameter "usgPrefix"

    .prologue
    .line 1207
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/common/flags/Flags;->usage(Ljava/io/PrintStream;ZLjava/lang/String;)V

    .line 1208
    return-void
.end method

.method public static usage(Ljava/io/PrintStream;ZLjava/lang/String;)V
    .locals 10
    .parameter "printStream"
    .parameter "shortForm"
    .parameter "usgPrefix"

    .prologue
    const/4 v9, 0x0

    .line 1219
    if-nez p2, :cond_0

    .line 1220
    const-string v5, "Usage: java [jvm-flags...] %s [flags...] [args...]"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/common/flags/Flags;->getProgramName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 1225
    :cond_0
    invoke-static {}, Lcom/google/common/flags/Flags;->initMaps()V

    .line 1227
    invoke-virtual {p0, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1228
    const-string v5, "where flags are"

    invoke-virtual {p0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1229
    const-string v5, " Standard flags:"

    invoke-virtual {p0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1230
    const-string v5, "help"

    const-string v6, "describes all flags"

    invoke-static {p0, v5, v6, v9}, Lcom/google/common/flags/Flags;->formatFlag(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    const-string v5, "helpshort"

    const-string v6, "describes the main class\' flags"

    invoke-static {p0, v5, v6, v9}, Lcom/google/common/flags/Flags;->formatFlag(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1234
    const-string v5, "helpxml"

    const-string v6, "emits XML description of all flags"

    invoke-static {p0, v5, v6, v9}, Lcom/google/common/flags/Flags;->formatFlag(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1237
    const-string v4, ""

    .line 1238
    .local v4, previousClass:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/common/flags/Flags;->sortFlags(Z)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1239
    .local v1, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1240
    .local v2, flagName:Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/flags/FlagDescription;

    .line 1241
    .local v0, descr:Lcom/google/common/flags/FlagDescription;
    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getDocLevel()Lcom/google/common/flags/DocLevel;

    move-result-object v5

    sget-object v6, Lcom/google/common/flags/DocLevel;->PUBLIC:Lcom/google/common/flags/DocLevel;

    if-ne v5, v6, :cond_1

    .line 1242
    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getContainerClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1243
    invoke-virtual {p0}, Ljava/io/PrintStream;->println()V

    .line 1244
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "  Flags for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getContainerClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1246
    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getContainerClassName()Ljava/lang/String;

    move-result-object v4

    .line 1248
    :cond_2
    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getDoc()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Lcom/google/common/flags/Flags;->flag(Lcom/google/common/flags/FlagDescription;)Lcom/google/common/flags/Flag;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/flags/Flags;->getDefaultAsString(Lcom/google/common/flags/Flag;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v2, v5, v6}, Lcom/google/common/flags/Flags;->formatFlag(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1252
    .end local v0           #descr:Lcom/google/common/flags/FlagDescription;
    .end local v1           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    .end local v2           #flagName:Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private static valueToString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .parameter "defaultValue"

    .prologue
    .line 1271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1272
    .local v0, b:Ljava/lang/StringBuilder;
    instance-of v5, p0, Ljava/util/List;

    if-eqz v5, :cond_1

    move-object v4, p0

    .line 1273
    check-cast v4, Ljava/util/List;

    .line 1274
    .local v4, v:Ljava/util/List;,"Ljava/util/List<*>;"
    const/4 v1, 0x1

    .line 1275
    .local v1, first:Z
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1276
    .local v3, o:Ljava/lang/Object;
    if-nez v1, :cond_0

    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1277
    :cond_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1278
    const/4 v1, 0x0

    goto :goto_0

    .line 1280
    .end local v1           #first:Z
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #o:Ljava/lang/Object;
    .end local v4           #v:Ljava/util/List;,"Ljava/util/List<*>;"
    :cond_1
    instance-of v5, p0, Ljava/lang/Class;

    if-eqz v5, :cond_3

    .line 1281
    check-cast p0, Ljava/lang/Class;

    .end local p0
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1285
    :cond_2
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 1283
    .restart local p0
    :cond_3
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private static verifyAndResetFlag(Ljava/lang/String;)V
    .locals 4
    .parameter "flagName"

    .prologue
    .line 1654
    invoke-static {}, Lcom/google/common/flags/Flags;->longNameMap()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/flags/FlagDescription;

    .line 1656
    .local v0, desc:Lcom/google/common/flags/FlagDescription;
    if-nez v0, :cond_0

    .line 1657
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid flag name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1661
    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/google/common/flags/Flags;->flag(Lcom/google/common/flags/FlagDescription;)Lcom/google/common/flags/Flag;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/flags/Flag;->resetForTest()V
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1665
    :goto_0
    return-void

    .line 1662
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static whitelistAllowsAliasing(Lcom/google/common/flags/FlagDescription;)Z
    .locals 4
    .parameter "flag"

    .prologue
    const/4 v2, 0x1

    .line 477
    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 479
    :cond_0
    sget-object v3, Lcom/google/common/flags/Flags$FlagMapHolder;->whitelistedPrefixes:Ljava/util/Collection;

    if-nez v3, :cond_2

    .line 489
    :cond_1
    :goto_0
    return v2

    .line 480
    :cond_2
    sget-object v3, Lcom/google/common/flags/Flags$FlagMapHolder;->whitelistedPrefixes:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 481
    .local v1, prefix:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->isField()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getFullyQualifiedFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_4
    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/google/common/flags/FlagDescription;->getLongFlagName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .line 489
    .end local v1           #prefix:Ljava/lang/String;
    :cond_5
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xmlUsage(Ljava/io/PrintWriter;)V
    .locals 9
    .parameter "out"

    .prologue
    .line 1322
    const-string v6, "<?xml version=\"1.0\" encoding=\"UTF-8\"?><AllFlags>"

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1323
    const-string v6, "program"

    invoke-static {}, Lcom/google/common/flags/Flags;->getProgramName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1324
    const-string v6, "usage"

    invoke-static {}, Lcom/google/common/flags/Flags;->getProgramName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1325
    new-instance v5, Ljava/util/TreeMap;

    invoke-static {}, Lcom/google/common/flags/Flags;->canonicalFlagMap()Ljava/util/Map;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    .line 1327
    .local v5, sortedMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1328
    .local v1, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1329
    .local v4, name:Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/flags/FlagDescription;

    .line 1331
    .local v0, description:Lcom/google/common/flags/FlagDescription;
    invoke-static {v0}, Lcom/google/common/flags/Flags;->whitelistAllowsAliasing(Lcom/google/common/flags/FlagDescription;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1335
    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getDocLevel()Lcom/google/common/flags/DocLevel;

    move-result-object v6

    sget-object v7, Lcom/google/common/flags/DocLevel;->PUBLIC:Lcom/google/common/flags/DocLevel;

    if-ne v6, v7, :cond_0

    .line 1336
    invoke-static {v0}, Lcom/google/common/flags/Flags;->flag(Lcom/google/common/flags/FlagDescription;)Lcom/google/common/flags/Flag;

    move-result-object v2

    .line 1337
    .local v2, flag:Lcom/google/common/flags/Flag;,"Lcom/google/common/flags/Flag<*>;"
    const-string v6, "<flag>"

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1338
    const-string v6, "file"

    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getContainerClassName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1340
    const-string v6, "name"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "--"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1341
    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getAltName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1342
    const-string v6, "shortname"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "--"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getShortFlagName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1345
    :cond_1
    const-string v6, "meaning"

    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getDoc()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1346
    const-string v7, "default"

    invoke-virtual {v2}, Lcom/google/common/flags/Flag;->getDefault()Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_2

    const-string v6, "null"

    :goto_1
    invoke-static {v7, v6}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1348
    const-string v7, "current"

    invoke-virtual {v2}, Lcom/google/common/flags/Flag;->get()Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_3

    const-string v6, "null"

    :goto_2
    invoke-static {v7, v6}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1350
    const-string v6, "type"

    invoke-virtual {v0}, Lcom/google/common/flags/FlagDescription;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/common/flags/Flags;->simpleType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/common/flags/XmlSupport;->toXmlElement(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1352
    const-string v6, "</flag>"

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1346
    :cond_2
    invoke-static {v2}, Lcom/google/common/flags/Flags;->getDefaultAsString(Lcom/google/common/flags/Flag;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 1348
    :cond_3
    invoke-static {v2}, Lcom/google/common/flags/Flags;->getAsString(Lcom/google/common/flags/Flag;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 1355
    .end local v0           #description:Lcom/google/common/flags/FlagDescription;
    .end local v1           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/common/flags/FlagDescription;>;"
    .end local v2           #flag:Lcom/google/common/flags/Flag;,"Lcom/google/common/flags/Flag<*>;"
    .end local v4           #name:Ljava/lang/String;
    :cond_4
    const-string v6, "</AllFlags>"

    invoke-virtual {p0, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1356
    invoke-virtual {p0}, Ljava/io/PrintWriter;->flush()V

    .line 1357
    return-void
.end method
