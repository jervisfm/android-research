.class public Lcom/google/common/logging/RotatingLogStream;
.super Ljava/io/OutputStream;
.source "RotatingLogStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/logging/RotatingLogStream$SymbolicLinkProvider;,
        Lcom/google/common/logging/RotatingLogStream$Listener;
    }
.end annotation


# static fields
.field public static final kDefaultDateFormat:Ljava/text/SimpleDateFormat; = null

.field public static final kDefaultRotateSize:J = 0x70800000L

.field private static final skipLogSymLinkCreation:Lcom/google/common/flags/Flag;
    .annotation runtime Lcom/google/common/flags/FlagSpec;
        help = "If enabled, then rotating logs will not create symbolic links from the base filename to the current log.  This is provided to avoid the Runtime.exec() call in RotatingLogStream that can cause OutOfMemoryError due to fork() temporarily doubling memory footprint."
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/flags/Flag",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile symlinker:Lcom/google/common/logging/RotatingLogStream$SymbolicLinkProvider;


# instance fields
.field private final basename_:Ljava/lang/String;

.field private currentCombinedLogSize_:J

.field private currentFileDescriptor_:Ljava/io/FileDescriptor;

.field private currentFile_:Ljava/io/File;

.field private currentSize_:J

.field protected final dateFormat_:Ljava/text/DateFormat;

.field private final extension_:Ljava/lang/String;

.field private files_:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final linkname_:Ljava/lang/String;

.field private final listeners_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/common/logging/RotatingLogStream$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private maxCombinedLogSize_:J

.field private output_:Ljava/io/OutputStream;

.field private rotateSize_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 88
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "-yyyy_MM_dd_HH_mm_ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/common/logging/RotatingLogStream;->kDefaultDateFormat:Ljava/text/SimpleDateFormat;

    .line 96
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/common/flags/Flag;->value(Z)Lcom/google/common/flags/Flag;

    move-result-object v0

    sput-object v0, Lcom/google/common/logging/RotatingLogStream;->skipLogSymLinkCreation:Lcom/google/common/flags/Flag;

    .line 121
    new-instance v0, Lcom/google/common/logging/RotatingLogStream$1;

    invoke-direct {v0}, Lcom/google/common/logging/RotatingLogStream$1;-><init>()V

    sput-object v0, Lcom/google/common/logging/RotatingLogStream;->symlinker:Lcom/google/common/logging/RotatingLogStream$SymbolicLinkProvider;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "basename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    sget-object v0, Lcom/google/common/logging/RotatingLogStream;->kDefaultDateFormat:Ljava/text/SimpleDateFormat;

    invoke-direct {p0, p1, p1, v0}, Lcom/google/common/logging/RotatingLogStream;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/text/DateFormat;)V

    .line 211
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/text/DateFormat;)V
    .locals 6
    .parameter "basename"
    .end parameter
    .parameter "linkname"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "extension"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "dateFormat"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    sget-object v0, Lcom/google/common/logging/RotatingLogStream;->skipLogSymLinkCreation:Lcom/google/common/flags/Flag;

    invoke-virtual {v0}, Lcom/google/common/flags/Flag;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/common/logging/RotatingLogStream;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/text/DateFormat;Z)V

    .line 192
    return-void

    .line 190
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/text/DateFormat;Z)V
    .locals 2
    .parameter "basename"
    .end parameter
    .parameter "linkname"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "extension"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "dateFormat"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "allowSymLinkCreation"
    .end parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->listeners_:Ljava/util/List;

    .line 166
    iput-object p4, p0, Lcom/google/common/logging/RotatingLogStream;->dateFormat_:Ljava/text/DateFormat;

    .line 167
    iput-object p1, p0, Lcom/google/common/logging/RotatingLogStream;->basename_:Ljava/lang/String;

    .line 168
    if-eqz p5, :cond_0

    .end local p2
    :goto_0
    iput-object p2, p0, Lcom/google/common/logging/RotatingLogStream;->linkname_:Ljava/lang/String;

    .line 169
    iput-object p3, p0, Lcom/google/common/logging/RotatingLogStream;->extension_:Ljava/lang/String;

    .line 170
    const-wide/32 v0, 0x70800000

    iput-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->rotateSize_:J

    .line 171
    invoke-direct {p0}, Lcom/google/common/logging/RotatingLogStream;->openNewFile()V

    .line 172
    return-void

    .line 168
    .restart local p2
    :cond_0
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/text/DateFormat;)V
    .locals 1
    .parameter "basename"
    .end parameter
    .parameter "linkname"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .parameter "dateFormat"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/common/logging/RotatingLogStream;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/text/DateFormat;)V

    .line 206
    return-void
.end method

.method private deleteOldestLogFiles(J)V
    .locals 11
    .parameter "totalExpectedLogsSize"

    .prologue
    .line 421
    :goto_0
    iget-wide v9, p0, Lcom/google/common/logging/RotatingLogStream;->maxCombinedLogSize_:J

    cmp-long v9, p1, v9

    if-ltz v9, :cond_0

    .line 422
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-gt v9, v10, :cond_1

    .line 443
    :cond_0
    return-void

    .line 425
    :cond_1
    const/4 v4, 0x0

    .line 426
    .local v4, oldestFileIdx:I
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    .line 427
    .local v5, oldestFileTimestamp:J
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v3, v9, :cond_3

    .line 428
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    .line 429
    .local v7, timestamp:J
    cmp-long v9, v7, v5

    if-gez v9, :cond_2

    .line 430
    move-wide v5, v7

    .line 431
    move v4, v3

    .line 427
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 435
    .end local v7           #timestamp:J
    :cond_3
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 436
    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    .line 437
    .local v1, fLength:J
    iget-wide v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J

    sub-long/2addr v9, v1

    iput-wide v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J

    .line 438
    sub-long/2addr p1, v1

    .line 441
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private findAllFilesWithBasenameAndExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .parameter "basename"
    .end parameter
    .parameter "extension"
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451
    const-string v1, "."

    .line 452
    .local v1, dir:Ljava/lang/String;
    sget-char v10, Ljava/io/File;->separatorChar:C

    invoke-virtual {p1, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 453
    .local v2, dirSeparatorIdx:I
    const/4 v10, -0x1

    if-eq v2, v10, :cond_0

    .line 454
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 455
    add-int/lit8 v10, v2, 0x1

    invoke-virtual {p1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 457
    :cond_0
    move-object v7, p1

    .line 458
    .local v7, relativeBaseName:Ljava/lang/String;
    iget-object v10, p0, Lcom/google/common/logging/RotatingLogStream;->linkname_:Ljava/lang/String;

    if-eqz v10, :cond_1

    new-instance v10, Ljava/io/File;

    iget-object v11, p0, Lcom/google/common/logging/RotatingLogStream;->linkname_:Ljava/lang/String;

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 462
    .local v8, relativeLinkName:Ljava/lang/String;
    :goto_0
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v11, Lcom/google/common/logging/RotatingLogStream$2;

    invoke-direct {v11, p0, v8, v7, p2}, Lcom/google/common/logging/RotatingLogStream$2;-><init>(Lcom/google/common/logging/RotatingLogStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v4

    .line 482
    .local v4, files:[Ljava/io/File;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 483
    .local v9, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    move-object v0, v4

    .local v0, arr$:[Ljava/io/File;
    array-length v6, v0

    .local v6, len$:I
    const/4 v5, 0x0

    .local v5, i$:I
    :goto_1
    if-ge v5, v6, :cond_2

    aget-object v3, v0, v5

    .line 484
    .local v3, file:Ljava/io/File;
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 458
    .end local v0           #arr$:[Ljava/io/File;
    .end local v3           #file:Ljava/io/File;
    .end local v4           #files:[Ljava/io/File;
    .end local v5           #i$:I
    .end local v6           #len$:I
    .end local v8           #relativeLinkName:Ljava/lang/String;
    .end local v9           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_1
    const/4 v8, 0x0

    goto :goto_0

    .line 486
    .restart local v0       #arr$:[Ljava/io/File;
    .restart local v4       #files:[Ljava/io/File;
    .restart local v5       #i$:I
    .restart local v6       #len$:I
    .restart local v8       #relativeLinkName:Ljava/lang/String;
    .restart local v9       #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_2
    return-object v9
.end method

.method private openNewFile()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/google/common/logging/RotatingLogStream;->getNextFilename()Ljava/lang/String;

    move-result-object v2

    .line 316
    .local v2, fn:Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 317
    .local v7, newFile:Ljava/io/File;
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentFile_:Ljava/io/File;

    invoke-virtual {v7, v9}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 370
    :cond_0
    return-void

    .line 320
    :cond_1
    iput-object v7, p0, Lcom/google/common/logging/RotatingLogStream;->currentFile_:Ljava/io/File;

    .line 321
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    if-eqz v9, :cond_2

    .line 322
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/google/common/logging/RotatingLogStream;->currentFile_:Ljava/io/File;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    :cond_2
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentFile_:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, absFn:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentFile_:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    iput-wide v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentSize_:J

    .line 326
    iget-wide v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J

    iget-wide v11, p0, Lcom/google/common/logging/RotatingLogStream;->currentSize_:J

    add-long/2addr v9, v11

    iput-wide v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J

    .line 331
    iget-wide v9, p0, Lcom/google/common/logging/RotatingLogStream;->rotateSize_:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-eqz v9, :cond_3

    iget-wide v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentSize_:J

    iget-wide v11, p0, Lcom/google/common/logging/RotatingLogStream;->rotateSize_:J

    cmp-long v9, v9, v11

    if-ltz v9, :cond_3

    .line 332
    sget-object v9, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Current log files size >= rotate size for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 336
    :cond_3
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentFile_:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v8

    .line 337
    .local v8, parentFile:Ljava/io/File;
    if-eqz v8, :cond_4

    .line 339
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 342
    :cond_4
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v9, 0x1

    invoke-direct {v1, v2, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    .line 343
    .local v1, fileOutputStream:Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v9

    iput-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->currentFileDescriptor_:Ljava/io/FileDescriptor;

    .line 345
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    if-eqz v9, :cond_5

    .line 346
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 348
    :cond_5
    invoke-virtual {p0, v1}, Lcom/google/common/logging/RotatingLogStream;->wrapFileOutputStream(Ljava/io/FileOutputStream;)Ljava/io/OutputStream;

    move-result-object v9

    iput-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    .line 352
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->linkname_:Ljava/lang/String;

    if-eqz v9, :cond_7

    .line 356
    move-object v5, v2

    .line 357
    .local v5, linkSrc:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->linkname_:Ljava/lang/String;

    const/16 v10, 0x2f

    invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 358
    .local v4, lastSlash:I
    const/4 v9, -0x1

    if-eq v4, v9, :cond_6

    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->linkname_:Ljava/lang/String;

    const/4 v10, 0x0

    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 360
    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 363
    :cond_6
    sget-object v9, Lcom/google/common/logging/RotatingLogStream;->symlinker:Lcom/google/common/logging/RotatingLogStream$SymbolicLinkProvider;

    iget-object v10, p0, Lcom/google/common/logging/RotatingLogStream;->linkname_:Ljava/lang/String;

    invoke-interface {v9, v5, v10}, Lcom/google/common/logging/RotatingLogStream$SymbolicLinkProvider;->createSymbolicLink(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    .end local v4           #lastSlash:I
    .end local v5           #linkSrc:Ljava/lang/String;
    :cond_7
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->listeners_:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/common/logging/RotatingLogStream$Listener;

    .line 368
    .local v6, listener:Lcom/google/common/logging/RotatingLogStream$Listener;
    iget-object v9, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    invoke-interface {v6, v9}, Lcom/google/common/logging/RotatingLogStream$Listener;->logFileCreated(Ljava/io/OutputStream;)V

    goto :goto_0
.end method

.method public static setSymbolicLinkProvider(Lcom/google/common/logging/RotatingLogStream$SymbolicLinkProvider;)V
    .locals 0
    .parameter "p"

    .prologue
    .line 144
    sput-object p0, Lcom/google/common/logging/RotatingLogStream;->symlinker:Lcom/google/common/logging/RotatingLogStream$SymbolicLinkProvider;

    .line 145
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/common/logging/RotatingLogStream$Listener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->listeners_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 447
    return-void
.end method

.method checkRotate(I)V
    .locals 4
    .parameter "lenToWrite"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 391
    iget-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentSize_:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/common/logging/RotatingLogStream;->rotateSize_:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/common/logging/RotatingLogStream;->getNextFilename()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    int-to-long v2, p1

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/common/logging/RotatingLogStream;->rotateSize_:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/google/common/logging/RotatingLogStream;->rotate()V

    .line 406
    :cond_0
    :goto_0
    iget-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentSize_:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentSize_:J

    .line 407
    iget-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J

    .line 408
    return-void

    .line 396
    :cond_1
    iget-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentSize_:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/common/logging/RotatingLogStream;->rotateSize_:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 397
    invoke-virtual {p0}, Lcom/google/common/logging/RotatingLogStream;->rotate()V

    .line 400
    :cond_2
    iget-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/common/logging/RotatingLogStream;->deleteOldestLogFiles(J)V

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    monitor-exit p0

    return-void

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    monitor-exit p0

    return-void

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getCurrentCombinedLogSize()J
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 412
    iget-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J

    return-wide v0
.end method

.method public getCurrentFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentFile_:Ljava/io/File;

    return-object v0
.end method

.method public getCurrentFileDescriptor()Ljava/io/FileDescriptor;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentFileDescriptor_:Ljava/io/FileDescriptor;

    return-object v0
.end method

.method getCurrentLogSize()J
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 417
    iget-wide v0, p0, Lcom/google/common/logging/RotatingLogStream;->currentSize_:J

    return-wide v0
.end method

.method getNextFilename()Ljava/lang/String;
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->extension_:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/common/logging/RotatingLogStream;->basename_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/logging/RotatingLogStream;->dateFormat_:Ljava/text/DateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 380
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/common/logging/RotatingLogStream;->basename_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/logging/RotatingLogStream;->dateFormat_:Ljava/text/DateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/logging/RotatingLogStream;->extension_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized rotate()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/common/logging/RotatingLogStream;->flush()V

    .line 221
    invoke-direct {p0}, Lcom/google/common/logging/RotatingLogStream;->openNewFile()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    monitor-exit p0

    return-void

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setMaxCombinedLogSize(J)V
    .locals 6
    .parameter "maxCombinedLogSize"

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    iget-wide v4, p0, Lcom/google/common/logging/RotatingLogStream;->rotateSize_:J

    cmp-long v4, p1, v4

    if-gez v4, :cond_0

    .line 237
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Max combined log size should be >= single log rotate size"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 241
    :cond_0
    :try_start_1
    iput-wide p1, p0, Lcom/google/common/logging/RotatingLogStream;->maxCombinedLogSize_:J

    .line 242
    iget-object v4, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    .line 244
    iget-object v4, p0, Lcom/google/common/logging/RotatingLogStream;->basename_:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/common/logging/RotatingLogStream;->extension_:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/google/common/logging/RotatingLogStream;->findAllFilesWithBasenameAndExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    .line 245
    const-wide/16 v2, 0x0

    .line 246
    .local v2, length:J
    iget-object v4, p0, Lcom/google/common/logging/RotatingLogStream;->files_:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 247
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0

    .line 249
    .end local v0           #file:Ljava/io/File;
    :cond_1
    iput-wide v2, p0, Lcom/google/common/logging/RotatingLogStream;->currentCombinedLogSize_:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #length:J
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setRotateSize(J)V
    .locals 1
    .parameter "rotateSize"

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/common/logging/RotatingLogStream;->rotateSize_:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    monitor-exit p0

    return-void

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected wrapFileOutputStream(Ljava/io/FileOutputStream;)Ljava/io/OutputStream;
    .locals 1
    .parameter "fileOutputStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 311
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public declared-synchronized write(I)V
    .locals 1
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 283
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/common/logging/RotatingLogStream;->checkRotate(I)V

    .line 284
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    monitor-exit p0

    return-void

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([B)V
    .locals 1
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    array-length v0, p1

    invoke-virtual {p0, v0}, Lcom/google/common/logging/RotatingLogStream;->checkRotate(I)V

    .line 270
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    monitor-exit p0

    return-void

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([BII)V
    .locals 1
    .parameter "b"
    .parameter "off"
    .parameter "len"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p3}, Lcom/google/common/logging/RotatingLogStream;->checkRotate(I)V

    .line 278
    iget-object v0, p0, Lcom/google/common/logging/RotatingLogStream;->output_:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    monitor-exit p0

    return-void

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
