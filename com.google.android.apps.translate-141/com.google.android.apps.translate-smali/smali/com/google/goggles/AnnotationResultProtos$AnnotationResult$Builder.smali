.class public final Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AnnotationResultProtos.java"

# interfaces
.implements Lcom/google/goggles/AnnotationResultProtos$AnnotationResultOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;",
        ">;",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResultOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private boundingBoxRequestId_:Ljava/lang/Object;

.field private boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

.field private canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

.field private contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

.field private directUrl_:Ljava/lang/Object;

.field private facts_:Lcom/google/goggles/FactsProtos$Facts;

.field private isAd_:Z

.field private isSimilarProduct_:Z

.field private isWithdrawn_:Z

.field private language_:Ljava/lang/Object;

.field private latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

.field private locationText_:Ljava/lang/Object;

.field private moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

.field private musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

.field private personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

.field private placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

.field private productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

.field private resultId_:Ljava/lang/Object;

.field private resultUrl_:Ljava/lang/Object;

.field private shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

.field private subtitle_:Ljava/lang/Object;

.field private subtype_:Ljava/lang/Object;

.field private textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

.field private title_:Ljava/lang/Object;

.field private ttsDescription_:Ljava/lang/Object;

.field private type_:Ljava/lang/Object;

.field private urlGroups_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/goggles/UrlGroupProtos$UrlGroup;",
            ">;"
        }
    .end annotation
.end field

.field private urls_:Lcom/google/protobuf/LazyStringList;

.field private websearchUrl_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3781
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 4365
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    .line 4401
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    .line 4437
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    .line 4473
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    .line 4509
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    .line 4545
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    .line 4581
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    .line 4617
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 4660
    invoke-static {}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->getDefaultInstance()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    .line 4703
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    .line 4759
    invoke-static {}, Lcom/google/goggles/LatLngProtos$LatLng;->getDefaultInstance()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    .line 4802
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    .line 4838
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    .line 4874
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    .line 4963
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    .line 4999
    invoke-static {}, Lcom/google/goggles/FactsProtos$Facts;->getDefaultInstance()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    .line 5042
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 5085
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 5128
    invoke-static {}, Lcom/google/goggles/ProductInformationProtos$ProductInformation;->getDefaultInstance()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    .line 5171
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    .line 5214
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 5257
    invoke-static {}, Lcom/google/goggles/MusicInformationProtos$MusicInformation;->getDefaultInstance()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    .line 5342
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    .line 5378
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 5414
    invoke-static {}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;->getDefaultInstance()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    .line 5457
    invoke-static {}, Lcom/google/goggles/ShareActionProtos$ShareAction;->getDefaultInstance()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    .line 3782
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->maybeForceBuilderInitialization()V

    .line 3783
    return-void
.end method

.method static synthetic access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3776
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3700()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 3776
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3872
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    .line 3873
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3874
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 3877
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 3788
    new-instance v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    invoke-direct {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;-><init>()V

    return-object v0
.end method

.method private ensureUrlGroupsIsMutable()V
    .locals 2

    .prologue
    .line 4877
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-eq v0, v1, :cond_0

    .line 4878
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    .line 4879
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4881
    :cond_0
    return-void
.end method

.method private ensureUrlsIsMutable()V
    .locals 2

    .prologue
    .line 4705
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    .line 4706
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    .line 4707
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4709
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 3786
    return-void
.end method


# virtual methods
.method public addAllUrlGroups(Ljava/lang/Iterable;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/goggles/UrlGroupProtos$UrlGroup;",
            ">;)",
            "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;"
        }
    .end annotation

    .prologue
    .line 4944
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4945
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 4947
    return-object p0
.end method

.method public addAllUrls(Ljava/lang/Iterable;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4741
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlsIsMutable()V

    .line 4742
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 4744
    return-object p0
.end method

.method public addUrlGroups(ILcom/google/goggles/UrlGroupProtos$UrlGroup$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 4937
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4938
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/goggles/UrlGroupProtos$UrlGroup$Builder;->build()Lcom/google/goggles/UrlGroupProtos$UrlGroup;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 4940
    return-object p0
.end method

.method public addUrlGroups(ILcom/google/goggles/UrlGroupProtos$UrlGroup;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 4920
    if-nez p2, :cond_0

    .line 4921
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4923
    :cond_0
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4924
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 4926
    return-object p0
.end method

.method public addUrlGroups(Lcom/google/goggles/UrlGroupProtos$UrlGroup$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 4930
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4931
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/goggles/UrlGroupProtos$UrlGroup$Builder;->build()Lcom/google/goggles/UrlGroupProtos$UrlGroup;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4933
    return-object p0
.end method

.method public addUrlGroups(Lcom/google/goggles/UrlGroupProtos$UrlGroup;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4910
    if-nez p1, :cond_0

    .line 4911
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4913
    :cond_0
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4914
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4916
    return-object p0
.end method

.method public addUrls(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4731
    if-nez p1, :cond_0

    .line 4732
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4734
    :cond_0
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlsIsMutable()V

    .line 4735
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 4737
    return-object p0
.end method

.method addUrls(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4753
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlsIsMutable()V

    .line 4754
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 4756
    return-void
.end method

.method public build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 2

    .prologue
    .line 3863
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    .line 3864
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3865
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 3867
    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 10

    .prologue
    const/high16 v9, 0x8

    const/high16 v8, 0x4

    const/high16 v7, 0x2

    const/high16 v6, 0x1

    const v5, 0x8000

    .line 3881
    new-instance v1, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;-><init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;Lcom/google/goggles/AnnotationResultProtos$1;)V

    .line 3882
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3883
    const/4 v0, 0x0

    .line 3884
    and-int/lit8 v3, v2, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 3885
    const/4 v0, 0x1

    .line 3887
    :cond_0
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->title_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$3902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3888
    and-int/lit8 v3, v2, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 3889
    or-int/lit8 v0, v0, 0x2

    .line 3891
    :cond_1
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtitle_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4002(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3892
    and-int/lit8 v3, v2, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 3893
    or-int/lit8 v0, v0, 0x4

    .line 3895
    :cond_2
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->type_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3896
    and-int/lit8 v3, v2, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 3897
    or-int/lit8 v0, v0, 0x8

    .line 3899
    :cond_3
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtype_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3900
    and-int/lit8 v3, v2, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 3901
    or-int/lit8 v0, v0, 0x10

    .line 3903
    :cond_4
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->ttsDescription_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3904
    and-int/lit8 v3, v2, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 3905
    or-int/lit8 v0, v0, 0x20

    .line 3907
    :cond_5
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3908
    and-int/lit8 v3, v2, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 3909
    or-int/lit8 v0, v0, 0x40

    .line 3911
    :cond_6
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->directUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3912
    and-int/lit16 v3, v2, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 3913
    or-int/lit16 v0, v0, 0x80

    .line 3915
    :cond_7
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 3916
    and-int/lit16 v3, v2, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 3917
    or-int/lit16 v0, v0, 0x100

    .line 3919
    :cond_8
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    .line 3920
    iget v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 3921
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    .line 3923
    iget v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x201

    iput v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3925
    :cond_9
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4802(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 3926
    and-int/lit16 v3, v2, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 3927
    or-int/lit16 v0, v0, 0x200

    .line 3929
    :cond_a
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/LatLngProtos$LatLng;

    .line 3930
    and-int/lit16 v3, v2, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 3931
    or-int/lit16 v0, v0, 0x400

    .line 3933
    :cond_b
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->locationText_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5002(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3934
    and-int/lit16 v3, v2, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    .line 3935
    or-int/lit16 v0, v0, 0x800

    .line 3937
    :cond_c
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->language_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3938
    iget v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    .line 3939
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    .line 3940
    iget v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x2001

    iput v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3942
    :cond_d
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/util/List;)Ljava/util/List;

    .line 3943
    and-int/lit16 v3, v2, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_e

    .line 3944
    or-int/lit16 v0, v0, 0x1000

    .line 3946
    :cond_e
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->websearchUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3947
    and-int v3, v2, v5

    if-ne v3, v5, :cond_f

    .line 3948
    or-int/lit16 v0, v0, 0x2000

    .line 3950
    :cond_f
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->facts_:Lcom/google/goggles/FactsProtos$Facts;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/FactsProtos$Facts;

    .line 3951
    and-int v3, v2, v6

    if-ne v3, v6, :cond_10

    .line 3952
    or-int/lit16 v0, v0, 0x4000

    .line 3954
    :cond_10
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 3955
    and-int v3, v2, v7

    if-ne v3, v7, :cond_11

    .line 3956
    or-int/2addr v0, v5

    .line 3958
    :cond_11
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 3959
    and-int v3, v2, v8

    if-ne v3, v8, :cond_12

    .line 3960
    or-int/2addr v0, v6

    .line 3962
    :cond_12
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    .line 3963
    and-int v3, v2, v9

    if-ne v3, v9, :cond_13

    .line 3964
    or-int/2addr v0, v7

    .line 3966
    :cond_13
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5802(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    .line 3967
    const/high16 v3, 0x10

    and-int/2addr v3, v2

    const/high16 v4, 0x10

    if-ne v3, v4, :cond_14

    .line 3968
    or-int/2addr v0, v8

    .line 3970
    :cond_14
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 3971
    const/high16 v3, 0x20

    and-int/2addr v3, v2

    const/high16 v4, 0x20

    if-ne v3, v4, :cond_15

    .line 3972
    or-int/2addr v0, v9

    .line 3974
    :cond_15
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6002(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    .line 3975
    const/high16 v3, 0x40

    and-int/2addr v3, v2

    const/high16 v4, 0x40

    if-ne v3, v4, :cond_16

    .line 3976
    const/high16 v3, 0x10

    or-int/2addr v0, v3

    .line 3978
    :cond_16
    iget-boolean v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isWithdrawn_:Z

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isWithdrawn_:Z
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Z)Z

    .line 3979
    const/high16 v3, 0x80

    and-int/2addr v3, v2

    const/high16 v4, 0x80

    if-ne v3, v4, :cond_17

    .line 3980
    const/high16 v3, 0x20

    or-int/2addr v0, v3

    .line 3982
    :cond_17
    iget-boolean v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isAd_:Z

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isAd_:Z
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Z)Z

    .line 3983
    const/high16 v3, 0x100

    and-int/2addr v3, v2

    const/high16 v4, 0x100

    if-ne v3, v4, :cond_18

    .line 3984
    const/high16 v3, 0x40

    or-int/2addr v0, v3

    .line 3986
    :cond_18
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3987
    const/high16 v3, 0x200

    and-int/2addr v3, v2

    const/high16 v4, 0x200

    if-ne v3, v4, :cond_19

    .line 3988
    const/high16 v3, 0x80

    or-int/2addr v0, v3

    .line 3990
    :cond_19
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBoxRequestId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3991
    const/high16 v3, 0x400

    and-int/2addr v3, v2

    const/high16 v4, 0x400

    if-ne v3, v4, :cond_1a

    .line 3992
    const/high16 v3, 0x100

    or-int/2addr v0, v3

    .line 3994
    :cond_1a
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    .line 3995
    const/high16 v3, 0x800

    and-int/2addr v3, v2

    const/high16 v4, 0x800

    if-ne v3, v4, :cond_1b

    .line 3996
    const/high16 v3, 0x200

    or-int/2addr v0, v3

    .line 3998
    :cond_1b
    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;
    invoke-static {v1, v3}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/ShareActionProtos$ShareAction;

    .line 3999
    const/high16 v3, 0x1000

    and-int/2addr v2, v3

    const/high16 v3, 0x1000

    if-ne v2, v3, :cond_1c

    .line 4000
    const/high16 v2, 0x400

    or-int/2addr v0, v2

    .line 4002
    :cond_1c
    iget-boolean v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isSimilarProduct_:Z

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isSimilarProduct_:Z
    invoke-static {v1, v2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Z)Z

    .line 4003
    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I
    invoke-static {v1, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$6802(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;I)I

    .line 4004
    return-object v1
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3792
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 3793
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    .line 3794
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3795
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    .line 3796
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3797
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    .line 3798
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3799
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    .line 3800
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3801
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    .line 3802
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3803
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    .line 3804
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3805
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    .line 3806
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3807
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 3808
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3809
    invoke-static {}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->getDefaultInstance()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    .line 3810
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3811
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    .line 3812
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3813
    invoke-static {}, Lcom/google/goggles/LatLngProtos$LatLng;->getDefaultInstance()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    .line 3814
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3815
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    .line 3816
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3817
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    .line 3818
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3819
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    .line 3820
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3821
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    .line 3822
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3823
    invoke-static {}, Lcom/google/goggles/FactsProtos$Facts;->getDefaultInstance()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    .line 3824
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3825
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 3826
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3827
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 3828
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3829
    invoke-static {}, Lcom/google/goggles/ProductInformationProtos$ProductInformation;->getDefaultInstance()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    .line 3830
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3831
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    .line 3832
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3833
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 3834
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3835
    invoke-static {}, Lcom/google/goggles/MusicInformationProtos$MusicInformation;->getDefaultInstance()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    .line 3836
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3837
    iput-boolean v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isWithdrawn_:Z

    .line 3838
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3839
    iput-boolean v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isAd_:Z

    .line 3840
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x800001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3841
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    .line 3842
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x1000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3843
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 3844
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x2000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3845
    invoke-static {}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;->getDefaultInstance()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    .line 3846
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x4000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3847
    invoke-static {}, Lcom/google/goggles/ShareActionProtos$ShareAction;->getDefaultInstance()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    .line 3848
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x8000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3849
    iput-boolean v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isSimilarProduct_:Z

    .line 3850
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x10000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 3851
    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearBoundingBox()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4653
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 4655
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4656
    return-object p0
.end method

.method public clearBoundingBoxRequestId()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5402
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x2000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5403
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getBoundingBoxRequestId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 5405
    return-object p0
.end method

.method public clearCanonicalImage()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4696
    invoke-static {}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->getDefaultInstance()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    .line 4698
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4699
    return-object p0
.end method

.method public clearContributionInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5250
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 5252
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5253
    return-object p0
.end method

.method public clearDirectUrl()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4605
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4606
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDirectUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    .line 4608
    return-object p0
.end method

.method public clearFacts()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5035
    invoke-static {}, Lcom/google/goggles/FactsProtos$Facts;->getDefaultInstance()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    .line 5037
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5038
    return-object p0
.end method

.method public clearIsAd()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5335
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x800001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5336
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isAd_:Z

    .line 5338
    return-object p0
.end method

.method public clearIsSimilarProduct()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5514
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x10000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5515
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isSimilarProduct_:Z

    .line 5517
    return-object p0
.end method

.method public clearIsWithdrawn()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5314
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5315
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isWithdrawn_:Z

    .line 5317
    return-object p0
.end method

.method public clearLanguage()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4862
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4863
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    .line 4865
    return-object p0
.end method

.method public clearLatLng()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4795
    invoke-static {}, Lcom/google/goggles/LatLngProtos$LatLng;->getDefaultInstance()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    .line 4797
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4798
    return-object p0
.end method

.method public clearLocationText()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4826
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4827
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLocationText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    .line 4829
    return-object p0
.end method

.method public clearMoreLikeThisAction()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5450
    invoke-static {}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;->getDefaultInstance()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    .line 5452
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x4000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5453
    return-object p0
.end method

.method public clearMusicInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5293
    invoke-static {}, Lcom/google/goggles/MusicInformationProtos$MusicInformation;->getDefaultInstance()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    .line 5295
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5296
    return-object p0
.end method

.method public clearPersonInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5078
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 5080
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5081
    return-object p0
.end method

.method public clearPlaceInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5121
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 5123
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5124
    return-object p0
.end method

.method public clearProductInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5164
    invoke-static {}, Lcom/google/goggles/ProductInformationProtos$ProductInformation;->getDefaultInstance()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    .line 5166
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5167
    return-object p0
.end method

.method public clearResultId()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5366
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x1000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5367
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getResultId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    .line 5369
    return-object p0
.end method

.method public clearResultUrl()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4569
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4570
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getResultUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    .line 4572
    return-object p0
.end method

.method public clearShareAction()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5493
    invoke-static {}, Lcom/google/goggles/ShareActionProtos$ShareAction;->getDefaultInstance()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    .line 5495
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x8000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5496
    return-object p0
.end method

.method public clearSubtitle()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4425
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4426
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    .line 4428
    return-object p0
.end method

.method public clearSubtype()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4497
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4498
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSubtype()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    .line 4500
    return-object p0
.end method

.method public clearTextInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 5207
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    .line 5209
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5210
    return-object p0
.end method

.method public clearTitle()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4389
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4390
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    .line 4392
    return-object p0
.end method

.method public clearTtsDescription()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4533
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4534
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTtsDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    .line 4536
    return-object p0
.end method

.method public clearType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4461
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4462
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    .line 4464
    return-object p0
.end method

.method public clearUrlGroups()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4950
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    .line 4951
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4953
    return-object p0
.end method

.method public clearUrls()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4747
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    .line 4748
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4750
    return-object p0
.end method

.method public clearWebsearchUrl()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 4987
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4988
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getWebsearchUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    .line 4990
    return-object p0
.end method

.method public clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2

    .prologue
    .line 3855
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBoundingBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 1

    .prologue
    .line 4622
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object v0
.end method

.method public getBoundingBoxRequestId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5383
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 5384
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5385
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 5386
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 5389
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCanonicalImage()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;
    .locals 1

    .prologue
    .line 4665
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    return-object v0
.end method

.method public getContributionInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1

    .prologue
    .line 5219
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1

    .prologue
    .line 3859
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3776
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public getDirectUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4586
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    .line 4587
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4588
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4589
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    .line 4592
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getFacts()Lcom/google/goggles/FactsProtos$Facts;
    .locals 1

    .prologue
    .line 5004
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    return-object v0
.end method

.method public getIsAd()Z
    .locals 1

    .prologue
    .line 5326
    iget-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isAd_:Z

    return v0
.end method

.method public getIsSimilarProduct()Z
    .locals 1

    .prologue
    .line 5505
    iget-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isSimilarProduct_:Z

    return v0
.end method

.method public getIsWithdrawn()Z
    .locals 1

    .prologue
    .line 5305
    iget-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isWithdrawn_:Z

    return v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4843
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    .line 4844
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4845
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4846
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    .line 4849
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getLatLng()Lcom/google/goggles/LatLngProtos$LatLng;
    .locals 1

    .prologue
    .line 4764
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    return-object v0
.end method

.method public getLocationText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4807
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    .line 4808
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4809
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4810
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    .line 4813
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMoreLikeThisAction()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;
    .locals 1

    .prologue
    .line 5419
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    return-object v0
.end method

.method public getMusicInfo()Lcom/google/goggles/MusicInformationProtos$MusicInformation;
    .locals 1

    .prologue
    .line 5262
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    return-object v0
.end method

.method public getPersonInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 1

    .prologue
    .line 5047
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    return-object v0
.end method

.method public getPlaceInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1

    .prologue
    .line 5090
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    return-object v0
.end method

.method public getProductInfo()Lcom/google/goggles/ProductInformationProtos$ProductInformation;
    .locals 1

    .prologue
    .line 5133
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    return-object v0
.end method

.method public getResultId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5347
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    .line 5348
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5349
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 5350
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    .line 5353
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getResultUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4550
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    .line 4551
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4552
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4553
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    .line 4556
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getShareAction()Lcom/google/goggles/ShareActionProtos$ShareAction;
    .locals 1

    .prologue
    .line 5462
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4406
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    .line 4407
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4408
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4409
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    .line 4412
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSubtype()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4478
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    .line 4479
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4480
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4481
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    .line 4484
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getTextInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;
    .locals 1

    .prologue
    .line 5176
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4370
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    .line 4371
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4372
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4373
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    .line 4376
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getTtsDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4514
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    .line 4515
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4516
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4517
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    .line 4520
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4442
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    .line 4443
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4444
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4445
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    .line 4448
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getUrlGroups(I)Lcom/google/goggles/UrlGroupProtos$UrlGroup;
    .locals 1
    .parameter

    .prologue
    .line 4890
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/UrlGroupProtos$UrlGroup;

    return-object v0
.end method

.method public getUrlGroupsCount()I
    .locals 1

    .prologue
    .line 4887
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getUrlGroupsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/goggles/UrlGroupProtos$UrlGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4884
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUrls(I)Ljava/lang/String;
    .locals 1
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4718
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getUrlsCount()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4715
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getUrlsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4712
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWebsearchUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4968
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    .line 4969
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4970
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4971
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    .line 4974
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBoundingBox()Z
    .locals 2

    .prologue
    .line 4619
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBoundingBoxRequestId()Z
    .locals 2

    .prologue
    const/high16 v1, 0x200

    .line 5380
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCanonicalImage()Z
    .locals 2

    .prologue
    .line 4662
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContributionInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x10

    .line 5216
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDirectUrl()Z
    .locals 2

    .prologue
    .line 4583
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFacts()Z
    .locals 2

    .prologue
    const v1, 0x8000

    .line 5001
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsAd()Z
    .locals 2

    .prologue
    const/high16 v1, 0x80

    .line 5323
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsSimilarProduct()Z
    .locals 2

    .prologue
    const/high16 v1, 0x1000

    .line 5502
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsWithdrawn()Z
    .locals 2

    .prologue
    const/high16 v1, 0x40

    .line 5302
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLanguage()Z
    .locals 2

    .prologue
    .line 4840
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLatLng()Z
    .locals 2

    .prologue
    .line 4761
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLocationText()Z
    .locals 2

    .prologue
    .line 4804
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMoreLikeThisAction()Z
    .locals 2

    .prologue
    const/high16 v1, 0x400

    .line 5416
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMusicInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x20

    .line 5259
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPersonInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x1

    .line 5044
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPlaceInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x2

    .line 5087
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x4

    .line 5130
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultId()Z
    .locals 2

    .prologue
    const/high16 v1, 0x100

    .line 5344
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultUrl()Z
    .locals 2

    .prologue
    .line 4547
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShareAction()Z
    .locals 2

    .prologue
    const/high16 v1, 0x800

    .line 5459
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubtitle()Z
    .locals 2

    .prologue
    .line 4403
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubtype()Z
    .locals 2

    .prologue
    .line 4475
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTextInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x8

    .line 5173
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitle()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4367
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTtsDescription()Z
    .locals 2

    .prologue
    .line 4511
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 4439
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWebsearchUrl()Z
    .locals 2

    .prologue
    .line 4965
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4114
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasBoundingBox()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4115
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getBoundingBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4144
    :cond_0
    :goto_0
    return v1

    .line 4120
    :cond_1
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasCanonicalImage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4121
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getCanonicalImage()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4126
    :cond_2
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasLatLng()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4127
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getLatLng()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/LatLngProtos$LatLng;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v0, v1

    .line 4132
    :goto_1
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getUrlGroupsCount()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 4133
    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getUrlGroups(I)Lcom/google/goggles/UrlGroupProtos$UrlGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/goggles/UrlGroupProtos$UrlGroup;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4132
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4138
    :cond_4
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasTextInfo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4139
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getTextInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4144
    :cond_5
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public mergeBoundingBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 4641
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 4643
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    invoke-static {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->newBuilder(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->mergeFrom(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->buildPartial()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 4649
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4650
    return-object p0

    .line 4646
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    goto :goto_0
.end method

.method public mergeCanonicalImage(Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 4684
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    invoke-static {}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->getDefaultInstance()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 4686
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    invoke-static {v0}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->newBuilder(Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;->mergeFrom(Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;->buildPartial()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    .line 4692
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4693
    return-object p0

    .line 4689
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    goto :goto_0
.end method

.method public mergeContributionInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x10

    .line 5238
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5240
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 5246
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5247
    return-object p0

    .line 5243
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    goto :goto_0
.end method

.method public mergeFacts(Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const v2, 0x8000

    .line 5023
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    invoke-static {}, Lcom/google/goggles/FactsProtos$Facts;->getDefaultInstance()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5025
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    invoke-static {v0}, Lcom/google/goggles/FactsProtos$Facts;->newBuilder(Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/FactsProtos$Facts$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/FactsProtos$Facts$Builder;->mergeFrom(Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/FactsProtos$Facts$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/FactsProtos$Facts$Builder;->buildPartial()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    .line 5031
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5032
    return-object p0

    .line 5028
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 4008
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 4110
    :cond_0
    :goto_0
    return-object p0

    .line 4009
    :cond_1
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4010
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setTitle(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4012
    :cond_2
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasSubtitle()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4013
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setSubtitle(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4015
    :cond_3
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasType()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4016
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setType(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4018
    :cond_4
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasSubtype()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4019
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSubtype()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setSubtype(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4021
    :cond_5
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasTtsDescription()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4022
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTtsDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setTtsDescription(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4024
    :cond_6
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasResultUrl()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4025
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getResultUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setResultUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4027
    :cond_7
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasDirectUrl()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4028
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDirectUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setDirectUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4030
    :cond_8
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasBoundingBox()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4031
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getBoundingBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeBoundingBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4033
    :cond_9
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasCanonicalImage()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4034
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getCanonicalImage()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeCanonicalImage(Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4036
    :cond_a
    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 4037
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 4038
    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    .line 4039
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4046
    :cond_b
    :goto_1
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasLatLng()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 4047
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLatLng()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeLatLng(Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4049
    :cond_c
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasLocationText()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 4050
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLocationText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setLocationText(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4052
    :cond_d
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasLanguage()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 4053
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setLanguage(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4055
    :cond_e
    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5200(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 4056
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 4057
    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5200(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    .line 4058
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4065
    :cond_f
    :goto_2
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasWebsearchUrl()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 4066
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getWebsearchUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setWebsearchUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4068
    :cond_10
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasFacts()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 4069
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getFacts()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFacts(Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4071
    :cond_11
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasPersonInfo()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 4072
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getPersonInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergePersonInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4074
    :cond_12
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasPlaceInfo()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 4075
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getPlaceInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergePlaceInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4077
    :cond_13
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasProductInfo()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 4078
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getProductInfo()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeProductInfo(Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4080
    :cond_14
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasTextInfo()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 4081
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTextInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeTextInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4083
    :cond_15
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasContributionInfo()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 4084
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getContributionInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeContributionInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4086
    :cond_16
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasMusicInfo()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 4087
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getMusicInfo()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeMusicInfo(Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4089
    :cond_17
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasIsWithdrawn()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 4090
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getIsWithdrawn()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setIsWithdrawn(Z)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4092
    :cond_18
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasIsAd()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 4093
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getIsAd()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setIsAd(Z)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4095
    :cond_19
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasResultId()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 4096
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getResultId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setResultId(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4098
    :cond_1a
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasBoundingBoxRequestId()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 4099
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getBoundingBoxRequestId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setBoundingBoxRequestId(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4101
    :cond_1b
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasMoreLikeThisAction()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 4102
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getMoreLikeThisAction()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeMoreLikeThisAction(Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4104
    :cond_1c
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasShareAction()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 4105
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getShareAction()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeShareAction(Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    .line 4107
    :cond_1d
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasIsSimilarProduct()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4108
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getIsSimilarProduct()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setIsSimilarProduct(Z)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4041
    :cond_1e
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlsIsMutable()V

    .line 4042
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$4800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 4060
    :cond_1f
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4061
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->access$5200(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4152
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 4153
    sparse-switch v0, :sswitch_data_0

    .line 4158
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4160
    :sswitch_0
    return-object p0

    .line 4165
    :sswitch_1
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->newBuilder()Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    move-result-object v0

    .line 4166
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasBoundingBox()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4167
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getBoundingBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->mergeFrom(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    .line 4169
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4170
    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->buildPartial()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setBoundingBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto :goto_0

    .line 4174
    :sswitch_2
    invoke-static {}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->newBuilder()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;

    move-result-object v0

    .line 4175
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasCanonicalImage()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4176
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getCanonicalImage()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;->mergeFrom(Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;

    .line 4178
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4179
    invoke-virtual {v0}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;->buildPartial()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setCanonicalImage(Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto :goto_0

    .line 4183
    :sswitch_3
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlsIsMutable()V

    .line 4184
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 4188
    :sswitch_4
    invoke-static {}, Lcom/google/goggles/LatLngProtos$LatLng;->newBuilder()Lcom/google/goggles/LatLngProtos$LatLng$Builder;

    move-result-object v0

    .line 4189
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasLatLng()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4190
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getLatLng()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/LatLngProtos$LatLng$Builder;->mergeFrom(Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/LatLngProtos$LatLng$Builder;

    .line 4192
    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4193
    invoke-virtual {v0}, Lcom/google/goggles/LatLngProtos$LatLng$Builder;->buildPartial()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setLatLng(Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto :goto_0

    .line 4197
    :sswitch_5
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4198
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    goto :goto_0

    .line 4202
    :sswitch_6
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4203
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4207
    :sswitch_7
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4208
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4212
    :sswitch_8
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4213
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4217
    :sswitch_9
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4218
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4222
    :sswitch_a
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4223
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4227
    :sswitch_b
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4228
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4232
    :sswitch_c
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4233
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4237
    :sswitch_d
    invoke-static {}, Lcom/google/goggles/UrlGroupProtos$UrlGroup;->newBuilder()Lcom/google/goggles/UrlGroupProtos$UrlGroup$Builder;

    move-result-object v0

    .line 4238
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4239
    invoke-virtual {v0}, Lcom/google/goggles/UrlGroupProtos$UrlGroup$Builder;->buildPartial()Lcom/google/goggles/UrlGroupProtos$UrlGroup;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->addUrlGroups(Lcom/google/goggles/UrlGroupProtos$UrlGroup;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4243
    :sswitch_e
    invoke-static {}, Lcom/google/goggles/FactsProtos$Facts;->newBuilder()Lcom/google/goggles/FactsProtos$Facts$Builder;

    move-result-object v0

    .line 4244
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasFacts()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4245
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getFacts()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/FactsProtos$Facts$Builder;->mergeFrom(Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/FactsProtos$Facts$Builder;

    .line 4247
    :cond_4
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4248
    invoke-virtual {v0}, Lcom/google/goggles/FactsProtos$Facts$Builder;->buildPartial()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setFacts(Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4252
    :sswitch_f
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    .line 4253
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasPersonInfo()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4254
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getPersonInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    .line 4256
    :cond_5
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4257
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setPersonInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4261
    :sswitch_10
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    .line 4262
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasPlaceInfo()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 4263
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getPlaceInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    .line 4265
    :cond_6
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4266
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setPlaceInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4270
    :sswitch_11
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4271
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4275
    :sswitch_12
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4276
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4280
    :sswitch_13
    invoke-static {}, Lcom/google/goggles/ProductInformationProtos$ProductInformation;->newBuilder()Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;

    move-result-object v0

    .line 4281
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasProductInfo()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 4282
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getProductInfo()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;->mergeFrom(Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;

    .line 4284
    :cond_7
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4285
    invoke-virtual {v0}, Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;->buildPartial()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setProductInfo(Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4289
    :sswitch_14
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;

    move-result-object v0

    .line 4290
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasTextInfo()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 4291
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getTextInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;

    .line 4293
    :cond_8
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4294
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setTextInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4298
    :sswitch_15
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    .line 4299
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasContributionInfo()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 4300
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getContributionInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    .line 4302
    :cond_9
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4303
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setContributionInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4307
    :sswitch_16
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4308
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4312
    :sswitch_17
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x40

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4313
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isWithdrawn_:Z

    goto/16 :goto_0

    .line 4317
    :sswitch_18
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4318
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isAd_:Z

    goto/16 :goto_0

    .line 4322
    :sswitch_19
    invoke-static {}, Lcom/google/goggles/MusicInformationProtos$MusicInformation;->newBuilder()Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;

    move-result-object v0

    .line 4323
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasMusicInfo()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 4324
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getMusicInfo()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;->mergeFrom(Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;

    .line 4326
    :cond_a
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4327
    invoke-virtual {v0}, Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;->buildPartial()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setMusicInfo(Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4331
    :sswitch_1a
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4332
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4336
    :sswitch_1b
    invoke-static {}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;->newBuilder()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;

    move-result-object v0

    .line 4337
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasMoreLikeThisAction()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 4338
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getMoreLikeThisAction()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;->mergeFrom(Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;

    .line 4340
    :cond_b
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4341
    invoke-virtual {v0}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;->buildPartial()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setMoreLikeThisAction(Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4345
    :sswitch_1c
    invoke-static {}, Lcom/google/goggles/ShareActionProtos$ShareAction;->newBuilder()Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;

    move-result-object v0

    .line 4346
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->hasShareAction()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 4347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->getShareAction()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;->mergeFrom(Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;

    .line 4349
    :cond_c
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4350
    invoke-virtual {v0}, Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;->buildPartial()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->setShareAction(Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    goto/16 :goto_0

    .line 4354
    :sswitch_1d
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x1000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4355
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isSimilarProduct_:Z

    goto/16 :goto_0

    .line 4153
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x9a -> :sswitch_11
        0xa2 -> :sswitch_12
        0xaa -> :sswitch_13
        0xb2 -> :sswitch_14
        0xba -> :sswitch_15
        0xc2 -> :sswitch_16
        0xc8 -> :sswitch_17
        0xd0 -> :sswitch_18
        0xda -> :sswitch_19
        0xe2 -> :sswitch_1a
        0xea -> :sswitch_1b
        0xf2 -> :sswitch_1c
        0xf8 -> :sswitch_1d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3776
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3776
    check-cast p1, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    invoke-virtual {p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3776
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeLatLng(Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 4783
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    invoke-static {}, Lcom/google/goggles/LatLngProtos$LatLng;->getDefaultInstance()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 4785
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    invoke-static {v0}, Lcom/google/goggles/LatLngProtos$LatLng;->newBuilder(Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/LatLngProtos$LatLng$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/LatLngProtos$LatLng$Builder;->mergeFrom(Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/LatLngProtos$LatLng$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/LatLngProtos$LatLng$Builder;->buildPartial()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    .line 4791
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4792
    return-object p0

    .line 4788
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    goto :goto_0
.end method

.method public mergeMoreLikeThisAction(Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x400

    .line 5438
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    invoke-static {}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;->getDefaultInstance()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5440
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    invoke-static {v0}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;->newBuilder(Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;->mergeFrom(Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;->buildPartial()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    .line 5446
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5447
    return-object p0

    .line 5443
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    goto :goto_0
.end method

.method public mergeMusicInfo(Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x20

    .line 5281
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    invoke-static {}, Lcom/google/goggles/MusicInformationProtos$MusicInformation;->getDefaultInstance()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5283
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    invoke-static {v0}, Lcom/google/goggles/MusicInformationProtos$MusicInformation;->newBuilder(Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;->mergeFrom(Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;->buildPartial()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    .line 5289
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5290
    return-object p0

    .line 5286
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    goto :goto_0
.end method

.method public mergePersonInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x1

    .line 5066
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5068
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 5074
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5075
    return-object p0

    .line 5071
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    goto :goto_0
.end method

.method public mergePlaceInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x2

    .line 5109
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5111
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 5117
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5118
    return-object p0

    .line 5114
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    goto :goto_0
.end method

.method public mergeProductInfo(Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x4

    .line 5152
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    invoke-static {}, Lcom/google/goggles/ProductInformationProtos$ProductInformation;->getDefaultInstance()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5154
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    invoke-static {v0}, Lcom/google/goggles/ProductInformationProtos$ProductInformation;->newBuilder(Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;->mergeFrom(Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;->buildPartial()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    .line 5160
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5161
    return-object p0

    .line 5157
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    goto :goto_0
.end method

.method public mergeShareAction(Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x800

    .line 5481
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    invoke-static {}, Lcom/google/goggles/ShareActionProtos$ShareAction;->getDefaultInstance()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5483
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    invoke-static {v0}, Lcom/google/goggles/ShareActionProtos$ShareAction;->newBuilder(Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;->mergeFrom(Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;->buildPartial()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    .line 5489
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5490
    return-object p0

    .line 5486
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    goto :goto_0
.end method

.method public mergeTextInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x8

    .line 5195
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5197
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    .line 5203
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5204
    return-object p0

    .line 5200
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    goto :goto_0
.end method

.method public removeUrlGroups(I)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4956
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4957
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 4959
    return-object p0
.end method

.method public setBoundingBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4635
    invoke-virtual {p1}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->build()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 4637
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4638
    return-object p0
.end method

.method public setBoundingBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4625
    if-nez p1, :cond_0

    .line 4626
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4628
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 4630
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4631
    return-object p0
.end method

.method public setBoundingBoxRequestId(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5393
    if-nez p1, :cond_0

    .line 5394
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5396
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5397
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 5399
    return-object p0
.end method

.method setBoundingBoxRequestId(Lcom/google/protobuf/ByteString;)V
    .locals 2
    .parameter

    .prologue
    .line 5408
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5409
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 5411
    return-void
.end method

.method public setCanonicalImage(Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4678
    invoke-virtual {p1}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage$Builder;->build()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    .line 4680
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4681
    return-object p0
.end method

.method public setCanonicalImage(Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4668
    if-nez p1, :cond_0

    .line 4669
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4671
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    .line 4673
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4674
    return-object p0
.end method

.method public setContributionInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5232
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 5234
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5235
    return-object p0
.end method

.method public setContributionInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5222
    if-nez p1, :cond_0

    .line 5223
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5225
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 5227
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5228
    return-object p0
.end method

.method public setDirectUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4596
    if-nez p1, :cond_0

    .line 4597
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4599
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4600
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    .line 4602
    return-object p0
.end method

.method setDirectUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4611
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4612
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->directUrl_:Ljava/lang/Object;

    .line 4614
    return-void
.end method

.method public setFacts(Lcom/google/goggles/FactsProtos$Facts$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5017
    invoke-virtual {p1}, Lcom/google/goggles/FactsProtos$Facts$Builder;->build()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    .line 5019
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5020
    return-object p0
.end method

.method public setFacts(Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5007
    if-nez p1, :cond_0

    .line 5008
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5010
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    .line 5012
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5013
    return-object p0
.end method

.method public setIsAd(Z)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5329
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5330
    iput-boolean p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isAd_:Z

    .line 5332
    return-object p0
.end method

.method public setIsSimilarProduct(Z)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5508
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x1000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5509
    iput-boolean p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isSimilarProduct_:Z

    .line 5511
    return-object p0
.end method

.method public setIsWithdrawn(Z)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5308
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x40

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5309
    iput-boolean p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->isWithdrawn_:Z

    .line 5311
    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4853
    if-nez p1, :cond_0

    .line 4854
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4856
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4857
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    .line 4859
    return-object p0
.end method

.method setLanguage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4868
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4869
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->language_:Ljava/lang/Object;

    .line 4871
    return-void
.end method

.method public setLatLng(Lcom/google/goggles/LatLngProtos$LatLng$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4777
    invoke-virtual {p1}, Lcom/google/goggles/LatLngProtos$LatLng$Builder;->build()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    .line 4779
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4780
    return-object p0
.end method

.method public setLatLng(Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4767
    if-nez p1, :cond_0

    .line 4768
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4770
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    .line 4772
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4773
    return-object p0
.end method

.method public setLocationText(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4817
    if-nez p1, :cond_0

    .line 4818
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4820
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4821
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    .line 4823
    return-object p0
.end method

.method setLocationText(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4832
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4833
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->locationText_:Ljava/lang/Object;

    .line 4835
    return-void
.end method

.method public setMoreLikeThisAction(Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5432
    invoke-virtual {p1}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction$Builder;->build()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    .line 5434
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x400

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5435
    return-object p0
.end method

.method public setMoreLikeThisAction(Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5422
    if-nez p1, :cond_0

    .line 5423
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5425
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    .line 5427
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x400

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5428
    return-object p0
.end method

.method public setMusicInfo(Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5275
    invoke-virtual {p1}, Lcom/google/goggles/MusicInformationProtos$MusicInformation$Builder;->build()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    .line 5277
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5278
    return-object p0
.end method

.method public setMusicInfo(Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5265
    if-nez p1, :cond_0

    .line 5266
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5268
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    .line 5270
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5271
    return-object p0
.end method

.method public setPersonInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5060
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 5062
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5063
    return-object p0
.end method

.method public setPersonInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5050
    if-nez p1, :cond_0

    .line 5051
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5053
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 5055
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5056
    return-object p0
.end method

.method public setPlaceInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5103
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 5105
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5106
    return-object p0
.end method

.method public setPlaceInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5093
    if-nez p1, :cond_0

    .line 5094
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5096
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 5098
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5099
    return-object p0
.end method

.method public setProductInfo(Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5146
    invoke-virtual {p1}, Lcom/google/goggles/ProductInformationProtos$ProductInformation$Builder;->build()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    .line 5148
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x4

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5149
    return-object p0
.end method

.method public setProductInfo(Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5136
    if-nez p1, :cond_0

    .line 5137
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5139
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    .line 5141
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x4

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5142
    return-object p0
.end method

.method public setResultId(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5357
    if-nez p1, :cond_0

    .line 5358
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5360
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5361
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    .line 5363
    return-object p0
.end method

.method setResultId(Lcom/google/protobuf/ByteString;)V
    .locals 2
    .parameter

    .prologue
    .line 5372
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5373
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultId_:Ljava/lang/Object;

    .line 5375
    return-void
.end method

.method public setResultUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4560
    if-nez p1, :cond_0

    .line 4561
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4563
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4564
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    .line 4566
    return-object p0
.end method

.method setResultUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4575
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4576
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->resultUrl_:Ljava/lang/Object;

    .line 4578
    return-void
.end method

.method public setShareAction(Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5475
    invoke-virtual {p1}, Lcom/google/goggles/ShareActionProtos$ShareAction$Builder;->build()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    .line 5477
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x800

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5478
    return-object p0
.end method

.method public setShareAction(Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5465
    if-nez p1, :cond_0

    .line 5466
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5468
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    .line 5470
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x800

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5471
    return-object p0
.end method

.method public setSubtitle(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4416
    if-nez p1, :cond_0

    .line 4417
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4419
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4420
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    .line 4422
    return-object p0
.end method

.method setSubtitle(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4431
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4432
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtitle_:Ljava/lang/Object;

    .line 4434
    return-void
.end method

.method public setSubtype(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4488
    if-nez p1, :cond_0

    .line 4489
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4491
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4492
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    .line 4494
    return-object p0
.end method

.method setSubtype(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4503
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4504
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->subtype_:Ljava/lang/Object;

    .line 4506
    return-void
.end method

.method public setTextInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5189
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    .line 5191
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5192
    return-object p0
.end method

.method public setTextInfo(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5179
    if-nez p1, :cond_0

    .line 5180
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5182
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    .line 5184
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    const/high16 v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 5185
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4380
    if-nez p1, :cond_0

    .line 4381
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4383
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4384
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    .line 4386
    return-object p0
.end method

.method setTitle(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4395
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4396
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->title_:Ljava/lang/Object;

    .line 4398
    return-void
.end method

.method public setTtsDescription(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4524
    if-nez p1, :cond_0

    .line 4525
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4527
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4528
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    .line 4530
    return-object p0
.end method

.method setTtsDescription(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4539
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4540
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ttsDescription_:Ljava/lang/Object;

    .line 4542
    return-void
.end method

.method public setType(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4452
    if-nez p1, :cond_0

    .line 4453
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4455
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4456
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    .line 4458
    return-object p0
.end method

.method setType(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4467
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4468
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->type_:Ljava/lang/Object;

    .line 4470
    return-void
.end method

.method public setUrlGroups(ILcom/google/goggles/UrlGroupProtos$UrlGroup$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 4904
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4905
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/goggles/UrlGroupProtos$UrlGroup$Builder;->build()Lcom/google/goggles/UrlGroupProtos$UrlGroup;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4907
    return-object p0
.end method

.method public setUrlGroups(ILcom/google/goggles/UrlGroupProtos$UrlGroup;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 4894
    if-nez p2, :cond_0

    .line 4895
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4897
    :cond_0
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlGroupsIsMutable()V

    .line 4898
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4900
    return-object p0
.end method

.method public setUrls(ILjava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4722
    if-nez p2, :cond_0

    .line 4723
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4725
    :cond_0
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->ensureUrlsIsMutable()V

    .line 4726
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4728
    return-object p0
.end method

.method public setWebsearchUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4978
    if-nez p1, :cond_0

    .line 4979
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4981
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4982
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    .line 4984
    return-object p0
.end method

.method setWebsearchUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4993
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->bitField0_:I

    .line 4994
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->websearchUrl_:Ljava/lang/Object;

    .line 4996
    return-void
.end method
