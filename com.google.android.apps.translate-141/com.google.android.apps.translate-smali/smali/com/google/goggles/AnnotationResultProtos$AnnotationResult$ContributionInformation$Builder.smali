.class public final Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AnnotationResultProtos.java"

# interfaces
.implements Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformationOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;",
        ">;",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformationOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

.field private creationTimestampMs_:J

.field private momentId_:Ljava/lang/Object;

.field private reportAbuseUrl_:Ljava/lang/Object;

.field private userIsOwner_:Z

.field private userSubmittedDescription_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2459
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 2629
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 2693
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2729
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2786
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    .line 2460
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->maybeForceBuilderInitialization()V

    .line 2461
    return-void
.end method

.method static synthetic access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2454
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2454
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2504
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    .line 2505
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2506
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 2509
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2466
    new-instance v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    invoke-direct {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 2464
    return-void
.end method


# virtual methods
.method public build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 2

    .prologue
    .line 2495
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    .line 2496
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2497
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2499
    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2513
    new-instance v2, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;-><init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;Lcom/google/goggles/AnnotationResultProtos$1;)V

    .line 2514
    iget v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2515
    const/4 v1, 0x0

    .line 2516
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 2519
    :goto_0
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->access$2902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 2520
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2521
    or-int/lit8 v0, v0, 0x2

    .line 2523
    :cond_0
    iget-wide v4, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->creationTimestampMs_:J

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->creationTimestampMs_:J
    invoke-static {v2, v4, v5}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->access$3002(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;J)J

    .line 2524
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2525
    or-int/lit8 v0, v0, 0x4

    .line 2527
    :cond_1
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userSubmittedDescription_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->access$3102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2528
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2529
    or-int/lit8 v0, v0, 0x8

    .line 2531
    :cond_2
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->reportAbuseUrl_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->access$3202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2532
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2533
    or-int/lit8 v0, v0, 0x10

    .line 2535
    :cond_3
    iget-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userIsOwner_:Z

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userIsOwner_:Z
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->access$3302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Z)Z

    .line 2536
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 2537
    or-int/lit8 v0, v0, 0x20

    .line 2539
    :cond_4
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->momentId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->access$3402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2540
    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->access$3502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;I)I

    .line 2541
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 2

    .prologue
    .line 2470
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 2471
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 2472
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2473
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->creationTimestampMs_:J

    .line 2474
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2475
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2476
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2477
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2478
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2479
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userIsOwner_:Z

    .line 2480
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2481
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    .line 2482
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2483
    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearContributor()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2665
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 2667
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2668
    return-object p0
.end method

.method public clearCreationTimestampMs()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 2

    .prologue
    .line 2686
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2687
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->creationTimestampMs_:J

    .line 2689
    return-object p0
.end method

.method public clearMomentId()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2810
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2811
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getMomentId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    .line 2813
    return-object p0
.end method

.method public clearReportAbuseUrl()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2753
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2754
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getReportAbuseUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2756
    return-object p0
.end method

.method public clearUserIsOwner()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2779
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2780
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userIsOwner_:Z

    .line 2782
    return-object p0
.end method

.method public clearUserSubmittedDescription()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2717
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2718
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getUserSubmittedDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2720
    return-object p0
.end method

.method public clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 2

    .prologue
    .line 2487
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getContributor()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 1

    .prologue
    .line 2634
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    return-object v0
.end method

.method public getCreationTimestampMs()J
    .locals 2

    .prologue
    .line 2677
    iget-wide v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->creationTimestampMs_:J

    return-wide v0
.end method

.method public getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1

    .prologue
    .line 2491
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public getMomentId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2791
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    .line 2792
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2793
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2794
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    .line 2797
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getReportAbuseUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2734
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2735
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2736
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2737
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2740
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getUserIsOwner()Z
    .locals 1

    .prologue
    .line 2770
    iget-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userIsOwner_:Z

    return v0
.end method

.method public getUserSubmittedDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2698
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2699
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2700
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2701
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2704
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasContributor()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2631
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCreationTimestampMs()Z
    .locals 2

    .prologue
    .line 2674
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMomentId()Z
    .locals 2

    .prologue
    .line 2788
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReportAbuseUrl()Z
    .locals 2

    .prologue
    .line 2731
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserIsOwner()Z
    .locals 2

    .prologue
    .line 2767
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserSubmittedDescription()Z
    .locals 2

    .prologue
    .line 2695
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 2568
    const/4 v0, 0x1

    return v0
.end method

.method public mergeContributor(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 2
    .parameter

    .prologue
    .line 2653
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2655
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 2661
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2662
    return-object p0

    .line 2658
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 2
    .parameter

    .prologue
    .line 2545
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2564
    :cond_0
    :goto_0
    return-object p0

    .line 2546
    :cond_1
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->hasContributor()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2547
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getContributor()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeContributor(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    .line 2549
    :cond_2
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->hasCreationTimestampMs()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2550
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getCreationTimestampMs()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->setCreationTimestampMs(J)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    .line 2552
    :cond_3
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->hasUserSubmittedDescription()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2553
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getUserSubmittedDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->setUserSubmittedDescription(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    .line 2555
    :cond_4
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->hasReportAbuseUrl()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2556
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getReportAbuseUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->setReportAbuseUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    .line 2558
    :cond_5
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->hasUserIsOwner()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2559
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getUserIsOwner()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->setUserIsOwner(Z)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    .line 2561
    :cond_6
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->hasMomentId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2562
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getMomentId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->setMomentId(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2576
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 2577
    sparse-switch v0, :sswitch_data_0

    .line 2582
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2584
    :sswitch_0
    return-object p0

    .line 2589
    :sswitch_1
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    .line 2590
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->hasContributor()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2591
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->getContributor()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    .line 2593
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 2594
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->setContributor(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    goto :goto_0

    .line 2598
    :sswitch_2
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2599
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->creationTimestampMs_:J

    goto :goto_0

    .line 2603
    :sswitch_3
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2604
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    goto :goto_0

    .line 2608
    :sswitch_4
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2609
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    goto :goto_0

    .line 2613
    :sswitch_5
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2614
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userIsOwner_:Z

    goto :goto_0

    .line 2618
    :sswitch_6
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2619
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    goto :goto_0

    .line 2577
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2454
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2454
    check-cast p1, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    invoke-virtual {p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2454
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setContributor(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2647
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 2649
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2650
    return-object p0
.end method

.method public setContributor(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2637
    if-nez p1, :cond_0

    .line 2638
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2640
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 2642
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2643
    return-object p0
.end method

.method public setCreationTimestampMs(J)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2680
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2681
    iput-wide p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->creationTimestampMs_:J

    .line 2683
    return-object p0
.end method

.method public setMomentId(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2801
    if-nez p1, :cond_0

    .line 2802
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2804
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2805
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    .line 2807
    return-object p0
.end method

.method setMomentId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2816
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2817
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->momentId_:Ljava/lang/Object;

    .line 2819
    return-void
.end method

.method public setReportAbuseUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2744
    if-nez p1, :cond_0

    .line 2745
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2747
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2748
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2750
    return-object p0
.end method

.method setReportAbuseUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2759
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2760
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2762
    return-void
.end method

.method public setUserIsOwner(Z)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2773
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2774
    iput-boolean p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userIsOwner_:Z

    .line 2776
    return-object p0
.end method

.method public setUserSubmittedDescription(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2708
    if-nez p1, :cond_0

    .line 2709
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2711
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2712
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2714
    return-object p0
.end method

.method setUserSubmittedDescription(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2723
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->bitField0_:I

    .line 2724
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2726
    return-void
.end method
