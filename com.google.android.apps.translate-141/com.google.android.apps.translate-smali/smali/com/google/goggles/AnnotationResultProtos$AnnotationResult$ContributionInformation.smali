.class public final Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AnnotationResultProtos.java"

# interfaces
.implements Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformationOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContributionInformation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    }
.end annotation


# static fields
.field public static final CONTRIBUTOR_FIELD_NUMBER:I = 0x1

.field public static final CREATION_TIMESTAMP_MS_FIELD_NUMBER:I = 0x3

.field public static final MOMENT_ID_FIELD_NUMBER:I = 0x7

.field public static final REPORT_ABUSE_URL_FIELD_NUMBER:I = 0x5

.field public static final USER_IS_OWNER_FIELD_NUMBER:I = 0x6

.field public static final USER_SUBMITTED_DESCRIPTION_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

.field private creationTimestampMs_:J

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private momentId_:Ljava/lang/Object;

.field private reportAbuseUrl_:Ljava/lang/Object;

.field private userIsOwner_:Z

.field private userSubmittedDescription_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2825
    new-instance v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;-><init>(Z)V

    sput-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 2826
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    invoke-direct {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->initFields()V

    .line 2827
    return-void
.end method

.method private constructor <init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2159
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 2307
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->memoizedIsInitialized:B

    .line 2339
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->memoizedSerializedSize:I

    .line 2160
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;Lcom/google/goggles/AnnotationResultProtos$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2154
    invoke-direct {p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;-><init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2161
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2307
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->memoizedIsInitialized:B

    .line 2339
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->memoizedSerializedSize:I

    .line 2161
    return-void
.end method

.method static synthetic access$2902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2154
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    return-object p1
.end method

.method static synthetic access$3002(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2154
    iput-wide p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->creationTimestampMs_:J

    return-wide p1
.end method

.method static synthetic access$3102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2154
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userSubmittedDescription_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2154
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->reportAbuseUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2154
    iput-boolean p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userIsOwner_:Z

    return p1
.end method

.method static synthetic access$3402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2154
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->momentId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2154
    iput p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1

    .prologue
    .line 2165
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    return-object v0
.end method

.method private getMomentIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2288
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->momentId_:Ljava/lang/Object;

    .line 2289
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2290
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2292
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->momentId_:Ljava/lang/Object;

    .line 2295
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getReportAbuseUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2246
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2247
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2248
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2250
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2253
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getUserSubmittedDescriptionBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2214
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2215
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2216
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2218
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2221
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    .line 2300
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 2301
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->creationTimestampMs_:J

    .line 2302
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2303
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2304
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userIsOwner_:Z

    .line 2305
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->momentId_:Ljava/lang/Object;

    .line 2306
    return-void
.end method

.method public static newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2447
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2700()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2450
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2416
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    .line 2417
    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2418
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    .line 2420
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2427
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    .line 2428
    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2429
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    .line 2431
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2383
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2389
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2437
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2443
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2405
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2411
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2394
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2400
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;->access$2600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContributor()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 1

    .prologue
    .line 2180
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    return-object v0
.end method

.method public getCreationTimestampMs()J
    .locals 2

    .prologue
    .line 2190
    iget-wide v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->creationTimestampMs_:J

    return-wide v0
.end method

.method public getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1

    .prologue
    .line 2169
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2154
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    return-object v0
.end method

.method public getMomentId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2274
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->momentId_:Ljava/lang/Object;

    .line 2275
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2276
    check-cast v0, Ljava/lang/String;

    .line 2284
    :goto_0
    return-object v0

    .line 2278
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2280
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2281
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2282
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->momentId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2284
    goto :goto_0
.end method

.method public getReportAbuseUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2232
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->reportAbuseUrl_:Ljava/lang/Object;

    .line 2233
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2234
    check-cast v0, Ljava/lang/String;

    .line 2242
    :goto_0
    return-object v0

    .line 2236
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2238
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2239
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2240
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->reportAbuseUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2242
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    .line 2341
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->memoizedSerializedSize:I

    .line 2342
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2370
    :goto_0
    return v0

    .line 2344
    :cond_0
    const/4 v0, 0x0

    .line 2345
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2346
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2349
    :cond_1
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 2350
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->creationTimestampMs_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2353
    :cond_2
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2354
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getUserSubmittedDescriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2357
    :cond_3
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2358
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getReportAbuseUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2361
    :cond_4
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 2362
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userIsOwner_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2365
    :cond_5
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 2366
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getMomentIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2369
    :cond_6
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getUserIsOwner()Z
    .locals 1

    .prologue
    .line 2264
    iget-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userIsOwner_:Z

    return v0
.end method

.method public getUserSubmittedDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2200
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userSubmittedDescription_:Ljava/lang/Object;

    .line 2201
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2202
    check-cast v0, Ljava/lang/String;

    .line 2210
    :goto_0
    return-object v0

    .line 2204
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2206
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2207
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2208
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userSubmittedDescription_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2210
    goto :goto_0
.end method

.method public hasContributor()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2177
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCreationTimestampMs()Z
    .locals 2

    .prologue
    .line 2187
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMomentId()Z
    .locals 2

    .prologue
    .line 2271
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReportAbuseUrl()Z
    .locals 2

    .prologue
    .line 2229
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserIsOwner()Z
    .locals 2

    .prologue
    .line 2261
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserSubmittedDescription()Z
    .locals 2

    .prologue
    .line 2197
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2309
    iget-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->memoizedIsInitialized:B

    .line 2310
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2313
    :goto_0
    return v0

    .line 2310
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2312
    :cond_1
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public newBuilderForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2448
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2154
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilderForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;
    .locals 1

    .prologue
    .line 2452
    invoke-static {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2154
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->toBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 2377
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    .line 2318
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getSerializedSize()I

    .line 2319
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2320
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->contributor_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 2322
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2323
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->creationTimestampMs_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 2325
    :cond_1
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2326
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getUserSubmittedDescriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2328
    :cond_2
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2329
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getReportAbuseUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2331
    :cond_3
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2332
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->userIsOwner_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 2334
    :cond_4
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 2335
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getMomentIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2337
    :cond_5
    return-void
.end method
