.class public final Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AnnotationResultProtos.java"

# interfaces
.implements Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformationOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;",
        ">;",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformationOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private displayName_:Ljava/lang/Object;

.field private profilePhotoUrl_:Ljava/lang/Object;

.field private profileUrl_:Ljava/lang/Object;

.field private userId_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 462
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 600
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    .line 636
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    .line 672
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    .line 708
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    .line 463
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->maybeForceBuilderInitialization()V

    .line 464
    return-void
.end method

.method static synthetic access$000(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 457
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1

    .prologue
    .line 457
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    .line 504
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 505
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 508
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1

    .prologue
    .line 469
    new-instance v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    invoke-direct {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 467
    return-void
.end method


# virtual methods
.method public build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 2

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    .line 495
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 496
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 498
    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 512
    new-instance v2, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;-><init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;Lcom/google/goggles/AnnotationResultProtos$1;)V

    .line 513
    iget v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 514
    const/4 v1, 0x0

    .line 515
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 518
    :goto_0
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->userId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->access$302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 520
    or-int/lit8 v0, v0, 0x2

    .line 522
    :cond_0
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->displayName_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->access$402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 524
    or-int/lit8 v0, v0, 0x4

    .line 526
    :cond_1
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->profileUrl_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->access$502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 528
    or-int/lit8 v0, v0, 0x8

    .line 530
    :cond_2
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->profilePhotoUrl_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->access$602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->access$702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;I)I

    .line 532
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1

    .prologue
    .line 473
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 474
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    .line 475
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 476
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    .line 477
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 478
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    .line 479
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 480
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    .line 481
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 482
    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearDisplayName()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1

    .prologue
    .line 660
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 661
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    .line 663
    return-object p0
.end method

.method public clearProfilePhotoUrl()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1

    .prologue
    .line 732
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 733
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    .line 735
    return-object p0
.end method

.method public clearProfileUrl()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1

    .prologue
    .line 696
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 697
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getProfileUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    .line 699
    return-object p0
.end method

.method public clearUserId()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1

    .prologue
    .line 624
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 625
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getUserId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    .line 627
    return-object p0
.end method

.method public clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 2

    .prologue
    .line 486
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 1

    .prologue
    .line 490
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    .line 642
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 643
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 644
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    .line 647
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getProfilePhotoUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    .line 714
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 715
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 716
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    .line 719
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getProfileUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    .line 678
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 679
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 680
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    .line 683
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    .line 606
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 607
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 608
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    .line 611
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasDisplayName()Z
    .locals 2

    .prologue
    .line 638
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProfilePhotoUrl()Z
    .locals 2

    .prologue
    .line 710
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProfileUrl()Z
    .locals 2

    .prologue
    .line 674
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserId()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 602
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 553
    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 536
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-object p0

    .line 537
    :cond_1
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->hasUserId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 538
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getUserId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->setUserId(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    .line 540
    :cond_2
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 541
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->setDisplayName(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    .line 543
    :cond_3
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->hasProfileUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 544
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getProfileUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->setProfileUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    .line 546
    :cond_4
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->hasProfilePhotoUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->setProfilePhotoUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 561
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 562
    sparse-switch v0, :sswitch_data_0

    .line 567
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 569
    :sswitch_0
    return-object p0

    .line 574
    :sswitch_1
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 575
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    goto :goto_0

    .line 579
    :sswitch_2
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 580
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    goto :goto_0

    .line 584
    :sswitch_3
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 585
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    goto :goto_0

    .line 589
    :sswitch_4
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 590
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    goto :goto_0

    .line 562
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 457
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 457
    check-cast p1, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-virtual {p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 457
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 651
    if-nez p1, :cond_0

    .line 652
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 654
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 655
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    .line 657
    return-object p0
.end method

.method setDisplayName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 666
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 667
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->displayName_:Ljava/lang/Object;

    .line 669
    return-void
.end method

.method public setProfilePhotoUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 723
    if-nez p1, :cond_0

    .line 724
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 726
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 727
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    .line 729
    return-object p0
.end method

.method setProfilePhotoUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 738
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 739
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    .line 741
    return-void
.end method

.method public setProfileUrl(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 687
    if-nez p1, :cond_0

    .line 688
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 690
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 691
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    .line 693
    return-object p0
.end method

.method setProfileUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 702
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 703
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->profileUrl_:Ljava/lang/Object;

    .line 705
    return-void
.end method

.method public setUserId(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 615
    if-nez p1, :cond_0

    .line 616
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 618
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 619
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    .line 621
    return-object p0
.end method

.method setUserId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 630
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->bitField0_:I

    .line 631
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation$Builder;->userId_:Ljava/lang/Object;

    .line 633
    return-void
.end method
