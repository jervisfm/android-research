.class public final Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AnnotationResultProtos.java"

# interfaces
.implements Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformationOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlaceInformation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;
    }
.end annotation


# static fields
.field public static final PLACE_REFERENCE_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private placeReference_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1067
    new-instance v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;-><init>(Z)V

    sput-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 1068
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    invoke-direct {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->initFields()V

    .line 1069
    return-void
.end method

.method private constructor <init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 766
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 815
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->memoizedIsInitialized:B

    .line 832
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->memoizedSerializedSize:I

    .line 767
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;Lcom/google/goggles/AnnotationResultProtos$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 761
    invoke-direct {p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;-><init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 768
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 815
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->memoizedIsInitialized:B

    .line 832
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->memoizedSerializedSize:I

    .line 768
    return-void
.end method

.method static synthetic access$1102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 761
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->placeReference_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 761
    iput p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1

    .prologue
    .line 772
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    return-object v0
.end method

.method private getPlaceReferenceBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 801
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->placeReference_:Ljava/lang/Object;

    .line 802
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 803
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 805
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->placeReference_:Ljava/lang/Object;

    .line 808
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 813
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->placeReference_:Ljava/lang/Object;

    .line 814
    return-void
.end method

.method public static newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;
    .locals 1

    .prologue
    .line 920
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$900()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;
    .locals 1
    .parameter

    .prologue
    .line 923
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 889
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    .line 890
    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 891
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    .line 893
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 900
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    .line 901
    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 902
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    .line 904
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 856
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 862
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 910
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 916
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 878
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 884
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 867
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 873
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;->access$800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1

    .prologue
    .line 776
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    return-object v0
.end method

.method public getPlaceReference()Ljava/lang/String;
    .locals 2

    .prologue
    .line 787
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->placeReference_:Ljava/lang/Object;

    .line 788
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 789
    check-cast v0, Ljava/lang/String;

    .line 797
    :goto_0
    return-object v0

    .line 791
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 793
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 794
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 795
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->placeReference_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 797
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 834
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->memoizedSerializedSize:I

    .line 835
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 843
    :goto_0
    return v0

    .line 837
    :cond_0
    const/4 v0, 0x0

    .line 838
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 839
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getPlaceReferenceBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 842
    :cond_1
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasPlaceReference()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 784
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 817
    iget-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->memoizedIsInitialized:B

    .line 818
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 821
    :goto_0
    return v0

    .line 818
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 820
    :cond_1
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public newBuilderForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;
    .locals 1

    .prologue
    .line 921
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilderForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;
    .locals 1

    .prologue
    .line 925
    invoke-static {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->toBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 850
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 826
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getSerializedSize()I

    .line 827
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 828
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getPlaceReferenceBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 830
    :cond_0
    return-void
.end method
