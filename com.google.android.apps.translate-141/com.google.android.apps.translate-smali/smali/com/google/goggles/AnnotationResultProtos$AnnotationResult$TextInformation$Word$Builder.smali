.class public final Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AnnotationResultProtos.java"

# interfaces
.implements Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$WordOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;",
        ">;",
        "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$WordOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

.field private characterBoxes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/goggles/BoundingBoxProtos$BoundingBox;",
            ">;"
        }
    .end annotation
.end field

.field private text_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1352
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 1501
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 1544
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    .line 1580
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    .line 1353
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->maybeForceBuilderInitialization()V

    .line 1354
    return-void
.end method

.method static synthetic access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1347
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1347
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1391
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    .line 1392
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1393
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 1396
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1359
    new-instance v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    invoke-direct {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCharacterBoxesIsMutable()V
    .locals 2

    .prologue
    .line 1583
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1584
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    .line 1585
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1587
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 1357
    return-void
.end method


# virtual methods
.method public addAllCharacterBoxes(Ljava/lang/Iterable;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/goggles/BoundingBoxProtos$BoundingBox;",
            ">;)",
            "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;"
        }
    .end annotation

    .prologue
    .line 1650
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1651
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 1653
    return-object p0
.end method

.method public addCharacterBoxes(ILcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1643
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1644
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->build()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1646
    return-object p0
.end method

.method public addCharacterBoxes(ILcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1626
    if-nez p2, :cond_0

    .line 1627
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1629
    :cond_0
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1630
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1632
    return-object p0
.end method

.method public addCharacterBoxes(Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 2
    .parameter

    .prologue
    .line 1636
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1637
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->build()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1639
    return-object p0
.end method

.method public addCharacterBoxes(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1616
    if-nez p1, :cond_0

    .line 1617
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1619
    :cond_0
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1620
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1622
    return-object p0
.end method

.method public build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 2

    .prologue
    .line 1382
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    .line 1383
    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1384
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1386
    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->build()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1400
    new-instance v2, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;-><init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;Lcom/google/goggles/AnnotationResultProtos$1;)V

    .line 1401
    iget v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1402
    const/4 v1, 0x0

    .line 1403
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 1406
    :goto_0
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->access$1602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 1407
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 1408
    or-int/lit8 v0, v0, 0x2

    .line 1410
    :cond_0
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->text_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->access$1702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1411
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 1412
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    .line 1413
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1415
    :cond_1
    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->access$1802(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;Ljava/util/List;)Ljava/util/List;

    .line 1416
    #setter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->access$1902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;I)I

    .line 1417
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1363
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 1364
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 1365
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1366
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    .line 1367
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1368
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    .line 1369
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1370
    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->clear()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearBox()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1537
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 1539
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1540
    return-object p0
.end method

.method public clearCharacterBoxes()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1656
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    .line 1657
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1659
    return-object p0
.end method

.method public clearText()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1568
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1569
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    .line 1571
    return-object p0
.end method

.method public clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 2

    .prologue
    .line 1374
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildPartial()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->clone()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 1

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object v0
.end method

.method public getCharacterBoxes(I)Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 1
    .parameter

    .prologue
    .line 1596
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object v0
.end method

.method public getCharacterBoxesCount()I
    .locals 1

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCharacterBoxesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/goggles/BoundingBoxProtos$BoundingBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1

    .prologue
    .line 1378
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1347
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1549
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    .line 1550
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1551
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 1552
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    .line 1555
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBox()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1503
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    .line 1546
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1442
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->hasBox()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1443
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->getBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1454
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 1448
    :goto_1
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->getCharacterBoxesCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1449
    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->getCharacterBoxes(I)Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1448
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1454
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public mergeBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 2
    .parameter

    .prologue
    .line 1525
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1527
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    invoke-static {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->newBuilder(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->mergeFrom(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->buildPartial()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 1533
    :goto_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1534
    return-object p0

    .line 1530
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 2
    .parameter

    .prologue
    .line 1421
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1438
    :cond_0
    :goto_0
    return-object p0

    .line 1422
    :cond_1
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->hasBox()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1423
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    .line 1425
    :cond_2
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->hasText()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1426
    invoke-virtual {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->setText(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    .line 1428
    :cond_3
    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->access$1800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1429
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1430
    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->access$1800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    .line 1431
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    goto :goto_0

    .line 1433
    :cond_4
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1434
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    #getter for: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->access$1800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1462
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 1463
    sparse-switch v0, :sswitch_data_0

    .line 1468
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1470
    :sswitch_0
    return-object p0

    .line 1475
    :sswitch_1
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->newBuilder()Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    move-result-object v0

    .line 1476
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->hasBox()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1477
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->getBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->mergeFrom(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    .line 1479
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 1480
    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->buildPartial()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->setBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    goto :goto_0

    .line 1484
    :sswitch_2
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1485
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    goto :goto_0

    .line 1489
    :sswitch_3
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->newBuilder()Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;

    move-result-object v0

    .line 1490
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 1491
    invoke-virtual {v0}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->buildPartial()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->addCharacterBoxes(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    goto :goto_0

    .line 1463
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1347
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1347
    check-cast p1, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    invoke-virtual {p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1347
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public removeCharacterBoxes(I)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1662
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1663
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1665
    return-object p0
.end method

.method public setBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1519
    invoke-virtual {p1}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->build()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 1521
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1522
    return-object p0
.end method

.method public setBox(Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1509
    if-nez p1, :cond_0

    .line 1510
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1512
    :cond_0
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 1514
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1515
    return-object p0
.end method

.method public setCharacterBoxes(ILcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1610
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1611
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox$Builder;->build()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1613
    return-object p0
.end method

.method public setCharacterBoxes(ILcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1600
    if-nez p2, :cond_0

    .line 1601
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1603
    :cond_0
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->ensureCharacterBoxesIsMutable()V

    .line 1604
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1606
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1559
    if-nez p1, :cond_0

    .line 1560
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1562
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1563
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    .line 1565
    return-object p0
.end method

.method setText(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 1574
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->bitField0_:I

    .line 1575
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->text_:Ljava/lang/Object;

    .line 1577
    return-void
.end method
