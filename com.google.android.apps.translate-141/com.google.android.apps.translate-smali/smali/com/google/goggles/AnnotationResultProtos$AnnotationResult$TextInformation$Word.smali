.class public final Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AnnotationResultProtos.java"

# interfaces
.implements Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$WordOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Word"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    }
.end annotation


# static fields
.field public static final BOX_FIELD_NUMBER:I = 0x1

.field public static final CHARACTER_BOXES_FIELD_NUMBER:I = 0x3

.field public static final TEXT_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

.field private characterBoxes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/goggles/BoundingBoxProtos$BoundingBox;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private text_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1672
    new-instance v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;-><init>(Z)V

    sput-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    .line 1673
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    invoke-direct {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->initFields()V

    .line 1674
    return-void
.end method

.method private constructor <init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1127
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 1209
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedIsInitialized:B

    .line 1244
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedSerializedSize:I

    .line 1128
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;Lcom/google/goggles/AnnotationResultProtos$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1122
    invoke-direct {p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;-><init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1129
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1209
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedIsInitialized:B

    .line 1244
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedSerializedSize:I

    .line 1129
    return-void
.end method

.method static synthetic access$1602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1122
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1122
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->text_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 1122
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1122
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1122
    iput p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1

    .prologue
    .line 1133
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    return-object v0
.end method

.method private getTextBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->text_:Ljava/lang/Object;

    .line 1173
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1174
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1176
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->text_:Ljava/lang/Object;

    .line 1179
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 1205
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 1206
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->text_:Ljava/lang/Object;

    .line 1207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    .line 1208
    return-void
.end method

.method public static newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1340
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1400()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1343
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1309
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    .line 1310
    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1311
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    .line 1313
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1320
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    .line 1321
    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1322
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    .line 1324
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1276
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1282
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1330
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1336
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1298
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1304
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1287
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1293
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;->access$1300(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 1

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object v0
.end method

.method public getCharacterBoxes(I)Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 1
    .parameter

    .prologue
    .line 1197
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object v0
.end method

.method public getCharacterBoxesCount()I
    .locals 1

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCharacterBoxesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/goggles/BoundingBoxProtos$BoundingBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1187
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    return-object v0
.end method

.method public getCharacterBoxesOrBuilder(I)Lcom/google/goggles/BoundingBoxProtos$BoundingBoxOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/BoundingBoxProtos$BoundingBoxOrBuilder;

    return-object v0
.end method

.method public getCharacterBoxesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/goggles/BoundingBoxProtos$BoundingBoxOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1191
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;
    .locals 1

    .prologue
    .line 1137
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1122
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1246
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedSerializedSize:I

    .line 1247
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 1263
    :goto_0
    return v2

    .line 1250
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 1251
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v1

    .line 1254
    :goto_1
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1255
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v0

    .line 1258
    :goto_2
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1259
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 1258
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 1262
    :cond_2
    iput v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->text_:Ljava/lang/Object;

    .line 1159
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1160
    check-cast v0, Ljava/lang/String;

    .line 1168
    :goto_0
    return-object v0

    .line 1162
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1164
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1165
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1166
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->text_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1168
    goto :goto_0
.end method

.method public hasBox()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1145
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    .line 1155
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1211
    iget-byte v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedIsInitialized:B

    .line 1212
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 1227
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 1212
    goto :goto_0

    .line 1214
    :cond_1
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->hasBox()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1215
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1216
    iput-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedIsInitialized:B

    move v0, v1

    .line 1217
    goto :goto_0

    :cond_2
    move v2, v1

    .line 1220
    :goto_1
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getCharacterBoxesCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 1221
    invoke-virtual {p0, v2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getCharacterBoxes(I)Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1222
    iput-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedIsInitialized:B

    move v0, v1

    .line 1223
    goto :goto_0

    .line 1220
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1226
    :cond_4
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public newBuilderForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1341
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1122
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilderForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;
    .locals 1

    .prologue
    .line 1345
    invoke-static {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1122
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->toBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1270
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1232
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getSerializedSize()I

    .line 1233
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1234
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->box_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 1236
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1237
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1239
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1240
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation$Word;->characterBoxes_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 1239
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1242
    :cond_2
    return-void
.end method
