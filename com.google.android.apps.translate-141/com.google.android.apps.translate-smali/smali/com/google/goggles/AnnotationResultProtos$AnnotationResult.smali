.class public final Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AnnotationResultProtos.java"

# interfaces
.implements Lcom/google/goggles/AnnotationResultProtos$AnnotationResultOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/AnnotationResultProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AnnotationResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;,
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;,
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformationOrBuilder;,
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;,
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformationOrBuilder;,
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;,
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformationOrBuilder;,
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;,
        Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformationOrBuilder;
    }
.end annotation


# static fields
.field public static final ANNOTATION_RESULT_FIELD_NUMBER:I = 0xef6c5f

.field public static final BOUNDING_BOX_FIELD_NUMBER:I = 0x1

.field public static final BOUNDING_BOX_REQUEST_ID_FIELD_NUMBER:I = 0xd

.field public static final CANONICAL_IMAGE_FIELD_NUMBER:I = 0x3

.field public static final CONTRIBUTION_INFO_FIELD_NUMBER:I = 0x17

.field public static final DIRECT_URL_FIELD_NUMBER:I = 0x14

.field public static final FACTS_FIELD_NUMBER:I = 0xf

.field public static final IS_AD_FIELD_NUMBER:I = 0x1a

.field public static final IS_SIMILAR_PRODUCT_FIELD_NUMBER:I = 0x1f

.field public static final IS_WITHDRAWN_FIELD_NUMBER:I = 0x19

.field public static final LANGUAGE_FIELD_NUMBER:I = 0x6

.field public static final LAT_LNG_FIELD_NUMBER:I = 0x5

.field public static final LOCATION_TEXT_FIELD_NUMBER:I = 0x18

.field public static final MORE_LIKE_THIS_ACTION_FIELD_NUMBER:I = 0x1d

.field public static final MUSIC_INFO_FIELD_NUMBER:I = 0x1b

.field public static final PERSON_INFO_FIELD_NUMBER:I = 0x10

.field public static final PLACE_INFO_FIELD_NUMBER:I = 0x11

.field public static final PRODUCT_INFO_FIELD_NUMBER:I = 0x15

.field public static final RESULT_ID_FIELD_NUMBER:I = 0xc

.field public static final RESULT_URL_FIELD_NUMBER:I = 0xb

.field public static final SHARE_ACTION_FIELD_NUMBER:I = 0x1e

.field public static final SUBTITLE_FIELD_NUMBER:I = 0x8

.field public static final SUBTYPE_FIELD_NUMBER:I = 0xa

.field public static final TEXT_INFO_FIELD_NUMBER:I = 0x16

.field public static final TITLE_FIELD_NUMBER:I = 0x7

.field public static final TTS_DESCRIPTION_FIELD_NUMBER:I = 0x1c

.field public static final TYPE_FIELD_NUMBER:I = 0x9

.field public static final URLS_FIELD_NUMBER:I = 0x4

.field public static final URL_GROUPS_FIELD_NUMBER:I = 0xe

.field public static final WEBSEARCH_URL_FIELD_NUMBER:I = 0x13

.field public static final annotationResult:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lcom/google/goggles/ResultProtos$Result;",
            "Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private boundingBoxRequestId_:Ljava/lang/Object;

.field private boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

.field private canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

.field private contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

.field private directUrl_:Ljava/lang/Object;

.field private facts_:Lcom/google/goggles/FactsProtos$Facts;

.field private isAd_:Z

.field private isSimilarProduct_:Z

.field private isWithdrawn_:Z

.field private language_:Ljava/lang/Object;

.field private latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

.field private locationText_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

.field private musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

.field private personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

.field private placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

.field private productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

.field private resultId_:Ljava/lang/Object;

.field private resultUrl_:Ljava/lang/Object;

.field private shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

.field private subtitle_:Ljava/lang/Object;

.field private subtype_:Ljava/lang/Object;

.field private textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

.field private title_:Ljava/lang/Object;

.field private ttsDescription_:Ljava/lang/Object;

.field private type_:Ljava/lang/Object;

.field private urlGroups_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/goggles/UrlGroupProtos$UrlGroup;",
            ">;"
        }
    .end annotation
.end field

.field private urls_:Lcom/google/protobuf/LazyStringList;

.field private websearchUrl_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 5524
    new-instance v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;-><init>(Z)V

    sput-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    .line 5525
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    invoke-direct {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->initFields()V

    .line 5533
    invoke-static {}, Lcom/google/goggles/ResultProtos$Result;->getDefaultInstance()Lcom/google/goggles/ResultProtos$Result;

    move-result-object v0

    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v1

    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0xef6c5f

    sget-object v5, Lcom/google/protobuf/WireFormat$FieldType;->MESSAGE:Lcom/google/protobuf/WireFormat$FieldType;

    invoke-static/range {v0 .. v5}, Lcom/google/protobuf/GeneratedMessageLite;->newSingularGeneratedExtension(Lcom/google/protobuf/MessageLite;Ljava/lang/Object;Lcom/google/protobuf/MessageLite;Lcom/google/protobuf/Internal$EnumLiteMap;ILcom/google/protobuf/WireFormat$FieldType;)Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->annotationResult:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    return-void
.end method

.method private constructor <init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 139
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 3433
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    .line 3564
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedSerializedSize:I

    .line 140
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;Lcom/google/goggles/AnnotationResultProtos$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;-><init>(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 141
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3433
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    .line 3564
    iput v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedSerializedSize:I

    .line 141
    return-void
.end method

.method static synthetic access$3902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->title_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4002(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtitle_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->type_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtype_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->ttsDescription_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->directUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/BoundingBoxProtos$BoundingBox;)Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object p1
.end method

.method static synthetic access$4702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;)Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    return-object p1
.end method

.method static synthetic access$4800(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$4802(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$4902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/LatLngProtos$LatLng;)Lcom/google/goggles/LatLngProtos$LatLng;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    return-object p1
.end method

.method static synthetic access$5002(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->locationText_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$5102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->language_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$5200(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$5302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->websearchUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$5402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/FactsProtos$Facts;)Lcom/google/goggles/FactsProtos$Facts;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    return-object p1
.end method

.method static synthetic access$5502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    return-object p1
.end method

.method static synthetic access$5602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    return-object p1
.end method

.method static synthetic access$5702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/ProductInformationProtos$ProductInformation;)Lcom/google/goggles/ProductInformationProtos$ProductInformation;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    return-object p1
.end method

.method static synthetic access$5802(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    return-object p1
.end method

.method static synthetic access$5902(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    return-object p1
.end method

.method static synthetic access$6002(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/MusicInformationProtos$MusicInformation;)Lcom/google/goggles/MusicInformationProtos$MusicInformation;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    return-object p1
.end method

.method static synthetic access$6102(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isWithdrawn_:Z

    return p1
.end method

.method static synthetic access$6202(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isAd_:Z

    return p1
.end method

.method static synthetic access$6302(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$6402(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBoxRequestId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$6502(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;)Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    return-object p1
.end method

.method static synthetic access$6602(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Lcom/google/goggles/ShareActionProtos$ShareAction;)Lcom/google/goggles/ShareActionProtos$ShareAction;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    return-object p1
.end method

.method static synthetic access$6702(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isSimilarProduct_:Z

    return p1
.end method

.method static synthetic access$6802(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 134
    iput p1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    return p1
.end method

.method private getBoundingBoxRequestIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3361
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 3362
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3363
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3365
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 3368
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    return-object v0
.end method

.method private getDirectUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3046
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->directUrl_:Ljava/lang/Object;

    .line 3047
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3048
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3050
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->directUrl_:Ljava/lang/Object;

    .line 3053
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLanguageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3154
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->language_:Ljava/lang/Object;

    .line 3155
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3156
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3158
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->language_:Ljava/lang/Object;

    .line 3161
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLocationTextBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3122
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->locationText_:Ljava/lang/Object;

    .line 3123
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3124
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3126
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->locationText_:Ljava/lang/Object;

    .line 3129
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getResultIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3329
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultId_:Ljava/lang/Object;

    .line 3330
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3331
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3333
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultId_:Ljava/lang/Object;

    .line 3336
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getResultUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3014
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultUrl_:Ljava/lang/Object;

    .line 3015
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3016
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3018
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultUrl_:Ljava/lang/Object;

    .line 3021
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSubtitleBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2886
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtitle_:Ljava/lang/Object;

    .line 2887
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2888
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2890
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtitle_:Ljava/lang/Object;

    .line 2893
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSubtypeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2950
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtype_:Ljava/lang/Object;

    .line 2951
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2952
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2954
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtype_:Ljava/lang/Object;

    .line 2957
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTitleBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2854
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->title_:Ljava/lang/Object;

    .line 2855
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2856
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2858
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->title_:Ljava/lang/Object;

    .line 2861
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTtsDescriptionBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2982
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->ttsDescription_:Ljava/lang/Object;

    .line 2983
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2984
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2986
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->ttsDescription_:Ljava/lang/Object;

    .line 2989
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTypeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2918
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->type_:Ljava/lang/Object;

    .line 2919
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2920
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2922
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->type_:Ljava/lang/Object;

    .line 2925
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getWebsearchUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3207
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->websearchUrl_:Ljava/lang/Object;

    .line 3208
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3209
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3211
    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->websearchUrl_:Ljava/lang/Object;

    .line 3214
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3403
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->title_:Ljava/lang/Object;

    .line 3404
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtitle_:Ljava/lang/Object;

    .line 3405
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->type_:Ljava/lang/Object;

    .line 3406
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtype_:Ljava/lang/Object;

    .line 3407
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->ttsDescription_:Ljava/lang/Object;

    .line 3408
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultUrl_:Ljava/lang/Object;

    .line 3409
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->directUrl_:Ljava/lang/Object;

    .line 3410
    invoke-static {}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->getDefaultInstance()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    .line 3411
    invoke-static {}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->getDefaultInstance()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    .line 3412
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    .line 3413
    invoke-static {}, Lcom/google/goggles/LatLngProtos$LatLng;->getDefaultInstance()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    .line 3414
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->locationText_:Ljava/lang/Object;

    .line 3415
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->language_:Ljava/lang/Object;

    .line 3416
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    .line 3417
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->websearchUrl_:Ljava/lang/Object;

    .line 3418
    invoke-static {}, Lcom/google/goggles/FactsProtos$Facts;->getDefaultInstance()Lcom/google/goggles/FactsProtos$Facts;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    .line 3419
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    .line 3420
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    .line 3421
    invoke-static {}, Lcom/google/goggles/ProductInformationProtos$ProductInformation;->getDefaultInstance()Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    .line 3422
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    .line 3423
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;->getDefaultInstance()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    .line 3424
    invoke-static {}, Lcom/google/goggles/MusicInformationProtos$MusicInformation;->getDefaultInstance()Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    .line 3425
    iput-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isWithdrawn_:Z

    .line 3426
    iput-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isAd_:Z

    .line 3427
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultId_:Ljava/lang/Object;

    .line 3428
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 3429
    invoke-static {}, Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;->getDefaultInstance()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    .line 3430
    invoke-static {}, Lcom/google/goggles/ShareActionProtos$ShareAction;->getDefaultInstance()Lcom/google/goggles/ShareActionProtos$ShareAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    .line 3431
    iput-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isSimilarProduct_:Z

    .line 3432
    return-void
.end method

.method public static newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 3769
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->create()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3700()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3772
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3738
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    .line 3739
    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3740
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    .line 3742
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3749
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    .line 3750
    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3751
    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    .line 3753
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3705
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3711
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3759
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3765
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3727
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3733
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3716
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3722
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    #calls: Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->buildParsed()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    invoke-static {v0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;->access$3600(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBoundingBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;
    .locals 1

    .prologue
    .line 3064
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    return-object v0
.end method

.method public getBoundingBoxRequestId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3347
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBoxRequestId_:Ljava/lang/Object;

    .line 3348
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3349
    check-cast v0, Ljava/lang/String;

    .line 3357
    :goto_0
    return-object v0

    .line 3351
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3353
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3354
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3355
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBoxRequestId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3357
    goto :goto_0
.end method

.method public getCanonicalImage()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;
    .locals 1

    .prologue
    .line 3074
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    return-object v0
.end method

.method public getContributionInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;
    .locals 1

    .prologue
    .line 3275
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->defaultInstance:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDefaultInstanceForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;

    move-result-object v0

    return-object v0
.end method

.method public getDirectUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3032
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->directUrl_:Ljava/lang/Object;

    .line 3033
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3034
    check-cast v0, Ljava/lang/String;

    .line 3042
    :goto_0
    return-object v0

    .line 3036
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3038
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3039
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3040
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->directUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3042
    goto :goto_0
.end method

.method public getFacts()Lcom/google/goggles/FactsProtos$Facts;
    .locals 1

    .prologue
    .line 3225
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    return-object v0
.end method

.method public getIsAd()Z
    .locals 1

    .prologue
    .line 3305
    iget-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isAd_:Z

    return v0
.end method

.method public getIsSimilarProduct()Z
    .locals 1

    .prologue
    .line 3399
    iget-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isSimilarProduct_:Z

    return v0
.end method

.method public getIsWithdrawn()Z
    .locals 1

    .prologue
    .line 3295
    iget-boolean v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isWithdrawn_:Z

    return v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3140
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->language_:Ljava/lang/Object;

    .line 3141
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3142
    check-cast v0, Ljava/lang/String;

    .line 3150
    :goto_0
    return-object v0

    .line 3144
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3146
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3147
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3148
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->language_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3150
    goto :goto_0
.end method

.method public getLatLng()Lcom/google/goggles/LatLngProtos$LatLng;
    .locals 1

    .prologue
    .line 3098
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    return-object v0
.end method

.method public getLocationText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3108
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->locationText_:Ljava/lang/Object;

    .line 3109
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3110
    check-cast v0, Ljava/lang/String;

    .line 3118
    :goto_0
    return-object v0

    .line 3112
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3114
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3115
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3116
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->locationText_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3118
    goto :goto_0
.end method

.method public getMoreLikeThisAction()Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;
    .locals 1

    .prologue
    .line 3379
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    return-object v0
.end method

.method public getMusicInfo()Lcom/google/goggles/MusicInformationProtos$MusicInformation;
    .locals 1

    .prologue
    .line 3285
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    return-object v0
.end method

.method public getPersonInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;
    .locals 1

    .prologue
    .line 3235
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    return-object v0
.end method

.method public getPlaceInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;
    .locals 1

    .prologue
    .line 3245
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    return-object v0
.end method

.method public getProductInfo()Lcom/google/goggles/ProductInformationProtos$ProductInformation;
    .locals 1

    .prologue
    .line 3255
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    return-object v0
.end method

.method public getResultId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3315
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultId_:Ljava/lang/Object;

    .line 3316
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3317
    check-cast v0, Ljava/lang/String;

    .line 3325
    :goto_0
    return-object v0

    .line 3319
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3321
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3322
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3323
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3325
    goto :goto_0
.end method

.method public getResultUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3000
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultUrl_:Ljava/lang/Object;

    .line 3001
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3002
    check-cast v0, Ljava/lang/String;

    .line 3010
    :goto_0
    return-object v0

    .line 3004
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3006
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3007
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3008
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->resultUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3010
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 9

    .prologue
    const v8, 0x8000

    const/16 v7, 0x10

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 3566
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedSerializedSize:I

    .line 3567
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    .line 3692
    :goto_0
    return v2

    .line 3570
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_1d

    .line 3571
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v1

    .line 3574
    :goto_1
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1

    .line 3575
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v1

    .line 3580
    :goto_2
    iget-object v4, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 3581
    iget-object v4, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v2}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v3, v4

    .line 3580
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 3584
    :cond_2
    add-int/2addr v0, v3

    .line 3585
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getUrlsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3587
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_3

    .line 3588
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3591
    :cond_3
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_4

    .line 3592
    const/4 v2, 0x6

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLanguageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3595
    :cond_4
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v5, :cond_5

    .line 3596
    const/4 v2, 0x7

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3599
    :cond_5
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 3600
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSubtitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3603
    :cond_6
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    .line 3604
    const/16 v2, 0x9

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3607
    :cond_7
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_8

    .line 3608
    const/16 v2, 0xa

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSubtypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3611
    :cond_8
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_9

    .line 3612
    const/16 v2, 0xb

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getResultUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3615
    :cond_9
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v3, 0x40

    and-int/2addr v2, v3

    const/high16 v3, 0x40

    if-ne v2, v3, :cond_a

    .line 3616
    const/16 v2, 0xc

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getResultIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3619
    :cond_a
    iget v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v3, 0x80

    and-int/2addr v2, v3

    const/high16 v3, 0x80

    if-ne v2, v3, :cond_b

    .line 3620
    const/16 v2, 0xd

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getBoundingBoxRequestIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    move v2, v0

    .line 3623
    :goto_3
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 3624
    const/16 v3, 0xe

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 3623
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_3

    .line 3627
    :cond_c
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 3628
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3631
    :cond_d
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_e

    .line 3632
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-static {v7, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3635
    :cond_e
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v8

    if-ne v0, v8, :cond_f

    .line 3636
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3639
    :cond_f
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_10

    .line 3640
    const/16 v0, 0x13

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getWebsearchUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3643
    :cond_10
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_11

    .line 3644
    const/16 v0, 0x14

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDirectUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3647
    :cond_11
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x1

    and-int/2addr v0, v1

    const/high16 v1, 0x1

    if-ne v0, v1, :cond_12

    .line 3648
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3651
    :cond_12
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x2

    and-int/2addr v0, v1

    const/high16 v1, 0x2

    if-ne v0, v1, :cond_13

    .line 3652
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3655
    :cond_13
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x4

    and-int/2addr v0, v1

    const/high16 v1, 0x4

    if-ne v0, v1, :cond_14

    .line 3656
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3659
    :cond_14
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_15

    .line 3660
    const/16 v0, 0x18

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLocationTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3663
    :cond_15
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x10

    and-int/2addr v0, v1

    const/high16 v1, 0x10

    if-ne v0, v1, :cond_16

    .line 3664
    const/16 v0, 0x19

    iget-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isWithdrawn_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 3667
    :cond_16
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x20

    and-int/2addr v0, v1

    const/high16 v1, 0x20

    if-ne v0, v1, :cond_17

    .line 3668
    const/16 v0, 0x1a

    iget-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isAd_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 3671
    :cond_17
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x8

    and-int/2addr v0, v1

    const/high16 v1, 0x8

    if-ne v0, v1, :cond_18

    .line 3672
    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3675
    :cond_18
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_19

    .line 3676
    const/16 v0, 0x1c

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTtsDescriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3679
    :cond_19
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x100

    and-int/2addr v0, v1

    const/high16 v1, 0x100

    if-ne v0, v1, :cond_1a

    .line 3680
    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3683
    :cond_1a
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x200

    and-int/2addr v0, v1

    const/high16 v1, 0x200

    if-ne v0, v1, :cond_1b

    .line 3684
    const/16 v0, 0x1e

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 3687
    :cond_1b
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x400

    and-int/2addr v0, v1

    const/high16 v1, 0x400

    if-ne v0, v1, :cond_1c

    .line 3688
    const/16 v0, 0x1f

    iget-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isSimilarProduct_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 3691
    :cond_1c
    iput v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_1d
    move v0, v1

    goto/16 :goto_1
.end method

.method public getShareAction()Lcom/google/goggles/ShareActionProtos$ShareAction;
    .locals 1

    .prologue
    .line 3389
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2872
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtitle_:Ljava/lang/Object;

    .line 2873
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2874
    check-cast v0, Ljava/lang/String;

    .line 2882
    :goto_0
    return-object v0

    .line 2876
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2878
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2879
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2880
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtitle_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2882
    goto :goto_0
.end method

.method public getSubtype()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2936
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtype_:Ljava/lang/Object;

    .line 2937
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2938
    check-cast v0, Ljava/lang/String;

    .line 2946
    :goto_0
    return-object v0

    .line 2940
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2942
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2943
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2944
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->subtype_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2946
    goto :goto_0
.end method

.method public getTextInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;
    .locals 1

    .prologue
    .line 3265
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2840
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->title_:Ljava/lang/Object;

    .line 2841
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2842
    check-cast v0, Ljava/lang/String;

    .line 2850
    :goto_0
    return-object v0

    .line 2844
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2846
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2847
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2848
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->title_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2850
    goto :goto_0
.end method

.method public getTtsDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2968
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->ttsDescription_:Ljava/lang/Object;

    .line 2969
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2970
    check-cast v0, Ljava/lang/String;

    .line 2978
    :goto_0
    return-object v0

    .line 2972
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2974
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2975
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2976
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->ttsDescription_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2978
    goto :goto_0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2904
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->type_:Ljava/lang/Object;

    .line 2905
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2906
    check-cast v0, Ljava/lang/String;

    .line 2914
    :goto_0
    return-object v0

    .line 2908
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2910
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2911
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2912
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->type_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2914
    goto :goto_0
.end method

.method public getUrlGroups(I)Lcom/google/goggles/UrlGroupProtos$UrlGroup;
    .locals 1
    .parameter

    .prologue
    .line 3179
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/UrlGroupProtos$UrlGroup;

    return-object v0
.end method

.method public getUrlGroupsCount()I
    .locals 1

    .prologue
    .line 3176
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getUrlGroupsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/goggles/UrlGroupProtos$UrlGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3169
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    return-object v0
.end method

.method public getUrlGroupsOrBuilder(I)Lcom/google/goggles/UrlGroupProtos$UrlGroupOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 3183
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/goggles/UrlGroupProtos$UrlGroupOrBuilder;

    return-object v0
.end method

.method public getUrlGroupsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/goggles/UrlGroupProtos$UrlGroupOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3173
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    return-object v0
.end method

.method public getUrls(I)Ljava/lang/String;
    .locals 1
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3088
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getUrlsCount()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3085
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getUrlsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3082
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getWebsearchUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3193
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->websearchUrl_:Ljava/lang/Object;

    .line 3194
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3195
    check-cast v0, Ljava/lang/String;

    .line 3203
    :goto_0
    return-object v0

    .line 3197
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3199
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3200
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3201
    iput-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->websearchUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3203
    goto :goto_0
.end method

.method public hasBoundingBox()Z
    .locals 2

    .prologue
    .line 3061
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBoundingBoxRequestId()Z
    .locals 2

    .prologue
    const/high16 v1, 0x80

    .line 3344
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCanonicalImage()Z
    .locals 2

    .prologue
    .line 3071
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContributionInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x4

    .line 3272
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDirectUrl()Z
    .locals 2

    .prologue
    .line 3029
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFacts()Z
    .locals 2

    .prologue
    .line 3222
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsAd()Z
    .locals 2

    .prologue
    const/high16 v1, 0x20

    .line 3302
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsSimilarProduct()Z
    .locals 2

    .prologue
    const/high16 v1, 0x400

    .line 3396
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsWithdrawn()Z
    .locals 2

    .prologue
    const/high16 v1, 0x10

    .line 3292
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLanguage()Z
    .locals 2

    .prologue
    .line 3137
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLatLng()Z
    .locals 2

    .prologue
    .line 3095
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLocationText()Z
    .locals 2

    .prologue
    .line 3105
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMoreLikeThisAction()Z
    .locals 2

    .prologue
    const/high16 v1, 0x100

    .line 3376
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMusicInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x8

    .line 3282
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPersonInfo()Z
    .locals 2

    .prologue
    .line 3232
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPlaceInfo()Z
    .locals 2

    .prologue
    const v1, 0x8000

    .line 3242
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x1

    .line 3252
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultId()Z
    .locals 2

    .prologue
    const/high16 v1, 0x40

    .line 3312
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultUrl()Z
    .locals 2

    .prologue
    .line 2997
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShareAction()Z
    .locals 2

    .prologue
    const/high16 v1, 0x200

    .line 3386
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubtitle()Z
    .locals 2

    .prologue
    .line 2869
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubtype()Z
    .locals 2

    .prologue
    .line 2933
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTextInfo()Z
    .locals 2

    .prologue
    const/high16 v1, 0x2

    .line 3262
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitle()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2837
    iget v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTtsDescription()Z
    .locals 2

    .prologue
    .line 2965
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 2901
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWebsearchUrl()Z
    .locals 2

    .prologue
    .line 3190
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3435
    iget-byte v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    .line 3436
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    :goto_0
    move v1, v0

    .line 3469
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 3436
    goto :goto_0

    .line 3438
    :cond_1
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasBoundingBox()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3439
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getBoundingBox()Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/goggles/BoundingBoxProtos$BoundingBox;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3440
    iput-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    goto :goto_1

    .line 3444
    :cond_2
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasCanonicalImage()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3445
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getCanonicalImage()Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_3

    .line 3446
    iput-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    goto :goto_1

    .line 3450
    :cond_3
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasLatLng()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3451
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLatLng()Lcom/google/goggles/LatLngProtos$LatLng;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/goggles/LatLngProtos$LatLng;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_4

    .line 3452
    iput-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    goto :goto_1

    :cond_4
    move v2, v1

    .line 3456
    :goto_2
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getUrlGroupsCount()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 3457
    invoke-virtual {p0, v2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getUrlGroups(I)Lcom/google/goggles/UrlGroupProtos$UrlGroup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/goggles/UrlGroupProtos$UrlGroup;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_5

    .line 3458
    iput-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    goto :goto_1

    .line 3456
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 3462
    :cond_6
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->hasTextInfo()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 3463
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTextInfo()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_7

    .line 3464
    iput-byte v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    goto :goto_1

    .line 3468
    :cond_7
    iput-byte v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->memoizedIsInitialized:B

    move v1, v0

    .line 3469
    goto :goto_1
.end method

.method public newBuilderForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 3770
    invoke-static {}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilderForType()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;
    .locals 1

    .prologue
    .line 3774
    invoke-static {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->newBuilder(Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;)Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->toBuilder()Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 3699
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3474
    invoke-virtual {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSerializedSize()I

    .line 3475
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_0

    .line 3476
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->boundingBox_:Lcom/google/goggles/BoundingBoxProtos$BoundingBox;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3478
    :cond_0
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_1

    .line 3479
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->canonicalImage_:Lcom/google/goggles/CanonicalImageProtos$CanonicalImage;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    move v0, v1

    .line 3481
    :goto_0
    iget-object v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 3482
    iget-object v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urls_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3481
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3484
    :cond_2
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_3

    .line 3485
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->latLng_:Lcom/google/goggles/LatLngProtos$LatLng;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3487
    :cond_3
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_4

    .line 3488
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLanguageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3490
    :cond_4
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 3491
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3493
    :cond_5
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_6

    .line 3494
    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSubtitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3496
    :cond_6
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_7

    .line 3497
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3499
    :cond_7
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_8

    .line 3500
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getSubtypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3502
    :cond_8
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_9

    .line 3503
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getResultUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3505
    :cond_9
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v2, 0x40

    and-int/2addr v0, v2

    const/high16 v2, 0x40

    if-ne v0, v2, :cond_a

    .line 3506
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getResultIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3508
    :cond_a
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v2, 0x80

    and-int/2addr v0, v2

    const/high16 v2, 0x80

    if-ne v0, v2, :cond_b

    .line 3509
    const/16 v0, 0xd

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getBoundingBoxRequestIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3511
    :cond_b
    :goto_1
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 3512
    const/16 v2, 0xe

    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->urlGroups_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3511
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3514
    :cond_c
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 3515
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->facts_:Lcom/google/goggles/FactsProtos$Facts;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3517
    :cond_d
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_e

    .line 3518
    iget-object v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->personInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PersonInformation;

    invoke-virtual {p1, v6, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3520
    :cond_e
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_f

    .line 3521
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->placeInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$PlaceInformation;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3523
    :cond_f
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_10

    .line 3524
    const/16 v0, 0x13

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getWebsearchUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3526
    :cond_10
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_11

    .line 3527
    const/16 v0, 0x14

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getDirectUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3529
    :cond_11
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x1

    and-int/2addr v0, v1

    const/high16 v1, 0x1

    if-ne v0, v1, :cond_12

    .line 3530
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->productInfo_:Lcom/google/goggles/ProductInformationProtos$ProductInformation;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3532
    :cond_12
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x2

    and-int/2addr v0, v1

    const/high16 v1, 0x2

    if-ne v0, v1, :cond_13

    .line 3533
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->textInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$TextInformation;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3535
    :cond_13
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x4

    and-int/2addr v0, v1

    const/high16 v1, 0x4

    if-ne v0, v1, :cond_14

    .line 3536
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->contributionInfo_:Lcom/google/goggles/AnnotationResultProtos$AnnotationResult$ContributionInformation;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3538
    :cond_14
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_15

    .line 3539
    const/16 v0, 0x18

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getLocationTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3541
    :cond_15
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x10

    and-int/2addr v0, v1

    const/high16 v1, 0x10

    if-ne v0, v1, :cond_16

    .line 3542
    const/16 v0, 0x19

    iget-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isWithdrawn_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 3544
    :cond_16
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x20

    and-int/2addr v0, v1

    const/high16 v1, 0x20

    if-ne v0, v1, :cond_17

    .line 3545
    const/16 v0, 0x1a

    iget-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isAd_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 3547
    :cond_17
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x8

    and-int/2addr v0, v1

    const/high16 v1, 0x8

    if-ne v0, v1, :cond_18

    .line 3548
    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->musicInfo_:Lcom/google/goggles/MusicInformationProtos$MusicInformation;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3550
    :cond_18
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_19

    .line 3551
    const/16 v0, 0x1c

    invoke-direct {p0}, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->getTtsDescriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3553
    :cond_19
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x100

    and-int/2addr v0, v1

    const/high16 v1, 0x100

    if-ne v0, v1, :cond_1a

    .line 3554
    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->moreLikeThisAction_:Lcom/google/goggles/MoreLikeThisActionProtos$MoreLikeThisAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3556
    :cond_1a
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x200

    and-int/2addr v0, v1

    const/high16 v1, 0x200

    if-ne v0, v1, :cond_1b

    .line 3557
    const/16 v0, 0x1e

    iget-object v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->shareAction_:Lcom/google/goggles/ShareActionProtos$ShareAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3559
    :cond_1b
    iget v0, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->bitField0_:I

    const/high16 v1, 0x400

    and-int/2addr v0, v1

    const/high16 v1, 0x400

    if-ne v0, v1, :cond_1c

    .line 3560
    const/16 v0, 0x1f

    iget-boolean v1, p0, Lcom/google/goggles/AnnotationResultProtos$AnnotationResult;->isSimilarProduct_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 3562
    :cond_1c
    return-void
.end method
