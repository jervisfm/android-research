.class public final Lcom/google/goggles/RestrictsProtos$Category$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "RestrictsProtos.java"

# interfaces
.implements Lcom/google/goggles/RestrictsProtos$CategoryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/goggles/RestrictsProtos$Category;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/goggles/RestrictsProtos$Category;",
        "Lcom/google/goggles/RestrictsProtos$Category$Builder;",
        ">;",
        "Lcom/google/goggles/RestrictsProtos$CategoryOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private displayName_:Ljava/lang/Object;

.field private domainSpecificId_:I

.field private domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;

.field private name_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1086
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 1228
    sget-object v0, Lcom/google/goggles/RestrictsProtos$Category$Domain;->PRETTY_NAME:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    .line 1252
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    .line 1288
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    .line 1087
    invoke-direct {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->maybeForceBuilderInitialization()V

    .line 1088
    return-void
.end method

.method static synthetic access$700(Lcom/google/goggles/RestrictsProtos$Category$Builder;)Lcom/google/goggles/RestrictsProtos$Category;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1081
    invoke-direct {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->buildParsed()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800()Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1

    .prologue
    .line 1081
    invoke-static {}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->create()Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/goggles/RestrictsProtos$Category;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1127
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->buildPartial()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    .line 1128
    invoke-virtual {v0}, Lcom/google/goggles/RestrictsProtos$Category;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1129
    invoke-static {v0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 1132
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1

    .prologue
    .line 1093
    new-instance v0, Lcom/google/goggles/RestrictsProtos$Category$Builder;

    invoke-direct {v0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 1091
    return-void
.end method


# virtual methods
.method public build()Lcom/google/goggles/RestrictsProtos$Category;
    .locals 2

    .prologue
    .line 1118
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->buildPartial()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    .line 1119
    invoke-virtual {v0}, Lcom/google/goggles/RestrictsProtos$Category;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1120
    invoke-static {v0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1122
    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->build()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/goggles/RestrictsProtos$Category;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1136
    new-instance v2, Lcom/google/goggles/RestrictsProtos$Category;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/goggles/RestrictsProtos$Category;-><init>(Lcom/google/goggles/RestrictsProtos$Category$Builder;Lcom/google/goggles/RestrictsProtos$1;)V

    .line 1137
    iget v3, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1138
    const/4 v1, 0x0

    .line 1139
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 1142
    :goto_0
    iget-object v1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    #setter for: Lcom/google/goggles/RestrictsProtos$Category;->domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;
    invoke-static {v2, v1}, Lcom/google/goggles/RestrictsProtos$Category;->access$1002(Lcom/google/goggles/RestrictsProtos$Category;Lcom/google/goggles/RestrictsProtos$Category$Domain;)Lcom/google/goggles/RestrictsProtos$Category$Domain;

    .line 1143
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1144
    or-int/lit8 v0, v0, 0x2

    .line 1146
    :cond_0
    iget-object v1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/RestrictsProtos$Category;->name_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/RestrictsProtos$Category;->access$1102(Lcom/google/goggles/RestrictsProtos$Category;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1148
    or-int/lit8 v0, v0, 0x4

    .line 1150
    :cond_1
    iget-object v1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    #setter for: Lcom/google/goggles/RestrictsProtos$Category;->displayName_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/goggles/RestrictsProtos$Category;->access$1202(Lcom/google/goggles/RestrictsProtos$Category;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 1152
    or-int/lit8 v0, v0, 0x8

    .line 1154
    :cond_2
    iget v1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domainSpecificId_:I

    #setter for: Lcom/google/goggles/RestrictsProtos$Category;->domainSpecificId_:I
    invoke-static {v2, v1}, Lcom/google/goggles/RestrictsProtos$Category;->access$1302(Lcom/google/goggles/RestrictsProtos$Category;I)I

    .line 1155
    #setter for: Lcom/google/goggles/RestrictsProtos$Category;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/goggles/RestrictsProtos$Category;->access$1402(Lcom/google/goggles/RestrictsProtos$Category;I)I

    .line 1156
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->buildPartial()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1

    .prologue
    .line 1097
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 1098
    sget-object v0, Lcom/google/goggles/RestrictsProtos$Category$Domain;->PRETTY_NAME:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    .line 1099
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    .line 1101
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    .line 1103
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1104
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domainSpecificId_:I

    .line 1105
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1106
    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->clear()Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->clear()Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearDisplayName()Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1

    .prologue
    .line 1312
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1313
    invoke-static {}, Lcom/google/goggles/RestrictsProtos$Category;->getDefaultInstance()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/RestrictsProtos$Category;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    .line 1315
    return-object p0
.end method

.method public clearDomain()Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1

    .prologue
    .line 1245
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1246
    sget-object v0, Lcom/google/goggles/RestrictsProtos$Category$Domain;->PRETTY_NAME:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    .line 1248
    return-object p0
.end method

.method public clearDomainSpecificId()Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1

    .prologue
    .line 1338
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1339
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domainSpecificId_:I

    .line 1341
    return-object p0
.end method

.method public clearName()Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1

    .prologue
    .line 1276
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1277
    invoke-static {}, Lcom/google/goggles/RestrictsProtos$Category;->getDefaultInstance()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/goggles/RestrictsProtos$Category;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    .line 1279
    return-object p0
.end method

.method public clone()Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 2

    .prologue
    .line 1110
    invoke-static {}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->create()Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->buildPartial()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->mergeFrom(Lcom/google/goggles/RestrictsProtos$Category;)Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->clone()Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->clone()Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->clone()Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->clone()Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/goggles/RestrictsProtos$Category;
    .locals 1

    .prologue
    .line 1114
    invoke-static {}, Lcom/google/goggles/RestrictsProtos$Category;->getDefaultInstance()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->getDefaultInstanceForType()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->getDefaultInstanceForType()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1293
    iget-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    .line 1294
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1295
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 1296
    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    .line 1299
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getDomain()Lcom/google/goggles/RestrictsProtos$Category$Domain;
    .locals 1

    .prologue
    .line 1233
    iget-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    return-object v0
.end method

.method public getDomainSpecificId()I
    .locals 1

    .prologue
    .line 1329
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domainSpecificId_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1257
    iget-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    .line 1258
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1259
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 1260
    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    .line 1263
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasDisplayName()Z
    .locals 2

    .prologue
    .line 1290
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDomain()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1230
    iget v1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDomainSpecificId()Z
    .locals 2

    .prologue
    .line 1326
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasName()Z
    .locals 2

    .prologue
    .line 1254
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 1177
    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/goggles/RestrictsProtos$Category;)Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1160
    invoke-static {}, Lcom/google/goggles/RestrictsProtos$Category;->getDefaultInstance()Lcom/google/goggles/RestrictsProtos$Category;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1173
    :cond_0
    :goto_0
    return-object p0

    .line 1161
    :cond_1
    invoke-virtual {p1}, Lcom/google/goggles/RestrictsProtos$Category;->hasDomain()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1162
    invoke-virtual {p1}, Lcom/google/goggles/RestrictsProtos$Category;->getDomain()Lcom/google/goggles/RestrictsProtos$Category$Domain;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->setDomain(Lcom/google/goggles/RestrictsProtos$Category$Domain;)Lcom/google/goggles/RestrictsProtos$Category$Builder;

    .line 1164
    :cond_2
    invoke-virtual {p1}, Lcom/google/goggles/RestrictsProtos$Category;->hasName()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1165
    invoke-virtual {p1}, Lcom/google/goggles/RestrictsProtos$Category;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->setName(Ljava/lang/String;)Lcom/google/goggles/RestrictsProtos$Category$Builder;

    .line 1167
    :cond_3
    invoke-virtual {p1}, Lcom/google/goggles/RestrictsProtos$Category;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1168
    invoke-virtual {p1}, Lcom/google/goggles/RestrictsProtos$Category;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->setDisplayName(Ljava/lang/String;)Lcom/google/goggles/RestrictsProtos$Category$Builder;

    .line 1170
    :cond_4
    invoke-virtual {p1}, Lcom/google/goggles/RestrictsProtos$Category;->hasDomainSpecificId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1171
    invoke-virtual {p1}, Lcom/google/goggles/RestrictsProtos$Category;->getDomainSpecificId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->setDomainSpecificId(I)Lcom/google/goggles/RestrictsProtos$Category$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1185
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 1186
    sparse-switch v0, :sswitch_data_0

    .line 1191
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1193
    :sswitch_0
    return-object p0

    .line 1198
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 1199
    invoke-static {v0}, Lcom/google/goggles/RestrictsProtos$Category$Domain;->valueOf(I)Lcom/google/goggles/RestrictsProtos$Category$Domain;

    move-result-object v0

    .line 1200
    if-eqz v0, :cond_0

    .line 1201
    iget v1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1202
    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    goto :goto_0

    .line 1207
    :sswitch_2
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1208
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    goto :goto_0

    .line 1212
    :sswitch_3
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1213
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domainSpecificId_:I

    goto :goto_0

    .line 1217
    :sswitch_4
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1218
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    goto :goto_0

    .line 1186
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1081
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1081
    check-cast p1, Lcom/google/goggles/RestrictsProtos$Category;

    invoke-virtual {p0, p1}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->mergeFrom(Lcom/google/goggles/RestrictsProtos$Category;)Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1081
    invoke-virtual {p0, p1, p2}, Lcom/google/goggles/RestrictsProtos$Category$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/goggles/RestrictsProtos$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1303
    if-nez p1, :cond_0

    .line 1304
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1306
    :cond_0
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1307
    iput-object p1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    .line 1309
    return-object p0
.end method

.method setDisplayName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 1318
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1319
    iput-object p1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->displayName_:Ljava/lang/Object;

    .line 1321
    return-void
.end method

.method public setDomain(Lcom/google/goggles/RestrictsProtos$Category$Domain;)Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1236
    if-nez p1, :cond_0

    .line 1237
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1239
    :cond_0
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1240
    iput-object p1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domain_:Lcom/google/goggles/RestrictsProtos$Category$Domain;

    .line 1242
    return-object p0
.end method

.method public setDomainSpecificId(I)Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1332
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1333
    iput p1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->domainSpecificId_:I

    .line 1335
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/goggles/RestrictsProtos$Category$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1267
    if-nez p1, :cond_0

    .line 1268
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1270
    :cond_0
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1271
    iput-object p1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    .line 1273
    return-object p0
.end method

.method setName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 1282
    iget v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->bitField0_:I

    .line 1283
    iput-object p1, p0, Lcom/google/goggles/RestrictsProtos$Category$Builder;->name_:Ljava/lang/Object;

    .line 1285
    return-void
.end method
