.class public final Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "BoundingBoxP.java"

# interfaces
.implements Lcom/google/protos/aksara/BoundingBoxP$BoundingBoxOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;",
        "Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;",
        ">;",
        "Lcom/google/protos/aksara/BoundingBoxP$BoundingBoxOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private height_:I

.field private left_:I

.field private top_:I

.field private width_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 341
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 445
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->left_:I

    .line 478
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->top_:I

    .line 511
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->width_:I

    .line 544
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->height_:I

    .line 342
    invoke-direct {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->maybeForceBuilderInitialization()V

    .line 343
    return-void
.end method

.method static synthetic access$100()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 336
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->create()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 348
    new-instance v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 346
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->build()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 2

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->buildPartial()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    .line 374
    .local v0, result:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    invoke-virtual {v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 375
    invoke-static {v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 377
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->buildPartial()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 5

    .prologue
    .line 381
    new-instance v1, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/BoundingBoxP$1;)V

    .line 382
    .local v1, result:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 383
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 384
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 385
    or-int/lit8 v2, v2, 0x1

    .line 387
    :cond_0
    iget v3, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->left_:I

    #setter for: Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->left_:I
    invoke-static {v1, v3}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->access$302(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I

    .line 388
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 389
    or-int/lit8 v2, v2, 0x2

    .line 391
    :cond_1
    iget v3, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->top_:I

    #setter for: Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->top_:I
    invoke-static {v1, v3}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->access$402(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I

    .line 392
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 393
    or-int/lit8 v2, v2, 0x4

    .line 395
    :cond_2
    iget v3, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->width_:I

    #setter for: Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->width_:I
    invoke-static {v1, v3}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->access$502(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I

    .line 396
    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 397
    or-int/lit8 v2, v2, 0x8

    .line 399
    :cond_3
    iget v3, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->height_:I

    #setter for: Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->height_:I
    invoke-static {v1, v3}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->access$602(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I

    .line 400
    #setter for: Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->access$702(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I

    .line 401
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->clear()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->clear()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 352
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 353
    iput v1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->left_:I

    .line 354
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 355
    iput v1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->top_:I

    .line 356
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 357
    iput v1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->width_:I

    .line 358
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 359
    iput v1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->height_:I

    .line 360
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 361
    return-object p0
.end method

.method public clearHeight()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 570
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 571
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->height_:I

    .line 573
    return-object p0
.end method

.method public clearLeft()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 471
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 472
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->left_:I

    .line 474
    return-object p0
.end method

.method public clearTop()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 504
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 505
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->top_:I

    .line 507
    return-object p0
.end method

.method public clearWidth()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 537
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 538
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->width_:I

    .line 540
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->clone()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->clone()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->clone()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 2

    .prologue
    .line 365
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->create()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->buildPartial()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->clone()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1

    .prologue
    .line 369
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getDefaultInstance()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 555
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->height_:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->left_:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 489
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->top_:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 522
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->width_:I

    return v0
.end method

.method public hasHeight()Z
    .locals 2

    .prologue
    .line 549
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeft()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 450
    iget v1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTop()Z
    .locals 2

    .prologue
    .line 483
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWidth()Z
    .locals 2

    .prologue
    .line 516
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 422
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 336
    check-cast p1, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 336
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    const/4 v2, 0x0

    .line 431
    .local v2, parsedMessage:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    if-eqz v2, :cond_0

    .line 437
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    .line 440
    :cond_0
    return-object p0

    .line 432
    :catch_0
    move-exception v1

    .line 433
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-object v2, v0

    .line 434
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 436
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 437
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1
    .parameter "other"

    .prologue
    .line 405
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getDefaultInstance()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 418
    :cond_0
    :goto_0
    return-object p0

    .line 406
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->hasLeft()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 407
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getLeft()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->setLeft(I)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    .line 409
    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->hasTop()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 410
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getTop()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->setTop(I)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    .line 412
    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->hasWidth()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 413
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->setWidth(I)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    .line 415
    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->setHeight(I)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    goto :goto_0
.end method

.method public setHeight(I)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 561
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 562
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->height_:I

    .line 564
    return-object p0
.end method

.method public setLeft(I)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 462
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 463
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->left_:I

    .line 465
    return-object p0
.end method

.method public setTop(I)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 495
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 496
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->top_:I

    .line 498
    return-object p0
.end method

.method public setWidth(I)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 528
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->bitField0_:I

    .line 529
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->width_:I

    .line 531
    return-object p0
.end method
