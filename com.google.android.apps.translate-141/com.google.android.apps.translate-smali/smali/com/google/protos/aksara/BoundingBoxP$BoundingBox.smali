.class public final Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "BoundingBoxP.java"

# interfaces
.implements Lcom/google/protos/aksara/BoundingBoxP$BoundingBoxOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/BoundingBoxP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BoundingBox"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    }
.end annotation


# static fields
.field public static final HEIGHT_FIELD_NUMBER:I = 0x4

.field public static final LEFT_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/Parser; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;",
            ">;"
        }
    .end annotation
.end field

.field public static final TOP_FIELD_NUMBER:I = 0x2

.field public static final WIDTH_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private height_:I

.field private left_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private top_:I

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 128
    new-instance v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$1;

    invoke-direct {v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$1;-><init>()V

    sput-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    .line 580
    new-instance v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;-><init>(Z)V

    sput-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->defaultInstance:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 581
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->defaultInstance:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    invoke-direct {v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->initFields()V

    .line 582
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 6
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 79
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 214
    iput-byte v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedIsInitialized:B

    .line 240
    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedSerializedSize:I

    .line 80
    invoke-direct {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->initFields()V

    .line 81
    const/4 v2, 0x0

    .line 83
    .local v2, mutable_bitField0_:I
    const/4 v0, 0x0

    .line 84
    .local v0, done:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 85
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v3

    .line 86
    .local v3, tag:I
    sparse-switch v3, :sswitch_data_0

    .line 91
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 93
    const/4 v0, 0x1

    goto :goto_0

    .line 88
    :sswitch_0
    const/4 v0, 0x1

    .line 89
    goto :goto_0

    .line 98
    :sswitch_1
    iget v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    .line 99
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->left_:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 119
    .end local v3           #tag:I
    :catch_0
    move-exception v1

    .line 120
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v4

    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->makeExtensionsImmutable()V

    throw v4

    .line 103
    .restart local v3       #tag:I
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    .line 104
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->top_:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 121
    .end local v3           #tag:I
    :catch_1
    move-exception v1

    .line 122
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 108
    .end local v1           #e:Ljava/io/IOException;
    .restart local v3       #tag:I
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    .line 109
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->width_:I

    goto :goto_0

    .line 113
    :sswitch_4
    iget v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    .line 114
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    iput v4, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->height_:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 125
    .end local v3           #tag:I
    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->makeExtensionsImmutable()V

    .line 127
    return-void

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/protos/aksara/BoundingBoxP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V
    .locals 1
    .parameter "builder"

    .prologue
    const/4 v0, -0x1

    .line 62
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 214
    iput-byte v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedIsInitialized:B

    .line 240
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedSerializedSize:I

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/BoundingBoxP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter "noInit"

    .prologue
    const/4 v0, -0x1

    .line 65
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 214
    iput-byte v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedIsInitialized:B

    .line 240
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedSerializedSize:I

    .line 65
    return-void
.end method

.method static synthetic access$302(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->left_:I

    return p1
.end method

.method static synthetic access$402(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->top_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->width_:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->height_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    iput p1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->defaultInstance:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 209
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->left_:I

    .line 210
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->top_:I

    .line 211
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->width_:I

    .line 212
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->height_:I

    .line 213
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 326
    #calls: Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->create()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->access$100()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1
    .parameter "prototype"

    .prologue
    .line 329
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->newBuilder()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 276
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 282
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 317
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 286
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 292
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getDefaultInstanceForType()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->defaultInstance:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->height_:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->left_:I

    return v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    sget-object v0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 242
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedSerializedSize:I

    .line 243
    .local v0, size:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 263
    .end local v0           #size:I
    .local v1, size:I
    :goto_0
    return v1

    .line 245
    .end local v1           #size:I
    .restart local v0       #size:I
    :cond_0
    const/4 v0, 0x0

    .line 246
    iget v2, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    .line 247
    iget v2, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->left_:I

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 250
    :cond_1
    iget v2, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    .line 251
    iget v2, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->top_:I

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 254
    :cond_2
    iget v2, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    .line 255
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->width_:I

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 258
    :cond_3
    iget v2, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    .line 259
    iget v2, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->height_:I

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 262
    :cond_4
    iput v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedSerializedSize:I

    move v1, v0

    .line 263
    .end local v0           #size:I
    .restart local v1       #size:I
    goto :goto_0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->top_:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->width_:I

    return v0
.end method

.method public hasHeight()Z
    .locals 2

    .prologue
    .line 199
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeft()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 151
    iget v1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTop()Z
    .locals 2

    .prologue
    .line 167
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWidth()Z
    .locals 2

    .prologue
    .line 183
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 216
    iget-byte v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedIsInitialized:B

    .line 217
    .local v0, isInitialized:B
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    .line 220
    :goto_0
    return v1

    .line 217
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 219
    :cond_1
    iput-byte v1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->newBuilderForType()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 327
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->newBuilder()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->toBuilder()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    .locals 1

    .prologue
    .line 331
    invoke-static {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->newBuilder(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 270
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 225
    invoke-virtual {p0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getSerializedSize()I

    .line 226
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 227
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->left_:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 229
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 230
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->top_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 232
    :cond_1
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 233
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->width_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 235
    :cond_2
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 236
    iget v0, p0, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->height_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 238
    :cond_3
    return-void
.end method
