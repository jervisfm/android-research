.class public final Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$CostTypeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$CostType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$CostType;",
        "Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$CostTypeOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private description_:Lcom/google/protobuf/LazyStringList;

.field private name_:Ljava/lang/Object;

.field private weight_:D


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 5307
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 5413
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5487
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->weight_:D

    .line 5520
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5308
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->maybeForceBuilderInitialization()V

    .line 5309
    return-void
.end method

.method static synthetic access$4800()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1

    .prologue
    .line 5302
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1

    .prologue
    .line 5314
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;-><init>()V

    return-object v0
.end method

.method private ensureDescriptionIsMutable()V
    .locals 2

    .prologue
    .line 5522
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 5523
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5524
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5526
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 5312
    return-void
.end method


# virtual methods
.method public addAllDescription(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;"
        }
    .end annotation

    .prologue
    .line 5584
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->ensureDescriptionIsMutable()V

    .line 5585
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 5587
    return-object p0
.end method

.method public addDescription(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 5571
    if-nez p1, :cond_0

    .line 5572
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5574
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->ensureDescriptionIsMutable()V

    .line 5575
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 5577
    return-object p0
.end method

.method public addDescriptionBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 5603
    if-nez p1, :cond_0

    .line 5604
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5606
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->ensureDescriptionIsMutable()V

    .line 5607
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 5609
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 2

    .prologue
    .line 5337
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    .line 5338
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5339
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 5341
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 5

    .prologue
    .line 5345
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 5346
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5347
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 5348
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 5349
    or-int/lit8 v2, v2, 0x1

    .line 5351
    :cond_0
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->access$5002(Lcom/google/protos/aksara/lattice/LatticeP$CostType;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5352
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 5353
    or-int/lit8 v2, v2, 0x2

    .line 5355
    :cond_1
    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->weight_:D

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$CostType;->weight_:D
    invoke-static {v1, v3, v4}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->access$5102(Lcom/google/protos/aksara/lattice/LatticeP$CostType;D)D

    .line 5356
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 5357
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5359
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5361
    :cond_2
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->access$5202(Lcom/google/protos/aksara/lattice/LatticeP$CostType;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 5362
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->access$5302(Lcom/google/protos/aksara/lattice/LatticeP$CostType;I)I

    .line 5363
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 2

    .prologue
    .line 5318
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 5319
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5320
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5321
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->weight_:D

    .line 5322
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5323
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5324
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5325
    return-object p0
.end method

.method public clearDescription()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1

    .prologue
    .line 5593
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5594
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5596
    return-object p0
.end method

.method public clearName()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1

    .prologue
    .line 5467
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5468
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5470
    return-object p0
.end method

.method public clearWeight()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 2

    .prologue
    .line 5513
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5514
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->weight_:D

    .line 5516
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 2

    .prologue
    .line 5329
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5302
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1

    .prologue
    .line 5333
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    return-object v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 5544
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 5551
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionCount()I
    .locals 1

    .prologue
    .line 5538
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getDescriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5532
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 5424
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5425
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    .line 5426
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 5428
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5431
    .end local v1           #s:Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_0
.end method

.method public getNameBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 5439
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5440
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 5441
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5444
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5447
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public getWeight()D
    .locals 2

    .prologue
    .line 5498
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->weight_:D

    return-wide v0
.end method

.method public hasName()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5418
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWeight()Z
    .locals 2

    .prologue
    .line 5492
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 5390
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5302
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 5302
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5302
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5397
    const/4 v2, 0x0

    .line 5399
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5404
    if-eqz v2, :cond_0

    .line 5405
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    .line 5408
    :cond_0
    return-object p0

    .line 5400
    :catch_0
    move-exception v1

    .line 5401
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-object v2, v0

    .line 5402
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5404
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 5405
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 2
    .parameter "other"

    .prologue
    .line 5367
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 5386
    :cond_0
    :goto_0
    return-object p0

    .line 5368
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5369
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5370
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->access$5000(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5373
    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->hasWeight()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5374
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getWeight()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->setWeight(D)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    .line 5376
    :cond_3
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->access$5200(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5377
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5378
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->access$5200(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5379
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    goto :goto_0

    .line 5381
    :cond_4
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->ensureDescriptionIsMutable()V

    .line 5382
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->access$5200(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setDescription(ILjava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 5558
    if-nez p2, :cond_0

    .line 5559
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5561
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->ensureDescriptionIsMutable()V

    .line 5562
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 5564
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 5455
    if-nez p1, :cond_0

    .line 5456
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5458
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5459
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5461
    return-object p0
.end method

.method public setNameBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 5477
    if-nez p1, :cond_0

    .line 5478
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5480
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5481
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->name_:Ljava/lang/Object;

    .line 5483
    return-object p0
.end method

.method public setWeight(D)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 5504
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->bitField0_:I

    .line 5505
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->weight_:D

    .line 5507
    return-object p0
.end method
