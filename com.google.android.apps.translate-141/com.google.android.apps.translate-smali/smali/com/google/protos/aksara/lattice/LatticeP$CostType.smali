.class public final Lcom/google/protos/aksara/lattice/LatticeP$CostType;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$CostTypeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CostType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    }
.end annotation


# static fields
.field public static final DESCRIPTION_FIELD_NUMBER:I = 0xf

.field public static final NAME_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/Parser; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostType;",
            ">;"
        }
    .end annotation
.end field

.field public static final WEIGHT_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$CostType;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private description_:Lcom/google/protobuf/LazyStringList;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/Object;

.field private weight_:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5072
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType$1;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$1;-><init>()V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    .line 5616
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;-><init>(Z)V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    .line 5617
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->initFields()V

    .line 5618
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 7
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x4

    .line 5022
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5182
    iput-byte v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedIsInitialized:B

    .line 5205
    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedSerializedSize:I

    .line 5023
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->initFields()V

    .line 5024
    const/4 v2, 0x0

    .line 5026
    .local v2, mutable_bitField0_:I
    const/4 v0, 0x0

    .line 5027
    .local v0, done:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 5028
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v3

    .line 5029
    .local v3, tag:I
    sparse-switch v3, :sswitch_data_0

    .line 5034
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 5036
    const/4 v0, 0x1

    goto :goto_0

    .line 5031
    :sswitch_0
    const/4 v0, 0x1

    .line 5032
    goto :goto_0

    .line 5041
    :sswitch_1
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    .line 5042
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 5060
    .end local v3           #tag:I
    :catch_0
    move-exception v1

    .line 5061
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5066
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v4

    and-int/lit8 v5, v2, 0x4

    if-ne v5, v6, :cond_1

    .line 5067
    new-instance v5, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v5, v6}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5069
    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->makeExtensionsImmutable()V

    throw v4

    .line 5046
    .restart local v3       #tag:I
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    .line 5047
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->weight_:D
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 5062
    .end local v3           #tag:I
    :catch_1
    move-exception v1

    .line 5063
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5051
    .end local v1           #e:Ljava/io/IOException;
    .restart local v3       #tag:I
    :sswitch_3
    and-int/lit8 v4, v2, 0x4

    if-eq v4, v6, :cond_2

    .line 5052
    :try_start_4
    new-instance v4, Lcom/google/protobuf/LazyStringArrayList;

    invoke-direct {v4}, Lcom/google/protobuf/LazyStringArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5053
    or-int/lit8 v2, v2, 0x4

    .line 5055
    :cond_2
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 5066
    .end local v3           #tag:I
    :cond_3
    and-int/lit8 v4, v2, 0x4

    if-ne v4, v6, :cond_4

    .line 5067
    new-instance v4, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v4, v5}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5069
    :cond_4
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->makeExtensionsImmutable()V

    .line 5071
    return-void

    .line 5029
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x7a -> :sswitch_3
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5000
    invoke-direct {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V
    .locals 1
    .parameter "builder"

    .prologue
    const/4 v0, -0x1

    .line 5005
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 5182
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedIsInitialized:B

    .line 5205
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedSerializedSize:I

    .line 5007
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5000
    invoke-direct {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter "noInit"

    .prologue
    const/4 v0, -0x1

    .line 5008
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5182
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedIsInitialized:B

    .line 5205
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedSerializedSize:I

    .line 5008
    return-void
.end method

.method static synthetic access$5000(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 5000
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/google/protos/aksara/lattice/LatticeP$CostType;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5000
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$5102(Lcom/google/protos/aksara/lattice/LatticeP$CostType;D)D
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5000
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->weight_:D

    return-wide p1
.end method

.method static synthetic access$5200(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter "x0"

    .prologue
    .line 5000
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$5202(Lcom/google/protos/aksara/lattice/LatticeP$CostType;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5000
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$5302(Lcom/google/protos/aksara/lattice/LatticeP$CostType;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5000
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1

    .prologue
    .line 5012
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    .line 5178
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;

    .line 5179
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->weight_:D

    .line 5180
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    .line 5181
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1

    .prologue
    .line 5292
    #calls: Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->access$4800()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1
    .parameter "prototype"

    .prologue
    .line 5295
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5272
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5278
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5242
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5248
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5283
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5289
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5262
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5268
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5252
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5258
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5000
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1

    .prologue
    .line 5016
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 5167
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 5174
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionCount()I
    .locals 1

    .prologue
    .line 5161
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getDescriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5155
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 5101
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;

    .line 5102
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 5103
    check-cast v1, Ljava/lang/String;

    .line 5111
    .end local v1           #ref:Ljava/lang/Object;
    :goto_0
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    move-object v0, v1

    .line 5105
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 5107
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 5108
    .local v2, s:Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isValidUtf8()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5109
    iput-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    .line 5111
    goto :goto_0
.end method

.method public getNameBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 5119
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;

    .line 5120
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 5121
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5124
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->name_:Ljava/lang/Object;

    .line 5127
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5084
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 5207
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedSerializedSize:I

    .line 5208
    .local v2, size:I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 5229
    .end local v2           #size:I
    .local v3, size:I
    :goto_0
    return v3

    .line 5210
    .end local v3           #size:I
    .restart local v2       #size:I
    :cond_0
    const/4 v2, 0x0

    .line 5211
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1

    .line 5212
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    .line 5215
    :cond_1
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-ne v4, v6, :cond_2

    .line 5216
    iget-wide v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->weight_:D

    invoke-static {v6, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v4

    add-int/2addr v2, v4

    .line 5220
    :cond_2
    const/4 v0, 0x0

    .line 5221
    .local v0, dataSize:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 5222
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5221
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5225
    :cond_3
    add-int/2addr v2, v0

    .line 5226
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getDescriptionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 5228
    iput v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedSerializedSize:I

    move v3, v2

    .line 5229
    .end local v2           #size:I
    .restart local v3       #size:I
    goto :goto_0
.end method

.method public getWeight()D
    .locals 2

    .prologue
    .line 5144
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->weight_:D

    return-wide v0
.end method

.method public hasName()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5095
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWeight()Z
    .locals 2

    .prologue
    .line 5138
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 5184
    iget-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedIsInitialized:B

    .line 5185
    .local v0, isInitialized:B
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    .line 5188
    :goto_0
    return v1

    .line 5185
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 5187
    :cond_1
    iput-byte v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5000
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1

    .prologue
    .line 5293
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5000
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;
    .locals 1

    .prologue
    .line 5297
    invoke-static {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 5236
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5193
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getSerializedSize()I

    .line 5194
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 5195
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->getNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5197
    :cond_0
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 5198
    iget-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->weight_:D

    invoke-virtual {p1, v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 5200
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 5201
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5203
    :cond_2
    return-void
.end method
