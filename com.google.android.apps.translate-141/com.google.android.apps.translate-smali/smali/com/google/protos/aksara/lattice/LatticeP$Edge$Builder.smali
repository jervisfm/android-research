.class public final Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$EdgeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$Edge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
        "Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$EdgeOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

.field private cost_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;"
        }
    .end annotation
.end field

.field private description_:Lcom/google/protobuf/LazyStringList;

.field private label_:Ljava/lang/Object;

.field private target_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 929
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    .line 1105
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1179
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    .line 1304
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getDefaultInstance()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 1365
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 930
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->maybeForceBuilderInitialization()V

    .line 931
    return-void
.end method

.method static synthetic access$700()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 925
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 936
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCostIsMutable()V
    .locals 2

    .prologue
    .line 1182
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1183
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    .line 1184
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1186
    :cond_0
    return-void
.end method

.method private ensureDescriptionIsMutable()V
    .locals 2

    .prologue
    .line 1367
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 1368
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1369
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1371
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 934
    return-void
.end method


# virtual methods
.method public addAllCost(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;"
        }
    .end annotation

    .prologue
    .line 1279
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/protos/aksara/lattice/LatticeP$Cost;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1280
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 1282
    return-object p0
.end method

.method public addAllDescription(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;"
        }
    .end annotation

    .prologue
    .line 1429
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureDescriptionIsMutable()V

    .line 1430
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 1432
    return-object p0
.end method

.method public addCost(ILcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 1269
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1270
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1272
    return-object p0
.end method

.method public addCost(ILcom/google/protos/aksara/lattice/LatticeP$Cost;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 1246
    if-nez p2, :cond_0

    .line 1247
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1249
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1250
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1252
    return-object p0
.end method

.method public addCost(Lcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 2
    .parameter "builderForValue"

    .prologue
    .line 1259
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1260
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1262
    return-object p0
.end method

.method public addCost(Lcom/google/protos/aksara/lattice/LatticeP$Cost;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1233
    if-nez p1, :cond_0

    .line 1234
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1236
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1237
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1239
    return-object p0
.end method

.method public addDescription(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1416
    if-nez p1, :cond_0

    .line 1417
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1419
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureDescriptionIsMutable()V

    .line 1420
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 1422
    return-object p0
.end method

.method public addDescriptionBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1448
    if-nez p1, :cond_0

    .line 1449
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1451
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureDescriptionIsMutable()V

    .line 1452
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 1454
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 2

    .prologue
    .line 963
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    .line 964
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 965
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 967
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 5

    .prologue
    .line 971
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 972
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 973
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 974
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 975
    or-int/lit8 v2, v2, 0x1

    .line 977
    :cond_0
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->target_:I

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->target_:I
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$902(Lcom/google/protos/aksara/lattice/LatticeP$Edge;I)I

    .line 978
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 979
    or-int/lit8 v2, v2, 0x2

    .line 981
    :cond_1
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1002(Lcom/google/protos/aksara/lattice/LatticeP$Edge;Ljava/lang/Object;)Ljava/lang/Object;

    .line 982
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 983
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    .line 984
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 986
    :cond_2
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1102(Lcom/google/protos/aksara/lattice/LatticeP$Edge;Ljava/util/List;)Ljava/util/List;

    .line 987
    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 988
    or-int/lit8 v2, v2, 0x4

    .line 990
    :cond_3
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1202(Lcom/google/protos/aksara/lattice/LatticeP$Edge;Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 991
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 992
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 994
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x11

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 996
    :cond_4
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1302(Lcom/google/protos/aksara/lattice/LatticeP$Edge;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 997
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1402(Lcom/google/protos/aksara/lattice/LatticeP$Edge;I)I

    .line 998
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 940
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->clear()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;

    .line 941
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->target_:I

    .line 942
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 943
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 944
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 945
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    .line 946
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 947
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getDefaultInstance()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 948
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 949
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 950
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 951
    return-object p0
.end method

.method public clearBounds()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 1358
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getDefaultInstance()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 1360
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1361
    return-object p0
.end method

.method public clearCost()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 1288
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    .line 1289
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1291
    return-object p0
.end method

.method public clearDescription()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 1438
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1439
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1441
    return-object p0
.end method

.method public clearLabel()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 1159
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1160
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getLabel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1162
    return-object p0
.end method

.method public clearTarget()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 1098
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1099
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->target_:I

    .line 1101
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 2

    .prologue
    .line 955
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBounds()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public getCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Cost;
    .locals 1
    .parameter "index"

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    return-object v0
.end method

.method public getCostCount()I
    .locals 1

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCostList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1

    .prologue
    .line 959
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    return-object v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 1389
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 1396
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionCount()I
    .locals 1

    .prologue
    .line 1383
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getDescriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1377
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1117
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    .line 1118
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1120
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1123
    .end local v1           #s:Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_0
.end method

.method public getLabelBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 1131
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1132
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1133
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1136
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1139
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public getTarget()I
    .locals 1

    .prologue
    .line 1083
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->target_:I

    return v0
.end method

.method public hasBounds()Z
    .locals 2

    .prologue
    .line 1309
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLabel()Z
    .locals 2

    .prologue
    .line 1110
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTarget()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1077
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1039
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->getCostCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1040
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->getCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/aksara/lattice/LatticeP$Cost;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1049
    :cond_0
    :goto_1
    return v1

    .line 1039
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1045
    :cond_2
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->extensionsAreInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1049
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public mergeBounds(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 2
    .parameter "value"

    .prologue
    .line 1343
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getDefaultInstance()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1345
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    invoke-static {v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->newBuilder(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->buildPartial()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 1351
    :goto_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1352
    return-object p0

    .line 1348
    :cond_0
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 925
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 925
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 925
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1056
    const/4 v2, 0x0

    .line 1058
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1063
    if-eqz v2, :cond_0

    .line 1064
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    .line 1067
    :cond_0
    return-object p0

    .line 1059
    :catch_0
    move-exception v1

    .line 1060
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-object v2, v0

    .line 1061
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1063
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 1064
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 2
    .parameter "other"

    .prologue
    .line 1002
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1035
    :goto_0
    return-object p0

    .line 1003
    :cond_0
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1004
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getTarget()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->setTarget(I)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    .line 1006
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1007
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1008
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1000(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1011
    :cond_2
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1100(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1012
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1013
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1100(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    .line 1014
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1021
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->hasBounds()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1022
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getBounds()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeBounds(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    .line 1024
    :cond_4
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1300(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1025
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1026
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1300(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1027
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1034
    :cond_5
    :goto_2
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto :goto_0

    .line 1016
    :cond_6
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1017
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1100(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1029
    :cond_7
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureDescriptionIsMutable()V

    .line 1030
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->access$1300(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public removeCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "index"

    .prologue
    .line 1297
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1298
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1300
    return-object p0
.end method

.method public setBounds(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "builderForValue"

    .prologue
    .line 1334
    invoke-virtual {p1}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->build()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 1336
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1337
    return-object p0
.end method

.method public setBounds(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1321
    if-nez p1, :cond_0

    .line 1322
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1324
    :cond_0
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 1326
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1327
    return-object p0
.end method

.method public setCost(ILcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 1224
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1225
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1227
    return-object p0
.end method

.method public setCost(ILcom/google/protos/aksara/lattice/LatticeP$Cost;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 1211
    if-nez p2, :cond_0

    .line 1212
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1214
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureCostIsMutable()V

    .line 1215
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1217
    return-object p0
.end method

.method public setDescription(ILjava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 1403
    if-nez p2, :cond_0

    .line 1404
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1406
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->ensureDescriptionIsMutable()V

    .line 1407
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1409
    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1147
    if-nez p1, :cond_0

    .line 1148
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1150
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1151
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1153
    return-object p0
.end method

.method public setLabelBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1169
    if-nez p1, :cond_0

    .line 1170
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1172
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1173
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->label_:Ljava/lang/Object;

    .line 1175
    return-object p0
.end method

.method public setTarget(I)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1089
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->bitField0_:I

    .line 1090
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->target_:I

    .line 1092
    return-object p0
.end method
