.class public final Lcom/google/protos/aksara/lattice/LatticeP$Edge;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$EdgeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Edge"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$EdgeOrBuilder;"
    }
.end annotation


# static fields
.field public static final BOUNDS_FIELD_NUMBER:I = 0x4

.field public static final COST_FIELD_NUMBER:I = 0x3

.field public static final DESCRIPTION_FIELD_NUMBER:I = 0xf

.field public static final LABEL_FIELD_NUMBER:I = 0x2

.field public static PARSER:Lcom/google/protobuf/Parser; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
            ">;"
        }
    .end annotation
.end field

.field public static final TARGET_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Edge;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

.field private cost_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;"
        }
    .end annotation
.end field

.field private description_:Lcom/google/protobuf/LazyStringList;

.field private label_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private target_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge$1;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$1;-><init>()V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    .line 1461
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;-><init>(Z)V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    .line 1462
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->initFields()V

    .line 1463
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 9
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/16 v8, 0x10

    const/4 v7, 0x4

    .line 538
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 776
    iput-byte v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedIsInitialized:B

    .line 819
    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedSerializedSize:I

    .line 539
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->initFields()V

    .line 540
    const/4 v2, 0x0

    .line 542
    .local v2, mutable_bitField0_:I
    const/4 v0, 0x0

    .line 543
    .local v0, done:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_7

    .line 544
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v4

    .line 545
    .local v4, tag:I
    sparse-switch v4, :sswitch_data_0

    .line 550
    invoke-virtual {p0, p1, p2, v4}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 552
    const/4 v0, 0x1

    goto :goto_0

    .line 547
    :sswitch_0
    const/4 v0, 0x1

    .line 548
    goto :goto_0

    .line 557
    :sswitch_1
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    .line 558
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->target_:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 597
    .end local v4           #tag:I
    :catch_0
    move-exception v1

    .line 598
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v5

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v5

    and-int/lit8 v6, v2, 0x4

    if-ne v6, v7, :cond_1

    .line 604
    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    .line 606
    :cond_1
    and-int/lit8 v6, v2, 0x10

    if-ne v6, v8, :cond_2

    .line 607
    new-instance v6, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v7, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v6, v7}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    .line 609
    :cond_2
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->makeExtensionsImmutable()V

    throw v5

    .line 562
    .restart local v4       #tag:I
    :sswitch_2
    :try_start_2
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    .line 563
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 599
    .end local v4           #tag:I
    :catch_1
    move-exception v1

    .line 600
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    new-instance v5, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v5

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 567
    .end local v1           #e:Ljava/io/IOException;
    .restart local v4       #tag:I
    :sswitch_3
    and-int/lit8 v5, v2, 0x4

    if-eq v5, v7, :cond_3

    .line 568
    :try_start_4
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    .line 569
    or-int/lit8 v2, v2, 0x4

    .line 571
    :cond_3
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    sget-object v6, Lcom/google/protos/aksara/lattice/LatticeP$Cost;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v6, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 575
    :sswitch_4
    const/4 v3, 0x0

    .line 576
    .local v3, subBuilder:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v5, v5, 0x4

    if-ne v5, v7, :cond_4

    .line 577
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    invoke-virtual {v5}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->toBuilder()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    move-result-object v3

    .line 579
    :cond_4
    sget-object v5, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v5, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v5

    check-cast v5, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 580
    if-eqz v3, :cond_5

    .line 581
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    invoke-virtual {v3, v5}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->mergeFrom(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;

    .line 582
    invoke-virtual {v3}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;->buildPartial()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 584
    :cond_5
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    goto/16 :goto_0

    .line 588
    .end local v3           #subBuilder:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
    :sswitch_5
    and-int/lit8 v5, v2, 0x10

    if-eq v5, v8, :cond_6

    .line 589
    new-instance v5, Lcom/google/protobuf/LazyStringArrayList;

    invoke-direct {v5}, Lcom/google/protobuf/LazyStringArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    .line 590
    or-int/lit8 v2, v2, 0x10

    .line 592
    :cond_6
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 603
    .end local v4           #tag:I
    :cond_7
    and-int/lit8 v5, v2, 0x4

    if-ne v5, v7, :cond_8

    .line 604
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    .line 606
    :cond_8
    and-int/lit8 v5, v2, 0x10

    if-ne v5, v8, :cond_9

    .line 607
    new-instance v5, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v5, v6}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    .line 609
    :cond_9
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->makeExtensionsImmutable()V

    .line 611
    return-void

    .line 545
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x7a -> :sswitch_5
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 516
    invoke-direct {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .local p1, builder:Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;,"Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder<Lcom/google/protos/aksara/lattice/LatticeP$Edge;*>;"
    const/4 v0, -0x1

    .line 521
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V

    .line 776
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedIsInitialized:B

    .line 819
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedSerializedSize:I

    .line 523
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 516
    invoke-direct {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter "noInit"

    .prologue
    const/4 v0, -0x1

    .line 524
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 776
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedIsInitialized:B

    .line 819
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedSerializedSize:I

    .line 524
    return-void
.end method

.method static synthetic access$1000(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/protos/aksara/lattice/LatticeP$Edge;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 516
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/protos/aksara/lattice/LatticeP$Edge;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 516
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/protos/aksara/lattice/LatticeP$Edge;Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 516
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter "x0"

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/protos/aksara/lattice/LatticeP$Edge;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 516
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/protos/aksara/lattice/LatticeP$Edge;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 516
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/protos/aksara/lattice/LatticeP$Edge;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 516
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->target_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1

    .prologue
    .line 528
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 770
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->target_:I

    .line 771
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;

    .line 772
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    .line 773
    invoke-static {}, Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;->getDefaultInstance()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    .line 774
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    .line 775
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 915
    #calls: Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->access$700()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1
    .parameter "prototype"

    .prologue
    .line 918
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 895
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 901
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 865
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 871
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 906
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 912
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 885
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 891
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 875
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 881
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method


# virtual methods
.method public getBounds()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    return-object v0
.end method

.method public getCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Cost;
    .locals 1
    .parameter "index"

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    return-object v0
.end method

.method public getCostCount()I
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCostList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;"
        }
    .end annotation

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    return-object v0
.end method

.method public getCostOrBuilder(I)Lcom/google/protos/aksara/lattice/LatticeP$CostOrBuilder;
    .locals 1
    .parameter "index"

    .prologue
    .line 720
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostOrBuilder;

    return-object v0
.end method

.method public getCostOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1

    .prologue
    .line 532
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 759
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionCount()I
    .locals 1

    .prologue
    .line 753
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getDescriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 747
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 4

    .prologue
    .line 657
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;

    .line 658
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 659
    check-cast v1, Ljava/lang/String;

    .line 667
    .end local v1           #ref:Ljava/lang/Object;
    :goto_0
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    move-object v0, v1

    .line 661
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 663
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 664
    .local v2, s:Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isValidUtf8()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 665
    iput-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    .line 667
    goto :goto_0
.end method

.method public getLabelBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 675
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;

    .line 676
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 677
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 680
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->label_:Ljava/lang/Object;

    .line 683
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 821
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedSerializedSize:I

    .line 822
    .local v2, size:I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 852
    .end local v2           #size:I
    .local v3, size:I
    :goto_0
    return v3

    .line 824
    .end local v3           #size:I
    .restart local v2       #size:I
    :cond_0
    const/4 v2, 0x0

    .line 825
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1

    .line 826
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->target_:I

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 829
    :cond_1
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-ne v4, v6, :cond_2

    .line 830
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getLabelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v6, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    .line 833
    :cond_2
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 834
    const/4 v5, 0x3

    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 833
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 837
    :cond_3
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v7, :cond_4

    .line 838
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    invoke-static {v7, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 842
    :cond_4
    const/4 v0, 0x0

    .line 843
    .local v0, dataSize:I
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 844
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    .line 843
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 847
    :cond_5
    add-int/2addr v2, v0

    .line 848
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getDescriptionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 850
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->extensionsSerializedSize()I

    move-result v4

    add-int/2addr v2, v4

    .line 851
    iput v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedSerializedSize:I

    move v3, v2

    .line 852
    .end local v2           #size:I
    .restart local v3       #size:I
    goto :goto_0
.end method

.method public getTarget()I
    .locals 1

    .prologue
    .line 641
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->target_:I

    return v0
.end method

.method public hasBounds()Z
    .locals 2

    .prologue
    .line 730
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLabel()Z
    .locals 2

    .prologue
    .line 651
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTarget()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 635
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 778
    iget-byte v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedIsInitialized:B

    .line 779
    .local v1, isInitialized:B
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    if-ne v1, v2, :cond_0

    .line 792
    :goto_0
    return v2

    :cond_0
    move v2, v3

    .line 779
    goto :goto_0

    .line 781
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getCostCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 782
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protos/aksara/lattice/LatticeP$Cost;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_2

    .line 783
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedIsInitialized:B

    move v2, v3

    .line 784
    goto :goto_0

    .line 781
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 787
    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->extensionsAreInitialized()Z

    move-result v4

    if-nez v4, :cond_4

    .line 788
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedIsInitialized:B

    move v2, v3

    .line 789
    goto :goto_0

    .line 791
    :cond_4
    iput-byte v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 916
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;
    .locals 1

    .prologue
    .line 920
    invoke-static {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 859
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 797
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getSerializedSize()I

    .line 799
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v0

    .line 801
    .local v0, extensionWriter:Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;,"Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage<Lcom/google/protos/aksara/lattice/LatticeP$Edge;>.ExtensionWriter;"
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_0

    .line 802
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->target_:I

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 804
    :cond_0
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 805
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->getLabelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 807
    :cond_1
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 808
    const/4 v3, 0x3

    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->cost_:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 807
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 810
    :cond_2
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    .line 811
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->bounds_:Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;

    invoke-virtual {p1, v5, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 813
    :cond_3
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 814
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 813
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 816
    :cond_4
    const/high16 v2, 0x2000

    invoke-virtual {v0, v2, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 817
    return-void
.end method
