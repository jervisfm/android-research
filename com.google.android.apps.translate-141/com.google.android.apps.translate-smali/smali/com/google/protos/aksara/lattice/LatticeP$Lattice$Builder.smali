.class public final Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$LatticeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$Lattice;",
        "Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$LatticeOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private costType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostType;",
            ">;"
        }
    .end annotation
.end field

.field private maxEdgeFactor_:D

.field private node_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
            ">;"
        }
    .end annotation
.end field

.field private path_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path;",
            ">;"
        }
    .end annotation
.end field

.field private stateType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$StateType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 6137
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    .line 6299
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    .line 6424
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    .line 6549
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    .line 6674
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    .line 6138
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->maybeForceBuilderInitialization()V

    .line 6139
    return-void
.end method

.method static synthetic access$5500()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6133
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6144
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCostTypeIsMutable()V
    .locals 2

    .prologue
    .line 6427
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 6428
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    .line 6429
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6431
    :cond_0
    return-void
.end method

.method private ensureNodeIsMutable()V
    .locals 2

    .prologue
    .line 6302
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 6303
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    .line 6304
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6306
    :cond_0
    return-void
.end method

.method private ensurePathIsMutable()V
    .locals 2

    .prologue
    .line 6552
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 6553
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    .line 6554
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6556
    :cond_0
    return-void
.end method

.method private ensureStateTypeIsMutable()V
    .locals 2

    .prologue
    .line 6677
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 6678
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    .line 6679
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6681
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 6142
    return-void
.end method


# virtual methods
.method public addAllCostType(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostType;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;"
        }
    .end annotation

    .prologue
    .line 6524
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/protos/aksara/lattice/LatticeP$CostType;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6525
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 6527
    return-object p0
.end method

.method public addAllNode(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;"
        }
    .end annotation

    .prologue
    .line 6399
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/protos/aksara/lattice/LatticeP$Node;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6400
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 6402
    return-object p0
.end method

.method public addAllPath(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;"
        }
    .end annotation

    .prologue
    .line 6649
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/protos/aksara/lattice/LatticeP$Path;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6650
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 6652
    return-object p0
.end method

.method public addAllStateType(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$StateType;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;"
        }
    .end annotation

    .prologue
    .line 6774
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/protos/aksara/lattice/LatticeP$StateType;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6775
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 6777
    return-object p0
.end method

.method public addCostType(ILcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 6514
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6515
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 6517
    return-object p0
.end method

.method public addCostType(ILcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 6491
    if-nez p2, :cond_0

    .line 6492
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6494
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6495
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 6497
    return-object p0
.end method

.method public addCostType(Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "builderForValue"

    .prologue
    .line 6504
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6505
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6507
    return-object p0
.end method

.method public addCostType(Lcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 6478
    if-nez p1, :cond_0

    .line 6479
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6481
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6482
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6484
    return-object p0
.end method

.method public addNode(ILcom/google/protos/aksara/lattice/LatticeP$Node$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 6389
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6390
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 6392
    return-object p0
.end method

.method public addNode(ILcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 6366
    if-nez p2, :cond_0

    .line 6367
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6369
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6370
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 6372
    return-object p0
.end method

.method public addNode(Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "builderForValue"

    .prologue
    .line 6379
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6380
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6382
    return-object p0
.end method

.method public addNode(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 6353
    if-nez p1, :cond_0

    .line 6354
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6356
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6357
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6359
    return-object p0
.end method

.method public addPath(ILcom/google/protos/aksara/lattice/LatticeP$Path$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 6639
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6640
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 6642
    return-object p0
.end method

.method public addPath(ILcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 6616
    if-nez p2, :cond_0

    .line 6617
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6619
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6620
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 6622
    return-object p0
.end method

.method public addPath(Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "builderForValue"

    .prologue
    .line 6629
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6630
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6632
    return-object p0
.end method

.method public addPath(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 6603
    if-nez p1, :cond_0

    .line 6604
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6606
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6607
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6609
    return-object p0
.end method

.method public addStateType(ILcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 6764
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6765
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 6767
    return-object p0
.end method

.method public addStateType(ILcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 6741
    if-nez p2, :cond_0

    .line 6742
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6744
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6745
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 6747
    return-object p0
.end method

.method public addStateType(Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "builderForValue"

    .prologue
    .line 6754
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6755
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6757
    return-object p0
.end method

.method public addStateType(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 6728
    if-nez p1, :cond_0

    .line 6729
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6731
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6732
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6734
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 2

    .prologue
    .line 6171
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v0

    .line 6172
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6173
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 6175
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 5

    .prologue
    .line 6179
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 6180
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6181
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 6182
    .local v2, to_bitField0_:I
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 6183
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    .line 6184
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6186
    :cond_0
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5702(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;Ljava/util/List;)Ljava/util/List;

    .line 6187
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 6188
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    .line 6189
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6191
    :cond_1
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5802(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;Ljava/util/List;)Ljava/util/List;

    .line 6192
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 6193
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    .line 6194
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6196
    :cond_2
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5902(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;Ljava/util/List;)Ljava/util/List;

    .line 6197
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 6198
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    .line 6199
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6201
    :cond_3
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$6002(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;Ljava/util/List;)Ljava/util/List;

    .line 6202
    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 6203
    or-int/lit8 v2, v2, 0x1

    .line 6205
    :cond_4
    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->maxEdgeFactor_:D

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->maxEdgeFactor_:D
    invoke-static {v1, v3, v4}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$6102(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;D)D

    .line 6206
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$6202(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;I)I

    .line 6207
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2

    .prologue
    .line 6148
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->clear()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;

    .line 6149
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    .line 6150
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6151
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    .line 6152
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6153
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    .line 6154
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6155
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    .line 6156
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6157
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->maxEdgeFactor_:D

    .line 6158
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6159
    return-object p0
.end method

.method public clearCostType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6533
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    .line 6534
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6536
    return-object p0
.end method

.method public clearMaxEdgeFactor()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2

    .prologue
    .line 6825
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6826
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->maxEdgeFactor_:D

    .line 6828
    return-object p0
.end method

.method public clearNode()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6408
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    .line 6409
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6411
    return-object p0
.end method

.method public clearPath()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6658
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    .line 6659
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6661
    return-object p0
.end method

.method public clearStateType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6783
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    .line 6784
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6786
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2

    .prologue
    .line 6163
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCostType(I)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "index"

    .prologue
    .line 6449
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public getCostTypeCount()I
    .locals 1

    .prologue
    .line 6443
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCostTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6437
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6133
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1

    .prologue
    .line 6167
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v0

    return-object v0
.end method

.method public getMaxEdgeFactor()D
    .locals 2

    .prologue
    .line 6810
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->maxEdgeFactor_:D

    return-wide v0
.end method

.method public getNode(I)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "index"

    .prologue
    .line 6324
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public getNodeCount()I
    .locals 1

    .prologue
    .line 6318
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNodeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6312
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPath(I)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "index"

    .prologue
    .line 6574
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public getPathCount()I
    .locals 1

    .prologue
    .line 6568
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPathList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6562
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getStateType(I)Lcom/google/protos/aksara/lattice/LatticeP$StateType;
    .locals 1
    .parameter "index"

    .prologue
    .line 6699
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    return-object v0
.end method

.method public getStateTypeCount()I
    .locals 1

    .prologue
    .line 6693
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getStateTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$StateType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6687
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasMaxEdgeFactor()Z
    .locals 2

    .prologue
    .line 6804
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6260
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->getNodeCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 6261
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->getNode(I)Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_1

    .line 6276
    :cond_0
    :goto_1
    return v1

    .line 6260
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6266
    :cond_2
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->getPathCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 6267
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->getPath(I)Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6266
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6272
    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->extensionsAreInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6276
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6133
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 6133
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6133
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6283
    const/4 v2, 0x0

    .line 6285
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6290
    if-eqz v2, :cond_0

    .line 6291
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    .line 6294
    :cond_0
    return-object p0

    .line 6286
    :catch_0
    move-exception v1

    .line 6287
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-object v2, v0

    .line 6288
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6290
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 6291
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "other"

    .prologue
    .line 6211
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 6256
    :goto_0
    return-object p0

    .line 6212
    :cond_0
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5700(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6213
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 6214
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5700(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    .line 6215
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6222
    :cond_1
    :goto_1
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5800(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 6223
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 6224
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5800(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    .line 6225
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6232
    :cond_2
    :goto_2
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5900(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 6233
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 6234
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5900(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    .line 6235
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6242
    :cond_3
    :goto_3
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$6000(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 6243
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 6244
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$6000(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    .line 6245
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6252
    :cond_4
    :goto_4
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->hasMaxEdgeFactor()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6253
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getMaxEdgeFactor()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->setMaxEdgeFactor(D)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    .line 6255
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto/16 :goto_0

    .line 6217
    :cond_6
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6218
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5700(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 6227
    :cond_7
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6228
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5800(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 6237
    :cond_8
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6238
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$5900(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 6247
    :cond_9
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6248
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->access$6000(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public removeCostType(I)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"

    .prologue
    .line 6542
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6543
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 6545
    return-object p0
.end method

.method public removeNode(I)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"

    .prologue
    .line 6417
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6418
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 6420
    return-object p0
.end method

.method public removePath(I)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"

    .prologue
    .line 6667
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6668
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 6670
    return-object p0
.end method

.method public removeStateType(I)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"

    .prologue
    .line 6792
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6793
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 6795
    return-object p0
.end method

.method public setCostType(ILcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 6469
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6470
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$CostType$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 6472
    return-object p0
.end method

.method public setCostType(ILcom/google/protos/aksara/lattice/LatticeP$CostType;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 6456
    if-nez p2, :cond_0

    .line 6457
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6459
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureCostTypeIsMutable()V

    .line 6460
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->costType_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 6462
    return-object p0
.end method

.method public setMaxEdgeFactor(D)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 6816
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->bitField0_:I

    .line 6817
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->maxEdgeFactor_:D

    .line 6819
    return-object p0
.end method

.method public setNode(ILcom/google/protos/aksara/lattice/LatticeP$Node$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 6344
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6345
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 6347
    return-object p0
.end method

.method public setNode(ILcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 6331
    if-nez p2, :cond_0

    .line 6332
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6334
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureNodeIsMutable()V

    .line 6335
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->node_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 6337
    return-object p0
.end method

.method public setPath(ILcom/google/protos/aksara/lattice/LatticeP$Path$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 6594
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6595
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 6597
    return-object p0
.end method

.method public setPath(ILcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 6581
    if-nez p2, :cond_0

    .line 6582
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6584
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensurePathIsMutable()V

    .line 6585
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->path_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 6587
    return-object p0
.end method

.method public setStateType(ILcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 6719
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6720
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 6722
    return-object p0
.end method

.method public setStateType(ILcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 6706
    if-nez p2, :cond_0

    .line 6707
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6709
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->ensureStateTypeIsMutable()V

    .line 6710
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->stateType_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 6712
    return-object p0
.end method
