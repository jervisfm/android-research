.class public final Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$LatticeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lattice"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$Lattice;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$LatticeOrBuilder;"
    }
.end annotation


# static fields
.field public static final COST_TYPE_FIELD_NUMBER:I = 0x2

.field public static final MAX_EDGE_FACTOR_FIELD_NUMBER:I = 0x5

.field public static final NODE_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/Parser; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Lattice;",
            ">;"
        }
    .end annotation
.end field

.field public static final PATH_FIELD_NUMBER:I = 0x3

.field public static final STATE_TYPE_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private costType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostType;",
            ">;"
        }
    .end annotation
.end field

.field private maxEdgeFactor_:D

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private node_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
            ">;"
        }
    .end annotation
.end field

.field private path_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path;",
            ">;"
        }
    .end annotation
.end field

.field private stateType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$StateType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5800
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$1;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$1;-><init>()V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    .line 6835
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;-><init>(Z)V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    .line 6836
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->initFields()V

    .line 6837
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 10
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 5722
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 5983
    iput-byte v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedIsInitialized:B

    .line 6032
    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedSerializedSize:I

    .line 5723
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->initFields()V

    .line 5724
    const/4 v2, 0x0

    .line 5726
    .local v2, mutable_bitField0_:I
    const/4 v0, 0x0

    .line 5727
    .local v0, done:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_9

    .line 5728
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v3

    .line 5729
    .local v3, tag:I
    sparse-switch v3, :sswitch_data_0

    .line 5734
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 5736
    const/4 v0, 0x1

    goto :goto_0

    .line 5731
    :sswitch_0
    const/4 v0, 0x1

    .line 5732
    goto :goto_0

    .line 5741
    :sswitch_1
    and-int/lit8 v4, v2, 0x1

    if-eq v4, v6, :cond_1

    .line 5742
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    .line 5743
    or-int/lit8 v2, v2, 0x1

    .line 5745
    :cond_1
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    sget-object v5, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v5, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 5779
    .end local v3           #tag:I
    :catch_0
    move-exception v1

    .line 5780
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5785
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v4

    and-int/lit8 v5, v2, 0x1

    if-ne v5, v6, :cond_2

    .line 5786
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    .line 5788
    :cond_2
    and-int/lit8 v5, v2, 0x2

    if-ne v5, v7, :cond_3

    .line 5789
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    .line 5791
    :cond_3
    and-int/lit8 v5, v2, 0x4

    if-ne v5, v8, :cond_4

    .line 5792
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    .line 5794
    :cond_4
    and-int/lit8 v5, v2, 0x8

    if-ne v5, v9, :cond_5

    .line 5795
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    .line 5797
    :cond_5
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->makeExtensionsImmutable()V

    throw v4

    .line 5749
    .restart local v3       #tag:I
    :sswitch_2
    and-int/lit8 v4, v2, 0x2

    if-eq v4, v7, :cond_6

    .line 5750
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    .line 5751
    or-int/lit8 v2, v2, 0x2

    .line 5753
    :cond_6
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    sget-object v5, Lcom/google/protos/aksara/lattice/LatticeP$CostType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v5, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 5781
    .end local v3           #tag:I
    :catch_1
    move-exception v1

    .line 5782
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5757
    .end local v1           #e:Ljava/io/IOException;
    .restart local v3       #tag:I
    :sswitch_3
    and-int/lit8 v4, v2, 0x4

    if-eq v4, v8, :cond_7

    .line 5758
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    .line 5759
    or-int/lit8 v2, v2, 0x4

    .line 5761
    :cond_7
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    sget-object v5, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v5, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 5765
    :sswitch_4
    and-int/lit8 v4, v2, 0x8

    if-eq v4, v9, :cond_8

    .line 5766
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    .line 5767
    or-int/lit8 v2, v2, 0x8

    .line 5769
    :cond_8
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    sget-object v5, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v5, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 5773
    :sswitch_5
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->bitField0_:I

    .line 5774
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->maxEdgeFactor_:D
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 5785
    .end local v3           #tag:I
    :cond_9
    and-int/lit8 v4, v2, 0x1

    if-ne v4, v6, :cond_a

    .line 5786
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    .line 5788
    :cond_a
    and-int/lit8 v4, v2, 0x2

    if-ne v4, v7, :cond_b

    .line 5789
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    .line 5791
    :cond_b
    and-int/lit8 v4, v2, 0x4

    if-ne v4, v8, :cond_c

    .line 5792
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    .line 5794
    :cond_c
    and-int/lit8 v4, v2, 0x8

    if-ne v4, v9, :cond_d

    .line 5795
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    .line 5797
    :cond_d
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->makeExtensionsImmutable()V

    .line 5799
    return-void

    .line 5729
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5700
    invoke-direct {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Lattice;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .local p1, builder:Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;,"Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder<Lcom/google/protos/aksara/lattice/LatticeP$Lattice;*>;"
    const/4 v0, -0x1

    .line 5705
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V

    .line 5983
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedIsInitialized:B

    .line 6032
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedSerializedSize:I

    .line 5707
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5700
    invoke-direct {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter "noInit"

    .prologue
    const/4 v0, -0x1

    .line 5708
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 5983
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedIsInitialized:B

    .line 6032
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedSerializedSize:I

    .line 5708
    return-void
.end method

.method static synthetic access$5700(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 5700
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5702(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5700
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$5800(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 5700
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5802(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5700
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$5900(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 5700
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5902(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5700
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$6000(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 5700
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$6002(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5700
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$6102(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;D)D
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5700
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->maxEdgeFactor_:D

    return-wide p1
.end method

.method static synthetic access$6202(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 5700
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1

    .prologue
    .line 5712
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    .line 5977
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    .line 5978
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    .line 5979
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    .line 5980
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    .line 5981
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->maxEdgeFactor_:D

    .line 5982
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6123
    #calls: Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->access$5500()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1
    .parameter "prototype"

    .prologue
    .line 6126
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6103
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6109
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6073
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6079
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6114
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6120
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6093
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6099
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6083
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6089
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method


# virtual methods
.method public getCostType(I)Lcom/google/protos/aksara/lattice/LatticeP$CostType;
    .locals 1
    .parameter "index"

    .prologue
    .line 5878
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostType;

    return-object v0
.end method

.method public getCostTypeCount()I
    .locals 1

    .prologue
    .line 5872
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCostTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5859
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    return-object v0
.end method

.method public getCostTypeOrBuilder(I)Lcom/google/protos/aksara/lattice/LatticeP$CostTypeOrBuilder;
    .locals 1
    .parameter "index"

    .prologue
    .line 5885
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostTypeOrBuilder;

    return-object v0
.end method

.method public getCostTypeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostTypeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5866
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5700
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice;
    .locals 1

    .prologue
    .line 5716
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Lattice;

    return-object v0
.end method

.method public getMaxEdgeFactor()D
    .locals 2

    .prologue
    .line 5973
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->maxEdgeFactor_:D

    return-wide v0
.end method

.method public getNode(I)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "index"

    .prologue
    .line 5842
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public getNodeCount()I
    .locals 1

    .prologue
    .line 5836
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNodeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5823
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    return-object v0
.end method

.method public getNodeOrBuilder(I)Lcom/google/protos/aksara/lattice/LatticeP$NodeOrBuilder;
    .locals 1
    .parameter "index"

    .prologue
    .line 5849
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NodeOrBuilder;

    return-object v0
.end method

.method public getNodeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$NodeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5830
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    return-object v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Lattice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5812
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getPath(I)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "index"

    .prologue
    .line 5914
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public getPathCount()I
    .locals 1

    .prologue
    .line 5908
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPathList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5895
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    return-object v0
.end method

.method public getPathOrBuilder(I)Lcom/google/protos/aksara/lattice/LatticeP$PathOrBuilder;
    .locals 1
    .parameter "index"

    .prologue
    .line 5921
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$PathOrBuilder;

    return-object v0
.end method

.method public getPathOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$PathOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5902
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 6034
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedSerializedSize:I

    .line 6035
    .local v1, size:I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 6060
    .end local v1           #size:I
    .local v2, size:I
    :goto_0
    return v2

    .line 6037
    .end local v2           #size:I
    .restart local v1       #size:I
    :cond_0
    const/4 v1, 0x0

    .line 6038
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 6039
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 6038
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6042
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 6043
    const/4 v4, 0x2

    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 6042
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6046
    :cond_2
    const/4 v0, 0x0

    :goto_3
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 6047
    const/4 v4, 0x3

    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 6046
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6050
    :cond_3
    const/4 v0, 0x0

    :goto_4
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 6051
    const/4 v4, 0x4

    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 6050
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 6054
    :cond_4
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v5, :cond_5

    .line 6055
    const/4 v3, 0x5

    iget-wide v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->maxEdgeFactor_:D

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v1, v3

    .line 6058
    :cond_5
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->extensionsSerializedSize()I

    move-result v3

    add-int/2addr v1, v3

    .line 6059
    iput v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedSerializedSize:I

    move v2, v1

    .line 6060
    .end local v1           #size:I
    .restart local v2       #size:I
    goto :goto_0
.end method

.method public getStateType(I)Lcom/google/protos/aksara/lattice/LatticeP$StateType;
    .locals 1
    .parameter "index"

    .prologue
    .line 5950
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    return-object v0
.end method

.method public getStateTypeCount()I
    .locals 1

    .prologue
    .line 5944
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getStateTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$StateType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5931
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    return-object v0
.end method

.method public getStateTypeOrBuilder(I)Lcom/google/protos/aksara/lattice/LatticeP$StateTypeOrBuilder;
    .locals 1
    .parameter "index"

    .prologue
    .line 5957
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$StateTypeOrBuilder;

    return-object v0
.end method

.method public getStateTypeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$StateTypeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5938
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    return-object v0
.end method

.method public hasMaxEdgeFactor()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5967
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5985
    iget-byte v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedIsInitialized:B

    .line 5986
    .local v1, isInitialized:B
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    if-ne v1, v2, :cond_0

    :goto_0
    move v3, v2

    .line 6005
    :goto_1
    return v3

    :cond_0
    move v2, v3

    .line 5986
    goto :goto_0

    .line 5988
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_2
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getNodeCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 5989
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getNode(I)Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_2

    .line 5990
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedIsInitialized:B

    goto :goto_1

    .line 5988
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5994
    :cond_3
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getPathCount()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 5995
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getPath(I)Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_4

    .line 5996
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedIsInitialized:B

    goto :goto_1

    .line 5994
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6000
    :cond_5
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->extensionsAreInitialized()Z

    move-result v4

    if-nez v4, :cond_6

    .line 6001
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedIsInitialized:B

    goto :goto_1

    .line 6004
    :cond_6
    iput-byte v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->memoizedIsInitialized:B

    move v3, v2

    .line 6005
    goto :goto_1
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5700
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6124
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5700
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;
    .locals 1

    .prologue
    .line 6128
    invoke-static {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$Lattice;)Lcom/google/protos/aksara/lattice/LatticeP$Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 6067
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 6010
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->getSerializedSize()I

    .line 6012
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v0

    .line 6014
    .local v0, extensionWriter:Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;,"Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage<Lcom/google/protos/aksara/lattice/LatticeP$Lattice;>.ExtensionWriter;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 6015
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->node_:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 6014
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6017
    :cond_0
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 6018
    const/4 v3, 0x2

    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->costType_:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 6017
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6020
    :cond_1
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 6021
    const/4 v3, 0x3

    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->path_:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 6020
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6023
    :cond_2
    const/4 v1, 0x0

    :goto_3
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 6024
    const/4 v3, 0x4

    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->stateType_:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 6023
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 6026
    :cond_3
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_4

    .line 6027
    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Lattice;->maxEdgeFactor_:D

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 6029
    :cond_4
    const/high16 v2, 0x2000

    invoke-virtual {v0, v2, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 6030
    return-void
.end method
