.class public final Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptionsOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;",
        "Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptionsOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private computeNodePosteriors_:Z

.field private includeEdgeBounds_:Z

.field private includeNodeState_:Z

.field private logCostMultiplier_:F

.field private maxAbsoluteCost_:D

.field private maxEdgeFactor_:D

.field private maxRelativeCost_:D

.field private nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const-wide/high16 v1, 0x7ff0

    .line 7993
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 8133
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxEdgeFactor_:D

    .line 8232
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->logCostMultiplier_:F

    .line 8265
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 8359
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxAbsoluteCost_:D

    .line 8392
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxRelativeCost_:D

    .line 7994
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maybeForceBuilderInitialization()V

    .line 7995
    return-void
.end method

.method static synthetic access$7300()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 7988
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 8000
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 7998
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 2

    .prologue
    .line 8033
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v0

    .line 8034
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8035
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 8037
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 5

    .prologue
    .line 8041
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 8042
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8043
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 8044
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 8045
    or-int/lit8 v2, v2, 0x1

    .line 8047
    :cond_0
    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxEdgeFactor_:D

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxEdgeFactor_:D
    invoke-static {v1, v3, v4}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$7502(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;D)D

    .line 8048
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 8049
    or-int/lit8 v2, v2, 0x2

    .line 8051
    :cond_1
    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeNodeState_:Z

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeNodeState_:Z
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$7602(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;Z)Z

    .line 8052
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 8053
    or-int/lit8 v2, v2, 0x4

    .line 8055
    :cond_2
    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->computeNodePosteriors_:Z

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->computeNodePosteriors_:Z
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$7702(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;Z)Z

    .line 8056
    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 8057
    or-int/lit8 v2, v2, 0x8

    .line 8059
    :cond_3
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->logCostMultiplier_:F

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->logCostMultiplier_:F
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$7802(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;F)F

    .line 8060
    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 8061
    or-int/lit8 v2, v2, 0x10

    .line 8063
    :cond_4
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$7902(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 8064
    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 8065
    or-int/lit8 v2, v2, 0x20

    .line 8067
    :cond_5
    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeEdgeBounds_:Z

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeEdgeBounds_:Z
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$8002(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;Z)Z

    .line 8068
    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 8069
    or-int/lit8 v2, v2, 0x40

    .line 8071
    :cond_6
    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxAbsoluteCost_:D

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxAbsoluteCost_:D
    invoke-static {v1, v3, v4}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$8102(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;D)D

    .line 8072
    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 8073
    or-int/lit16 v2, v2, 0x80

    .line 8075
    :cond_7
    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxRelativeCost_:D

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxRelativeCost_:D
    invoke-static {v1, v3, v4}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$8202(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;D)D

    .line 8076
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->access$8302(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;I)I

    .line 8077
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-wide/high16 v1, 0x7ff0

    .line 8004
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 8005
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxEdgeFactor_:D

    .line 8006
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8007
    iput-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeNodeState_:Z

    .line 8008
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8009
    iput-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->computeNodePosteriors_:Z

    .line 8010
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8011
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->logCostMultiplier_:F

    .line 8012
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8013
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 8014
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8015
    iput-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeEdgeBounds_:Z

    .line 8016
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8017
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxAbsoluteCost_:D

    .line 8018
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8019
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxRelativeCost_:D

    .line 8020
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8021
    return-object p0
.end method

.method public clearComputeNodePosteriors()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 8225
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8226
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->computeNodePosteriors_:Z

    .line 8228
    return-object p0
.end method

.method public clearIncludeEdgeBounds()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 8352
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8353
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeEdgeBounds_:Z

    .line 8355
    return-object p0
.end method

.method public clearIncludeNodeState()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 8192
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeNodeState_:Z

    .line 8195
    return-object p0
.end method

.method public clearLogCostMultiplier()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 8258
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8259
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->logCostMultiplier_:F

    .line 8261
    return-object p0
.end method

.method public clearMaxAbsoluteCost()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 2

    .prologue
    .line 8385
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8386
    const-wide/high16 v0, 0x7ff0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxAbsoluteCost_:D

    .line 8388
    return-object p0
.end method

.method public clearMaxEdgeFactor()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 2

    .prologue
    .line 8159
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8160
    const-wide/high16 v0, 0x7ff0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxEdgeFactor_:D

    .line 8162
    return-object p0
.end method

.method public clearMaxRelativeCost()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 2

    .prologue
    .line 8418
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8419
    const-wide/high16 v0, 0x7ff0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxRelativeCost_:D

    .line 8421
    return-object p0
.end method

.method public clearNbest()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 8319
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 8321
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8322
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 2

    .prologue
    .line 8025
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getComputeNodePosteriors()Z
    .locals 1

    .prologue
    .line 8210
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->computeNodePosteriors_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7988
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1

    .prologue
    .line 8029
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v0

    return-object v0
.end method

.method public getIncludeEdgeBounds()Z
    .locals 1

    .prologue
    .line 8337
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeEdgeBounds_:Z

    return v0
.end method

.method public getIncludeNodeState()Z
    .locals 1

    .prologue
    .line 8177
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeNodeState_:Z

    return v0
.end method

.method public getLogCostMultiplier()F
    .locals 1

    .prologue
    .line 8243
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->logCostMultiplier_:F

    return v0
.end method

.method public getMaxAbsoluteCost()D
    .locals 2

    .prologue
    .line 8370
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxAbsoluteCost_:D

    return-wide v0
.end method

.method public getMaxEdgeFactor()D
    .locals 2

    .prologue
    .line 8144
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxEdgeFactor_:D

    return-wide v0
.end method

.method public getMaxRelativeCost()D
    .locals 2

    .prologue
    .line 8403
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxRelativeCost_:D

    return-wide v0
.end method

.method public getNbest()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1

    .prologue
    .line 8276
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public hasComputeNodePosteriors()Z
    .locals 2

    .prologue
    .line 8204
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIncludeEdgeBounds()Z
    .locals 2

    .prologue
    .line 8331
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIncludeNodeState()Z
    .locals 2

    .prologue
    .line 8171
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLogCostMultiplier()Z
    .locals 2

    .prologue
    .line 8237
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxAbsoluteCost()Z
    .locals 2

    .prologue
    .line 8364
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxEdgeFactor()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 8138
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxRelativeCost()Z
    .locals 2

    .prologue
    .line 8397
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNbest()Z
    .locals 2

    .prologue
    .line 8270
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 8110
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7988
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 7988
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7988
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8117
    const/4 v2, 0x0

    .line 8119
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8124
    if-eqz v2, :cond_0

    .line 8125
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    .line 8128
    :cond_0
    return-object p0

    .line 8120
    :catch_0
    move-exception v1

    .line 8121
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-object v2, v0

    .line 8122
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8124
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 8125
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 2
    .parameter "other"

    .prologue
    .line 8081
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 8106
    :cond_0
    :goto_0
    return-object p0

    .line 8082
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->hasMaxEdgeFactor()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8083
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getMaxEdgeFactor()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->setMaxEdgeFactor(D)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    .line 8085
    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->hasIncludeNodeState()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8086
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getIncludeNodeState()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->setIncludeNodeState(Z)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    .line 8088
    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->hasComputeNodePosteriors()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 8089
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getComputeNodePosteriors()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->setComputeNodePosteriors(Z)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    .line 8091
    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->hasLogCostMultiplier()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 8092
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getLogCostMultiplier()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->setLogCostMultiplier(F)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    .line 8094
    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->hasNbest()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 8095
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getNbest()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->mergeNbest(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    .line 8097
    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->hasIncludeEdgeBounds()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 8098
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getIncludeEdgeBounds()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->setIncludeEdgeBounds(Z)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    .line 8100
    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->hasMaxAbsoluteCost()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 8101
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getMaxAbsoluteCost()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->setMaxAbsoluteCost(D)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    .line 8103
    :cond_8
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->hasMaxRelativeCost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8104
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getMaxRelativeCost()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->setMaxRelativeCost(D)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    goto :goto_0
.end method

.method public mergeNbest(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 2
    .parameter "value"

    .prologue
    .line 8304
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 8306
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 8312
    :goto_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8313
    return-object p0

    .line 8309
    :cond_0
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    goto :goto_0
.end method

.method public setComputeNodePosteriors(Z)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 8216
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8217
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->computeNodePosteriors_:Z

    .line 8219
    return-object p0
.end method

.method public setIncludeEdgeBounds(Z)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 8343
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8344
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeEdgeBounds_:Z

    .line 8346
    return-object p0
.end method

.method public setIncludeNodeState(Z)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 8183
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8184
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->includeNodeState_:Z

    .line 8186
    return-object p0
.end method

.method public setLogCostMultiplier(F)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 8249
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8250
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->logCostMultiplier_:F

    .line 8252
    return-object p0
.end method

.method public setMaxAbsoluteCost(D)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 8376
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8377
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxAbsoluteCost_:D

    .line 8379
    return-object p0
.end method

.method public setMaxEdgeFactor(D)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 8150
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8151
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxEdgeFactor_:D

    .line 8153
    return-object p0
.end method

.method public setMaxRelativeCost(D)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 8409
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8410
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->maxRelativeCost_:D

    .line 8412
    return-object p0
.end method

.method public setNbest(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "builderForValue"

    .prologue
    .line 8295
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 8297
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8298
    return-object p0
.end method

.method public setNbest(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 8282
    if-nez p1, :cond_0

    .line 8283
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8285
    :cond_0
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 8287
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->bitField0_:I

    .line 8288
    return-object p0
.end method
