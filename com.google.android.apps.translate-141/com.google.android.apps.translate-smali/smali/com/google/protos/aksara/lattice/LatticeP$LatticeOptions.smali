.class public final Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptionsOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatticeOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    }
.end annotation


# static fields
.field public static final COMPUTE_NODE_POSTERIORS_FIELD_NUMBER:I = 0x3

.field public static final INCLUDE_EDGE_BOUNDS_FIELD_NUMBER:I = 0x10

.field public static final INCLUDE_NODE_STATE_FIELD_NUMBER:I = 0x2

.field public static final LOG_COST_MULTIPLIER_FIELD_NUMBER:I = 0x4

.field public static final MAX_ABSOLUTE_COST_FIELD_NUMBER:I = 0x11

.field public static final MAX_EDGE_FACTOR_FIELD_NUMBER:I = 0x1

.field public static final MAX_RELATIVE_COST_FIELD_NUMBER:I = 0x12

.field public static final NBEST_FIELD_NUMBER:I = 0xf

.field public static PARSER:Lcom/google/protobuf/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;",
            ">;"
        }
    .end annotation
.end field

.field private static final defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private computeNodePosteriors_:Z

.field private includeEdgeBounds_:Z

.field private includeNodeState_:Z

.field private logCostMultiplier_:F

.field private maxAbsoluteCost_:D

.field private maxEdgeFactor_:D

.field private maxRelativeCost_:D

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7684
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$1;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$1;-><init>()V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    .line 8428
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;-><init>(Z)V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    .line 8429
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->initFields()V

    .line 8430
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 7
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 7607
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 7838
    iput-byte v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedIsInitialized:B

    .line 7876
    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedSerializedSize:I

    .line 7608
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->initFields()V

    .line 7609
    const/4 v2, 0x0

    .line 7611
    .local v2, mutable_bitField0_:I
    const/4 v0, 0x0

    .line 7612
    .local v0, done:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 7613
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v4

    .line 7614
    .local v4, tag:I
    sparse-switch v4, :sswitch_data_0

    .line 7619
    invoke-virtual {p0, p1, p2, v4}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 7621
    const/4 v0, 0x1

    goto :goto_0

    .line 7616
    :sswitch_0
    const/4 v0, 0x1

    .line 7617
    goto :goto_0

    .line 7626
    :sswitch_1
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    .line 7627
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readDouble()D

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxEdgeFactor_:D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 7675
    .end local v4           #tag:I
    :catch_0
    move-exception v1

    .line 7676
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v5

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7681
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v5

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->makeExtensionsImmutable()V

    throw v5

    .line 7631
    .restart local v4       #tag:I
    :sswitch_2
    :try_start_2
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    .line 7632
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeNodeState_:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 7677
    .end local v4           #tag:I
    :catch_1
    move-exception v1

    .line 7678
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    new-instance v5, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v5

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7636
    .end local v1           #e:Ljava/io/IOException;
    .restart local v4       #tag:I
    :sswitch_3
    :try_start_4
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    .line 7637
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->computeNodePosteriors_:Z

    goto :goto_0

    .line 7641
    :sswitch_4
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    .line 7642
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->logCostMultiplier_:F

    goto :goto_0

    .line 7646
    :sswitch_5
    const/4 v3, 0x0

    .line 7647
    .local v3, subBuilder:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v5, v5, 0x10

    const/16 v6, 0x10

    if-ne v5, v6, :cond_1

    .line 7648
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    invoke-virtual {v5}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v3

    .line 7650
    :cond_1
    sget-object v5, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v5, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v5

    check-cast v5, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 7651
    if-eqz v3, :cond_2

    .line 7652
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    invoke-virtual {v3, v5}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    .line 7653
    invoke-virtual {v3}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 7655
    :cond_2
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    goto/16 :goto_0

    .line 7659
    .end local v3           #subBuilder:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    :sswitch_6
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    .line 7660
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeEdgeBounds_:Z

    goto/16 :goto_0

    .line 7664
    :sswitch_7
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    .line 7665
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readDouble()D

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxAbsoluteCost_:D

    goto/16 :goto_0

    .line 7669
    :sswitch_8
    iget v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    .line 7670
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readDouble()D

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxRelativeCost_:D
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 7681
    .end local v4           #tag:I
    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->makeExtensionsImmutable()V

    .line 7683
    return-void

    .line 7614
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x7a -> :sswitch_5
        0x80 -> :sswitch_6
        0x89 -> :sswitch_7
        0x91 -> :sswitch_8
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7585
    invoke-direct {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V
    .locals 1
    .parameter "builder"

    .prologue
    const/4 v0, -0x1

    .line 7590
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 7838
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedIsInitialized:B

    .line 7876
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedSerializedSize:I

    .line 7592
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    invoke-direct {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter "noInit"

    .prologue
    const/4 v0, -0x1

    .line 7593
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 7838
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedIsInitialized:B

    .line 7876
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedSerializedSize:I

    .line 7593
    return-void
.end method

.method static synthetic access$7502(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;D)D
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxEdgeFactor_:D

    return-wide p1
.end method

.method static synthetic access$7602(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeNodeState_:Z

    return p1
.end method

.method static synthetic access$7702(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->computeNodePosteriors_:Z

    return p1
.end method

.method static synthetic access$7802(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;F)F
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->logCostMultiplier_:F

    return p1
.end method

.method static synthetic access$7902(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object p1
.end method

.method static synthetic access$8002(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeEdgeBounds_:Z

    return p1
.end method

.method static synthetic access$8102(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;D)D
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxAbsoluteCost_:D

    return-wide p1
.end method

.method static synthetic access$8202(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;D)D
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxRelativeCost_:D

    return-wide p1
.end method

.method static synthetic access$8302(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 7585
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1

    .prologue
    .line 7597
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method private initFields()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-wide/high16 v1, 0x7ff0

    .line 7829
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxEdgeFactor_:D

    .line 7830
    iput-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeNodeState_:Z

    .line 7831
    iput-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->computeNodePosteriors_:Z

    .line 7832
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->logCostMultiplier_:F

    .line 7833
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 7834
    iput-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeEdgeBounds_:Z

    .line 7835
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxAbsoluteCost_:D

    .line 7836
    iput-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxRelativeCost_:D

    .line 7837
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 7978
    #calls: Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->access$7300()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1
    .parameter "prototype"

    .prologue
    .line 7981
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7958
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7964
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7928
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7934
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7969
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7975
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7948
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7954
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7938
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7944
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method


# virtual methods
.method public getComputeNodePosteriors()Z
    .locals 1

    .prologue
    .line 7745
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->computeNodePosteriors_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7585
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;
    .locals 1

    .prologue
    .line 7601
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;

    return-object v0
.end method

.method public getIncludeEdgeBounds()Z
    .locals 1

    .prologue
    .line 7793
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeEdgeBounds_:Z

    return v0
.end method

.method public getIncludeNodeState()Z
    .locals 1

    .prologue
    .line 7729
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeNodeState_:Z

    return v0
.end method

.method public getLogCostMultiplier()F
    .locals 1

    .prologue
    .line 7761
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->logCostMultiplier_:F

    return v0
.end method

.method public getMaxAbsoluteCost()D
    .locals 2

    .prologue
    .line 7809
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxAbsoluteCost_:D

    return-wide v0
.end method

.method public getMaxEdgeFactor()D
    .locals 2

    .prologue
    .line 7713
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxEdgeFactor_:D

    return-wide v0
.end method

.method public getMaxRelativeCost()D
    .locals 2

    .prologue
    .line 7825
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxRelativeCost_:D

    return-wide v0
.end method

.method public getNbest()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1

    .prologue
    .line 7777
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7696
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 7878
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedSerializedSize:I

    .line 7879
    .local v0, size:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 7915
    .end local v0           #size:I
    .local v1, size:I
    :goto_0
    return v1

    .line 7881
    .end local v1           #size:I
    .restart local v0       #size:I
    :cond_0
    const/4 v0, 0x0

    .line 7882
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    .line 7883
    iget-wide v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxEdgeFactor_:D

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v2

    add-int/2addr v0, v2

    .line 7886
    :cond_1
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_2

    .line 7887
    iget-boolean v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeNodeState_:Z

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7890
    :cond_2
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_3

    .line 7891
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->computeNodePosteriors_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7894
    :cond_3
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    .line 7895
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->logCostMultiplier_:F

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 7898
    :cond_4
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v7, :cond_5

    .line 7899
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7902
    :cond_5
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    .line 7903
    iget-boolean v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeEdgeBounds_:Z

    invoke-static {v7, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7906
    :cond_6
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    .line 7907
    const/16 v2, 0x11

    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxAbsoluteCost_:D

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v2

    add-int/2addr v0, v2

    .line 7910
    :cond_7
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8

    .line 7911
    const/16 v2, 0x12

    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxRelativeCost_:D

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v2

    add-int/2addr v0, v2

    .line 7914
    :cond_8
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedSerializedSize:I

    move v1, v0

    .line 7915
    .end local v0           #size:I
    .restart local v1       #size:I
    goto :goto_0
.end method

.method public hasComputeNodePosteriors()Z
    .locals 2

    .prologue
    .line 7739
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIncludeEdgeBounds()Z
    .locals 2

    .prologue
    .line 7787
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIncludeNodeState()Z
    .locals 2

    .prologue
    .line 7723
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLogCostMultiplier()Z
    .locals 2

    .prologue
    .line 7755
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxAbsoluteCost()Z
    .locals 2

    .prologue
    .line 7803
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxEdgeFactor()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 7707
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxRelativeCost()Z
    .locals 2

    .prologue
    .line 7819
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNbest()Z
    .locals 2

    .prologue
    .line 7771
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 7840
    iget-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedIsInitialized:B

    .line 7841
    .local v0, isInitialized:B
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    .line 7844
    :goto_0
    return v1

    .line 7841
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 7843
    :cond_1
    iput-byte v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7585
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 7979
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7585
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;
    .locals 1

    .prologue
    .line 7983
    invoke-static {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;)Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 7922
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x10

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 7849
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->getSerializedSize()I

    .line 7850
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 7851
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxEdgeFactor_:D

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 7853
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 7854
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeNodeState_:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 7856
    :cond_1
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 7857
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->computeNodePosteriors_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 7859
    :cond_2
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 7860
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->logCostMultiplier_:F

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 7862
    :cond_3
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_4

    .line 7863
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->nbest_:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 7865
    :cond_4
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 7866
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->includeEdgeBounds_:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 7868
    :cond_5
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 7869
    const/16 v0, 0x11

    iget-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxAbsoluteCost_:D

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 7871
    :cond_6
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 7872
    const/16 v0, 0x12

    iget-wide v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$LatticeOptions;->maxRelativeCost_:D

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 7874
    :cond_7
    return-void
.end method
