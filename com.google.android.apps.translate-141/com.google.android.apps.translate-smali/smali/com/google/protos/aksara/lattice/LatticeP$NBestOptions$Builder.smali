.class public final Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$NBestOptionsOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;",
        "Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$NBestOptionsOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private includeCosts_:Z

.field private includeEdges_:Z

.field private includeLabels_:Z

.field private numPaths_:I

.field private uniqueText_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 7211
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 7357
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->uniqueText_:Z

    .line 7212
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->maybeForceBuilderInitialization()V

    .line 7213
    return-void
.end method

.method static synthetic access$6400()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7206
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7218
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 7216
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 2

    .prologue
    .line 7245
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    .line 7246
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7247
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 7249
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 5

    .prologue
    .line 7253
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 7254
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7255
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 7256
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 7257
    or-int/lit8 v2, v2, 0x1

    .line 7259
    :cond_0
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->numPaths_:I

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->numPaths_:I
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->access$6602(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;I)I

    .line 7260
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 7261
    or-int/lit8 v2, v2, 0x2

    .line 7263
    :cond_1
    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->uniqueText_:Z

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->uniqueText_:Z
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->access$6702(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;Z)Z

    .line 7264
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 7265
    or-int/lit8 v2, v2, 0x4

    .line 7267
    :cond_2
    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeEdges_:Z

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeEdges_:Z
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->access$6802(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;Z)Z

    .line 7268
    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 7269
    or-int/lit8 v2, v2, 0x8

    .line 7271
    :cond_3
    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeLabels_:Z

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeLabels_:Z
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->access$6902(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;Z)Z

    .line 7272
    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 7273
    or-int/lit8 v2, v2, 0x10

    .line 7275
    :cond_4
    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeCosts_:Z

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeCosts_:Z
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->access$7002(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;Z)Z

    .line 7276
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->access$7102(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;I)I

    .line 7277
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7222
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 7223
    iput v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->numPaths_:I

    .line 7224
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->uniqueText_:Z

    .line 7226
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7227
    iput-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeEdges_:Z

    .line 7228
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7229
    iput-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeLabels_:Z

    .line 7230
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7231
    iput-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeCosts_:Z

    .line 7232
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7233
    return-object p0
.end method

.method public clearIncludeCosts()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7482
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7483
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeCosts_:Z

    .line 7485
    return-object p0
.end method

.method public clearIncludeEdges()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7416
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7417
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeEdges_:Z

    .line 7419
    return-object p0
.end method

.method public clearIncludeLabels()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7449
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7450
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeLabels_:Z

    .line 7452
    return-object p0
.end method

.method public clearNumPaths()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7350
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7351
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->numPaths_:I

    .line 7353
    return-object p0
.end method

.method public clearUniqueText()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7383
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->uniqueText_:Z

    .line 7386
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 2

    .prologue
    .line 7237
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7206
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1

    .prologue
    .line 7241
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    return-object v0
.end method

.method public getIncludeCosts()Z
    .locals 1

    .prologue
    .line 7467
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeCosts_:Z

    return v0
.end method

.method public getIncludeEdges()Z
    .locals 1

    .prologue
    .line 7401
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeEdges_:Z

    return v0
.end method

.method public getIncludeLabels()Z
    .locals 1

    .prologue
    .line 7434
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeLabels_:Z

    return v0
.end method

.method public getNumPaths()I
    .locals 1

    .prologue
    .line 7335
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->numPaths_:I

    return v0
.end method

.method public getUniqueText()Z
    .locals 1

    .prologue
    .line 7368
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->uniqueText_:Z

    return v0
.end method

.method public hasIncludeCosts()Z
    .locals 2

    .prologue
    .line 7461
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIncludeEdges()Z
    .locals 2

    .prologue
    .line 7395
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIncludeLabels()Z
    .locals 2

    .prologue
    .line 7428
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNumPaths()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 7329
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUniqueText()Z
    .locals 2

    .prologue
    .line 7362
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 7301
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7206
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 7206
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7206
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7308
    const/4 v2, 0x0

    .line 7310
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7315
    if-eqz v2, :cond_0

    .line 7316
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    .line 7319
    :cond_0
    return-object p0

    .line 7311
    :catch_0
    move-exception v1

    .line 7312
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-object v2, v0

    .line 7313
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7315
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 7316
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1
    .parameter "other"

    .prologue
    .line 7281
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 7297
    :cond_0
    :goto_0
    return-object p0

    .line 7282
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->hasNumPaths()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7283
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getNumPaths()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->setNumPaths(I)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    .line 7285
    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->hasUniqueText()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7286
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getUniqueText()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->setUniqueText(Z)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    .line 7288
    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->hasIncludeEdges()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7289
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getIncludeEdges()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->setIncludeEdges(Z)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    .line 7291
    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->hasIncludeLabels()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 7292
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getIncludeLabels()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->setIncludeLabels(Z)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    .line 7294
    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->hasIncludeCosts()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7295
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getIncludeCosts()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->setIncludeCosts(Z)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    goto :goto_0
.end method

.method public setIncludeCosts(Z)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 7473
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7474
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeCosts_:Z

    .line 7476
    return-object p0
.end method

.method public setIncludeEdges(Z)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 7407
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7408
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeEdges_:Z

    .line 7410
    return-object p0
.end method

.method public setIncludeLabels(Z)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 7440
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7441
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->includeLabels_:Z

    .line 7443
    return-object p0
.end method

.method public setNumPaths(I)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 7341
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7342
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->numPaths_:I

    .line 7344
    return-object p0
.end method

.method public setUniqueText(Z)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 7374
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->bitField0_:I

    .line 7375
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->uniqueText_:Z

    .line 7377
    return-object p0
.end method
