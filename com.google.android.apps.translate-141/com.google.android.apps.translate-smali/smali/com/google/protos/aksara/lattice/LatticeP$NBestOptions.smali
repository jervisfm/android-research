.class public final Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$NBestOptionsOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NBestOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    }
.end annotation


# static fields
.field public static final INCLUDE_COSTS_FIELD_NUMBER:I = 0x5

.field public static final INCLUDE_EDGES_FIELD_NUMBER:I = 0x3

.field public static final INCLUDE_LABELS_FIELD_NUMBER:I = 0x4

.field public static final NUM_PATHS_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/Parser; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final UNIQUE_TEXT_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private includeCosts_:Z

.field private includeEdges_:Z

.field private includeLabels_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private numPaths_:I

.field private uniqueText_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6974
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$1;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$1;-><init>()V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    .line 7492
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;-><init>(Z)V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    .line 7493
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->initFields()V

    .line 7494
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 6
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 6920
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 7077
    iput-byte v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedIsInitialized:B

    .line 7106
    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedSerializedSize:I

    .line 6921
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->initFields()V

    .line 6922
    const/4 v2, 0x0

    .line 6924
    .local v2, mutable_bitField0_:I
    const/4 v0, 0x0

    .line 6925
    .local v0, done:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 6926
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v3

    .line 6927
    .local v3, tag:I
    sparse-switch v3, :sswitch_data_0

    .line 6932
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 6934
    const/4 v0, 0x1

    goto :goto_0

    .line 6929
    :sswitch_0
    const/4 v0, 0x1

    .line 6930
    goto :goto_0

    .line 6939
    :sswitch_1
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    .line 6940
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->numPaths_:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 6965
    .end local v3           #tag:I
    :catch_0
    move-exception v1

    .line 6966
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6971
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v4

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->makeExtensionsImmutable()V

    throw v4

    .line 6944
    .restart local v3       #tag:I
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    .line 6945
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->uniqueText_:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 6967
    .end local v3           #tag:I
    :catch_1
    move-exception v1

    .line 6968
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 6949
    .end local v1           #e:Ljava/io/IOException;
    .restart local v3       #tag:I
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    .line 6950
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeEdges_:Z

    goto :goto_0

    .line 6954
    :sswitch_4
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    .line 6955
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeLabels_:Z

    goto :goto_0

    .line 6959
    :sswitch_5
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    .line 6960
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeCosts_:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 6971
    .end local v3           #tag:I
    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->makeExtensionsImmutable()V

    .line 6973
    return-void

    .line 6927
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6898
    invoke-direct {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V
    .locals 1
    .parameter "builder"

    .prologue
    const/4 v0, -0x1

    .line 6903
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 7077
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedIsInitialized:B

    .line 7106
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedSerializedSize:I

    .line 6905
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 6898
    invoke-direct {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter "noInit"

    .prologue
    const/4 v0, -0x1

    .line 6906
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 7077
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedIsInitialized:B

    .line 7106
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedSerializedSize:I

    .line 6906
    return-void
.end method

.method static synthetic access$6602(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 6898
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->numPaths_:I

    return p1
.end method

.method static synthetic access$6702(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 6898
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->uniqueText_:Z

    return p1
.end method

.method static synthetic access$6802(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 6898
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeEdges_:Z

    return p1
.end method

.method static synthetic access$6902(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 6898
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeLabels_:Z

    return p1
.end method

.method static synthetic access$7002(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 6898
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeCosts_:Z

    return p1
.end method

.method static synthetic access$7102(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 6898
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1

    .prologue
    .line 6910
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7071
    iput v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->numPaths_:I

    .line 7072
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->uniqueText_:Z

    .line 7073
    iput-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeEdges_:Z

    .line 7074
    iput-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeLabels_:Z

    .line 7075
    iput-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeCosts_:Z

    .line 7076
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7196
    #calls: Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->access$6400()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1
    .parameter "prototype"

    .prologue
    .line 7199
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7176
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7182
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7146
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7152
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7187
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7193
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7166
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7172
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7156
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7162
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6898
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
    .locals 1

    .prologue
    .line 6914
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;

    return-object v0
.end method

.method public getIncludeCosts()Z
    .locals 1

    .prologue
    .line 7067
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeCosts_:Z

    return v0
.end method

.method public getIncludeEdges()Z
    .locals 1

    .prologue
    .line 7035
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeEdges_:Z

    return v0
.end method

.method public getIncludeLabels()Z
    .locals 1

    .prologue
    .line 7051
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeLabels_:Z

    return v0
.end method

.method public getNumPaths()I
    .locals 1

    .prologue
    .line 7003
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->numPaths_:I

    return v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6986
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 7108
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedSerializedSize:I

    .line 7109
    .local v0, size:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 7133
    .end local v0           #size:I
    .local v1, size:I
    :goto_0
    return v1

    .line 7111
    .end local v1           #size:I
    .restart local v0       #size:I
    :cond_0
    const/4 v0, 0x0

    .line 7112
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    .line 7113
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->numPaths_:I

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7116
    :cond_1
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    .line 7117
    iget-boolean v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->uniqueText_:Z

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7120
    :cond_2
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    .line 7121
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeEdges_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7124
    :cond_3
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    .line 7125
    iget-boolean v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeLabels_:Z

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7128
    :cond_4
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    .line 7129
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeCosts_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7132
    :cond_5
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedSerializedSize:I

    move v1, v0

    .line 7133
    .end local v0           #size:I
    .restart local v1       #size:I
    goto :goto_0
.end method

.method public getUniqueText()Z
    .locals 1

    .prologue
    .line 7019
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->uniqueText_:Z

    return v0
.end method

.method public hasIncludeCosts()Z
    .locals 2

    .prologue
    .line 7061
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIncludeEdges()Z
    .locals 2

    .prologue
    .line 7029
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIncludeLabels()Z
    .locals 2

    .prologue
    .line 7045
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNumPaths()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6997
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUniqueText()Z
    .locals 2

    .prologue
    .line 7013
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 7079
    iget-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedIsInitialized:B

    .line 7080
    .local v0, isInitialized:B
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    .line 7083
    :goto_0
    return v1

    .line 7080
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 7082
    :cond_1
    iput-byte v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6898
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7197
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6898
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
    .locals 1

    .prologue
    .line 7201
    invoke-static {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 7140
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 7088
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->getSerializedSize()I

    .line 7089
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 7090
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->numPaths_:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 7092
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 7093
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->uniqueText_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 7095
    :cond_1
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 7096
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeEdges_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 7098
    :cond_2
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 7099
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeLabels_:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 7101
    :cond_3
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 7102
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;->includeCosts_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 7104
    :cond_4
    return-void
.end method
