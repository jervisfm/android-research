.class public final Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$NodeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$Node;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
        "Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$NodeOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private description_:Lcom/google/protobuf/LazyStringList;

.field private edge_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
            ">;"
        }
    .end annotation
.end field

.field private isTransient_:Z

.field private pixel_:I

.field private posterior_:F

.field private state_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2529
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    .line 2693
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    .line 2884
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    .line 3042
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 2530
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->maybeForceBuilderInitialization()V

    .line 2531
    return-void
.end method

.method static synthetic access$2200()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2525
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2536
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;-><init>()V

    return-object v0
.end method

.method private ensureDescriptionIsMutable()V
    .locals 2

    .prologue
    .line 3044
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 3045
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 3046
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 3048
    :cond_0
    return-void
.end method

.method private ensureEdgeIsMutable()V
    .locals 2

    .prologue
    .line 2696
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 2697
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    .line 2698
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2700
    :cond_0
    return-void
.end method

.method private ensureStateIsMutable()V
    .locals 2

    .prologue
    .line 2887
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 2888
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    .line 2889
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2891
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 2534
    return-void
.end method


# virtual methods
.method public addAllDescription(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;"
        }
    .end annotation

    .prologue
    .line 3106
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureDescriptionIsMutable()V

    .line 3107
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 3109
    return-object p0
.end method

.method public addAllEdge(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;"
        }
    .end annotation

    .prologue
    .line 2793
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/protos/aksara/lattice/LatticeP$Edge;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2794
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 2796
    return-object p0
.end method

.method public addAllState(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$State;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;"
        }
    .end annotation

    .prologue
    .line 2984
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/protos/aksara/lattice/LatticeP$State;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 2985
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 2987
    return-object p0
.end method

.method public addDescription(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 3093
    if-nez p1, :cond_0

    .line 3094
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3096
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureDescriptionIsMutable()V

    .line 3097
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 3099
    return-object p0
.end method

.method public addDescriptionBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 3125
    if-nez p1, :cond_0

    .line 3126
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3128
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureDescriptionIsMutable()V

    .line 3129
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 3131
    return-object p0
.end method

.method public addEdge(ILcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 2783
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2784
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2786
    return-object p0
.end method

.method public addEdge(ILcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 2760
    if-nez p2, :cond_0

    .line 2761
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2763
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2764
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2766
    return-object p0
.end method

.method public addEdge(Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2
    .parameter "builderForValue"

    .prologue
    .line 2773
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2774
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2776
    return-object p0
.end method

.method public addEdge(Lcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 2747
    if-nez p1, :cond_0

    .line 2748
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2750
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2751
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2753
    return-object p0
.end method

.method public addState(ILcom/google/protos/aksara/lattice/LatticeP$State$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 2974
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 2975
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2977
    return-object p0
.end method

.method public addState(ILcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 2951
    if-nez p2, :cond_0

    .line 2952
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2954
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 2955
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2957
    return-object p0
.end method

.method public addState(Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2
    .parameter "builderForValue"

    .prologue
    .line 2964
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 2965
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2967
    return-object p0
.end method

.method public addState(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 2938
    if-nez p1, :cond_0

    .line 2939
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2941
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 2942
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2944
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 2

    .prologue
    .line 2565
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v0

    .line 2566
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$Node;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2567
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 2569
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 5

    .prologue
    .line 2573
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Node;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 2574
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$Node;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2575
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 2576
    .local v2, to_bitField0_:I
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 2577
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    .line 2578
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2580
    :cond_0
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2402(Lcom/google/protos/aksara/lattice/LatticeP$Node;Ljava/util/List;)Ljava/util/List;

    .line 2581
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 2582
    or-int/lit8 v2, v2, 0x1

    .line 2584
    :cond_1
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->pixel_:I

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->pixel_:I
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2502(Lcom/google/protos/aksara/lattice/LatticeP$Node;I)I

    .line 2585
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 2586
    or-int/lit8 v2, v2, 0x2

    .line 2588
    :cond_2
    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->isTransient_:Z

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->isTransient_:Z
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2602(Lcom/google/protos/aksara/lattice/LatticeP$Node;Z)Z

    .line 2589
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 2590
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    .line 2591
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2593
    :cond_3
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2702(Lcom/google/protos/aksara/lattice/LatticeP$Node;Ljava/util/List;)Ljava/util/List;

    .line 2594
    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 2595
    or-int/lit8 v2, v2, 0x4

    .line 2597
    :cond_4
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->posterior_:F

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->posterior_:F
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2802(Lcom/google/protos/aksara/lattice/LatticeP$Node;F)F

    .line 2598
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 2599
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 2601
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x21

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2603
    :cond_5
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2902(Lcom/google/protos/aksara/lattice/LatticeP$Node;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 2604
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$3002(Lcom/google/protos/aksara/lattice/LatticeP$Node;I)I

    .line 2605
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2540
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->clear()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;

    .line 2541
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    .line 2542
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2543
    iput v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->pixel_:I

    .line 2544
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2545
    iput-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->isTransient_:Z

    .line 2546
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2547
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    .line 2548
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2549
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->posterior_:F

    .line 2550
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2551
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 2552
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2553
    return-object p0
.end method

.method public clearDescription()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 3115
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 3116
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 3118
    return-object p0
.end method

.method public clearEdge()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2802
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    .line 2803
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2805
    return-object p0
.end method

.method public clearIsTransient()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2877
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2878
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->isTransient_:Z

    .line 2880
    return-object p0
.end method

.method public clearPixel()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2844
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2845
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->pixel_:I

    .line 2847
    return-object p0
.end method

.method public clearPosterior()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 3035
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 3036
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->posterior_:F

    .line 3038
    return-object p0
.end method

.method public clearState()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2993
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    .line 2994
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2996
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2

    .prologue
    .line 2557
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2525
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1

    .prologue
    .line 2561
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v0

    return-object v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 3066
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 3073
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionCount()I
    .locals 1

    .prologue
    .line 3060
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getDescriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3054
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getEdge(I)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "index"

    .prologue
    .line 2718
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public getEdgeCount()I
    .locals 1

    .prologue
    .line 2712
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEdgeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2706
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getIsTransient()Z
    .locals 1

    .prologue
    .line 2862
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->isTransient_:Z

    return v0
.end method

.method public getPixel()I
    .locals 1

    .prologue
    .line 2829
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->pixel_:I

    return v0
.end method

.method public getPosterior()F
    .locals 1

    .prologue
    .line 3020
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->posterior_:F

    return v0
.end method

.method public getState(I)Lcom/google/protos/aksara/lattice/LatticeP$State;
    .locals 1
    .parameter "index"

    .prologue
    .line 2909
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$State;

    return-object v0
.end method

.method public getStateCount()I
    .locals 1

    .prologue
    .line 2903
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getStateList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$State;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2897
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasIsTransient()Z
    .locals 2

    .prologue
    .line 2856
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPixel()Z
    .locals 2

    .prologue
    .line 2823
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPosterior()Z
    .locals 2

    .prologue
    .line 3014
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2654
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->getEdgeCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 2655
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->getEdge(I)Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2670
    :cond_0
    :goto_1
    return v1

    .line 2654
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2660
    :cond_2
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->getStateCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 2661
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->getState(I)Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/aksara/lattice/LatticeP$State;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2660
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2666
    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->extensionsAreInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2670
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2525
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 2525
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2525
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2677
    const/4 v2, 0x0

    .line 2679
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$Node;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2684
    if-eqz v2, :cond_0

    .line 2685
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    .line 2688
    :cond_0
    return-object p0

    .line 2680
    :catch_0
    move-exception v1

    .line 2681
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-object v2, v0

    .line 2682
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2684
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 2685
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2
    .parameter "other"

    .prologue
    .line 2609
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2650
    :goto_0
    return-object p0

    .line 2610
    :cond_0
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2400(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2611
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2612
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2400(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    .line 2613
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2620
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->hasPixel()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2621
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getPixel()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->setPixel(I)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    .line 2623
    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->hasIsTransient()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2624
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getIsTransient()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->setIsTransient(Z)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    .line 2626
    :cond_3
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2700(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2627
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2628
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2700(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    .line 2629
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2636
    :cond_4
    :goto_2
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->hasPosterior()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2637
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getPosterior()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->setPosterior(F)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    .line 2639
    :cond_5
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2900(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2640
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2641
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2900(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 2642
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2649
    :cond_6
    :goto_3
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto/16 :goto_0

    .line 2615
    :cond_7
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2616
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2400(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2631
    :cond_8
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 2632
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2700(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 2644
    :cond_9
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureDescriptionIsMutable()V

    .line 2645
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->access$2900(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public removeEdge(I)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "index"

    .prologue
    .line 2811
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2812
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2814
    return-object p0
.end method

.method public removeState(I)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "index"

    .prologue
    .line 3002
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 3003
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 3005
    return-object p0
.end method

.method public setDescription(ILjava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 3080
    if-nez p2, :cond_0

    .line 3081
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3083
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureDescriptionIsMutable()V

    .line 3084
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3086
    return-object p0
.end method

.method public setEdge(ILcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 2738
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2739
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Edge$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2741
    return-object p0
.end method

.method public setEdge(ILcom/google/protos/aksara/lattice/LatticeP$Edge;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 2725
    if-nez p2, :cond_0

    .line 2726
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2728
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureEdgeIsMutable()V

    .line 2729
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->edge_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2731
    return-object p0
.end method

.method public setIsTransient(Z)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 2868
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2869
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->isTransient_:Z

    .line 2871
    return-object p0
.end method

.method public setPixel(I)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 2835
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 2836
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->pixel_:I

    .line 2838
    return-object p0
.end method

.method public setPosterior(F)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 3026
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->bitField0_:I

    .line 3027
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->posterior_:F

    .line 3029
    return-object p0
.end method

.method public setState(ILcom/google/protos/aksara/lattice/LatticeP$State$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 2929
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 2930
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2932
    return-object p0
.end method

.method public setState(ILcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 2916
    if-nez p2, :cond_0

    .line 2917
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2919
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->ensureStateIsMutable()V

    .line 2920
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->state_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2922
    return-object p0
.end method
