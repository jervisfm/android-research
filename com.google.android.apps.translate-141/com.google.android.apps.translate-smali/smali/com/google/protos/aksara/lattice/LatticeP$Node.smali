.class public final Lcom/google/protos/aksara/lattice/LatticeP$Node;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$NodeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Node"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$NodeOrBuilder;"
    }
.end annotation


# static fields
.field public static final DESCRIPTION_FIELD_NUMBER:I = 0xf

.field public static final EDGE_FIELD_NUMBER:I = 0x1

.field public static final IS_TRANSIENT_FIELD_NUMBER:I = 0x10

.field public static PARSER:Lcom/google/protobuf/Parser; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
            ">;"
        }
    .end annotation
.end field

.field public static final PIXEL_FIELD_NUMBER:I = 0x2

.field public static final POSTERIOR_FIELD_NUMBER:I = 0x4

.field public static final STATE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Node;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private description_:Lcom/google/protobuf/LazyStringList;

.field private edge_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
            ">;"
        }
    .end annotation
.end field

.field private isTransient_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private pixel_:I

.field private posterior_:F

.field private state_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2189
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Node$1;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$1;-><init>()V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    .line 3138
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;-><init>(Z)V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Node;

    .line 3139
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Node;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->initFields()V

    .line 3140
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 9
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/16 v8, 0x20

    const/16 v7, 0x8

    const/4 v6, 0x1

    .line 2112
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 2363
    iput-byte v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedIsInitialized:B

    .line 2415
    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedSerializedSize:I

    .line 2113
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->initFields()V

    .line 2114
    const/4 v2, 0x0

    .line 2116
    .local v2, mutable_bitField0_:I
    const/4 v0, 0x0

    .line 2117
    .local v0, done:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_7

    .line 2118
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v3

    .line 2119
    .local v3, tag:I
    sparse-switch v3, :sswitch_data_0

    .line 2124
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2126
    const/4 v0, 0x1

    goto :goto_0

    .line 2121
    :sswitch_0
    const/4 v0, 0x1

    .line 2122
    goto :goto_0

    .line 2131
    :sswitch_1
    and-int/lit8 v4, v2, 0x1

    if-eq v4, v6, :cond_1

    .line 2132
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    .line 2133
    or-int/lit8 v2, v2, 0x1

    .line 2135
    :cond_1
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    sget-object v5, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v5, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2171
    .end local v3           #tag:I
    :catch_0
    move-exception v1

    .line 2172
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2177
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v4

    and-int/lit8 v5, v2, 0x1

    if-ne v5, v6, :cond_2

    .line 2178
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    .line 2180
    :cond_2
    and-int/lit8 v5, v2, 0x8

    if-ne v5, v7, :cond_3

    .line 2181
    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    .line 2183
    :cond_3
    and-int/lit8 v5, v2, 0x20

    if-ne v5, v8, :cond_4

    .line 2184
    new-instance v5, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v5, v6}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    .line 2186
    :cond_4
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->makeExtensionsImmutable()V

    throw v4

    .line 2139
    .restart local v3       #tag:I
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    .line 2140
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->pixel_:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 2173
    .end local v3           #tag:I
    :catch_1
    move-exception v1

    .line 2174
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v4

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2144
    .end local v1           #e:Ljava/io/IOException;
    .restart local v3       #tag:I
    :sswitch_3
    and-int/lit8 v4, v2, 0x8

    if-eq v4, v7, :cond_5

    .line 2145
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    .line 2146
    or-int/lit8 v2, v2, 0x8

    .line 2148
    :cond_5
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    sget-object v5, Lcom/google/protos/aksara/lattice/LatticeP$State;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v5, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2152
    :sswitch_4
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    .line 2153
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFloat()F

    move-result v4

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->posterior_:F

    goto/16 :goto_0

    .line 2157
    :sswitch_5
    and-int/lit8 v4, v2, 0x20

    if-eq v4, v8, :cond_6

    .line 2158
    new-instance v4, Lcom/google/protobuf/LazyStringArrayList;

    invoke-direct {v4}, Lcom/google/protobuf/LazyStringArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    .line 2159
    or-int/lit8 v2, v2, 0x20

    .line 2161
    :cond_6
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    .line 2165
    :sswitch_6
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    .line 2166
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isTransient_:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 2177
    .end local v3           #tag:I
    :cond_7
    and-int/lit8 v4, v2, 0x1

    if-ne v4, v6, :cond_8

    .line 2178
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    .line 2180
    :cond_8
    and-int/lit8 v4, v2, 0x8

    if-ne v4, v7, :cond_9

    .line 2181
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    .line 2183
    :cond_9
    and-int/lit8 v4, v2, 0x20

    if-ne v4, v8, :cond_a

    .line 2184
    new-instance v4, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v4, v5}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    .line 2186
    :cond_a
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->makeExtensionsImmutable()V

    .line 2188
    return-void

    .line 2119
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x7a -> :sswitch_5
        0x80 -> :sswitch_6
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2090
    invoke-direct {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Node;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .local p1, builder:Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;,"Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder<Lcom/google/protos/aksara/lattice/LatticeP$Node;*>;"
    const/4 v0, -0x1

    .line 2095
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V

    .line 2363
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedIsInitialized:B

    .line 2415
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedSerializedSize:I

    .line 2097
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 2090
    invoke-direct {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Node;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter "noInit"

    .prologue
    const/4 v0, -0x1

    .line 2098
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 2363
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedIsInitialized:B

    .line 2415
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedSerializedSize:I

    .line 2098
    return-void
.end method

.method static synthetic access$2400(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/protos/aksara/lattice/LatticeP$Node;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 2090
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2502(Lcom/google/protos/aksara/lattice/LatticeP$Node;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 2090
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->pixel_:I

    return p1
.end method

.method static synthetic access$2602(Lcom/google/protos/aksara/lattice/LatticeP$Node;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 2090
    iput-boolean p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isTransient_:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/google/protos/aksara/lattice/LatticeP$Node;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 2090
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2802(Lcom/google/protos/aksara/lattice/LatticeP$Node;F)F
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 2090
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->posterior_:F

    return p1
.end method

.method static synthetic access$2900(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter "x0"

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/google/protos/aksara/lattice/LatticeP$Node;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 2090
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$3002(Lcom/google/protos/aksara/lattice/LatticeP$Node;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 2090
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1

    .prologue
    .line 2102
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2356
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    .line 2357
    iput v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->pixel_:I

    .line 2358
    iput-boolean v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isTransient_:Z

    .line 2359
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    .line 2360
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->posterior_:F

    .line 2361
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    .line 2362
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2515
    #calls: Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->access$2200()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1
    .parameter "prototype"

    .prologue
    .line 2518
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2495
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2501
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2465
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2471
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2506
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2512
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2485
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2491
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2475
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2481
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2090
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Node;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Node;
    .locals 1

    .prologue
    .line 2106
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Node;

    return-object v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 2345
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 2352
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionCount()I
    .locals 1

    .prologue
    .line 2339
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getDescriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2333
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getEdge(I)Lcom/google/protos/aksara/lattice/LatticeP$Edge;
    .locals 1
    .parameter "index"

    .prologue
    .line 2231
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    return-object v0
.end method

.method public getEdgeCount()I
    .locals 1

    .prologue
    .line 2225
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEdgeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Edge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2212
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    return-object v0
.end method

.method public getEdgeOrBuilder(I)Lcom/google/protos/aksara/lattice/LatticeP$EdgeOrBuilder;
    .locals 1
    .parameter "index"

    .prologue
    .line 2238
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$EdgeOrBuilder;

    return-object v0
.end method

.method public getEdgeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$EdgeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2219
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    return-object v0
.end method

.method public getIsTransient()Z
    .locals 1

    .prologue
    .line 2270
    iget-boolean v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isTransient_:Z

    return v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Node;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2201
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getPixel()I
    .locals 1

    .prologue
    .line 2254
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->pixel_:I

    return v0
.end method

.method public getPosterior()F
    .locals 1

    .prologue
    .line 2322
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->posterior_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 2417
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedSerializedSize:I

    .line 2418
    .local v2, size:I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 2452
    .end local v2           #size:I
    .local v3, size:I
    :goto_0
    return v3

    .line 2420
    .end local v3           #size:I
    .restart local v2       #size:I
    :cond_0
    const/4 v2, 0x0

    .line 2421
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 2422
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2421
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2425
    :cond_1
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_2

    .line 2426
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->pixel_:I

    invoke-static {v6, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 2429
    :cond_2
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 2430
    const/4 v5, 0x3

    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2429
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2433
    :cond_3
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v7, :cond_4

    .line 2434
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->posterior_:F

    invoke-static {v7, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v2, v4

    .line 2438
    :cond_4
    const/4 v0, 0x0

    .line 2439
    .local v0, dataSize:I
    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 2440
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2439
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2443
    :cond_5
    add-int/2addr v2, v0

    .line 2444
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getDescriptionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 2446
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-ne v4, v6, :cond_6

    .line 2447
    const/16 v4, 0x10

    iget-boolean v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isTransient_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 2450
    :cond_6
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->extensionsSerializedSize()I

    move-result v4

    add-int/2addr v2, v4

    .line 2451
    iput v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedSerializedSize:I

    move v3, v2

    .line 2452
    .end local v2           #size:I
    .restart local v3       #size:I
    goto/16 :goto_0
.end method

.method public getState(I)Lcom/google/protos/aksara/lattice/LatticeP$State;
    .locals 1
    .parameter "index"

    .prologue
    .line 2299
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$State;

    return-object v0
.end method

.method public getStateCount()I
    .locals 1

    .prologue
    .line 2293
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getStateList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$State;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2280
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    return-object v0
.end method

.method public getStateOrBuilder(I)Lcom/google/protos/aksara/lattice/LatticeP$StateOrBuilder;
    .locals 1
    .parameter "index"

    .prologue
    .line 2306
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$StateOrBuilder;

    return-object v0
.end method

.method public getStateOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$StateOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2287
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    return-object v0
.end method

.method public hasIsTransient()Z
    .locals 2

    .prologue
    .line 2264
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPixel()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2248
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPosterior()Z
    .locals 2

    .prologue
    .line 2316
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2365
    iget-byte v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedIsInitialized:B

    .line 2366
    .local v1, isInitialized:B
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    if-ne v1, v2, :cond_0

    :goto_0
    move v3, v2

    .line 2385
    :goto_1
    return v3

    :cond_0
    move v2, v3

    .line 2366
    goto :goto_0

    .line 2368
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_2
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getEdgeCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 2369
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getEdge(I)Lcom/google/protos/aksara/lattice/LatticeP$Edge;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protos/aksara/lattice/LatticeP$Edge;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2370
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedIsInitialized:B

    goto :goto_1

    .line 2368
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2374
    :cond_3
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getStateCount()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 2375
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getState(I)Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protos/aksara/lattice/LatticeP$State;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_4

    .line 2376
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedIsInitialized:B

    goto :goto_1

    .line 2374
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2380
    :cond_5
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->extensionsAreInitialized()Z

    move-result v4

    if-nez v4, :cond_6

    .line 2381
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedIsInitialized:B

    goto :goto_1

    .line 2384
    :cond_6
    iput-byte v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->memoizedIsInitialized:B

    move v3, v2

    .line 2385
    goto :goto_1
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2090
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2516
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2090
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;
    .locals 1

    .prologue
    .line 2520
    invoke-static {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$Node;)Lcom/google/protos/aksara/lattice/LatticeP$Node$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 2459
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2390
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->getSerializedSize()I

    .line 2392
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Node;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v0

    .line 2394
    .local v0, extensionWriter:Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;,"Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage<Lcom/google/protos/aksara/lattice/LatticeP$Node;>.ExtensionWriter;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2395
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->edge_:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 2394
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2397
    :cond_0
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    .line 2398
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->pixel_:I

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2400
    :cond_1
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2401
    const/4 v3, 0x3

    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->state_:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 2400
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2403
    :cond_2
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    .line 2404
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->posterior_:F

    invoke-virtual {p1, v5, v2}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 2406
    :cond_3
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 2407
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2406
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2409
    :cond_4
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_5

    .line 2410
    const/16 v2, 0x10

    iget-boolean v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Node;->isTransient_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 2412
    :cond_5
    const/high16 v2, 0x2000

    invoke-virtual {v0, v2, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 2413
    return-void
.end method
