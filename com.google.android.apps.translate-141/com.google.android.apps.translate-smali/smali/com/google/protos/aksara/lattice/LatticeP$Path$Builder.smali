.class public final Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$PathOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$Path;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$Path;",
        "Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$PathOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private cost_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;"
        }
    .end annotation
.end field

.field private description_:Ljava/lang/Object;

.field private edgeIndex_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private label_:Lcom/google/protobuf/LazyStringList;

.field private text_:Ljava/lang/Object;

.field private weightedCost_:D


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3724
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 3881
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3955
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    .line 4048
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    .line 4114
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    .line 4272
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 3725
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->maybeForceBuilderInitialization()V

    .line 3726
    return-void
.end method

.method static synthetic access$3200()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 3719
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 3731
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCostIsMutable()V
    .locals 2

    .prologue
    .line 4117
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 4118
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    .line 4119
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4121
    :cond_0
    return-void
.end method

.method private ensureEdgeIndexIsMutable()V
    .locals 2

    .prologue
    .line 4050
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 4051
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    .line 4052
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4054
    :cond_0
    return-void
.end method

.method private ensureLabelIsMutable()V
    .locals 2

    .prologue
    .line 3957
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 3958
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    .line 3959
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3961
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 3729
    return-void
.end method


# virtual methods
.method public addAllCost(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;"
        }
    .end annotation

    .prologue
    .line 4214
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/protos/aksara/lattice/LatticeP$Cost;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 4215
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 4217
    return-object p0
.end method

.method public addAllEdgeIndex(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;"
        }
    .end annotation

    .prologue
    .line 4098
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureEdgeIndexIsMutable()V

    .line 4099
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 4101
    return-object p0
.end method

.method public addAllLabel(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;"
        }
    .end annotation

    .prologue
    .line 4019
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureLabelIsMutable()V

    .line 4020
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 4022
    return-object p0
.end method

.method public addCost(ILcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 4204
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 4205
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 4207
    return-object p0
.end method

.method public addCost(ILcom/google/protos/aksara/lattice/LatticeP$Cost;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 4181
    if-nez p2, :cond_0

    .line 4182
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4184
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 4185
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 4187
    return-object p0
.end method

.method public addCost(Lcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2
    .parameter "builderForValue"

    .prologue
    .line 4194
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 4195
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4197
    return-object p0
.end method

.method public addCost(Lcom/google/protos/aksara/lattice/LatticeP$Cost;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4168
    if-nez p1, :cond_0

    .line 4169
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4171
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 4172
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4174
    return-object p0
.end method

.method public addEdgeIndex(I)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2
    .parameter "value"

    .prologue
    .line 4088
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureEdgeIndexIsMutable()V

    .line 4089
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4091
    return-object p0
.end method

.method public addLabel(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4006
    if-nez p1, :cond_0

    .line 4007
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4009
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureLabelIsMutable()V

    .line 4010
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 4012
    return-object p0
.end method

.method public addLabelBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4038
    if-nez p1, :cond_0

    .line 4039
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4041
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureLabelIsMutable()V

    .line 4042
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 4044
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 2

    .prologue
    .line 3760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    .line 3761
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$Path;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3762
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 3764
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 5

    .prologue
    .line 3768
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Path;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 3769
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$Path;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3770
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 3771
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 3772
    or-int/lit8 v2, v2, 0x1

    .line 3774
    :cond_0
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3402(Lcom/google/protos/aksara/lattice/LatticeP$Path;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3775
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 3776
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    .line 3778
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3780
    :cond_1
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3502(Lcom/google/protos/aksara/lattice/LatticeP$Path;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 3781
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 3782
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    .line 3783
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3785
    :cond_2
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3602(Lcom/google/protos/aksara/lattice/LatticeP$Path;Ljava/util/List;)Ljava/util/List;

    .line 3786
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 3787
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    .line 3788
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3790
    :cond_3
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3702(Lcom/google/protos/aksara/lattice/LatticeP$Path;Ljava/util/List;)Ljava/util/List;

    .line 3791
    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 3792
    or-int/lit8 v2, v2, 0x2

    .line 3794
    :cond_4
    iget-wide v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->weightedCost_:D

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->weightedCost_:D
    invoke-static {v1, v3, v4}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3802(Lcom/google/protos/aksara/lattice/LatticeP$Path;D)D

    .line 3795
    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 3796
    or-int/lit8 v2, v2, 0x4

    .line 3798
    :cond_5
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3902(Lcom/google/protos/aksara/lattice/LatticeP$Path;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3799
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$4002(Lcom/google/protos/aksara/lattice/LatticeP$Path;I)I

    .line 3800
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2

    .prologue
    .line 3735
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 3736
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3737
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3738
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    .line 3739
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3740
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    .line 3741
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3742
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    .line 3743
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3744
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->weightedCost_:D

    .line 3745
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3746
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 3747
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3748
    return-object p0
.end method

.method public clearCost()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 4223
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    .line 4224
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4226
    return-object p0
.end method

.method public clearDescription()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 4326
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4327
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 4329
    return-object p0
.end method

.method public clearEdgeIndex()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 4107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    .line 4108
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4110
    return-object p0
.end method

.method public clearLabel()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 4028
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    .line 4029
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4031
    return-object p0
.end method

.method public clearText()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 3935
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3936
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3938
    return-object p0
.end method

.method public clearWeightedCost()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2

    .prologue
    .line 4265
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4266
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->weightedCost_:D

    .line 4268
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2

    .prologue
    .line 3752
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Cost;
    .locals 1
    .parameter "index"

    .prologue
    .line 4139
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    return-object v0
.end method

.method public getCostCount()I
    .locals 1

    .prologue
    .line 4133
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCostList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4127
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3719
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1

    .prologue
    .line 3756
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4283
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 4284
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    .line 4285
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 4287
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 4290
    .end local v1           #s:Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_0
.end method

.method public getDescriptionBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 4298
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 4299
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 4300
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 4303
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 4306
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public getEdgeIndex(I)I
    .locals 1
    .parameter "index"

    .prologue
    .line 4072
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getEdgeIndexCount()I
    .locals 1

    .prologue
    .line 4066
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEdgeIndexList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4060
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLabel(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 3979
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLabelBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 3986
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getLabelCount()I
    .locals 1

    .prologue
    .line 3973
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getLabelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3967
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 3892
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3893
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    .line 3894
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3896
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3899
    .end local v1           #s:Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_0
.end method

.method public getTextBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 3907
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3908
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3909
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3912
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3915
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public getWeightedCost()D
    .locals 2

    .prologue
    .line 4250
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->weightedCost_:D

    return-wide v0
.end method

.method public hasDescription()Z
    .locals 2

    .prologue
    .line 4277
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3886
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWeightedCost()Z
    .locals 2

    .prologue
    .line 4244
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    .line 3852
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->getCostCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 3853
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->getCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/aksara/lattice/LatticeP$Cost;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3855
    const/4 v1, 0x0

    .line 3858
    :goto_1
    return v1

    .line 3852
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3858
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3719
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 3719
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3719
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3865
    const/4 v2, 0x0

    .line 3867
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$Path;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3872
    if-eqz v2, :cond_0

    .line 3873
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    .line 3876
    :cond_0
    return-object p0

    .line 3868
    :catch_0
    move-exception v1

    .line 3869
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-object v2, v0

    .line 3870
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3872
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 3873
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2
    .parameter "other"

    .prologue
    .line 3804
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3848
    :cond_0
    :goto_0
    return-object p0

    .line 3805
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->hasText()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3806
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3807
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3400(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3810
    :cond_2
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3500(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3811
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3812
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3500(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    .line 3813
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3820
    :cond_3
    :goto_1
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3600(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3821
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3822
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3600(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    .line 3823
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3830
    :cond_4
    :goto_2
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3700(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3831
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3832
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3700(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    .line 3833
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3840
    :cond_5
    :goto_3
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->hasWeightedCost()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3841
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getWeightedCost()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->setWeightedCost(D)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    .line 3843
    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3844
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3845
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3900(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 3815
    :cond_7
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureLabelIsMutable()V

    .line 3816
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3500(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 3825
    :cond_8
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureEdgeIndexIsMutable()V

    .line 3826
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3600(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 3835
    :cond_9
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 3836
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->access$3700(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public removeCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "index"

    .prologue
    .line 4232
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 4233
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 4235
    return-object p0
.end method

.method public setCost(ILcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 4159
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 4160
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/protos/aksara/lattice/LatticeP$Cost$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4162
    return-object p0
.end method

.method public setCost(ILcom/google/protos/aksara/lattice/LatticeP$Cost;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 4146
    if-nez p2, :cond_0

    .line 4147
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4149
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureCostIsMutable()V

    .line 4150
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->cost_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4152
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4314
    if-nez p1, :cond_0

    .line 4315
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4317
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4318
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 4320
    return-object p0
.end method

.method public setDescriptionBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4336
    if-nez p1, :cond_0

    .line 4337
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4339
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4340
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->description_:Ljava/lang/Object;

    .line 4342
    return-object p0
.end method

.method public setEdgeIndex(II)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 2
    .parameter "index"
    .parameter "value"

    .prologue
    .line 4079
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureEdgeIndexIsMutable()V

    .line 4080
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->edgeIndex_:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4082
    return-object p0
.end method

.method public setLabel(ILjava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 3993
    if-nez p2, :cond_0

    .line 3994
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3996
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->ensureLabelIsMutable()V

    .line 3997
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3999
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 3923
    if-nez p1, :cond_0

    .line 3924
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3926
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3927
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3929
    return-object p0
.end method

.method public setTextBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 3945
    if-nez p1, :cond_0

    .line 3946
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3948
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 3949
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->text_:Ljava/lang/Object;

    .line 3951
    return-object p0
.end method

.method public setWeightedCost(D)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4256
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->bitField0_:I

    .line 4257
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->weightedCost_:D

    .line 4259
    return-object p0
.end method
