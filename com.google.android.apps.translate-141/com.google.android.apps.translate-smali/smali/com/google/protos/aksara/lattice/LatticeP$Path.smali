.class public final Lcom/google/protos/aksara/lattice/LatticeP$Path;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$PathOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Path"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    }
.end annotation


# static fields
.field public static final COST_FIELD_NUMBER:I = 0x4

.field public static final DESCRIPTION_FIELD_NUMBER:I = 0x6

.field public static final EDGE_INDEX_FIELD_NUMBER:I = 0x3

.field public static final LABEL_FIELD_NUMBER:I = 0x2

.field public static PARSER:Lcom/google/protobuf/Parser; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path;",
            ">;"
        }
    .end annotation
.end field

.field public static final TEXT_FIELD_NUMBER:I = 0x1

.field public static final WEIGHTED_COST_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Path;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private cost_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;"
        }
    .end annotation
.end field

.field private description_:Ljava/lang/Object;

.field private edgeIndex_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private label_:Lcom/google/protobuf/LazyStringList;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private text_:Ljava/lang/Object;

.field private weightedCost_:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3352
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Path$1;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$1;-><init>()V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    .line 4349
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;-><init>(Z)V

    sput-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Path;

    .line 4350
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Path;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->initFields()V

    .line 4351
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 11
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/16 v10, 0x8

    const/4 v8, 0x2

    const/4 v9, 0x4

    .line 3262
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3567
    iput-byte v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedIsInitialized:B

    .line 3605
    iput v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedSerializedSize:I

    .line 3263
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->initFields()V

    .line 3264
    const/4 v4, 0x0

    .line 3266
    .local v4, mutable_bitField0_:I
    const/4 v0, 0x0

    .line 3267
    .local v0, done:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_9

    .line 3268
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v5

    .line 3269
    .local v5, tag:I
    sparse-switch v5, :sswitch_data_0

    .line 3274
    invoke-virtual {p0, p1, p2, v5}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3276
    const/4 v0, 0x1

    goto :goto_0

    .line 3271
    :sswitch_0
    const/4 v0, 0x1

    .line 3272
    goto :goto_0

    .line 3281
    :sswitch_1
    iget v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    .line 3282
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 3334
    .end local v5           #tag:I
    :catch_0
    move-exception v1

    .line 3335
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v6

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3340
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v6

    and-int/lit8 v7, v4, 0x2

    if-ne v7, v8, :cond_1

    .line 3341
    new-instance v7, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v8, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v7, v8}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v7, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    .line 3343
    :cond_1
    and-int/lit8 v7, v4, 0x4

    if-ne v7, v9, :cond_2

    .line 3344
    iget-object v7, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-static {v7}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    iput-object v7, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    .line 3346
    :cond_2
    and-int/lit8 v7, v4, 0x8

    if-ne v7, v10, :cond_3

    .line 3347
    iget-object v7, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-static {v7}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    iput-object v7, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    .line 3349
    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->makeExtensionsImmutable()V

    throw v6

    .line 3286
    .restart local v5       #tag:I
    :sswitch_2
    and-int/lit8 v6, v4, 0x2

    if-eq v6, v8, :cond_4

    .line 3287
    :try_start_2
    new-instance v6, Lcom/google/protobuf/LazyStringArrayList;

    invoke-direct {v6}, Lcom/google/protobuf/LazyStringArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    .line 3288
    or-int/lit8 v4, v4, 0x2

    .line 3290
    :cond_4
    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 3336
    .end local v5           #tag:I
    :catch_1
    move-exception v1

    .line 3337
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    new-instance v6, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v6

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3294
    .end local v1           #e:Ljava/io/IOException;
    .restart local v5       #tag:I
    :sswitch_3
    and-int/lit8 v6, v4, 0x4

    if-eq v6, v9, :cond_5

    .line 3295
    :try_start_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    .line 3296
    or-int/lit8 v4, v4, 0x4

    .line 3298
    :cond_5
    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 3302
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v2

    .line 3303
    .local v2, length:I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v3

    .line 3304
    .local v3, limit:I
    and-int/lit8 v6, v4, 0x4

    if-eq v6, v9, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v6

    if-lez v6, :cond_6

    .line 3305
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    .line 3306
    or-int/lit8 v4, v4, 0x4

    .line 3308
    :cond_6
    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v6

    if-lez v6, :cond_7

    .line 3309
    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3311
    :cond_7
    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto/16 :goto_0

    .line 3315
    .end local v2           #length:I
    .end local v3           #limit:I
    :sswitch_5
    and-int/lit8 v6, v4, 0x8

    if-eq v6, v10, :cond_8

    .line 3316
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    .line 3317
    or-int/lit8 v4, v4, 0x8

    .line 3319
    :cond_8
    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    sget-object v7, Lcom/google/protos/aksara/lattice/LatticeP$Cost;->PARSER:Lcom/google/protobuf/Parser;

    invoke-virtual {p1, v7, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 3323
    :sswitch_6
    iget v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    .line 3324
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->weightedCost_:D

    goto/16 :goto_0

    .line 3328
    :sswitch_7
    iget v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    .line 3329
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 3340
    .end local v5           #tag:I
    :cond_9
    and-int/lit8 v6, v4, 0x2

    if-ne v6, v8, :cond_a

    .line 3341
    new-instance v6, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v7, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v6, v7}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    .line 3343
    :cond_a
    and-int/lit8 v6, v4, 0x4

    if-ne v6, v9, :cond_b

    .line 3344
    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    .line 3346
    :cond_b
    and-int/lit8 v6, v4, 0x8

    if-ne v6, v10, :cond_c

    .line 3347
    iget-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    .line 3349
    :cond_c
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->makeExtensionsImmutable()V

    .line 3351
    return-void

    .line 3269
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x29 -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3240
    invoke-direct {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$Path;-><init>(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V
    .locals 1
    .parameter "builder"

    .prologue
    const/4 v0, -0x1

    .line 3245
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 3567
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedIsInitialized:B

    .line 3605
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedSerializedSize:I

    .line 3247
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 3240
    invoke-direct {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$Path;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter "noInit"

    .prologue
    const/4 v0, -0x1

    .line 3248
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3567
    iput-byte v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedIsInitialized:B

    .line 3605
    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedSerializedSize:I

    .line 3248
    return-void
.end method

.method static synthetic access$3400(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 3240
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/google/protos/aksara/lattice/LatticeP$Path;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 3240
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter "x0"

    .prologue
    .line 3240
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/google/protos/aksara/lattice/LatticeP$Path;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 3240
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 3240
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3602(Lcom/google/protos/aksara/lattice/LatticeP$Path;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 3240
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3700(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 3240
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/google/protos/aksara/lattice/LatticeP$Path;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 3240
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3802(Lcom/google/protos/aksara/lattice/LatticeP$Path;D)D
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 3240
    iput-wide p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->weightedCost_:D

    return-wide p1
.end method

.method static synthetic access$3900(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 3240
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/google/protos/aksara/lattice/LatticeP$Path;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 3240
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4002(Lcom/google/protos/aksara/lattice/LatticeP$Path;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 3240
    iput p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1

    .prologue
    .line 3252
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    .line 3560
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;

    .line 3561
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    .line 3562
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    .line 3563
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    .line 3564
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->weightedCost_:D

    .line 3565
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;

    .line 3566
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 3709
    #calls: Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->access$3200()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1
    .parameter "prototype"

    .prologue
    .line 3712
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3689
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3695
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3659
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3665
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3700
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3706
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3679
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3685
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3669
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0}, Lcom/google/protobuf/Parser;->parseFrom([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1
    .parameter "data"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3675
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/Parser;->parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method


# virtual methods
.method public getCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Cost;
    .locals 1
    .parameter "index"

    .prologue
    .line 3490
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    return-object v0
.end method

.method public getCostCount()I
    .locals 1

    .prologue
    .line 3484
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCostList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Cost;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3471
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    return-object v0
.end method

.method public getCostOrBuilder(I)Lcom/google/protos/aksara/lattice/LatticeP$CostOrBuilder;
    .locals 1
    .parameter "index"

    .prologue
    .line 3497
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$CostOrBuilder;

    return-object v0
.end method

.method public getCostOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/protos/aksara/lattice/LatticeP$CostOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3478
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3240
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Path;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$Path;
    .locals 1

    .prologue
    .line 3256
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->defaultInstance:Lcom/google/protos/aksara/lattice/LatticeP$Path;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3529
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;

    .line 3530
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 3531
    check-cast v1, Ljava/lang/String;

    .line 3539
    .end local v1           #ref:Ljava/lang/Object;
    :goto_0
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    move-object v0, v1

    .line 3533
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3535
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 3536
    .local v2, s:Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isValidUtf8()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3537
    iput-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    .line 3539
    goto :goto_0
.end method

.method public getDescriptionBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 3547
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;

    .line 3548
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3549
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3552
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->description_:Ljava/lang/Object;

    .line 3555
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public getEdgeIndex(I)I
    .locals 1
    .parameter "index"

    .prologue
    .line 3461
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getEdgeIndexCount()I
    .locals 1

    .prologue
    .line 3455
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEdgeIndexList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3449
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    return-object v0
.end method

.method public getLabel(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 3431
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLabelBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 3438
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getLabelCount()I
    .locals 1

    .prologue
    .line 3425
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getLabelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3419
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getParserForType()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "Lcom/google/protos/aksara/lattice/LatticeP$Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3364
    sget-object v0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->PARSER:Lcom/google/protobuf/Parser;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v5, 0x1

    .line 3607
    iget v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedSerializedSize:I

    .line 3608
    .local v2, size:I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 3646
    .end local v2           #size:I
    .local v3, size:I
    :goto_0
    return v3

    .line 3610
    .end local v3           #size:I
    .restart local v2       #size:I
    :cond_0
    const/4 v2, 0x0

    .line 3611
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1

    .line 3612
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    .line 3616
    :cond_1
    const/4 v0, 0x0

    .line 3617
    .local v0, dataSize:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 3618
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3617
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3621
    :cond_2
    add-int/2addr v2, v0

    .line 3622
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getLabelList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 3625
    const/4 v0, 0x0

    .line 3626
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 3627
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 3626
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3630
    :cond_3
    add-int/2addr v2, v0

    .line 3631
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getEdgeIndexList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 3633
    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 3634
    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v7, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 3633
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 3637
    :cond_4
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    .line 3638
    const/4 v4, 0x5

    iget-wide v5, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->weightedCost_:D

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v4

    add-int/2addr v2, v4

    .line 3641
    :cond_5
    iget v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v7, :cond_6

    .line 3642
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getDescriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    .line 3645
    :cond_6
    iput v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedSerializedSize:I

    move v3, v2

    .line 3646
    .end local v2           #size:I
    .restart local v3       #size:I
    goto/16 :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3381
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;

    .line 3382
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 3383
    check-cast v1, Ljava/lang/String;

    .line 3391
    .end local v1           #ref:Ljava/lang/Object;
    :goto_0
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    move-object v0, v1

    .line 3385
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3387
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 3388
    .local v2, s:Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isValidUtf8()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3389
    iput-object v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    .line 3391
    goto :goto_0
.end method

.method public getTextBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 3399
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;

    .line 3400
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3401
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3404
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->text_:Ljava/lang/Object;

    .line 3407
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public getWeightedCost()D
    .locals 2

    .prologue
    .line 3513
    iget-wide v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->weightedCost_:D

    return-wide v0
.end method

.method public hasDescription()Z
    .locals 2

    .prologue
    .line 3523
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3375
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWeightedCost()Z
    .locals 2

    .prologue
    .line 3507
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3569
    iget-byte v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedIsInitialized:B

    .line 3570
    .local v1, isInitialized:B
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    if-ne v1, v2, :cond_0

    .line 3579
    :goto_0
    return v2

    :cond_0
    move v2, v3

    .line 3570
    goto :goto_0

    .line 3572
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getCostCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 3573
    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getCost(I)Lcom/google/protos/aksara/lattice/LatticeP$Cost;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protos/aksara/lattice/LatticeP$Cost;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_2

    .line 3574
    iput-byte v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedIsInitialized:B

    move v2, v3

    .line 3575
    goto :goto_0

    .line 3572
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3578
    :cond_3
    iput-byte v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3240
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 3710
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->newBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3240
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;
    .locals 1

    .prologue
    .line 3714
    invoke-static {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->newBuilder(Lcom/google/protos/aksara/lattice/LatticeP$Path;)Lcom/google/protos/aksara/lattice/LatticeP$Path$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 3653
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3584
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getSerializedSize()I

    .line 3585
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 3586
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3588
    :cond_0
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 3589
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->label_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3588
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3591
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 3592
    const/4 v2, 0x3

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->edgeIndex_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3591
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3594
    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 3595
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->cost_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3594
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3597
    :cond_3
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_4

    .line 3598
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->weightedCost_:D

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 3600
    :cond_4
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$Path;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_5

    .line 3601
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$Path;->getDescriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3603
    :cond_5
    return-void
.end method
