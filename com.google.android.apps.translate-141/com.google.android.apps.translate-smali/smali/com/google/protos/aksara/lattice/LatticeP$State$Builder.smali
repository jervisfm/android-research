.class public final Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$StateOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$State;",
        "Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$StateOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private description_:Lcom/google/protobuf/LazyStringList;

.field private state_:Lcom/google/protobuf/ByteString;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1764
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    .line 1864
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->state_:Lcom/google/protobuf/ByteString;

    .line 1900
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1765
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->maybeForceBuilderInitialization()V

    .line 1766
    return-void
.end method

.method static synthetic access$1600()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1

    .prologue
    .line 1760
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1

    .prologue
    .line 1771
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;-><init>()V

    return-object v0
.end method

.method private ensureDescriptionIsMutable()V
    .locals 2

    .prologue
    .line 1902
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1903
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1904
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1906
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 1769
    return-void
.end method


# virtual methods
.method public addAllDescription(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;"
        }
    .end annotation

    .prologue
    .line 1964
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->ensureDescriptionIsMutable()V

    .line 1965
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 1967
    return-object p0
.end method

.method public addDescription(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1951
    if-nez p1, :cond_0

    .line 1952
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1954
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->ensureDescriptionIsMutable()V

    .line 1955
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 1957
    return-object p0
.end method

.method public addDescriptionBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1983
    if-nez p1, :cond_0

    .line 1984
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1986
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->ensureDescriptionIsMutable()V

    .line 1987
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 1989
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$State;
    .locals 2

    .prologue
    .line 1792
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v0

    .line 1793
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$State;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$State;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1794
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 1796
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$State;
    .locals 5

    .prologue
    .line 1800
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$State;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$State;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 1801
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$State;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1802
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 1803
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1804
    or-int/lit8 v2, v2, 0x1

    .line 1806
    :cond_0
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->state_:Lcom/google/protobuf/ByteString;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$State;->state_:Lcom/google/protobuf/ByteString;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$State;->access$1802(Lcom/google/protos/aksara/lattice/LatticeP$State;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 1807
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 1808
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1810
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1812
    :cond_1
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$State;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$State;->access$1902(Lcom/google/protos/aksara/lattice/LatticeP$State;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 1813
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$State;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$State;->access$2002(Lcom/google/protos/aksara/lattice/LatticeP$State;I)I

    .line 1814
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1

    .prologue
    .line 1775
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;->clear()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;

    .line 1776
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->state_:Lcom/google/protobuf/ByteString;

    .line 1777
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1778
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1779
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1780
    return-object p0
.end method

.method public clearDescription()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1

    .prologue
    .line 1973
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1974
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1976
    return-object p0
.end method

.method public clearState()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1

    .prologue
    .line 1893
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1894
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$State;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$State;->getState()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->state_:Lcom/google/protobuf/ByteString;

    .line 1896
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 2

    .prologue
    .line 1784
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1760
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$State;
    .locals 1

    .prologue
    .line 1788
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$State;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v0

    return-object v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 1924
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 1931
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionCount()I
    .locals 1

    .prologue
    .line 1918
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getDescriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1912
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lcom/google/protobuf/ByteString;
    .locals 1

    .prologue
    .line 1875
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->state_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public hasState()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1869
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 1837
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->extensionsAreInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1839
    const/4 v0, 0x0

    .line 1841
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1760
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 1760
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$State;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1760
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1848
    const/4 v2, 0x0

    .line 1850
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$State;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$State;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1855
    if-eqz v2, :cond_0

    .line 1856
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    .line 1859
    :cond_0
    return-object p0

    .line 1851
    :catch_0
    move-exception v1

    .line 1852
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-object v2, v0

    .line 1853
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 1856
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 2
    .parameter "other"

    .prologue
    .line 1818
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$State;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$State;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1833
    :goto_0
    return-object p0

    .line 1819
    :cond_0
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$State;->hasState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1820
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$State;->getState()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->setState(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;

    .line 1822
    :cond_1
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$State;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$State;->access$1900(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1823
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1824
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$State;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$State;->access$1900(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 1825
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1832
    :cond_2
    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto :goto_0

    .line 1827
    :cond_3
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->ensureDescriptionIsMutable()V

    .line 1828
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$State;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$State;->access$1900(Lcom/google/protos/aksara/lattice/LatticeP$State;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public setDescription(ILjava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 1938
    if-nez p2, :cond_0

    .line 1939
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1941
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->ensureDescriptionIsMutable()V

    .line 1942
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1944
    return-object p0
.end method

.method public setState(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 1881
    if-nez p1, :cond_0

    .line 1882
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1884
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->bitField0_:I

    .line 1885
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$State$Builder;->state_:Lcom/google/protobuf/ByteString;

    .line 1887
    return-object p0
.end method
