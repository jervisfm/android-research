.class public final Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LatticeP.java"

# interfaces
.implements Lcom/google/protos/aksara/lattice/LatticeP$StateTypeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/aksara/lattice/LatticeP$StateType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/aksara/lattice/LatticeP$StateType;",
        "Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;",
        ">;",
        "Lcom/google/protos/aksara/lattice/LatticeP$StateTypeOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private description_:Lcom/google/protobuf/LazyStringList;

.field private name_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 4675
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 4772
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4846
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 4676
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->maybeForceBuilderInitialization()V

    .line 4677
    return-void
.end method

.method static synthetic access$4200()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1

    .prologue
    .line 4670
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1

    .prologue
    .line 4682
    new-instance v0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    invoke-direct {v0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;-><init>()V

    return-object v0
.end method

.method private ensureDescriptionIsMutable()V
    .locals 2

    .prologue
    .line 4848
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 4849
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 4850
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4852
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 4680
    return-void
.end method


# virtual methods
.method public addAllDescription(Ljava/lang/Iterable;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;"
        }
    .end annotation

    .prologue
    .line 4910
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->ensureDescriptionIsMutable()V

    .line 4911
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 4913
    return-object p0
.end method

.method public addDescription(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4897
    if-nez p1, :cond_0

    .line 4898
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4900
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->ensureDescriptionIsMutable()V

    .line 4901
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 4903
    return-object p0
.end method

.method public addDescriptionBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4929
    if-nez p1, :cond_0

    .line 4930
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4932
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->ensureDescriptionIsMutable()V

    .line 4933
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 4935
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->build()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/aksara/lattice/LatticeP$StateType;
    .locals 2

    .prologue
    .line 4703
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v0

    .line 4704
    .local v0, result:Lcom/google/protos/aksara/lattice/LatticeP$StateType;
    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4705
    invoke-static {v0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v1

    throw v1

    .line 4707
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$StateType;
    .locals 5

    .prologue
    .line 4711
    new-instance v1, Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;Lcom/google/protos/aksara/lattice/LatticeP$1;)V

    .line 4712
    .local v1, result:Lcom/google/protos/aksara/lattice/LatticeP$StateType;
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4713
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 4714
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 4715
    or-int/lit8 v2, v2, 0x1

    .line 4717
    :cond_0
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$StateType;->name_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->access$4402(Lcom/google/protos/aksara/lattice/LatticeP$StateType;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4718
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 4719
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 4721
    iget v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4723
    :cond_1
    iget-object v3, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$StateType;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->access$4502(Lcom/google/protos/aksara/lattice/LatticeP$StateType;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 4724
    #setter for: Lcom/google/protos/aksara/lattice/LatticeP$StateType;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->access$4602(Lcom/google/protos/aksara/lattice/LatticeP$StateType;I)I

    .line 4725
    return-object v1
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->clear()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1

    .prologue
    .line 4686
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 4687
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4688
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4689
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 4690
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4691
    return-object p0
.end method

.method public clearDescription()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1

    .prologue
    .line 4919
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 4920
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4922
    return-object p0
.end method

.method public clearName()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1

    .prologue
    .line 4826
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4827
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4829
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 2

    .prologue
    .line 4695
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->create()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->buildPartial()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->clone()Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4670
    invoke-virtual {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/aksara/lattice/LatticeP$StateType;
    .locals 1

    .prologue
    .line 4699
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v0

    return-object v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .parameter "index"

    .prologue
    .line 4870
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionBytes(I)Lcom/google/protobuf/ByteString;
    .locals 1
    .parameter "index"

    .prologue
    .line 4877
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionCount()I
    .locals 1

    .prologue
    .line 4864
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getDescriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4858
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4783
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4784
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    .line 4785
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 4787
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4790
    .end local v1           #s:Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_0
.end method

.method public getNameBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    .prologue
    .line 4798
    iget-object v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4799
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 4800
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 4803
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4806
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_0
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_0
.end method

.method public hasName()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4777
    iget v1, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 4749
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4670
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 4670
    check-cast p1, Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4670
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 4
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4756
    const/4 v2, 0x0

    .line 4758
    .local v2, parsedMessage:Lcom/google/protos/aksara/lattice/LatticeP$StateType;
    :try_start_0
    sget-object v3, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->PARSER:Lcom/google/protobuf/Parser;

    invoke-interface {v3, p1, p2}, Lcom/google/protobuf/Parser;->parsePartialFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4763
    if-eqz v2, :cond_0

    .line 4764
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    .line 4767
    :cond_0
    return-object p0

    .line 4759
    :catch_0
    move-exception v1

    .line 4760
    .local v1, e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :try_start_1
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-object v2, v0

    .line 4761
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4763
    .end local v1           #e:Lcom/google/protobuf/InvalidProtocolBufferException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 4764
    invoke-virtual {p0, v2}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;

    :cond_1
    throw v3
.end method

.method public mergeFrom(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 2
    .parameter "other"

    .prologue
    .line 4729
    invoke-static {}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->getDefaultInstance()Lcom/google/protos/aksara/lattice/LatticeP$StateType;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 4745
    :cond_0
    :goto_0
    return-object p0

    .line 4730
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4731
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4732
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$StateType;->name_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->access$4400(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4735
    :cond_2
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$StateType;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->access$4500(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4736
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4737
    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$StateType;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->access$4500(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    .line 4738
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    goto :goto_0

    .line 4740
    :cond_3
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->ensureDescriptionIsMutable()V

    .line 4741
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/google/protos/aksara/lattice/LatticeP$StateType;->description_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/protos/aksara/lattice/LatticeP$StateType;->access$4500(Lcom/google/protos/aksara/lattice/LatticeP$StateType;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setDescription(ILjava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1
    .parameter "index"
    .parameter "value"

    .prologue
    .line 4884
    if-nez p2, :cond_0

    .line 4885
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4887
    :cond_0
    invoke-direct {p0}, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->ensureDescriptionIsMutable()V

    .line 4888
    iget-object v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->description_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4890
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4814
    if-nez p1, :cond_0

    .line 4815
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4817
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4818
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4820
    return-object p0
.end method

.method public setNameBytes(Lcom/google/protobuf/ByteString;)Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;
    .locals 1
    .parameter "value"

    .prologue
    .line 4836
    if-nez p1, :cond_0

    .line 4837
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4839
    :cond_0
    iget v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->bitField0_:I

    .line 4840
    iput-object p1, p0, Lcom/google/protos/aksara/lattice/LatticeP$StateType$Builder;->name_:Ljava/lang/Object;

    .line 4842
    return-object p0
.end method
