.class public final Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "OndeviceSpec.java"

# interfaces
.implements Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpecOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;",
        ">;",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpecOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private clientName_:Ljava/lang/Object;

.field private clientVersion_:I

.field private compressRequests_:Z

.field private deviceName_:Ljava/lang/Object;

.field private deviceVersion_:I

.field private feedbackBatchSize_:I

.field private recoPath_:Ljava/lang/Object;

.field private sendFeedbackImmediately_:Z

.field private server_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1869
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 2077
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    .line 2113
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    .line 2149
    iput-boolean v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->compressRequests_:Z

    .line 2170
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    .line 2227
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    .line 2284
    iput-boolean v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->sendFeedbackImmediately_:Z

    .line 2305
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->feedbackBatchSize_:I

    .line 1870
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->maybeForceBuilderInitialization()V

    .line 1871
    return-void
.end method

.method static synthetic access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1864
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 1864
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1920
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    .line 1921
    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1922
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 1925
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 1876
    new-instance v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    invoke-direct {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 1874
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->build()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 2

    .prologue
    .line 1911
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    .line 1912
    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1913
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1915
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1929
    new-instance v2, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;-><init>(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;Lcom/google/protos/research_handwriting/OndeviceSpec$1;)V

    .line 1930
    iget v3, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1931
    const/4 v1, 0x0

    .line 1932
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    .line 1935
    :goto_0
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->server_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$2202(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1936
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1937
    or-int/lit8 v0, v0, 0x2

    .line 1939
    :cond_0
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->recoPath_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$2302(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1940
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1941
    or-int/lit8 v0, v0, 0x4

    .line 1943
    :cond_1
    iget-boolean v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->compressRequests_:Z

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->compressRequests_:Z
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$2402(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Z)Z

    .line 1944
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 1945
    or-int/lit8 v0, v0, 0x8

    .line 1947
    :cond_2
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceName_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$2502(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 1949
    or-int/lit8 v0, v0, 0x10

    .line 1951
    :cond_3
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceVersion_:I

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceVersion_:I
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$2602(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;I)I

    .line 1952
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 1953
    or-int/lit8 v0, v0, 0x20

    .line 1955
    :cond_4
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientName_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$2702(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1956
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 1957
    or-int/lit8 v0, v0, 0x40

    .line 1959
    :cond_5
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientVersion_:I

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientVersion_:I
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$2802(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;I)I

    .line 1960
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 1961
    or-int/lit16 v0, v0, 0x80

    .line 1963
    :cond_6
    iget-boolean v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->sendFeedbackImmediately_:Z

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->sendFeedbackImmediately_:Z
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$2902(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Z)Z

    .line 1964
    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    .line 1965
    or-int/lit16 v0, v0, 0x100

    .line 1967
    :cond_7
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->feedbackBatchSize_:I

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->feedbackBatchSize_:I
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$3002(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;I)I

    .line 1968
    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->access$3102(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;I)I

    .line 1969
    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clear()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clear()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1880
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 1881
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    .line 1882
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1883
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    .line 1884
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1885
    iput-boolean v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->compressRequests_:Z

    .line 1886
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1887
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    .line 1888
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1889
    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceVersion_:I

    .line 1890
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1891
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    .line 1892
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1893
    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientVersion_:I

    .line 1894
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1895
    iput-boolean v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->sendFeedbackImmediately_:Z

    .line 1896
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1897
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->feedbackBatchSize_:I

    .line 1898
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 1899
    return-object p0
.end method

.method public clearClientName()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2251
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2252
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getClientName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    .line 2254
    return-object p0
.end method

.method public clearClientVersion()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2277
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2278
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientVersion_:I

    .line 2280
    return-object p0
.end method

.method public clearCompressRequests()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2163
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->compressRequests_:Z

    .line 2166
    return-object p0
.end method

.method public clearDeviceName()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2194
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2195
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    .line 2197
    return-object p0
.end method

.method public clearDeviceVersion()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2220
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2221
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceVersion_:I

    .line 2223
    return-object p0
.end method

.method public clearFeedbackBatchSize()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2319
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2320
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->feedbackBatchSize_:I

    .line 2322
    return-object p0
.end method

.method public clearRecoPath()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2137
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2138
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getRecoPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    .line 2140
    return-object p0
.end method

.method public clearSendFeedbackImmediately()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2298
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2299
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->sendFeedbackImmediately_:Z

    .line 2301
    return-object p0
.end method

.method public clearServer()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2101
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2102
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getServer()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    .line 2104
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 2

    .prologue
    .line 1903
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getClientName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2232
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    .line 2233
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2234
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2235
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    .line 2238
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getClientVersion()I
    .locals 1

    .prologue
    .line 2268
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientVersion_:I

    return v0
.end method

.method public getCompressRequests()Z
    .locals 1

    .prologue
    .line 2154
    iget-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->compressRequests_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1

    .prologue
    .line 1907
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2175
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    .line 2176
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2177
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2178
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    .line 2181
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getDeviceVersion()I
    .locals 1

    .prologue
    .line 2211
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceVersion_:I

    return v0
.end method

.method public getFeedbackBatchSize()I
    .locals 1

    .prologue
    .line 2310
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->feedbackBatchSize_:I

    return v0
.end method

.method public getRecoPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2118
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    .line 2119
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2120
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2121
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    .line 2124
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSendFeedbackImmediately()Z
    .locals 1

    .prologue
    .line 2289
    iget-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->sendFeedbackImmediately_:Z

    return v0
.end method

.method public getServer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2082
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    .line 2083
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2084
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2085
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    .line 2088
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasClientName()Z
    .locals 2

    .prologue
    .line 2229
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClientVersion()Z
    .locals 2

    .prologue
    .line 2265
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCompressRequests()Z
    .locals 2

    .prologue
    .line 2151
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceName()Z
    .locals 2

    .prologue
    .line 2172
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceVersion()Z
    .locals 2

    .prologue
    .line 2208
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFeedbackBatchSize()Z
    .locals 2

    .prologue
    .line 2307
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecoPath()Z
    .locals 2

    .prologue
    .line 2115
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSendFeedbackImmediately()Z
    .locals 2

    .prologue
    .line 2286
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasServer()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2079
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 2005
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1864
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1864
    check-cast p1, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    invoke-virtual {p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1864
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2013
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 2014
    sparse-switch v0, :sswitch_data_0

    .line 2019
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2021
    :sswitch_0
    return-object p0

    .line 2026
    :sswitch_1
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2027
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    goto :goto_0

    .line 2031
    :sswitch_2
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2032
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    goto :goto_0

    .line 2036
    :sswitch_3
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2037
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->compressRequests_:Z

    goto :goto_0

    .line 2041
    :sswitch_4
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2042
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    goto :goto_0

    .line 2046
    :sswitch_5
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2047
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceVersion_:I

    goto :goto_0

    .line 2051
    :sswitch_6
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2052
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    goto :goto_0

    .line 2056
    :sswitch_7
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2057
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientVersion_:I

    goto :goto_0

    .line 2061
    :sswitch_8
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2062
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->sendFeedbackImmediately_:Z

    goto :goto_0

    .line 2066
    :sswitch_9
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2067
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->feedbackBatchSize_:I

    goto/16 :goto_0

    .line 2014
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1973
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2001
    :cond_0
    :goto_0
    return-object p0

    .line 1974
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasServer()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1975
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getServer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setServer(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    .line 1977
    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasRecoPath()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1978
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getRecoPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setRecoPath(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    .line 1980
    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasCompressRequests()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1981
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getCompressRequests()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setCompressRequests(Z)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    .line 1983
    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasDeviceName()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1984
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setDeviceName(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    .line 1986
    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasDeviceVersion()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1987
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDeviceVersion()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setDeviceVersion(I)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    .line 1989
    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasClientName()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1990
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getClientName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setClientName(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    .line 1992
    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasClientVersion()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1993
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getClientVersion()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setClientVersion(I)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    .line 1995
    :cond_8
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasSendFeedbackImmediately()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1996
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getSendFeedbackImmediately()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setSendFeedbackImmediately(Z)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    .line 1998
    :cond_9
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasFeedbackBatchSize()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1999
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getFeedbackBatchSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->setFeedbackBatchSize(I)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    goto :goto_0
.end method

.method public setClientName(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2242
    if-nez p1, :cond_0

    .line 2243
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2245
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2246
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    .line 2248
    return-object p0
.end method

.method setClientName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2257
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2258
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientName_:Ljava/lang/Object;

    .line 2260
    return-void
.end method

.method public setClientVersion(I)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2271
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2272
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->clientVersion_:I

    .line 2274
    return-object p0
.end method

.method public setCompressRequests(Z)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2157
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2158
    iput-boolean p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->compressRequests_:Z

    .line 2160
    return-object p0
.end method

.method public setDeviceName(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2185
    if-nez p1, :cond_0

    .line 2186
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2188
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2189
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    .line 2191
    return-object p0
.end method

.method setDeviceName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2200
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2201
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceName_:Ljava/lang/Object;

    .line 2203
    return-void
.end method

.method public setDeviceVersion(I)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2214
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2215
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->deviceVersion_:I

    .line 2217
    return-object p0
.end method

.method public setFeedbackBatchSize(I)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2313
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2314
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->feedbackBatchSize_:I

    .line 2316
    return-object p0
.end method

.method public setRecoPath(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2128
    if-nez p1, :cond_0

    .line 2129
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2131
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2132
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    .line 2134
    return-object p0
.end method

.method setRecoPath(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2143
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2144
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->recoPath_:Ljava/lang/Object;

    .line 2146
    return-void
.end method

.method public setSendFeedbackImmediately(Z)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2292
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2293
    iput-boolean p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->sendFeedbackImmediately_:Z

    .line 2295
    return-object p0
.end method

.method public setServer(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2092
    if-nez p1, :cond_0

    .line 2093
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2095
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2096
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    .line 2098
    return-object p0
.end method

.method setServer(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2107
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->bitField0_:I

    .line 2108
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->server_:Ljava/lang/Object;

    .line 2110
    return-void
.end method
