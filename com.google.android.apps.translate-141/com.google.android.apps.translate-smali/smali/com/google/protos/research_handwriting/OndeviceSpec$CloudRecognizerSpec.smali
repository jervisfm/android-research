.class public final Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "OndeviceSpec.java"

# interfaces
.implements Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpecOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/research_handwriting/OndeviceSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CloudRecognizerSpec"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    }
.end annotation


# static fields
.field public static final CLIENT_NAME_FIELD_NUMBER:I = 0x6

.field public static final CLIENT_VERSION_FIELD_NUMBER:I = 0x7

.field public static final COMPRESS_REQUESTS_FIELD_NUMBER:I = 0x3

.field public static final DEVICE_NAME_FIELD_NUMBER:I = 0x4

.field public static final DEVICE_VERSION_FIELD_NUMBER:I = 0x5

.field public static final FEEDBACK_BATCH_SIZE_FIELD_NUMBER:I = 0x9

.field public static final RECO_PATH_FIELD_NUMBER:I = 0x2

.field public static final SEND_FEEDBACK_IMMEDIATELY_FIELD_NUMBER:I = 0x8

.field public static final SERVER_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private clientName_:Ljava/lang/Object;

.field private clientVersion_:I

.field private compressRequests_:Z

.field private deviceName_:Ljava/lang/Object;

.field private deviceVersion_:I

.field private feedbackBatchSize_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private recoPath_:Ljava/lang/Object;

.field private sendFeedbackImmediately_:Z

.field private server_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2329
    new-instance v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;-><init>(Z)V

    sput-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    .line 2330
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    invoke-direct {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->initFields()V

    .line 2331
    return-void
.end method

.method private constructor <init>(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1493
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 1696
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->memoizedIsInitialized:B

    .line 1737
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->memoizedSerializedSize:I

    .line 1494
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;Lcom/google/protos/research_handwriting/OndeviceSpec$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    invoke-direct {p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;-><init>(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1495
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1696
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->memoizedIsInitialized:B

    .line 1737
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->memoizedSerializedSize:I

    .line 1495
    return-void
.end method

.method static synthetic access$2202(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->server_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->recoPath_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2402(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput-boolean p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->compressRequests_:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2602(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceVersion_:I

    return p1
.end method

.method static synthetic access$2702(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2802(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientVersion_:I

    return p1
.end method

.method static synthetic access$2902(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput-boolean p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->sendFeedbackImmediately_:Z

    return p1
.end method

.method static synthetic access$3002(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->feedbackBatchSize_:I

    return p1
.end method

.method static synthetic access$3102(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1488
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    return p1
.end method

.method private getClientNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1644
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientName_:Ljava/lang/Object;

    .line 1645
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1646
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1648
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientName_:Ljava/lang/Object;

    .line 1651
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1

    .prologue
    .line 1499
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    return-object v0
.end method

.method private getDeviceNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1602
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceName_:Ljava/lang/Object;

    .line 1603
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1604
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1606
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceName_:Ljava/lang/Object;

    .line 1609
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getRecoPathBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1560
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->recoPath_:Ljava/lang/Object;

    .line 1561
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1562
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1564
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->recoPath_:Ljava/lang/Object;

    .line 1567
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getServerBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1528
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->server_:Ljava/lang/Object;

    .line 1529
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1530
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1532
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->server_:Ljava/lang/Object;

    .line 1535
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1686
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->server_:Ljava/lang/Object;

    .line 1687
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->recoPath_:Ljava/lang/Object;

    .line 1688
    iput-boolean v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->compressRequests_:Z

    .line 1689
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceName_:Ljava/lang/Object;

    .line 1690
    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceVersion_:I

    .line 1691
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientName_:Ljava/lang/Object;

    .line 1692
    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientVersion_:I

    .line 1693
    iput-boolean v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->sendFeedbackImmediately_:Z

    .line 1694
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->feedbackBatchSize_:I

    .line 1695
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 1857
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$2000()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1860
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1826
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    .line 1827
    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1828
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    .line 1830
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1837
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    .line 1838
    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1839
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    .line 1841
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1793
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1799
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1847
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1853
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1815
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1821
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1804
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1810
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;->access$1900(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClientName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1630
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientName_:Ljava/lang/Object;

    .line 1631
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1632
    check-cast v0, Ljava/lang/String;

    .line 1640
    :goto_0
    return-object v0

    .line 1634
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1636
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1637
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1638
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1640
    goto :goto_0
.end method

.method public getClientVersion()I
    .locals 1

    .prologue
    .line 1662
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientVersion_:I

    return v0
.end method

.method public getCompressRequests()Z
    .locals 1

    .prologue
    .line 1578
    iget-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->compressRequests_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1488
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    .locals 1

    .prologue
    .line 1503
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceName_:Ljava/lang/Object;

    .line 1589
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1590
    check-cast v0, Ljava/lang/String;

    .line 1598
    :goto_0
    return-object v0

    .line 1592
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1594
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1595
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1596
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1598
    goto :goto_0
.end method

.method public getDeviceVersion()I
    .locals 1

    .prologue
    .line 1620
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceVersion_:I

    return v0
.end method

.method public getFeedbackBatchSize()I
    .locals 1

    .prologue
    .line 1682
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->feedbackBatchSize_:I

    return v0
.end method

.method public getRecoPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->recoPath_:Ljava/lang/Object;

    .line 1547
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1548
    check-cast v0, Ljava/lang/String;

    .line 1556
    :goto_0
    return-object v0

    .line 1550
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1552
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1553
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1554
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->recoPath_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1556
    goto :goto_0
.end method

.method public getSendFeedbackImmediately()Z
    .locals 1

    .prologue
    .line 1672
    iget-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->sendFeedbackImmediately_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1739
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->memoizedSerializedSize:I

    .line 1740
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1780
    :goto_0
    return v0

    .line 1742
    :cond_0
    const/4 v0, 0x0

    .line 1743
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 1744
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getServerBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1747
    :cond_1
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 1748
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getRecoPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1751
    :cond_2
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 1752
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->compressRequests_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1755
    :cond_3
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 1756
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDeviceNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1759
    :cond_4
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 1760
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceVersion_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1763
    :cond_5
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 1764
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getClientNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1767
    :cond_6
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 1768
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientVersion_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1771
    :cond_7
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 1772
    iget-boolean v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->sendFeedbackImmediately_:Z

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1775
    :cond_8
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 1776
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->feedbackBatchSize_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1779
    :cond_9
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getServer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1514
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->server_:Ljava/lang/Object;

    .line 1515
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1516
    check-cast v0, Ljava/lang/String;

    .line 1524
    :goto_0
    return-object v0

    .line 1518
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1520
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1521
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1522
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->server_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1524
    goto :goto_0
.end method

.method public hasClientName()Z
    .locals 2

    .prologue
    .line 1627
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClientVersion()Z
    .locals 2

    .prologue
    .line 1659
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCompressRequests()Z
    .locals 2

    .prologue
    .line 1575
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceName()Z
    .locals 2

    .prologue
    .line 1585
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceVersion()Z
    .locals 2

    .prologue
    .line 1617
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFeedbackBatchSize()Z
    .locals 2

    .prologue
    .line 1679
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecoPath()Z
    .locals 2

    .prologue
    .line 1543
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSendFeedbackImmediately()Z
    .locals 2

    .prologue
    .line 1669
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasServer()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1511
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1698
    iget-byte v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->memoizedIsInitialized:B

    .line 1699
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 1702
    :goto_0
    return v0

    .line 1699
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1701
    :cond_1
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1488
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilderForType()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 1858
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1488
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->toBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 1862
    invoke-static {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->newBuilder(Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1787
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1707
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getSerializedSize()I

    .line 1708
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1709
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getServerBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1711
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1712
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getRecoPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1714
    :cond_1
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 1715
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->compressRequests_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 1717
    :cond_2
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 1718
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDeviceNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1720
    :cond_3
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1721
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->deviceVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1723
    :cond_4
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 1724
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getClientNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1726
    :cond_5
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 1727
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->clientVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1729
    :cond_6
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 1730
    iget-boolean v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->sendFeedbackImmediately_:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 1732
    :cond_7
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 1733
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->feedbackBatchSize_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1735
    :cond_8
    return-void
.end method
