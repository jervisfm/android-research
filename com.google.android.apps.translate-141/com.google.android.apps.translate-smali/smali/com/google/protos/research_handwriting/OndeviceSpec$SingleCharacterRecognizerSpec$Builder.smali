.class public final Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "OndeviceSpec.java"

# interfaces
.implements Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpecOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;",
        ">;",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpecOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private charRescoringFile_:Ljava/lang/Object;

.field private classifierType_:Ljava/lang/Object;

.field private inkreader_:Ljava/lang/Object;

.field private model_:Ljava/lang/Object;

.field private numResults_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2668
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 2820
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 2856
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 2892
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    .line 2928
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 2669
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->maybeForceBuilderInitialization()V

    .line 2670
    return-void
.end method

.method static synthetic access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2663
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3300()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2663
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2711
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    .line 2712
    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2713
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 2716
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2675
    new-instance v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    invoke-direct {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 2673
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->build()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 2

    .prologue
    .line 2702
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    .line 2703
    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2704
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2706
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2720
    new-instance v2, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;-><init>(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;Lcom/google/protos/research_handwriting/OndeviceSpec$1;)V

    .line 2721
    iget v3, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2722
    const/4 v1, 0x0

    .line 2723
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 2726
    :goto_0
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->inkreader_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->access$3502(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2728
    or-int/lit8 v0, v0, 0x2

    .line 2730
    :cond_0
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->model_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->access$3602(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2732
    or-int/lit8 v0, v0, 0x4

    .line 2734
    :cond_1
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->charRescoringFile_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->access$3702(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2735
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2736
    or-int/lit8 v0, v0, 0x8

    .line 2738
    :cond_2
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->classifierType_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->access$3802(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 2740
    or-int/lit8 v0, v0, 0x10

    .line 2742
    :cond_3
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->numResults_:I

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->numResults_:I
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->access$3902(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;I)I

    .line 2743
    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->access$4002(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;I)I

    .line 2744
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->clear()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->clear()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2679
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 2680
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 2681
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2682
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 2683
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2684
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    .line 2685
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2686
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 2687
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2688
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->numResults_:I

    .line 2689
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2690
    return-object p0
.end method

.method public clearCharRescoringFile()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2916
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2917
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getCharRescoringFile()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    .line 2919
    return-object p0
.end method

.method public clearClassifierType()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2952
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2953
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getClassifierType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 2955
    return-object p0
.end method

.method public clearInkreader()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2844
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2845
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getInkreader()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 2847
    return-object p0
.end method

.method public clearModel()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2880
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2881
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getModel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 2883
    return-object p0
.end method

.method public clearNumResults()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2978
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2979
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->numResults_:I

    .line 2981
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 2

    .prologue
    .line 2694
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCharRescoringFile()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2897
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    .line 2898
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2899
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2900
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    .line 2903
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getClassifierType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2933
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 2934
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2935
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2936
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 2939
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2663
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1

    .prologue
    .line 2698
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getInkreader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2825
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 2826
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2827
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2828
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 2831
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2861
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 2862
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2863
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2864
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 2867
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNumResults()I
    .locals 1

    .prologue
    .line 2969
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->numResults_:I

    return v0
.end method

.method public hasCharRescoringFile()Z
    .locals 2

    .prologue
    .line 2894
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClassifierType()Z
    .locals 2

    .prologue
    .line 2930
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInkreader()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2822
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasModel()Z
    .locals 2

    .prologue
    .line 2858
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNumResults()Z
    .locals 2

    .prologue
    .line 2966
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 2768
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2663
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2663
    check-cast p1, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    invoke-virtual {p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2663
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2776
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 2777
    sparse-switch v0, :sswitch_data_0

    .line 2782
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2784
    :sswitch_0
    return-object p0

    .line 2789
    :sswitch_1
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2790
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    goto :goto_0

    .line 2794
    :sswitch_2
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2795
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    goto :goto_0

    .line 2799
    :sswitch_3
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2800
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    goto :goto_0

    .line 2804
    :sswitch_4
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2805
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    goto :goto_0

    .line 2809
    :sswitch_5
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2810
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->numResults_:I

    goto :goto_0

    .line 2777
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2748
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2764
    :cond_0
    :goto_0
    return-object p0

    .line 2749
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->hasInkreader()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2750
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getInkreader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->setInkreader(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    .line 2752
    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->hasModel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2753
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getModel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->setModel(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    .line 2755
    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->hasCharRescoringFile()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2756
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getCharRescoringFile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->setCharRescoringFile(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    .line 2758
    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->hasClassifierType()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2759
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getClassifierType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->setClassifierType(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    .line 2761
    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->hasNumResults()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2762
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getNumResults()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->setNumResults(I)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    goto :goto_0
.end method

.method public setCharRescoringFile(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2907
    if-nez p1, :cond_0

    .line 2908
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2910
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2911
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    .line 2913
    return-object p0
.end method

.method setCharRescoringFile(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2922
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2923
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->charRescoringFile_:Ljava/lang/Object;

    .line 2925
    return-void
.end method

.method public setClassifierType(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2943
    if-nez p1, :cond_0

    .line 2944
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2946
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2947
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 2949
    return-object p0
.end method

.method setClassifierType(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2958
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2959
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 2961
    return-void
.end method

.method public setInkreader(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2835
    if-nez p1, :cond_0

    .line 2836
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2838
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2839
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 2841
    return-object p0
.end method

.method setInkreader(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2850
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2851
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 2853
    return-void
.end method

.method public setModel(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2871
    if-nez p1, :cond_0

    .line 2872
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2874
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2875
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 2877
    return-object p0
.end method

.method setModel(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2886
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2887
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 2889
    return-void
.end method

.method public setNumResults(I)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2972
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->bitField0_:I

    .line 2973
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->numResults_:I

    .line 2975
    return-object p0
.end method
