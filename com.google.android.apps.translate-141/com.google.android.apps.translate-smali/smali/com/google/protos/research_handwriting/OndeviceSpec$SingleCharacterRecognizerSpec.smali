.class public final Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "OndeviceSpec.java"

# interfaces
.implements Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpecOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/research_handwriting/OndeviceSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SingleCharacterRecognizerSpec"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    }
.end annotation


# static fields
.field public static final CHAR_RESCORING_FILE_FIELD_NUMBER:I = 0x3

.field public static final CLASSIFIER_TYPE_FIELD_NUMBER:I = 0x4

.field public static final INKREADER_FIELD_NUMBER:I = 0x1

.field public static final MODEL_FIELD_NUMBER:I = 0x2

.field public static final NUM_RESULTS_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private charRescoringFile_:Ljava/lang/Object;

.field private classifierType_:Ljava/lang/Object;

.field private inkreader_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private model_:Ljava/lang/Object;

.field private numResults_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2988
    new-instance v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;-><init>(Z)V

    sput-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    .line 2989
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    invoke-direct {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->initFields()V

    .line 2990
    return-void
.end method

.method private constructor <init>(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2364
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 2523
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->memoizedIsInitialized:B

    .line 2552
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->memoizedSerializedSize:I

    .line 2365
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;Lcom/google/protos/research_handwriting/OndeviceSpec$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2359
    invoke-direct {p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;-><init>(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2366
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2523
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->memoizedIsInitialized:B

    .line 2552
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->memoizedSerializedSize:I

    .line 2366
    return-void
.end method

.method static synthetic access$3502(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2359
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->inkreader_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3602(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2359
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->model_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2359
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->charRescoringFile_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3802(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2359
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->classifierType_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3902(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2359
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->numResults_:I

    return p1
.end method

.method static synthetic access$4002(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2359
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    return p1
.end method

.method private getCharRescoringFileBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2463
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->charRescoringFile_:Ljava/lang/Object;

    .line 2464
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2465
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2467
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->charRescoringFile_:Ljava/lang/Object;

    .line 2470
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getClassifierTypeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2495
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->classifierType_:Ljava/lang/Object;

    .line 2496
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2497
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2499
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->classifierType_:Ljava/lang/Object;

    .line 2502
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1

    .prologue
    .line 2370
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    return-object v0
.end method

.method private getInkreaderBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2399
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->inkreader_:Ljava/lang/Object;

    .line 2400
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2401
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2403
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->inkreader_:Ljava/lang/Object;

    .line 2406
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getModelBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2431
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->model_:Ljava/lang/Object;

    .line 2432
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2433
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2435
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->model_:Ljava/lang/Object;

    .line 2438
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 2517
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->inkreader_:Ljava/lang/Object;

    .line 2518
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->model_:Ljava/lang/Object;

    .line 2519
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->charRescoringFile_:Ljava/lang/Object;

    .line 2520
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->classifierType_:Ljava/lang/Object;

    .line 2521
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->numResults_:I

    .line 2522
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2656
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3300()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2659
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2625
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    .line 2626
    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2627
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    .line 2629
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2636
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    .line 2637
    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2638
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    .line 2640
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2592
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2598
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2646
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2652
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2614
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2620
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2603
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2609
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;->access$3200(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCharRescoringFile()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2449
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->charRescoringFile_:Ljava/lang/Object;

    .line 2450
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2451
    check-cast v0, Ljava/lang/String;

    .line 2459
    :goto_0
    return-object v0

    .line 2453
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2455
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2456
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2457
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->charRescoringFile_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2459
    goto :goto_0
.end method

.method public getClassifierType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2481
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->classifierType_:Ljava/lang/Object;

    .line 2482
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2483
    check-cast v0, Ljava/lang/String;

    .line 2491
    :goto_0
    return-object v0

    .line 2485
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2487
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2488
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2489
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->classifierType_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2491
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2359
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;
    .locals 1

    .prologue
    .line 2374
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;

    return-object v0
.end method

.method public getInkreader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2385
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->inkreader_:Ljava/lang/Object;

    .line 2386
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2387
    check-cast v0, Ljava/lang/String;

    .line 2395
    :goto_0
    return-object v0

    .line 2389
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2391
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2392
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2393
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->inkreader_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2395
    goto :goto_0
.end method

.method public getModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2417
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->model_:Ljava/lang/Object;

    .line 2418
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2419
    check-cast v0, Ljava/lang/String;

    .line 2427
    :goto_0
    return-object v0

    .line 2421
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2423
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2424
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2425
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->model_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2427
    goto :goto_0
.end method

.method public getNumResults()I
    .locals 1

    .prologue
    .line 2513
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->numResults_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2554
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->memoizedSerializedSize:I

    .line 2555
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2579
    :goto_0
    return v0

    .line 2557
    :cond_0
    const/4 v0, 0x0

    .line 2558
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2559
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getInkreaderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2562
    :cond_1
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2563
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getModelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2566
    :cond_2
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2567
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getCharRescoringFileBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2570
    :cond_3
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2571
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getClassifierTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2574
    :cond_4
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 2575
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->numResults_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2578
    :cond_5
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasCharRescoringFile()Z
    .locals 2

    .prologue
    .line 2446
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClassifierType()Z
    .locals 2

    .prologue
    .line 2478
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInkreader()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2382
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasModel()Z
    .locals 2

    .prologue
    .line 2414
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNumResults()Z
    .locals 2

    .prologue
    .line 2510
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2525
    iget-byte v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->memoizedIsInitialized:B

    .line 2526
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2529
    :goto_0
    return v0

    .line 2526
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2528
    :cond_1
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2359
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilderForType()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2657
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2359
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->toBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 2661
    invoke-static {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->newBuilder(Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 2586
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2534
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getSerializedSize()I

    .line 2535
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2536
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getInkreaderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2538
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2539
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getModelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2541
    :cond_1
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2542
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getCharRescoringFileBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2544
    :cond_2
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2545
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->getClassifierTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2547
    :cond_3
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2548
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;->numResults_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2550
    :cond_4
    return-void
.end method
