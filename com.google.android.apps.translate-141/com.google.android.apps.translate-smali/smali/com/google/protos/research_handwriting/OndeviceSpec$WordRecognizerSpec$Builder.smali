.class public final Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "OndeviceSpec.java"

# interfaces
.implements Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpecOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;",
        ">;",
        "Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpecOrBuilder;"
    }
.end annotation


# instance fields
.field private beamSize_:I

.field private bitField0_:I

.field private classifierType_:Ljava/lang/Object;

.field private inkreader_:Ljava/lang/Object;

.field private languageModel_:Ljava/lang/Object;

.field private lmOrder_:I

.field private model_:Ljava/lang/Object;

.field private symbols_:Ljava/lang/Object;

.field private weights_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3422
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 3633
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 3669
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 3705
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 3741
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    .line 3777
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    .line 3855
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    .line 3423
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->maybeForceBuilderInitialization()V

    .line 3424
    return-void
.end method

.method static synthetic access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3417
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4200()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3417
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3471
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    .line 3472
    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3473
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 3476
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3429
    new-instance v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    invoke-direct {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;-><init>()V

    return-object v0
.end method

.method private ensureWeightsIsMutable()V
    .locals 2

    .prologue
    .line 3857
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 3858
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    .line 3859
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3861
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 3427
    return-void
.end method


# virtual methods
.method public addAllWeights(Ljava/lang/Iterable;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Float;",
            ">;)",
            "Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;"
        }
    .end annotation

    .prologue
    .line 3887
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->ensureWeightsIsMutable()V

    .line 3888
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 3890
    return-object p0
.end method

.method public addWeights(F)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 2
    .parameter

    .prologue
    .line 3880
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->ensureWeightsIsMutable()V

    .line 3881
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3883
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->build()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 2

    .prologue
    .line 3462
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    .line 3463
    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3464
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 3466
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3480
    new-instance v2, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;-><init>(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;Lcom/google/protos/research_handwriting/OndeviceSpec$1;)V

    .line 3481
    iget v3, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3482
    const/4 v1, 0x0

    .line 3483
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 3486
    :goto_0
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->inkreader_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$4402(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3487
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 3488
    or-int/lit8 v0, v0, 0x2

    .line 3490
    :cond_0
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->model_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$4502(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3491
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 3492
    or-int/lit8 v0, v0, 0x4

    .line 3494
    :cond_1
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->classifierType_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$4602(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3495
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 3496
    or-int/lit8 v0, v0, 0x8

    .line 3498
    :cond_2
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->symbols_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$4702(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3499
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 3500
    or-int/lit8 v0, v0, 0x10

    .line 3502
    :cond_3
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->languageModel_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$4802(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3503
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 3504
    or-int/lit8 v0, v0, 0x20

    .line 3506
    :cond_4
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->lmOrder_:I

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->lmOrder_:I
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$4902(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;I)I

    .line 3507
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 3508
    or-int/lit8 v0, v0, 0x40

    .line 3510
    :cond_5
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->beamSize_:I

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->beamSize_:I
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$5002(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;I)I

    .line 3511
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 3512
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    .line 3513
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3515
    :cond_6
    iget-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$5102(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/util/List;)Ljava/util/List;

    .line 3516
    #setter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$5202(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;I)I

    .line 3517
    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->clear()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->clear()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3433
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 3434
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 3435
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3436
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 3437
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3438
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 3439
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3440
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    .line 3441
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3442
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    .line 3443
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3444
    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->lmOrder_:I

    .line 3445
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3446
    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->beamSize_:I

    .line 3447
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3448
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    .line 3449
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3450
    return-object p0
.end method

.method public clearBeamSize()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3848
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3849
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->beamSize_:I

    .line 3851
    return-object p0
.end method

.method public clearClassifierType()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3729
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3730
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getClassifierType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 3732
    return-object p0
.end method

.method public clearInkreader()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3657
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3658
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getInkreader()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 3660
    return-object p0
.end method

.method public clearLanguageModel()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3801
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3802
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getLanguageModel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    .line 3804
    return-object p0
.end method

.method public clearLmOrder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3827
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3828
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->lmOrder_:I

    .line 3830
    return-object p0
.end method

.method public clearModel()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3693
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3694
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getModel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 3696
    return-object p0
.end method

.method public clearSymbols()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3765
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3766
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getSymbols()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    .line 3768
    return-object p0
.end method

.method public clearWeights()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3893
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    .line 3894
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3896
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 2

    .prologue
    .line 3454
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildPartial()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->clone()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBeamSize()I
    .locals 1

    .prologue
    .line 3839
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->beamSize_:I

    return v0
.end method

.method public getClassifierType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3710
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 3711
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3712
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 3713
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 3716
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3417
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1

    .prologue
    .line 3458
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getInkreader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3638
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 3639
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3640
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 3641
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 3644
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getLanguageModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3782
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    .line 3783
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3784
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 3785
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    .line 3788
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getLmOrder()I
    .locals 1

    .prologue
    .line 3818
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->lmOrder_:I

    return v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3674
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 3675
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3676
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 3677
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 3680
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSymbols()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3746
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    .line 3747
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3748
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 3749
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    .line 3752
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getWeights(I)F
    .locals 1
    .parameter

    .prologue
    .line 3870
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getWeightsCount()I
    .locals 1

    .prologue
    .line 3867
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getWeightsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3864
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasBeamSize()Z
    .locals 2

    .prologue
    .line 3836
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClassifierType()Z
    .locals 2

    .prologue
    .line 3707
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInkreader()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3635
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLanguageModel()Z
    .locals 2

    .prologue
    .line 3779
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLmOrder()Z
    .locals 2

    .prologue
    .line 3815
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasModel()Z
    .locals 2

    .prologue
    .line 3671
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSymbols()Z
    .locals 2

    .prologue
    .line 3743
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 3557
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3417
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3417
    check-cast p1, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    invoke-virtual {p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3417
    invoke-virtual {p0, p1, p2}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3565
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 3566
    sparse-switch v0, :sswitch_data_0

    .line 3571
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3573
    :sswitch_0
    return-object p0

    .line 3578
    :sswitch_1
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3579
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    goto :goto_0

    .line 3583
    :sswitch_2
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3584
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    goto :goto_0

    .line 3588
    :sswitch_3
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3589
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    goto :goto_0

    .line 3593
    :sswitch_4
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3594
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    goto :goto_0

    .line 3598
    :sswitch_5
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3599
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    goto :goto_0

    .line 3603
    :sswitch_6
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3604
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->lmOrder_:I

    goto :goto_0

    .line 3608
    :sswitch_7
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3609
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->beamSize_:I

    goto :goto_0

    .line 3613
    :sswitch_8
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->ensureWeightsIsMutable()V

    .line 3614
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFloat()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3618
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    .line 3619
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v0

    .line 3620
    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v1

    if-lez v1, :cond_1

    .line 3621
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->addWeights(F)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    goto :goto_1

    .line 3623
    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto/16 :goto_0

    .line 3566
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_9
        0x45 -> :sswitch_8
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 2
    .parameter

    .prologue
    .line 3521
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3553
    :cond_0
    :goto_0
    return-object p0

    .line 3522
    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->hasInkreader()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3523
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getInkreader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->setInkreader(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    .line 3525
    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->hasModel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3526
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getModel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->setModel(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    .line 3528
    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->hasClassifierType()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3529
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getClassifierType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->setClassifierType(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    .line 3531
    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->hasSymbols()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3532
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getSymbols()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->setSymbols(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    .line 3534
    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->hasLanguageModel()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3535
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getLanguageModel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->setLanguageModel(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    .line 3537
    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->hasLmOrder()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3538
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getLmOrder()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->setLmOrder(I)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    .line 3540
    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->hasBeamSize()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3541
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getBeamSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->setBeamSize(I)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    .line 3543
    :cond_8
    #getter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$5100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3544
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3545
    #getter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$5100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    .line 3546
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    goto :goto_0

    .line 3548
    :cond_9
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->ensureWeightsIsMutable()V

    .line 3549
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    #getter for: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->access$5100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setBeamSize(I)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3842
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3843
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->beamSize_:I

    .line 3845
    return-object p0
.end method

.method public setClassifierType(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3720
    if-nez p1, :cond_0

    .line 3721
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3723
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3724
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 3726
    return-object p0
.end method

.method setClassifierType(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 3735
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3736
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->classifierType_:Ljava/lang/Object;

    .line 3738
    return-void
.end method

.method public setInkreader(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3648
    if-nez p1, :cond_0

    .line 3649
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3651
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3652
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 3654
    return-object p0
.end method

.method setInkreader(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 3663
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3664
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->inkreader_:Ljava/lang/Object;

    .line 3666
    return-void
.end method

.method public setLanguageModel(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3792
    if-nez p1, :cond_0

    .line 3793
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3795
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3796
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    .line 3798
    return-object p0
.end method

.method setLanguageModel(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 3807
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3808
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->languageModel_:Ljava/lang/Object;

    .line 3810
    return-void
.end method

.method public setLmOrder(I)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3821
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3822
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->lmOrder_:I

    .line 3824
    return-object p0
.end method

.method public setModel(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3684
    if-nez p1, :cond_0

    .line 3685
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3687
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3688
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 3690
    return-object p0
.end method

.method setModel(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 3699
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3700
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->model_:Ljava/lang/Object;

    .line 3702
    return-void
.end method

.method public setSymbols(Ljava/lang/String;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3756
    if-nez p1, :cond_0

    .line 3757
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3759
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3760
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    .line 3762
    return-object p0
.end method

.method setSymbols(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 3771
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->bitField0_:I

    .line 3772
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->symbols_:Ljava/lang/Object;

    .line 3774
    return-void
.end method

.method public setWeights(IF)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 3874
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->ensureWeightsIsMutable()V

    .line 3875
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->weights_:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3877
    return-object p0
.end method
