.class public final Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "OndeviceSpec.java"

# interfaces
.implements Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpecOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/research_handwriting/OndeviceSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WordRecognizerSpec"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    }
.end annotation


# static fields
.field public static final BEAM_SIZE_FIELD_NUMBER:I = 0x7

.field public static final CLASSIFIER_TYPE_FIELD_NUMBER:I = 0x3

.field public static final INKREADER_FIELD_NUMBER:I = 0x1

.field public static final LANGUAGE_MODEL_FIELD_NUMBER:I = 0x5

.field public static final LM_ORDER_FIELD_NUMBER:I = 0x6

.field public static final MODEL_FIELD_NUMBER:I = 0x2

.field public static final SYMBOLS_FIELD_NUMBER:I = 0x4

.field public static final WEIGHTS_FIELD_NUMBER:I = 0x8

.field private static final defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

.field private static final serialVersionUID:J


# instance fields
.field private beamSize_:I

.field private bitField0_:I

.field private classifierType_:Ljava/lang/Object;

.field private inkreader_:Ljava/lang/Object;

.field private languageModel_:Ljava/lang/Object;

.field private lmOrder_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private model_:Ljava/lang/Object;

.field private symbols_:Ljava/lang/Object;

.field private weights_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3903
    new-instance v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;-><init>(Z)V

    sput-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    .line 3904
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    invoke-direct {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->initFields()V

    .line 3905
    return-void
.end method

.method private constructor <init>(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 3036
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 3254
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->memoizedIsInitialized:B

    .line 3292
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->memoizedSerializedSize:I

    .line 3037
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;Lcom/google/protos/research_handwriting/OndeviceSpec$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    invoke-direct {p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;-><init>(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 3038
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3254
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->memoizedIsInitialized:B

    .line 3292
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->memoizedSerializedSize:I

    .line 3038
    return-void
.end method

.method static synthetic access$4402(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->inkreader_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4502(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->model_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4602(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->classifierType_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4702(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->symbols_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4802(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->languageModel_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4902(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->lmOrder_:I

    return p1
.end method

.method static synthetic access$5002(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->beamSize_:I

    return p1
.end method

.method static synthetic access$5100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 3031
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5102(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput-object p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$5202(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3031
    iput p1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    return p1
.end method

.method private getClassifierTypeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3135
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->classifierType_:Ljava/lang/Object;

    .line 3136
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3137
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3139
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->classifierType_:Ljava/lang/Object;

    .line 3142
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1

    .prologue
    .line 3042
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    return-object v0
.end method

.method private getInkreaderBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3071
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->inkreader_:Ljava/lang/Object;

    .line 3072
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3073
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3075
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->inkreader_:Ljava/lang/Object;

    .line 3078
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLanguageModelBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3199
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->languageModel_:Ljava/lang/Object;

    .line 3200
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3201
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3203
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->languageModel_:Ljava/lang/Object;

    .line 3206
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getModelBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3103
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->model_:Ljava/lang/Object;

    .line 3104
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3105
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3107
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->model_:Ljava/lang/Object;

    .line 3110
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSymbolsBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3167
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->symbols_:Ljava/lang/Object;

    .line 3168
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3169
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3171
    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->symbols_:Ljava/lang/Object;

    .line 3174
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3245
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->inkreader_:Ljava/lang/Object;

    .line 3246
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->model_:Ljava/lang/Object;

    .line 3247
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->classifierType_:Ljava/lang/Object;

    .line 3248
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->symbols_:Ljava/lang/Object;

    .line 3249
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->languageModel_:Ljava/lang/Object;

    .line 3250
    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->lmOrder_:I

    .line 3251
    iput v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->beamSize_:I

    .line 3252
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;

    .line 3253
    return-void
.end method

.method public static newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3410
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->create()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4200()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3413
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3379
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    .line 3380
    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3381
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    .line 3383
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3390
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    .line 3391
    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3392
    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    .line 3394
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3346
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3352
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3400
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3406
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3368
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3374
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3357
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3363
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    #calls: Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->buildParsed()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    invoke-static {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;->access$4100(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBeamSize()I
    .locals 1

    .prologue
    .line 3227
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->beamSize_:I

    return v0
.end method

.method public getClassifierType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3121
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->classifierType_:Ljava/lang/Object;

    .line 3122
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3123
    check-cast v0, Ljava/lang/String;

    .line 3131
    :goto_0
    return-object v0

    .line 3125
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3127
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3128
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3129
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->classifierType_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3131
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3031
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;
    .locals 1

    .prologue
    .line 3046
    sget-object v0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->defaultInstance:Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;

    return-object v0
.end method

.method public getInkreader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3057
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->inkreader_:Ljava/lang/Object;

    .line 3058
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3059
    check-cast v0, Ljava/lang/String;

    .line 3067
    :goto_0
    return-object v0

    .line 3061
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3063
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3064
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3065
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->inkreader_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3067
    goto :goto_0
.end method

.method public getLanguageModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3185
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->languageModel_:Ljava/lang/Object;

    .line 3186
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3187
    check-cast v0, Ljava/lang/String;

    .line 3195
    :goto_0
    return-object v0

    .line 3189
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3191
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3192
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3193
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->languageModel_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3195
    goto :goto_0
.end method

.method public getLmOrder()I
    .locals 1

    .prologue
    .line 3217
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->lmOrder_:I

    return v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3089
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->model_:Ljava/lang/Object;

    .line 3090
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3091
    check-cast v0, Ljava/lang/String;

    .line 3099
    :goto_0
    return-object v0

    .line 3093
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3095
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3096
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3097
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->model_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3099
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3294
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->memoizedSerializedSize:I

    .line 3295
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3333
    :goto_0
    return v0

    .line 3297
    :cond_0
    const/4 v0, 0x0

    .line 3298
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 3299
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getInkreaderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3302
    :cond_1
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 3303
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getModelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3306
    :cond_2
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 3307
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getClassifierTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3310
    :cond_3
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 3311
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getSymbolsBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3314
    :cond_4
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 3315
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getLanguageModelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3318
    :cond_5
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 3319
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->lmOrder_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3322
    :cond_6
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 3323
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->beamSize_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3328
    :cond_7
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getWeightsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    .line 3329
    add-int/2addr v0, v1

    .line 3330
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getWeightsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3332
    iput v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getSymbols()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3153
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->symbols_:Ljava/lang/Object;

    .line 3154
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3155
    check-cast v0, Ljava/lang/String;

    .line 3163
    :goto_0
    return-object v0

    .line 3157
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3159
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3160
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3161
    iput-object v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->symbols_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3163
    goto :goto_0
.end method

.method public getWeights(I)F
    .locals 1
    .parameter

    .prologue
    .line 3241
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getWeightsCount()I
    .locals 1

    .prologue
    .line 3238
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getWeightsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3235
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;

    return-object v0
.end method

.method public hasBeamSize()Z
    .locals 2

    .prologue
    .line 3224
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClassifierType()Z
    .locals 2

    .prologue
    .line 3118
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInkreader()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3054
    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLanguageModel()Z
    .locals 2

    .prologue
    .line 3182
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLmOrder()Z
    .locals 2

    .prologue
    .line 3214
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasModel()Z
    .locals 2

    .prologue
    .line 3086
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSymbols()Z
    .locals 2

    .prologue
    .line 3150
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3256
    iget-byte v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->memoizedIsInitialized:B

    .line 3257
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 3260
    :goto_0
    return v0

    .line 3257
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3259
    :cond_1
    iput-byte v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3031
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilderForType()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3411
    invoke-static {}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3031
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->toBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;
    .locals 1

    .prologue
    .line 3415
    invoke-static {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->newBuilder(Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;)Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 3340
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3265
    invoke-virtual {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getSerializedSize()I

    .line 3266
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3267
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getInkreaderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3269
    :cond_0
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3270
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getModelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3272
    :cond_1
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 3273
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getClassifierTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3275
    :cond_2
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 3276
    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getSymbolsBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3278
    :cond_3
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 3279
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->getLanguageModelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3281
    :cond_4
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 3282
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->lmOrder_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3284
    :cond_5
    iget v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 3285
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->beamSize_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3287
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 3288
    iget-object v0, p0, Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;->weights_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 3287
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3290
    :cond_7
    return-void
.end method
