.class public final Lcom/google/protos/research_handwriting/OndeviceSpec;
.super Ljava/lang/Object;
.source "OndeviceSpec.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/research_handwriting/OndeviceSpec$1;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpec;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$WordRecognizerSpecOrBuilder;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpec;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$SingleCharacterRecognizerSpecOrBuilder;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpecOrBuilder;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpec;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpecOrBuilder;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpecs;,
        Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpecsOrBuilder;
    }
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 3912
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static registerAllExtensions(Lcom/google/protobuf/ExtensionRegistryLite;)V
    .locals 0
    .parameter

    .prologue
    .line 10
    return-void
.end method
