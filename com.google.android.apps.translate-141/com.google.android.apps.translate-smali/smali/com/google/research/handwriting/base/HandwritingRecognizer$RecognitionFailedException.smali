.class public Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;
.super Ljava/lang/Exception;
.source "HandwritingRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/research/handwriting/base/HandwritingRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecognitionFailedException"
.end annotation


# instance fields
.field public final e:Ljava/lang/Exception;

.field public final s:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .parameter "s"
    .parameter "e"

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 57
    iput-object p2, p0, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;->e:Ljava/lang/Exception;

    .line 58
    iput-object p1, p0, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;->s:Ljava/lang/String;

    .line 59
    return-void
.end method
