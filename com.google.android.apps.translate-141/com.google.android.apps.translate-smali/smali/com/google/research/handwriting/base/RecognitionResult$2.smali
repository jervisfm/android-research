.class Lcom/google/research/handwriting/base/RecognitionResult$2;
.super Ljava/lang/Object;
.source "RecognitionResult.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/research/handwriting/base/RecognitionResult;->sort()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/research/handwriting/base/ScoredCandidate;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/research/handwriting/base/RecognitionResult;


# direct methods
.method constructor <init>(Lcom/google/research/handwriting/base/RecognitionResult;)V
    .locals 0
    .parameter

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/research/handwriting/base/RecognitionResult$2;->this$0:Lcom/google/research/handwriting/base/RecognitionResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/research/handwriting/base/ScoredCandidate;Lcom/google/research/handwriting/base/ScoredCandidate;)I
    .locals 2
    .parameter "lhs"
    .parameter "rhs"

    .prologue
    .line 92
    iget v0, p2, Lcom/google/research/handwriting/base/ScoredCandidate;->score:F

    iget v1, p1, Lcom/google/research/handwriting/base/ScoredCandidate;->score:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 89
    check-cast p1, Lcom/google/research/handwriting/base/ScoredCandidate;

    .end local p1
    check-cast p2, Lcom/google/research/handwriting/base/ScoredCandidate;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/research/handwriting/base/RecognitionResult$2;->compare(Lcom/google/research/handwriting/base/ScoredCandidate;Lcom/google/research/handwriting/base/ScoredCandidate;)I

    move-result v0

    return v0
.end method
