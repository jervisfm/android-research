.class public Lcom/google/research/handwriting/base/RecognitionResult;
.super Ljava/lang/Object;
.source "RecognitionResult.java"

# interfaces
.implements Ljava/lang/Iterable;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/google/research/handwriting/base/ScoredCandidate;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CANCELED:Lcom/google/research/handwriting/base/RecognitionResult;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/research/handwriting/base/RecognitionResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private debugInfo:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final scoredResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/research/handwriting/base/ScoredCandidate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/google/research/handwriting/base/RecognitionResult$1;

    invoke-direct {v0}, Lcom/google/research/handwriting/base/RecognitionResult$1;-><init>()V

    sput-object v0, Lcom/google/research/handwriting/base/RecognitionResult;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 68
    new-instance v0, Lcom/google/research/handwriting/base/RecognitionResult;

    const-string v1, "cancelled"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/research/handwriting/base/RecognitionResult;-><init>(Ljava/lang/String;Ljava/util/List;)V

    sput-object v0, Lcom/google/research/handwriting/base/RecognitionResult;->CANCELED:Lcom/google/research/handwriting/base/RecognitionResult;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .parameter "in"

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->debugInfo:Ljava/lang/String;

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->id:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->debugInfo:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    .line 38
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    sget-object v1, Lcom/google/research/handwriting/base/ScoredCandidate;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "id"

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->debugInfo:Ljava/lang/String;

    .line 25
    iput-object p1, p0, Lcom/google/research/handwriting/base/RecognitionResult;->id:Ljava/lang/String;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .parameter "id"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/research/handwriting/base/ScoredCandidate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, candidates:Ljava/util/List;,"Ljava/util/List<Lcom/google/research/handwriting/base/ScoredCandidate;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->debugInfo:Ljava/lang/String;

    .line 30
    iput-object p1, p0, Lcom/google/research/handwriting/base/RecognitionResult;->id:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public add(Lcom/google/research/handwriting/base/ScoredCandidate;)V
    .locals 1
    .parameter "result"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public get(I)Lcom/google/research/handwriting/base/ScoredCandidate;
    .locals 1
    .parameter "i"

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/research/handwriting/base/ScoredCandidate;

    return-object v0
.end method

.method public getDebugInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->debugInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->id:Ljava/lang/String;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/research/handwriting/base/ScoredCandidate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public numResult()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public setDebugInfo(Ljava/lang/String;)V
    .locals 0
    .parameter "debugInfo"

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/research/handwriting/base/RecognitionResult;->debugInfo:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public sort()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    new-instance v1, Lcom/google/research/handwriting/base/RecognitionResult$2;

    invoke-direct {v1, p0}, Lcom/google/research/handwriting/base/RecognitionResult$2;-><init>(Lcom/google/research/handwriting/base/RecognitionResult;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 95
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter "out"
    .parameter "flags"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->debugInfo:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 45
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    :goto_0
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->scoredResults:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 50
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/research/handwriting/base/RecognitionResult;->debugInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
