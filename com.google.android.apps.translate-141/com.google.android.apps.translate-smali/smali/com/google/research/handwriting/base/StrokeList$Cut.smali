.class public Lcom/google/research/handwriting/base/StrokeList$Cut;
.super Ljava/lang/Object;
.source "StrokeList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/research/handwriting/base/StrokeList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Cut"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/research/handwriting/base/StrokeList$Cut$CutType;
    }
.end annotation


# instance fields
.field public final cost:F

.field public final point:I

.field public final stroke:I

.field public final type:Lcom/google/research/handwriting/base/StrokeList$Cut$CutType;


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .parameter "stroke"
    .parameter "point"

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput p1, p0, Lcom/google/research/handwriting/base/StrokeList$Cut;->stroke:I

    .line 151
    iput p2, p0, Lcom/google/research/handwriting/base/StrokeList$Cut;->point:I

    .line 152
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/research/handwriting/base/StrokeList$Cut;->cost:F

    .line 153
    sget-object v0, Lcom/google/research/handwriting/base/StrokeList$Cut$CutType;->STROKES:Lcom/google/research/handwriting/base/StrokeList$Cut$CutType;

    iput-object v0, p0, Lcom/google/research/handwriting/base/StrokeList$Cut;->type:Lcom/google/research/handwriting/base/StrokeList$Cut$CutType;

    .line 154
    return-void
.end method

.method public constructor <init>(IIFLcom/google/research/handwriting/base/StrokeList$Cut$CutType;)V
    .locals 0
    .parameter "stroke"
    .parameter "point"
    .parameter "cost"
    .parameter "type"

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput p1, p0, Lcom/google/research/handwriting/base/StrokeList$Cut;->stroke:I

    .line 158
    iput p2, p0, Lcom/google/research/handwriting/base/StrokeList$Cut;->point:I

    .line 159
    iput p3, p0, Lcom/google/research/handwriting/base/StrokeList$Cut;->cost:F

    .line 160
    iput-object p4, p0, Lcom/google/research/handwriting/base/StrokeList$Cut;->type:Lcom/google/research/handwriting/base/StrokeList$Cut$CutType;

    .line 161
    return-void
.end method
