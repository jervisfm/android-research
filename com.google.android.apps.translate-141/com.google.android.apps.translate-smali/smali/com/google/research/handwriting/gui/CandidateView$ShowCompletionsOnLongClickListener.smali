.class Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;
.super Ljava/lang/Object;
.source "CandidateView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/research/handwriting/gui/CandidateView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowCompletionsOnLongClickListener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;
    }
.end annotation


# instance fields
.field private final completions:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final textview:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/google/research/handwriting/gui/CandidateView;


# direct methods
.method public constructor <init>(Lcom/google/research/handwriting/gui/CandidateView;Ljava/util/LinkedList;Landroid/widget/TextView;)V
    .locals 0
    .parameter
    .parameter
    .parameter "tv"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 234
    .local p2, completions:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    iput-object p2, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->completions:Ljava/util/LinkedList;

    .line 236
    iput-object p3, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->textview:Landroid/widget/TextView;

    .line 237
    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 14
    .parameter "v"

    .prologue
    const/4 v13, 0x1

    const/4 v12, -0x2

    const/4 v11, 0x0

    .line 274
    const/4 v8, 0x2

    const-string v9, "ShowCompletionsOnLongClickListener"

    const-string v10, "Long clicked with completions"

    invoke-static {v8, v9, v10}, Lcom/google/research/handwriting/base/LogV;->i(ILjava/lang/String;Ljava/lang/String;)V

    .line 275
    new-instance v6, Landroid/widget/PopupWindow;

    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/google/research/handwriting/gui/CandidateView;->access$100(Lcom/google/research/handwriting/gui/CandidateView;)Landroid/content/Context;

    move-result-object v8

    invoke-direct {v6, v8}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 276
    .local v6, popup:Landroid/widget/PopupWindow;
    new-instance v3, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;

    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/google/research/handwriting/gui/CandidateView;->access$100(Lcom/google/research/handwriting/gui/CandidateView;)Landroid/content/Context;

    move-result-object v8

    invoke-direct {v3, p0, v8}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;-><init>(Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;Landroid/content/Context;)V

    .line 278
    .local v3, completionLayout:Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;
    invoke-virtual {v3, v6}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;->setPopup(Landroid/widget/PopupWindow;)V

    .line 279
    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->textview:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;->setAnchor(Landroid/view/View;)V

    .line 280
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v12, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v8}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 282
    invoke-virtual {v3, v11}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;->setBackgroundColor(I)V

    .line 283
    invoke-virtual {v3, v13}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;->setOrientation(I)V

    .line 284
    invoke-virtual {v6, v13}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 285
    new-instance v8, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$1;

    invoke-direct {v8, p0}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$1;-><init>(Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;)V

    invoke-virtual {v6, v8}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 291
    const/4 v2, 0x0

    .line 292
    .local v2, cCounter:I
    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->textview:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getWidth()I

    move-result v5

    .line 293
    .local v5, minWidth:I
    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->completions:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 294
    .local v1, c:Ljava/lang/String;
    new-instance v0, Landroid/widget/Button;

    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/google/research/handwriting/gui/CandidateView;->access$100(Lcom/google/research/handwriting/gui/CandidateView;)Landroid/content/Context;

    move-result-object v8

    invoke-direct {v0, v8}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 295
    .local v0, b:Landroid/widget/Button;
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 296
    sget v8, Lcom/google/research/handwriting/gui/R$drawable;->btn_candidate:I

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 297
    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mColorCompletionNormal:I
    invoke-static {v8}, Lcom/google/research/handwriting/gui/CandidateView;->access$200(Lcom/google/research/handwriting/gui/CandidateView;)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setTextColor(I)V

    .line 301
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 302
    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mCandidatePadding:I
    invoke-static {v8}, Lcom/google/research/handwriting/gui/CandidateView;->access$300(Lcom/google/research/handwriting/gui/CandidateView;)I

    move-result v8

    iget-object v9, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mCandidatePadding:I
    invoke-static {v9}, Lcom/google/research/handwriting/gui/CandidateView;->access$300(Lcom/google/research/handwriting/gui/CandidateView;)I

    move-result v9

    invoke-virtual {v0, v8, v11, v9, v11}, Landroid/widget/Button;->setPadding(IIII)V

    .line 303
    new-instance v8, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$2;

    invoke-direct {v8, p0, v6}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$2;-><init>(Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;Landroid/widget/PopupWindow;)V

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    invoke-virtual {v3, v0}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;->addView(Landroid/view/View;)V

    .line 311
    add-int/lit8 v2, v2, 0x1

    const/4 v8, 0x3

    if-le v2, v8, :cond_1

    .line 323
    .end local v0           #b:Landroid/widget/Button;
    .end local v1           #c:Ljava/lang/String;
    :cond_0
    invoke-virtual {v6, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 324
    invoke-virtual {v6, v12}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 325
    invoke-virtual {v6, v12}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 326
    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->textview:Landroid/widget/TextView;

    invoke-virtual {v6, v8, v11, v11}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 327
    return v13

    .line 318
    .restart local v0       #b:Landroid/widget/Button;
    .restart local v1       #c:Ljava/lang/String;
    :cond_1
    new-instance v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/google/research/handwriting/gui/CandidateView;->access$100(Lcom/google/research/handwriting/gui/CandidateView;)Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 319
    .local v7, separator:Landroid/widget/TextView;
    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mColorCompletionDivider:I
    invoke-static {v8}, Lcom/google/research/handwriting/gui/CandidateView;->access$500(Lcom/google/research/handwriting/gui/CandidateView;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 320
    iget-object v8, p0, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;->this$0:Lcom/google/research/handwriting/gui/CandidateView;

    #getter for: Lcom/google/research/handwriting/gui/CandidateView;->mDividerWidth:I
    invoke-static {v8}, Lcom/google/research/handwriting/gui/CandidateView;->access$600(Lcom/google/research/handwriting/gui/CandidateView;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setHeight(I)V

    .line 321
    invoke-virtual {v3, v7}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener$LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
