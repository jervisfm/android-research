.class public Lcom/google/research/handwriting/gui/CandidateView;
.super Landroid/widget/LinearLayout;
.source "CandidateView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;
    }
.end annotation


# static fields
.field private static final MAX_SUGGESTIONS:I = 0x20


# instance fields
.field private final mCandidatePadding:I

.field private final mColorCompletionDivider:I

.field private final mColorCompletionNormal:I

.field private final mColorFirstCandidate:I

.field private final mColorIncorrect:I

.field private final mColorNonSelectable:I

.field private final mColorNormal:I

.field private final mColorSelected:I

.field private mCompletion:Z

.field private mCompletions:Lcom/google/research/handwriting/gui/SuggestedWords;

.field private final mContext:Landroid/content/Context;

.field private final mDividerWidth:I

.field private mListener:Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;

.field mScrollView:Landroid/widget/HorizontalScrollView;

.field private mShowCompletionListBelowCandidates:Z

.field private mSuggestions:Lcom/google/research/handwriting/gui/SuggestedWords;

.field private final mWords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private selectedWord:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/16 v9, 0x64

    const/4 v7, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mWords:Ljava/util/ArrayList;

    .line 50
    iput-boolean v7, p0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletion:Z

    .line 54
    sget-object v6, Lcom/google/research/handwriting/gui/SuggestedWords;->EMPTY:Lcom/google/research/handwriting/gui/SuggestedWords;

    iput-object v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mSuggestions:Lcom/google/research/handwriting/gui/SuggestedWords;

    .line 55
    sget-object v6, Lcom/google/research/handwriting/gui/SuggestedWords;->EMPTY:Lcom/google/research/handwriting/gui/SuggestedWords;

    iput-object v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletions:Lcom/google/research/handwriting/gui/SuggestedWords;

    .line 56
    iput-boolean v7, p0, Lcom/google/research/handwriting/gui/CandidateView;->mShowCompletionListBelowCandidates:Z

    .line 67
    iput-object p1, p0, Lcom/google/research/handwriting/gui/CandidateView;->mContext:Landroid/content/Context;

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 69
    .local v3, res:Landroid/content/res/Resources;
    sget v6, Lcom/google/research/handwriting/gui/R$color;->candidate_normal:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorNormal:I

    .line 70
    sget v6, Lcom/google/research/handwriting/gui/R$color;->candidate_selected:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorSelected:I

    .line 71
    sget v6, Lcom/google/research/handwriting/gui/R$color;->candidate_incorrect:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorIncorrect:I

    .line 72
    sget v6, Lcom/google/research/handwriting/gui/R$color;->candidate_not_selectable:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorNonSelectable:I

    .line 73
    sget v6, Lcom/google/research/handwriting/gui/R$color;->candidate_first:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorFirstCandidate:I

    .line 74
    sget v6, Lcom/google/research/handwriting/gui/R$color;->completion_normal:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorCompletionNormal:I

    .line 75
    sget v6, Lcom/google/research/handwriting/gui/R$color;->completion_divider:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorCompletionDivider:I

    .line 76
    sget v6, Lcom/google/research/handwriting/gui/R$dimen;->completion_divider_width:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mDividerWidth:I

    .line 77
    sget v6, Lcom/google/research/handwriting/gui/R$dimen;->candidate_padding:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mCandidatePadding:I

    .line 79
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 80
    .local v2, inflater:Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    const/16 v6, 0x20

    if-ge v1, v6, :cond_1

    .line 81
    sget v6, Lcom/google/research/handwriting/gui/R$layout;->candidate:I

    const/4 v8, 0x0

    invoke-virtual {v2, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 82
    .local v5, v:Landroid/view/View;
    sget v6, Lcom/google/research/handwriting/gui/R$id;->candidate_word:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 86
    .local v4, tv:Landroid/widget/TextView;
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setHeight(I)V

    .line 87
    invoke-virtual {p0, v9}, Lcom/google/research/handwriting/gui/CandidateView;->setMinimumHeight(I)V

    .line 89
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 90
    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    sget v6, Lcom/google/research/handwriting/gui/R$id;->candidate_divider:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 93
    .local v0, divider:Landroid/widget/ImageView;
    if-nez v1, :cond_0

    const/16 v6, 0x8

    :goto_1
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 94
    iget-object v6, p0, Lcom/google/research/handwriting/gui/CandidateView;->mWords:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v6, v7

    .line 93
    goto :goto_1

    .line 96
    .end local v0           #divider:Landroid/widget/ImageView;
    .end local v4           #tv:Landroid/widget/TextView;
    .end local v5           #v:Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/google/research/handwriting/gui/CandidateView;->getScrollY()I

    move-result v6

    invoke-virtual {p0, v7, v6}, Lcom/google/research/handwriting/gui/CandidateView;->scrollTo(II)V

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/google/research/handwriting/gui/CandidateView;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->mShowCompletionListBelowCandidates:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/research/handwriting/gui/CandidateView;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/research/handwriting/gui/CandidateView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 35
    iget v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorCompletionNormal:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/research/handwriting/gui/CandidateView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 35
    iget v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->mCandidatePadding:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/research/handwriting/gui/CandidateView;)Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->mListener:Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/research/handwriting/gui/CandidateView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 35
    iget v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->mColorCompletionDivider:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/research/handwriting/gui/CandidateView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 35
    iget v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->mDividerWidth:I

    return v0
.end method

.method private updateSuggestions(Ljava/lang/String;)V
    .locals 17
    .parameter "selectedWord"

    .prologue
    .line 157
    invoke-virtual/range {p0 .. p0}, Lcom/google/research/handwriting/gui/CandidateView;->clear()V

    .line 160
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 161
    .local v3, completionMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/LinkedList<Ljava/lang/String;>;>;"
    const/4 v6, 0x0

    .local v6, i:I
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletions:Lcom/google/research/handwriting/gui/SuggestedWords;

    invoke-virtual {v13}, Lcom/google/research/handwriting/gui/SuggestedWords;->size()I

    move-result v13

    if-ge v6, v13, :cond_1

    .line 162
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletions:Lcom/google/research/handwriting/gui/SuggestedWords;

    invoke-virtual {v13, v6}, Lcom/google/research/handwriting/gui/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, completion:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletions:Lcom/google/research/handwriting/gui/SuggestedWords;

    invoke-virtual {v13, v6}, Lcom/google/research/handwriting/gui/SuggestedWords;->getExtra(I)Lcom/google/research/handwriting/gui/SuggestedWords$ExtraInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/research/handwriting/gui/SuggestedWords$ExtraInfo;->getInfo()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, base:Ljava/lang/String;
    const/4 v13, 0x2

    const-string v14, "CandidateView.updateSuggestions"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Completion = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " base = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/google/research/handwriting/base/LogV;->i(ILjava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 167
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/LinkedList;

    invoke-virtual {v13, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 161
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 169
    :cond_0
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 170
    .local v8, ll:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Ljava/lang/String;>;"
    invoke-virtual {v8, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 171
    invoke-virtual {v3, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 175
    .end local v1           #base:Ljava/lang/String;
    .end local v2           #completion:Ljava/lang/String;
    .end local v8           #ll:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Ljava/lang/String;>;"
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mSuggestions:Lcom/google/research/handwriting/gui/SuggestedWords;

    invoke-virtual {v13}, Lcom/google/research/handwriting/gui/SuggestedWords;->size()I

    move-result v4

    .line 176
    .local v4, count:I
    const/16 v13, 0x1f

    if-le v4, v13, :cond_2

    .line 177
    const/4 v13, 0x1

    const-string v14, "CandidateView"

    const-string v15, "Got more results than I can display. Cutting to 31."

    invoke-static {v13, v14, v15}, Lcom/google/research/handwriting/base/LogV;->i(ILjava/lang/String;Ljava/lang/String;)V

    .line 179
    const/16 v4, 0x1f

    .line 181
    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-ge v6, v4, :cond_a

    .line 182
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mSuggestions:Lcom/google/research/handwriting/gui/SuggestedWords;

    invoke-virtual {v13, v6}, Lcom/google/research/handwriting/gui/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v12

    .line 183
    .local v12, word:Ljava/lang/CharSequence;
    if-nez v12, :cond_3

    .line 181
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 187
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mWords:Ljava/util/ArrayList;

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/View;

    .line 188
    .local v11, v:Landroid/view/View;
    sget v13, Lcom/google/research/handwriting/gui/R$id;->candidate_word:I

    invoke-virtual {v11, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 189
    .local v10, tv:Landroid/widget/TextView;
    sget v13, Lcom/google/research/handwriting/gui/R$id;->candidate_extra_info:I

    invoke-virtual {v11, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 190
    .local v5, dv:Landroid/widget/TextView;
    const/16 v13, 0x8

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 192
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mColorNormal:I

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 194
    if-nez v6, :cond_4

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletion:Z

    if-nez v13, :cond_4

    .line 195
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mColorFirstCandidate:I

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 198
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 199
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mColorSelected:I

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 204
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mSuggestions:Lcom/google/research/handwriting/gui/SuggestedWords;

    invoke-virtual {v13}, Lcom/google/research/handwriting/gui/SuggestedWords;->clickable()Z

    move-result v13

    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletion:Z

    if-nez v13, :cond_6

    .line 205
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mColorNonSelectable:I

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 208
    :cond_6
    invoke-virtual {v10}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v7

    .line 209
    .local v7, leftPadding:I
    invoke-virtual {v10}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v9

    .line 210
    .local v9, rightPadding:I
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 211
    const/4 v13, 0x1

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 212
    new-instance v14, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/LinkedList;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v13, v10}, Lcom/google/research/handwriting/gui/CandidateView$ShowCompletionsOnLongClickListener;-><init>(Lcom/google/research/handwriting/gui/CandidateView;Ljava/util/LinkedList;Landroid/widget/TextView;)V

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 214
    sget v13, Lcom/google/research/handwriting/gui/R$drawable;->btn_candidate_with_ellipsis:I

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 220
    :goto_5
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v10, v7, v13, v9, v14}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 222
    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mSuggestions:Lcom/google/research/handwriting/gui/SuggestedWords;

    invoke-virtual {v13}, Lcom/google/research/handwriting/gui/SuggestedWords;->clickable()Z

    move-result v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setClickable(Z)V

    .line 224
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/research/handwriting/gui/CandidateView;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 200
    .end local v7           #leftPadding:I
    .end local v9           #rightPadding:I
    :cond_7
    const-string v13, "\u2205[incorrect]"

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_8

    const-string v13, "\u2205[no recognition results]"

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 202
    :cond_8
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/research/handwriting/gui/CandidateView;->mColorIncorrect:I

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4

    .line 216
    .restart local v7       #leftPadding:I
    .restart local v9       #rightPadding:I
    :cond_9
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 217
    sget v13, Lcom/google/research/handwriting/gui/R$drawable;->btn_candidate:I

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_5

    .line 226
    .end local v5           #dv:Landroid/widget/TextView;
    .end local v7           #leftPadding:I
    .end local v9           #rightPadding:I
    .end local v10           #tv:Landroid/widget/TextView;
    .end local v11           #v:Landroid/view/View;
    .end local v12           #word:Ljava/lang/CharSequence;
    :cond_a
    const/4 v13, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/research/handwriting/gui/CandidateView;->getScrollY()I

    move-result v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/google/research/handwriting/gui/CandidateView;->scrollTo(II)V

    .line 227
    invoke-virtual/range {p0 .. p0}, Lcom/google/research/handwriting/gui/CandidateView;->requestLayout()V

    .line 228
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 0

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/research/handwriting/gui/CandidateView;->removeAllViews()V

    .line 336
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 340
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 341
    .local v0, index:I
    iget-object v2, p0, Lcom/google/research/handwriting/gui/CandidateView;->mSuggestions:Lcom/google/research/handwriting/gui/SuggestedWords;

    invoke-virtual {v2, v0}, Lcom/google/research/handwriting/gui/SuggestedWords;->getWord(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 342
    .local v1, word:Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/google/research/handwriting/gui/CandidateView;->mListener:Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;

    iget-boolean v3, p0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletion:Z

    invoke-interface {v2, v0, v1, v3}, Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;->onPickSuggestion(ILjava/lang/CharSequence;Z)V

    .line 343
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 347
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 348
    return-void
.end method

.method public setCompletionListLocationBelowCandidates(Z)V
    .locals 0
    .parameter "showBelow"

    .prologue
    .line 354
    iput-boolean p1, p0, Lcom/google/research/handwriting/gui/CandidateView;->mShowCompletionListBelowCandidates:Z

    .line 355
    return-void
.end method

.method public setListener(Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;Z)V
    .locals 0
    .parameter "listener"
    .parameter "completion"

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/research/handwriting/gui/CandidateView;->mListener:Lcom/google/research/handwriting/gui/CandidateViewHandler$OnPickSuggestionListener;

    .line 133
    iput-boolean p2, p0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletion:Z

    .line 134
    return-void
.end method

.method public setSelectedWord(Ljava/lang/String;)V
    .locals 0
    .parameter "selectedWord"

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/research/handwriting/gui/CandidateView;->selectedWord:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setSuggestions(Lcom/google/research/handwriting/gui/SuggestedWords;Lcom/google/research/handwriting/gui/SuggestedWords;)V
    .locals 1
    .parameter "suggestions"
    .parameter "completions"

    .prologue
    .line 143
    const-string v0, ""

    iput-object v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->selectedWord:Ljava/lang/String;

    .line 144
    iput-object p1, p0, Lcom/google/research/handwriting/gui/CandidateView;->mSuggestions:Lcom/google/research/handwriting/gui/SuggestedWords;

    .line 145
    iput-object p2, p0, Lcom/google/research/handwriting/gui/CandidateView;->mCompletions:Lcom/google/research/handwriting/gui/SuggestedWords;

    .line 146
    iget-object v0, p0, Lcom/google/research/handwriting/gui/CandidateView;->selectedWord:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/research/handwriting/gui/CandidateView;->updateSuggestions(Ljava/lang/String;)V

    .line 147
    return-void
.end method
