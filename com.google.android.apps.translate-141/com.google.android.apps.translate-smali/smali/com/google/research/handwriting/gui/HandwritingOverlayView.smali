.class public Lcom/google/research/handwriting/gui/HandwritingOverlayView;
.super Landroid/widget/RelativeLayout;
.source "HandwritingOverlayView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/research/handwriting/gui/HandwritingOverlayView$1;,
        Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;,
        Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;,
        Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;
    }
.end annotation


# static fields
.field private static final PEN_TOLERANCE:I = 0x3


# instance fields
.field private lastX:F

.field private lastY:F

.field private mBuffer:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private final mClipRect:Landroid/graphics/RectF;

.field private mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

.field private final mMultitouchDetector:Lcom/google/research/handwriting/gui/MultitouchGestureDetector;

.field private final mPaintStroke:Landroid/graphics/Paint;

.field private final mPaintStrokeRecognized:Landroid/graphics/Paint;

.field private final mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

.field private mStrokeColor:I

.field private mStrokeColorRecognized:I

.field private mUndoCopy:Landroid/graphics/Bitmap;

.field private mUsingHistoryEvent:Z

.field private final mUsingMultitouchDetector:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 296
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 297
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 307
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 308
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 319
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 258
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColor:I

    .line 259
    const/16 v0, -0x100

    iput v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColorRecognized:I

    .line 275
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mClipRect:Landroid/graphics/RectF;

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mUsingHistoryEvent:Z

    .line 283
    new-instance v0, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-direct {v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;-><init>()V

    iput-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    .line 285
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mUsingMultitouchDetector:Z

    .line 320
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    .line 321
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStrokeRecognized:Landroid/graphics/Paint;

    .line 323
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/research/handwriting/gui/R$color;->strokeColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColor:I

    .line 324
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/research/handwriting/gui/R$color;->strokeColorRecognized:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColorRecognized:I

    .line 329
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mMultitouchDetector:Lcom/google/research/handwriting/gui/MultitouchGestureDetector;

    .line 332
    invoke-direct {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->initializeDrawingStyles()V

    .line 333
    return-void
.end method

.method private createBuffer()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 371
    invoke-virtual {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->getHeight()I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 372
    .local v0, height:I
    invoke-virtual {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->getWidth()I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 374
    .local v1, width:I
    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v1, :cond_0

    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 377
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    .line 378
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    .line 380
    :cond_1
    return-void
.end method

.method private drawPenDown(FFFLandroid/graphics/Paint;)V
    .locals 5
    .parameter "x"
    .parameter "y"
    .parameter "pressure"
    .parameter "paint"

    .prologue
    .line 630
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 631
    invoke-direct {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->createBuffer()V

    .line 633
    :cond_0
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mClipRect:Landroid/graphics/RectF;

    .line 634
    .local v0, clip:Landroid/graphics/RectF;
    invoke-virtual {v0, p1, p2, p1, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 636
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->resetSmoothing()V

    .line 637
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getMaxRadius()F

    move-result v1

    neg-float v1, v1

    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v2}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getMaxRadius()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 638
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    .line 639
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 640
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v2, p3}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getRadius(F)F

    move-result v2

    invoke-virtual {v1, p1, p2, v2, p4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 641
    iget v1, v0, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, v0, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, 0x1

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->invalidate(IIII)V

    .line 642
    return-void
.end method

.method private drawPenMove(FFFFFLandroid/graphics/Paint;)V
    .locals 8
    .parameter "x"
    .parameter "y"
    .parameter "lastX"
    .parameter "lastY"
    .parameter "pressure"
    .parameter "paint"

    .prologue
    .line 660
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 661
    invoke-direct {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->createBuffer()V

    .line 663
    :cond_0
    iget-object v6, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mClipRect:Landroid/graphics/RectF;

    .line 664
    .local v6, clip:Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    .line 665
    .local v0, canvas:Landroid/graphics/Canvas;
    invoke-static {p1, p3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {p2, p4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {p1, p3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {p2, p4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {v6, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 666
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getMaxRadius()F

    move-result v1

    neg-float v1, v1

    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v2}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getMaxRadius()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {v6, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 667
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v1, p5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getStrokeWidth(F)F

    move-result v7

    .line 668
    .local v7, strokeWidth:F
    invoke-virtual {p6, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 669
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p6, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 670
    sget-object v1, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {v0, v6, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    move v1, p3

    move v2, p4

    move v3, p1

    move v4, p2

    move-object v5, p6

    .line 671
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 672
    iget v1, v6, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, v6, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v3, v6, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, 0x1

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->invalidate(IIII)V

    .line 673
    return-void
.end method

.method private drawPenUp(FFFLandroid/graphics/Paint;)V
    .locals 6
    .parameter "x"
    .parameter "y"
    .parameter "pressure"
    .parameter "paint"

    .prologue
    .line 686
    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 687
    invoke-direct {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->createBuffer()V

    .line 689
    :cond_0
    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mClipRect:Landroid/graphics/RectF;

    .line 690
    .local v1, clip:Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    .line 691
    .local v0, canvas:Landroid/graphics/Canvas;
    invoke-virtual {v1, p1, p2, p1, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 692
    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v2}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getMaxRadius()F

    move-result v2

    neg-float v2, v2

    iget-object v3, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v3}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getMaxRadius()F

    move-result v3

    neg-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 693
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 694
    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v2, p3}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getRadius(F)F

    move-result v2

    invoke-virtual {v0, p1, p2, v2, p4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 695
    iget v2, v1, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v1, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v1, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->invalidate(IIII)V

    .line 696
    return-void
.end method

.method private drawStrokeListImpl(Lcom/google/research/handwriting/base/StrokeList;FFFLandroid/graphics/Paint;Z)V
    .locals 12
    .parameter "strokes"
    .parameter "scale"
    .parameter "offsetX"
    .parameter "offsetY"
    .parameter "paint"
    .parameter "useFixedPressure"

    .prologue
    .line 589
    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, s:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/google/research/handwriting/base/Stroke;>;"
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 590
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/research/handwriting/base/Stroke;

    .line 591
    .local v11, stroke:Lcom/google/research/handwriting/base/Stroke;
    invoke-virtual {v11}, Lcom/google/research/handwriting/base/Stroke;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 593
    .local v9, pIter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/google/research/handwriting/base/Stroke$Point;>;"
    const/4 v4, 0x0

    .line 594
    .local v4, lastX:F
    const/4 v5, 0x0

    .line 595
    .local v5, lastY:F
    const/4 v6, 0x0

    .line 596
    .local v6, lastP:F
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/research/handwriting/base/Stroke$Point;

    .line 598
    .local v8, p:Lcom/google/research/handwriting/base/Stroke$Point;
    iget v1, v8, Lcom/google/research/handwriting/base/Stroke$Point;->x:F

    add-float/2addr v1, p3

    mul-float v4, v1, p2

    .line 599
    iget v1, v8, Lcom/google/research/handwriting/base/Stroke$Point;->y:F

    add-float v1, v1, p4

    mul-float v5, v1, p2

    .line 600
    if-eqz p6, :cond_3

    const/high16 v6, 0x3f80

    .line 601
    :goto_1
    move-object/from16 v0, p5

    invoke-direct {p0, v4, v5, v6, v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawPenDown(FFFLandroid/graphics/Paint;)V

    .line 604
    .end local v8           #p:Lcom/google/research/handwriting/base/Stroke$Point;
    :cond_0
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 605
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/research/handwriting/base/Stroke$Point;

    .line 606
    .restart local v8       #p:Lcom/google/research/handwriting/base/Stroke$Point;
    iget v1, v8, Lcom/google/research/handwriting/base/Stroke$Point;->x:F

    add-float/2addr v1, p3

    mul-float v2, v1, p2

    .line 607
    .local v2, x:F
    iget v1, v8, Lcom/google/research/handwriting/base/Stroke$Point;->y:F

    add-float v1, v1, p4

    mul-float v3, v1, p2

    .line 608
    .local v3, y:F
    if-eqz p6, :cond_4

    const/high16 v6, 0x3f80

    .line 609
    :goto_3
    cmpl-float v1, v2, v4

    if-nez v1, :cond_1

    cmpl-float v1, v3, v5

    if-eqz v1, :cond_2

    :cond_1
    move-object v1, p0

    move-object/from16 v7, p5

    .line 610
    invoke-direct/range {v1 .. v7}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawPenMove(FFFFFLandroid/graphics/Paint;)V

    .line 612
    :cond_2
    move v4, v2

    .line 613
    move v5, v3

    .line 614
    goto :goto_2

    .line 600
    .end local v2           #x:F
    .end local v3           #y:F
    :cond_3
    iget v1, v8, Lcom/google/research/handwriting/base/Stroke$Point;->p:F

    mul-float v6, v1, p2

    goto :goto_1

    .line 608
    .restart local v2       #x:F
    .restart local v3       #y:F
    :cond_4
    iget v1, v8, Lcom/google/research/handwriting/base/Stroke$Point;->p:F

    mul-float v6, v1, p2

    goto :goto_3

    .line 615
    .end local v2           #x:F
    .end local v3           #y:F
    .end local v8           #p:Lcom/google/research/handwriting/base/Stroke$Point;
    :cond_5
    move-object/from16 v0, p5

    invoke-direct {p0, v4, v5, v6, v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawPenUp(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 617
    .end local v4           #lastX:F
    .end local v5           #lastY:F
    .end local v6           #lastP:F
    .end local v9           #pIter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/google/research/handwriting/base/Stroke$Point;>;"
    .end local v11           #stroke:Lcom/google/research/handwriting/base/Stroke;
    :cond_6
    return-void
.end method

.method private initializeDrawingStyles()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 407
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->setWillNotDraw(Z)V

    .line 409
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 410
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 411
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 412
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 413
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 414
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->defaultStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 416
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStrokeRecognized:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 417
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStrokeRecognized:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 418
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStrokeRecognized:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 419
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStrokeRecognized:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 420
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStrokeRecognized:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColorRecognized:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 421
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStrokeRecognized:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->defaultStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 422
    return-void
.end method

.method private processTouchDown(FFJF)V
    .locals 6
    .parameter "x"
    .parameter "y"
    .parameter "time"
    .parameter "pressure"

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    invoke-direct {p0, p1, p2, p5, v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawPenDown(FFFLandroid/graphics/Paint;)V

    .line 485
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;->onPenDown(FFJF)V

    .line 488
    :cond_0
    iput p1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastX:F

    .line 489
    iput p2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastY:F

    .line 490
    return-void
.end method

.method private processTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .parameter "event"

    .prologue
    const/4 v9, 0x1

    .line 431
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 446
    .local v6, action:I
    packed-switch v6, :pswitch_data_0

    .line 467
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 448
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->processTouchDown(FFJF)V

    move v0, v9

    .line 449
    goto :goto_0

    .line 451
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v8

    .line 452
    .local v8, historySize:I
    iget-boolean v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mUsingHistoryEvent:Z

    if-eqz v0, :cond_0

    if-lez v8, :cond_0

    .line 453
    const/4 v7, 0x0

    .local v7, h:I
    :goto_1
    if-ge v7, v8, :cond_0

    .line 454
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v1

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v2

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v3

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->processTouchMove(FFJF)V

    .line 453
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 458
    .end local v7           #h:I
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->processTouchMove(FFJF)V

    move v0, v9

    .line 459
    goto :goto_0

    .line 462
    .end local v8           #historySize:I
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->processTouchUp(FFJF)V

    move v0, v9

    .line 463
    goto :goto_0

    .line 446
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private processTouchMove(FFJF)V
    .locals 7
    .parameter "x"
    .parameter "y"
    .parameter "time"
    .parameter "pressure"

    .prologue
    const/high16 v1, 0x4040

    .line 502
    iget v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastX:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastY:F

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 512
    :goto_0
    return-void

    .line 506
    :cond_0
    iget v3, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastX:F

    iget v4, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastY:F

    iget-object v6, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawPenMove(FFFFFLandroid/graphics/Paint;)V

    .line 507
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    if-eqz v0, :cond_1

    .line 508
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;->onPenMove(FFJF)V

    .line 510
    :cond_1
    iput p1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastX:F

    .line 511
    iput p2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastY:F

    goto :goto_0
.end method


# virtual methods
.method public cancelStroke()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 540
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mUndoCopy:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 541
    .local v3, w:I
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mUndoCopy:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 542
    .local v7, h:I
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 543
    .local v1, pixels:[I
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mUndoCopy:Landroid/graphics/Bitmap;

    move v4, v2

    move v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 544
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    move v4, v2

    move v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 545
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mUndoCopy:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v8, v8, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 546
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v4, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p0, v2, v2, v0, v4}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->invalidate(IIII)V

    .line 547
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    invoke-interface {v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;->cancelStroke()V

    .line 548
    return-void
.end method

.method public clear()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 702
    invoke-direct {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->createBuffer()V

    .line 703
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v3, v2

    iget-object v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v4, v2

    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 704
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 705
    invoke-virtual {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->invalidate()V

    .line 706
    return-void
.end method

.method public clearRegion(FFFF)V
    .locals 6
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    .line 808
    .local v0, canvas:Landroid/graphics/Canvas;
    if-eqz v0, :cond_0

    .line 809
    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 810
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 811
    float-to-int v1, p1

    float-to-int v2, p2

    float-to-int v3, p3

    float-to-int v4, p4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->invalidate(IIII)V

    .line 813
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "event"

    .prologue
    .line 392
    invoke-virtual {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 400
    :goto_0
    return v0

    .line 395
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->processTouchEvent(Landroid/view/MotionEvent;)Z

    .line 397
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 398
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 400
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public drawStrokeList(Lcom/google/research/handwriting/base/StrokeList;FFFLandroid/graphics/Paint;)V
    .locals 7
    .parameter "strokes"
    .parameter "scale"
    .parameter "offsetX"
    .parameter "offsetY"
    .parameter "paint"

    .prologue
    .line 580
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawStrokeListImpl(Lcom/google/research/handwriting/base/StrokeList;FFFLandroid/graphics/Paint;Z)V

    .line 581
    return-void
.end method

.method public drawStrokeList(Lcom/google/research/handwriting/base/StrokeList;Z)V
    .locals 6
    .parameter "strokes"
    .parameter "recognized"

    .prologue
    const/4 v3, 0x0

    .line 560
    const/high16 v2, 0x3f80

    if-eqz p2, :cond_0

    iget-object v5, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStrokeRecognized:Landroid/graphics/Paint;

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawStrokeList(Lcom/google/research/handwriting/base/StrokeList;FFFLandroid/graphics/Paint;)V

    .line 561
    return-void

    .line 560
    :cond_0
    iget-object v5, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method public drawStrokeListWithFixedWidth(Lcom/google/research/handwriting/base/StrokeList;FFFLandroid/graphics/Paint;F)V
    .locals 9
    .parameter "strokes"
    .parameter "scale"
    .parameter "offsetX"
    .parameter "offsetY"
    .parameter "paint"
    .parameter "strokeWidth"

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getMinRadius()F

    move-result v8

    .line 570
    .local v8, savedMinPressure:F
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->getMaxRadius()F

    move-result v7

    .line 571
    .local v7, savedMaxPressure:F
    invoke-virtual {p0, p6}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->setMinStrokeWidth(F)V

    .line 572
    invoke-virtual {p0, p6}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->setMaxStrokeWidth(F)V

    .line 573
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawStrokeListImpl(Lcom/google/research/handwriting/base/StrokeList;FFFLandroid/graphics/Paint;Z)V

    .line 574
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v0, v8}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->setMinRadius(F)V

    .line 575
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    invoke-virtual {v0, v7}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->setMaxRadius(F)V

    .line 576
    return-void
.end method

.method public getStrokeColor()I
    .locals 1

    .prologue
    .line 336
    iget v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColor:I

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 384
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 386
    iput-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    .line 387
    iput-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mCanvas:Landroid/graphics/Canvas;

    .line 388
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .parameter "canvas"

    .prologue
    const/4 v2, 0x0

    .line 360
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 361
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 362
    invoke-direct {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->createBuffer()V

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mBuffer:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 365
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .parameter "state"

    .prologue
    .line 773
    instance-of v1, p1, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 774
    check-cast v0, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;

    .line 775
    .local v0, ss:Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;
    invoke-virtual {v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 776
    iget v1, v0, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;->strokeColor:I

    iput v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColor:I

    .line 777
    iget v1, v0, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;->strokeColorRecognized:I

    iput v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColorRecognized:I

    .line 778
    invoke-direct {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->initializeDrawingStyles()V

    .line 782
    .end local v0           #ss:Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;
    :goto_0
    return-void

    .line 780
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 764
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 765
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;

    invoke-direct {v0, v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 766
    .local v0, ss:Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;
    iget v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColor:I

    iput v2, v0, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;->strokeColor:I

    .line 767
    iget v2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColorRecognized:I

    iput v2, v0, Lcom/google/research/handwriting/gui/HandwritingOverlayView$SavedState;->strokeColorRecognized:I

    .line 768
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    .prologue
    .line 350
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 352
    invoke-direct {p0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->createBuffer()V

    .line 353
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    invoke-interface {v0, p1, p2}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;->onSizeChanged(II)V

    .line 356
    :cond_0
    return-void
.end method

.method public processTouchUp(FFJF)V
    .locals 7
    .parameter "x"
    .parameter "y"
    .parameter "time"
    .parameter "pressure"

    .prologue
    .line 524
    iget v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastX:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastY:F

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_0

    .line 525
    iget v3, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastX:F

    iget v4, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastY:F

    iget-object v6, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawPenMove(FFFFFLandroid/graphics/Paint;)V

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    invoke-direct {p0, p1, p2, p5, v0}, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->drawPenUp(FFFLandroid/graphics/Paint;)V

    .line 528
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    if-eqz v0, :cond_1

    .line 529
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;->onPenUp(FFJF)V

    .line 531
    :cond_1
    iput p1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastX:F

    .line 532
    iput p2, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->lastY:F

    .line 533
    return-void
.end method

.method public setHandwritingOverlayListener(Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 716
    iput-object p1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mListener:Lcom/google/research/handwriting/gui/HandwritingOverlayView$HandwritingOverlayListener;

    .line 717
    return-void
.end method

.method public setMaxStrokeWidth(F)V
    .locals 2
    .parameter "strokeWidth"

    .prologue
    .line 791
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    const/high16 v1, 0x4000

    div-float v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->setMaxRadius(F)V

    .line 792
    return-void
.end method

.method public setMinStrokeWidth(F)V
    .locals 2
    .parameter "strokeWidth"

    .prologue
    .line 795
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPressureNormalizer:Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;

    const/high16 v1, 0x4000

    div-float v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/google/research/handwriting/gui/HandwritingOverlayView$PressureNormalizer;->setMinRadius(F)V

    .line 796
    return-void
.end method

.method public setStrokeColor(I)V
    .locals 2
    .parameter "color"

    .prologue
    .line 340
    iput p1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColor:I

    .line 341
    iget-object v0, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mPaintStroke:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mStrokeColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 342
    return-void
.end method

.method public setUsingHistoryEvent(Z)V
    .locals 0
    .parameter "value"

    .prologue
    .line 345
    iput-boolean p1, p0, Lcom/google/research/handwriting/gui/HandwritingOverlayView;->mUsingHistoryEvent:Z

    .line 346
    return-void
.end method
