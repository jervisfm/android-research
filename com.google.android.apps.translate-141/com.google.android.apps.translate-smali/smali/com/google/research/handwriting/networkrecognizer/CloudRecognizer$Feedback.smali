.class public Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;
.super Ljava/lang/Object;
.source "CloudRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Feedback"
.end annotation


# instance fields
.field final debugInfo:Ljava/lang/String;

.field final label:Ljava/lang/String;

.field final mode:Ljava/lang/String;

.field final resultId:Ljava/lang/String;

.field final strokes:Lcom/google/research/handwriting/base/StrokeList;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/research/handwriting/base/StrokeList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "label"
    .parameter "strokes"
    .parameter "mode"
    .parameter "debugInfo"

    .prologue
    .line 513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    iput-object p1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->label:Ljava/lang/String;

    .line 515
    iput-object p2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    .line 516
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->resultId:Ljava/lang/String;

    .line 517
    iput-object p3, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->mode:Ljava/lang/String;

    .line 518
    iput-object p4, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->debugInfo:Ljava/lang/String;

    .line 519
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "label"
    .parameter "resultId"
    .parameter "mode"
    .parameter "debugInfo"

    .prologue
    .line 521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 522
    iput-object p1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->label:Ljava/lang/String;

    .line 523
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    .line 524
    iput-object p2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->resultId:Ljava/lang/String;

    .line 525
    iput-object p3, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->mode:Ljava/lang/String;

    .line 526
    iput-object p4, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->debugInfo:Ljava/lang/String;

    .line 527
    return-void
.end method


# virtual methods
.method public asJsonObject(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3
    .parameter "language"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 530
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 531
    .local v0, request:Lorg/json/JSONObject;
    const-string v1, "feedback"

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->label:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 532
    const-string v1, "language"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 533
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->debugInfo:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->debugInfo:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 534
    const-string v1, "debug_info"

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->debugInfo:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 536
    :cond_0
    const-string v1, "select_type"

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->mode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 538
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->resultId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 539
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    #calls: Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addWritingGuide(Lorg/json/JSONObject;Lcom/google/research/handwriting/base/StrokeList;)V
    invoke-static {v0, v1}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->access$000(Lorg/json/JSONObject;Lcom/google/research/handwriting/base/StrokeList;)V

    .line 540
    const-string v1, "ink"

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    invoke-virtual {v2}, Lcom/google/research/handwriting/base/StrokeList;->asJsonArray()Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 541
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    invoke-virtual {v1}, Lcom/google/research/handwriting/base/StrokeList;->getPreContext()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    invoke-virtual {v2}, Lcom/google/research/handwriting/base/StrokeList;->getPostContext()Ljava/lang/String;

    move-result-object v2

    #calls: Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addContext(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->access$100(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :goto_0
    return-object v0

    .line 543
    :cond_1
    const-string v1, "ink_hash"

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->resultId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0
.end method
