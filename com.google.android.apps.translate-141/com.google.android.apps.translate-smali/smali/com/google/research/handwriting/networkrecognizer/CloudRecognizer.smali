.class public Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;
.super Ljava/lang/Object;
.source "CloudRecognizer.java"

# interfaces
.implements Lcom/google/research/handwriting/base/HandwritingRecognizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;,
        Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;,
        Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;,
        Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CloudRecognizer"


# instance fields
.field private final feedbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;",
            ">;"
        }
    .end annotation
.end field

.field private final httpClient:Lorg/apache/http/client/HttpClient;

.field private final settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;


# direct methods
.method public constructor <init>(Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpec;)V
    .locals 3
    .parameter "spec"

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->feedbacks:Ljava/util/List;

    .line 77
    invoke-static {}, Lcom/google/research/handwriting/networkrecognizer/HandwritingHttpClient;->getNewHttpClient()Lcom/google/research/handwriting/networkrecognizer/HandwritingHttpClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 78
    new-instance v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-direct {v1}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;-><init>()V

    iput-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    .line 79
    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpec;->getCloudSpec()Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;

    move-result-object v0

    .line 80
    .local v0, cspec:Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpec;->getLanguageCode()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->language:Ljava/lang/String;

    .line 81
    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasServer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getServer()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->server:Ljava/lang/String;

    .line 84
    :cond_0
    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->hasRecoPath()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getRecoPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->recoPath:Ljava/lang/String;

    .line 87
    :cond_1
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getCompressRequests()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->compressRequests:Z

    .line 88
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->deviceName:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getDeviceVersion()I

    move-result v2

    iput v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->deviceVersion:I

    .line 90
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getClientName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->clientName:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getClientVersion()I

    move-result v2

    iput v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->clientVersion:I

    .line 92
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {p1}, Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpec;->getVerbosity()I

    move-result v2

    iput v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->verbosity:I

    .line 93
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getSendFeedbackImmediately()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->sendFeedbackImmediately:Z

    .line 94
    iget-object v1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    invoke-virtual {v0}, Lcom/google/protos/research_handwriting/OndeviceSpec$CloudRecognizerSpec;->getFeedbackBatchSize()I

    move-result v2

    iput v2, v1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->feedbackBatchSize:I

    .line 95
    return-void
.end method

.method public constructor <init>(Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpec;Landroid/content/Context;)V
    .locals 0
    .parameter "spec"
    .parameter "context"

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;-><init>(Lcom/google/protos/research_handwriting/OndeviceSpec$HandwritingRecognizerSpec;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/client/HttpClient;Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;)V
    .locals 1
    .parameter "httpClient"
    .parameter "settings"

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->feedbacks:Ljava/util/List;

    .line 98
    iput-object p1, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 99
    iput-object p2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    .line 100
    return-void
.end method

.method static synthetic access$000(Lorg/json/JSONObject;Lcom/google/research/handwriting/base/StrokeList;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addWritingGuide(Lorg/json/JSONObject;Lcom/google/research/handwriting/base/StrokeList;)V

    return-void
.end method

.method static synthetic access$100(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {p0, p1, p2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addContext(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static addContext(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "request"
    .parameter "preContext"
    .parameter "postContext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 302
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 303
    const-string v0, "pre_context"

    invoke-virtual {p0, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 305
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 306
    const-string v0, "post_context"

    invoke-virtual {p0, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 308
    :cond_1
    return-void
.end method

.method private static addInputType(Lorg/json/JSONObject;I)V
    .locals 1
    .parameter "req"
    .parameter "inputType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 287
    const-string v0, "input_type"

    invoke-virtual {p0, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 288
    return-void
.end method

.method private static addWritingGuide(Lorg/json/JSONObject;Lcom/google/research/handwriting/base/StrokeList;)V
    .locals 3
    .parameter "iRequest"
    .parameter "strokes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 292
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 293
    .local v0, writingGuide:Lorg/json/JSONObject;
    const-string v1, "writing_area_width"

    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getWritingGuideWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 295
    const-string v1, "writing_area_height"

    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getWritingGuideHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 297
    const-string v1, "writing_guide"

    invoke-virtual {p0, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 298
    return-void
.end method

.method private static gzipString(Ljava/lang/String;)[B
    .locals 5
    .parameter "inputString"

    .prologue
    .line 311
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 313
    .local v1, output:Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/OutputStreamWriter;

    new-instance v3, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v3, v1}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    const-string v4, "UTF-8"

    invoke-direct {v2, v3, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 314
    .local v2, writer:Ljava/io/OutputStreamWriter;
    invoke-virtual {v2, p0}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 315
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    .end local v2           #writer:Ljava/io/OutputStreamWriter;
    :goto_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    .line 316
    :catch_0
    move-exception v0

    .line 317
    .local v0, ignored:Ljava/io/IOException;
    const-string v3, "CloudRecognizer"

    const-string v4, "gzipString"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private logVi(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "level"
    .parameter "tag"
    .parameter "message"

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    if-nez v0, :cond_1

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget v0, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->verbosity:I

    if-lt v0, p1, :cond_0

    .line 450
    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private prepareJSONObject()Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 279
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 280
    .local v0, req:Lorg/json/JSONObject;
    const-string v1, "app_version"

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget v2, v2, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->clientVersion:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 281
    const-string v1, "api_level"

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget v2, v2, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->deviceVersion:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 282
    const-string v1, "device"

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v2, v2, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 283
    return-object v0
.end method

.method private static setHttpDebugging()V
    .locals 2

    .prologue
    .line 473
    const-string v0, "org.apache.http.wire"

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 475
    const-string v0, "org.apache.http.headers"

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 478
    const-string v0, "org.apache.commons.logging.Log"

    const-string v1, "org.apache.commons.logging.impl.SimpleLog"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 480
    const-string v0, "org.apache.commons.logging.simplelog.showdatetime"

    const-string v1, "true"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 481
    const-string v0, "org.apache.commons.logging.simplelog.log.httpclient.wire"

    const-string v1, "debug"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 482
    const-string v0, "org.apache.commons.logging.simplelog.log.org.apache.http"

    const-string v1, "debug"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 483
    const-string v0, "org.apache.commons.logging.simplelog.log.org.apache.http.headers"

    const-string v1, "debug"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 484
    return-void
.end method

.method private timing(JILjava/lang/String;)V
    .locals 5
    .parameter "time"
    .parameter "id"
    .parameter "message"

    .prologue
    .line 456
    const/4 v0, 0x3

    const-string v1, "CloudRecognizer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Timing("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, p1

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->logVi(ILjava/lang/String;Ljava/lang/String;)V

    .line 457
    return-void
.end method


# virtual methods
.method public buildFeedbackRequest(Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;)Ljava/lang/String;
    .locals 3
    .parameter "fb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->prepareJSONObject()Lorg/json/JSONObject;

    move-result-object v0

    .line 252
    .local v0, outerRequest:Lorg/json/JSONObject;
    iget-object v2, p1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    if-eqz v2, :cond_0

    .line 253
    iget-object v2, p1, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    invoke-virtual {v2}, Lcom/google/research/handwriting/base/StrokeList;->getInputType()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addInputType(Lorg/json/JSONObject;I)V

    .line 255
    :cond_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 256
    .local v1, requests:Lorg/json/JSONArray;
    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v2, v2, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->language:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->asJsonObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 257
    const-string v2, "requests"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 258
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public buildFeedbackRequest(Ljava/lang/String;Lcom/google/research/handwriting/base/StrokeList;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "label"
    .parameter "strokes"
    .parameter "mode"
    .parameter "debugInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 179
    new-instance v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;-><init>(Ljava/lang/String;Lcom/google/research/handwriting/base/StrokeList;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->buildFeedbackRequest(Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public buildFeedbacksRequest(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 262
    .local p1, fbs:Ljava/util/List;,"Ljava/util/List<Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;>;"
    invoke-direct {p0}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->prepareJSONObject()Lorg/json/JSONObject;

    move-result-object v2

    .line 263
    .local v2, outerRequest:Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 264
    .local v3, requests:Lorg/json/JSONArray;
    const-string v4, "requests"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 265
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;

    .line 269
    .local v0, fb:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget-object v4, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    if-eqz v4, :cond_0

    .line 270
    iget-object v4, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->strokes:Lcom/google/research/handwriting/base/StrokeList;

    invoke-virtual {v4}, Lcom/google/research/handwriting/base/StrokeList;->getInputType()I

    move-result v4

    invoke-static {v2, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addInputType(Lorg/json/JSONObject;I)V

    .line 272
    :cond_0
    iget-object v4, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v4, v4, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->language:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->asJsonObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 274
    .end local v0           #fb:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;
    :cond_1
    iget-object v4, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->feedbacks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 275
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method buildRecognitionRequestString(Lcom/google/research/handwriting/base/StrokeList;)Ljava/lang/String;
    .locals 7
    .parameter "strokes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 228
    invoke-direct {p0}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->prepareJSONObject()Lorg/json/JSONObject;

    move-result-object v3

    .line 229
    .local v3, outerRequest:Lorg/json/JSONObject;
    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getInputType()I

    move-result v5

    invoke-static {v3, v5}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addInputType(Lorg/json/JSONObject;I)V

    .line 230
    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getEnablePreSpace()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 231
    const-string v5, "options"

    const-string v6, "enable_pre_space"

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 234
    :cond_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 235
    .local v2, iRequest:Lorg/json/JSONObject;
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 236
    .local v4, requests:Lorg/json/JSONArray;
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 237
    const-string v5, "requests"

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 238
    invoke-static {v2, p1}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addWritingGuide(Lorg/json/JSONObject;Lcom/google/research/handwriting/base/StrokeList;)V

    .line 239
    const-string v5, "language"

    iget-object v6, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v6, v6, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->language:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 240
    const-string v5, "ink"

    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->asJsonArray()Lorg/json/JSONArray;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 241
    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getPreContext()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getPostContext()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v5, v6}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->addContext(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v5, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->feedbacks:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;

    .line 244
    .local v0, fb:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;
    iget-object v5, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v5, v5, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->language:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;->asJsonObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 246
    .end local v0           #fb:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;
    :cond_1
    iget-object v5, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->feedbacks:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 247
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method executeHttpRequest(Ljava/lang/String;JI)Ljava/lang/String;
    .locals 17
    .parameter "requestString"
    .parameter "creationTime"
    .parameter "requestId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;,
            Lorg/apache/http/client/HttpResponseException;
        }
    .end annotation

    .prologue
    .line 190
    const-string v4, "before creating request"

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->timing(JILjava/lang/String;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v4, v4, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->server:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v5, v5, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->recoPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v6, v6, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->clientName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-boolean v7, v7, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->useTranslateApi:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v8, v8, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->language:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-object v9, v9, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->targetLanguage:Ljava/lang/String;

    invoke-static/range {v4 .. v9}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizerProtocolStrings;->recognitionUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 197
    .local v16, uri:Ljava/lang/String;
    const/4 v4, 0x3

    const-string v5, "CloudRecognizer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "URL = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->logVi(ILjava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v12, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v16

    invoke-direct {v12, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 199
    .local v12, request:Lorg/apache/http/client/methods/HttpPost;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-boolean v4, v4, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->compressRequests:Z

    if-eqz v4, :cond_0

    .line 200
    invoke-static/range {p1 .. p1}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->gzipString(Ljava/lang/String;)[B

    move-result-object v13

    .line 201
    .local v13, requestBytes:[B
    const-string v4, "passed after building request string"

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->timing(JILjava/lang/String;)V

    .line 202
    new-instance v10, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v10, v13}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 203
    .local v10, entity:Lorg/apache/http/entity/ByteArrayEntity;
    const-string v4, "application/octet-stream"

    invoke-virtual {v10, v4}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v12, v10}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 210
    .end local v10           #entity:Lorg/apache/http/entity/ByteArrayEntity;
    .end local v13           #requestBytes:[B
    :goto_0
    const/4 v4, 0x3

    const-string v5, "CloudRecognizer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SENDING to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->logVi(ILjava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v4, "before sending request to server"

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->timing(JILjava/lang/String;)V

    .line 212
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v4, v12}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 213
    .local v11, httpResponse:Lorg/apache/http/HttpResponse;
    const-string v4, "got response from server"

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->timing(JILjava/lang/String;)V

    .line 214
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v15

    .line 215
    .local v15, statusCode:I
    const/4 v14, 0x0

    .line 216
    .local v14, response:Ljava/lang/String;
    const/16 v4, 0xc8

    if-ne v4, v15, :cond_1

    .line 217
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v14

    .line 218
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 223
    const-string v4, "network handling done entirely"

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->timing(JILjava/lang/String;)V

    .line 224
    return-object v14

    .line 206
    .end local v11           #httpResponse:Lorg/apache/http/HttpResponse;
    .end local v14           #response:Ljava/lang/String;
    .end local v15           #statusCode:I
    :cond_0
    new-instance v10, Lorg/apache/http/entity/StringEntity;

    const-string v4, "UTF-8"

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v4}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    .local v10, entity:Lorg/apache/http/entity/StringEntity;
    const-string v4, "application/json"

    invoke-virtual {v10, v4}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 208
    invoke-virtual {v12, v10}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 221
    .end local v10           #entity:Lorg/apache/http/entity/StringEntity;
    .restart local v11       #httpResponse:Lorg/apache/http/HttpResponse;
    .restart local v14       #response:Ljava/lang/String;
    .restart local v15       #statusCode:I
    :cond_1
    new-instance v4, Lorg/apache/http/client/HttpResponseException;

    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v15, v5}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v4
.end method

.method public feedback(Ljava/lang/String;Lcom/google/research/handwriting/base/StrokeList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "label"
    .parameter "strokes"
    .parameter "mode"
    .parameter "debugInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/research/handwriting/base/HandwritingRecognizer$SendingFeedbackFailedException;
        }
    .end annotation

    .prologue
    .line 166
    new-instance v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;-><init>(Ljava/lang/String;Lcom/google/research/handwriting/base/StrokeList;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->recordOrSendFeedback(Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;)V

    .line 167
    return-void
.end method

.method public feedback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "label"
    .parameter "resultId"
    .parameter "mode"
    .parameter "debugInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/research/handwriting/base/HandwritingRecognizer$SendingFeedbackFailedException;
        }
    .end annotation

    .prologue
    .line 172
    new-instance v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->recordOrSendFeedback(Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;)V

    .line 173
    return-void
.end method

.method public getSecondaryRecognizer()Lcom/google/research/handwriting/base/HandwritingRecognizer;
    .locals 1

    .prologue
    .line 499
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSettings()Lcom/google/research/handwriting/base/HandwritingRecognizer$HandwritingRecognizerSettings;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    return-object v0
.end method

.method public hasSecondaryRecognizer()Z
    .locals 1

    .prologue
    .line 488
    const/4 v0, 0x0

    return v0
.end method

.method parseServerResponse(Ljava/lang/String;JLcom/google/research/handwriting/base/StrokeList;)Lcom/google/research/handwriting/base/RecognitionResult;
    .locals 23
    .parameter "serverResponse"
    .parameter "creationTime"
    .parameter "strokes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;
        }
    .end annotation

    .prologue
    .line 326
    if-nez p1, :cond_0

    .line 327
    new-instance v19, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;

    const-string v20, "No answer."

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;-><init>(Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;Ljava/lang/String;)V

    throw v19

    .line 330
    :cond_0
    new-instance v19, Lorg/json/JSONTokener;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/json/JSONArray;

    .line 331
    .local v12, responseArray:Lorg/json/JSONArray;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v19

    const-string v20, "SUCCESS"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 332
    new-instance v19, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;

    const-string v20, "Not SUCCESS."

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;-><init>(Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;Ljava/lang/String;)V

    throw v19

    .line 336
    :cond_1
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v13

    .line 340
    .local v13, responses:Lorg/json/JSONArray;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v11

    .line 353
    .local v11, response:Lorg/json/JSONArray;
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    .line 354
    new-instance v19, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;

    const-string v20, "Invalid response. Less than two entries in response."

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;-><init>(Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;Ljava/lang/String;)V

    throw v19

    .line 357
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->useTranslateApi:Z

    move/from16 v19, v0

    if-eqz v19, :cond_5

    .line 358
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_3

    .line 359
    const/16 v19, 0x1

    const-string v20, "CloudRecognizer"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Strange response. "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " entries."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->logVi(ILjava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_3
    :goto_0
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 370
    .local v10, inkHash:Ljava/lang/String;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v14

    .line 371
    .local v14, resultsArray:Lorg/json/JSONArray;
    const/4 v7, 0x0

    .line 372
    .local v7, completionsArray:Lorg/json/JSONArray;
    const-string v8, ""

    .line 373
    .local v8, debugInfo:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->useTranslateApi:Z

    move/from16 v19, v0

    if-eqz v19, :cond_7

    .line 374
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_6

    .line 375
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v7

    .line 376
    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 408
    :cond_4
    :goto_1
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v19

    if-lez v19, :cond_9

    const-string v19, " IME:"

    :goto_2
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, p2

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "ms"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 411
    new-instance v15, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;

    invoke-direct {v15, v10}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;-><init>(Ljava/lang/String;)V

    .line 412
    .local v15, rr:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;
    invoke-virtual {v15, v8}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;->setDebugInfo(Ljava/lang/String;)V

    .line 413
    const/16 v17, 0x0

    .local v17, w:I
    :goto_3
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v17

    move/from16 v1, v19

    if-ge v0, v1, :cond_b

    .line 414
    new-instance v6, Ljava/util/ArrayList;

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 415
    .local v6, completions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v7, :cond_a

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    move/from16 v1, v17

    if-le v0, v1, :cond_a

    .line 416
    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v5

    .line 417
    .local v5, comp:Lorg/json/JSONArray;
    new-instance v6, Ljava/util/ArrayList;

    .end local v6           #completions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 418
    .restart local v6       #completions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, c:I
    :goto_4
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v4, v0, :cond_a

    .line 419
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 362
    .end local v4           #c:I
    .end local v5           #comp:Lorg/json/JSONArray;
    .end local v6           #completions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v7           #completionsArray:Lorg/json/JSONArray;
    .end local v8           #debugInfo:Ljava/lang/String;
    .end local v10           #inkHash:Ljava/lang/String;
    .end local v14           #resultsArray:Lorg/json/JSONArray;
    .end local v15           #rr:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;
    .end local v17           #w:I
    :cond_5
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_3

    .line 363
    const/16 v19, 0x1

    const-string v20, "CloudRecognizer"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Strange response. "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " entries."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->logVi(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 377
    .restart local v7       #completionsArray:Lorg/json/JSONArray;
    .restart local v8       #debugInfo:Ljava/lang/String;
    .restart local v10       #inkHash:Ljava/lang/String;
    .restart local v14       #resultsArray:Lorg/json/JSONArray;
    :cond_6
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_4

    .line 379
    const/16 v19, 0x2

    :try_start_0
    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto/16 :goto_1

    .line 380
    :catch_0
    move-exception v9

    .line 381
    .local v9, e:Lorg/json/JSONException;
    const/16 v19, 0x1

    const-string v20, "CloudRecognizer"

    const-string v21, "3 entries, but no completions array -> debug_info"

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->logVi(ILjava/lang/String;Ljava/lang/String;)V

    .line 382
    const/4 v7, 0x0

    .line 383
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 384
    goto/16 :goto_1

    .line 388
    .end local v9           #e:Lorg/json/JSONException;
    :cond_7
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_8

    .line 389
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v7

    .line 391
    const/16 v19, 0x3

    :try_start_1
    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v19

    const-string v20, "debug_info"

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 398
    :cond_8
    :goto_5
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 400
    const/16 v19, 0x2

    :try_start_2
    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v7

    goto/16 :goto_1

    .line 392
    :catch_1
    move-exception v9

    .line 393
    .restart local v9       #e:Lorg/json/JSONException;
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    .line 401
    .end local v9           #e:Lorg/json/JSONException;
    :catch_2
    move-exception v9

    .line 402
    .restart local v9       #e:Lorg/json/JSONException;
    const/16 v19, 0x1

    const-string v20, "CloudRecognizer"

    const-string v21, "3 entries, but no completions array -> debug_info"

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->logVi(ILjava/lang/String;Ljava/lang/String;)V

    .line 403
    const/4 v7, 0x0

    .line 404
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 408
    .end local v9           #e:Lorg/json/JSONException;
    :cond_9
    const-string v19, "IME:"

    goto/16 :goto_2

    .line 422
    .restart local v6       #completions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v15       #rr:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;
    .restart local v17       #w:I
    :cond_a
    new-instance v19, Lcom/google/research/handwriting/base/ScoredCandidate;

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v20

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2, v6}, Lcom/google/research/handwriting/base/ScoredCandidate;-><init>(Ljava/lang/String;FLjava/util/List;)V

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;->add(Lcom/google/research/handwriting/base/ScoredCandidate;)V

    .line 413
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3

    .line 425
    .end local v6           #completions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->useTranslateApi:Z

    move/from16 v19, v0

    if-eqz v19, :cond_c

    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_c

    .line 426
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v18

    .line 427
    .local v18, wordTranslationsArray:Lorg/json/JSONArray;
    new-instance v19, Ljava/util/ArrayList;

    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONArray;->length()I

    move-result v20

    invoke-direct/range {v19 .. v20}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v19

    iput-object v0, v15, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;->targetTexts:Ljava/util/List;

    .line 428
    const/16 v17, 0x0

    :goto_6
    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v17

    move/from16 v1, v19

    if-ge v0, v1, :cond_c

    .line 429
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 430
    .local v16, targetText:Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v19

    if-nez v19, :cond_d

    .line 438
    .end local v16           #targetText:Ljava/lang/String;
    .end local v18           #wordTranslationsArray:Lorg/json/JSONArray;
    :cond_c
    return-object v15

    .line 435
    .restart local v16       #targetText:Ljava/lang/String;
    .restart local v18       #wordTranslationsArray:Lorg/json/JSONArray;
    :cond_d
    iget-object v0, v15, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;->targetTexts:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 428
    add-int/lit8 v17, v17, 0x1

    goto :goto_6
.end method

.method public recognize(Lcom/google/research/handwriting/base/StrokeList;Lcom/google/research/handwriting/base/HandwritingRecognizer$CancelStruct;)Lcom/google/research/handwriting/base/RecognitionResult;
    .locals 12
    .parameter "strokes"
    .parameter "cs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 112
    .local v1, creationTime:J
    const/4 v4, 0x0

    .line 114
    .local v4, requestId:I
    const/4 v6, 0x0

    .line 115
    .local v6, response:Ljava/lang/String;
    const/4 v7, 0x0

    .line 117
    .local v7, result:Lcom/google/research/handwriting/base/RecognitionResult;
    :try_start_0
    const-string v9, "before creating JSON"

    invoke-direct {p0, v1, v2, v4, v9}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->timing(JILjava/lang/String;)V

    .line 118
    invoke-virtual {p0, p1}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->buildRecognitionRequestString(Lcom/google/research/handwriting/base/StrokeList;)Ljava/lang/String;

    move-result-object v5

    .line 119
    .local v5, requestString:Ljava/lang/String;
    const-string v9, "JSON created"

    invoke-direct {p0, v1, v2, v4, v9}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->timing(JILjava/lang/String;)V

    .line 120
    invoke-virtual {p0, v5, v1, v2, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->executeHttpRequest(Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v6

    .line 121
    invoke-virtual {p0, v6, v1, v2, p1}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->parseServerResponse(Ljava/lang/String;JLcom/google/research/handwriting/base/StrokeList;)Lcom/google/research/handwriting/base/RecognitionResult;

    move-result-object v7

    .line 122
    iget-object v9, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-boolean v9, v9, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->useTranslateApi:Z

    if-eqz v9, :cond_2

    .line 123
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    .local v8, sourceText:Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getPreContext()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 125
    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getPreContext()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    :cond_0
    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getPostContext()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 128
    invoke-virtual {p1}, Lcom/google/research/handwriting/base/StrokeList;->getPostContext()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_1
    move-object v0, v7

    check-cast v0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;

    move-object v9, v0

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognitionResult;->sourceText:Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException; {:try_start_0 .. :try_end_0} :catch_5

    .line 145
    .end local v8           #sourceText:Ljava/lang/StringBuilder;
    :cond_2
    return-object v7

    .line 132
    .end local v5           #requestString:Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 133
    .local v3, e:Lorg/apache/http/client/HttpResponseException;
    new-instance v9, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;

    const-string v10, "network problems"

    invoke-direct {v9, v10, v3}, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 134
    .end local v3           #e:Lorg/apache/http/client/HttpResponseException;
    :catch_1
    move-exception v3

    .line 135
    .local v3, e:Ljava/io/UnsupportedEncodingException;
    new-instance v9, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;

    const-string v10, "encoding problem"

    invoke-direct {v9, v10, v3}, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 136
    .end local v3           #e:Ljava/io/UnsupportedEncodingException;
    :catch_2
    move-exception v3

    .line 137
    .local v3, e:Lorg/apache/http/client/ClientProtocolException;
    new-instance v9, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;

    const-string v10, "protocol problem"

    invoke-direct {v9, v10, v3}, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 138
    .end local v3           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v3

    .line 139
    .local v3, e:Ljava/io/IOException;
    new-instance v9, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;

    const-string v10, "network io problem"

    invoke-direct {v9, v10, v3}, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 140
    .end local v3           #e:Ljava/io/IOException;
    :catch_4
    move-exception v3

    .line 141
    .local v3, e:Lorg/json/JSONException;
    new-instance v9, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;

    const-string v10, "json problem"

    invoke-direct {v9, v10, v3}, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 142
    .end local v3           #e:Lorg/json/JSONException;
    :catch_5
    move-exception v3

    .line 143
    .local v3, e:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;
    new-instance v9, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "invalid response: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v3, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$InvalidResponseFromServerException;->errorMessage:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Lcom/google/research/handwriting/base/HandwritingRecognizer$RecognitionFailedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9
.end method

.method public recordOrSendFeedback(Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$Feedback;)V
    .locals 6
    .parameter "feedback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/research/handwriting/base/HandwritingRecognizer$SendingFeedbackFailedException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->feedbacks:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget-boolean v2, v2, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->sendFeedbackImmediately:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->feedbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->settings:Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;

    iget v3, v3, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer$CloudRecognizerSettings;->feedbackBatchSize:I

    if-lt v2, v3, :cond_1

    .line 153
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->feedbacks:Ljava/util/List;

    invoke-virtual {p0, v2}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->buildFeedbacksRequest(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->executeHttpRequest(Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, res:Ljava/lang/String;
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/research/handwriting/networkrecognizer/CloudRecognizer;->parseServerResponse(Ljava/lang/String;JLcom/google/research/handwriting/base/StrokeList;)Lcom/google/research/handwriting/base/RecognitionResult;

    .line 156
    const-string v2, "CloudRecognizer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sending feedback succeeded: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v1           #res:Ljava/lang/String;
    :cond_1
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Lcom/google/research/handwriting/base/HandwritingRecognizer$SendingFeedbackFailedException;

    const-string v3, "Sending feedback failed"

    invoke-direct {v2, v3, v0}, Lcom/google/research/handwriting/base/HandwritingRecognizer$SendingFeedbackFailedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method public setSecondaryRecognizer(Lcom/google/research/handwriting/base/HandwritingRecognizer;)V
    .locals 0
    .parameter "recognizer"

    .prologue
    .line 494
    return-void
.end method
