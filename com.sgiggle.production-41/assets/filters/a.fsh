// XproII
precision mediump float;
varying vec2 vTextureCoord;
uniform sampler2D sTexture1;
uniform vec2 uTextureSize;
void main() {
    vec4 c4 = texture2D(sTexture1, vTextureCoord);
    float dx = 1.0 / uTextureSize.x;
    float dy = 1.0 / uTextureSize.y;
    vec2 coordl = vec2(vTextureCoord.x-dx, vTextureCoord.y);
    vec2 coordr = vec2(vTextureCoord.x+dx, vTextureCoord.y);
    vec2 coordt = vec2(vTextureCoord.x, vTextureCoord.y-dy);
    vec2 coordb = vec2(vTextureCoord.x, vTextureCoord.y+dy);
    vec2 coordtl = vec2(vTextureCoord.x-dx, vTextureCoord.y-dy);
    vec2 coordtr = vec2(vTextureCoord.x+dx, vTextureCoord.y-dy);
    vec2 coordbl = vec2(vTextureCoord.x-dx, vTextureCoord.y+dy);
    vec2 coordbr = vec2(vTextureCoord.x+dx, vTextureCoord.y+dy);
    
    vec4 cl = texture2D(sTexture1, coordl);
    vec4 cr = texture2D(sTexture1, coordr);
    vec4 ct = texture2D(sTexture1, coordt);
    vec4 cb = texture2D(sTexture1, coordb);
    vec4 ctl = texture2D(sTexture1, coordtl);
    vec4 ctr = texture2D(sTexture1, coordtr);
    vec4 cbl = texture2D(sTexture1, coordbl);
    vec4 cbr = texture2D(sTexture1, coordbr);
    
    //  c4 = (cl + cr + ct + cb + c4 + ctl + ctr + cbl + cbr) / 9.0;
    //  c4 = (cl + cr + ct + cb + c4) / 5.0;
    
    c4.x = -3.2*c4.x*c4.x*c4.x + 4.8*c4.x*c4.x - 0.6*c4.x + 0.1;
    c4.y = -3.2*c4.y*c4.y*c4.y + 4.8*c4.y*c4.y - 0.6*c4.y + 0.2;
    c4.z = 0.77 * c4.z + 0.31;
    c4 = c4 * sin(1.35 * (vTextureCoord.x + vTextureCoord.y));
    gl_FragColor = c4;
}
