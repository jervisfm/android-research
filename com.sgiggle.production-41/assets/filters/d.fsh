precision mediump float;
varying vec2 vTextureCoord;
uniform sampler2D sTexture1;
uniform vec2 uTextureSize;
void main() {
    vec4 c4 = texture2D(sTexture1, vTextureCoord);
    float dx = 1.0 / uTextureSize.x;
    float dy = 1.0 / uTextureSize.y;
    vec2 coordl = vec2(vTextureCoord.x-dx, vTextureCoord.y);
    vec2 coordr = vec2(vTextureCoord.x+dx, vTextureCoord.y);
    vec2 coordt = vec2(vTextureCoord.x, vTextureCoord.y-dy);
    vec2 coordb = vec2(vTextureCoord.x, vTextureCoord.y+dy);
    vec2 coordtl = vec2(vTextureCoord.x-dx, vTextureCoord.y-dy);
    vec2 coordtr = vec2(vTextureCoord.x+dx, vTextureCoord.y-dy);
    vec2 coordbl = vec2(vTextureCoord.x-dx, vTextureCoord.y+dy);
    vec2 coordbr = vec2(vTextureCoord.x+dx, vTextureCoord.y+dy);
    
    vec4 cl = texture2D(sTexture1, coordl);
    vec4 cr = texture2D(sTexture1, coordr);
    vec4 ct = texture2D(sTexture1, coordt);
    vec4 cb = texture2D(sTexture1, coordb);
    vec4 ctl = texture2D(sTexture1, coordtl);
    vec4 ctr = texture2D(sTexture1, coordtr);
    vec4 cbl = texture2D(sTexture1, coordbl);
    vec4 cbr = texture2D(sTexture1, coordbr);
    
    float wl = 1.0 / (0.001 + distance(cl, c4));
    float wr = 1.0 / (0.001 + distance(cr, c4));
    float wt = 1.0 / (0.001 + distance(ct, c4));
    float wb = 1.0 / (0.001 + distance(cb, c4));
    float wtl = 0.717 / (0.001 + distance(ctl, c4));
    float wtr = 0.717 / (0.001 + distance(ctr, c4));
    float wbl = 0.717 / (0.001 + distance(cbl, c4));
    float wbr = 0.717 / (0.001 + distance(cbr, c4));
    float w = wl + wr + wt + wb + wtl + wtr + wbl + wbr;
    
    c4 = (wl * cl + wr * cr + wt * ct + wb * cb + wtl * ctl + wtr * ctr + wbl * cbl + wbr * cbr) / w * 0.888 + 0.112 * c4;
    
    const vec4 mgray = vec4(0.257, 0.504, 0.098, 1.0);
    
    vec4 gc = c4 * mgray + 0.0625;
    vec4 gl = cl * mgray + 0.0625;
    vec4 gr = cr * mgray + 0.0625;
    vec4 gt = ct * mgray + 0.0625;
    vec4 gb = cb * mgray + 0.0625;
    
    float diff = 
    abs(distance(gc, gl)) +
    abs(distance(gc, gr)) +
    abs(distance(gc, gt)) +
    abs(distance(gc, gb));
    if (diff > 0.09)
        gl_FragColor = vec4(0.3, 0.3, 0.3, 0.7);
    else {
        c4.x = ceil(3.0 * c4.x) / 3.0 - 0.16 + 0.15;
        c4.y = ceil(4.0 * c4.y) / 4.0 - 0.05 + 0.15;
        c4.z = ceil(4.0 * c4.z) / 4.0 - 0.05 + 0.15;
    }
    gl_FragColor = c4;
}
