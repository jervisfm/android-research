package android.support.v4.app;

import android.util.AndroidRuntimeException;

final class SuperNotCalledException extends AndroidRuntimeException
{
  public SuperNotCalledException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     android.support.v4.app.SuperNotCalledException
 * JD-Core Version:    0.6.2
 */