package com.facebook.android;

public abstract class BaseDialogListener
  implements Facebook.DialogListener
{
  public void onCancel()
  {
  }

  public void onError(DialogError paramDialogError)
  {
    paramDialogError.printStackTrace();
  }

  public void onFacebookError(FacebookError paramFacebookError)
  {
    paramFacebookError.printStackTrace();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.facebook.android.BaseDialogListener
 * JD-Core Version:    0.6.2
 */