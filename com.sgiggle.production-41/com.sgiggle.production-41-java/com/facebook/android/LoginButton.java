package com.facebook.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class LoginButton extends ImageButton
{
  protected Activity mActivity;
  private Facebook mFb;
  private Handler mHandler;
  private boolean mIsLogoutAllowed;
  private String[] mPermissions;
  private SessionListener mSessionListener = new SessionListener(null);

  public LoginButton(Context paramContext)
  {
    super(paramContext);
  }

  public LoginButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public LoginButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void init(Activity paramActivity, Facebook paramFacebook, boolean paramBoolean)
  {
    init(paramActivity, paramFacebook, new String[0], paramBoolean);
  }

  public void init(Activity paramActivity, Facebook paramFacebook, String[] paramArrayOfString, boolean paramBoolean)
  {
    this.mActivity = paramActivity;
    this.mFb = paramFacebook;
    this.mPermissions = paramArrayOfString;
    this.mIsLogoutAllowed = paramBoolean;
    this.mHandler = new Handler();
    setBackgroundColor(0);
    setAdjustViewBounds(true);
    if ((paramFacebook.isSessionValid()) && (this.mIsLogoutAllowed));
    for (int i = 2130837640; ; i = 2130837620)
    {
      setBackgroundResource(i);
      drawableStateChanged();
      SessionEvents.addAuthListener(this.mSessionListener);
      if (this.mIsLogoutAllowed)
        SessionEvents.addLogoutListener(this.mSessionListener);
      setOnClickListener(new ButtonOnClickListener());
      return;
    }
  }

  public class ButtonOnClickListener
    implements View.OnClickListener
  {
    public ButtonOnClickListener()
    {
    }

    public void onClick(View paramView)
    {
      if ((LoginButton.this.mFb.isSessionValid()) && (LoginButton.this.mIsLogoutAllowed))
      {
        SessionEvents.onLogoutBegin();
        new AsyncFacebookRunner(LoginButton.this.mFb).logout(LoginButton.this.getContext(), new LoginButton.LogoutRequestListener(LoginButton.this, null));
        return;
      }
      LoginButton.this.mFb.authorize(LoginButton.this.mActivity, LoginButton.this.mPermissions, new LoginButton.LoginDialogListener(LoginButton.this, null));
    }
  }

  private final class LoginDialogListener
    implements Facebook.DialogListener
  {
    private LoginDialogListener()
    {
    }

    public void onCancel()
    {
      SessionEvents.onLoginError("Action Canceled");
    }

    public void onComplete(Bundle paramBundle)
    {
      SessionEvents.onLoginSuccess();
    }

    public void onError(DialogError paramDialogError)
    {
      SessionEvents.onLoginError(paramDialogError.getMessage());
    }

    public void onFacebookError(FacebookError paramFacebookError)
    {
      SessionEvents.onLoginError(paramFacebookError.getMessage());
    }
  }

  private class LogoutRequestListener extends BaseRequestListener
  {
    private LogoutRequestListener()
    {
    }

    public void onComplete(String paramString, Object paramObject)
    {
      LoginButton.this.mHandler.post(new Runnable()
      {
        public void run()
        {
          SessionEvents.onLogoutFinish();
        }
      });
    }
  }

  private class SessionListener
    implements SessionEvents.AuthListener, SessionEvents.LogoutListener
  {
    private SessionListener()
    {
    }

    public void onAuthFail(String paramString)
    {
    }

    public void onAuthSucceed()
    {
      if (LoginButton.this.mIsLogoutAllowed)
        LoginButton.this.setBackgroundResource(2130837640);
      SessionStore.save(LoginButton.this.mFb, LoginButton.this.getContext());
    }

    public void onLogoutBegin()
    {
    }

    public void onLogoutFinish()
    {
      SessionStore.clear(LoginButton.this.getContext());
      LoginButton.this.setBackgroundResource(2130837620);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.facebook.android.LoginButton
 * JD-Core Version:    0.6.2
 */