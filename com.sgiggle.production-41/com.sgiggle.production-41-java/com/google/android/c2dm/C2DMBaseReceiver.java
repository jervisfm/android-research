package com.google.android.c2dm;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import com.sgiggle.util.Log;
import java.io.IOException;

public abstract class C2DMBaseReceiver extends IntentService
{
  private static final String C2DM_INTENT = "com.google.android.c2dm.intent.RECEIVE";
  private static final String C2DM_RETRY = "com.google.android.c2dm.intent.RETRY";
  public static final String ERR_ACCOUNT_MISSING = "ACCOUNT_MISSING";
  public static final String ERR_AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";
  public static final String ERR_INVALID_PARAMETERS = "INVALID_PARAMETERS";
  public static final String ERR_INVALID_SENDER = "INVALID_SENDER";
  public static final String ERR_PHONE_REGISTRATION_ERROR = "PHONE_REGISTRATION_ERROR";
  public static final String ERR_SERVICE_NOT_AVAILABLE = "SERVICE_NOT_AVAILABLE";
  public static final String ERR_TOO_MANY_REGISTRATIONS = "TOO_MANY_REGISTRATIONS";
  public static final String EXTRA_ERROR = "error";
  public static final String EXTRA_REGISTRATION_ID = "registration_id";
  public static final String EXTRA_UNREGISTERED = "unregistered";
  public static final String REGISTRATION_CALLBACK_INTENT = "com.google.android.c2dm.intent.REGISTRATION";
  private static final String TAG = "C2DM";
  private static final String WAKELOCK_KEY = "C2DM_LIB";
  private static PowerManager.WakeLock mWakeLock;
  private final String senderId;

  public C2DMBaseReceiver(String paramString)
  {
    super(paramString);
    this.senderId = paramString;
  }

  private static void acquireWakeLock(Context paramContext)
  {
    if (mWakeLock == null)
      mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "C2DM_LIB");
    mWakeLock.acquire();
  }

  private void handleRegistration(Context paramContext, Intent paramIntent)
  {
    String str1 = paramIntent.getStringExtra("registration_id");
    String str2 = paramIntent.getStringExtra("error");
    String str3 = paramIntent.getStringExtra("unregistered");
    Log.d("C2DM", "dmControl: registrationId = " + str1 + ", error = " + str2 + ", removed = " + str3);
    if (str3 != null)
    {
      C2DMessaging.clearRegistrationId(paramContext);
      onUnregistered(paramContext);
    }
    do
    {
      return;
      if (str2 == null)
        break;
      C2DMessaging.clearRegistrationId(paramContext);
      Log.e("C2DM", "Registration error " + str2);
      onError(paramContext, str2);
    }
    while (!"SERVICE_NOT_AVAILABLE".equals(str2));
    long l = C2DMessaging.getBackoff(paramContext);
    Log.d("C2DM", "Scheduling registration retry, backoff = " + l);
    Intent localIntent = new Intent("com.google.android.c2dm.intent.RETRY");
    localIntent.setClass(paramContext, getClass());
    PendingIntent localPendingIntent = PendingIntent.getService(paramContext, 0, localIntent, 0);
    ((AlarmManager)paramContext.getSystemService("alarm")).set(0, l + System.currentTimeMillis(), localPendingIntent);
    C2DMessaging.setBackoff(paramContext, l * 2L);
    return;
    try
    {
      onRegistered(paramContext, str1);
      C2DMessaging.setRegistrationId(paramContext, str1);
      return;
    }
    catch (IOException localIOException)
    {
      Log.e("C2DM", "Registration error " + localIOException.getMessage());
    }
  }

  private static void releaseWakeLock()
  {
    if (mWakeLock != null)
    {
      mWakeLock.release();
      mWakeLock = null;
    }
  }

  static void runIntentInService(Context paramContext, Intent paramIntent)
  {
    acquireWakeLock(paramContext);
    String str = paramContext.getPackageName() + ".C2dmReceiver";
    paramIntent.setClassName(paramContext, str);
    Log.d("C2DM", "runIntentInService: C2DM received. Passing Intent to service '" + str + "'");
    paramContext.startService(paramIntent);
  }

  public abstract void onError(Context paramContext, String paramString);

  public final void onHandleIntent(Intent paramIntent)
  {
    Log.d("C2DM", "onHandleIntent: intent.getAction() = [" + paramIntent.getAction() + "]");
    while (true)
    {
      Context localContext;
      try
      {
        localContext = getApplicationContext();
        if (paramIntent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION"))
        {
          handleRegistration(localContext, paramIntent);
          return;
        }
        if (paramIntent.getAction().equals("com.google.android.c2dm.intent.RECEIVE"))
        {
          onMessage(localContext, paramIntent);
          continue;
        }
      }
      finally
      {
        releaseWakeLock();
      }
      if (paramIntent.getAction().equals("com.google.android.c2dm.intent.RETRY"))
      {
        acquireWakeLock(localContext);
        C2DMessaging.register(localContext, this.senderId);
      }
    }
  }

  protected abstract void onMessage(Context paramContext, Intent paramIntent);

  public void onRegistered(Context paramContext, String paramString)
    throws IOException
  {
  }

  public void onUnregistered(Context paramContext)
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.google.android.c2dm.C2DMBaseReceiver
 * JD-Core Version:    0.6.2
 */