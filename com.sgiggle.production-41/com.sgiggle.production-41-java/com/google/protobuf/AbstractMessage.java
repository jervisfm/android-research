package com.google.protobuf;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public abstract class AbstractMessage extends AbstractMessageLite
  implements Message
{
  private int memoizedSize = -1;

  protected static int hashBoolean(boolean paramBoolean)
  {
    if (paramBoolean)
      return 1231;
    return 1237;
  }

  protected static int hashEnum(Internal.EnumLite paramEnumLite)
  {
    return paramEnumLite.getNumber();
  }

  protected static int hashEnumList(List<? extends Internal.EnumLite> paramList)
  {
    int i = 1;
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Internal.EnumLite localEnumLite = (Internal.EnumLite)localIterator.next();
      i = i * 31 + hashEnum(localEnumLite);
    }
    return i;
  }

  protected static int hashLong(long paramLong)
  {
    return (int)(paramLong ^ paramLong >>> 32);
  }

  public boolean equals(Object paramObject)
  {
    if (paramObject == this)
      return true;
    if (!(paramObject instanceof Message))
      return false;
    Message localMessage = (Message)paramObject;
    if (getDescriptorForType() != localMessage.getDescriptorForType())
      return false;
    return (getAllFields().equals(localMessage.getAllFields())) && (getUnknownFields().equals(localMessage.getUnknownFields()));
  }

  public int getSerializedSize()
  {
    int i = this.memoizedSize;
    if (i != -1)
      return i;
    int j = 0;
    boolean bool = getDescriptorForType().getOptions().getMessageSetWireFormat();
    Iterator localIterator = getAllFields().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      Descriptors.FieldDescriptor localFieldDescriptor = (Descriptors.FieldDescriptor)localEntry.getKey();
      Object localObject = localEntry.getValue();
      if ((bool) && (localFieldDescriptor.isExtension()) && (localFieldDescriptor.getType() == Descriptors.FieldDescriptor.Type.MESSAGE) && (!localFieldDescriptor.isRepeated()))
        j += CodedOutputStream.computeMessageSetExtensionSize(localFieldDescriptor.getNumber(), (Message)localObject);
      else
        j += FieldSet.computeFieldSize(localFieldDescriptor, localObject);
    }
    UnknownFieldSet localUnknownFieldSet = getUnknownFields();
    if (bool);
    for (int k = j + localUnknownFieldSet.getSerializedSizeAsMessageSet(); ; k = j + localUnknownFieldSet.getSerializedSize())
    {
      this.memoizedSize = k;
      return k;
    }
  }

  public int hashCode()
  {
    (41 * 19);
    return 29 * hashFields(779 + getDescriptorForType().hashCode(), getAllFields()) + getUnknownFields().hashCode();
  }

  protected int hashFields(int paramInt, Map<Descriptors.FieldDescriptor, Object> paramMap)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      Descriptors.FieldDescriptor localFieldDescriptor = (Descriptors.FieldDescriptor)localEntry.getKey();
      Object localObject = localEntry.getValue();
      int i = paramInt * 37 + localFieldDescriptor.getNumber();
      if (localFieldDescriptor.getType() != Descriptors.FieldDescriptor.Type.ENUM)
      {
        paramInt = i * 53 + localObject.hashCode();
      }
      else if (localFieldDescriptor.isRepeated())
      {
        List localList = (List)localObject;
        paramInt = i * 53 + hashEnumList(localList);
      }
      else
      {
        paramInt = i * 53 + hashEnum((Internal.EnumLite)localObject);
      }
    }
    return paramInt;
  }

  public boolean isInitialized()
  {
    Iterator localIterator1 = getDescriptorForType().getFields().iterator();
    while (localIterator1.hasNext())
    {
      Descriptors.FieldDescriptor localFieldDescriptor2 = (Descriptors.FieldDescriptor)localIterator1.next();
      if ((localFieldDescriptor2.isRequired()) && (!hasField(localFieldDescriptor2)))
        return false;
    }
    Iterator localIterator2 = getAllFields().entrySet().iterator();
    while (true)
      if (localIterator2.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator2.next();
        Descriptors.FieldDescriptor localFieldDescriptor1 = (Descriptors.FieldDescriptor)localEntry.getKey();
        if (localFieldDescriptor1.getJavaType() == Descriptors.FieldDescriptor.JavaType.MESSAGE)
        {
          if (localFieldDescriptor1.isRepeated())
          {
            Iterator localIterator3 = ((List)localEntry.getValue()).iterator();
            if (!localIterator3.hasNext())
              continue;
            if (((Message)localIterator3.next()).isInitialized())
              break;
            return false;
          }
          if (!((Message)localEntry.getValue()).isInitialized())
            return false;
        }
      }
    return true;
  }

  public final String toString()
  {
    return TextFormat.printToString(this);
  }

  public void writeTo(CodedOutputStream paramCodedOutputStream)
    throws IOException
  {
    boolean bool = getDescriptorForType().getOptions().getMessageSetWireFormat();
    Iterator localIterator = getAllFields().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      Descriptors.FieldDescriptor localFieldDescriptor = (Descriptors.FieldDescriptor)localEntry.getKey();
      Object localObject = localEntry.getValue();
      if ((bool) && (localFieldDescriptor.isExtension()) && (localFieldDescriptor.getType() == Descriptors.FieldDescriptor.Type.MESSAGE) && (!localFieldDescriptor.isRepeated()))
        paramCodedOutputStream.writeMessageSetExtension(localFieldDescriptor.getNumber(), (Message)localObject);
      else
        FieldSet.writeField(localFieldDescriptor, localObject, paramCodedOutputStream);
    }
    UnknownFieldSet localUnknownFieldSet = getUnknownFields();
    if (bool)
    {
      localUnknownFieldSet.writeAsMessageSetTo(paramCodedOutputStream);
      return;
    }
    localUnknownFieldSet.writeTo(paramCodedOutputStream);
  }

  public static abstract class Builder<BuilderType extends Builder> extends AbstractMessageLite.Builder<BuilderType>
    implements Message.Builder
  {
    private static List<String> findMissingFields(Message paramMessage)
    {
      ArrayList localArrayList = new ArrayList();
      findMissingFields(paramMessage, "", localArrayList);
      return localArrayList;
    }

    private static void findMissingFields(Message paramMessage, String paramString, List<String> paramList)
    {
      Iterator localIterator1 = paramMessage.getDescriptorForType().getFields().iterator();
      while (localIterator1.hasNext())
      {
        Descriptors.FieldDescriptor localFieldDescriptor2 = (Descriptors.FieldDescriptor)localIterator1.next();
        if ((localFieldDescriptor2.isRequired()) && (!paramMessage.hasField(localFieldDescriptor2)))
          paramList.add(paramString + localFieldDescriptor2.getName());
      }
      Iterator localIterator2 = paramMessage.getAllFields().entrySet().iterator();
      while (localIterator2.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator2.next();
        Descriptors.FieldDescriptor localFieldDescriptor1 = (Descriptors.FieldDescriptor)localEntry.getKey();
        Object localObject = localEntry.getValue();
        if (localFieldDescriptor1.getJavaType() == Descriptors.FieldDescriptor.JavaType.MESSAGE)
          if (localFieldDescriptor1.isRepeated())
          {
            int i = 0;
            Iterator localIterator3 = ((List)localObject).iterator();
            while (localIterator3.hasNext())
            {
              Message localMessage = (Message)localIterator3.next();
              int j = i + 1;
              findMissingFields(localMessage, subMessagePrefix(paramString, localFieldDescriptor1, i), paramList);
              i = j;
            }
          }
          else if (paramMessage.hasField(localFieldDescriptor1))
          {
            findMissingFields((Message)localObject, subMessagePrefix(paramString, localFieldDescriptor1, -1), paramList);
          }
      }
    }

    static boolean mergeFieldFrom(CodedInputStream paramCodedInputStream, UnknownFieldSet.Builder paramBuilder, ExtensionRegistryLite paramExtensionRegistryLite, Message.Builder paramBuilder1, int paramInt)
      throws IOException
    {
      Descriptors.Descriptor localDescriptor = paramBuilder1.getDescriptorForType();
      if ((localDescriptor.getOptions().getMessageSetWireFormat()) && (paramInt == WireFormat.MESSAGE_SET_ITEM_TAG))
      {
        mergeMessageSetExtensionFromCodedStream(paramCodedInputStream, paramBuilder, paramExtensionRegistryLite, paramBuilder1);
        return true;
      }
      int i = WireFormat.getTagWireType(paramInt);
      int j = WireFormat.getTagFieldNumber(paramInt);
      Message localMessage = null;
      ExtensionRegistry.ExtensionInfo localExtensionInfo;
      Descriptors.FieldDescriptor localFieldDescriptor;
      int k;
      int m;
      if (localDescriptor.isExtensionNumber(j))
        if ((paramExtensionRegistryLite instanceof ExtensionRegistry))
        {
          localExtensionInfo = ((ExtensionRegistry)paramExtensionRegistryLite).findExtensionByNumber(localDescriptor, j);
          if (localExtensionInfo == null)
          {
            localFieldDescriptor = null;
            k = 0;
            if (localFieldDescriptor != null)
              break label200;
            m = 1;
          }
        }
      while (true)
      {
        if (m == 0)
          break label263;
        return paramBuilder.mergeFieldFrom(paramInt, paramCodedInputStream);
        localFieldDescriptor = localExtensionInfo.descriptor;
        localMessage = localExtensionInfo.defaultInstance;
        if ((localMessage != null) || (localFieldDescriptor.getJavaType() != Descriptors.FieldDescriptor.JavaType.MESSAGE))
          break;
        throw new IllegalStateException("Message-typed extension lacked default instance: " + localFieldDescriptor.getFullName());
        localMessage = null;
        localFieldDescriptor = null;
        break;
        localFieldDescriptor = localDescriptor.findFieldByNumber(j);
        localMessage = null;
        break;
        label200: if (i == FieldSet.getWireFormatForFieldType(localFieldDescriptor.getLiteType(), false))
        {
          k = 0;
          m = 0;
        }
        else if ((localFieldDescriptor.isPackable()) && (i == FieldSet.getWireFormatForFieldType(localFieldDescriptor.getLiteType(), true)))
        {
          k = 1;
          m = 0;
        }
        else
        {
          m = 1;
          k = 0;
        }
      }
      label263: if (k != 0)
      {
        int i1 = paramCodedInputStream.pushLimit(paramCodedInputStream.readRawVarint32());
        if (localFieldDescriptor.getLiteType() == WireFormat.FieldType.ENUM)
          while (paramCodedInputStream.getBytesUntilLimit() > 0)
          {
            int i2 = paramCodedInputStream.readEnum();
            Descriptors.EnumValueDescriptor localEnumValueDescriptor = localFieldDescriptor.getEnumType().findValueByNumber(i2);
            if (localEnumValueDescriptor == null)
              return true;
            paramBuilder1.addRepeatedField(localFieldDescriptor, localEnumValueDescriptor);
          }
        while (paramCodedInputStream.getBytesUntilLimit() > 0)
        {
          Object localObject2 = FieldSet.readPrimitiveField(paramCodedInputStream, localFieldDescriptor.getLiteType());
          paramBuilder1.addRepeatedField(localFieldDescriptor, localObject2);
        }
        paramCodedInputStream.popLimit(i1);
      }
      while (true)
      {
        return true;
        Object localObject1;
        switch (AbstractMessage.1.$SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[localFieldDescriptor.getType().ordinal()])
        {
        default:
          localObject1 = FieldSet.readPrimitiveField(paramCodedInputStream, localFieldDescriptor.getLiteType());
        case 1:
        case 2:
        case 3:
        }
        while (true)
          if (localFieldDescriptor.isRepeated())
          {
            paramBuilder1.addRepeatedField(localFieldDescriptor, localObject1);
            break;
            if (localMessage != null);
            for (Message.Builder localBuilder2 = localMessage.newBuilderForType(); ; localBuilder2 = paramBuilder1.newBuilderForField(localFieldDescriptor))
            {
              if (!localFieldDescriptor.isRepeated())
                localBuilder2.mergeFrom((Message)paramBuilder1.getField(localFieldDescriptor));
              paramCodedInputStream.readGroup(localFieldDescriptor.getNumber(), localBuilder2, paramExtensionRegistryLite);
              localObject1 = localBuilder2.build();
              break;
            }
            if (localMessage != null);
            for (Message.Builder localBuilder1 = localMessage.newBuilderForType(); ; localBuilder1 = paramBuilder1.newBuilderForField(localFieldDescriptor))
            {
              if (!localFieldDescriptor.isRepeated())
                localBuilder1.mergeFrom((Message)paramBuilder1.getField(localFieldDescriptor));
              paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
              localObject1 = localBuilder1.build();
              break;
            }
            int n = paramCodedInputStream.readEnum();
            localObject1 = localFieldDescriptor.getEnumType().findValueByNumber(n);
            if (localObject1 == null)
            {
              paramBuilder.mergeVarintField(j, n);
              return true;
            }
          }
        paramBuilder1.setField(localFieldDescriptor, localObject1);
      }
    }

    private static void mergeMessageSetExtensionFromCodedStream(CodedInputStream paramCodedInputStream, UnknownFieldSet.Builder paramBuilder, ExtensionRegistryLite paramExtensionRegistryLite, Message.Builder paramBuilder1)
      throws IOException
    {
      Descriptors.Descriptor localDescriptor = paramBuilder1.getDescriptorForType();
      int i = 0;
      ByteString localByteString = null;
      Message.Builder localBuilder = null;
      Descriptors.FieldDescriptor localFieldDescriptor = null;
      int j = paramCodedInputStream.readTag();
      if (j == 0);
      while (true)
      {
        paramCodedInputStream.checkLastTagWas(WireFormat.MESSAGE_SET_ITEM_END_TAG);
        if (localBuilder != null)
          paramBuilder1.setField(localFieldDescriptor, localBuilder.build());
        return;
        if (j == WireFormat.MESSAGE_SET_TYPE_ID_TAG)
        {
          i = paramCodedInputStream.readUInt32();
          if (i == 0)
            break;
          if ((paramExtensionRegistryLite instanceof ExtensionRegistry));
          for (ExtensionRegistry.ExtensionInfo localExtensionInfo = ((ExtensionRegistry)paramExtensionRegistryLite).findExtensionByNumber(localDescriptor, i); ; localExtensionInfo = null)
          {
            if (localExtensionInfo == null)
              break label184;
            localFieldDescriptor = localExtensionInfo.descriptor;
            localBuilder = localExtensionInfo.defaultInstance.newBuilderForType();
            Message localMessage = (Message)paramBuilder1.getField(localFieldDescriptor);
            if (localMessage != null)
              localBuilder.mergeFrom(localMessage);
            if (localByteString == null)
              break;
            localBuilder.mergeFrom(CodedInputStream.newInstance(localByteString.newInput()));
            localByteString = null;
            break;
          }
          label184: if (localByteString == null)
            break;
          paramBuilder.mergeField(i, UnknownFieldSet.Field.newBuilder().addLengthDelimited(localByteString).build());
          localByteString = null;
          break;
        }
        if (j == WireFormat.MESSAGE_SET_MESSAGE_TAG)
        {
          if (i == 0)
          {
            localByteString = paramCodedInputStream.readBytes();
            break;
          }
          if (localBuilder == null)
          {
            paramBuilder.mergeField(i, UnknownFieldSet.Field.newBuilder().addLengthDelimited(paramCodedInputStream.readBytes()).build());
            break;
          }
          paramCodedInputStream.readMessage(localBuilder, paramExtensionRegistryLite);
          break;
        }
        if (paramCodedInputStream.skipField(j))
          break;
      }
    }

    protected static UninitializedMessageException newUninitializedMessageException(Message paramMessage)
    {
      return new UninitializedMessageException(findMissingFields(paramMessage));
    }

    private static String subMessagePrefix(String paramString, Descriptors.FieldDescriptor paramFieldDescriptor, int paramInt)
    {
      StringBuilder localStringBuilder = new StringBuilder(paramString);
      if (paramFieldDescriptor.isExtension())
        localStringBuilder.append('(').append(paramFieldDescriptor.getFullName()).append(')');
      while (true)
      {
        if (paramInt != -1)
          localStringBuilder.append('[').append(paramInt).append(']');
        localStringBuilder.append('.');
        return localStringBuilder.toString();
        localStringBuilder.append(paramFieldDescriptor.getName());
      }
    }

    public BuilderType clear()
    {
      Iterator localIterator = getAllFields().entrySet().iterator();
      while (localIterator.hasNext())
        clearField((Descriptors.FieldDescriptor)((Map.Entry)localIterator.next()).getKey());
      return this;
    }

    public abstract BuilderType clone();

    public boolean mergeDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return super.mergeDelimitedFrom(paramInputStream);
    }

    public boolean mergeDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return super.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public BuilderType mergeFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (Builder)super.mergeFrom(paramByteString);
    }

    public BuilderType mergeFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Builder)super.mergeFrom(paramByteString, paramExtensionRegistryLite);
    }

    public BuilderType mergeFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return mergeFrom(paramCodedInputStream, ExtensionRegistry.getEmptyRegistry());
    }

    public BuilderType mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
      int i = paramCodedInputStream.readTag();
      if (i == 0);
      while (true)
      {
        setUnknownFields(localBuilder.build());
        return this;
        if (mergeFieldFrom(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, this, i))
          break;
      }
    }

    public BuilderType mergeFrom(Message paramMessage)
    {
      if (paramMessage.getDescriptorForType() != getDescriptorForType())
        throw new IllegalArgumentException("mergeFrom(Message) can only merge messages of the same type.");
      Iterator localIterator1 = paramMessage.getAllFields().entrySet().iterator();
      while (localIterator1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator1.next();
        Descriptors.FieldDescriptor localFieldDescriptor = (Descriptors.FieldDescriptor)localEntry.getKey();
        if (localFieldDescriptor.isRepeated())
        {
          Iterator localIterator2 = ((List)localEntry.getValue()).iterator();
          while (localIterator2.hasNext())
            addRepeatedField(localFieldDescriptor, localIterator2.next());
        }
        else if (localFieldDescriptor.getJavaType() == Descriptors.FieldDescriptor.JavaType.MESSAGE)
        {
          Message localMessage = (Message)getField(localFieldDescriptor);
          if (localMessage == localMessage.getDefaultInstanceForType())
            setField(localFieldDescriptor, localEntry.getValue());
          else
            setField(localFieldDescriptor, localMessage.newBuilderForType().mergeFrom(localMessage).mergeFrom((Message)localEntry.getValue()).build());
        }
        else
        {
          setField(localFieldDescriptor, localEntry.getValue());
        }
      }
      mergeUnknownFields(paramMessage.getUnknownFields());
      return this;
    }

    public BuilderType mergeFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Builder)super.mergeFrom(paramInputStream);
    }

    public BuilderType mergeFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Builder)super.mergeFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public BuilderType mergeFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (Builder)super.mergeFrom(paramArrayOfByte);
    }

    public BuilderType mergeFrom(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws InvalidProtocolBufferException
    {
      return (Builder)super.mergeFrom(paramArrayOfByte, paramInt1, paramInt2);
    }

    public BuilderType mergeFrom(byte[] paramArrayOfByte, int paramInt1, int paramInt2, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Builder)super.mergeFrom(paramArrayOfByte, paramInt1, paramInt2, paramExtensionRegistryLite);
    }

    public BuilderType mergeFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Builder)super.mergeFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public BuilderType mergeUnknownFields(UnknownFieldSet paramUnknownFieldSet)
    {
      setUnknownFields(UnknownFieldSet.newBuilder(getUnknownFields()).mergeFrom(paramUnknownFieldSet).build());
      return this;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.google.protobuf.AbstractMessage
 * JD-Core Version:    0.6.2
 */