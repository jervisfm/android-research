package com.google.protobuf;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class DescriptorProtos
{
  private static Descriptors.FileDescriptor descriptor;
  private static Descriptors.Descriptor internal_static_google_protobuf_DescriptorProto_ExtensionRange_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_DescriptorProto_ExtensionRange_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_DescriptorProto_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_DescriptorProto_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_EnumDescriptorProto_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_EnumDescriptorProto_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_EnumOptions_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_EnumOptions_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_EnumValueDescriptorProto_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_EnumValueDescriptorProto_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_EnumValueOptions_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_EnumValueOptions_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_FieldDescriptorProto_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_FieldDescriptorProto_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_FieldOptions_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_FieldOptions_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_FileDescriptorProto_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_FileDescriptorProto_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_FileDescriptorSet_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_FileDescriptorSet_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_FileOptions_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_FileOptions_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_MessageOptions_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_MessageOptions_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_MethodDescriptorProto_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_MethodDescriptorProto_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_MethodOptions_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_MethodOptions_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_ServiceDescriptorProto_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_ServiceDescriptorProto_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_ServiceOptions_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_ServiceOptions_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_SourceCodeInfo_Location_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_SourceCodeInfo_Location_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_SourceCodeInfo_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_SourceCodeInfo_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_UninterpretedOption_NamePart_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_UninterpretedOption_NamePart_fieldAccessorTable;
  private static Descriptors.Descriptor internal_static_google_protobuf_UninterpretedOption_descriptor;
  private static GeneratedMessage.FieldAccessorTable internal_static_google_protobuf_UninterpretedOption_fieldAccessorTable;

  static
  {
    String[] arrayOfString = { "\n google/protobuf/descriptor.proto\022\017google.protobuf\"G\n\021FileDescriptorSet\0222\n\004file\030\001 \003(\0132$.google.protobuf.FileDescriptorProto\"\003\n\023FileDescriptorProto\022\f\n\004name\030\001 \001(\t\022\017\n\007package\030\002 \001(\t\022\022\n\ndependency\030\003 \003(\t\0226\n\fmessage_type\030\004 \003(\0132 .google.protobuf.DescriptorProto\0227\n\tenum_type\030\005 \003(\0132$.google.protobuf.EnumDescriptorProto\0228\n\007service\030\006 \003(\0132'.google.protobuf.ServiceDescriptorProto\0228\n\textension\030\007 \003(\0132%.google.p", "rotobuf.FieldDescriptorProto\022-\n\007options\030\b \001(\0132\034.google.protobuf.FileOptions\0229\n\020source_code_info\030\t \001(\0132\037.google.protobuf.SourceCodeInfo\"©\003\n\017DescriptorProto\022\f\n\004name\030\001 \001(\t\0224\n\005field\030\002 \003(\0132%.google.protobuf.FieldDescriptorProto\0228\n\textension\030\006 \003(\0132%.google.protobuf.FieldDescriptorProto\0225\n\013nested_type\030\003 \003(\0132 .google.protobuf.DescriptorProto\0227\n\tenum_type\030\004 \003(\0132$.google.protobuf.EnumDescriptorProto\022H\n\017exte", "nsion_range\030\005 \003(\0132/.google.protobuf.DescriptorProto.ExtensionRange\0220\n\007options\030\007 \001(\0132\037.google.protobuf.MessageOptions\032,\n\016ExtensionRange\022\r\n\005start\030\001 \001(\005\022\013\n\003end\030\002 \001(\005\"\005\n\024FieldDescriptorProto\022\f\n\004name\030\001 \001(\t\022\016\n\006number\030\003 \001(\005\022:\n\005label\030\004 \001(\0162+.google.protobuf.FieldDescriptorProto.Label\0228\n\004type\030\005 \001(\0162*.google.protobuf.FieldDescriptorProto.Type\022\021\n\ttype_name\030\006 \001(\t\022\020\n\bextendee\030\002 \001(\t\022\025\n\rdefault_value\030\007 \001(\t\022.\n\007o", "ptions\030\b \001(\0132\035.google.protobuf.FieldOptions\"¶\002\n\004Type\022\017\n\013TYPE_DOUBLE\020\001\022\016\n\nTYPE_FLOAT\020\002\022\016\n\nTYPE_INT64\020\003\022\017\n\013TYPE_UINT64\020\004\022\016\n\nTYPE_INT32\020\005\022\020\n\fTYPE_FIXED64\020\006\022\020\n\fTYPE_FIXED32\020\007\022\r\n\tTYPE_BOOL\020\b\022\017\n\013TYPE_STRING\020\t\022\016\n\nTYPE_GROUP\020\n\022\020\n\fTYPE_MESSAGE\020\013\022\016\n\nTYPE_BYTES\020\f\022\017\n\013TYPE_UINT32\020\r\022\r\n\tTYPE_ENUM\020\016\022\021\n\rTYPE_SFIXED32\020\017\022\021\n\rTYPE_SFIXED64\020\020\022\017\n\013TYPE_SINT32\020\021\022\017\n\013TYPE_SINT64\020\022\"C\n\005Label\022\022\n\016LABEL_OPTIONAL\020\001\022\022\n\016LABEL_REQUI", "RED\020\002\022\022\n\016LABEL_REPEATED\020\003\"\001\n\023EnumDescriptorProto\022\f\n\004name\030\001 \001(\t\0228\n\005value\030\002 \003(\0132).google.protobuf.EnumValueDescriptorProto\022-\n\007options\030\003 \001(\0132\034.google.protobuf.EnumOptions\"l\n\030EnumValueDescriptorProto\022\f\n\004name\030\001 \001(\t\022\016\n\006number\030\002 \001(\005\0222\n\007options\030\003 \001(\0132!.google.protobuf.EnumValueOptions\"\001\n\026ServiceDescriptorProto\022\f\n\004name\030\001 \001(\t\0226\n\006method\030\002 \003(\0132&.google.protobuf.MethodDescriptorProto\0220\n\007options\030\003 \001(\0132\037.googl", "e.protobuf.ServiceOptions\"\n\025MethodDescriptorProto\022\f\n\004name\030\001 \001(\t\022\022\n\ninput_type\030\002 \001(\t\022\023\n\013output_type\030\003 \001(\t\022/\n\007options\030\004 \001(\0132\036.google.protobuf.MethodOptions\"Õ\003\n\013FileOptions\022\024\n\fjava_package\030\001 \001(\t\022\034\n\024java_outer_classname\030\b \001(\t\022\"\n\023java_multiple_files\030\n \001(\b:\005false\022,\n\035java_generate_equals_and_hash\030\024 \001(\b:\005false\022F\n\foptimize_for\030\t \001(\0162).google.protobuf.FileOptions.OptimizeMode:\005SPEED\022\"\n\023cc_generic_services\030", "\020 \001(\b:\005false\022$\n\025java_generic_services\030\021 \001(\b:\005false\022\"\n\023py_generic_services\030\022 \001(\b:\005false\022C\n\024uninterpreted_option\030ç\007 \003(\0132$.google.protobuf.UninterpretedOption\":\n\fOptimizeMode\022\t\n\005SPEED\020\001\022\r\n\tCODE_SIZE\020\002\022\020\n\fLITE_RUNTIME\020\003*\t\bè\007\020\002\"¸\001\n\016MessageOptions\022&\n\027message_set_wire_format\030\001 \001(\b:\005false\022.\n\037no_standard_descriptor_accessor\030\002 \001(\b:\005false\022C\n\024uninterpreted_option\030ç\007 \003(\0132$.google.protobuf.UninterpretedOpti", "", "Options\022C\n\024uninterpreted_option\030ç\007 \003(\0132$.google.protobuf.UninterpretedOption*\t\bè\007\020\002\"`\n\016ServiceOptions\022C\n\024uninterpreted_option\030ç\007 \003(\0132$.google.protobuf.UninterpretedOption*\t\bè\007\020\002\"_\n\rMethodOptions\022C\n\024uninterpreted_option\030ç\007 \003(\0132$.google.protobuf.UninterpretedOption*\t\bè\007\020\002\"\002\n\023UninterpretedOption\022;\n\004name\030\002 \003(\0132-.google.protobuf.UninterpretedOption.NamePart\022\030\n\020identifier_value\030\003 \001(\t\022\032\n\022pos", "itive_int_value\030\004 \001(\004\022\032\n\022negative_int_value\030\005 \001(\003\022\024\n\fdouble_value\030\006 \001(\001\022\024\n\fstring_value\030\007 \001(\f\022\027\n\017aggregate_value\030\b \001(\t\0323\n\bNamePart\022\021\n\tname_part\030\001 \002(\t\022\024\n\fis_extension\030\002 \002(\b\"|\n\016SourceCodeInfo\022:\n\blocation\030\001 \003(\0132(.google.protobuf.SourceCodeInfo.Location\032.\n\bLocation\022\020\n\004path\030\001 \003(\005B\002\020\001\022\020\n\004span\030\002 \003(\005B\002\020\001B)\n\023com.google.protobufB\020DescriptorProtosH\001" };
    Descriptors.FileDescriptor.InternalDescriptorAssigner local1 = new Descriptors.FileDescriptor.InternalDescriptorAssigner()
    {
      public ExtensionRegistry assignDescriptors(Descriptors.FileDescriptor paramAnonymousFileDescriptor)
      {
        DescriptorProtos.access$20602(paramAnonymousFileDescriptor);
        DescriptorProtos.access$002((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(0));
        DescriptorProtos.access$102(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_FileDescriptorSet_descriptor, new String[] { "File" }, DescriptorProtos.FileDescriptorSet.class, DescriptorProtos.FileDescriptorSet.Builder.class));
        DescriptorProtos.access$702((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(1));
        DescriptorProtos.access$802(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_FileDescriptorProto_descriptor, new String[] { "Name", "Package", "Dependency", "MessageType", "EnumType", "Service", "Extension", "Options", "SourceCodeInfo" }, DescriptorProtos.FileDescriptorProto.class, DescriptorProtos.FileDescriptorProto.Builder.class));
        DescriptorProtos.access$2302((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(2));
        DescriptorProtos.access$2402(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_DescriptorProto_descriptor, new String[] { "Name", "Field", "Extension", "NestedType", "EnumType", "ExtensionRange", "Options" }, DescriptorProtos.DescriptorProto.class, DescriptorProtos.DescriptorProto.Builder.class));
        DescriptorProtos.access$2502((Descriptors.Descriptor)DescriptorProtos.internal_static_google_protobuf_DescriptorProto_descriptor.getNestedTypes().get(0));
        DescriptorProtos.access$2602(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_DescriptorProto_ExtensionRange_descriptor, new String[] { "Start", "End" }, DescriptorProtos.DescriptorProto.ExtensionRange.class, DescriptorProtos.DescriptorProto.ExtensionRange.Builder.class));
        DescriptorProtos.access$4602((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(3));
        DescriptorProtos.access$4702(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_FieldDescriptorProto_descriptor, new String[] { "Name", "Number", "Label", "Type", "TypeName", "Extendee", "DefaultValue", "Options" }, DescriptorProtos.FieldDescriptorProto.class, DescriptorProtos.FieldDescriptorProto.Builder.class));
        DescriptorProtos.access$6102((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(4));
        DescriptorProtos.access$6202(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_EnumDescriptorProto_descriptor, new String[] { "Name", "Value", "Options" }, DescriptorProtos.EnumDescriptorProto.class, DescriptorProtos.EnumDescriptorProto.Builder.class));
        DescriptorProtos.access$7102((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(5));
        DescriptorProtos.access$7202(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_EnumValueDescriptorProto_descriptor, new String[] { "Name", "Number", "Options" }, DescriptorProtos.EnumValueDescriptorProto.class, DescriptorProtos.EnumValueDescriptorProto.Builder.class));
        DescriptorProtos.access$8102((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(6));
        DescriptorProtos.access$8202(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_ServiceDescriptorProto_descriptor, new String[] { "Name", "Method", "Options" }, DescriptorProtos.ServiceDescriptorProto.class, DescriptorProtos.ServiceDescriptorProto.Builder.class));
        DescriptorProtos.access$9102((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(7));
        DescriptorProtos.access$9202(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_MethodDescriptorProto_descriptor, new String[] { "Name", "InputType", "OutputType", "Options" }, DescriptorProtos.MethodDescriptorProto.class, DescriptorProtos.MethodDescriptorProto.Builder.class));
        DescriptorProtos.access$10202((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(8));
        DescriptorProtos.access$10302(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_FileOptions_descriptor, new String[] { "JavaPackage", "JavaOuterClassname", "JavaMultipleFiles", "JavaGenerateEqualsAndHash", "OptimizeFor", "CcGenericServices", "JavaGenericServices", "PyGenericServices", "UninterpretedOption" }, DescriptorProtos.FileOptions.class, DescriptorProtos.FileOptions.Builder.class));
        DescriptorProtos.access$11802((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(9));
        DescriptorProtos.access$11902(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_MessageOptions_descriptor, new String[] { "MessageSetWireFormat", "NoStandardDescriptorAccessor", "UninterpretedOption" }, DescriptorProtos.MessageOptions.class, DescriptorProtos.MessageOptions.Builder.class));
        DescriptorProtos.access$12802((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(10));
        DescriptorProtos.access$12902(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_FieldOptions_descriptor, new String[] { "Ctype", "Packed", "Deprecated", "ExperimentalMapKey", "UninterpretedOption" }, DescriptorProtos.FieldOptions.class, DescriptorProtos.FieldOptions.Builder.class));
        DescriptorProtos.access$14002((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(11));
        DescriptorProtos.access$14102(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_EnumOptions_descriptor, new String[] { "UninterpretedOption" }, DescriptorProtos.EnumOptions.class, DescriptorProtos.EnumOptions.Builder.class));
        DescriptorProtos.access$14702((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(12));
        DescriptorProtos.access$14802(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_EnumValueOptions_descriptor, new String[] { "UninterpretedOption" }, DescriptorProtos.EnumValueOptions.class, DescriptorProtos.EnumValueOptions.Builder.class));
        DescriptorProtos.access$15402((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(13));
        DescriptorProtos.access$15502(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_ServiceOptions_descriptor, new String[] { "UninterpretedOption" }, DescriptorProtos.ServiceOptions.class, DescriptorProtos.ServiceOptions.Builder.class));
        DescriptorProtos.access$16102((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(14));
        DescriptorProtos.access$16202(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_MethodOptions_descriptor, new String[] { "UninterpretedOption" }, DescriptorProtos.MethodOptions.class, DescriptorProtos.MethodOptions.Builder.class));
        DescriptorProtos.access$16802((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(15));
        DescriptorProtos.access$16902(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_descriptor, new String[] { "Name", "IdentifierValue", "PositiveIntValue", "NegativeIntValue", "DoubleValue", "StringValue", "AggregateValue" }, DescriptorProtos.UninterpretedOption.class, DescriptorProtos.UninterpretedOption.Builder.class));
        DescriptorProtos.access$17002((Descriptors.Descriptor)DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_descriptor.getNestedTypes().get(0));
        DescriptorProtos.access$17102(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_NamePart_descriptor, new String[] { "NamePart", "IsExtension" }, DescriptorProtos.UninterpretedOption.NamePart.class, DescriptorProtos.UninterpretedOption.NamePart.Builder.class));
        DescriptorProtos.access$19102((Descriptors.Descriptor)DescriptorProtos.getDescriptor().getMessageTypes().get(16));
        DescriptorProtos.access$19202(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_descriptor, new String[] { "Location" }, DescriptorProtos.SourceCodeInfo.class, DescriptorProtos.SourceCodeInfo.Builder.class));
        DescriptorProtos.access$19302((Descriptors.Descriptor)DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_descriptor.getNestedTypes().get(0));
        DescriptorProtos.access$19402(new GeneratedMessage.FieldAccessorTable(DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_Location_descriptor, new String[] { "Path", "Span" }, DescriptorProtos.SourceCodeInfo.Location.class, DescriptorProtos.SourceCodeInfo.Location.Builder.class));
        return null;
      }
    };
    Descriptors.FileDescriptor.internalBuildGeneratedFileFrom(arrayOfString, new Descriptors.FileDescriptor[0], local1);
  }

  public static Descriptors.FileDescriptor getDescriptor()
  {
    return descriptor;
  }

  public static void registerAllExtensions(ExtensionRegistry paramExtensionRegistry)
  {
  }

  public static final class DescriptorProto extends GeneratedMessage
    implements DescriptorProtos.DescriptorProtoOrBuilder
  {
    public static final int ENUM_TYPE_FIELD_NUMBER = 4;
    public static final int EXTENSION_FIELD_NUMBER = 6;
    public static final int EXTENSION_RANGE_FIELD_NUMBER = 5;
    public static final int FIELD_FIELD_NUMBER = 2;
    public static final int NAME_FIELD_NUMBER = 1;
    public static final int NESTED_TYPE_FIELD_NUMBER = 3;
    public static final int OPTIONS_FIELD_NUMBER = 7;
    private static final DescriptorProto defaultInstance = new DescriptorProto(true);
    private int bitField0_;
    private List<DescriptorProtos.EnumDescriptorProto> enumType_;
    private List<ExtensionRange> extensionRange_;
    private List<DescriptorProtos.FieldDescriptorProto> extension_;
    private List<DescriptorProtos.FieldDescriptorProto> field_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object name_;
    private List<DescriptorProto> nestedType_;
    private DescriptorProtos.MessageOptions options_;

    static
    {
      defaultInstance.initFields();
    }

    private DescriptorProto(Builder paramBuilder)
    {
      super();
    }

    private DescriptorProto(boolean paramBoolean)
    {
    }

    public static DescriptorProto getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_DescriptorProto_descriptor;
    }

    private ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.name_ = "";
      this.field_ = Collections.emptyList();
      this.extension_ = Collections.emptyList();
      this.nestedType_ = Collections.emptyList();
      this.enumType_ = Collections.emptyList();
      this.extensionRange_ = Collections.emptyList();
      this.options_ = DescriptorProtos.MessageOptions.getDefaultInstance();
    }

    public static Builder newBuilder()
    {
      return Builder.access$3500();
    }

    public static Builder newBuilder(DescriptorProto paramDescriptorProto)
    {
      return newBuilder().mergeFrom(paramDescriptorProto);
    }

    public static DescriptorProto parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static DescriptorProto parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static DescriptorProto parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static DescriptorProto parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static DescriptorProto parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static DescriptorProto parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static DescriptorProto parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static DescriptorProto parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static DescriptorProto parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static DescriptorProto parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public DescriptorProto getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public DescriptorProtos.EnumDescriptorProto getEnumType(int paramInt)
    {
      return (DescriptorProtos.EnumDescriptorProto)this.enumType_.get(paramInt);
    }

    public int getEnumTypeCount()
    {
      return this.enumType_.size();
    }

    public List<DescriptorProtos.EnumDescriptorProto> getEnumTypeList()
    {
      return this.enumType_;
    }

    public DescriptorProtos.EnumDescriptorProtoOrBuilder getEnumTypeOrBuilder(int paramInt)
    {
      return (DescriptorProtos.EnumDescriptorProtoOrBuilder)this.enumType_.get(paramInt);
    }

    public List<? extends DescriptorProtos.EnumDescriptorProtoOrBuilder> getEnumTypeOrBuilderList()
    {
      return this.enumType_;
    }

    public DescriptorProtos.FieldDescriptorProto getExtension(int paramInt)
    {
      return (DescriptorProtos.FieldDescriptorProto)this.extension_.get(paramInt);
    }

    public int getExtensionCount()
    {
      return this.extension_.size();
    }

    public List<DescriptorProtos.FieldDescriptorProto> getExtensionList()
    {
      return this.extension_;
    }

    public DescriptorProtos.FieldDescriptorProtoOrBuilder getExtensionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.extension_.get(paramInt);
    }

    public List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getExtensionOrBuilderList()
    {
      return this.extension_;
    }

    public ExtensionRange getExtensionRange(int paramInt)
    {
      return (ExtensionRange)this.extensionRange_.get(paramInt);
    }

    public int getExtensionRangeCount()
    {
      return this.extensionRange_.size();
    }

    public List<ExtensionRange> getExtensionRangeList()
    {
      return this.extensionRange_;
    }

    public ExtensionRangeOrBuilder getExtensionRangeOrBuilder(int paramInt)
    {
      return (ExtensionRangeOrBuilder)this.extensionRange_.get(paramInt);
    }

    public List<? extends ExtensionRangeOrBuilder> getExtensionRangeOrBuilderList()
    {
      return this.extensionRange_;
    }

    public DescriptorProtos.FieldDescriptorProto getField(int paramInt)
    {
      return (DescriptorProtos.FieldDescriptorProto)this.field_.get(paramInt);
    }

    public int getFieldCount()
    {
      return this.field_.size();
    }

    public List<DescriptorProtos.FieldDescriptorProto> getFieldList()
    {
      return this.field_;
    }

    public DescriptorProtos.FieldDescriptorProtoOrBuilder getFieldOrBuilder(int paramInt)
    {
      return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.field_.get(paramInt);
    }

    public List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getFieldOrBuilderList()
    {
      return this.field_;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.name_ = str;
      return str;
    }

    public DescriptorProto getNestedType(int paramInt)
    {
      return (DescriptorProto)this.nestedType_.get(paramInt);
    }

    public int getNestedTypeCount()
    {
      return this.nestedType_.size();
    }

    public List<DescriptorProto> getNestedTypeList()
    {
      return this.nestedType_;
    }

    public DescriptorProtos.DescriptorProtoOrBuilder getNestedTypeOrBuilder(int paramInt)
    {
      return (DescriptorProtos.DescriptorProtoOrBuilder)this.nestedType_.get(paramInt);
    }

    public List<? extends DescriptorProtos.DescriptorProtoOrBuilder> getNestedTypeOrBuilderList()
    {
      return this.nestedType_;
    }

    public DescriptorProtos.MessageOptions getOptions()
    {
      return this.options_;
    }

    public DescriptorProtos.MessageOptionsOrBuilder getOptionsOrBuilder()
    {
      return this.options_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      for (int m = 0; m < this.field_.size(); m++)
        k += CodedOutputStream.computeMessageSize(2, (MessageLite)this.field_.get(m));
      for (int n = 0; n < this.nestedType_.size(); n++)
        k += CodedOutputStream.computeMessageSize(3, (MessageLite)this.nestedType_.get(n));
      for (int i1 = 0; i1 < this.enumType_.size(); i1++)
        k += CodedOutputStream.computeMessageSize(4, (MessageLite)this.enumType_.get(i1));
      for (int i2 = 0; i2 < this.extensionRange_.size(); i2++)
        k += CodedOutputStream.computeMessageSize(5, (MessageLite)this.extensionRange_.get(i2));
      for (int i3 = 0; i3 < this.extension_.size(); i3++)
        k += CodedOutputStream.computeMessageSize(6, (MessageLite)this.extension_.get(i3));
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeMessageSize(7, this.options_);
      int i4 = k + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = i4;
      return i4;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasOptions()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_DescriptorProto_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getFieldCount(); j++)
        if (!getField(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      for (int k = 0; k < getExtensionCount(); k++)
        if (!getExtension(k).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      for (int m = 0; m < getNestedTypeCount(); m++)
        if (!getNestedType(m).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      for (int n = 0; n < getEnumTypeCount(); n++)
        if (!getEnumType(n).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if ((hasOptions()) && (!getOptions().isInitialized()))
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      for (int i = 0; i < this.field_.size(); i++)
        paramCodedOutputStream.writeMessage(2, (MessageLite)this.field_.get(i));
      for (int j = 0; j < this.nestedType_.size(); j++)
        paramCodedOutputStream.writeMessage(3, (MessageLite)this.nestedType_.get(j));
      for (int k = 0; k < this.enumType_.size(); k++)
        paramCodedOutputStream.writeMessage(4, (MessageLite)this.enumType_.get(k));
      for (int m = 0; m < this.extensionRange_.size(); m++)
        paramCodedOutputStream.writeMessage(5, (MessageLite)this.extensionRange_.get(m));
      for (int n = 0; n < this.extension_.size(); n++)
        paramCodedOutputStream.writeMessage(6, (MessageLite)this.extension_.get(n));
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeMessage(7, this.options_);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.DescriptorProtoOrBuilder
    {
      private int bitField0_;
      private RepeatedFieldBuilder<DescriptorProtos.EnumDescriptorProto, DescriptorProtos.EnumDescriptorProto.Builder, DescriptorProtos.EnumDescriptorProtoOrBuilder> enumTypeBuilder_;
      private List<DescriptorProtos.EnumDescriptorProto> enumType_ = Collections.emptyList();
      private RepeatedFieldBuilder<DescriptorProtos.FieldDescriptorProto, DescriptorProtos.FieldDescriptorProto.Builder, DescriptorProtos.FieldDescriptorProtoOrBuilder> extensionBuilder_;
      private RepeatedFieldBuilder<DescriptorProtos.DescriptorProto.ExtensionRange, DescriptorProtos.DescriptorProto.ExtensionRange.Builder, DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder> extensionRangeBuilder_;
      private List<DescriptorProtos.DescriptorProto.ExtensionRange> extensionRange_ = Collections.emptyList();
      private List<DescriptorProtos.FieldDescriptorProto> extension_ = Collections.emptyList();
      private RepeatedFieldBuilder<DescriptorProtos.FieldDescriptorProto, DescriptorProtos.FieldDescriptorProto.Builder, DescriptorProtos.FieldDescriptorProtoOrBuilder> fieldBuilder_;
      private List<DescriptorProtos.FieldDescriptorProto> field_ = Collections.emptyList();
      private Object name_ = "";
      private RepeatedFieldBuilder<DescriptorProtos.DescriptorProto, Builder, DescriptorProtos.DescriptorProtoOrBuilder> nestedTypeBuilder_;
      private List<DescriptorProtos.DescriptorProto> nestedType_ = Collections.emptyList();
      private SingleFieldBuilder<DescriptorProtos.MessageOptions, DescriptorProtos.MessageOptions.Builder, DescriptorProtos.MessageOptionsOrBuilder> optionsBuilder_;
      private DescriptorProtos.MessageOptions options_ = DescriptorProtos.MessageOptions.getDefaultInstance();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.DescriptorProto buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.DescriptorProto localDescriptorProto = buildPartial();
        if (!localDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localDescriptorProto).asInvalidProtocolBufferException();
        return localDescriptorProto;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureEnumTypeIsMutable()
      {
        if ((0x10 & this.bitField0_) != 16)
        {
          this.enumType_ = new ArrayList(this.enumType_);
          this.bitField0_ = (0x10 | this.bitField0_);
        }
      }

      private void ensureExtensionIsMutable()
      {
        if ((0x4 & this.bitField0_) != 4)
        {
          this.extension_ = new ArrayList(this.extension_);
          this.bitField0_ = (0x4 | this.bitField0_);
        }
      }

      private void ensureExtensionRangeIsMutable()
      {
        if ((0x20 & this.bitField0_) != 32)
        {
          this.extensionRange_ = new ArrayList(this.extensionRange_);
          this.bitField0_ = (0x20 | this.bitField0_);
        }
      }

      private void ensureFieldIsMutable()
      {
        if ((0x2 & this.bitField0_) != 2)
        {
          this.field_ = new ArrayList(this.field_);
          this.bitField0_ = (0x2 | this.bitField0_);
        }
      }

      private void ensureNestedTypeIsMutable()
      {
        if ((0x8 & this.bitField0_) != 8)
        {
          this.nestedType_ = new ArrayList(this.nestedType_);
          this.bitField0_ = (0x8 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_DescriptorProto_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.EnumDescriptorProto, DescriptorProtos.EnumDescriptorProto.Builder, DescriptorProtos.EnumDescriptorProtoOrBuilder> getEnumTypeFieldBuilder()
      {
        List localList;
        if (this.enumTypeBuilder_ == null)
        {
          localList = this.enumType_;
          if ((0x10 & this.bitField0_) != 16)
            break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
          this.enumTypeBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.enumType_ = null;
          return this.enumTypeBuilder_;
        }
      }

      private RepeatedFieldBuilder<DescriptorProtos.FieldDescriptorProto, DescriptorProtos.FieldDescriptorProto.Builder, DescriptorProtos.FieldDescriptorProtoOrBuilder> getExtensionFieldBuilder()
      {
        List localList;
        if (this.extensionBuilder_ == null)
        {
          localList = this.extension_;
          if ((0x4 & this.bitField0_) != 4)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.extensionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.extension_ = null;
          return this.extensionBuilder_;
        }
      }

      private RepeatedFieldBuilder<DescriptorProtos.DescriptorProto.ExtensionRange, DescriptorProtos.DescriptorProto.ExtensionRange.Builder, DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder> getExtensionRangeFieldBuilder()
      {
        List localList;
        if (this.extensionRangeBuilder_ == null)
        {
          localList = this.extensionRange_;
          if ((0x20 & this.bitField0_) != 32)
            break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
          this.extensionRangeBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.extensionRange_ = null;
          return this.extensionRangeBuilder_;
        }
      }

      private RepeatedFieldBuilder<DescriptorProtos.FieldDescriptorProto, DescriptorProtos.FieldDescriptorProto.Builder, DescriptorProtos.FieldDescriptorProtoOrBuilder> getFieldFieldBuilder()
      {
        List localList;
        if (this.fieldBuilder_ == null)
        {
          localList = this.field_;
          if ((0x2 & this.bitField0_) != 2)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.fieldBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.field_ = null;
          return this.fieldBuilder_;
        }
      }

      private RepeatedFieldBuilder<DescriptorProtos.DescriptorProto, Builder, DescriptorProtos.DescriptorProtoOrBuilder> getNestedTypeFieldBuilder()
      {
        List localList;
        if (this.nestedTypeBuilder_ == null)
        {
          localList = this.nestedType_;
          if ((0x8 & this.bitField0_) != 8)
            break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
          this.nestedTypeBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.nestedType_ = null;
          return this.nestedTypeBuilder_;
        }
      }

      private SingleFieldBuilder<DescriptorProtos.MessageOptions, DescriptorProtos.MessageOptions.Builder, DescriptorProtos.MessageOptionsOrBuilder> getOptionsFieldBuilder()
      {
        if (this.optionsBuilder_ == null)
        {
          this.optionsBuilder_ = new SingleFieldBuilder(this.options_, getParentForChildren(), isClean());
          this.options_ = null;
        }
        return this.optionsBuilder_;
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
        {
          getFieldFieldBuilder();
          getExtensionFieldBuilder();
          getNestedTypeFieldBuilder();
          getEnumTypeFieldBuilder();
          getExtensionRangeFieldBuilder();
          getOptionsFieldBuilder();
        }
      }

      public Builder addAllEnumType(Iterable<? extends DescriptorProtos.EnumDescriptorProto> paramIterable)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.enumType_);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addAllExtension(Iterable<? extends DescriptorProtos.FieldDescriptorProto> paramIterable)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.extension_);
          onChanged();
          return this;
        }
        this.extensionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addAllExtensionRange(Iterable<? extends DescriptorProtos.DescriptorProto.ExtensionRange> paramIterable)
      {
        if (this.extensionRangeBuilder_ == null)
        {
          ensureExtensionRangeIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.extensionRange_);
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addAllField(Iterable<? extends DescriptorProtos.FieldDescriptorProto> paramIterable)
      {
        if (this.fieldBuilder_ == null)
        {
          ensureFieldIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.field_);
          onChanged();
          return this;
        }
        this.fieldBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addAllNestedType(Iterable<? extends DescriptorProtos.DescriptorProto> paramIterable)
      {
        if (this.nestedTypeBuilder_ == null)
        {
          ensureNestedTypeIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.nestedType_);
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addEnumType(int paramInt, DescriptorProtos.EnumDescriptorProto.Builder paramBuilder)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          this.enumType_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addEnumType(int paramInt, DescriptorProtos.EnumDescriptorProto paramEnumDescriptorProto)
      {
        if (this.enumTypeBuilder_ == null)
        {
          if (paramEnumDescriptorProto == null)
            throw new NullPointerException();
          ensureEnumTypeIsMutable();
          this.enumType_.add(paramInt, paramEnumDescriptorProto);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addMessage(paramInt, paramEnumDescriptorProto);
        return this;
      }

      public Builder addEnumType(DescriptorProtos.EnumDescriptorProto.Builder paramBuilder)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          this.enumType_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addEnumType(DescriptorProtos.EnumDescriptorProto paramEnumDescriptorProto)
      {
        if (this.enumTypeBuilder_ == null)
        {
          if (paramEnumDescriptorProto == null)
            throw new NullPointerException();
          ensureEnumTypeIsMutable();
          this.enumType_.add(paramEnumDescriptorProto);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addMessage(paramEnumDescriptorProto);
        return this;
      }

      public DescriptorProtos.EnumDescriptorProto.Builder addEnumTypeBuilder()
      {
        return (DescriptorProtos.EnumDescriptorProto.Builder)getEnumTypeFieldBuilder().addBuilder(DescriptorProtos.EnumDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.EnumDescriptorProto.Builder addEnumTypeBuilder(int paramInt)
      {
        return (DescriptorProtos.EnumDescriptorProto.Builder)getEnumTypeFieldBuilder().addBuilder(paramInt, DescriptorProtos.EnumDescriptorProto.getDefaultInstance());
      }

      public Builder addExtension(int paramInt, DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          this.extension_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addExtension(int paramInt, DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.extensionBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureExtensionIsMutable();
          this.extension_.add(paramInt, paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.extensionBuilder_.addMessage(paramInt, paramFieldDescriptorProto);
        return this;
      }

      public Builder addExtension(DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          this.extension_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addExtension(DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.extensionBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureExtensionIsMutable();
          this.extension_.add(paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.extensionBuilder_.addMessage(paramFieldDescriptorProto);
        return this;
      }

      public DescriptorProtos.FieldDescriptorProto.Builder addExtensionBuilder()
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getExtensionFieldBuilder().addBuilder(DescriptorProtos.FieldDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.FieldDescriptorProto.Builder addExtensionBuilder(int paramInt)
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getExtensionFieldBuilder().addBuilder(paramInt, DescriptorProtos.FieldDescriptorProto.getDefaultInstance());
      }

      public Builder addExtensionRange(int paramInt, DescriptorProtos.DescriptorProto.ExtensionRange.Builder paramBuilder)
      {
        if (this.extensionRangeBuilder_ == null)
        {
          ensureExtensionRangeIsMutable();
          this.extensionRange_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addExtensionRange(int paramInt, DescriptorProtos.DescriptorProto.ExtensionRange paramExtensionRange)
      {
        if (this.extensionRangeBuilder_ == null)
        {
          if (paramExtensionRange == null)
            throw new NullPointerException();
          ensureExtensionRangeIsMutable();
          this.extensionRange_.add(paramInt, paramExtensionRange);
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.addMessage(paramInt, paramExtensionRange);
        return this;
      }

      public Builder addExtensionRange(DescriptorProtos.DescriptorProto.ExtensionRange.Builder paramBuilder)
      {
        if (this.extensionRangeBuilder_ == null)
        {
          ensureExtensionRangeIsMutable();
          this.extensionRange_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addExtensionRange(DescriptorProtos.DescriptorProto.ExtensionRange paramExtensionRange)
      {
        if (this.extensionRangeBuilder_ == null)
        {
          if (paramExtensionRange == null)
            throw new NullPointerException();
          ensureExtensionRangeIsMutable();
          this.extensionRange_.add(paramExtensionRange);
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.addMessage(paramExtensionRange);
        return this;
      }

      public DescriptorProtos.DescriptorProto.ExtensionRange.Builder addExtensionRangeBuilder()
      {
        return (DescriptorProtos.DescriptorProto.ExtensionRange.Builder)getExtensionRangeFieldBuilder().addBuilder(DescriptorProtos.DescriptorProto.ExtensionRange.getDefaultInstance());
      }

      public DescriptorProtos.DescriptorProto.ExtensionRange.Builder addExtensionRangeBuilder(int paramInt)
      {
        return (DescriptorProtos.DescriptorProto.ExtensionRange.Builder)getExtensionRangeFieldBuilder().addBuilder(paramInt, DescriptorProtos.DescriptorProto.ExtensionRange.getDefaultInstance());
      }

      public Builder addField(int paramInt, DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.fieldBuilder_ == null)
        {
          ensureFieldIsMutable();
          this.field_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.fieldBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addField(int paramInt, DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.fieldBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureFieldIsMutable();
          this.field_.add(paramInt, paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.fieldBuilder_.addMessage(paramInt, paramFieldDescriptorProto);
        return this;
      }

      public Builder addField(DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.fieldBuilder_ == null)
        {
          ensureFieldIsMutable();
          this.field_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.fieldBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addField(DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.fieldBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureFieldIsMutable();
          this.field_.add(paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.fieldBuilder_.addMessage(paramFieldDescriptorProto);
        return this;
      }

      public DescriptorProtos.FieldDescriptorProto.Builder addFieldBuilder()
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getFieldFieldBuilder().addBuilder(DescriptorProtos.FieldDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.FieldDescriptorProto.Builder addFieldBuilder(int paramInt)
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getFieldFieldBuilder().addBuilder(paramInt, DescriptorProtos.FieldDescriptorProto.getDefaultInstance());
      }

      public Builder addNestedType(int paramInt, Builder paramBuilder)
      {
        if (this.nestedTypeBuilder_ == null)
        {
          ensureNestedTypeIsMutable();
          this.nestedType_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addNestedType(int paramInt, DescriptorProtos.DescriptorProto paramDescriptorProto)
      {
        if (this.nestedTypeBuilder_ == null)
        {
          if (paramDescriptorProto == null)
            throw new NullPointerException();
          ensureNestedTypeIsMutable();
          this.nestedType_.add(paramInt, paramDescriptorProto);
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.addMessage(paramInt, paramDescriptorProto);
        return this;
      }

      public Builder addNestedType(Builder paramBuilder)
      {
        if (this.nestedTypeBuilder_ == null)
        {
          ensureNestedTypeIsMutable();
          this.nestedType_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addNestedType(DescriptorProtos.DescriptorProto paramDescriptorProto)
      {
        if (this.nestedTypeBuilder_ == null)
        {
          if (paramDescriptorProto == null)
            throw new NullPointerException();
          ensureNestedTypeIsMutable();
          this.nestedType_.add(paramDescriptorProto);
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.addMessage(paramDescriptorProto);
        return this;
      }

      public Builder addNestedTypeBuilder()
      {
        return (Builder)getNestedTypeFieldBuilder().addBuilder(DescriptorProtos.DescriptorProto.getDefaultInstance());
      }

      public Builder addNestedTypeBuilder(int paramInt)
      {
        return (Builder)getNestedTypeFieldBuilder().addBuilder(paramInt, DescriptorProtos.DescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.DescriptorProto build()
      {
        DescriptorProtos.DescriptorProto localDescriptorProto = buildPartial();
        if (!localDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localDescriptorProto);
        return localDescriptorProto;
      }

      public DescriptorProtos.DescriptorProto buildPartial()
      {
        DescriptorProtos.DescriptorProto localDescriptorProto = new DescriptorProtos.DescriptorProto(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.DescriptorProto.access$3802(localDescriptorProto, this.name_);
        if (this.fieldBuilder_ == null)
        {
          if ((0x2 & this.bitField0_) == 2)
          {
            this.field_ = Collections.unmodifiableList(this.field_);
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          }
          DescriptorProtos.DescriptorProto.access$3902(localDescriptorProto, this.field_);
          if (this.extensionBuilder_ != null)
            break label346;
          if ((0x4 & this.bitField0_) == 4)
          {
            this.extension_ = Collections.unmodifiableList(this.extension_);
            this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          }
          DescriptorProtos.DescriptorProto.access$4002(localDescriptorProto, this.extension_);
          label137: if (this.nestedTypeBuilder_ != null)
            break label361;
          if ((0x8 & this.bitField0_) == 8)
          {
            this.nestedType_ = Collections.unmodifiableList(this.nestedType_);
            this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          }
          DescriptorProtos.DescriptorProto.access$4102(localDescriptorProto, this.nestedType_);
          label187: if (this.enumTypeBuilder_ != null)
            break label376;
          if ((0x10 & this.bitField0_) == 16)
          {
            this.enumType_ = Collections.unmodifiableList(this.enumType_);
            this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          }
          DescriptorProtos.DescriptorProto.access$4202(localDescriptorProto, this.enumType_);
          label237: if (this.extensionRangeBuilder_ != null)
            break label391;
          if ((0x20 & this.bitField0_) == 32)
          {
            this.extensionRange_ = Collections.unmodifiableList(this.extensionRange_);
            this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
          }
          DescriptorProtos.DescriptorProto.access$4302(localDescriptorProto, this.extensionRange_);
          label287: if ((i & 0x40) == 64)
            k |= 2;
          if (this.optionsBuilder_ != null)
            break label406;
          DescriptorProtos.DescriptorProto.access$4402(localDescriptorProto, this.options_);
        }
        while (true)
        {
          DescriptorProtos.DescriptorProto.access$4502(localDescriptorProto, k);
          onBuilt();
          return localDescriptorProto;
          DescriptorProtos.DescriptorProto.access$3902(localDescriptorProto, this.fieldBuilder_.build());
          break;
          label346: DescriptorProtos.DescriptorProto.access$4002(localDescriptorProto, this.extensionBuilder_.build());
          break label137;
          label361: DescriptorProtos.DescriptorProto.access$4102(localDescriptorProto, this.nestedTypeBuilder_.build());
          break label187;
          label376: DescriptorProtos.DescriptorProto.access$4202(localDescriptorProto, this.enumTypeBuilder_.build());
          break label237;
          label391: DescriptorProtos.DescriptorProto.access$4302(localDescriptorProto, this.extensionRangeBuilder_.build());
          break label287;
          label406: DescriptorProtos.DescriptorProto.access$4402(localDescriptorProto, (DescriptorProtos.MessageOptions)this.optionsBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        if (this.fieldBuilder_ == null)
        {
          this.field_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          if (this.extensionBuilder_ != null)
            break label184;
          this.extension_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          label72: if (this.nestedTypeBuilder_ != null)
            break label194;
          this.nestedType_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          label97: if (this.enumTypeBuilder_ != null)
            break label204;
          this.enumType_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          label122: if (this.extensionRangeBuilder_ != null)
            break label214;
          this.extensionRange_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
          label147: if (this.optionsBuilder_ != null)
            break label224;
          this.options_ = DescriptorProtos.MessageOptions.getDefaultInstance();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
          return this;
          this.fieldBuilder_.clear();
          break;
          label184: this.extensionBuilder_.clear();
          break label72;
          label194: this.nestedTypeBuilder_.clear();
          break label97;
          label204: this.enumTypeBuilder_.clear();
          break label122;
          label214: this.extensionRangeBuilder_.clear();
          break label147;
          label224: this.optionsBuilder_.clear();
        }
      }

      public Builder clearEnumType()
      {
        if (this.enumTypeBuilder_ == null)
        {
          this.enumType_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.clear();
        return this;
      }

      public Builder clearExtension()
      {
        if (this.extensionBuilder_ == null)
        {
          this.extension_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          onChanged();
          return this;
        }
        this.extensionBuilder_.clear();
        return this;
      }

      public Builder clearExtensionRange()
      {
        if (this.extensionRangeBuilder_ == null)
        {
          this.extensionRange_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.clear();
        return this;
      }

      public Builder clearField()
      {
        if (this.fieldBuilder_ == null)
        {
          this.field_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          onChanged();
          return this;
        }
        this.fieldBuilder_.clear();
        return this;
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = DescriptorProtos.DescriptorProto.getDefaultInstance().getName();
        onChanged();
        return this;
      }

      public Builder clearNestedType()
      {
        if (this.nestedTypeBuilder_ == null)
        {
          this.nestedType_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.clear();
        return this;
      }

      public Builder clearOptions()
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = DescriptorProtos.MessageOptions.getDefaultInstance();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.DescriptorProto getDefaultInstanceForType()
      {
        return DescriptorProtos.DescriptorProto.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.DescriptorProto.getDescriptor();
      }

      public DescriptorProtos.EnumDescriptorProto getEnumType(int paramInt)
      {
        if (this.enumTypeBuilder_ == null)
          return (DescriptorProtos.EnumDescriptorProto)this.enumType_.get(paramInt);
        return (DescriptorProtos.EnumDescriptorProto)this.enumTypeBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.EnumDescriptorProto.Builder getEnumTypeBuilder(int paramInt)
      {
        return (DescriptorProtos.EnumDescriptorProto.Builder)getEnumTypeFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.EnumDescriptorProto.Builder> getEnumTypeBuilderList()
      {
        return getEnumTypeFieldBuilder().getBuilderList();
      }

      public int getEnumTypeCount()
      {
        if (this.enumTypeBuilder_ == null)
          return this.enumType_.size();
        return this.enumTypeBuilder_.getCount();
      }

      public List<DescriptorProtos.EnumDescriptorProto> getEnumTypeList()
      {
        if (this.enumTypeBuilder_ == null)
          return Collections.unmodifiableList(this.enumType_);
        return this.enumTypeBuilder_.getMessageList();
      }

      public DescriptorProtos.EnumDescriptorProtoOrBuilder getEnumTypeOrBuilder(int paramInt)
      {
        if (this.enumTypeBuilder_ == null)
          return (DescriptorProtos.EnumDescriptorProtoOrBuilder)this.enumType_.get(paramInt);
        return (DescriptorProtos.EnumDescriptorProtoOrBuilder)this.enumTypeBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.EnumDescriptorProtoOrBuilder> getEnumTypeOrBuilderList()
      {
        if (this.enumTypeBuilder_ != null)
          return this.enumTypeBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.enumType_);
      }

      public DescriptorProtos.FieldDescriptorProto getExtension(int paramInt)
      {
        if (this.extensionBuilder_ == null)
          return (DescriptorProtos.FieldDescriptorProto)this.extension_.get(paramInt);
        return (DescriptorProtos.FieldDescriptorProto)this.extensionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.FieldDescriptorProto.Builder getExtensionBuilder(int paramInt)
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getExtensionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.FieldDescriptorProto.Builder> getExtensionBuilderList()
      {
        return getExtensionFieldBuilder().getBuilderList();
      }

      public int getExtensionCount()
      {
        if (this.extensionBuilder_ == null)
          return this.extension_.size();
        return this.extensionBuilder_.getCount();
      }

      public List<DescriptorProtos.FieldDescriptorProto> getExtensionList()
      {
        if (this.extensionBuilder_ == null)
          return Collections.unmodifiableList(this.extension_);
        return this.extensionBuilder_.getMessageList();
      }

      public DescriptorProtos.FieldDescriptorProtoOrBuilder getExtensionOrBuilder(int paramInt)
      {
        if (this.extensionBuilder_ == null)
          return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.extension_.get(paramInt);
        return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.extensionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getExtensionOrBuilderList()
      {
        if (this.extensionBuilder_ != null)
          return this.extensionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.extension_);
      }

      public DescriptorProtos.DescriptorProto.ExtensionRange getExtensionRange(int paramInt)
      {
        if (this.extensionRangeBuilder_ == null)
          return (DescriptorProtos.DescriptorProto.ExtensionRange)this.extensionRange_.get(paramInt);
        return (DescriptorProtos.DescriptorProto.ExtensionRange)this.extensionRangeBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.DescriptorProto.ExtensionRange.Builder getExtensionRangeBuilder(int paramInt)
      {
        return (DescriptorProtos.DescriptorProto.ExtensionRange.Builder)getExtensionRangeFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.DescriptorProto.ExtensionRange.Builder> getExtensionRangeBuilderList()
      {
        return getExtensionRangeFieldBuilder().getBuilderList();
      }

      public int getExtensionRangeCount()
      {
        if (this.extensionRangeBuilder_ == null)
          return this.extensionRange_.size();
        return this.extensionRangeBuilder_.getCount();
      }

      public List<DescriptorProtos.DescriptorProto.ExtensionRange> getExtensionRangeList()
      {
        if (this.extensionRangeBuilder_ == null)
          return Collections.unmodifiableList(this.extensionRange_);
        return this.extensionRangeBuilder_.getMessageList();
      }

      public DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder getExtensionRangeOrBuilder(int paramInt)
      {
        if (this.extensionRangeBuilder_ == null)
          return (DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder)this.extensionRange_.get(paramInt);
        return (DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder)this.extensionRangeBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder> getExtensionRangeOrBuilderList()
      {
        if (this.extensionRangeBuilder_ != null)
          return this.extensionRangeBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.extensionRange_);
      }

      public DescriptorProtos.FieldDescriptorProto getField(int paramInt)
      {
        if (this.fieldBuilder_ == null)
          return (DescriptorProtos.FieldDescriptorProto)this.field_.get(paramInt);
        return (DescriptorProtos.FieldDescriptorProto)this.fieldBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.FieldDescriptorProto.Builder getFieldBuilder(int paramInt)
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getFieldFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.FieldDescriptorProto.Builder> getFieldBuilderList()
      {
        return getFieldFieldBuilder().getBuilderList();
      }

      public int getFieldCount()
      {
        if (this.fieldBuilder_ == null)
          return this.field_.size();
        return this.fieldBuilder_.getCount();
      }

      public List<DescriptorProtos.FieldDescriptorProto> getFieldList()
      {
        if (this.fieldBuilder_ == null)
          return Collections.unmodifiableList(this.field_);
        return this.fieldBuilder_.getMessageList();
      }

      public DescriptorProtos.FieldDescriptorProtoOrBuilder getFieldOrBuilder(int paramInt)
      {
        if (this.fieldBuilder_ == null)
          return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.field_.get(paramInt);
        return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.fieldBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getFieldOrBuilderList()
      {
        if (this.fieldBuilder_ != null)
          return this.fieldBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.field_);
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.DescriptorProto getNestedType(int paramInt)
      {
        if (this.nestedTypeBuilder_ == null)
          return (DescriptorProtos.DescriptorProto)this.nestedType_.get(paramInt);
        return (DescriptorProtos.DescriptorProto)this.nestedTypeBuilder_.getMessage(paramInt);
      }

      public Builder getNestedTypeBuilder(int paramInt)
      {
        return (Builder)getNestedTypeFieldBuilder().getBuilder(paramInt);
      }

      public List<Builder> getNestedTypeBuilderList()
      {
        return getNestedTypeFieldBuilder().getBuilderList();
      }

      public int getNestedTypeCount()
      {
        if (this.nestedTypeBuilder_ == null)
          return this.nestedType_.size();
        return this.nestedTypeBuilder_.getCount();
      }

      public List<DescriptorProtos.DescriptorProto> getNestedTypeList()
      {
        if (this.nestedTypeBuilder_ == null)
          return Collections.unmodifiableList(this.nestedType_);
        return this.nestedTypeBuilder_.getMessageList();
      }

      public DescriptorProtos.DescriptorProtoOrBuilder getNestedTypeOrBuilder(int paramInt)
      {
        if (this.nestedTypeBuilder_ == null)
          return (DescriptorProtos.DescriptorProtoOrBuilder)this.nestedType_.get(paramInt);
        return (DescriptorProtos.DescriptorProtoOrBuilder)this.nestedTypeBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.DescriptorProtoOrBuilder> getNestedTypeOrBuilderList()
      {
        if (this.nestedTypeBuilder_ != null)
          return this.nestedTypeBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.nestedType_);
      }

      public DescriptorProtos.MessageOptions getOptions()
      {
        if (this.optionsBuilder_ == null)
          return this.options_;
        return (DescriptorProtos.MessageOptions)this.optionsBuilder_.getMessage();
      }

      public DescriptorProtos.MessageOptions.Builder getOptionsBuilder()
      {
        this.bitField0_ = (0x40 | this.bitField0_);
        onChanged();
        return (DescriptorProtos.MessageOptions.Builder)getOptionsFieldBuilder().getBuilder();
      }

      public DescriptorProtos.MessageOptionsOrBuilder getOptionsOrBuilder()
      {
        if (this.optionsBuilder_ != null)
          return (DescriptorProtos.MessageOptionsOrBuilder)this.optionsBuilder_.getMessageOrBuilder();
        return this.options_;
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasOptions()
      {
        return (0x40 & this.bitField0_) == 64;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_DescriptorProto_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getFieldCount(); i++)
          if (!getField(i).isInitialized())
            return false;
        for (int j = 0; j < getExtensionCount(); j++)
          if (!getExtension(j).isInitialized())
            return false;
        for (int k = 0; k < getNestedTypeCount(); k++)
          if (!getNestedType(k).isInitialized())
            return false;
        for (int m = 0; m < getEnumTypeCount(); m++)
          if (!getEnumType(m).isInitialized())
            return false;
        return (!hasOptions()) || (getOptions().isInitialized());
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            DescriptorProtos.FieldDescriptorProto.Builder localBuilder6 = DescriptorProtos.FieldDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder6, paramExtensionRegistryLite);
            addField(localBuilder6.buildPartial());
            break;
          case 26:
            Builder localBuilder5 = DescriptorProtos.DescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder5, paramExtensionRegistryLite);
            addNestedType(localBuilder5.buildPartial());
            break;
          case 34:
            DescriptorProtos.EnumDescriptorProto.Builder localBuilder4 = DescriptorProtos.EnumDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder4, paramExtensionRegistryLite);
            addEnumType(localBuilder4.buildPartial());
            break;
          case 42:
            DescriptorProtos.DescriptorProto.ExtensionRange.Builder localBuilder3 = DescriptorProtos.DescriptorProto.ExtensionRange.newBuilder();
            paramCodedInputStream.readMessage(localBuilder3, paramExtensionRegistryLite);
            addExtensionRange(localBuilder3.buildPartial());
            break;
          case 50:
            DescriptorProtos.FieldDescriptorProto.Builder localBuilder2 = DescriptorProtos.FieldDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder2, paramExtensionRegistryLite);
            addExtension(localBuilder2.buildPartial());
            break;
          case 58:
            DescriptorProtos.MessageOptions.Builder localBuilder1 = DescriptorProtos.MessageOptions.newBuilder();
            if (hasOptions())
              localBuilder1.mergeFrom(getOptions());
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            setOptions(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.DescriptorProto paramDescriptorProto)
      {
        if (paramDescriptorProto == DescriptorProtos.DescriptorProto.getDefaultInstance())
          return this;
        if (paramDescriptorProto.hasName())
          setName(paramDescriptorProto.getName());
        if (this.fieldBuilder_ == null)
          if (!paramDescriptorProto.field_.isEmpty())
          {
            if (this.field_.isEmpty())
            {
              this.field_ = paramDescriptorProto.field_;
              this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
              onChanged();
            }
          }
          else
          {
            label79: if (this.extensionBuilder_ != null)
              break label459;
            if (!paramDescriptorProto.extension_.isEmpty())
            {
              if (!this.extension_.isEmpty())
                break label438;
              this.extension_ = paramDescriptorProto.extension_;
              this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
              label129: onChanged();
            }
            label133: if (this.nestedTypeBuilder_ != null)
              break label575;
            if (!paramDescriptorProto.nestedType_.isEmpty())
            {
              if (!this.nestedType_.isEmpty())
                break label554;
              this.nestedType_ = paramDescriptorProto.nestedType_;
              this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
              label183: onChanged();
            }
            label187: if (this.enumTypeBuilder_ != null)
              break label691;
            if (!paramDescriptorProto.enumType_.isEmpty())
            {
              if (!this.enumType_.isEmpty())
                break label670;
              this.enumType_ = paramDescriptorProto.enumType_;
              this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
              label237: onChanged();
            }
            label241: if (this.extensionRangeBuilder_ != null)
              break label807;
            if (!paramDescriptorProto.extensionRange_.isEmpty())
            {
              if (!this.extensionRange_.isEmpty())
                break label786;
              this.extensionRange_ = paramDescriptorProto.extensionRange_;
              this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
              label291: onChanged();
            }
          }
        while (true)
        {
          if (paramDescriptorProto.hasOptions())
            mergeOptions(paramDescriptorProto.getOptions());
          mergeUnknownFields(paramDescriptorProto.getUnknownFields());
          return this;
          ensureFieldIsMutable();
          this.field_.addAll(paramDescriptorProto.field_);
          break;
          if (paramDescriptorProto.field_.isEmpty())
            break label79;
          if (this.fieldBuilder_.isEmpty())
          {
            this.fieldBuilder_.dispose();
            this.fieldBuilder_ = null;
            this.field_ = paramDescriptorProto.field_;
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
            if (GeneratedMessage.alwaysUseFieldBuilders);
            for (RepeatedFieldBuilder localRepeatedFieldBuilder5 = getFieldFieldBuilder(); ; localRepeatedFieldBuilder5 = null)
            {
              this.fieldBuilder_ = localRepeatedFieldBuilder5;
              break;
            }
          }
          this.fieldBuilder_.addAllMessages(paramDescriptorProto.field_);
          break label79;
          label438: ensureExtensionIsMutable();
          this.extension_.addAll(paramDescriptorProto.extension_);
          break label129;
          label459: if (paramDescriptorProto.extension_.isEmpty())
            break label133;
          if (this.extensionBuilder_.isEmpty())
          {
            this.extensionBuilder_.dispose();
            this.extensionBuilder_ = null;
            this.extension_ = paramDescriptorProto.extension_;
            this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
            if (GeneratedMessage.alwaysUseFieldBuilders);
            for (RepeatedFieldBuilder localRepeatedFieldBuilder4 = getExtensionFieldBuilder(); ; localRepeatedFieldBuilder4 = null)
            {
              this.extensionBuilder_ = localRepeatedFieldBuilder4;
              break;
            }
          }
          this.extensionBuilder_.addAllMessages(paramDescriptorProto.extension_);
          break label133;
          label554: ensureNestedTypeIsMutable();
          this.nestedType_.addAll(paramDescriptorProto.nestedType_);
          break label183;
          label575: if (paramDescriptorProto.nestedType_.isEmpty())
            break label187;
          if (this.nestedTypeBuilder_.isEmpty())
          {
            this.nestedTypeBuilder_.dispose();
            this.nestedTypeBuilder_ = null;
            this.nestedType_ = paramDescriptorProto.nestedType_;
            this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
            if (GeneratedMessage.alwaysUseFieldBuilders);
            for (RepeatedFieldBuilder localRepeatedFieldBuilder3 = getNestedTypeFieldBuilder(); ; localRepeatedFieldBuilder3 = null)
            {
              this.nestedTypeBuilder_ = localRepeatedFieldBuilder3;
              break;
            }
          }
          this.nestedTypeBuilder_.addAllMessages(paramDescriptorProto.nestedType_);
          break label187;
          label670: ensureEnumTypeIsMutable();
          this.enumType_.addAll(paramDescriptorProto.enumType_);
          break label237;
          label691: if (paramDescriptorProto.enumType_.isEmpty())
            break label241;
          if (this.enumTypeBuilder_.isEmpty())
          {
            this.enumTypeBuilder_.dispose();
            this.enumTypeBuilder_ = null;
            this.enumType_ = paramDescriptorProto.enumType_;
            this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
            if (GeneratedMessage.alwaysUseFieldBuilders);
            for (RepeatedFieldBuilder localRepeatedFieldBuilder2 = getEnumTypeFieldBuilder(); ; localRepeatedFieldBuilder2 = null)
            {
              this.enumTypeBuilder_ = localRepeatedFieldBuilder2;
              break;
            }
          }
          this.enumTypeBuilder_.addAllMessages(paramDescriptorProto.enumType_);
          break label241;
          label786: ensureExtensionRangeIsMutable();
          this.extensionRange_.addAll(paramDescriptorProto.extensionRange_);
          break label291;
          label807: if (!paramDescriptorProto.extensionRange_.isEmpty())
          {
            if (this.extensionRangeBuilder_.isEmpty())
            {
              this.extensionRangeBuilder_.dispose();
              this.extensionRangeBuilder_ = null;
              this.extensionRange_ = paramDescriptorProto.extensionRange_;
              this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder1 = getExtensionRangeFieldBuilder(); ; localRepeatedFieldBuilder1 = null)
              {
                this.extensionRangeBuilder_ = localRepeatedFieldBuilder1;
                break;
              }
            }
            this.extensionRangeBuilder_.addAllMessages(paramDescriptorProto.extensionRange_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.DescriptorProto))
          return mergeFrom((DescriptorProtos.DescriptorProto)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder mergeOptions(DescriptorProtos.MessageOptions paramMessageOptions)
      {
        if (this.optionsBuilder_ == null)
          if (((0x40 & this.bitField0_) == 64) && (this.options_ != DescriptorProtos.MessageOptions.getDefaultInstance()))
          {
            this.options_ = DescriptorProtos.MessageOptions.newBuilder(this.options_).mergeFrom(paramMessageOptions).buildPartial();
            onChanged();
          }
        while (true)
        {
          this.bitField0_ = (0x40 | this.bitField0_);
          return this;
          this.options_ = paramMessageOptions;
          break;
          this.optionsBuilder_.mergeFrom(paramMessageOptions);
        }
      }

      public Builder removeEnumType(int paramInt)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          this.enumType_.remove(paramInt);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.remove(paramInt);
        return this;
      }

      public Builder removeExtension(int paramInt)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          this.extension_.remove(paramInt);
          onChanged();
          return this;
        }
        this.extensionBuilder_.remove(paramInt);
        return this;
      }

      public Builder removeExtensionRange(int paramInt)
      {
        if (this.extensionRangeBuilder_ == null)
        {
          ensureExtensionRangeIsMutable();
          this.extensionRange_.remove(paramInt);
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.remove(paramInt);
        return this;
      }

      public Builder removeField(int paramInt)
      {
        if (this.fieldBuilder_ == null)
        {
          ensureFieldIsMutable();
          this.field_.remove(paramInt);
          onChanged();
          return this;
        }
        this.fieldBuilder_.remove(paramInt);
        return this;
      }

      public Builder removeNestedType(int paramInt)
      {
        if (this.nestedTypeBuilder_ == null)
        {
          ensureNestedTypeIsMutable();
          this.nestedType_.remove(paramInt);
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.remove(paramInt);
        return this;
      }

      public Builder setEnumType(int paramInt, DescriptorProtos.EnumDescriptorProto.Builder paramBuilder)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          this.enumType_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setEnumType(int paramInt, DescriptorProtos.EnumDescriptorProto paramEnumDescriptorProto)
      {
        if (this.enumTypeBuilder_ == null)
        {
          if (paramEnumDescriptorProto == null)
            throw new NullPointerException();
          ensureEnumTypeIsMutable();
          this.enumType_.set(paramInt, paramEnumDescriptorProto);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.setMessage(paramInt, paramEnumDescriptorProto);
        return this;
      }

      public Builder setExtension(int paramInt, DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          this.extension_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setExtension(int paramInt, DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.extensionBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureExtensionIsMutable();
          this.extension_.set(paramInt, paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.extensionBuilder_.setMessage(paramInt, paramFieldDescriptorProto);
        return this;
      }

      public Builder setExtensionRange(int paramInt, DescriptorProtos.DescriptorProto.ExtensionRange.Builder paramBuilder)
      {
        if (this.extensionRangeBuilder_ == null)
        {
          ensureExtensionRangeIsMutable();
          this.extensionRange_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setExtensionRange(int paramInt, DescriptorProtos.DescriptorProto.ExtensionRange paramExtensionRange)
      {
        if (this.extensionRangeBuilder_ == null)
        {
          if (paramExtensionRange == null)
            throw new NullPointerException();
          ensureExtensionRangeIsMutable();
          this.extensionRange_.set(paramInt, paramExtensionRange);
          onChanged();
          return this;
        }
        this.extensionRangeBuilder_.setMessage(paramInt, paramExtensionRange);
        return this;
      }

      public Builder setField(int paramInt, DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.fieldBuilder_ == null)
        {
          ensureFieldIsMutable();
          this.field_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.fieldBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setField(int paramInt, DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.fieldBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureFieldIsMutable();
          this.field_.set(paramInt, paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.fieldBuilder_.setMessage(paramInt, paramFieldDescriptorProto);
        return this;
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        onChanged();
        return this;
      }

      void setName(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        onChanged();
      }

      public Builder setNestedType(int paramInt, Builder paramBuilder)
      {
        if (this.nestedTypeBuilder_ == null)
        {
          ensureNestedTypeIsMutable();
          this.nestedType_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setNestedType(int paramInt, DescriptorProtos.DescriptorProto paramDescriptorProto)
      {
        if (this.nestedTypeBuilder_ == null)
        {
          if (paramDescriptorProto == null)
            throw new NullPointerException();
          ensureNestedTypeIsMutable();
          this.nestedType_.set(paramInt, paramDescriptorProto);
          onChanged();
          return this;
        }
        this.nestedTypeBuilder_.setMessage(paramInt, paramDescriptorProto);
        return this;
      }

      public Builder setOptions(DescriptorProtos.MessageOptions.Builder paramBuilder)
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = paramBuilder.build();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x40 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramBuilder.build());
        }
      }

      public Builder setOptions(DescriptorProtos.MessageOptions paramMessageOptions)
      {
        if (this.optionsBuilder_ == null)
        {
          if (paramMessageOptions == null)
            throw new NullPointerException();
          this.options_ = paramMessageOptions;
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x40 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramMessageOptions);
        }
      }
    }

    public static final class ExtensionRange extends GeneratedMessage
      implements DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder
    {
      public static final int END_FIELD_NUMBER = 2;
      public static final int START_FIELD_NUMBER = 1;
      private static final ExtensionRange defaultInstance = new ExtensionRange(true);
      private int bitField0_;
      private int end_;
      private byte memoizedIsInitialized = -1;
      private int memoizedSerializedSize = -1;
      private int start_;

      static
      {
        defaultInstance.initFields();
      }

      private ExtensionRange(Builder paramBuilder)
      {
        super();
      }

      private ExtensionRange(boolean paramBoolean)
      {
      }

      public static ExtensionRange getDefaultInstance()
      {
        return defaultInstance;
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_DescriptorProto_ExtensionRange_descriptor;
      }

      private void initFields()
      {
        this.start_ = 0;
        this.end_ = 0;
      }

      public static Builder newBuilder()
      {
        return Builder.access$2800();
      }

      public static Builder newBuilder(ExtensionRange paramExtensionRange)
      {
        return newBuilder().mergeFrom(paramExtensionRange);
      }

      public static ExtensionRange parseDelimitedFrom(InputStream paramInputStream)
        throws IOException
      {
        Builder localBuilder = newBuilder();
        if (localBuilder.mergeDelimitedFrom(paramInputStream))
          return localBuilder.buildParsed();
        return null;
      }

      public static ExtensionRange parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        Builder localBuilder = newBuilder();
        if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
          return localBuilder.buildParsed();
        return null;
      }

      public static ExtensionRange parseFrom(ByteString paramByteString)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
      }

      public static ExtensionRange parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
      }

      public static ExtensionRange parseFrom(CodedInputStream paramCodedInputStream)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
      }

      public static ExtensionRange parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
      }

      public static ExtensionRange parseFrom(InputStream paramInputStream)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
      }

      public static ExtensionRange parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
      }

      public static ExtensionRange parseFrom(byte[] paramArrayOfByte)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
      }

      public static ExtensionRange parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
      }

      public ExtensionRange getDefaultInstanceForType()
      {
        return defaultInstance;
      }

      public int getEnd()
      {
        return this.end_;
      }

      public int getSerializedSize()
      {
        int i = this.memoizedSerializedSize;
        if (i != -1)
          return i;
        int j = 0x1 & this.bitField0_;
        int k = 0;
        if (j == 1)
          k = 0 + CodedOutputStream.computeInt32Size(1, this.start_);
        if ((0x2 & this.bitField0_) == 2)
          k += CodedOutputStream.computeInt32Size(2, this.end_);
        int m = k + getUnknownFields().getSerializedSize();
        this.memoizedSerializedSize = m;
        return m;
      }

      public int getStart()
      {
        return this.start_;
      }

      public boolean hasEnd()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasStart()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_DescriptorProto_ExtensionRange_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        int i = this.memoizedIsInitialized;
        if (i != -1)
          return i == 1;
        this.memoizedIsInitialized = 1;
        return true;
      }

      public Builder newBuilderForType()
      {
        return newBuilder();
      }

      protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        return new Builder(paramBuilderParent, null);
      }

      public Builder toBuilder()
      {
        return newBuilder(this);
      }

      protected Object writeReplace()
        throws ObjectStreamException
      {
        return super.writeReplace();
      }

      public void writeTo(CodedOutputStream paramCodedOutputStream)
        throws IOException
      {
        getSerializedSize();
        if ((0x1 & this.bitField0_) == 1)
          paramCodedOutputStream.writeInt32(1, this.start_);
        if ((0x2 & this.bitField0_) == 2)
          paramCodedOutputStream.writeInt32(2, this.end_);
        getUnknownFields().writeTo(paramCodedOutputStream);
      }

      public static final class Builder extends GeneratedMessage.Builder<Builder>
        implements DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder
      {
        private int bitField0_;
        private int end_;
        private int start_;

        private Builder()
        {
          maybeForceBuilderInitialization();
        }

        private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
        {
          super();
          maybeForceBuilderInitialization();
        }

        private DescriptorProtos.DescriptorProto.ExtensionRange buildParsed()
          throws InvalidProtocolBufferException
        {
          DescriptorProtos.DescriptorProto.ExtensionRange localExtensionRange = buildPartial();
          if (!localExtensionRange.isInitialized())
            throw newUninitializedMessageException(localExtensionRange).asInvalidProtocolBufferException();
          return localExtensionRange;
        }

        private static Builder create()
        {
          return new Builder();
        }

        public static final Descriptors.Descriptor getDescriptor()
        {
          return DescriptorProtos.internal_static_google_protobuf_DescriptorProto_ExtensionRange_descriptor;
        }

        private void maybeForceBuilderInitialization()
        {
          if (GeneratedMessage.alwaysUseFieldBuilders);
        }

        public DescriptorProtos.DescriptorProto.ExtensionRange build()
        {
          DescriptorProtos.DescriptorProto.ExtensionRange localExtensionRange = buildPartial();
          if (!localExtensionRange.isInitialized())
            throw newUninitializedMessageException(localExtensionRange);
          return localExtensionRange;
        }

        public DescriptorProtos.DescriptorProto.ExtensionRange buildPartial()
        {
          DescriptorProtos.DescriptorProto.ExtensionRange localExtensionRange = new DescriptorProtos.DescriptorProto.ExtensionRange(this, null);
          int i = this.bitField0_;
          int j = i & 0x1;
          int k = 0;
          if (j == 1)
            k = 0x0 | 0x1;
          DescriptorProtos.DescriptorProto.ExtensionRange.access$3102(localExtensionRange, this.start_);
          if ((i & 0x2) == 2)
            k |= 2;
          DescriptorProtos.DescriptorProto.ExtensionRange.access$3202(localExtensionRange, this.end_);
          DescriptorProtos.DescriptorProto.ExtensionRange.access$3302(localExtensionRange, k);
          onBuilt();
          return localExtensionRange;
        }

        public Builder clear()
        {
          super.clear();
          this.start_ = 0;
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          this.end_ = 0;
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          return this;
        }

        public Builder clearEnd()
        {
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          this.end_ = 0;
          onChanged();
          return this;
        }

        public Builder clearStart()
        {
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          this.start_ = 0;
          onChanged();
          return this;
        }

        public Builder clone()
        {
          return create().mergeFrom(buildPartial());
        }

        public DescriptorProtos.DescriptorProto.ExtensionRange getDefaultInstanceForType()
        {
          return DescriptorProtos.DescriptorProto.ExtensionRange.getDefaultInstance();
        }

        public Descriptors.Descriptor getDescriptorForType()
        {
          return DescriptorProtos.DescriptorProto.ExtensionRange.getDescriptor();
        }

        public int getEnd()
        {
          return this.end_;
        }

        public int getStart()
        {
          return this.start_;
        }

        public boolean hasEnd()
        {
          return (0x2 & this.bitField0_) == 2;
        }

        public boolean hasStart()
        {
          return (0x1 & this.bitField0_) == 1;
        }

        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
        {
          return DescriptorProtos.internal_static_google_protobuf_DescriptorProto_ExtensionRange_fieldAccessorTable;
        }

        public final boolean isInitialized()
        {
          return true;
        }

        public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
          throws IOException
        {
          UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
          while (true)
          {
            int i = paramCodedInputStream.readTag();
            switch (i)
            {
            default:
              if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
              {
                setUnknownFields(localBuilder.build());
                onChanged();
                return this;
              }
              break;
            case 0:
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            case 8:
              this.bitField0_ = (0x1 | this.bitField0_);
              this.start_ = paramCodedInputStream.readInt32();
              break;
            case 16:
              this.bitField0_ = (0x2 | this.bitField0_);
              this.end_ = paramCodedInputStream.readInt32();
            }
          }
        }

        public Builder mergeFrom(DescriptorProtos.DescriptorProto.ExtensionRange paramExtensionRange)
        {
          if (paramExtensionRange == DescriptorProtos.DescriptorProto.ExtensionRange.getDefaultInstance())
            return this;
          if (paramExtensionRange.hasStart())
            setStart(paramExtensionRange.getStart());
          if (paramExtensionRange.hasEnd())
            setEnd(paramExtensionRange.getEnd());
          mergeUnknownFields(paramExtensionRange.getUnknownFields());
          return this;
        }

        public Builder mergeFrom(Message paramMessage)
        {
          if ((paramMessage instanceof DescriptorProtos.DescriptorProto.ExtensionRange))
            return mergeFrom((DescriptorProtos.DescriptorProto.ExtensionRange)paramMessage);
          super.mergeFrom(paramMessage);
          return this;
        }

        public Builder setEnd(int paramInt)
        {
          this.bitField0_ = (0x2 | this.bitField0_);
          this.end_ = paramInt;
          onChanged();
          return this;
        }

        public Builder setStart(int paramInt)
        {
          this.bitField0_ = (0x1 | this.bitField0_);
          this.start_ = paramInt;
          onChanged();
          return this;
        }
      }
    }

    public static abstract interface ExtensionRangeOrBuilder extends MessageOrBuilder
    {
      public abstract int getEnd();

      public abstract int getStart();

      public abstract boolean hasEnd();

      public abstract boolean hasStart();
    }
  }

  public static abstract interface DescriptorProtoOrBuilder extends MessageOrBuilder
  {
    public abstract DescriptorProtos.EnumDescriptorProto getEnumType(int paramInt);

    public abstract int getEnumTypeCount();

    public abstract List<DescriptorProtos.EnumDescriptorProto> getEnumTypeList();

    public abstract DescriptorProtos.EnumDescriptorProtoOrBuilder getEnumTypeOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.EnumDescriptorProtoOrBuilder> getEnumTypeOrBuilderList();

    public abstract DescriptorProtos.FieldDescriptorProto getExtension(int paramInt);

    public abstract int getExtensionCount();

    public abstract List<DescriptorProtos.FieldDescriptorProto> getExtensionList();

    public abstract DescriptorProtos.FieldDescriptorProtoOrBuilder getExtensionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getExtensionOrBuilderList();

    public abstract DescriptorProtos.DescriptorProto.ExtensionRange getExtensionRange(int paramInt);

    public abstract int getExtensionRangeCount();

    public abstract List<DescriptorProtos.DescriptorProto.ExtensionRange> getExtensionRangeList();

    public abstract DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder getExtensionRangeOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.DescriptorProto.ExtensionRangeOrBuilder> getExtensionRangeOrBuilderList();

    public abstract DescriptorProtos.FieldDescriptorProto getField(int paramInt);

    public abstract int getFieldCount();

    public abstract List<DescriptorProtos.FieldDescriptorProto> getFieldList();

    public abstract DescriptorProtos.FieldDescriptorProtoOrBuilder getFieldOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getFieldOrBuilderList();

    public abstract String getName();

    public abstract DescriptorProtos.DescriptorProto getNestedType(int paramInt);

    public abstract int getNestedTypeCount();

    public abstract List<DescriptorProtos.DescriptorProto> getNestedTypeList();

    public abstract DescriptorProtoOrBuilder getNestedTypeOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtoOrBuilder> getNestedTypeOrBuilderList();

    public abstract DescriptorProtos.MessageOptions getOptions();

    public abstract DescriptorProtos.MessageOptionsOrBuilder getOptionsOrBuilder();

    public abstract boolean hasName();

    public abstract boolean hasOptions();
  }

  public static final class EnumDescriptorProto extends GeneratedMessage
    implements DescriptorProtos.EnumDescriptorProtoOrBuilder
  {
    public static final int NAME_FIELD_NUMBER = 1;
    public static final int OPTIONS_FIELD_NUMBER = 3;
    public static final int VALUE_FIELD_NUMBER = 2;
    private static final EnumDescriptorProto defaultInstance = new EnumDescriptorProto(true);
    private int bitField0_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object name_;
    private DescriptorProtos.EnumOptions options_;
    private List<DescriptorProtos.EnumValueDescriptorProto> value_;

    static
    {
      defaultInstance.initFields();
    }

    private EnumDescriptorProto(Builder paramBuilder)
    {
      super();
    }

    private EnumDescriptorProto(boolean paramBoolean)
    {
    }

    public static EnumDescriptorProto getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_EnumDescriptorProto_descriptor;
    }

    private ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.name_ = "";
      this.value_ = Collections.emptyList();
      this.options_ = DescriptorProtos.EnumOptions.getDefaultInstance();
    }

    public static Builder newBuilder()
    {
      return Builder.access$6400();
    }

    public static Builder newBuilder(EnumDescriptorProto paramEnumDescriptorProto)
    {
      return newBuilder().mergeFrom(paramEnumDescriptorProto);
    }

    public static EnumDescriptorProto parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static EnumDescriptorProto parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static EnumDescriptorProto parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static EnumDescriptorProto parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static EnumDescriptorProto parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static EnumDescriptorProto parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static EnumDescriptorProto parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static EnumDescriptorProto parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static EnumDescriptorProto parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static EnumDescriptorProto parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public EnumDescriptorProto getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.name_ = str;
      return str;
    }

    public DescriptorProtos.EnumOptions getOptions()
    {
      return this.options_;
    }

    public DescriptorProtos.EnumOptionsOrBuilder getOptionsOrBuilder()
    {
      return this.options_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      for (int m = 0; m < this.value_.size(); m++)
        k += CodedOutputStream.computeMessageSize(2, (MessageLite)this.value_.get(m));
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeMessageSize(3, this.options_);
      int n = k + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = n;
      return n;
    }

    public DescriptorProtos.EnumValueDescriptorProto getValue(int paramInt)
    {
      return (DescriptorProtos.EnumValueDescriptorProto)this.value_.get(paramInt);
    }

    public int getValueCount()
    {
      return this.value_.size();
    }

    public List<DescriptorProtos.EnumValueDescriptorProto> getValueList()
    {
      return this.value_;
    }

    public DescriptorProtos.EnumValueDescriptorProtoOrBuilder getValueOrBuilder(int paramInt)
    {
      return (DescriptorProtos.EnumValueDescriptorProtoOrBuilder)this.value_.get(paramInt);
    }

    public List<? extends DescriptorProtos.EnumValueDescriptorProtoOrBuilder> getValueOrBuilderList()
    {
      return this.value_;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasOptions()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_EnumDescriptorProto_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getValueCount(); j++)
        if (!getValue(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if ((hasOptions()) && (!getOptions().isInitialized()))
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      for (int i = 0; i < this.value_.size(); i++)
        paramCodedOutputStream.writeMessage(2, (MessageLite)this.value_.get(i));
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeMessage(3, this.options_);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.EnumDescriptorProtoOrBuilder
    {
      private int bitField0_;
      private Object name_ = "";
      private SingleFieldBuilder<DescriptorProtos.EnumOptions, DescriptorProtos.EnumOptions.Builder, DescriptorProtos.EnumOptionsOrBuilder> optionsBuilder_;
      private DescriptorProtos.EnumOptions options_ = DescriptorProtos.EnumOptions.getDefaultInstance();
      private RepeatedFieldBuilder<DescriptorProtos.EnumValueDescriptorProto, DescriptorProtos.EnumValueDescriptorProto.Builder, DescriptorProtos.EnumValueDescriptorProtoOrBuilder> valueBuilder_;
      private List<DescriptorProtos.EnumValueDescriptorProto> value_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.EnumDescriptorProto buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.EnumDescriptorProto localEnumDescriptorProto = buildPartial();
        if (!localEnumDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localEnumDescriptorProto).asInvalidProtocolBufferException();
        return localEnumDescriptorProto;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureValueIsMutable()
      {
        if ((0x2 & this.bitField0_) != 2)
        {
          this.value_ = new ArrayList(this.value_);
          this.bitField0_ = (0x2 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_EnumDescriptorProto_descriptor;
      }

      private SingleFieldBuilder<DescriptorProtos.EnumOptions, DescriptorProtos.EnumOptions.Builder, DescriptorProtos.EnumOptionsOrBuilder> getOptionsFieldBuilder()
      {
        if (this.optionsBuilder_ == null)
        {
          this.optionsBuilder_ = new SingleFieldBuilder(this.options_, getParentForChildren(), isClean());
          this.options_ = null;
        }
        return this.optionsBuilder_;
      }

      private RepeatedFieldBuilder<DescriptorProtos.EnumValueDescriptorProto, DescriptorProtos.EnumValueDescriptorProto.Builder, DescriptorProtos.EnumValueDescriptorProtoOrBuilder> getValueFieldBuilder()
      {
        List localList;
        if (this.valueBuilder_ == null)
        {
          localList = this.value_;
          if ((0x2 & this.bitField0_) != 2)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.valueBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.value_ = null;
          return this.valueBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
        {
          getValueFieldBuilder();
          getOptionsFieldBuilder();
        }
      }

      public Builder addAllValue(Iterable<? extends DescriptorProtos.EnumValueDescriptorProto> paramIterable)
      {
        if (this.valueBuilder_ == null)
        {
          ensureValueIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.value_);
          onChanged();
          return this;
        }
        this.valueBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addValue(int paramInt, DescriptorProtos.EnumValueDescriptorProto.Builder paramBuilder)
      {
        if (this.valueBuilder_ == null)
        {
          ensureValueIsMutable();
          this.value_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.valueBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addValue(int paramInt, DescriptorProtos.EnumValueDescriptorProto paramEnumValueDescriptorProto)
      {
        if (this.valueBuilder_ == null)
        {
          if (paramEnumValueDescriptorProto == null)
            throw new NullPointerException();
          ensureValueIsMutable();
          this.value_.add(paramInt, paramEnumValueDescriptorProto);
          onChanged();
          return this;
        }
        this.valueBuilder_.addMessage(paramInt, paramEnumValueDescriptorProto);
        return this;
      }

      public Builder addValue(DescriptorProtos.EnumValueDescriptorProto.Builder paramBuilder)
      {
        if (this.valueBuilder_ == null)
        {
          ensureValueIsMutable();
          this.value_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.valueBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addValue(DescriptorProtos.EnumValueDescriptorProto paramEnumValueDescriptorProto)
      {
        if (this.valueBuilder_ == null)
        {
          if (paramEnumValueDescriptorProto == null)
            throw new NullPointerException();
          ensureValueIsMutable();
          this.value_.add(paramEnumValueDescriptorProto);
          onChanged();
          return this;
        }
        this.valueBuilder_.addMessage(paramEnumValueDescriptorProto);
        return this;
      }

      public DescriptorProtos.EnumValueDescriptorProto.Builder addValueBuilder()
      {
        return (DescriptorProtos.EnumValueDescriptorProto.Builder)getValueFieldBuilder().addBuilder(DescriptorProtos.EnumValueDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.EnumValueDescriptorProto.Builder addValueBuilder(int paramInt)
      {
        return (DescriptorProtos.EnumValueDescriptorProto.Builder)getValueFieldBuilder().addBuilder(paramInt, DescriptorProtos.EnumValueDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.EnumDescriptorProto build()
      {
        DescriptorProtos.EnumDescriptorProto localEnumDescriptorProto = buildPartial();
        if (!localEnumDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localEnumDescriptorProto);
        return localEnumDescriptorProto;
      }

      public DescriptorProtos.EnumDescriptorProto buildPartial()
      {
        DescriptorProtos.EnumDescriptorProto localEnumDescriptorProto = new DescriptorProtos.EnumDescriptorProto(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.EnumDescriptorProto.access$6702(localEnumDescriptorProto, this.name_);
        if (this.valueBuilder_ == null)
        {
          if ((0x2 & this.bitField0_) == 2)
          {
            this.value_ = Collections.unmodifiableList(this.value_);
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          }
          DescriptorProtos.EnumDescriptorProto.access$6802(localEnumDescriptorProto, this.value_);
          if ((i & 0x4) == 4)
            k |= 2;
          if (this.optionsBuilder_ != null)
            break label146;
          DescriptorProtos.EnumDescriptorProto.access$6902(localEnumDescriptorProto, this.options_);
        }
        while (true)
        {
          DescriptorProtos.EnumDescriptorProto.access$7002(localEnumDescriptorProto, k);
          onBuilt();
          return localEnumDescriptorProto;
          DescriptorProtos.EnumDescriptorProto.access$6802(localEnumDescriptorProto, this.valueBuilder_.build());
          break;
          label146: DescriptorProtos.EnumDescriptorProto.access$6902(localEnumDescriptorProto, (DescriptorProtos.EnumOptions)this.optionsBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        if (this.valueBuilder_ == null)
        {
          this.value_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          if (this.optionsBuilder_ != null)
            break label84;
          this.options_ = DescriptorProtos.EnumOptions.getDefaultInstance();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          return this;
          this.valueBuilder_.clear();
          break;
          label84: this.optionsBuilder_.clear();
        }
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = DescriptorProtos.EnumDescriptorProto.getDefaultInstance().getName();
        onChanged();
        return this;
      }

      public Builder clearOptions()
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = DescriptorProtos.EnumOptions.getDefaultInstance();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clearValue()
      {
        if (this.valueBuilder_ == null)
        {
          this.value_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          onChanged();
          return this;
        }
        this.valueBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.EnumDescriptorProto getDefaultInstanceForType()
      {
        return DescriptorProtos.EnumDescriptorProto.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.EnumDescriptorProto.getDescriptor();
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.EnumOptions getOptions()
      {
        if (this.optionsBuilder_ == null)
          return this.options_;
        return (DescriptorProtos.EnumOptions)this.optionsBuilder_.getMessage();
      }

      public DescriptorProtos.EnumOptions.Builder getOptionsBuilder()
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        onChanged();
        return (DescriptorProtos.EnumOptions.Builder)getOptionsFieldBuilder().getBuilder();
      }

      public DescriptorProtos.EnumOptionsOrBuilder getOptionsOrBuilder()
      {
        if (this.optionsBuilder_ != null)
          return (DescriptorProtos.EnumOptionsOrBuilder)this.optionsBuilder_.getMessageOrBuilder();
        return this.options_;
      }

      public DescriptorProtos.EnumValueDescriptorProto getValue(int paramInt)
      {
        if (this.valueBuilder_ == null)
          return (DescriptorProtos.EnumValueDescriptorProto)this.value_.get(paramInt);
        return (DescriptorProtos.EnumValueDescriptorProto)this.valueBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.EnumValueDescriptorProto.Builder getValueBuilder(int paramInt)
      {
        return (DescriptorProtos.EnumValueDescriptorProto.Builder)getValueFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.EnumValueDescriptorProto.Builder> getValueBuilderList()
      {
        return getValueFieldBuilder().getBuilderList();
      }

      public int getValueCount()
      {
        if (this.valueBuilder_ == null)
          return this.value_.size();
        return this.valueBuilder_.getCount();
      }

      public List<DescriptorProtos.EnumValueDescriptorProto> getValueList()
      {
        if (this.valueBuilder_ == null)
          return Collections.unmodifiableList(this.value_);
        return this.valueBuilder_.getMessageList();
      }

      public DescriptorProtos.EnumValueDescriptorProtoOrBuilder getValueOrBuilder(int paramInt)
      {
        if (this.valueBuilder_ == null)
          return (DescriptorProtos.EnumValueDescriptorProtoOrBuilder)this.value_.get(paramInt);
        return (DescriptorProtos.EnumValueDescriptorProtoOrBuilder)this.valueBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.EnumValueDescriptorProtoOrBuilder> getValueOrBuilderList()
      {
        if (this.valueBuilder_ != null)
          return this.valueBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.value_);
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasOptions()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_EnumDescriptorProto_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getValueCount(); i++)
          if (!getValue(i).isInitialized())
            return false;
        return (!hasOptions()) || (getOptions().isInitialized());
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            DescriptorProtos.EnumValueDescriptorProto.Builder localBuilder2 = DescriptorProtos.EnumValueDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder2, paramExtensionRegistryLite);
            addValue(localBuilder2.buildPartial());
            break;
          case 26:
            DescriptorProtos.EnumOptions.Builder localBuilder1 = DescriptorProtos.EnumOptions.newBuilder();
            if (hasOptions())
              localBuilder1.mergeFrom(getOptions());
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            setOptions(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.EnumDescriptorProto paramEnumDescriptorProto)
      {
        if (paramEnumDescriptorProto == DescriptorProtos.EnumDescriptorProto.getDefaultInstance())
          return this;
        if (paramEnumDescriptorProto.hasName())
          setName(paramEnumDescriptorProto.getName());
        if (this.valueBuilder_ == null)
          if (!paramEnumDescriptorProto.value_.isEmpty())
          {
            if (!this.value_.isEmpty())
              break label106;
            this.value_ = paramEnumDescriptorProto.value_;
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          if (paramEnumDescriptorProto.hasOptions())
            mergeOptions(paramEnumDescriptorProto.getOptions());
          mergeUnknownFields(paramEnumDescriptorProto.getUnknownFields());
          return this;
          label106: ensureValueIsMutable();
          this.value_.addAll(paramEnumDescriptorProto.value_);
          break;
          if (!paramEnumDescriptorProto.value_.isEmpty())
          {
            if (this.valueBuilder_.isEmpty())
            {
              this.valueBuilder_.dispose();
              this.valueBuilder_ = null;
              this.value_ = paramEnumDescriptorProto.value_;
              this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getValueFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.valueBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.valueBuilder_.addAllMessages(paramEnumDescriptorProto.value_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.EnumDescriptorProto))
          return mergeFrom((DescriptorProtos.EnumDescriptorProto)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder mergeOptions(DescriptorProtos.EnumOptions paramEnumOptions)
      {
        if (this.optionsBuilder_ == null)
          if (((0x4 & this.bitField0_) == 4) && (this.options_ != DescriptorProtos.EnumOptions.getDefaultInstance()))
          {
            this.options_ = DescriptorProtos.EnumOptions.newBuilder(this.options_).mergeFrom(paramEnumOptions).buildPartial();
            onChanged();
          }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.options_ = paramEnumOptions;
          break;
          this.optionsBuilder_.mergeFrom(paramEnumOptions);
        }
      }

      public Builder removeValue(int paramInt)
      {
        if (this.valueBuilder_ == null)
        {
          ensureValueIsMutable();
          this.value_.remove(paramInt);
          onChanged();
          return this;
        }
        this.valueBuilder_.remove(paramInt);
        return this;
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        onChanged();
        return this;
      }

      void setName(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        onChanged();
      }

      public Builder setOptions(DescriptorProtos.EnumOptions.Builder paramBuilder)
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = paramBuilder.build();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramBuilder.build());
        }
      }

      public Builder setOptions(DescriptorProtos.EnumOptions paramEnumOptions)
      {
        if (this.optionsBuilder_ == null)
        {
          if (paramEnumOptions == null)
            throw new NullPointerException();
          this.options_ = paramEnumOptions;
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramEnumOptions);
        }
      }

      public Builder setValue(int paramInt, DescriptorProtos.EnumValueDescriptorProto.Builder paramBuilder)
      {
        if (this.valueBuilder_ == null)
        {
          ensureValueIsMutable();
          this.value_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.valueBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setValue(int paramInt, DescriptorProtos.EnumValueDescriptorProto paramEnumValueDescriptorProto)
      {
        if (this.valueBuilder_ == null)
        {
          if (paramEnumValueDescriptorProto == null)
            throw new NullPointerException();
          ensureValueIsMutable();
          this.value_.set(paramInt, paramEnumValueDescriptorProto);
          onChanged();
          return this;
        }
        this.valueBuilder_.setMessage(paramInt, paramEnumValueDescriptorProto);
        return this;
      }
    }
  }

  public static abstract interface EnumDescriptorProtoOrBuilder extends MessageOrBuilder
  {
    public abstract String getName();

    public abstract DescriptorProtos.EnumOptions getOptions();

    public abstract DescriptorProtos.EnumOptionsOrBuilder getOptionsOrBuilder();

    public abstract DescriptorProtos.EnumValueDescriptorProto getValue(int paramInt);

    public abstract int getValueCount();

    public abstract List<DescriptorProtos.EnumValueDescriptorProto> getValueList();

    public abstract DescriptorProtos.EnumValueDescriptorProtoOrBuilder getValueOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.EnumValueDescriptorProtoOrBuilder> getValueOrBuilderList();

    public abstract boolean hasName();

    public abstract boolean hasOptions();
  }

  public static final class EnumOptions extends GeneratedMessage.ExtendableMessage<EnumOptions>
    implements DescriptorProtos.EnumOptionsOrBuilder
  {
    public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
    private static final EnumOptions defaultInstance = new EnumOptions(true);
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_;

    static
    {
      defaultInstance.initFields();
    }

    private EnumOptions(Builder paramBuilder)
    {
      super();
    }

    private EnumOptions(boolean paramBoolean)
    {
    }

    public static EnumOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_EnumOptions_descriptor;
    }

    private void initFields()
    {
      this.uninterpretedOption_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$14300();
    }

    public static Builder newBuilder(EnumOptions paramEnumOptions)
    {
      return newBuilder().mergeFrom(paramEnumOptions);
    }

    public static EnumOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static EnumOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static EnumOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static EnumOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static EnumOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static EnumOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static EnumOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static EnumOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static EnumOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static EnumOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public EnumOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.uninterpretedOption_.size(); k++)
        j += CodedOutputStream.computeMessageSize(999, (MessageLite)this.uninterpretedOption_.get(k));
      int m = j + extensionsSerializedSize() + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
    }

    public int getUninterpretedOptionCount()
    {
      return this.uninterpretedOption_.size();
    }

    public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
    {
      return this.uninterpretedOption_;
    }

    public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
    }

    public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
    {
      return this.uninterpretedOption_;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_EnumOptions_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getUninterpretedOptionCount(); j++)
        if (!getUninterpretedOption(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessage.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      for (int i = 0; i < this.uninterpretedOption_.size(); i++)
        paramCodedOutputStream.writeMessage(999, (MessageLite)this.uninterpretedOption_.get(i));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.ExtendableBuilder<DescriptorProtos.EnumOptions, Builder>
      implements DescriptorProtos.EnumOptionsOrBuilder
    {
      private int bitField0_;
      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> uninterpretedOptionBuilder_;
      private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.EnumOptions buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.EnumOptions localEnumOptions = buildPartial();
        if (!localEnumOptions.isInitialized())
          throw newUninitializedMessageException(localEnumOptions).asInvalidProtocolBufferException();
        return localEnumOptions;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureUninterpretedOptionIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.uninterpretedOption_ = new ArrayList(this.uninterpretedOption_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_EnumOptions_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionFieldBuilder()
      {
        List localList;
        if (this.uninterpretedOptionBuilder_ == null)
        {
          localList = this.uninterpretedOption_;
          if ((0x1 & this.bitField0_) != 1)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.uninterpretedOptionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.uninterpretedOption_ = null;
          return this.uninterpretedOptionBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getUninterpretedOptionFieldBuilder();
      }

      public Builder addAllUninterpretedOption(Iterable<? extends DescriptorProtos.UninterpretedOption> paramIterable)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          GeneratedMessage.ExtendableBuilder.addAll(paramIterable, this.uninterpretedOption_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramUninterpretedOption);
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramUninterpretedOption);
        return this;
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder()
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(paramInt, DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.EnumOptions build()
      {
        DescriptorProtos.EnumOptions localEnumOptions = buildPartial();
        if (!localEnumOptions.isInitialized())
          throw newUninitializedMessageException(localEnumOptions);
        return localEnumOptions;
      }

      public DescriptorProtos.EnumOptions buildPartial()
      {
        DescriptorProtos.EnumOptions localEnumOptions = new DescriptorProtos.EnumOptions(this, null);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if ((0x1 & this.bitField0_) == 1)
          {
            this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
          DescriptorProtos.EnumOptions.access$14602(localEnumOptions, this.uninterpretedOption_);
        }
        while (true)
        {
          onBuilt();
          return localEnumOptions;
          DescriptorProtos.EnumOptions.access$14602(localEnumOptions, this.uninterpretedOptionBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clearUninterpretedOption()
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.EnumOptions getDefaultInstanceForType()
      {
        return DescriptorProtos.EnumOptions.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.EnumOptions.getDescriptor();
      }

      public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption)this.uninterpretedOptionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.UninterpretedOption.Builder getUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.UninterpretedOption.Builder> getUninterpretedOptionBuilderList()
      {
        return getUninterpretedOptionFieldBuilder().getBuilderList();
      }

      public int getUninterpretedOptionCount()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return this.uninterpretedOption_.size();
        return this.uninterpretedOptionBuilder_.getCount();
      }

      public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return Collections.unmodifiableList(this.uninterpretedOption_);
        return this.uninterpretedOptionBuilder_.getMessageList();
      }

      public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOptionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
      {
        if (this.uninterpretedOptionBuilder_ != null)
          return this.uninterpretedOptionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.uninterpretedOption_);
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_EnumOptions_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getUninterpretedOptionCount(); i++)
          if (!getUninterpretedOption(i).isInitialized())
            return false;
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 7994:
            DescriptorProtos.UninterpretedOption.Builder localBuilder1 = DescriptorProtos.UninterpretedOption.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addUninterpretedOption(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.EnumOptions paramEnumOptions)
      {
        if (paramEnumOptions == DescriptorProtos.EnumOptions.getDefaultInstance())
          return this;
        if (this.uninterpretedOptionBuilder_ == null)
          if (!paramEnumOptions.uninterpretedOption_.isEmpty())
          {
            if (!this.uninterpretedOption_.isEmpty())
              break label79;
            this.uninterpretedOption_ = paramEnumOptions.uninterpretedOption_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeExtensionFields(paramEnumOptions);
          mergeUnknownFields(paramEnumOptions.getUnknownFields());
          return this;
          label79: ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.addAll(paramEnumOptions.uninterpretedOption_);
          break;
          if (!paramEnumOptions.uninterpretedOption_.isEmpty())
          {
            if (this.uninterpretedOptionBuilder_.isEmpty())
            {
              this.uninterpretedOptionBuilder_.dispose();
              this.uninterpretedOptionBuilder_ = null;
              this.uninterpretedOption_ = paramEnumOptions.uninterpretedOption_;
              this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getUninterpretedOptionFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.uninterpretedOptionBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.uninterpretedOptionBuilder_.addAllMessages(paramEnumOptions.uninterpretedOption_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.EnumOptions))
          return mergeFrom((DescriptorProtos.EnumOptions)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.remove(paramInt);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.remove(paramInt);
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramUninterpretedOption);
        return this;
      }
    }
  }

  public static abstract interface EnumOptionsOrBuilder extends GeneratedMessage.ExtendableMessageOrBuilder<DescriptorProtos.EnumOptions>
  {
    public abstract DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt);

    public abstract int getUninterpretedOptionCount();

    public abstract List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList();

    public abstract DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList();
  }

  public static final class EnumValueDescriptorProto extends GeneratedMessage
    implements DescriptorProtos.EnumValueDescriptorProtoOrBuilder
  {
    public static final int NAME_FIELD_NUMBER = 1;
    public static final int NUMBER_FIELD_NUMBER = 2;
    public static final int OPTIONS_FIELD_NUMBER = 3;
    private static final EnumValueDescriptorProto defaultInstance = new EnumValueDescriptorProto(true);
    private int bitField0_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object name_;
    private int number_;
    private DescriptorProtos.EnumValueOptions options_;

    static
    {
      defaultInstance.initFields();
    }

    private EnumValueDescriptorProto(Builder paramBuilder)
    {
      super();
    }

    private EnumValueDescriptorProto(boolean paramBoolean)
    {
    }

    public static EnumValueDescriptorProto getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_EnumValueDescriptorProto_descriptor;
    }

    private ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.name_ = "";
      this.number_ = 0;
      this.options_ = DescriptorProtos.EnumValueOptions.getDefaultInstance();
    }

    public static Builder newBuilder()
    {
      return Builder.access$7400();
    }

    public static Builder newBuilder(EnumValueDescriptorProto paramEnumValueDescriptorProto)
    {
      return newBuilder().mergeFrom(paramEnumValueDescriptorProto);
    }

    public static EnumValueDescriptorProto parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static EnumValueDescriptorProto parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static EnumValueDescriptorProto parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static EnumValueDescriptorProto parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static EnumValueDescriptorProto parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static EnumValueDescriptorProto parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static EnumValueDescriptorProto parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static EnumValueDescriptorProto parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static EnumValueDescriptorProto parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static EnumValueDescriptorProto parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public EnumValueDescriptorProto getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.name_ = str;
      return str;
    }

    public int getNumber()
    {
      return this.number_;
    }

    public DescriptorProtos.EnumValueOptions getOptions()
    {
      return this.options_;
    }

    public DescriptorProtos.EnumValueOptionsOrBuilder getOptionsOrBuilder()
    {
      return this.options_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeInt32Size(2, this.number_);
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeMessageSize(3, this.options_);
      int m = k + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasNumber()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasOptions()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_EnumValueDescriptorProto_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      if ((hasOptions()) && (!getOptions().isInitialized()))
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeInt32(2, this.number_);
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeMessage(3, this.options_);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.EnumValueDescriptorProtoOrBuilder
    {
      private int bitField0_;
      private Object name_ = "";
      private int number_;
      private SingleFieldBuilder<DescriptorProtos.EnumValueOptions, DescriptorProtos.EnumValueOptions.Builder, DescriptorProtos.EnumValueOptionsOrBuilder> optionsBuilder_;
      private DescriptorProtos.EnumValueOptions options_ = DescriptorProtos.EnumValueOptions.getDefaultInstance();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.EnumValueDescriptorProto buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.EnumValueDescriptorProto localEnumValueDescriptorProto = buildPartial();
        if (!localEnumValueDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localEnumValueDescriptorProto).asInvalidProtocolBufferException();
        return localEnumValueDescriptorProto;
      }

      private static Builder create()
      {
        return new Builder();
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_EnumValueDescriptorProto_descriptor;
      }

      private SingleFieldBuilder<DescriptorProtos.EnumValueOptions, DescriptorProtos.EnumValueOptions.Builder, DescriptorProtos.EnumValueOptionsOrBuilder> getOptionsFieldBuilder()
      {
        if (this.optionsBuilder_ == null)
        {
          this.optionsBuilder_ = new SingleFieldBuilder(this.options_, getParentForChildren(), isClean());
          this.options_ = null;
        }
        return this.optionsBuilder_;
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getOptionsFieldBuilder();
      }

      public DescriptorProtos.EnumValueDescriptorProto build()
      {
        DescriptorProtos.EnumValueDescriptorProto localEnumValueDescriptorProto = buildPartial();
        if (!localEnumValueDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localEnumValueDescriptorProto);
        return localEnumValueDescriptorProto;
      }

      public DescriptorProtos.EnumValueDescriptorProto buildPartial()
      {
        DescriptorProtos.EnumValueDescriptorProto localEnumValueDescriptorProto = new DescriptorProtos.EnumValueDescriptorProto(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.EnumValueDescriptorProto.access$7702(localEnumValueDescriptorProto, this.name_);
        if ((i & 0x2) == 2)
          k |= 2;
        DescriptorProtos.EnumValueDescriptorProto.access$7802(localEnumValueDescriptorProto, this.number_);
        if ((i & 0x4) == 4)
          k |= 4;
        if (this.optionsBuilder_ == null)
          DescriptorProtos.EnumValueDescriptorProto.access$7902(localEnumValueDescriptorProto, this.options_);
        while (true)
        {
          DescriptorProtos.EnumValueDescriptorProto.access$8002(localEnumValueDescriptorProto, k);
          onBuilt();
          return localEnumValueDescriptorProto;
          DescriptorProtos.EnumValueDescriptorProto.access$7902(localEnumValueDescriptorProto, (DescriptorProtos.EnumValueOptions)this.optionsBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.number_ = 0;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        if (this.optionsBuilder_ == null)
          this.options_ = DescriptorProtos.EnumValueOptions.getDefaultInstance();
        while (true)
        {
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = DescriptorProtos.EnumValueDescriptorProto.getDefaultInstance().getName();
        onChanged();
        return this;
      }

      public Builder clearNumber()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.number_ = 0;
        onChanged();
        return this;
      }

      public Builder clearOptions()
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = DescriptorProtos.EnumValueOptions.getDefaultInstance();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.EnumValueDescriptorProto getDefaultInstanceForType()
      {
        return DescriptorProtos.EnumValueDescriptorProto.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.EnumValueDescriptorProto.getDescriptor();
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public int getNumber()
      {
        return this.number_;
      }

      public DescriptorProtos.EnumValueOptions getOptions()
      {
        if (this.optionsBuilder_ == null)
          return this.options_;
        return (DescriptorProtos.EnumValueOptions)this.optionsBuilder_.getMessage();
      }

      public DescriptorProtos.EnumValueOptions.Builder getOptionsBuilder()
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        onChanged();
        return (DescriptorProtos.EnumValueOptions.Builder)getOptionsFieldBuilder().getBuilder();
      }

      public DescriptorProtos.EnumValueOptionsOrBuilder getOptionsOrBuilder()
      {
        if (this.optionsBuilder_ != null)
          return (DescriptorProtos.EnumValueOptionsOrBuilder)this.optionsBuilder_.getMessageOrBuilder();
        return this.options_;
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasNumber()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasOptions()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_EnumValueDescriptorProto_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        return (!hasOptions()) || (getOptions().isInitialized());
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramCodedInputStream.readBytes();
            break;
          case 16:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.number_ = paramCodedInputStream.readInt32();
            break;
          case 26:
            DescriptorProtos.EnumValueOptions.Builder localBuilder1 = DescriptorProtos.EnumValueOptions.newBuilder();
            if (hasOptions())
              localBuilder1.mergeFrom(getOptions());
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            setOptions(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.EnumValueDescriptorProto paramEnumValueDescriptorProto)
      {
        if (paramEnumValueDescriptorProto == DescriptorProtos.EnumValueDescriptorProto.getDefaultInstance())
          return this;
        if (paramEnumValueDescriptorProto.hasName())
          setName(paramEnumValueDescriptorProto.getName());
        if (paramEnumValueDescriptorProto.hasNumber())
          setNumber(paramEnumValueDescriptorProto.getNumber());
        if (paramEnumValueDescriptorProto.hasOptions())
          mergeOptions(paramEnumValueDescriptorProto.getOptions());
        mergeUnknownFields(paramEnumValueDescriptorProto.getUnknownFields());
        return this;
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.EnumValueDescriptorProto))
          return mergeFrom((DescriptorProtos.EnumValueDescriptorProto)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder mergeOptions(DescriptorProtos.EnumValueOptions paramEnumValueOptions)
      {
        if (this.optionsBuilder_ == null)
          if (((0x4 & this.bitField0_) == 4) && (this.options_ != DescriptorProtos.EnumValueOptions.getDefaultInstance()))
          {
            this.options_ = DescriptorProtos.EnumValueOptions.newBuilder(this.options_).mergeFrom(paramEnumValueOptions).buildPartial();
            onChanged();
          }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.options_ = paramEnumValueOptions;
          break;
          this.optionsBuilder_.mergeFrom(paramEnumValueOptions);
        }
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        onChanged();
        return this;
      }

      void setName(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        onChanged();
      }

      public Builder setNumber(int paramInt)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.number_ = paramInt;
        onChanged();
        return this;
      }

      public Builder setOptions(DescriptorProtos.EnumValueOptions.Builder paramBuilder)
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = paramBuilder.build();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramBuilder.build());
        }
      }

      public Builder setOptions(DescriptorProtos.EnumValueOptions paramEnumValueOptions)
      {
        if (this.optionsBuilder_ == null)
        {
          if (paramEnumValueOptions == null)
            throw new NullPointerException();
          this.options_ = paramEnumValueOptions;
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramEnumValueOptions);
        }
      }
    }
  }

  public static abstract interface EnumValueDescriptorProtoOrBuilder extends MessageOrBuilder
  {
    public abstract String getName();

    public abstract int getNumber();

    public abstract DescriptorProtos.EnumValueOptions getOptions();

    public abstract DescriptorProtos.EnumValueOptionsOrBuilder getOptionsOrBuilder();

    public abstract boolean hasName();

    public abstract boolean hasNumber();

    public abstract boolean hasOptions();
  }

  public static final class EnumValueOptions extends GeneratedMessage.ExtendableMessage<EnumValueOptions>
    implements DescriptorProtos.EnumValueOptionsOrBuilder
  {
    public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
    private static final EnumValueOptions defaultInstance = new EnumValueOptions(true);
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_;

    static
    {
      defaultInstance.initFields();
    }

    private EnumValueOptions(Builder paramBuilder)
    {
      super();
    }

    private EnumValueOptions(boolean paramBoolean)
    {
    }

    public static EnumValueOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_EnumValueOptions_descriptor;
    }

    private void initFields()
    {
      this.uninterpretedOption_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$15000();
    }

    public static Builder newBuilder(EnumValueOptions paramEnumValueOptions)
    {
      return newBuilder().mergeFrom(paramEnumValueOptions);
    }

    public static EnumValueOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static EnumValueOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static EnumValueOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static EnumValueOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static EnumValueOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static EnumValueOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static EnumValueOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static EnumValueOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static EnumValueOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static EnumValueOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public EnumValueOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.uninterpretedOption_.size(); k++)
        j += CodedOutputStream.computeMessageSize(999, (MessageLite)this.uninterpretedOption_.get(k));
      int m = j + extensionsSerializedSize() + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
    }

    public int getUninterpretedOptionCount()
    {
      return this.uninterpretedOption_.size();
    }

    public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
    {
      return this.uninterpretedOption_;
    }

    public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
    }

    public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
    {
      return this.uninterpretedOption_;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_EnumValueOptions_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getUninterpretedOptionCount(); j++)
        if (!getUninterpretedOption(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessage.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      for (int i = 0; i < this.uninterpretedOption_.size(); i++)
        paramCodedOutputStream.writeMessage(999, (MessageLite)this.uninterpretedOption_.get(i));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.ExtendableBuilder<DescriptorProtos.EnumValueOptions, Builder>
      implements DescriptorProtos.EnumValueOptionsOrBuilder
    {
      private int bitField0_;
      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> uninterpretedOptionBuilder_;
      private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.EnumValueOptions buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.EnumValueOptions localEnumValueOptions = buildPartial();
        if (!localEnumValueOptions.isInitialized())
          throw newUninitializedMessageException(localEnumValueOptions).asInvalidProtocolBufferException();
        return localEnumValueOptions;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureUninterpretedOptionIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.uninterpretedOption_ = new ArrayList(this.uninterpretedOption_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_EnumValueOptions_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionFieldBuilder()
      {
        List localList;
        if (this.uninterpretedOptionBuilder_ == null)
        {
          localList = this.uninterpretedOption_;
          if ((0x1 & this.bitField0_) != 1)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.uninterpretedOptionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.uninterpretedOption_ = null;
          return this.uninterpretedOptionBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getUninterpretedOptionFieldBuilder();
      }

      public Builder addAllUninterpretedOption(Iterable<? extends DescriptorProtos.UninterpretedOption> paramIterable)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          GeneratedMessage.ExtendableBuilder.addAll(paramIterable, this.uninterpretedOption_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramUninterpretedOption);
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramUninterpretedOption);
        return this;
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder()
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(paramInt, DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.EnumValueOptions build()
      {
        DescriptorProtos.EnumValueOptions localEnumValueOptions = buildPartial();
        if (!localEnumValueOptions.isInitialized())
          throw newUninitializedMessageException(localEnumValueOptions);
        return localEnumValueOptions;
      }

      public DescriptorProtos.EnumValueOptions buildPartial()
      {
        DescriptorProtos.EnumValueOptions localEnumValueOptions = new DescriptorProtos.EnumValueOptions(this, null);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if ((0x1 & this.bitField0_) == 1)
          {
            this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
          DescriptorProtos.EnumValueOptions.access$15302(localEnumValueOptions, this.uninterpretedOption_);
        }
        while (true)
        {
          onBuilt();
          return localEnumValueOptions;
          DescriptorProtos.EnumValueOptions.access$15302(localEnumValueOptions, this.uninterpretedOptionBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clearUninterpretedOption()
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.EnumValueOptions getDefaultInstanceForType()
      {
        return DescriptorProtos.EnumValueOptions.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.EnumValueOptions.getDescriptor();
      }

      public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption)this.uninterpretedOptionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.UninterpretedOption.Builder getUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.UninterpretedOption.Builder> getUninterpretedOptionBuilderList()
      {
        return getUninterpretedOptionFieldBuilder().getBuilderList();
      }

      public int getUninterpretedOptionCount()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return this.uninterpretedOption_.size();
        return this.uninterpretedOptionBuilder_.getCount();
      }

      public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return Collections.unmodifiableList(this.uninterpretedOption_);
        return this.uninterpretedOptionBuilder_.getMessageList();
      }

      public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOptionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
      {
        if (this.uninterpretedOptionBuilder_ != null)
          return this.uninterpretedOptionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.uninterpretedOption_);
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_EnumValueOptions_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getUninterpretedOptionCount(); i++)
          if (!getUninterpretedOption(i).isInitialized())
            return false;
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 7994:
            DescriptorProtos.UninterpretedOption.Builder localBuilder1 = DescriptorProtos.UninterpretedOption.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addUninterpretedOption(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.EnumValueOptions paramEnumValueOptions)
      {
        if (paramEnumValueOptions == DescriptorProtos.EnumValueOptions.getDefaultInstance())
          return this;
        if (this.uninterpretedOptionBuilder_ == null)
          if (!paramEnumValueOptions.uninterpretedOption_.isEmpty())
          {
            if (!this.uninterpretedOption_.isEmpty())
              break label79;
            this.uninterpretedOption_ = paramEnumValueOptions.uninterpretedOption_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeExtensionFields(paramEnumValueOptions);
          mergeUnknownFields(paramEnumValueOptions.getUnknownFields());
          return this;
          label79: ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.addAll(paramEnumValueOptions.uninterpretedOption_);
          break;
          if (!paramEnumValueOptions.uninterpretedOption_.isEmpty())
          {
            if (this.uninterpretedOptionBuilder_.isEmpty())
            {
              this.uninterpretedOptionBuilder_.dispose();
              this.uninterpretedOptionBuilder_ = null;
              this.uninterpretedOption_ = paramEnumValueOptions.uninterpretedOption_;
              this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getUninterpretedOptionFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.uninterpretedOptionBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.uninterpretedOptionBuilder_.addAllMessages(paramEnumValueOptions.uninterpretedOption_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.EnumValueOptions))
          return mergeFrom((DescriptorProtos.EnumValueOptions)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.remove(paramInt);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.remove(paramInt);
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramUninterpretedOption);
        return this;
      }
    }
  }

  public static abstract interface EnumValueOptionsOrBuilder extends GeneratedMessage.ExtendableMessageOrBuilder<DescriptorProtos.EnumValueOptions>
  {
    public abstract DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt);

    public abstract int getUninterpretedOptionCount();

    public abstract List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList();

    public abstract DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList();
  }

  public static final class FieldDescriptorProto extends GeneratedMessage
    implements DescriptorProtos.FieldDescriptorProtoOrBuilder
  {
    public static final int DEFAULT_VALUE_FIELD_NUMBER = 7;
    public static final int EXTENDEE_FIELD_NUMBER = 2;
    public static final int LABEL_FIELD_NUMBER = 4;
    public static final int NAME_FIELD_NUMBER = 1;
    public static final int NUMBER_FIELD_NUMBER = 3;
    public static final int OPTIONS_FIELD_NUMBER = 8;
    public static final int TYPE_FIELD_NUMBER = 5;
    public static final int TYPE_NAME_FIELD_NUMBER = 6;
    private static final FieldDescriptorProto defaultInstance = new FieldDescriptorProto(true);
    private int bitField0_;
    private Object defaultValue_;
    private Object extendee_;
    private Label label_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object name_;
    private int number_;
    private DescriptorProtos.FieldOptions options_;
    private Object typeName_;
    private Type type_;

    static
    {
      defaultInstance.initFields();
    }

    private FieldDescriptorProto(Builder paramBuilder)
    {
      super();
    }

    private FieldDescriptorProto(boolean paramBoolean)
    {
    }

    public static FieldDescriptorProto getDefaultInstance()
    {
      return defaultInstance;
    }

    private ByteString getDefaultValueBytes()
    {
      Object localObject = this.defaultValue_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.defaultValue_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_FieldDescriptorProto_descriptor;
    }

    private ByteString getExtendeeBytes()
    {
      Object localObject = this.extendee_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.extendee_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getTypeNameBytes()
    {
      Object localObject = this.typeName_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.typeName_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.name_ = "";
      this.number_ = 0;
      this.label_ = Label.LABEL_OPTIONAL;
      this.type_ = Type.TYPE_DOUBLE;
      this.typeName_ = "";
      this.extendee_ = "";
      this.defaultValue_ = "";
      this.options_ = DescriptorProtos.FieldOptions.getDefaultInstance();
    }

    public static Builder newBuilder()
    {
      return Builder.access$4900();
    }

    public static Builder newBuilder(FieldDescriptorProto paramFieldDescriptorProto)
    {
      return newBuilder().mergeFrom(paramFieldDescriptorProto);
    }

    public static FieldDescriptorProto parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static FieldDescriptorProto parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static FieldDescriptorProto parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static FieldDescriptorProto parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static FieldDescriptorProto parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static FieldDescriptorProto parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static FieldDescriptorProto parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static FieldDescriptorProto parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static FieldDescriptorProto parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static FieldDescriptorProto parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public FieldDescriptorProto getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDefaultValue()
    {
      Object localObject = this.defaultValue_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.defaultValue_ = str;
      return str;
    }

    public String getExtendee()
    {
      Object localObject = this.extendee_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.extendee_ = str;
      return str;
    }

    public Label getLabel()
    {
      return this.label_;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.name_ = str;
      return str;
    }

    public int getNumber()
    {
      return this.number_;
    }

    public DescriptorProtos.FieldOptions getOptions()
    {
      return this.options_;
    }

    public DescriptorProtos.FieldOptionsOrBuilder getOptionsOrBuilder()
    {
      return this.options_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      if ((0x20 & this.bitField0_) == 32)
        k += CodedOutputStream.computeBytesSize(2, getExtendeeBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeInt32Size(3, this.number_);
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeEnumSize(4, this.label_.getNumber());
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeEnumSize(5, this.type_.getNumber());
      if ((0x10 & this.bitField0_) == 16)
        k += CodedOutputStream.computeBytesSize(6, getTypeNameBytes());
      if ((0x40 & this.bitField0_) == 64)
        k += CodedOutputStream.computeBytesSize(7, getDefaultValueBytes());
      if ((0x80 & this.bitField0_) == 128)
        k += CodedOutputStream.computeMessageSize(8, this.options_);
      int m = k + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public Type getType()
    {
      return this.type_;
    }

    public String getTypeName()
    {
      Object localObject = this.typeName_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.typeName_ = str;
      return str;
    }

    public boolean hasDefaultValue()
    {
      return (0x40 & this.bitField0_) == 64;
    }

    public boolean hasExtendee()
    {
      return (0x20 & this.bitField0_) == 32;
    }

    public boolean hasLabel()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasNumber()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasOptions()
    {
      return (0x80 & this.bitField0_) == 128;
    }

    public boolean hasType()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasTypeName()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_FieldDescriptorProto_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      if ((hasOptions()) && (!getOptions().isInitialized()))
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      if ((0x20 & this.bitField0_) == 32)
        paramCodedOutputStream.writeBytes(2, getExtendeeBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeInt32(3, this.number_);
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeEnum(4, this.label_.getNumber());
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeEnum(5, this.type_.getNumber());
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeBytes(6, getTypeNameBytes());
      if ((0x40 & this.bitField0_) == 64)
        paramCodedOutputStream.writeBytes(7, getDefaultValueBytes());
      if ((0x80 & this.bitField0_) == 128)
        paramCodedOutputStream.writeMessage(8, this.options_);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.FieldDescriptorProtoOrBuilder
    {
      private int bitField0_;
      private Object defaultValue_ = "";
      private Object extendee_ = "";
      private DescriptorProtos.FieldDescriptorProto.Label label_ = DescriptorProtos.FieldDescriptorProto.Label.LABEL_OPTIONAL;
      private Object name_ = "";
      private int number_;
      private SingleFieldBuilder<DescriptorProtos.FieldOptions, DescriptorProtos.FieldOptions.Builder, DescriptorProtos.FieldOptionsOrBuilder> optionsBuilder_;
      private DescriptorProtos.FieldOptions options_ = DescriptorProtos.FieldOptions.getDefaultInstance();
      private Object typeName_ = "";
      private DescriptorProtos.FieldDescriptorProto.Type type_ = DescriptorProtos.FieldDescriptorProto.Type.TYPE_DOUBLE;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.FieldDescriptorProto buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.FieldDescriptorProto localFieldDescriptorProto = buildPartial();
        if (!localFieldDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localFieldDescriptorProto).asInvalidProtocolBufferException();
        return localFieldDescriptorProto;
      }

      private static Builder create()
      {
        return new Builder();
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_FieldDescriptorProto_descriptor;
      }

      private SingleFieldBuilder<DescriptorProtos.FieldOptions, DescriptorProtos.FieldOptions.Builder, DescriptorProtos.FieldOptionsOrBuilder> getOptionsFieldBuilder()
      {
        if (this.optionsBuilder_ == null)
        {
          this.optionsBuilder_ = new SingleFieldBuilder(this.options_, getParentForChildren(), isClean());
          this.options_ = null;
        }
        return this.optionsBuilder_;
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getOptionsFieldBuilder();
      }

      public DescriptorProtos.FieldDescriptorProto build()
      {
        DescriptorProtos.FieldDescriptorProto localFieldDescriptorProto = buildPartial();
        if (!localFieldDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localFieldDescriptorProto);
        return localFieldDescriptorProto;
      }

      public DescriptorProtos.FieldDescriptorProto buildPartial()
      {
        DescriptorProtos.FieldDescriptorProto localFieldDescriptorProto = new DescriptorProtos.FieldDescriptorProto(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.FieldDescriptorProto.access$5202(localFieldDescriptorProto, this.name_);
        if ((i & 0x2) == 2)
          k |= 2;
        DescriptorProtos.FieldDescriptorProto.access$5302(localFieldDescriptorProto, this.number_);
        if ((i & 0x4) == 4)
          k |= 4;
        DescriptorProtos.FieldDescriptorProto.access$5402(localFieldDescriptorProto, this.label_);
        if ((i & 0x8) == 8)
          k |= 8;
        DescriptorProtos.FieldDescriptorProto.access$5502(localFieldDescriptorProto, this.type_);
        if ((i & 0x10) == 16)
          k |= 16;
        DescriptorProtos.FieldDescriptorProto.access$5602(localFieldDescriptorProto, this.typeName_);
        if ((i & 0x20) == 32)
          k |= 32;
        DescriptorProtos.FieldDescriptorProto.access$5702(localFieldDescriptorProto, this.extendee_);
        if ((i & 0x40) == 64)
          k |= 64;
        DescriptorProtos.FieldDescriptorProto.access$5802(localFieldDescriptorProto, this.defaultValue_);
        if ((i & 0x80) == 128)
          k |= 128;
        if (this.optionsBuilder_ == null)
          DescriptorProtos.FieldDescriptorProto.access$5902(localFieldDescriptorProto, this.options_);
        while (true)
        {
          DescriptorProtos.FieldDescriptorProto.access$6002(localFieldDescriptorProto, k);
          onBuilt();
          return localFieldDescriptorProto;
          DescriptorProtos.FieldDescriptorProto.access$5902(localFieldDescriptorProto, (DescriptorProtos.FieldOptions)this.optionsBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.number_ = 0;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.label_ = DescriptorProtos.FieldDescriptorProto.Label.LABEL_OPTIONAL;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.type_ = DescriptorProtos.FieldDescriptorProto.Type.TYPE_DOUBLE;
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.typeName_ = "";
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.extendee_ = "";
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.defaultValue_ = "";
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        if (this.optionsBuilder_ == null)
          this.options_ = DescriptorProtos.FieldOptions.getDefaultInstance();
        while (true)
        {
          this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clearDefaultValue()
      {
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.defaultValue_ = DescriptorProtos.FieldDescriptorProto.getDefaultInstance().getDefaultValue();
        onChanged();
        return this;
      }

      public Builder clearExtendee()
      {
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.extendee_ = DescriptorProtos.FieldDescriptorProto.getDefaultInstance().getExtendee();
        onChanged();
        return this;
      }

      public Builder clearLabel()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.label_ = DescriptorProtos.FieldDescriptorProto.Label.LABEL_OPTIONAL;
        onChanged();
        return this;
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = DescriptorProtos.FieldDescriptorProto.getDefaultInstance().getName();
        onChanged();
        return this;
      }

      public Builder clearNumber()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.number_ = 0;
        onChanged();
        return this;
      }

      public Builder clearOptions()
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = DescriptorProtos.FieldOptions.getDefaultInstance();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clearType()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.type_ = DescriptorProtos.FieldDescriptorProto.Type.TYPE_DOUBLE;
        onChanged();
        return this;
      }

      public Builder clearTypeName()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.typeName_ = DescriptorProtos.FieldDescriptorProto.getDefaultInstance().getTypeName();
        onChanged();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.FieldDescriptorProto getDefaultInstanceForType()
      {
        return DescriptorProtos.FieldDescriptorProto.getDefaultInstance();
      }

      public String getDefaultValue()
      {
        Object localObject = this.defaultValue_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.defaultValue_ = str;
          return str;
        }
        return (String)localObject;
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.FieldDescriptorProto.getDescriptor();
      }

      public String getExtendee()
      {
        Object localObject = this.extendee_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.extendee_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.FieldDescriptorProto.Label getLabel()
      {
        return this.label_;
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public int getNumber()
      {
        return this.number_;
      }

      public DescriptorProtos.FieldOptions getOptions()
      {
        if (this.optionsBuilder_ == null)
          return this.options_;
        return (DescriptorProtos.FieldOptions)this.optionsBuilder_.getMessage();
      }

      public DescriptorProtos.FieldOptions.Builder getOptionsBuilder()
      {
        this.bitField0_ = (0x80 | this.bitField0_);
        onChanged();
        return (DescriptorProtos.FieldOptions.Builder)getOptionsFieldBuilder().getBuilder();
      }

      public DescriptorProtos.FieldOptionsOrBuilder getOptionsOrBuilder()
      {
        if (this.optionsBuilder_ != null)
          return (DescriptorProtos.FieldOptionsOrBuilder)this.optionsBuilder_.getMessageOrBuilder();
        return this.options_;
      }

      public DescriptorProtos.FieldDescriptorProto.Type getType()
      {
        return this.type_;
      }

      public String getTypeName()
      {
        Object localObject = this.typeName_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.typeName_ = str;
          return str;
        }
        return (String)localObject;
      }

      public boolean hasDefaultValue()
      {
        return (0x40 & this.bitField0_) == 64;
      }

      public boolean hasExtendee()
      {
        return (0x20 & this.bitField0_) == 32;
      }

      public boolean hasLabel()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasNumber()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasOptions()
      {
        return (0x80 & this.bitField0_) == 128;
      }

      public boolean hasType()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasTypeName()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_FieldDescriptorProto_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        return (!hasOptions()) || (getOptions().isInitialized());
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            this.bitField0_ = (0x20 | this.bitField0_);
            this.extendee_ = paramCodedInputStream.readBytes();
            break;
          case 24:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.number_ = paramCodedInputStream.readInt32();
            break;
          case 32:
            int k = paramCodedInputStream.readEnum();
            DescriptorProtos.FieldDescriptorProto.Label localLabel = DescriptorProtos.FieldDescriptorProto.Label.valueOf(k);
            if (localLabel == null)
            {
              localBuilder.mergeVarintField(4, k);
            }
            else
            {
              this.bitField0_ = (0x4 | this.bitField0_);
              this.label_ = localLabel;
            }
            break;
          case 40:
            int j = paramCodedInputStream.readEnum();
            DescriptorProtos.FieldDescriptorProto.Type localType = DescriptorProtos.FieldDescriptorProto.Type.valueOf(j);
            if (localType == null)
            {
              localBuilder.mergeVarintField(5, j);
            }
            else
            {
              this.bitField0_ = (0x8 | this.bitField0_);
              this.type_ = localType;
            }
            break;
          case 50:
            this.bitField0_ = (0x10 | this.bitField0_);
            this.typeName_ = paramCodedInputStream.readBytes();
            break;
          case 58:
            this.bitField0_ = (0x40 | this.bitField0_);
            this.defaultValue_ = paramCodedInputStream.readBytes();
            break;
          case 66:
            DescriptorProtos.FieldOptions.Builder localBuilder1 = DescriptorProtos.FieldOptions.newBuilder();
            if (hasOptions())
              localBuilder1.mergeFrom(getOptions());
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            setOptions(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (paramFieldDescriptorProto == DescriptorProtos.FieldDescriptorProto.getDefaultInstance())
          return this;
        if (paramFieldDescriptorProto.hasName())
          setName(paramFieldDescriptorProto.getName());
        if (paramFieldDescriptorProto.hasNumber())
          setNumber(paramFieldDescriptorProto.getNumber());
        if (paramFieldDescriptorProto.hasLabel())
          setLabel(paramFieldDescriptorProto.getLabel());
        if (paramFieldDescriptorProto.hasType())
          setType(paramFieldDescriptorProto.getType());
        if (paramFieldDescriptorProto.hasTypeName())
          setTypeName(paramFieldDescriptorProto.getTypeName());
        if (paramFieldDescriptorProto.hasExtendee())
          setExtendee(paramFieldDescriptorProto.getExtendee());
        if (paramFieldDescriptorProto.hasDefaultValue())
          setDefaultValue(paramFieldDescriptorProto.getDefaultValue());
        if (paramFieldDescriptorProto.hasOptions())
          mergeOptions(paramFieldDescriptorProto.getOptions());
        mergeUnknownFields(paramFieldDescriptorProto.getUnknownFields());
        return this;
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.FieldDescriptorProto))
          return mergeFrom((DescriptorProtos.FieldDescriptorProto)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder mergeOptions(DescriptorProtos.FieldOptions paramFieldOptions)
      {
        if (this.optionsBuilder_ == null)
          if (((0x80 & this.bitField0_) == 128) && (this.options_ != DescriptorProtos.FieldOptions.getDefaultInstance()))
          {
            this.options_ = DescriptorProtos.FieldOptions.newBuilder(this.options_).mergeFrom(paramFieldOptions).buildPartial();
            onChanged();
          }
        while (true)
        {
          this.bitField0_ = (0x80 | this.bitField0_);
          return this;
          this.options_ = paramFieldOptions;
          break;
          this.optionsBuilder_.mergeFrom(paramFieldOptions);
        }
      }

      public Builder setDefaultValue(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x40 | this.bitField0_);
        this.defaultValue_ = paramString;
        onChanged();
        return this;
      }

      void setDefaultValue(ByteString paramByteString)
      {
        this.bitField0_ = (0x40 | this.bitField0_);
        this.defaultValue_ = paramByteString;
        onChanged();
      }

      public Builder setExtendee(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x20 | this.bitField0_);
        this.extendee_ = paramString;
        onChanged();
        return this;
      }

      void setExtendee(ByteString paramByteString)
      {
        this.bitField0_ = (0x20 | this.bitField0_);
        this.extendee_ = paramByteString;
        onChanged();
      }

      public Builder setLabel(DescriptorProtos.FieldDescriptorProto.Label paramLabel)
      {
        if (paramLabel == null)
          throw new NullPointerException();
        this.bitField0_ = (0x4 | this.bitField0_);
        this.label_ = paramLabel;
        onChanged();
        return this;
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        onChanged();
        return this;
      }

      void setName(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        onChanged();
      }

      public Builder setNumber(int paramInt)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.number_ = paramInt;
        onChanged();
        return this;
      }

      public Builder setOptions(DescriptorProtos.FieldOptions.Builder paramBuilder)
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = paramBuilder.build();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x80 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramBuilder.build());
        }
      }

      public Builder setOptions(DescriptorProtos.FieldOptions paramFieldOptions)
      {
        if (this.optionsBuilder_ == null)
        {
          if (paramFieldOptions == null)
            throw new NullPointerException();
          this.options_ = paramFieldOptions;
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x80 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramFieldOptions);
        }
      }

      public Builder setType(DescriptorProtos.FieldDescriptorProto.Type paramType)
      {
        if (paramType == null)
          throw new NullPointerException();
        this.bitField0_ = (0x8 | this.bitField0_);
        this.type_ = paramType;
        onChanged();
        return this;
      }

      public Builder setTypeName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x10 | this.bitField0_);
        this.typeName_ = paramString;
        onChanged();
        return this;
      }

      void setTypeName(ByteString paramByteString)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.typeName_ = paramByteString;
        onChanged();
      }
    }

    public static enum Label
      implements ProtocolMessageEnum
    {
      public static final int LABEL_OPTIONAL_VALUE = 1;
      public static final int LABEL_REPEATED_VALUE = 3;
      public static final int LABEL_REQUIRED_VALUE = 2;
      private static final Label[] VALUES = arrayOfLabel2;
      private static Internal.EnumLiteMap<Label> internalValueMap;
      private final int index;
      private final int value;

      static
      {
        LABEL_REPEATED = new Label("LABEL_REPEATED", 2, 2, 3);
        Label[] arrayOfLabel1 = new Label[3];
        arrayOfLabel1[0] = LABEL_OPTIONAL;
        arrayOfLabel1[1] = LABEL_REQUIRED;
        arrayOfLabel1[2] = LABEL_REPEATED;
        $VALUES = arrayOfLabel1;
        internalValueMap = new Internal.EnumLiteMap()
        {
          public DescriptorProtos.FieldDescriptorProto.Label findValueByNumber(int paramAnonymousInt)
          {
            return DescriptorProtos.FieldDescriptorProto.Label.valueOf(paramAnonymousInt);
          }
        };
        Label[] arrayOfLabel2 = new Label[3];
        arrayOfLabel2[0] = LABEL_OPTIONAL;
        arrayOfLabel2[1] = LABEL_REQUIRED;
        arrayOfLabel2[2] = LABEL_REPEATED;
      }

      private Label(int paramInt1, int paramInt2)
      {
        this.index = paramInt1;
        this.value = paramInt2;
      }

      public static final Descriptors.EnumDescriptor getDescriptor()
      {
        return (Descriptors.EnumDescriptor)DescriptorProtos.FieldDescriptorProto.getDescriptor().getEnumTypes().get(1);
      }

      public static Internal.EnumLiteMap<Label> internalGetValueMap()
      {
        return internalValueMap;
      }

      public static Label valueOf(int paramInt)
      {
        switch (paramInt)
        {
        default:
          return null;
        case 1:
          return LABEL_OPTIONAL;
        case 2:
          return LABEL_REQUIRED;
        case 3:
        }
        return LABEL_REPEATED;
      }

      public static Label valueOf(Descriptors.EnumValueDescriptor paramEnumValueDescriptor)
      {
        if (paramEnumValueDescriptor.getType() != getDescriptor())
          throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        return VALUES[paramEnumValueDescriptor.getIndex()];
      }

      public final Descriptors.EnumDescriptor getDescriptorForType()
      {
        return getDescriptor();
      }

      public final int getNumber()
      {
        return this.value;
      }

      public final Descriptors.EnumValueDescriptor getValueDescriptor()
      {
        return (Descriptors.EnumValueDescriptor)getDescriptor().getValues().get(this.index);
      }
    }

    public static enum Type
      implements ProtocolMessageEnum
    {
      public static final int TYPE_BOOL_VALUE = 8;
      public static final int TYPE_BYTES_VALUE = 12;
      public static final int TYPE_DOUBLE_VALUE = 1;
      public static final int TYPE_ENUM_VALUE = 14;
      public static final int TYPE_FIXED32_VALUE = 7;
      public static final int TYPE_FIXED64_VALUE = 6;
      public static final int TYPE_FLOAT_VALUE = 2;
      public static final int TYPE_GROUP_VALUE = 10;
      public static final int TYPE_INT32_VALUE = 5;
      public static final int TYPE_INT64_VALUE = 3;
      public static final int TYPE_MESSAGE_VALUE = 11;
      public static final int TYPE_SFIXED32_VALUE = 15;
      public static final int TYPE_SFIXED64_VALUE = 16;
      public static final int TYPE_SINT32_VALUE = 17;
      public static final int TYPE_SINT64_VALUE = 18;
      public static final int TYPE_STRING_VALUE = 9;
      public static final int TYPE_UINT32_VALUE = 13;
      public static final int TYPE_UINT64_VALUE = 4;
      private static final Type[] VALUES = arrayOfType2;
      private static Internal.EnumLiteMap<Type> internalValueMap;
      private final int index;
      private final int value;

      static
      {
        TYPE_INT32 = new Type("TYPE_INT32", 4, 4, 5);
        TYPE_FIXED64 = new Type("TYPE_FIXED64", 5, 5, 6);
        TYPE_FIXED32 = new Type("TYPE_FIXED32", 6, 6, 7);
        TYPE_BOOL = new Type("TYPE_BOOL", 7, 7, 8);
        TYPE_STRING = new Type("TYPE_STRING", 8, 8, 9);
        TYPE_GROUP = new Type("TYPE_GROUP", 9, 9, 10);
        TYPE_MESSAGE = new Type("TYPE_MESSAGE", 10, 10, 11);
        TYPE_BYTES = new Type("TYPE_BYTES", 11, 11, 12);
        TYPE_UINT32 = new Type("TYPE_UINT32", 12, 12, 13);
        TYPE_ENUM = new Type("TYPE_ENUM", 13, 13, 14);
        TYPE_SFIXED32 = new Type("TYPE_SFIXED32", 14, 14, 15);
        TYPE_SFIXED64 = new Type("TYPE_SFIXED64", 15, 15, 16);
        TYPE_SINT32 = new Type("TYPE_SINT32", 16, 16, 17);
        TYPE_SINT64 = new Type("TYPE_SINT64", 17, 17, 18);
        Type[] arrayOfType1 = new Type[18];
        arrayOfType1[0] = TYPE_DOUBLE;
        arrayOfType1[1] = TYPE_FLOAT;
        arrayOfType1[2] = TYPE_INT64;
        arrayOfType1[3] = TYPE_UINT64;
        arrayOfType1[4] = TYPE_INT32;
        arrayOfType1[5] = TYPE_FIXED64;
        arrayOfType1[6] = TYPE_FIXED32;
        arrayOfType1[7] = TYPE_BOOL;
        arrayOfType1[8] = TYPE_STRING;
        arrayOfType1[9] = TYPE_GROUP;
        arrayOfType1[10] = TYPE_MESSAGE;
        arrayOfType1[11] = TYPE_BYTES;
        arrayOfType1[12] = TYPE_UINT32;
        arrayOfType1[13] = TYPE_ENUM;
        arrayOfType1[14] = TYPE_SFIXED32;
        arrayOfType1[15] = TYPE_SFIXED64;
        arrayOfType1[16] = TYPE_SINT32;
        arrayOfType1[17] = TYPE_SINT64;
        $VALUES = arrayOfType1;
        internalValueMap = new Internal.EnumLiteMap()
        {
          public DescriptorProtos.FieldDescriptorProto.Type findValueByNumber(int paramAnonymousInt)
          {
            return DescriptorProtos.FieldDescriptorProto.Type.valueOf(paramAnonymousInt);
          }
        };
        Type[] arrayOfType2 = new Type[18];
        arrayOfType2[0] = TYPE_DOUBLE;
        arrayOfType2[1] = TYPE_FLOAT;
        arrayOfType2[2] = TYPE_INT64;
        arrayOfType2[3] = TYPE_UINT64;
        arrayOfType2[4] = TYPE_INT32;
        arrayOfType2[5] = TYPE_FIXED64;
        arrayOfType2[6] = TYPE_FIXED32;
        arrayOfType2[7] = TYPE_BOOL;
        arrayOfType2[8] = TYPE_STRING;
        arrayOfType2[9] = TYPE_GROUP;
        arrayOfType2[10] = TYPE_MESSAGE;
        arrayOfType2[11] = TYPE_BYTES;
        arrayOfType2[12] = TYPE_UINT32;
        arrayOfType2[13] = TYPE_ENUM;
        arrayOfType2[14] = TYPE_SFIXED32;
        arrayOfType2[15] = TYPE_SFIXED64;
        arrayOfType2[16] = TYPE_SINT32;
        arrayOfType2[17] = TYPE_SINT64;
      }

      private Type(int paramInt1, int paramInt2)
      {
        this.index = paramInt1;
        this.value = paramInt2;
      }

      public static final Descriptors.EnumDescriptor getDescriptor()
      {
        return (Descriptors.EnumDescriptor)DescriptorProtos.FieldDescriptorProto.getDescriptor().getEnumTypes().get(0);
      }

      public static Internal.EnumLiteMap<Type> internalGetValueMap()
      {
        return internalValueMap;
      }

      public static Type valueOf(int paramInt)
      {
        switch (paramInt)
        {
        default:
          return null;
        case 1:
          return TYPE_DOUBLE;
        case 2:
          return TYPE_FLOAT;
        case 3:
          return TYPE_INT64;
        case 4:
          return TYPE_UINT64;
        case 5:
          return TYPE_INT32;
        case 6:
          return TYPE_FIXED64;
        case 7:
          return TYPE_FIXED32;
        case 8:
          return TYPE_BOOL;
        case 9:
          return TYPE_STRING;
        case 10:
          return TYPE_GROUP;
        case 11:
          return TYPE_MESSAGE;
        case 12:
          return TYPE_BYTES;
        case 13:
          return TYPE_UINT32;
        case 14:
          return TYPE_ENUM;
        case 15:
          return TYPE_SFIXED32;
        case 16:
          return TYPE_SFIXED64;
        case 17:
          return TYPE_SINT32;
        case 18:
        }
        return TYPE_SINT64;
      }

      public static Type valueOf(Descriptors.EnumValueDescriptor paramEnumValueDescriptor)
      {
        if (paramEnumValueDescriptor.getType() != getDescriptor())
          throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        return VALUES[paramEnumValueDescriptor.getIndex()];
      }

      public final Descriptors.EnumDescriptor getDescriptorForType()
      {
        return getDescriptor();
      }

      public final int getNumber()
      {
        return this.value;
      }

      public final Descriptors.EnumValueDescriptor getValueDescriptor()
      {
        return (Descriptors.EnumValueDescriptor)getDescriptor().getValues().get(this.index);
      }
    }
  }

  public static abstract interface FieldDescriptorProtoOrBuilder extends MessageOrBuilder
  {
    public abstract String getDefaultValue();

    public abstract String getExtendee();

    public abstract DescriptorProtos.FieldDescriptorProto.Label getLabel();

    public abstract String getName();

    public abstract int getNumber();

    public abstract DescriptorProtos.FieldOptions getOptions();

    public abstract DescriptorProtos.FieldOptionsOrBuilder getOptionsOrBuilder();

    public abstract DescriptorProtos.FieldDescriptorProto.Type getType();

    public abstract String getTypeName();

    public abstract boolean hasDefaultValue();

    public abstract boolean hasExtendee();

    public abstract boolean hasLabel();

    public abstract boolean hasName();

    public abstract boolean hasNumber();

    public abstract boolean hasOptions();

    public abstract boolean hasType();

    public abstract boolean hasTypeName();
  }

  public static final class FieldOptions extends GeneratedMessage.ExtendableMessage<FieldOptions>
    implements DescriptorProtos.FieldOptionsOrBuilder
  {
    public static final int CTYPE_FIELD_NUMBER = 1;
    public static final int DEPRECATED_FIELD_NUMBER = 3;
    public static final int EXPERIMENTAL_MAP_KEY_FIELD_NUMBER = 9;
    public static final int PACKED_FIELD_NUMBER = 2;
    public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
    private static final FieldOptions defaultInstance = new FieldOptions(true);
    private int bitField0_;
    private CType ctype_;
    private boolean deprecated_;
    private Object experimentalMapKey_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private boolean packed_;
    private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_;

    static
    {
      defaultInstance.initFields();
    }

    private FieldOptions(Builder paramBuilder)
    {
      super();
    }

    private FieldOptions(boolean paramBoolean)
    {
    }

    public static FieldOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_FieldOptions_descriptor;
    }

    private ByteString getExperimentalMapKeyBytes()
    {
      Object localObject = this.experimentalMapKey_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.experimentalMapKey_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.ctype_ = CType.STRING;
      this.packed_ = false;
      this.deprecated_ = false;
      this.experimentalMapKey_ = "";
      this.uninterpretedOption_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$13100();
    }

    public static Builder newBuilder(FieldOptions paramFieldOptions)
    {
      return newBuilder().mergeFrom(paramFieldOptions);
    }

    public static FieldOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static FieldOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static FieldOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static FieldOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static FieldOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static FieldOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static FieldOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static FieldOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static FieldOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static FieldOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public CType getCtype()
    {
      return this.ctype_;
    }

    public FieldOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public boolean getDeprecated()
    {
      return this.deprecated_;
    }

    public String getExperimentalMapKey()
    {
      Object localObject = this.experimentalMapKey_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.experimentalMapKey_ = str;
      return str;
    }

    public boolean getPacked()
    {
      return this.packed_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeEnumSize(1, this.ctype_.getNumber());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBoolSize(2, this.packed_);
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBoolSize(3, this.deprecated_);
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeBytesSize(9, getExperimentalMapKeyBytes());
      for (int m = 0; m < this.uninterpretedOption_.size(); m++)
        k += CodedOutputStream.computeMessageSize(999, (MessageLite)this.uninterpretedOption_.get(m));
      int n = k + extensionsSerializedSize() + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = n;
      return n;
    }

    public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
    }

    public int getUninterpretedOptionCount()
    {
      return this.uninterpretedOption_.size();
    }

    public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
    {
      return this.uninterpretedOption_;
    }

    public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
    }

    public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
    {
      return this.uninterpretedOption_;
    }

    public boolean hasCtype()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasDeprecated()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasExperimentalMapKey()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasPacked()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_FieldOptions_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getUninterpretedOptionCount(); j++)
        if (!getUninterpretedOption(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessage.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeEnum(1, this.ctype_.getNumber());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBool(2, this.packed_);
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBool(3, this.deprecated_);
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeBytes(9, getExperimentalMapKeyBytes());
      for (int i = 0; i < this.uninterpretedOption_.size(); i++)
        paramCodedOutputStream.writeMessage(999, (MessageLite)this.uninterpretedOption_.get(i));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.ExtendableBuilder<DescriptorProtos.FieldOptions, Builder>
      implements DescriptorProtos.FieldOptionsOrBuilder
    {
      private int bitField0_;
      private DescriptorProtos.FieldOptions.CType ctype_ = DescriptorProtos.FieldOptions.CType.STRING;
      private boolean deprecated_;
      private Object experimentalMapKey_ = "";
      private boolean packed_;
      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> uninterpretedOptionBuilder_;
      private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.FieldOptions buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.FieldOptions localFieldOptions = buildPartial();
        if (!localFieldOptions.isInitialized())
          throw newUninitializedMessageException(localFieldOptions).asInvalidProtocolBufferException();
        return localFieldOptions;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureUninterpretedOptionIsMutable()
      {
        if ((0x10 & this.bitField0_) != 16)
        {
          this.uninterpretedOption_ = new ArrayList(this.uninterpretedOption_);
          this.bitField0_ = (0x10 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_FieldOptions_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionFieldBuilder()
      {
        List localList;
        if (this.uninterpretedOptionBuilder_ == null)
        {
          localList = this.uninterpretedOption_;
          if ((0x10 & this.bitField0_) != 16)
            break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
          this.uninterpretedOptionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.uninterpretedOption_ = null;
          return this.uninterpretedOptionBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getUninterpretedOptionFieldBuilder();
      }

      public Builder addAllUninterpretedOption(Iterable<? extends DescriptorProtos.UninterpretedOption> paramIterable)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          GeneratedMessage.ExtendableBuilder.addAll(paramIterable, this.uninterpretedOption_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramUninterpretedOption);
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramUninterpretedOption);
        return this;
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder()
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(paramInt, DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.FieldOptions build()
      {
        DescriptorProtos.FieldOptions localFieldOptions = buildPartial();
        if (!localFieldOptions.isInitialized())
          throw newUninitializedMessageException(localFieldOptions);
        return localFieldOptions;
      }

      public DescriptorProtos.FieldOptions buildPartial()
      {
        DescriptorProtos.FieldOptions localFieldOptions = new DescriptorProtos.FieldOptions(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.FieldOptions.access$13402(localFieldOptions, this.ctype_);
        if ((i & 0x2) == 2)
          k |= 2;
        DescriptorProtos.FieldOptions.access$13502(localFieldOptions, this.packed_);
        if ((i & 0x4) == 4)
          k |= 4;
        DescriptorProtos.FieldOptions.access$13602(localFieldOptions, this.deprecated_);
        if ((i & 0x8) == 8)
          k |= 8;
        DescriptorProtos.FieldOptions.access$13702(localFieldOptions, this.experimentalMapKey_);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if ((0x10 & this.bitField0_) == 16)
          {
            this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
            this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          }
          DescriptorProtos.FieldOptions.access$13802(localFieldOptions, this.uninterpretedOption_);
        }
        while (true)
        {
          DescriptorProtos.FieldOptions.access$13902(localFieldOptions, k);
          onBuilt();
          return localFieldOptions;
          DescriptorProtos.FieldOptions.access$13802(localFieldOptions, this.uninterpretedOptionBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.ctype_ = DescriptorProtos.FieldOptions.CType.STRING;
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.packed_ = false;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.deprecated_ = false;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.experimentalMapKey_ = "";
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clearCtype()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.ctype_ = DescriptorProtos.FieldOptions.CType.STRING;
        onChanged();
        return this;
      }

      public Builder clearDeprecated()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.deprecated_ = false;
        onChanged();
        return this;
      }

      public Builder clearExperimentalMapKey()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.experimentalMapKey_ = DescriptorProtos.FieldOptions.getDefaultInstance().getExperimentalMapKey();
        onChanged();
        return this;
      }

      public Builder clearPacked()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.packed_ = false;
        onChanged();
        return this;
      }

      public Builder clearUninterpretedOption()
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.FieldOptions.CType getCtype()
      {
        return this.ctype_;
      }

      public DescriptorProtos.FieldOptions getDefaultInstanceForType()
      {
        return DescriptorProtos.FieldOptions.getDefaultInstance();
      }

      public boolean getDeprecated()
      {
        return this.deprecated_;
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.FieldOptions.getDescriptor();
      }

      public String getExperimentalMapKey()
      {
        Object localObject = this.experimentalMapKey_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.experimentalMapKey_ = str;
          return str;
        }
        return (String)localObject;
      }

      public boolean getPacked()
      {
        return this.packed_;
      }

      public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption)this.uninterpretedOptionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.UninterpretedOption.Builder getUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.UninterpretedOption.Builder> getUninterpretedOptionBuilderList()
      {
        return getUninterpretedOptionFieldBuilder().getBuilderList();
      }

      public int getUninterpretedOptionCount()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return this.uninterpretedOption_.size();
        return this.uninterpretedOptionBuilder_.getCount();
      }

      public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return Collections.unmodifiableList(this.uninterpretedOption_);
        return this.uninterpretedOptionBuilder_.getMessageList();
      }

      public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOptionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
      {
        if (this.uninterpretedOptionBuilder_ != null)
          return this.uninterpretedOptionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.uninterpretedOption_);
      }

      public boolean hasCtype()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasDeprecated()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasExperimentalMapKey()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasPacked()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_FieldOptions_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getUninterpretedOptionCount(); i++)
          if (!getUninterpretedOption(i).isInitialized())
            return false;
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 8:
            int j = paramCodedInputStream.readEnum();
            DescriptorProtos.FieldOptions.CType localCType = DescriptorProtos.FieldOptions.CType.valueOf(j);
            if (localCType == null)
            {
              localBuilder.mergeVarintField(1, j);
            }
            else
            {
              this.bitField0_ = (0x1 | this.bitField0_);
              this.ctype_ = localCType;
            }
            break;
          case 16:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.packed_ = paramCodedInputStream.readBool();
            break;
          case 24:
            this.bitField0_ = (0x4 | this.bitField0_);
            this.deprecated_ = paramCodedInputStream.readBool();
            break;
          case 74:
            this.bitField0_ = (0x8 | this.bitField0_);
            this.experimentalMapKey_ = paramCodedInputStream.readBytes();
            break;
          case 7994:
            DescriptorProtos.UninterpretedOption.Builder localBuilder1 = DescriptorProtos.UninterpretedOption.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addUninterpretedOption(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.FieldOptions paramFieldOptions)
      {
        if (paramFieldOptions == DescriptorProtos.FieldOptions.getDefaultInstance())
          return this;
        if (paramFieldOptions.hasCtype())
          setCtype(paramFieldOptions.getCtype());
        if (paramFieldOptions.hasPacked())
          setPacked(paramFieldOptions.getPacked());
        if (paramFieldOptions.hasDeprecated())
          setDeprecated(paramFieldOptions.getDeprecated());
        if (paramFieldOptions.hasExperimentalMapKey())
          setExperimentalMapKey(paramFieldOptions.getExperimentalMapKey());
        if (this.uninterpretedOptionBuilder_ == null)
          if (!paramFieldOptions.uninterpretedOption_.isEmpty())
          {
            if (!this.uninterpretedOption_.isEmpty())
              break label143;
            this.uninterpretedOption_ = paramFieldOptions.uninterpretedOption_;
            this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeExtensionFields(paramFieldOptions);
          mergeUnknownFields(paramFieldOptions.getUnknownFields());
          return this;
          label143: ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.addAll(paramFieldOptions.uninterpretedOption_);
          break;
          if (!paramFieldOptions.uninterpretedOption_.isEmpty())
          {
            if (this.uninterpretedOptionBuilder_.isEmpty())
            {
              this.uninterpretedOptionBuilder_.dispose();
              this.uninterpretedOptionBuilder_ = null;
              this.uninterpretedOption_ = paramFieldOptions.uninterpretedOption_;
              this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getUninterpretedOptionFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.uninterpretedOptionBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.uninterpretedOptionBuilder_.addAllMessages(paramFieldOptions.uninterpretedOption_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.FieldOptions))
          return mergeFrom((DescriptorProtos.FieldOptions)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.remove(paramInt);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.remove(paramInt);
        return this;
      }

      public Builder setCtype(DescriptorProtos.FieldOptions.CType paramCType)
      {
        if (paramCType == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.ctype_ = paramCType;
        onChanged();
        return this;
      }

      public Builder setDeprecated(boolean paramBoolean)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.deprecated_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setExperimentalMapKey(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x8 | this.bitField0_);
        this.experimentalMapKey_ = paramString;
        onChanged();
        return this;
      }

      void setExperimentalMapKey(ByteString paramByteString)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.experimentalMapKey_ = paramByteString;
        onChanged();
      }

      public Builder setPacked(boolean paramBoolean)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.packed_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramUninterpretedOption);
        return this;
      }
    }

    public static enum CType
      implements ProtocolMessageEnum
    {
      public static final int CORD_VALUE = 1;
      public static final int STRING_PIECE_VALUE = 2;
      public static final int STRING_VALUE;
      private static final CType[] VALUES = arrayOfCType2;
      private static Internal.EnumLiteMap<CType> internalValueMap;
      private final int index;
      private final int value;

      static
      {
        CORD = new CType("CORD", 1, 1, 1);
        STRING_PIECE = new CType("STRING_PIECE", 2, 2, 2);
        CType[] arrayOfCType1 = new CType[3];
        arrayOfCType1[0] = STRING;
        arrayOfCType1[1] = CORD;
        arrayOfCType1[2] = STRING_PIECE;
        $VALUES = arrayOfCType1;
        internalValueMap = new Internal.EnumLiteMap()
        {
          public DescriptorProtos.FieldOptions.CType findValueByNumber(int paramAnonymousInt)
          {
            return DescriptorProtos.FieldOptions.CType.valueOf(paramAnonymousInt);
          }
        };
        CType[] arrayOfCType2 = new CType[3];
        arrayOfCType2[0] = STRING;
        arrayOfCType2[1] = CORD;
        arrayOfCType2[2] = STRING_PIECE;
      }

      private CType(int paramInt1, int paramInt2)
      {
        this.index = paramInt1;
        this.value = paramInt2;
      }

      public static final Descriptors.EnumDescriptor getDescriptor()
      {
        return (Descriptors.EnumDescriptor)DescriptorProtos.FieldOptions.getDescriptor().getEnumTypes().get(0);
      }

      public static Internal.EnumLiteMap<CType> internalGetValueMap()
      {
        return internalValueMap;
      }

      public static CType valueOf(int paramInt)
      {
        switch (paramInt)
        {
        default:
          return null;
        case 0:
          return STRING;
        case 1:
          return CORD;
        case 2:
        }
        return STRING_PIECE;
      }

      public static CType valueOf(Descriptors.EnumValueDescriptor paramEnumValueDescriptor)
      {
        if (paramEnumValueDescriptor.getType() != getDescriptor())
          throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        return VALUES[paramEnumValueDescriptor.getIndex()];
      }

      public final Descriptors.EnumDescriptor getDescriptorForType()
      {
        return getDescriptor();
      }

      public final int getNumber()
      {
        return this.value;
      }

      public final Descriptors.EnumValueDescriptor getValueDescriptor()
      {
        return (Descriptors.EnumValueDescriptor)getDescriptor().getValues().get(this.index);
      }
    }
  }

  public static abstract interface FieldOptionsOrBuilder extends GeneratedMessage.ExtendableMessageOrBuilder<DescriptorProtos.FieldOptions>
  {
    public abstract DescriptorProtos.FieldOptions.CType getCtype();

    public abstract boolean getDeprecated();

    public abstract String getExperimentalMapKey();

    public abstract boolean getPacked();

    public abstract DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt);

    public abstract int getUninterpretedOptionCount();

    public abstract List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList();

    public abstract DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList();

    public abstract boolean hasCtype();

    public abstract boolean hasDeprecated();

    public abstract boolean hasExperimentalMapKey();

    public abstract boolean hasPacked();
  }

  public static final class FileDescriptorProto extends GeneratedMessage
    implements DescriptorProtos.FileDescriptorProtoOrBuilder
  {
    public static final int DEPENDENCY_FIELD_NUMBER = 3;
    public static final int ENUM_TYPE_FIELD_NUMBER = 5;
    public static final int EXTENSION_FIELD_NUMBER = 7;
    public static final int MESSAGE_TYPE_FIELD_NUMBER = 4;
    public static final int NAME_FIELD_NUMBER = 1;
    public static final int OPTIONS_FIELD_NUMBER = 8;
    public static final int PACKAGE_FIELD_NUMBER = 2;
    public static final int SERVICE_FIELD_NUMBER = 6;
    public static final int SOURCE_CODE_INFO_FIELD_NUMBER = 9;
    private static final FileDescriptorProto defaultInstance = new FileDescriptorProto(true);
    private int bitField0_;
    private LazyStringList dependency_;
    private List<DescriptorProtos.EnumDescriptorProto> enumType_;
    private List<DescriptorProtos.FieldDescriptorProto> extension_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<DescriptorProtos.DescriptorProto> messageType_;
    private Object name_;
    private DescriptorProtos.FileOptions options_;
    private Object package_;
    private List<DescriptorProtos.ServiceDescriptorProto> service_;
    private DescriptorProtos.SourceCodeInfo sourceCodeInfo_;

    static
    {
      defaultInstance.initFields();
    }

    private FileDescriptorProto(Builder paramBuilder)
    {
      super();
    }

    private FileDescriptorProto(boolean paramBoolean)
    {
    }

    public static FileDescriptorProto getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_FileDescriptorProto_descriptor;
    }

    private ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getPackageBytes()
    {
      Object localObject = this.package_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.package_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.name_ = "";
      this.package_ = "";
      this.dependency_ = LazyStringArrayList.EMPTY;
      this.messageType_ = Collections.emptyList();
      this.enumType_ = Collections.emptyList();
      this.service_ = Collections.emptyList();
      this.extension_ = Collections.emptyList();
      this.options_ = DescriptorProtos.FileOptions.getDefaultInstance();
      this.sourceCodeInfo_ = DescriptorProtos.SourceCodeInfo.getDefaultInstance();
    }

    public static Builder newBuilder()
    {
      return Builder.access$1000();
    }

    public static Builder newBuilder(FileDescriptorProto paramFileDescriptorProto)
    {
      return newBuilder().mergeFrom(paramFileDescriptorProto);
    }

    public static FileDescriptorProto parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static FileDescriptorProto parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static FileDescriptorProto parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static FileDescriptorProto parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static FileDescriptorProto parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static FileDescriptorProto parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static FileDescriptorProto parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static FileDescriptorProto parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static FileDescriptorProto parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static FileDescriptorProto parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public FileDescriptorProto getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDependency(int paramInt)
    {
      return (String)this.dependency_.get(paramInt);
    }

    public int getDependencyCount()
    {
      return this.dependency_.size();
    }

    public List<String> getDependencyList()
    {
      return this.dependency_;
    }

    public DescriptorProtos.EnumDescriptorProto getEnumType(int paramInt)
    {
      return (DescriptorProtos.EnumDescriptorProto)this.enumType_.get(paramInt);
    }

    public int getEnumTypeCount()
    {
      return this.enumType_.size();
    }

    public List<DescriptorProtos.EnumDescriptorProto> getEnumTypeList()
    {
      return this.enumType_;
    }

    public DescriptorProtos.EnumDescriptorProtoOrBuilder getEnumTypeOrBuilder(int paramInt)
    {
      return (DescriptorProtos.EnumDescriptorProtoOrBuilder)this.enumType_.get(paramInt);
    }

    public List<? extends DescriptorProtos.EnumDescriptorProtoOrBuilder> getEnumTypeOrBuilderList()
    {
      return this.enumType_;
    }

    public DescriptorProtos.FieldDescriptorProto getExtension(int paramInt)
    {
      return (DescriptorProtos.FieldDescriptorProto)this.extension_.get(paramInt);
    }

    public int getExtensionCount()
    {
      return this.extension_.size();
    }

    public List<DescriptorProtos.FieldDescriptorProto> getExtensionList()
    {
      return this.extension_;
    }

    public DescriptorProtos.FieldDescriptorProtoOrBuilder getExtensionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.extension_.get(paramInt);
    }

    public List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getExtensionOrBuilderList()
    {
      return this.extension_;
    }

    public DescriptorProtos.DescriptorProto getMessageType(int paramInt)
    {
      return (DescriptorProtos.DescriptorProto)this.messageType_.get(paramInt);
    }

    public int getMessageTypeCount()
    {
      return this.messageType_.size();
    }

    public List<DescriptorProtos.DescriptorProto> getMessageTypeList()
    {
      return this.messageType_;
    }

    public DescriptorProtos.DescriptorProtoOrBuilder getMessageTypeOrBuilder(int paramInt)
    {
      return (DescriptorProtos.DescriptorProtoOrBuilder)this.messageType_.get(paramInt);
    }

    public List<? extends DescriptorProtos.DescriptorProtoOrBuilder> getMessageTypeOrBuilderList()
    {
      return this.messageType_;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.name_ = str;
      return str;
    }

    public DescriptorProtos.FileOptions getOptions()
    {
      return this.options_;
    }

    public DescriptorProtos.FileOptionsOrBuilder getOptionsOrBuilder()
    {
      return this.options_;
    }

    public String getPackage()
    {
      Object localObject = this.package_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.package_ = str;
      return str;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBytesSize(2, getPackageBytes());
      int m = 0;
      for (int n = 0; n < this.dependency_.size(); n++)
        m += CodedOutputStream.computeBytesSizeNoTag(this.dependency_.getByteString(n));
      int i1 = k + m + 1 * getDependencyList().size();
      for (int i2 = 0; i2 < this.messageType_.size(); i2++)
        i1 += CodedOutputStream.computeMessageSize(4, (MessageLite)this.messageType_.get(i2));
      for (int i3 = 0; i3 < this.enumType_.size(); i3++)
        i1 += CodedOutputStream.computeMessageSize(5, (MessageLite)this.enumType_.get(i3));
      for (int i4 = 0; i4 < this.service_.size(); i4++)
        i1 += CodedOutputStream.computeMessageSize(6, (MessageLite)this.service_.get(i4));
      for (int i5 = 0; i5 < this.extension_.size(); i5++)
        i1 += CodedOutputStream.computeMessageSize(7, (MessageLite)this.extension_.get(i5));
      if ((0x4 & this.bitField0_) == 4)
        i1 += CodedOutputStream.computeMessageSize(8, this.options_);
      if ((0x8 & this.bitField0_) == 8)
        i1 += CodedOutputStream.computeMessageSize(9, this.sourceCodeInfo_);
      int i6 = i1 + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = i6;
      return i6;
    }

    public DescriptorProtos.ServiceDescriptorProto getService(int paramInt)
    {
      return (DescriptorProtos.ServiceDescriptorProto)this.service_.get(paramInt);
    }

    public int getServiceCount()
    {
      return this.service_.size();
    }

    public List<DescriptorProtos.ServiceDescriptorProto> getServiceList()
    {
      return this.service_;
    }

    public DescriptorProtos.ServiceDescriptorProtoOrBuilder getServiceOrBuilder(int paramInt)
    {
      return (DescriptorProtos.ServiceDescriptorProtoOrBuilder)this.service_.get(paramInt);
    }

    public List<? extends DescriptorProtos.ServiceDescriptorProtoOrBuilder> getServiceOrBuilderList()
    {
      return this.service_;
    }

    public DescriptorProtos.SourceCodeInfo getSourceCodeInfo()
    {
      return this.sourceCodeInfo_;
    }

    public DescriptorProtos.SourceCodeInfoOrBuilder getSourceCodeInfoOrBuilder()
    {
      return this.sourceCodeInfo_;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasOptions()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasPackage()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasSourceCodeInfo()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_FileDescriptorProto_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getMessageTypeCount(); j++)
        if (!getMessageType(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      for (int k = 0; k < getEnumTypeCount(); k++)
        if (!getEnumType(k).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      for (int m = 0; m < getServiceCount(); m++)
        if (!getService(m).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      for (int n = 0; n < getExtensionCount(); n++)
        if (!getExtension(n).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if ((hasOptions()) && (!getOptions().isInitialized()))
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBytes(2, getPackageBytes());
      for (int i = 0; i < this.dependency_.size(); i++)
        paramCodedOutputStream.writeBytes(3, this.dependency_.getByteString(i));
      for (int j = 0; j < this.messageType_.size(); j++)
        paramCodedOutputStream.writeMessage(4, (MessageLite)this.messageType_.get(j));
      for (int k = 0; k < this.enumType_.size(); k++)
        paramCodedOutputStream.writeMessage(5, (MessageLite)this.enumType_.get(k));
      for (int m = 0; m < this.service_.size(); m++)
        paramCodedOutputStream.writeMessage(6, (MessageLite)this.service_.get(m));
      for (int n = 0; n < this.extension_.size(); n++)
        paramCodedOutputStream.writeMessage(7, (MessageLite)this.extension_.get(n));
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeMessage(8, this.options_);
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeMessage(9, this.sourceCodeInfo_);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.FileDescriptorProtoOrBuilder
    {
      private int bitField0_;
      private LazyStringList dependency_ = LazyStringArrayList.EMPTY;
      private RepeatedFieldBuilder<DescriptorProtos.EnumDescriptorProto, DescriptorProtos.EnumDescriptorProto.Builder, DescriptorProtos.EnumDescriptorProtoOrBuilder> enumTypeBuilder_;
      private List<DescriptorProtos.EnumDescriptorProto> enumType_ = Collections.emptyList();
      private RepeatedFieldBuilder<DescriptorProtos.FieldDescriptorProto, DescriptorProtos.FieldDescriptorProto.Builder, DescriptorProtos.FieldDescriptorProtoOrBuilder> extensionBuilder_;
      private List<DescriptorProtos.FieldDescriptorProto> extension_ = Collections.emptyList();
      private RepeatedFieldBuilder<DescriptorProtos.DescriptorProto, DescriptorProtos.DescriptorProto.Builder, DescriptorProtos.DescriptorProtoOrBuilder> messageTypeBuilder_;
      private List<DescriptorProtos.DescriptorProto> messageType_ = Collections.emptyList();
      private Object name_ = "";
      private SingleFieldBuilder<DescriptorProtos.FileOptions, DescriptorProtos.FileOptions.Builder, DescriptorProtos.FileOptionsOrBuilder> optionsBuilder_;
      private DescriptorProtos.FileOptions options_ = DescriptorProtos.FileOptions.getDefaultInstance();
      private Object package_ = "";
      private RepeatedFieldBuilder<DescriptorProtos.ServiceDescriptorProto, DescriptorProtos.ServiceDescriptorProto.Builder, DescriptorProtos.ServiceDescriptorProtoOrBuilder> serviceBuilder_;
      private List<DescriptorProtos.ServiceDescriptorProto> service_ = Collections.emptyList();
      private SingleFieldBuilder<DescriptorProtos.SourceCodeInfo, DescriptorProtos.SourceCodeInfo.Builder, DescriptorProtos.SourceCodeInfoOrBuilder> sourceCodeInfoBuilder_;
      private DescriptorProtos.SourceCodeInfo sourceCodeInfo_ = DescriptorProtos.SourceCodeInfo.getDefaultInstance();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.FileDescriptorProto buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.FileDescriptorProto localFileDescriptorProto = buildPartial();
        if (!localFileDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localFileDescriptorProto).asInvalidProtocolBufferException();
        return localFileDescriptorProto;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureDependencyIsMutable()
      {
        if ((0x4 & this.bitField0_) != 4)
        {
          this.dependency_ = new LazyStringArrayList(this.dependency_);
          this.bitField0_ = (0x4 | this.bitField0_);
        }
      }

      private void ensureEnumTypeIsMutable()
      {
        if ((0x10 & this.bitField0_) != 16)
        {
          this.enumType_ = new ArrayList(this.enumType_);
          this.bitField0_ = (0x10 | this.bitField0_);
        }
      }

      private void ensureExtensionIsMutable()
      {
        if ((0x40 & this.bitField0_) != 64)
        {
          this.extension_ = new ArrayList(this.extension_);
          this.bitField0_ = (0x40 | this.bitField0_);
        }
      }

      private void ensureMessageTypeIsMutable()
      {
        if ((0x8 & this.bitField0_) != 8)
        {
          this.messageType_ = new ArrayList(this.messageType_);
          this.bitField0_ = (0x8 | this.bitField0_);
        }
      }

      private void ensureServiceIsMutable()
      {
        if ((0x20 & this.bitField0_) != 32)
        {
          this.service_ = new ArrayList(this.service_);
          this.bitField0_ = (0x20 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_FileDescriptorProto_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.EnumDescriptorProto, DescriptorProtos.EnumDescriptorProto.Builder, DescriptorProtos.EnumDescriptorProtoOrBuilder> getEnumTypeFieldBuilder()
      {
        List localList;
        if (this.enumTypeBuilder_ == null)
        {
          localList = this.enumType_;
          if ((0x10 & this.bitField0_) != 16)
            break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
          this.enumTypeBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.enumType_ = null;
          return this.enumTypeBuilder_;
        }
      }

      private RepeatedFieldBuilder<DescriptorProtos.FieldDescriptorProto, DescriptorProtos.FieldDescriptorProto.Builder, DescriptorProtos.FieldDescriptorProtoOrBuilder> getExtensionFieldBuilder()
      {
        List localList;
        if (this.extensionBuilder_ == null)
        {
          localList = this.extension_;
          if ((0x40 & this.bitField0_) != 64)
            break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
          this.extensionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.extension_ = null;
          return this.extensionBuilder_;
        }
      }

      private RepeatedFieldBuilder<DescriptorProtos.DescriptorProto, DescriptorProtos.DescriptorProto.Builder, DescriptorProtos.DescriptorProtoOrBuilder> getMessageTypeFieldBuilder()
      {
        List localList;
        if (this.messageTypeBuilder_ == null)
        {
          localList = this.messageType_;
          if ((0x8 & this.bitField0_) != 8)
            break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
          this.messageTypeBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.messageType_ = null;
          return this.messageTypeBuilder_;
        }
      }

      private SingleFieldBuilder<DescriptorProtos.FileOptions, DescriptorProtos.FileOptions.Builder, DescriptorProtos.FileOptionsOrBuilder> getOptionsFieldBuilder()
      {
        if (this.optionsBuilder_ == null)
        {
          this.optionsBuilder_ = new SingleFieldBuilder(this.options_, getParentForChildren(), isClean());
          this.options_ = null;
        }
        return this.optionsBuilder_;
      }

      private RepeatedFieldBuilder<DescriptorProtos.ServiceDescriptorProto, DescriptorProtos.ServiceDescriptorProto.Builder, DescriptorProtos.ServiceDescriptorProtoOrBuilder> getServiceFieldBuilder()
      {
        List localList;
        if (this.serviceBuilder_ == null)
        {
          localList = this.service_;
          if ((0x20 & this.bitField0_) != 32)
            break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
          this.serviceBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.service_ = null;
          return this.serviceBuilder_;
        }
      }

      private SingleFieldBuilder<DescriptorProtos.SourceCodeInfo, DescriptorProtos.SourceCodeInfo.Builder, DescriptorProtos.SourceCodeInfoOrBuilder> getSourceCodeInfoFieldBuilder()
      {
        if (this.sourceCodeInfoBuilder_ == null)
        {
          this.sourceCodeInfoBuilder_ = new SingleFieldBuilder(this.sourceCodeInfo_, getParentForChildren(), isClean());
          this.sourceCodeInfo_ = null;
        }
        return this.sourceCodeInfoBuilder_;
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
        {
          getMessageTypeFieldBuilder();
          getEnumTypeFieldBuilder();
          getServiceFieldBuilder();
          getExtensionFieldBuilder();
          getOptionsFieldBuilder();
          getSourceCodeInfoFieldBuilder();
        }
      }

      public Builder addAllDependency(Iterable<String> paramIterable)
      {
        ensureDependencyIsMutable();
        GeneratedMessage.Builder.addAll(paramIterable, this.dependency_);
        onChanged();
        return this;
      }

      public Builder addAllEnumType(Iterable<? extends DescriptorProtos.EnumDescriptorProto> paramIterable)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.enumType_);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addAllExtension(Iterable<? extends DescriptorProtos.FieldDescriptorProto> paramIterable)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.extension_);
          onChanged();
          return this;
        }
        this.extensionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addAllMessageType(Iterable<? extends DescriptorProtos.DescriptorProto> paramIterable)
      {
        if (this.messageTypeBuilder_ == null)
        {
          ensureMessageTypeIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.messageType_);
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addAllService(Iterable<? extends DescriptorProtos.ServiceDescriptorProto> paramIterable)
      {
        if (this.serviceBuilder_ == null)
        {
          ensureServiceIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.service_);
          onChanged();
          return this;
        }
        this.serviceBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addDependency(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDependencyIsMutable();
        this.dependency_.add(paramString);
        onChanged();
        return this;
      }

      void addDependency(ByteString paramByteString)
      {
        ensureDependencyIsMutable();
        this.dependency_.add(paramByteString);
        onChanged();
      }

      public Builder addEnumType(int paramInt, DescriptorProtos.EnumDescriptorProto.Builder paramBuilder)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          this.enumType_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addEnumType(int paramInt, DescriptorProtos.EnumDescriptorProto paramEnumDescriptorProto)
      {
        if (this.enumTypeBuilder_ == null)
        {
          if (paramEnumDescriptorProto == null)
            throw new NullPointerException();
          ensureEnumTypeIsMutable();
          this.enumType_.add(paramInt, paramEnumDescriptorProto);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addMessage(paramInt, paramEnumDescriptorProto);
        return this;
      }

      public Builder addEnumType(DescriptorProtos.EnumDescriptorProto.Builder paramBuilder)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          this.enumType_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addEnumType(DescriptorProtos.EnumDescriptorProto paramEnumDescriptorProto)
      {
        if (this.enumTypeBuilder_ == null)
        {
          if (paramEnumDescriptorProto == null)
            throw new NullPointerException();
          ensureEnumTypeIsMutable();
          this.enumType_.add(paramEnumDescriptorProto);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.addMessage(paramEnumDescriptorProto);
        return this;
      }

      public DescriptorProtos.EnumDescriptorProto.Builder addEnumTypeBuilder()
      {
        return (DescriptorProtos.EnumDescriptorProto.Builder)getEnumTypeFieldBuilder().addBuilder(DescriptorProtos.EnumDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.EnumDescriptorProto.Builder addEnumTypeBuilder(int paramInt)
      {
        return (DescriptorProtos.EnumDescriptorProto.Builder)getEnumTypeFieldBuilder().addBuilder(paramInt, DescriptorProtos.EnumDescriptorProto.getDefaultInstance());
      }

      public Builder addExtension(int paramInt, DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          this.extension_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addExtension(int paramInt, DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.extensionBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureExtensionIsMutable();
          this.extension_.add(paramInt, paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.extensionBuilder_.addMessage(paramInt, paramFieldDescriptorProto);
        return this;
      }

      public Builder addExtension(DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          this.extension_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addExtension(DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.extensionBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureExtensionIsMutable();
          this.extension_.add(paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.extensionBuilder_.addMessage(paramFieldDescriptorProto);
        return this;
      }

      public DescriptorProtos.FieldDescriptorProto.Builder addExtensionBuilder()
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getExtensionFieldBuilder().addBuilder(DescriptorProtos.FieldDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.FieldDescriptorProto.Builder addExtensionBuilder(int paramInt)
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getExtensionFieldBuilder().addBuilder(paramInt, DescriptorProtos.FieldDescriptorProto.getDefaultInstance());
      }

      public Builder addMessageType(int paramInt, DescriptorProtos.DescriptorProto.Builder paramBuilder)
      {
        if (this.messageTypeBuilder_ == null)
        {
          ensureMessageTypeIsMutable();
          this.messageType_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addMessageType(int paramInt, DescriptorProtos.DescriptorProto paramDescriptorProto)
      {
        if (this.messageTypeBuilder_ == null)
        {
          if (paramDescriptorProto == null)
            throw new NullPointerException();
          ensureMessageTypeIsMutable();
          this.messageType_.add(paramInt, paramDescriptorProto);
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.addMessage(paramInt, paramDescriptorProto);
        return this;
      }

      public Builder addMessageType(DescriptorProtos.DescriptorProto.Builder paramBuilder)
      {
        if (this.messageTypeBuilder_ == null)
        {
          ensureMessageTypeIsMutable();
          this.messageType_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addMessageType(DescriptorProtos.DescriptorProto paramDescriptorProto)
      {
        if (this.messageTypeBuilder_ == null)
        {
          if (paramDescriptorProto == null)
            throw new NullPointerException();
          ensureMessageTypeIsMutable();
          this.messageType_.add(paramDescriptorProto);
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.addMessage(paramDescriptorProto);
        return this;
      }

      public DescriptorProtos.DescriptorProto.Builder addMessageTypeBuilder()
      {
        return (DescriptorProtos.DescriptorProto.Builder)getMessageTypeFieldBuilder().addBuilder(DescriptorProtos.DescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.DescriptorProto.Builder addMessageTypeBuilder(int paramInt)
      {
        return (DescriptorProtos.DescriptorProto.Builder)getMessageTypeFieldBuilder().addBuilder(paramInt, DescriptorProtos.DescriptorProto.getDefaultInstance());
      }

      public Builder addService(int paramInt, DescriptorProtos.ServiceDescriptorProto.Builder paramBuilder)
      {
        if (this.serviceBuilder_ == null)
        {
          ensureServiceIsMutable();
          this.service_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.serviceBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addService(int paramInt, DescriptorProtos.ServiceDescriptorProto paramServiceDescriptorProto)
      {
        if (this.serviceBuilder_ == null)
        {
          if (paramServiceDescriptorProto == null)
            throw new NullPointerException();
          ensureServiceIsMutable();
          this.service_.add(paramInt, paramServiceDescriptorProto);
          onChanged();
          return this;
        }
        this.serviceBuilder_.addMessage(paramInt, paramServiceDescriptorProto);
        return this;
      }

      public Builder addService(DescriptorProtos.ServiceDescriptorProto.Builder paramBuilder)
      {
        if (this.serviceBuilder_ == null)
        {
          ensureServiceIsMutable();
          this.service_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.serviceBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addService(DescriptorProtos.ServiceDescriptorProto paramServiceDescriptorProto)
      {
        if (this.serviceBuilder_ == null)
        {
          if (paramServiceDescriptorProto == null)
            throw new NullPointerException();
          ensureServiceIsMutable();
          this.service_.add(paramServiceDescriptorProto);
          onChanged();
          return this;
        }
        this.serviceBuilder_.addMessage(paramServiceDescriptorProto);
        return this;
      }

      public DescriptorProtos.ServiceDescriptorProto.Builder addServiceBuilder()
      {
        return (DescriptorProtos.ServiceDescriptorProto.Builder)getServiceFieldBuilder().addBuilder(DescriptorProtos.ServiceDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.ServiceDescriptorProto.Builder addServiceBuilder(int paramInt)
      {
        return (DescriptorProtos.ServiceDescriptorProto.Builder)getServiceFieldBuilder().addBuilder(paramInt, DescriptorProtos.ServiceDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.FileDescriptorProto build()
      {
        DescriptorProtos.FileDescriptorProto localFileDescriptorProto = buildPartial();
        if (!localFileDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localFileDescriptorProto);
        return localFileDescriptorProto;
      }

      public DescriptorProtos.FileDescriptorProto buildPartial()
      {
        DescriptorProtos.FileDescriptorProto localFileDescriptorProto = new DescriptorProtos.FileDescriptorProto(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.FileDescriptorProto.access$1302(localFileDescriptorProto, this.name_);
        if ((i & 0x2) == 2)
          k |= 2;
        DescriptorProtos.FileDescriptorProto.access$1402(localFileDescriptorProto, this.package_);
        if ((0x4 & this.bitField0_) == 4)
        {
          this.dependency_ = new UnmodifiableLazyStringList(this.dependency_);
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        }
        DescriptorProtos.FileDescriptorProto.access$1502(localFileDescriptorProto, this.dependency_);
        if (this.messageTypeBuilder_ == null)
        {
          if ((0x8 & this.bitField0_) == 8)
          {
            this.messageType_ = Collections.unmodifiableList(this.messageType_);
            this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          }
          DescriptorProtos.FileDescriptorProto.access$1602(localFileDescriptorProto, this.messageType_);
          if (this.enumTypeBuilder_ != null)
            break label403;
          if ((0x10 & this.bitField0_) == 16)
          {
            this.enumType_ = Collections.unmodifiableList(this.enumType_);
            this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          }
          DescriptorProtos.FileDescriptorProto.access$1702(localFileDescriptorProto, this.enumType_);
          label208: if (this.serviceBuilder_ != null)
            break label418;
          if ((0x20 & this.bitField0_) == 32)
          {
            this.service_ = Collections.unmodifiableList(this.service_);
            this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
          }
          DescriptorProtos.FileDescriptorProto.access$1802(localFileDescriptorProto, this.service_);
          label258: if (this.extensionBuilder_ != null)
            break label433;
          if ((0x40 & this.bitField0_) == 64)
          {
            this.extension_ = Collections.unmodifiableList(this.extension_);
            this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
          }
          DescriptorProtos.FileDescriptorProto.access$1902(localFileDescriptorProto, this.extension_);
          label308: if ((i & 0x80) == 128)
            k |= 4;
          if (this.optionsBuilder_ != null)
            break label448;
          DescriptorProtos.FileDescriptorProto.access$2002(localFileDescriptorProto, this.options_);
          label341: if ((i & 0x100) == 256)
            k |= 8;
          if (this.sourceCodeInfoBuilder_ != null)
            break label466;
          DescriptorProtos.FileDescriptorProto.access$2102(localFileDescriptorProto, this.sourceCodeInfo_);
        }
        while (true)
        {
          DescriptorProtos.FileDescriptorProto.access$2202(localFileDescriptorProto, k);
          onBuilt();
          return localFileDescriptorProto;
          DescriptorProtos.FileDescriptorProto.access$1602(localFileDescriptorProto, this.messageTypeBuilder_.build());
          break;
          label403: DescriptorProtos.FileDescriptorProto.access$1702(localFileDescriptorProto, this.enumTypeBuilder_.build());
          break label208;
          label418: DescriptorProtos.FileDescriptorProto.access$1802(localFileDescriptorProto, this.serviceBuilder_.build());
          break label258;
          label433: DescriptorProtos.FileDescriptorProto.access$1902(localFileDescriptorProto, this.extensionBuilder_.build());
          break label308;
          label448: DescriptorProtos.FileDescriptorProto.access$2002(localFileDescriptorProto, (DescriptorProtos.FileOptions)this.optionsBuilder_.build());
          break label341;
          label466: DescriptorProtos.FileDescriptorProto.access$2102(localFileDescriptorProto, (DescriptorProtos.SourceCodeInfo)this.sourceCodeInfoBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.package_ = "";
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.dependency_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        if (this.messageTypeBuilder_ == null)
        {
          this.messageType_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          if (this.enumTypeBuilder_ != null)
            break label221;
          this.enumType_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          label107: if (this.serviceBuilder_ != null)
            break label231;
          this.service_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
          label132: if (this.extensionBuilder_ != null)
            break label241;
          this.extension_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
          label157: if (this.optionsBuilder_ != null)
            break label251;
          this.options_ = DescriptorProtos.FileOptions.getDefaultInstance();
          label171: this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
          if (this.sourceCodeInfoBuilder_ != null)
            break label262;
          this.sourceCodeInfo_ = DescriptorProtos.SourceCodeInfo.getDefaultInstance();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
          return this;
          this.messageTypeBuilder_.clear();
          break;
          label221: this.enumTypeBuilder_.clear();
          break label107;
          label231: this.serviceBuilder_.clear();
          break label132;
          label241: this.extensionBuilder_.clear();
          break label157;
          label251: this.optionsBuilder_.clear();
          break label171;
          label262: this.sourceCodeInfoBuilder_.clear();
        }
      }

      public Builder clearDependency()
      {
        this.dependency_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        onChanged();
        return this;
      }

      public Builder clearEnumType()
      {
        if (this.enumTypeBuilder_ == null)
        {
          this.enumType_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.clear();
        return this;
      }

      public Builder clearExtension()
      {
        if (this.extensionBuilder_ == null)
        {
          this.extension_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
          onChanged();
          return this;
        }
        this.extensionBuilder_.clear();
        return this;
      }

      public Builder clearMessageType()
      {
        if (this.messageTypeBuilder_ == null)
        {
          this.messageType_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.clear();
        return this;
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = DescriptorProtos.FileDescriptorProto.getDefaultInstance().getName();
        onChanged();
        return this;
      }

      public Builder clearOptions()
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = DescriptorProtos.FileOptions.getDefaultInstance();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clearPackage()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.package_ = DescriptorProtos.FileDescriptorProto.getDefaultInstance().getPackage();
        onChanged();
        return this;
      }

      public Builder clearService()
      {
        if (this.serviceBuilder_ == null)
        {
          this.service_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
          onChanged();
          return this;
        }
        this.serviceBuilder_.clear();
        return this;
      }

      public Builder clearSourceCodeInfo()
      {
        if (this.sourceCodeInfoBuilder_ == null)
        {
          this.sourceCodeInfo_ = DescriptorProtos.SourceCodeInfo.getDefaultInstance();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
          return this;
          this.sourceCodeInfoBuilder_.clear();
        }
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.FileDescriptorProto getDefaultInstanceForType()
      {
        return DescriptorProtos.FileDescriptorProto.getDefaultInstance();
      }

      public String getDependency(int paramInt)
      {
        return (String)this.dependency_.get(paramInt);
      }

      public int getDependencyCount()
      {
        return this.dependency_.size();
      }

      public List<String> getDependencyList()
      {
        return Collections.unmodifiableList(this.dependency_);
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.FileDescriptorProto.getDescriptor();
      }

      public DescriptorProtos.EnumDescriptorProto getEnumType(int paramInt)
      {
        if (this.enumTypeBuilder_ == null)
          return (DescriptorProtos.EnumDescriptorProto)this.enumType_.get(paramInt);
        return (DescriptorProtos.EnumDescriptorProto)this.enumTypeBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.EnumDescriptorProto.Builder getEnumTypeBuilder(int paramInt)
      {
        return (DescriptorProtos.EnumDescriptorProto.Builder)getEnumTypeFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.EnumDescriptorProto.Builder> getEnumTypeBuilderList()
      {
        return getEnumTypeFieldBuilder().getBuilderList();
      }

      public int getEnumTypeCount()
      {
        if (this.enumTypeBuilder_ == null)
          return this.enumType_.size();
        return this.enumTypeBuilder_.getCount();
      }

      public List<DescriptorProtos.EnumDescriptorProto> getEnumTypeList()
      {
        if (this.enumTypeBuilder_ == null)
          return Collections.unmodifiableList(this.enumType_);
        return this.enumTypeBuilder_.getMessageList();
      }

      public DescriptorProtos.EnumDescriptorProtoOrBuilder getEnumTypeOrBuilder(int paramInt)
      {
        if (this.enumTypeBuilder_ == null)
          return (DescriptorProtos.EnumDescriptorProtoOrBuilder)this.enumType_.get(paramInt);
        return (DescriptorProtos.EnumDescriptorProtoOrBuilder)this.enumTypeBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.EnumDescriptorProtoOrBuilder> getEnumTypeOrBuilderList()
      {
        if (this.enumTypeBuilder_ != null)
          return this.enumTypeBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.enumType_);
      }

      public DescriptorProtos.FieldDescriptorProto getExtension(int paramInt)
      {
        if (this.extensionBuilder_ == null)
          return (DescriptorProtos.FieldDescriptorProto)this.extension_.get(paramInt);
        return (DescriptorProtos.FieldDescriptorProto)this.extensionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.FieldDescriptorProto.Builder getExtensionBuilder(int paramInt)
      {
        return (DescriptorProtos.FieldDescriptorProto.Builder)getExtensionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.FieldDescriptorProto.Builder> getExtensionBuilderList()
      {
        return getExtensionFieldBuilder().getBuilderList();
      }

      public int getExtensionCount()
      {
        if (this.extensionBuilder_ == null)
          return this.extension_.size();
        return this.extensionBuilder_.getCount();
      }

      public List<DescriptorProtos.FieldDescriptorProto> getExtensionList()
      {
        if (this.extensionBuilder_ == null)
          return Collections.unmodifiableList(this.extension_);
        return this.extensionBuilder_.getMessageList();
      }

      public DescriptorProtos.FieldDescriptorProtoOrBuilder getExtensionOrBuilder(int paramInt)
      {
        if (this.extensionBuilder_ == null)
          return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.extension_.get(paramInt);
        return (DescriptorProtos.FieldDescriptorProtoOrBuilder)this.extensionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getExtensionOrBuilderList()
      {
        if (this.extensionBuilder_ != null)
          return this.extensionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.extension_);
      }

      public DescriptorProtos.DescriptorProto getMessageType(int paramInt)
      {
        if (this.messageTypeBuilder_ == null)
          return (DescriptorProtos.DescriptorProto)this.messageType_.get(paramInt);
        return (DescriptorProtos.DescriptorProto)this.messageTypeBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.DescriptorProto.Builder getMessageTypeBuilder(int paramInt)
      {
        return (DescriptorProtos.DescriptorProto.Builder)getMessageTypeFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.DescriptorProto.Builder> getMessageTypeBuilderList()
      {
        return getMessageTypeFieldBuilder().getBuilderList();
      }

      public int getMessageTypeCount()
      {
        if (this.messageTypeBuilder_ == null)
          return this.messageType_.size();
        return this.messageTypeBuilder_.getCount();
      }

      public List<DescriptorProtos.DescriptorProto> getMessageTypeList()
      {
        if (this.messageTypeBuilder_ == null)
          return Collections.unmodifiableList(this.messageType_);
        return this.messageTypeBuilder_.getMessageList();
      }

      public DescriptorProtos.DescriptorProtoOrBuilder getMessageTypeOrBuilder(int paramInt)
      {
        if (this.messageTypeBuilder_ == null)
          return (DescriptorProtos.DescriptorProtoOrBuilder)this.messageType_.get(paramInt);
        return (DescriptorProtos.DescriptorProtoOrBuilder)this.messageTypeBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.DescriptorProtoOrBuilder> getMessageTypeOrBuilderList()
      {
        if (this.messageTypeBuilder_ != null)
          return this.messageTypeBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.messageType_);
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.FileOptions getOptions()
      {
        if (this.optionsBuilder_ == null)
          return this.options_;
        return (DescriptorProtos.FileOptions)this.optionsBuilder_.getMessage();
      }

      public DescriptorProtos.FileOptions.Builder getOptionsBuilder()
      {
        this.bitField0_ = (0x80 | this.bitField0_);
        onChanged();
        return (DescriptorProtos.FileOptions.Builder)getOptionsFieldBuilder().getBuilder();
      }

      public DescriptorProtos.FileOptionsOrBuilder getOptionsOrBuilder()
      {
        if (this.optionsBuilder_ != null)
          return (DescriptorProtos.FileOptionsOrBuilder)this.optionsBuilder_.getMessageOrBuilder();
        return this.options_;
      }

      public String getPackage()
      {
        Object localObject = this.package_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.package_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.ServiceDescriptorProto getService(int paramInt)
      {
        if (this.serviceBuilder_ == null)
          return (DescriptorProtos.ServiceDescriptorProto)this.service_.get(paramInt);
        return (DescriptorProtos.ServiceDescriptorProto)this.serviceBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.ServiceDescriptorProto.Builder getServiceBuilder(int paramInt)
      {
        return (DescriptorProtos.ServiceDescriptorProto.Builder)getServiceFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.ServiceDescriptorProto.Builder> getServiceBuilderList()
      {
        return getServiceFieldBuilder().getBuilderList();
      }

      public int getServiceCount()
      {
        if (this.serviceBuilder_ == null)
          return this.service_.size();
        return this.serviceBuilder_.getCount();
      }

      public List<DescriptorProtos.ServiceDescriptorProto> getServiceList()
      {
        if (this.serviceBuilder_ == null)
          return Collections.unmodifiableList(this.service_);
        return this.serviceBuilder_.getMessageList();
      }

      public DescriptorProtos.ServiceDescriptorProtoOrBuilder getServiceOrBuilder(int paramInt)
      {
        if (this.serviceBuilder_ == null)
          return (DescriptorProtos.ServiceDescriptorProtoOrBuilder)this.service_.get(paramInt);
        return (DescriptorProtos.ServiceDescriptorProtoOrBuilder)this.serviceBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.ServiceDescriptorProtoOrBuilder> getServiceOrBuilderList()
      {
        if (this.serviceBuilder_ != null)
          return this.serviceBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.service_);
      }

      public DescriptorProtos.SourceCodeInfo getSourceCodeInfo()
      {
        if (this.sourceCodeInfoBuilder_ == null)
          return this.sourceCodeInfo_;
        return (DescriptorProtos.SourceCodeInfo)this.sourceCodeInfoBuilder_.getMessage();
      }

      public DescriptorProtos.SourceCodeInfo.Builder getSourceCodeInfoBuilder()
      {
        this.bitField0_ = (0x100 | this.bitField0_);
        onChanged();
        return (DescriptorProtos.SourceCodeInfo.Builder)getSourceCodeInfoFieldBuilder().getBuilder();
      }

      public DescriptorProtos.SourceCodeInfoOrBuilder getSourceCodeInfoOrBuilder()
      {
        if (this.sourceCodeInfoBuilder_ != null)
          return (DescriptorProtos.SourceCodeInfoOrBuilder)this.sourceCodeInfoBuilder_.getMessageOrBuilder();
        return this.sourceCodeInfo_;
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasOptions()
      {
        return (0x80 & this.bitField0_) == 128;
      }

      public boolean hasPackage()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasSourceCodeInfo()
      {
        return (0x100 & this.bitField0_) == 256;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_FileDescriptorProto_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getMessageTypeCount(); i++)
          if (!getMessageType(i).isInitialized())
            return false;
        for (int j = 0; j < getEnumTypeCount(); j++)
          if (!getEnumType(j).isInitialized())
            return false;
        for (int k = 0; k < getServiceCount(); k++)
          if (!getService(k).isInitialized())
            return false;
        for (int m = 0; m < getExtensionCount(); m++)
          if (!getExtension(m).isInitialized())
            return false;
        return (!hasOptions()) || (getOptions().isInitialized());
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.package_ = paramCodedInputStream.readBytes();
            break;
          case 26:
            ensureDependencyIsMutable();
            this.dependency_.add(paramCodedInputStream.readBytes());
            break;
          case 34:
            DescriptorProtos.DescriptorProto.Builder localBuilder6 = DescriptorProtos.DescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder6, paramExtensionRegistryLite);
            addMessageType(localBuilder6.buildPartial());
            break;
          case 42:
            DescriptorProtos.EnumDescriptorProto.Builder localBuilder5 = DescriptorProtos.EnumDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder5, paramExtensionRegistryLite);
            addEnumType(localBuilder5.buildPartial());
            break;
          case 50:
            DescriptorProtos.ServiceDescriptorProto.Builder localBuilder4 = DescriptorProtos.ServiceDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder4, paramExtensionRegistryLite);
            addService(localBuilder4.buildPartial());
            break;
          case 58:
            DescriptorProtos.FieldDescriptorProto.Builder localBuilder3 = DescriptorProtos.FieldDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder3, paramExtensionRegistryLite);
            addExtension(localBuilder3.buildPartial());
            break;
          case 66:
            DescriptorProtos.FileOptions.Builder localBuilder2 = DescriptorProtos.FileOptions.newBuilder();
            if (hasOptions())
              localBuilder2.mergeFrom(getOptions());
            paramCodedInputStream.readMessage(localBuilder2, paramExtensionRegistryLite);
            setOptions(localBuilder2.buildPartial());
            break;
          case 74:
            DescriptorProtos.SourceCodeInfo.Builder localBuilder1 = DescriptorProtos.SourceCodeInfo.newBuilder();
            if (hasSourceCodeInfo())
              localBuilder1.mergeFrom(getSourceCodeInfo());
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            setSourceCodeInfo(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.FileDescriptorProto paramFileDescriptorProto)
      {
        if (paramFileDescriptorProto == DescriptorProtos.FileDescriptorProto.getDefaultInstance())
          return this;
        if (paramFileDescriptorProto.hasName())
          setName(paramFileDescriptorProto.getName());
        if (paramFileDescriptorProto.hasPackage())
          setPackage(paramFileDescriptorProto.getPackage());
        if (!paramFileDescriptorProto.dependency_.isEmpty())
        {
          if (this.dependency_.isEmpty())
          {
            this.dependency_ = paramFileDescriptorProto.dependency_;
            this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
            onChanged();
          }
        }
        else
        {
          if (this.messageTypeBuilder_ != null)
            break label389;
          if (!paramFileDescriptorProto.messageType_.isEmpty())
          {
            if (!this.messageType_.isEmpty())
              break label368;
            this.messageType_ = paramFileDescriptorProto.messageType_;
            this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
            label138: onChanged();
          }
          label142: if (this.enumTypeBuilder_ != null)
            break label505;
          if (!paramFileDescriptorProto.enumType_.isEmpty())
          {
            if (!this.enumType_.isEmpty())
              break label484;
            this.enumType_ = paramFileDescriptorProto.enumType_;
            this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
            label192: onChanged();
          }
          label196: if (this.serviceBuilder_ != null)
            break label621;
          if (!paramFileDescriptorProto.service_.isEmpty())
          {
            if (!this.service_.isEmpty())
              break label600;
            this.service_ = paramFileDescriptorProto.service_;
            this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
            label246: onChanged();
          }
          label250: if (this.extensionBuilder_ != null)
            break label737;
          if (!paramFileDescriptorProto.extension_.isEmpty())
          {
            if (!this.extension_.isEmpty())
              break label716;
            this.extension_ = paramFileDescriptorProto.extension_;
            this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
            label300: onChanged();
          }
        }
        while (true)
        {
          if (paramFileDescriptorProto.hasOptions())
            mergeOptions(paramFileDescriptorProto.getOptions());
          if (paramFileDescriptorProto.hasSourceCodeInfo())
            mergeSourceCodeInfo(paramFileDescriptorProto.getSourceCodeInfo());
          mergeUnknownFields(paramFileDescriptorProto.getUnknownFields());
          return this;
          ensureDependencyIsMutable();
          this.dependency_.addAll(paramFileDescriptorProto.dependency_);
          break;
          label368: ensureMessageTypeIsMutable();
          this.messageType_.addAll(paramFileDescriptorProto.messageType_);
          break label138;
          label389: if (paramFileDescriptorProto.messageType_.isEmpty())
            break label142;
          if (this.messageTypeBuilder_.isEmpty())
          {
            this.messageTypeBuilder_.dispose();
            this.messageTypeBuilder_ = null;
            this.messageType_ = paramFileDescriptorProto.messageType_;
            this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
            if (GeneratedMessage.alwaysUseFieldBuilders);
            for (RepeatedFieldBuilder localRepeatedFieldBuilder4 = getMessageTypeFieldBuilder(); ; localRepeatedFieldBuilder4 = null)
            {
              this.messageTypeBuilder_ = localRepeatedFieldBuilder4;
              break;
            }
          }
          this.messageTypeBuilder_.addAllMessages(paramFileDescriptorProto.messageType_);
          break label142;
          label484: ensureEnumTypeIsMutable();
          this.enumType_.addAll(paramFileDescriptorProto.enumType_);
          break label192;
          label505: if (paramFileDescriptorProto.enumType_.isEmpty())
            break label196;
          if (this.enumTypeBuilder_.isEmpty())
          {
            this.enumTypeBuilder_.dispose();
            this.enumTypeBuilder_ = null;
            this.enumType_ = paramFileDescriptorProto.enumType_;
            this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
            if (GeneratedMessage.alwaysUseFieldBuilders);
            for (RepeatedFieldBuilder localRepeatedFieldBuilder3 = getEnumTypeFieldBuilder(); ; localRepeatedFieldBuilder3 = null)
            {
              this.enumTypeBuilder_ = localRepeatedFieldBuilder3;
              break;
            }
          }
          this.enumTypeBuilder_.addAllMessages(paramFileDescriptorProto.enumType_);
          break label196;
          label600: ensureServiceIsMutable();
          this.service_.addAll(paramFileDescriptorProto.service_);
          break label246;
          label621: if (paramFileDescriptorProto.service_.isEmpty())
            break label250;
          if (this.serviceBuilder_.isEmpty())
          {
            this.serviceBuilder_.dispose();
            this.serviceBuilder_ = null;
            this.service_ = paramFileDescriptorProto.service_;
            this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
            if (GeneratedMessage.alwaysUseFieldBuilders);
            for (RepeatedFieldBuilder localRepeatedFieldBuilder2 = getServiceFieldBuilder(); ; localRepeatedFieldBuilder2 = null)
            {
              this.serviceBuilder_ = localRepeatedFieldBuilder2;
              break;
            }
          }
          this.serviceBuilder_.addAllMessages(paramFileDescriptorProto.service_);
          break label250;
          label716: ensureExtensionIsMutable();
          this.extension_.addAll(paramFileDescriptorProto.extension_);
          break label300;
          label737: if (!paramFileDescriptorProto.extension_.isEmpty())
          {
            if (this.extensionBuilder_.isEmpty())
            {
              this.extensionBuilder_.dispose();
              this.extensionBuilder_ = null;
              this.extension_ = paramFileDescriptorProto.extension_;
              this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder1 = getExtensionFieldBuilder(); ; localRepeatedFieldBuilder1 = null)
              {
                this.extensionBuilder_ = localRepeatedFieldBuilder1;
                break;
              }
            }
            this.extensionBuilder_.addAllMessages(paramFileDescriptorProto.extension_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.FileDescriptorProto))
          return mergeFrom((DescriptorProtos.FileDescriptorProto)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder mergeOptions(DescriptorProtos.FileOptions paramFileOptions)
      {
        if (this.optionsBuilder_ == null)
          if (((0x80 & this.bitField0_) == 128) && (this.options_ != DescriptorProtos.FileOptions.getDefaultInstance()))
          {
            this.options_ = DescriptorProtos.FileOptions.newBuilder(this.options_).mergeFrom(paramFileOptions).buildPartial();
            onChanged();
          }
        while (true)
        {
          this.bitField0_ = (0x80 | this.bitField0_);
          return this;
          this.options_ = paramFileOptions;
          break;
          this.optionsBuilder_.mergeFrom(paramFileOptions);
        }
      }

      public Builder mergeSourceCodeInfo(DescriptorProtos.SourceCodeInfo paramSourceCodeInfo)
      {
        if (this.sourceCodeInfoBuilder_ == null)
          if (((0x100 & this.bitField0_) == 256) && (this.sourceCodeInfo_ != DescriptorProtos.SourceCodeInfo.getDefaultInstance()))
          {
            this.sourceCodeInfo_ = DescriptorProtos.SourceCodeInfo.newBuilder(this.sourceCodeInfo_).mergeFrom(paramSourceCodeInfo).buildPartial();
            onChanged();
          }
        while (true)
        {
          this.bitField0_ = (0x100 | this.bitField0_);
          return this;
          this.sourceCodeInfo_ = paramSourceCodeInfo;
          break;
          this.sourceCodeInfoBuilder_.mergeFrom(paramSourceCodeInfo);
        }
      }

      public Builder removeEnumType(int paramInt)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          this.enumType_.remove(paramInt);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.remove(paramInt);
        return this;
      }

      public Builder removeExtension(int paramInt)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          this.extension_.remove(paramInt);
          onChanged();
          return this;
        }
        this.extensionBuilder_.remove(paramInt);
        return this;
      }

      public Builder removeMessageType(int paramInt)
      {
        if (this.messageTypeBuilder_ == null)
        {
          ensureMessageTypeIsMutable();
          this.messageType_.remove(paramInt);
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.remove(paramInt);
        return this;
      }

      public Builder removeService(int paramInt)
      {
        if (this.serviceBuilder_ == null)
        {
          ensureServiceIsMutable();
          this.service_.remove(paramInt);
          onChanged();
          return this;
        }
        this.serviceBuilder_.remove(paramInt);
        return this;
      }

      public Builder setDependency(int paramInt, String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDependencyIsMutable();
        this.dependency_.set(paramInt, paramString);
        onChanged();
        return this;
      }

      public Builder setEnumType(int paramInt, DescriptorProtos.EnumDescriptorProto.Builder paramBuilder)
      {
        if (this.enumTypeBuilder_ == null)
        {
          ensureEnumTypeIsMutable();
          this.enumType_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setEnumType(int paramInt, DescriptorProtos.EnumDescriptorProto paramEnumDescriptorProto)
      {
        if (this.enumTypeBuilder_ == null)
        {
          if (paramEnumDescriptorProto == null)
            throw new NullPointerException();
          ensureEnumTypeIsMutable();
          this.enumType_.set(paramInt, paramEnumDescriptorProto);
          onChanged();
          return this;
        }
        this.enumTypeBuilder_.setMessage(paramInt, paramEnumDescriptorProto);
        return this;
      }

      public Builder setExtension(int paramInt, DescriptorProtos.FieldDescriptorProto.Builder paramBuilder)
      {
        if (this.extensionBuilder_ == null)
        {
          ensureExtensionIsMutable();
          this.extension_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.extensionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setExtension(int paramInt, DescriptorProtos.FieldDescriptorProto paramFieldDescriptorProto)
      {
        if (this.extensionBuilder_ == null)
        {
          if (paramFieldDescriptorProto == null)
            throw new NullPointerException();
          ensureExtensionIsMutable();
          this.extension_.set(paramInt, paramFieldDescriptorProto);
          onChanged();
          return this;
        }
        this.extensionBuilder_.setMessage(paramInt, paramFieldDescriptorProto);
        return this;
      }

      public Builder setMessageType(int paramInt, DescriptorProtos.DescriptorProto.Builder paramBuilder)
      {
        if (this.messageTypeBuilder_ == null)
        {
          ensureMessageTypeIsMutable();
          this.messageType_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setMessageType(int paramInt, DescriptorProtos.DescriptorProto paramDescriptorProto)
      {
        if (this.messageTypeBuilder_ == null)
        {
          if (paramDescriptorProto == null)
            throw new NullPointerException();
          ensureMessageTypeIsMutable();
          this.messageType_.set(paramInt, paramDescriptorProto);
          onChanged();
          return this;
        }
        this.messageTypeBuilder_.setMessage(paramInt, paramDescriptorProto);
        return this;
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        onChanged();
        return this;
      }

      void setName(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        onChanged();
      }

      public Builder setOptions(DescriptorProtos.FileOptions.Builder paramBuilder)
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = paramBuilder.build();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x80 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramBuilder.build());
        }
      }

      public Builder setOptions(DescriptorProtos.FileOptions paramFileOptions)
      {
        if (this.optionsBuilder_ == null)
        {
          if (paramFileOptions == null)
            throw new NullPointerException();
          this.options_ = paramFileOptions;
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x80 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramFileOptions);
        }
      }

      public Builder setPackage(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.package_ = paramString;
        onChanged();
        return this;
      }

      void setPackage(ByteString paramByteString)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.package_ = paramByteString;
        onChanged();
      }

      public Builder setService(int paramInt, DescriptorProtos.ServiceDescriptorProto.Builder paramBuilder)
      {
        if (this.serviceBuilder_ == null)
        {
          ensureServiceIsMutable();
          this.service_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.serviceBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setService(int paramInt, DescriptorProtos.ServiceDescriptorProto paramServiceDescriptorProto)
      {
        if (this.serviceBuilder_ == null)
        {
          if (paramServiceDescriptorProto == null)
            throw new NullPointerException();
          ensureServiceIsMutable();
          this.service_.set(paramInt, paramServiceDescriptorProto);
          onChanged();
          return this;
        }
        this.serviceBuilder_.setMessage(paramInt, paramServiceDescriptorProto);
        return this;
      }

      public Builder setSourceCodeInfo(DescriptorProtos.SourceCodeInfo.Builder paramBuilder)
      {
        if (this.sourceCodeInfoBuilder_ == null)
        {
          this.sourceCodeInfo_ = paramBuilder.build();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x100 | this.bitField0_);
          return this;
          this.sourceCodeInfoBuilder_.setMessage(paramBuilder.build());
        }
      }

      public Builder setSourceCodeInfo(DescriptorProtos.SourceCodeInfo paramSourceCodeInfo)
      {
        if (this.sourceCodeInfoBuilder_ == null)
        {
          if (paramSourceCodeInfo == null)
            throw new NullPointerException();
          this.sourceCodeInfo_ = paramSourceCodeInfo;
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x100 | this.bitField0_);
          return this;
          this.sourceCodeInfoBuilder_.setMessage(paramSourceCodeInfo);
        }
      }
    }
  }

  public static abstract interface FileDescriptorProtoOrBuilder extends MessageOrBuilder
  {
    public abstract String getDependency(int paramInt);

    public abstract int getDependencyCount();

    public abstract List<String> getDependencyList();

    public abstract DescriptorProtos.EnumDescriptorProto getEnumType(int paramInt);

    public abstract int getEnumTypeCount();

    public abstract List<DescriptorProtos.EnumDescriptorProto> getEnumTypeList();

    public abstract DescriptorProtos.EnumDescriptorProtoOrBuilder getEnumTypeOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.EnumDescriptorProtoOrBuilder> getEnumTypeOrBuilderList();

    public abstract DescriptorProtos.FieldDescriptorProto getExtension(int paramInt);

    public abstract int getExtensionCount();

    public abstract List<DescriptorProtos.FieldDescriptorProto> getExtensionList();

    public abstract DescriptorProtos.FieldDescriptorProtoOrBuilder getExtensionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.FieldDescriptorProtoOrBuilder> getExtensionOrBuilderList();

    public abstract DescriptorProtos.DescriptorProto getMessageType(int paramInt);

    public abstract int getMessageTypeCount();

    public abstract List<DescriptorProtos.DescriptorProto> getMessageTypeList();

    public abstract DescriptorProtos.DescriptorProtoOrBuilder getMessageTypeOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.DescriptorProtoOrBuilder> getMessageTypeOrBuilderList();

    public abstract String getName();

    public abstract DescriptorProtos.FileOptions getOptions();

    public abstract DescriptorProtos.FileOptionsOrBuilder getOptionsOrBuilder();

    public abstract String getPackage();

    public abstract DescriptorProtos.ServiceDescriptorProto getService(int paramInt);

    public abstract int getServiceCount();

    public abstract List<DescriptorProtos.ServiceDescriptorProto> getServiceList();

    public abstract DescriptorProtos.ServiceDescriptorProtoOrBuilder getServiceOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.ServiceDescriptorProtoOrBuilder> getServiceOrBuilderList();

    public abstract DescriptorProtos.SourceCodeInfo getSourceCodeInfo();

    public abstract DescriptorProtos.SourceCodeInfoOrBuilder getSourceCodeInfoOrBuilder();

    public abstract boolean hasName();

    public abstract boolean hasOptions();

    public abstract boolean hasPackage();

    public abstract boolean hasSourceCodeInfo();
  }

  public static final class FileDescriptorSet extends GeneratedMessage
    implements DescriptorProtos.FileDescriptorSetOrBuilder
  {
    public static final int FILE_FIELD_NUMBER = 1;
    private static final FileDescriptorSet defaultInstance = new FileDescriptorSet(true);
    private List<DescriptorProtos.FileDescriptorProto> file_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;

    static
    {
      defaultInstance.initFields();
    }

    private FileDescriptorSet(Builder paramBuilder)
    {
      super();
    }

    private FileDescriptorSet(boolean paramBoolean)
    {
    }

    public static FileDescriptorSet getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_FileDescriptorSet_descriptor;
    }

    private void initFields()
    {
      this.file_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$300();
    }

    public static Builder newBuilder(FileDescriptorSet paramFileDescriptorSet)
    {
      return newBuilder().mergeFrom(paramFileDescriptorSet);
    }

    public static FileDescriptorSet parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static FileDescriptorSet parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static FileDescriptorSet parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static FileDescriptorSet parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static FileDescriptorSet parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static FileDescriptorSet parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static FileDescriptorSet parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static FileDescriptorSet parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static FileDescriptorSet parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static FileDescriptorSet parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public FileDescriptorSet getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public DescriptorProtos.FileDescriptorProto getFile(int paramInt)
    {
      return (DescriptorProtos.FileDescriptorProto)this.file_.get(paramInt);
    }

    public int getFileCount()
    {
      return this.file_.size();
    }

    public List<DescriptorProtos.FileDescriptorProto> getFileList()
    {
      return this.file_;
    }

    public DescriptorProtos.FileDescriptorProtoOrBuilder getFileOrBuilder(int paramInt)
    {
      return (DescriptorProtos.FileDescriptorProtoOrBuilder)this.file_.get(paramInt);
    }

    public List<? extends DescriptorProtos.FileDescriptorProtoOrBuilder> getFileOrBuilderList()
    {
      return this.file_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.file_.size(); k++)
        j += CodedOutputStream.computeMessageSize(1, (MessageLite)this.file_.get(k));
      int m = j + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_FileDescriptorSet_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getFileCount(); j++)
        if (!getFile(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      for (int i = 0; i < this.file_.size(); i++)
        paramCodedOutputStream.writeMessage(1, (MessageLite)this.file_.get(i));
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.FileDescriptorSetOrBuilder
    {
      private int bitField0_;
      private RepeatedFieldBuilder<DescriptorProtos.FileDescriptorProto, DescriptorProtos.FileDescriptorProto.Builder, DescriptorProtos.FileDescriptorProtoOrBuilder> fileBuilder_;
      private List<DescriptorProtos.FileDescriptorProto> file_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.FileDescriptorSet buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.FileDescriptorSet localFileDescriptorSet = buildPartial();
        if (!localFileDescriptorSet.isInitialized())
          throw newUninitializedMessageException(localFileDescriptorSet).asInvalidProtocolBufferException();
        return localFileDescriptorSet;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureFileIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.file_ = new ArrayList(this.file_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_FileDescriptorSet_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.FileDescriptorProto, DescriptorProtos.FileDescriptorProto.Builder, DescriptorProtos.FileDescriptorProtoOrBuilder> getFileFieldBuilder()
      {
        List localList;
        if (this.fileBuilder_ == null)
        {
          localList = this.file_;
          if ((0x1 & this.bitField0_) != 1)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.fileBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.file_ = null;
          return this.fileBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getFileFieldBuilder();
      }

      public Builder addAllFile(Iterable<? extends DescriptorProtos.FileDescriptorProto> paramIterable)
      {
        if (this.fileBuilder_ == null)
        {
          ensureFileIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.file_);
          onChanged();
          return this;
        }
        this.fileBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addFile(int paramInt, DescriptorProtos.FileDescriptorProto.Builder paramBuilder)
      {
        if (this.fileBuilder_ == null)
        {
          ensureFileIsMutable();
          this.file_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.fileBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addFile(int paramInt, DescriptorProtos.FileDescriptorProto paramFileDescriptorProto)
      {
        if (this.fileBuilder_ == null)
        {
          if (paramFileDescriptorProto == null)
            throw new NullPointerException();
          ensureFileIsMutable();
          this.file_.add(paramInt, paramFileDescriptorProto);
          onChanged();
          return this;
        }
        this.fileBuilder_.addMessage(paramInt, paramFileDescriptorProto);
        return this;
      }

      public Builder addFile(DescriptorProtos.FileDescriptorProto.Builder paramBuilder)
      {
        if (this.fileBuilder_ == null)
        {
          ensureFileIsMutable();
          this.file_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.fileBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addFile(DescriptorProtos.FileDescriptorProto paramFileDescriptorProto)
      {
        if (this.fileBuilder_ == null)
        {
          if (paramFileDescriptorProto == null)
            throw new NullPointerException();
          ensureFileIsMutable();
          this.file_.add(paramFileDescriptorProto);
          onChanged();
          return this;
        }
        this.fileBuilder_.addMessage(paramFileDescriptorProto);
        return this;
      }

      public DescriptorProtos.FileDescriptorProto.Builder addFileBuilder()
      {
        return (DescriptorProtos.FileDescriptorProto.Builder)getFileFieldBuilder().addBuilder(DescriptorProtos.FileDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.FileDescriptorProto.Builder addFileBuilder(int paramInt)
      {
        return (DescriptorProtos.FileDescriptorProto.Builder)getFileFieldBuilder().addBuilder(paramInt, DescriptorProtos.FileDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.FileDescriptorSet build()
      {
        DescriptorProtos.FileDescriptorSet localFileDescriptorSet = buildPartial();
        if (!localFileDescriptorSet.isInitialized())
          throw newUninitializedMessageException(localFileDescriptorSet);
        return localFileDescriptorSet;
      }

      public DescriptorProtos.FileDescriptorSet buildPartial()
      {
        DescriptorProtos.FileDescriptorSet localFileDescriptorSet = new DescriptorProtos.FileDescriptorSet(this, null);
        if (this.fileBuilder_ == null)
        {
          if ((0x1 & this.bitField0_) == 1)
          {
            this.file_ = Collections.unmodifiableList(this.file_);
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
          DescriptorProtos.FileDescriptorSet.access$602(localFileDescriptorSet, this.file_);
        }
        while (true)
        {
          onBuilt();
          return localFileDescriptorSet;
          DescriptorProtos.FileDescriptorSet.access$602(localFileDescriptorSet, this.fileBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        if (this.fileBuilder_ == null)
        {
          this.file_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          return this;
        }
        this.fileBuilder_.clear();
        return this;
      }

      public Builder clearFile()
      {
        if (this.fileBuilder_ == null)
        {
          this.file_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          onChanged();
          return this;
        }
        this.fileBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.FileDescriptorSet getDefaultInstanceForType()
      {
        return DescriptorProtos.FileDescriptorSet.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.FileDescriptorSet.getDescriptor();
      }

      public DescriptorProtos.FileDescriptorProto getFile(int paramInt)
      {
        if (this.fileBuilder_ == null)
          return (DescriptorProtos.FileDescriptorProto)this.file_.get(paramInt);
        return (DescriptorProtos.FileDescriptorProto)this.fileBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.FileDescriptorProto.Builder getFileBuilder(int paramInt)
      {
        return (DescriptorProtos.FileDescriptorProto.Builder)getFileFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.FileDescriptorProto.Builder> getFileBuilderList()
      {
        return getFileFieldBuilder().getBuilderList();
      }

      public int getFileCount()
      {
        if (this.fileBuilder_ == null)
          return this.file_.size();
        return this.fileBuilder_.getCount();
      }

      public List<DescriptorProtos.FileDescriptorProto> getFileList()
      {
        if (this.fileBuilder_ == null)
          return Collections.unmodifiableList(this.file_);
        return this.fileBuilder_.getMessageList();
      }

      public DescriptorProtos.FileDescriptorProtoOrBuilder getFileOrBuilder(int paramInt)
      {
        if (this.fileBuilder_ == null)
          return (DescriptorProtos.FileDescriptorProtoOrBuilder)this.file_.get(paramInt);
        return (DescriptorProtos.FileDescriptorProtoOrBuilder)this.fileBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.FileDescriptorProtoOrBuilder> getFileOrBuilderList()
      {
        if (this.fileBuilder_ != null)
          return this.fileBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.file_);
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_FileDescriptorSet_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getFileCount(); i++)
          if (!getFile(i).isInitialized())
            return false;
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            DescriptorProtos.FileDescriptorProto.Builder localBuilder1 = DescriptorProtos.FileDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addFile(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.FileDescriptorSet paramFileDescriptorSet)
      {
        if (paramFileDescriptorSet == DescriptorProtos.FileDescriptorSet.getDefaultInstance())
          return this;
        if (this.fileBuilder_ == null)
          if (!paramFileDescriptorSet.file_.isEmpty())
          {
            if (!this.file_.isEmpty())
              break label74;
            this.file_ = paramFileDescriptorSet.file_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeUnknownFields(paramFileDescriptorSet.getUnknownFields());
          return this;
          label74: ensureFileIsMutable();
          this.file_.addAll(paramFileDescriptorSet.file_);
          break;
          if (!paramFileDescriptorSet.file_.isEmpty())
          {
            if (this.fileBuilder_.isEmpty())
            {
              this.fileBuilder_.dispose();
              this.fileBuilder_ = null;
              this.file_ = paramFileDescriptorSet.file_;
              this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getFileFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.fileBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.fileBuilder_.addAllMessages(paramFileDescriptorSet.file_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.FileDescriptorSet))
          return mergeFrom((DescriptorProtos.FileDescriptorSet)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeFile(int paramInt)
      {
        if (this.fileBuilder_ == null)
        {
          ensureFileIsMutable();
          this.file_.remove(paramInt);
          onChanged();
          return this;
        }
        this.fileBuilder_.remove(paramInt);
        return this;
      }

      public Builder setFile(int paramInt, DescriptorProtos.FileDescriptorProto.Builder paramBuilder)
      {
        if (this.fileBuilder_ == null)
        {
          ensureFileIsMutable();
          this.file_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.fileBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setFile(int paramInt, DescriptorProtos.FileDescriptorProto paramFileDescriptorProto)
      {
        if (this.fileBuilder_ == null)
        {
          if (paramFileDescriptorProto == null)
            throw new NullPointerException();
          ensureFileIsMutable();
          this.file_.set(paramInt, paramFileDescriptorProto);
          onChanged();
          return this;
        }
        this.fileBuilder_.setMessage(paramInt, paramFileDescriptorProto);
        return this;
      }
    }
  }

  public static abstract interface FileDescriptorSetOrBuilder extends MessageOrBuilder
  {
    public abstract DescriptorProtos.FileDescriptorProto getFile(int paramInt);

    public abstract int getFileCount();

    public abstract List<DescriptorProtos.FileDescriptorProto> getFileList();

    public abstract DescriptorProtos.FileDescriptorProtoOrBuilder getFileOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.FileDescriptorProtoOrBuilder> getFileOrBuilderList();
  }

  public static final class FileOptions extends GeneratedMessage.ExtendableMessage<FileOptions>
    implements DescriptorProtos.FileOptionsOrBuilder
  {
    public static final int CC_GENERIC_SERVICES_FIELD_NUMBER = 16;
    public static final int JAVA_GENERATE_EQUALS_AND_HASH_FIELD_NUMBER = 20;
    public static final int JAVA_GENERIC_SERVICES_FIELD_NUMBER = 17;
    public static final int JAVA_MULTIPLE_FILES_FIELD_NUMBER = 10;
    public static final int JAVA_OUTER_CLASSNAME_FIELD_NUMBER = 8;
    public static final int JAVA_PACKAGE_FIELD_NUMBER = 1;
    public static final int OPTIMIZE_FOR_FIELD_NUMBER = 9;
    public static final int PY_GENERIC_SERVICES_FIELD_NUMBER = 18;
    public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
    private static final FileOptions defaultInstance = new FileOptions(true);
    private int bitField0_;
    private boolean ccGenericServices_;
    private boolean javaGenerateEqualsAndHash_;
    private boolean javaGenericServices_;
    private boolean javaMultipleFiles_;
    private Object javaOuterClassname_;
    private Object javaPackage_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private OptimizeMode optimizeFor_;
    private boolean pyGenericServices_;
    private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_;

    static
    {
      defaultInstance.initFields();
    }

    private FileOptions(Builder paramBuilder)
    {
      super();
    }

    private FileOptions(boolean paramBoolean)
    {
    }

    public static FileOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_FileOptions_descriptor;
    }

    private ByteString getJavaOuterClassnameBytes()
    {
      Object localObject = this.javaOuterClassname_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.javaOuterClassname_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getJavaPackageBytes()
    {
      Object localObject = this.javaPackage_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.javaPackage_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.javaPackage_ = "";
      this.javaOuterClassname_ = "";
      this.javaMultipleFiles_ = false;
      this.javaGenerateEqualsAndHash_ = false;
      this.optimizeFor_ = OptimizeMode.SPEED;
      this.ccGenericServices_ = false;
      this.javaGenericServices_ = false;
      this.pyGenericServices_ = false;
      this.uninterpretedOption_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$10500();
    }

    public static Builder newBuilder(FileOptions paramFileOptions)
    {
      return newBuilder().mergeFrom(paramFileOptions);
    }

    public static FileOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static FileOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static FileOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static FileOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static FileOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static FileOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static FileOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static FileOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static FileOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static FileOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public boolean getCcGenericServices()
    {
      return this.ccGenericServices_;
    }

    public FileOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public boolean getJavaGenerateEqualsAndHash()
    {
      return this.javaGenerateEqualsAndHash_;
    }

    public boolean getJavaGenericServices()
    {
      return this.javaGenericServices_;
    }

    public boolean getJavaMultipleFiles()
    {
      return this.javaMultipleFiles_;
    }

    public String getJavaOuterClassname()
    {
      Object localObject = this.javaOuterClassname_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.javaOuterClassname_ = str;
      return str;
    }

    public String getJavaPackage()
    {
      Object localObject = this.javaPackage_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.javaPackage_ = str;
      return str;
    }

    public OptimizeMode getOptimizeFor()
    {
      return this.optimizeFor_;
    }

    public boolean getPyGenericServices()
    {
      return this.pyGenericServices_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getJavaPackageBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBytesSize(8, getJavaOuterClassnameBytes());
      if ((0x10 & this.bitField0_) == 16)
        k += CodedOutputStream.computeEnumSize(9, this.optimizeFor_.getNumber());
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBoolSize(10, this.javaMultipleFiles_);
      if ((0x20 & this.bitField0_) == 32)
        k += CodedOutputStream.computeBoolSize(16, this.ccGenericServices_);
      if ((0x40 & this.bitField0_) == 64)
        k += CodedOutputStream.computeBoolSize(17, this.javaGenericServices_);
      if ((0x80 & this.bitField0_) == 128)
        k += CodedOutputStream.computeBoolSize(18, this.pyGenericServices_);
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeBoolSize(20, this.javaGenerateEqualsAndHash_);
      for (int m = 0; m < this.uninterpretedOption_.size(); m++)
        k += CodedOutputStream.computeMessageSize(999, (MessageLite)this.uninterpretedOption_.get(m));
      int n = k + extensionsSerializedSize() + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = n;
      return n;
    }

    public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
    }

    public int getUninterpretedOptionCount()
    {
      return this.uninterpretedOption_.size();
    }

    public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
    {
      return this.uninterpretedOption_;
    }

    public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
    }

    public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
    {
      return this.uninterpretedOption_;
    }

    public boolean hasCcGenericServices()
    {
      return (0x20 & this.bitField0_) == 32;
    }

    public boolean hasJavaGenerateEqualsAndHash()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasJavaGenericServices()
    {
      return (0x40 & this.bitField0_) == 64;
    }

    public boolean hasJavaMultipleFiles()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasJavaOuterClassname()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasJavaPackage()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasOptimizeFor()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    public boolean hasPyGenericServices()
    {
      return (0x80 & this.bitField0_) == 128;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_FileOptions_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getUninterpretedOptionCount(); j++)
        if (!getUninterpretedOption(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessage.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getJavaPackageBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBytes(8, getJavaOuterClassnameBytes());
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeEnum(9, this.optimizeFor_.getNumber());
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBool(10, this.javaMultipleFiles_);
      if ((0x20 & this.bitField0_) == 32)
        paramCodedOutputStream.writeBool(16, this.ccGenericServices_);
      if ((0x40 & this.bitField0_) == 64)
        paramCodedOutputStream.writeBool(17, this.javaGenericServices_);
      if ((0x80 & this.bitField0_) == 128)
        paramCodedOutputStream.writeBool(18, this.pyGenericServices_);
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeBool(20, this.javaGenerateEqualsAndHash_);
      for (int i = 0; i < this.uninterpretedOption_.size(); i++)
        paramCodedOutputStream.writeMessage(999, (MessageLite)this.uninterpretedOption_.get(i));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.ExtendableBuilder<DescriptorProtos.FileOptions, Builder>
      implements DescriptorProtos.FileOptionsOrBuilder
    {
      private int bitField0_;
      private boolean ccGenericServices_;
      private boolean javaGenerateEqualsAndHash_;
      private boolean javaGenericServices_;
      private boolean javaMultipleFiles_;
      private Object javaOuterClassname_ = "";
      private Object javaPackage_ = "";
      private DescriptorProtos.FileOptions.OptimizeMode optimizeFor_ = DescriptorProtos.FileOptions.OptimizeMode.SPEED;
      private boolean pyGenericServices_;
      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> uninterpretedOptionBuilder_;
      private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.FileOptions buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.FileOptions localFileOptions = buildPartial();
        if (!localFileOptions.isInitialized())
          throw newUninitializedMessageException(localFileOptions).asInvalidProtocolBufferException();
        return localFileOptions;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureUninterpretedOptionIsMutable()
      {
        if ((0x100 & this.bitField0_) != 256)
        {
          this.uninterpretedOption_ = new ArrayList(this.uninterpretedOption_);
          this.bitField0_ = (0x100 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_FileOptions_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionFieldBuilder()
      {
        List localList;
        if (this.uninterpretedOptionBuilder_ == null)
        {
          localList = this.uninterpretedOption_;
          if ((0x100 & this.bitField0_) != 256)
            break label59;
        }
        label59: for (boolean bool = true; ; bool = false)
        {
          this.uninterpretedOptionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.uninterpretedOption_ = null;
          return this.uninterpretedOptionBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getUninterpretedOptionFieldBuilder();
      }

      public Builder addAllUninterpretedOption(Iterable<? extends DescriptorProtos.UninterpretedOption> paramIterable)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          GeneratedMessage.ExtendableBuilder.addAll(paramIterable, this.uninterpretedOption_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramUninterpretedOption);
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramUninterpretedOption);
        return this;
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder()
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(paramInt, DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.FileOptions build()
      {
        DescriptorProtos.FileOptions localFileOptions = buildPartial();
        if (!localFileOptions.isInitialized())
          throw newUninitializedMessageException(localFileOptions);
        return localFileOptions;
      }

      public DescriptorProtos.FileOptions buildPartial()
      {
        DescriptorProtos.FileOptions localFileOptions = new DescriptorProtos.FileOptions(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.FileOptions.access$10802(localFileOptions, this.javaPackage_);
        if ((i & 0x2) == 2)
          k |= 2;
        DescriptorProtos.FileOptions.access$10902(localFileOptions, this.javaOuterClassname_);
        if ((i & 0x4) == 4)
          k |= 4;
        DescriptorProtos.FileOptions.access$11002(localFileOptions, this.javaMultipleFiles_);
        if ((i & 0x8) == 8)
          k |= 8;
        DescriptorProtos.FileOptions.access$11102(localFileOptions, this.javaGenerateEqualsAndHash_);
        if ((i & 0x10) == 16)
          k |= 16;
        DescriptorProtos.FileOptions.access$11202(localFileOptions, this.optimizeFor_);
        if ((i & 0x20) == 32)
          k |= 32;
        DescriptorProtos.FileOptions.access$11302(localFileOptions, this.ccGenericServices_);
        if ((i & 0x40) == 64)
          k |= 64;
        DescriptorProtos.FileOptions.access$11402(localFileOptions, this.javaGenericServices_);
        if ((i & 0x80) == 128)
          k |= 128;
        DescriptorProtos.FileOptions.access$11502(localFileOptions, this.pyGenericServices_);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if ((0x100 & this.bitField0_) == 256)
          {
            this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
            this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
          }
          DescriptorProtos.FileOptions.access$11602(localFileOptions, this.uninterpretedOption_);
        }
        while (true)
        {
          DescriptorProtos.FileOptions.access$11702(localFileOptions, k);
          onBuilt();
          return localFileOptions;
          DescriptorProtos.FileOptions.access$11602(localFileOptions, this.uninterpretedOptionBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.javaPackage_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.javaOuterClassname_ = "";
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.javaMultipleFiles_ = false;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.javaGenerateEqualsAndHash_ = false;
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.optimizeFor_ = DescriptorProtos.FileOptions.OptimizeMode.SPEED;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.ccGenericServices_ = false;
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.javaGenericServices_ = false;
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.pyGenericServices_ = false;
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clearCcGenericServices()
      {
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.ccGenericServices_ = false;
        onChanged();
        return this;
      }

      public Builder clearJavaGenerateEqualsAndHash()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.javaGenerateEqualsAndHash_ = false;
        onChanged();
        return this;
      }

      public Builder clearJavaGenericServices()
      {
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.javaGenericServices_ = false;
        onChanged();
        return this;
      }

      public Builder clearJavaMultipleFiles()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.javaMultipleFiles_ = false;
        onChanged();
        return this;
      }

      public Builder clearJavaOuterClassname()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.javaOuterClassname_ = DescriptorProtos.FileOptions.getDefaultInstance().getJavaOuterClassname();
        onChanged();
        return this;
      }

      public Builder clearJavaPackage()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.javaPackage_ = DescriptorProtos.FileOptions.getDefaultInstance().getJavaPackage();
        onChanged();
        return this;
      }

      public Builder clearOptimizeFor()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.optimizeFor_ = DescriptorProtos.FileOptions.OptimizeMode.SPEED;
        onChanged();
        return this;
      }

      public Builder clearPyGenericServices()
      {
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        this.pyGenericServices_ = false;
        onChanged();
        return this;
      }

      public Builder clearUninterpretedOption()
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public boolean getCcGenericServices()
      {
        return this.ccGenericServices_;
      }

      public DescriptorProtos.FileOptions getDefaultInstanceForType()
      {
        return DescriptorProtos.FileOptions.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.FileOptions.getDescriptor();
      }

      public boolean getJavaGenerateEqualsAndHash()
      {
        return this.javaGenerateEqualsAndHash_;
      }

      public boolean getJavaGenericServices()
      {
        return this.javaGenericServices_;
      }

      public boolean getJavaMultipleFiles()
      {
        return this.javaMultipleFiles_;
      }

      public String getJavaOuterClassname()
      {
        Object localObject = this.javaOuterClassname_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.javaOuterClassname_ = str;
          return str;
        }
        return (String)localObject;
      }

      public String getJavaPackage()
      {
        Object localObject = this.javaPackage_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.javaPackage_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.FileOptions.OptimizeMode getOptimizeFor()
      {
        return this.optimizeFor_;
      }

      public boolean getPyGenericServices()
      {
        return this.pyGenericServices_;
      }

      public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption)this.uninterpretedOptionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.UninterpretedOption.Builder getUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.UninterpretedOption.Builder> getUninterpretedOptionBuilderList()
      {
        return getUninterpretedOptionFieldBuilder().getBuilderList();
      }

      public int getUninterpretedOptionCount()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return this.uninterpretedOption_.size();
        return this.uninterpretedOptionBuilder_.getCount();
      }

      public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return Collections.unmodifiableList(this.uninterpretedOption_);
        return this.uninterpretedOptionBuilder_.getMessageList();
      }

      public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOptionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
      {
        if (this.uninterpretedOptionBuilder_ != null)
          return this.uninterpretedOptionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.uninterpretedOption_);
      }

      public boolean hasCcGenericServices()
      {
        return (0x20 & this.bitField0_) == 32;
      }

      public boolean hasJavaGenerateEqualsAndHash()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasJavaGenericServices()
      {
        return (0x40 & this.bitField0_) == 64;
      }

      public boolean hasJavaMultipleFiles()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasJavaOuterClassname()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasJavaPackage()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasOptimizeFor()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public boolean hasPyGenericServices()
      {
        return (0x80 & this.bitField0_) == 128;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_FileOptions_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getUninterpretedOptionCount(); i++)
          if (!getUninterpretedOption(i).isInitialized())
            return false;
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.javaPackage_ = paramCodedInputStream.readBytes();
            break;
          case 66:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.javaOuterClassname_ = paramCodedInputStream.readBytes();
            break;
          case 72:
            int j = paramCodedInputStream.readEnum();
            DescriptorProtos.FileOptions.OptimizeMode localOptimizeMode = DescriptorProtos.FileOptions.OptimizeMode.valueOf(j);
            if (localOptimizeMode == null)
            {
              localBuilder.mergeVarintField(9, j);
            }
            else
            {
              this.bitField0_ = (0x10 | this.bitField0_);
              this.optimizeFor_ = localOptimizeMode;
            }
            break;
          case 80:
            this.bitField0_ = (0x4 | this.bitField0_);
            this.javaMultipleFiles_ = paramCodedInputStream.readBool();
            break;
          case 128:
            this.bitField0_ = (0x20 | this.bitField0_);
            this.ccGenericServices_ = paramCodedInputStream.readBool();
            break;
          case 136:
            this.bitField0_ = (0x40 | this.bitField0_);
            this.javaGenericServices_ = paramCodedInputStream.readBool();
            break;
          case 144:
            this.bitField0_ = (0x80 | this.bitField0_);
            this.pyGenericServices_ = paramCodedInputStream.readBool();
            break;
          case 160:
            this.bitField0_ = (0x8 | this.bitField0_);
            this.javaGenerateEqualsAndHash_ = paramCodedInputStream.readBool();
            break;
          case 7994:
            DescriptorProtos.UninterpretedOption.Builder localBuilder1 = DescriptorProtos.UninterpretedOption.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addUninterpretedOption(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.FileOptions paramFileOptions)
      {
        if (paramFileOptions == DescriptorProtos.FileOptions.getDefaultInstance())
          return this;
        if (paramFileOptions.hasJavaPackage())
          setJavaPackage(paramFileOptions.getJavaPackage());
        if (paramFileOptions.hasJavaOuterClassname())
          setJavaOuterClassname(paramFileOptions.getJavaOuterClassname());
        if (paramFileOptions.hasJavaMultipleFiles())
          setJavaMultipleFiles(paramFileOptions.getJavaMultipleFiles());
        if (paramFileOptions.hasJavaGenerateEqualsAndHash())
          setJavaGenerateEqualsAndHash(paramFileOptions.getJavaGenerateEqualsAndHash());
        if (paramFileOptions.hasOptimizeFor())
          setOptimizeFor(paramFileOptions.getOptimizeFor());
        if (paramFileOptions.hasCcGenericServices())
          setCcGenericServices(paramFileOptions.getCcGenericServices());
        if (paramFileOptions.hasJavaGenericServices())
          setJavaGenericServices(paramFileOptions.getJavaGenericServices());
        if (paramFileOptions.hasPyGenericServices())
          setPyGenericServices(paramFileOptions.getPyGenericServices());
        if (this.uninterpretedOptionBuilder_ == null)
          if (!paramFileOptions.uninterpretedOption_.isEmpty())
          {
            if (!this.uninterpretedOption_.isEmpty())
              break label208;
            this.uninterpretedOption_ = paramFileOptions.uninterpretedOption_;
            this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeExtensionFields(paramFileOptions);
          mergeUnknownFields(paramFileOptions.getUnknownFields());
          return this;
          label208: ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.addAll(paramFileOptions.uninterpretedOption_);
          break;
          if (!paramFileOptions.uninterpretedOption_.isEmpty())
          {
            if (this.uninterpretedOptionBuilder_.isEmpty())
            {
              this.uninterpretedOptionBuilder_.dispose();
              this.uninterpretedOptionBuilder_ = null;
              this.uninterpretedOption_ = paramFileOptions.uninterpretedOption_;
              this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getUninterpretedOptionFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.uninterpretedOptionBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.uninterpretedOptionBuilder_.addAllMessages(paramFileOptions.uninterpretedOption_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.FileOptions))
          return mergeFrom((DescriptorProtos.FileOptions)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.remove(paramInt);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.remove(paramInt);
        return this;
      }

      public Builder setCcGenericServices(boolean paramBoolean)
      {
        this.bitField0_ = (0x20 | this.bitField0_);
        this.ccGenericServices_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setJavaGenerateEqualsAndHash(boolean paramBoolean)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.javaGenerateEqualsAndHash_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setJavaGenericServices(boolean paramBoolean)
      {
        this.bitField0_ = (0x40 | this.bitField0_);
        this.javaGenericServices_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setJavaMultipleFiles(boolean paramBoolean)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.javaMultipleFiles_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setJavaOuterClassname(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.javaOuterClassname_ = paramString;
        onChanged();
        return this;
      }

      void setJavaOuterClassname(ByteString paramByteString)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.javaOuterClassname_ = paramByteString;
        onChanged();
      }

      public Builder setJavaPackage(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.javaPackage_ = paramString;
        onChanged();
        return this;
      }

      void setJavaPackage(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.javaPackage_ = paramByteString;
        onChanged();
      }

      public Builder setOptimizeFor(DescriptorProtos.FileOptions.OptimizeMode paramOptimizeMode)
      {
        if (paramOptimizeMode == null)
          throw new NullPointerException();
        this.bitField0_ = (0x10 | this.bitField0_);
        this.optimizeFor_ = paramOptimizeMode;
        onChanged();
        return this;
      }

      public Builder setPyGenericServices(boolean paramBoolean)
      {
        this.bitField0_ = (0x80 | this.bitField0_);
        this.pyGenericServices_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramUninterpretedOption);
        return this;
      }
    }

    public static enum OptimizeMode
      implements ProtocolMessageEnum
    {
      public static final int CODE_SIZE_VALUE = 2;
      public static final int LITE_RUNTIME_VALUE = 3;
      public static final int SPEED_VALUE = 1;
      private static final OptimizeMode[] VALUES = arrayOfOptimizeMode2;
      private static Internal.EnumLiteMap<OptimizeMode> internalValueMap;
      private final int index;
      private final int value;

      static
      {
        CODE_SIZE = new OptimizeMode("CODE_SIZE", 1, 1, 2);
        LITE_RUNTIME = new OptimizeMode("LITE_RUNTIME", 2, 2, 3);
        OptimizeMode[] arrayOfOptimizeMode1 = new OptimizeMode[3];
        arrayOfOptimizeMode1[0] = SPEED;
        arrayOfOptimizeMode1[1] = CODE_SIZE;
        arrayOfOptimizeMode1[2] = LITE_RUNTIME;
        $VALUES = arrayOfOptimizeMode1;
        internalValueMap = new Internal.EnumLiteMap()
        {
          public DescriptorProtos.FileOptions.OptimizeMode findValueByNumber(int paramAnonymousInt)
          {
            return DescriptorProtos.FileOptions.OptimizeMode.valueOf(paramAnonymousInt);
          }
        };
        OptimizeMode[] arrayOfOptimizeMode2 = new OptimizeMode[3];
        arrayOfOptimizeMode2[0] = SPEED;
        arrayOfOptimizeMode2[1] = CODE_SIZE;
        arrayOfOptimizeMode2[2] = LITE_RUNTIME;
      }

      private OptimizeMode(int paramInt1, int paramInt2)
      {
        this.index = paramInt1;
        this.value = paramInt2;
      }

      public static final Descriptors.EnumDescriptor getDescriptor()
      {
        return (Descriptors.EnumDescriptor)DescriptorProtos.FileOptions.getDescriptor().getEnumTypes().get(0);
      }

      public static Internal.EnumLiteMap<OptimizeMode> internalGetValueMap()
      {
        return internalValueMap;
      }

      public static OptimizeMode valueOf(int paramInt)
      {
        switch (paramInt)
        {
        default:
          return null;
        case 1:
          return SPEED;
        case 2:
          return CODE_SIZE;
        case 3:
        }
        return LITE_RUNTIME;
      }

      public static OptimizeMode valueOf(Descriptors.EnumValueDescriptor paramEnumValueDescriptor)
      {
        if (paramEnumValueDescriptor.getType() != getDescriptor())
          throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
        return VALUES[paramEnumValueDescriptor.getIndex()];
      }

      public final Descriptors.EnumDescriptor getDescriptorForType()
      {
        return getDescriptor();
      }

      public final int getNumber()
      {
        return this.value;
      }

      public final Descriptors.EnumValueDescriptor getValueDescriptor()
      {
        return (Descriptors.EnumValueDescriptor)getDescriptor().getValues().get(this.index);
      }
    }
  }

  public static abstract interface FileOptionsOrBuilder extends GeneratedMessage.ExtendableMessageOrBuilder<DescriptorProtos.FileOptions>
  {
    public abstract boolean getCcGenericServices();

    public abstract boolean getJavaGenerateEqualsAndHash();

    public abstract boolean getJavaGenericServices();

    public abstract boolean getJavaMultipleFiles();

    public abstract String getJavaOuterClassname();

    public abstract String getJavaPackage();

    public abstract DescriptorProtos.FileOptions.OptimizeMode getOptimizeFor();

    public abstract boolean getPyGenericServices();

    public abstract DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt);

    public abstract int getUninterpretedOptionCount();

    public abstract List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList();

    public abstract DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList();

    public abstract boolean hasCcGenericServices();

    public abstract boolean hasJavaGenerateEqualsAndHash();

    public abstract boolean hasJavaGenericServices();

    public abstract boolean hasJavaMultipleFiles();

    public abstract boolean hasJavaOuterClassname();

    public abstract boolean hasJavaPackage();

    public abstract boolean hasOptimizeFor();

    public abstract boolean hasPyGenericServices();
  }

  public static final class MessageOptions extends GeneratedMessage.ExtendableMessage<MessageOptions>
    implements DescriptorProtos.MessageOptionsOrBuilder
  {
    public static final int MESSAGE_SET_WIRE_FORMAT_FIELD_NUMBER = 1;
    public static final int NO_STANDARD_DESCRIPTOR_ACCESSOR_FIELD_NUMBER = 2;
    public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
    private static final MessageOptions defaultInstance = new MessageOptions(true);
    private int bitField0_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private boolean messageSetWireFormat_;
    private boolean noStandardDescriptorAccessor_;
    private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_;

    static
    {
      defaultInstance.initFields();
    }

    private MessageOptions(Builder paramBuilder)
    {
      super();
    }

    private MessageOptions(boolean paramBoolean)
    {
    }

    public static MessageOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_MessageOptions_descriptor;
    }

    private void initFields()
    {
      this.messageSetWireFormat_ = false;
      this.noStandardDescriptorAccessor_ = false;
      this.uninterpretedOption_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$12100();
    }

    public static Builder newBuilder(MessageOptions paramMessageOptions)
    {
      return newBuilder().mergeFrom(paramMessageOptions);
    }

    public static MessageOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static MessageOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static MessageOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static MessageOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static MessageOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static MessageOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static MessageOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static MessageOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static MessageOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static MessageOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public MessageOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public boolean getMessageSetWireFormat()
    {
      return this.messageSetWireFormat_;
    }

    public boolean getNoStandardDescriptorAccessor()
    {
      return this.noStandardDescriptorAccessor_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBoolSize(1, this.messageSetWireFormat_);
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBoolSize(2, this.noStandardDescriptorAccessor_);
      for (int m = 0; m < this.uninterpretedOption_.size(); m++)
        k += CodedOutputStream.computeMessageSize(999, (MessageLite)this.uninterpretedOption_.get(m));
      int n = k + extensionsSerializedSize() + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = n;
      return n;
    }

    public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
    }

    public int getUninterpretedOptionCount()
    {
      return this.uninterpretedOption_.size();
    }

    public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
    {
      return this.uninterpretedOption_;
    }

    public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
    }

    public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
    {
      return this.uninterpretedOption_;
    }

    public boolean hasMessageSetWireFormat()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasNoStandardDescriptorAccessor()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_MessageOptions_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getUninterpretedOptionCount(); j++)
        if (!getUninterpretedOption(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessage.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBool(1, this.messageSetWireFormat_);
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBool(2, this.noStandardDescriptorAccessor_);
      for (int i = 0; i < this.uninterpretedOption_.size(); i++)
        paramCodedOutputStream.writeMessage(999, (MessageLite)this.uninterpretedOption_.get(i));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.ExtendableBuilder<DescriptorProtos.MessageOptions, Builder>
      implements DescriptorProtos.MessageOptionsOrBuilder
    {
      private int bitField0_;
      private boolean messageSetWireFormat_;
      private boolean noStandardDescriptorAccessor_;
      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> uninterpretedOptionBuilder_;
      private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.MessageOptions buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.MessageOptions localMessageOptions = buildPartial();
        if (!localMessageOptions.isInitialized())
          throw newUninitializedMessageException(localMessageOptions).asInvalidProtocolBufferException();
        return localMessageOptions;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureUninterpretedOptionIsMutable()
      {
        if ((0x4 & this.bitField0_) != 4)
        {
          this.uninterpretedOption_ = new ArrayList(this.uninterpretedOption_);
          this.bitField0_ = (0x4 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_MessageOptions_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionFieldBuilder()
      {
        List localList;
        if (this.uninterpretedOptionBuilder_ == null)
        {
          localList = this.uninterpretedOption_;
          if ((0x4 & this.bitField0_) != 4)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.uninterpretedOptionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.uninterpretedOption_ = null;
          return this.uninterpretedOptionBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getUninterpretedOptionFieldBuilder();
      }

      public Builder addAllUninterpretedOption(Iterable<? extends DescriptorProtos.UninterpretedOption> paramIterable)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          GeneratedMessage.ExtendableBuilder.addAll(paramIterable, this.uninterpretedOption_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramUninterpretedOption);
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramUninterpretedOption);
        return this;
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder()
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(paramInt, DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.MessageOptions build()
      {
        DescriptorProtos.MessageOptions localMessageOptions = buildPartial();
        if (!localMessageOptions.isInitialized())
          throw newUninitializedMessageException(localMessageOptions);
        return localMessageOptions;
      }

      public DescriptorProtos.MessageOptions buildPartial()
      {
        DescriptorProtos.MessageOptions localMessageOptions = new DescriptorProtos.MessageOptions(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.MessageOptions.access$12402(localMessageOptions, this.messageSetWireFormat_);
        if ((i & 0x2) == 2)
          k |= 2;
        DescriptorProtos.MessageOptions.access$12502(localMessageOptions, this.noStandardDescriptorAccessor_);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if ((0x4 & this.bitField0_) == 4)
          {
            this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
            this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          }
          DescriptorProtos.MessageOptions.access$12602(localMessageOptions, this.uninterpretedOption_);
        }
        while (true)
        {
          DescriptorProtos.MessageOptions.access$12702(localMessageOptions, k);
          onBuilt();
          return localMessageOptions;
          DescriptorProtos.MessageOptions.access$12602(localMessageOptions, this.uninterpretedOptionBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.messageSetWireFormat_ = false;
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.noStandardDescriptorAccessor_ = false;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clearMessageSetWireFormat()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.messageSetWireFormat_ = false;
        onChanged();
        return this;
      }

      public Builder clearNoStandardDescriptorAccessor()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.noStandardDescriptorAccessor_ = false;
        onChanged();
        return this;
      }

      public Builder clearUninterpretedOption()
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.MessageOptions getDefaultInstanceForType()
      {
        return DescriptorProtos.MessageOptions.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.MessageOptions.getDescriptor();
      }

      public boolean getMessageSetWireFormat()
      {
        return this.messageSetWireFormat_;
      }

      public boolean getNoStandardDescriptorAccessor()
      {
        return this.noStandardDescriptorAccessor_;
      }

      public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption)this.uninterpretedOptionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.UninterpretedOption.Builder getUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.UninterpretedOption.Builder> getUninterpretedOptionBuilderList()
      {
        return getUninterpretedOptionFieldBuilder().getBuilderList();
      }

      public int getUninterpretedOptionCount()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return this.uninterpretedOption_.size();
        return this.uninterpretedOptionBuilder_.getCount();
      }

      public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return Collections.unmodifiableList(this.uninterpretedOption_);
        return this.uninterpretedOptionBuilder_.getMessageList();
      }

      public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOptionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
      {
        if (this.uninterpretedOptionBuilder_ != null)
          return this.uninterpretedOptionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.uninterpretedOption_);
      }

      public boolean hasMessageSetWireFormat()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasNoStandardDescriptorAccessor()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_MessageOptions_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getUninterpretedOptionCount(); i++)
          if (!getUninterpretedOption(i).isInitialized())
            return false;
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 8:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.messageSetWireFormat_ = paramCodedInputStream.readBool();
            break;
          case 16:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.noStandardDescriptorAccessor_ = paramCodedInputStream.readBool();
            break;
          case 7994:
            DescriptorProtos.UninterpretedOption.Builder localBuilder1 = DescriptorProtos.UninterpretedOption.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addUninterpretedOption(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.MessageOptions paramMessageOptions)
      {
        if (paramMessageOptions == DescriptorProtos.MessageOptions.getDefaultInstance())
          return this;
        if (paramMessageOptions.hasMessageSetWireFormat())
          setMessageSetWireFormat(paramMessageOptions.getMessageSetWireFormat());
        if (paramMessageOptions.hasNoStandardDescriptorAccessor())
          setNoStandardDescriptorAccessor(paramMessageOptions.getNoStandardDescriptorAccessor());
        if (this.uninterpretedOptionBuilder_ == null)
          if (!paramMessageOptions.uninterpretedOption_.isEmpty())
          {
            if (!this.uninterpretedOption_.isEmpty())
              break label111;
            this.uninterpretedOption_ = paramMessageOptions.uninterpretedOption_;
            this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeExtensionFields(paramMessageOptions);
          mergeUnknownFields(paramMessageOptions.getUnknownFields());
          return this;
          label111: ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.addAll(paramMessageOptions.uninterpretedOption_);
          break;
          if (!paramMessageOptions.uninterpretedOption_.isEmpty())
          {
            if (this.uninterpretedOptionBuilder_.isEmpty())
            {
              this.uninterpretedOptionBuilder_.dispose();
              this.uninterpretedOptionBuilder_ = null;
              this.uninterpretedOption_ = paramMessageOptions.uninterpretedOption_;
              this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getUninterpretedOptionFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.uninterpretedOptionBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.uninterpretedOptionBuilder_.addAllMessages(paramMessageOptions.uninterpretedOption_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.MessageOptions))
          return mergeFrom((DescriptorProtos.MessageOptions)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.remove(paramInt);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.remove(paramInt);
        return this;
      }

      public Builder setMessageSetWireFormat(boolean paramBoolean)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.messageSetWireFormat_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setNoStandardDescriptorAccessor(boolean paramBoolean)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.noStandardDescriptorAccessor_ = paramBoolean;
        onChanged();
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramUninterpretedOption);
        return this;
      }
    }
  }

  public static abstract interface MessageOptionsOrBuilder extends GeneratedMessage.ExtendableMessageOrBuilder<DescriptorProtos.MessageOptions>
  {
    public abstract boolean getMessageSetWireFormat();

    public abstract boolean getNoStandardDescriptorAccessor();

    public abstract DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt);

    public abstract int getUninterpretedOptionCount();

    public abstract List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList();

    public abstract DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList();

    public abstract boolean hasMessageSetWireFormat();

    public abstract boolean hasNoStandardDescriptorAccessor();
  }

  public static final class MethodDescriptorProto extends GeneratedMessage
    implements DescriptorProtos.MethodDescriptorProtoOrBuilder
  {
    public static final int INPUT_TYPE_FIELD_NUMBER = 2;
    public static final int NAME_FIELD_NUMBER = 1;
    public static final int OPTIONS_FIELD_NUMBER = 4;
    public static final int OUTPUT_TYPE_FIELD_NUMBER = 3;
    private static final MethodDescriptorProto defaultInstance = new MethodDescriptorProto(true);
    private int bitField0_;
    private Object inputType_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object name_;
    private DescriptorProtos.MethodOptions options_;
    private Object outputType_;

    static
    {
      defaultInstance.initFields();
    }

    private MethodDescriptorProto(Builder paramBuilder)
    {
      super();
    }

    private MethodDescriptorProto(boolean paramBoolean)
    {
    }

    public static MethodDescriptorProto getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_MethodDescriptorProto_descriptor;
    }

    private ByteString getInputTypeBytes()
    {
      Object localObject = this.inputType_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.inputType_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getOutputTypeBytes()
    {
      Object localObject = this.outputType_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.outputType_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.name_ = "";
      this.inputType_ = "";
      this.outputType_ = "";
      this.options_ = DescriptorProtos.MethodOptions.getDefaultInstance();
    }

    public static Builder newBuilder()
    {
      return Builder.access$9400();
    }

    public static Builder newBuilder(MethodDescriptorProto paramMethodDescriptorProto)
    {
      return newBuilder().mergeFrom(paramMethodDescriptorProto);
    }

    public static MethodDescriptorProto parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static MethodDescriptorProto parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static MethodDescriptorProto parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static MethodDescriptorProto parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static MethodDescriptorProto parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static MethodDescriptorProto parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static MethodDescriptorProto parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static MethodDescriptorProto parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static MethodDescriptorProto parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static MethodDescriptorProto parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public MethodDescriptorProto getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getInputType()
    {
      Object localObject = this.inputType_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.inputType_ = str;
      return str;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.name_ = str;
      return str;
    }

    public DescriptorProtos.MethodOptions getOptions()
    {
      return this.options_;
    }

    public DescriptorProtos.MethodOptionsOrBuilder getOptionsOrBuilder()
    {
      return this.options_;
    }

    public String getOutputType()
    {
      Object localObject = this.outputType_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.outputType_ = str;
      return str;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBytesSize(2, getInputTypeBytes());
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBytesSize(3, getOutputTypeBytes());
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeMessageSize(4, this.options_);
      int m = k + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public boolean hasInputType()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasOptions()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasOutputType()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_MethodDescriptorProto_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      if ((hasOptions()) && (!getOptions().isInitialized()))
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBytes(2, getInputTypeBytes());
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBytes(3, getOutputTypeBytes());
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeMessage(4, this.options_);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.MethodDescriptorProtoOrBuilder
    {
      private int bitField0_;
      private Object inputType_ = "";
      private Object name_ = "";
      private SingleFieldBuilder<DescriptorProtos.MethodOptions, DescriptorProtos.MethodOptions.Builder, DescriptorProtos.MethodOptionsOrBuilder> optionsBuilder_;
      private DescriptorProtos.MethodOptions options_ = DescriptorProtos.MethodOptions.getDefaultInstance();
      private Object outputType_ = "";

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.MethodDescriptorProto buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.MethodDescriptorProto localMethodDescriptorProto = buildPartial();
        if (!localMethodDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localMethodDescriptorProto).asInvalidProtocolBufferException();
        return localMethodDescriptorProto;
      }

      private static Builder create()
      {
        return new Builder();
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_MethodDescriptorProto_descriptor;
      }

      private SingleFieldBuilder<DescriptorProtos.MethodOptions, DescriptorProtos.MethodOptions.Builder, DescriptorProtos.MethodOptionsOrBuilder> getOptionsFieldBuilder()
      {
        if (this.optionsBuilder_ == null)
        {
          this.optionsBuilder_ = new SingleFieldBuilder(this.options_, getParentForChildren(), isClean());
          this.options_ = null;
        }
        return this.optionsBuilder_;
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getOptionsFieldBuilder();
      }

      public DescriptorProtos.MethodDescriptorProto build()
      {
        DescriptorProtos.MethodDescriptorProto localMethodDescriptorProto = buildPartial();
        if (!localMethodDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localMethodDescriptorProto);
        return localMethodDescriptorProto;
      }

      public DescriptorProtos.MethodDescriptorProto buildPartial()
      {
        DescriptorProtos.MethodDescriptorProto localMethodDescriptorProto = new DescriptorProtos.MethodDescriptorProto(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.MethodDescriptorProto.access$9702(localMethodDescriptorProto, this.name_);
        if ((i & 0x2) == 2)
          k |= 2;
        DescriptorProtos.MethodDescriptorProto.access$9802(localMethodDescriptorProto, this.inputType_);
        if ((i & 0x4) == 4)
          k |= 4;
        DescriptorProtos.MethodDescriptorProto.access$9902(localMethodDescriptorProto, this.outputType_);
        if ((i & 0x8) == 8)
          k |= 8;
        if (this.optionsBuilder_ == null)
          DescriptorProtos.MethodDescriptorProto.access$10002(localMethodDescriptorProto, this.options_);
        while (true)
        {
          DescriptorProtos.MethodDescriptorProto.access$10102(localMethodDescriptorProto, k);
          onBuilt();
          return localMethodDescriptorProto;
          DescriptorProtos.MethodDescriptorProto.access$10002(localMethodDescriptorProto, (DescriptorProtos.MethodOptions)this.optionsBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.inputType_ = "";
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.outputType_ = "";
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        if (this.optionsBuilder_ == null)
          this.options_ = DescriptorProtos.MethodOptions.getDefaultInstance();
        while (true)
        {
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clearInputType()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.inputType_ = DescriptorProtos.MethodDescriptorProto.getDefaultInstance().getInputType();
        onChanged();
        return this;
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = DescriptorProtos.MethodDescriptorProto.getDefaultInstance().getName();
        onChanged();
        return this;
      }

      public Builder clearOptions()
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = DescriptorProtos.MethodOptions.getDefaultInstance();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clearOutputType()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.outputType_ = DescriptorProtos.MethodDescriptorProto.getDefaultInstance().getOutputType();
        onChanged();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.MethodDescriptorProto getDefaultInstanceForType()
      {
        return DescriptorProtos.MethodDescriptorProto.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.MethodDescriptorProto.getDescriptor();
      }

      public String getInputType()
      {
        Object localObject = this.inputType_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.inputType_ = str;
          return str;
        }
        return (String)localObject;
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.MethodOptions getOptions()
      {
        if (this.optionsBuilder_ == null)
          return this.options_;
        return (DescriptorProtos.MethodOptions)this.optionsBuilder_.getMessage();
      }

      public DescriptorProtos.MethodOptions.Builder getOptionsBuilder()
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        onChanged();
        return (DescriptorProtos.MethodOptions.Builder)getOptionsFieldBuilder().getBuilder();
      }

      public DescriptorProtos.MethodOptionsOrBuilder getOptionsOrBuilder()
      {
        if (this.optionsBuilder_ != null)
          return (DescriptorProtos.MethodOptionsOrBuilder)this.optionsBuilder_.getMessageOrBuilder();
        return this.options_;
      }

      public String getOutputType()
      {
        Object localObject = this.outputType_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.outputType_ = str;
          return str;
        }
        return (String)localObject;
      }

      public boolean hasInputType()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasOptions()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasOutputType()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_MethodDescriptorProto_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        return (!hasOptions()) || (getOptions().isInitialized());
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.inputType_ = paramCodedInputStream.readBytes();
            break;
          case 26:
            this.bitField0_ = (0x4 | this.bitField0_);
            this.outputType_ = paramCodedInputStream.readBytes();
            break;
          case 34:
            DescriptorProtos.MethodOptions.Builder localBuilder1 = DescriptorProtos.MethodOptions.newBuilder();
            if (hasOptions())
              localBuilder1.mergeFrom(getOptions());
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            setOptions(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.MethodDescriptorProto paramMethodDescriptorProto)
      {
        if (paramMethodDescriptorProto == DescriptorProtos.MethodDescriptorProto.getDefaultInstance())
          return this;
        if (paramMethodDescriptorProto.hasName())
          setName(paramMethodDescriptorProto.getName());
        if (paramMethodDescriptorProto.hasInputType())
          setInputType(paramMethodDescriptorProto.getInputType());
        if (paramMethodDescriptorProto.hasOutputType())
          setOutputType(paramMethodDescriptorProto.getOutputType());
        if (paramMethodDescriptorProto.hasOptions())
          mergeOptions(paramMethodDescriptorProto.getOptions());
        mergeUnknownFields(paramMethodDescriptorProto.getUnknownFields());
        return this;
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.MethodDescriptorProto))
          return mergeFrom((DescriptorProtos.MethodDescriptorProto)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder mergeOptions(DescriptorProtos.MethodOptions paramMethodOptions)
      {
        if (this.optionsBuilder_ == null)
          if (((0x8 & this.bitField0_) == 8) && (this.options_ != DescriptorProtos.MethodOptions.getDefaultInstance()))
          {
            this.options_ = DescriptorProtos.MethodOptions.newBuilder(this.options_).mergeFrom(paramMethodOptions).buildPartial();
            onChanged();
          }
        while (true)
        {
          this.bitField0_ = (0x8 | this.bitField0_);
          return this;
          this.options_ = paramMethodOptions;
          break;
          this.optionsBuilder_.mergeFrom(paramMethodOptions);
        }
      }

      public Builder setInputType(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.inputType_ = paramString;
        onChanged();
        return this;
      }

      void setInputType(ByteString paramByteString)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.inputType_ = paramByteString;
        onChanged();
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        onChanged();
        return this;
      }

      void setName(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        onChanged();
      }

      public Builder setOptions(DescriptorProtos.MethodOptions.Builder paramBuilder)
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = paramBuilder.build();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x8 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramBuilder.build());
        }
      }

      public Builder setOptions(DescriptorProtos.MethodOptions paramMethodOptions)
      {
        if (this.optionsBuilder_ == null)
        {
          if (paramMethodOptions == null)
            throw new NullPointerException();
          this.options_ = paramMethodOptions;
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x8 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramMethodOptions);
        }
      }

      public Builder setOutputType(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x4 | this.bitField0_);
        this.outputType_ = paramString;
        onChanged();
        return this;
      }

      void setOutputType(ByteString paramByteString)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.outputType_ = paramByteString;
        onChanged();
      }
    }
  }

  public static abstract interface MethodDescriptorProtoOrBuilder extends MessageOrBuilder
  {
    public abstract String getInputType();

    public abstract String getName();

    public abstract DescriptorProtos.MethodOptions getOptions();

    public abstract DescriptorProtos.MethodOptionsOrBuilder getOptionsOrBuilder();

    public abstract String getOutputType();

    public abstract boolean hasInputType();

    public abstract boolean hasName();

    public abstract boolean hasOptions();

    public abstract boolean hasOutputType();
  }

  public static final class MethodOptions extends GeneratedMessage.ExtendableMessage<MethodOptions>
    implements DescriptorProtos.MethodOptionsOrBuilder
  {
    public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
    private static final MethodOptions defaultInstance = new MethodOptions(true);
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_;

    static
    {
      defaultInstance.initFields();
    }

    private MethodOptions(Builder paramBuilder)
    {
      super();
    }

    private MethodOptions(boolean paramBoolean)
    {
    }

    public static MethodOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_MethodOptions_descriptor;
    }

    private void initFields()
    {
      this.uninterpretedOption_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$16400();
    }

    public static Builder newBuilder(MethodOptions paramMethodOptions)
    {
      return newBuilder().mergeFrom(paramMethodOptions);
    }

    public static MethodOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static MethodOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static MethodOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static MethodOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static MethodOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static MethodOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static MethodOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static MethodOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static MethodOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static MethodOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public MethodOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.uninterpretedOption_.size(); k++)
        j += CodedOutputStream.computeMessageSize(999, (MessageLite)this.uninterpretedOption_.get(k));
      int m = j + extensionsSerializedSize() + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
    }

    public int getUninterpretedOptionCount()
    {
      return this.uninterpretedOption_.size();
    }

    public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
    {
      return this.uninterpretedOption_;
    }

    public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
    }

    public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
    {
      return this.uninterpretedOption_;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_MethodOptions_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getUninterpretedOptionCount(); j++)
        if (!getUninterpretedOption(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessage.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      for (int i = 0; i < this.uninterpretedOption_.size(); i++)
        paramCodedOutputStream.writeMessage(999, (MessageLite)this.uninterpretedOption_.get(i));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.ExtendableBuilder<DescriptorProtos.MethodOptions, Builder>
      implements DescriptorProtos.MethodOptionsOrBuilder
    {
      private int bitField0_;
      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> uninterpretedOptionBuilder_;
      private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.MethodOptions buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.MethodOptions localMethodOptions = buildPartial();
        if (!localMethodOptions.isInitialized())
          throw newUninitializedMessageException(localMethodOptions).asInvalidProtocolBufferException();
        return localMethodOptions;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureUninterpretedOptionIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.uninterpretedOption_ = new ArrayList(this.uninterpretedOption_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_MethodOptions_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionFieldBuilder()
      {
        List localList;
        if (this.uninterpretedOptionBuilder_ == null)
        {
          localList = this.uninterpretedOption_;
          if ((0x1 & this.bitField0_) != 1)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.uninterpretedOptionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.uninterpretedOption_ = null;
          return this.uninterpretedOptionBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getUninterpretedOptionFieldBuilder();
      }

      public Builder addAllUninterpretedOption(Iterable<? extends DescriptorProtos.UninterpretedOption> paramIterable)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          GeneratedMessage.ExtendableBuilder.addAll(paramIterable, this.uninterpretedOption_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramUninterpretedOption);
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramUninterpretedOption);
        return this;
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder()
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(paramInt, DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.MethodOptions build()
      {
        DescriptorProtos.MethodOptions localMethodOptions = buildPartial();
        if (!localMethodOptions.isInitialized())
          throw newUninitializedMessageException(localMethodOptions);
        return localMethodOptions;
      }

      public DescriptorProtos.MethodOptions buildPartial()
      {
        DescriptorProtos.MethodOptions localMethodOptions = new DescriptorProtos.MethodOptions(this, null);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if ((0x1 & this.bitField0_) == 1)
          {
            this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
          DescriptorProtos.MethodOptions.access$16702(localMethodOptions, this.uninterpretedOption_);
        }
        while (true)
        {
          onBuilt();
          return localMethodOptions;
          DescriptorProtos.MethodOptions.access$16702(localMethodOptions, this.uninterpretedOptionBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clearUninterpretedOption()
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.MethodOptions getDefaultInstanceForType()
      {
        return DescriptorProtos.MethodOptions.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.MethodOptions.getDescriptor();
      }

      public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption)this.uninterpretedOptionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.UninterpretedOption.Builder getUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.UninterpretedOption.Builder> getUninterpretedOptionBuilderList()
      {
        return getUninterpretedOptionFieldBuilder().getBuilderList();
      }

      public int getUninterpretedOptionCount()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return this.uninterpretedOption_.size();
        return this.uninterpretedOptionBuilder_.getCount();
      }

      public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return Collections.unmodifiableList(this.uninterpretedOption_);
        return this.uninterpretedOptionBuilder_.getMessageList();
      }

      public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOptionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
      {
        if (this.uninterpretedOptionBuilder_ != null)
          return this.uninterpretedOptionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.uninterpretedOption_);
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_MethodOptions_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getUninterpretedOptionCount(); i++)
          if (!getUninterpretedOption(i).isInitialized())
            return false;
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 7994:
            DescriptorProtos.UninterpretedOption.Builder localBuilder1 = DescriptorProtos.UninterpretedOption.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addUninterpretedOption(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.MethodOptions paramMethodOptions)
      {
        if (paramMethodOptions == DescriptorProtos.MethodOptions.getDefaultInstance())
          return this;
        if (this.uninterpretedOptionBuilder_ == null)
          if (!paramMethodOptions.uninterpretedOption_.isEmpty())
          {
            if (!this.uninterpretedOption_.isEmpty())
              break label79;
            this.uninterpretedOption_ = paramMethodOptions.uninterpretedOption_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeExtensionFields(paramMethodOptions);
          mergeUnknownFields(paramMethodOptions.getUnknownFields());
          return this;
          label79: ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.addAll(paramMethodOptions.uninterpretedOption_);
          break;
          if (!paramMethodOptions.uninterpretedOption_.isEmpty())
          {
            if (this.uninterpretedOptionBuilder_.isEmpty())
            {
              this.uninterpretedOptionBuilder_.dispose();
              this.uninterpretedOptionBuilder_ = null;
              this.uninterpretedOption_ = paramMethodOptions.uninterpretedOption_;
              this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getUninterpretedOptionFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.uninterpretedOptionBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.uninterpretedOptionBuilder_.addAllMessages(paramMethodOptions.uninterpretedOption_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.MethodOptions))
          return mergeFrom((DescriptorProtos.MethodOptions)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.remove(paramInt);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.remove(paramInt);
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramUninterpretedOption);
        return this;
      }
    }
  }

  public static abstract interface MethodOptionsOrBuilder extends GeneratedMessage.ExtendableMessageOrBuilder<DescriptorProtos.MethodOptions>
  {
    public abstract DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt);

    public abstract int getUninterpretedOptionCount();

    public abstract List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList();

    public abstract DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList();
  }

  public static final class ServiceDescriptorProto extends GeneratedMessage
    implements DescriptorProtos.ServiceDescriptorProtoOrBuilder
  {
    public static final int METHOD_FIELD_NUMBER = 2;
    public static final int NAME_FIELD_NUMBER = 1;
    public static final int OPTIONS_FIELD_NUMBER = 3;
    private static final ServiceDescriptorProto defaultInstance = new ServiceDescriptorProto(true);
    private int bitField0_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<DescriptorProtos.MethodDescriptorProto> method_;
    private Object name_;
    private DescriptorProtos.ServiceOptions options_;

    static
    {
      defaultInstance.initFields();
    }

    private ServiceDescriptorProto(Builder paramBuilder)
    {
      super();
    }

    private ServiceDescriptorProto(boolean paramBoolean)
    {
    }

    public static ServiceDescriptorProto getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_ServiceDescriptorProto_descriptor;
    }

    private ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.name_ = "";
      this.method_ = Collections.emptyList();
      this.options_ = DescriptorProtos.ServiceOptions.getDefaultInstance();
    }

    public static Builder newBuilder()
    {
      return Builder.access$8400();
    }

    public static Builder newBuilder(ServiceDescriptorProto paramServiceDescriptorProto)
    {
      return newBuilder().mergeFrom(paramServiceDescriptorProto);
    }

    public static ServiceDescriptorProto parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static ServiceDescriptorProto parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static ServiceDescriptorProto parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static ServiceDescriptorProto parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static ServiceDescriptorProto parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static ServiceDescriptorProto parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static ServiceDescriptorProto parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static ServiceDescriptorProto parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static ServiceDescriptorProto parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static ServiceDescriptorProto parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public ServiceDescriptorProto getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public DescriptorProtos.MethodDescriptorProto getMethod(int paramInt)
    {
      return (DescriptorProtos.MethodDescriptorProto)this.method_.get(paramInt);
    }

    public int getMethodCount()
    {
      return this.method_.size();
    }

    public List<DescriptorProtos.MethodDescriptorProto> getMethodList()
    {
      return this.method_;
    }

    public DescriptorProtos.MethodDescriptorProtoOrBuilder getMethodOrBuilder(int paramInt)
    {
      return (DescriptorProtos.MethodDescriptorProtoOrBuilder)this.method_.get(paramInt);
    }

    public List<? extends DescriptorProtos.MethodDescriptorProtoOrBuilder> getMethodOrBuilderList()
    {
      return this.method_;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.name_ = str;
      return str;
    }

    public DescriptorProtos.ServiceOptions getOptions()
    {
      return this.options_;
    }

    public DescriptorProtos.ServiceOptionsOrBuilder getOptionsOrBuilder()
    {
      return this.options_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      for (int m = 0; m < this.method_.size(); m++)
        k += CodedOutputStream.computeMessageSize(2, (MessageLite)this.method_.get(m));
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeMessageSize(3, this.options_);
      int n = k + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = n;
      return n;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasOptions()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_ServiceDescriptorProto_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getMethodCount(); j++)
        if (!getMethod(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if ((hasOptions()) && (!getOptions().isInitialized()))
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      for (int i = 0; i < this.method_.size(); i++)
        paramCodedOutputStream.writeMessage(2, (MessageLite)this.method_.get(i));
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeMessage(3, this.options_);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.ServiceDescriptorProtoOrBuilder
    {
      private int bitField0_;
      private RepeatedFieldBuilder<DescriptorProtos.MethodDescriptorProto, DescriptorProtos.MethodDescriptorProto.Builder, DescriptorProtos.MethodDescriptorProtoOrBuilder> methodBuilder_;
      private List<DescriptorProtos.MethodDescriptorProto> method_ = Collections.emptyList();
      private Object name_ = "";
      private SingleFieldBuilder<DescriptorProtos.ServiceOptions, DescriptorProtos.ServiceOptions.Builder, DescriptorProtos.ServiceOptionsOrBuilder> optionsBuilder_;
      private DescriptorProtos.ServiceOptions options_ = DescriptorProtos.ServiceOptions.getDefaultInstance();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.ServiceDescriptorProto buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.ServiceDescriptorProto localServiceDescriptorProto = buildPartial();
        if (!localServiceDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localServiceDescriptorProto).asInvalidProtocolBufferException();
        return localServiceDescriptorProto;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureMethodIsMutable()
      {
        if ((0x2 & this.bitField0_) != 2)
        {
          this.method_ = new ArrayList(this.method_);
          this.bitField0_ = (0x2 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_ServiceDescriptorProto_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.MethodDescriptorProto, DescriptorProtos.MethodDescriptorProto.Builder, DescriptorProtos.MethodDescriptorProtoOrBuilder> getMethodFieldBuilder()
      {
        List localList;
        if (this.methodBuilder_ == null)
        {
          localList = this.method_;
          if ((0x2 & this.bitField0_) != 2)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.methodBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.method_ = null;
          return this.methodBuilder_;
        }
      }

      private SingleFieldBuilder<DescriptorProtos.ServiceOptions, DescriptorProtos.ServiceOptions.Builder, DescriptorProtos.ServiceOptionsOrBuilder> getOptionsFieldBuilder()
      {
        if (this.optionsBuilder_ == null)
        {
          this.optionsBuilder_ = new SingleFieldBuilder(this.options_, getParentForChildren(), isClean());
          this.options_ = null;
        }
        return this.optionsBuilder_;
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
        {
          getMethodFieldBuilder();
          getOptionsFieldBuilder();
        }
      }

      public Builder addAllMethod(Iterable<? extends DescriptorProtos.MethodDescriptorProto> paramIterable)
      {
        if (this.methodBuilder_ == null)
        {
          ensureMethodIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.method_);
          onChanged();
          return this;
        }
        this.methodBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addMethod(int paramInt, DescriptorProtos.MethodDescriptorProto.Builder paramBuilder)
      {
        if (this.methodBuilder_ == null)
        {
          ensureMethodIsMutable();
          this.method_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.methodBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addMethod(int paramInt, DescriptorProtos.MethodDescriptorProto paramMethodDescriptorProto)
      {
        if (this.methodBuilder_ == null)
        {
          if (paramMethodDescriptorProto == null)
            throw new NullPointerException();
          ensureMethodIsMutable();
          this.method_.add(paramInt, paramMethodDescriptorProto);
          onChanged();
          return this;
        }
        this.methodBuilder_.addMessage(paramInt, paramMethodDescriptorProto);
        return this;
      }

      public Builder addMethod(DescriptorProtos.MethodDescriptorProto.Builder paramBuilder)
      {
        if (this.methodBuilder_ == null)
        {
          ensureMethodIsMutable();
          this.method_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.methodBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addMethod(DescriptorProtos.MethodDescriptorProto paramMethodDescriptorProto)
      {
        if (this.methodBuilder_ == null)
        {
          if (paramMethodDescriptorProto == null)
            throw new NullPointerException();
          ensureMethodIsMutable();
          this.method_.add(paramMethodDescriptorProto);
          onChanged();
          return this;
        }
        this.methodBuilder_.addMessage(paramMethodDescriptorProto);
        return this;
      }

      public DescriptorProtos.MethodDescriptorProto.Builder addMethodBuilder()
      {
        return (DescriptorProtos.MethodDescriptorProto.Builder)getMethodFieldBuilder().addBuilder(DescriptorProtos.MethodDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.MethodDescriptorProto.Builder addMethodBuilder(int paramInt)
      {
        return (DescriptorProtos.MethodDescriptorProto.Builder)getMethodFieldBuilder().addBuilder(paramInt, DescriptorProtos.MethodDescriptorProto.getDefaultInstance());
      }

      public DescriptorProtos.ServiceDescriptorProto build()
      {
        DescriptorProtos.ServiceDescriptorProto localServiceDescriptorProto = buildPartial();
        if (!localServiceDescriptorProto.isInitialized())
          throw newUninitializedMessageException(localServiceDescriptorProto);
        return localServiceDescriptorProto;
      }

      public DescriptorProtos.ServiceDescriptorProto buildPartial()
      {
        DescriptorProtos.ServiceDescriptorProto localServiceDescriptorProto = new DescriptorProtos.ServiceDescriptorProto(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        DescriptorProtos.ServiceDescriptorProto.access$8702(localServiceDescriptorProto, this.name_);
        if (this.methodBuilder_ == null)
        {
          if ((0x2 & this.bitField0_) == 2)
          {
            this.method_ = Collections.unmodifiableList(this.method_);
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          }
          DescriptorProtos.ServiceDescriptorProto.access$8802(localServiceDescriptorProto, this.method_);
          if ((i & 0x4) == 4)
            k |= 2;
          if (this.optionsBuilder_ != null)
            break label146;
          DescriptorProtos.ServiceDescriptorProto.access$8902(localServiceDescriptorProto, this.options_);
        }
        while (true)
        {
          DescriptorProtos.ServiceDescriptorProto.access$9002(localServiceDescriptorProto, k);
          onBuilt();
          return localServiceDescriptorProto;
          DescriptorProtos.ServiceDescriptorProto.access$8802(localServiceDescriptorProto, this.methodBuilder_.build());
          break;
          label146: DescriptorProtos.ServiceDescriptorProto.access$8902(localServiceDescriptorProto, (DescriptorProtos.ServiceOptions)this.optionsBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        if (this.methodBuilder_ == null)
        {
          this.method_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          if (this.optionsBuilder_ != null)
            break label84;
          this.options_ = DescriptorProtos.ServiceOptions.getDefaultInstance();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          return this;
          this.methodBuilder_.clear();
          break;
          label84: this.optionsBuilder_.clear();
        }
      }

      public Builder clearMethod()
      {
        if (this.methodBuilder_ == null)
        {
          this.method_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          onChanged();
          return this;
        }
        this.methodBuilder_.clear();
        return this;
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = DescriptorProtos.ServiceDescriptorProto.getDefaultInstance().getName();
        onChanged();
        return this;
      }

      public Builder clearOptions()
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = DescriptorProtos.ServiceOptions.getDefaultInstance();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          return this;
          this.optionsBuilder_.clear();
        }
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.ServiceDescriptorProto getDefaultInstanceForType()
      {
        return DescriptorProtos.ServiceDescriptorProto.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.ServiceDescriptorProto.getDescriptor();
      }

      public DescriptorProtos.MethodDescriptorProto getMethod(int paramInt)
      {
        if (this.methodBuilder_ == null)
          return (DescriptorProtos.MethodDescriptorProto)this.method_.get(paramInt);
        return (DescriptorProtos.MethodDescriptorProto)this.methodBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.MethodDescriptorProto.Builder getMethodBuilder(int paramInt)
      {
        return (DescriptorProtos.MethodDescriptorProto.Builder)getMethodFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.MethodDescriptorProto.Builder> getMethodBuilderList()
      {
        return getMethodFieldBuilder().getBuilderList();
      }

      public int getMethodCount()
      {
        if (this.methodBuilder_ == null)
          return this.method_.size();
        return this.methodBuilder_.getCount();
      }

      public List<DescriptorProtos.MethodDescriptorProto> getMethodList()
      {
        if (this.methodBuilder_ == null)
          return Collections.unmodifiableList(this.method_);
        return this.methodBuilder_.getMessageList();
      }

      public DescriptorProtos.MethodDescriptorProtoOrBuilder getMethodOrBuilder(int paramInt)
      {
        if (this.methodBuilder_ == null)
          return (DescriptorProtos.MethodDescriptorProtoOrBuilder)this.method_.get(paramInt);
        return (DescriptorProtos.MethodDescriptorProtoOrBuilder)this.methodBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.MethodDescriptorProtoOrBuilder> getMethodOrBuilderList()
      {
        if (this.methodBuilder_ != null)
          return this.methodBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.method_);
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.ServiceOptions getOptions()
      {
        if (this.optionsBuilder_ == null)
          return this.options_;
        return (DescriptorProtos.ServiceOptions)this.optionsBuilder_.getMessage();
      }

      public DescriptorProtos.ServiceOptions.Builder getOptionsBuilder()
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        onChanged();
        return (DescriptorProtos.ServiceOptions.Builder)getOptionsFieldBuilder().getBuilder();
      }

      public DescriptorProtos.ServiceOptionsOrBuilder getOptionsOrBuilder()
      {
        if (this.optionsBuilder_ != null)
          return (DescriptorProtos.ServiceOptionsOrBuilder)this.optionsBuilder_.getMessageOrBuilder();
        return this.options_;
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasOptions()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_ServiceDescriptorProto_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getMethodCount(); i++)
          if (!getMethod(i).isInitialized())
            return false;
        return (!hasOptions()) || (getOptions().isInitialized());
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            DescriptorProtos.MethodDescriptorProto.Builder localBuilder2 = DescriptorProtos.MethodDescriptorProto.newBuilder();
            paramCodedInputStream.readMessage(localBuilder2, paramExtensionRegistryLite);
            addMethod(localBuilder2.buildPartial());
            break;
          case 26:
            DescriptorProtos.ServiceOptions.Builder localBuilder1 = DescriptorProtos.ServiceOptions.newBuilder();
            if (hasOptions())
              localBuilder1.mergeFrom(getOptions());
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            setOptions(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.ServiceDescriptorProto paramServiceDescriptorProto)
      {
        if (paramServiceDescriptorProto == DescriptorProtos.ServiceDescriptorProto.getDefaultInstance())
          return this;
        if (paramServiceDescriptorProto.hasName())
          setName(paramServiceDescriptorProto.getName());
        if (this.methodBuilder_ == null)
          if (!paramServiceDescriptorProto.method_.isEmpty())
          {
            if (!this.method_.isEmpty())
              break label106;
            this.method_ = paramServiceDescriptorProto.method_;
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          if (paramServiceDescriptorProto.hasOptions())
            mergeOptions(paramServiceDescriptorProto.getOptions());
          mergeUnknownFields(paramServiceDescriptorProto.getUnknownFields());
          return this;
          label106: ensureMethodIsMutable();
          this.method_.addAll(paramServiceDescriptorProto.method_);
          break;
          if (!paramServiceDescriptorProto.method_.isEmpty())
          {
            if (this.methodBuilder_.isEmpty())
            {
              this.methodBuilder_.dispose();
              this.methodBuilder_ = null;
              this.method_ = paramServiceDescriptorProto.method_;
              this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getMethodFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.methodBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.methodBuilder_.addAllMessages(paramServiceDescriptorProto.method_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.ServiceDescriptorProto))
          return mergeFrom((DescriptorProtos.ServiceDescriptorProto)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder mergeOptions(DescriptorProtos.ServiceOptions paramServiceOptions)
      {
        if (this.optionsBuilder_ == null)
          if (((0x4 & this.bitField0_) == 4) && (this.options_ != DescriptorProtos.ServiceOptions.getDefaultInstance()))
          {
            this.options_ = DescriptorProtos.ServiceOptions.newBuilder(this.options_).mergeFrom(paramServiceOptions).buildPartial();
            onChanged();
          }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.options_ = paramServiceOptions;
          break;
          this.optionsBuilder_.mergeFrom(paramServiceOptions);
        }
      }

      public Builder removeMethod(int paramInt)
      {
        if (this.methodBuilder_ == null)
        {
          ensureMethodIsMutable();
          this.method_.remove(paramInt);
          onChanged();
          return this;
        }
        this.methodBuilder_.remove(paramInt);
        return this;
      }

      public Builder setMethod(int paramInt, DescriptorProtos.MethodDescriptorProto.Builder paramBuilder)
      {
        if (this.methodBuilder_ == null)
        {
          ensureMethodIsMutable();
          this.method_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.methodBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setMethod(int paramInt, DescriptorProtos.MethodDescriptorProto paramMethodDescriptorProto)
      {
        if (this.methodBuilder_ == null)
        {
          if (paramMethodDescriptorProto == null)
            throw new NullPointerException();
          ensureMethodIsMutable();
          this.method_.set(paramInt, paramMethodDescriptorProto);
          onChanged();
          return this;
        }
        this.methodBuilder_.setMessage(paramInt, paramMethodDescriptorProto);
        return this;
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        onChanged();
        return this;
      }

      void setName(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        onChanged();
      }

      public Builder setOptions(DescriptorProtos.ServiceOptions.Builder paramBuilder)
      {
        if (this.optionsBuilder_ == null)
        {
          this.options_ = paramBuilder.build();
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramBuilder.build());
        }
      }

      public Builder setOptions(DescriptorProtos.ServiceOptions paramServiceOptions)
      {
        if (this.optionsBuilder_ == null)
        {
          if (paramServiceOptions == null)
            throw new NullPointerException();
          this.options_ = paramServiceOptions;
          onChanged();
        }
        while (true)
        {
          this.bitField0_ = (0x4 | this.bitField0_);
          return this;
          this.optionsBuilder_.setMessage(paramServiceOptions);
        }
      }
    }
  }

  public static abstract interface ServiceDescriptorProtoOrBuilder extends MessageOrBuilder
  {
    public abstract DescriptorProtos.MethodDescriptorProto getMethod(int paramInt);

    public abstract int getMethodCount();

    public abstract List<DescriptorProtos.MethodDescriptorProto> getMethodList();

    public abstract DescriptorProtos.MethodDescriptorProtoOrBuilder getMethodOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.MethodDescriptorProtoOrBuilder> getMethodOrBuilderList();

    public abstract String getName();

    public abstract DescriptorProtos.ServiceOptions getOptions();

    public abstract DescriptorProtos.ServiceOptionsOrBuilder getOptionsOrBuilder();

    public abstract boolean hasName();

    public abstract boolean hasOptions();
  }

  public static final class ServiceOptions extends GeneratedMessage.ExtendableMessage<ServiceOptions>
    implements DescriptorProtos.ServiceOptionsOrBuilder
  {
    public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
    private static final ServiceOptions defaultInstance = new ServiceOptions(true);
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_;

    static
    {
      defaultInstance.initFields();
    }

    private ServiceOptions(Builder paramBuilder)
    {
      super();
    }

    private ServiceOptions(boolean paramBoolean)
    {
    }

    public static ServiceOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_ServiceOptions_descriptor;
    }

    private void initFields()
    {
      this.uninterpretedOption_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$15700();
    }

    public static Builder newBuilder(ServiceOptions paramServiceOptions)
    {
      return newBuilder().mergeFrom(paramServiceOptions);
    }

    public static ServiceOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static ServiceOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static ServiceOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static ServiceOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static ServiceOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static ServiceOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static ServiceOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static ServiceOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static ServiceOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static ServiceOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public ServiceOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.uninterpretedOption_.size(); k++)
        j += CodedOutputStream.computeMessageSize(999, (MessageLite)this.uninterpretedOption_.get(k));
      int m = j + extensionsSerializedSize() + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
    }

    public int getUninterpretedOptionCount()
    {
      return this.uninterpretedOption_.size();
    }

    public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
    {
      return this.uninterpretedOption_;
    }

    public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
    {
      return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
    }

    public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
    {
      return this.uninterpretedOption_;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_ServiceOptions_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getUninterpretedOptionCount(); j++)
        if (!getUninterpretedOption(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessage.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      for (int i = 0; i < this.uninterpretedOption_.size(); i++)
        paramCodedOutputStream.writeMessage(999, (MessageLite)this.uninterpretedOption_.get(i));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.ExtendableBuilder<DescriptorProtos.ServiceOptions, Builder>
      implements DescriptorProtos.ServiceOptionsOrBuilder
    {
      private int bitField0_;
      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> uninterpretedOptionBuilder_;
      private List<DescriptorProtos.UninterpretedOption> uninterpretedOption_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.ServiceOptions buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.ServiceOptions localServiceOptions = buildPartial();
        if (!localServiceOptions.isInitialized())
          throw newUninitializedMessageException(localServiceOptions).asInvalidProtocolBufferException();
        return localServiceOptions;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureUninterpretedOptionIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.uninterpretedOption_ = new ArrayList(this.uninterpretedOption_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_ServiceOptions_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption, DescriptorProtos.UninterpretedOption.Builder, DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionFieldBuilder()
      {
        List localList;
        if (this.uninterpretedOptionBuilder_ == null)
        {
          localList = this.uninterpretedOption_;
          if ((0x1 & this.bitField0_) != 1)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.uninterpretedOptionBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.uninterpretedOption_ = null;
          return this.uninterpretedOptionBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getUninterpretedOptionFieldBuilder();
      }

      public Builder addAllUninterpretedOption(Iterable<? extends DescriptorProtos.UninterpretedOption> paramIterable)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          GeneratedMessage.ExtendableBuilder.addAll(paramIterable, this.uninterpretedOption_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramInt, paramUninterpretedOption);
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addUninterpretedOption(DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.add(paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.addMessage(paramUninterpretedOption);
        return this;
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder()
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption.Builder addUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().addBuilder(paramInt, DescriptorProtos.UninterpretedOption.getDefaultInstance());
      }

      public DescriptorProtos.ServiceOptions build()
      {
        DescriptorProtos.ServiceOptions localServiceOptions = buildPartial();
        if (!localServiceOptions.isInitialized())
          throw newUninitializedMessageException(localServiceOptions);
        return localServiceOptions;
      }

      public DescriptorProtos.ServiceOptions buildPartial()
      {
        DescriptorProtos.ServiceOptions localServiceOptions = new DescriptorProtos.ServiceOptions(this, null);
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if ((0x1 & this.bitField0_) == 1)
          {
            this.uninterpretedOption_ = Collections.unmodifiableList(this.uninterpretedOption_);
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
          DescriptorProtos.ServiceOptions.access$16002(localServiceOptions, this.uninterpretedOption_);
        }
        while (true)
        {
          onBuilt();
          return localServiceOptions;
          DescriptorProtos.ServiceOptions.access$16002(localServiceOptions, this.uninterpretedOptionBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clearUninterpretedOption()
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          this.uninterpretedOption_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.ServiceOptions getDefaultInstanceForType()
      {
        return DescriptorProtos.ServiceOptions.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.ServiceOptions.getDescriptor();
      }

      public DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption)this.uninterpretedOptionBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.UninterpretedOption.Builder getUninterpretedOptionBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.Builder)getUninterpretedOptionFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.UninterpretedOption.Builder> getUninterpretedOptionBuilderList()
      {
        return getUninterpretedOptionFieldBuilder().getBuilderList();
      }

      public int getUninterpretedOptionCount()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return this.uninterpretedOption_.size();
        return this.uninterpretedOptionBuilder_.getCount();
      }

      public List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList()
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return Collections.unmodifiableList(this.uninterpretedOption_);
        return this.uninterpretedOptionBuilder_.getMessageList();
      }

      public DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
          return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOption_.get(paramInt);
        return (DescriptorProtos.UninterpretedOptionOrBuilder)this.uninterpretedOptionBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList()
      {
        if (this.uninterpretedOptionBuilder_ != null)
          return this.uninterpretedOptionBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.uninterpretedOption_);
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_ServiceOptions_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getUninterpretedOptionCount(); i++)
          if (!getUninterpretedOption(i).isInitialized())
            return false;
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 7994:
            DescriptorProtos.UninterpretedOption.Builder localBuilder1 = DescriptorProtos.UninterpretedOption.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addUninterpretedOption(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.ServiceOptions paramServiceOptions)
      {
        if (paramServiceOptions == DescriptorProtos.ServiceOptions.getDefaultInstance())
          return this;
        if (this.uninterpretedOptionBuilder_ == null)
          if (!paramServiceOptions.uninterpretedOption_.isEmpty())
          {
            if (!this.uninterpretedOption_.isEmpty())
              break label79;
            this.uninterpretedOption_ = paramServiceOptions.uninterpretedOption_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeExtensionFields(paramServiceOptions);
          mergeUnknownFields(paramServiceOptions.getUnknownFields());
          return this;
          label79: ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.addAll(paramServiceOptions.uninterpretedOption_);
          break;
          if (!paramServiceOptions.uninterpretedOption_.isEmpty())
          {
            if (this.uninterpretedOptionBuilder_.isEmpty())
            {
              this.uninterpretedOptionBuilder_.dispose();
              this.uninterpretedOptionBuilder_ = null;
              this.uninterpretedOption_ = paramServiceOptions.uninterpretedOption_;
              this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getUninterpretedOptionFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.uninterpretedOptionBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.uninterpretedOptionBuilder_.addAllMessages(paramServiceOptions.uninterpretedOption_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.ServiceOptions))
          return mergeFrom((DescriptorProtos.ServiceOptions)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeUninterpretedOption(int paramInt)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.remove(paramInt);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.remove(paramInt);
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption.Builder paramBuilder)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setUninterpretedOption(int paramInt, DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (this.uninterpretedOptionBuilder_ == null)
        {
          if (paramUninterpretedOption == null)
            throw new NullPointerException();
          ensureUninterpretedOptionIsMutable();
          this.uninterpretedOption_.set(paramInt, paramUninterpretedOption);
          onChanged();
          return this;
        }
        this.uninterpretedOptionBuilder_.setMessage(paramInt, paramUninterpretedOption);
        return this;
      }
    }
  }

  public static abstract interface ServiceOptionsOrBuilder extends GeneratedMessage.ExtendableMessageOrBuilder<DescriptorProtos.ServiceOptions>
  {
    public abstract DescriptorProtos.UninterpretedOption getUninterpretedOption(int paramInt);

    public abstract int getUninterpretedOptionCount();

    public abstract List<DescriptorProtos.UninterpretedOption> getUninterpretedOptionList();

    public abstract DescriptorProtos.UninterpretedOptionOrBuilder getUninterpretedOptionOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.UninterpretedOptionOrBuilder> getUninterpretedOptionOrBuilderList();
  }

  public static final class SourceCodeInfo extends GeneratedMessage
    implements DescriptorProtos.SourceCodeInfoOrBuilder
  {
    public static final int LOCATION_FIELD_NUMBER = 1;
    private static final SourceCodeInfo defaultInstance = new SourceCodeInfo(true);
    private List<Location> location_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;

    static
    {
      defaultInstance.initFields();
    }

    private SourceCodeInfo(Builder paramBuilder)
    {
      super();
    }

    private SourceCodeInfo(boolean paramBoolean)
    {
    }

    public static SourceCodeInfo getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_descriptor;
    }

    private void initFields()
    {
      this.location_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$20200();
    }

    public static Builder newBuilder(SourceCodeInfo paramSourceCodeInfo)
    {
      return newBuilder().mergeFrom(paramSourceCodeInfo);
    }

    public static SourceCodeInfo parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static SourceCodeInfo parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static SourceCodeInfo parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static SourceCodeInfo parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static SourceCodeInfo parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static SourceCodeInfo parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static SourceCodeInfo parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static SourceCodeInfo parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static SourceCodeInfo parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static SourceCodeInfo parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public SourceCodeInfo getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public Location getLocation(int paramInt)
    {
      return (Location)this.location_.get(paramInt);
    }

    public int getLocationCount()
    {
      return this.location_.size();
    }

    public List<Location> getLocationList()
    {
      return this.location_;
    }

    public LocationOrBuilder getLocationOrBuilder(int paramInt)
    {
      return (LocationOrBuilder)this.location_.get(paramInt);
    }

    public List<? extends LocationOrBuilder> getLocationOrBuilderList()
    {
      return this.location_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.location_.size(); k++)
        j += CodedOutputStream.computeMessageSize(1, (MessageLite)this.location_.get(k));
      int m = j + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      for (int i = 0; i < this.location_.size(); i++)
        paramCodedOutputStream.writeMessage(1, (MessageLite)this.location_.get(i));
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.SourceCodeInfoOrBuilder
    {
      private int bitField0_;
      private RepeatedFieldBuilder<DescriptorProtos.SourceCodeInfo.Location, DescriptorProtos.SourceCodeInfo.Location.Builder, DescriptorProtos.SourceCodeInfo.LocationOrBuilder> locationBuilder_;
      private List<DescriptorProtos.SourceCodeInfo.Location> location_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.SourceCodeInfo buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.SourceCodeInfo localSourceCodeInfo = buildPartial();
        if (!localSourceCodeInfo.isInitialized())
          throw newUninitializedMessageException(localSourceCodeInfo).asInvalidProtocolBufferException();
        return localSourceCodeInfo;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureLocationIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.location_ = new ArrayList(this.location_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.SourceCodeInfo.Location, DescriptorProtos.SourceCodeInfo.Location.Builder, DescriptorProtos.SourceCodeInfo.LocationOrBuilder> getLocationFieldBuilder()
      {
        List localList;
        if (this.locationBuilder_ == null)
        {
          localList = this.location_;
          if ((0x1 & this.bitField0_) != 1)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.locationBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.location_ = null;
          return this.locationBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getLocationFieldBuilder();
      }

      public Builder addAllLocation(Iterable<? extends DescriptorProtos.SourceCodeInfo.Location> paramIterable)
      {
        if (this.locationBuilder_ == null)
        {
          ensureLocationIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.location_);
          onChanged();
          return this;
        }
        this.locationBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addLocation(int paramInt, DescriptorProtos.SourceCodeInfo.Location.Builder paramBuilder)
      {
        if (this.locationBuilder_ == null)
        {
          ensureLocationIsMutable();
          this.location_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.locationBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addLocation(int paramInt, DescriptorProtos.SourceCodeInfo.Location paramLocation)
      {
        if (this.locationBuilder_ == null)
        {
          if (paramLocation == null)
            throw new NullPointerException();
          ensureLocationIsMutable();
          this.location_.add(paramInt, paramLocation);
          onChanged();
          return this;
        }
        this.locationBuilder_.addMessage(paramInt, paramLocation);
        return this;
      }

      public Builder addLocation(DescriptorProtos.SourceCodeInfo.Location.Builder paramBuilder)
      {
        if (this.locationBuilder_ == null)
        {
          ensureLocationIsMutable();
          this.location_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.locationBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addLocation(DescriptorProtos.SourceCodeInfo.Location paramLocation)
      {
        if (this.locationBuilder_ == null)
        {
          if (paramLocation == null)
            throw new NullPointerException();
          ensureLocationIsMutable();
          this.location_.add(paramLocation);
          onChanged();
          return this;
        }
        this.locationBuilder_.addMessage(paramLocation);
        return this;
      }

      public DescriptorProtos.SourceCodeInfo.Location.Builder addLocationBuilder()
      {
        return (DescriptorProtos.SourceCodeInfo.Location.Builder)getLocationFieldBuilder().addBuilder(DescriptorProtos.SourceCodeInfo.Location.getDefaultInstance());
      }

      public DescriptorProtos.SourceCodeInfo.Location.Builder addLocationBuilder(int paramInt)
      {
        return (DescriptorProtos.SourceCodeInfo.Location.Builder)getLocationFieldBuilder().addBuilder(paramInt, DescriptorProtos.SourceCodeInfo.Location.getDefaultInstance());
      }

      public DescriptorProtos.SourceCodeInfo build()
      {
        DescriptorProtos.SourceCodeInfo localSourceCodeInfo = buildPartial();
        if (!localSourceCodeInfo.isInitialized())
          throw newUninitializedMessageException(localSourceCodeInfo);
        return localSourceCodeInfo;
      }

      public DescriptorProtos.SourceCodeInfo buildPartial()
      {
        DescriptorProtos.SourceCodeInfo localSourceCodeInfo = new DescriptorProtos.SourceCodeInfo(this, null);
        if (this.locationBuilder_ == null)
        {
          if ((0x1 & this.bitField0_) == 1)
          {
            this.location_ = Collections.unmodifiableList(this.location_);
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
          DescriptorProtos.SourceCodeInfo.access$20502(localSourceCodeInfo, this.location_);
        }
        while (true)
        {
          onBuilt();
          return localSourceCodeInfo;
          DescriptorProtos.SourceCodeInfo.access$20502(localSourceCodeInfo, this.locationBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        if (this.locationBuilder_ == null)
        {
          this.location_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          return this;
        }
        this.locationBuilder_.clear();
        return this;
      }

      public Builder clearLocation()
      {
        if (this.locationBuilder_ == null)
        {
          this.location_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          onChanged();
          return this;
        }
        this.locationBuilder_.clear();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public DescriptorProtos.SourceCodeInfo getDefaultInstanceForType()
      {
        return DescriptorProtos.SourceCodeInfo.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.SourceCodeInfo.getDescriptor();
      }

      public DescriptorProtos.SourceCodeInfo.Location getLocation(int paramInt)
      {
        if (this.locationBuilder_ == null)
          return (DescriptorProtos.SourceCodeInfo.Location)this.location_.get(paramInt);
        return (DescriptorProtos.SourceCodeInfo.Location)this.locationBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.SourceCodeInfo.Location.Builder getLocationBuilder(int paramInt)
      {
        return (DescriptorProtos.SourceCodeInfo.Location.Builder)getLocationFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.SourceCodeInfo.Location.Builder> getLocationBuilderList()
      {
        return getLocationFieldBuilder().getBuilderList();
      }

      public int getLocationCount()
      {
        if (this.locationBuilder_ == null)
          return this.location_.size();
        return this.locationBuilder_.getCount();
      }

      public List<DescriptorProtos.SourceCodeInfo.Location> getLocationList()
      {
        if (this.locationBuilder_ == null)
          return Collections.unmodifiableList(this.location_);
        return this.locationBuilder_.getMessageList();
      }

      public DescriptorProtos.SourceCodeInfo.LocationOrBuilder getLocationOrBuilder(int paramInt)
      {
        if (this.locationBuilder_ == null)
          return (DescriptorProtos.SourceCodeInfo.LocationOrBuilder)this.location_.get(paramInt);
        return (DescriptorProtos.SourceCodeInfo.LocationOrBuilder)this.locationBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.SourceCodeInfo.LocationOrBuilder> getLocationOrBuilderList()
      {
        if (this.locationBuilder_ != null)
          return this.locationBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.location_);
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 10:
            DescriptorProtos.SourceCodeInfo.Location.Builder localBuilder1 = DescriptorProtos.SourceCodeInfo.Location.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addLocation(localBuilder1.buildPartial());
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.SourceCodeInfo paramSourceCodeInfo)
      {
        if (paramSourceCodeInfo == DescriptorProtos.SourceCodeInfo.getDefaultInstance())
          return this;
        if (this.locationBuilder_ == null)
          if (!paramSourceCodeInfo.location_.isEmpty())
          {
            if (!this.location_.isEmpty())
              break label74;
            this.location_ = paramSourceCodeInfo.location_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          mergeUnknownFields(paramSourceCodeInfo.getUnknownFields());
          return this;
          label74: ensureLocationIsMutable();
          this.location_.addAll(paramSourceCodeInfo.location_);
          break;
          if (!paramSourceCodeInfo.location_.isEmpty())
          {
            if (this.locationBuilder_.isEmpty())
            {
              this.locationBuilder_.dispose();
              this.locationBuilder_ = null;
              this.location_ = paramSourceCodeInfo.location_;
              this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getLocationFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.locationBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.locationBuilder_.addAllMessages(paramSourceCodeInfo.location_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.SourceCodeInfo))
          return mergeFrom((DescriptorProtos.SourceCodeInfo)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeLocation(int paramInt)
      {
        if (this.locationBuilder_ == null)
        {
          ensureLocationIsMutable();
          this.location_.remove(paramInt);
          onChanged();
          return this;
        }
        this.locationBuilder_.remove(paramInt);
        return this;
      }

      public Builder setLocation(int paramInt, DescriptorProtos.SourceCodeInfo.Location.Builder paramBuilder)
      {
        if (this.locationBuilder_ == null)
        {
          ensureLocationIsMutable();
          this.location_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.locationBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setLocation(int paramInt, DescriptorProtos.SourceCodeInfo.Location paramLocation)
      {
        if (this.locationBuilder_ == null)
        {
          if (paramLocation == null)
            throw new NullPointerException();
          ensureLocationIsMutable();
          this.location_.set(paramInt, paramLocation);
          onChanged();
          return this;
        }
        this.locationBuilder_.setMessage(paramInt, paramLocation);
        return this;
      }
    }

    public static final class Location extends GeneratedMessage
      implements DescriptorProtos.SourceCodeInfo.LocationOrBuilder
    {
      public static final int PATH_FIELD_NUMBER = 1;
      public static final int SPAN_FIELD_NUMBER = 2;
      private static final Location defaultInstance = new Location(true);
      private byte memoizedIsInitialized = -1;
      private int memoizedSerializedSize = -1;
      private int pathMemoizedSerializedSize = -1;
      private List<Integer> path_;
      private int spanMemoizedSerializedSize = -1;
      private List<Integer> span_;

      static
      {
        defaultInstance.initFields();
      }

      private Location(Builder paramBuilder)
      {
        super();
      }

      private Location(boolean paramBoolean)
      {
      }

      public static Location getDefaultInstance()
      {
        return defaultInstance;
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_Location_descriptor;
      }

      private void initFields()
      {
        this.path_ = Collections.emptyList();
        this.span_ = Collections.emptyList();
      }

      public static Builder newBuilder()
      {
        return Builder.access$19600();
      }

      public static Builder newBuilder(Location paramLocation)
      {
        return newBuilder().mergeFrom(paramLocation);
      }

      public static Location parseDelimitedFrom(InputStream paramInputStream)
        throws IOException
      {
        Builder localBuilder = newBuilder();
        if (localBuilder.mergeDelimitedFrom(paramInputStream))
          return localBuilder.buildParsed();
        return null;
      }

      public static Location parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        Builder localBuilder = newBuilder();
        if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
          return localBuilder.buildParsed();
        return null;
      }

      public static Location parseFrom(ByteString paramByteString)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
      }

      public static Location parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
      }

      public static Location parseFrom(CodedInputStream paramCodedInputStream)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
      }

      public static Location parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
      }

      public static Location parseFrom(InputStream paramInputStream)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
      }

      public static Location parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
      }

      public static Location parseFrom(byte[] paramArrayOfByte)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
      }

      public static Location parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
      }

      public Location getDefaultInstanceForType()
      {
        return defaultInstance;
      }

      public int getPath(int paramInt)
      {
        return ((Integer)this.path_.get(paramInt)).intValue();
      }

      public int getPathCount()
      {
        return this.path_.size();
      }

      public List<Integer> getPathList()
      {
        return this.path_;
      }

      public int getSerializedSize()
      {
        int i = this.memoizedSerializedSize;
        if (i != -1)
          return i;
        int j = 0;
        for (int k = 0; k < this.path_.size(); k++)
          j += CodedOutputStream.computeInt32SizeNoTag(((Integer)this.path_.get(k)).intValue());
        int m = 0 + j;
        if (!getPathList().isEmpty())
          m = m + 1 + CodedOutputStream.computeInt32SizeNoTag(j);
        this.pathMemoizedSerializedSize = j;
        int n = 0;
        for (int i1 = 0; i1 < this.span_.size(); i1++)
          n += CodedOutputStream.computeInt32SizeNoTag(((Integer)this.span_.get(i1)).intValue());
        int i2 = m + n;
        if (!getSpanList().isEmpty())
          i2 = i2 + 1 + CodedOutputStream.computeInt32SizeNoTag(n);
        this.spanMemoizedSerializedSize = n;
        int i3 = i2 + getUnknownFields().getSerializedSize();
        this.memoizedSerializedSize = i3;
        return i3;
      }

      public int getSpan(int paramInt)
      {
        return ((Integer)this.span_.get(paramInt)).intValue();
      }

      public int getSpanCount()
      {
        return this.span_.size();
      }

      public List<Integer> getSpanList()
      {
        return this.span_;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_Location_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        int i = this.memoizedIsInitialized;
        if (i != -1)
          return i == 1;
        this.memoizedIsInitialized = 1;
        return true;
      }

      public Builder newBuilderForType()
      {
        return newBuilder();
      }

      protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        return new Builder(paramBuilderParent, null);
      }

      public Builder toBuilder()
      {
        return newBuilder(this);
      }

      protected Object writeReplace()
        throws ObjectStreamException
      {
        return super.writeReplace();
      }

      public void writeTo(CodedOutputStream paramCodedOutputStream)
        throws IOException
      {
        getSerializedSize();
        if (getPathList().size() > 0)
        {
          paramCodedOutputStream.writeRawVarint32(10);
          paramCodedOutputStream.writeRawVarint32(this.pathMemoizedSerializedSize);
        }
        for (int i = 0; i < this.path_.size(); i++)
          paramCodedOutputStream.writeInt32NoTag(((Integer)this.path_.get(i)).intValue());
        if (getSpanList().size() > 0)
        {
          paramCodedOutputStream.writeRawVarint32(18);
          paramCodedOutputStream.writeRawVarint32(this.spanMemoizedSerializedSize);
        }
        for (int j = 0; j < this.span_.size(); j++)
          paramCodedOutputStream.writeInt32NoTag(((Integer)this.span_.get(j)).intValue());
        getUnknownFields().writeTo(paramCodedOutputStream);
      }

      public static final class Builder extends GeneratedMessage.Builder<Builder>
        implements DescriptorProtos.SourceCodeInfo.LocationOrBuilder
      {
        private int bitField0_;
        private List<Integer> path_ = Collections.emptyList();
        private List<Integer> span_ = Collections.emptyList();

        private Builder()
        {
          maybeForceBuilderInitialization();
        }

        private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
        {
          super();
          maybeForceBuilderInitialization();
        }

        private DescriptorProtos.SourceCodeInfo.Location buildParsed()
          throws InvalidProtocolBufferException
        {
          DescriptorProtos.SourceCodeInfo.Location localLocation = buildPartial();
          if (!localLocation.isInitialized())
            throw newUninitializedMessageException(localLocation).asInvalidProtocolBufferException();
          return localLocation;
        }

        private static Builder create()
        {
          return new Builder();
        }

        private void ensurePathIsMutable()
        {
          if ((0x1 & this.bitField0_) != 1)
          {
            this.path_ = new ArrayList(this.path_);
            this.bitField0_ = (0x1 | this.bitField0_);
          }
        }

        private void ensureSpanIsMutable()
        {
          if ((0x2 & this.bitField0_) != 2)
          {
            this.span_ = new ArrayList(this.span_);
            this.bitField0_ = (0x2 | this.bitField0_);
          }
        }

        public static final Descriptors.Descriptor getDescriptor()
        {
          return DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_Location_descriptor;
        }

        private void maybeForceBuilderInitialization()
        {
          if (GeneratedMessage.alwaysUseFieldBuilders);
        }

        public Builder addAllPath(Iterable<? extends Integer> paramIterable)
        {
          ensurePathIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.path_);
          onChanged();
          return this;
        }

        public Builder addAllSpan(Iterable<? extends Integer> paramIterable)
        {
          ensureSpanIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.span_);
          onChanged();
          return this;
        }

        public Builder addPath(int paramInt)
        {
          ensurePathIsMutable();
          this.path_.add(Integer.valueOf(paramInt));
          onChanged();
          return this;
        }

        public Builder addSpan(int paramInt)
        {
          ensureSpanIsMutable();
          this.span_.add(Integer.valueOf(paramInt));
          onChanged();
          return this;
        }

        public DescriptorProtos.SourceCodeInfo.Location build()
        {
          DescriptorProtos.SourceCodeInfo.Location localLocation = buildPartial();
          if (!localLocation.isInitialized())
            throw newUninitializedMessageException(localLocation);
          return localLocation;
        }

        public DescriptorProtos.SourceCodeInfo.Location buildPartial()
        {
          DescriptorProtos.SourceCodeInfo.Location localLocation = new DescriptorProtos.SourceCodeInfo.Location(this, null);
          if ((0x1 & this.bitField0_) == 1)
          {
            this.path_ = Collections.unmodifiableList(this.path_);
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
          DescriptorProtos.SourceCodeInfo.Location.access$19902(localLocation, this.path_);
          if ((0x2 & this.bitField0_) == 2)
          {
            this.span_ = Collections.unmodifiableList(this.span_);
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          }
          DescriptorProtos.SourceCodeInfo.Location.access$20002(localLocation, this.span_);
          onBuilt();
          return localLocation;
        }

        public Builder clear()
        {
          super.clear();
          this.path_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          this.span_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          return this;
        }

        public Builder clearPath()
        {
          this.path_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          onChanged();
          return this;
        }

        public Builder clearSpan()
        {
          this.span_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          onChanged();
          return this;
        }

        public Builder clone()
        {
          return create().mergeFrom(buildPartial());
        }

        public DescriptorProtos.SourceCodeInfo.Location getDefaultInstanceForType()
        {
          return DescriptorProtos.SourceCodeInfo.Location.getDefaultInstance();
        }

        public Descriptors.Descriptor getDescriptorForType()
        {
          return DescriptorProtos.SourceCodeInfo.Location.getDescriptor();
        }

        public int getPath(int paramInt)
        {
          return ((Integer)this.path_.get(paramInt)).intValue();
        }

        public int getPathCount()
        {
          return this.path_.size();
        }

        public List<Integer> getPathList()
        {
          return Collections.unmodifiableList(this.path_);
        }

        public int getSpan(int paramInt)
        {
          return ((Integer)this.span_.get(paramInt)).intValue();
        }

        public int getSpanCount()
        {
          return this.span_.size();
        }

        public List<Integer> getSpanList()
        {
          return Collections.unmodifiableList(this.span_);
        }

        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
        {
          return DescriptorProtos.internal_static_google_protobuf_SourceCodeInfo_Location_fieldAccessorTable;
        }

        public final boolean isInitialized()
        {
          return true;
        }

        public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
          throws IOException
        {
          UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
          while (true)
          {
            int i = paramCodedInputStream.readTag();
            switch (i)
            {
            default:
              if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
              {
                setUnknownFields(localBuilder.build());
                onChanged();
                return this;
              }
              break;
            case 0:
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            case 8:
              ensurePathIsMutable();
              this.path_.add(Integer.valueOf(paramCodedInputStream.readInt32()));
              break;
            case 10:
              int k = paramCodedInputStream.pushLimit(paramCodedInputStream.readRawVarint32());
              while (paramCodedInputStream.getBytesUntilLimit() > 0)
                addPath(paramCodedInputStream.readInt32());
              paramCodedInputStream.popLimit(k);
              break;
            case 16:
              ensureSpanIsMutable();
              this.span_.add(Integer.valueOf(paramCodedInputStream.readInt32()));
              break;
            case 18:
              int j = paramCodedInputStream.pushLimit(paramCodedInputStream.readRawVarint32());
              while (paramCodedInputStream.getBytesUntilLimit() > 0)
                addSpan(paramCodedInputStream.readInt32());
              paramCodedInputStream.popLimit(j);
            }
          }
        }

        public Builder mergeFrom(DescriptorProtos.SourceCodeInfo.Location paramLocation)
        {
          if (paramLocation == DescriptorProtos.SourceCodeInfo.Location.getDefaultInstance())
            return this;
          if (!paramLocation.path_.isEmpty())
          {
            if (this.path_.isEmpty())
            {
              this.path_ = paramLocation.path_;
              this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
              onChanged();
            }
          }
          else if (!paramLocation.span_.isEmpty())
          {
            if (!this.span_.isEmpty())
              break label135;
            this.span_ = paramLocation.span_;
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          }
          while (true)
          {
            onChanged();
            mergeUnknownFields(paramLocation.getUnknownFields());
            return this;
            ensurePathIsMutable();
            this.path_.addAll(paramLocation.path_);
            break;
            label135: ensureSpanIsMutable();
            this.span_.addAll(paramLocation.span_);
          }
        }

        public Builder mergeFrom(Message paramMessage)
        {
          if ((paramMessage instanceof DescriptorProtos.SourceCodeInfo.Location))
            return mergeFrom((DescriptorProtos.SourceCodeInfo.Location)paramMessage);
          super.mergeFrom(paramMessage);
          return this;
        }

        public Builder setPath(int paramInt1, int paramInt2)
        {
          ensurePathIsMutable();
          this.path_.set(paramInt1, Integer.valueOf(paramInt2));
          onChanged();
          return this;
        }

        public Builder setSpan(int paramInt1, int paramInt2)
        {
          ensureSpanIsMutable();
          this.span_.set(paramInt1, Integer.valueOf(paramInt2));
          onChanged();
          return this;
        }
      }
    }

    public static abstract interface LocationOrBuilder extends MessageOrBuilder
    {
      public abstract int getPath(int paramInt);

      public abstract int getPathCount();

      public abstract List<Integer> getPathList();

      public abstract int getSpan(int paramInt);

      public abstract int getSpanCount();

      public abstract List<Integer> getSpanList();
    }
  }

  public static abstract interface SourceCodeInfoOrBuilder extends MessageOrBuilder
  {
    public abstract DescriptorProtos.SourceCodeInfo.Location getLocation(int paramInt);

    public abstract int getLocationCount();

    public abstract List<DescriptorProtos.SourceCodeInfo.Location> getLocationList();

    public abstract DescriptorProtos.SourceCodeInfo.LocationOrBuilder getLocationOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.SourceCodeInfo.LocationOrBuilder> getLocationOrBuilderList();
  }

  public static final class UninterpretedOption extends GeneratedMessage
    implements DescriptorProtos.UninterpretedOptionOrBuilder
  {
    public static final int AGGREGATE_VALUE_FIELD_NUMBER = 8;
    public static final int DOUBLE_VALUE_FIELD_NUMBER = 6;
    public static final int IDENTIFIER_VALUE_FIELD_NUMBER = 3;
    public static final int NAME_FIELD_NUMBER = 2;
    public static final int NEGATIVE_INT_VALUE_FIELD_NUMBER = 5;
    public static final int POSITIVE_INT_VALUE_FIELD_NUMBER = 4;
    public static final int STRING_VALUE_FIELD_NUMBER = 7;
    private static final UninterpretedOption defaultInstance = new UninterpretedOption(true);
    private Object aggregateValue_;
    private int bitField0_;
    private double doubleValue_;
    private Object identifierValue_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<NamePart> name_;
    private long negativeIntValue_;
    private long positiveIntValue_;
    private ByteString stringValue_;

    static
    {
      defaultInstance.initFields();
    }

    private UninterpretedOption(Builder paramBuilder)
    {
      super();
    }

    private UninterpretedOption(boolean paramBoolean)
    {
    }

    private ByteString getAggregateValueBytes()
    {
      Object localObject = this.aggregateValue_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.aggregateValue_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public static UninterpretedOption getDefaultInstance()
    {
      return defaultInstance;
    }

    public static final Descriptors.Descriptor getDescriptor()
    {
      return DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_descriptor;
    }

    private ByteString getIdentifierValueBytes()
    {
      Object localObject = this.identifierValue_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.identifierValue_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.name_ = Collections.emptyList();
      this.identifierValue_ = "";
      this.positiveIntValue_ = 0L;
      this.negativeIntValue_ = 0L;
      this.doubleValue_ = 0.0D;
      this.stringValue_ = ByteString.EMPTY;
      this.aggregateValue_ = "";
    }

    public static Builder newBuilder()
    {
      return Builder.access$18000();
    }

    public static Builder newBuilder(UninterpretedOption paramUninterpretedOption)
    {
      return newBuilder().mergeFrom(paramUninterpretedOption);
    }

    public static UninterpretedOption parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static UninterpretedOption parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static UninterpretedOption parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static UninterpretedOption parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static UninterpretedOption parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static UninterpretedOption parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static UninterpretedOption parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static UninterpretedOption parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static UninterpretedOption parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static UninterpretedOption parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public String getAggregateValue()
    {
      Object localObject = this.aggregateValue_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.aggregateValue_ = str;
      return str;
    }

    public UninterpretedOption getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public double getDoubleValue()
    {
      return this.doubleValue_;
    }

    public String getIdentifierValue()
    {
      Object localObject = this.identifierValue_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.identifierValue_ = str;
      return str;
    }

    public NamePart getName(int paramInt)
    {
      return (NamePart)this.name_.get(paramInt);
    }

    public int getNameCount()
    {
      return this.name_.size();
    }

    public List<NamePart> getNameList()
    {
      return this.name_;
    }

    public NamePartOrBuilder getNameOrBuilder(int paramInt)
    {
      return (NamePartOrBuilder)this.name_.get(paramInt);
    }

    public List<? extends NamePartOrBuilder> getNameOrBuilderList()
    {
      return this.name_;
    }

    public long getNegativeIntValue()
    {
      return this.negativeIntValue_;
    }

    public long getPositiveIntValue()
    {
      return this.positiveIntValue_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.name_.size(); k++)
        j += CodedOutputStream.computeMessageSize(2, (MessageLite)this.name_.get(k));
      if ((0x1 & this.bitField0_) == 1)
        j += CodedOutputStream.computeBytesSize(3, getIdentifierValueBytes());
      if ((0x2 & this.bitField0_) == 2)
        j += CodedOutputStream.computeUInt64Size(4, this.positiveIntValue_);
      if ((0x4 & this.bitField0_) == 4)
        j += CodedOutputStream.computeInt64Size(5, this.negativeIntValue_);
      if ((0x8 & this.bitField0_) == 8)
        j += CodedOutputStream.computeDoubleSize(6, this.doubleValue_);
      if ((0x10 & this.bitField0_) == 16)
        j += CodedOutputStream.computeBytesSize(7, this.stringValue_);
      if ((0x20 & this.bitField0_) == 32)
        j += CodedOutputStream.computeBytesSize(8, getAggregateValueBytes());
      int m = j + getUnknownFields().getSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public ByteString getStringValue()
    {
      return this.stringValue_;
    }

    public boolean hasAggregateValue()
    {
      return (0x20 & this.bitField0_) == 32;
    }

    public boolean hasDoubleValue()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasIdentifierValue()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasNegativeIntValue()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasPositiveIntValue()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasStringValue()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
    {
      return DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_fieldAccessorTable;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getNameCount(); j++)
        if (!getName(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
    {
      return new Builder(paramBuilderParent, null);
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      for (int i = 0; i < this.name_.size(); i++)
        paramCodedOutputStream.writeMessage(2, (MessageLite)this.name_.get(i));
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(3, getIdentifierValueBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeUInt64(4, this.positiveIntValue_);
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeInt64(5, this.negativeIntValue_);
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeDouble(6, this.doubleValue_);
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeBytes(7, this.stringValue_);
      if ((0x20 & this.bitField0_) == 32)
        paramCodedOutputStream.writeBytes(8, getAggregateValueBytes());
      getUnknownFields().writeTo(paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessage.Builder<Builder>
      implements DescriptorProtos.UninterpretedOptionOrBuilder
    {
      private Object aggregateValue_ = "";
      private int bitField0_;
      private double doubleValue_;
      private Object identifierValue_ = "";
      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption.NamePart, DescriptorProtos.UninterpretedOption.NamePart.Builder, DescriptorProtos.UninterpretedOption.NamePartOrBuilder> nameBuilder_;
      private List<DescriptorProtos.UninterpretedOption.NamePart> name_ = Collections.emptyList();
      private long negativeIntValue_;
      private long positiveIntValue_;
      private ByteString stringValue_ = ByteString.EMPTY;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        super();
        maybeForceBuilderInitialization();
      }

      private DescriptorProtos.UninterpretedOption buildParsed()
        throws InvalidProtocolBufferException
      {
        DescriptorProtos.UninterpretedOption localUninterpretedOption = buildPartial();
        if (!localUninterpretedOption.isInitialized())
          throw newUninitializedMessageException(localUninterpretedOption).asInvalidProtocolBufferException();
        return localUninterpretedOption;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureNameIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.name_ = new ArrayList(this.name_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_descriptor;
      }

      private RepeatedFieldBuilder<DescriptorProtos.UninterpretedOption.NamePart, DescriptorProtos.UninterpretedOption.NamePart.Builder, DescriptorProtos.UninterpretedOption.NamePartOrBuilder> getNameFieldBuilder()
      {
        List localList;
        if (this.nameBuilder_ == null)
        {
          localList = this.name_;
          if ((0x1 & this.bitField0_) != 1)
            break label55;
        }
        label55: for (boolean bool = true; ; bool = false)
        {
          this.nameBuilder_ = new RepeatedFieldBuilder(localList, bool, getParentForChildren(), isClean());
          this.name_ = null;
          return this.nameBuilder_;
        }
      }

      private void maybeForceBuilderInitialization()
      {
        if (GeneratedMessage.alwaysUseFieldBuilders)
          getNameFieldBuilder();
      }

      public Builder addAllName(Iterable<? extends DescriptorProtos.UninterpretedOption.NamePart> paramIterable)
      {
        if (this.nameBuilder_ == null)
        {
          ensureNameIsMutable();
          GeneratedMessage.Builder.addAll(paramIterable, this.name_);
          onChanged();
          return this;
        }
        this.nameBuilder_.addAllMessages(paramIterable);
        return this;
      }

      public Builder addName(int paramInt, DescriptorProtos.UninterpretedOption.NamePart.Builder paramBuilder)
      {
        if (this.nameBuilder_ == null)
        {
          ensureNameIsMutable();
          this.name_.add(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.nameBuilder_.addMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addName(int paramInt, DescriptorProtos.UninterpretedOption.NamePart paramNamePart)
      {
        if (this.nameBuilder_ == null)
        {
          if (paramNamePart == null)
            throw new NullPointerException();
          ensureNameIsMutable();
          this.name_.add(paramInt, paramNamePart);
          onChanged();
          return this;
        }
        this.nameBuilder_.addMessage(paramInt, paramNamePart);
        return this;
      }

      public Builder addName(DescriptorProtos.UninterpretedOption.NamePart.Builder paramBuilder)
      {
        if (this.nameBuilder_ == null)
        {
          ensureNameIsMutable();
          this.name_.add(paramBuilder.build());
          onChanged();
          return this;
        }
        this.nameBuilder_.addMessage(paramBuilder.build());
        return this;
      }

      public Builder addName(DescriptorProtos.UninterpretedOption.NamePart paramNamePart)
      {
        if (this.nameBuilder_ == null)
        {
          if (paramNamePart == null)
            throw new NullPointerException();
          ensureNameIsMutable();
          this.name_.add(paramNamePart);
          onChanged();
          return this;
        }
        this.nameBuilder_.addMessage(paramNamePart);
        return this;
      }

      public DescriptorProtos.UninterpretedOption.NamePart.Builder addNameBuilder()
      {
        return (DescriptorProtos.UninterpretedOption.NamePart.Builder)getNameFieldBuilder().addBuilder(DescriptorProtos.UninterpretedOption.NamePart.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption.NamePart.Builder addNameBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.NamePart.Builder)getNameFieldBuilder().addBuilder(paramInt, DescriptorProtos.UninterpretedOption.NamePart.getDefaultInstance());
      }

      public DescriptorProtos.UninterpretedOption build()
      {
        DescriptorProtos.UninterpretedOption localUninterpretedOption = buildPartial();
        if (!localUninterpretedOption.isInitialized())
          throw newUninitializedMessageException(localUninterpretedOption);
        return localUninterpretedOption;
      }

      public DescriptorProtos.UninterpretedOption buildPartial()
      {
        DescriptorProtos.UninterpretedOption localUninterpretedOption = new DescriptorProtos.UninterpretedOption(this, null);
        int i = this.bitField0_;
        if (this.nameBuilder_ == null)
        {
          if ((0x1 & this.bitField0_) == 1)
          {
            this.name_ = Collections.unmodifiableList(this.name_);
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
          DescriptorProtos.UninterpretedOption.access$18302(localUninterpretedOption, this.name_);
        }
        while (true)
        {
          int j = i & 0x2;
          int k = 0;
          if (j == 2)
            k = 0x0 | 0x1;
          DescriptorProtos.UninterpretedOption.access$18402(localUninterpretedOption, this.identifierValue_);
          if ((i & 0x4) == 4)
            k |= 2;
          DescriptorProtos.UninterpretedOption.access$18502(localUninterpretedOption, this.positiveIntValue_);
          if ((i & 0x8) == 8)
            k |= 4;
          DescriptorProtos.UninterpretedOption.access$18602(localUninterpretedOption, this.negativeIntValue_);
          if ((i & 0x10) == 16)
            k |= 8;
          DescriptorProtos.UninterpretedOption.access$18702(localUninterpretedOption, this.doubleValue_);
          if ((i & 0x20) == 32)
            k |= 16;
          DescriptorProtos.UninterpretedOption.access$18802(localUninterpretedOption, this.stringValue_);
          if ((i & 0x40) == 64)
            k |= 32;
          DescriptorProtos.UninterpretedOption.access$18902(localUninterpretedOption, this.aggregateValue_);
          DescriptorProtos.UninterpretedOption.access$19002(localUninterpretedOption, k);
          onBuilt();
          return localUninterpretedOption;
          DescriptorProtos.UninterpretedOption.access$18302(localUninterpretedOption, this.nameBuilder_.build());
        }
      }

      public Builder clear()
      {
        super.clear();
        if (this.nameBuilder_ == null)
        {
          this.name_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        }
        while (true)
        {
          this.identifierValue_ = "";
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          this.positiveIntValue_ = 0L;
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          this.negativeIntValue_ = 0L;
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          this.doubleValue_ = 0.0D;
          this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          this.stringValue_ = ByteString.EMPTY;
          this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
          this.aggregateValue_ = "";
          this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
          return this;
          this.nameBuilder_.clear();
        }
      }

      public Builder clearAggregateValue()
      {
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.aggregateValue_ = DescriptorProtos.UninterpretedOption.getDefaultInstance().getAggregateValue();
        onChanged();
        return this;
      }

      public Builder clearDoubleValue()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.doubleValue_ = 0.0D;
        onChanged();
        return this;
      }

      public Builder clearIdentifierValue()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.identifierValue_ = DescriptorProtos.UninterpretedOption.getDefaultInstance().getIdentifierValue();
        onChanged();
        return this;
      }

      public Builder clearName()
      {
        if (this.nameBuilder_ == null)
        {
          this.name_ = Collections.emptyList();
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          onChanged();
          return this;
        }
        this.nameBuilder_.clear();
        return this;
      }

      public Builder clearNegativeIntValue()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.negativeIntValue_ = 0L;
        onChanged();
        return this;
      }

      public Builder clearPositiveIntValue()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.positiveIntValue_ = 0L;
        onChanged();
        return this;
      }

      public Builder clearStringValue()
      {
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.stringValue_ = DescriptorProtos.UninterpretedOption.getDefaultInstance().getStringValue();
        onChanged();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public String getAggregateValue()
      {
        Object localObject = this.aggregateValue_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.aggregateValue_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.UninterpretedOption getDefaultInstanceForType()
      {
        return DescriptorProtos.UninterpretedOption.getDefaultInstance();
      }

      public Descriptors.Descriptor getDescriptorForType()
      {
        return DescriptorProtos.UninterpretedOption.getDescriptor();
      }

      public double getDoubleValue()
      {
        return this.doubleValue_;
      }

      public String getIdentifierValue()
      {
        Object localObject = this.identifierValue_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.identifierValue_ = str;
          return str;
        }
        return (String)localObject;
      }

      public DescriptorProtos.UninterpretedOption.NamePart getName(int paramInt)
      {
        if (this.nameBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption.NamePart)this.name_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption.NamePart)this.nameBuilder_.getMessage(paramInt);
      }

      public DescriptorProtos.UninterpretedOption.NamePart.Builder getNameBuilder(int paramInt)
      {
        return (DescriptorProtos.UninterpretedOption.NamePart.Builder)getNameFieldBuilder().getBuilder(paramInt);
      }

      public List<DescriptorProtos.UninterpretedOption.NamePart.Builder> getNameBuilderList()
      {
        return getNameFieldBuilder().getBuilderList();
      }

      public int getNameCount()
      {
        if (this.nameBuilder_ == null)
          return this.name_.size();
        return this.nameBuilder_.getCount();
      }

      public List<DescriptorProtos.UninterpretedOption.NamePart> getNameList()
      {
        if (this.nameBuilder_ == null)
          return Collections.unmodifiableList(this.name_);
        return this.nameBuilder_.getMessageList();
      }

      public DescriptorProtos.UninterpretedOption.NamePartOrBuilder getNameOrBuilder(int paramInt)
      {
        if (this.nameBuilder_ == null)
          return (DescriptorProtos.UninterpretedOption.NamePartOrBuilder)this.name_.get(paramInt);
        return (DescriptorProtos.UninterpretedOption.NamePartOrBuilder)this.nameBuilder_.getMessageOrBuilder(paramInt);
      }

      public List<? extends DescriptorProtos.UninterpretedOption.NamePartOrBuilder> getNameOrBuilderList()
      {
        if (this.nameBuilder_ != null)
          return this.nameBuilder_.getMessageOrBuilderList();
        return Collections.unmodifiableList(this.name_);
      }

      public long getNegativeIntValue()
      {
        return this.negativeIntValue_;
      }

      public long getPositiveIntValue()
      {
        return this.positiveIntValue_;
      }

      public ByteString getStringValue()
      {
        return this.stringValue_;
      }

      public boolean hasAggregateValue()
      {
        return (0x40 & this.bitField0_) == 64;
      }

      public boolean hasDoubleValue()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public boolean hasIdentifierValue()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasNegativeIntValue()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasPositiveIntValue()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasStringValue()
      {
        return (0x20 & this.bitField0_) == 32;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getNameCount(); i++)
          if (!getName(i).isInitialized())
            return false;
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
            {
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            }
            break;
          case 0:
            setUnknownFields(localBuilder.build());
            onChanged();
            return this;
          case 18:
            DescriptorProtos.UninterpretedOption.NamePart.Builder localBuilder1 = DescriptorProtos.UninterpretedOption.NamePart.newBuilder();
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            addName(localBuilder1.buildPartial());
            break;
          case 26:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.identifierValue_ = paramCodedInputStream.readBytes();
            break;
          case 32:
            this.bitField0_ = (0x4 | this.bitField0_);
            this.positiveIntValue_ = paramCodedInputStream.readUInt64();
            break;
          case 40:
            this.bitField0_ = (0x8 | this.bitField0_);
            this.negativeIntValue_ = paramCodedInputStream.readInt64();
            break;
          case 49:
            this.bitField0_ = (0x10 | this.bitField0_);
            this.doubleValue_ = paramCodedInputStream.readDouble();
            break;
          case 58:
            this.bitField0_ = (0x20 | this.bitField0_);
            this.stringValue_ = paramCodedInputStream.readBytes();
            break;
          case 66:
            this.bitField0_ = (0x40 | this.bitField0_);
            this.aggregateValue_ = paramCodedInputStream.readBytes();
          }
        }
      }

      public Builder mergeFrom(DescriptorProtos.UninterpretedOption paramUninterpretedOption)
      {
        if (paramUninterpretedOption == DescriptorProtos.UninterpretedOption.getDefaultInstance())
          return this;
        if (this.nameBuilder_ == null)
          if (!paramUninterpretedOption.name_.isEmpty())
          {
            if (!this.name_.isEmpty())
              break label170;
            this.name_ = paramUninterpretedOption.name_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
            onChanged();
          }
        while (true)
        {
          if (paramUninterpretedOption.hasIdentifierValue())
            setIdentifierValue(paramUninterpretedOption.getIdentifierValue());
          if (paramUninterpretedOption.hasPositiveIntValue())
            setPositiveIntValue(paramUninterpretedOption.getPositiveIntValue());
          if (paramUninterpretedOption.hasNegativeIntValue())
            setNegativeIntValue(paramUninterpretedOption.getNegativeIntValue());
          if (paramUninterpretedOption.hasDoubleValue())
            setDoubleValue(paramUninterpretedOption.getDoubleValue());
          if (paramUninterpretedOption.hasStringValue())
            setStringValue(paramUninterpretedOption.getStringValue());
          if (paramUninterpretedOption.hasAggregateValue())
            setAggregateValue(paramUninterpretedOption.getAggregateValue());
          mergeUnknownFields(paramUninterpretedOption.getUnknownFields());
          return this;
          label170: ensureNameIsMutable();
          this.name_.addAll(paramUninterpretedOption.name_);
          break;
          if (!paramUninterpretedOption.name_.isEmpty())
          {
            if (this.nameBuilder_.isEmpty())
            {
              this.nameBuilder_.dispose();
              this.nameBuilder_ = null;
              this.name_ = paramUninterpretedOption.name_;
              this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
              if (GeneratedMessage.alwaysUseFieldBuilders);
              for (RepeatedFieldBuilder localRepeatedFieldBuilder = getNameFieldBuilder(); ; localRepeatedFieldBuilder = null)
              {
                this.nameBuilder_ = localRepeatedFieldBuilder;
                break;
              }
            }
            this.nameBuilder_.addAllMessages(paramUninterpretedOption.name_);
          }
        }
      }

      public Builder mergeFrom(Message paramMessage)
      {
        if ((paramMessage instanceof DescriptorProtos.UninterpretedOption))
          return mergeFrom((DescriptorProtos.UninterpretedOption)paramMessage);
        super.mergeFrom(paramMessage);
        return this;
      }

      public Builder removeName(int paramInt)
      {
        if (this.nameBuilder_ == null)
        {
          ensureNameIsMutable();
          this.name_.remove(paramInt);
          onChanged();
          return this;
        }
        this.nameBuilder_.remove(paramInt);
        return this;
      }

      public Builder setAggregateValue(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x40 | this.bitField0_);
        this.aggregateValue_ = paramString;
        onChanged();
        return this;
      }

      void setAggregateValue(ByteString paramByteString)
      {
        this.bitField0_ = (0x40 | this.bitField0_);
        this.aggregateValue_ = paramByteString;
        onChanged();
      }

      public Builder setDoubleValue(double paramDouble)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.doubleValue_ = paramDouble;
        onChanged();
        return this;
      }

      public Builder setIdentifierValue(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.identifierValue_ = paramString;
        onChanged();
        return this;
      }

      void setIdentifierValue(ByteString paramByteString)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.identifierValue_ = paramByteString;
        onChanged();
      }

      public Builder setName(int paramInt, DescriptorProtos.UninterpretedOption.NamePart.Builder paramBuilder)
      {
        if (this.nameBuilder_ == null)
        {
          ensureNameIsMutable();
          this.name_.set(paramInt, paramBuilder.build());
          onChanged();
          return this;
        }
        this.nameBuilder_.setMessage(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setName(int paramInt, DescriptorProtos.UninterpretedOption.NamePart paramNamePart)
      {
        if (this.nameBuilder_ == null)
        {
          if (paramNamePart == null)
            throw new NullPointerException();
          ensureNameIsMutable();
          this.name_.set(paramInt, paramNamePart);
          onChanged();
          return this;
        }
        this.nameBuilder_.setMessage(paramInt, paramNamePart);
        return this;
      }

      public Builder setNegativeIntValue(long paramLong)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.negativeIntValue_ = paramLong;
        onChanged();
        return this;
      }

      public Builder setPositiveIntValue(long paramLong)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.positiveIntValue_ = paramLong;
        onChanged();
        return this;
      }

      public Builder setStringValue(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x20 | this.bitField0_);
        this.stringValue_ = paramByteString;
        onChanged();
        return this;
      }
    }

    public static final class NamePart extends GeneratedMessage
      implements DescriptorProtos.UninterpretedOption.NamePartOrBuilder
    {
      public static final int IS_EXTENSION_FIELD_NUMBER = 2;
      public static final int NAME_PART_FIELD_NUMBER = 1;
      private static final NamePart defaultInstance = new NamePart(true);
      private int bitField0_;
      private boolean isExtension_;
      private byte memoizedIsInitialized = -1;
      private int memoizedSerializedSize = -1;
      private Object namePart_;

      static
      {
        defaultInstance.initFields();
      }

      private NamePart(Builder paramBuilder)
      {
        super();
      }

      private NamePart(boolean paramBoolean)
      {
      }

      public static NamePart getDefaultInstance()
      {
        return defaultInstance;
      }

      public static final Descriptors.Descriptor getDescriptor()
      {
        return DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_NamePart_descriptor;
      }

      private ByteString getNamePartBytes()
      {
        Object localObject = this.namePart_;
        if ((localObject instanceof String))
        {
          ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
          this.namePart_ = localByteString;
          return localByteString;
        }
        return (ByteString)localObject;
      }

      private void initFields()
      {
        this.namePart_ = "";
        this.isExtension_ = false;
      }

      public static Builder newBuilder()
      {
        return Builder.access$17300();
      }

      public static Builder newBuilder(NamePart paramNamePart)
      {
        return newBuilder().mergeFrom(paramNamePart);
      }

      public static NamePart parseDelimitedFrom(InputStream paramInputStream)
        throws IOException
      {
        Builder localBuilder = newBuilder();
        if (localBuilder.mergeDelimitedFrom(paramInputStream))
          return localBuilder.buildParsed();
        return null;
      }

      public static NamePart parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        Builder localBuilder = newBuilder();
        if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
          return localBuilder.buildParsed();
        return null;
      }

      public static NamePart parseFrom(ByteString paramByteString)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
      }

      public static NamePart parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
      }

      public static NamePart parseFrom(CodedInputStream paramCodedInputStream)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
      }

      public static NamePart parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
      }

      public static NamePart parseFrom(InputStream paramInputStream)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
      }

      public static NamePart parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
      }

      public static NamePart parseFrom(byte[] paramArrayOfByte)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
      }

      public static NamePart parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
      }

      public NamePart getDefaultInstanceForType()
      {
        return defaultInstance;
      }

      public boolean getIsExtension()
      {
        return this.isExtension_;
      }

      public String getNamePart()
      {
        Object localObject = this.namePart_;
        if ((localObject instanceof String))
          return (String)localObject;
        ByteString localByteString = (ByteString)localObject;
        String str = localByteString.toStringUtf8();
        if (Internal.isValidUtf8(localByteString))
          this.namePart_ = str;
        return str;
      }

      public int getSerializedSize()
      {
        int i = this.memoizedSerializedSize;
        if (i != -1)
          return i;
        int j = 0x1 & this.bitField0_;
        int k = 0;
        if (j == 1)
          k = 0 + CodedOutputStream.computeBytesSize(1, getNamePartBytes());
        if ((0x2 & this.bitField0_) == 2)
          k += CodedOutputStream.computeBoolSize(2, this.isExtension_);
        int m = k + getUnknownFields().getSerializedSize();
        this.memoizedSerializedSize = m;
        return m;
      }

      public boolean hasIsExtension()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasNamePart()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
      {
        return DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_NamePart_fieldAccessorTable;
      }

      public final boolean isInitialized()
      {
        int i = this.memoizedIsInitialized;
        if (i != -1)
          return i == 1;
        if (!hasNamePart())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
        if (!hasIsExtension())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
      }

      public Builder newBuilderForType()
      {
        return newBuilder();
      }

      protected Builder newBuilderForType(GeneratedMessage.BuilderParent paramBuilderParent)
      {
        return new Builder(paramBuilderParent, null);
      }

      public Builder toBuilder()
      {
        return newBuilder(this);
      }

      protected Object writeReplace()
        throws ObjectStreamException
      {
        return super.writeReplace();
      }

      public void writeTo(CodedOutputStream paramCodedOutputStream)
        throws IOException
      {
        getSerializedSize();
        if ((0x1 & this.bitField0_) == 1)
          paramCodedOutputStream.writeBytes(1, getNamePartBytes());
        if ((0x2 & this.bitField0_) == 2)
          paramCodedOutputStream.writeBool(2, this.isExtension_);
        getUnknownFields().writeTo(paramCodedOutputStream);
      }

      public static final class Builder extends GeneratedMessage.Builder<Builder>
        implements DescriptorProtos.UninterpretedOption.NamePartOrBuilder
      {
        private int bitField0_;
        private boolean isExtension_;
        private Object namePart_ = "";

        private Builder()
        {
          maybeForceBuilderInitialization();
        }

        private Builder(GeneratedMessage.BuilderParent paramBuilderParent)
        {
          super();
          maybeForceBuilderInitialization();
        }

        private DescriptorProtos.UninterpretedOption.NamePart buildParsed()
          throws InvalidProtocolBufferException
        {
          DescriptorProtos.UninterpretedOption.NamePart localNamePart = buildPartial();
          if (!localNamePart.isInitialized())
            throw newUninitializedMessageException(localNamePart).asInvalidProtocolBufferException();
          return localNamePart;
        }

        private static Builder create()
        {
          return new Builder();
        }

        public static final Descriptors.Descriptor getDescriptor()
        {
          return DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_NamePart_descriptor;
        }

        private void maybeForceBuilderInitialization()
        {
          if (GeneratedMessage.alwaysUseFieldBuilders);
        }

        public DescriptorProtos.UninterpretedOption.NamePart build()
        {
          DescriptorProtos.UninterpretedOption.NamePart localNamePart = buildPartial();
          if (!localNamePart.isInitialized())
            throw newUninitializedMessageException(localNamePart);
          return localNamePart;
        }

        public DescriptorProtos.UninterpretedOption.NamePart buildPartial()
        {
          DescriptorProtos.UninterpretedOption.NamePart localNamePart = new DescriptorProtos.UninterpretedOption.NamePart(this, null);
          int i = this.bitField0_;
          int j = i & 0x1;
          int k = 0;
          if (j == 1)
            k = 0x0 | 0x1;
          DescriptorProtos.UninterpretedOption.NamePart.access$17602(localNamePart, this.namePart_);
          if ((i & 0x2) == 2)
            k |= 2;
          DescriptorProtos.UninterpretedOption.NamePart.access$17702(localNamePart, this.isExtension_);
          DescriptorProtos.UninterpretedOption.NamePart.access$17802(localNamePart, k);
          onBuilt();
          return localNamePart;
        }

        public Builder clear()
        {
          super.clear();
          this.namePart_ = "";
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          this.isExtension_ = false;
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          return this;
        }

        public Builder clearIsExtension()
        {
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          this.isExtension_ = false;
          onChanged();
          return this;
        }

        public Builder clearNamePart()
        {
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          this.namePart_ = DescriptorProtos.UninterpretedOption.NamePart.getDefaultInstance().getNamePart();
          onChanged();
          return this;
        }

        public Builder clone()
        {
          return create().mergeFrom(buildPartial());
        }

        public DescriptorProtos.UninterpretedOption.NamePart getDefaultInstanceForType()
        {
          return DescriptorProtos.UninterpretedOption.NamePart.getDefaultInstance();
        }

        public Descriptors.Descriptor getDescriptorForType()
        {
          return DescriptorProtos.UninterpretedOption.NamePart.getDescriptor();
        }

        public boolean getIsExtension()
        {
          return this.isExtension_;
        }

        public String getNamePart()
        {
          Object localObject = this.namePart_;
          if (!(localObject instanceof String))
          {
            String str = ((ByteString)localObject).toStringUtf8();
            this.namePart_ = str;
            return str;
          }
          return (String)localObject;
        }

        public boolean hasIsExtension()
        {
          return (0x2 & this.bitField0_) == 2;
        }

        public boolean hasNamePart()
        {
          return (0x1 & this.bitField0_) == 1;
        }

        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable()
        {
          return DescriptorProtos.internal_static_google_protobuf_UninterpretedOption_NamePart_fieldAccessorTable;
        }

        public final boolean isInitialized()
        {
          if (!hasNamePart())
            return false;
          return hasIsExtension();
        }

        public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
          throws IOException
        {
          UnknownFieldSet.Builder localBuilder = UnknownFieldSet.newBuilder(getUnknownFields());
          while (true)
          {
            int i = paramCodedInputStream.readTag();
            switch (i)
            {
            default:
              if (!parseUnknownField(paramCodedInputStream, localBuilder, paramExtensionRegistryLite, i))
              {
                setUnknownFields(localBuilder.build());
                onChanged();
                return this;
              }
              break;
            case 0:
              setUnknownFields(localBuilder.build());
              onChanged();
              return this;
            case 10:
              this.bitField0_ = (0x1 | this.bitField0_);
              this.namePart_ = paramCodedInputStream.readBytes();
              break;
            case 16:
              this.bitField0_ = (0x2 | this.bitField0_);
              this.isExtension_ = paramCodedInputStream.readBool();
            }
          }
        }

        public Builder mergeFrom(DescriptorProtos.UninterpretedOption.NamePart paramNamePart)
        {
          if (paramNamePart == DescriptorProtos.UninterpretedOption.NamePart.getDefaultInstance())
            return this;
          if (paramNamePart.hasNamePart())
            setNamePart(paramNamePart.getNamePart());
          if (paramNamePart.hasIsExtension())
            setIsExtension(paramNamePart.getIsExtension());
          mergeUnknownFields(paramNamePart.getUnknownFields());
          return this;
        }

        public Builder mergeFrom(Message paramMessage)
        {
          if ((paramMessage instanceof DescriptorProtos.UninterpretedOption.NamePart))
            return mergeFrom((DescriptorProtos.UninterpretedOption.NamePart)paramMessage);
          super.mergeFrom(paramMessage);
          return this;
        }

        public Builder setIsExtension(boolean paramBoolean)
        {
          this.bitField0_ = (0x2 | this.bitField0_);
          this.isExtension_ = paramBoolean;
          onChanged();
          return this;
        }

        public Builder setNamePart(String paramString)
        {
          if (paramString == null)
            throw new NullPointerException();
          this.bitField0_ = (0x1 | this.bitField0_);
          this.namePart_ = paramString;
          onChanged();
          return this;
        }

        void setNamePart(ByteString paramByteString)
        {
          this.bitField0_ = (0x1 | this.bitField0_);
          this.namePart_ = paramByteString;
          onChanged();
        }
      }
    }

    public static abstract interface NamePartOrBuilder extends MessageOrBuilder
    {
      public abstract boolean getIsExtension();

      public abstract String getNamePart();

      public abstract boolean hasIsExtension();

      public abstract boolean hasNamePart();
    }
  }

  public static abstract interface UninterpretedOptionOrBuilder extends MessageOrBuilder
  {
    public abstract String getAggregateValue();

    public abstract double getDoubleValue();

    public abstract String getIdentifierValue();

    public abstract DescriptorProtos.UninterpretedOption.NamePart getName(int paramInt);

    public abstract int getNameCount();

    public abstract List<DescriptorProtos.UninterpretedOption.NamePart> getNameList();

    public abstract DescriptorProtos.UninterpretedOption.NamePartOrBuilder getNameOrBuilder(int paramInt);

    public abstract List<? extends DescriptorProtos.UninterpretedOption.NamePartOrBuilder> getNameOrBuilderList();

    public abstract long getNegativeIntValue();

    public abstract long getPositiveIntValue();

    public abstract ByteString getStringValue();

    public abstract boolean hasAggregateValue();

    public abstract boolean hasDoubleValue();

    public abstract boolean hasIdentifierValue();

    public abstract boolean hasNegativeIntValue();

    public abstract boolean hasPositiveIntValue();

    public abstract boolean hasStringValue();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.google.protobuf.DescriptorProtos
 * JD-Core Version:    0.6.2
 */