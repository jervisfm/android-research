package com.google.protobuf;

import java.io.UnsupportedEncodingException;

public class Internal
{
  public static ByteString bytesDefaultValue(String paramString)
  {
    try
    {
      ByteString localByteString = ByteString.copyFrom(paramString.getBytes("ISO-8859-1"));
      return localByteString;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new IllegalStateException("Java VM does not support a standard character set.", localUnsupportedEncodingException);
    }
  }

  public static boolean isValidUtf8(ByteString paramByteString)
  {
    int i = paramByteString.size();
    int j = 0;
    while (j < i)
    {
      int k = j + 1;
      int m = 0xFF & paramByteString.byteAt(j);
      if (m < 128)
      {
        j = k;
      }
      else
      {
        if ((m < 194) || (m > 244))
          return false;
        if (k >= i)
          return false;
        j = k + 1;
        int n = 0xFF & paramByteString.byteAt(k);
        if ((n < 128) || (n > 191))
          return false;
        if (m > 223)
        {
          if (j >= i)
            return false;
          int i1 = j + 1;
          int i2 = 0xFF & paramByteString.byteAt(j);
          if ((i2 < 128) || (i2 > 191))
            return false;
          if (m <= 239)
          {
            if (((m == 224) && (n < 160)) || ((m == 237) && (n > 159)))
              return false;
          }
          else
          {
            if (i1 >= i)
              return false;
            int i3 = i1 + 1;
            int i4 = 0xFF & paramByteString.byteAt(i1);
            if ((i4 < 128) || (i4 > 191))
              return false;
            if (((m == 240) && (n < 144)) || ((m == 244) && (n > 143)))
              return false;
            i1 = i3;
          }
          j = i1;
        }
      }
    }
    return true;
  }

  public static String stringDefaultValue(String paramString)
  {
    try
    {
      String str = new String(paramString.getBytes("ISO-8859-1"), "UTF-8");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new IllegalStateException("Java VM does not support a standard character set.", localUnsupportedEncodingException);
    }
  }

  public static abstract interface EnumLite
  {
    public abstract int getNumber();
  }

  public static abstract interface EnumLiteMap<T extends Internal.EnumLite>
  {
    public abstract T findValueByNumber(int paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.google.protobuf.Internal
 * JD-Core Version:    0.6.2
 */