package com.google.protobuf;

import java.util.Map;

public abstract interface MessageOrBuilder extends MessageLiteOrBuilder
{
  public abstract Map<Descriptors.FieldDescriptor, Object> getAllFields();

  public abstract Message getDefaultInstanceForType();

  public abstract Descriptors.Descriptor getDescriptorForType();

  public abstract Object getField(Descriptors.FieldDescriptor paramFieldDescriptor);

  public abstract Object getRepeatedField(Descriptors.FieldDescriptor paramFieldDescriptor, int paramInt);

  public abstract int getRepeatedFieldCount(Descriptors.FieldDescriptor paramFieldDescriptor);

  public abstract UnknownFieldSet getUnknownFields();

  public abstract boolean hasField(Descriptors.FieldDescriptor paramFieldDescriptor);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.google.protobuf.MessageOrBuilder
 * JD-Core Version:    0.6.2
 */