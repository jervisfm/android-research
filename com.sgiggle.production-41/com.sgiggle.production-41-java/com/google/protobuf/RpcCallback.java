package com.google.protobuf;

public abstract interface RpcCallback<ParameterType>
{
  public abstract void run(ParameterType paramParameterType);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.google.protobuf.RpcCallback
 * JD-Core Version:    0.6.2
 */