package com.google.protobuf;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class TextFormat
{
  private static final int BUFFER_SIZE = 4096;
  private static final Printer DEFAULT_PRINTER = new Printer(false, null);
  private static final Printer SINGLE_LINE_PRINTER = new Printer(true, null);

  private static int digitValue(byte paramByte)
  {
    if ((48 <= paramByte) && (paramByte <= 57))
      return paramByte - 48;
    if ((97 <= paramByte) && (paramByte <= 122))
      return 10 + (paramByte - 97);
    return 10 + (paramByte - 65);
  }

  static String escapeBytes(ByteString paramByteString)
  {
    StringBuilder localStringBuilder = new StringBuilder(paramByteString.size());
    int i = 0;
    if (i < paramByteString.size())
    {
      int j = paramByteString.byteAt(i);
      switch (j)
      {
      default:
        if (j >= 32)
          localStringBuilder.append((char)j);
        break;
      case 7:
      case 8:
      case 12:
      case 10:
      case 13:
      case 9:
      case 11:
      case 92:
      case 39:
      case 34:
      }
      while (true)
      {
        i++;
        break;
        localStringBuilder.append("\\a");
        continue;
        localStringBuilder.append("\\b");
        continue;
        localStringBuilder.append("\\f");
        continue;
        localStringBuilder.append("\\n");
        continue;
        localStringBuilder.append("\\r");
        continue;
        localStringBuilder.append("\\t");
        continue;
        localStringBuilder.append("\\v");
        continue;
        localStringBuilder.append("\\\\");
        continue;
        localStringBuilder.append("\\'");
        continue;
        localStringBuilder.append("\\\"");
        continue;
        localStringBuilder.append('\\');
        localStringBuilder.append((char)(48 + (0x3 & j >>> 6)));
        localStringBuilder.append((char)(48 + (0x7 & j >>> 3)));
        localStringBuilder.append((char)(48 + (j & 0x7)));
      }
    }
    return localStringBuilder.toString();
  }

  static String escapeText(String paramString)
  {
    return escapeBytes(ByteString.copyFromUtf8(paramString));
  }

  private static boolean isHex(byte paramByte)
  {
    return ((48 <= paramByte) && (paramByte <= 57)) || ((97 <= paramByte) && (paramByte <= 102)) || ((65 <= paramByte) && (paramByte <= 70));
  }

  private static boolean isOctal(byte paramByte)
  {
    return (48 <= paramByte) && (paramByte <= 55);
  }

  public static void merge(CharSequence paramCharSequence, ExtensionRegistry paramExtensionRegistry, Message.Builder paramBuilder)
    throws TextFormat.ParseException
  {
    Tokenizer localTokenizer = new Tokenizer(paramCharSequence, null);
    while (!localTokenizer.atEnd())
      mergeField(localTokenizer, paramExtensionRegistry, paramBuilder);
  }

  public static void merge(CharSequence paramCharSequence, Message.Builder paramBuilder)
    throws TextFormat.ParseException
  {
    merge(paramCharSequence, ExtensionRegistry.getEmptyRegistry(), paramBuilder);
  }

  public static void merge(Readable paramReadable, ExtensionRegistry paramExtensionRegistry, Message.Builder paramBuilder)
    throws IOException
  {
    merge(toStringBuilder(paramReadable), paramExtensionRegistry, paramBuilder);
  }

  public static void merge(Readable paramReadable, Message.Builder paramBuilder)
    throws IOException
  {
    merge(paramReadable, ExtensionRegistry.getEmptyRegistry(), paramBuilder);
  }

  private static void mergeField(Tokenizer paramTokenizer, ExtensionRegistry paramExtensionRegistry, Message.Builder paramBuilder)
    throws TextFormat.ParseException
  {
    Descriptors.Descriptor localDescriptor = paramBuilder.getDescriptorForType();
    ExtensionRegistry.ExtensionInfo localExtensionInfo;
    Descriptors.FieldDescriptor localFieldDescriptor;
    String str2;
    label203: Message.Builder localBuilder;
    if (paramTokenizer.tryConsume("["))
    {
      StringBuilder localStringBuilder = new StringBuilder(paramTokenizer.consumeIdentifier());
      while (paramTokenizer.tryConsume("."))
      {
        localStringBuilder.append('.');
        localStringBuilder.append(paramTokenizer.consumeIdentifier());
      }
      localExtensionInfo = paramExtensionRegistry.findExtensionByName(localStringBuilder.toString());
      if (localExtensionInfo == null)
        throw paramTokenizer.parseExceptionPreviousToken("Extension \"" + localStringBuilder + "\" not found in the ExtensionRegistry.");
      if (localExtensionInfo.descriptor.getContainingType() != localDescriptor)
        throw paramTokenizer.parseExceptionPreviousToken("Extension \"" + localStringBuilder + "\" does not extend message type \"" + localDescriptor.getFullName() + "\".");
      paramTokenizer.consume("]");
      localFieldDescriptor = localExtensionInfo.descriptor;
      if (localFieldDescriptor.getJavaType() != Descriptors.FieldDescriptor.JavaType.MESSAGE)
        break label472;
      paramTokenizer.tryConsume(":");
      if (!paramTokenizer.tryConsume("<"))
        break label403;
      str2 = ">";
      if (localExtensionInfo != null)
        break label418;
      localBuilder = paramBuilder.newBuilderForField(localFieldDescriptor);
    }
    while (true)
    {
      if (paramTokenizer.tryConsume(str2))
        break label443;
      if (paramTokenizer.atEnd())
      {
        throw paramTokenizer.parseException("Expected \"" + str2 + "\".");
        String str3 = paramTokenizer.consumeIdentifier();
        localFieldDescriptor = localDescriptor.findFieldByName(str3);
        if (localFieldDescriptor == null)
        {
          localFieldDescriptor = localDescriptor.findFieldByName(str3.toLowerCase(Locale.US));
          if ((localFieldDescriptor != null) && (localFieldDescriptor.getType() != Descriptors.FieldDescriptor.Type.GROUP))
            localFieldDescriptor = null;
        }
        if ((localFieldDescriptor != null) && (localFieldDescriptor.getType() == Descriptors.FieldDescriptor.Type.GROUP) && (!localFieldDescriptor.getMessageType().getName().equals(str3)))
          localFieldDescriptor = null;
        localExtensionInfo = null;
        if (localFieldDescriptor != null)
          break;
        throw paramTokenizer.parseExceptionPreviousToken("Message type \"" + localDescriptor.getFullName() + "\" has no field named \"" + str3 + "\".");
        label403: paramTokenizer.consume("{");
        str2 = "}";
        break label203;
        label418: localBuilder = localExtensionInfo.defaultInstance.newBuilderForType();
        continue;
      }
      mergeField(paramTokenizer, paramExtensionRegistry, localBuilder);
    }
    label443: Object localObject = localBuilder.build();
    while (localFieldDescriptor.isRepeated())
    {
      paramBuilder.addRepeatedField(localFieldDescriptor, localObject);
      return;
      label472: paramTokenizer.consume(":");
      switch (1.$SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[localFieldDescriptor.getType().ordinal()])
      {
      default:
        localObject = null;
        break;
      case 1:
      case 2:
      case 3:
        localObject = Integer.valueOf(paramTokenizer.consumeInt32());
        break;
      case 4:
      case 5:
      case 6:
        localObject = Long.valueOf(paramTokenizer.consumeInt64());
        break;
      case 10:
      case 11:
        localObject = Integer.valueOf(paramTokenizer.consumeUInt32());
        break;
      case 12:
      case 13:
        localObject = Long.valueOf(paramTokenizer.consumeUInt64());
        break;
      case 8:
        localObject = Float.valueOf(paramTokenizer.consumeFloat());
        break;
      case 9:
        localObject = Double.valueOf(paramTokenizer.consumeDouble());
        break;
      case 7:
        localObject = Boolean.valueOf(paramTokenizer.consumeBoolean());
        break;
      case 14:
        localObject = paramTokenizer.consumeString();
        break;
      case 15:
        localObject = paramTokenizer.consumeByteString();
        break;
      case 16:
        Descriptors.EnumDescriptor localEnumDescriptor = localFieldDescriptor.getEnumType();
        if (paramTokenizer.lookingAtInteger())
        {
          int i = paramTokenizer.consumeInt32();
          localObject = localEnumDescriptor.findValueByNumber(i);
          if (localObject == null)
            throw paramTokenizer.parseExceptionPreviousToken("Enum type \"" + localEnumDescriptor.getFullName() + "\" has no value with number " + i + '.');
        }
        else
        {
          String str1 = paramTokenizer.consumeIdentifier();
          localObject = localEnumDescriptor.findValueByName(str1);
          if (localObject == null)
            throw paramTokenizer.parseExceptionPreviousToken("Enum type \"" + localEnumDescriptor.getFullName() + "\" has no value named \"" + str1 + "\".");
        }
        break;
      case 17:
      case 18:
        throw new RuntimeException("Can't get here.");
      }
    }
    paramBuilder.setField(localFieldDescriptor, localObject);
  }

  static int parseInt32(String paramString)
    throws NumberFormatException
  {
    return (int)parseInteger(paramString, true, false);
  }

  static long parseInt64(String paramString)
    throws NumberFormatException
  {
    return parseInteger(paramString, true, true);
  }

  private static long parseInteger(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    throws NumberFormatException
  {
    boolean bool = paramString.startsWith("-", 0);
    int i = 0;
    int j = 0;
    if (bool)
    {
      if (!paramBoolean1)
        throw new NumberFormatException("Number must be positive: " + paramString);
      j = 0 + 1;
      i = 1;
    }
    int k = 10;
    if (paramString.startsWith("0x", j))
    {
      j += 2;
      k = 16;
    }
    String str;
    long l;
    while (true)
    {
      str = paramString.substring(j);
      if (str.length() >= 16)
        break label236;
      l = Long.parseLong(str, k);
      if (i != 0)
        l = -l;
      if (paramBoolean2)
        break label432;
      if (!paramBoolean1)
        break;
      if ((l <= 2147483647L) && (l >= -2147483648L))
        break label432;
      throw new NumberFormatException("Number out of range for 32-bit signed integer: " + paramString);
      if (paramString.startsWith("0", j))
        k = 8;
    }
    if ((l >= 4294967296L) || (l < 0L))
    {
      throw new NumberFormatException("Number out of range for 32-bit unsigned integer: " + paramString);
      label236: BigInteger localBigInteger = new BigInteger(str, k);
      if (i != 0)
        localBigInteger = localBigInteger.negate();
      if (!paramBoolean2)
      {
        if (paramBoolean1)
        {
          if (localBigInteger.bitLength() > 31)
            throw new NumberFormatException("Number out of range for 32-bit signed integer: " + paramString);
        }
        else if (localBigInteger.bitLength() > 32)
          throw new NumberFormatException("Number out of range for 32-bit unsigned integer: " + paramString);
      }
      else if (paramBoolean1)
      {
        if (localBigInteger.bitLength() > 63)
          throw new NumberFormatException("Number out of range for 64-bit signed integer: " + paramString);
      }
      else if (localBigInteger.bitLength() > 64)
        throw new NumberFormatException("Number out of range for 64-bit unsigned integer: " + paramString);
      l = localBigInteger.longValue();
    }
    label432: return l;
  }

  static int parseUInt32(String paramString)
    throws NumberFormatException
  {
    return (int)parseInteger(paramString, false, false);
  }

  static long parseUInt64(String paramString)
    throws NumberFormatException
  {
    return parseInteger(paramString, false, true);
  }

  public static void print(Message paramMessage, Appendable paramAppendable)
    throws IOException
  {
    DEFAULT_PRINTER.print(paramMessage, new TextGenerator(paramAppendable, null));
  }

  public static void print(UnknownFieldSet paramUnknownFieldSet, Appendable paramAppendable)
    throws IOException
  {
    DEFAULT_PRINTER.printUnknownFields(paramUnknownFieldSet, new TextGenerator(paramAppendable, null));
  }

  public static void printField(Descriptors.FieldDescriptor paramFieldDescriptor, Object paramObject, Appendable paramAppendable)
    throws IOException
  {
    DEFAULT_PRINTER.printField(paramFieldDescriptor, paramObject, new TextGenerator(paramAppendable, null));
  }

  public static String printFieldToString(Descriptors.FieldDescriptor paramFieldDescriptor, Object paramObject)
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      printField(paramFieldDescriptor, paramObject, localStringBuilder);
      String str = localStringBuilder.toString();
      return str;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException(localIOException);
    }
  }

  public static void printFieldValue(Descriptors.FieldDescriptor paramFieldDescriptor, Object paramObject, Appendable paramAppendable)
    throws IOException
  {
    DEFAULT_PRINTER.printFieldValue(paramFieldDescriptor, paramObject, new TextGenerator(paramAppendable, null));
  }

  public static String printToString(Message paramMessage)
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      print(paramMessage, localStringBuilder);
      String str = localStringBuilder.toString();
      return str;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException(localIOException);
    }
  }

  public static String printToString(UnknownFieldSet paramUnknownFieldSet)
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      print(paramUnknownFieldSet, localStringBuilder);
      String str = localStringBuilder.toString();
      return str;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException(localIOException);
    }
  }

  private static void printUnknownFieldValue(int paramInt, Object paramObject, TextGenerator paramTextGenerator)
    throws IOException
  {
    switch (WireFormat.getTagWireType(paramInt))
    {
    case 4:
    default:
      throw new IllegalArgumentException("Bad tag: " + paramInt);
    case 0:
      paramTextGenerator.print(unsignedToString(((Long)paramObject).longValue()));
      return;
    case 5:
      Locale localLocale2 = (Locale)null;
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = ((Integer)paramObject);
      paramTextGenerator.print(String.format(localLocale2, "0x%08x", arrayOfObject2));
      return;
    case 1:
      Locale localLocale1 = (Locale)null;
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = ((Long)paramObject);
      paramTextGenerator.print(String.format(localLocale1, "0x%016x", arrayOfObject1));
      return;
    case 2:
      paramTextGenerator.print("\"");
      paramTextGenerator.print(escapeBytes((ByteString)paramObject));
      paramTextGenerator.print("\"");
      return;
    case 3:
    }
    DEFAULT_PRINTER.printUnknownFields((UnknownFieldSet)paramObject, paramTextGenerator);
  }

  public static void printUnknownFieldValue(int paramInt, Object paramObject, Appendable paramAppendable)
    throws IOException
  {
    printUnknownFieldValue(paramInt, paramObject, new TextGenerator(paramAppendable, null));
  }

  public static String shortDebugString(Message paramMessage)
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      SINGLE_LINE_PRINTER.print(paramMessage, new TextGenerator(localStringBuilder, null));
      String str = localStringBuilder.toString().trim();
      return str;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException(localIOException);
    }
  }

  public static String shortDebugString(UnknownFieldSet paramUnknownFieldSet)
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      SINGLE_LINE_PRINTER.printUnknownFields(paramUnknownFieldSet, new TextGenerator(localStringBuilder, null));
      String str = localStringBuilder.toString().trim();
      return str;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException(localIOException);
    }
  }

  private static StringBuilder toStringBuilder(Readable paramReadable)
    throws IOException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    CharBuffer localCharBuffer = CharBuffer.allocate(4096);
    while (true)
    {
      int i = paramReadable.read(localCharBuffer);
      if (i == -1)
        return localStringBuilder;
      localCharBuffer.flip();
      localStringBuilder.append(localCharBuffer, 0, i);
    }
  }

  static ByteString unescapeBytes(CharSequence paramCharSequence)
    throws TextFormat.InvalidEscapeSequenceException
  {
    ByteString localByteString = ByteString.copyFromUtf8(paramCharSequence.toString());
    byte[] arrayOfByte = new byte[localByteString.size()];
    int i = 0;
    int j = 0;
    if (j < localByteString.size())
    {
      int k = localByteString.byteAt(j);
      byte b;
      if (k == 92)
        if (j + 1 < localByteString.size())
        {
          j++;
          b = localByteString.byteAt(j);
          if (isOctal(b))
          {
            int i12 = digitValue(b);
            if ((j + 1 < localByteString.size()) && (isOctal(localByteString.byteAt(j + 1))))
            {
              j++;
              i12 = i12 * 8 + digitValue(localByteString.byteAt(j));
            }
            if ((j + 1 < localByteString.size()) && (isOctal(localByteString.byteAt(j + 1))))
            {
              j++;
              i12 = i12 * 8 + digitValue(localByteString.byteAt(j));
            }
            int i13 = i + 1;
            arrayOfByte[i] = ((byte)i12);
            i = i13;
          }
        }
      while (true)
      {
        j++;
        break;
        switch (b)
        {
        default:
          throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\" + (char)b + '\'');
        case 97:
          int i11 = i + 1;
          arrayOfByte[i] = 7;
          i = i11;
          break;
        case 98:
          int i10 = i + 1;
          arrayOfByte[i] = 8;
          i = i10;
          break;
        case 102:
          int i9 = i + 1;
          arrayOfByte[i] = 12;
          i = i9;
          break;
        case 110:
          int i8 = i + 1;
          arrayOfByte[i] = 10;
          i = i8;
          break;
        case 114:
          int i7 = i + 1;
          arrayOfByte[i] = 13;
          i = i7;
          break;
        case 116:
          int i6 = i + 1;
          arrayOfByte[i] = 9;
          i = i6;
          break;
        case 118:
          int i5 = i + 1;
          arrayOfByte[i] = 11;
          i = i5;
          break;
        case 92:
          int i4 = i + 1;
          arrayOfByte[i] = 92;
          i = i4;
          break;
        case 39:
          int i3 = i + 1;
          arrayOfByte[i] = 39;
          i = i3;
          break;
        case 34:
          int i2 = i + 1;
          arrayOfByte[i] = 34;
          i = i2;
          break;
        case 120:
          if ((j + 1 < localByteString.size()) && (isHex(localByteString.byteAt(j + 1))))
          {
            j++;
            int n = digitValue(localByteString.byteAt(j));
            if ((j + 1 < localByteString.size()) && (isHex(localByteString.byteAt(j + 1))))
            {
              j++;
              n = n * 16 + digitValue(localByteString.byteAt(j));
            }
            int i1 = i + 1;
            arrayOfByte[i] = ((byte)n);
            i = i1;
          }
          else
          {
            throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\x' with no digits");
            throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\' at end of string.");
            int m = i + 1;
            arrayOfByte[i] = k;
            i = m;
          }
          break;
        }
      }
    }
    return ByteString.copyFrom(arrayOfByte, 0, i);
  }

  static String unescapeText(String paramString)
    throws TextFormat.InvalidEscapeSequenceException
  {
    return unescapeBytes(paramString).toStringUtf8();
  }

  private static String unsignedToString(int paramInt)
  {
    if (paramInt >= 0)
      return Integer.toString(paramInt);
    return Long.toString(0xFFFFFFFF & paramInt);
  }

  private static String unsignedToString(long paramLong)
  {
    if (paramLong >= 0L)
      return Long.toString(paramLong);
    return BigInteger.valueOf(0xFFFFFFFF & paramLong).setBit(63).toString();
  }

  static class InvalidEscapeSequenceException extends IOException
  {
    private static final long serialVersionUID = -8164033650142593304L;

    InvalidEscapeSequenceException(String paramString)
    {
      super();
    }
  }

  public static class ParseException extends IOException
  {
    private static final long serialVersionUID = 3196188060225107702L;

    public ParseException(String paramString)
    {
      super();
    }
  }

  private static final class Printer
  {
    final boolean singleLineMode;

    private Printer(boolean paramBoolean)
    {
      this.singleLineMode = paramBoolean;
    }

    private void print(Message paramMessage, TextFormat.TextGenerator paramTextGenerator)
      throws IOException
    {
      Iterator localIterator = paramMessage.getAllFields().entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        printField((Descriptors.FieldDescriptor)localEntry.getKey(), localEntry.getValue(), paramTextGenerator);
      }
      printUnknownFields(paramMessage.getUnknownFields(), paramTextGenerator);
    }

    private void printField(Descriptors.FieldDescriptor paramFieldDescriptor, Object paramObject, TextFormat.TextGenerator paramTextGenerator)
      throws IOException
    {
      if (paramFieldDescriptor.isRepeated())
      {
        Iterator localIterator = ((List)paramObject).iterator();
        while (localIterator.hasNext())
          printSingleField(paramFieldDescriptor, localIterator.next(), paramTextGenerator);
      }
      printSingleField(paramFieldDescriptor, paramObject, paramTextGenerator);
    }

    private void printFieldValue(Descriptors.FieldDescriptor paramFieldDescriptor, Object paramObject, TextFormat.TextGenerator paramTextGenerator)
      throws IOException
    {
      switch (TextFormat.1.$SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[paramFieldDescriptor.getType().ordinal()])
      {
      default:
        return;
      case 1:
      case 2:
      case 3:
        paramTextGenerator.print(((Integer)paramObject).toString());
        return;
      case 4:
      case 5:
      case 6:
        paramTextGenerator.print(((Long)paramObject).toString());
        return;
      case 7:
        paramTextGenerator.print(((Boolean)paramObject).toString());
        return;
      case 8:
        paramTextGenerator.print(((Float)paramObject).toString());
        return;
      case 9:
        paramTextGenerator.print(((Double)paramObject).toString());
        return;
      case 10:
      case 11:
        paramTextGenerator.print(TextFormat.unsignedToString(((Integer)paramObject).intValue()));
        return;
      case 12:
      case 13:
        paramTextGenerator.print(TextFormat.unsignedToString(((Long)paramObject).longValue()));
        return;
      case 14:
        paramTextGenerator.print("\"");
        paramTextGenerator.print(TextFormat.escapeText((String)paramObject));
        paramTextGenerator.print("\"");
        return;
      case 15:
        paramTextGenerator.print("\"");
        paramTextGenerator.print(TextFormat.escapeBytes((ByteString)paramObject));
        paramTextGenerator.print("\"");
        return;
      case 16:
        paramTextGenerator.print(((Descriptors.EnumValueDescriptor)paramObject).getName());
        return;
      case 17:
      case 18:
      }
      print((Message)paramObject, paramTextGenerator);
    }

    private void printSingleField(Descriptors.FieldDescriptor paramFieldDescriptor, Object paramObject, TextFormat.TextGenerator paramTextGenerator)
      throws IOException
    {
      if (paramFieldDescriptor.isExtension())
      {
        paramTextGenerator.print("[");
        if ((paramFieldDescriptor.getContainingType().getOptions().getMessageSetWireFormat()) && (paramFieldDescriptor.getType() == Descriptors.FieldDescriptor.Type.MESSAGE) && (paramFieldDescriptor.isOptional()) && (paramFieldDescriptor.getExtensionScope() == paramFieldDescriptor.getMessageType()))
        {
          paramTextGenerator.print(paramFieldDescriptor.getMessageType().getFullName());
          paramTextGenerator.print("]");
          label71: if (paramFieldDescriptor.getJavaType() != Descriptors.FieldDescriptor.JavaType.MESSAGE)
            break label184;
          if (!this.singleLineMode)
            break label171;
          paramTextGenerator.print(" { ");
        }
      }
      while (true)
      {
        printFieldValue(paramFieldDescriptor, paramObject, paramTextGenerator);
        if (paramFieldDescriptor.getJavaType() != Descriptors.FieldDescriptor.JavaType.MESSAGE)
          break label204;
        if (!this.singleLineMode)
          break label193;
        paramTextGenerator.print("} ");
        return;
        paramTextGenerator.print(paramFieldDescriptor.getFullName());
        break;
        if (paramFieldDescriptor.getType() == Descriptors.FieldDescriptor.Type.GROUP)
        {
          paramTextGenerator.print(paramFieldDescriptor.getMessageType().getName());
          break label71;
        }
        paramTextGenerator.print(paramFieldDescriptor.getName());
        break label71;
        label171: paramTextGenerator.print(" {\n");
        paramTextGenerator.indent();
        continue;
        label184: paramTextGenerator.print(": ");
      }
      label193: paramTextGenerator.outdent();
      paramTextGenerator.print("}\n");
      return;
      label204: if (this.singleLineMode)
      {
        paramTextGenerator.print(" ");
        return;
      }
      paramTextGenerator.print("\n");
    }

    private void printUnknownField(int paramInt1, int paramInt2, List<?> paramList, TextFormat.TextGenerator paramTextGenerator)
      throws IOException
    {
      Iterator localIterator = paramList.iterator();
      if (localIterator.hasNext())
      {
        Object localObject = localIterator.next();
        paramTextGenerator.print(String.valueOf(paramInt1));
        paramTextGenerator.print(": ");
        TextFormat.printUnknownFieldValue(paramInt2, localObject, paramTextGenerator);
        if (this.singleLineMode);
        for (String str = " "; ; str = "\n")
        {
          paramTextGenerator.print(str);
          break;
        }
      }
    }

    private void printUnknownFields(UnknownFieldSet paramUnknownFieldSet, TextFormat.TextGenerator paramTextGenerator)
      throws IOException
    {
      Iterator localIterator1 = paramUnknownFieldSet.asMap().entrySet().iterator();
      while (localIterator1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator1.next();
        int i = ((Integer)localEntry.getKey()).intValue();
        UnknownFieldSet.Field localField = (UnknownFieldSet.Field)localEntry.getValue();
        printUnknownField(i, 0, localField.getVarintList(), paramTextGenerator);
        printUnknownField(i, 5, localField.getFixed32List(), paramTextGenerator);
        printUnknownField(i, 1, localField.getFixed64List(), paramTextGenerator);
        printUnknownField(i, 2, localField.getLengthDelimitedList(), paramTextGenerator);
        Iterator localIterator2 = localField.getGroupList().iterator();
        while (localIterator2.hasNext())
        {
          UnknownFieldSet localUnknownFieldSet = (UnknownFieldSet)localIterator2.next();
          paramTextGenerator.print(((Integer)localEntry.getKey()).toString());
          if (this.singleLineMode)
            paramTextGenerator.print(" { ");
          while (true)
          {
            printUnknownFields(localUnknownFieldSet, paramTextGenerator);
            if (!this.singleLineMode)
              break label214;
            paramTextGenerator.print("} ");
            break;
            paramTextGenerator.print(" {\n");
            paramTextGenerator.indent();
          }
          label214: paramTextGenerator.outdent();
          paramTextGenerator.print("}\n");
        }
      }
    }
  }

  private static final class TextGenerator
  {
    private boolean atStartOfLine = true;
    private final StringBuilder indent = new StringBuilder();
    private final Appendable output;

    private TextGenerator(Appendable paramAppendable)
    {
      this.output = paramAppendable;
    }

    private void write(CharSequence paramCharSequence, int paramInt)
      throws IOException
    {
      if (paramInt == 0)
        return;
      if (this.atStartOfLine)
      {
        this.atStartOfLine = false;
        this.output.append(this.indent);
      }
      this.output.append(paramCharSequence);
    }

    public void indent()
    {
      this.indent.append("  ");
    }

    public void outdent()
    {
      int i = this.indent.length();
      if (i == 0)
        throw new IllegalArgumentException(" Outdent() without matching Indent().");
      this.indent.delete(i - 2, i);
    }

    public void print(CharSequence paramCharSequence)
      throws IOException
    {
      int i = paramCharSequence.length();
      int j = 0;
      for (int k = 0; k < i; k++)
        if (paramCharSequence.charAt(k) == '\n')
        {
          write(paramCharSequence.subSequence(j, i), 1 + (k - j));
          j = k + 1;
          this.atStartOfLine = true;
        }
      write(paramCharSequence.subSequence(j, i), i - j);
    }
  }

  private static final class Tokenizer
  {
    private static final Pattern DOUBLE_INFINITY = Pattern.compile("-?inf(inity)?", 2);
    private static final Pattern FLOAT_INFINITY = Pattern.compile("-?inf(inity)?f?", 2);
    private static final Pattern FLOAT_NAN = Pattern.compile("nanf?", 2);
    private static final Pattern TOKEN;
    private static final Pattern WHITESPACE = Pattern.compile("(\\s|(#.*$))++", 8);
    private int column = 0;
    private String currentToken;
    private int line = 0;
    private final Matcher matcher;
    private int pos = 0;
    private int previousColumn = 0;
    private int previousLine = 0;
    private final CharSequence text;

    static
    {
      TOKEN = Pattern.compile("[a-zA-Z_][0-9a-zA-Z_+-]*+|[.]?[0-9+-][0-9a-zA-Z_.+-]*+|\"([^\"\n\\\\]|\\\\.)*+(\"|\\\\?$)|'([^'\n\\\\]|\\\\.)*+('|\\\\?$)", 8);
    }

    private Tokenizer(CharSequence paramCharSequence)
    {
      this.text = paramCharSequence;
      this.matcher = WHITESPACE.matcher(paramCharSequence);
      skipWhitespace();
      nextToken();
    }

    private void consumeByteString(List<ByteString> paramList)
      throws TextFormat.ParseException
    {
      if (this.currentToken.length() > 0);
      for (int i = this.currentToken.charAt(0); (i != 34) && (i != 39); i = 0)
        throw parseException("Expected string.");
      if ((this.currentToken.length() < 2) || (this.currentToken.charAt(this.currentToken.length() - 1) != i))
        throw parseException("String missing ending quote.");
      try
      {
        ByteString localByteString = TextFormat.unescapeBytes(this.currentToken.substring(1, this.currentToken.length() - 1));
        nextToken();
        paramList.add(localByteString);
        return;
      }
      catch (TextFormat.InvalidEscapeSequenceException localInvalidEscapeSequenceException)
      {
        throw parseException(localInvalidEscapeSequenceException.getMessage());
      }
    }

    private TextFormat.ParseException floatParseException(NumberFormatException paramNumberFormatException)
    {
      return parseException("Couldn't parse number: " + paramNumberFormatException.getMessage());
    }

    private TextFormat.ParseException integerParseException(NumberFormatException paramNumberFormatException)
    {
      return parseException("Couldn't parse integer: " + paramNumberFormatException.getMessage());
    }

    private void skipWhitespace()
    {
      this.matcher.usePattern(WHITESPACE);
      if (this.matcher.lookingAt())
        this.matcher.region(this.matcher.end(), this.matcher.regionEnd());
    }

    public boolean atEnd()
    {
      return this.currentToken.length() == 0;
    }

    public void consume(String paramString)
      throws TextFormat.ParseException
    {
      if (!tryConsume(paramString))
        throw parseException("Expected \"" + paramString + "\".");
    }

    public boolean consumeBoolean()
      throws TextFormat.ParseException
    {
      if ((this.currentToken.equals("true")) || (this.currentToken.equals("t")) || (this.currentToken.equals("1")))
      {
        nextToken();
        return true;
      }
      if ((this.currentToken.equals("false")) || (this.currentToken.equals("f")) || (this.currentToken.equals("0")))
      {
        nextToken();
        return false;
      }
      throw parseException("Expected \"true\" or \"false\".");
    }

    public ByteString consumeByteString()
      throws TextFormat.ParseException
    {
      ArrayList localArrayList = new ArrayList();
      consumeByteString(localArrayList);
      while ((this.currentToken.startsWith("'")) || (this.currentToken.startsWith("\"")))
        consumeByteString(localArrayList);
      return ByteString.copyFrom(localArrayList);
    }

    public double consumeDouble()
      throws TextFormat.ParseException
    {
      if (DOUBLE_INFINITY.matcher(this.currentToken).matches())
      {
        boolean bool = this.currentToken.startsWith("-");
        nextToken();
        if (bool)
          return (-1.0D / 0.0D);
        return (1.0D / 0.0D);
      }
      if (this.currentToken.equalsIgnoreCase("nan"))
      {
        nextToken();
        return (0.0D / 0.0D);
      }
      try
      {
        double d = Double.parseDouble(this.currentToken);
        nextToken();
        return d;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw floatParseException(localNumberFormatException);
      }
    }

    public float consumeFloat()
      throws TextFormat.ParseException
    {
      if (FLOAT_INFINITY.matcher(this.currentToken).matches())
      {
        boolean bool = this.currentToken.startsWith("-");
        nextToken();
        if (bool)
          return (1.0F / -1.0F);
        return (1.0F / 1.0F);
      }
      if (FLOAT_NAN.matcher(this.currentToken).matches())
      {
        nextToken();
        return (0.0F / 0.0F);
      }
      try
      {
        float f = Float.parseFloat(this.currentToken);
        nextToken();
        return f;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw floatParseException(localNumberFormatException);
      }
    }

    public String consumeIdentifier()
      throws TextFormat.ParseException
    {
      int i = 0;
      while (i < this.currentToken.length())
      {
        int j = this.currentToken.charAt(i);
        if (((97 <= j) && (j <= 122)) || ((65 <= j) && (j <= 90)) || ((48 <= j) && (j <= 57)) || (j == 95) || (j == 46))
          i++;
        else
          throw parseException("Expected identifier.");
      }
      String str = this.currentToken;
      nextToken();
      return str;
    }

    public int consumeInt32()
      throws TextFormat.ParseException
    {
      try
      {
        int i = TextFormat.parseInt32(this.currentToken);
        nextToken();
        return i;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw integerParseException(localNumberFormatException);
      }
    }

    public long consumeInt64()
      throws TextFormat.ParseException
    {
      try
      {
        long l = TextFormat.parseInt64(this.currentToken);
        nextToken();
        return l;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw integerParseException(localNumberFormatException);
      }
    }

    public String consumeString()
      throws TextFormat.ParseException
    {
      return consumeByteString().toStringUtf8();
    }

    public int consumeUInt32()
      throws TextFormat.ParseException
    {
      try
      {
        int i = TextFormat.parseUInt32(this.currentToken);
        nextToken();
        return i;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw integerParseException(localNumberFormatException);
      }
    }

    public long consumeUInt64()
      throws TextFormat.ParseException
    {
      try
      {
        long l = TextFormat.parseUInt64(this.currentToken);
        nextToken();
        return l;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw integerParseException(localNumberFormatException);
      }
    }

    public boolean lookingAtInteger()
    {
      if (this.currentToken.length() == 0)
        return false;
      int i = this.currentToken.charAt(0);
      return ((48 <= i) && (i <= 57)) || (i == 45) || (i == 43);
    }

    public void nextToken()
    {
      this.previousLine = this.line;
      this.previousColumn = this.column;
      if (this.pos < this.matcher.regionStart())
      {
        if (this.text.charAt(this.pos) == '\n')
          this.line = (1 + this.line);
        for (this.column = 0; ; this.column = (1 + this.column))
        {
          this.pos = (1 + this.pos);
          break;
        }
      }
      if (this.matcher.regionStart() == this.matcher.regionEnd())
      {
        this.currentToken = "";
        return;
      }
      this.matcher.usePattern(TOKEN);
      if (this.matcher.lookingAt())
      {
        this.currentToken = this.matcher.group();
        this.matcher.region(this.matcher.end(), this.matcher.regionEnd());
      }
      while (true)
      {
        skipWhitespace();
        return;
        this.currentToken = String.valueOf(this.text.charAt(this.pos));
        this.matcher.region(1 + this.pos, this.matcher.regionEnd());
      }
    }

    public TextFormat.ParseException parseException(String paramString)
    {
      return new TextFormat.ParseException(1 + this.line + ":" + (1 + this.column) + ": " + paramString);
    }

    public TextFormat.ParseException parseExceptionPreviousToken(String paramString)
    {
      return new TextFormat.ParseException(1 + this.previousLine + ":" + (1 + this.previousColumn) + ": " + paramString);
    }

    public boolean tryConsume(String paramString)
    {
      if (this.currentToken.equals(paramString))
      {
        nextToken();
        return true;
      }
      return false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.google.protobuf.TextFormat
 * JD-Core Version:    0.6.2
 */