package com.htc.lockscreen;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import com.htc.lockscreen.ntf.HtcLSNotification;

public class HtcLSUtility
{
  public static final String BASE_PACKAG_NAME = "com.htc.idlescreen.base";

  public static float getSenseVersion()
  {
    try
    {
      float f = Float.parseFloat("3.5");
      return f;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return 2.0F;
  }

  public static boolean isWithIdleScreenBase(Context paramContext)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    try
    {
      localPackageManager.getPackageInfo("com.htc.idlescreen.base", 0);
      return true;
    }
    catch (Exception localException)
    {
      return false;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
    }
    return false;
  }

  public static void removeNotification(Context paramContext, int paramInt)
  {
    String str = paramContext.getPackageName();
    if (TextUtils.isEmpty(str))
      throw new IllegalArgumentException("packageName is null or empty");
    Intent localIntent = new Intent("HtcLockScreenRemoveNotfiication");
    localIntent.putExtra("PackageName", str);
    localIntent.putExtra("id", paramInt);
    paramContext.sendBroadcast(localIntent);
  }

  static void removeView(Context paramContext, HtcLSViewGroup paramHtcLSViewGroup)
  {
    Intent localIntent = new Intent("HtcLockScreenRemoveView");
    localIntent.putExtra("PackageName", paramHtcLSViewGroup.getPackageName());
    localIntent.putExtra("id", paramHtcLSViewGroup.getId());
    paramContext.sendBroadcast(localIntent);
  }

  public static void removeView(Context paramContext, String paramString, int paramInt)
  {
    Intent localIntent = new Intent("HtcLockScreenRemoveView");
    localIntent.putExtra("PackageName", paramString);
    localIntent.putExtra("id", paramInt);
    paramContext.sendBroadcast(localIntent);
  }

  public static void setNotification(Context paramContext, int paramInt, HtcLSNotification paramHtcLSNotification)
  {
    String str = paramContext.getPackageName();
    if (TextUtils.isEmpty(str))
      throw new IllegalArgumentException("packageName is null or empty");
    if (paramHtcLSNotification == null)
      throw new IllegalArgumentException("notification data is null");
    if (paramInt <= 0)
      throw new IllegalArgumentException("id is <= 0");
    Intent localIntent = new Intent("HtcLockScreenSetNotification");
    localIntent.putExtra("notification", paramHtcLSNotification);
    localIntent.putExtra("PackageName", str);
    localIntent.putExtra("id", paramInt);
    paramContext.sendBroadcast(localIntent);
  }

  static void setView(Context paramContext, HtcLSViewGroup paramHtcLSViewGroup)
  {
    Intent localIntent = new Intent("HtcLockScreenSetView");
    localIntent.putExtra("View", paramHtcLSViewGroup);
    localIntent.putExtra("PackageName", paramHtcLSViewGroup.getPackageName());
    localIntent.putExtra("id", paramHtcLSViewGroup.getId());
    paramContext.sendBroadcast(localIntent);
  }

  public static boolean supportRemotePhoneService(Context paramContext)
  {
    if (getSenseVersion() >= 3.5F)
      return isWithIdleScreenBase(paramContext);
    return false;
  }

  public static boolean useIdleScreen()
  {
    return getSenseVersion() >= 3.0F;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.htc.lockscreen.HtcLSUtility
 * JD-Core Version:    0.6.2
 */