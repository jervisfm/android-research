package com.htc.lockscreen.idlescreen.phonecall;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import com.htc.lockscreen.idlescreen.remote.IRemoteService.Stub;
import com.htc.lockscreen.telephony.PhoneState;

public abstract class IdlePhoneCallService extends Service
{
  public static final String ACTION_PHONE_SERIVCE = "com.htc.lockscreen.phone.service";
  public static final String ACTION_UPDATE_PHONE_STATE = "phoneState";
  public static final int CODE_ANSWER = 1;
  public static final int CODE_REJECT = 2;
  public static final int CODE_SENDMSG = 4;
  public static final int CODE_SILENT = 3;
  public static final String KEY_CALL_ID = "call_id";
  public static final String KEY_PACKAGE_NAME = "packageName";
  public static final String KEY_PHONE_STATE = "phoneState";
  public static final String KEY_RING = "ringing";
  public static final String KEY_WITH_PHONESTATE = "withPhoneState";
  private final IBinder mPublicBinder = new IdlePhoneCallServiceWrapper();
  private Handler mUIHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      super.handleMessage(paramAnonymousMessage);
      int i = paramAnonymousMessage.what;
      int j = paramAnonymousMessage.arg1;
      switch (i)
      {
      default:
        return;
      case 1:
        IdlePhoneCallService.this.answerCall(j);
        return;
      case 2:
        IdlePhoneCallService.this.rejectCall(j);
        return;
      case 3:
        IdlePhoneCallService.this.silentCall(j);
        return;
      case 4:
      }
      IdlePhoneCallService.this.sendMessage(j);
    }
  };

  public static void endPhoneUI(Context paramContext)
  {
    endPhoneUI(paramContext, 0);
  }

  public static void endPhoneUI(Context paramContext, int paramInt)
  {
    Intent localIntent = new Intent("phoneState");
    localIntent.putExtra("packageName", paramContext.getPackageName());
    localIntent.putExtra("call_id", paramInt);
    localIntent.putExtra("withPhoneState", false);
    paramContext.sendBroadcast(localIntent);
  }

  public static void startPhoneUI(Context paramContext, PhoneState paramPhoneState)
  {
    updatePhoneUI(paramContext, paramPhoneState);
  }

  public static void updatePhoneUI(Context paramContext, PhoneState paramPhoneState)
  {
    if (paramPhoneState == null)
      return;
    Intent localIntent = new Intent("phoneState");
    localIntent.putExtra("packageName", paramContext.getPackageName());
    localIntent.putExtra("call_id", paramPhoneState.getId());
    localIntent.putExtra("withPhoneState", true);
    localIntent.putExtra("phoneState", paramPhoneState);
    paramContext.sendBroadcast(localIntent);
  }

  public abstract void answerCall(int paramInt);

  public final IBinder onBind(Intent paramIntent)
  {
    return this.mPublicBinder;
  }

  public boolean onUnbind(Intent paramIntent)
  {
    return super.onUnbind(paramIntent);
  }

  public void performPhoneAction(int paramInt1, int paramInt2)
  {
    Message localMessage = Message.obtain();
    localMessage.what = paramInt1;
    localMessage.arg1 = paramInt2;
    this.mUIHandler.sendMessage(localMessage);
  }

  public abstract void rejectCall(int paramInt);

  public abstract void sendMessage(int paramInt);

  public abstract void silentCall(int paramInt);

  class IdlePhoneCallServiceWrapper extends IRemoteService.Stub
  {
    IdlePhoneCallServiceWrapper()
    {
    }

    public Bundle[] getData(int paramInt, Bundle paramBundle)
      throws RemoteException
    {
      return null;
    }

    public int getInt(int paramInt)
      throws RemoteException
    {
      return 0;
    }

    public String getString(int paramInt)
      throws RemoteException
    {
      return null;
    }

    public void performAction(int paramInt, Bundle paramBundle)
      throws RemoteException
    {
      int i = paramBundle.getInt("call_id");
      IdlePhoneCallService.this.performPhoneAction(paramInt, i);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.htc.lockscreen.idlescreen.phonecall.IdlePhoneCallService
 * JD-Core Version:    0.6.2
 */