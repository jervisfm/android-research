package com.htc.lockscreen.telephony;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PhoneState
  implements Parcelable
{
  public static final int CALL_STATE_IDLE = 0;
  public static final int CALL_STATE_OFFHOOK = 2;
  public static final int CALL_STATE_RING = 1;
  public static final Parcelable.Creator<PhoneState> CREATOR = new Parcelable.Creator()
  {
    public PhoneState createFromParcel(Parcel paramAnonymousParcel)
    {
      return new PhoneState(paramAnonymousParcel);
    }

    public PhoneState[] newArray(int paramAnonymousInt)
    {
      return new PhoneState[paramAnonymousInt];
    }
  };
  public static final int FLAG_SENDMSG = 2;
  public static final int FLAG_SILENT = 1;
  public int mCallState = 0;
  public String mCallType = "";
  public String mDisplayNumber = "";
  public String mEventDesp = "";
  public Bitmap mEventIcon = null;
  public int mFlag = 0;
  public String mHint = "";
  public int mId = 0;
  public String mLocation = "";
  public String mName = "";
  public String mOpName = "";
  public String mPackageName = "";
  public Bitmap mPhoto = null;
  public Bitmap mSNIcon = null;
  public String mSNStatus = "";
  public ComponentName mService = null;

  public PhoneState(int paramInt, String paramString1, String paramString2, Bitmap paramBitmap1, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Bitmap paramBitmap2, String paramString8, Bitmap paramBitmap3)
  {
    this.mCallState = paramInt;
    this.mOpName = paramString1;
    this.mCallType = paramString2;
    this.mPhoto = paramBitmap1;
    this.mName = paramString3;
    this.mDisplayNumber = paramString4;
    this.mLocation = paramString5;
    this.mHint = paramString6;
    this.mSNStatus = paramString7;
    this.mSNIcon = paramBitmap2;
    this.mEventDesp = paramString8;
    this.mEventIcon = paramBitmap3;
  }

  public PhoneState(Context paramContext)
  {
    this.mPackageName = paramContext.getPackageName();
  }

  public PhoneState(Parcel paramParcel)
  {
    this.mPackageName = paramParcel.readString();
    this.mId = paramParcel.readInt();
    this.mCallState = paramParcel.readInt();
    this.mOpName = paramParcel.readString();
    this.mCallType = paramParcel.readString();
    this.mName = paramParcel.readString();
    this.mDisplayNumber = paramParcel.readString();
    this.mLocation = paramParcel.readString();
    this.mHint = paramParcel.readString();
    this.mSNStatus = paramParcel.readString();
    this.mEventDesp = paramParcel.readString();
    this.mFlag = paramParcel.readInt();
    if (paramParcel.readInt() > 0)
      this.mPhoto = ((Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel));
    if (paramParcel.readInt() > 0)
      this.mSNIcon = ((Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel));
    if (paramParcel.readInt() > 0)
      this.mEventIcon = ((Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel));
    if (paramParcel.readInt() > 0)
      this.mService = ((ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel));
  }

  public int describeContents()
  {
    return 0;
  }

  public String getBirthdayInfo()
  {
    return this.mEventDesp;
  }

  public int getCallState()
  {
    return this.mCallState;
  }

  public String getDisplayNumber()
  {
    return this.mDisplayNumber;
  }

  public int getFlag()
  {
    return this.mFlag;
  }

  public int getId()
  {
    return this.mId;
  }

  public String getLocation()
  {
    return this.mLocation;
  }

  public String getName()
  {
    return this.mName;
  }

  public String getPackageName()
  {
    return this.mPackageName;
  }

  public ComponentName getPhoneComponent()
  {
    return this.mService;
  }

  public Bitmap getPhoto()
  {
    return this.mPhoto;
  }

  public String getSocailState()
  {
    return this.mSNStatus;
  }

  public Bitmap getSocialIcon()
  {
    return this.mSNIcon;
  }

  public void setBirthdayInfo(String paramString)
  {
    this.mEventDesp = paramString;
  }

  public void setCallState(int paramInt)
  {
    this.mCallState = paramInt;
  }

  public void setDisplayNumber(String paramString)
  {
    this.mDisplayNumber = paramString;
  }

  public void setFlag(int paramInt)
  {
    this.mFlag = paramInt;
  }

  public void setId(int paramInt)
  {
    this.mId = paramInt;
  }

  public void setLocation(String paramString)
  {
    this.mLocation = paramString;
  }

  public void setName(String paramString)
  {
    this.mName = paramString;
  }

  public void setPhoneComponent(String paramString1, String paramString2)
  {
    this.mService = new ComponentName(paramString1, paramString2);
  }

  public void setPhoto(Bitmap paramBitmap)
  {
    this.mPhoto = paramBitmap;
  }

  public void setSocailState(String paramString)
  {
    this.mSNStatus = paramString;
  }

  public void setSocialIcon(Bitmap paramBitmap)
  {
    this.mSNIcon = paramBitmap;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeInt(this.mId);
    paramParcel.writeInt(this.mCallState);
    paramParcel.writeString(this.mOpName);
    paramParcel.writeString(this.mCallType);
    paramParcel.writeString(this.mName);
    paramParcel.writeString(this.mDisplayNumber);
    paramParcel.writeString(this.mLocation);
    paramParcel.writeString(this.mHint);
    paramParcel.writeString(this.mSNStatus);
    paramParcel.writeString(this.mEventDesp);
    paramParcel.writeInt(this.mFlag);
    if (this.mPhoto != null)
    {
      paramParcel.writeInt(1);
      this.mPhoto.writeToParcel(paramParcel, paramInt);
      if (this.mSNIcon == null)
        break label189;
      paramParcel.writeInt(1);
      this.mSNIcon.writeToParcel(paramParcel, paramInt);
      label138: if (this.mEventIcon == null)
        break label197;
      paramParcel.writeInt(1);
      this.mEventIcon.writeToParcel(paramParcel, paramInt);
    }
    while (true)
    {
      if (this.mService == null)
        break label205;
      paramParcel.writeInt(1);
      this.mService.writeToParcel(paramParcel, paramInt);
      return;
      paramParcel.writeInt(0);
      break;
      label189: paramParcel.writeInt(0);
      break label138;
      label197: paramParcel.writeInt(0);
    }
    label205: paramParcel.writeInt(0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.htc.lockscreen.telephony.PhoneState
 * JD-Core Version:    0.6.2
 */