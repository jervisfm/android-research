package com.lge.secondcamera;

import android.hardware.Camera;

class DualCamera
{
  static
  {
    System.loadLibrary("dualcamera_library_jni");
  }

  private native void setCamera(int paramInt);

  public Camera SelectCamera(int paramInt)
  {
    setCamera(paramInt);
    return Camera.open();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.lge.secondcamera.DualCamera
 * JD-Core Version:    0.6.2
 */