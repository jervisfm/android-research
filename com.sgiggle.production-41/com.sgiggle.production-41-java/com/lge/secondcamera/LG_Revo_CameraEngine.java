package com.lge.secondcamera;

import android.hardware.Camera;

public final class LG_Revo_CameraEngine
{
  private Camera mCam;

  public void CloseCamera(Camera paramCamera)
  {
    paramCamera.release();
  }

  public Camera OpenCamera(int paramInt)
  {
    return new DualCamera().SelectCamera(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.lge.secondcamera.LG_Revo_CameraEngine
 * JD-Core Version:    0.6.2
 */