package com.sgiggle.GLES20;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;

public class FilterManager
{
  public static native boolean addFilter(int paramInt, String paramString1, String paramString2);

  public static native int download(String paramString, int paramInt);

  public static int install(Context paramContext, String paramString)
  {
    int i = 0;
    for (char c = 'a'; ; c = (char)(c + '\001'))
    {
      String str1 = read(paramContext, paramString + "/" + c + ".vsh");
      String str2 = read(paramContext, paramString + "/" + c + ".fsh");
      if ((str1 == "") || (str2 == ""))
        return i;
      addFilter(i, str1, str2);
      i++;
    }
  }

  private static String read(Context paramContext, String paramString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    try
    {
      InputStream localInputStream = paramContext.getAssets().open(paramString, 3);
      byte[] arrayOfByte = new byte[2048];
      while (true)
      {
        int i = localInputStream.read(arrayOfByte);
        if (i == -1)
          break;
        localStringBuffer.append(new String(arrayOfByte, 0, i));
      }
    }
    catch (IOException localIOException)
    {
    }
    return localStringBuffer.toString();
  }

  public static native void setFilter(int paramInt);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.GLES20.FilterManager
 * JD-Core Version:    0.6.2
 */