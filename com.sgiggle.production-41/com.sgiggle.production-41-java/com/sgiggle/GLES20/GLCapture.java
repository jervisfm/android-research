package com.sgiggle.GLES20;

import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.hardware.Camera;
import android.opengl.GLES20;
import java.lang.reflect.Method;

public class GLCapture
  implements SurfaceTexture.OnFrameAvailableListener
{
  private static final int GL_TEXTURE_EXTERNAL_OES = 36197;
  private static final int METHOD_GET_TIMESTAMP = 0;
  private static final int METHOD_RELEASE = 1;
  private static final String TAG = "GLCapture";
  private static GLCapture mInstance;
  private boolean mAvailable;
  private Camera mCamera;
  private GLRenderer mGLRenderer = GLRenderer.getInstance();
  private int mHeight;
  private Method[] mMethods = new Method[2];
  private Object[] mNullObjects = new Object[0];
  private SurfaceTexture mSurfaceTexture;
  private int[] mTextures = new int[1];
  private long mTimestamp;
  private float[] mTransform = new float[16];
  private int mWidth;

  static
  {
    if (GLRenderer.hasGLCapture())
      mInstance = new GLCapture();
  }

  private GLCapture()
  {
    try
    {
      Class localClass = Class.forName("android.graphics.SurfaceTexture");
      this.mMethods[0] = localClass.getMethod("getTimestamp", (Class[])null);
      this.mMethods[1] = localClass.getMethod("release", (Class[])null);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public static GLCapture getInstance()
  {
    return mInstance;
  }

  private native void render(float[] paramArrayOfFloat, int paramInt1, int paramInt2, int paramInt3, long paramLong);

  private void setPreviewTexture(SurfaceTexture paramSurfaceTexture)
  {
    try
    {
      Camera localCamera = this.mCamera;
      if (localCamera != null);
      try
      {
        this.mCamera.setPreviewTexture(paramSurfaceTexture);
        return;
      }
      catch (Exception localException)
      {
        while (true)
          localException.printStackTrace();
      }
    }
    finally
    {
    }
  }

  // ERROR //
  public void draw()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 106	com/sgiggle/GLES20/GLCapture:mAvailable	Z
    //   6: istore_2
    //   7: iload_2
    //   8: ifne +6 -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: aload_0
    //   15: iconst_0
    //   16: putfield 106	com/sgiggle/GLES20/GLCapture:mAvailable	Z
    //   19: aload_0
    //   20: getfield 108	com/sgiggle/GLES20/GLCapture:mSurfaceTexture	Landroid/graphics/SurfaceTexture;
    //   23: invokevirtual 113	android/graphics/SurfaceTexture:updateTexImage	()V
    //   26: aload_0
    //   27: getfield 108	com/sgiggle/GLES20/GLCapture:mSurfaceTexture	Landroid/graphics/SurfaceTexture;
    //   30: aload_0
    //   31: getfield 59	com/sgiggle/GLES20/GLCapture:mTransform	[F
    //   34: invokevirtual 117	android/graphics/SurfaceTexture:getTransformMatrix	([F)V
    //   37: aload_0
    //   38: getfield 63	com/sgiggle/GLES20/GLCapture:mMethods	[Ljava/lang/reflect/Method;
    //   41: iconst_0
    //   42: aaload
    //   43: astore_3
    //   44: aload_3
    //   45: ifnull +88 -> 133
    //   48: aload_0
    //   49: getfield 63	com/sgiggle/GLES20/GLCapture:mMethods	[Ljava/lang/reflect/Method;
    //   52: iconst_0
    //   53: aaload
    //   54: aload_0
    //   55: getfield 108	com/sgiggle/GLES20/GLCapture:mSurfaceTexture	Landroid/graphics/SurfaceTexture;
    //   58: aload_0
    //   59: getfield 65	com/sgiggle/GLES20/GLCapture:mNullObjects	[Ljava/lang/Object;
    //   62: invokevirtual 121	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   65: checkcast 123	java/lang/Long
    //   68: invokevirtual 127	java/lang/Long:longValue	()J
    //   71: lstore 11
    //   73: lload 11
    //   75: lstore 9
    //   77: lload 9
    //   79: ldc2_w 128
    //   82: ldiv
    //   83: lstore 14
    //   85: lload 14
    //   87: lstore 6
    //   89: aload_0
    //   90: aload_0
    //   91: getfield 59	com/sgiggle/GLES20/GLCapture:mTransform	[F
    //   94: aload_0
    //   95: getfield 57	com/sgiggle/GLES20/GLCapture:mTextures	[I
    //   98: iconst_0
    //   99: iaload
    //   100: aload_0
    //   101: getfield 131	com/sgiggle/GLES20/GLCapture:mWidth	I
    //   104: aload_0
    //   105: getfield 133	com/sgiggle/GLES20/GLCapture:mHeight	I
    //   108: lload 6
    //   110: invokespecial 135	com/sgiggle/GLES20/GLCapture:render	([FIIIJ)V
    //   113: goto -102 -> 11
    //   116: astore_1
    //   117: aload_0
    //   118: monitorexit
    //   119: aload_1
    //   120: athrow
    //   121: astore 8
    //   123: lconst_0
    //   124: lstore 9
    //   126: lload 9
    //   128: lstore 6
    //   130: goto -41 -> 89
    //   133: aload_0
    //   134: getfield 137	com/sgiggle/GLES20/GLCapture:mTimestamp	J
    //   137: lstore 4
    //   139: lload 4
    //   141: lstore 6
    //   143: goto -54 -> 89
    //   146: astore 13
    //   148: goto -22 -> 126
    //
    // Exception table:
    //   from	to	target	type
    //   2	7	116	finally
    //   14	44	116	finally
    //   48	73	116	finally
    //   77	85	116	finally
    //   89	113	116	finally
    //   133	139	116	finally
    //   48	73	121	java/lang/Exception
    //   77	85	146	java/lang/Exception
  }

  public boolean init()
  {
    try
    {
      GLES20.glGenTextures(1, this.mTextures, 0);
      GLES20.glActiveTexture(33984);
      GLES20.glBindTexture(36197, this.mTextures[0]);
      GLES20.glTexParameteri(36197, 10241, 9729);
      GLES20.glTexParameteri(36197, 10240, 9729);
      GLES20.glTexParameteri(36197, 10242, 33071);
      GLES20.glTexParameteri(36197, 10243, 33071);
      this.mSurfaceTexture = new SurfaceTexture(this.mTextures[0]);
      this.mSurfaceTexture.setOnFrameAvailableListener(this);
      setPreviewTexture(this.mSurfaceTexture);
      return true;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void onFrameAvailable(SurfaceTexture paramSurfaceTexture)
  {
    try
    {
      this.mAvailable = true;
      if (this.mMethods[0] == null)
        this.mTimestamp = System.currentTimeMillis();
      this.mGLRenderer.requestRender();
      return;
    }
    finally
    {
    }
  }

  public void setCamera(Camera paramCamera, int paramInt1, int paramInt2)
  {
    try
    {
      this.mCamera = paramCamera;
      this.mWidth = paramInt1;
      this.mHeight = paramInt2;
      if (this.mSurfaceTexture != null)
        setPreviewTexture(this.mSurfaceTexture);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void uninit()
  {
    try
    {
      if (this.mSurfaceTexture != null)
      {
        Method localMethod = this.mMethods[1];
        if (localMethod == null);
      }
      try
      {
        this.mMethods[1].invoke(this.mSurfaceTexture, this.mNullObjects);
        this.mSurfaceTexture = null;
        this.mAvailable = false;
        this.mCamera = null;
        return;
      }
      catch (Exception localException)
      {
        while (true)
          localException.printStackTrace();
      }
    }
    finally
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.GLES20.GLCapture
 * JD-Core Version:    0.6.2
 */