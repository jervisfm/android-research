package com.sgiggle.GLES20;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Build;
import android.os.Build.VERSION;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import com.sgiggle.util.Log;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GLRenderer
  implements GLSurfaceView.Renderer, SurfaceHolder.Callback
{
  private static final String TAG = "GLRenderer";
  private static boolean mHasGLCapture;
  private static GLRenderer mInstance;
  private static boolean mIsCaptureSupported;
  private static boolean mIsSupported;
  private static boolean mWorkWithoutPreviewDisplay;
  private static GLCapture mglCapture;
  private boolean mGLSurfaceExist;
  private SurfaceHolder mGLSurfaceHolder;
  private GLSurfaceView mGLSurfaceView;
  private Renderer mRenderer;

  private GLRenderer(Context paramContext)
  {
  }

  public static GLRenderer getInstance()
  {
    return mInstance;
  }

  public static boolean hasGLCapture()
  {
    return mHasGLCapture;
  }

  private static native void initJNI(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3);

  public static boolean isCaptureSupported()
  {
    return mIsCaptureSupported;
  }

  public static boolean isNotNeededPreviewDisplay()
  {
    return mWorkWithoutPreviewDisplay;
  }

  public static boolean isSupported()
  {
    return mIsSupported;
  }

  private native void renderNative(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3, int paramInt4);

  private native void setSurface(GLSurfaceView paramGLSurfaceView);

  public static void updateContext(Context paramContext)
  {
    Log.v("GLRenderer", "" + Build.MANUFACTURER + "," + Build.MODEL + "," + Build.VERSION.SDK_INT);
    mInstance = new GLRenderer(paramContext);
    if ((Build.VERSION.SDK_INT >= 8) && (((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("SCH-I510")) || (Build.MODEL.equals("SHW-M250S")) || (Build.MODEL.equals("SHW-M250K")) || (Build.MODEL.equals("GT-I9100")) || (Build.MODEL.equals("GT-I9100G")) || (Build.MODEL.equals("GT-I9100T")) || (Build.MODEL.equals("GT-I9000")) || (Build.MODEL.equals("SHW-M110S")) || (Build.MODEL.equals("SPH-D710")) || (Build.MODEL.equals("SGH-T989")) || (Build.MODEL.endsWith("SGH-I727")) || (Build.MODEL.endsWith("SGH-I777")) || (Build.MODEL.equals("SGH-I717")) || (Build.MODEL.equals("GT-N7000")) || (Build.MODEL.equals("GT-P7510")) || (Build.MODEL.equals("SAMSUNG-SGH-I897")) || (Build.MODEL.equals("Nexus S")) || (Build.MODEL.equals("YP-GB1")) || (Build.MODEL.equals("GT-P1010")) || (Build.MODEL.equals("SGH-T959")) || (Build.MODEL.equals("SPH-M820-BST")) || (Build.MODEL.startsWith("Galaxy Nexus")) || (Build.MODEL.startsWith("GT-I9001")) || (Build.MODEL.equals("SAMSUNG-SGH-I997")) || (Build.MODEL.startsWith("GT-I9300")))) || ((Build.MANUFACTURER.compareToIgnoreCase("HTC") == 0) && ((Build.MODEL.equals("ADR6400L")) || (Build.MODEL.equals("ADR6425LVW")) || (Build.MODEL.equals("ADR6330VW")) || (Build.MODEL.equals("HTC Sensation 4G")) || (Build.MODEL.equals("T-Mobile G2")) || (Build.MODEL.equals("PC36100")) || (Build.MODEL.equals("HTC Sensation XL with Beats Audio X315e")) || (Build.MODEL.equals("HTC Rhyme S510b")) || (Build.MODEL.equals("HTC VLE_U")) || (Build.MODEL.equals("HTC PG09410")) || (Build.MODEL.equals("HTC Flyer P510e")) || (Build.MODEL.startsWith("HTC One X")) || (Build.MODEL.equals("HTC Glacier")))) || ((Build.MANUFACTURER.compareToIgnoreCase("Motorola") == 0) && ((Build.MODEL.equals("Xoom")) || (Build.MODEL.equals("DROID3")) || (Build.MODEL.equals("DROID4")) || (Build.MODEL.equals("DROID RAZR")) || (Build.MODEL.equals("DROID BIONIC")) || (Build.MODEL.equals("DROID X2")))) || ((Build.MANUFACTURER.compareToIgnoreCase("LGE") == 0) && ((Build.MODEL.equals("LG-P970")) || (Build.MODEL.equals("VS920 4G")) || (Build.MODEL.equals("LG-MS910")) || (Build.MODEL.equals("LGL85C")))) || ((Build.MANUFACTURER.compareToIgnoreCase("PANTECH") == 0) && (Build.MODEL.equals("IM-A720L"))) || ((Build.MANUFACTURER.compareToIgnoreCase("HUAWEI") == 0) && (Build.MODEL.equals("M886")))))
      mIsSupported = true;
    if (Build.VERSION.SDK_INT >= 13)
    {
      mIsSupported = true;
      mIsCaptureSupported = true;
      mHasGLCapture = true;
      mglCapture = GLCapture.getInstance();
    }
    if ((mIsSupported) && (Build.MANUFACTURER.compareToIgnoreCase("HTC") == 0) && (Build.MODEL.equals("HTC Glacier")))
      mWorkWithoutPreviewDisplay = true;
    initJNI(Build.VERSION.SDK_INT, mIsSupported, mIsCaptureSupported, mHasGLCapture);
  }

  public void init(GLSurfaceView paramGLSurfaceView, Renderer paramRenderer)
  {
    try
    {
      GLSurfaceViewEx.init(paramGLSurfaceView, this);
      this.mGLSurfaceHolder = paramGLSurfaceView.getHolder();
      this.mGLSurfaceHolder.addCallback(this);
      this.mGLSurfaceView = paramGLSurfaceView;
      this.mRenderer = paramRenderer;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void onDrawFrame(GL10 paramGL10)
  {
    try
    {
      if (mglCapture != null)
        mglCapture.draw();
      this.mRenderer.draw();
      return;
    }
    finally
    {
    }
  }

  public void onSurfaceChanged(GL10 paramGL10, int paramInt1, int paramInt2)
  {
    Log.v("GLRenderer", "onSurfaceChanged " + this.mGLSurfaceHolder + " " + paramInt1 + " " + paramInt2);
    try
    {
      this.mGLSurfaceExist = true;
      this.mRenderer.init(paramInt1, paramInt2);
      if (mglCapture != null)
        mglCapture.init();
      setSurface(this.mGLSurfaceView);
      return;
    }
    finally
    {
    }
  }

  public void onSurfaceCreated(GL10 paramGL10, EGLConfig paramEGLConfig)
  {
    Log.v("GLRenderer", "onSurfaceCreated " + this.mGLSurfaceHolder);
  }

  public void render(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3, int paramInt4)
  {
    try
    {
      renderNative(paramInt1, paramArrayOfByte, paramInt2, paramInt3, paramInt4);
      requestRender();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void requestRender()
  {
    try
    {
      if (this.mGLSurfaceView != null)
        this.mGLSurfaceView.requestRender();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public native void setClip(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

  public void setRenderMode(int paramInt)
  {
    try
    {
      if (this.mGLSurfaceView != null)
        this.mGLSurfaceView.setRenderMode(paramInt);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public native void setTransform(int paramInt, float paramFloat, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4);

  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    Log.v("GLRenderer", "surfaceChanged " + paramSurfaceHolder + " " + paramInt1 + " " + paramInt2 + " " + paramInt3);
  }

  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    Log.v("GLRenderer", "surfaceCreated " + paramSurfaceHolder);
  }

  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    Log.v("GLRenderer", "surfaceDestroyed " + paramSurfaceHolder);
    try
    {
      if (paramSurfaceHolder != this.mGLSurfaceHolder)
      {
        Log.w("GLRenderer", "surfaceDestroyed not current surface");
        return;
      }
      this.mGLSurfaceExist = false;
      setSurface(null);
      if (mglCapture != null)
        mglCapture.uninit();
      this.mRenderer.uninit();
      return;
    }
    finally
    {
    }
  }

  public static abstract interface Type
  {
    public static final int INVALID = -1;
    public static final int LAST = 2;
    public static final int PLAYBACK = 0;
    public static final int PREVIEW = 1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.GLES20.GLRenderer
 * JD-Core Version:    0.6.2
 */