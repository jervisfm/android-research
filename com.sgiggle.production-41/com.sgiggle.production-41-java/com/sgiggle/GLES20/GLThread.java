package com.sgiggle.GLES20;

import android.view.Surface;

public class GLThread
{
  public static native boolean init();

  public static native int loadLibraries();

  public static native void requestRender();

  public static native void setSurface(Surface paramSurface, int paramInt1, int paramInt2);

  public static native void uninit();

  public static native void unloadLibraries();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.GLES20.GLThread
 * JD-Core Version:    0.6.2
 */