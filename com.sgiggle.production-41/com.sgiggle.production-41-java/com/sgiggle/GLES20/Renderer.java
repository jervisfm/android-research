package com.sgiggle.GLES20;

public abstract interface Renderer
{
  public abstract void draw();

  public abstract void init(int paramInt1, int paramInt2);

  public abstract void uninit();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.GLES20.Renderer
 * JD-Core Version:    0.6.2
 */