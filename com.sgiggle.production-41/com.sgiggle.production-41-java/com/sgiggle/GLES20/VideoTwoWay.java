package com.sgiggle.GLES20;

public class VideoTwoWay
  implements Renderer
{
  private static final String TAG = "VideoTwoWay";
  private GLRenderer glRenderer = GLRenderer.getInstance();
  private int native_object = create();

  private static native int create();

  private static native void destroy(int paramInt);

  private native boolean isInside(int paramInt, float paramFloat1, float paramFloat2);

  private native void nativeDraw(int paramInt);

  private native void nativeInit(int paramInt1, int paramInt2, int paramInt3);

  private native void nativeUninit(int paramInt);

  private native void setView1(int paramInt1, int paramInt2);

  private native void setView2(int paramInt1, int paramInt2, int paramInt3);

  private native void setView3(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

  private native void swapView(int paramInt);

  public void draw()
  {
    try
    {
      if (this.native_object != 0)
        nativeDraw(this.native_object);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void init(int paramInt1, int paramInt2)
  {
    try
    {
      if (this.native_object != 0)
        nativeInit(this.native_object, paramInt1, paramInt2);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public boolean isInside(float paramFloat1, float paramFloat2)
  {
    return isInside(this.native_object, paramFloat1, paramFloat2);
  }

  public void release()
  {
    try
    {
      destroy(this.native_object);
      this.native_object = 0;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void setSurface(GLSurfaceViewEx paramGLSurfaceViewEx)
  {
    this.glRenderer.init(paramGLSurfaceViewEx, this);
  }

  public void setView(int paramInt)
  {
    setView1(this.native_object, paramInt);
    this.glRenderer.requestRender();
  }

  public void setView(int paramInt1, int paramInt2)
  {
    setView2(this.native_object, paramInt1, paramInt2);
    this.glRenderer.requestRender();
  }

  public void setView(int paramInt1, int paramInt2, int paramInt3)
  {
    setView3(this.native_object, paramInt1, paramInt2, paramInt3);
    this.glRenderer.requestRender();
  }

  public void swapView()
  {
    swapView(this.native_object);
  }

  public void uninit()
  {
    try
    {
      if (this.native_object != 0)
        nativeUninit(this.native_object);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static abstract interface Type
  {
    public static final int BORDER = 2;
    public static final int CAFE = 3;
    public static final int CAFE_ALTERNATE = 4;
    public static final int INVALID = -1;
    public static final int LAST = 6;
    public static final int PLAYBACK = 0;
    public static final int PREVIEW = 1;
    public static final int TEST = 5;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.GLES20.VideoTwoWay
 * JD-Core Version:    0.6.2
 */