package com.sgiggle.MediaPlayerController;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.view.SurfaceHolder;
import com.sgiggle.util.Log;

public class MediaPlayerController
  implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnVideoSizeChangedListener
{
  private static final String TAG = "MediaPlayerController";
  private SurfaceHolder holder;
  private MediaPlayer mediaPlayer;

  static
  {
    System.loadLibrary("S1");
  }

  private native int nativeStartServer();

  private native int nativeStopServer();

  private boolean prepare(String paramString)
  {
    this.mediaPlayer = new MediaPlayer();
    if (this.mediaPlayer == null)
      return false;
    try
    {
      this.mediaPlayer.setDataSource(paramString);
      this.mediaPlayer.setDisplay(this.holder);
      this.mediaPlayer.setOnBufferingUpdateListener(this);
      this.mediaPlayer.setOnCompletionListener(this);
      this.mediaPlayer.setOnErrorListener(this);
      this.mediaPlayer.setOnInfoListener(this);
      this.mediaPlayer.setOnPreparedListener(this);
      this.mediaPlayer.setOnSeekCompleteListener(this);
      this.mediaPlayer.setOnVideoSizeChangedListener(this);
      this.mediaPlayer.prepareAsync();
      return true;
    }
    catch (Exception localException)
    {
      Log.e("MediaPlayerController", "prepareAsync failed ", localException);
      this.mediaPlayer.release();
      this.mediaPlayer = null;
    }
    return false;
  }

  public void onBufferingUpdate(MediaPlayer paramMediaPlayer, int paramInt)
  {
    Log.v("MediaPlayerController", "onBufferingUpdate " + paramInt);
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    Log.v("MediaPlayerController", "onCompletion");
    stopPlaying();
  }

  public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    Log.v("MediaPlayerController", "onError " + paramInt1 + " " + paramInt2);
    return false;
  }

  public boolean onInfo(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    Log.v("MediaPlayerController", "onInfo " + paramInt1 + " " + paramInt2);
    return false;
  }

  public void onPrepared(MediaPlayer paramMediaPlayer)
  {
    Log.v("MediaPlayerController", "OnPrepared");
    this.mediaPlayer.start();
  }

  public void onSeekComplete(MediaPlayer paramMediaPlayer)
  {
    Log.v("MediaPlayerController", "onSeekComplete");
  }

  public void onVideoSizeChanged(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    Log.v("MediaPlayerController", "onVideoSizeChanged " + paramInt1 + " " + paramInt2);
  }

  public void setPreviewDisplay(SurfaceHolder paramSurfaceHolder)
    throws IllegalStateException
  {
    if (this.mediaPlayer != null)
      throw new IllegalStateException("already started");
    this.holder = paramSurfaceHolder;
  }

  public boolean startPlaying()
  {
    if (this.mediaPlayer != null)
      return false;
    if (this.holder == null)
      return false;
    if (!prepare("rtsp://127.0.0.1:8554"))
      return false;
    return nativeStartServer() == 0;
  }

  public void stopPlaying()
  {
    if (this.mediaPlayer == null);
    do
    {
      return;
      nativeStopServer();
    }
    while (this.mediaPlayer == null);
    this.mediaPlayer.reset();
    this.mediaPlayer.release();
    this.mediaPlayer = null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.MediaPlayerController.MediaPlayerController
 * JD-Core Version:    0.6.2
 */