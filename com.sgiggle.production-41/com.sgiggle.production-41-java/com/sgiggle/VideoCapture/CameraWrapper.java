package com.sgiggle.VideoCapture;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.CameraSlave;
import android.os.Build;
import android.os.Build.VERSION;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import com.lge.secondcamera.LG_Revo_CameraEngine;
import com.sgiggle.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;

public class CameraWrapper
{
  private static final int CLASS_CAMERA_INFO = 0;
  private static final int FIELD_FACING = 0;
  private static final int FIELD_ORIENTATION = 1;
  private static final int METHOD_ADD_CALLBACK_BUFFER = 0;
  private static final int METHOD_DELL_OPEN = 10;
  private static final int METHOD_GET_CAMERA_INFO = 4;
  private static final int METHOD_GET_FRONT_FACING_CAMERA = 3;
  private static final int METHOD_GET_NUMBER_OF_CAMERAS = 5;
  private static final int METHOD_HUAWEI_GET_CAMERA_NUM = 9;
  private static final int METHOD_HUAWEI_GET_CURRENT_CAMERA = 8;
  private static final int METHOD_HUAWEI_SET_CURRENT_CAMERA = 7;
  private static final int METHOD_MOTOROLA_GET_FRONT_CAMERA = 11;
  private static final int METHOD_OPEN = 6;
  private static final int METHOD_RECONNECT = 12;
  private static final int METHOD_SET_DISPLAY_ORIENTATION = 2;
  private static final int METHOD_SET_PREVIEW_CALLBACK_WITH_BUFFER = 1;
  public static final String TAG = "CameraWrapper";
  private static Object[] arg1 = new Object[1];
  private static Camera camera;
  private static int camera_count;
  private static Class[] classes = new Class[1];
  private static Field[] fields;
  private static boolean hasBack;
  private static boolean hasFront;
  private static CameraWrapper instance = new CameraWrapper();
  public static boolean isDell;
  public static boolean isDellStreak7;
  public static boolean isGingerbread;
  public static boolean isHuawei;
  public static boolean isLG;
  public static boolean isMotorola;
  public static boolean isPantech;
  public static boolean isSamsung;
  public static boolean isSamsungTablet;
  public static boolean isSprint;
  private static Method[] methods = new Method[13];
  private static Object oem_camera;
  private static int screenHeight;
  private static int screenWidth;

  static
  {
    fields = new Field[2];
  }

  private CameraWrapper()
  {
    init();
  }

  public static void getCameraInfo(int paramInt, CameraInfo paramCameraInfo)
  {
    if (isGingerbread);
    do
    {
      do
      {
        do
        {
          do
          {
            try
            {
              Object localObject = classes[0].newInstance();
              Object[] arrayOfObject = new Object[2];
              arrayOfObject[0] = Integer.valueOf(paramInt);
              arrayOfObject[1] = localObject;
              methods[4].invoke(null, arrayOfObject);
              paramCameraInfo.facing = fields[0].getInt(localObject);
              paramCameraInfo.orientation = fields[1].getInt(localObject);
              Log.v("CameraWrapper", "" + paramInt + " facing " + paramCameraInfo.facing + " orientation " + paramCameraInfo.orientation);
              return;
            }
            catch (Exception localException)
            {
              localException.printStackTrace();
              return;
            }
            if (camera_count != 1)
              break;
          }
          while (paramInt != 0);
          if (hasBack)
          {
            paramCameraInfo.facing = 0;
            paramCameraInfo.orientation = 90;
            return;
          }
        }
        while (!hasFront);
        paramCameraInfo.facing = 1;
        paramCameraInfo.orientation = 0;
        return;
      }
      while (camera_count != 2);
      if (paramInt == 0)
      {
        paramCameraInfo.facing = 0;
        paramCameraInfo.orientation = 90;
        return;
      }
    }
    while (paramInt != 1);
    paramCameraInfo.facing = 1;
    paramCameraInfo.orientation = 0;
  }

  public static int getNumberOfCameras()
  {
    return camera_count;
  }

  private static void init()
  {
    initClass();
    if (Build.VERSION.SDK_INT >= 9)
      isGingerbread = true;
    while (true)
    {
      initCameraCount();
      return;
      if ((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (!Build.MODEL.equals("SPH-M820-BST")))
      {
        isSamsung = true;
      }
      else if (Build.MANUFACTURER.compareToIgnoreCase("HUAWEI") == 0)
      {
        isHuawei = true;
      }
      else if (methods[3] != null)
      {
        isSprint = true;
      }
      else if (methods[10] != null)
      {
        isDell = true;
      }
      else if (methods[11] != null)
      {
        isMotorola = true;
      }
      else if ((Build.MANUFACTURER.equals("Dell Inc.")) && (Build.MODEL.equals("Dell Streak 7")))
      {
        isDellStreak7 = true;
      }
      else if ((Build.MANUFACTURER.compareToIgnoreCase("lge") == 0) && ((Build.MODEL.equals("LG-P999")) || (Build.MODEL.startsWith("LG-P990")) || (Build.MODEL.equals("LG-SU660")) || (Build.MODEL.equals("VS910 4G")) || (Build.MODEL.startsWith("LG-P925")) || (Build.MODEL.startsWith("LG-P920")) || (Build.MODEL.startsWith("LG-SU760")) || (Build.MODEL.startsWith("LG-P970")) || (Build.MODEL.startsWith("LG-KU5900")) || (Build.MODEL.startsWith("LGL85C"))))
      {
        isLG = true;
        if (Build.MODEL.equals("VS910 4G"))
          oem_camera = new LG_Revo_CameraEngine();
      }
      else if (methods[6] != null)
      {
        isPantech = true;
      }
    }
  }

  private static void initCameraCount()
  {
    if (isGingerbread);
    while (true)
    {
      try
      {
        int i1 = Integer.parseInt(methods[5].invoke(null, new Object[0]).toString());
        i = i1;
        camera_count = i;
        return;
      }
      catch (Exception localException6)
      {
        localException6.printStackTrace();
      }
      label42: int i = 0;
      continue;
      Camera localCamera;
      if (isSamsung)
      {
        i = 0 + 1;
        hasBack = true;
        localCamera = openCameraSafe(1);
        if (localCamera == null)
          continue;
      }
      try
      {
        localCamera.startPreview();
        localCamera.stopPreview();
        i++;
        hasFront = true;
        localCamera.release();
        continue;
        if ((!isHuawei) || ((Build.VERSION.SDK_INT < 8) || (methods[9] != null)));
        try
        {
          Object localObject = methods[9].invoke(null, new Object[0]);
          if (localObject == null)
            break label42;
          int m = Integer.parseInt(localObject.toString());
          i = m;
          if (i == 1)
          {
            int n;
            Exception localException2;
            try
            {
              n = Integer.parseInt(methods[8].invoke(null, new Object[0]).toString());
              if (n != 0)
                break label213;
              hasBack = true;
            }
            catch (Exception localException4)
            {
              k = i;
              localException2 = localException4;
            }
            localException2.printStackTrace();
            i = k;
            continue;
            label213: if (n != 1)
              continue;
            hasFront = true;
            continue;
          }
          if (i != 2)
            continue;
          hasBack = true;
          hasFront = true;
          continue;
          hasBack = true;
          i = 1;
          continue;
          try
          {
            Properties localProperties = new Properties();
            FileInputStream localFileInputStream = new FileInputStream(new File("/sys/camera/sensor"));
            if (localFileInputStream == null)
              break label42;
            localProperties.load(localFileInputStream);
            if (localProperties.getProperty("OUTER_CAMERA").compareToIgnoreCase("true") != 0)
              break label430;
            j = 0 + 1;
            hasBack = true;
            if (localProperties.getProperty("INNER_CAMERA").compareToIgnoreCase("true") != 0)
              break label424;
            i = j + 1;
            hasFront = true;
            localFileInputStream.close();
          }
          catch (Exception localException1)
          {
            hasBack = true;
            i = 1;
          }
          continue;
          if ((isSprint) || (isDell) || (isMotorola) || (isDellStreak7) || (isLG) || (isPantech))
          {
            hasBack = true;
            hasFront = true;
            i = 2;
            continue;
          }
          hasBack = true;
          i = 1;
        }
        catch (Exception localException3)
        {
          while (true)
            int k = 0;
        }
      }
      catch (Exception localException5)
      {
        while (true)
        {
          continue;
          label424: i = j;
          continue;
          label430: int j = 0;
        }
      }
    }
  }

  private static void initClass()
  {
    while (true)
    {
      int j;
      try
      {
        arrayOfMethod3 = Class.forName("android.hardware.Camera").getMethods();
        j = 0;
        if (j < arrayOfMethod3.length)
          if (arrayOfMethod3[j].getName().compareTo("addCallbackBuffer") == 0)
            methods[0] = arrayOfMethod3[j];
          else if (arrayOfMethod3[j].getName().compareTo("setPreviewCallbackWithBuffer") == 0)
            methods[1] = arrayOfMethod3[j];
      }
      catch (Exception localException1)
      {
        try
        {
          Method[] arrayOfMethod3;
          Class localClass5 = Class.forName("android.hardware.Camera$CameraInfo");
          classes[0] = localClass5;
          fields[0] = localClass5.getField("facing");
          fields[1] = localClass5.getField("orientation");
          try
          {
            Class localClass4 = Class.forName("com.sprint.hardware.twinCamDevice.FrontFacingCamera");
            methods[3] = localClass4.getDeclaredMethod("getFrontFacingCamera", new Class[0]);
            try
            {
              Class localClass3 = Class.forName("android.hardware.HtcFrontFacingCamera");
              Method[] arrayOfMethod2 = localClass3.getMethods();
              int i = 0;
              if (i < arrayOfMethod2.length)
              {
                if (arrayOfMethod2[i].getName().compareTo("setMirrorMode") == 0)
                  methods[3] = localClass3.getDeclaredMethod("getCamera", new Class[0]);
                i++;
                continue;
                if (arrayOfMethod3[j].getName().compareTo("setDisplayOrientation") == 0)
                  methods[2] = arrayOfMethod3[j];
                else if (arrayOfMethod3[j].getName().compareTo("getCameraInfo") == 0)
                  methods[4] = arrayOfMethod3[j];
                else if (arrayOfMethod3[j].getName().compareTo("getNumberOfCameras") == 0)
                  methods[5] = arrayOfMethod3[j];
                else if (arrayOfMethod3[j].toGenericString().compareTo("public static android.hardware.Camera android.hardware.Camera.open(int)") == 0)
                  methods[6] = arrayOfMethod3[j];
                else if (arrayOfMethod3[j].getName().compareTo("setCurrentCamera") == 0)
                  methods[7] = arrayOfMethod3[j];
                else if (arrayOfMethod3[j].getName().compareTo("getCurrentCamera") == 0)
                  methods[8] = arrayOfMethod3[j];
                else if (arrayOfMethod3[j].getName().compareTo("getCameraNum") == 0)
                  methods[9] = arrayOfMethod3[j];
                else if (arrayOfMethod3[j].getName().compareTo("reconnect") == 0)
                  methods[12] = arrayOfMethod3[j];
              }
            }
            catch (Exception localException4)
            {
              try
              {
                Class localClass2 = Class.forName("com.dell.android.hardware.CameraExtensions");
                Method[] arrayOfMethod1 = methods;
                Class[] arrayOfClass = new Class[1];
                arrayOfClass[0] = Integer.TYPE;
                arrayOfMethod1[10] = localClass2.getDeclaredMethod("open", arrayOfClass);
                try
                {
                  Class localClass1 = Class.forName("com.motorola.hardware.frontcamera.FrontCamera");
                  methods[11] = localClass1.getDeclaredMethod("getFrontCamera", new Class[0]);
                  return;
                }
                catch (Exception localException6)
                {
                  return;
                }
              }
              catch (Exception localException5)
              {
                continue;
              }
            }
          }
          catch (Exception localException3)
          {
            continue;
          }
        }
        catch (Exception localException2)
        {
          continue;
        }
      }
      j++;
    }
  }

  public static CameraWrapper open(int paramInt)
  {
    camera = openCameraSafe(paramInt);
    if (camera != null)
      return instance;
    return null;
  }

  protected static Camera openCameraSafe(int paramInt)
  {
    if (isGingerbread);
    while (true)
    {
      Object localObject;
      try
      {
        arg1[0] = Integer.valueOf(paramInt);
        localObject = (Camera)methods[6].invoke(null, arg1);
        if ((localObject != null) && (isSamsungTablet))
        {
          Camera.Parameters localParameters1 = ((Camera)localObject).getParameters();
          localParameters1.set("cam_mode", 1);
          ((Camera)localObject).setParameters(localParameters1);
        }
        return localObject;
      }
      catch (Exception localException15)
      {
        localException15.printStackTrace();
        localObject = null;
        continue;
      }
      if (isSamsung)
        try
        {
          Camera localCamera8 = Camera.open();
          localObject = localCamera8;
          if (localObject == null)
            continue;
          localParameters5 = ((Camera)localObject).getParameters();
          if (paramInt == 0)
          {
            localParameters5.set("camera-id", 1);
            ((Camera)localObject).setParameters(localParameters5);
          }
        }
        catch (Exception localException14)
        {
          while (true)
          {
            Camera.Parameters localParameters5;
            localException14.printStackTrace();
            localObject = null;
            continue;
            localParameters5.set("camera-id", 2);
          }
        }
      if (isSprint)
      {
        if (paramInt == 0)
        {
          try
          {
            Camera localCamera7 = Camera.open();
            localObject = localCamera7;
          }
          catch (Exception localException13)
          {
            localException13.printStackTrace();
            localObject = null;
          }
          continue;
        }
        try
        {
          localObject = (Camera)methods[3].invoke(null, new Object[0]);
        }
        catch (Exception localException12)
        {
          localException12.printStackTrace();
          localObject = null;
        }
        continue;
      }
      if ((!isHuawei) || (Build.VERSION.SDK_INT >= 8));
      try
      {
        arg1[0] = Integer.valueOf(paramInt);
        methods[7].invoke(null, arg1);
        while (true)
        {
          try
          {
            label246: Camera localCamera6 = Camera.open();
            localObject = localCamera6;
            if ((localObject == null) || (paramInt != 1))
              break;
            Camera.Parameters localParameters4 = ((Camera)localObject).getParameters();
            localParameters4.set("mirror", "enable");
            ((Camera)localObject).setParameters(localParameters4);
          }
          catch (Exception localException11)
          {
            localException11.printStackTrace();
            localObject = null;
            continue;
          }
          if (paramInt == 0)
            try
            {
              Camera localCamera5 = Camera.open();
              localObject = localCamera5;
            }
            catch (Exception localException9)
            {
              localException9.printStackTrace();
              localObject = null;
            }
          else
            try
            {
              CameraSlave localCameraSlave = CameraSlave.open();
              localObject = localCameraSlave;
            }
            catch (Exception localException8)
            {
              localException8.printStackTrace();
              localObject = null;
            }
        }
        if (isDell)
        {
          try
          {
            arg1[0] = Integer.valueOf(paramInt);
            localObject = (Camera)methods[10].invoke(null, arg1);
          }
          catch (Exception localException7)
          {
            localException7.printStackTrace();
            localObject = null;
          }
          continue;
        }
        if (isMotorola)
        {
          if (paramInt == 0)
          {
            try
            {
              Camera localCamera4 = Camera.open();
              localObject = localCamera4;
            }
            catch (Exception localException6)
            {
              localException6.printStackTrace();
              localObject = null;
            }
            continue;
          }
          try
          {
            localObject = (Camera)methods[11].invoke(null, new Object[0]);
          }
          catch (Exception localException5)
          {
            localException5.printStackTrace();
            localObject = null;
          }
          continue;
        }
        if (isDellStreak7)
          try
          {
            Camera localCamera3 = Camera.open();
            localObject = localCamera3;
            if (localObject == null)
              continue;
            localParameters3 = ((Camera)localObject).getParameters();
            if (paramInt == 0)
            {
              localParameters3.set("camera-sensor", 1);
              ((Camera)localObject).setParameters(localParameters3);
            }
          }
          catch (Exception localException4)
          {
            while (true)
            {
              Camera.Parameters localParameters3;
              localException4.printStackTrace();
              localObject = null;
              continue;
              localParameters3.set("camera-sensor", 0);
            }
          }
        if (isLG)
        {
          if (Build.MODEL.equals("VS910 4G"))
          {
            if (oem_camera == null)
              break label888;
            localObject = ((LG_Revo_CameraEngine)oem_camera).OpenCamera(paramInt);
            continue;
          }
          try
          {
            Camera localCamera2 = Camera.open();
            localObject = localCamera2;
            if (localObject == null)
              continue;
            localParameters2 = ((Camera)localObject).getParameters();
            if ((Build.MODEL.startsWith("LG-P970")) || (Build.MODEL.startsWith("LG-KU5900")) || (Build.MODEL.startsWith("LGL85C")))
              if (paramInt == 0)
              {
                i = 0;
                localParameters2.set("lge-camera", i);
                if (paramInt != 0)
                  break label683;
                j = 0;
                localParameters2.set("video-input", j);
                ((Camera)localObject).setParameters(localParameters2);
              }
          }
          catch (Exception localException3)
          {
            while (true)
            {
              Camera.Parameters localParameters2;
              localException3.printStackTrace();
              localObject = null;
              continue;
              int i = 1;
              continue;
              label683: int j = 1;
              continue;
              if ((Build.MODEL.startsWith("LG-P925")) || (Build.MODEL.startsWith("LG-P920")) || (Build.MODEL.startsWith("LG-SU760")))
              {
                localParameters2.setPictureSize(640, 480);
                int k;
                if (paramInt == 0)
                {
                  k = 0;
                  label740: localParameters2.set("lge-camera", k);
                  localParameters2.set("s3d-supported", "false");
                  if (paramInt != 0)
                    break label787;
                }
                label787: for (int m = 0; ; m = 1)
                {
                  localParameters2.set("camera-index", m);
                  break;
                  k = 1;
                  break label740;
                }
              }
              if (paramInt == 0)
                localParameters2.set("camera-sensor", 0);
              else
                localParameters2.set("camera-sensor", 1);
            }
          }
        }
        if (isPantech)
        {
          try
          {
            arg1[0] = Integer.valueOf(paramInt);
            localObject = (Camera)methods[6].invoke(null, arg1);
          }
          catch (Exception localException2)
          {
            localException2.printStackTrace();
            localObject = null;
          }
          continue;
        }
        if (paramInt == 0)
          try
          {
            Camera localCamera1 = Camera.open();
            localObject = localCamera1;
          }
          catch (Exception localException1)
          {
            localException1.printStackTrace();
          }
        label888: localObject = null;
      }
      catch (Exception localException10)
      {
        break label246;
      }
    }
  }

  public static void updateContext(Context paramContext)
  {
    WindowManager localWindowManager = (WindowManager)paramContext.getSystemService("window");
    screenWidth = localWindowManager.getDefaultDisplay().getWidth();
    screenHeight = localWindowManager.getDefaultDisplay().getHeight();
    if (((screenWidth == 1024) && (screenHeight == 600)) || (screenHeight == 1024) || (screenWidth == 600))
      isSamsungTablet = true;
  }

  public final void addCallbackBuffer(byte[] paramArrayOfByte)
  {
    if (methods[0] == null)
      return;
    try
    {
      arg1[0] = paramArrayOfByte;
      methods[0].invoke(camera, arg1);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public final void autoFocus(Camera.AutoFocusCallback paramAutoFocusCallback)
  {
    if (camera != null)
      camera.autoFocus(paramAutoFocusCallback);
  }

  public final void cancelAutoFocus()
  {
    if (camera != null)
      camera.cancelAutoFocus();
  }

  public Camera getCamera()
  {
    return camera;
  }

  public Camera.Parameters getParameters()
  {
    if (camera != null)
      return camera.getParameters();
    return null;
  }

  public final void lock()
  {
    if (camera != null)
      camera.lock();
  }

  public final void reconnect()
    throws IOException
  {
    if (methods[12] == null)
      return;
    try
    {
      methods[12].invoke(camera, (Object[])null);
      return;
    }
    catch (Exception localException)
    {
      if ((localException instanceof IOException))
        throw ((IOException)localException);
      localException.printStackTrace();
    }
  }

  public final void release()
  {
    if (camera != null)
      camera.release();
    camera = null;
  }

  public final void setDisplayOrientation(int paramInt)
  {
    if (methods[2] == null)
      return;
    try
    {
      arg1[0] = Integer.valueOf(paramInt);
      methods[2].invoke(camera, arg1);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public void setParameters(Camera.Parameters paramParameters)
  {
    if (camera != null)
      camera.setParameters(paramParameters);
  }

  public final void setPreviewCallback(Camera.PreviewCallback paramPreviewCallback)
  {
    if (camera != null)
      camera.setPreviewCallback(paramPreviewCallback);
  }

  public final void setPreviewCallbackWithBuffer(Camera.PreviewCallback paramPreviewCallback)
  {
    if (methods[1] == null)
    {
      camera.setPreviewCallback(paramPreviewCallback);
      return;
    }
    try
    {
      arg1[0] = paramPreviewCallback;
      methods[1].invoke(camera, arg1);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public final void setPreviewDisplay(SurfaceHolder paramSurfaceHolder)
  {
    try
    {
      if (camera != null)
        camera.setPreviewDisplay(paramSurfaceHolder);
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  public final void startPreview()
  {
    if (camera != null)
      camera.startPreview();
  }

  public final void stopPreview()
  {
    if (camera != null)
      camera.stopPreview();
  }

  public final void unlock()
  {
    if (camera != null)
      camera.unlock();
  }

  public static class CameraInfo
  {
    public static final int CAMERA_FACING_BACK = 0;
    public static final int CAMERA_FACING_FRONT = 1;
    public int facing;
    public int orientation;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.CameraWrapper
 * JD-Core Version:    0.6.2
 */