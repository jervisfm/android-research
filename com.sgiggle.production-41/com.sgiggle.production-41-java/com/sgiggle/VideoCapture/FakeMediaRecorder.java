package com.sgiggle.VideoCapture;

import com.sgiggle.util.Log;
import java.io.FileDescriptor;

public class FakeMediaRecorder
{
  private static final String TAG = "FakeMediaRecorder";
  private FileDescriptor fd;
  private String filename = "capture";
  private boolean isStarted = false;

  private native int nativeStart(FileDescriptor paramFileDescriptor, String paramString);

  private native int nativeStop(FileDescriptor paramFileDescriptor);

  public void setInputFile(String paramString)
  {
    this.filename = paramString;
  }

  public void setOutputFile(FileDescriptor paramFileDescriptor)
  {
    this.fd = paramFileDescriptor;
  }

  public void start()
    throws IllegalStateException
  {
    if (this.fd == null)
      throw new IllegalStateException("fd is not set");
    if (this.filename == null)
      throw new IllegalStateException("filename is not set");
    if (nativeStart(this.fd, this.filename) != 0)
    {
      Log.e("FakeMediaRecorder", "nativeStart failed");
      throw new IllegalStateException("nativeStart failed");
    }
    this.isStarted = true;
  }

  public void stop()
    throws IllegalStateException
  {
    if (!this.isStarted)
      throw new IllegalStateException("not started");
    if (nativeStop(this.fd) != 0)
      Log.e("FakeMediaRecorder", "nativeStop failed");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.FakeMediaRecorder
 * JD-Core Version:    0.6.2
 */