package com.sgiggle.VideoCapture;

import com.sgiggle.util.Log;

class FrameRateReducer
{
  public static final String TAG = "FrameRateReducer";
  private int m_head;
  private int m_rateDenom = 1;
  private int m_rateNum = 1;
  private int m_rejectedCount;
  private int m_selectedCount;
  private boolean[] m_selectionQueue;

  public FrameRateReducer()
  {
    this(10);
  }

  public FrameRateReducer(int paramInt)
  {
    this.m_selectionQueue = new boolean[paramInt];
    for (int i = 0; i < paramInt; i++)
      this.m_selectionQueue[i] = true;
    this.m_selectedCount = paramInt;
    this.m_rejectedCount = 0;
    this.m_head = 0;
  }

  public boolean nextFrame()
  {
    int i = Math.abs(10000 * (1 + this.m_selectedCount) / (1 + (this.m_selectedCount + this.m_rejectedCount)) - 10000 * this.m_rateNum / this.m_rateDenom);
    int j = Math.abs(10000 * this.m_selectedCount / (1 + (this.m_selectedCount + this.m_rejectedCount)) - 10000 * this.m_rateNum / this.m_rateDenom);
    Log.d("FrameRateReducer", "next rate called m_selectedCount " + this.m_selectedCount + " m_rejectedCount " + this.m_rejectedCount + " posHypErr " + i + " negHypErr " + j + " m_rateNum " + this.m_rateNum + " m_rateDenom " + this.m_rateDenom);
    boolean bool;
    if (i < j)
    {
      if (this.m_selectionQueue[this.m_head] != 0)
        break label327;
      this.m_selectedCount = (1 + this.m_selectedCount);
      this.m_rejectedCount -= 1;
      this.m_selectionQueue[this.m_head] = true;
      bool = true;
    }
    while (true)
    {
      Log.d("FrameRateReducer", "decided " + bool + " m_selectedCount " + this.m_selectedCount + " m_rejectedCount " + this.m_rejectedCount);
      this.m_head = ((1 + this.m_head) % this.m_selectionQueue.length);
      return bool;
      if (this.m_selectionQueue[this.m_head] != 0)
      {
        this.m_selectedCount -= 1;
        this.m_rejectedCount = (1 + this.m_rejectedCount);
        this.m_selectionQueue[this.m_head] = false;
      }
      bool = false;
      continue;
      label327: bool = true;
    }
  }

  public void setRate(int paramInt1, int paramInt2)
  {
    Log.d("FrameRateReducer", "Setting rate " + paramInt1 + "  " + paramInt2);
    this.m_rateNum = paramInt1;
    this.m_rateDenom = paramInt2;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.FrameRateReducer
 * JD-Core Version:    0.6.2
 */