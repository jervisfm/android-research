package com.sgiggle.VideoCapture;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;

public class OpenmaxBinder
{
  private static native void loadLibraries(String paramString);

  public static void updateContext(Context paramContext)
  {
    String str = paramContext.getApplicationInfo().dataDir;
    if ((Build.MANUFACTURER.toLowerCase().startsWith("htc")) && ((Build.MODEL.startsWith("HTC Sensation")) || (Build.MODEL.startsWith("HTC Bliss")) || (Build.MODEL.startsWith("HTC Rhyme")) || (Build.MODEL.startsWith("HTC Runnymede")) || (Build.MODEL.startsWith("ADR6330"))))
      loadLibraries(str);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.OpenmaxBinder
 * JD-Core Version:    0.6.2
 */