package com.sgiggle.VideoCapture;

import com.sgiggle.util.Log;
import java.util.concurrent.LinkedBlockingQueue;

class PreprocessorThread extends Thread
{
  public static final String TAG = "PreprocessorThread";
  private LinkedBlockingQueue<TimestampedFrame> m_queue = new LinkedBlockingQueue(30);
  private boolean m_shouldBeRunning;
  private VideoCaptureRaw m_videoCapture;

  public PreprocessorThread(VideoCaptureRaw paramVideoCaptureRaw)
  {
    super("PreprocessorThread");
    Log.d("PreprocessorThread", "PreprocessorThread created");
    this.m_shouldBeRunning = true;
    this.m_videoCapture = paramVideoCaptureRaw;
    start();
  }

  private native void captureCallback(byte[] paramArrayOfByte, int paramInt1, int paramInt2, long paramLong);

  private void processFrame(byte[] paramArrayOfByte, long paramLong)
  {
    captureCallback(paramArrayOfByte, this.m_videoCapture.getCameraWidth(), this.m_videoCapture.getCameraHeight(), paramLong);
  }

  public void offer(TimestamperThread paramTimestamperThread, byte[] paramArrayOfByte, long paramLong)
  {
    this.m_queue.offer(new TimestampedFrame(paramTimestamperThread, paramArrayOfByte, paramLong));
  }

  public int queueSize()
  {
    return this.m_queue.size();
  }

  public void run()
  {
    Log.d("PreprocessorThread", "Preprocessor thread started");
    VideoCaptureRaw.registerPrThread();
    while (true)
    {
      if (this.m_shouldBeRunning);
      try
      {
        TimestampedFrame localTimestampedFrame = (TimestampedFrame)this.m_queue.take();
        processFrame(localTimestampedFrame.data, localTimestampedFrame.timestamp);
        localTimestampedFrame.timestamper.returnBufferToCamera(localTimestampedFrame.data);
        continue;
        Log.d("PreprocessorThread", "Preprocessor thread terminated");
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
      }
    }
  }

  public void stopPreprocessor()
  {
    Log.d("PreprocessorThread", "stopPreprocessor");
    this.m_shouldBeRunning = false;
    interrupt();
  }

  private class TimestampedFrame
  {
    public byte[] data;
    public long timestamp;
    TimestamperThread timestamper;

    public TimestampedFrame(TimestamperThread paramArrayOfByte, byte[] paramLong, long arg4)
    {
      this.data = paramLong;
      Object localObject;
      this.timestamp = localObject;
      this.timestamper = paramArrayOfByte;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.PreprocessorThread
 * JD-Core Version:    0.6.2
 */