package com.sgiggle.VideoCapture;

import com.sgiggle.util.Log;

class TimestampPredictor
{
  public static final String TAG = "TimestampPredictor";
  private final double B = 45.0D;
  private final double D = 285.0D;
  private final double K = 0.001212121212121212D;
  private final long KB = 228780L;
  private final long KD = 1448941L;
  private final long KN = 50840L;
  private final int N = 10;
  private final int Q = 4194304;
  private int m_head = 0;
  private int[] m_history = new int[10];
  private int m_numConseqOutliers = 0;
  private int m_warmupIterLeft = 10;
  private long t0 = -1L;

  public TimestampPredictor()
  {
    Log.d("TimestampPredictor", "Intialized: Q=4194304; B=45.0; D=285.0; K=0.0012121212121212121; KB=228780; KD=1448941; KN=50840");
  }

  public long push(long paramLong)
  {
    if (this.t0 == -1L)
      this.t0 = paramLong;
    int i = (int)(paramLong - this.t0);
    if (this.m_warmupIterLeft > 0)
    {
      this.m_history[this.m_head] = i;
      this.m_warmupIterLeft -= 1;
      this.m_head = ((1 + this.m_head) % 10);
      return paramLong;
    }
    int j = this.m_head;
    long l1 = 0L;
    int k = 0;
    long l2 = 0L;
    while (k < 10)
    {
      l1 += this.m_history[((j + k) % 10)];
      l2 += k * this.m_history[((j + k) % 10)];
      k++;
    }
    long l3 = (1448941L * l1 - 228780L * l2) / 4194304L;
    int m = (int)(128L * (l1 * -228780L + 50840L * l2) / 4194304L);
    long l4 = l3 + m * 10 / 128;
    int n = Math.abs((int)(l3 + m * 10 / 128 - i));
    long l5 = 0L;
    for (int i1 = 0; i1 < 10; i1++)
      l5 += Math.abs(l3 + m * i1 / 128 - this.m_history[((j + i1) % 10)]);
    boolean bool = (l5 + n) / 11L < m;
    int i2 = 0;
    if (bool)
      i2 = 1;
    if (n * 128 > m)
      this.m_numConseqOutliers = (1 + this.m_numConseqOutliers);
    if (this.m_numConseqOutliers == 1)
      this.m_history[this.m_head] = ((int)l4);
    while (true)
    {
      this.m_head = ((1 + this.m_head) % 10);
      if (i2 != 0)
        break;
      return l4 + this.t0;
      if (this.m_numConseqOutliers > 1)
      {
        i2 = 1;
      }
      else
      {
        this.m_history[this.m_head] = i;
        this.m_numConseqOutliers = 0;
      }
    }
    this.m_warmupIterLeft = 10;
    this.m_numConseqOutliers = 0;
    this.m_head = 0;
    this.t0 = -1L;
    return paramLong;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.TimestampPredictor
 * JD-Core Version:    0.6.2
 */