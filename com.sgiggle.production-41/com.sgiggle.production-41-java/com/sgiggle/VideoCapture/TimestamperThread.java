package com.sgiggle.VideoCapture;

import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import com.sgiggle.GLES20.GLRenderer;
import com.sgiggle.util.Log;
import java.util.concurrent.Semaphore;

class TimestamperThread extends Thread
  implements Camera.PreviewCallback
{
  static final String TAG = "TimestamperThread";
  final int PREVIEW_CALLBACK_BUFFER_COUNT = 5;
  private final int PREVIEW_INTERVAL = 50;
  private byte[][] m_buffers;
  private Handler m_commandHandler;
  private FrameRateReducer m_frameRateReducer = new FrameRateReducer();
  private Semaphore m_initializationSemaphore;
  private PreprocessorThread m_preprocessorThread;
  private boolean m_slowDownToPreprocessor = false;
  private long m_time_previewed;
  private VideoCaptureRaw m_videoCaptureRaw;

  public TimestamperThread(VideoCaptureRaw paramVideoCaptureRaw, PreprocessorThread paramPreprocessorThread)
  {
    super("TimestamperThread");
    this.m_videoCaptureRaw = paramVideoCaptureRaw;
    this.m_preprocessorThread = paramPreprocessorThread;
    this.m_initializationSemaphore = new Semaphore(0);
    start();
    this.m_initializationSemaphore.acquireUninterruptibly();
  }

  public void addCallbackBuffers()
  {
    Log.d("TimestamperThread", "addCallbackBuffers");
    CameraWrapper localCameraWrapper = this.m_videoCaptureRaw.getCameraWrapper();
    if (localCameraWrapper == null)
    {
      Log.e("TimestamperThread", "CameraWrapper is null");
      return;
    }
    Camera.Parameters localParameters = localCameraWrapper.getParameters();
    PixelFormat localPixelFormat = new PixelFormat();
    PixelFormat.getPixelFormatInfo(localParameters.getPreviewFormat(), localPixelFormat);
    this.m_buffers = new byte[5][];
    for (int i = 0; i < this.m_buffers.length; i++)
      this.m_buffers[i] = new byte[this.m_videoCaptureRaw.getCameraWidth() * this.m_videoCaptureRaw.getCameraHeight() * localPixelFormat.bitsPerPixel / 8];
    localCameraWrapper.setPreviewCallbackWithBuffer(null);
    for (int j = 0; j < this.m_buffers.length; j++)
      localCameraWrapper.addCallbackBuffer(this.m_buffers[j]);
    localCameraWrapper.setPreviewCallbackWithBuffer(this);
  }

  public void autoFocus(Camera.AutoFocusCallback paramAutoFocusCallback)
  {
    AutoFocusCommand localAutoFocusCommand = new AutoFocusCommand(this.m_videoCaptureRaw, false, paramAutoFocusCallback);
    if (Thread.currentThread() == this)
    {
      localAutoFocusCommand.run();
      return;
    }
    this.m_commandHandler.post(localAutoFocusCommand);
  }

  public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera)
  {
    long l = System.currentTimeMillis();
    GLRenderer localGLRenderer = this.m_videoCaptureRaw.getGlRenderer();
    if ((localGLRenderer != null) && (l - this.m_time_previewed > 50L))
    {
      localGLRenderer.render(1, paramArrayOfByte, paramArrayOfByte.length, this.m_videoCaptureRaw.getCameraWidth(), this.m_videoCaptureRaw.getCameraHeight());
      this.m_time_previewed = l;
    }
    int i = this.m_preprocessorThread.queueSize();
    if (i <= 2)
    {
      if (this.m_slowDownToPreprocessor)
        Log.d("TimestamperThread", "Preprocessor queue size is " + i + ". Leave slow down mode.");
      this.m_slowDownToPreprocessor = false;
    }
    if (i >= 5)
    {
      this.m_slowDownToPreprocessor = true;
      Log.d("TimestamperThread", "Preprocessor queue size is " + i + ". Go to slow down mode.");
    }
    int k;
    int j;
    if (this.m_slowDownToPreprocessor)
    {
      k = 30 - i * 3;
      if (k < 0)
        k = 0;
      if (k > 28)
      {
        this.m_frameRateReducer.setRate(1, 1);
        if (this.m_frameRateReducer.nextFrame())
          break label242;
        j = 1;
      }
    }
    while (true)
    {
      if (j == 0)
      {
        this.m_preprocessorThread.offer(this, paramArrayOfByte, l);
        return;
        this.m_frameRateReducer.setRate(k, 28);
        break;
        label242: j = 0;
        continue;
      }
      returnBufferToCamera(paramArrayOfByte);
      return;
      j = 0;
    }
  }

  public void resumeRecording()
  {
    // Byte code:
    //   0: ldc 10
    //   2: ldc 213
    //   4: invokestatic 74	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   7: pop
    //   8: new 215	com/sgiggle/VideoCapture/TimestamperThread$3
    //   11: dup
    //   12: aload_0
    //   13: ldc 217
    //   15: aload_0
    //   16: getfield 49	com/sgiggle/VideoCapture/TimestamperThread:m_videoCaptureRaw	Lcom/sgiggle/VideoCapture/VideoCaptureRaw;
    //   19: iconst_1
    //   20: invokespecial 220	com/sgiggle/VideoCapture/TimestamperThread$3:<init>	(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V
    //   23: astore_2
    //   24: aload_0
    //   25: getfield 143	com/sgiggle/VideoCapture/TimestamperThread:m_commandHandler	Landroid/os/Handler;
    //   28: aload_2
    //   29: invokevirtual 149	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   32: pop
    //   33: return
  }

  public void returnBufferToCamera(byte[] paramArrayOfByte)
  {
    if (Thread.currentThread() != this)
    {
      this.m_commandHandler.post(new ReturnBufferToCameraCommand(this.m_videoCaptureRaw, false, paramArrayOfByte));
      return;
    }
    CameraWrapper localCameraWrapper = this.m_videoCaptureRaw.getCameraWrapper();
    if (localCameraWrapper != null)
    {
      localCameraWrapper.addCallbackBuffer(paramArrayOfByte);
      return;
    }
    Log.w("TimestamperThread", "returnBufferToCamera: cameraWrapper is null");
  }

  public void run()
  {
    Log.d("TimestamperThread", "run");
    VideoCaptureRaw.registerPrThread();
    Process.setThreadPriority(-12);
    Looper.prepare();
    this.m_commandHandler = new Handler();
    this.m_initializationSemaphore.release();
    Looper.loop();
  }

  public void startRecording(int paramInt1, int paramInt2)
  {
    Log.d("TimestamperThread", "startRecording");
    StartRecordingCommand localStartRecordingCommand = new StartRecordingCommand(this.m_videoCaptureRaw, true, paramInt1, paramInt2);
    this.m_time_previewed = System.currentTimeMillis();
    this.m_commandHandler.post(localStartRecordingCommand);
    localStartRecordingCommand.waitForDone();
  }

  public void stopRecording()
  {
    // Byte code:
    //   0: ldc 10
    //   2: ldc_w 263
    //   5: invokestatic 74	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   8: pop
    //   9: new 265	com/sgiggle/VideoCapture/TimestamperThread$1
    //   12: dup
    //   13: aload_0
    //   14: ldc 217
    //   16: aload_0
    //   17: getfield 49	com/sgiggle/VideoCapture/TimestamperThread:m_videoCaptureRaw	Lcom/sgiggle/VideoCapture/VideoCaptureRaw;
    //   20: iconst_1
    //   21: invokespecial 266	com/sgiggle/VideoCapture/TimestamperThread$1:<init>	(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V
    //   24: astore_2
    //   25: aload_0
    //   26: getfield 143	com/sgiggle/VideoCapture/TimestamperThread:m_commandHandler	Landroid/os/Handler;
    //   29: aload_2
    //   30: invokevirtual 149	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   33: pop
    //   34: aload_2
    //   35: invokevirtual 261	com/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand:waitForDone	()V
    //   38: return
  }

  public void stopThread()
  {
    // Byte code:
    //   0: ldc 10
    //   2: ldc_w 268
    //   5: invokestatic 74	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   8: pop
    //   9: new 270	com/sgiggle/VideoCapture/TimestamperThread$4
    //   12: dup
    //   13: aload_0
    //   14: ldc_w 272
    //   17: aload_0
    //   18: getfield 49	com/sgiggle/VideoCapture/TimestamperThread:m_videoCaptureRaw	Lcom/sgiggle/VideoCapture/VideoCaptureRaw;
    //   21: iconst_1
    //   22: invokespecial 273	com/sgiggle/VideoCapture/TimestamperThread$4:<init>	(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V
    //   25: astore_2
    //   26: aload_0
    //   27: getfield 143	com/sgiggle/VideoCapture/TimestamperThread:m_commandHandler	Landroid/os/Handler;
    //   30: aload_2
    //   31: invokevirtual 149	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   34: pop
    //   35: aload_2
    //   36: invokevirtual 261	com/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand:waitForDone	()V
    //   39: return
  }

  public void suspendRecording()
  {
    // Byte code:
    //   0: ldc 10
    //   2: ldc_w 275
    //   5: invokestatic 74	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   8: pop
    //   9: new 277	com/sgiggle/VideoCapture/TimestamperThread$2
    //   12: dup
    //   13: aload_0
    //   14: ldc 217
    //   16: aload_0
    //   17: getfield 49	com/sgiggle/VideoCapture/TimestamperThread:m_videoCaptureRaw	Lcom/sgiggle/VideoCapture/VideoCaptureRaw;
    //   20: iconst_1
    //   21: invokespecial 278	com/sgiggle/VideoCapture/TimestamperThread$2:<init>	(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V
    //   24: astore_2
    //   25: aload_0
    //   26: getfield 143	com/sgiggle/VideoCapture/TimestamperThread:m_commandHandler	Landroid/os/Handler;
    //   29: aload_2
    //   30: invokevirtual 149	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   33: pop
    //   34: aload_2
    //   35: invokevirtual 261	com/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand:waitForDone	()V
    //   38: return
  }

  class AutoFocusCommand extends TimestamperThread.TimestamperCommand
  {
    private Camera.AutoFocusCallback m_autoFocusCallback;

    public AutoFocusCommand(VideoCaptureRaw paramBoolean, boolean paramAutoFocusCallback, Camera.AutoFocusCallback arg4)
    {
      super("Auto Focus", paramBoolean, paramAutoFocusCallback);
      Object localObject;
      this.m_autoFocusCallback = localObject;
    }

    public void commandImpl()
    {
      try
      {
        TimestamperThread.this.m_videoCaptureRaw.getCameraWrapper().autoFocus(this.m_autoFocusCallback);
        return;
      }
      catch (Exception localException)
      {
        Log.w("TimestamperThread", "autoFocus failed: " + localException);
      }
    }
  }

  class ReturnBufferToCameraCommand extends TimestamperThread.TimestamperCommand
  {
    private byte[] m_buffer;

    public ReturnBufferToCameraCommand(VideoCaptureRaw paramBoolean, boolean paramArrayOfByte, byte[] arg4)
    {
      super("Return buffer", paramBoolean, paramArrayOfByte);
      Object localObject;
      this.m_buffer = localObject;
    }

    public void commandImpl()
    {
      TimestamperThread.this.returnBufferToCamera(this.m_buffer);
    }
  }

  class StartRecordingCommand extends TimestamperThread.TimestamperCommand
  {
    private int m_address;
    private int m_data;

    public StartRecordingCommand(VideoCaptureRaw paramBoolean, boolean paramInt1, int paramInt2, int arg5)
    {
      super("Start recording", paramBoolean, paramInt1);
      this.m_address = paramInt2;
      int i;
      this.m_data = i;
    }

    public void commandImpl()
    {
      this.m_videoCapture.doStartRecording(this.m_address, this.m_data);
    }
  }

  abstract class TimestamperCommand
    implements Runnable
  {
    private Semaphore doneSemaphore;
    protected VideoCaptureRaw m_videoCapture;
    private String name;

    public TimestamperCommand(String paramVideoCaptureRaw, VideoCaptureRaw paramBoolean, boolean arg4)
    {
      int i;
      if (i != 0);
      for (this.doneSemaphore = new Semaphore(0); ; this.doneSemaphore = null)
      {
        this.name = paramVideoCaptureRaw;
        this.m_videoCapture = paramBoolean;
        return;
      }
    }

    abstract void commandImpl();

    public String getName()
    {
      return this.name;
    }

    public void notifyDone()
    {
      if (this.doneSemaphore != null)
        this.doneSemaphore.release();
    }

    public final void run()
    {
      commandImpl();
      notifyDone();
    }

    public void waitForDone()
    {
      if (this.doneSemaphore != null)
      {
        this.doneSemaphore.acquireUninterruptibly();
        return;
      }
      Log.w("TimestamperThread", "Trying to waiting for done on a non synchronized command. Will exit immediatly.");
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.TimestamperThread
 * JD-Core Version:    0.6.2
 */