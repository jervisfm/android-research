package com.sgiggle.VideoCapture;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;

public class VSoftCodec
{
  private static native void loadLibraries(String paramString, int paramInt);

  public static void updateContext(Context paramContext)
  {
    String str = paramContext.getApplicationInfo().dataDir;
    int i = -1;
    if ((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("SPH-M900")) || (Build.MODEL.equals("SPH-M910")) || (Build.MODEL.equals("SPH-M920"))))
      i = 2;
    loadLibraries(str, i);
  }

  private static abstract interface libType
  {
    public static final int NONE = -1;
    public static final int v6 = 2;
    public static final int v7 = 1;
    public static final int v7_NEON;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.VSoftCodec
 * JD-Core Version:    0.6.2
 */