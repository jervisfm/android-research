package com.sgiggle.VideoCapture;

import android.hardware.Camera;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.media.MediaRecorder.OnInfoListener;
import android.view.SurfaceHolder;
import com.sgiggle.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;

public class VideoCapture
  implements MediaRecorder.OnErrorListener, MediaRecorder.OnInfoListener
{
  private static final String TAG = "VideoCapture";
  public static VideoCapture sInstance = null;
  private Camera camera;
  private FileDescriptor clientFd;
  private FakeMediaRecorder fakeRecorder;
  private String filename;
  private int frameRate = 10;
  private int height = 144;
  private SurfaceHolder holder;
  private boolean isCreateData = false;
  private boolean isPreviewing;
  private boolean isRecording;
  private boolean isUseCamera = false;
  private boolean isUseFakeMediaRecorder = false;
  private boolean isUseNative = true;
  private MediaRecorder recorder;
  private FileDescriptor serverFd;
  private int width = 176;

  static
  {
    System.loadLibrary("S1");
  }

  public VideoCapture()
  {
    sInstance = this;
  }

  private void closeCamera()
  {
    Log.v("VideoCapture", "closeCamera");
    this.camera.lock();
    this.camera.stopPreview();
    this.camera.release();
    this.camera = null;
  }

  private void closeRecorder()
  {
    Log.v("VideoCapture", "closeRecorder");
    if (this.isUseNative)
      callNativeStop();
    if (this.isUseFakeMediaRecorder)
    {
      this.fakeRecorder = null;
      return;
    }
    this.recorder.reset();
    this.recorder.release();
    this.recorder = null;
  }

  // ERROR //
  private FileDescriptor getFD(String paramString)
  {
    // Byte code:
    //   0: new 110	java/io/File
    //   3: dup
    //   4: aload_1
    //   5: invokespecial 112	java/io/File:<init>	(Ljava/lang/String;)V
    //   8: astore_2
    //   9: aload_2
    //   10: invokevirtual 116	java/io/File:createNewFile	()Z
    //   13: pop
    //   14: aload_2
    //   15: ldc 117
    //   17: invokestatic 123	android/os/ParcelFileDescriptor:open	(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    //   20: invokevirtual 127	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
    //   23: astore 6
    //   25: aload 6
    //   27: areturn
    //   28: astore_3
    //   29: aload_3
    //   30: invokevirtual 130	java/lang/Exception:printStackTrace	()V
    //   33: aconst_null
    //   34: areturn
    //   35: astore 5
    //   37: aload 5
    //   39: invokevirtual 131	java/io/FileNotFoundException:printStackTrace	()V
    //   42: aconst_null
    //   43: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   0	14	28	java/lang/Exception
    //   14	25	35	java/io/FileNotFoundException
  }

  private native int nativeSetup(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2);

  private native int nativeStart(FileDescriptor paramFileDescriptor, String paramString);

  private native int nativeStop(FileDescriptor paramFileDescriptor);

  private void setupCamera()
    throws Exception
  {
    Log.v("VideoCapture", "setupCamera");
    try
    {
      this.camera = Camera.open();
      this.camera.lock();
      this.camera.setPreviewDisplay(null);
      this.camera.startPreview();
      this.camera.setPreviewDisplay(this.holder);
      this.camera.unlock();
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      this.camera.release();
      this.camera = null;
      throw localException;
    }
  }

  private void setupRecorder(String paramString)
    throws Exception
  {
    Log.v("VideoCapture", "setupRecorder");
    if (this.isCreateData == true)
    {
      this.isUseNative = true;
      callNativeStart(this.filename);
    }
    while (this.isUseFakeMediaRecorder)
    {
      this.fakeRecorder = new FakeMediaRecorder();
      this.fakeRecorder.setOutputFile(this.serverFd);
      this.fakeRecorder.setInputFile(this.filename);
      return;
      if (paramString.charAt(0) == '/')
      {
        this.isUseNative = false;
        this.serverFd = getFD(paramString);
      }
      else
      {
        this.isUseNative = true;
        callNativeStart(paramString);
      }
    }
    try
    {
      this.recorder = new MediaRecorder();
      if (this.isUseCamera)
        this.recorder.setCamera(this.camera);
      this.recorder.setVideoSource(1);
      this.recorder.setOutputFormat(2);
      this.recorder.setVideoSize(this.width, this.height);
      this.recorder.setVideoFrameRate(this.frameRate);
      this.recorder.setVideoEncoder(2);
      this.recorder.setOutputFile(this.serverFd);
      this.recorder.setPreviewDisplay(this.holder.getSurface());
      this.recorder.prepare();
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      this.recorder.release();
      this.recorder = null;
      throw localException;
    }
  }

  private void startRecorder()
    throws Exception
  {
    Log.v("VideoCapture", "startRecorder");
    if (this.isUseFakeMediaRecorder)
    {
      this.fakeRecorder.start();
      return;
    }
    this.recorder.setOnErrorListener(this);
    this.recorder.setOnInfoListener(this);
    this.recorder.start();
  }

  private void stopRecorder()
  {
    Log.v("VideoCapture", "stopRecorder");
    if (this.isUseFakeMediaRecorder)
    {
      this.fakeRecorder.stop();
      return;
    }
    this.recorder.setOnErrorListener(null);
    this.recorder.setOnInfoListener(null);
    this.recorder.stop();
  }

  public void callNativeStart(String paramString)
    throws Exception
  {
    this.serverFd = new FileDescriptor();
    this.clientFd = new FileDescriptor();
    if (nativeSetup(this.serverFd, this.clientFd) != 0)
      throw new IOException("nativeSetup failed");
    if (nativeStart(this.clientFd, paramString) != 0)
    {
      this.clientFd = null;
      throw new IOException("nativeStart failed");
    }
  }

  public void callNativeStop()
  {
    if (this.clientFd != null)
    {
      nativeStop(this.clientFd);
      this.clientFd = null;
    }
  }

  public void onError(MediaRecorder paramMediaRecorder, int paramInt1, int paramInt2)
  {
    Log.e("VideoCapture", "onError" + paramMediaRecorder + " " + paramInt1 + " " + paramInt2);
  }

  public void onInfo(MediaRecorder paramMediaRecorder, int paramInt1, int paramInt2)
  {
    Log.e("VideoCapture", "onInfo" + paramInt1 + " " + paramInt2);
  }

  public void setCaptureFile(String paramString)
  {
    if ((this.isRecording) || (this.isPreviewing))
      throw new IllegalStateException("already started");
    if (paramString == null)
    {
      this.isCreateData = false;
      return;
    }
    this.filename = paramString;
    this.isCreateData = true;
  }

  public void setCapturedFile(String paramString)
  {
    if ((this.isRecording) || (this.isPreviewing))
      throw new IllegalStateException("already started");
    if (paramString == null)
    {
      this.isUseFakeMediaRecorder = false;
      return;
    }
    this.filename = paramString;
    this.isUseFakeMediaRecorder = true;
  }

  public void setPreviewDisplay(SurfaceHolder paramSurfaceHolder)
    throws IllegalStateException
  {
    if ((this.isRecording) || (this.isPreviewing))
      throw new IllegalStateException("already started");
    this.holder = paramSurfaceHolder;
  }

  public void setVideoFormat(int paramInt)
    throws IllegalStateException
  {
    if (this.isRecording)
      throw new IllegalStateException("already started");
  }

  public void setVideoFrameRate(int paramInt)
    throws IllegalStateException
  {
    if (this.isRecording)
      throw new IllegalStateException("already started");
    this.frameRate = paramInt;
  }

  public void setVideoSize(int paramInt)
    throws IllegalStateException
  {
    if (this.isRecording)
      throw new IllegalStateException("already started");
    switch (paramInt)
    {
    default:
      return;
    case 0:
      this.width = 176;
      this.height = 144;
      return;
    case 1:
      this.width = 352;
      this.height = 288;
      return;
    case 2:
    }
    this.width = 740;
    this.height = 480;
  }

  public void startPreview()
    throws Exception
  {
    if (this.isPreviewing)
      throw new IllegalStateException("already previewing");
    if (this.isRecording)
      throw new IllegalStateException("already recording");
    setupCamera();
    this.isPreviewing = true;
  }

  public void startRecording(String paramString)
    throws Exception
  {
    if (this.isRecording)
      throw new IllegalStateException("already recording");
    if (this.isPreviewing)
      closeCamera();
    try
    {
      setupRecorder(paramString);
      startRecorder();
      this.isRecording = true;
      return;
    }
    catch (Exception localException)
    {
      throw localException;
    }
  }

  public void stopPreview()
  {
    if (!this.isPreviewing)
      return;
    closeCamera();
    this.isPreviewing = false;
  }

  public void stopRecording()
  {
    if (!this.isRecording)
      return;
    stopRecorder();
    closeRecorder();
    if (this.isPreviewing);
    try
    {
      setupCamera();
      label27: this.isRecording = false;
      return;
    }
    catch (Exception localException)
    {
      break label27;
    }
  }

  public static abstract interface VideoFormat
  {
    public static final int H264 = 0;
    public static final int RAW = 1;
  }

  public static abstract interface VideoSize
  {
    public static final int SIZE_720x480 = 2;
    public static final int SIZE_CIF = 1;
    public static final int SIZE_QCIF;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.VideoCapture
 * JD-Core Version:    0.6.2
 */