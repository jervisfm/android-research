package com.sgiggle.VideoCapture;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import com.sgiggle.GLES20.GLCapture;
import com.sgiggle.GLES20.GLRenderer;
import com.sgiggle.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class VideoCaptureRaw
  implements Camera.AutoFocusCallback, SensorEventListener
{
  private static final int CAMERA_HEIGHT = 240;
  private static final int CAMERA_WIDTH = 320;
  private static final int CAPTURE_HEIGHT = 128;
  private static final int CAPTURE_VGA_HEIGHT = 416;
  private static final int CAPTURE_VGA_WIDTH = 624;
  private static final int CAPTURE_WIDTH = 192;
  public static final int COMMAND_START = 0;
  public static final int COMMAND_STOP = 1;
  private static final int HVGA_HEIGHT = 320;
  private static final int HVGA_WIDTH = 480;
  public static final int MESSAGE_UPDATE_LAYOUT = 2;
  private static final float RATIO_CAPTURE = 1.5F;
  private static final int ROTATION_TRANSFORM_MIRROR_ROTATE270_CLIP = 3;
  private static final int ROTATION_TRANSFORM_MIRROR_UPSIDEDOWN = 1;
  private static final int ROTATION_TRANSFORM_NONE = 0;
  private static final int ROTATION_TRANSFORM_ROTATE270_MIRROR_CLIP = 4;
  private static final int ROTATION_TRANSFORM_UPSIDEDOWN = 2;
  private static final Comparator<Camera.Size> SizeComparator;
  public static final String TAG = "VideoCaptureRaw";
  private static final int VGA_HEIGHT = 480;
  private static final int VGA_WIDTH = 640;
  private static SurfaceHolder[] _holder;
  private static int[] cameraIds;
  private static List<Camera.Size>[] cameraSizes;
  private static int[] camera_height;
  private static int[] camera_width;
  public static int[] capture_height;
  public static int[] capture_rotation;
  public static int[] capture_width;
  private static final Size[] crop_size;
  private static int current_camera;
  private static boolean hasBackCamera;
  private static boolean hasFrontCamera;
  private static boolean[] isGetPreviewSizeInitialized;
  private static boolean isGingerbread;
  private static boolean isInitialized;
  private static boolean isSamsungTablet10Dot1;
  private static boolean isSamsungTablet7Inch;
  private static PreprocessorThread preprocessor;
  private static VideoCaptureRaw[] sInstance = { null, null };
  private static int screenHeight;
  private static int screenWidth;
  private static SensorManager sm;
  private static TimestamperThread timestamper;
  private static Handler ui_handler;
  private static int userFrontCameraRotation;
  private static WindowManager wm;
  private boolean _stopped = false;
  private boolean _suspended = false;
  private Sensor accSensor;
  private float acc_x;
  private float acc_y;
  private float acc_z;
  private CameraWrapper camera;
  private int frameRate = 10;
  private GLCapture glCapture;
  private GLRenderer glRenderer;
  private boolean glRendererHasCapture;
  private boolean isFocusing;
  private boolean isFoundPeak;
  private boolean isSupportAutoFocus;
  private int maxHeight = 128;
  private int maxWidth = 192;
  private float threshold_high = 2.0F;
  private float threshold_low = 0.2F;
  private float threshold_stable = 0.5F;
  private int type;
  private float value_peak;
  private float value_prev;
  private float weight_now = 0.1F;
  private float weight_prev = 1.0F - this.weight_now;

  static
  {
    _holder = new SurfaceHolder[] { null, null };
    current_camera = -1;
    isGetPreviewSizeInitialized = new boolean[] { 0, 0 };
    cameraSizes = new List[2];
    camera_width = new int[] { 320, 320 };
    camera_height = new int[] { 240, 240 };
    capture_width = new int[] { 192, 192 };
    capture_height = new int[] { 128, 128 };
    capture_rotation = new int[] { 0, 0 };
    cameraIds = new int[] { -1, -1 };
    userFrontCameraRotation = -1;
    SizeComparator = new Comparator()
    {
      public int compare(Camera.Size paramAnonymousSize1, Camera.Size paramAnonymousSize2)
      {
        int i = paramAnonymousSize1.width * paramAnonymousSize1.height;
        int j = paramAnonymousSize2.width * paramAnonymousSize2.height;
        if (i < j)
          return -1;
        if (i == j)
          return 0;
        return 1;
      }
    };
    Size[] arrayOfSize = new Size[6];
    arrayOfSize[0] = new Size(192, 128);
    arrayOfSize[1] = new Size(240, 160);
    arrayOfSize[2] = new Size(320, 214);
    arrayOfSize[3] = new Size(384, 256);
    arrayOfSize[4] = new Size(480, 320);
    arrayOfSize[5] = new Size(576, 384);
    crop_size = arrayOfSize;
    StackTraceElement[] arrayOfStackTraceElement = new Throwable().getStackTrace();
    int i = 0;
    if (i < arrayOfStackTraceElement.length)
      if (!arrayOfStackTraceElement[i].getClassName().startsWith("com.sgiggle.VideoCaptureTest"));
    for (int j = 1; ; j = 0)
    {
      if (j != 0)
        System.loadLibrary("VideoCapture");
      return;
      i++;
      break;
    }
  }

  public VideoCaptureRaw(int paramInt1, int paramInt2, int paramInt3)
  {
    Log.v("VideoCaptureRaw", "VideoCaptureRaw(type=" + this.type + " maxWidth=" + paramInt2 + " maxHeight=" + paramInt3 + ")");
    initCameraSize(paramInt2, paramInt3);
    if (paramInt1 == 1)
      this.type = 0;
    while (true)
      try
      {
        sInstance[this.type] = this;
        if (GLRenderer.isSupported())
        {
          this.glRenderer = GLRenderer.getInstance();
          if (GLRenderer.hasGLCapture())
            this.glCapture = GLCapture.getInstance();
          if (GLRenderer.isCaptureSupported())
            this.glRendererHasCapture = true;
        }
        return;
        this.type = 1;
      }
      finally
      {
      }
  }

  public static boolean checkCamera(int paramInt)
  {
    if (Build.VERSION.SDK_INT >= 9)
      isGingerbread = true;
    initCameraIds();
    if (paramInt == 1)
      return hasBackCamera;
    if (paramInt == 2)
      return hasFrontCamera;
    return false;
  }

  public static int getCameraCount()
  {
    boolean bool = hasBackCamera;
    int i = 0;
    if (bool)
      i = 0 + 1;
    if (hasFrontCamera)
      i++;
    return i;
  }

  public static void getPreviewSize(int paramInt1, int paramInt2, int paramInt3)
  {
    Log.v("VideoCaptureRaw", "getPreviewSize() cameraType=" + paramInt1 + " maxWidth=" + paramInt2 + " maxHeight=" + paramInt3);
    camera_width[paramInt1] = 0;
    camera_height[paramInt1] = 0;
    int i = cameraIds[paramInt1];
    if (isGetPreviewSizeInitialized[paramInt1] == 0)
    {
      Camera localCamera = CameraWrapper.openCameraSafe(i);
      if (localCamera != null)
      {
        if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.equals("PC36100")))
        {
          localCamera.startPreview();
          localCamera.stopPreview();
        }
        cameraSizes[paramInt1] = localCamera.getParameters().getSupportedPreviewSizes();
        localCamera.release();
        Collections.sort(cameraSizes[paramInt1], SizeComparator);
        for (int m = 0; m < cameraSizes[paramInt1].size(); m++)
        {
          Camera.Size localSize2 = (Camera.Size)cameraSizes[paramInt1].get(m);
          Log.v("VideoCaptureRaw", "preview size of camera " + paramInt1 + " " + localSize2.width + " " + localSize2.height);
        }
      }
      isGetPreviewSizeInitialized[paramInt1] = true;
    }
    int j;
    if (cameraSizes[paramInt1] != null)
    {
      j = 0;
      if (j < cameraSizes[paramInt1].size())
      {
        Camera.Size localSize1 = (Camera.Size)cameraSizes[paramInt1].get(j);
        if ((localSize1.width < paramInt2) || (localSize1.height < paramInt3))
          break label516;
        camera_width[paramInt1] = localSize1.width;
        camera_height[paramInt1] = localSize1.height;
      }
      if ((camera_width[paramInt1] > 0) && (camera_height[paramInt1] > 0))
      {
        int k = setPreviewSize(camera_width[paramInt1], camera_height[paramInt1]);
        if ((k < 0) || (k >= crop_size.length))
          break label522;
        capture_width[paramInt1] = crop_size[k].width;
        capture_height[paramInt1] = crop_size[k].height;
        Log.v("VideoCaptureRaw", "getPreviewSize returns camera: (" + camera_width[paramInt1] + "x" + camera_height[paramInt1] + "), crop: (" + capture_width[paramInt1] + "x" + capture_height[paramInt1] + ")");
      }
    }
    while (true)
    {
      if ((camera_width[paramInt1] == 0) || (camera_height[paramInt1] == 0))
      {
        capture_width[paramInt1] = paramInt2;
        capture_height[paramInt1] = paramInt3;
        Log.e("VideoCaptureRaw", "Cannot get supported preview size!");
      }
      return;
      label516: j++;
      break;
      label522: camera_width[paramInt1] = 0;
      camera_height[paramInt1] = 0;
    }
  }

  private void initBackCameraSize()
  {
    if ((!initBackCameraSizeHardCoded()) && (!selectCameraSizeForHTC(0, this.maxWidth, this.maxHeight)))
      getPreviewSize(0, this.maxWidth, this.maxHeight);
  }

  public static boolean initBackCameraSizeHardCoded()
  {
    if ((!isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Motorola") == 0) && (Build.MODEL.equals("DROID")))
    {
      camera_width[0] = 320;
      camera_height[0] = 240;
      capture_width[0] = 320;
      capture_height[0] = 214;
      return true;
    }
    if ((!isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0))
    {
      if ((Build.MODEL.equals("GT-I9003")) || (Build.MODEL.equals("GT-I9003L")))
      {
        camera_width[0] = 320;
        camera_height[0] = 240;
        capture_width[0] = 320;
        capture_height[0] = 214;
      }
      while (true)
      {
        return true;
        if ((Build.MODEL.equals("SPH-M820-BST")) || (Build.MODEL.startsWith("SCH-M828C")))
        {
          camera_width[0] = 240;
          camera_height[0] = 160;
          capture_width[0] = 240;
          capture_height[0] = 160;
        }
        else if (Build.MODEL.equals("GT-P1010"))
        {
          camera_width[0] = 320;
          camera_height[0] = 240;
          capture_width[0] = camera_height[0];
          capture_height[0] = ((int)(capture_width[0] / 1.5F));
        }
        else
        {
          camera_width[0] = 256;
          camera_height[0] = 192;
          capture_width[0] = 240;
          capture_height[0] = 160;
        }
      }
    }
    if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("SPH-D700")) || (Build.MODEL.equals("SGH-T759"))))
    {
      camera_width[0] = 192;
      camera_height[0] = 144;
      capture_width[0] = 192;
      capture_height[0] = 128;
      return true;
    }
    if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("GT-I9003")) || (Build.MODEL.equals("GT-I9003L"))))
    {
      camera_width[0] = 320;
      camera_height[0] = 240;
      capture_width[0] = 320;
      capture_height[0] = 214;
      return true;
    }
    if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (Build.MODEL.equals("GT-I9103")))
    {
      camera_width[0] = 192;
      camera_height[0] = 128;
      capture_width[0] = 192;
      capture_height[0] = 128;
      return true;
    }
    if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("SPH-D710")) || (Build.MODEL.equals("SHW-M250S")) || (Build.MODEL.equals("GT-I9100"))))
    {
      camera_width[0] = 640;
      camera_height[0] = 480;
      capture_width[0] = 576;
      capture_height[0] = 384;
      return true;
    }
    if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("SGH-T989")) || (Build.MODEL.equals("SAMSUNG-SGH-I727"))))
    {
      camera_width[0] = 480;
      camera_height[0] = 320;
      capture_width[0] = 480;
      capture_height[0] = 320;
      return true;
    }
    if (((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (Build.MODEL.equals("GT-P7510"))) || ((Build.MANUFACTURER.compareToIgnoreCase("asus") == 0) && (Build.MODEL.equals("Transformer TF101"))))
    {
      camera_width[0] = 480;
      camera_height[0] = 320;
      capture_width[0] = 480;
      capture_height[0] = 320;
      return true;
    }
    if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.equals("PC36100")))
    {
      camera_width[0] = 240;
      camera_height[0] = 160;
      capture_width[0] = 240;
      capture_height[0] = 160;
      return true;
    }
    if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.equals("PG06100")))
    {
      camera_width[0] = 240;
      camera_height[0] = 160;
      capture_width[0] = 240;
      capture_height[0] = 160;
      return true;
    }
    if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.equals("ADR6400L")))
    {
      camera_width[0] = 240;
      camera_height[0] = 160;
      capture_width[0] = 240;
      capture_height[0] = 160;
      return true;
    }
    if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.startsWith("HTC Desire S")))
    {
      camera_width[0] = 192;
      camera_height[0] = 144;
      capture_width[0] = 192;
      capture_height[0] = 128;
      return true;
    }
    if (Build.MODEL.equals("LG-MS910"))
    {
      camera_width[0] = 240;
      camera_height[0] = 160;
      capture_width[0] = 240;
      capture_height[0] = 160;
      return true;
    }
    if ((Build.MANUFACTURER.compareToIgnoreCase("lge") == 0) && (Build.MODEL.startsWith("VS910 4G")))
    {
      camera_width[0] = 240;
      camera_height[0] = 160;
      capture_width[0] = 240;
      capture_height[0] = 160;
      return true;
    }
    if ((Build.MANUFACTURER.compareToIgnoreCase("lge") == 0) && (Build.MODEL.startsWith("LG-P925")))
    {
      camera_width[0] = 320;
      camera_height[0] = 240;
      capture_width[0] = 320;
      capture_height[0] = 214;
      return true;
    }
    if ((Build.MANUFACTURER.compareToIgnoreCase("lge") == 0) && (Build.MODEL.startsWith("LG-P999")))
    {
      camera_width[0] = 192;
      camera_height[0] = 128;
      capture_width[0] = 192;
      capture_height[0] = 128;
      return true;
    }
    if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("HUAWEI") == 0) && (Build.MODEL.equals("M886")))
    {
      camera_width[0] = 240;
      camera_height[0] = 160;
      capture_width[0] = 240;
      capture_height[0] = 160;
      if (Build.VERSION.RELEASE.compareTo("2.3.5") >= 0)
        capture_rotation[0] = 2;
      while (true)
      {
        return true;
        capture_rotation[0] = 1;
      }
    }
    if ((isGingerbread) && (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) && (Build.MODEL.equals("HUAWEI-M920")))
    {
      camera_width[0] = 240;
      camera_height[0] = 160;
      capture_width[0] = 240;
      capture_height[0] = 160;
      return true;
    }
    if ((isGingerbread) && (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) && (Build.MODEL.equals("HUAWEI-M921")))
    {
      camera_width[0] = 240;
      camera_height[0] = 160;
      capture_width[0] = 240;
      capture_height[0] = 160;
      capture_rotation[0] = 1;
      return true;
    }
    if (Build.MODEL.startsWith("Galaxy Nexus"))
    {
      camera_width[0] = 640;
      camera_height[0] = 480;
      capture_width[0] = 576;
      capture_height[0] = 384;
      return true;
    }
    return false;
  }

  private static void initCameraIds()
  {
    if (isInitialized)
      return;
    isInitialized = true;
    int i = CameraWrapper.getNumberOfCameras();
    CameraWrapper.CameraInfo localCameraInfo = new CameraWrapper.CameraInfo();
    int j = 0;
    label25: if (j < i)
    {
      CameraWrapper.getCameraInfo(j, localCameraInfo);
      if ((hasBackCamera) || (localCameraInfo.facing != 0))
        break label64;
      cameraIds[0] = j;
      hasBackCamera = true;
    }
    while (true)
    {
      j++;
      break label25;
      break;
      label64: if ((!hasFrontCamera) && (localCameraInfo.facing == 1))
      {
        cameraIds[1] = j;
        hasFrontCamera = true;
      }
    }
  }

  private void initCameraSize(int paramInt1, int paramInt2)
  {
    Log.v("VideoCaptureRaw", "initCameraSize( " + paramInt1 + "x" + paramInt2 + " )");
    this.maxWidth = paramInt1;
    this.maxHeight = paramInt2;
    if (hasBackCamera)
      initBackCameraSize();
    if (hasFrontCamera)
      initFrontCameraSize();
    if (userFrontCameraRotation != -1)
      capture_rotation[1] = userFrontCameraRotation;
    VideoView.setCameraSize(0, camera_width[0], camera_height[0]);
    if (((capture_rotation[1] == 3) || (capture_rotation[1] == 4)) && ((!isGingerbread) || (Build.MANUFACTURER.compareToIgnoreCase("Samsung") != 0) || ((!Build.MODEL.equals("GT-I9003")) && (!Build.MODEL.equals("GT-I9003L")))))
    {
      VideoView.setCameraSize(1, camera_height[1], camera_width[1]);
      VideoView.setCaptureSize(0, capture_width[0], capture_height[0]);
      if (capture_width[1] <= capture_height[1])
        break label394;
      VideoView.setCaptureSize(1, capture_width[1], capture_height[1]);
    }
    while (true)
    {
      Log.v("VideoCaptureRaw", "[front] camera: " + camera_width[1] + "x" + camera_height[1] + " crop: " + capture_width[1] + "x" + capture_height[1] + " rotation: " + capture_rotation[1] + " [back] camera: " + camera_width[0] + "x" + camera_height[0] + " crop: " + capture_width[0] + "x" + capture_height[0] + " rotation: " + capture_rotation[0]);
      updateLayout();
      return;
      VideoView.setCameraSize(1, camera_width[1], camera_height[1]);
      break;
      label394: VideoView.setCaptureSize(1, capture_height[1], capture_width[1]);
    }
  }

  public static List<Integer> initFPSRange(Camera.Parameters paramParameters, int paramInt1, int paramInt2)
  {
    new int[] { 0, 0 };
    List localList = paramParameters.getSupportedPreviewFpsRange();
    Iterator localIterator2;
    Object localObject;
    int[] arrayOfInt1;
    int k;
    int m;
    if (!localList.isEmpty())
    {
      Iterator localIterator1 = localList.iterator();
      while (localIterator1.hasNext())
      {
        int[] arrayOfInt2 = (int[])localIterator1.next();
        Log.d("VideoCaptureRaw", "getSupportedPreviewFpsRange() returned:" + Arrays.toString(arrayOfInt2));
      }
      localIterator2 = localList.iterator();
      localObject = new int[] { 0, 0 };
      if (!localIterator2.hasNext())
        break label399;
      arrayOfInt1 = (int[])localIterator2.next();
      if (rangesIntersect(arrayOfInt1[0], arrayOfInt1[1], paramInt1 * 1000, paramInt2 * 1000))
      {
        k = arrayOfInt1[0];
        m = arrayOfInt1[1];
      }
    }
    while (true)
    {
      try
      {
        paramParameters.setPreviewFpsRange(k, m);
        Log.v("VideoCaptureRaw", "setPreviewFpsRange(" + k + "," + m + ")");
        Integer[] arrayOfInteger = new Integer[2];
        arrayOfInteger[0] = Integer.valueOf(k);
        arrayOfInteger[1] = Integer.valueOf(m);
        return Arrays.asList(arrayOfInteger);
        if (paramInt2 * 1000 < arrayOfInt1[0])
        {
          if (localObject[1] > 0)
          {
            int n = localObject[0];
            int i1 = localObject[1];
            k = n;
            m = i1;
            continue;
          }
          k = arrayOfInt1[0];
          m = arrayOfInt1[1];
          continue;
        }
        if (localIterator2.hasNext())
        {
          localObject = arrayOfInt1;
          break;
        }
        k = arrayOfInt1[0];
        m = arrayOfInt1[1];
        continue;
        int i = paramInt1 * 1000;
        int j = paramInt2 * 1000;
        k = i;
        m = j;
        continue;
      }
      catch (Throwable localThrowable)
      {
        Log.e("VideoCaptureRaw", "Exception setPreviewFpsRange(): " + localThrowable);
        continue;
      }
      label399: m = 0;
      k = 0;
    }
  }

  public static int initFrameRate(Camera.Parameters paramParameters, int paramInt)
  {
    Object localObject;
    int j;
    int k;
    try
    {
      List localList = paramParameters.getSupportedPreviewFrameRates();
      localObject = localList;
      if (localObject == null)
        return 0;
    }
    catch (Exception localException1)
    {
      while (true)
      {
        ArrayList localArrayList = new ArrayList();
        localArrayList.add(Integer.valueOf(paramInt));
        localObject = localArrayList;
      }
      Collections.sort((List)localObject);
      for (int i = 0; i < ((List)localObject).size(); i++)
        Log.d("VideoCaptureRaw", "getSupportedPreviewFrameRates " + ((List)localObject).get(i));
      j = 0;
      k = paramInt;
    }
    while (true)
    {
      if (j < ((List)localObject).size())
      {
        k = ((Integer)((List)localObject).get(j)).intValue();
        if (k < paramInt);
      }
      else
      {
        try
        {
          paramParameters.setPreviewFrameRate(k);
          Log.v("VideoCaptureRaw", "setPreviewFrameRate(" + k + ")");
          return k;
        }
        catch (Exception localException2)
        {
          Log.e("VideoCaptureRaw", "Exception setPreviewFrameRate(): " + localException2);
          return k;
        }
      }
      j++;
    }
  }

  private void initFrontCameraSize()
  {
    Log.v("VideoCaptureRaw", "init front camera (manufacturer=" + Build.MANUFACTURER + " model=" + Build.MODEL + " version=" + Build.VERSION.SDK_INT + ")");
    if ((!initFrontCameraSizeHardCoded()) && (!selectCameraSizeForHTC(1, this.maxWidth, this.maxHeight)))
    {
      getPreviewSize(1, this.maxWidth, this.maxHeight);
      if (!isGingerbread)
        break label254;
      if ((!Build.MODEL.equals("HTC Flyer P510e")) && (!Build.MODEL.equals("HTC Glacier")) && (!Build.MODEL.startsWith("LG-P990")) && (!Build.MODEL.equals("LG-SU660")) && (!Build.MODEL.equals("LG-SU760")) && ((!Build.MODEL.startsWith("HTC Incredible S")) || (Build.VERSION.RELEASE.compareTo("2.3.5") >= 0)) && ((!Build.MODEL.startsWith("GT-I9001")) || (Build.VERSION.RELEASE.compareTo("2.3.5") < 0) || (Build.VERSION.SDK_INT >= 14)))
        break label245;
      capture_rotation[1] = 2;
    }
    while (true)
    {
      if ((Build.MODEL.equals("HTC Flyer P510e")) && (Build.VERSION.SDK_INT >= 11))
        capture_rotation[1] = 1;
      return;
      label245: capture_rotation[1] = 1;
    }
    label254: capture_rotation[1] = 2;
  }

  public static boolean initFrontCameraSizeHardCoded()
  {
    if (Build.MODEL.equals("Nexus S"))
    {
      camera_width[1] = 320;
      camera_height[1] = 240;
      capture_width[1] = 320;
      capture_height[1] = 214;
      capture_rotation[1] = 1;
      return true;
    }
    if ((!isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0))
    {
      if ((Build.MODEL.equals("SPH-M920")) || (Build.MODEL.equals("SGH-T959V")) || (Build.MODEL.equals("SCH-I510")) || (Build.MODEL.equals("SGH-T839")) || (Build.MODEL.equals("SAMSUNG-SGH-I997")) || (Build.MODEL.equals("YP-GB1")) || (Build.MODEL.equals("YP-G1")))
      {
        camera_width[1] = 192;
        camera_height[1] = 144;
        capture_width[1] = 192;
        capture_height[1] = 128;
        capture_rotation[1] = 2;
      }
      while (true)
      {
        return true;
        if ((Build.MODEL.equals("GT-I9003")) || (Build.MODEL.equals("GT-I9003L")))
        {
          camera_width[1] = 176;
          camera_height[1] = 144;
          capture_width[1] = 96;
          capture_height[1] = 144;
          capture_rotation[1] = 3;
        }
        else if (Build.MODEL.equals("GT-P1010"))
        {
          camera_width[1] = 320;
          camera_height[1] = 240;
          capture_width[1] = 160;
          capture_height[1] = 240;
          capture_rotation[1] = 3;
        }
        else
        {
          camera_width[1] = 256;
          camera_height[1] = 192;
          capture_width[1] = 128;
          capture_height[1] = 192;
          capture_rotation[1] = 3;
        }
      }
    }
    if (((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (Build.MODEL.equals("GT-P7510"))) || ((Build.MANUFACTURER.compareToIgnoreCase("asus") == 0) && (Build.MODEL.equals("Transformer TF101"))))
    {
      camera_width[1] = 480;
      camera_height[1] = 320;
      capture_width[1] = 480;
      capture_height[1] = 320;
      capture_rotation[1] = 1;
      return true;
    }
    if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.equals("PC36100")))
    {
      if (isGingerbread)
      {
        camera_width[1] = 192;
        camera_height[1] = 144;
        capture_width[1] = 192;
        capture_height[1] = 128;
        if (Build.VERSION.RELEASE.compareTo("2.3.5") < 0)
          break label557;
        capture_rotation[1] = 1;
      }
      while (true)
      {
        return true;
        camera_width[1] = 240;
        camera_height[1] = 160;
        capture_width[1] = 240;
        capture_height[1] = 160;
        break;
        label557: capture_rotation[1] = 2;
      }
    }
    if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.equals("ADR6400L")))
    {
      camera_width[1] = 240;
      camera_height[1] = 160;
      capture_width[1] = 240;
      capture_height[1] = 160;
      int[] arrayOfInt3 = capture_rotation;
      if (isGingerbread);
      for (int k = 1; ; k = 2)
      {
        arrayOfInt3[1] = k;
        return true;
      }
    }
    if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.startsWith("HTC Desire S")))
    {
      camera_width[1] = 192;
      camera_height[1] = 144;
      capture_width[1] = 192;
      capture_height[1] = 128;
      capture_rotation[1] = 2;
      return true;
    }
    if (Build.MODEL.equals("LG-MS910"))
    {
      camera_width[1] = 240;
      camera_height[1] = 160;
      capture_width[1] = 240;
      capture_height[1] = 160;
      int[] arrayOfInt2 = capture_rotation;
      if (isGingerbread);
      for (int j = 1; ; j = 2)
      {
        arrayOfInt2[1] = j;
        return true;
      }
    }
    if ((Build.MANUFACTURER.equals("Sony Ericsson")) && (Build.MODEL.startsWith("R800")))
    {
      camera_width[1] = 320;
      camera_height[1] = 240;
      capture_width[1] = 320;
      capture_height[1] = 214;
      capture_rotation[1] = 1;
      return true;
    }
    if ((Build.MODEL.equals("GT-I9003")) || (Build.MODEL.equals("GT-I9003L")))
    {
      camera_width[1] = 640;
      camera_height[1] = 480;
      capture_width[1] = 320;
      capture_height[1] = 480;
      capture_rotation[1] = 3;
      return true;
    }
    int i;
    if ((Build.MANUFACTURER.compareToIgnoreCase("lge") == 0) && (Build.MODEL.startsWith("VS910 4G")))
    {
      camera_width[1] = 240;
      camera_height[1] = 160;
      capture_width[1] = 192;
      capture_height[1] = 128;
      int[] arrayOfInt1 = capture_rotation;
      if (isGingerbread)
      {
        i = 1;
        arrayOfInt1[1] = i;
      }
    }
    do
    {
      return false;
      i = 2;
      break;
      if ((Build.MANUFACTURER.compareToIgnoreCase("lge") == 0) && (Build.MODEL.startsWith("LG-P925")) && (Build.VERSION.SDK_INT < 9))
      {
        camera_width[1] = 320;
        camera_height[1] = 240;
        capture_width[1] = 320;
        capture_height[1] = 214;
        capture_rotation[1] = 2;
        return true;
      }
      if ((Build.MANUFACTURER.compareToIgnoreCase("lge") == 0) && (Build.MODEL.startsWith("LG-P999")))
      {
        camera_width[1] = 192;
        camera_height[1] = 128;
        capture_width[1] = 192;
        capture_height[1] = 128;
        capture_rotation[1] = 2;
        return true;
      }
      if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("SPH-D700")) || (Build.MODEL.equals("SGH-T759"))))
      {
        camera_width[1] = 192;
        camera_height[1] = 144;
        capture_width[1] = 192;
        capture_height[1] = 128;
        capture_rotation[1] = 1;
        return true;
      }
      if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (Build.MODEL.equals("GT-I9000")))
      {
        camera_width[1] = 256;
        camera_height[1] = 192;
        if (Build.VERSION.RELEASE.compareTo("2.3.3") >= 0)
        {
          capture_width[1] = 192;
          capture_height[1] = 128;
          capture_rotation[1] = 1;
        }
        while (true)
        {
          return true;
          capture_width[1] = 128;
          capture_height[1] = 192;
          capture_rotation[1] = 3;
        }
      }
      if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (Build.MODEL.equals("GT-I9103")))
      {
        camera_width[1] = 192;
        camera_height[1] = 128;
        capture_width[1] = 192;
        capture_height[1] = 128;
        capture_rotation[1] = 1;
        return true;
      }
      if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("SPH-D710")) || (Build.MODEL.equals("SHW-M250S")) || (Build.MODEL.equals("GT-I9100"))))
      {
        camera_width[1] = 640;
        camera_height[1] = 480;
        capture_width[1] = 576;
        capture_height[1] = 384;
        capture_rotation[1] = 1;
        return true;
      }
      if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && ((Build.MODEL.equals("SGH-T989")) || (Build.MODEL.equals("SAMSUNG-SGH-I727"))))
      {
        camera_width[1] = 480;
        camera_height[1] = 320;
        capture_width[1] = 480;
        capture_height[1] = 320;
        capture_rotation[1] = 1;
        return true;
      }
      if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("HUAWEI") == 0) && (Build.MODEL.equals("M886")))
      {
        camera_width[1] = 320;
        camera_height[1] = 240;
        if (Build.VERSION.RELEASE.compareTo("2.3.5") >= 0)
        {
          capture_rotation[1] = 1;
          capture_width[1] = 240;
          capture_height[1] = 160;
        }
        while (true)
        {
          return true;
          capture_width[1] = 160;
          capture_height[1] = 240;
          capture_rotation[1] = 3;
        }
      }
      if ((isGingerbread) && (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) && (Build.MODEL.equals("HUAWEI-M920")))
      {
        camera_width[1] = 320;
        camera_height[1] = 240;
        capture_width[1] = 240;
        capture_height[1] = 160;
        capture_rotation[1] = 1;
        return true;
      }
      if ((isGingerbread) && (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) && (Build.MODEL.equals("HUAWEI-M921")))
      {
        camera_width[1] = 320;
        camera_height[1] = 240;
        capture_width[1] = 240;
        capture_height[1] = 160;
        capture_rotation[1] = 1;
        return true;
      }
      if ((!isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("PANTECH") == 0))
      {
        camera_width[1] = 320;
        camera_height[1] = 240;
        capture_width[1] = 160;
        capture_height[1] = 240;
        capture_rotation[1] = 3;
        return true;
      }
      if (Build.MODEL.startsWith("Galaxy Nexus"))
      {
        camera_width[1] = 640;
        camera_height[1] = 480;
        capture_width[1] = 576;
        capture_height[1] = 384;
        capture_rotation[1] = 1;
        return true;
      }
      if ((Build.MANUFACTURER.compareToIgnoreCase("Lenovo") == 0) && (Build.MODEL.equals("A1_07")))
      {
        camera_width[1] = 320;
        camera_height[1] = 240;
        capture_width[1] = 320;
        capture_height[1] = 214;
        capture_rotation[1] = 2;
        return true;
      }
    }
    while (!Build.MODEL.equals("LT22i"));
    camera_width[1] = 320;
    camera_height[1] = 240;
    capture_width[1] = 320;
    capture_height[1] = 214;
    capture_rotation[1] = 1;
    return true;
  }

  private boolean isBecameStable(float[] paramArrayOfFloat)
  {
    paramArrayOfFloat[2] -= 9.81F;
    this.acc_x = (paramArrayOfFloat[0] * this.weight_now + this.acc_x * this.weight_prev);
    this.acc_y = (paramArrayOfFloat[1] * this.weight_now + this.acc_y * this.weight_prev);
    this.acc_z = (paramArrayOfFloat[2] * this.weight_now + this.acc_z * this.weight_prev);
    float f1 = paramArrayOfFloat[0] - this.acc_x;
    float f2 = paramArrayOfFloat[1] - this.acc_y;
    float f3 = paramArrayOfFloat[2] - this.acc_z;
    float f4 = Math.abs(f1) + Math.abs(f2) + Math.abs(f3);
    boolean bool;
    if ((f4 - this.value_prev > this.threshold_high) || (f4 > this.value_peak))
    {
      this.value_peak = f4;
      this.isFoundPeak = true;
      bool = false;
    }
    while (true)
    {
      this.value_prev = f4;
      return bool;
      if ((this.isFoundPeak) && (f4 < this.value_peak * this.threshold_low) && (this.value_prev - f4 < this.threshold_stable))
      {
        this.isFoundPeak = false;
        bool = true;
      }
      else
      {
        bool = false;
      }
    }
  }

  private boolean isCameraThread()
  {
    return (Thread.currentThread() == timestamper) || (this.glRendererHasCapture == true);
  }

  private static boolean rangesIntersect(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i;
    if (paramInt1 > paramInt3)
    {
      i = paramInt1;
      if (paramInt2 >= paramInt4)
        break label31;
    }
    label31: for (int j = paramInt2; ; j = paramInt4)
    {
      if (i > j)
        break label37;
      return true;
      i = paramInt3;
      break;
    }
    label37: return false;
  }

  public static native void registerPrThread();

  private static boolean selectCameraSizeForHTC(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((Build.MODEL.startsWith("HTC Bliss")) || (Build.MODEL.startsWith("HTC Rhyme")) || (Build.MODEL.startsWith("HTC Runnymede")) || (Build.MODEL.startsWith("HTC Sensation")));
    for (int i = 1; i == 0; i = 0)
      return false;
    if ((paramInt2 <= 192) && (paramInt3 <= 128))
    {
      camera_width[paramInt1] = 240;
      camera_height[paramInt1] = 160;
      capture_width[paramInt1] = 240;
      capture_height[paramInt1] = 160;
    }
    while (true)
    {
      capture_rotation[1] = 1;
      return true;
      if ((paramInt2 <= 384) && (paramInt3 <= 256))
      {
        camera_width[paramInt1] = 384;
        camera_height[paramInt1] = 288;
        capture_width[paramInt1] = 384;
        capture_height[paramInt1] = 256;
      }
      else if ((paramInt2 <= 480) && (paramInt3 <= 320))
      {
        camera_width[paramInt1] = 480;
        camera_height[paramInt1] = 320;
        capture_width[paramInt1] = 480;
        capture_height[paramInt1] = 320;
      }
      else
      {
        camera_width[paramInt1] = 640;
        camera_height[paramInt1] = 480;
        capture_width[paramInt1] = 576;
        capture_height[paramInt1] = 384;
      }
    }
  }

  public static void setActivityHandler(Handler paramHandler)
  {
    try
    {
      ui_handler = paramHandler;
      return;
    }
    finally
    {
    }
  }

  private native void setCallback(int paramInt1, int paramInt2);

  private void setCurrentCamera(int paramInt)
  {
    current_camera = paramInt;
  }

  public static void setPreviewDisplay(SurfaceHolder paramSurfaceHolder)
  {
    _holder[0] = paramSurfaceHolder;
    _holder[1] = paramSurfaceHolder;
  }

  private static int setPreviewSize(int paramInt1, int paramInt2)
  {
    int i = crop_size.length;
    for (int j = crop_size.length - 1; ; j--)
      if (j >= 0)
      {
        if ((paramInt1 >= crop_size[j].width) && (paramInt2 >= crop_size[j].height))
          i = j;
      }
      else
        return i;
  }

  public static void setUserFrontCamRotation(int paramInt)
  {
    if ((paramInt == -1) || ((paramInt >= 0) && (paramInt <= 4)))
      userFrontCameraRotation = paramInt;
  }

  // ERROR //
  private boolean setupCameraParams(CameraWrapper paramCameraWrapper)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 707	com/sgiggle/VideoCapture/CameraWrapper:getParameters	()Landroid/hardware/Camera$Parameters;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 710	android/hardware/Camera$Parameters:getPreviewFormat	()I
    //   9: bipush 17
    //   11: if_icmpeq +34 -> 45
    //   14: ldc 47
    //   16: new 217	java/lang/StringBuilder
    //   19: dup
    //   20: invokespecial 218	java/lang/StringBuilder:<init>	()V
    //   23: ldc_w 712
    //   26: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: aload_2
    //   30: invokevirtual 710	android/hardware/Camera$Parameters:getPreviewFormat	()I
    //   33: invokevirtual 229	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   36: invokevirtual 238	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   39: invokestatic 380	com/sgiggle/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   42: pop
    //   43: iconst_0
    //   44: ireturn
    //   45: aload_2
    //   46: invokevirtual 715	android/hardware/Camera$Parameters:getSupportedFocusModes	()Ljava/util/List;
    //   49: astore_3
    //   50: iconst_0
    //   51: istore 4
    //   53: iconst_0
    //   54: istore 5
    //   56: iconst_0
    //   57: istore 6
    //   59: iload 4
    //   61: aload_3
    //   62: invokeinterface 347 1 0
    //   67: if_icmpge +69 -> 136
    //   70: aload_3
    //   71: iload 4
    //   73: invokeinterface 351 2 0
    //   78: checkcast 178	java/lang/String
    //   81: astore 25
    //   83: aload 25
    //   85: ldc_w 717
    //   88: invokevirtual 312	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   91: ifeq +24 -> 115
    //   94: iconst_1
    //   95: istore 26
    //   97: iload 6
    //   99: istore 27
    //   101: iinc 4 1
    //   104: iload 27
    //   106: istore 6
    //   108: iload 26
    //   110: istore 5
    //   112: goto -53 -> 59
    //   115: aload 25
    //   117: ldc_w 719
    //   120: invokevirtual 312	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   123: ifeq +475 -> 598
    //   126: iload 5
    //   128: istore 26
    //   130: iconst_1
    //   131: istore 27
    //   133: goto -32 -> 101
    //   136: aload_0
    //   137: iconst_0
    //   138: putfield 721	com/sgiggle/VideoCapture/VideoCaptureRaw:isSupportAutoFocus	Z
    //   141: iload 6
    //   143: ifeq +338 -> 481
    //   146: aload_2
    //   147: ldc_w 719
    //   150: invokevirtual 724	android/hardware/Camera$Parameters:setFocusMode	(Ljava/lang/String;)V
    //   153: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   156: ldc_w 440
    //   159: invokevirtual 312	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   162: ifne +35 -> 197
    //   165: getstatic 306	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   168: ldc_w 640
    //   171: invokevirtual 312	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   174: ifeq +37 -> 211
    //   177: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   180: ldc_w 642
    //   183: invokevirtual 182	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   186: ifeq +25 -> 211
    //   189: aload_0
    //   190: getfield 226	com/sgiggle/VideoCapture/VideoCaptureRaw:type	I
    //   193: iconst_1
    //   194: if_icmpne +17 -> 211
    //   197: ldc 47
    //   199: ldc_w 726
    //   202: invokestatic 380	com/sgiggle/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   205: pop
    //   206: aload_0
    //   207: iconst_0
    //   208: putfield 721	com/sgiggle/VideoCapture/VideoCaptureRaw:isSupportAutoFocus	Z
    //   211: getstatic 306	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   214: ldc_w 442
    //   217: invokevirtual 463	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   220: ifeq +25 -> 245
    //   223: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   226: ldc_w 444
    //   229: invokevirtual 182	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   232: ifeq +13 -> 245
    //   235: aload_2
    //   236: sipush 1280
    //   239: sipush 960
    //   242: invokevirtual 729	android/hardware/Camera$Parameters:setPictureSize	(II)V
    //   245: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   248: ldc_w 624
    //   251: invokevirtual 312	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   254: ifne +87 -> 341
    //   257: getstatic 306	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   260: ldc_w 640
    //   263: invokevirtual 312	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   266: ifeq +15 -> 281
    //   269: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   272: ldc_w 642
    //   275: invokevirtual 182	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   278: ifne +63 -> 341
    //   281: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   284: ldc_w 693
    //   287: invokevirtual 182	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   290: ifne +51 -> 341
    //   293: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   296: ldc_w 687
    //   299: invokevirtual 182	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   302: ifne +39 -> 341
    //   305: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   308: ldc_w 689
    //   311: invokevirtual 182	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   314: ifne +27 -> 341
    //   317: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   320: ldc_w 691
    //   323: invokevirtual 182	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   326: ifne +15 -> 341
    //   329: getstatic 315	android/os/Build:MODEL	Ljava/lang/String;
    //   332: ldc_w 731
    //   335: invokevirtual 182	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   338: ifeq +9 -> 347
    //   341: aload_0
    //   342: bipush 15
    //   344: putfield 194	com/sgiggle/VideoCapture/VideoCaptureRaw:frameRate	I
    //   347: aload_2
    //   348: invokevirtual 735	java/lang/Object:getClass	()Ljava/lang/Class;
    //   351: astore 20
    //   353: aload 20
    //   355: ldc_w 736
    //   358: aconst_null
    //   359: checkcast 738	[Ljava/lang/Class;
    //   362: invokevirtual 744	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   365: astore 21
    //   367: aload 21
    //   369: astore 9
    //   371: iconst_2
    //   372: anewarray 740	java/lang/Class
    //   375: astore 23
    //   377: aload 23
    //   379: iconst_0
    //   380: getstatic 748	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   383: aastore
    //   384: aload 23
    //   386: iconst_1
    //   387: getstatic 748	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   390: aastore
    //   391: aload 20
    //   393: ldc_w 749
    //   396: aload 23
    //   398: invokevirtual 744	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   401: astore 24
    //   403: aload 24
    //   405: astore 11
    //   407: aload 9
    //   409: astore 10
    //   411: aload_2
    //   412: invokevirtual 735	java/lang/Object:getClass	()Ljava/lang/Class;
    //   415: ldc_w 750
    //   418: aconst_null
    //   419: checkcast 738	[Ljava/lang/Class;
    //   422: invokevirtual 744	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   425: astore 19
    //   427: aload 19
    //   429: astore 13
    //   431: aload 10
    //   433: ifnull +91 -> 524
    //   436: aload 11
    //   438: ifnull +86 -> 524
    //   441: aload_2
    //   442: aload_0
    //   443: getfield 194	com/sgiggle/VideoCapture/VideoCaptureRaw:frameRate	I
    //   446: aload_0
    //   447: getfield 194	com/sgiggle/VideoCapture/VideoCaptureRaw:frameRate	I
    //   450: invokestatic 752	com/sgiggle/VideoCapture/VideoCaptureRaw:initFPSRange	(Landroid/hardware/Camera$Parameters;II)Ljava/util/List;
    //   453: pop
    //   454: aload_2
    //   455: getstatic 135	com/sgiggle/VideoCapture/VideoCaptureRaw:camera_width	[I
    //   458: aload_0
    //   459: getfield 226	com/sgiggle/VideoCapture/VideoCaptureRaw:type	I
    //   462: iaload
    //   463: getstatic 137	com/sgiggle/VideoCapture/VideoCaptureRaw:camera_height	[I
    //   466: aload_0
    //   467: getfield 226	com/sgiggle/VideoCapture/VideoCaptureRaw:type	I
    //   470: iaload
    //   471: invokevirtual 754	android/hardware/Camera$Parameters:setPreviewSize	(II)V
    //   474: aload_1
    //   475: aload_2
    //   476: invokevirtual 758	com/sgiggle/VideoCapture/CameraWrapper:setParameters	(Landroid/hardware/Camera$Parameters;)V
    //   479: iconst_1
    //   480: ireturn
    //   481: iload 5
    //   483: ifeq -330 -> 153
    //   486: aload_2
    //   487: ldc_w 717
    //   490: invokevirtual 724	android/hardware/Camera$Parameters:setFocusMode	(Ljava/lang/String;)V
    //   493: aload_0
    //   494: iconst_1
    //   495: putfield 721	com/sgiggle/VideoCapture/VideoCaptureRaw:isSupportAutoFocus	Z
    //   498: goto -345 -> 153
    //   501: astore 8
    //   503: aconst_null
    //   504: astore 9
    //   506: aload 9
    //   508: astore 10
    //   510: aconst_null
    //   511: astore 11
    //   513: goto -102 -> 411
    //   516: astore 12
    //   518: aconst_null
    //   519: astore 13
    //   521: goto -90 -> 431
    //   524: aload 13
    //   526: ifnull +15 -> 541
    //   529: aload_2
    //   530: aload_0
    //   531: getfield 194	com/sgiggle/VideoCapture/VideoCaptureRaw:frameRate	I
    //   534: invokestatic 760	com/sgiggle/VideoCapture/VideoCaptureRaw:initFrameRate	(Landroid/hardware/Camera$Parameters;I)I
    //   537: pop
    //   538: goto -84 -> 454
    //   541: aload_2
    //   542: aload_0
    //   543: getfield 194	com/sgiggle/VideoCapture/VideoCaptureRaw:frameRate	I
    //   546: invokevirtual 595	android/hardware/Camera$Parameters:setPreviewFrameRate	(I)V
    //   549: goto -95 -> 454
    //   552: astore 14
    //   554: ldc 47
    //   556: new 217	java/lang/StringBuilder
    //   559: dup
    //   560: invokespecial 218	java/lang/StringBuilder:<init>	()V
    //   563: ldc_w 762
    //   566: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   569: aload 14
    //   571: invokevirtual 570	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   574: invokevirtual 238	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   577: invokestatic 244	com/sgiggle/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   580: pop
    //   581: ldc 47
    //   583: aload_2
    //   584: invokevirtual 765	android/hardware/Camera$Parameters:flatten	()Ljava/lang/String;
    //   587: invokestatic 244	com/sgiggle/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   590: pop
    //   591: iconst_0
    //   592: ireturn
    //   593: astore 22
    //   595: goto -89 -> 506
    //   598: iload 5
    //   600: istore 26
    //   602: iload 6
    //   604: istore 27
    //   606: goto -505 -> 101
    //
    // Exception table:
    //   from	to	target	type
    //   347	367	501	java/lang/Exception
    //   411	427	516	java/lang/Exception
    //   474	479	552	java/lang/RuntimeException
    //   371	403	593	java/lang/Exception
  }

  private void setupGLRenderer()
  {
    this.glRenderer.setClip(1, camera_width[this.type], camera_height[this.type], capture_width[this.type], capture_height[this.type]);
    if (this.glCapture != null)
    {
      if (this.type == 0)
        this.glRenderer.setTransform(1, 0.0F, false, true, false, false);
      while (this.type != 1)
        return;
      this.glRenderer.setTransform(1, 0.0F, false, true, true, true);
      return;
    }
    switch (capture_rotation[this.type])
    {
    default:
      Log.e("VideoCaptureRaw", "unknown rotation " + capture_rotation[this.type]);
      return;
    case 0:
      this.glRenderer.setTransform(1, 0.0F, false, false, false, false);
      return;
    case 1:
      this.glRenderer.setTransform(1, 0.0F, true, false, false, false);
      return;
    case 2:
      this.glRenderer.setTransform(1, 0.0F, false, false, false, false);
      return;
    case 3:
      this.glRenderer.setTransform(1, 270.0F, false, false, false, false);
      return;
    case 4:
    }
    this.glRenderer.setTransform(1, 270.0F, true, false, false, false);
  }

  private boolean startCameraRecording()
  {
    while (true)
    {
      try
      {
        if (this.camera == null)
        {
          this.camera = CameraWrapper.open(cameraIds[this.type]);
          if (this.camera == null)
          {
            Log.e("VideoCaptureRaw", "no camera");
            return false;
          }
          if (!setupCameraParams(this.camera))
          {
            this.camera.release();
            this.camera = null;
            return false;
          }
        }
        if (!this.glRendererHasCapture)
          timestamper.addCallbackBuffers();
        if (this.glRenderer != null)
          break label451;
        if ((this.type == 1) && (Build.VERSION.SDK_INT >= 8) && ((capture_rotation[1] == 3) || (capture_rotation[1] == 4)) && ((!isGingerbread) || (Build.MANUFACTURER.compareToIgnoreCase("Samsung") != 0) || ((!Build.MODEL.equals("GT-I9003")) && (!Build.MODEL.equals("GT-I9003L")))))
        {
          this.camera.setDisplayOrientation(270);
          if (this.glCapture == null)
            break label458;
          this.glCapture.setCamera(this.camera.getCamera(), camera_width[this.type], camera_height[this.type]);
          this.camera.startPreview();
          if ((sm == null) || (this.isSupportAutoFocus != true))
            break;
          this.accSensor = sm.getDefaultSensor(1);
          if (this.accSensor == null)
            break;
          sm.registerListener(this, this.accSensor, 3);
          break;
        }
        if ((this.type == 1) && (Build.VERSION.SDK_INT == 10) && (Build.MODEL.equals("A1_07")) && (Build.MANUFACTURER.compareToIgnoreCase("Lenovo") == 0))
        {
          this.camera.setDisplayOrientation(180);
          continue;
        }
      }
      catch (Exception localException)
      {
        Log.e("VideoCaptureRaw", "failed to start recording " + localException.getMessage());
        localException.printStackTrace();
        if (this.camera != null)
        {
          this.camera.release();
          this.camera = null;
        }
        return false;
      }
      if (isSamsungTablet10Dot1)
      {
        this.camera.setDisplayOrientation(90);
      }
      else if ((this.type == 0) && (isGingerbread) && (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) && (Build.MODEL.equals("M886")) && (Build.VERSION.RELEASE.compareTo("2.3.5") < 0))
      {
        this.camera.setDisplayOrientation(180);
        continue;
        label451: setupGLRenderer();
        continue;
        label458: if ((this.glRenderer != null) && (GLRenderer.isNotNeededPreviewDisplay()))
          this.camera.setPreviewDisplay(null);
        else
          this.camera.setPreviewDisplay(_holder[this.type]);
      }
    }
    return true;
  }

  public static void staticResumeRecording()
  {
    Log.d("VideoCaptureRaw", "staticResumeRecording");
    try
    {
      if (current_camera == -1)
      {
        Log.v("VideoCaptureRaw", "staticResumeRecording: no active camera");
        return;
      }
      if (sInstance[current_camera] != null)
        sInstance[current_camera].resumeRecording();
      return;
    }
    finally
    {
    }
  }

  public static void staticSuspendRecording()
  {
    Log.d("VideoCaptureRaw", "staticSuspendRecording");
    try
    {
      if (current_camera == -1)
      {
        Log.v("VideoCaptureRaw", "staticSuspendRecording: no active camera");
        return;
      }
      if (sInstance[current_camera] != null)
        sInstance[current_camera].suspendRecording();
      return;
    }
    finally
    {
    }
  }

  private void stopCameraRecording()
  {
    suspendCameraRecording();
    if (this.camera != null)
    {
      this.camera.setPreviewCallback(null);
      this.camera.release();
      this.camera = null;
    }
  }

  private void suspendRecording()
  {
    suspendCameraRecording();
    this._suspended = true;
  }

  public static void updateContext(Context paramContext)
  {
    sm = (SensorManager)paramContext.getSystemService("sensor");
    wm = (WindowManager)paramContext.getSystemService("window");
    screenWidth = wm.getDefaultDisplay().getWidth();
    screenHeight = wm.getDefaultDisplay().getHeight();
    if ((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (((screenWidth == 1024) && (screenHeight == 600)) || ((screenHeight == 1024) && (screenWidth == 600))))
      isSamsungTablet7Inch = true;
    if ((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (((screenWidth == 1280) && (screenHeight == 800)) || ((screenHeight == 1280) && (screenWidth == 800))))
      isSamsungTablet10Dot1 = true;
  }

  private void updateLayout()
  {
    try
    {
      if (ui_handler != null)
      {
        Log.d("VideoCaptureRaw", "updateLayout: sending message to ui_handler");
        Message.obtain(ui_handler, 2).sendToTarget();
      }
      while (true)
      {
        return;
        Log.d("VideoCaptureRaw", "updateLayout: no ui_handler");
      }
    }
    finally
    {
    }
  }

  protected boolean doStartRecording(int paramInt1, int paramInt2)
  {
    Log.v("VideoCaptureRaw", "doStartRecording() starts");
    setCurrentCamera(this.type);
    setCallback(paramInt1, paramInt2);
    if (this.glCapture != null)
      return startCameraRecording();
    if (_holder[this.type] == null)
    {
      Log.i("VideoCaptureRaw", "startRecording: no surface, setting SUSPENDED state");
      this._suspended = true;
      return true;
    }
    this._suspended = false;
    this._stopped = false;
    return startCameraRecording();
  }

  protected void doStopRecording()
  {
    Log.v("VideoCaptureRaw", "doStopRecording() starts");
    setCurrentCamera(-1);
    this._suspended = false;
    setCallback(0, 0);
    stopCameraRecording();
  }

  public int getCameraHeight()
  {
    return camera_height[this.type];
  }

  public int getCameraWidth()
  {
    return camera_width[this.type];
  }

  protected CameraWrapper getCameraWrapper()
  {
    return this.camera;
  }

  public int getCaptureHeight()
  {
    return capture_height[this.type];
  }

  public int getCaptureRotation()
  {
    return capture_rotation[this.type];
  }

  public int getCaptureWidth()
  {
    return capture_width[this.type];
  }

  protected GLRenderer getGlRenderer()
  {
    if (this.glCapture != null)
      return null;
    return this.glRenderer;
  }

  public void onAccuracyChanged(Sensor paramSensor, int paramInt)
  {
  }

  public void onAutoFocus(boolean paramBoolean, Camera paramCamera)
  {
    this.isFocusing = false;
  }

  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    if ((this._stopped == true) || (this.camera == null));
    while ((!isBecameStable(paramSensorEvent.values)) || (this.isFocusing))
      return;
    this.isFocusing = true;
    try
    {
      if (timestamper != null)
        timestamper.autoFocus(this);
      return;
    }
    finally
    {
    }
  }

  protected void resumeRecording()
  {
    if (!isCameraThread())
    {
      Log.d("VideoCaptureRaw", "resumeRecording(): called from thread " + Thread.currentThread().getId() + ". Executing synchronously on camera thread.");
      if (timestamper != null)
      {
        timestamper.resumeRecording();
        return;
      }
      Log.e("VideoCaptureRaw", "timestamper is null. Ignoring resumeRecording. Should not have called resumeRecording before startRecording.");
      return;
    }
    Log.v("VideoCaptureRaw", "resumeRecording(): _suspended=" + this._suspended);
    if (this._suspended)
      startCameraRecording();
    this._suspended = false;
    this._stopped = false;
  }

  public void setCameraDisplayOrientation()
  {
    int i;
    CameraWrapper.CameraInfo localCameraInfo;
    switch (wm.getDefaultDisplay().getOrientation())
    {
    default:
      i = 0;
      localCameraInfo = new CameraWrapper.CameraInfo();
      CameraWrapper.getCameraInfo(cameraIds[current_camera], localCameraInfo);
      if (localCameraInfo.facing != 0)
        break;
    case 0:
    case 1:
    case 2:
    case 3:
    }
    for (int j = (360 + (localCameraInfo.orientation - i)) % 360; ; j = (360 - (i + localCameraInfo.orientation) % 360) % 360)
    {
      this.camera.setDisplayOrientation(j);
      return;
      i = 0;
      break;
      i = 90;
      break;
      i = 180;
      break;
      i = 270;
      break;
    }
  }

  public void setVideoFrameRate(int paramInt)
  {
    this.frameRate = paramInt;
    Log.v("VideoCaptureRaw", "setVideoFrameRate() frameRate=" + this.frameRate);
  }

  public boolean startRecording(int paramInt1, int paramInt2)
  {
    Log.d("VideoCaptureRaw", "startRecording called");
    if (this.glRendererHasCapture)
    {
      doStartRecording(paramInt1, paramInt2);
      return true;
    }
    preprocessor = new PreprocessorThread(this);
    timestamper = new TimestamperThread(this, preprocessor);
    timestamper.startRecording(paramInt1, paramInt2);
    return true;
  }

  public void stopRecording()
  {
    Log.d("VideoCaptureRaw", "stopRecording called");
    if (this.glRendererHasCapture)
    {
      doStopRecording();
      return;
    }
    timestamper.stopRecording();
    timestamper.stopThread();
    preprocessor.stopPreprocessor();
    try
    {
      timestamper = null;
      preprocessor = null;
      return;
    }
    finally
    {
    }
  }

  protected void suspendCameraRecording()
  {
    if (!isCameraThread())
      if (timestamper != null)
        timestamper.suspendRecording();
    do
    {
      do
      {
        return;
        Log.e("VideoCaptureRaw", "timestamper is null. Ignoring suspendCameraRecording. Should not have called suspendCameraRecord after stopRecording.");
        return;
        this._stopped = true;
        if ((sm != null) && (this.accSensor != null))
          sm.unregisterListener(this);
      }
      while (this.camera == null);
      if (this.isSupportAutoFocus == true)
        this.camera.cancelAutoFocus();
      this.camera.stopPreview();
      if (this.glCapture != null)
        this.glCapture.setCamera(null, 0, 0);
    }
    while (((Build.MANUFACTURER.compareToIgnoreCase("Motorola") != 0) || ((!Build.MODEL.equals("DROIDX")) && (!Build.MODEL.equals("DROID2")) && (!Build.MODEL.equals("A955")))) && ((Build.MANUFACTURER.compareToIgnoreCase("Samsung") != 0) || (!Build.MODEL.equals("SAMSUNG-SGH-I997"))) && (!isSamsungTablet7Inch) && ((!isGingerbread) || (Build.MANUFACTURER.compareToIgnoreCase("htc") != 0) || (!Build.MODEL.startsWith("ADR6400L"))));
    this.camera.setPreviewCallback(null);
    this.camera.release();
    this.camera = null;
  }

  public void unsetPreviewDisplay()
  {
    if (this.glRenderer == null)
    {
      _holder[0] = null;
      _holder[1] = null;
    }
  }

  public void updateParam(int paramInt1, int paramInt2, int paramInt3)
  {
    initCameraSize(paramInt2, paramInt3);
  }

  public static abstract interface CameraDevice
  {
    public static final int Back = 0;
    public static final int Front = 1;
    public static final int None = -1;
  }

  private static class Size
  {
    public int height;
    public int width;

    public Size(int paramInt1, int paramInt2)
    {
      this.width = paramInt1;
      this.height = paramInt2;
    }
  }

  public static abstract interface Type
  {
    public static final int Back = 1;
    public static final int Front = 2;
    public static final int None;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.VideoCaptureRaw
 * JD-Core Version:    0.6.2
 */