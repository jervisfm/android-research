package com.sgiggle.VideoCapture;

public class VideoCaptureRawWrapper
{
  private static final String TAG = "VideoCaptureRawWrapper";
  public int camera_height;
  public int camera_width;
  public int capture_height;
  public int capture_rotation;
  public int capture_width;
  VideoCaptureRaw m_videoCapture = null;

  public static boolean checkCamera(int paramInt)
  {
    return VideoCaptureRaw.checkCamera(paramInt);
  }

  public static boolean isResoluionFixed(int paramInt)
  {
    if (paramInt == 1)
      return VideoCaptureRaw.initBackCameraSizeHardCoded();
    return VideoCaptureRaw.initFrontCameraSizeHardCoded();
  }

  public void initialize(int paramInt1, int paramInt2, int paramInt3)
  {
    this.m_videoCapture = new VideoCaptureRaw(paramInt1, paramInt2, paramInt3);
    this.camera_width = this.m_videoCapture.getCameraWidth();
    this.camera_height = this.m_videoCapture.getCameraHeight();
    this.capture_width = this.m_videoCapture.getCaptureWidth();
    this.capture_height = this.m_videoCapture.getCaptureHeight();
    this.capture_rotation = this.m_videoCapture.getCaptureRotation();
  }

  public void setVideoFrameRate(int paramInt)
  {
    this.m_videoCapture.setVideoFrameRate(paramInt);
  }

  public boolean startRecording(int paramInt1, int paramInt2)
  {
    return this.m_videoCapture.startRecording(paramInt1, paramInt2);
  }

  public void stopRecording()
  {
    this.m_videoCapture.stopRecording();
  }

  public void unsetPreviewDisplay()
  {
    this.m_videoCapture.unsetPreviewDisplay();
  }

  public void updateParam(int paramInt1, int paramInt2, int paramInt3)
  {
    this.m_videoCapture.updateParam(paramInt1, paramInt2, paramInt3);
    this.camera_width = this.m_videoCapture.getCameraWidth();
    this.camera_height = this.m_videoCapture.getCameraHeight();
    this.capture_width = this.m_videoCapture.getCaptureWidth();
    this.capture_height = this.m_videoCapture.getCaptureHeight();
    this.capture_rotation = this.m_videoCapture.getCaptureRotation();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.VideoCaptureRawWrapper
 * JD-Core Version:    0.6.2
 */