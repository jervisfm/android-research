package com.sgiggle.VideoCapture;

public class VideoCaptureWrapper
{
  private static final String TAG = "VideoCaptureWrapper";
  VideoCapture m_videoCapture = null;

  public void initialize()
  {
    this.m_videoCapture = new VideoCapture();
  }

  public void setVideoFormat(int paramInt)
    throws IllegalStateException
  {
    this.m_videoCapture.setVideoFormat(paramInt);
  }

  public void setVideoFrameRate(int paramInt)
    throws IllegalStateException
  {
    this.m_videoCapture.setVideoFrameRate(paramInt);
  }

  public void setVideoSize(int paramInt)
    throws IllegalStateException
  {
    this.m_videoCapture.setVideoSize(paramInt);
  }

  public void startPreview()
    throws Exception
  {
    this.m_videoCapture.startPreview();
  }

  public void startRecording(String paramString)
    throws Exception
  {
    this.m_videoCapture.startRecording(paramString);
  }

  public void stopPreview()
  {
    this.m_videoCapture.stopPreview();
  }

  public void stopRecording()
  {
    this.m_videoCapture.stopRecording();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.VideoCaptureWrapper
 * JD-Core Version:    0.6.2
 */