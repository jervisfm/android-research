package com.sgiggle.VideoCapture;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class VideoFile
{
  private byte[] ba = new byte[4];
  private BufferedOutputStream bosl;
  private BufferedOutputStream bost;
  private BufferedOutputStream bosv;

  public VideoFile(String paramString)
  {
    openFiles(paramString);
  }

  public void closeFiles()
  {
    if (this.bosv != null);
    try
    {
      this.bosv.close();
      this.bosv = null;
      if (this.bosl == null);
    }
    catch (IOException localIOException2)
    {
      try
      {
        this.bosl.close();
        this.bosl = null;
        if (this.bost == null);
      }
      catch (IOException localIOException2)
      {
        try
        {
          while (true)
          {
            this.bost.close();
            this.bost = null;
            return;
            localIOException3 = localIOException3;
            localIOException3.printStackTrace();
          }
          localIOException2 = localIOException2;
          localIOException2.printStackTrace();
        }
        catch (IOException localIOException1)
        {
          localIOException1.printStackTrace();
        }
      }
    }
  }

  public void openFiles(String paramString)
  {
    try
    {
      this.bosv = new BufferedOutputStream(new FileOutputStream(new File(paramString + ".buffer")), 307200);
      this.bosl = new BufferedOutputStream(new FileOutputStream(new File(paramString + ".length")), 1024);
      this.bost = new BufferedOutputStream(new FileOutputStream(new File(paramString + ".time")), 1024);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public void writeFiles(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      this.bosv.write(paramArrayOfByte);
      this.ba[0] = ((byte)(0xFF & paramInt1 >> 24));
      this.ba[1] = ((byte)(0xFF & paramInt1 >> 16));
      this.ba[2] = ((byte)(0xFF & paramInt1 >> 8));
      this.ba[3] = ((byte)(0xFF & paramInt1 >> 0));
      this.bosl.write(this.ba);
      this.ba[0] = ((byte)(0xFF & paramInt2 >> 24));
      this.ba[1] = ((byte)(0xFF & paramInt2 >> 16));
      this.ba[2] = ((byte)(0xFF & paramInt2 >> 8));
      this.ba[3] = ((byte)(0xFF & paramInt2 >> 0));
      this.bost.write(this.ba);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.VideoFile
 * JD-Core Version:    0.6.2
 */