package com.sgiggle.VideoCapture;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceHolder;
import com.sgiggle.util.Log;
import java.nio.ByteBuffer;

public class VideoPreview
  implements Camera.PreviewCallback
{
  private static final String TAG = "VideoPreview";
  private ByteBuffer bb;
  private Bitmap bitmap;
  private Canvas canvas;
  private Rect dst;
  private int dst_height;
  private int dst_width;
  private SurfaceHolder holder;
  private boolean isStarted;
  private int src_height;
  private int src_width;

  static
  {
    int i = 0;
    StackTraceElement[] arrayOfStackTraceElement = new Throwable().getStackTrace();
    int j = 0;
    while (i < arrayOfStackTraceElement.length)
    {
      if (arrayOfStackTraceElement[i].getClassName().startsWith("com.sgiggle.VideoCaptureTest"))
        j = 1;
      i++;
    }
    if (j != 0)
      System.loadLibrary("VideoCapture");
  }

  private native void NV21toRGB565clip(byte[] paramArrayOfByte, int paramInt1, int paramInt2, ByteBuffer paramByteBuffer, int paramInt3, int paramInt4);

  private void clear(int paramInt)
  {
    try
    {
      if (this.holder != null)
      {
        Surface localSurface = this.holder.getSurface();
        if (localSurface != null)
        {
          this.canvas = localSurface.lockCanvas(null);
          this.canvas.drawColor(paramInt);
          localSurface.unlockCanvasAndPost(this.canvas);
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Log.e("VideoPreview", "clear(), exception: " + localException);
      localException.printStackTrace();
    }
  }

  private native void close();

  private native int init(int paramInt1, int paramInt2);

  private void renderBuffer(ByteBuffer paramByteBuffer)
  {
    if ((this.holder == null) || (!this.isStarted))
    {
      if (this.holder != null)
        Log.v("VideoPreview", "renderBuffer: _holder == null, can't render");
      return;
    }
    Surface localSurface;
    try
    {
      paramByteBuffer.position(0);
      this.bitmap.copyPixelsFromBuffer(paramByteBuffer);
      localSurface = this.holder.getSurface();
      if (localSurface == null)
      {
        Log.e("VideoPreview", "renderBuffer: getSurface() failed");
        return;
      }
    }
    catch (Surface.OutOfResourcesException localOutOfResourcesException)
    {
      localOutOfResourcesException.printStackTrace();
      return;
      Canvas localCanvas = localSurface.lockCanvas(null);
      this.canvas = localCanvas;
      if (localCanvas == null)
      {
        Log.e("VideoPreview", "renderBuffer: can't lock canvas");
        return;
      }
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localIllegalArgumentException.printStackTrace();
      return;
    }
    this.canvas.drawBitmap(this.bitmap, (Rect)null, this.dst, null);
    localSurface.unlockCanvasAndPost(this.canvas);
  }

  public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera)
  {
    try
    {
      render(paramArrayOfByte);
      return;
    }
    finally
    {
    }
  }

  public void render(byte[] paramArrayOfByte)
  {
    NV21toRGB565clip(paramArrayOfByte, this.src_width, this.src_height, this.bb, this.dst_width, this.dst_height);
    renderBuffer(this.bb);
  }

  public void setPreviewDisplay(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Log.v("VideoPreview", "setPreviewDisplay " + paramSurfaceHolder + " " + paramInt1 + " " + paramInt2 + " " + paramInt3 + " " + paramInt4);
    this.holder = paramSurfaceHolder;
    this.src_width = paramInt1;
    this.src_height = paramInt2;
    this.dst_width = paramInt3;
    this.dst_height = paramInt4;
  }

  public boolean startRenderer()
  {
    try
    {
      Log.v("VideoPreview", "startRenderer()");
      this.bb = ByteBuffer.allocateDirect(2 * (this.dst_width * this.dst_height));
      this.bitmap = Bitmap.createBitmap(this.dst_width, this.dst_height, Bitmap.Config.RGB_565);
      this.dst = new Rect(this.holder.getSurfaceFrame());
      clear(-16777216);
      clear(-16777216);
      init(this.dst_width, this.dst_height);
      this.isStarted = true;
      return true;
    }
    finally
    {
    }
  }

  public void stopRenderer()
  {
    try
    {
      Log.v("VideoPreview", "stopRenderer()");
      this.bb = null;
      this.bitmap = null;
      this.dst = null;
      close();
      this.isStarted = false;
      return;
    }
    finally
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.VideoPreview
 * JD-Core Version:    0.6.2
 */