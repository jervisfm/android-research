package com.sgiggle.VideoCapture;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Build.VERSION;
import android.view.Display;
import android.view.WindowManager;
import com.sgiggle.util.Log;
import java.lang.reflect.Array;

public class VideoView
{
  private static final float CAMERA_HEIGHT = 240.0F;
  private static final float CAMERA_WIDTH = 320.0F;
  private static final float CAPTURE_HEIGHT = 128.0F;
  private static final float CAPTURE_WIDTH = 192.0F;
  public static final int NUM_BLANK_FRAME = 6;
  public static final float RATIO_133 = 1.333333F;
  public static final float RATIO_15 = 1.5F;
  private static final float RATIO_BUTTON_WH = 0.1666667F;
  public static final float RATIO_DEFAULT = 1.5F;
  private static final float RATIO_GAP = 0.025F;
  private static final float RATIO_LS = 4.3F;
  private static final float RATIO_SWITCH_BUTTON = 0.3F;
  private static final float RATIO_SWITCH_BUTTON_WH = 0.5555556F;
  private static final String TAG = "VideoView";
  private static final float THRESHOLD_ASPECT_RATIO = 0.03F;
  private static final int VIEW_FRAME_WIDTH = 6;
  public static final int ViewIndexMax = 44;
  public static final int[] bigViews = { 3, 4, 5 };
  public static final int[] cameraBigViews;
  private static Rect[] cameraSize;
  public static final int[] cameraSmallViews;
  private static Rect[] captureSize;
  private static boolean isGingerbread;
  private static boolean isNeedUpdate;
  private static boolean isSamsungTablet7Inch;
  private static float[][] m_capture_aspect_ratio;
  private static boolean m_isH264Renderer;
  private static float[][] m_preview_aspect_ratio;
  private static float[] m_renderer_aspect_ratio;
  private static float[] m_screen_aspect_ratio;
  private static int[] orientations;
  public static final int[] rendererViews;
  public static final int[] smallCameraViews = { 7, 8 };
  private static Rect[][] views;

  static
  {
    cameraBigViews = new int[] { 3, 4 };
    cameraSmallViews = new int[] { 7, 8 };
    rendererViews = new int[] { 5, 9 };
    m_preview_aspect_ratio = new float[][] { { 1.5F, 1.5F }, { 0.6666667F, 0.6666667F } };
    m_capture_aspect_ratio = new float[][] { { 1.5F, 1.5F, 1.5F }, { 0.6666667F, 0.6666667F, 0.6666667F } };
    m_renderer_aspect_ratio = new float[] { 1.5F, 0.6666667F };
    m_screen_aspect_ratio = new float[] { 1.5F, 0.6666667F };
    if (Build.VERSION.SDK_INT >= 9);
    for (boolean bool = true; ; bool = false)
    {
      isGingerbread = bool;
      m_isH264Renderer = false;
      isSamsungTablet7Inch = false;
      return;
    }
  }

  private static void calcBigCameraSize(Rect[] paramArrayOfRect, int paramInt)
  {
    Rect localRect1 = paramArrayOfRect[0];
    Rect localRect2 = getVirtualSize(paramInt);
    int i = 0;
    if (i < cameraBigViews.length)
    {
      int j = cameraBigViews[i];
      paramArrayOfRect[j] = new Rect(0, 0, 0, 0);
      Rect localRect3 = paramArrayOfRect[j];
      int k;
      label59: Rect localRect4;
      Rect localRect5;
      if (j == 3)
      {
        k = 0;
        localRect4 = getCameraSize(k);
        localRect5 = getCaptureSize(k);
        if (paramInt != 0)
          break label496;
        float f4 = localRect4.width() / localRect5.width();
        int i2 = Math.round(localRect1.height() * (localRect4.height() / localRect5.height()));
        localRect3.top = Math.max(localRect2.top, Math.round(localRect1.exactCenterY() - i2 / 2));
        localRect3.bottom = Math.min(localRect2.bottom, paramArrayOfRect[0].height() - localRect3.top);
        localRect3.top = (paramArrayOfRect[0].height() - localRect3.bottom);
        int i3 = Math.round(Math.min(localRect3.height() * m_preview_aspect_ratio[paramInt][k], f4 * localRect1.width()));
        localRect3.left = localRect2.left;
        localRect3.right = Math.min(localRect2.right, i3 - localRect3.left);
        float f5 = localRect3.left + localRect3.width() * (1.0F - localRect5.width() / localRect4.width()) / 2.0F;
        if (f5 < 0.0F)
          localRect3.left = (localRect3.right - localRect3.width() - Math.round(f5 * f4));
        float f6 = localRect3.right - localRect3.width() * (1.0F - localRect5.width() / localRect4.width()) / 2.0F;
        if (f6 > localRect1.right)
          localRect3.right = (localRect3.left + localRect3.width() - Math.round(f4 * (f6 - localRect1.right)));
        if ((0x1 & localRect3.width()) == 1)
          localRect3.right -= 1;
        int i4 = Math.round(localRect3.width() / m_preview_aspect_ratio[paramInt][k]);
        if ((i4 > 2 + localRect3.height()) || (i4 < localRect3.height() - 2))
        {
          localRect3.top = Math.round(localRect1.exactCenterY() - i4 / 2);
          localRect3.bottom = (paramArrayOfRect[0].height() - localRect3.top);
        }
      }
      while (true)
      {
        i++;
        break;
        k = 1;
        break label59;
        label496: float f1 = localRect4.width() / localRect5.width();
        int m = Math.round(localRect1.width() * (localRect4.height() / localRect5.height()));
        localRect3.left = Math.max(localRect2.left, Math.round(localRect1.exactCenterX() - m / 2));
        localRect3.right = Math.min(localRect2.right, paramArrayOfRect[0].width() - localRect3.left);
        localRect3.left = (paramArrayOfRect[0].width() - localRect3.right);
        int n = Math.round(Math.min(localRect3.width() / m_preview_aspect_ratio[paramInt][k], f1 * localRect1.height()));
        localRect3.top = localRect2.top;
        localRect3.bottom = Math.min(localRect2.bottom, n - localRect3.top);
        float f2 = localRect3.top + localRect3.height() * (1.0F - localRect5.height() / localRect4.height()) / 2.0F;
        if (f2 < 0.0F)
          localRect3.top = (localRect3.bottom - localRect3.height() - Math.round(f2 * f1));
        float f3 = localRect3.bottom - localRect3.height() * (1.0F - localRect5.height() / localRect4.height()) / 2.0F;
        if (f3 > localRect1.bottom)
          localRect3.bottom = (localRect3.top + localRect3.height() - Math.round(f1 * (f3 - localRect1.bottom)));
        if ((0x1 & localRect3.height()) == 1)
          localRect3.bottom -= 1;
        int i1 = Math.round(localRect3.height() * m_preview_aspect_ratio[paramInt][k]);
        if ((i1 > 2 + localRect3.width()) || (i1 < localRect3.width() - 2))
        {
          localRect3.left = Math.round(localRect1.exactCenterX() - i1 / 2);
          localRect3.right = (paramArrayOfRect[0].width() - localRect3.left);
        }
      }
    }
  }

  private static Rect calcBigSize(float paramFloat, int paramInt)
  {
    Rect localRect = views[paramInt][0];
    int i = localRect.width();
    int j = localRect.height();
    if (m_screen_aspect_ratio[paramInt] > paramFloat)
    {
      int m = Math.round(paramFloat * j / 2.0F);
      if (paramInt == 0)
        return new Rect(0, 0, m * 2, j);
      return new Rect(i / 2 - m, 0, m + i / 2, j);
    }
    int k = Math.round(i / (2.0F * paramFloat));
    if (paramInt == 0)
      return new Rect(0, j / 2 - k, i, k + j / 2);
    return new Rect(0, 0, i, k * 2);
  }

  private static void calcBlankFrame(Rect[] paramArrayOfRect, int paramInt)
  {
    int[] arrayOfInt = { 3, 4, 5, 5 };
    Rect localRect1 = paramArrayOfRect[0];
    Rect[] arrayOfRect = new Rect[4];
    Rect localRect2 = views[paramInt][14];
    arrayOfRect[1] = localRect2;
    arrayOfRect[0] = localRect2;
    int m;
    label89: Rect localRect5;
    if (m_isH264Renderer)
    {
      arrayOfRect[2] = views[paramInt][12];
      arrayOfRect[3] = views[paramInt][13];
      if (paramInt != 0)
        break label668;
      m = 0;
      if (m >= arrayOfInt.length)
        return;
      localRect5 = paramArrayOfRect[arrayOfInt[m]];
      if (localRect5.right >= localRect1.width())
        break label465;
      if (localRect5.right < arrayOfRect[m].right)
        break label257;
      paramArrayOfRect[(15 + m * 6)] = new Rect(0, 0, 0, 0);
      paramArrayOfRect[(16 + m * 6)] = new Rect(localRect5.right, 0, localRect1.width(), localRect1.height());
      paramArrayOfRect[(17 + m * 6)] = new Rect(0, 0, 0, 0);
      paramArrayOfRect[(18 + m * 6)] = new Rect(0, 0, 0, 0);
    }
    while (true)
    {
      m++;
      break label89;
      Rect localRect3 = views[paramInt][11];
      arrayOfRect[3] = localRect3;
      arrayOfRect[2] = localRect3;
      break;
      label257: int n;
      if (localRect5.right < arrayOfRect[m].left)
      {
        int i1 = arrayOfRect[m].left;
        paramArrayOfRect[(15 + m * 6)] = new Rect(localRect5.right, 0, arrayOfRect[m].left, localRect1.height());
        n = i1;
      }
      while (true)
      {
        paramArrayOfRect[(16 + m * 6)] = new Rect(arrayOfRect[m].right, 0, localRect1.width(), localRect1.height());
        paramArrayOfRect[(17 + m * 6)] = new Rect(n, 0, arrayOfRect[m].right, arrayOfRect[m].top);
        paramArrayOfRect[(18 + m * 6)] = new Rect(n, arrayOfRect[m].bottom, arrayOfRect[m].right, localRect1.height());
        break;
        n = localRect5.right;
        paramArrayOfRect[(15 + m * 6)] = new Rect(0, 0, 0, 0);
      }
      label465: if (localRect5.bottom < localRect1.height())
      {
        paramArrayOfRect[(15 + m * 6)] = new Rect(0, 0, 0, 0);
        paramArrayOfRect[(16 + m * 6)] = new Rect(0, 0, 0, 0);
        paramArrayOfRect[(17 + m * 6)] = new Rect(0, 0, localRect1.width(), localRect5.top);
        paramArrayOfRect[(18 + m * 6)] = new Rect(0, localRect5.bottom, localRect1.width(), localRect1.height());
      }
      else
      {
        paramArrayOfRect[(15 + m * 6)] = new Rect(0, 0, 0, 0);
        paramArrayOfRect[(16 + m * 6)] = new Rect(0, 0, 0, 0);
        paramArrayOfRect[(17 + m * 6)] = new Rect(0, 0, 0, 0);
        paramArrayOfRect[(18 + m * 6)] = new Rect(0, 0, 0, 0);
      }
    }
    label668: int i = 0;
    if (i < arrayOfInt.length)
    {
      Rect localRect4 = paramArrayOfRect[arrayOfInt[i]];
      if (localRect4.bottom < localRect1.height())
        if (localRect4.bottom >= arrayOfRect[i].bottom)
        {
          paramArrayOfRect[(17 + i * 6)] = new Rect(0, 0, 0, 0);
          paramArrayOfRect[(18 + i * 6)] = new Rect(0, localRect4.bottom, localRect1.width(), localRect1.height());
          paramArrayOfRect[(16 + i * 6)] = new Rect(0, 0, 0, 0);
          paramArrayOfRect[(15 + i * 6)] = new Rect(0, 0, 0, 0);
        }
      while (true)
      {
        i++;
        break;
        int j;
        if (localRect4.bottom < arrayOfRect[i].top)
        {
          int k = arrayOfRect[i].top;
          paramArrayOfRect[(17 + i * 6)] = new Rect(0, localRect4.bottom, localRect1.width(), arrayOfRect[i].top);
          j = k;
        }
        while (true)
        {
          paramArrayOfRect[(18 + i * 6)] = new Rect(0, arrayOfRect[i].bottom, localRect1.width(), localRect1.height());
          paramArrayOfRect[(16 + i * 6)] = new Rect(arrayOfRect[i].right, j, localRect1.width(), arrayOfRect[i].bottom);
          paramArrayOfRect[(15 + i * 6)] = new Rect(0, j, arrayOfRect[i].left, arrayOfRect[i].bottom);
          break;
          j = localRect4.bottom;
          paramArrayOfRect[(17 + i * 6)] = new Rect(0, 0, 0, 0);
        }
        if (localRect4.right < localRect1.width())
        {
          paramArrayOfRect[(17 + i * 6)] = new Rect(0, 0, 0, 0);
          paramArrayOfRect[(18 + i * 6)] = new Rect(0, 0, 0, 0);
          paramArrayOfRect[(16 + i * 6)] = new Rect(localRect4.right, 0, localRect1.width(), localRect1.height());
          paramArrayOfRect[(15 + i * 6)] = new Rect(0, 0, localRect4.left, localRect1.height());
        }
        else
        {
          paramArrayOfRect[(17 + i * 6)] = new Rect(0, 0, 0, 0);
          paramArrayOfRect[(18 + i * 6)] = new Rect(0, 0, 0, 0);
          paramArrayOfRect[(16 + i * 6)] = new Rect(0, 0, 0, 0);
          paramArrayOfRect[(15 + i * 6)] = new Rect(0, 0, 0, 0);
        }
      }
    }
  }

  private static int calcBlankFrameAuxSize(Rect[] paramArrayOfRect, int paramInt)
  {
    paramArrayOfRect[19] = new Rect(0, 0, 0, 0);
    paramArrayOfRect[20] = new Rect(0, 0, 0, 0);
    paramArrayOfRect[31] = new Rect(0, 0, 0, 0);
    paramArrayOfRect[32] = new Rect(0, 0, 0, 0);
    paramArrayOfRect[37] = new Rect(0, 0, 0, 0);
    paramArrayOfRect[38] = new Rect(0, 0, 0, 0);
    int i;
    if (paramInt == 0)
    {
      paramArrayOfRect[25] = new Rect(0, 0, Math.min(paramArrayOfRect[0].right, paramArrayOfRect[4].right), 0);
      paramArrayOfRect[26] = new Rect(0, paramArrayOfRect[0].bottom, Math.min(paramArrayOfRect[0].right, paramArrayOfRect[4].right), paramArrayOfRect[0].bottom);
      if (!isSamsungTablet7Inch)
        break label365;
      i = Math.round((paramArrayOfRect[0].height() - paramArrayOfRect[4].width() / m_capture_aspect_ratio[paramInt][1]) / 2.0F);
      if (i > 0)
      {
        paramArrayOfRect[25].bottom = i;
        paramArrayOfRect[26].top = (paramArrayOfRect[0].height() - i);
      }
    }
    while (true)
    {
      if (i < 0)
        i = 0;
      return i;
      paramArrayOfRect[25] = new Rect(0, 0, 0, Math.min(paramArrayOfRect[0].bottom, paramArrayOfRect[4].bottom));
      paramArrayOfRect[26] = new Rect(paramArrayOfRect[0].right, 0, paramArrayOfRect[0].right, Math.min(paramArrayOfRect[0].bottom, paramArrayOfRect[4].bottom));
      if (isSamsungTablet7Inch)
      {
        i = Math.round((paramArrayOfRect[0].width() - paramArrayOfRect[4].height() * m_capture_aspect_ratio[paramInt][1]) / 2.0F);
        if (i > 0)
        {
          paramArrayOfRect[25].right = i;
          paramArrayOfRect[26].left = (paramArrayOfRect[0].width() - i);
        }
      }
      else
      {
        label365: i = 0;
      }
    }
  }

  private static Rect calcButtonBackground(Rect paramRect1, Rect paramRect2, int paramInt)
  {
    Rect localRect = views[paramInt][0];
    int k;
    int i;
    int m;
    int j;
    if (paramInt == 0)
    {
      if (paramRect1.right <= paramRect2.left)
        break label153;
      int i2 = paramRect2.left;
      k = Math.max(0, paramRect1.top);
      int i3 = Math.min(localRect.right, paramRect1.right);
      i = Math.min(localRect.bottom, paramRect1.bottom);
      m = i2;
      j = i3;
    }
    while (true)
    {
      return new Rect(m, k, j, i);
      if (paramRect1.bottom > paramRect2.top)
      {
        int n = Math.max(0, paramRect1.left);
        k = paramRect2.top;
        int i1 = Math.min(localRect.right, paramRect1.right);
        i = Math.min(localRect.bottom, paramRect1.bottom);
        m = n;
        j = i1;
      }
      else
      {
        label153: i = 0;
        j = 0;
        k = 0;
        m = 0;
      }
    }
  }

  private static Rect calcSmallSize(Rect paramRect, float paramFloat, int paramInt)
  {
    int j;
    int i;
    if (paramInt == 0)
    {
      int k = Math.round(paramRect.width() / 8.6F);
      int m = Math.round(k / paramFloat);
      j = k;
      i = m;
    }
    while (true)
    {
      return new Rect(0, 0, j * 2, i * 2);
      i = Math.round(paramRect.height() / 8.6F);
      j = Math.round(paramFloat * i);
    }
  }

  private static Rect calcSmallViewFrame(Rect paramRect)
  {
    int i = 6 + paramRect.width();
    int j = 6 + paramRect.height();
    int k = paramRect.left - 3;
    int m = paramRect.top - 3;
    return new Rect(k, m, i + k, j + m);
  }

  private static void calcSmallViewPosition(Rect paramRect1, Rect paramRect2, int paramInt)
  {
    Rect localRect1 = views[paramInt][2];
    Rect localRect2 = views[paramInt][0];
    int m;
    int i;
    if (paramInt == 0)
    {
      int k = Math.round(0.975F * views[paramInt][0].width()) - localRect1.width() - paramRect2.width();
      m = Math.max(localRect2.top, paramRect1.top) + Math.round(0.975F * Math.min(localRect2.height(), paramRect1.height())) - paramRect2.height();
      i = k;
    }
    for (int j = m; ; j = Math.round(0.975F * views[paramInt][0].height()) - localRect1.height() - paramRect2.height())
    {
      paramRect2.left = (i + paramRect2.left);
      paramRect2.right = (i + paramRect2.right);
      paramRect2.top = (j + paramRect2.top);
      paramRect2.bottom = (j + paramRect2.bottom);
      return;
      i = Math.min(localRect2.right, paramRect1.right) - Math.round(0.975F * Math.min(localRect2.width(), paramRect1.width()));
    }
  }

  private static Rect calcSwitchCameraButton(Rect paramRect, int paramInt)
  {
    int i = Math.min(views[paramInt][0].width(), paramRect.width());
    int j = Math.min(views[paramInt][0].height(), paramRect.height());
    int k = Math.round(0.025F * i);
    int m = Math.round(0.025F * j);
    int i3;
    int i2;
    if (paramInt == 0)
    {
      i3 = (int)(0.3F * j);
      i2 = (int)(0.5555556F * i3);
    }
    while (true)
    {
      if (paramRect.top > 0)
        m += paramRect.top;
      return new Rect(k, m, i2 + k, i3 + m);
      int n = (int)(0.3F * i);
      int i1 = (int)(0.5555556F * n);
      k = Math.min(views[paramInt][0].right, paramRect.right) - n - k;
      i2 = n;
      i3 = i1;
    }
  }

  private static void dump()
  {
    Log.v("VideoView", "BACK:  cameraSize=" + cameraSize[0].toShortString() + " captureSize=" + captureSize[0].toShortString() + " orientation=" + orientations[0]);
    Log.v("VideoView", "FRONT: cameraSize=" + cameraSize[1].toShortString() + " captureSize=" + captureSize[1].toShortString() + " orientation=" + orientations[1]);
    for (int i = 0; i <= 44; i++)
      if (views[0][i] != null)
        Log.v("VideoView", "views[L][" + i + "] " + views[0][i].toShortString() + " " + views[0][i].width() + " " + views[0][i].height());
    for (int j = 0; j <= 44; j++)
      if (views[1][j] != null)
        Log.v("VideoView", "views[P][" + j + "] " + views[1][j].toShortString() + " " + views[1][j].width() + " " + views[1][j].height());
  }

  public static Rect getBorderRect(int paramInt1, int paramInt2)
  {
    Rect localRect = getRect(paramInt1, paramInt2);
    int i = 6 + localRect.width();
    int j = 6 + localRect.height();
    int k = localRect.left - 3;
    int m = localRect.top - 3;
    return new Rect(k, m, i + k, j + m);
  }

  public static Rect getButton(int paramInt)
  {
    return views[paramInt][2];
  }

  public static Rect getCameraSize(int paramInt)
  {
    if (cameraSize[paramInt] == null)
      cameraSize[paramInt] = new Rect(0, 0, 320, 240);
    return cameraSize[paramInt];
  }

  public static Rect getCaptureSize(int paramInt)
  {
    if (captureSize[paramInt] == null)
      captureSize[paramInt] = new Rect(0, 0, 192, 128);
    return captureSize[paramInt];
  }

  public static int getOrientation(int paramInt)
  {
    return orientations[paramInt];
  }

  public static Rect getRect(int paramInt1, int paramInt2)
  {
    if (isNeedUpdate)
    {
      initSize();
      isNeedUpdate = false;
    }
    return views[paramInt1][paramInt2];
  }

  public static Rect getScreen(int paramInt)
  {
    return views[paramInt][0];
  }

  private static Rect getVirtualSize(int paramInt)
  {
    if (views[paramInt][1] == null)
      views[paramInt][1] = new Rect(0, 0, 2147483647, 2147483647);
    return views[paramInt][1];
  }

  public static void init(int paramInt1, int paramInt2)
  {
    cameraSize = new Rect[2];
    captureSize = new Rect[4];
    orientations = new int[2];
    views = (Rect[][])Array.newInstance(Rect.class, new int[] { 2, 45 });
    if (paramInt1 > paramInt2)
    {
      initLandscape(paramInt1, paramInt2, views[0]);
      initPortrait(paramInt2, paramInt1, views[1]);
    }
    while (true)
    {
      if ((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (((paramInt1 == 1024) && (paramInt2 == 600)) || ((paramInt2 == 1024) && (paramInt1 == 600))))
        isSamsungTablet7Inch = true;
      if ((Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (((paramInt1 == 1280) && (paramInt2 == 800)) || ((paramInt2 == 1280) && (paramInt1 == 800))))
      {
        setOrientation(0, 1);
        setOrientation(1, 1);
      }
      initVirtualSize();
      isNeedUpdate = true;
      return;
      initLandscape(paramInt2, paramInt1, views[0]);
      initPortrait(paramInt1, paramInt2, views[1]);
    }
  }

  private static void initLandscape(int paramInt1, int paramInt2, Rect[] paramArrayOfRect)
  {
    int i = Math.round(0.1666667F * paramInt2);
    paramArrayOfRect[0] = new Rect(0, 0, paramInt1, paramInt2);
    paramArrayOfRect[2] = new Rect(paramInt1 - i, 0, paramInt1, paramInt2);
    if ((paramInt1 != 0) && (paramInt2 != 0))
      m_screen_aspect_ratio[0] = (paramInt1 / paramInt2);
  }

  private static void initPortrait(int paramInt1, int paramInt2, Rect[] paramArrayOfRect)
  {
    int i = Math.round(0.1666667F * paramInt1);
    paramArrayOfRect[0] = new Rect(0, 0, paramInt1, paramInt2);
    paramArrayOfRect[2] = new Rect(0, paramInt2 - i, paramInt1, paramInt2);
    if ((paramInt1 != 0) && (paramInt2 != 0))
      m_screen_aspect_ratio[1] = (paramInt1 / paramInt2);
  }

  private static void initSize()
  {
    initView(0);
    initView(1);
  }

  private static void initView(int paramInt)
  {
    Rect[] arrayOfRect = views[paramInt];
    arrayOfRect[5] = calcBigSize(m_renderer_aspect_ratio[paramInt], paramInt);
    calcBigCameraSize(arrayOfRect, paramInt);
    int i = calcBlankFrameAuxSize(arrayOfRect, paramInt);
    arrayOfRect[6] = calcSmallSize(arrayOfRect[0], m_capture_aspect_ratio[paramInt][2], paramInt);
    arrayOfRect[9] = calcSmallSize(arrayOfRect[0], m_renderer_aspect_ratio[paramInt], paramInt);
    calcSmallViewPosition(arrayOfRect[5], arrayOfRect[6], paramInt);
    calcSmallViewPosition(arrayOfRect[4], arrayOfRect[9], paramInt);
    if (paramInt == 0)
    {
      Rect localRect3 = arrayOfRect[9];
      localRect3.top -= i;
      Rect localRect4 = arrayOfRect[9];
      localRect4.bottom -= i;
    }
    while (true)
    {
      arrayOfRect[7] = new Rect(arrayOfRect[6]);
      arrayOfRect[8] = new Rect(arrayOfRect[6]);
      layoutAdjustSmallCameraSize(arrayOfRect, paramInt);
      arrayOfRect[10] = new Rect(arrayOfRect[6]);
      arrayOfRect[11] = calcSmallViewFrame(views[paramInt][6]);
      arrayOfRect[12] = calcSmallViewFrame(views[paramInt][7]);
      arrayOfRect[13] = calcSmallViewFrame(views[paramInt][8]);
      arrayOfRect[14] = calcSmallViewFrame(views[paramInt][9]);
      calcBlankFrame(arrayOfRect, paramInt);
      arrayOfRect[39] = calcButtonBackground(arrayOfRect[3], arrayOfRect[2], paramInt);
      arrayOfRect[40] = calcButtonBackground(arrayOfRect[4], arrayOfRect[2], paramInt);
      arrayOfRect[41] = calcButtonBackground(arrayOfRect[5], arrayOfRect[2], paramInt);
      arrayOfRect[42] = calcSwitchCameraButton(arrayOfRect[3], paramInt);
      arrayOfRect[43] = calcSwitchCameraButton(arrayOfRect[4], paramInt);
      arrayOfRect[44] = calcSwitchCameraButton(arrayOfRect[5], paramInt);
      return;
      Rect localRect1 = arrayOfRect[9];
      localRect1.left = (i + localRect1.left);
      Rect localRect2 = arrayOfRect[9];
      localRect2.right = (i + localRect2.right);
    }
  }

  private static void initVirtualSize()
  {
    if ((!isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0))
      if ((Build.MODEL.equals("Nexus S")) || (Build.MODEL.equals("GT-P1010")))
      {
        setVirtualSize(0, getScreen(0));
        setVirtualSize(1, getScreen(1));
      }
    do
    {
      while (true)
      {
        return;
        if ((!Build.MODEL.equals("SPH-M920")) && (!Build.MODEL.equals("SGH-T959V")) && (!Build.MODEL.equals("SCH-I510")) && (!Build.MODEL.equals("SGH-T839")) && (!Build.MODEL.equals("SAMSUNG-SGH-I997")) && (!Build.MODEL.equals("YP-GB1")))
        {
          if (isSamsungTablet7Inch)
          {
            setVirtualSize(0, 1024, 1024);
            setVirtualSize(1, 1024, 1024);
          }
          while (Build.VERSION.SDK_INT < 8)
          {
            setOrientation(1, 1);
            return;
            if ((Build.MODEL.equals("GT-I9003")) || (Build.MODEL.equals("GT-I9003L")))
            {
              setVirtualSize(0, getScreen(0));
              setVirtualSize(1, getScreen(1));
            }
            else
            {
              setVirtualSize(0, 800, 800);
              setVirtualSize(1, 800, 800);
            }
          }
        }
      }
      if ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0) && (Build.MODEL.equals("GT-I9000")))
      {
        setVirtualSize(0, 800, 800);
        setVirtualSize(1, 800, 800);
        return;
      }
      if (((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.equals("HTC Glacier"))) || ((isGingerbread) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0)) || ((Build.MANUFACTURER.equals("Dell Inc.")) && (Build.MODEL.equals("Dell Streak 7"))) || ((Build.MANUFACTURER.compareToIgnoreCase("HUAWEI") == 0) && (Build.MODEL.equals("M886"))) || ((Build.MANUFACTURER.compareToIgnoreCase("lge") == 0) && ((Build.MODEL.startsWith("LG-P990")) || (Build.MODEL.equals("LG-P999")) || (Build.MODEL.equals("LG-SU660")) || (Build.MODEL.startsWith("VS910 4G")) || (Build.MODEL.equals("LG-MS910")))) || ((Build.MANUFACTURER.equals("HTC")) && ((Build.MODEL.equals("ADR6400L")) || (Build.MODEL.startsWith("HTC Sensation")) || (Build.MODEL.startsWith("HTC Sensation XL")) || (Build.MODEL.startsWith("HTC Bliss")) || (Build.MODEL.startsWith("HTC Rhyme")) || (Build.MODEL.startsWith("HTC Runnymede")) || (Build.MODEL.equals("PG86100")))) || (Build.MANUFACTURER.compareToIgnoreCase("Motorola") == 0) || ((Build.MANUFACTURER.equals("Sony Ericsson")) && (Build.MODEL.startsWith("R800"))) || (Build.MANUFACTURER.compareToIgnoreCase("Lenovo") == 0))
      {
        setVirtualSize(0, getScreen(0));
        setVirtualSize(1, getScreen(1));
        return;
      }
    }
    while ((isGingerbread) || (Build.MANUFACTURER.compareToIgnoreCase("PANTECH") != 0));
    setVirtualSize(0, 800, 800);
    setVirtualSize(1, 800, 800);
  }

  private static boolean isRectEqual(Rect paramRect1, Rect paramRect2)
  {
    return (paramRect1.left == paramRect2.left) || (paramRect1.right == paramRect2.right) || (paramRect1.top == paramRect2.top) || (paramRect1.bottom == paramRect2.bottom);
  }

  private static boolean isRectInclude(Rect paramRect1, Rect paramRect2)
  {
    return (paramRect2.isEmpty()) || ((paramRect1.left <= paramRect2.left) && (paramRect1.right >= paramRect2.right) && (paramRect1.top <= paramRect2.top) && (paramRect1.bottom >= paramRect2.bottom));
  }

  private static boolean isRectLEqualP(Rect paramRect1, Rect paramRect2)
  {
    int i = views[0][0].height();
    return ((paramRect1.isEmpty()) && (paramRect2.isEmpty())) || ((paramRect1.left == paramRect2.top) && (paramRect1.right == paramRect2.bottom) && (paramRect1.top == i - paramRect2.right) && (paramRect1.bottom == i - paramRect2.left));
  }

  private static boolean isRectNoIntersaction(Rect paramRect1, Rect paramRect2)
  {
    return (paramRect1.left >= paramRect2.right) || (paramRect1.right <= paramRect2.left) || (paramRect1.top >= paramRect2.bottom) || (paramRect1.bottom <= paramRect2.top);
  }

  private static void layoutAdjustSmallCameraSize(Rect[] paramArrayOfRect, int paramInt)
  {
    Rect localRect1 = getVirtualSize(paramInt);
    int i = 0;
    if (i < cameraSmallViews.length)
    {
      int j = cameraSmallViews[i];
      Rect localRect2 = paramArrayOfRect[j];
      int k = (int)localRect2.exactCenterX();
      int m = (int)localRect2.exactCenterY();
      int n;
      label54: Rect localRect3;
      Rect localRect4;
      int i2;
      int i3;
      if (j == 7)
      {
        n = 0;
        localRect3 = getCameraSize(n);
        localRect4 = getCaptureSize(n);
        if (paramInt != 0)
          break label318;
        int i8 = Math.min(localRect1.right - k, Math.round(localRect2.width() * (localRect3.width() / (2 * localRect4.width()))));
        i2 = Math.min(localRect1.bottom - m, Math.round(localRect2.height() * (localRect3.height() / (2 * localRect4.height()))));
        i3 = i8;
        label150: if (i3 < i2 * m_preview_aspect_ratio[paramInt][n])
          break label399;
        i3 = Math.round(i2 * m_preview_aspect_ratio[paramInt][n]);
        label186: localRect2.left = (k - i3);
        localRect2.right = (i3 + k);
        localRect2.top = (m - i2);
        localRect2.bottom = (i2 + m);
        if (localRect2.width() >= paramArrayOfRect[6].width() - 2)
          break label419;
        i6 = paramArrayOfRect[6].width() / 2;
        i7 = Math.round(i6 / m_preview_aspect_ratio[paramInt][n]);
        localRect2.left = (k - i6);
        localRect2.right = (k + i6);
        localRect2.top = (localRect2.bottom - i7 * 2);
      }
      label318: 
      while (localRect2.height() >= paramArrayOfRect[6].height() - 2)
      {
        int i6;
        int i7;
        i++;
        break;
        n = 1;
        break label54;
        int i1 = Math.min(k - localRect1.left, Math.round(localRect2.width() * (localRect3.height() / (2 * localRect4.height()))));
        i2 = Math.min(localRect1.bottom - m, Math.round(localRect2.height() * (localRect3.width() / (2 * localRect4.width()))));
        i3 = i1;
        break label150;
        i2 = Math.round(i3 / m_preview_aspect_ratio[paramInt][n]);
        break label186;
      }
      label399: label419: int i4 = paramArrayOfRect[6].height() / 2;
      int i5 = Math.round(i4 * m_preview_aspect_ratio[paramInt][n]);
      if (paramInt == 0)
        localRect2.left = (localRect2.right - i5 * 2);
      while (true)
      {
        localRect2.top = (m - i4);
        localRect2.bottom = (m + i4);
        break;
        localRect2.right = (localRect2.left + i5 * 2);
      }
    }
  }

  public static void setCameraSize(int paramInt1, int paramInt2, int paramInt3)
  {
    cameraSize[paramInt1] = new Rect(0, 0, paramInt2, paramInt3);
    if ((paramInt2 != 0) && (paramInt3 != 0))
    {
      m_preview_aspect_ratio[0][paramInt1] = (paramInt2 / paramInt3);
      m_preview_aspect_ratio[1][paramInt1] = (paramInt3 / paramInt2);
      if ((m_preview_aspect_ratio[0][paramInt1] <= 2.0F) && (m_preview_aspect_ratio[0][paramInt1] >= 0.5D))
        break label122;
      Log.e("VideoView", "m_preview_aspect_ratio[Orientation.LANDSCAPE][" + paramInt1 + "] is not in a reasonable range: " + m_preview_aspect_ratio[0][paramInt1]);
    }
    while (true)
    {
      isNeedUpdate = true;
      return;
      label122: Log.v("VideoView", "m_preview_aspect_ratio[Orientation.LANDSCAPE][" + paramInt1 + "]=" + m_preview_aspect_ratio[0][paramInt1]);
    }
  }

  public static void setCaptureSize(int paramInt1, int paramInt2, int paramInt3)
  {
    captureSize[paramInt1] = new Rect(0, 0, paramInt2, paramInt3);
    if ((paramInt2 != 0) && (paramInt3 != 0))
    {
      m_capture_aspect_ratio[0][paramInt1] = (paramInt2 / paramInt3);
      m_capture_aspect_ratio[1][paramInt1] = (paramInt3 / paramInt2);
      if ((m_capture_aspect_ratio[0][paramInt1] <= 2.0F) && (m_capture_aspect_ratio[0][paramInt1] >= 0.5D))
        break label122;
      Log.e("VideoView", "m_capture_aspect_ratio[Orientation.LANDSCAPE][" + paramInt1 + "] is not in a reasonable range: " + m_capture_aspect_ratio[0][paramInt1]);
    }
    while (true)
    {
      isNeedUpdate = true;
      return;
      label122: Log.v("VideoView", "m_capture_aspect_ratio[Orientation.LANDSCAPE][" + paramInt1 + "]=" + m_capture_aspect_ratio[0][paramInt1]);
    }
  }

  public static void setIsH264Renderer(boolean paramBoolean)
  {
    m_isH264Renderer = paramBoolean;
    isNeedUpdate = true;
  }

  public static void setOrientation(int paramInt1, int paramInt2)
  {
    Log.v("VideoView", "set orientation of camera" + paramInt1 + " to " + paramInt2);
    orientations[paramInt1] = paramInt2;
    isNeedUpdate = true;
  }

  public static boolean setRenderSize(int paramInt1, int paramInt2)
  {
    if ((paramInt1 == 0) || (paramInt2 == 0))
      Log.e("VideoView", "setRenderSize() width=" + paramInt1 + " height=" + paramInt2 + " (cannot be zero!)");
    float f = paramInt1 / paramInt2;
    if ((m_renderer_aspect_ratio[0] - f > 0.03F) || (f - m_renderer_aspect_ratio[0] > 0.03F))
    {
      m_renderer_aspect_ratio[0] = f;
      m_renderer_aspect_ratio[1] = (paramInt2 / paramInt1);
      isNeedUpdate = true;
      if ((m_renderer_aspect_ratio[0] > 2.0F) || (m_renderer_aspect_ratio[0] < 0.5D))
        Log.e("VideoView", "m_renderer_aspect_ratio is not in a reasonable range: " + m_renderer_aspect_ratio[0]);
      while (true)
      {
        return true;
        Log.v("VideoView", "m_renderer_aspect_ratio[Orientation.LANDSCAPE]=" + m_renderer_aspect_ratio[0]);
      }
    }
    return false;
  }

  public static void setSamsungTablet(boolean paramBoolean)
  {
    Log.v("VideoView", "Setting samsung tablet to " + paramBoolean);
    isSamsungTablet7Inch = paramBoolean;
  }

  private static void setVirtualSize(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = (paramInt2 - views[paramInt1][0].width()) / 2;
    int j = (paramInt3 - views[paramInt1][0].height()) / 2;
    views[paramInt1][1] = new Rect(-i, -j, i + views[paramInt1][0].width(), j + views[paramInt1][0].height());
    isNeedUpdate = true;
  }

  private static void setVirtualSize(int paramInt, Rect paramRect)
  {
    views[paramInt][1] = new Rect(paramRect);
    isNeedUpdate = true;
  }

  private static void testView()
  {
    for (int i = 0; i <= 1; i++)
    {
      Rect[] arrayOfRect1 = views[i];
      if (!isRectInclude(arrayOfRect1[1], arrayOfRect1[0]))
        Log.e("VideoView", "ERROR: views[" + i + "]: VIRTUAL_SCREEN            doesn't include SCREEN");
      if (!isRectInclude(arrayOfRect1[0], arrayOfRect1[2]))
        Log.e("VideoView", "ERROR: views[" + i + "]: SCREEN                    doesn't include BUTTON");
      if (!isRectInclude(arrayOfRect1[0], arrayOfRect1[11]))
        Log.e("VideoView", "ERROR: views[" + i + "]: SCREEN                    doesn't include BORDER_PREFERRED_SMALL");
      if (!isRectInclude(arrayOfRect1[0], arrayOfRect1[14]))
        Log.e("VideoView", "ERROR: views[" + i + "]: SCREEN                    doesn't include BORDER_RENDERER_SMALL");
      if (!isRectInclude(arrayOfRect1[1], arrayOfRect1[3]))
        Log.e("VideoView", "ERROR: views[" + i + "]: VIRTUAL_SCREEN            doesn't include CAMERA_BACK_BIG");
      if (!isRectInclude(arrayOfRect1[1], arrayOfRect1[4]))
        Log.e("VideoView", "ERROR: views[" + i + "]: VIRTUAL_SCREEN            doesn't include CAMERA_FRONT_BIG");
      if (m_isH264Renderer)
      {
        if (!isRectInclude(arrayOfRect1[0], arrayOfRect1[12]))
          Log.e("VideoView", "ERROR: views[" + i + "]: SCREEN                    doesn't include BORDER_CAMERA_BACK_SMALL");
        if (!isRectInclude(arrayOfRect1[0], arrayOfRect1[13]))
          Log.e("VideoView", "ERROR: views[" + i + "]: SCREEN                    doesn't include BORDER_CAMERA_FRONT_SMALL");
        if (!isRectInclude(arrayOfRect1[0], arrayOfRect1[5]))
          Log.e("VideoView", "ERROR: views[" + i + "]: SCREEN                    doesn't include RENDERER_BIG");
        if (!isRectInclude(arrayOfRect1[7], arrayOfRect1[6]))
          Log.e("VideoView", "ERROR: views[" + i + "]: CAMERA_BACK_SMALL         doesn't include PREFERRED_SMALL");
        if (!isRectInclude(arrayOfRect1[8], arrayOfRect1[6]))
          Log.e("VideoView", "ERROR: views[" + i + "]: CAMERA_FRONT_SMALL        doesn't include PREFERRED_SMALL");
        if (!isRectInclude(arrayOfRect1[11], arrayOfRect1[6]))
          Log.e("VideoView", "ERROR: views[" + i + "]: BORDER_PREFERRED_SMALL    doesn't include PREFERRED_SMALL");
        if (!isRectInclude(arrayOfRect1[14], arrayOfRect1[9]))
          Log.e("VideoView", "ERROR: views[" + i + "]: BORDER_PREFERRED_SMALL    doesn't include RENDERER_SMALL");
        if (!isRectInclude(arrayOfRect1[12], arrayOfRect1[7]))
          Log.e("VideoView", "ERROR: views[" + i + "]: BORDER_CAMERA_BACK_SMALL  doesn't include CAMERA_BACK_SMALL");
        if (!isRectInclude(arrayOfRect1[13], arrayOfRect1[8]))
          Log.e("VideoView", "ERROR: views[" + i + "]: BORDER_CAMERA_FRONT_SMALL doesn't include CAMERA_FRONT_SMALL");
        if (!isRectInclude(arrayOfRect1[3], arrayOfRect1[42]))
          Log.e("VideoView", "ERROR: views[" + i + "]: CAMERA_BACK_BIG           doesn't include BUTTON_SWITCH_CAMERA_BACK");
        if (!isRectInclude(arrayOfRect1[4], arrayOfRect1[43]))
          Log.e("VideoView", "ERROR: views[" + i + "]: CAMERA_FRONT_BIG          doesn't include BUTTON_SWITCH_CAMERA_FRONT");
        if (!isRectInclude(arrayOfRect1[5], arrayOfRect1[44]))
          Log.e("VideoView", "ERROR: views[" + i + "]: RENDERER_BIG              doesn't include BUTTON_SWITCH_RENDERER");
        if (arrayOfRect1[39].isEmpty())
          break label1348;
        if (!isRectInclude(arrayOfRect1[2], arrayOfRect1[39]))
          Log.e("VideoView", "ERROR: views[" + i + "]: BUTTON                    doesn't include BUTTON_BACKGROUND_CAMERA_BACK");
        if (!isRectInclude(arrayOfRect1[3], arrayOfRect1[39]))
          Log.e("VideoView", "ERROR: views[" + i + "]: CAMERA_BACK_BIG           doesn't include BUTTON_BACKGROUND_CAMERA_BACK");
        label972: if (arrayOfRect1[40].isEmpty())
          break label1397;
        if (!isRectInclude(arrayOfRect1[2], arrayOfRect1[40]))
          Log.e("VideoView", "ERROR: views[" + i + "]: BUTTON                    doesn't include BUTTON_BACKGROUND_CAMERA_FRONT");
        if (!isRectInclude(arrayOfRect1[4], arrayOfRect1[40]))
          Log.e("VideoView", "ERROR: views[" + i + "]: CAMERA_FRONT_BIG          doesn't include BUTTON_BACKGROUND_CAMERA_FRONT");
        label1077: if (arrayOfRect1[41].isEmpty())
          break label1446;
        if (!isRectInclude(arrayOfRect1[2], arrayOfRect1[41]))
          Log.e("VideoView", "ERROR: views[" + i + "]: BUTTON                    doesn't include BUTTON_BACKGROUND_RENDERER");
        if (!isRectInclude(arrayOfRect1[5], arrayOfRect1[41]))
          Log.e("VideoView", "ERROR: views[" + i + "]: RENDERER_BIG              doesn't include BUTTON_BACKGROUND_RENDERER");
      }
      while (true)
      {
        for (int n = 15; n <= 44; n++)
          if (!isRectInclude(arrayOfRect1[0], arrayOfRect1[n]))
            Log.e("VideoView", "ERROR: views[" + i + "]: SCREEN                    doesn't include view " + n);
        if (!isRectInclude(arrayOfRect1[1], arrayOfRect1[7]))
          Log.e("VideoView", "ERROR: views[" + i + "]: VIRTUAL_SCREEN            doesn't include CAMERA_BACK_SMALL");
        if (isRectInclude(arrayOfRect1[1], arrayOfRect1[8]))
          break;
        Log.e("VideoView", "ERROR: views[" + i + "]: VIRTUAL_SCREEN            doesn't include CAMERA_FRONT_SMALL");
        break;
        label1348: if (isRectNoIntersaction(arrayOfRect1[3], arrayOfRect1[2]))
          break label972;
        Log.e("VideoView", "ERROR: views[" + i + "]: CAMERA_BACK_BIG           intersacts BUTTON");
        break label972;
        label1397: if (isRectNoIntersaction(arrayOfRect1[4], arrayOfRect1[2]))
          break label1077;
        Log.e("VideoView", "ERROR: views[" + i + "]: CAMERA_FRONT_BIG          intersacts BUTTON");
        break label1077;
        label1446: if (!isRectNoIntersaction(arrayOfRect1[5], arrayOfRect1[2]))
          Log.e("VideoView", "ERROR: views[" + i + "]: RENDERER_BIG              intersacts BUTTON");
      }
      for (int i1 = 15; i1 <= 38; i1 += 6)
        for (int i4 = i1; i4 < i1 + 6; i4++)
          for (int i5 = i4 + 1; i5 < i1 + 6; i5++)
            if (!isRectNoIntersaction(arrayOfRect1[i4], arrayOfRect1[i5]))
              Log.e("VideoView", "ERROR: views[" + i + "]: view " + i4 + " intersacts view " + i5);
      int[] arrayOfInt = { 3, 4, 5, 5 };
      Rect[] arrayOfRect2 = new Rect[4];
      Rect localRect1 = views[i][14];
      arrayOfRect2[1] = localRect1;
      arrayOfRect2[0] = localRect1;
      if (m_isH264Renderer)
      {
        arrayOfRect2[2] = views[i][12];
        arrayOfRect2[3] = views[i][13];
      }
      for (int i2 = 0; ; i2++)
      {
        if (i2 >= 4)
          break label2178;
        int i3 = 15 + i2 * 6;
        while (true)
          if (i3 < 4 + (15 + i2 * 6))
          {
            if (!isRectNoIntersaction(arrayOfRect1[arrayOfInt[i2]], arrayOfRect1[i3]))
              Log.e("VideoView", "ERROR: views[" + i + "]: view " + arrayOfInt[i2] + " intersacts view " + i3);
            if (!isRectNoIntersaction(arrayOfRect2[i2], arrayOfRect1[i3]))
              Log.e("VideoView", "ERROR: views[" + i + "]: border " + i2 + " intersacts view " + i3);
            i3++;
            continue;
            Rect localRect2 = views[i][11];
            arrayOfRect2[3] = localRect2;
            arrayOfRect2[2] = localRect2;
            break;
          }
        if (!isRectInclude(arrayOfRect1[arrayOfInt[i2]], arrayOfRect1[i3]))
          Log.e("VideoView", "ERROR: views[" + i + "]: view " + arrayOfInt[i2] + " doesn't include " + i3);
        if (!isRectNoIntersaction(arrayOfRect2[i2], arrayOfRect1[i3]))
          Log.e("VideoView", "ERROR: views[" + i + "]: view " + i3 + " intersacts border " + i2);
        if (!isRectInclude(arrayOfRect1[arrayOfInt[i2]], arrayOfRect1[(i3 + 1)]))
          Log.e("VideoView", "ERROR: views[" + i + "]: view " + arrayOfInt[i2] + " doesn't include " + (i3 + 1));
        if (!isRectNoIntersaction(arrayOfRect2[i2], arrayOfRect1[(i3 + 1)]))
          Log.e("VideoView", "ERROR: views[" + i + "]: border " + i2 + " intersacts view " + (i3 + 1));
      }
      label2178: if (!isRectNoIntersaction(arrayOfRect1[2], arrayOfRect1[6]))
        Log.e("VideoView", "ERROR: views[" + i + "]: BUTTON                    intersacts      PREFERRED_SMALL");
      if (!isRectNoIntersaction(arrayOfRect1[2], arrayOfRect1[9]))
        Log.e("VideoView", "ERROR: views[" + i + "]: BUTTON                    intersacts      RENDERER_SMALL");
      if (!isRectEqual(arrayOfRect1[6], arrayOfRect1[10]))
        Log.e("VideoView", "ERROR: views[" + i + "]: PREFERRED_SMALL           doesn't equal   TRANSPARENT");
    }
    for (int j = 0; j <= 10; j++)
      if (!isRectLEqualP(views[0][j], views[1][j]))
        Log.e("VideoView", "ERROR: views[L][" + j + "] doesn't equal views[P][" + j + "]");
    for (int k = 39; k <= 44; k++)
      if (!isRectLEqualP(views[0][k], views[1][k]))
        Log.e("VideoView", "ERROR: views[L][" + k + "] doesn't equal views[P][" + k + "]");
    for (int m = 0; m < 24; m += 6)
    {
      if (!isRectLEqualP(views[0][(m + 15)], views[1][(m + 17)]))
        Log.e("VideoView", "ERROR: views[L][" + (m + 15) + "] doesn't equal views[P][" + (m + 17) + "]");
      if (!isRectLEqualP(views[0][(m + 16)], views[1][(m + 18)]))
        Log.e("VideoView", "ERROR: views[L][" + (m + 16) + "] doesn't equal views[P][" + (m + 18) + "]");
      if (!isRectLEqualP(views[0][(m + 17)], views[1][(m + 16)]))
        Log.e("VideoView", "ERROR: views[L][" + (m + 17) + "] doesn't equal views[P][" + (m + 16) + "]");
      if (!isRectLEqualP(views[0][(m + 18)], views[1][(m + 15)]))
        Log.e("VideoView", "ERROR: views[L][" + (m + 18) + "] doesn't equal views[P][" + (m + 15) + "]");
      if (!isRectLEqualP(views[0][(m + 19)], views[1][(m + 20)]))
        Log.e("VideoView", "ERROR: views[L][" + (m + 19) + "] doesn't equal views[P][" + (m + 20) + "]");
      if (!isRectLEqualP(views[0][(m + 20)], views[1][(m + 19)]))
        Log.e("VideoView", "ERROR: views[L][" + (m + 20) + "] doesn't equal views[P][" + (m + 19) + "]");
    }
  }

  public static void updateContext(Context paramContext)
  {
    WindowManager localWindowManager = (WindowManager)paramContext.getSystemService("window");
    int i = localWindowManager.getDefaultDisplay().getWidth();
    int j = localWindowManager.getDefaultDisplay().getHeight();
    Log.v("VideoView", "updateContext(): width " + i + " height " + j);
    init(i, j);
  }

  public static abstract interface CameraType
  {
    public static final int BACK = 0;
    public static final int FRONT = 1;
    public static final int NONE = -1;
  }

  public static abstract interface Orientation
  {
    public static final int LANDSCAPE = 0;
    public static final int PORTRAIT = 1;
  }

  public static abstract interface ViewIndex
  {
    public static final int BLANK_FRAME_CAMERA_BACK_AUX0 = 19;
    public static final int BLANK_FRAME_CAMERA_BACK_AUX1 = 20;
    public static final int BLANK_FRAME_CAMERA_BACK_BOTTOM = 18;
    public static final int BLANK_FRAME_CAMERA_BACK_LEFT = 15;
    public static final int BLANK_FRAME_CAMERA_BACK_RIGHT = 16;
    public static final int BLANK_FRAME_CAMERA_BACK_TOP = 17;
    public static final int BLANK_FRAME_CAMERA_FRONT_AUX0 = 25;
    public static final int BLANK_FRAME_CAMERA_FRONT_AUX1 = 26;
    public static final int BLANK_FRAME_CAMERA_FRONT_BOTTOM = 24;
    public static final int BLANK_FRAME_CAMERA_FRONT_LEFT = 21;
    public static final int BLANK_FRAME_CAMERA_FRONT_RIGHT = 22;
    public static final int BLANK_FRAME_CAMERA_FRONT_TOP = 23;
    public static final int BLANK_FRAME_RENDERER_BACK_AUX0 = 31;
    public static final int BLANK_FRAME_RENDERER_BACK_AUX1 = 32;
    public static final int BLANK_FRAME_RENDERER_BACK_BOTTOM = 30;
    public static final int BLANK_FRAME_RENDERER_BACK_LEFT = 27;
    public static final int BLANK_FRAME_RENDERER_BACK_RIGHT = 28;
    public static final int BLANK_FRAME_RENDERER_BACK_TOP = 29;
    public static final int BLANK_FRAME_RENDERER_FRONT_AUX0 = 37;
    public static final int BLANK_FRAME_RENDERER_FRONT_AUX1 = 38;
    public static final int BLANK_FRAME_RENDERER_FRONT_BOTTOM = 36;
    public static final int BLANK_FRAME_RENDERER_FRONT_LEFT = 33;
    public static final int BLANK_FRAME_RENDERER_FRONT_RIGHT = 34;
    public static final int BLANK_FRAME_RENDERER_FRONT_TOP = 35;
    public static final int BORDER_CAMERA_BACK_SMALL = 12;
    public static final int BORDER_CAMERA_FRONT_SMALL = 13;
    public static final int BORDER_PREFERRED_SMALL = 11;
    public static final int BORDER_RENDERER_SMALL = 14;
    public static final int BUTTON = 2;
    public static final int BUTTON_BACKGROUND_CAMERA_BACK = 39;
    public static final int BUTTON_BACKGROUND_CAMERA_FRONT = 40;
    public static final int BUTTON_BACKGROUND_RENDERER = 41;
    public static final int BUTTON_SWITCH_CAMERA_BACK = 42;
    public static final int BUTTON_SWITCH_CAMERA_FRONT = 43;
    public static final int BUTTON_SWITCH_RENDERER = 44;
    public static final int CAMERA_BACK_BIG = 3;
    public static final int CAMERA_BACK_SMALL = 7;
    public static final int CAMERA_FRONT_BIG = 4;
    public static final int CAMERA_FRONT_SMALL = 8;
    public static final int PREFERRED_SMALL = 6;
    public static final int RENDERER_BIG = 5;
    public static final int RENDERER_SMALL = 9;
    public static final int SCREEN = 0;
    public static final int TRANSPARENT = 10;
    public static final int VIRTUAL_SCREEN = 1;
  }

  public static abstract interface ViewSize
  {
    public static final int BIG = 0;
    public static final int NONE = -1;
    public static final int SMALL = 1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoCapture.VideoView
 * JD-Core Version:    0.6.2
 */