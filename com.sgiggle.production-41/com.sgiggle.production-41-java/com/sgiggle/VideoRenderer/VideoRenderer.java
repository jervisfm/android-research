package com.sgiggle.VideoRenderer;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.sgiggle.util.Log;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.ReentrantLock;

public class VideoRenderer
{
  public static final int MESSAGE_RENDERER_SIZE = 3;
  private static final String TAG = "VideoRenderer";
  private static final float THRESHOLD_ASPECT_RATIO = 0.03F;
  private static SurfaceHolder _holder;
  private static boolean _holderModified;
  private static Bitmap.Config config;
  private static boolean isLandscape;
  private static Rect parentRect;
  private static VideoRenderer sInstance = null;
  private static final ReentrantLock s_lock;
  private static Rect transparentRect;
  private static Handler ui_handler;
  private ByteBuffer bb;
  private Bitmap bitmap;
  private int bpp;
  private Canvas canvas;
  private float current_aspect_ratio;
  private int current_height;
  private Rect current_parentRect = new Rect();
  private Rect current_transparentRect = new Rect();
  private int current_width;
  private Rect dst;
  private int expansion;
  private boolean isNativeBitmap;
  private boolean isStarted;
  private int rotation;
  private int small_height;
  private int small_width;
  private int small_x;
  private int small_y;
  private int[] transparent_region;

  static
  {
    _holderModified = false;
    config = Bitmap.Config.RGB_565;
    parentRect = new Rect();
    transparentRect = new Rect();
    s_lock = new ReentrantLock(true);
    StackTraceElement[] arrayOfStackTraceElement = new Throwable().getStackTrace();
    int i = 0;
    if (i < arrayOfStackTraceElement.length)
      if (!arrayOfStackTraceElement[i].getClassName().startsWith("com.sgiggle.VideoCaptureTest"));
    for (int j = 1; ; j = 0)
    {
      if (j != 0)
        System.loadLibrary("VideoCapture");
      return;
      i++;
      break;
    }
  }

  public VideoRenderer()
  {
    try
    {
      s_lock.lock();
      sInstance = this;
      return;
    }
    finally
    {
      s_lock.unlock();
    }
  }

  private native void NV21toRGBclip(byte[] paramArrayOfByte, int paramInt1, int paramInt2, ByteBuffer paramByteBuffer, int paramInt3, int paramInt4, int paramInt5);

  public static void clear(int paramInt)
  {
    if (isH264Renderer());
    Rect localRect;
    int i;
    int j;
    do
    {
      return;
      localRect = _holder.getSurfaceFrame();
      i = localRect.width();
      j = localRect.height();
    }
    while ((i == 0) || (j == 0));
    Bitmap localBitmap = Bitmap.createBitmap(i, j, config);
    localBitmap.eraseColor(paramInt);
    Bitmap.Config localConfig1 = config;
    Bitmap.Config localConfig2 = Bitmap.Config.ARGB_8888;
    int k = 0;
    int m;
    int n;
    int i1;
    int i2;
    int[] arrayOfInt;
    if (localConfig1 == localConfig2)
    {
      m = transparentRect.width();
      n = transparentRect.height();
      i1 = transparentRect.left - parentRect.left;
      i2 = transparentRect.top - parentRect.top;
      arrayOfInt = new int[m * n];
      if (m <= i - i1)
        break label314;
    }
    label296: label314: for (int i3 = i - i1; ; i3 = m)
    {
      if (n > j - i2);
      for (int i4 = j - i2; ; i4 = n)
      {
        Log.v("VideoRenderer", "clear() (" + i1 + "," + i2 + ") " + i3 + "x" + i4);
        k = 0;
        if (i3 > 0)
        {
          k = 0;
          if (i4 > 0)
            localBitmap.setPixels(arrayOfInt, 0, i3, i1, i2, i3, i4);
        }
        label245: if (k < 2)
        {
          Canvas localCanvas = _holder.lockCanvas(null);
          if (localCanvas == null)
            break label296;
          localCanvas.drawBitmap(localBitmap, (Rect)null, localRect, null);
          _holder.unlockCanvasAndPost(localCanvas);
        }
        while (true)
        {
          k++;
          break label245;
          break;
          Log.w("VideoRenderer", "clear: canvas is null");
        }
      }
    }
  }

  private native void convertColor(int paramInt1, int paramInt2, int paramInt3, ByteBuffer paramByteBuffer);

  private native void convertColorBitmap(int paramInt1, int paramInt2, int paramInt3, Bitmap paramBitmap);

  public static native boolean hasH264Renderer();

  private boolean init(int paramInt1, int paramInt2)
  {
    if ((_holder == null) || (!this.isStarted))
      return false;
    if ((this.bitmap == null) || (paramInt1 != this.current_width) || (paramInt2 != this.current_height) || ((parentRect != null) && (!parentRect.equals(this.current_parentRect))) || ((transparentRect != null) && (!transparentRect.equals(this.current_transparentRect))))
    {
      Log.v("VideoRenderer", "init " + paramInt1 + " " + paramInt2 + " " + parentRect.width() + " " + parentRect.height() + " " + transparentRect.width() + " " + transparentRect.height());
      if ((parentRect != null) && (!parentRect.equals(this.current_parentRect)))
        this.current_parentRect.set(parentRect);
      if ((transparentRect != null) && (!transparentRect.equals(this.current_transparentRect)))
        this.current_transparentRect.set(transparentRect);
      this.current_width = paramInt1;
      this.current_height = paramInt2;
      if ((paramInt1 == 0) || (paramInt2 == 0))
        Log.e("VideoRenderer", "init() width=" + paramInt1 + " height=" + paramInt2 + " (cannot be zero!)");
      float f1 = paramInt1 / paramInt2;
      if ((this.current_aspect_ratio - f1 > 0.03F) || (f1 - this.current_aspect_ratio > 0.03F))
      {
        updateRendererSize(paramInt1, paramInt2);
        this.current_aspect_ratio = f1;
      }
      if (parentRect != null)
      {
        if ((parentRect.width() == 0) || (parentRect.height() == 0))
          Log.e("VideoRenderer", "init() parentRect.width=" + parentRect.width() + " parentRect.height=" + parentRect.height() + " (cannot be zero!)");
        if (isLandscape);
        for (float f4 = parentRect.width() / parentRect.height(); (f4 - f1 > 0.03F) || (f1 - f4 > 0.03F); f4 = parentRect.height() / parentRect.width())
          return false;
      }
      this.expansion = (parentRect.width() / paramInt1);
      if (this.expansion < 3)
        break label894;
      config = Bitmap.Config.ARGB_8888;
      this.expansion = 2;
      if (config != Bitmap.Config.RGB_565)
        break label902;
      this.bpp = 2;
      label513: if (!isLandscape)
        break label910;
      this.bitmap = Bitmap.createBitmap(paramInt1 * this.expansion, paramInt2 * this.expansion, config);
      this.rotation = 0;
      label546: if (this.bitmap.getRowBytes() != this.bitmap.getWidth() * this.bpp)
        this.isNativeBitmap = false;
      if (config != Bitmap.Config.RGB_565)
        break label941;
      if (!this.isNativeBitmap)
        this.bb = ByteBuffer.allocateDirect(paramInt1 * paramInt2 * this.bpp);
      initColorConverter(0, paramInt1, paramInt2, this.bpp, this.rotation, this.expansion);
      this.transparent_region = null;
    }
    while (true)
    {
      Log.v("VideoRenderer", "bitmap " + this.bitmap.getWidth() + " " + this.bitmap.getHeight());
      Log.v("VideoRenderer", "isNativeBitmap " + this.isNativeBitmap);
      Log.v("VideoRenderer", "bpp " + this.bpp);
      Log.v("VideoRenderer", "rotation " + this.rotation);
      Log.v("VideoRenderer", "expansion " + this.expansion);
      Log.v("VideoRenderer", "transparent_region (" + this.small_x + "," + this.small_y + ") " + this.small_width + "x" + this.small_height);
      if ((this.dst == null) || (_holderModified))
      {
        _holderModified = false;
        this.dst = new Rect(_holder.getSurfaceFrame());
      }
      return true;
      label894: this.expansion = 1;
      break;
      label902: this.bpp = 4;
      break label513;
      label910: this.bitmap = Bitmap.createBitmap(paramInt2 * this.expansion, paramInt1 * this.expansion, config);
      this.rotation = 90;
      break label546;
      label941: if (!this.isNativeBitmap)
        this.bb = ByteBuffer.allocateDirect(paramInt2 * (paramInt1 * this.expansion) * this.expansion * this.bpp);
      initColorConverter(1, paramInt1, paramInt2, this.bpp, this.rotation, this.expansion);
      if (!transparentRect.isEmpty())
      {
        float f2 = this.bitmap.getWidth() / parentRect.width();
        float f3 = this.bitmap.getHeight() / parentRect.height();
        this.small_width = Math.round(f2 * transparentRect.width());
        this.small_height = Math.round(f3 * transparentRect.height());
        this.small_x = Math.round(f2 * (transparentRect.left - parentRect.left));
        this.small_y = Math.round(f3 * (transparentRect.top - parentRect.top));
        if (this.small_width > this.bitmap.getWidth() - this.small_x)
          this.small_width = (this.bitmap.getWidth() - this.small_x);
        if (this.small_height > this.bitmap.getHeight() - this.small_y)
          this.small_height = (this.bitmap.getHeight() - this.small_y);
        if ((this.small_width > 0) && (this.small_height > 0))
          this.transparent_region = new int[this.small_width * this.small_height];
        else
          this.transparent_region = null;
      }
      else
      {
        this.transparent_region = null;
      }
    }
  }

  private native void initColorConverter(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

  public static native boolean isH264Renderer();

  private void renderBuffer(Bitmap paramBitmap)
  {
    if (this.transparent_region != null)
      paramBitmap.setPixels(this.transparent_region, 0, this.small_width, this.small_x, this.small_y, this.small_width, this.small_height);
    this.canvas = _holder.lockCanvas();
    if (this.canvas != null)
    {
      this.canvas.drawBitmap(paramBitmap, (Rect)null, this.dst, null);
      _holder.unlockCanvasAndPost(this.canvas);
      return;
    }
    Log.w("VideoRenderer", "renderBuffer: canvas is null");
  }

  private void renderBuffer(ByteBuffer paramByteBuffer)
  {
    paramByteBuffer.position(0);
    this.bitmap.copyPixelsFromBuffer(paramByteBuffer);
    renderBuffer(this.bitmap);
  }

  public static void setActivityHandler(Handler paramHandler)
  {
    try
    {
      s_lock.lock();
      ui_handler = paramHandler;
      return;
    }
    finally
    {
      s_lock.unlock();
    }
  }

  public static void setBitmapConfig(Bitmap.Config paramConfig, Rect paramRect1, Rect paramRect2)
  {
    while (true)
    {
      try
      {
        s_lock.lock();
        config = paramConfig;
        parentRect.set(paramRect1);
        if (paramRect2 != null)
        {
          transparentRect.set(paramRect2);
          if (paramRect1.width() >= paramRect1.height())
            isLandscape = true;
        }
        else
        {
          transparentRect.set(0, 0, 0, 0);
          continue;
        }
      }
      finally
      {
        s_lock.unlock();
      }
      isLandscape = false;
    }
  }

  public static void setPreviewDisplay(SurfaceHolder paramSurfaceHolder)
  {
    Log.v("VideoRenderer", "setPreviewDisplay(holder=" + paramSurfaceHolder + ")");
    _holder = paramSurfaceHolder;
    _holderModified = true;
    if (paramSurfaceHolder == null);
    for (Surface localSurface = null; ; localSurface = paramSurfaceHolder.getSurface())
    {
      setSurface(localSurface);
      return;
    }
  }

  private static native void setSurface(Surface paramSurface);

  public static void staticStartRenderer()
  {
    try
    {
      s_lock.lock();
      if (sInstance != null)
        sInstance.startRenderer();
      return;
    }
    finally
    {
      s_lock.unlock();
    }
  }

  public static void staticStopRenderer()
  {
    try
    {
      s_lock.lock();
      if (sInstance != null)
        sInstance.stopRenderer();
      return;
    }
    finally
    {
      s_lock.unlock();
    }
  }

  private void updateRendererSize(int paramInt1, int paramInt2)
  {
    Log.d("VideoRenderer", "updateRendererSize()");
    try
    {
      s_lock.lock();
      if (ui_handler != null)
      {
        Log.d("VideoRenderer", "updateRendererSize: sending message to ui_handler");
        Message.obtain(ui_handler, 3, paramInt1, paramInt2).sendToTarget();
      }
      while (true)
      {
        return;
        Log.d("VideoRenderer", "updateLayout: no ui_handler");
      }
    }
    finally
    {
      s_lock.unlock();
    }
  }

  public void render(int paramInt1, int paramInt2, int paramInt3)
  {
    try
    {
      s_lock.lock();
      boolean bool = init(paramInt2, paramInt3);
      if (!bool)
        return;
      if (this.isNativeBitmap)
      {
        convertColorBitmap(paramInt1, paramInt2, paramInt3, this.bitmap);
        renderBuffer(this.bitmap);
      }
      while (true)
      {
        return;
        convertColor(paramInt1, paramInt2, paramInt3, this.bb);
        renderBuffer(this.bb);
      }
    }
    finally
    {
      s_lock.unlock();
    }
  }

  public void render(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2)
  {
    if (!init(paramInt1, paramInt2))
      return;
    renderBuffer(paramByteBuffer);
  }

  public void render(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    if (!init(paramInt3, paramInt4))
      return;
    NV21toRGBclip(paramArrayOfByte, paramInt1, paramInt2, this.bb, paramInt3, paramInt4, paramInt5);
    renderBuffer(this.bb);
  }

  public boolean startRenderer()
  {
    if (_holder == null)
      Log.e("VideoRenderer", "no surface");
    this.isStarted = true;
    return true;
  }

  public void stopRenderer()
  {
    this.isStarted = false;
    config = Bitmap.Config.RGB_565;
    this.bitmap = null;
    this.dst = null;
    this.bb = null;
    this.rotation = 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoRenderer.VideoRenderer
 * JD-Core Version:    0.6.2
 */