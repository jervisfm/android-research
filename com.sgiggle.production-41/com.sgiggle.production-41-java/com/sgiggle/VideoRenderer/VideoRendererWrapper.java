package com.sgiggle.VideoRenderer;

public class VideoRendererWrapper
{
  private static final String TAG = "VideoRendererWrapper";
  VideoRenderer m_videoRenderer = null;

  public void initialize()
  {
    this.m_videoRenderer = new VideoRenderer();
  }

  public void render(int paramInt1, int paramInt2, int paramInt3)
  {
    this.m_videoRenderer.render(paramInt1, paramInt2, paramInt3);
  }

  public boolean startRenderer()
  {
    return this.m_videoRenderer.startRenderer();
  }

  public void stopRenderer()
  {
    this.m_videoRenderer.stopRenderer();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.VideoRenderer.VideoRendererWrapper
 * JD-Core Version:    0.6.2
 */