package com.sgiggle.animation;

import android.opengl.GLSurfaceView.Renderer;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class KissRenderer
  implements GLSurfaceView.Renderer
{
  private static native void nativeInit();

  private static native void nativeRender();

  private static native void nativeResize(int paramInt1, int paramInt2);

  public void onDrawFrame(GL10 paramGL10)
  {
    nativeRender();
  }

  public void onSurfaceChanged(GL10 paramGL10, int paramInt1, int paramInt2)
  {
    nativeResize(paramInt1, paramInt2);
  }

  public void onSurfaceCreated(GL10 paramGL10, EGLConfig paramEGLConfig)
  {
    nativeInit();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.animation.KissRenderer
 * JD-Core Version:    0.6.2
 */