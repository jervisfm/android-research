package com.sgiggle.cafe;

import android.content.res.AssetManager;
import com.sgiggle.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CafePrivate
{
  private static AssetManager mAssetMgr;

  private static AssetManager GetAssetMgr()
  {
    return mAssetMgr;
  }

  public static void Init(AssetManager paramAssetManager)
  {
    mAssetMgr = paramAssetManager;
    LinkNative();
  }

  private static native void LinkNative();

  public static InputStream OpenForReadFile(String paramString)
  {
    try
    {
      InputStream localInputStream = GetAssetMgr().open(paramString);
      return localInputStream;
    }
    catch (IOException localIOException)
    {
      Log.e("CAFE-PRIVATE", "LOG-FAILED-GetAssetMgr().open(" + paramString + ")");
    }
    return null;
  }

  public static byte[] ReadFile(InputStream paramInputStream, int paramInt)
  {
    ByteArrayOutputStream localByteArrayOutputStream;
    byte[] arrayOfByte1;
    try
    {
      localByteArrayOutputStream = new ByteArrayOutputStream();
      if (paramInt >= 0)
        break label76;
      arrayOfByte1 = new byte[1024];
      while (true)
      {
        int i = paramInputStream.read(arrayOfByte1);
        if (i == -1)
          break;
        localByteArrayOutputStream.write(arrayOfByte1, 0, i);
      }
    }
    catch (IOException localIOException)
    {
      Log.e("CAFE-PRIVATE", "ReadFile ERROR");
      return new byte[0];
    }
    arrayOfByte1[0] = 0;
    localByteArrayOutputStream.write(arrayOfByte1, 0, 1);
    while (true)
    {
      return localByteArrayOutputStream.toByteArray();
      label76: byte[] arrayOfByte2 = new byte[paramInt];
      int j = paramInputStream.read(arrayOfByte2);
      if (j != -1)
        localByteArrayOutputStream.write(arrayOfByte2, 0, j);
      if (j < paramInt)
      {
        arrayOfByte2[0] = 0;
        localByteArrayOutputStream.write(arrayOfByte2, 0, 1);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.CafePrivate
 * JD-Core Version:    0.6.2
 */