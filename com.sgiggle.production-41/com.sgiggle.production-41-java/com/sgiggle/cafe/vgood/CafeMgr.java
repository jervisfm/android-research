package com.sgiggle.cafe.vgood;

import android.content.Context;
import com.sgiggle.cafe.CafePrivate;
import com.sgiggle.xmpp.SessionMessages.VGoodSupportType;

public class CafeMgr
{
  public static final String DEFAULT_CINEMATIC = "Surprise.Cafe";
  public static final String DEFAULT_CLIP = "";
  public static final int MAIN_VIEW = 0;
  public static final int VIEW_ALTERNATE = 1;
  public static final int VIEW_MAIN;
  static int sInitCouter = 0;
  public static boolean vgoodPurchased = false;
  public static SessionMessages.VGoodSupportType vgoodSupport;

  private static native void Free();

  public static void FreeEngine()
  {
    sInitCouter -= 1;
    if (sInitCouter <= 0)
    {
      Free();
      sInitCouter = 0;
    }
  }

  public static native void FreeGraphics();

  private static native void Init(boolean paramBoolean1, String paramString1, String paramString2, String paramString3, boolean paramBoolean2);

  public static void InitEngine(Context paramContext)
  {
    if (sInitCouter == 0)
    {
      CafePrivate.Init(paramContext.getAssets());
      if (CafeOpenGlConfigChooser.getOpenGlEsVersion() != 2)
        break label42;
    }
    label42: for (boolean bool = true; ; bool = false)
    {
      Init(true, "Media", "MediaZ", "Save", bool);
      sInitCouter = 1 + sInitCouter;
      return;
    }
  }

  public static native void InvalidateRenderView(int paramInt);

  public static native void LoadSurprise(String paramString1, String paramString2);

  public static native void OnSurfaceCreated();

  public static native void OnTouchBegan(int paramInt, float paramFloat1, float paramFloat2);

  public static native void OnTouchEnded(int paramInt, float paramFloat1, float paramFloat2);

  public static native void OnTouchMoved(int paramInt, float paramFloat1, float paramFloat2);

  public static native void Pause();

  public static native void PlayClip(int paramInt, String paramString, boolean paramBoolean);

  public static native void Render();

  public static native void RenderClear();

  public static native void RenderMain();

  public static native void RenderView(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3);

  public static void Reset()
  {
    Free();
    if (CafeOpenGlConfigChooser.getOpenGlEsVersion() == 2);
    for (boolean bool = true; ; bool = false)
    {
      Init(true, "Media", "MediaZ", "Save", bool);
      return;
    }
  }

  public static native void Resume();

  public static native void SetCallbacks();

  public static native void SetGodMode(boolean paramBoolean);

  public static native float SetRenderClearColor(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

  public static native void SetRenderView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

  public static native void SetSurpriseTrackVisibility(int paramInt, String paramString, boolean paramBoolean);

  public static native int StartAvatarSurpriseForClip(int paramInt, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, long paramLong, boolean paramBoolean2);

  public static native int StartSurprise(int paramInt, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, boolean paramBoolean);

  public static int StartSurprise(String paramString, long paramLong1, long paramLong2, boolean paramBoolean)
  {
    return StartSurprise(0, paramString, "Surprise.Cafe", "", paramLong1, paramLong2, paramBoolean);
  }

  public static native void StopAllSurprises();

  public static native void StopSurprise(int paramInt);

  public static native void forceUpdateAvatarTrack(boolean paramBoolean);

  public static void initVGoodStatus(SessionMessages.VGoodSupportType paramVGoodSupportType, boolean paramBoolean)
  {
    vgoodSupport = paramVGoodSupportType;
    vgoodPurchased = paramBoolean;
  }

  public static native void setAvatarSurpriseId(boolean paramBoolean, int paramInt);

  public static native void updateAvatarTracksVisibility();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.vgood.CafeMgr
 * JD-Core Version:    0.6.2
 */