package com.sgiggle.cafe.vgood;

import android.opengl.GLSurfaceView.EGLConfigChooser;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

public class CafeOpenGlConfigChooser
  implements GLSurfaceView.EGLConfigChooser
{
  private static int m_version = 0;

  static
  {
    checkVersion();
  }

  private static void checkVersion()
  {
    EGL10 localEGL10 = (EGL10)EGLContext.getEGL();
    EGLDisplay localEGLDisplay = localEGL10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
    localEGL10.eglInitialize(localEGLDisplay, new int[2]);
    if (getOpenGlEs2Config(localEGL10, localEGLDisplay) != null)
      m_version = 2;
    while (true)
    {
      localEGL10.eglTerminate(localEGLDisplay);
      return;
      if (getOpenGlEsConfig(localEGL10, localEGLDisplay) != null)
        m_version = 1;
      else
        m_version = 0;
    }
  }

  private static EGLConfig getAnyConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay)
  {
    EGLConfig[] arrayOfEGLConfig = new EGLConfig[1];
    int[] arrayOfInt = new int[1];
    paramEGL10.eglGetConfigs(paramEGLDisplay, arrayOfEGLConfig, 1, arrayOfInt);
    if (arrayOfInt[0] > 0)
      return arrayOfEGLConfig[0];
    return null;
  }

  private static EGLConfig getOpenGlEs2Config(EGL10 paramEGL10, EGLDisplay paramEGLDisplay)
  {
    int[] arrayOfInt1 = { 12324, 8, 12323, 8, 12322, 8, 12321, 8, 12352, 4, 12344 };
    EGLConfig[] arrayOfEGLConfig = new EGLConfig[1];
    int[] arrayOfInt2 = new int[1];
    paramEGL10.eglChooseConfig(paramEGLDisplay, arrayOfInt1, arrayOfEGLConfig, 1, arrayOfInt2);
    if (arrayOfInt2[0] > 0)
      return arrayOfEGLConfig[0];
    return null;
  }

  private static EGLConfig getOpenGlEsConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay)
  {
    int[] arrayOfInt1 = { 12324, 8, 12323, 8, 12322, 8, 12321, 8, 12344 };
    EGLConfig[] arrayOfEGLConfig = new EGLConfig[1];
    int[] arrayOfInt2 = new int[1];
    paramEGL10.eglChooseConfig(paramEGLDisplay, arrayOfInt1, arrayOfEGLConfig, 1, arrayOfInt2);
    if (arrayOfInt2[0] > 0)
      return arrayOfEGLConfig[0];
    return null;
  }

  public static int getOpenGlEsVersion()
  {
    return m_version;
  }

  public EGLConfig chooseConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay)
  {
    if (m_version == 2)
      return getOpenGlEs2Config(paramEGL10, paramEGLDisplay);
    if (m_version == 1)
      return getOpenGlEsConfig(paramEGL10, paramEGLDisplay);
    return getAnyConfig(paramEGL10, paramEGLDisplay);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.vgood.CafeOpenGlConfigChooser
 * JD-Core Version:    0.6.2
 */