package com.sgiggle.cafe.vgood;

import android.graphics.Rect;
import android.opengl.GLSurfaceView.Renderer;
import com.sgiggle.util.Log;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CafeRenderer
  implements GLSurfaceView.Renderer
{
  static final String TAG = "Cafe-UI";
  boolean mAutoAdjustRotation = false;
  private ArrayList<CafeViewRenderer> mCafeViewRenderers = new ArrayList();
  boolean mRotate = false;

  public CafeRenderer()
  {
  }

  public CafeRenderer(boolean paramBoolean1, boolean paramBoolean2)
  {
    this.mRotate = paramBoolean1;
    this.mAutoAdjustRotation = paramBoolean2;
  }

  public static void setCafeMainView(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    Log.v("Cafe-UI", "setCafeMainView " + paramInt1 + " " + paramInt2 + " " + paramBoolean1 + " " + paramBoolean2);
    Object localObject;
    int j;
    if (((paramBoolean2) && (paramInt1 > paramInt2)) || (paramBoolean1))
    {
      int k = paramInt2 * 3 / 2;
      if (k > paramInt1);
      for (Rect localRect2 = new Rect(paramInt1 - k, 0, paramInt1, paramInt2); ; localRect2 = new Rect(0, 0, paramInt1, paramInt2))
      {
        localObject = localRect2;
        j = 90;
        CafeMgr.SetRenderView(0, ((Rect)localObject).left, ((Rect)localObject).top, ((Rect)localObject).width(), ((Rect)localObject).height(), j);
        CafeMgr.SetRenderClearColor(0, 0.0F, 0.0F, 0.0F, 0.0F);
        return;
      }
    }
    int i = paramInt1 * 3 / 2;
    if (i > paramInt2);
    for (Rect localRect1 = new Rect(0, paramInt2 - i, paramInt1, paramInt2); ; localRect1 = new Rect(0, 0, paramInt1, paramInt2))
    {
      localObject = localRect1;
      j = 0;
      break;
    }
  }

  public void addCafeViewRenderer(CafeViewRenderer paramCafeViewRenderer)
  {
    this.mCafeViewRenderers.add(paramCafeViewRenderer);
  }

  public void clearCafeViewRenderers()
  {
    this.mCafeViewRenderers.clear();
  }

  public void onDrawFrame(GL10 paramGL10)
  {
    if (this.mCafeViewRenderers.size() > 0)
      for (int i = 0; i < this.mCafeViewRenderers.size(); i++)
      {
        CafeViewRenderer localCafeViewRenderer = (CafeViewRenderer)this.mCafeViewRenderers.get(i);
        if (localCafeViewRenderer != null)
          localCafeViewRenderer.render();
      }
    CafeMgr.RenderMain();
  }

  public void onSurfaceChanged(GL10 paramGL10, int paramInt1, int paramInt2)
  {
    if (this.mCafeViewRenderers.size() > 0)
      for (int i = 0; i < this.mCafeViewRenderers.size(); i++)
      {
        CafeViewRenderer localCafeViewRenderer = (CafeViewRenderer)this.mCafeViewRenderers.get(i);
        if (localCafeViewRenderer != null)
          localCafeViewRenderer.onSurfaceChanged(paramInt1, paramInt2, this.mRotate, this.mAutoAdjustRotation);
      }
    setCafeMainView(paramInt1, paramInt2, this.mRotate, this.mAutoAdjustRotation);
  }

  public void onSurfaceCreated(GL10 paramGL10, EGLConfig paramEGLConfig)
  {
    CafeMgr.OnSurfaceCreated();
    for (int i = 0; i < this.mCafeViewRenderers.size(); i++)
    {
      CafeViewRenderer localCafeViewRenderer = (CafeViewRenderer)this.mCafeViewRenderers.get(i);
      if (localCafeViewRenderer != null)
        localCafeViewRenderer.onSurfaceCreated();
    }
  }

  public static abstract interface CafeViewRenderer
  {
    public abstract void onSurfaceChanged(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2);

    public abstract void onSurfaceCreated();

    public abstract void render();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.vgood.CafeRenderer
 * JD-Core Version:    0.6.2
 */