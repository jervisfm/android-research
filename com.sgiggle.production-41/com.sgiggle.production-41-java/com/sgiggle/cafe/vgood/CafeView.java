package com.sgiggle.cafe.vgood;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class CafeView extends GLSurfaceView
{
  private boolean m_deliverTouchToCafe = false;
  private CafeRenderer m_renderer;

  public CafeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }

  private void init()
  {
    if ((Build.VERSION.SDK_INT >= 8) && (CafeOpenGlConfigChooser.getOpenGlEsVersion() == 2))
      setEGLContextClientVersion(2);
    setEGLConfigChooser(new CafeOpenGlConfigChooser());
    this.m_renderer = new CafeRenderer();
    setRenderer(this.m_renderer);
    getHolder().setFormat(-3);
  }

  public CafeRenderer getRenderer()
  {
    return this.m_renderer;
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = 0;
    if (!this.m_deliverTouchToCafe)
      return super.onTouchEvent(paramMotionEvent);
    int j = getHeight();
    switch (0xFF & paramMotionEvent.getAction())
    {
    case 3:
    case 4:
    default:
      return false;
    case 0:
    case 5:
      if (Build.VERSION.SDK_INT < 8)
        break;
    case 1:
    case 6:
    case 2:
    }
    for (int n = paramMotionEvent.getActionIndex(); ; n = 0)
    {
      CafeMgr.OnTouchBegan(paramMotionEvent.getPointerId(n), paramMotionEvent.getX(n), j - paramMotionEvent.getY(n));
      return true;
      if (Build.VERSION.SDK_INT >= 8);
      for (int m = paramMotionEvent.getActionIndex(); ; m = 0)
      {
        CafeMgr.OnTouchEnded(paramMotionEvent.getPointerId(m), paramMotionEvent.getX(m), j - paramMotionEvent.getY(m));
        return true;
        int k = paramMotionEvent.getPointerCount();
        while (i < k)
        {
          CafeMgr.OnTouchMoved(paramMotionEvent.getPointerId(i), paramMotionEvent.getX(i), j - paramMotionEvent.getY(i));
          i++;
        }
        return true;
      }
    }
  }

  public void setDeliverTouchToCafe(boolean paramBoolean)
  {
    this.m_deliverTouchToCafe = paramBoolean;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.vgood.CafeView
 * JD-Core Version:    0.6.2
 */