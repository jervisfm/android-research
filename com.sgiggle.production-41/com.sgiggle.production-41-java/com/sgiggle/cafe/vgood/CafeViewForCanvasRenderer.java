package com.sgiggle.cafe.vgood;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.SurfaceHolder;

public class CafeViewForCanvasRenderer extends GLSurfaceViewForCanvasRenderer
{
  private CafeRenderer m_renderer;

  public CafeViewForCanvasRenderer(Context paramContext)
  {
    super(paramContext);
    init(false, true);
  }

  public CafeViewForCanvasRenderer(Context paramContext, int paramInt)
  {
    super(paramContext);
    if (paramInt == 0);
    for (boolean bool = true; ; bool = false)
    {
      init(bool, true);
      return;
    }
  }

  public CafeViewForCanvasRenderer(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(false, true);
  }

  private void init(boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((Build.VERSION.SDK_INT >= 8) && (CafeOpenGlConfigChooser.getOpenGlEsVersion() == 2))
      setEGLContextClientVersion(2);
    setEGLConfigChooser(new CafeOpenGlConfigChooser());
    this.m_renderer = new CafeRenderer(paramBoolean1, paramBoolean2);
    setRenderer(this.m_renderer);
    getHolder().setFormat(-3);
  }

  public CafeRenderer getRenderer()
  {
    return this.m_renderer;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.vgood.CafeViewForCanvasRenderer
 * JD-Core Version:    0.6.2
 */