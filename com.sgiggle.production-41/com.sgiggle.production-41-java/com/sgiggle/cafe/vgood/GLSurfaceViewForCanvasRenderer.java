package com.sgiggle.cafe.vgood;

import android.content.Context;
import android.opengl.GLDebugHelper;
import android.opengl.GLSurfaceView.EGLConfigChooser;
import android.opengl.GLSurfaceView.Renderer;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import com.sgiggle.util.Log;
import java.io.Writer;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class GLSurfaceViewForCanvasRenderer extends SurfaceView
  implements SurfaceHolder.Callback
{
  public static final int DEBUG_CHECK_GL_ERROR = 1;
  public static final int DEBUG_LOG_GL_CALLS = 2;
  private static final boolean DRAW_TWICE_AFTER_SIZE_CHANGED = true;
  private static final boolean LOG_EGL = false;
  private static final boolean LOG_PAUSE_RESUME = false;
  private static final boolean LOG_RENDERER = false;
  private static final boolean LOG_RENDERER_DRAW_FRAME = false;
  private static final boolean LOG_SURFACE = false;
  private static final boolean LOG_THREADS = false;
  public static final int RENDERMODE_CONTINUOUSLY = 1;
  public static final int RENDERMODE_WHEN_DIRTY = 0;
  private static final String TAG = "GLSurfaceView";
  private static final GLThreadManager sGLThreadManager = new GLThreadManager(null);
  private int mDebugFlags;
  private GLSurfaceView.EGLConfigChooser mEGLConfigChooser;
  private int mEGLContextClientVersion;
  private EGLContextFactory mEGLContextFactory;
  private EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
  public GLThread mGLThread;
  private GLWrapper mGLWrapper;
  private boolean mHasSurface;
  private int mRenderMode;
  private GLSurfaceView.Renderer mRenderer;
  private boolean mSizeChanged = true;
  private int mSurfaceHeight;
  private int mSurfaceWidth;

  public GLSurfaceViewForCanvasRenderer(Context paramContext)
  {
    super(paramContext);
    init();
  }

  public GLSurfaceViewForCanvasRenderer(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }

  private void checkRenderThreadState()
  {
    if (this.mGLThread != null)
      throw new IllegalStateException("setRenderer has already been called for this instance.");
  }

  private void init()
  {
    SurfaceHolder localSurfaceHolder = getHolder();
    localSurfaceHolder.addCallback(this);
    localSurfaceHolder.setType(2);
    this.mRenderMode = 1;
  }

  public int getDebugFlags()
  {
    return this.mDebugFlags;
  }

  public int getRenderMode()
  {
    return this.mRenderMode;
  }

  public boolean isDestroyed()
  {
    return !this.mHasSurface;
  }

  public void onPause()
  {
    this.mGLThread.onPause();
    this.mGLThread.requestExitAndWait();
    this.mGLThread = null;
  }

  public void onResume()
  {
    if (this.mEGLConfigChooser == null)
      this.mEGLConfigChooser = new SimpleEGLConfigChooser(true);
    if (this.mGLThread != null)
      this.mGLThread.interrupt();
    this.mGLThread = new GLThread(this.mRenderer);
    this.mGLThread.start();
    this.mGLThread.setRenderMode(this.mRenderMode);
    if (this.mHasSurface)
      this.mGLThread.surfaceCreated();
    if ((this.mSurfaceWidth > 0) && (this.mSurfaceHeight > 0))
      this.mGLThread.onWindowResize(this.mSurfaceWidth, this.mSurfaceHeight);
    this.mGLThread.onResume();
  }

  public void queueEvent(Runnable paramRunnable)
  {
    this.mGLThread.requestExitAndWait();
    if (this.mGLThread != null)
      this.mGLThread.queueEvent(paramRunnable);
  }

  public void requestRender()
  {
    this.mGLThread.requestRender();
  }

  public void setDebugFlags(int paramInt)
  {
    this.mDebugFlags = paramInt;
  }

  public void setEGLConfigChooser(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    setEGLConfigChooser(new ComponentSizeChooser(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6));
  }

  public void setEGLConfigChooser(GLSurfaceView.EGLConfigChooser paramEGLConfigChooser)
  {
    checkRenderThreadState();
    this.mEGLConfigChooser = paramEGLConfigChooser;
    if (this.mRenderer != null)
      throw new IllegalStateException("setRenderer has already been called for this instance.");
  }

  public void setEGLConfigChooser(boolean paramBoolean)
  {
    setEGLConfigChooser(new SimpleEGLConfigChooser(paramBoolean));
  }

  public void setEGLContextClientVersion(int paramInt)
  {
    checkRenderThreadState();
    this.mEGLContextClientVersion = paramInt;
  }

  public void setEGLContextFactory(EGLContextFactory paramEGLContextFactory)
  {
    checkRenderThreadState();
    this.mEGLContextFactory = paramEGLContextFactory;
  }

  public void setEGLWindowSurfaceFactory(EGLWindowSurfaceFactory paramEGLWindowSurfaceFactory)
  {
    checkRenderThreadState();
    this.mEGLWindowSurfaceFactory = paramEGLWindowSurfaceFactory;
  }

  public void setGLWrapper(GLWrapper paramGLWrapper)
  {
    this.mGLWrapper = paramGLWrapper;
  }

  public void setRenderMode(int paramInt)
  {
    this.mRenderMode = paramInt;
    if (this.mGLThread != null)
    {
      this.mGLThread.setRenderMode(paramInt);
      return;
    }
    Log.e("GLSurfaceView", "NO mGLThread Running yet");
  }

  public void setRenderer(GLSurfaceView.Renderer paramRenderer)
  {
    checkRenderThreadState();
    if (this.mEGLConfigChooser == null)
      this.mEGLConfigChooser = new SimpleEGLConfigChooser(true);
    if (this.mEGLContextFactory == null)
      this.mEGLContextFactory = new DefaultContextFactory(null);
    if (this.mEGLWindowSurfaceFactory == null)
      this.mEGLWindowSurfaceFactory = new DefaultWindowSurfaceFactory(null);
    if (this.mRenderer != null)
      throw new IllegalStateException("setRenderer has already been called for this instance.");
    this.mRenderer = paramRenderer;
  }

  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.mGLThread != null)
      this.mGLThread.onWindowResize(paramInt2, paramInt3);
    this.mSurfaceWidth = paramInt2;
    this.mSurfaceHeight = paramInt3;
  }

  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    if (this.mGLThread != null)
      this.mGLThread.surfaceCreated();
    this.mHasSurface = true;
  }

  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    Log.e("GLSurfaceView", "surface get destroyed");
    if (this.mGLThread != null)
      this.mGLThread.surfaceDestroyed();
    this.mHasSurface = false;
  }

  private abstract class BaseConfigChooser
    implements GLSurfaceView.EGLConfigChooser
  {
    protected int[] mConfigSpec;

    public BaseConfigChooser(int[] arg2)
    {
      int[] arrayOfInt;
      this.mConfigSpec = filterConfigSpec(arrayOfInt);
    }

    private int[] filterConfigSpec(int[] paramArrayOfInt)
    {
      if (GLSurfaceViewForCanvasRenderer.this.mEGLContextClientVersion != 2)
        return paramArrayOfInt;
      int i = paramArrayOfInt.length;
      int[] arrayOfInt = new int[i + 2];
      System.arraycopy(paramArrayOfInt, 0, arrayOfInt, 0, i - 1);
      arrayOfInt[(i - 1)] = 12352;
      arrayOfInt[i] = 4;
      arrayOfInt[(i + 1)] = 12344;
      return arrayOfInt;
    }

    public EGLConfig chooseConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay)
    {
      int[] arrayOfInt = new int[1];
      if (!paramEGL10.eglChooseConfig(paramEGLDisplay, this.mConfigSpec, null, 0, arrayOfInt))
        throw new IllegalArgumentException("eglChooseConfig failed");
      int i = arrayOfInt[0];
      if (i <= 0)
        throw new IllegalArgumentException("No configs match configSpec");
      EGLConfig[] arrayOfEGLConfig = new EGLConfig[i];
      if (!paramEGL10.eglChooseConfig(paramEGLDisplay, this.mConfigSpec, arrayOfEGLConfig, i, arrayOfInt))
        throw new IllegalArgumentException("eglChooseConfig#2 failed");
      EGLConfig localEGLConfig = chooseConfig(paramEGL10, paramEGLDisplay, arrayOfEGLConfig);
      if (localEGLConfig == null)
        throw new IllegalArgumentException("No config chosen");
      return localEGLConfig;
    }

    abstract EGLConfig chooseConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig[] paramArrayOfEGLConfig);
  }

  private class ComponentSizeChooser extends GLSurfaceViewForCanvasRenderer.BaseConfigChooser
  {
    protected int mAlphaSize;
    protected int mBlueSize;
    protected int mDepthSize;
    protected int mGreenSize;
    protected int mRedSize;
    protected int mStencilSize;
    private int[] mValue = new int[1];

    public ComponentSizeChooser(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int arg7)
    {
      super(new int[] { 12324, paramInt1, 12323, paramInt2, 12322, paramInt3, 12321, paramInt4, 12325, paramInt5, 12326, i, 12344 });
      this.mRedSize = paramInt1;
      this.mGreenSize = paramInt2;
      this.mBlueSize = paramInt3;
      this.mAlphaSize = paramInt4;
      this.mDepthSize = paramInt5;
      this.mStencilSize = i;
    }

    private int findConfigAttrib(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int paramInt1, int paramInt2)
    {
      if (paramEGL10.eglGetConfigAttrib(paramEGLDisplay, paramEGLConfig, paramInt1, this.mValue))
        return this.mValue[0];
      return paramInt2;
    }

    public EGLConfig chooseConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig[] paramArrayOfEGLConfig)
    {
      int i = paramArrayOfEGLConfig.length;
      for (int j = 0; j < i; j++)
      {
        EGLConfig localEGLConfig = paramArrayOfEGLConfig[j];
        int k = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12325, 0);
        int m = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12326, 0);
        if ((k >= this.mDepthSize) && (m >= this.mStencilSize))
        {
          int n = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12324, 0);
          int i1 = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12323, 0);
          int i2 = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12322, 0);
          int i3 = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12321, 0);
          if ((n == this.mRedSize) && (i1 == this.mGreenSize) && (i2 == this.mBlueSize) && (i3 == this.mAlphaSize))
            return localEGLConfig;
        }
      }
      return null;
    }
  }

  private class DefaultContextFactory
    implements GLSurfaceViewForCanvasRenderer.EGLContextFactory
  {
    private int EGL_CONTEXT_CLIENT_VERSION = 12440;

    private DefaultContextFactory()
    {
    }

    public EGLContext createContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig)
    {
      int[] arrayOfInt = new int[3];
      arrayOfInt[0] = this.EGL_CONTEXT_CLIENT_VERSION;
      arrayOfInt[1] = GLSurfaceViewForCanvasRenderer.this.mEGLContextClientVersion;
      arrayOfInt[2] = 12344;
      EGLContext localEGLContext = EGL10.EGL_NO_CONTEXT;
      if (GLSurfaceViewForCanvasRenderer.this.mEGLContextClientVersion != 0);
      while (true)
      {
        return paramEGL10.eglCreateContext(paramEGLDisplay, paramEGLConfig, localEGLContext, arrayOfInt);
        arrayOfInt = null;
      }
    }

    public void destroyContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLContext paramEGLContext)
    {
      if (!paramEGL10.eglDestroyContext(paramEGLDisplay, paramEGLContext))
      {
        Log.e("DefaultContextFactory", "display:" + paramEGLDisplay + " context: " + paramEGLContext);
        throw new RuntimeException("eglDestroyContext failed: " + paramEGL10.eglGetError());
      }
    }
  }

  private static class DefaultWindowSurfaceFactory
    implements GLSurfaceViewForCanvasRenderer.EGLWindowSurfaceFactory
  {
    public EGLSurface createWindowSurface(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject)
    {
      return paramEGL10.eglCreateWindowSurface(paramEGLDisplay, paramEGLConfig, paramObject, null);
    }

    public void destroySurface(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface)
    {
      paramEGL10.eglDestroySurface(paramEGLDisplay, paramEGLSurface);
    }
  }

  public static abstract interface EGLContextFactory
  {
    public abstract EGLContext createContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig);

    public abstract void destroyContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLContext paramEGLContext);
  }

  public static abstract interface EGLWindowSurfaceFactory
  {
    public abstract EGLSurface createWindowSurface(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject);

    public abstract void destroySurface(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface);
  }

  private class EglHelper
  {
    EGL10 mEgl;
    EGLConfig mEglConfig;
    EGLContext mEglContext;
    EGLDisplay mEglDisplay;
    EGLSurface mEglSurface;

    public EglHelper()
    {
    }

    private void throwEglException(String paramString)
    {
      throwEglException(paramString, this.mEgl.eglGetError());
    }

    private void throwEglException(String paramString, int paramInt)
    {
      throw new RuntimeException(paramString + " failed: " + paramInt);
    }

    public GL createSurface(SurfaceHolder paramSurfaceHolder)
    {
      if (this.mEgl == null)
        throw new RuntimeException("egl not initialized");
      if (this.mEglDisplay == null)
        throw new RuntimeException("eglDisplay not initialized");
      if (this.mEglConfig == null)
        throw new RuntimeException("mEglConfig not initialized");
      if ((this.mEglSurface != null) && (this.mEglSurface != EGL10.EGL_NO_SURFACE))
      {
        this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
        GLSurfaceViewForCanvasRenderer.this.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
      }
      this.mEglSurface = GLSurfaceViewForCanvasRenderer.this.mEGLWindowSurfaceFactory.createWindowSurface(this.mEgl, this.mEglDisplay, this.mEglConfig, paramSurfaceHolder);
      int i;
      GL localGL;
      if ((this.mEglSurface == null) || (this.mEglSurface == EGL10.EGL_NO_SURFACE))
      {
        i = this.mEgl.eglGetError();
        if (i == 12299)
        {
          Log.e("EglHelper", "createWindowSurface returned EGL_BAD_NATIVE_WINDOW.");
          localGL = null;
        }
      }
      do
      {
        return localGL;
        throwEglException("createWindowSurface", i);
        if (!this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext))
          throwEglException("eglMakeCurrent");
        localGL = this.mEglContext.getGL();
        if (GLSurfaceViewForCanvasRenderer.this.mGLWrapper != null)
          localGL = GLSurfaceViewForCanvasRenderer.this.mGLWrapper.wrap(localGL);
      }
      while ((0x3 & GLSurfaceViewForCanvasRenderer.this.mDebugFlags) == 0);
      int j = 0x1 & GLSurfaceViewForCanvasRenderer.this.mDebugFlags;
      int k = 0;
      if (j != 0)
        k = 0x0 | 0x1;
      if ((0x2 & GLSurfaceViewForCanvasRenderer.this.mDebugFlags) != 0);
      for (GLSurfaceViewForCanvasRenderer.LogWriter localLogWriter = new GLSurfaceViewForCanvasRenderer.LogWriter(); ; localLogWriter = null)
        return GLDebugHelper.wrap(localGL, k, localLogWriter);
    }

    public void destroySurface()
    {
      if ((this.mEglSurface != null) && (this.mEglSurface != EGL10.EGL_NO_SURFACE))
      {
        this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
        GLSurfaceViewForCanvasRenderer.this.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
        this.mEglSurface = null;
      }
    }

    public void finish()
    {
      if (this.mEglContext != null)
      {
        GLSurfaceViewForCanvasRenderer.this.mEGLContextFactory.destroyContext(this.mEgl, this.mEglDisplay, this.mEglContext);
        this.mEglContext = null;
      }
      if (this.mEglDisplay != null)
      {
        this.mEgl.eglTerminate(this.mEglDisplay);
        this.mEglDisplay = null;
      }
    }

    public void start()
    {
      this.mEgl = ((EGL10)EGLContext.getEGL());
      this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
      if (this.mEglDisplay == EGL10.EGL_NO_DISPLAY)
        throw new RuntimeException("eglGetDisplay failed");
      int[] arrayOfInt = new int[2];
      if (!this.mEgl.eglInitialize(this.mEglDisplay, arrayOfInt))
        throw new RuntimeException("eglInitialize failed");
      this.mEglConfig = GLSurfaceViewForCanvasRenderer.this.mEGLConfigChooser.chooseConfig(this.mEgl, this.mEglDisplay);
      this.mEglContext = GLSurfaceViewForCanvasRenderer.this.mEGLContextFactory.createContext(this.mEgl, this.mEglDisplay, this.mEglConfig);
      if ((this.mEglContext == null) || (this.mEglContext == EGL10.EGL_NO_CONTEXT))
      {
        this.mEglContext = null;
        throwEglException("createContext");
      }
      this.mEglSurface = null;
    }

    public boolean swap()
    {
      if (!this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface))
      {
        int i = this.mEgl.eglGetError();
        switch (i)
        {
        case 12300:
        case 12301:
        default:
          throwEglException("eglSwapBuffers", i);
        case 12302:
        case 12299:
        }
      }
      while (true)
      {
        return true;
        return false;
        Log.e("EglHelper", "eglSwapBuffers returned EGL_BAD_NATIVE_WINDOW. tid=" + Thread.currentThread().getId());
      }
    }
  }

  public class GLThread extends Thread
  {
    private GLSurfaceViewForCanvasRenderer.EglHelper mEglHelper;
    private ArrayList<Runnable> mEventQueue = new ArrayList();
    private boolean mExited;
    private boolean mHaveEglContext;
    private boolean mHaveEglSurface;
    private int mHeight = 0;
    private boolean mPaused;
    private boolean mRenderComplete;
    private int mRenderMode = 1;
    private GLSurfaceView.Renderer mRenderer;
    private boolean mRequestPaused;
    private boolean mRequestRender = true;
    private boolean mShouldExit;
    private boolean mShouldReleaseEglContext;
    private boolean mWaitingForSurface;
    private int mWidth = 0;

    GLThread(GLSurfaceView.Renderer arg2)
    {
      super();
      GLSurfaceViewForCanvasRenderer.access$802(GLSurfaceViewForCanvasRenderer.this, true);
      Object localObject;
      this.mRenderer = localObject;
    }

    // ERROR //
    private void guardedRun()
      throws InterruptedException
    {
      // Byte code:
      //   0: aload_0
      //   1: new 71	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper
      //   4: dup
      //   5: aload_0
      //   6: getfield 32	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:this$0	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
      //   9: invokespecial 74	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper:<init>	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)V
      //   12: putfield 76	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mEglHelper	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;
      //   15: aload_0
      //   16: iconst_0
      //   17: putfield 78	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglContext	Z
      //   20: aload_0
      //   21: iconst_0
      //   22: putfield 80	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglSurface	Z
      //   25: iconst_0
      //   26: istore_1
      //   27: aconst_null
      //   28: astore_2
      //   29: aconst_null
      //   30: astore_3
      //   31: iconst_0
      //   32: istore 4
      //   34: iconst_0
      //   35: istore 5
      //   37: iconst_0
      //   38: istore 6
      //   40: iconst_0
      //   41: istore 7
      //   43: iconst_0
      //   44: istore 8
      //   46: iconst_0
      //   47: istore 9
      //   49: iconst_0
      //   50: istore 10
      //   52: iconst_0
      //   53: istore 11
      //   55: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   58: astore 15
      //   60: aload 15
      //   62: monitorenter
      //   63: aload_0
      //   64: getfield 86	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mShouldExit	Z
      //   67: ifeq +34 -> 101
      //   70: aload 15
      //   72: monitorexit
      //   73: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   76: astore 38
      //   78: aload 38
      //   80: monitorenter
      //   81: aload_0
      //   82: invokespecial 89	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglSurfaceLocked	()V
      //   85: aload_0
      //   86: invokespecial 92	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglContextLocked	()V
      //   89: aload 38
      //   91: monitorexit
      //   92: return
      //   93: astore 39
      //   95: aload 38
      //   97: monitorexit
      //   98: aload 39
      //   100: athrow
      //   101: aload_0
      //   102: getfield 44	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mEventQueue	Ljava/util/ArrayList;
      //   105: invokevirtual 96	java/util/ArrayList:isEmpty	()Z
      //   108: ifne +102 -> 210
      //   111: aload_0
      //   112: getfield 44	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mEventQueue	Ljava/util/ArrayList;
      //   115: iconst_0
      //   116: invokevirtual 100	java/util/ArrayList:remove	(I)Ljava/lang/Object;
      //   119: checkcast 102	java/lang/Runnable
      //   122: astore 37
      //   124: iload 4
      //   126: istore 20
      //   128: iload 9
      //   130: istore 21
      //   132: iload 6
      //   134: istore 22
      //   136: iload_1
      //   137: istore 23
      //   139: iload 7
      //   141: istore 24
      //   143: iload 8
      //   145: istore 25
      //   147: iload 5
      //   149: istore 26
      //   151: iload 10
      //   153: istore 27
      //   155: aload 37
      //   157: astore 28
      //   159: aload 15
      //   161: monitorexit
      //   162: aload 28
      //   164: ifnull +485 -> 649
      //   167: aload 28
      //   169: invokeinterface 105 1 0
      //   174: iload 27
      //   176: istore 10
      //   178: iload 26
      //   180: istore 5
      //   182: iload 25
      //   184: istore 8
      //   186: iload 24
      //   188: istore 7
      //   190: iload 23
      //   192: istore_1
      //   193: iload 22
      //   195: istore 6
      //   197: iload 21
      //   199: istore 9
      //   201: iload 20
      //   203: istore 4
      //   205: aconst_null
      //   206: astore_3
      //   207: goto -152 -> 55
      //   210: aload_0
      //   211: getfield 107	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mPaused	Z
      //   214: aload_0
      //   215: getfield 109	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mRequestPaused	Z
      //   218: if_icmpeq +17 -> 235
      //   221: aload_0
      //   222: aload_0
      //   223: getfield 109	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mRequestPaused	Z
      //   226: putfield 107	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mPaused	Z
      //   229: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   232: invokevirtual 114	java/lang/Object:notifyAll	()V
      //   235: aload_0
      //   236: getfield 116	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mShouldReleaseEglContext	Z
      //   239: ifeq +19 -> 258
      //   242: aload_0
      //   243: invokespecial 89	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglSurfaceLocked	()V
      //   246: aload_0
      //   247: invokespecial 92	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglContextLocked	()V
      //   250: aload_0
      //   251: iconst_0
      //   252: putfield 116	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mShouldReleaseEglContext	Z
      //   255: iconst_1
      //   256: istore 8
      //   258: iload 9
      //   260: ifeq +14 -> 274
      //   263: aload_0
      //   264: invokespecial 89	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglSurfaceLocked	()V
      //   267: aload_0
      //   268: invokespecial 92	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglContextLocked	()V
      //   271: iconst_0
      //   272: istore 9
      //   274: aload_0
      //   275: getfield 80	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglSurface	Z
      //   278: ifeq +43 -> 321
      //   281: aload_0
      //   282: getfield 107	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mPaused	Z
      //   285: ifeq +36 -> 321
      //   288: aload_0
      //   289: invokespecial 89	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglSurfaceLocked	()V
      //   292: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   295: invokevirtual 121	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager:shouldReleaseEGLContextWhenPausing	()Z
      //   298: ifeq +7 -> 305
      //   301: aload_0
      //   302: invokespecial 92	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglContextLocked	()V
      //   305: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   308: invokevirtual 124	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager:shouldTerminateEGLWhenPausing	()Z
      //   311: ifeq +10 -> 321
      //   314: aload_0
      //   315: getfield 76	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mEglHelper	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;
      //   318: invokevirtual 127	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper:finish	()V
      //   321: aload_0
      //   322: getfield 32	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:this$0	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
      //   325: invokestatic 131	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$1000	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z
      //   328: ifne +32 -> 360
      //   331: aload_0
      //   332: getfield 133	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mWaitingForSurface	Z
      //   335: ifne +25 -> 360
      //   338: aload_0
      //   339: getfield 80	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglSurface	Z
      //   342: ifeq +7 -> 349
      //   345: aload_0
      //   346: invokespecial 89	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglSurfaceLocked	()V
      //   349: aload_0
      //   350: iconst_1
      //   351: putfield 133	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mWaitingForSurface	Z
      //   354: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   357: invokevirtual 114	java/lang/Object:notifyAll	()V
      //   360: aload_0
      //   361: getfield 32	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:this$0	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
      //   364: invokestatic 131	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$1000	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z
      //   367: ifeq +21 -> 388
      //   370: aload_0
      //   371: getfield 133	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mWaitingForSurface	Z
      //   374: ifeq +14 -> 388
      //   377: aload_0
      //   378: iconst_0
      //   379: putfield 133	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mWaitingForSurface	Z
      //   382: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   385: invokevirtual 114	java/lang/Object:notifyAll	()V
      //   388: iload 7
      //   390: ifeq +19 -> 409
      //   393: aload_0
      //   394: iconst_1
      //   395: putfield 135	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mRenderComplete	Z
      //   398: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   401: invokevirtual 114	java/lang/Object:notifyAll	()V
      //   404: iconst_0
      //   405: istore_1
      //   406: iconst_0
      //   407: istore 7
      //   409: aload_0
      //   410: invokespecial 138	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:readyToDraw	()Z
      //   413: ifeq +227 -> 640
      //   416: aload_0
      //   417: getfield 78	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglContext	Z
      //   420: ifne +11 -> 431
      //   423: iload 8
      //   425: ifeq +125 -> 550
      //   428: iconst_0
      //   429: istore 8
      //   431: aload_0
      //   432: getfield 78	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglContext	Z
      //   435: ifeq +21 -> 456
      //   438: aload_0
      //   439: getfield 80	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglSurface	Z
      //   442: ifne +14 -> 456
      //   445: aload_0
      //   446: iconst_1
      //   447: putfield 80	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglSurface	Z
      //   450: iconst_1
      //   451: istore 4
      //   453: iconst_1
      //   454: istore 6
      //   456: aload_0
      //   457: getfield 80	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglSurface	Z
      //   460: ifeq +180 -> 640
      //   463: aload_0
      //   464: getfield 32	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:this$0	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
      //   467: invokestatic 141	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$800	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z
      //   470: ifeq +162 -> 632
      //   473: aload_0
      //   474: getfield 46	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mWidth	I
      //   477: istore 5
      //   479: aload_0
      //   480: getfield 48	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHeight	I
      //   483: istore 35
      //   485: aload_0
      //   486: getfield 32	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:this$0	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
      //   489: iconst_0
      //   490: invokestatic 58	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$802	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)Z
      //   493: pop
      //   494: iconst_1
      //   495: istore_1
      //   496: iconst_1
      //   497: istore 6
      //   499: iload 35
      //   501: istore 10
      //   503: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   506: invokevirtual 114	java/lang/Object:notifyAll	()V
      //   509: aload_3
      //   510: astore 19
      //   512: iload 4
      //   514: istore 20
      //   516: iload 9
      //   518: istore 21
      //   520: iload 6
      //   522: istore 22
      //   524: iload_1
      //   525: istore 23
      //   527: iload 7
      //   529: istore 24
      //   531: iload 8
      //   533: istore 25
      //   535: iload 5
      //   537: istore 26
      //   539: iload 10
      //   541: istore 27
      //   543: aload 19
      //   545: astore 28
      //   547: goto -388 -> 159
      //   550: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   553: aload_0
      //   554: invokevirtual 145	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager:tryAcquireEglContextLocked	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)Z
      //   557: istore 17
      //   559: iload 17
      //   561: ifeq -130 -> 431
      //   564: aload_0
      //   565: getfield 76	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mEglHelper	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;
      //   568: invokevirtual 148	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper:start	()V
      //   571: aload_0
      //   572: iconst_1
      //   573: putfield 78	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mHaveEglContext	Z
      //   576: iconst_1
      //   577: istore 11
      //   579: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   582: invokevirtual 114	java/lang/Object:notifyAll	()V
      //   585: goto -154 -> 431
      //   588: astore 16
      //   590: aload 15
      //   592: monitorexit
      //   593: aload 16
      //   595: athrow
      //   596: astore 12
      //   598: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   601: astore 13
      //   603: aload 13
      //   605: monitorenter
      //   606: aload_0
      //   607: invokespecial 89	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglSurfaceLocked	()V
      //   610: aload_0
      //   611: invokespecial 92	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglContextLocked	()V
      //   614: aload 13
      //   616: monitorexit
      //   617: aload 12
      //   619: athrow
      //   620: astore 18
      //   622: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   625: aload_0
      //   626: invokevirtual 152	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager:releaseEglContextLocked	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V
      //   629: aload 18
      //   631: athrow
      //   632: aload_0
      //   633: iconst_0
      //   634: putfield 50	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mRequestRender	Z
      //   637: goto -134 -> 503
      //   640: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   643: invokevirtual 155	java/lang/Object:wait	()V
      //   646: goto -583 -> 63
      //   649: iload 20
      //   651: ifeq +69 -> 720
      //   654: aload_0
      //   655: getfield 76	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mEglHelper	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;
      //   658: aload_0
      //   659: getfield 32	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:this$0	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
      //   662: invokevirtual 159	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:getHolder	()Landroid/view/SurfaceHolder;
      //   665: invokevirtual 163	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper:createSurface	(Landroid/view/SurfaceHolder;)Ljavax/microedition/khronos/opengles/GL;
      //   668: checkcast 165	javax/microedition/khronos/opengles/GL10
      //   671: astore 32
      //   673: aload 32
      //   675: ifnonnull +31 -> 706
      //   678: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   681: astore 33
      //   683: aload 33
      //   685: monitorenter
      //   686: aload_0
      //   687: invokespecial 89	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglSurfaceLocked	()V
      //   690: aload_0
      //   691: invokespecial 92	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:stopEglContextLocked	()V
      //   694: aload 33
      //   696: monitorexit
      //   697: return
      //   698: astore 34
      //   700: aload 33
      //   702: monitorexit
      //   703: aload 34
      //   705: athrow
      //   706: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   709: aload 32
      //   711: invokevirtual 169	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager:checkGLDriver	(Ljavax/microedition/khronos/opengles/GL10;)V
      //   714: aload 32
      //   716: astore_2
      //   717: iconst_0
      //   718: istore 20
      //   720: iload 11
      //   722: ifeq +23 -> 745
      //   725: aload_0
      //   726: getfield 60	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mRenderer	Landroid/opengl/GLSurfaceView$Renderer;
      //   729: aload_2
      //   730: aload_0
      //   731: getfield 76	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mEglHelper	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;
      //   734: getfield 173	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper:mEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
      //   737: invokeinterface 179 3 0
      //   742: iconst_0
      //   743: istore 11
      //   745: iload 22
      //   747: ifeq +20 -> 767
      //   750: aload_0
      //   751: getfield 60	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mRenderer	Landroid/opengl/GLSurfaceView$Renderer;
      //   754: aload_2
      //   755: iload 26
      //   757: iload 27
      //   759: invokeinterface 183 4 0
      //   764: iconst_0
      //   765: istore 22
      //   767: aload_0
      //   768: getfield 60	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mRenderer	Landroid/opengl/GLSurfaceView$Renderer;
      //   771: aload_2
      //   772: invokeinterface 186 2 0
      //   777: aload_0
      //   778: getfield 76	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:mEglHelper	Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;
      //   781: invokevirtual 189	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper:swap	()Z
      //   784: istore 29
      //   786: iload 29
      //   788: ifne +6 -> 794
      //   791: iconst_1
      //   792: istore 21
      //   794: iload 23
      //   796: ifeq +51 -> 847
      //   799: aload 28
      //   801: astore 31
      //   803: iload 27
      //   805: istore 10
      //   807: iload 26
      //   809: istore 5
      //   811: iload 25
      //   813: istore 8
      //   815: iconst_1
      //   816: istore 7
      //   818: iload 23
      //   820: istore_1
      //   821: iload 22
      //   823: istore 6
      //   825: iload 21
      //   827: istore 9
      //   829: iload 20
      //   831: istore 4
      //   833: aload 31
      //   835: astore_3
      //   836: goto -781 -> 55
      //   839: astore 14
      //   841: aload 13
      //   843: monitorexit
      //   844: aload 14
      //   846: athrow
      //   847: aload 28
      //   849: astore 30
      //   851: iload 27
      //   853: istore 10
      //   855: iload 26
      //   857: istore 5
      //   859: iload 25
      //   861: istore 8
      //   863: iload 24
      //   865: istore 7
      //   867: iload 23
      //   869: istore_1
      //   870: iload 22
      //   872: istore 6
      //   874: iload 21
      //   876: istore 9
      //   878: iload 20
      //   880: istore 4
      //   882: aload 30
      //   884: astore_3
      //   885: goto -830 -> 55
      //
      // Exception table:
      //   from	to	target	type
      //   81	92	93	finally
      //   95	98	93	finally
      //   63	73	588	finally
      //   101	124	588	finally
      //   159	162	588	finally
      //   210	235	588	finally
      //   235	255	588	finally
      //   263	271	588	finally
      //   274	305	588	finally
      //   305	321	588	finally
      //   321	349	588	finally
      //   349	360	588	finally
      //   360	388	588	finally
      //   393	404	588	finally
      //   409	423	588	finally
      //   431	450	588	finally
      //   456	494	588	finally
      //   503	509	588	finally
      //   550	559	588	finally
      //   564	571	588	finally
      //   571	576	588	finally
      //   579	585	588	finally
      //   590	593	588	finally
      //   622	632	588	finally
      //   632	637	588	finally
      //   640	646	588	finally
      //   55	63	596	finally
      //   167	174	596	finally
      //   593	596	596	finally
      //   654	673	596	finally
      //   706	714	596	finally
      //   725	742	596	finally
      //   750	764	596	finally
      //   767	786	596	finally
      //   564	571	620	java/lang/RuntimeException
      //   686	697	698	finally
      //   700	703	698	finally
      //   606	617	839	finally
      //   841	844	839	finally
    }

    private boolean readyToDraw()
    {
      return (!this.mPaused) && (GLSurfaceViewForCanvasRenderer.this.mHasSurface) && (this.mWidth > 0) && (this.mHeight > 0) && ((this.mRequestRender) || (this.mRenderMode == 1));
    }

    private void stopEglContextLocked()
    {
      if (this.mHaveEglContext)
      {
        this.mEglHelper.finish();
        this.mHaveEglContext = false;
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.releaseEglContextLocked(this);
      }
    }

    private void stopEglSurfaceLocked()
    {
      if (this.mHaveEglSurface)
      {
        this.mHaveEglSurface = false;
        this.mEglHelper.destroySurface();
      }
    }

    public boolean ableToDraw()
    {
      return (this.mHaveEglContext) && (this.mHaveEglSurface) && (readyToDraw());
    }

    public int getRenderMode()
    {
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        int i = this.mRenderMode;
        return i;
      }
    }

    public void onPause()
    {
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        this.mRequestPaused = true;
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        while (true)
          if (!this.mExited)
          {
            boolean bool = this.mPaused;
            if (!bool)
              try
              {
                GLSurfaceViewForCanvasRenderer.sGLThreadManager.wait();
              }
              catch (InterruptedException localInterruptedException)
              {
                Thread.currentThread().interrupt();
              }
          }
      }
    }

    public void onResume()
    {
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        this.mRequestPaused = false;
        this.mRequestRender = true;
        this.mRenderComplete = false;
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        while (true)
          if ((!this.mExited) && (this.mPaused))
          {
            boolean bool = this.mRenderComplete;
            if (!bool)
              try
              {
                GLSurfaceViewForCanvasRenderer.sGLThreadManager.wait();
              }
              catch (InterruptedException localInterruptedException)
              {
                Thread.currentThread().interrupt();
              }
          }
      }
    }

    public void onWindowResize(int paramInt1, int paramInt2)
    {
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        this.mWidth = paramInt1;
        this.mHeight = paramInt2;
        GLSurfaceViewForCanvasRenderer.access$802(GLSurfaceViewForCanvasRenderer.this, true);
        this.mRequestRender = true;
        this.mRenderComplete = false;
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        while (true)
          if ((!this.mExited) && (!this.mPaused) && (!this.mRenderComplete) && (GLSurfaceViewForCanvasRenderer.this.mGLThread != null))
          {
            boolean bool = GLSurfaceViewForCanvasRenderer.this.mGLThread.ableToDraw();
            if (bool)
              try
              {
                GLSurfaceViewForCanvasRenderer.sGLThreadManager.wait();
              }
              catch (InterruptedException localInterruptedException)
              {
                Thread.currentThread().interrupt();
              }
          }
      }
    }

    public void queueEvent(Runnable paramRunnable)
    {
      if (paramRunnable == null)
        throw new IllegalArgumentException("r must not be null");
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        this.mEventQueue.add(paramRunnable);
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        return;
      }
    }

    public void requestExitAndWait()
    {
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        this.mShouldExit = true;
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        while (true)
        {
          boolean bool = this.mExited;
          if (!bool)
            try
            {
              GLSurfaceViewForCanvasRenderer.sGLThreadManager.wait();
            }
            catch (InterruptedException localInterruptedException)
            {
              Thread.currentThread().interrupt();
            }
        }
      }
    }

    public void requestReleaseEglContextLocked()
    {
      this.mShouldReleaseEglContext = true;
      GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
    }

    public void requestRender()
    {
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        this.mRequestRender = true;
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        return;
      }
    }

    // ERROR //
    public void run()
    {
      // Byte code:
      //   0: aload_0
      //   1: new 228	java/lang/StringBuilder
      //   4: dup
      //   5: invokespecial 229	java/lang/StringBuilder:<init>	()V
      //   8: ldc 231
      //   10: invokevirtual 235	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   13: aload_0
      //   14: invokevirtual 239	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:getId	()J
      //   17: invokevirtual 242	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   20: invokevirtual 246	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   23: invokevirtual 249	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:setName	(Ljava/lang/String;)V
      //   26: aload_0
      //   27: invokespecial 251	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread:guardedRun	()V
      //   30: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   33: aload_0
      //   34: invokevirtual 254	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager:threadExiting	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V
      //   37: return
      //   38: astore_2
      //   39: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   42: aload_0
      //   43: invokevirtual 254	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager:threadExiting	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V
      //   46: return
      //   47: astore_1
      //   48: invokestatic 84	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer:access$900	()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
      //   51: aload_0
      //   52: invokevirtual 254	com/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager:threadExiting	(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V
      //   55: aload_1
      //   56: athrow
      //
      // Exception table:
      //   from	to	target	type
      //   26	30	38	java/lang/InterruptedException
      //   26	30	47	finally
    }

    public void setRenderMode(int paramInt)
    {
      if ((paramInt < 0) || (paramInt > 1))
        throw new IllegalArgumentException("renderMode");
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        this.mRenderMode = paramInt;
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        return;
      }
    }

    public void surfaceCreated()
    {
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        GLSurfaceViewForCanvasRenderer.access$1002(GLSurfaceViewForCanvasRenderer.this, true);
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        while (true)
          if (this.mWaitingForSurface)
          {
            boolean bool = this.mExited;
            if (!bool)
              try
              {
                GLSurfaceViewForCanvasRenderer.sGLThreadManager.wait();
              }
              catch (InterruptedException localInterruptedException)
              {
                Thread.currentThread().interrupt();
              }
          }
      }
    }

    public void surfaceDestroyed()
    {
      synchronized (GLSurfaceViewForCanvasRenderer.sGLThreadManager)
      {
        GLSurfaceViewForCanvasRenderer.access$1002(GLSurfaceViewForCanvasRenderer.this, false);
        GLSurfaceViewForCanvasRenderer.sGLThreadManager.notifyAll();
        while (true)
          if (!this.mWaitingForSurface)
          {
            boolean bool = this.mExited;
            if (!bool)
              try
              {
                GLSurfaceViewForCanvasRenderer.sGLThreadManager.wait();
              }
              catch (InterruptedException localInterruptedException)
              {
                Thread.currentThread().interrupt();
              }
          }
      }
    }
  }

  private static class GLThreadManager
  {
    private static String TAG = "GLThreadManager";
    private static final int kGLES_20 = 131072;
    private static final String kMSM7K_RENDERER_PREFIX = "Q3Dimension MSM7500 ";
    private GLSurfaceViewForCanvasRenderer.GLThread mEglOwner;
    private boolean mGLESDriverCheckComplete;
    private int mGLESVersion;
    private boolean mGLESVersionCheckComplete;
    private boolean mMultipleGLESContextsAllowed;

    private void checkGLESVersion()
    {
      if (!this.mGLESVersionCheckComplete)
        this.mGLESVersionCheckComplete = true;
    }

    public void checkGLDriver(GL10 paramGL10)
    {
      try
      {
        if (!this.mGLESDriverCheckComplete)
        {
          checkGLESVersion();
          if (this.mGLESVersion < 131072)
            if (paramGL10.glGetString(7937).startsWith("Q3Dimension MSM7500 "))
              break label58;
        }
        label58: for (boolean bool = true; ; bool = false)
        {
          this.mMultipleGLESContextsAllowed = bool;
          notifyAll();
          this.mGLESDriverCheckComplete = true;
          return;
        }
      }
      finally
      {
      }
    }

    public void releaseEglContextLocked(GLSurfaceViewForCanvasRenderer.GLThread paramGLThread)
    {
      if (this.mEglOwner == paramGLThread)
        this.mEglOwner = null;
      notifyAll();
    }

    public boolean shouldReleaseEGLContextWhenPausing()
    {
      return true;
    }

    public boolean shouldTerminateEGLWhenPausing()
    {
      try
      {
        checkGLESVersion();
        boolean bool1 = this.mMultipleGLESContextsAllowed;
        if (!bool1)
        {
          bool2 = true;
          return bool2;
        }
        boolean bool2 = false;
      }
      finally
      {
      }
    }

    public void threadExiting(GLSurfaceViewForCanvasRenderer.GLThread paramGLThread)
    {
      try
      {
        GLSurfaceViewForCanvasRenderer.GLThread.access$1102(paramGLThread, true);
        if (this.mEglOwner == paramGLThread)
          this.mEglOwner = null;
        notifyAll();
        return;
      }
      finally
      {
      }
    }

    public boolean tryAcquireEglContextLocked(GLSurfaceViewForCanvasRenderer.GLThread paramGLThread)
    {
      if ((this.mEglOwner == paramGLThread) || (this.mEglOwner == null))
      {
        this.mEglOwner = paramGLThread;
        notifyAll();
        return true;
      }
      checkGLESVersion();
      if (this.mMultipleGLESContextsAllowed)
        return true;
      if (this.mEglOwner != null)
        this.mEglOwner.requestReleaseEglContextLocked();
      return false;
    }
  }

  public static abstract interface GLWrapper
  {
    public abstract GL wrap(GL paramGL);
  }

  static class LogWriter extends Writer
  {
    private StringBuilder mBuilder = new StringBuilder();

    private void flushBuilder()
    {
      if (this.mBuilder.length() > 0)
      {
        Log.v("GLSurfaceView", this.mBuilder.toString());
        this.mBuilder.delete(0, this.mBuilder.length());
      }
    }

    public void close()
    {
      flushBuilder();
    }

    public void flush()
    {
      flushBuilder();
    }

    public void write(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
      int i = 0;
      if (i < paramInt2)
      {
        char c = paramArrayOfChar[(paramInt1 + i)];
        if (c == '\n')
          flushBuilder();
        while (true)
        {
          i++;
          break;
          this.mBuilder.append(c);
        }
      }
    }
  }

  private class SimpleEGLConfigChooser extends GLSurfaceViewForCanvasRenderer.ComponentSizeChooser
  {
    public SimpleEGLConfigChooser(boolean arg2)
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.vgood.GLSurfaceViewForCanvasRenderer
 * JD-Core Version:    0.6.2
 */