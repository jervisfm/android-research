package com.sgiggle.cafe.vgood;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.sgiggle.media_engine.MediaEngineMessage.AddAvatarMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InitiateVGoodMessage;
import com.sgiggle.media_engine.MediaEngineMessage.LockedVGoodSelectedMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RemoveAvatarMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.VideoTwoWayActivity;
import com.sgiggle.production.adapter.VGoodSelectorAdapter;
import com.sgiggle.production.widget.HorizontalListView;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.VGoodBundle;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic.Type;
import java.util.ArrayList;
import java.util.List;

public class VGoodButtonsBarView extends LinearLayout
  implements AdapterView.OnItemClickListener
{
  public static final int STATE_DISABLE = 1;
  public static final int STATE_NORMAL = 0;
  private static final String TAG = "VGoodButtonsBarView";
  private List<SessionMessages.VGoodBundle> allData;
  private SessionMessages.VGoodBundle currentShowBundle = null;
  private VGoodSelectorAdapter mAdapter;
  private int mCurrentShowPos = -1;
  private HorizontalListView mHorizontalListview;
  private ListView mList = null;
  private VideoTwoWayActivity mParent;
  private AdapterView<Adapter> mSelector;
  private ScrollListener scrollListener;
  private boolean showingCurrentPlayButton = false;

  public VGoodButtonsBarView(VideoTwoWayActivity paramVideoTwoWayActivity, int paramInt)
  {
    super(paramVideoTwoWayActivity);
    this.mParent = paramVideoTwoWayActivity;
    this.mAdapter = new VGoodSelectorAdapter(getContext());
    this.mAdapter.setEmptyCount(4);
    this.scrollListener = new ScrollListener()
    {
      public void isGoingToStop()
      {
        VGoodButtonsBarView.this.mParent.restartControlBarTimer();
      }

      public void startScrolling()
      {
        VGoodButtonsBarView.this.mParent.cancelControlBarTimer();
      }
    };
    if (paramInt == 0)
    {
      Log.d("VGoodButtonsBarView", "orientation is landscape");
      LayoutInflater.from(getContext()).inflate(2130903134, this, true);
      this.mList = ((ListView)findViewById(2131361854));
      this.mList.setSelection(this.mAdapter.getCount());
      this.mSelector = this.mList;
      this.mAdapter.setReversed(true);
      this.mAdapter.setRotated(true);
      this.mList.setOnScrollListener(new AbsListView.OnScrollListener()
      {
        public void onScroll(AbsListView paramAnonymousAbsListView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
        {
        }

        public void onScrollStateChanged(AbsListView paramAnonymousAbsListView, int paramAnonymousInt)
        {
          switch (paramAnonymousInt)
          {
          default:
            return;
          case 2:
            VGoodButtonsBarView.this.scrollListener.isGoingToStop();
            return;
          case 1:
            VGoodButtonsBarView.this.scrollListener.startScrolling();
            return;
          case 0:
          }
          VGoodButtonsBarView.this.scrollListener.isGoingToStop();
        }
      });
    }
    while (true)
    {
      this.mSelector.setAdapter(this.mAdapter);
      this.mSelector.setOnItemClickListener(this);
      return;
      Log.d("VGoodButtonsBarView", "orientation is portrait");
      LayoutInflater.from(getContext()).inflate(2130903133, this, true);
      this.mHorizontalListview = ((HorizontalListView)findViewById(2131361854));
      this.mSelector = this.mHorizontalListview;
      this.mHorizontalListview.setScrollListener(this.scrollListener);
    }
  }

  private void addAvatar(SessionMessages.VGoodBundle paramVGoodBundle)
  {
    MediaEngineMessage.AddAvatarMessage localAddAvatarMessage = new MediaEngineMessage.AddAvatarMessage(paramVGoodBundle.getCinematic().getAssetId());
    MessageRouter.getInstance().postMessage("jingle", localAddAvatarMessage);
  }

  private boolean bundleWithSameAssetId(SessionMessages.VGoodBundle paramVGoodBundle1, SessionMessages.VGoodBundle paramVGoodBundle2)
  {
    return (paramVGoodBundle1 != null) && (paramVGoodBundle2 != null) && (paramVGoodBundle1.hasCinematic()) && (paramVGoodBundle2.hasCinematic()) && (paramVGoodBundle1.getCinematic().getAssetId() == paramVGoodBundle2.getCinematic().getAssetId());
  }

  private int findFilterPositionByIndex(long paramLong)
  {
    for (int i = 0; i < this.mAdapter.getCount(); i++)
    {
      SessionMessages.VGoodBundle localVGoodBundle = (SessionMessages.VGoodBundle)this.mSelector.getItemAtPosition(i);
      if ((localVGoodBundle != null) && (localVGoodBundle.getCinematic().getType() == SessionMessages.VGoodCinematic.Type.FILTER) && (localVGoodBundle.getCinematic().getAssetId() == paramLong))
        return i;
    }
    return -1;
  }

  private void onAvatarClicked(SessionMessages.VGoodBundle paramVGoodBundle)
  {
    if (bundleWithSameAssetId(this.currentShowBundle, paramVGoodBundle))
    {
      removeAvatar(paramVGoodBundle);
      return;
    }
    addAvatar(paramVGoodBundle);
  }

  private void playVGood(SessionMessages.VGoodBundle paramVGoodBundle)
  {
    this.mParent.restartControlBarTimer();
    MediaEngineMessage.InitiateVGoodMessage localInitiateVGoodMessage = new MediaEngineMessage.InitiateVGoodMessage(Integer.valueOf((int)paramVGoodBundle.getCinematic().getAssetId()));
    MessageRouter.getInstance().postMessage("jingle", localInitiateVGoodMessage);
  }

  private void removeAvatar(SessionMessages.VGoodBundle paramVGoodBundle)
  {
    MediaEngineMessage.RemoveAvatarMessage localRemoveAvatarMessage = new MediaEngineMessage.RemoveAvatarMessage(paramVGoodBundle.getCinematic().getAssetId());
    MessageRouter.getInstance().postMessage("jingle", localRemoveAvatarMessage);
  }

  private void showBubbleForLockedVgood()
  {
    this.mParent.setupWarningBubble(this.mParent.getString(2131296534), this.mParent.getString(2131296535), false, false, true);
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.LockedVGoodSelectedMessage());
  }

  public void enableButtonByTypes(List<SessionMessages.VGoodCinematic.Type> paramList, boolean paramBoolean)
  {
    if (this.allData == null)
    {
      Log.e("VGoodButtonsBarView", "enableButtonByTypes: setData has never been called");
      return;
    }
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    int j = 0;
    int k = -1;
    label33: int i1;
    label109: int n;
    int m;
    if (i < this.allData.size())
    {
      SessionMessages.VGoodBundle localVGoodBundle = (SessionMessages.VGoodBundle)this.allData.get(i);
      if ((localVGoodBundle == null) || (!paramList.contains(localVGoodBundle.getCinematic().getType())))
        break label203;
      localArrayList.add(localVGoodBundle);
      if (!bundleWithSameAssetId(this.currentShowBundle, localVGoodBundle))
        break label196;
      i1 = j;
      int i2 = j + 1;
      n = i1;
      m = i2;
    }
    while (true)
    {
      i++;
      k = n;
      j = m;
      break label33;
      this.mAdapter.setData(localArrayList, paramBoolean);
      if (k != -1)
        this.mAdapter.setHightlightWithBackground(k);
      if (!paramList.contains(SessionMessages.VGoodCinematic.Type.VGOOD_ANIMATION))
        this.mAdapter.setShowEmptySlots(false);
      if (this.mHorizontalListview == null)
        break;
      this.mHorizontalListview.reset();
      return;
      label196: i1 = k;
      break label109;
      label203: m = j;
      n = k;
    }
  }

  public void findBundleById(long paramLong)
  {
    this.mCurrentShowPos = -1;
    this.currentShowBundle = null;
    for (int i = 0; ; i++)
      if (i < this.mAdapter.getCount())
      {
        SessionMessages.VGoodBundle localVGoodBundle = (SessionMessages.VGoodBundle)this.mSelector.getItemAtPosition(i);
        if ((localVGoodBundle != null) && (localVGoodBundle.getCinematic().getType() == SessionMessages.VGoodCinematic.Type.AVATAR) && (localVGoodBundle.getCinematic().getAssetId() == paramLong))
        {
          this.mCurrentShowPos = i;
          this.currentShowBundle = localVGoodBundle;
        }
      }
      else
      {
        return;
      }
  }

  public int getButtonState()
  {
    if (isEnabled())
      return 0;
    return 1;
  }

  public void hideCurrentPlayingAnimation()
  {
    Log.v("VGoodButtonsBarView", "hideCurrentPlayingAnimation" + this.showingCurrentPlayButton);
    if (!this.showingCurrentPlayButton)
      return;
    this.mAdapter.clearHightlightWithBackground();
    this.currentShowBundle = null;
    this.mCurrentShowPos = -1;
    this.showingCurrentPlayButton = false;
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    if (!paramView.isShown());
    SessionMessages.VGoodBundle localVGoodBundle;
    do
    {
      do
      {
        return;
        localVGoodBundle = (SessionMessages.VGoodBundle)paramAdapterView.getItemAtPosition(paramInt);
      }
      while ((this.mParent.hasControlBarAnimation()) || ((!this.mAdapter.isEnabled()) && (localVGoodBundle != null) && (localVGoodBundle.getCinematic().getType() != SessionMessages.VGoodCinematic.Type.FILTER)));
      Log.d("VGoodButtonsBarView", "onItemClick:" + paramInt + ", isItemEnabled:" + this.mAdapter.isItemEnabled(paramInt));
    }
    while (!this.mAdapter.isItemEnabled(paramInt));
    if (localVGoodBundle != null)
    {
      if (localVGoodBundle.getCinematic().getType() == SessionMessages.VGoodCinematic.Type.AVATAR)
        onAvatarClicked(localVGoodBundle);
      while (true)
      {
        this.mAdapter.setHighlight(paramInt);
        return;
        if (localVGoodBundle.getCinematic().getType() == SessionMessages.VGoodCinematic.Type.FILTER)
          this.mParent.setFilter((int)localVGoodBundle.getCinematic().getAssetId(), paramInt);
        else
          playVGood(localVGoodBundle);
      }
    }
    showBubbleForLockedVgood();
  }

  public void reenableAllButtons(boolean paramBoolean)
  {
    if (this.allData == null)
      Log.e("VGoodButtonsBarView", "reenableAllButtons: setData has never been called");
    do
    {
      return;
      this.mAdapter.setData(this.allData, paramBoolean);
      this.mAdapter.setShowEmptySlots(true);
    }
    while (this.mHorizontalListview != null);
    this.mList.setSelection(this.mList.getCount() - 1);
  }

  public void refreshCurrentPlayingAnimation(long paramLong)
  {
    Log.d("VGoodButtonsBarView", "refreshCurrentPlayingAnimation:currentShowPos=" + this.mCurrentShowPos);
    if (!this.showingCurrentPlayButton)
      return;
    hideCurrentPlayingAnimation();
    showCurrentPlayingAnimation(paramLong);
  }

  public void setData(List<SessionMessages.VGoodBundle> paramList, boolean paramBoolean)
  {
    if (paramList == null)
      return;
    this.allData = new ArrayList(paramList);
    this.mAdapter.setData(paramList, paramBoolean);
    this.currentShowBundle = null;
    this.mCurrentShowPos = -1;
  }

  public void setEmptySlots(int paramInt)
  {
    int i = 4;
    VGoodSelectorAdapter localVGoodSelectorAdapter = this.mAdapter;
    if (paramInt > i);
    while (true)
    {
      localVGoodSelectorAdapter.setEmptyCount(i);
      return;
      i = paramInt;
    }
  }

  public void setFilterHightlightWithBackground(int paramInt)
  {
    int i = findFilterPositionByIndex(paramInt);
    Log.v("VGoodButtonsBarView", "setFilterHightlightWithBackground filterIndex=" + paramInt + " position=" + i);
    if (i != -1)
      this.mAdapter.setHightlightWithBackground(i);
  }

  public void setHightlightWithBackground(int paramInt)
  {
    this.mAdapter.setHightlightWithBackground(paramInt);
  }

  public void showCurrentPlayingAnimation(long paramLong)
  {
    Log.d("VGoodButtonsBarView", "showCurrentPlayingAnimation " + paramLong + " " + this.showingCurrentPlayButton);
    if (this.showingCurrentPlayButton)
      return;
    findBundleById(paramLong);
    if (this.currentShowBundle == null)
    {
      Log.w("VGoodButtonsBarView", "can not find avatar in selector " + paramLong);
      return;
    }
    Log.d("VGoodButtonsBarView", "showCurrentPlayingAnimation:currentShowPos=" + this.mCurrentShowPos);
    this.mAdapter.clearHighlight();
    this.mAdapter.setHightlightWithBackground(this.mCurrentShowPos);
    this.showingCurrentPlayButton = true;
  }

  public void updateButtons(int paramInt)
  {
    if (paramInt == 0)
    {
      this.mAdapter.setEnabled(true);
      this.mAdapter.clearHighlight();
      this.mSelector.setEnabled(true);
      return;
    }
    this.mAdapter.setEnabled(false);
    this.mSelector.setEnabled(false);
  }

  public static abstract interface ScrollListener
  {
    public abstract void isGoingToStop();

    public abstract void startScrolling();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.vgood.VGoodButtonsBarView
 * JD-Core Version:    0.6.2
 */