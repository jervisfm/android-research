package com.sgiggle.cafe.vgood;

import com.sgiggle.media_engine.MediaEngineMessage.DisplayAnimationEvent;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ControlAnimationPayload;

public class VGoodRenderer
  implements CafeRenderer.CafeViewRenderer
{
  private static final String TAG = "VGOOD_RENDERER";
  private MediaEngineMessage.DisplayAnimationEvent mAnimationEvent = null;
  private boolean mGLRendererMode = false;
  private boolean mSurfaceReady = false;
  private volatile int mSurpriseId = -1;

  public VGoodRenderer(boolean paramBoolean)
  {
    this.mGLRendererMode = paramBoolean;
  }

  private void startAnimation()
  {
    if (((this.mGLRendererMode) || (this.mSurfaceReady)) && (this.mAnimationEvent != null))
    {
      Log.v("VGOOD_RENDERER", "startAnimation");
      CafeMgr.Pause();
      CafeMgr.Resume();
      long l1 = ((SessionMessages.ControlAnimationPayload)this.mAnimationEvent.payload()).getAssetId();
      long l2 = ((SessionMessages.ControlAnimationPayload)this.mAnimationEvent.payload()).getSeed();
      String str = ((SessionMessages.ControlAnimationPayload)this.mAnimationEvent.payload()).getAssetPath();
      CafeMgr.LoadSurprise(str, "Surprise.Cafe");
      this.mSurpriseId = CafeMgr.StartSurprise(str, l2, l1, false);
      this.mAnimationEvent = null;
    }
  }

  public void handleDisplayAnimationEvent(MediaEngineMessage.DisplayAnimationEvent paramDisplayAnimationEvent)
  {
    Log.v("VGOOD_RENDERER", "handleDisplayAnimationEvent");
    this.mAnimationEvent = paramDisplayAnimationEvent;
    startAnimation();
  }

  public void onSurfaceChanged(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    Log.v("VGOOD_RENDERER", "onSurfaceChanged w " + paramInt1 + " h " + paramInt2);
    if (this.mGLRendererMode)
    {
      Log.i("VGOOD_RENDERER", "GL2.0 should not call onSurfaceChanged ");
      return;
    }
    CafeRenderer.setCafeMainView(paramInt1, paramInt2, paramBoolean1, paramBoolean2);
    this.mSurfaceReady = true;
    startAnimation();
  }

  public void onSurfaceCreated()
  {
    Log.v("VGOOD_RENDERER", "onSurfaceCreated");
  }

  public void render()
  {
    CafeMgr.RenderMain();
  }

  public void stopAnimation()
  {
    if (this.mSurpriseId > 0)
    {
      CafeMgr.StopSurprise(this.mSurpriseId);
      CafeMgr.Pause();
    }
    this.mSurpriseId = -1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.cafe.vgood.VGoodRenderer
 * JD-Core Version:    0.6.2
 */