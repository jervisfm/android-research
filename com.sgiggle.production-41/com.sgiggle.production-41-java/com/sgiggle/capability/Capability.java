package com.sgiggle.capability;

public class Capability
{
  public static final int FEATURE = 0;
  public static final int HARDWARD = 1;
  public static final int SOFTWARE = 2;

  public static native boolean getBool(int paramInt, String paramString);

  public static native int getInt(int paramInt, String paramString);

  public static native long getLong(int paramInt, String paramString);

  public static native String getString(int paramInt, String paramString);

  public static native String[] keys(int paramInt);

  public static native void reset(int paramInt, String paramString);

  public static native void set(int paramInt1, String paramString, int paramInt2);

  public static native void set(int paramInt, String paramString, long paramLong);

  public static native void set(int paramInt, String paramString1, String paramString2);

  public static native void set(int paramInt, String paramString, boolean paramBoolean);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.capability.Capability
 * JD-Core Version:    0.6.2
 */