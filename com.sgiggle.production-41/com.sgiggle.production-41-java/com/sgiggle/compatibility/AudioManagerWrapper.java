package com.sgiggle.compatibility;

import android.media.AudioManager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AudioManagerWrapper
{
  private static Method m_isBluetoothScoAvailableOffCall;
  private static Method m_startBluetoothSco;
  private static Method m_stopBluetoothSco;

  static
  {
    try
    {
      Class.forName("android.media.AudioManager");
      m_startBluetoothSco = AudioManager.class.getMethod("startBluetoothSco", new Class[0]);
      m_stopBluetoothSco = AudioManager.class.getMethod("stopBluetoothSco", new Class[0]);
      m_isBluetoothScoAvailableOffCall = AudioManager.class.getMethod("isBluetoothScoAvailableOffCall", new Class[0]);
      return;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  public static void checkAvailable()
  {
  }

  public static boolean isBluetoothScoAvailableOffCall(AudioManager paramAudioManager)
  {
    try
    {
      boolean bool = ((Boolean)m_isBluetoothScoAvailableOffCall.invoke(paramAudioManager, new Object[0])).booleanValue();
      return bool;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localIllegalArgumentException.printStackTrace();
      return false;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      while (true)
        localIllegalAccessException.printStackTrace();
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      while (true)
        localInvocationTargetException.printStackTrace();
    }
  }

  public static void startBluetoothSco(AudioManager paramAudioManager)
  {
    try
    {
      m_startBluetoothSco.invoke(paramAudioManager, new Object[0]);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localIllegalArgumentException.printStackTrace();
      return;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      localIllegalAccessException.printStackTrace();
      return;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      localInvocationTargetException.printStackTrace();
    }
  }

  public static void stopBluetoothSco(AudioManager paramAudioManager)
  {
    try
    {
      m_stopBluetoothSco.invoke(paramAudioManager, new Object[0]);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localIllegalArgumentException.printStackTrace();
      return;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      localIllegalAccessException.printStackTrace();
      return;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      localInvocationTargetException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.compatibility.AudioManagerWrapper
 * JD-Core Version:    0.6.2
 */