package com.sgiggle.contacts;

public class Contact
{
  public long deviceContactId = -1L;
  public String displayName;
  public String[] emailAddresses;
  public String firstName;
  public boolean hasPicture;
  public String lastName;
  public String middleName;
  public String namePrefix;
  public String nameSuffix;
  public boolean nativeFavorite;
  public int[] phoneTypes;
  public String[] subscriberNumbers;

  public Contact()
  {
    this("", "", "", "", "", "");
  }

  public Contact(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    this.namePrefix = paramString1;
    this.firstName = paramString2;
    this.middleName = paramString3;
    this.lastName = paramString4;
    this.nameSuffix = paramString5;
    this.displayName = paramString6;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.contacts.Contact
 * JD-Core Version:    0.6.2
 */