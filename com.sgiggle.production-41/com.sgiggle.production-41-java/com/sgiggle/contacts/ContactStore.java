package com.sgiggle.contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.RawContacts;
import android.text.TextUtils;
import com.sgiggle.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ContactStore
{
  private static final int PHONE_TYPE_GENERIC = 0;
  private static final int PHONE_TYPE_HOME = 2;
  private static final int PHONE_TYPE_MAIN = 4;
  private static final int PHONE_TYPE_MOBILE = 1;
  private static final int PHONE_TYPE_WORK = 3;
  private static final String TAG = "Tango.ContactStore";
  private static ContentResolver s_contentResolver;
  private static Set<Long> s_rawContactIdsFromTangoSync = new HashSet();

  private static void addSubscriberNumbersToContact(String paramString, Contact paramContact)
  {
    Log.d("Tango.ContactStore", "addSubscriberNumbersToContact called");
    Cursor localCursor = s_contentResolver.query(ContactsContract.Data.CONTENT_URI, new String[] { "data1", "data2" }, "contact_id=? AND mimetype='vnd.android.cursor.item/phone_v2'", new String[] { paramString }, null);
    if (localCursor == null)
    {
      Log.w("Tango.ContactStore", "addSubscriberNumbersToContact(): ContentResolver failed to query.");
      return;
    }
    int i = localCursor.getCount();
    String[] arrayOfString;
    int[] arrayOfInt1;
    if (i > 0)
    {
      localCursor.moveToFirst();
      arrayOfString = new String[i];
      int[] arrayOfInt2 = new int[i];
      for (int j = 0; j < i; j++)
      {
        arrayOfString[j] = localCursor.getString(0);
        arrayOfInt2[j] = translatePhoneType(localCursor.getInt(1));
        Log.d("Tango.ContactStore", "number = " + arrayOfString[j] + ", type = " + arrayOfInt2[j]);
        localCursor.moveToNext();
      }
      arrayOfInt1 = arrayOfInt2;
    }
    while (true)
    {
      localCursor.close();
      paramContact.subscriberNumbers = arrayOfString;
      paramContact.phoneTypes = arrayOfInt1;
      return;
      arrayOfInt1 = null;
      arrayOfString = null;
    }
  }

  private static Contact buildContact(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    Log.v("Tango.ContactStore", "buildContact: " + paramString7);
    Contact localContact = new Contact(paramString2, paramString3, paramString4, paramString5, paramString6, paramString7);
    addSubscriberNumbersToContact(paramString1, localContact);
    localContact.emailAddresses = getEmailAddresses(paramString1);
    localContact.deviceContactId = Long.decode(paramString1).longValue();
    localContact.hasPicture = hasPicture(localContact.deviceContactId);
    localContact.nativeFavorite = nativeFavorite(localContact.deviceContactId);
    return localContact;
  }

  private static void checkThumbnails4Pictures(ContentResolver paramContentResolver, HashMap<Long, Contact> paramHashMap)
  {
    Log.d("Tango.ContactStore", "checkThumbnails4Pictures()...");
    String[] arrayOfString = { "_id", "photo_id" };
    Cursor localCursor = paramContentResolver.query(ContactsContract.Contacts.CONTENT_URI, arrayOfString, "PHOTO_ID is not null", null, null);
    if (localCursor != null)
    {
      while (localCursor.moveToNext())
      {
        Contact localContact = (Contact)paramHashMap.get(Long.valueOf(localCursor.getLong(0)));
        if (localContact != null)
          localContact.hasPicture = true;
      }
      localCursor.close();
    }
  }

  public static Contact[] getAllContacts()
  {
    Log.d("Tango.ContactStore", "getAllContacts()...");
    if (s_contentResolver == null)
    {
      Log.w("Tango.ContactStore", "getAllContacts(): s_contentResolver is null. Return NULL.");
      return null;
    }
    HashMap localHashMap = loadInitialContactIds(s_contentResolver);
    loadAllNames(s_contentResolver, localHashMap);
    loadAllPhoneNumbers(s_contentResolver, localHashMap);
    loadAllEmails(s_contentResolver, localHashMap);
    checkThumbnails4Pictures(s_contentResolver, localHashMap);
    getNativeFavoriteContacts(s_contentResolver, localHashMap);
    Log.d("Tango.ContactStore", "getAllContacts(): loaded count = " + localHashMap.size());
    if (localHashMap.size() > 0)
      return (Contact[])localHashMap.values().toArray(new Contact[localHashMap.size()]);
    return null;
  }

  private static String getAlternativeDisplayName(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    if (!TextUtils.isEmpty(paramString1))
      localStringBuffer.append(paramString1);
    if (!TextUtils.isEmpty(paramString4))
    {
      if (localStringBuffer.length() > 0)
        localStringBuffer.append(" ");
      localStringBuffer.append(paramString4);
    }
    for (int i = 1; ; i = 0)
    {
      if (!TextUtils.isEmpty(paramString2))
      {
        if (localStringBuffer.length() > 0)
        {
          if (i != 0)
          {
            localStringBuffer.append(",");
            i = 0;
          }
          localStringBuffer.append(" ");
        }
        localStringBuffer.append(paramString2);
      }
      if (!TextUtils.isEmpty(paramString3))
      {
        if (localStringBuffer.length() > 0)
        {
          if (i != 0)
            localStringBuffer.append(",");
          localStringBuffer.append(" ");
        }
        localStringBuffer.append(paramString3);
      }
      if (!TextUtils.isEmpty(paramString5))
      {
        if (localStringBuffer.length() > 0)
          localStringBuffer.append(", ");
        localStringBuffer.append(paramString5);
      }
      return localStringBuffer.toString();
    }
  }

  public static Contact getContactByNumber(String paramString)
  {
    if (s_contentResolver == null)
    {
      Log.w("Tango.ContactStore", "getContactByNumber(): s_contentResolver is null. Return NULL.");
      return null;
    }
    if ((paramString == null) || (paramString.length() == 0))
      return null;
    Uri localUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(paramString));
    Cursor localCursor1 = s_contentResolver.query(localUri, new String[] { "_id", "display_name" }, null, null, null);
    if (localCursor1 == null)
    {
      Log.w("Tango.ContactStore", "getContactByNumber(): ContentResolver failed to query.");
      return null;
    }
    while (true)
    {
      try
      {
        if (localCursor1.moveToNext())
        {
          String str = localCursor1.getString(0);
          localContact = buildContact(str, "", "", "", "", "", localCursor1.getString(1));
          ContactOrderPair localContactOrderPair = ContactOrderPair.getFromPhone(s_contentResolver);
          String[] arrayOfString1;
          Cursor localCursor2;
          if ((localContactOrderPair.getDisplayOrder() == ContactStore.ContactOrderPair.ContactOrder.ALTERNATIVE) && (Build.VERSION.SDK_INT >= 11))
          {
            arrayOfString1 = new String[] { "contact_id", "display_name_alt", "data4", "data2", "data5", "data3", "data6" };
            String[] arrayOfString2 = { str, "vnd.android.cursor.item/name" };
            localCursor2 = s_contentResolver.query(ContactsContract.Data.CONTENT_URI, arrayOfString1, "contact_id = ? AND mimetype = ?", arrayOfString2, null);
            if (localCursor2 == null)
            {
              Log.w("Tango.ContactStore", "getContactByNumber(): ContentResolver failed to query.");
              return null;
            }
          }
          else
          {
            arrayOfString1 = new String[] { "contact_id", "display_name", "data4", "data2", "data5", "data3", "data6" };
            continue;
          }
          if (localCursor2.moveToNext())
          {
            if ((localCursor2 == null) || ((TextUtils.isEmpty(localCursor2.getString(2))) && (TextUtils.isEmpty(localCursor2.getString(3))) && (TextUtils.isEmpty(localCursor2.getString(4))) && (TextUtils.isEmpty(localCursor2.getString(5))) && (TextUtils.isEmpty(localCursor2.getString(6)))))
              continue;
            localContact.displayName = localCursor2.getString(1);
            localContact.namePrefix = localCursor2.getString(2);
            localContact.firstName = localCursor2.getString(3);
            localContact.middleName = localCursor2.getString(4);
            localContact.lastName = localCursor2.getString(5);
            localContact.nameSuffix = localCursor2.getString(6);
            if ((localContactOrderPair.getDisplayOrder() == ContactStore.ContactOrderPair.ContactOrder.ALTERNATIVE) && (Build.VERSION.SDK_INT < 11))
              localContact.displayName = getAlternativeDisplayName(localContact.namePrefix, localContact.firstName, localContact.middleName, localContact.lastName, localContact.nameSuffix);
          }
          localCursor2.close();
          return localContact;
        }
      }
      finally
      {
        localCursor1.close();
      }
      Contact localContact = null;
    }
  }

  private static Uri getContactUri(long paramLong)
  {
    return Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, Long.toString(paramLong));
  }

  private static String[] getEmailAddresses(String paramString)
  {
    Cursor localCursor = s_contentResolver.query(ContactsContract.Data.CONTENT_URI, new String[] { "data1" }, "contact_id=? AND mimetype='vnd.android.cursor.item/email_v2'", new String[] { paramString }, null);
    if (localCursor == null)
    {
      Log.w("Tango.ContactStore", "getEmailAddresses(): ContentResolver failed to query.");
      return null;
    }
    int i = localCursor.getCount();
    String[] arrayOfString2;
    if (i > 0)
    {
      localCursor.moveToFirst();
      arrayOfString2 = new String[i];
      for (int j = 0; j < i; j++)
      {
        arrayOfString2[j] = localCursor.getString(0);
        localCursor.moveToNext();
      }
    }
    for (String[] arrayOfString1 = arrayOfString2; ; arrayOfString1 = null)
    {
      localCursor.close();
      return arrayOfString1;
    }
  }

  private static void getNativeFavoriteContacts(ContentResolver paramContentResolver, HashMap<Long, Contact> paramHashMap)
  {
    Log.d("Tango.ContactStore", "getNativeFavoriteContacts()...");
    String[] arrayOfString = { "_id", "starred" };
    Cursor localCursor = s_contentResolver.query(ContactsContract.Contacts.CONTENT_URI, arrayOfString, "starred=?", new String[] { "1" }, null);
    if (localCursor != null)
    {
      while (localCursor.moveToNext())
      {
        Log.d("Tango.ContactStore", "Found favorite contact: " + localCursor.getLong(0));
        Contact localContact = (Contact)paramHashMap.get(Long.valueOf(localCursor.getLong(0)));
        if (localContact != null)
          localContact.nativeFavorite = true;
      }
      localCursor.close();
    }
  }

  private static Cursor getNativeFavortieCursor4Contact(long paramLong, String[] paramArrayOfString)
  {
    Uri localUri = getContactUri(paramLong);
    String[] arrayOfString = { "_id", "starred" };
    return s_contentResolver.query(localUri, arrayOfString, "starred=?", new String[] { "1" }, null);
  }

  public static Bitmap getPhotoByContactId(long paramLong)
  {
    if (s_contentResolver == null)
    {
      Log.w("Tango.ContactStore", "getPhotoByContactId(): s_contentResolver is null. Return NULL.");
      return null;
    }
    InputStream localInputStream = openContactPhoto(paramLong);
    if (localInputStream == null)
      return null;
    Bitmap localBitmap = BitmapFactory.decodeStream(localInputStream);
    try
    {
      localInputStream.close();
      label37: return localBitmap;
    }
    catch (IOException localIOException)
    {
      break label37;
    }
  }

  private static Cursor getThumbnailsCursor4Contact(long paramLong, String[] paramArrayOfString)
  {
    Uri localUri = getContactUri(paramLong);
    return s_contentResolver.query(localUri, paramArrayOfString, "PHOTO_ID is not null", null, null);
  }

  private static boolean hasPicture(long paramLong)
  {
    Cursor localCursor = getThumbnailsCursor4Contact(paramLong, new String[0]);
    if (localCursor != null)
    {
      boolean bool = localCursor.moveToFirst();
      localCursor.close();
      if (bool)
        return true;
    }
    return false;
  }

  private static void loadAllEmails(ContentResolver paramContentResolver, HashMap<Long, Contact> paramHashMap)
  {
    Log.d("Tango.ContactStore", "loadAllEmails()...");
    Cursor localCursor = paramContentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, new String[] { "contact_id", "data1", "raw_contact_id" }, null, null, null);
    if (localCursor == null)
    {
      Log.w("Tango.ContactStore", "loadAllEmails(): ContentResolver failed to query.");
      return;
    }
    while (localCursor.moveToNext())
    {
      long l = localCursor.getLong(2);
      String str = localCursor.getString(1);
      if (s_rawContactIdsFromTangoSync.contains(Long.valueOf(l)))
      {
        Log.w("Tango.ContactStore", "Discard contact data from tango_sync: Email.RAW_CONTACT_ID=" + l + ", Email.DATA=" + str);
      }
      else
      {
        Contact localContact = (Contact)paramHashMap.get(Long.valueOf(localCursor.getLong(0)));
        if (localContact != null)
        {
          ArrayList localArrayList = new ArrayList();
          if ((localContact.emailAddresses != null) && (localContact.emailAddresses.length > 0))
            localArrayList.addAll(Arrays.asList(localContact.emailAddresses));
          localArrayList.add(str);
          localContact.emailAddresses = ((String[])localArrayList.toArray(new String[localArrayList.size()]));
        }
      }
    }
    localCursor.close();
    Log.d("Tango.ContactStore", "loadAllEmails(): done loaded.");
  }

  private static void loadAllNames(ContentResolver paramContentResolver, HashMap<Long, Contact> paramHashMap)
  {
    Log.d("Tango.ContactStore", "loadAllNames()...");
    ContactOrderPair localContactOrderPair = ContactOrderPair.getFromPhone(paramContentResolver);
    String[] arrayOfString1 = { "contact_id", "data4", "data2", "data5", "data3", "data6" };
    String[] arrayOfString2 = { "vnd.android.cursor.item/name" };
    Cursor localCursor = paramContentResolver.query(ContactsContract.Data.CONTENT_URI, arrayOfString1, "mimetype = ?", arrayOfString2, null);
    if (localCursor == null)
    {
      Log.w("Tango.ContactStore", "loadAllNames(): ContentResolver failed to query.");
      return;
    }
    while (localCursor.moveToNext())
    {
      Contact localContact = (Contact)paramHashMap.get(Long.valueOf(localCursor.getLong(0)));
      if ((localContact != null) && ((!TextUtils.isEmpty(localCursor.getString(1))) || (!TextUtils.isEmpty(localCursor.getString(2))) || (!TextUtils.isEmpty(localCursor.getString(3))) || (!TextUtils.isEmpty(localCursor.getString(4))) || (!TextUtils.isEmpty(localCursor.getString(5)))))
      {
        localContact.namePrefix = localCursor.getString(1);
        localContact.firstName = localCursor.getString(2);
        localContact.middleName = localCursor.getString(3);
        localContact.lastName = localCursor.getString(4);
        localContact.nameSuffix = localCursor.getString(5);
        if ((localContactOrderPair.getDisplayOrder() == ContactStore.ContactOrderPair.ContactOrder.ALTERNATIVE) && (Build.VERSION.SDK_INT < 11))
          localContact.displayName = getAlternativeDisplayName(localContact.namePrefix, localContact.firstName, localContact.middleName, localContact.lastName, localContact.nameSuffix);
      }
    }
    localCursor.close();
    Log.d("Tango.ContactStore", "loadAllNames(): done loaded.");
  }

  private static void loadAllPhoneNumbers(ContentResolver paramContentResolver, HashMap<Long, Contact> paramHashMap)
  {
    Log.d("Tango.ContactStore", "loadAllPhoneNumbers()...");
    Cursor localCursor = paramContentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[] { "contact_id", "data1", "data2", "raw_contact_id" }, null, null, null);
    if (localCursor == null)
    {
      Log.w("Tango.ContactStore", "loadAllPhoneNumbers(): ContentResolver failed to query.");
      return;
    }
    while (localCursor.moveToNext())
    {
      long l = localCursor.getLong(3);
      String str = localCursor.getString(1);
      if (s_rawContactIdsFromTangoSync.contains(Long.valueOf(l)))
      {
        Log.w("Tango.ContactStore", "Discard contact data from tango_sync: Phone.RAW_CONTACT_ID=" + l + ", Phone.NUMBER=" + str);
      }
      else
      {
        Contact localContact = (Contact)paramHashMap.get(Long.valueOf(localCursor.getLong(0)));
        if (localContact != null)
        {
          ArrayList localArrayList1 = new ArrayList();
          if ((localContact.subscriberNumbers != null) && (localContact.subscriberNumbers.length > 0))
            localArrayList1.addAll(Arrays.asList(localContact.subscriberNumbers));
          localArrayList1.add(str);
          localContact.subscriberNumbers = ((String[])localArrayList1.toArray(new String[localArrayList1.size()]));
          ArrayList localArrayList2 = new ArrayList();
          if (localContact.phoneTypes != null)
          {
            int[] arrayOfInt = localContact.phoneTypes;
            int j = arrayOfInt.length;
            for (int k = 0; k < j; k++)
              localArrayList2.add(Integer.valueOf(arrayOfInt[k]));
          }
          localArrayList2.add(Integer.valueOf(translatePhoneType(localCursor.getInt(2))));
          localContact.phoneTypes = new int[localArrayList2.size()];
          for (int i = 0; i < localArrayList2.size(); i++)
            localContact.phoneTypes[i] = ((Integer)localArrayList2.get(i)).intValue();
        }
      }
    }
    localCursor.close();
    Log.d("Tango.ContactStore", "loadAllPhoneNumbers(): done loaded.");
  }

  private static HashMap<Long, Contact> loadInitialContactIds(ContentResolver paramContentResolver)
  {
    Log.d("Tango.ContactStore", "loadInitialContactIds()...");
    HashMap localHashMap = new HashMap();
    ContactOrderPair localContactOrderPair = ContactOrderPair.getFromPhone(paramContentResolver);
    String str1;
    if (Build.MANUFACTURER.equalsIgnoreCase("SHARP"))
    {
      str1 = null;
      if ((localContactOrderPair.getDisplayOrder() != ContactStore.ContactOrderPair.ContactOrder.ALTERNATIVE) || (Build.VERSION.SDK_INT < 11))
        break label111;
    }
    Cursor localCursor1;
    label111: for (String[] arrayOfString1 = { "_id", "display_name_alt" }; ; arrayOfString1 = new String[] { "_id", "display_name" })
    {
      localCursor1 = paramContentResolver.query(ContactsContract.Contacts.CONTENT_URI, arrayOfString1, str1, null, null);
      if (localCursor1 != null)
        break label131;
      Log.w("Tango.ContactStore", "loadInitialContactIds(): ContentResolver failed to query.");
      return localHashMap;
      str1 = "in_visible_group=1";
      break;
    }
    label131: 
    while (localCursor1.moveToNext())
    {
      Contact localContact = new Contact();
      localContact.deviceContactId = localCursor1.getLong(0);
      localContact.displayName = localCursor1.getString(1);
      localHashMap.put(Long.valueOf(localContact.deviceContactId), localContact);
    }
    localCursor1.close();
    String[] arrayOfString2 = { "_id", "contact_id", "account_name", "account_type" };
    Cursor localCursor2 = paramContentResolver.query(ContactsContract.RawContacts.CONTENT_URI, arrayOfString2, null, null, null);
    s_rawContactIdsFromTangoSync.clear();
    while (localCursor2.moveToNext())
    {
      long l1 = localCursor2.getLong(0);
      long l2 = localCursor2.getLong(1);
      String str2 = localCursor2.getString(2);
      String str3 = localCursor2.getString(3);
      if ((str2 != null) && (str3 != null) && (str3.startsWith("com.sgiggle.app")))
      {
        s_rawContactIdsFromTangoSync.add(Long.valueOf(l1));
        Log.w("Tango.ContactStore", "Add RawContact from tango_sync to black list: RawContacts._ID=" + l1 + ", RawContacts.CONTACT_ID=" + l2 + ", RawContacts.ACCOUNT_NAME=" + str2 + ", RawContacts.ACCOUNT_TYPE=" + str3);
      }
    }
    Log.d("Tango.ContactStore", "loadInitialContactIds(): loaded count = " + localHashMap.size());
    return localHashMap;
  }

  private static boolean nativeFavorite(long paramLong)
  {
    Cursor localCursor = getNativeFavortieCursor4Contact(paramLong, new String[0]);
    if (localCursor != null)
    {
      boolean bool = localCursor.moveToFirst();
      localCursor.close();
      if (bool)
        return true;
    }
    return false;
  }

  private static InputStream openContactPhoto(long paramLong)
  {
    return ContactsContract.Contacts.openContactPhotoInputStream(s_contentResolver, getContactUri(paramLong));
  }

  private static int translatePhoneType(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return 0;
    case 2:
      return 1;
    case 1:
      return 2;
    case 3:
      return 3;
    case 12:
    }
    return 4;
  }

  private static int[] translatePhoneTypes(ArrayList<Integer> paramArrayList)
  {
    int[] arrayOfInt = new int[paramArrayList.size()];
    for (int i = 0; i < arrayOfInt.length; i++)
      arrayOfInt[i] = translatePhoneType(((Integer)paramArrayList.get(i)).intValue());
    return arrayOfInt;
  }

  public static void updateContext(Context paramContext)
  {
    if (paramContext == null)
      Log.e("Tango.ContactStore", "updateContext: context is unexpectedly null");
    do
    {
      return;
      s_contentResolver = paramContext.getContentResolver();
    }
    while (s_contentResolver != null);
    Log.e("Tango.ContactStore", "updateContext: getContextResolver() returned null.");
  }

  public static class ContactOrderPair
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    private ContactOrder m_displayOrder = ContactOrder.PRIMARY;
    private ContactOrder m_sortOrder = ContactOrder.PRIMARY;

    private ContactOrderPair(ContactOrder paramContactOrder1, ContactOrder paramContactOrder2)
    {
      this.m_sortOrder = paramContactOrder1;
      this.m_displayOrder = paramContactOrder2;
    }

    public static ContactOrderPair getDefault()
    {
      return new ContactOrderPair(ContactOrder.PRIMARY, ContactOrder.PRIMARY);
    }

    // ERROR //
    public static ContactOrderPair getFromPhone(ContentResolver paramContentResolver)
    {
      // Byte code:
      //   0: getstatic 23	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:PRIMARY	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   3: astore_1
      //   4: getstatic 23	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:PRIMARY	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   7: astore_2
      //   8: ldc 37
      //   10: getstatic 43	android/os/Build:MANUFACTURER	Ljava/lang/String;
      //   13: invokevirtual 49	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   16: ifeq +266 -> 282
      //   19: ldc 51
      //   21: getstatic 54	android/os/Build:MODEL	Ljava/lang/String;
      //   24: invokevirtual 49	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   27: ifne +255 -> 282
      //   30: ldc 56
      //   32: invokestatic 62	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
      //   35: astore 13
      //   37: aload_0
      //   38: aload 13
      //   40: iconst_2
      //   41: anewarray 45	java/lang/String
      //   44: dup
      //   45: iconst_0
      //   46: ldc 64
      //   48: aastore
      //   49: dup
      //   50: iconst_1
      //   51: ldc 66
      //   53: aastore
      //   54: aconst_null
      //   55: aconst_null
      //   56: aconst_null
      //   57: invokevirtual 72	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
      //   60: astore 16
      //   62: aload 16
      //   64: ifnull +296 -> 360
      //   67: aload 16
      //   69: invokeinterface 78 1 0
      //   74: ifeq +286 -> 360
      //   77: ldc 80
      //   79: aload 16
      //   81: iconst_1
      //   82: invokeinterface 84 2 0
      //   87: invokevirtual 49	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   90: ifeq +157 -> 247
      //   93: getstatic 87	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:ALTERNATIVE	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   96: astore 5
      //   98: ldc 80
      //   100: aload 16
      //   102: iconst_0
      //   103: invokeinterface 84 2 0
      //   108: invokevirtual 49	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   111: ifeq +144 -> 255
      //   114: getstatic 87	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:ALTERNATIVE	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   117: astore 21
      //   119: aload 21
      //   121: astore 22
      //   123: aload 5
      //   125: astore 18
      //   127: aload 22
      //   129: astore 17
      //   131: aload 16
      //   133: ifnull +10 -> 143
      //   136: aload 16
      //   138: invokeinterface 90 1 0
      //   143: aload 17
      //   145: astore 6
      //   147: aload 18
      //   149: astore 5
      //   151: new 92	java/lang/StringBuilder
      //   154: dup
      //   155: invokespecial 93	java/lang/StringBuilder:<init>	()V
      //   158: ldc 95
      //   160: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   163: astore 7
      //   165: aload 5
      //   167: getstatic 23	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:PRIMARY	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   170: if_acmpne +155 -> 325
      //   173: ldc 101
      //   175: astore 8
      //   177: ldc 103
      //   179: aload 7
      //   181: aload 8
      //   183: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   186: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   189: invokestatic 113	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   192: pop
      //   193: new 92	java/lang/StringBuilder
      //   196: dup
      //   197: invokespecial 93	java/lang/StringBuilder:<init>	()V
      //   200: ldc 115
      //   202: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   205: astore 10
      //   207: aload 6
      //   209: getstatic 23	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:PRIMARY	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   212: if_acmpne +120 -> 332
      //   215: ldc 101
      //   217: astore 11
      //   219: ldc 103
      //   221: aload 10
      //   223: aload 11
      //   225: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   228: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   231: invokestatic 113	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   234: pop
      //   235: new 2	com/sgiggle/contacts/ContactStore$ContactOrderPair
      //   238: dup
      //   239: aload 5
      //   241: aload 6
      //   243: invokespecial 31	com/sgiggle/contacts/ContactStore$ContactOrderPair:<init>	(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V
      //   246: areturn
      //   247: getstatic 23	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:PRIMARY	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   250: astore 5
      //   252: goto -154 -> 98
      //   255: getstatic 23	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:PRIMARY	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   258: astore 21
      //   260: goto -141 -> 119
      //   263: astore 14
      //   265: aload_2
      //   266: astore 6
      //   268: aload_1
      //   269: astore 5
      //   271: ldc 103
      //   273: ldc 117
      //   275: invokestatic 120	com/sgiggle/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   278: pop
      //   279: goto -128 -> 151
      //   282: aload_0
      //   283: ldc 122
      //   285: getstatic 23	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:PRIMARY	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   288: invokevirtual 126	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:getValue	()I
      //   291: invokestatic 132	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
      //   294: invokestatic 136	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:fromInt	(I)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   297: astore_3
      //   298: aload_0
      //   299: ldc 138
      //   301: getstatic 23	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:PRIMARY	Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   304: invokevirtual 126	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:getValue	()I
      //   307: invokestatic 132	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
      //   310: invokestatic 136	com/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder:fromInt	(I)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
      //   313: astore 4
      //   315: aload_3
      //   316: astore 5
      //   318: aload 4
      //   320: astore 6
      //   322: goto -171 -> 151
      //   325: ldc 140
      //   327: astore 8
      //   329: goto -152 -> 177
      //   332: ldc 140
      //   334: astore 11
      //   336: goto -117 -> 219
      //   339: astore 20
      //   341: aload_2
      //   342: astore 6
      //   344: goto -73 -> 271
      //   347: astore 19
      //   349: aload 17
      //   351: astore 6
      //   353: aload 18
      //   355: astore 5
      //   357: goto -86 -> 271
      //   360: aload_2
      //   361: astore 17
      //   363: aload_1
      //   364: astore 18
      //   366: goto -235 -> 131
      //
      // Exception table:
      //   from	to	target	type
      //   37	62	263	java/lang/IllegalArgumentException
      //   67	98	263	java/lang/IllegalArgumentException
      //   247	252	263	java/lang/IllegalArgumentException
      //   98	119	339	java/lang/IllegalArgumentException
      //   255	260	339	java/lang/IllegalArgumentException
      //   136	143	347	java/lang/IllegalArgumentException
    }

    public static ContactOrderPair getFromPhone(Context paramContext)
    {
      return getFromPhone(paramContext.getContentResolver());
    }

    public ContactOrder getDisplayOrder()
    {
      return this.m_displayOrder;
    }

    public ContactOrder getSortOrder()
    {
      return this.m_sortOrder;
    }

    public static enum ContactOrder
    {
      private int m_value;

      static
      {
        ALTERNATIVE = new ContactOrder("ALTERNATIVE", 1, 2);
        ContactOrder[] arrayOfContactOrder = new ContactOrder[2];
        arrayOfContactOrder[0] = PRIMARY;
        arrayOfContactOrder[1] = ALTERNATIVE;
      }

      private ContactOrder(int paramInt)
      {
        this.m_value = paramInt;
      }

      public static ContactOrder fromInt(int paramInt)
      {
        if (paramInt == 2)
          return ALTERNATIVE;
        return PRIMARY;
      }

      public int getValue()
      {
        return this.m_value;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.contacts.ContactStore
 * JD-Core Version:    0.6.2
 */