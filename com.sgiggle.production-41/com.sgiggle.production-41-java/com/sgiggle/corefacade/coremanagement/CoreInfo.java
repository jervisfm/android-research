package com.sgiggle.corefacade.coremanagement;

public class CoreInfo
{
  protected boolean swigCMemOwn;
  private long swigCPtr;

  public CoreInfo()
  {
    this(coremanagementJNI.new_CoreInfo(), true);
  }

  protected CoreInfo(long paramLong, boolean paramBoolean)
  {
    this.swigCMemOwn = paramBoolean;
    this.swigCPtr = paramLong;
  }

  protected static long getCPtr(CoreInfo paramCoreInfo)
  {
    if (paramCoreInfo == null)
      return 0L;
    return paramCoreInfo.swigCPtr;
  }

  public static String getVersionString()
  {
    return coremanagementJNI.CoreInfo_getVersionString();
  }

  public void delete()
  {
    try
    {
      if (this.swigCPtr != 0L)
      {
        if (this.swigCMemOwn)
        {
          this.swigCMemOwn = false;
          coremanagementJNI.delete_CoreInfo(this.swigCPtr);
        }
        this.swigCPtr = 0L;
      }
      return;
    }
    finally
    {
    }
  }

  protected void finalize()
  {
    delete();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.corefacade.coremanagement.CoreInfo
 * JD-Core Version:    0.6.2
 */