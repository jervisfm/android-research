package com.sgiggle.corefacade.coremanagement;

public class coremanagementJNI
{
  public static final native String CoreInfo_getVersionString();

  public static final native void delete_CoreInfo(long paramLong);

  public static final native long new_CoreInfo();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.corefacade.coremanagement.coremanagementJNI
 * JD-Core Version:    0.6.2
 */