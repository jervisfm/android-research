package com.sgiggle.corefacade.telephony;

public class PhoneFormatter
{
  protected boolean swigCMemOwn;
  private long swigCPtr;

  public PhoneFormatter()
  {
    this(telephonyJNI.new_PhoneFormatter(), true);
  }

  protected PhoneFormatter(long paramLong, boolean paramBoolean)
  {
    this.swigCMemOwn = paramBoolean;
    this.swigCPtr = paramLong;
  }

  public static String format(String paramString1, String paramString2)
  {
    return telephonyJNI.PhoneFormatter_format(paramString1, paramString2);
  }

  protected static long getCPtr(PhoneFormatter paramPhoneFormatter)
  {
    if (paramPhoneFormatter == null)
      return 0L;
    return paramPhoneFormatter.swigCPtr;
  }

  public void delete()
  {
    try
    {
      if (this.swigCPtr != 0L)
      {
        if (this.swigCMemOwn)
        {
          this.swigCMemOwn = false;
          telephonyJNI.delete_PhoneFormatter(this.swigCPtr);
        }
        this.swigCPtr = 0L;
      }
      return;
    }
    finally
    {
    }
  }

  protected void finalize()
  {
    delete();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.corefacade.telephony.PhoneFormatter
 * JD-Core Version:    0.6.2
 */