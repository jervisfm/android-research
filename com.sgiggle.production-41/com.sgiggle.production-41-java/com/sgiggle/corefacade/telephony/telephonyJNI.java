package com.sgiggle.corefacade.telephony;

public class telephonyJNI
{
  public static final native String PhoneFormatter_format(String paramString1, String paramString2);

  public static final native void delete_PhoneFormatter(long paramLong);

  public static final native long new_PhoneFormatter();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.corefacade.telephony.telephonyJNI
 * JD-Core Version:    0.6.2
 */