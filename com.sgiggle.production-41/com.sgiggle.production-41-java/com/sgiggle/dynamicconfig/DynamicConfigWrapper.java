package com.sgiggle.dynamicconfig;

public class DynamicConfigWrapper
{
  public static final String DYNCFG_AUDIO_HAS_NO_EARPIECE = "noearpiece";
  public static final String DYNCFG_AUDIO_MODE = "audio_mode";
  public static final String DYNCFG_AUDIO_SOURCE = "audio_source";
  public static final String DYNCFG_CAMERA_AUTOFOCUS = "fcam_autofocus";
  public static final String DYNCFG_CAMERA_BACK_CAPTURE_HEIGHT_KEY = "bcam_captH";
  public static final String DYNCFG_CAMERA_BACK_CAPTURE_WIDTH_KEY = "bcam_captW";
  public static final String DYNCFG_CAMERA_BACK_HEIGHT_KEY = "bcam_H";
  public static final String DYNCFG_CAMERA_BACK_WIDTH_KEY = "bcam_W";
  public static final String DYNCFG_CAMERA_FRAME_RATE = "fcam_frate";
  public static final String DYNCFG_CAMERA_FRONT_CAPTURE_HEIGHT_KEY = "fcam_captH";
  public static final String DYNCFG_CAMERA_FRONT_CAPTURE_WIDTH_KEY = "fcam_captW";
  public static final String DYNCFG_CAMERA_FRONT_HEIGHT_KEY = "fcam_H";
  public static final String DYNCFG_CAMERA_FRONT_ROTATION_KEY = "fcam_rotation";
  public static final String DYNCFG_CAMERA_FRONT_WIDTH_KEY = "fcam_W";
  public static final String DYNCFG_CAMERA_RELEASE_WHEN_SUSPEND = "fcam_release";
  public static final String DYNCFG_CAMERA_USE_FPSRANGE_METHOD = "fcam_usefpsrange";
  public static final String DYNCFG_CAMERA_VIRTUALSIZE_LANDSCAPE_KEY = "fcam_vsize-land";
  public static final String DYNCFG_CAMERA_VIRTUALSIZE_PORTRAIT_KEY = "fcam_vsize-port";
  public static final String DYNCFG_VIDEO_NOSWAP = "noswap";
  public static final String DYNCFG_VIDEO_VSOFT_FLAG = "vsoft_flag";

  public static String get(String paramString)
  {
    return nativeGet(paramString);
  }

  private static native String nativeGet(String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.dynamicconfig.DynamicConfigWrapper
 * JD-Core Version:    0.6.2
 */