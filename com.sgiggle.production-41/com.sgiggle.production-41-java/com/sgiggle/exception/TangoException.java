package com.sgiggle.exception;

import android.os.Build;
import android.os.Build.VERSION;

public class TangoException extends RuntimeException
{
  public TangoException(Throwable paramThrowable)
  {
    super(getDeviceInfo(), paramThrowable);
  }

  private static String format(String paramString1, String paramString2)
  {
    return paramString1 + ":" + paramString2 + "|";
  }

  private static String getDeviceInfo()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(format("Manufacturer", Build.MANUFACTURER));
    localStringBuilder.append(format("Model", Build.MODEL));
    localStringBuilder.append(format("Device", Build.DEVICE));
    localStringBuilder.append(format("Build", Build.DISPLAY));
    localStringBuilder.append(format("OS", Build.VERSION.RELEASE));
    return localStringBuilder.toString() + "\n";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.exception.TangoException
 * JD-Core Version:    0.6.2
 */