package com.sgiggle.iphelper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

public class IpHelper
{
  private static final boolean TANGO_ALLOW_LINKING;
  private static String logTag = "IPHELPER";
  private static Context s_appContext;

  private static void getCStr(byte[] paramArrayOfByte, String paramString)
    throws Exception
  {
    byte[] arrayOfByte = paramString.getBytes("UTF-8");
    for (int i = 0; (i < arrayOfByte.length) && (i < paramArrayOfByte.length - 1); i++)
      paramArrayOfByte[i] = arrayOfByte[i];
    paramArrayOfByte[i] = 0;
  }

  public static void getCountryCode(byte[] paramArrayOfByte)
  {
    try
    {
      getCStr(paramArrayOfByte, ((TelephonyManager)s_appContext.getSystemService("phone")).getNetworkCountryIso());
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public static void getDevID(byte[] paramArrayOfByte)
  {
    int i = Build.VERSION.SDK_INT;
    TelephonyManager localTelephonyManager = (TelephonyManager)s_appContext.getSystemService("phone");
    if ((localTelephonyManager != null) && (localTelephonyManager.getDeviceId() != null))
    {
      String str3 = localTelephonyManager.getDeviceId();
      if ((str3 != null) && (str3.length() > 0) && (!"0".equals(str3)))
        try
        {
          getCStr(paramArrayOfByte, str3);
          return;
        }
        catch (Exception localException4)
        {
          Log.e(logTag, "Error performing getCStr: ", localException4);
        }
    }
    if (i >= 9);
    try
    {
      Field localField = Class.forName("android.os.Build").getField("SERIAL");
      String str2 = (String)localField.get(localField);
      if (str2 != null)
      {
        int j = str2.length();
        if (j > 0)
          try
          {
            getCStr(paramArrayOfByte, str2);
            return;
          }
          catch (Exception localException3)
          {
            Log.e(logTag, "Error performing getCStr: ", localException3);
          }
      }
      String str1 = Settings.Secure.getString(s_appContext.getContentResolver(), "android_id");
      if ((str1 != null) && (str1.length() > 0))
        try
        {
          getCStr(paramArrayOfByte, str1);
          return;
        }
        catch (Exception localException1)
        {
          Log.e(logTag, "Error performing getCStr: ", localException1);
        }
      Log.e(logTag, "Unable to retrieve a valid device id");
      return;
    }
    catch (Exception localException2)
    {
      while (true)
        Log.e(logTag, "Unable to get build serial information");
    }
  }

  public static String getDevIDBase64()
  {
    try
    {
      byte[] arrayOfByte = new byte[256];
      getDevID(arrayOfByte);
      String str = Base64.encodeToString(getString(arrayOfByte).getBytes(), 2);
      return str;
    }
    catch (Exception localException)
    {
      Log.e(logTag, "Error performing getString: ", localException);
    }
    return null;
  }

  public static void getDevName(byte[] paramArrayOfByte)
  {
    String str = Build.MODEL + "_" + Build.PRODUCT;
    try
    {
      getCStr(paramArrayOfByte, str);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public static int getDpi()
  {
    try
    {
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      ((WindowManager)s_appContext.getSystemService("window")).getDefaultDisplay().getMetrics(localDisplayMetrics);
      int i = localDisplayMetrics.densityDpi;
      return i;
    }
    catch (Exception localException)
    {
      Log.e(logTag, localException.toString());
    }
    return 0;
  }

  public static int getLocalIpv4Address(byte[] paramArrayOfByte)
  {
    int i;
    try
    {
      Enumeration localEnumeration1 = NetworkInterface.getNetworkInterfaces();
      i = 0;
      while (localEnumeration1.hasMoreElements())
      {
        Enumeration localEnumeration2 = ((NetworkInterface)localEnumeration1.nextElement()).getInetAddresses();
        while (localEnumeration2.hasMoreElements())
        {
          InetAddress localInetAddress = (InetAddress)localEnumeration2.nextElement();
          int j;
          if ((!localInetAddress.isLoopbackAddress()) && (localInetAddress.getAddress().length == 4))
          {
            paramArrayOfByte[(0 + i * 4)] = localInetAddress.getAddress()[0];
            paramArrayOfByte[(1 + i * 4)] = localInetAddress.getAddress()[1];
            paramArrayOfByte[(2 + i * 4)] = localInetAddress.getAddress()[2];
            paramArrayOfByte[(3 + i * 4)] = localInetAddress.getAddress()[3];
            j = i + 1;
            int k = paramArrayOfByte.length;
            if (j >= k)
              return j;
          }
          else
          {
            j = i;
          }
          i = j;
        }
      }
    }
    catch (SocketException localSocketException)
    {
      Log.e(logTag, localSocketException.toString());
      return 0;
    }
    return i;
  }

  public static void getLocale(byte[] paramArrayOfByte)
  {
    try
    {
      getCStr(paramArrayOfByte, Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry());
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public static void getOsVersion(byte[] paramArrayOfByte)
  {
    String str = Build.VERSION.RELEASE;
    try
    {
      getCStr(paramArrayOfByte, str);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public static int getScreenHeight()
  {
    try
    {
      int i = s_appContext.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
      return i;
    }
    catch (Exception localException)
    {
      Log.e(logTag, localException.toString());
    }
    return 0;
  }

  public static int getScreenWidth()
  {
    try
    {
      int i = s_appContext.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
      return i;
    }
    catch (Exception localException)
    {
      Log.e(logTag, localException.toString());
    }
    return 0;
  }

  private static String getString(byte[] paramArrayOfByte)
    throws Exception
  {
    for (int i = 0; (i < paramArrayOfByte.length) && (paramArrayOfByte[i] != 0); i++);
    return new String(paramArrayOfByte, 0, i, "UTF-8");
  }

  public static boolean isSMSCapable()
  {
    return !s_appContext.getPackageManager().queryIntentActivities(new Intent("android.intent.action.SENDTO", Uri.parse("smsto:")), 65536).isEmpty();
  }

  public static boolean isSmartPhone()
  {
    return true;
  }

  public static boolean isTelephonyCapable()
  {
    TelephonyManager localTelephonyManager = (TelephonyManager)s_appContext.getSystemService("phone");
    if (!s_appContext.getPackageManager().queryIntentActivities(new Intent("android.intent.action.CALL", Uri.parse("tel:")), 65536).isEmpty());
    for (int i = 1; (i == 0) || (localTelephonyManager == null) || (localTelephonyManager.getPhoneType() == 0); i = 0)
      return false;
    return true;
  }

  public static boolean needPersistentConnection()
  {
    return Build.VERSION.SDK_INT <= 7;
  }

  public static void removeFromPreviousContext()
  {
    s_appContext = null;
  }

  public static void updateContext(Context paramContext)
  {
    s_appContext = paramContext;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.iphelper.IpHelper
 * JD-Core Version:    0.6.2
 */