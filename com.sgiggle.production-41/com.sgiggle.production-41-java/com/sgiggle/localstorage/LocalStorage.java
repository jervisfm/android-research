package com.sgiggle.localstorage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.Process;
import com.sgiggle.util.Log;
import java.io.File;

public class LocalStorage
{
  private static final String PREFERENCE_FILE_NAME = "LocalStorage";
  private static final String PREF_STORAGE_DIR = "storage_dir";
  private static final String PREF_STORAGE_TYPE = "storage_type";
  private static final String STORAGE_TYPE_EXTERNAL = "external";
  private static final String STORAGE_TYPE_INTERNAL = "internal";
  private static BroadcastReceiver mExternalStorageReceiver;

  public static String getCacheDir(Context paramContext)
  {
    String str = new String();
    File localFile2;
    if ("mounted".equals(Environment.getExternalStorageState()))
    {
      if (Build.VERSION.SDK_INT < 8)
        break label67;
      localFile2 = paramContext.getExternalCacheDir();
      if (localFile2 == null);
    }
    label67: File localFile1;
    for (str = localFile2.getAbsolutePath(); ; str = localFile1.getAbsolutePath())
      do
      {
        if ((str == null) || (str.equals("")))
          str = paramContext.getCacheDir().getAbsolutePath();
        return str;
        localFile1 = new File(getStorageDirOnLowLevelPhone(paramContext) + "/appdata/");
        if (!localFile1.exists())
          localFile1.mkdirs();
      }
      while ((!localFile1.isDirectory()) || (!localFile1.exists()));
  }

  public static String getStorageDir(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("LocalStorage", 0);
    String str1 = localSharedPreferences.getString("storage_dir", null);
    String str2;
    if (str1 == null)
    {
      if ("mounted".equals(Environment.getExternalStorageState()))
      {
        if (Build.VERSION.SDK_INT < 8)
          break label121;
        File localFile3 = paramContext.getExternalFilesDir("storage");
        if (localFile3 != null)
          str1 = localFile3.getAbsolutePath();
      }
      if (str1 != null)
        break label203;
      str2 = paramContext.getDir("storage", 0).getAbsolutePath();
    }
    for (int i = 0; ; i = 1)
    {
      if (i != 0);
      for (String str3 = "external"; ; str3 = "internal")
      {
        localSharedPreferences.edit().putString("storage_dir", str2).putString("storage_type", str3).commit();
        return str2;
        label121: File localFile2 = new File(getStorageDirOnLowLevelPhone(paramContext));
        if (!localFile2.exists())
          localFile2.mkdirs();
        if ((!localFile2.isDirectory()) || (!localFile2.exists()))
          break;
        str1 = localFile2.getAbsolutePath();
        break;
      }
      File localFile1 = new File(str1);
      if (!localFile1.exists())
        localFile1.mkdirs();
      return str1;
      label203: str2 = str1;
    }
  }

  private static String getStorageDirOnLowLevelPhone(Context paramContext)
  {
    return "/sdcard/Android/data/" + paramContext.getPackageName() + "/files/storage/";
  }

  public static boolean isUsingExternalStorage(Context paramContext)
  {
    return "external".equals(paramContext.getSharedPreferences("LocalStorage", 0).getString("storage_type", null));
  }

  private static void registerKiller(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("LocalStorage", 0);
    if ("external".equals(localSharedPreferences.getString("storage_type", null)))
    {
      final boolean bool = "mounted".equals(Environment.getExternalStorageState());
      mExternalStorageReceiver = new BroadcastReceiver()
      {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
          boolean bool = "mounted".equals(Environment.getExternalStorageState());
          Uri localUri = paramAnonymousIntent.getData();
          if (localUri != null);
          for (String str = localUri.getPath(); ; str = null)
          {
            if ((str != null) && (this.val$storageDir.startsWith(str)) && (bool != bool))
            {
              Log.d("Tango", "External storage status changed, killing ourselfs.");
              Process.killProcess(Process.myPid());
            }
            return;
          }
        }
      };
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addDataScheme("file");
      localIntentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
      localIntentFilter.addAction("android.intent.action.MEDIA_REMOVED");
      localIntentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
      paramContext.registerReceiver(mExternalStorageReceiver, localIntentFilter);
    }
  }

  private static native void setDirs(String paramString1, String paramString2, String paramString3);

  public static boolean updateContext(Context paramContext)
  {
    if (paramContext.getFilesDir() != null)
    {
      String str1 = paramContext.getFilesDir().getAbsolutePath();
      String str2 = getCacheDir(paramContext);
      String str3 = getStorageDir(paramContext);
      Log.v("Tango", "Storage:" + str3);
      setDirs(str1, str2, str3);
      registerKiller(paramContext);
      return true;
    }
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.localstorage.LocalStorage
 * JD-Core Version:    0.6.2
 */