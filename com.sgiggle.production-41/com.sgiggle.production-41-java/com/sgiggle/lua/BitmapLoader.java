package com.sgiggle.lua;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;

public class BitmapLoader
{
  public static byte[] copyPixels(Bitmap paramBitmap)
  {
    ByteBuffer localByteBuffer = ByteBuffer.allocate(paramBitmap.getRowBytes() * paramBitmap.getHeight());
    paramBitmap.copyPixelsToBuffer(localByteBuffer);
    return localByteBuffer.array();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.lua.BitmapLoader
 * JD-Core Version:    0.6.2
 */