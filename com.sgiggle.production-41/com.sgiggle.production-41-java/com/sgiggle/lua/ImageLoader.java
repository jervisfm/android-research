package com.sgiggle.lua;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.Log;

public class ImageLoader
{
  private static final String TAG = "ImageLoader";

  public static Bitmap load(String paramString)
  {
    Log.v("ImageLoader", "load " + paramString);
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
    return BitmapFactory.decodeFile(paramString, localOptions);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.lua.ImageLoader
 * JD-Core Version:    0.6.2
 */