package com.sgiggle.lua;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.util.Log;

public class TextLoader
{
  private static final String TAG = "TextLoader";

  public static int getHeight(String paramString1, String paramString2, int paramInt)
  {
    Paint.FontMetrics localFontMetrics = getPaint(paramString2, paramInt).getFontMetrics();
    return (int)Math.ceil(localFontMetrics.bottom - localFontMetrics.top);
  }

  private static Paint getPaint(String paramString, int paramInt)
  {
    Paint localPaint = new Paint();
    localPaint.setAntiAlias(true);
    localPaint.setTextSize(paramInt);
    localPaint.setTextAlign(Paint.Align.CENTER);
    return localPaint;
  }

  public static int getWidth(String paramString1, String paramString2, int paramInt)
  {
    return (int)Math.ceil(getPaint(paramString2, paramInt).measureText(paramString1));
  }

  public static Bitmap load(String paramString1, String paramString2, int paramInt)
  {
    Log.v("TextLoader", "load " + paramString1 + " " + paramString2 + " " + paramInt);
    Paint localPaint = getPaint(paramString2, paramInt);
    int i = getWidth(paramString1, paramString2, paramInt);
    int j = getHeight(paramString1, paramString2, paramInt);
    Paint.FontMetrics localFontMetrics = localPaint.getFontMetrics();
    float f1 = i / 2;
    float f2 = j / 2 - (localFontMetrics.ascent + localFontMetrics.descent) / 2.0F;
    Bitmap localBitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ALPHA_8);
    Canvas localCanvas = new Canvas(localBitmap);
    localPaint.setColor(-1);
    localCanvas.drawText(paramString1, f1, f2, localPaint);
    return localBitmap;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.lua.TextLoader
 * JD-Core Version:    0.6.2
 */