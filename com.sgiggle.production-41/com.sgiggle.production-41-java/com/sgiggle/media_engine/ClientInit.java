package com.sgiggle.media_engine;

public final class ClientInit
{
  public static native void start();

  public static native void stop();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.media_engine.ClientInit
 * JD-Core Version:    0.6.2
 */