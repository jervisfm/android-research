package com.sgiggle.media_engine;

import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageFactory;
import com.sgiggle.messaging.SerializableMessage;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.AlertNumbersPayload;
import com.sgiggle.xmpp.SessionMessages.AnchorPayload;
import com.sgiggle.xmpp.SessionMessages.AnchorPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.AppLogEntriesPayload;
import com.sgiggle.xmpp.SessionMessages.AudioControlPayload;
import com.sgiggle.xmpp.SessionMessages.AudioControlPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.AudioModePayload;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.AvatarErrorPayload;
import com.sgiggle.xmpp.SessionMessages.AvatarInfo;
import com.sgiggle.xmpp.SessionMessages.AvatarInfo.Builder;
import com.sgiggle.xmpp.SessionMessages.AvatarRenderRequestPayload;
import com.sgiggle.xmpp.SessionMessages.BadgeInviteCountPayload;
import com.sgiggle.xmpp.SessionMessages.BandwidthPayload;
import com.sgiggle.xmpp.SessionMessages.BoolPayload;
import com.sgiggle.xmpp.SessionMessages.BoolPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.CallEntriesPayload;
import com.sgiggle.xmpp.SessionMessages.CallEntry;
import com.sgiggle.xmpp.SessionMessages.CallQualitySurveyDataPlayload;
import com.sgiggle.xmpp.SessionMessages.CallQualitySurveyDataPlayload.AgreeAnswerOptions;
import com.sgiggle.xmpp.SessionMessages.CallQualitySurveyDataPlayload.Builder;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.Contact.Builder;
import com.sgiggle.xmpp.SessionMessages.ContactDetailPayload;
import com.sgiggle.xmpp.SessionMessages.ContactDetailPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ContactDetailPayload.Source;
import com.sgiggle.xmpp.SessionMessages.ContactSearchPayload;
import com.sgiggle.xmpp.SessionMessages.ContactSearchResultPayload;
import com.sgiggle.xmpp.SessionMessages.ContactTangoCustomerSupportPayload;
import com.sgiggle.xmpp.SessionMessages.ContactTangoCustomerSupportPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ContactsPayload;
import com.sgiggle.xmpp.SessionMessages.ContactsPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ControlAnimationPayload;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage.Builder;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageNotificationReceivedPayload;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageNotificationReceivedPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ConversationMessagePayload;
import com.sgiggle.xmpp.SessionMessages.ConversationMessagePayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.ConversationPayload;
import com.sgiggle.xmpp.SessionMessages.ConversationPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ConversationSummaryItemPayload;
import com.sgiggle.xmpp.SessionMessages.ConversationSummaryListPayload;
import com.sgiggle.xmpp.SessionMessages.CountryCode;
import com.sgiggle.xmpp.SessionMessages.CountryCode.Builder;
import com.sgiggle.xmpp.SessionMessages.DeleteCallEntriesPayload;
import com.sgiggle.xmpp.SessionMessages.DeleteCallEntriesPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.DeleteVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.DeleteVideoMailPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.DeleteVideoMailPayload.Type;
import com.sgiggle.xmpp.SessionMessages.DeviceTokenType;
import com.sgiggle.xmpp.SessionMessages.DisplaySettingsPayload;
import com.sgiggle.xmpp.SessionMessages.DisplaySettingsPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.DisplayStorePayload;
import com.sgiggle.xmpp.SessionMessages.EmailVerificationPayload;
import com.sgiggle.xmpp.SessionMessages.FBDidLoginPayload;
import com.sgiggle.xmpp.SessionMessages.FBDidLoginPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.FinishPlayVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.FinishPlayVideoMailPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ForwardMessageResultPayload;
import com.sgiggle.xmpp.SessionMessages.GameErrorPayload;
import com.sgiggle.xmpp.SessionMessages.GameOffSessionPayload;
import com.sgiggle.xmpp.SessionMessages.GameOffSessionPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.InCallAlertPayload;
import com.sgiggle.xmpp.SessionMessages.InviteContactsSelectedPayload;
import com.sgiggle.xmpp.SessionMessages.InviteContactsSelectedPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.InviteEmailComposerPayload;
import com.sgiggle.xmpp.SessionMessages.InviteEmailSelectionPayload;
import com.sgiggle.xmpp.SessionMessages.InviteOptionsPayload;
import com.sgiggle.xmpp.SessionMessages.InviteSMSSelectedPayload;
import com.sgiggle.xmpp.SessionMessages.InviteSMSSelectedPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.InviteSMSSelectionPayload;
import com.sgiggle.xmpp.SessionMessages.InviteSendPayload;
import com.sgiggle.xmpp.SessionMessages.InviteSendPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.Invitee;
import com.sgiggle.xmpp.SessionMessages.KeepTangoPushNotificationAlivePayload;
import com.sgiggle.xmpp.SessionMessages.KeepTangoPushNotificationAlivePayload.Builder;
import com.sgiggle.xmpp.SessionMessages.KeyValuePair;
import com.sgiggle.xmpp.SessionMessages.LeaveVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.LeaveVideoMailPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.LoginCallPayload;
import com.sgiggle.xmpp.SessionMessages.LoginCallPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.LoginCompletedPayload;
import com.sgiggle.xmpp.SessionMessages.LoginPayload;
import com.sgiggle.xmpp.SessionMessages.LoginPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.MediaType;
import com.sgiggle.xmpp.SessionMessages.NotificationMessagePayload;
import com.sgiggle.xmpp.SessionMessages.NotificationMessagePayload.Builder;
import com.sgiggle.xmpp.SessionMessages.OperationErrorPayload;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.OtherRegisteredDevicePayload;
import com.sgiggle.xmpp.SessionMessages.OtherRegisteredDevicePayload.Builder;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber.Builder;
import com.sgiggle.xmpp.SessionMessages.PlayVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.PostCallContentPayload;
import com.sgiggle.xmpp.SessionMessages.PostCallContentPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.PostCallContentType;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogPayload;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogRequestPayload;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogRequestPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ProductDetailsPayload;
import com.sgiggle.xmpp.SessionMessages.ProductDetailsPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.PurchaseAttemtPayload;
import com.sgiggle.xmpp.SessionMessages.PurchaseAttemtPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.PurchasePayload;
import com.sgiggle.xmpp.SessionMessages.PurchasePayload.Builder;
import com.sgiggle.xmpp.SessionMessages.PurchasePayload.Type;
import com.sgiggle.xmpp.SessionMessages.PurchaseResultPayload;
import com.sgiggle.xmpp.SessionMessages.PushNotificationPayload;
import com.sgiggle.xmpp.SessionMessages.PushNotificationPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.PushNotificationType;
import com.sgiggle.xmpp.SessionMessages.PutAppInForegroundPayload;
import com.sgiggle.xmpp.SessionMessages.PutAppInForegroundPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.RecordVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.RegisterUserPayload;
import com.sgiggle.xmpp.SessionMessages.RegisterUserPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.RequestCallLogPayload;
import com.sgiggle.xmpp.SessionMessages.RequestCallLogPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.RetrieveOfflineMessageStatusPayload;
import com.sgiggle.xmpp.SessionMessages.SMSComposerPayload;
import com.sgiggle.xmpp.SessionMessages.SMSComposerPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.SMSComposerType;
import com.sgiggle.xmpp.SessionMessages.SMSVerificationPayload;
import com.sgiggle.xmpp.SessionMessages.SelectContactPayload;
import com.sgiggle.xmpp.SessionMessages.SelectContactPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.SelectContactRequestPayload;
import com.sgiggle.xmpp.SessionMessages.SelectContactRequestPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.SelectContactType;
import com.sgiggle.xmpp.SessionMessages.ShakeModePayload;
import com.sgiggle.xmpp.SessionMessages.ShakeModePayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ShowAdvertisementPayload;
import com.sgiggle.xmpp.SessionMessages.SnsAuthResultPayload;
import com.sgiggle.xmpp.SessionMessages.SnsAuthResultPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.SnsProcessResultPayload;
import com.sgiggle.xmpp.SessionMessages.StatsCollectorLogPayload;
import com.sgiggle.xmpp.SessionMessages.StatsCollectorLogPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.StopRecordingVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.StopRecordingVideoMailPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.StopRecordingVideoMailPayload.Type;
import com.sgiggle.xmpp.SessionMessages.StoreBadgeCountPayload;
import com.sgiggle.xmpp.SessionMessages.StringPayload;
import com.sgiggle.xmpp.SessionMessages.StringPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.SwapReadStatusesPayload;
import com.sgiggle.xmpp.SessionMessages.SwitchCameraPayload;
import com.sgiggle.xmpp.SessionMessages.SwitchCameraPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.TangoDeviceTokenPayload;
import com.sgiggle.xmpp.SessionMessages.TangoDeviceTokenPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.TestPayload;
import com.sgiggle.xmpp.SessionMessages.TestPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.UpdateAlertsPayload;
import com.sgiggle.xmpp.SessionMessages.UpdateConversationMessageNotificationPayload;
import com.sgiggle.xmpp.SessionMessages.UpdateRequiredPayload;
import com.sgiggle.xmpp.SessionMessages.UploadVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.UploadVideoMailPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.UploadVideoMailResponsePayload;
import com.sgiggle.xmpp.SessionMessages.VGoodErrorPayload;
import com.sgiggle.xmpp.SessionMessages.VGoodInitiatePayload;
import com.sgiggle.xmpp.SessionMessages.VGoodInitiatePayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ValidationCodeDeliveryPayload;
import com.sgiggle.xmpp.SessionMessages.ValidationCodeDeliveryPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.ValidationCodeDeliveryPayload.ValidationCodeDeliveryType;
import com.sgiggle.xmpp.SessionMessages.ValidationResultPayload;
import com.sgiggle.xmpp.SessionMessages.VideoMailConfigurationPayload;
import com.sgiggle.xmpp.SessionMessages.VideoMailDownloadURLPayload;
import com.sgiggle.xmpp.SessionMessages.VideoMailId;
import com.sgiggle.xmpp.SessionMessages.VideoMailId.Builder;
import com.sgiggle.xmpp.SessionMessages.VideoMailIdPayload;
import com.sgiggle.xmpp.SessionMessages.VideoMailIdPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.VideoMailNonTangoNotificationPayload;
import com.sgiggle.xmpp.SessionMessages.VideoMailNonTangoNotificationPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.VideoMailReceiversPayload;
import com.sgiggle.xmpp.SessionMessages.VideoMailReceiversPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.VideoMailWithCalleesPayload;
import com.sgiggle.xmpp.SessionMessages.VideoModePayload;
import com.sgiggle.xmpp.SessionMessages.WandLocationType;
import com.sgiggle.xmpp.SessionMessages.WandPressedPayload;
import com.sgiggle.xmpp.SessionMessages.WandPressedPayload.Builder;
import com.sgiggle.xmpp.SessionMessages.WebNavigationPayload;
import com.sgiggle.xmpp.SessionMessages.WelcomePayload;
import java.util.ArrayList;
import java.util.List;

public final class MediaEngineMessage
{
  public static final class AcceptCallMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public AcceptCallMessage()
    {
      super();
    }

    public AcceptCallMessage(String paramString)
    {
      super();
    }
  }

  public static final class AcknowledgeCallErrorMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public AcknowledgeCallErrorMessage()
    {
      super();
    }

    public AcknowledgeCallErrorMessage(String paramString)
    {
      super();
    }
  }

  public static final class AcknowledgeRegistrationErrorMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public AcknowledgeRegistrationErrorMessage()
    {
      super();
    }

    public AcknowledgeRegistrationErrorMessage(String paramString)
    {
      super();
    }
  }

  public static final class AddAvatarEvent extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public AddAvatarEvent()
    {
      super();
    }
  }

  public static final class AddAvatarMessage extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public AddAvatarMessage(long paramLong)
    {
      super();
    }
  }

  public static final class AddContactRequestMessage extends SerializableMessage<SessionMessages.ContactSearchPayload>
  {
    public AddContactRequestMessage()
    {
      super();
    }
  }

  public static final class AddFavoriteContactMessage extends SerializableMessage<SessionMessages.ContactsPayload>
  {
    public AddFavoriteContactMessage(List<SessionMessages.Contact> paramList)
    {
      super();
    }
  }

  public static final class AddVideoMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public AddVideoMessage()
    {
      super();
    }

    public AddVideoMessage(String paramString)
    {
      super();
    }
  }

  public static final class AdvertisementClickedMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public AdvertisementClickedMessage()
    {
      super();
    }
  }

  public static final class AdvertisementShownMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public AdvertisementShownMessage()
    {
      super();
    }
  }

  public static final class AllowAccessAddressBookMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public AllowAccessAddressBookMessage()
    {
      super();
    }
  }

  public static final class Audio2WayAvatarInProgressEvent extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public Audio2WayAvatarInProgressEvent()
    {
      super();
    }
  }

  public static final class AudioAvatarInProgressEvent extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public AudioAvatarInProgressEvent()
    {
      super();
    }
  }

  public static final class AudioControlMessage extends SerializableMessage<SessionMessages.AudioControlPayload>
  {
    public static final int AUDIO_CONTROL_MUTE = 2;
    public static final int AUDIO_CONTROL_SPEAKER = 1;

    public AudioControlMessage()
    {
      super();
    }

    protected AudioControlMessage(SessionMessages.AudioControlPayload paramAudioControlPayload)
    {
      super();
    }

    public AudioControlMessage(boolean paramBoolean1, boolean paramBoolean2)
    {
      super();
    }

    public static AudioControlMessage createMessage(int paramInt, boolean paramBoolean)
    {
      SessionMessages.AudioControlPayload.Builder localBuilder = SessionMessages.AudioControlPayload.newBuilder();
      localBuilder.setBase(makeBase(30033));
      if (paramInt == 1)
        localBuilder.setSpeakeron(paramBoolean);
      while (true)
      {
        return new AudioControlMessage(localBuilder.build());
        if (paramInt == 2)
          localBuilder.setMute(paramBoolean);
      }
    }
  }

  public static final class AudioInInitializationEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public AudioInInitializationEvent()
    {
      super();
    }
  }

  public static final class AudioInProgressEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public AudioInProgressEvent()
    {
      super();
    }
  }

  public static final class AudioModeChangedEvent extends SerializableMessage<SessionMessages.AudioModePayload>
  {
    public AudioModeChangedEvent()
    {
      super();
    }
  }

  public static final class AudioVideo2WayInProgressEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public AudioVideo2WayInProgressEvent()
    {
      super();
    }
  }

  public static final class AudioVideoInProgressEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public AudioVideoInProgressEvent()
    {
      super();
    }
  }

  public static final class AvatarBackgroundCleanupMessage extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public AvatarBackgroundCleanupMessage()
    {
      super();
    }
  }

  public static final class AvatarDemoAnimationTrackingMessage extends SerializableMessage<SessionMessages.VGoodInitiatePayload>
  {
    public AvatarDemoAnimationTrackingMessage(long paramLong, String paramString)
    {
      super();
    }
  }

  public static final class AvatarErrorEvent extends SerializableMessage<SessionMessages.AvatarErrorPayload>
  {
    public AvatarErrorEvent()
    {
      super();
    }
  }

  public static final class AvatarProductCatalogEvent extends SerializableMessage<SessionMessages.ProductCatalogPayload>
  {
    public AvatarProductCatalogEvent()
    {
      super();
    }
  }

  public static final class AvatarRenderRequestEvent extends SerializableMessage<SessionMessages.AvatarRenderRequestPayload>
  {
    public AvatarRenderRequestEvent()
    {
      super();
    }
  }

  public static final class BackToAvatarProductCatalogMessage extends SerializableMessage<SessionMessages.ProductCatalogRequestPayload>
  {
    public BackToAvatarProductCatalogMessage()
    {
      super();
    }
  }

  public static final class BackToVGoodProductCatalogMessage extends SerializableMessage<SessionMessages.ProductCatalogRequestPayload>
  {
    public BackToVGoodProductCatalogMessage()
    {
      super();
    }
  }

  public static final class BadgeInviteEvent extends SerializableMessage<SessionMessages.BadgeInviteCountPayload>
  {
    public BadgeInviteEvent()
    {
      super();
    }
  }

  public static final class BadgeStoreEvent extends SerializableMessage<SessionMessages.StoreBadgeCountPayload>
  {
    public BadgeStoreEvent()
    {
      super();
    }
  }

  public static final class CallDisconnectingEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public CallDisconnectingEvent()
    {
      super();
    }
  }

  public static final class CallErrorEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CallErrorEvent()
    {
      super();
    }
  }

  public static final class CallQualitySurveyDataMessage extends SerializableMessage<SessionMessages.CallQualitySurveyDataPlayload>
  {
    public CallQualitySurveyDataMessage(int paramInt, AgreeAnswerOptions paramAgreeAnswerOptions1, AgreeAnswerOptions paramAgreeAnswerOptions2, AgreeAnswerOptions paramAgreeAnswerOptions3, AgreeAnswerOptions paramAgreeAnswerOptions4, AgreeAnswerOptions paramAgreeAnswerOptions5, AgreeAnswerOptions paramAgreeAnswerOptions6, AgreeAnswerOptions paramAgreeAnswerOptions7, AgreeAnswerOptions paramAgreeAnswerOptions8, AgreeAnswerOptions paramAgreeAnswerOptions9, String paramString)
    {
      super();
    }

    private static SessionMessages.CallQualitySurveyDataPlayload.AgreeAnswerOptions MsgEnum2PayloadEnum(AgreeAnswerOptions paramAgreeAnswerOptions)
    {
      switch (MediaEngineMessage.1.$SwitchMap$com$sgiggle$media_engine$MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions[paramAgreeAnswerOptions.ordinal()])
      {
      default:
        throw new RuntimeException("Unexpected value of AgreeAnswerOptions: " + paramAgreeAnswerOptions);
      case 1:
        return SessionMessages.CallQualitySurveyDataPlayload.AgreeAnswerOptions.UNANSWERED;
      case 2:
        return SessionMessages.CallQualitySurveyDataPlayload.AgreeAnswerOptions.DISAGREE;
      case 3:
        return SessionMessages.CallQualitySurveyDataPlayload.AgreeAnswerOptions.AGREE;
      case 4:
      }
      return SessionMessages.CallQualitySurveyDataPlayload.AgreeAnswerOptions.NOT_SURE;
    }

    public static enum AgreeAnswerOptions
    {
      static
      {
        DISAGREE = new AgreeAnswerOptions("DISAGREE", 1);
        AGREE = new AgreeAnswerOptions("AGREE", 2);
        NOT_SURE = new AgreeAnswerOptions("NOT_SURE", 3);
        AgreeAnswerOptions[] arrayOfAgreeAnswerOptions = new AgreeAnswerOptions[4];
        arrayOfAgreeAnswerOptions[0] = UNANSWERED;
        arrayOfAgreeAnswerOptions[1] = DISAGREE;
        arrayOfAgreeAnswerOptions[2] = AGREE;
        arrayOfAgreeAnswerOptions[3] = NOT_SURE;
      }
    }
  }

  public static final class CallReceivedEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public CallReceivedEvent()
    {
      super();
    }
  }

  public static final class CalleeMissedCallEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public CalleeMissedCallEvent()
    {
      super();
    }
  }

  public static final class CancelAppStoreMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelAppStoreMessage()
    {
      super();
    }
  }

  public static final class CancelCallQualitySurveyMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelCallQualitySurveyMessage()
    {
      super();
    }
  }

  public static final class CancelChoosePictureEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelChoosePictureEvent()
    {
      super();
    }
  }

  public static final class CancelChoosePictureMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelChoosePictureMessage()
    {
      super();
    }
  }

  public static final class CancelContactDetailMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelContactDetailMessage()
    {
      super();
    }
  }

  public static final class CancelFacebookLikeMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelFacebookLikeMessage()
    {
      super();
    }
  }

  public static final class CancelGameCatalogMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelGameCatalogMessage()
    {
      super();
    }
  }

  public static final class CancelGameDemoMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelGameDemoMessage()
    {
      super();
    }
  }

  public static final class CancelInAppPurchaseMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelInAppPurchaseMessage()
    {
      super();
    }
  }

  public static final class CancelPostCallMessage extends SerializableMessage<SessionMessages.PostCallContentPayload>
  {
    public CancelPostCallMessage()
    {
      super();
    }
  }

  public static final class CancelPostProcessPictureEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelPostProcessPictureEvent()
    {
      super();
    }
  }

  public static final class CancelPostProcessPictureMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelPostProcessPictureMessage()
    {
      super();
    }
  }

  public static final class CancelSelectContactMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelSelectContactMessage()
    {
      super();
    }
  }

  public static final class CancelSgiggleInvitationMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public CancelSgiggleInvitationMessage()
    {
      super();
    }

    public CancelSgiggleInvitationMessage(String paramString)
    {
      super();
    }
  }

  public static final class CancelTakePictureEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelTakePictureEvent()
    {
      super();
    }
  }

  public static final class CancelTakePictureMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelTakePictureMessage()
    {
      super();
    }
  }

  public static final class CancelUploadVideoMailMessage extends SerializableMessage<SessionMessages.VideoMailIdPayload>
  {
    public CancelUploadVideoMailMessage(String paramString)
    {
      super();
    }
  }

  public static final class CancelVideoMailReceiversSelectionMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelVideoMailReceiversSelectionMessage()
    {
      super();
    }
  }

  public static final class CancelViewPictureEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelViewPictureEvent()
    {
      super();
    }
  }

  public static final class CancelViewPictureMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CancelViewPictureMessage()
    {
      super();
    }
  }

  public static final class ChoosePictureEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ChoosePictureEvent()
    {
      super();
    }
  }

  public static final class ChoosePictureMessage extends SerializableMessage<SessionMessages.AnchorPayload>
  {
    public ChoosePictureMessage()
    {
      super();
    }
  }

  public static final class ClearMissedCallMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public ClearMissedCallMessage()
    {
      super();
    }

    public ClearMissedCallMessage(String paramString)
    {
      super();
    }
  }

  public static final class ClearUnreadMissedCallNumberMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ClearUnreadMissedCallNumberMessage()
    {
      super();
    }
  }

  public static final class CloseConversationMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public CloseConversationMessage()
    {
      super();
    }
  }

  public static final class ComposingConversationMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ComposingConversationMessage()
    {
      super();
    }
  }

  public static final class ConfirmVideoMailNonTangoNotificationMessage extends SerializableMessage<SessionMessages.VideoMailNonTangoNotificationPayload>
  {
    public ConfirmVideoMailNonTangoNotificationMessage()
    {
      super();
    }

    public ConfirmVideoMailNonTangoNotificationMessage(String paramString1, String paramString2, List<SessionMessages.Contact> paramList, boolean paramBoolean)
    {
      super();
    }

    public ConfirmVideoMailNonTangoNotificationMessage(String paramString, List<SessionMessages.Contact> paramList, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class ContactSearchCancelAddMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ContactSearchCancelAddMessage()
    {
      super();
    }
  }

  public static final class ContactSearchRequestMessage extends SerializableMessage<SessionMessages.ContactSearchPayload>
  {
    public ContactSearchRequestMessage()
    {
      super();
    }
  }

  public static final class ContactTangoCustomerSupportEvent extends SerializableMessage<SessionMessages.ContactTangoCustomerSupportPayload>
  {
    public ContactTangoCustomerSupportEvent()
    {
      super();
    }

    public ContactTangoCustomerSupportEvent(String paramString)
    {
      super();
    }
  }

  public static final class ContactTangoCustomerSupportMessage extends SerializableMessage<SessionMessages.ContactTangoCustomerSupportPayload>
  {
    public ContactTangoCustomerSupportMessage()
    {
      super();
    }

    public ContactTangoCustomerSupportMessage(String paramString)
    {
      super();
    }
  }

  public static final class ContactsDisplayMainEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ContactsDisplayMainEvent()
    {
      super();
    }
  }

  public static final class ContactsDisplayMainMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ContactsDisplayMainMessage()
    {
      super();
    }
  }

  public static final class ConversationMessageNotificationReceivedMessage extends SerializableMessage<SessionMessages.ConversationMessageNotificationReceivedPayload>
  {
    public ConversationMessageNotificationReceivedMessage()
    {
      super();
    }

    public ConversationMessageNotificationReceivedMessage(int paramInt)
    {
      super();
    }

    public ConversationMessageNotificationReceivedMessage(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4)
    {
      super();
    }
  }

  public static final class ConversationMessageSendStatusEvent extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public ConversationMessageSendStatusEvent()
    {
      super();
    }
  }

  public static final class DeleteCallLogMessage extends SerializableMessage<SessionMessages.DeleteCallEntriesPayload>
  {
    public DeleteCallLogMessage()
    {
      super();
    }

    public DeleteCallLogMessage(SessionMessages.CallEntry paramCallEntry)
    {
      super();
    }
  }

  public static final class DeleteConversationEvent extends SerializableMessage<SessionMessages.StringPayload>
  {
    public DeleteConversationEvent()
    {
      super();
    }
  }

  public static final class DeleteConversationMessage extends SerializableMessage<SessionMessages.StringPayload>
  {
    public DeleteConversationMessage()
    {
      super();
    }

    public DeleteConversationMessage(String paramString)
    {
      super();
    }
  }

  public static final class DeleteSingleConversationMessageEvent extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public DeleteSingleConversationMessageEvent()
    {
      super();
    }
  }

  public static final class DeleteSingleConversationMessageMessage extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public DeleteSingleConversationMessageMessage()
    {
      super();
    }

    public DeleteSingleConversationMessageMessage(SessionMessages.ConversationMessage paramConversationMessage)
    {
      super();
    }
  }

  public static final class DeleteVideoMailMessage extends SerializableMessage<SessionMessages.DeleteVideoMailPayload>
  {
    public DeleteVideoMailMessage(String paramString1, String paramString2)
    {
      super();
    }
  }

  public static final class DisablePostCallMessage extends SerializableMessage<SessionMessages.PostCallContentPayload>
  {
    public DisablePostCallMessage(SessionMessages.PostCallContentType paramPostCallContentType)
    {
      super();
    }
  }

  public static final class DismissStoreBadgeMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DismissStoreBadgeMessage()
    {
      super();
    }

    public DismissStoreBadgeMessage(String paramString)
    {
      super();
    }
  }

  public static final class DismissUploadVideoMailFinishedMessage extends SerializableMessage<SessionMessages.VideoMailIdPayload>
  {
    public DismissUploadVideoMailFinishedMessage()
    {
      super();
    }

    public DismissUploadVideoMailFinishedMessage(String paramString)
    {
      super();
    }
  }

  public static final class DismissVerificationWithOtherDeviceMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DismissVerificationWithOtherDeviceMessage()
    {
      super();
    }
  }

  public static final class DisplayAlertNumbersEvent extends SerializableMessage<SessionMessages.AlertNumbersPayload>
  {
    public DisplayAlertNumbersEvent()
    {
      super();
    }
  }

  public static final class DisplayAnimationEvent extends SerializableMessage<SessionMessages.ControlAnimationPayload>
  {
    public DisplayAnimationEvent()
    {
      super();
    }
  }

  public static final class DisplayAppLogEvent extends SerializableMessage<SessionMessages.AppLogEntriesPayload>
  {
    public DisplayAppLogEvent()
    {
      super();
    }
  }

  public static final class DisplayAppStoreEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DisplayAppStoreEvent()
    {
      super();
    }
  }

  public static final class DisplayAvatarEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DisplayAvatarEvent()
    {
      super();
    }
  }

  public static final class DisplayAvatarPaymentMessage extends SerializableMessage<SessionMessages.ProductCatalogRequestPayload>
  {
    public DisplayAvatarPaymentMessage()
    {
      super();
    }
  }

  public static final class DisplayAvatarProductDetailEvent extends SerializableMessage<SessionMessages.ProductDetailsPayload>
  {
    public DisplayAvatarProductDetailEvent()
    {
      super();
    }
  }

  public static final class DisplayAvatarProductDetailMessage extends SerializableMessage<SessionMessages.ProductDetailsPayload>
  {
    public DisplayAvatarProductDetailMessage()
    {
      super();
    }

    public DisplayAvatarProductDetailMessage(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
    {
      super();
    }
  }

  public static final class DisplayCallLogEvent extends SerializableMessage<SessionMessages.CallEntriesPayload>
  {
    public DisplayCallLogEvent()
    {
      super();
    }
  }

  public static final class DisplayCallQualitySurveyEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DisplayCallQualitySurveyEvent()
    {
      super();
    }
  }

  public static final class DisplayContactDetailEvent extends SerializableMessage<SessionMessages.ContactDetailPayload>
  {
    public DisplayContactDetailEvent()
    {
      super();
    }
  }

  public static final class DisplayConversationMessageEvent extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public DisplayConversationMessageEvent()
    {
      super();
    }
  }

  public static final class DisplayDemoAvatarEvent extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public DisplayDemoAvatarEvent()
    {
      super();
    }
  }

  public static final class DisplayFAQEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DisplayFAQEvent()
    {
      super();
    }
  }

  public static final class DisplayFacebookLikeEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DisplayFacebookLikeEvent()
    {
      super();
    }
  }

  public static final class DisplayGameCatalogEvent extends SerializableMessage<SessionMessages.ProductCatalogPayload>
  {
    public DisplayGameCatalogEvent()
    {
      super();
    }
  }

  public static final class DisplayGameCatalogMessage extends SerializableMessage<SessionMessages.ProductCatalogRequestPayload>
  {
    public DisplayGameCatalogMessage()
    {
      super();
    }
  }

  public static final class DisplayGameDemoEvent extends SerializableMessage<SessionMessages.ProductDetailsPayload>
  {
    public DisplayGameDemoEvent()
    {
      super();
    }
  }

  public static final class DisplayGameDemoMessage extends SerializableMessage<SessionMessages.ProductDetailsPayload>
  {
    public DisplayGameDemoMessage(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
    {
      super();
    }
  }

  public static final class DisplayGameInCallEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public DisplayGameInCallEvent()
    {
      super();
    }
  }

  public static final class DisplayInviteContactEvent extends SerializableMessage<SessionMessages.ContactsPayload>
  {
    public DisplayInviteContactEvent()
    {
      super();
    }

    public DisplayInviteContactEvent(List<SessionMessages.Contact> paramList)
    {
      super();
    }
  }

  public static final class DisplayMessageNotificationEvent extends SerializableMessage<SessionMessages.NotificationMessagePayload>
  {
    public DisplayMessageNotificationEvent()
    {
      super();
    }
  }

  public static final class DisplayPersonalInfoMessage extends SerializableMessage<SessionMessages.RegisterUserPayload>
  {
    public DisplayPersonalInfoMessage()
    {
      super();
    }

    public DisplayPersonalInfoMessage(String paramString)
    {
      super();
    }
  }

  public static final class DisplayPostCallScreenEvent extends SerializableMessage<SessionMessages.PostCallContentPayload>
  {
    public DisplayPostCallScreenEvent()
    {
      super();
    }
  }

  public static final class DisplayProductCatalogEvent extends SerializableMessage<SessionMessages.ProductCatalogPayload>
  {
    public DisplayProductCatalogEvent()
    {
      super();
    }
  }

  public static final class DisplayProductDetailEvent extends SerializableMessage<SessionMessages.ProductDetailsPayload>
  {
    public DisplayProductDetailEvent()
    {
      super();
    }
  }

  public static final class DisplayProductDetailMessage extends SerializableMessage<SessionMessages.ProductDetailsPayload>
  {
    public DisplayProductDetailMessage()
    {
      super();
    }

    public DisplayProductDetailMessage(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
    {
      super();
    }
  }

  public static final class DisplayProductInfoEvent extends SerializableMessage<SessionMessages.WelcomePayload>
  {
    public DisplayProductInfoEvent()
    {
      super();
    }
  }

  public static final class DisplayRegisterUserEvent extends SerializableMessage<SessionMessages.RegisterUserPayload>
  {
    public DisplayRegisterUserEvent()
    {
      super();
    }
  }

  public static final class DisplaySMSSentEvent extends SerializableMessage<SessionMessages.StringPayload>
  {
    public DisplaySMSSentEvent()
    {
      super();
    }
  }

  public static final class DisplaySettingsEvent extends SerializableMessage<SessionMessages.RegisterUserPayload>
  {
    public DisplaySettingsEvent()
    {
      super();
    }
  }

  public static final class DisplaySettingsMessage extends SerializableMessage<SessionMessages.DisplaySettingsPayload>
  {
    public DisplaySettingsMessage()
    {
      super();
    }

    public DisplaySettingsMessage(SessionMessages.OperationalAlert paramOperationalAlert)
    {
      super();
    }
  }

  public static final class DisplayStoreEvent extends SerializableMessage<SessionMessages.DisplayStorePayload>
  {
    public DisplayStoreEvent()
    {
      super();
    }
  }

  public static final class DisplayStoreMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DisplayStoreMessage()
    {
      super();
    }
  }

  public static final class DisplaySupportWebsiteEvent extends SerializableMessage<SessionMessages.WebNavigationPayload>
  {
    public DisplaySupportWebsiteEvent()
    {
      super();
    }
  }

  public static final class DisplayVgoodEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DisplayVgoodEvent()
    {
      super();
    }
  }

  public static final class DisplayVgoodPaymentMessage extends SerializableMessage<SessionMessages.ProductCatalogRequestPayload>
  {
    public DisplayVgoodPaymentMessage()
    {
      super();
    }
  }

  public static final class DisplayVideoMailNonTangoNotificationEvent extends SerializableMessage<SessionMessages.VideoMailNonTangoNotificationPayload>
  {
    public DisplayVideoMailNonTangoNotificationEvent()
    {
      super();
    }
  }

  public static final class DisplayVideoMailReceiversEvent extends SerializableMessage<SessionMessages.VideoMailReceiversPayload>
  {
    public DisplayVideoMailReceiversEvent()
    {
      super();
    }
  }

  public static final class DisplayWelcomeScreenMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public DisplayWelcomeScreenMessage()
    {
      super();
    }
  }

  public static final class EditVideoMailMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public EditVideoMailMessage()
    {
      super();
    }
  }

  public static final class EmailContactSearchEvent extends SerializableMessage<SessionMessages.ContactSearchPayload>
  {
    public EmailContactSearchEvent()
    {
      super();
    }
  }

  public static final class EmailContactSearchMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public EmailContactSearchMessage()
    {
      super();
    }
  }

  public static final class EmailContactSearchResultEvent extends SerializableMessage<SessionMessages.ContactSearchResultPayload>
  {
    public EmailContactSearchResultEvent()
    {
      super();
    }
  }

  public static final class EmailValidationRequiredEvent extends SerializableMessage<SessionMessages.EmailVerificationPayload>
  {
    public EmailValidationRequiredEvent()
    {
      super();
    }
  }

  public static final class EmailValidationWrongCodeEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public EmailValidationWrongCodeEvent()
    {
      super();
    }
  }

  public static final class EndStateNoChangeMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public EndStateNoChangeMessage()
    {
      super();
    }

    public EndStateNoChangeMessage(String paramString)
    {
      super();
    }
  }

  public static final class EnterBackgroundEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public EnterBackgroundEvent()
    {
      super();
    }
  }

  public static final class FBDidLoginEvent extends SerializableMessage<SessionMessages.RegisterUserPayload>
  {
    public FBDidLoginEvent()
    {
      super();
    }
  }

  public static final class FBDidLoginMessage extends SerializableMessage<SessionMessages.FBDidLoginPayload>
  {
    public FBDidLoginMessage()
    {
      super();
    }

    public FBDidLoginMessage(String paramString, long paramLong)
    {
      super();
    }
  }

  public static final class FinishEditVideoMailMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public FinishEditVideoMailMessage()
    {
      super();
    }
  }

  public static final class FinishPlayGameDemoMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public FinishPlayGameDemoMessage()
    {
      super();
    }
  }

  public static final class FinishPlayVideoMailEvent extends SerializableMessage<SessionMessages.VideoMailIdPayload>
  {
    public FinishPlayVideoMailEvent()
    {
      super();
    }
  }

  public static final class FinishPlayVideoMailMessage extends SerializableMessage<SessionMessages.FinishPlayVideoMailPayload>
  {
    public FinishPlayVideoMailMessage(String paramString1, String paramString2, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class FinishPlayVideoMessageMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public FinishPlayVideoMessageMessage()
    {
      super();
    }
  }

  public static final class FinishSMSComposeMessage extends SerializableMessage<SessionMessages.BoolPayload>
  {
    public FinishSMSComposeMessage(boolean paramBoolean)
    {
      super();
    }
  }

  public static final class FinishUploadVideoMailMessage extends SerializableMessage<SessionMessages.VideoMailIdPayload>
  {
    public FinishUploadVideoMailMessage(String paramString)
    {
      super();
    }
  }

  public static final class FinishVideoRingbackMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public FinishVideoRingbackMessage()
    {
      super();
    }
  }

  public static final class ForwardMessageRequestMessage extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public ForwardMessageRequestMessage(SessionMessages.ConversationMessage paramConversationMessage)
    {
      super();
    }
  }

  public static final class ForwardMessageResultEvent extends SerializableMessage<SessionMessages.ForwardMessageResultPayload>
  {
    public ForwardMessageResultEvent()
    {
      super();
    }
  }

  public static final class ForwardToPostCallContentMessage extends SerializableMessage<SessionMessages.PostCallContentPayload>
  {
    public ForwardToPostCallContentMessage()
    {
      this(SessionMessages.PostCallContentType.POSTCALL_NONE);
    }

    public ForwardToPostCallContentMessage(SessionMessages.PostCallContentType paramPostCallContentType)
    {
      super();
    }
  }

  public static final class ForwardVideoMailFinishedEvent extends SerializableMessage<SessionMessages.VideoMailWithCalleesPayload>
  {
    public ForwardVideoMailFinishedEvent()
    {
      super();
    }
  }

  public static final class GameErrorEvent extends SerializableMessage<SessionMessages.GameErrorPayload>
  {
    public GameErrorEvent()
    {
      super();
    }
  }

  public static final class GameModeOffMessage extends SerializableMessage<SessionMessages.GameOffSessionPayload>
  {
    public GameModeOffMessage(boolean paramBoolean)
    {
      super();
    }
  }

  public static final class GameModeOnMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public GameModeOnMessage()
    {
      super();
    }
  }

  public static final class InCallAlertEvent extends SerializableMessage<SessionMessages.InCallAlertPayload>
  {
    public InCallAlertEvent()
    {
      super();
    }
  }

  public static final class InitiateVGoodMessage extends SerializableMessage<SessionMessages.VGoodInitiatePayload>
  {
    public InitiateVGoodMessage()
    {
      super();
    }

    public InitiateVGoodMessage(Integer paramInteger)
    {
      super();
    }
  }

  public static final class InviteContactMessage extends SerializableMessage<SessionMessages.ContactsPayload>
  {
    public InviteContactMessage()
    {
      super();
    }

    public InviteContactMessage(List<SessionMessages.Contact> paramList)
    {
      super();
    }
  }

  public static final class InviteDisplayMainEvent extends SerializableMessage<SessionMessages.InviteOptionsPayload>
  {
    public InviteDisplayMainEvent()
    {
      super();
    }
  }

  public static final class InviteDisplayMainMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public InviteDisplayMainMessage()
    {
      super();
    }
  }

  public static final class InviteEmailComposerEvent extends SerializableMessage<SessionMessages.InviteEmailComposerPayload>
  {
    public InviteEmailComposerEvent()
    {
      super();
    }
  }

  public static final class InviteEmailComposerMessage extends SerializableMessage<SessionMessages.InviteContactsSelectedPayload>
  {
    public InviteEmailComposerMessage()
    {
      super();
    }

    public InviteEmailComposerMessage(List<SessionMessages.Invitee> paramList)
    {
      super();
    }
  }

  public static final class InviteEmailSelectionEvent extends SerializableMessage<SessionMessages.InviteEmailSelectionPayload>
  {
    public InviteEmailSelectionEvent()
    {
      super();
    }
  }

  public static final class InviteEmailSelectionMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public InviteEmailSelectionMessage()
    {
      super();
    }
  }

  public static final class InviteEmailSendMessage extends SerializableMessage<SessionMessages.InviteSendPayload>
  {
    public InviteEmailSendMessage()
    {
      super();
    }

    public InviteEmailSendMessage(List<SessionMessages.Invitee> paramList, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class InviteRecommendedSelectedMessage extends SerializableMessage<SessionMessages.InviteSMSSelectedPayload>
  {
    public InviteRecommendedSelectedMessage()
    {
      super();
    }

    public InviteRecommendedSelectedMessage(List<SessionMessages.Contact> paramList, String paramString)
    {
      super();
    }

    protected static SessionMessages.InviteSMSSelectedPayload createPayload(List<SessionMessages.Contact> paramList, String paramString)
    {
      SessionMessages.InviteSMSSelectedPayload.Builder localBuilder = SessionMessages.InviteSMSSelectedPayload.newBuilder();
      localBuilder.setBase(makeBase(30069)).addAllContact(paramList);
      if (paramString != null)
        localBuilder.setRecommendationAlgorithm(paramString);
      return localBuilder.build();
    }
  }

  public static final class InviteRecommendedSelectionEvent extends SerializableMessage<SessionMessages.InviteSMSSelectionPayload>
  {
    public InviteRecommendedSelectionEvent()
    {
      super();
    }
  }

  public static final class InviteRecommendedSelectionMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public InviteRecommendedSelectionMessage()
    {
      super();
    }
  }

  public static final class InviteSMSInstructionEvent extends SerializableMessage<SessionMessages.InviteSMSSelectedPayload>
  {
    public InviteSMSInstructionEvent()
    {
      super();
    }
  }

  public static final class InviteSMSSelectedMessage extends SerializableMessage<SessionMessages.InviteSMSSelectedPayload>
  {
    public InviteSMSSelectedMessage()
    {
      super();
    }

    public InviteSMSSelectedMessage(List<SessionMessages.Contact> paramList)
    {
      super();
    }
  }

  public static final class InviteSMSSelectionEvent extends SerializableMessage<SessionMessages.InviteSMSSelectionPayload>
  {
    public InviteSMSSelectionEvent()
    {
      super();
    }
  }

  public static final class InviteSMSSelectionMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public InviteSMSSelectionMessage()
    {
      super();
    }
  }

  public static final class InviteSMSSendMessage extends SerializableMessage<SessionMessages.InviteSendPayload>
  {
    public InviteSMSSendMessage()
    {
      super();
    }

    public InviteSMSSendMessage(List<SessionMessages.Invitee> paramList, String paramString, boolean paramBoolean)
    {
      super();
    }

    public InviteSMSSendMessage(List<SessionMessages.Invitee> paramList, boolean paramBoolean)
    {
      this(paramList, null, paramBoolean);
    }

    protected static SessionMessages.InviteSendPayload createPayload(List<SessionMessages.Invitee> paramList, String paramString, boolean paramBoolean)
    {
      SessionMessages.InviteSendPayload.Builder localBuilder = SessionMessages.InviteSendPayload.newBuilder();
      localBuilder.setBase(makeBase(30070)).addAllInvitee(paramList).setSuccess(paramBoolean);
      if (paramString != null)
        localBuilder.setRecommendationAlgorithm(paramString);
      return localBuilder.build();
    }
  }

  public static final class InviteSnsComposeEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public InviteSnsComposeEvent()
    {
      super();
    }
  }

  public static final class InviteSnsPublishMessage extends SerializableMessage<SessionMessages.StringPayload>
  {
    public InviteSnsPublishMessage(String paramString)
    {
      super();
    }
  }

  public static final class InviteSnsPublishResultEvent extends SerializableMessage<SessionMessages.SnsProcessResultPayload>
  {
    public InviteSnsPublishResultEvent()
    {
      super();
    }
  }

  public static final class InviteViaSnsMessage extends SerializableMessage<SessionMessages.StringPayload>
  {
    public InviteViaSnsMessage(String paramString)
    {
      super();
    }
  }

  public static final class KeepTangoPushNotificationAliveMessage extends SerializableMessage<SessionMessages.KeepTangoPushNotificationAlivePayload>
  {
    public KeepTangoPushNotificationAliveMessage()
    {
      super();
    }
  }

  public static final class LaunchLogReportEmailEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public LaunchLogReportEmailEvent()
    {
      super();
    }
  }

  public static final class LaunchLogReportEmailMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public LaunchLogReportEmailMessage()
    {
      super();
    }
  }

  public static final class LeaveVideoMailMessage extends SerializableMessage<SessionMessages.LeaveVideoMailPayload>
  {
    public LeaveVideoMailMessage()
    {
      super();
    }

    public LeaveVideoMailMessage(SessionMessages.Contact paramContact, boolean paramBoolean)
    {
      super();
    }

    public LeaveVideoMailMessage(List<SessionMessages.Contact> paramList, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class LikeVideoRingbackMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public LikeVideoRingbackMessage()
    {
      super();
    }
  }

  public static final class LockedVGoodSelectedMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public LockedVGoodSelectedMessage()
    {
      super();
    }
  }

  public static final class LoginCompletedEvent extends SerializableMessage<SessionMessages.LoginCompletedPayload>
  {
    public LoginCompletedEvent()
    {
      super();
    }
  }

  public static final class LoginErrorEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public LoginErrorEvent()
    {
      super();
    }

    public LoginErrorEvent(String paramString)
    {
      super();
    }
  }

  public static final class LoginEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public LoginEvent()
    {
      super();
    }
  }

  public static final class LoginMessage extends SerializableMessage<SessionMessages.LoginPayload>
  {
    public LoginMessage()
    {
      super();
    }
  }

  public static final class LoginNotificationEvent extends SerializableMessage<SessionMessages.LoginPayload>
  {
    public LoginNotificationEvent()
    {
      super();
    }
  }

  public static final class LoginNotificationMessage extends SerializableMessage<SessionMessages.LoginPayload>
  {
    public LoginNotificationMessage()
    {
      super();
    }

    public LoginNotificationMessage(String paramString)
    {
      super();
    }
  }

  public static final class LoginNotificationUserAcceptedEvent extends SerializableMessage<SessionMessages.LoginPayload>
  {
    public LoginNotificationUserAcceptedEvent()
    {
      super();
    }
  }

  public static final class LoginNotificationUserAcceptedMessage extends SerializableMessage<SessionMessages.LoginPayload>
  {
    public LoginNotificationUserAcceptedMessage()
    {
      super();
    }

    public LoginNotificationUserAcceptedMessage(String paramString)
    {
      super();
    }
  }

  public static final class MakeCallMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public MakeCallMessage()
    {
      super();
    }

    public MakeCallMessage(String paramString1, String paramString2)
    {
      this(paramString1, paramString2, false);
    }

    public MakeCallMessage(String paramString1, String paramString2, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class MakePremiumCallEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public MakePremiumCallEvent()
    {
      super();
    }
  }

  public static final class MakePremiumCallMessage extends SerializableMessage<SessionMessages.LoginCallPayload>
  {
    public MakePremiumCallMessage()
    {
      super();
    }

    public MakePremiumCallMessage(String paramString)
    {
      super();
    }
  }

  public static final class MediaEngineMessageFactory
    implements MessageFactory
  {
    private static final String TAG = "MediaEngineMessageFactory";

    public Message create(int paramInt)
    {
      if ((paramInt < 30000) || (paramInt > 39999))
        return null;
      switch (paramInt)
      {
      default:
        Log.w("MediaEngineMessageFactory", "create(): message-id = [" + paramInt + "] not supported.");
        return null;
      case 30005:
        return new MediaEngineMessage.LoginMessage();
      case 30100:
        return new MediaEngineMessage.TestMessage();
      case 30001:
        return new MediaEngineMessage.LoginNotificationUserAcceptedMessage();
      case 30002:
        return new MediaEngineMessage.LoginNotificationMessage();
      case 30004:
        return new MediaEngineMessage.ReceivePushNotificationMessage();
      case 30003:
        return new MediaEngineMessage.MakePremiumCallMessage();
      case 30006:
        return new MediaEngineMessage.PutAppInBackgroundMessage();
      case 30007:
        return new MediaEngineMessage.MakeCallMessage();
      case 30009:
        return new MediaEngineMessage.RejectCallMessage();
      case 30011:
        return new MediaEngineMessage.TerminateCallMessage();
      case 30019:
        return new MediaEngineMessage.AcceptCallMessage();
      case 30017:
        return new MediaEngineMessage.SendSgiggleInvitationMessage();
      case 30023:
        return new MediaEngineMessage.CancelSgiggleInvitationMessage();
      case 30013:
        return new MediaEngineMessage.AddVideoMessage();
      case 30015:
        return new MediaEngineMessage.RemoveVideoMessage();
      case 30039:
        return new MediaEngineMessage.InitiateVGoodMessage();
      case 30043:
        return new MediaEngineMessage.RegisterUserMessage();
      case 30049:
        return new MediaEngineMessage.InviteDisplayMainMessage();
      case 30051:
        return new MediaEngineMessage.InviteEmailSelectionMessage();
      case 30053:
        return new MediaEngineMessage.InviteEmailSendMessage();
      case 30065:
        return new MediaEngineMessage.InviteSMSSelectionMessage();
      case 30067:
        return new MediaEngineMessage.InviteSMSSelectedMessage();
      case 30068:
        return new MediaEngineMessage.InviteRecommendedSelectionMessage();
      case 30069:
        return new MediaEngineMessage.InviteRecommendedSelectedMessage();
      case 30055:
        return new MediaEngineMessage.ContactsDisplayMainMessage();
      case 30057:
        return new MediaEngineMessage.DisplaySettingsMessage();
      case 30059:
        return new MediaEngineMessage.AllowAccessAddressBookMessage();
      case 30061:
        return new MediaEngineMessage.InviteEmailComposerMessage();
      case 30021:
        return new MediaEngineMessage.SavePersonalInfoMessage();
      case 30033:
        return new MediaEngineMessage.AudioControlMessage();
      case 30025:
        return new MediaEngineMessage.AcknowledgeCallErrorMessage();
      case 30027:
        return new MediaEngineMessage.EndStateNoChangeMessage();
      case 30029:
        return new MediaEngineMessage.DisplayPersonalInfoMessage();
      case 30063:
        return new MediaEngineMessage.ValidationRequestMessage();
      case 30071:
        return new MediaEngineMessage.RequestFAQMessage();
      case 30083:
        return new MediaEngineMessage.SendValidationCodeMessage();
      case 30077:
        return new MediaEngineMessage.SwitchCameraMessage();
      case 30079:
        return new MediaEngineMessage.ClearMissedCallMessage();
      case 30081:
        return new MediaEngineMessage.RequestFilterContactMessage();
      case 30086:
        return new MediaEngineMessage.PutAppInForegroundMessage();
      case 30087:
        return new MediaEngineMessage.KeepTangoPushNotificationAliveMessage();
      case 30088:
        return new MediaEngineMessage.TangoDeviceTokenMessage();
      case 30090:
        return new MediaEngineMessage.InviteContactMessage();
      case 30091:
        return new MediaEngineMessage.RequestCallLogMessage();
      case 30092:
        return new MediaEngineMessage.DeleteCallLogMessage();
      case 30089:
        return new MediaEngineMessage.AcknowledgeRegistrationErrorMessage();
      case 30036:
        return new MediaEngineMessage.LaunchLogReportEmailMessage();
      case 30093:
        return new MediaEngineMessage.ContactSearchRequestMessage();
      case 30094:
        return new MediaEngineMessage.AddContactRequestMessage();
      case 30099:
        return new MediaEngineMessage.OtherRegisteredDeviceMessage();
      case 30101:
        return new MediaEngineMessage.DismissVerificationWithOtherDeviceMessage();
      case 30102:
        return new MediaEngineMessage.ContactTangoCustomerSupportMessage();
      case 30103:
        return new MediaEngineMessage.ShakeActionDetectedMessage();
      case 30105:
        return new MediaEngineMessage.PhoneNumberContactSearchMessage();
      case 30104:
        return new MediaEngineMessage.EmailContactSearchMessage();
      case 30106:
        return new MediaEngineMessage.ReceiveMessageNotificationMessage();
      case 30114:
        return new MediaEngineMessage.RequestAppLogMessage();
      case 30116:
        return new MediaEngineMessage.QueryUnreadMissedCallNumberMessage();
      case 30117:
        return new MediaEngineMessage.ClearUnreadMissedCallNumberMessage();
      case 30203:
        return new MediaEngineMessage.VGoodAnimationCompleteMessage();
      case 30124:
        return new MediaEngineMessage.ValidationUserNoCodeMessage();
      case 30166:
        return new MediaEngineMessage.StartRecordingVideoMailMessage();
      case 30167:
        return new MediaEngineMessage.StopRecordingVideoMailMessage();
      case 30212:
        return new MediaEngineMessage.ProductCatalogFinishedMessage();
      case 30211:
        return new MediaEngineMessage.QueryProductCatalogMessage();
      case 30210:
        return new MediaEngineMessage.ReportPurchaseMessage();
      case 30227:
        return new MediaEngineMessage.DisplayProductDetailMessage();
      case 30226:
        return new MediaEngineMessage.BackToVGoodProductCatalogMessage();
      case 30191:
        return new MediaEngineMessage.PurchaseVGoodMessage();
      case 30189:
        return new MediaEngineMessage.DisplayVgoodPaymentMessage();
      case 30199:
        return new MediaEngineMessage.CancelInAppPurchaseMessage();
      case 30204:
        return new MediaEngineMessage.LockedVGoodSelectedMessage();
      case 30187:
        return new MediaEngineMessage.ToogleVideoViewButtonBarMessage();
      case 30195:
        return new MediaEngineMessage.ProductInfoOkMessage();
      case 30229:
        return new MediaEngineMessage.VGoodDemoAnimationCompleteMessage();
      case 30161:
        return new MediaEngineMessage.VideoMailReceiversMessage();
      case 30162:
        return new MediaEngineMessage.CancelVideoMailReceiversSelectionMessage();
      case 30163:
        return new MediaEngineMessage.ConfirmVideoMailNonTangoNotificationMessage();
      case 30172:
        return new MediaEngineMessage.DismissUploadVideoMailFinishedMessage();
      case 30231:
        return new MediaEngineMessage.WelcomeGoToMessage();
      case 30233:
        return new MediaEngineMessage.PurchaseAttemptMessage();
      case 30320:
        return new MediaEngineMessage.FBDidLoginMessage();
      case 30265:
        return new MediaEngineMessage.DisplayStoreMessage();
      case 30266:
        return new MediaEngineMessage.DismissStoreBadgeMessage();
      case 30244:
        return new MediaEngineMessage.DisplayAvatarPaymentMessage();
      case 30245:
        return new MediaEngineMessage.PurchaseAvatarMessage();
      case 30246:
        return new MediaEngineMessage.DisplayAvatarProductDetailMessage();
      case 30247:
        return new MediaEngineMessage.BackToAvatarProductCatalogMessage();
      case 30270:
        return new MediaEngineMessage.OpenConversationMessage();
      case 30271:
        return new MediaEngineMessage.SendConversationMessageMessage();
      case 30272:
        return new MediaEngineMessage.CloseConversationMessage();
      case 30273:
        return new MediaEngineMessage.OpenConversationListMessage();
      case 30274:
        return new MediaEngineMessage.DeleteConversationMessage();
      case 30275:
        return new MediaEngineMessage.PlayVideoMessageMessage();
      case 30276:
        return new MediaEngineMessage.FinishPlayVideoMessageMessage();
      case 30277:
        return new MediaEngineMessage.ShowMoreMessageMessage();
      case 30278:
        return new MediaEngineMessage.ConversationMessageNotificationReceivedMessage();
      case 30283:
        return new MediaEngineMessage.SelectContactRequestMessage();
      case 30284:
        return new MediaEngineMessage.SelectContactResultMessage();
      case 30285:
        return new MediaEngineMessage.CancelSelectContactMessage();
      case 30286:
        return new MediaEngineMessage.DeleteSingleConversationMessageMessage();
      case 30288:
        return new MediaEngineMessage.AdvertisementShownMessage();
      case 30289:
        return new MediaEngineMessage.AdvertisementClickedMessage();
      case 30290:
        return new MediaEngineMessage.TakePictureMessage();
      case 30291:
        return new MediaEngineMessage.CancelTakePictureMessage();
      case 30292:
        return new MediaEngineMessage.ChoosePictureMessage();
      case 30293:
        return new MediaEngineMessage.CancelChoosePictureMessage();
      case 30294:
        return new MediaEngineMessage.PostProcessPictureMessage();
      case 30295:
        return new MediaEngineMessage.CancelPostProcessPictureMessage();
      case 30296:
        return new MediaEngineMessage.ViewPictureMessage();
      case 30297:
        return new MediaEngineMessage.CancelViewPictureMessage();
      case 35001:
        return new MediaEngineMessage.LoginErrorEvent();
      case 35400:
        return new MediaEngineMessage.TestEvent();
      case 35003:
        return new MediaEngineMessage.LoginNotificationUserAcceptedEvent();
      case 35004:
        return new MediaEngineMessage.LoginNotificationEvent();
      case 35005:
        return new MediaEngineMessage.LoginEvent();
      case 35007:
        return new MediaEngineMessage.MakePremiumCallEvent();
      case 35009:
        return new MediaEngineMessage.ProcessNotificationEvent();
      case 35011:
        return new MediaEngineMessage.LoginCompletedEvent();
      case 35015:
        return new MediaEngineMessage.SendCallInvitationEvent();
      case 35017:
        return new MediaEngineMessage.CallReceivedEvent();
      case 35019:
        return new MediaEngineMessage.CallErrorEvent();
      case 35021:
        return new MediaEngineMessage.SendCallAcceptedEvent();
      case 35023:
        return new MediaEngineMessage.AudioInProgressEvent();
      case 35029:
        return new MediaEngineMessage.VideoInInitializationEvent();
      case 35025:
        return new MediaEngineMessage.AudioVideoInProgressEvent();
      case 35027:
        return new MediaEngineMessage.DisplayRegisterUserEvent();
      case 35031:
        return new MediaEngineMessage.DisplayRegisterUserEvent();
      case 35033:
        return new MediaEngineMessage.DisplayAnimationEvent();
      case 35037:
        return new MediaEngineMessage.UpdateTangoUsersEvent();
      case 35038:
        return new MediaEngineMessage.UpdateTangoAlertsEvent();
      case 35039:
        return new MediaEngineMessage.InviteDisplayMainEvent();
      case 35041:
        return new MediaEngineMessage.InviteEmailSelectionEvent();
      case 35057:
        return new MediaEngineMessage.InviteEmailComposerEvent();
      case 35059:
        return new MediaEngineMessage.InviteSMSSelectionEvent();
      case 35060:
        return new MediaEngineMessage.InviteRecommendedSelectionEvent();
      case 35061:
        return new MediaEngineMessage.InviteSMSInstructionEvent();
      case 35047:
        return new MediaEngineMessage.ContactsDisplayMainEvent();
      case 35049:
        return new MediaEngineMessage.DisplaySettingsEvent();
      case 35051:
        return new MediaEngineMessage.ValidationRequiredEvent();
      case 35052:
        return new MediaEngineMessage.EmailValidationRequiredEvent();
      case 35054:
        return new MediaEngineMessage.PushValidationRequiredEvent();
      case 35056:
        return new MediaEngineMessage.SMSValidationRequiredEvent();
      case 35067:
        return new MediaEngineMessage.DisplayFAQEvent();
      case 35069:
        return new MediaEngineMessage.AudioVideo2WayInProgressEvent();
      case 35071:
        return new MediaEngineMessage.AudioInInitializationEvent();
      case 35073:
        return new MediaEngineMessage.MissedCallEvent();
      case 35075:
        return new MediaEngineMessage.UpdateRequiredEvent();
      case 35077:
        return new MediaEngineMessage.NetworkLowBandwidthEvent();
      case 35079:
        return new MediaEngineMessage.NetworkHighBandwidthEvent();
      case 35080:
        return new MediaEngineMessage.InCallAlertEvent();
      case 35083:
        return new MediaEngineMessage.AudioModeChangedEvent();
      case 35089:
        return new MediaEngineMessage.VideoModeChangedEvent();
      case 35085:
        return new MediaEngineMessage.CallDisconnectingEvent();
      case 35088:
        return new MediaEngineMessage.ValidationResultEvent();
      case 35065:
        return new MediaEngineMessage.ValidationFailedEvent();
      case 35090:
        return new MediaEngineMessage.RegisterUserNoNetworkEvent();
      case 35091:
        return new MediaEngineMessage.DisplayInviteContactEvent();
      case 35092:
        return new MediaEngineMessage.DisplayCallLogEvent();
      case 35093:
        return new MediaEngineMessage.UpdateCallLogEvent();
      case 35036:
        return new MediaEngineMessage.LaunchLogReportEmailEvent();
      case 35094:
        return new MediaEngineMessage.SearchAndAddMainEvent();
      case 35100:
        return new MediaEngineMessage.QueryOtherRegisteredDeviceEvent();
      case 35101:
        return new MediaEngineMessage.VerficationWithOtherDeviceEvent();
      case 35102:
        return new MediaEngineMessage.ShakeModeChangeEvent();
      case 35103:
        return new MediaEngineMessage.ContactTangoCustomerSupportEvent();
      case 35105:
        return new MediaEngineMessage.PhoneNumberContactSearchEvent();
      case 35104:
        return new MediaEngineMessage.EmailContactSearchEvent();
      case 35106:
        return new MediaEngineMessage.EmailContactSearchResultEvent();
      case 35107:
        return new MediaEngineMessage.PhoneNumberContactSearchResultEvent();
      case 35108:
        return new MediaEngineMessage.DisplayMessageNotificationEvent();
      case 35112:
        return new MediaEngineMessage.EnterBackgroundEvent();
      case 35113:
        return new MediaEngineMessage.CalleeMissedCallEvent();
      case 35114:
        return new MediaEngineMessage.SMSRateLimitedEvent();
      case 35116:
        return new MediaEngineMessage.DisplayAppLogEvent();
      case 35118:
        return new MediaEngineMessage.DisplayAlertNumbersEvent();
      case 35119:
        return new MediaEngineMessage.DisplaySMSSentEvent();
      case 35123:
        return new MediaEngineMessage.EmailValidationWrongCodeEvent();
      case 35124:
        return new MediaEngineMessage.DisplaySupportWebsiteEvent();
      case 35170:
        return new MediaEngineMessage.UploadVideoMailResponseEvent();
      case 35180:
        return new MediaEngineMessage.PlayVideoMailEvent();
      case 35181:
        return new MediaEngineMessage.StartPlayVideoMailEvent();
      case 35182:
        return new MediaEngineMessage.PausePlayVideoMailEvent();
      case 35183:
        return new MediaEngineMessage.FinishPlayVideoMailEvent();
      case 35153:
        return new MediaEngineMessage.VideoMailConfigurationEvent();
      case 35155:
        return new MediaEngineMessage.VideoMailResultEvent();
      case 35166:
        return new MediaEngineMessage.RecordVideoMailEvent();
      case 35167:
        return new MediaEngineMessage.StartRecordingVideoMailEvent();
      case 35168:
        return new MediaEngineMessage.StopRecordingVideoMailEvent();
      case 35194:
        return new MediaEngineMessage.VGoodAnimationCompleteEvent();
      case 35202:
        return new MediaEngineMessage.DisplayProductCatalogEvent();
      case 35200:
        return new MediaEngineMessage.ReportPurchaseResultEvent();
      case 35201:
        return new MediaEngineMessage.WaitProductCatalogEvent();
      case 35217:
        return new MediaEngineMessage.InviteSnsComposeEvent();
      case 35218:
        return new MediaEngineMessage.InviteSnsPublishResultEvent();
      case 35219:
        return new MediaEngineMessage.SnsRequestAuthEvent();
      case 35220:
        return new MediaEngineMessage.SnsProcessingEvent();
      case 35221:
        return new MediaEngineMessage.SnsAuthFailedEvent();
      case 35222:
        return new MediaEngineMessage.StopVideoRingbackEvent();
      case 35240:
        return new MediaEngineMessage.DisplayCallQualitySurveyEvent();
      case 35191:
        return new MediaEngineMessage.DisplayPostCallScreenEvent();
      case 35214:
        return new MediaEngineMessage.DisplayAppStoreEvent();
      case 35215:
        return new MediaEngineMessage.DisplayFacebookLikeEvent();
      case 35216:
        return new MediaEngineMessage.DisplayContactDetailEvent();
      case 35189:
        return new MediaEngineMessage.DisplayVgoodEvent();
      case 35187:
        return new MediaEngineMessage.ToggleVideoViewButtonBarEvent();
      case 35193:
        return new MediaEngineMessage.DisplayProductInfoEvent();
      case 35225:
        return new MediaEngineMessage.VGoodErrorEvent();
      case 35226:
        return new MediaEngineMessage.GameErrorEvent();
      case 35223:
        return new MediaEngineMessage.VGoodProductCatalogEvent();
      case 35246:
        return new MediaEngineMessage.AvatarProductCatalogEvent();
      case 35300:
        return new MediaEngineMessage.DisplayGameCatalogEvent();
      case 35224:
        return new MediaEngineMessage.DisplayProductDetailEvent();
      case 35301:
        return new MediaEngineMessage.DisplayGameDemoEvent();
      case 35302:
        return new MediaEngineMessage.DisplayGameInCallEvent();
      case 35229:
        return new MediaEngineMessage.VGoodDemoAnimationCompleteEvent();
      case 35161:
        return new MediaEngineMessage.DisplayVideoMailReceiversEvent();
      case 35162:
        return new MediaEngineMessage.DisplayVideoMailNonTangoNotificationEvent();
      case 35173:
        return new MediaEngineMessage.UploadVideoMailFinishedEvent();
      case 35320:
        return new MediaEngineMessage.FBDidLoginEvent();
      case 35265:
        return new MediaEngineMessage.BadgeStoreEvent();
      case 35267:
        return new MediaEngineMessage.BadgeInviteEvent();
      case 35270:
        return new MediaEngineMessage.OpenConversationEvent();
      case 35271:
        return new MediaEngineMessage.DisplayConversationMessageEvent();
      case 35286:
        return new MediaEngineMessage.DeleteSingleConversationMessageEvent();
      case 35272:
        return new MediaEngineMessage.OpenConversationListEvent();
      case 35273:
        return new MediaEngineMessage.UpdateConversationSummaryEvent();
      case 35274:
        return new MediaEngineMessage.DeleteConversationEvent();
      case 35275:
        return new MediaEngineMessage.PlayVideoMessageEvent();
      case 35276:
        return new MediaEngineMessage.ConversationMessageSendStatusEvent();
      case 35321:
        return new MediaEngineMessage.MessageUpdateReadStatusEvent();
      case 35277:
        return new MediaEngineMessage.ShowMoreMessageEvent();
      case 35278:
        return new MediaEngineMessage.PlayMessageErrorEvent();
      case 35283:
        return new MediaEngineMessage.UpdateConversationMessageNotificationEvent();
      case 35279:
        return new MediaEngineMessage.QueryLeaveMessageEvent();
      case 35280:
        return new MediaEngineMessage.ForwardMessageResultEvent();
      case 35284:
        return new MediaEngineMessage.RetrieveOfflineMessageResultEvent();
      case 35281:
        return new MediaEngineMessage.StartSMSComposeEvent();
      case 35282:
        return new MediaEngineMessage.SelectContactEvent();
      case 35288:
        return new MediaEngineMessage.ShowAdvertisementEvent();
      case 35290:
        return new MediaEngineMessage.TakePictureEvent();
      case 35291:
        return new MediaEngineMessage.CancelTakePictureEvent();
      case 35292:
        return new MediaEngineMessage.ChoosePictureEvent();
      case 293:
        return new MediaEngineMessage.CancelChoosePictureEvent();
      case 35294:
        return new MediaEngineMessage.PostProcessPictureEvent();
      case 35295:
        return new MediaEngineMessage.CancelChoosePictureEvent();
      case 35296:
        return new MediaEngineMessage.SendPictureEvent();
      case 35297:
        return new MediaEngineMessage.ViewPictureEvent();
      case 35298:
        return new MediaEngineMessage.CancelViewPictureEvent();
      case 35264:
        return new MediaEngineMessage.DisplayStoreEvent();
      case 35250:
        return new MediaEngineMessage.AvatarErrorEvent();
      case 35251:
        return new MediaEngineMessage.SwitchAvatarEvent();
      case 35242:
        return new MediaEngineMessage.AudioAvatarInProgressEvent();
      case 35243:
        return new MediaEngineMessage.VideoAvatarInProgressEvent();
      case 35244:
        return new MediaEngineMessage.Audio2WayAvatarInProgressEvent();
      case 35245:
        return new MediaEngineMessage.AvatarRenderRequestEvent();
      case 35247:
        return new MediaEngineMessage.DisplayAvatarProductDetailEvent();
      case 35248:
        return new MediaEngineMessage.DisplayDemoAvatarEvent();
      case 35249:
      }
      return new MediaEngineMessage.DisplayAvatarEvent();
    }
  }

  public static final class MessageUpdateReadStatusEvent extends SerializableMessage<SessionMessages.SwapReadStatusesPayload>
  {
    public MessageUpdateReadStatusEvent()
    {
      super();
    }
  }

  public static final class MissedCallEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public MissedCallEvent()
    {
      super();
    }
  }

  public static final class NavigateBackMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public NavigateBackMessage()
    {
      super();
    }
  }

  public static final class NetworkHighBandwidthEvent extends SerializableMessage<SessionMessages.BandwidthPayload>
  {
    public NetworkHighBandwidthEvent()
    {
      super();
    }
  }

  public static final class NetworkLowBandwidthEvent extends SerializableMessage<SessionMessages.BandwidthPayload>
  {
    public NetworkLowBandwidthEvent()
    {
      super();
    }
  }

  public static final class OpenConversationEvent extends SerializableMessage<SessionMessages.ConversationPayload>
  {
    public OpenConversationEvent()
    {
      super();
    }
  }

  public static final class OpenConversationListEvent extends SerializableMessage<SessionMessages.ConversationSummaryListPayload>
  {
    public OpenConversationListEvent()
    {
      super();
    }
  }

  public static final class OpenConversationListMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public OpenConversationListMessage()
    {
      super();
    }
  }

  public static final class OpenConversationMessage extends SerializableMessage<SessionMessages.ConversationPayload>
  {
    public OpenConversationMessage()
    {
      super();
    }

    public OpenConversationMessage(SessionMessages.Contact paramContact)
    {
      super();
    }

    public OpenConversationMessage(String paramString)
    {
      super();
    }

    public OpenConversationMessage(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
      super();
    }
  }

  public static final class OtherRegisteredDeviceMessage extends SerializableMessage<SessionMessages.OtherRegisteredDevicePayload>
  {
    public OtherRegisteredDeviceMessage()
    {
      super();
    }

    public OtherRegisteredDeviceMessage(boolean paramBoolean)
    {
      super();
    }
  }

  public static final class PausePlayVideoMailEvent extends SerializableMessage<SessionMessages.VideoMailIdPayload>
  {
    public PausePlayVideoMailEvent()
    {
      super();
    }
  }

  public static final class PausePlayVideoMailMessage extends SerializableMessage<SessionMessages.VideoMailIdPayload>
  {
    public PausePlayVideoMailMessage(String paramString1, String paramString2)
    {
      super();
    }
  }

  public static final class PhoneNumberContactSearchEvent extends SerializableMessage<SessionMessages.ContactSearchPayload>
  {
    public PhoneNumberContactSearchEvent()
    {
      super();
    }
  }

  public static final class PhoneNumberContactSearchMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public PhoneNumberContactSearchMessage()
    {
      super();
    }
  }

  public static final class PhoneNumberContactSearchResultEvent extends SerializableMessage<SessionMessages.ContactSearchResultPayload>
  {
    public PhoneNumberContactSearchResultEvent()
    {
      super();
    }
  }

  public static final class PlayMessageErrorEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public PlayMessageErrorEvent()
    {
      super();
    }
  }

  public static final class PlayVideoMailEvent extends SerializableMessage<SessionMessages.PlayVideoMailPayload>
  {
    public PlayVideoMailEvent()
    {
      super();
    }
  }

  public static final class PlayVideoMailMessage extends SerializableMessage<SessionMessages.VideoMailIdPayload>
  {
    public PlayVideoMailMessage(String paramString1, String paramString2)
    {
      super();
    }
  }

  public static final class PlayVideoMessageEvent extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public PlayVideoMessageEvent()
    {
      super();
    }
  }

  public static final class PlayVideoMessageMessage extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public PlayVideoMessageMessage()
    {
      super();
    }

    public PlayVideoMessageMessage(String paramString1, String paramString2, int paramInt, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class PostProcessPictureEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public PostProcessPictureEvent()
    {
      super();
    }
  }

  public static final class PostProcessPictureMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public PostProcessPictureMessage()
    {
      super();
    }
  }

  public static final class ProcessNotificationEvent extends SerializableMessage<SessionMessages.LoginPayload>
  {
    public ProcessNotificationEvent()
    {
      super();
    }
  }

  public static final class ProductCatalogFinishedMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ProductCatalogFinishedMessage()
    {
      super();
    }
  }

  public static final class ProductInfoOkMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ProductInfoOkMessage()
    {
      super();
    }
  }

  public static final class PurchaseAttemptMessage extends SerializableMessage<SessionMessages.PurchaseAttemtPayload>
  {
    public PurchaseAttemptMessage()
    {
      super();
    }

    public PurchaseAttemptMessage(String paramString)
    {
      super();
    }
  }

  public static final class PurchaseAvatarMessage extends SerializableMessage<SessionMessages.PurchasePayload>
  {
    public PurchaseAvatarMessage()
    {
      super();
    }

    public PurchaseAvatarMessage(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4, long paramLong, String paramString5, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class PurchaseVGoodMessage extends SerializableMessage<SessionMessages.PurchasePayload>
  {
    public PurchaseVGoodMessage()
    {
      super();
    }

    public PurchaseVGoodMessage(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4, long paramLong, String paramString5, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class PushValidationRequiredEvent extends SerializableMessage<SessionMessages.BoolPayload>
  {
    public PushValidationRequiredEvent()
    {
      super();
    }
  }

  public static final class PutAppInBackgroundMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public PutAppInBackgroundMessage()
    {
      super();
    }
  }

  public static final class PutAppInForegroundMessage extends SerializableMessage<SessionMessages.PutAppInForegroundPayload>
  {
    public PutAppInForegroundMessage()
    {
      super();
    }
  }

  public static final class QueryLeaveMessageEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public QueryLeaveMessageEvent()
    {
      super();
    }
  }

  public static final class QueryLeaveMessageResultMessage extends SerializableMessage<SessionMessages.BoolPayload>
  {
    public QueryLeaveMessageResultMessage(boolean paramBoolean)
    {
      super();
    }
  }

  public static final class QueryOtherRegisteredDeviceEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public QueryOtherRegisteredDeviceEvent()
    {
      super();
    }
  }

  public static final class QueryProductCatalogMessage extends SerializableMessage<SessionMessages.ProductCatalogRequestPayload>
  {
    public QueryProductCatalogMessage()
    {
      super();
    }

    public QueryProductCatalogMessage(int paramInt, String paramString)
    {
      super();
    }
  }

  public static final class QueryUnreadMissedCallNumberMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public QueryUnreadMissedCallNumberMessage()
    {
      super();
    }
  }

  public static final class ReceiveMessageNotificationMessage extends SerializableMessage<SessionMessages.NotificationMessagePayload>
  {
    public ReceiveMessageNotificationMessage()
    {
      super();
    }

    public ReceiveMessageNotificationMessage(String paramString1, String paramString2)
    {
      super();
    }

    public ReceiveMessageNotificationMessage(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
    {
      super();
    }
  }

  public static final class ReceivePushNotificationMessage extends SerializableMessage<SessionMessages.PushNotificationPayload>
  {
    public ReceivePushNotificationMessage()
    {
      super();
    }

    public ReceivePushNotificationMessage(String paramString1, String paramString2, String paramString3)
    {
      super();
    }

    public ReceivePushNotificationMessage(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, int paramInt2)
    {
      super();
    }
  }

  public static final class RecordVideoMailEvent extends SerializableMessage<SessionMessages.RecordVideoMailPayload>
  {
    public RecordVideoMailEvent()
    {
      super();
    }
  }

  public static final class RegisterUserMessage extends SerializableMessage<SessionMessages.RegisterUserPayload>
  {
    public RegisterUserMessage()
    {
      super();
    }

    public RegisterUserMessage(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class RegisterUserNoNetworkEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public RegisterUserNoNetworkEvent()
    {
      super();
    }
  }

  public static final class RejectCallMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public RejectCallMessage()
    {
      super();
    }

    public RejectCallMessage(String paramString)
    {
      super();
    }
  }

  public static final class RemoveAvatarEvent extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public RemoveAvatarEvent()
    {
      super();
    }
  }

  public static final class RemoveAvatarMessage extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public RemoveAvatarMessage(long paramLong)
    {
      super();
    }
  }

  public static final class RemoveFavoriteContactMessage extends SerializableMessage<SessionMessages.ContactsPayload>
  {
    public RemoveFavoriteContactMessage(List<SessionMessages.Contact> paramList)
    {
      super();
    }
  }

  public static final class RemoveVideoMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public RemoveVideoMessage()
    {
      super();
    }

    public RemoveVideoMessage(String paramString)
    {
      super();
    }
  }

  public static final class ReportPurchaseMessage extends SerializableMessage<SessionMessages.PurchasePayload>
  {
    public ReportPurchaseMessage()
    {
      super();
    }

    public ReportPurchaseMessage(String paramString1, String paramString2, int paramInt, String paramString3, long paramLong, String paramString4)
    {
      super();
    }
  }

  public static final class ReportPurchaseResultEvent extends SerializableMessage<SessionMessages.PurchaseResultPayload>
  {
    public ReportPurchaseResultEvent()
    {
      super();
    }
  }

  public static final class ReportPurchasedAvatarMessage extends SerializableMessage<SessionMessages.PurchasePayload>
  {
    public ReportPurchasedAvatarMessage()
    {
      super();
    }

    public ReportPurchasedAvatarMessage(String paramString1, String paramString2, int paramInt, long paramLong)
    {
      super();
    }
  }

  public static final class RequestAppLogMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public RequestAppLogMessage()
    {
      super();
    }
  }

  public static final class RequestCallLogMessage extends SerializableMessage<SessionMessages.RequestCallLogPayload>
  {
    public RequestCallLogMessage()
    {
      super();
    }
  }

  public static final class RequestFAQMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public RequestFAQMessage()
    {
      super();
    }
  }

  public static final class RequestFilterContactMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public RequestFilterContactMessage()
    {
      super();
    }
  }

  public static final class RetrieveOfflineMessageResultEvent extends SerializableMessage<SessionMessages.RetrieveOfflineMessageStatusPayload>
  {
    public RetrieveOfflineMessageResultEvent()
    {
      super();
    }
  }

  public static final class SMSRateLimitedEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public SMSRateLimitedEvent()
    {
      super();
    }
  }

  public static final class SMSValidationRequiredEvent extends SerializableMessage<SessionMessages.SMSVerificationPayload>
  {
    public SMSValidationRequiredEvent()
    {
      super();
    }
  }

  public static final class SavePersonalInfoMessage extends SerializableMessage<SessionMessages.RegisterUserPayload>
  {
    public SavePersonalInfoMessage()
    {
      super();
    }

    public SavePersonalInfoMessage(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class SearchAndAddMainEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public SearchAndAddMainEvent()
    {
      super();
    }
  }

  public static final class SelectContactEvent extends SerializableMessage<SessionMessages.SelectContactPayload>
  {
    public SelectContactEvent()
    {
      super();
    }
  }

  public static final class SelectContactRequestMessage extends SerializableMessage<SessionMessages.SelectContactRequestPayload>
  {
    public SelectContactRequestMessage()
    {
      super();
    }

    public SelectContactRequestMessage(SessionMessages.SelectContactType paramSelectContactType)
    {
      super();
    }

    public SelectContactRequestMessage(SessionMessages.SelectContactType paramSelectContactType, SessionMessages.ConversationMessageType paramConversationMessageType)
    {
      super();
    }
  }

  public static final class SelectContactResultMessage extends SerializableMessage<SessionMessages.SelectContactPayload>
  {
    public SelectContactResultMessage()
    {
      super();
    }

    public SelectContactResultMessage(SessionMessages.SelectContactType paramSelectContactType, List<SessionMessages.Contact> paramList)
    {
      super();
    }
  }

  public static final class SendCallAcceptedEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public SendCallAcceptedEvent()
    {
      super();
    }
  }

  public static final class SendCallInvitationEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public SendCallInvitationEvent()
    {
      super();
    }
  }

  public static final class SendConversationMessageMessage extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public SendConversationMessageMessage()
    {
      super();
    }

    public SendConversationMessageMessage(String paramString, int paramInt, SessionMessages.ConversationMessageType paramConversationMessageType)
    {
      super();
    }

    public SendConversationMessageMessage(String paramString1, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString2)
    {
      super();
    }

    public SendConversationMessageMessage(String paramString1, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString2, int paramInt1, int paramInt2)
    {
      super();
    }

    public SendConversationMessageMessage(String paramString1, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString2, String paramString3)
    {
      super();
    }

    public SendConversationMessageMessage(String paramString1, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString2, String paramString3, int paramInt1, int paramInt2, long paramLong, int paramInt3, boolean paramBoolean)
    {
      super();
    }

    public SendConversationMessageMessage(String paramString1, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString2, String paramString3, int paramInt1, int paramInt2, String paramString4)
    {
      super();
    }
  }

  public static final class SendPictureEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public SendPictureEvent()
    {
      super();
    }
  }

  public static final class SendSgiggleInvitationMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public SendSgiggleInvitationMessage()
    {
      super();
    }

    public SendSgiggleInvitationMessage(String paramString)
    {
      super();
    }
  }

  public static final class SendValidationCodeMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public SendValidationCodeMessage()
    {
      super();
    }

    public SendValidationCodeMessage(String paramString)
    {
      super();
    }
  }

  public static final class SendValidationCodeRequestMessage extends SerializableMessage<SessionMessages.ValidationCodeDeliveryPayload>
  {
    public SendValidationCodeRequestMessage(SessionMessages.ValidationCodeDeliveryPayload.ValidationCodeDeliveryType paramValidationCodeDeliveryType)
    {
      super();
    }
  }

  public static final class ShakeActionDetectedMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ShakeActionDetectedMessage()
    {
      super();
    }
  }

  public static final class ShakeModeChangeEvent extends SerializableMessage<SessionMessages.ShakeModePayload>
  {
    public ShakeModeChangeEvent()
    {
      super();
    }

    public ShakeModeChangeEvent(boolean paramBoolean)
    {
      super();
    }
  }

  public static final class ShowAdvertisementEvent extends SerializableMessage<SessionMessages.ShowAdvertisementPayload>
  {
    public ShowAdvertisementEvent()
    {
      super();
    }
  }

  public static final class ShowMoreMessageEvent extends SerializableMessage<SessionMessages.ConversationPayload>
  {
    public ShowMoreMessageEvent()
    {
      super();
    }
  }

  public static final class ShowMoreMessageMessage extends SerializableMessage<SessionMessages.ConversationPayload>
  {
    public ShowMoreMessageMessage()
    {
      super();
    }

    public ShowMoreMessageMessage(String paramString, int paramInt)
    {
      super();
    }
  }

  public static final class SkipVideoRingbackMessage extends SerializableMessage<SessionMessages.BoolPayload>
  {
    public SkipVideoRingbackMessage(boolean paramBoolean)
    {
      super();
    }
  }

  public static final class SnsAuthFailedEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public SnsAuthFailedEvent()
    {
      super();
    }
  }

  public static final class SnsAuthResultMessage extends SerializableMessage<SessionMessages.SnsAuthResultPayload>
  {
    public SnsAuthResultMessage(String paramString1, boolean paramBoolean, int paramInt, String paramString2, String paramString3)
    {
      super();
    }
  }

  public static final class SnsCancelProcessMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public SnsCancelProcessMessage()
    {
      super();
    }
  }

  public static final class SnsProcessingEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public SnsProcessingEvent()
    {
      super();
    }
  }

  public static final class SnsProcessingTimeoutMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public SnsProcessingTimeoutMessage()
    {
      super();
    }
  }

  public static final class SnsRequestAuthEvent extends SerializableMessage<SessionMessages.StringPayload>
  {
    public SnsRequestAuthEvent()
    {
      super();
    }
  }

  public static final class StartDemoAvatarMessage extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public StartDemoAvatarMessage(long paramLong)
    {
      super();
    }
  }

  public static final class StartPlayVideoMailEvent extends SerializableMessage<SessionMessages.VideoMailDownloadURLPayload>
  {
    public StartPlayVideoMailEvent()
    {
      super();
    }
  }

  public static final class StartRecordingVideoMailEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public StartRecordingVideoMailEvent()
    {
      super();
    }
  }

  public static final class StartRecordingVideoMailMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public StartRecordingVideoMailMessage()
    {
      super();
    }
  }

  public static final class StartSMSComposeEvent extends SerializableMessage<SessionMessages.SMSComposerPayload>
  {
    public StartSMSComposeEvent()
    {
      super();
    }
  }

  public static final class StartSMSComposeMessage extends SerializableMessage<SessionMessages.SMSComposerPayload>
  {
    public StartSMSComposeMessage(SessionMessages.SMSComposerType paramSMSComposerType, boolean paramBoolean, SessionMessages.Contact paramContact, String paramString)
    {
      super();
    }

    public StartSMSComposeMessage(SessionMessages.SMSComposerType paramSMSComposerType, boolean paramBoolean, List<SessionMessages.Contact> paramList, String paramString)
    {
      super();
    }

    public StartSMSComposeMessage(SessionMessages.SMSComposerType paramSMSComposerType, boolean paramBoolean1, List<SessionMessages.Contact> paramList, String paramString1, boolean paramBoolean2, String paramString2, String paramString3)
    {
      super();
    }
  }

  public static final class StatsCollectorLogMessage extends SerializableMessage<SessionMessages.StatsCollectorLogPayload>
  {
    public StatsCollectorLogMessage(List<SessionMessages.KeyValuePair> paramList)
    {
      super();
    }
  }

  public static final class StopRecordingVideoMailEvent extends SerializableMessage<SessionMessages.StopRecordingVideoMailPayload>
  {
    public StopRecordingVideoMailEvent()
    {
      super();
    }
  }

  public static final class StopRecordingVideoMailMessage extends SerializableMessage<SessionMessages.StopRecordingVideoMailPayload>
  {
    public StopRecordingVideoMailMessage()
    {
      super();
    }

    public StopRecordingVideoMailMessage(SessionMessages.StopRecordingVideoMailPayload.Type paramType)
    {
      super();
    }
  }

  public static final class StopVideoRingbackEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public StopVideoRingbackEvent()
    {
      super();
    }
  }

  public static final class SwitchAvatarEvent extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public SwitchAvatarEvent()
    {
      super();
    }
  }

  public static final class SwitchCameraMessage extends SerializableMessage<SessionMessages.SwitchCameraPayload>
  {
    public SwitchCameraMessage()
    {
      super();
    }

    public SwitchCameraMessage(String paramString, int paramInt)
    {
      super();
    }
  }

  public static final class TakePictureEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public TakePictureEvent()
    {
      super();
    }
  }

  public static final class TakePictureMessage extends SerializableMessage<SessionMessages.AnchorPayload>
  {
    public TakePictureMessage()
    {
      super();
    }
  }

  public static final class TangoDeviceTokenMessage extends SerializableMessage<SessionMessages.TangoDeviceTokenPayload>
  {
    public TangoDeviceTokenMessage()
    {
      super();
    }

    public TangoDeviceTokenMessage(String paramString)
    {
      super();
    }
  }

  public static final class TerminateCallMessage extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public TerminateCallMessage()
    {
      super();
    }

    public TerminateCallMessage(String paramString)
    {
      super();
    }
  }

  public static final class TestEvent extends SerializableMessage<SessionMessages.TestPayload>
  {
    public TestEvent()
    {
      super();
    }
  }

  public static final class TestMessage extends SerializableMessage<SessionMessages.TestPayload>
  {
    public TestMessage()
    {
      super();
    }

    public TestMessage(String paramString, int paramInt)
    {
      super();
    }
  }

  public static final class ToggleVideoViewButtonBarEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ToggleVideoViewButtonBarEvent()
    {
      super();
    }
  }

  public static final class ToogleVideoViewButtonBarMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ToogleVideoViewButtonBarMessage()
    {
      super();
    }
  }

  public static final class UpdateCallLogEvent extends SerializableMessage<SessionMessages.CallEntriesPayload>
  {
    public UpdateCallLogEvent()
    {
      super();
    }
  }

  public static final class UpdateConversationMessageNotificationEvent extends SerializableMessage<SessionMessages.UpdateConversationMessageNotificationPayload>
  {
    public UpdateConversationMessageNotificationEvent()
    {
      super();
    }
  }

  public static final class UpdateConversationSummaryEvent extends SerializableMessage<SessionMessages.ConversationSummaryItemPayload>
  {
    public UpdateConversationSummaryEvent()
    {
      super();
    }
  }

  public static final class UpdateRequiredEvent extends SerializableMessage<SessionMessages.UpdateRequiredPayload>
  {
    public UpdateRequiredEvent()
    {
      super();
    }
  }

  public static final class UpdateTangoAlertsEvent extends SerializableMessage<SessionMessages.UpdateAlertsPayload>
  {
    public UpdateTangoAlertsEvent()
    {
      super();
    }
  }

  public static final class UpdateTangoUsersEvent extends SerializableMessage<SessionMessages.ContactsPayload>
  {
    public UpdateTangoUsersEvent()
    {
      super();
    }
  }

  public static final class UploadVideoMailFinishedEvent extends SerializableMessage<SessionMessages.VideoMailIdPayload>
  {
    public UploadVideoMailFinishedEvent()
    {
      super();
    }
  }

  public static final class UploadVideoMailMessage extends SerializableMessage<SessionMessages.UploadVideoMailPayload>
  {
    public UploadVideoMailMessage()
    {
      this(0, 0L, "", 0L, "", new ArrayList(), 0, false);
    }

    public UploadVideoMailMessage(int paramInt1, long paramLong1, String paramString1, long paramLong2, String paramString2, List<SessionMessages.Contact> paramList, int paramInt2, boolean paramBoolean)
    {
      super();
    }
  }

  public static final class UploadVideoMailResponseEvent extends SerializableMessage<SessionMessages.UploadVideoMailResponsePayload>
  {
    public UploadVideoMailResponseEvent()
    {
      super();
    }
  }

  public static final class VGoodAnimationCompleteEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public VGoodAnimationCompleteEvent()
    {
      super();
    }
  }

  public static final class VGoodAnimationCompleteMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public VGoodAnimationCompleteMessage()
    {
      super();
    }
  }

  public static final class VGoodDemoAnimationCompleteEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public VGoodDemoAnimationCompleteEvent()
    {
      super();
    }
  }

  public static final class VGoodDemoAnimationCompleteMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public VGoodDemoAnimationCompleteMessage()
    {
      super();
    }
  }

  public static final class VGoodDemoAnimationTrackingMessage extends SerializableMessage<SessionMessages.VGoodInitiatePayload>
  {
    public VGoodDemoAnimationTrackingMessage(long paramLong, String paramString)
    {
      super();
    }
  }

  public static final class VGoodErrorEvent extends SerializableMessage<SessionMessages.VGoodErrorPayload>
  {
    public VGoodErrorEvent()
    {
      super();
    }
  }

  public static final class VGoodProductCatalogEvent extends SerializableMessage<SessionMessages.ProductCatalogPayload>
  {
    public VGoodProductCatalogEvent()
    {
      super();
    }
  }

  public static final class VMailAttemptPurchaseTrackingMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public VMailAttemptPurchaseTrackingMessage(String paramString)
    {
      super();
    }
  }

  public static final class ValidationFailedEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ValidationFailedEvent()
    {
      super();
    }
  }

  public static final class ValidationRequestMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ValidationRequestMessage()
    {
      super();
    }

    public ValidationRequestMessage(String paramString)
    {
      super();
    }
  }

  public static final class ValidationRequiredEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ValidationRequiredEvent()
    {
      super();
    }
  }

  public static final class ValidationResultEvent extends SerializableMessage<SessionMessages.ValidationResultPayload>
  {
    public ValidationResultEvent()
    {
      super();
    }
  }

  public static final class ValidationUserInputedCodeMessage extends SerializableMessage<SessionMessages.StringPayload>
  {
    public ValidationUserInputedCodeMessage(String paramString)
    {
      super();
    }
  }

  public static final class ValidationUserNoCodeMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public ValidationUserNoCodeMessage()
    {
      super();
    }
  }

  public static final class VerficationWithOtherDeviceEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public VerficationWithOtherDeviceEvent()
    {
      super();
    }
  }

  public static final class VideoAvatarInProgressEvent extends SerializableMessage<SessionMessages.AvatarControlPayload>
  {
    public VideoAvatarInProgressEvent()
    {
      super();
    }
  }

  public static final class VideoInInitializationEvent extends SerializableMessage<SessionMessages.MediaSessionPayload>
  {
    public VideoInInitializationEvent()
    {
      super();
    }
  }

  public static final class VideoMailConfigurationEvent extends SerializableMessage<SessionMessages.VideoMailConfigurationPayload>
  {
    public VideoMailConfigurationEvent()
    {
      super();
    }
  }

  public static final class VideoMailReceiversMessage extends SerializableMessage<SessionMessages.VideoMailReceiversPayload>
  {
    public VideoMailReceiversMessage()
    {
      super();
    }

    public VideoMailReceiversMessage(SessionMessages.MediaType paramMediaType)
    {
      super();
    }
  }

  public static final class VideoMailResultEvent extends SerializableMessage<SessionMessages.OperationErrorPayload>
  {
    public VideoMailResultEvent()
    {
      super();
    }
  }

  public static final class VideoModeChangedEvent extends SerializableMessage<SessionMessages.VideoModePayload>
  {
    public VideoModeChangedEvent()
    {
      super();
    }
  }

  public static final class ViewContactDetailMessage extends SerializableMessage<SessionMessages.ContactDetailPayload>
  {
    public ViewContactDetailMessage(SessionMessages.Contact paramContact)
    {
      super();
    }

    public ViewContactDetailMessage(SessionMessages.Contact paramContact, SessionMessages.ContactDetailPayload.Source paramSource)
    {
      super();
    }
  }

  public static final class ViewPictureEvent extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public ViewPictureEvent()
    {
      super();
    }
  }

  public static final class ViewPictureMessage extends SerializableMessage<SessionMessages.ConversationMessagePayload>
  {
    public ViewPictureMessage()
    {
      super();
    }

    public ViewPictureMessage(String paramString, int paramInt)
    {
      super();
    }
  }

  public static final class WaitProductCatalogEvent extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public WaitProductCatalogEvent()
    {
      super();
    }
  }

  public static final class WandPressedMessage extends SerializableMessage<SessionMessages.WandPressedPayload>
  {
    public WandPressedMessage(SessionMessages.WandLocationType paramWandLocationType)
    {
      super();
    }
  }

  public static final class WelcomeGoToMessage extends SerializableMessage<SessionMessages.OptionalPayload>
  {
    public WelcomeGoToMessage()
    {
      super();
    }

    public WelcomeGoToMessage(String paramString)
    {
      super();
    }
  }

  public static final class event
  {
    public static final int AUDIO_2WAY_AVATAR_IN_PROGRESS_TYPE = 35244;
    public static final int AUDIO_AVATAR_IN_PROGRESS_TYPE = 35242;
    public static final int AUDIO_IN_INTIALIZATION_EVENT = 35071;
    public static final int AUDIO_IN_PROGRESS_EVENT = 35023;
    public static final int AUDIO_MODE_CHANGED_EVENT = 35083;
    public static final int AUDIO_VIDEO_2WAY_IN_PROGRESS_EVENT = 35069;
    public static final int AUDIO_VIDEO_IN_PROGRESS_EVENT = 35025;
    public static final int AVATAR_ERROR_TYPE = 35250;
    public static final int AVATAR_RENDER_REQUEST_TYPE = 35245;
    public static final int BADGE_INVITE_EVENT = 35267;
    public static final int BADGE_STORE_EVENT = 35265;
    public static final int CALLEE_MISSED_CALL_EVENT = 35113;
    public static final int CALL_DISCONNECTING_EVENT = 35085;
    public static final int CALL_ERROR_EVENT = 35019;
    public static final int CALL_RECEIVED_EVENT = 35017;
    public static final int CANCEL_CHOOSE_PICTURE_EVENT = 293;
    public static final int CANCEL_POST_PROCESS_PICTURE_EVENT = 35295;
    public static final int CANCEL_TAKE_PICTURE_EVENT = 35291;
    public static final int CANCEL_UPLOAD_VIDEO_MAIL_RESPONSE_EVENT = 35171;
    public static final int CANCEL_VIEW_PICTURE_EVENT = 35298;
    public static final int CHOOSE_PICTURE_EVENT = 35292;
    public static final int CONTACTS_DISPLAY_MAIN_EVENT = 35047;
    public static final int CONTACT_SEARCH_RESULT_FOUND_SELF_EVENT = 35115;
    public static final int CONTACT_TANGO_CUSTOMER_SUPPORT_EVENT = 35103;
    public static final int CONVERSATION_MESSAGE_SEND_STATUS_EVENT = 35276;
    public static final int DELETE_CONVERSATION_EVENT = 35274;
    public static final int DELETE_SINGLE_CONVERSATION_MESSAGE_EVENT = 35286;
    public static final int DISPLAY_ALERT_NUMBERS_EVENT = 35118;
    public static final int DISPLAY_ANIMATION_EVENT = 35033;
    public static final int DISPLAY_APPSTORE_UI_EVENT = 35214;
    public static final int DISPLAY_APP_LOG_EVENT = 35116;
    public static final int DISPLAY_AVATAR_PAYMENT_UI_TYPE = 35249;
    public static final int DISPLAY_AVATAR_PRODUCT_CATALOG_TYPE = 35246;
    public static final int DISPLAY_AVATAR_PRODUCT_DETAILS_TYPE = 35247;
    public static final int DISPLAY_CALL_LOG_EVENT = 35092;
    public static final int DISPLAY_CALL_QUALITY_SURVEY_UI_TYPE = 35240;
    public static final int DISPLAY_CONTACT_DETAIL_UI_EVENT = 35216;
    public static final int DISPLAY_CONVERSATION_MESSAGE_EVENT = 35271;
    public static final int DISPLAY_DEMO_AVATAR_TYPE = 35248;
    public static final int DISPLAY_FACEBOOK_LIKE_UI_EVENT = 35215;
    public static final int DISPLAY_FAQ_EVENT = 35067;
    public static final int DISPLAY_GAME_CATALOG_EVENT = 35300;
    public static final int DISPLAY_GAME_DEMO_EVENT = 35301;
    public static final int DISPLAY_GAME_IN_CALL_EVENT = 35302;
    public static final int DISPLAY_INVITE_CONTACT_EVENT = 35091;
    public static final int DISPLAY_MESSAGE_NOTIFICATION_EVENT = 35108;
    public static final int DISPLAY_POSTCALL_UI_EVENT = 35191;
    public static final int DISPLAY_PRODUCT_DETAILS_EVENT = 35224;
    public static final int DISPLAY_PRODUCT_INFO_UI_EVENT = 35193;
    public static final int DISPLAY_REGISTER_USER_EVENT = 35031;
    public static final int DISPLAY_SETTINGS_EVENT = 35049;
    public static final int DISPLAY_SMS_SENT_EVENT = 35119;
    public static final int DISPLAY_STORE_EVENT = 35264;
    public static final int DISPLAY_SUPPORT_WEBSITE_EVENT = 35124;
    public static final int DISPLAY_TANGO_PUSH_CALL_CANCEL_TYPE = 35311;
    public static final int DISPLAY_TANGO_PUSH_CALL_TYPE = 35310;
    public static final int DISPLAY_VGOOD_DEMO_SCREEN_EVENT = 35196;
    public static final int DISPLAY_VGOOD_PAYMENT_UI_EVENT = 35189;
    public static final int DISPLAY_VGOOD_PRODUCT_CATALOG_EVENT = 35223;
    public static final int DISPLAY_VGREETING_RECIPIENTS_EVENT = 35263;
    public static final int DISPLAY_VIDEO_MAIL_NON_TANGO_NOTIFICATION_EVENT = 35162;
    public static final int DISPLAY_VIDEO_MAIL_RECEIVERS_EVENT = 35161;
    public static final int EDIT_VIDEO_MAIL_EVENT = 35152;
    public static final int EMAIL_CONTACT_SEARCH_EVENT = 35104;
    public static final int EMAIL_CONTACT_SEARCH_RESULT_EVENT = 35106;
    public static final int EMAIL_VALIDATION_REQUIRED_EVENT = 35052;
    public static final int EMAIL_VALIDATION_WRONG_CODE_EVENT = 35123;
    public static final int ENTER_BACKGROUND_EVENT = 35112;
    public static final int FB_DID_LOGIN_EVENT = 35320;
    public static final int FINISH_PLAY_VIDEO_MAIL_EVENT = 35183;
    public static final int FORWARD_MESSAGE_RESULT_EVENT = 35280;
    public static final int GAME_ERROR_EVENT = 35226;
    public static final int INVITE_DISPLAY_MAIN_EVENT = 35039;
    public static final int INVITE_EMAIL_COMPOSER_EVENT = 35057;
    public static final int INVITE_EMAIL_SELECTION_EVENT = 35041;
    public static final int INVITE_RECOMMENDED_SELECTION_EVENT = 35060;
    public static final int INVITE_SMS_INSTRUCTION_EVENT = 35061;
    public static final int INVITE_SMS_SELECTION_EVENT = 35059;
    public static final int INVITE_SNS_COMPOSER_EVENT = 35217;
    public static final int INVITE_SNS_PUBLISH_RESULT_EVENT = 35218;
    public static final int IN_CALL_ALERT_EVENT = 35080;
    public static final int LAUNCH_LOG_REPORT_EMAIL_EVENT = 35036;
    public static final int LOAD_VGREETING_PLAYER_TYPE = 35266;
    public static final int LOGIN_COMPLETED_EVENT = 35011;
    public static final int LOGIN_ERROR_EVENT = 35001;
    public static final int LOGIN_EVENT = 35005;
    public static final int LOGIN_NOTIFICATION_EVENT = 35004;
    public static final int LOGIN_NOTIFICATION_USER_ACCEPTED_EVENT = 35003;
    public static final int MAKE_PREMIUM_CALL_EVENT = 35007;
    public static final int MISSED_CALL_EVENT = 35073;
    public static final int NETWORK_HIGH_BANDWIDTH_EVENT = 35079;
    public static final int NETWORK_LOW_BANDWIDTH_EVENT = 35077;
    public static final int OPEN_CONVERSATION_EVENT = 35270;
    public static final int OPEN_CONVERSATION_LIST_EVENT = 35272;
    public static final int PAUSE_PLAY_VIDEO_MAIL_EVENT = 35182;
    public static final int PHONENUMBER_CONTACT_SEARCH_EVENT = 35105;
    public static final int PHONENUMBER_CONTACT_SEARCH_RESULT_EVENT = 35107;
    public static final int PHONE_FORMATTED_EVENT = 35087;
    public static final int PLAY_MESSAGE_ERROR_EVENT = 35278;
    public static final int PLAY_VIDEO_MAIL_EVENT = 35180;
    public static final int PLAY_VIDEO_MESSAGE_EVENT = 35275;
    public static final int POST_PROCESS_PICTURE_EVENT = 35294;
    public static final int PROCESS_NOTIFICATION_EVENT = 35009;
    public static final int PRODUCT_CATALOG_EVENT = 35202;
    public static final int PRODUCT_CATALOG_WAITING_EVENT = 35201;
    public static final int PRODUCT_GREETING_WAITING_EVENT = 35262;
    public static final int PUSH_VALIDATION_REQUIRED_EVENT = 35054;
    public static final int QUERY_LEAVE_MESSAGE_EVENT = 35279;
    public static final int READ_STATUS_UPDATE_EVENT = 35321;
    public static final int RECORD_VIDEO_MAIL_EVENT = 35166;
    public static final int REGISTER_USER_NO_NETWORK_EVENT = 35090;
    public static final int REPORT_PURCHASE_RESULT_EVENT = 35200;
    public static final int RETRIEVE_OFFLINE_MESSAGE_RESULT_EVENT = 35284;
    public static final int SEARCH_AND_ADD_MAIN_EVENT = 35094;
    public static final int SELECT_CONTACT_EVENT = 35282;
    public static final int SEND_CALL_ACCEPTED_EVENT = 35021;
    public static final int SEND_CALL_INVITATION_EVENT = 35015;
    public static final int SEND_PERSONALINFO_EVENT = 35027;
    public static final int SEND_PICTURE_EVENT = 35296;
    public static final int SHAKE_MODE_CHANGE_EVENT = 35102;
    public static final int SHOW_ADVERTISEMENT_EVENT = 35288;
    public static final int SHOW_MORE_MESSAGE_EVENT = 35277;
    public static final int SMS_RATE_LIMITED_EVENT = 35114;
    public static final int SMS_VALIDATION_REQUIRED_EVENT = 35056;
    public static final int SNS_AUTH_FAILED_EVENT = 35221;
    public static final int SNS_PROCESSING_EVENT = 35220;
    public static final int SNS_REQUEST_AUTH_EVENT = 35219;
    public static final int START_PLAY_VIDEO_MAIL_EVENT = 35181;
    public static final int START_RECORDING_VIDEO_MAIL_EVENT = 35167;
    public static final int START_SMS_COMPOSE_EVENT = 35281;
    public static final int STOP_RECORDING_VIDEO_MAIL_EVENT = 35168;
    public static final int STOP_VIDEO_RINGBACK_EVENT = 35222;
    public static final int SUBSCRIBE_SERVICE_EVENT = 35185;
    public static final int SWITCH_AVATAR_TYPE = 35251;
    public static final int TAKE_PICTURE_EVENT = 35290;
    public static final int TEST_EVENT = 35400;
    public static final int TOGGLE_VIDEO_VIEW_BUTTON_BAR_EVENT = 35187;
    static final int UI_EVENT_START = 35000;
    public static final int UPDATE_CALL_LOG_EVENT = 35093;
    public static final int UPDATE_CONVERSATION_MESSAGE_NOTIFICATION_EVENT = 35283;
    public static final int UPDATE_CONVERSATION_SUMMARY_EVENT = 35273;
    public static final int UPDATE_REQUIRED_EVENT = 35075;
    public static final int UPDATE_TANGO_ALERTS_EVENT = 35038;
    public static final int UPDATE_TANGO_USERS_EVENT = 35037;
    public static final int UPLOAD_VIDEO_MAIL_FINISHED_EVENT = 35173;
    public static final int UPLOAD_VIDEO_MAIL_PROGRESS_EVENT = 35172;
    public static final int UPLOAD_VIDEO_MAIL_RESPONSE_EVENT = 35170;
    public static final int VALIDATION_CODE_INPUT_EVENT = 35098;
    public static final int VALIDATION_CODE_INPUT_WAIT_RESULT_EVENT = 35109;
    public static final int VALIDATION_CODE_REINPUT_EVENT = 35099;
    public static final int VALIDATION_CODE_REINPUT_WAIT_RESULT_EVENT = 35110;
    public static final int VALIDATION_CODE_REQUIRED_EVENT = 35097;
    public static final int VALIDATION_FAILED_TYPE_EVENT = 35065;
    public static final int VALIDATION_QUERY_OTHER_REGISTERED_DEVICE_EVENT = 35100;
    public static final int VALIDATION_REQUIRED_EVENT = 35051;
    public static final int VALIDATION_RESULT_EVENT = 35088;
    public static final int VALIDATION_SEND_CODE_RESULT_EVENT = 35111;
    public static final int VALIDATION_SHAKE_REQUIRED_EVENT = 35095;
    public static final int VALIDATION_SHAKE_REQUIRED_TIMEOUT_EVENT = 35096;
    public static final int VALIDATION_VERIFICATION_WITH_OTHER_DEVICE_EVENT = 35101;
    public static final int VGOOD_ANIMATION_COMPLETE_EVENT = 35194;
    public static final int VGOOD_CACHE_MISS_NOTIFICATION_EVENT = 35230;
    public static final int VGOOD_CACHE_MISS_TIMEOUT_EVENT = 35231;
    public static final int VGOOD_DEMO_ANIMATION_COMPLETE_EVENT = 35229;
    public static final int VGOOD_ERROR_EVENT = 35225;
    public static final int VGREETING_PRODUCT_CATALOG_EVENT = 35260;
    public static final int VGREETING_SEND_RESPONSE_EVENT = 35261;
    public static final int VIDEO_AVATAR_IN_PROGRESS_TYPE = 35243;
    public static final int VIDEO_IN_INITIALIZATION_EVENT = 35029;
    public static final int VIDEO_MAIL_CONFIGURATION_EVENT = 35153;
    public static final int VIDEO_MAIL_ERROR_RESULT_EVENT = 35155;
    public static final int VIDEO_MODE_CHANGED_EVENT = 35089;
    public static final int VIEW_PICTURE_EVENT = 35297;
  }

  public static final class message
  {
    static final int ACCEPT_CALL_TYPE = 30019;
    static final int ACKNOWLEDGE_CALLERROR_TYPE = 30025;
    static final int ACKNOWLEDGE_REGISTRATION_ERROR_TYPE = 30089;
    static final int ADD_CONTACT_REQUEST_TYPE = 30094;
    static final int ADD_FAVORITE_CONTACT_TYPE = 30220;
    static final int ADD_VIDEO_TYPE = 30013;
    static final int ADVERTISEMENT_CLICKED_MESSAGE_TYPE = 30289;
    static final int ADVERTISEMENT_SHOWN_MESSAGE_TYPE = 30288;
    static final int ALLOW_ACCESS_ADDRESSBOOK_TYPE = 30059;
    static final int ANCHOR_CHANGED_IN_CONVERSATION_PAGE_TYPE = 30298;
    static final int AUDIO_CONTROL_TYPE = 30033;
    static final int AVATAR_ADD_MESSAGE_TYPE = 30240;
    static final int AVATAR_BACKGROUND_CLEANUP_TYPE = 30250;
    static final int AVATAR_CHANGE_MESSAGE_TYPE = 30242;
    static final int AVATAR_DEMO_ANIMATION_TRACKING_TYPE = 30236;
    static final int AVATAR_REMOVE_MESSAGE_TYPE = 30241;
    static final int AVATAR_RENDER_STATUS_TYPE = 30243;
    static final int BACK_TO_AVATAR_PRODUCT_CATALOG_TYPE = 30247;
    static final int BACK_TO_VGOOD_PRODUCT_CATALOG_TYPE = 30226;
    static final int CALL_ERROR_TYPE = 30031;
    static final int CALL_QUALITY_SURVEY_DATA_TYPE = 30309;
    static final int CANCEL_APPSTORE_TYPE = 30216;
    static final int CANCEL_CALL_QUALITY_SURVEY_TYPE = 30239;
    static final int CANCEL_CHOOSE_PICTURE_TYPE = 30293;
    static final int CANCEL_CONTACT_DETAIL_TYPE = 30219;
    static final int CANCEL_FACEBOOK_LIKE_TYPE = 30217;
    static final int CANCEL_GAME_CATALOG_TYPE = 30302;
    static final int CANCEL_GAME_DEMO_TYPE = 30303;
    static final int CANCEL_IN_APP_PURCHASE_TYPE = 30199;
    static final int CANCEL_POSTCALL_TYPE = 30193;
    static final int CANCEL_POST_PROCESS_PICTURE_TYPE = 30295;
    static final int CANCEL_SELECT_CONTACT_TYPE = 30285;
    static final int CANCEL_SGIGGLE_INVITATION_TYPE = 30023;
    static final int CANCEL_TAKE_PICTURE_TYPE = 30291;
    static final int CANCEL_UPLOAD_VIDEO_MAIL_TYPE = 30175;
    static final int CANCEL_VGREETING_PLAYER_TYPE = 30268;
    static final int CANCEL_VGREETING_RECIPIENTS_SELECTION_TYPE = 30264;
    static final int CANCEL_VIDEO_GREETINGS_TYPE = 30260;
    static final int CANCEL_VIDEO_MAIL_RECEIVERS_SELECTION_TYPE = 30162;
    static final int CANCEL_VIEW_PICTURE_TYPE = 30297;
    static final int CHOOSE_PICTURE_TYPE = 30292;
    static final int CLEAR_BADGE_FOR_INVITES_TYPE = 30330;
    static final int CLEAR_MISSED_CALL_TYPE = 30079;
    static final int CLEAR_UNREAD_MISSED_CALL_NUMBER_TYPE = 30117;
    static final int CLOSE_CONVERSATION_TYPE = 30272;
    static final int COMPOSING_CONVERSATION_MESSAGE_TYPE = 30287;
    static final int CONFIRM_VIDEO_MAIL_NON_TANGO_NOTIFICATION_TYPE = 30163;
    static final int CONTACTS_DISPLAY_MAIN_TYPE = 30055;
    static final int CONTACT_SEARCH_CANCEL_TYPE = 30110;
    static final int CONTACT_SEARCH_FAILED_TYPE = 30109;
    static final int CONTACT_SEARCH_REQUEST_TYPE = 30093;
    static final int CONTACT_SEARCH_SUCCESS_TYPE = 30108;
    static final int CONTACT_SEARCH_TYPE_SWITCH_TYPE = 30113;
    static final int CONTACT_TANGO_CUSTOMER_SUPPORT_TYPE = 30102;
    static final int CONVERSATION_MESSAGE_NOTIFICATION_RECEIVED_TYPE = 30278;
    static final int DELETE_CALL_LOG_TYPE = 30092;
    static final int DELETE_CONVERSATION_TYPE = 30274;
    static final int DELETE_SINGLE_CONVERSATION_MESSAGE_TYPE = 30286;
    static final int DELETE_VIDEO_MAIL_TYPE = 30155;
    static final int DISABLE_POSTCALL_TYPE = 30232;
    static final int DISMISS_STORE_BADGE_TYPE = 30266;
    static final int DISMISS_UPLOAD_VIDEO_MAIL_FINISHED_TYPE = 30172;
    static final int DISPLAY_APPSTORE_TYPE = 30214;
    static final int DISPLAY_AVATAR_PAYMENT_UI_TYPE = 30244;
    static final int DISPLAY_AVATAR_PRODUCT_DETAILS_TYPE = 30246;
    static final int DISPLAY_FACEBOOK_LIKE_TYPE = 30215;
    static final int DISPLAY_GAME_CATALOG_TYPE = 30300;
    static final int DISPLAY_GAME_DEMO_TYPE = 30301;
    static final int DISPLAY_PERSONALINFO_TYPE = 30029;
    static final int DISPLAY_PRODUCT_DETAILS_TYPE = 30227;
    static final int DISPLAY_PRODUCT_INFO_UI_TYPE = 30197;
    static final int DISPLAY_SETTINGS_TYPE = 30057;
    static final int DISPLAY_STORE_TYPE = 30265;
    static final int DISPLAY_VGOOD_DEMO_SCREEN_TYPE = 30196;
    static final int DISPLAY_VGOOD_PAYMENT_UI_TYPE = 30189;
    static final int DISPLAY_WELCOME_SCREEN_UI_TYPE = 30205;
    static final int EDIT_VIDEO_MAIL_TYPE = 30151;
    static final int EMAIL_CONTACT_SEARCH_TYPE = 30104;
    static final int END_STATE_NO_CHANGE_TYPE = 30027;
    static final int FB_DID_LOGIN_TYPE = 30320;
    static final int FINISH_EDIT_VIDEO_MAIL_TYPE = 30152;
    static final int FINISH_PLAY_GAME_DEMO_TYPE = 30304;
    static final int FINISH_PLAY_VIDEO_MAIL_TYPE = 30182;
    static final int FINISH_PLAY_VIDEO_MESSAGE_TYPE = 30276;
    static final int FINISH_SMS_COMPOSE_TYPE = 30282;
    static final int FINISH_UPLOAD_VIDEO_MAIL_TYPE = 30171;
    static final int FINISH_VIDEO_RINGBACK_TYPE = 30225;
    static final int FORWARD_MESSAGE_REQUEST_TYPE = 30280;
    static final int FORWARD_TO_POSTCALL_CONTENT_TYPE = 30213;
    static final int FORWARD_VIDEO_MAIL_TYPE = 30160;
    static final int GAME_MODE_OFF_TYPE = 30307;
    static final int GAME_MODE_ON_TYPE = 30306;
    static final int INITIATE_VGOOD_TYPE = 30039;
    static final int INVITE_CONTACT_TYPE = 30090;
    static final int INVITE_DISPLAY_MAIN_TYPE = 30049;
    static final int INVITE_EMAIL_COMPOSER_TYPE = 30061;
    static final int INVITE_EMAIL_SELECTION_TYPE = 30051;
    static final int INVITE_EMAIL_SEND_TYPE = 30053;
    static final int INVITE_RECOMMENDED_SELECTED_TYPE = 30069;
    static final int INVITE_RECOMMENDED_SELECTION_TYPE = 30068;
    static final int INVITE_SMS_SELECTED_TYPE = 30067;
    static final int INVITE_SMS_SELECTION_TYPE = 30065;
    static final int INVITE_SMS_SEND_TYPE = 30070;
    static final int INVITE_SNS_PUBLISH_TYPE = 30207;
    static final int INVITE_VIA_SNS_TYPE = 30206;
    static final int KEEP_PUSH_NOTIFICATION_ALIVE_TYPE = 30087;
    static final int LAUNCH_LOG_REPORT_EMAIL_TYPE = 30036;
    static final int LEAVE_VIDEO_MAIL_TYPE = 30165;
    static final int LIKE_VIDEO_RINGBACK_TYPE = 30223;
    static final int LOAD_VGREETING_PLAYER_TYPE = 30267;
    static final int LOCKED_VGOOD_SELECTED_TYPE = 30204;
    static final int LOGIN_NOTIFICATION_TYPE = 30002;
    static final int LOGIN_NOTIFICATION_USER_ACCEPTED_TYPE = 30001;
    static final int LOGIN_TYPE = 30005;
    static final int MAKE_CALL_TYPE = 30007;
    static final int MAKE_PREMIUM_CALL_TYPE = 30003;
    static final int NAVIGATE_BACK_TYPE = 30308;
    static final int OPEN_CONVERSATION_LIST_TYPE = 30273;
    static final int OPEN_CONVERSATION_TYPE = 30270;
    static final int PAUSE_PLAY_VIDEO_MAIL_TYPE = 30181;
    static final int PHONENUMBER_CONTACT_SEARCH_TYPE = 30105;
    static final int PLAY_VGOOD_VIDEO_TYPE = 30201;
    static final int PLAY_VIDEO_MAIL_TYPE = 30180;
    static final int PLAY_VIDEO_MESSAGE_TYPE = 30275;
    static final int POST_PROCESS_PICTURE_TYPE = 30294;
    static final int PRODUCT_CATALOG_FINISHED_TYPE = 30212;
    static final int PRODUCT_CATALOG_REQUEST_TYPE = 30211;
    static final int PRODUCT_INFO_OK_TYPE = 30195;
    static final int PURCHASE_ATTEMPT_TYPE = 30233;
    static final int PURCHASE_AVATAR_TYPE = 30245;
    static final int PURCHASE_GAME_TYPE = 30305;
    static final int PURCHASE_VGOOD_TYPE = 30191;
    static final int PUT_APP_IN_BACKGROUND_TYPE = 30006;
    static final int PUT_APP_IN_FOREGROUND_TYPE = 30086;
    static final int QUERY_LEAVE_MESSAGE_TYPE = 30279;
    static final int QUERY_UNREAD_MISSED_CALL_NUMBER_TYPE = 30116;
    static final int RECEIVED_CALL_CANCEL_TYPE = 30350;
    static final int RECEIVE_MESSAGE_NOTIFICATION_TYPE = 30106;
    static final int RECEIVE_PUSH_NOTIFICATION_TYPE = 30004;
    static final int REGISTER_USER_TYPE = 30043;
    static final int REJECT_CALL_TYPE = 30009;
    static final int REMOVE_FAVORITE_CONTACT_TYPE = 30221;
    static final int REMOVE_VIDEO_TYPE = 30015;
    static final int REPORT_PURCHASED_AVATAR_TYPE = 30251;
    static final int REPORT_PURCHASE_TYPE = 30210;
    static final int REQUEST_APP_LOG_TYPE = 30114;
    static final int REQUEST_CALL_LOG_TYPE = 30091;
    static final int REQUEST_FAQ_TYPE = 30071;
    static final int REQUEST_FILTER_CONTACT_TYPE = 30081;
    static final int REQUEST_PHONE_FORMATTING_TYPE = 30084;
    static final int SAVE_PERSONALINFO_TYPE = 30021;
    static final int SEARCH_AND_ADD_DISPLAY_MAIN_TYPE = 30112;
    static final int SELECT_CONTACT_REQUEST_TYPE = 30283;
    static final int SELECT_CONTACT_RESULT_TYPE = 30284;
    static final int SEND_CONVERSATION_MESSAGE_TYPE = 30271;
    static final int SEND_INVITE_LIST = 30037;
    static final int SEND_SGIGGLE_INVITATION_TYPE = 30017;
    static final int SEND_VALIDATION_CODE_TYPE = 30083;
    static final int SEND_VGREETING_TYPE = 30262;
    static final int SHAKE_ACTION_DETECTED_TYPE = 30103;
    static final int SHOW_MORE_MESSAGE_TYPE = 30277;
    static final int SKIP_VIDEO_RINGBACK_TYPE = 30224;
    static final int SNS_AUTH_RESULT_TYPE = 30208;
    static final int SNS_CANCEL_PROCESS_TYPE = 30209;
    static final int SNS_PROCESSING_TIMEOUT_TYPE = 30222;
    static final int START_DEMO_AVATAR_TYPE = 30248;
    static final int START_RECORDING_VIDEO_MAIL_TYPE = 30166;
    static final int START_SMS_COMPOSE_TYPE = 30281;
    static final int STATS_COLLECTOR_LOG_TYPE = 30310;
    static final int STOP_BACKGROUND_TASK_TYPE = 30228;
    static final int STOP_RECORDING_VIDEO_MAIL_TYPE = 30167;
    static final int SUBSCRIBE_SERVICE_TYPE = 30185;
    static final int SWITCH_CAMERA_TYPE = 30077;
    static final int TAKE_PICTURE_TYPE = 30290;
    static final int TERMINATE_CALL_TYPE = 30011;
    static final int TEST_TYPE = 30100;
    static final int TOGGLE_VIDEO_VIEW_BUTTON_BAR_TYPE = 30187;
    static final int UPDATE_DEVICE_TOKEN_TYPE = 30088;
    static final int UPLOAD_VIDEO_MAIL_TYPE = 30170;
    static final int VALIDATION_CODE_REQUIRED_SEND_CODE_TYPE = 30098;
    static final int VALIDATION_CODE_REQUIRED_SUBMIT_TYPE = 30097;
    static final int VALIDATION_DISMISS_VERIFICATION_WITH_OTHER_DEVICE_TYPE = 30101;
    static final int VALIDATION_OTHER_REGISTERED_DEVICE_TYPE = 30099;
    static final int VALIDATION_REQUEST_TYPE = 30063;
    static final int VALIDATION_SEND_CODE_FAILED_TYPE = 30111;
    static final int VALIDATION_SEND_CODE_TYPE = 30107;
    static final int VALIDATION_SHAKE_REQUIRED_RETRY_TYPE = 30095;
    static final int VALIDATION_USER_INPUTED_CODE_TYPE = 30123;
    static final int VALIDATION_USER_NO_CODE_TYPE = 30124;
    static final int VALIDATION_VERIFY_PHONE_NUMBER_TYPE = 30096;
    static final int VGOOD_ANIMATION_COMPLETE_TYPE = 30203;
    static final int VGOOD_DEMO_ANIMATION_COMPLETE_TYPE = 30229;
    static final int VGOOD_DEMO_ANIMATION_TRACKING_TYPE = 30234;
    static final int VGREETING_PRODUCT_CATALOG_REQUEST_TYPE = 30261;
    static final int VGREETING_RECIPIENTS_SELECTION_TYPE = 30263;
    static final int VIDEO_MAIL_RECEIVERS_TYPE = 30161;
    static final int VIEW_CONTACT_DETAIL_TYPE = 30218;
    static final int VIEW_PICTURE_TYPE = 30296;
    static final int VMAIL_ATTEMPT_PURCHASE_TRACKING_TYPE = 30235;
    static final int WAND_PRESSED_TYPE = 30230;
    static final int WELCOME_GO_TO_TYPE = 30231;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.media_engine.MediaEngineMessage
 * JD-Core Version:    0.6.2
 */