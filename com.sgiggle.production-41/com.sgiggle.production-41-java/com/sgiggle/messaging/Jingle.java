package com.sgiggle.messaging;

import com.sgiggle.util.Log;

public class Jingle
{
  public static void sendAcceptSessionMessage()
  {
    Log.log(80, "JAVA: Sending Jingle a Accept message");
    KeyValueMap localKeyValueMap = new KeyValueMap();
    MessageRouter.getInstance().postMessage("jingle", new KeyValueMapMessage(10015, localKeyValueMap));
  }

  public static void sendCloseSessionMessage(String paramString1, String paramString2)
  {
    Log.log(80, "JAVA: Sending Jingle a close session message");
    KeyValueMap localKeyValueMap = new KeyValueMap();
    localKeyValueMap.set("sessionId", paramString2);
    localKeyValueMap.set("jid", paramString1);
    MessageRouter.getInstance().postMessage("jingle", new KeyValueMapMessage(10015, localKeyValueMap));
  }

  public static void sendLoginMessage(String paramString1, int paramInt, String paramString2, String paramString3)
  {
    Log.log(80, "JAVA: Sending Jingle a login message");
    KeyValueMap localKeyValueMap = new KeyValueMap();
    localKeyValueMap.set("xmppServerName", paramString1);
    localKeyValueMap.set("xmppServerPort", Integer.toString(paramInt));
    localKeyValueMap.set("userName", paramString2 + "@prosody");
    localKeyValueMap.set("userPassword", paramString3);
    localKeyValueMap.set("resource", "prosody");
    MessageRouter.getInstance().postMessage("jingle", new KeyValueMapMessage(10003, localKeyValueMap));
  }

  public static void sendLogoutMessage()
  {
    Log.log(80, "JAVA: Sending Jingle a logout message");
    KeyValueMap localKeyValueMap = new KeyValueMap();
    MessageRouter.getInstance().postMessage("jingle", new KeyValueMapMessage(10005, localKeyValueMap));
  }

  public static void sendPresenceMessage()
  {
    Log.log(80, "JAVA: Sending Jingle a Presence message");
    KeyValueMap localKeyValueMap = new KeyValueMap();
    localKeyValueMap.set("show", "mobile");
    MessageRouter.getInstance().postMessage("jingle", new KeyValueMapMessage(10061, localKeyValueMap));
  }

  public static void sendRegistrationMessage(String paramString1, int paramInt, String paramString2, String paramString3)
  {
    Log.log(80, "JAVA: Sending Jingle a login message");
    KeyValueMap localKeyValueMap = new KeyValueMap();
    localKeyValueMap.set("xmppServerName", paramString1);
    localKeyValueMap.set("xmppServerPort", Integer.toString(paramInt));
    localKeyValueMap.set("userName", paramString2);
    localKeyValueMap.set("userPassword", paramString3);
    localKeyValueMap.set("resource", "android");
    MessageRouter.getInstance().postMessage("jingle", new KeyValueMapMessage(10001, localKeyValueMap));
  }

  public static void sendStartSessionMessage(String paramString)
  {
    Log.log(80, "JAVA: Sending Jingle a start message " + paramString);
    KeyValueMap localKeyValueMap = new KeyValueMap();
    localKeyValueMap.set("jid", paramString + "@prosody");
    localKeyValueMap.set("resource", "prosody");
    MessageRouter.getInstance().postMessage("jingle", new KeyValueMapMessage(10011, localKeyValueMap));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.Jingle
 * JD-Core Version:    0.6.2
 */