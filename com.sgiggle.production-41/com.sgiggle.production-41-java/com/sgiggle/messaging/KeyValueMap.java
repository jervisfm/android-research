package com.sgiggle.messaging;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

public class KeyValueMap
{
  static final String KEY_FLAT_KEY = "__key__";
  static final String KEY_SIZE = "__size__";
  private HashMap<String, String> m_map = new HashMap();

  static ParseResult parseFlatKey(String paramString)
    throws Error
  {
    int i = paramString.indexOf("__key__");
    if (i == -1)
      throw new Error("Invalid flat key: " + paramString);
    ParseResult localParseResult = new ParseResult();
    localParseResult.m_index = Integer.parseInt(paramString.substring(0, i - 1));
    localParseResult.m_key = paramString.substring(i + "__key__".length());
    return localParseResult;
  }

  public Vector<KeyValueMap> asKeyValueMapVector()
    throws Error
  {
    Vector localVector = new Vector();
    if (!contains("__size__"))
      throw new Error("No '__size__' key found.");
    localVector.setSize(Integer.parseInt(get("__size__")));
    for (int i = 0; i < localVector.size(); i++)
      localVector.setElementAt(new KeyValueMap(), i);
    Iterator localIterator = this.m_map.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str1 = (String)localIterator.next();
      String str2 = get(str1);
      if (str1 != "__size__")
      {
        ParseResult localParseResult = parseFlatKey(str1);
        if (localParseResult.m_index < localVector.size())
          throw new Error("Invalid index: " + Integer.toString(localParseResult.m_index));
        ((KeyValueMap)localVector.elementAt(localParseResult.m_index)).set(localParseResult.m_key, str2);
      }
    }
    return localVector;
  }

  public String[] asPairArray()
  {
    String[] arrayOfString = new String[2 * this.m_map.size()];
    Iterator localIterator = this.m_map.keySet().iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      Object localObject1 = localIterator.next();
      Object localObject2 = this.m_map.get(localObject1);
      int j = i + 1;
      arrayOfString[i] = ((String)localObject1);
      i = j + 1;
      arrayOfString[j] = ((String)localObject2);
    }
    return arrayOfString;
  }

  public boolean contains(String paramString)
  {
    return this.m_map.containsKey(paramString);
  }

  public String get(String paramString)
  {
    return (String)this.m_map.get(paramString);
  }

  public void set(String paramString1, String paramString2)
  {
    this.m_map.put(paramString1, paramString2);
  }

  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Iterator localIterator = this.m_map.keySet().iterator();
    while (localIterator.hasNext())
    {
      if (0 == 0)
        localStringBuffer.append(";");
      Object localObject1 = localIterator.next();
      Object localObject2 = this.m_map.get(localObject1);
      localStringBuffer.append((String)localObject1);
      localStringBuffer.append("=");
      localStringBuffer.append((String)localObject2);
    }
    return localStringBuffer.toString();
  }

  static class ParseResult
  {
    public int m_index;
    public String m_key;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.KeyValueMap
 * JD-Core Version:    0.6.2
 */