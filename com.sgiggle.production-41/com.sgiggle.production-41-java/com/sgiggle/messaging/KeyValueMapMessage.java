package com.sgiggle.messaging;

public class KeyValueMapMessage extends SimpleMessage
{
  private KeyValueMap m_values;

  public KeyValueMapMessage(int paramInt, long paramLong, KeyValueMap paramKeyValueMap)
  {
    super(paramInt, paramLong);
    this.m_values = paramKeyValueMap;
  }

  public KeyValueMapMessage(int paramInt, KeyValueMap paramKeyValueMap)
  {
    super(paramInt);
    this.m_values = paramKeyValueMap;
  }

  public final Message clone()
  {
    return new KeyValueMapMessage(getType(), this.m_values);
  }

  public String get(String paramString)
  {
    return this.m_values.get(paramString);
  }

  public KeyValueMap getValues()
  {
    return this.m_values;
  }

  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("t=");
    localStringBuffer.append(getType());
    localStringBuffer.append(", ");
    localStringBuffer.append("s=");
    localStringBuffer.append(getSequenceId());
    localStringBuffer.append(", ");
    localStringBuffer.append("v=");
    localStringBuffer.append(this.m_values.toString());
    return localStringBuffer.toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.KeyValueMapMessage
 * JD-Core Version:    0.6.2
 */