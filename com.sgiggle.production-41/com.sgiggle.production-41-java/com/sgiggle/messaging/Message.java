package com.sgiggle.messaging;

public abstract class Message
{
  public static final String COMPONENT_ALL = "all";
  public static final String COMPONENT_GUI = "gui";
  public static final String COMPONENT_JINGLE = "jingle";
  public static final String COMPONENT_TESTING = "testing";
  public static final String COMPONENT_UI = "ui";
  public static final String COMPONENT_UNDEFINED = "";
  public static final int MESSAGE_GUI_END = 29999;
  public static final int MESSAGE_GUI_START = 20000;
  public static final int MESSAGE_JINGLE_END = 19999;
  public static final int MESSAGE_JINGLE_START = 10000;
  public static final int MESSAGE_RESERVED_END = 9999;
  public static final int MESSAGE_RESERVED_START = 1;
  public static final int MESSAGE_TESTING_END = 1000009999;
  public static final int MESSAGE_TESTING_START = 1000000000;
  public static final int MESSAGE_UI_END = 39999;
  public static final int MESSAGE_UI_START = 30000;
  public static final int MESSAGE_UNDEFINED;

  public abstract Message clone();

  public abstract void deserialize(byte[] paramArrayOfByte)
    throws Error;

  public abstract long getSequenceId();

  public abstract int getType();

  public abstract byte[] serialize()
    throws Error;

  public abstract String toString();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.Message
 * JD-Core Version:    0.6.2
 */