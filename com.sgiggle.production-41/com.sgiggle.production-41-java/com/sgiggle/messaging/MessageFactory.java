package com.sgiggle.messaging;

public abstract interface MessageFactory
{
  public abstract Message create(int paramInt);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.MessageFactory
 * JD-Core Version:    0.6.2
 */