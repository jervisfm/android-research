package com.sgiggle.messaging;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class MessageFactoryRegistry
{
  private static MessageFactoryRegistry s_instance = new MessageFactoryRegistry();
  private static Object s_lock = new Object();
  private HashMap<Integer, MessageFactory> m_factories = new HashMap();
  private int m_nextId;

  public static MessageFactoryRegistry getInstance()
  {
    synchronized (s_lock)
    {
      if (s_instance == null)
        s_instance = new MessageFactoryRegistry();
      MessageFactoryRegistry localMessageFactoryRegistry = s_instance;
      return localMessageFactoryRegistry;
    }
  }

  public Message create(int paramInt)
  {
    synchronized (this.m_factories)
    {
      Iterator localIterator = this.m_factories.keySet().iterator();
      while (localIterator.hasNext())
      {
        Message localMessage = ((MessageFactory)this.m_factories.get(localIterator.next())).create(paramInt);
        if (localMessage != null)
          return localMessage;
      }
      return null;
    }
  }

  public int registerFactory(MessageFactory paramMessageFactory)
  {
    synchronized (this.m_factories)
    {
      int i = this.m_nextId;
      this.m_nextId = (i + 1);
      this.m_factories.put(new Integer(i), paramMessageFactory);
      return i;
    }
  }

  public void unregisterFactory(int paramInt)
  {
    synchronized (this.m_factories)
    {
      this.m_factories.remove(new Integer(paramInt));
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.MessageFactoryRegistry
 * JD-Core Version:    0.6.2
 */