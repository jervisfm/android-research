package com.sgiggle.messaging;

public class MessageParamNames
{
  public static final String AVAILABLE = "available";
  public static final String ERROR = "error";
  public static final String JID = "jid";
  public static final String PAROSODY = "prosody";
  public static final String PRESENCE = "presence";
  public static final String RESOURCE = "resource";
  public static final String SESSIONID = "sessionId";
  public static final String SHOW = "show";
  public static final String STATE = "state";
  public static final String STATUS = "status";
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.MessageParamNames
 * JD-Core Version:    0.6.2
 */