package com.sgiggle.messaging;

import com.sgiggle.util.Log;
import java.util.HashMap;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MessageRouter
{
  private static MessageRouter s_instance = new MessageRouter();
  private long m_ackTimeoutMilliseconds = 0L;
  private final Condition m_condition = this.m_lock.newCondition();
  private boolean m_enteredBackgroundFlag = false;
  private final Lock m_lock = new ReentrantLock();
  private ReceiverTable m_receivers = new ReceiverTable();
  private MessageGetterThread m_thread = new MessageGetterThread();

  public MessageRouter()
  {
    this.m_thread.start();
  }

  public static MessageRouter getInstance()
  {
    return s_instance;
  }

  private native GNMResult getNextMessage();

  public static void initialize()
  {
    Log.log(80, "JAVA: Initializing MessageRouter.");
    if (s_instance != null)
    {
      Log.log(80, "JAVA: MessageRouter already initialized");
      return;
    }
    s_instance = new MessageRouter();
  }

  private native void post(String paramString, int paramInt, byte[] paramArrayOfByte);

  public static void shutdown()
  {
    if (s_instance == null)
      return;
    s_instance.stop();
    s_instance = null;
  }

  public void ack()
  {
    this.m_lock.lock();
    try
    {
      this.m_condition.signalAll();
      return;
    }
    finally
    {
      this.m_lock.unlock();
    }
  }

  public void enterBackground()
  {
    Log.log(80, "MessageAgentWithAck:enterBackground()");
    this.m_lock.lock();
    this.m_enteredBackgroundFlag = true;
    this.m_lock.unlock();
  }

  public native String getMessageName(int paramInt);

  public native long getNextSequenceId();

  public void postMessage(String paramString, Message paramMessage)
  {
    post(paramString, paramMessage.getType(), paramMessage.serialize());
  }

  public void registerReceiver(String paramString, MessageReceiver paramMessageReceiver)
  {
    synchronized (this.m_receivers)
    {
      this.m_receivers.add(paramString, paramMessageReceiver);
      return;
    }
  }

  public void setAckTimeout(long paramLong)
  {
    this.m_lock.lock();
    this.m_ackTimeoutMilliseconds = paramLong;
    this.m_lock.unlock();
  }

  public void stop()
  {
    this.m_thread.setStopped();
    try
    {
      this.m_thread.join();
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
    }
  }

  public void unregisterReceiver(String paramString, MessageReceiver paramMessageReceiver)
  {
    synchronized (this.m_receivers)
    {
      this.m_receivers.remove(paramString, paramMessageReceiver);
      return;
    }
  }

  public static final class GNMResult
  {
    public byte[] m_payload;
    public String m_target;
    public int m_type;
  }

  private class MessageGetterThread extends Thread
  {
    private boolean m_running = true;

    static
    {
      if (!MessageRouter.class.desiredAssertionStatus());
      for (boolean bool = true; ; bool = false)
      {
        $assertionsDisabled = bool;
        return;
      }
    }

    public MessageGetterThread()
    {
      super();
    }

    private void dispatchMessage(String paramString, Message paramMessage)
    {
      MessageRouter.ReceiverVector localReceiverVector;
      synchronized (MessageRouter.s_instance.m_receivers)
      {
        localReceiverVector = MessageRouter.s_instance.m_receivers.getInterested(paramString);
        if (localReceiverVector == null)
          return;
        Log.log(80, "Message '" + paramMessage + "' dispatched to " + paramString);
        if ((!$assertionsDisabled) && (localReceiverVector.size() != 1))
          throw new AssertionError();
      }
      for (int i = 0; i < localReceiverVector.size(); i++)
        localReceiverVector.elementAt(i).receiveMessage(paramMessage);
    }

    private boolean isRunning()
    {
      try
      {
        boolean bool = this.m_running;
        return bool;
      }
      finally
      {
      }
    }

    private Message parseMessage(MessageRouter.GNMResult paramGNMResult)
    {
      if (paramGNMResult.m_payload == null)
      {
        Log.log(16, 80, "MessageRouter: Cannot parse null message payload.");
        return null;
      }
      Message localMessage = MessageFactoryRegistry.getInstance().create(paramGNMResult.m_type);
      if (localMessage == null)
        return null;
      try
      {
        localMessage.deserialize(paramGNMResult.m_payload);
        return localMessage;
      }
      catch (Error localError)
      {
        Log.log(16, 80, "MessageRouter: Error parsing message type " + paramGNMResult.m_type + ": " + localError.getMessage());
      }
      return null;
    }

    public void run()
    {
      while (isRunning())
      {
        Log.log(80, "waiting for message...");
        MessageRouter.GNMResult localGNMResult = MessageRouter.s_instance.getNextMessage();
        Log.log(80, "MessageRouter: Got message target=" + localGNMResult.m_target + "; type=" + localGNMResult.m_type);
        Message localMessage = parseMessage(localGNMResult);
        if (localMessage != null)
        {
          MessageRouter.s_instance.m_lock.lock();
          MessageRouter.access$302(MessageRouter.this, false);
          try
          {
            dispatchMessage(localGNMResult.m_target, localMessage);
            long l = TimeUnit.MILLISECONDS.toNanos(MessageRouter.this.m_ackTimeoutMilliseconds);
            Log.log(80, "waiting " + localMessage + "....");
            if (MessageRouter.this.m_condition.awaitNanos(l) <= 0L)
            {
              if (MessageRouter.this.m_enteredBackgroundFlag)
                Log.log(16, 80, "MessageRouter: TIMEOUT: ack not received after app entered background, ignore this.");
              while (true)
              {
                MessageRouter.s_instance.m_lock.unlock();
                break;
                Log.log(16, 80, "MessageRouter: TIMEOUT: ack not received, contiue to get next message. " + localMessage);
              }
            }
          }
          catch (InterruptedException localInterruptedException)
          {
            while (true)
            {
              Log.log(16, 80, "MessageRouter: the waiting for ack is interrupted");
              MessageRouter.s_instance.m_lock.unlock();
              break;
              Log.log(80, "MessageRouter: wake up by ack, continue to get next message.");
            }
          }
          finally
          {
            MessageRouter.s_instance.m_lock.unlock();
          }
        }
      }
    }

    public void setStopped()
    {
      try
      {
        this.m_running = false;
        MessageRouter.this.m_condition.signal();
        return;
      }
      finally
      {
      }
    }
  }

  private class ReceiverTable
  {
    private HashMap<String, MessageRouter.ReceiverVector> m_map = new HashMap();

    ReceiverTable()
    {
    }

    void add(String paramString, MessageReceiver paramMessageReceiver)
    {
      MessageRouter.ReceiverVector localReceiverVector;
      if (this.m_map.containsKey(paramString))
        localReceiverVector = (MessageRouter.ReceiverVector)this.m_map.get(paramString);
      while (true)
      {
        localReceiverVector.add(paramMessageReceiver);
        return;
        localReceiverVector = new MessageRouter.ReceiverVector(MessageRouter.this);
        this.m_map.put(paramString, localReceiverVector);
      }
    }

    MessageRouter.ReceiverVector getInterested(String paramString)
    {
      if (!this.m_map.containsKey(paramString))
        return null;
      return (MessageRouter.ReceiverVector)this.m_map.get(paramString);
    }

    void remove(String paramString, MessageReceiver paramMessageReceiver)
    {
      if (!this.m_map.containsKey(paramString))
        return;
      ((MessageRouter.ReceiverVector)this.m_map.get(paramString)).remove(paramMessageReceiver);
    }
  }

  private class ReceiverVector
  {
    private Vector<MessageReceiver> m_vector = new Vector();

    ReceiverVector()
    {
    }

    void add(MessageReceiver paramMessageReceiver)
    {
      this.m_vector.add(paramMessageReceiver);
    }

    MessageReceiver elementAt(int paramInt)
    {
      return (MessageReceiver)this.m_vector.elementAt(paramInt);
    }

    void remove(MessageReceiver paramMessageReceiver)
    {
      this.m_vector.remove(paramMessageReceiver);
    }

    int size()
    {
      return this.m_vector.size();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.MessageRouter
 * JD-Core Version:    0.6.2
 */