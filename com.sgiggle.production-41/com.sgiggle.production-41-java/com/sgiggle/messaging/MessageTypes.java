package com.sgiggle.messaging;

public final class MessageTypes
{
  public static final int kAcceptBuddy = 10053;
  public static final int kAcceptSession = 10015;
  public static final int kAcceptedBuddy = 10054;
  public static final int kAcceptedSession = 10016;
  public static final int kAutoReply = 10063;
  public static final int kCloseSession = 10019;
  public static final int kClosedSession = 10020;
  public static final int kConnectUser = 10003;
  public static final int kDisConnectUser = 10005;
  public static final int kErrorReport = 10200;
  public static final int kFilterContact = 10077;
  public static final int kFilterContactUpdate = 10079;
  public static final int kGetBuddyList = 10031;
  public static final int kGetVCard = 10041;
  public static final int kInviteBuddy = 10051;
  public static final int kInvitedBuddy = 10052;
  public static final int kJINGLEOFFESET = 10000;
  public static final int kReceiveBuddyList = 10032;
  public static final int kReceiveBuddyMsg = 10046;
  public static final int kReceivePresence = 10062;
  public static final int kReceivedVCard = 10042;
  public static final int kRegisterUser = 10001;
  public static final int kRegistrationValidationNone = 10085;
  public static final int kRejectBuddy = 10055;
  public static final int kRejectSession = 10017;
  public static final int kRejectedBuddy = 10056;
  public static final int kRejectedSession = 10018;
  public static final int kRequestSession = 10014;
  public static final int kSendBuddyMsg = 10045;
  public static final int kSendPresence = 10061;
  public static final int kSetVCard = 10043;
  public static final int kStartSession = 10011;
  public static final int kStartedSession = 10012;
  public static final int kStateChanged = 10100;
  public static final int kkTriggerRegisterOnContactsLoaded = 10002;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.MessageTypes
 * JD-Core Version:    0.6.2
 */