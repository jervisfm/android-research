package com.sgiggle.messaging;

import com.google.protobuf.GeneratedMessageLite;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Base;
import com.sgiggle.xmpp.SessionMessages.Base.Builder;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SerializableMessage<TPAYLOAD extends GeneratedMessageLite> extends Message
{
  private String m_className;
  private Class<?> m_clazz;
  private TPAYLOAD m_payload;

  public SerializableMessage()
  {
  }

  public SerializableMessage(TPAYLOAD paramTPAYLOAD)
  {
    this.m_payload = paramTPAYLOAD;
    this.m_className = this.m_payload.getClass().getName();
    try
    {
      this.m_clazz = getClass().getClassLoader().loadClass(this.m_className);
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      Log.log(80, "j: failed to determine class for payload.");
    }
  }

  private SessionMessages.Base getBase()
  {
    try
    {
      Method localMethod = this.m_clazz.getMethod("getBase", (Class[])null);
      try
      {
        SessionMessages.Base localBase = (SessionMessages.Base)localMethod.invoke(this.m_payload, (Object[])null);
        return localBase;
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        Log.log(80, "j: getBase(1) caught exception: " + localIllegalAccessException.getMessage());
      }
      catch (InvocationTargetException localInvocationTargetException)
      {
        Log.log(80, "j: getBase(2) caught exception: " + localInvocationTargetException.getMessage());
      }
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      Log.log(80, "j: getBase(3) caught exception: " + localNoSuchMethodException.getMessage());
    }
    return null;
  }

  protected static SessionMessages.Base makeBase(int paramInt)
  {
    return makeBase(paramInt, 0L);
  }

  protected static SessionMessages.Base makeBase(int paramInt, long paramLong)
  {
    if (paramLong == 0L);
    for (long l = MessageRouter.getInstance().getNextSequenceId(); ; l = paramLong)
      return SessionMessages.Base.newBuilder().setType(paramInt).setSequenceId(l).build();
  }

  public Message clone()
  {
    return null;
  }

  // ERROR //
  public void deserialize(byte[] paramArrayOfByte)
    throws Error
  {
    // Byte code:
    //   0: iconst_1
    //   1: anewarray 30	java/lang/Class
    //   4: dup
    //   5: iconst_0
    //   6: ldc 149
    //   8: aastore
    //   9: astore_2
    //   10: aload_0
    //   11: getfield 48	com/sgiggle/messaging/SerializableMessage:m_clazz	Ljava/lang/Class;
    //   14: ldc 151
    //   16: aload_2
    //   17: invokevirtual 71	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   20: astore 4
    //   22: aload_0
    //   23: aload 4
    //   25: aconst_null
    //   26: iconst_1
    //   27: anewarray 24	java/lang/Object
    //   30: dup
    //   31: iconst_0
    //   32: aload_1
    //   33: aastore
    //   34: invokevirtual 79	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   37: checkcast 153	com/google/protobuf/GeneratedMessageLite
    //   40: putfield 22	com/sgiggle/messaging/SerializableMessage:m_payload	Lcom/google/protobuf/GeneratedMessageLite;
    //   43: return
    //   44: astore 7
    //   46: new 145	java/lang/Error
    //   49: dup
    //   50: aload 7
    //   52: invokevirtual 154	java/lang/IllegalArgumentException:getMessage	()Ljava/lang/String;
    //   55: invokespecial 157	java/lang/Error:<init>	(Ljava/lang/String;)V
    //   58: athrow
    //   59: astore_3
    //   60: new 145	java/lang/Error
    //   63: dup
    //   64: aload_3
    //   65: invokevirtual 102	java/lang/NoSuchMethodException:getMessage	()Ljava/lang/String;
    //   68: invokespecial 157	java/lang/Error:<init>	(Ljava/lang/String;)V
    //   71: athrow
    //   72: astore 6
    //   74: new 145	java/lang/Error
    //   77: dup
    //   78: aload 6
    //   80: invokevirtual 93	java/lang/IllegalAccessException:getMessage	()Ljava/lang/String;
    //   83: invokespecial 157	java/lang/Error:<init>	(Ljava/lang/String;)V
    //   86: athrow
    //   87: astore 5
    //   89: new 145	java/lang/Error
    //   92: dup
    //   93: aload 5
    //   95: invokevirtual 99	java/lang/reflect/InvocationTargetException:getMessage	()Ljava/lang/String;
    //   98: invokespecial 157	java/lang/Error:<init>	(Ljava/lang/String;)V
    //   101: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   22	43	44	java/lang/IllegalArgumentException
    //   0	22	59	java/lang/NoSuchMethodException
    //   22	43	59	java/lang/NoSuchMethodException
    //   46	59	59	java/lang/NoSuchMethodException
    //   74	87	59	java/lang/NoSuchMethodException
    //   89	102	59	java/lang/NoSuchMethodException
    //   22	43	72	java/lang/IllegalAccessException
    //   22	43	87	java/lang/reflect/InvocationTargetException
  }

  public long getSequenceId()
  {
    return getBase().getSequenceId();
  }

  public int getType()
  {
    return getBase().getType();
  }

  public TPAYLOAD payload()
  {
    return this.m_payload;
  }

  public byte[] serialize()
    throws Error
  {
    return this.m_payload.toByteArray();
  }

  public String toString()
  {
    return MessageRouter.getInstance().getMessageName(getType());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.SerializableMessage
 * JD-Core Version:    0.6.2
 */