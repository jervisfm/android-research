package com.sgiggle.messaging;

public final class SessionState
{
  public static final int STATE_DEINIT = 13;
  public static final int STATE_INIT = 0;
  public static final int STATE_INPROGRESS = 12;
  public static final int STATE_RECEIVEDACCEPT = 4;
  public static final int STATE_RECEIVEDINITIATE = 2;
  public static final int STATE_RECEIVEDMODIFY = 6;
  public static final int STATE_RECEIVEDREJECT = 8;
  public static final int STATE_RECEIVEDTERMINATE = 11;
  public static final int STATE_SENTACCEPT = 3;
  public static final int STATE_SENTINITIATE = 1;
  public static final int STATE_SENTMODIFY = 5;
  public static final int STATE_SENTREDIRECT = 9;
  public static final int STATE_SENTREJECT = 7;
  public static final int STATE_SENTTERMINATE = 10;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.SessionState
 * JD-Core Version:    0.6.2
 */