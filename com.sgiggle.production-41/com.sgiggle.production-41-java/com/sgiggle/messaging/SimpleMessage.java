package com.sgiggle.messaging;

public abstract class SimpleMessage extends Message
{
  private long m_sequenceId;
  private int m_type;

  public SimpleMessage(int paramInt)
  {
    this(paramInt, 0L);
  }

  public SimpleMessage(int paramInt, long paramLong)
  {
    this.m_type = paramInt;
    this.m_sequenceId = paramLong;
    if (this.m_sequenceId == 0L)
      this.m_sequenceId = MessageRouter.getInstance().getNextSequenceId();
  }

  public abstract Message clone();

  public void deserialize(byte[] paramArrayOfByte)
    throws Error
  {
    throw new Error("SimpleMessage does not support serialization.");
  }

  public long getSequenceId()
  {
    return this.m_sequenceId;
  }

  public int getType()
  {
    return this.m_type;
  }

  public byte[] serialize()
    throws Error
  {
    throw new Error("SimpleMessage does not support serialization.");
  }

  public void setSequenceId(long paramLong)
  {
    this.m_sequenceId = paramLong;
  }

  public void setType(int paramInt)
  {
    this.m_type = paramInt;
  }

  public String toString()
  {
    return "t=" + Integer.toString(this.m_type) + "; s=" + Long.toString(this.m_sequenceId);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.messaging.SimpleMessage
 * JD-Core Version:    0.6.2
 */