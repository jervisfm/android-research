package com.sgiggle.nativecalllog;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog.Calls;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class NativeCallLogStore
{
  private static final int CALL_TYPE_INBOUND_CONNECTED = 0;
  private static final int CALL_TYPE_INBOUND_MISSED = 1;
  private static final int CALL_TYPE_OUTBOUND_CONNECTED = 2;
  private static final int CALL_TYPE_OUTBOUND_NOT_ANSWERED = 3;
  private static final int CALL_TYPE_UNKNOWN = 4;
  private static final String TAG = "Tango.NativeCallLogStore";
  private static ContentResolver s_contentResolver;

  public static NativeCallLogEntry[] getNativeCallLogEntries()
  {
    Log.d("Tango.NativeCallLogStore", "getNativeCallLogEntries()...");
    if (s_contentResolver == null)
    {
      Log.w("Tango.NativeCallLogStore", "getNativeCallLogEntries(): s_contentResolver is null. Return NULL.");
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    String[] arrayOfString = { "number", "type", "date", "duration" };
    Cursor localCursor = s_contentResolver.query(CallLog.Calls.CONTENT_URI, arrayOfString, null, null, "date DESC");
    if (localCursor != null)
    {
      int i = localCursor.getColumnIndex("number");
      int j = localCursor.getColumnIndex("type");
      int k = localCursor.getColumnIndex("date");
      int m = localCursor.getColumnIndex("duration");
      if (localCursor.moveToNext())
        do
        {
          NativeCallLogEntry localNativeCallLogEntry = new NativeCallLogEntry();
          localNativeCallLogEntry.phoneNumber = localCursor.getString(i);
          localNativeCallLogEntry.startTime = localCursor.getLong(k);
          localNativeCallLogEntry.duration = localCursor.getLong(m);
          localNativeCallLogEntry.callType = translateCallType(localCursor.getInt(j), localNativeCallLogEntry.duration);
          localArrayList.add(localNativeCallLogEntry);
        }
        while (localCursor.moveToNext());
    }
    Log.d("Tango.NativeCallLogStore", "getNativeCallLogEntries(): loaded call log entries = " + localArrayList.size());
    return (NativeCallLogEntry[])localArrayList.toArray(new NativeCallLogEntry[0]);
  }

  private static int translateCallType(int paramInt, long paramLong)
  {
    switch (paramInt)
    {
    default:
      return 4;
    case 1:
      return 0;
    case 3:
      return 1;
    case 2:
    }
    if (paramLong > 0L)
      return 2;
    return 3;
  }

  public static void updateContext(Context paramContext)
  {
    if (paramContext == null)
      Log.e("Tango.NativeCallLogStore", "updateContext: context is unexpectedly null");
    do
    {
      return;
      s_contentResolver = paramContext.getContentResolver();
    }
    while (s_contentResolver != null);
    Log.e("Tango.NativeCallLogStore", "updateContext: getContextResolver() returned null.");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.nativecalllog.NativeCallLogStore
 * JD-Core Version:    0.6.2
 */