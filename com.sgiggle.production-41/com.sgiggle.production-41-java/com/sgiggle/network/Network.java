package com.sgiggle.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.sgiggle.util.FileLogger;
import com.sgiggle.util.Log;

public class Network extends BroadcastReceiver
{
  private static final String TAG = "com.sgiggle.network.Network";
  private static boolean m_beingEnforced = false;
  private static boolean m_customWifiSetting;
  private static NetworkInfo m_networkInUse = null;
  private static WifiInfo m_wifiInUse = null;
  private static WifiManager m_wifiManager;
  private static ConnectivityManager s_connectivityManager;

  static
  {
    m_customWifiSetting = false;
  }

  public static boolean enforce3GWhileInCall()
  {
    try
    {
      boolean bool1 = m_beingEnforced;
      boolean bool2;
      if (bool1)
        bool2 = true;
      while (true)
      {
        return bool2;
        m_customWifiSetting = getCustomerWifiSetting();
        if ((s_connectivityManager != null) && (m_wifiManager != null) && (m_networkInUse != null))
        {
          int i = m_networkInUse.getType();
          if (i == 1);
        }
        try
        {
          m_wifiManager.setWifiEnabled(false);
          m_beingEnforced = true;
          Log.i("com.sgiggle.network.Network", "enforce3GInCall disabled Wifi");
          bool2 = true;
        }
        catch (RuntimeException localRuntimeException)
        {
          while (true)
            Log.e("com.sgiggle.network.Network", "Caught RuntimeException exception on disable Wifi service: ", localRuntimeException);
        }
      }
    }
    finally
    {
    }
  }

  public static boolean getCustomerWifiSetting()
  {
    try
    {
      WifiManager localWifiManager = m_wifiManager;
      boolean bool = false;
      if (localWifiManager == null);
      while (true)
      {
        return bool;
        if (m_wifiManager.isWifiEnabled())
        {
          bool = true;
        }
        else if (m_wifiManager.getWifiState() != 3)
        {
          int i = m_wifiManager.getWifiState();
          bool = false;
          if (i != 2);
        }
        else
        {
          bool = true;
        }
      }
    }
    finally
    {
    }
  }

  public static int getNetworkStatus()
  {
    try
    {
      int i;
      if (s_connectivityManager == null)
      {
        Log.e("com.sgiggle.network.Network", "FATAL: s_connectivityManager() = null");
        i = -1;
      }
      while (true)
      {
        return i;
        NetworkInfo localNetworkInfo = s_connectivityManager.getActiveNetworkInfo();
        if (localNetworkInfo == null)
        {
          Log.e("com.sgiggle.network.Network", "FATAL: getActiveNetworkInfo() = null");
          i = -1;
        }
        else if (localNetworkInfo.isAvailable())
        {
          int j = localNetworkInfo.getType();
          i = j;
        }
        else
        {
          i = -1;
        }
      }
    }
    finally
    {
    }
  }

  public static boolean restoreCustomerWifiSettingAfterCall()
  {
    try
    {
      m_beingEnforced = false;
      if (m_wifiManager != null)
      {
        boolean bool1 = m_wifiManager.isWifiEnabled();
        boolean bool2 = m_customWifiSetting;
        if (bool1 == bool2);
      }
      try
      {
        m_wifiManager.setWifiEnabled(m_customWifiSetting);
        Log.v("com.sgiggle.network.Network", "restore CustomerWifiSettingAfterCall() to " + m_customWifiSetting);
        return true;
      }
      catch (RuntimeException localRuntimeException)
      {
        while (true)
          Log.e("com.sgiggle.network.Network", "Caught RuntimeException exception on restoring Wifi service: ", localRuntimeException);
      }
    }
    finally
    {
    }
  }

  public static void updateContext(Context paramContext)
  {
    if (paramContext == null);
    while (true)
    {
      try
      {
        Log.e("com.sgiggle.network.Network", "FATAL: updateContext(context = null)");
        return;
        ConnectivityManager localConnectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
        s_connectivityManager = localConnectivityManager;
        if (localConnectivityManager != null)
        {
          WifiManager localWifiManager = (WifiManager)paramContext.getSystemService("wifi");
          m_wifiManager = localWifiManager;
          if (localWifiManager != null);
        }
        else
        {
          Log.e("com.sgiggle.network.Network", "FATAL: getSystemService() = null");
          continue;
        }
      }
      finally
      {
      }
      m_customWifiSetting = getCustomerWifiSetting();
      m_networkInUse = s_connectivityManager.getActiveNetworkInfo();
      if (m_networkInUse != null)
      {
        String str = "Connectivity Manager set " + m_networkInUse.toString() + " ";
        if ((m_networkInUse.getType() == 1) && (m_wifiManager.getConnectionInfo() != null))
        {
          m_wifiInUse = m_wifiManager.getConnectionInfo();
          str = str + "Wifi " + m_wifiInUse.getBSSID();
        }
        Log.i("com.sgiggle.network.Network", str);
        FileLogger.toFile(str);
      }
    }
  }

  public boolean isSameWifiAccessPoint(WifiInfo paramWifiInfo)
  {
    if (paramWifiInfo != null);
    while (true)
    {
      try
      {
        String str = paramWifiInfo.getBSSID();
        if (str == null)
        {
          bool = false;
          return bool;
        }
        if ((m_wifiInUse != null) && (m_wifiInUse.getBSSID() != null))
        {
          int i = m_wifiInUse.getBSSID().compareTo(paramWifiInfo.getBSSID());
          if (i == 0)
          {
            bool = true;
            continue;
          }
        }
      }
      finally
      {
      }
      boolean bool = false;
    }
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    NetworkInfo localNetworkInfo;
    while (true)
    {
      String str2;
      try
      {
        if (s_connectivityManager == null)
        {
          Log.i("com.sgiggle.network.Network", "Ignoring pre-init network event (normal during reboot)");
          return;
        }
        if (!paramIntent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE"))
          continue;
        Log.v("com.sgiggle.network.Network", "Handling CONNECTIVITY_ACTION");
        FileLogger.toFile("Handling CONNECTIVITY_ACTION");
        localNetworkInfo = (NetworkInfo)paramIntent.getParcelableExtra("networkInfo");
        if ((localNetworkInfo == null) || (!localNetworkInfo.isConnected()))
          break;
        if (m_networkInUse != null)
        {
          String str4 = "old network " + m_networkInUse.toString();
          Log.v("com.sgiggle.network.Network", str4);
          FileLogger.toFile(str4);
        }
        str2 = "send network changed " + localNetworkInfo.toString() + " ";
        m_networkInUse = localNetworkInfo;
        if (localNetworkInfo.getType() == 1)
        {
          m_wifiInUse = m_wifiManager.getConnectionInfo();
          str3 = str2 + "Wifi " + m_wifiInUse.getBSSID() + " ";
          Log.v("com.sgiggle.network.Network", str3);
          FileLogger.toFile(str3);
          sendNetworkChangeMessage();
          continue;
        }
      }
      finally
      {
      }
      m_wifiInUse = null;
      String str3 = str2;
    }
    if (localNetworkInfo != null);
    for (String str1 = "ignored unconnected network change " + localNetworkInfo.toString(); ; str1 = "ignored unconnected network change ")
    {
      Log.v("com.sgiggle.network.Network", str1);
      FileLogger.toFile(str1);
      break;
    }
  }

  public native void sendNetworkChangeMessage();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.network.Network
 * JD-Core Version:    0.6.2
 */