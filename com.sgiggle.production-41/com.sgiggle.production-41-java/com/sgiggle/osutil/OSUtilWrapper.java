package com.sgiggle.osutil;

import android.os.Process;
import com.sgiggle.util.Log;

public class OSUtilWrapper
{
  static final String TAG = "OSUtilWrapper";

  public static void setPriority(int paramInt1, int paramInt2)
  {
    try
    {
      Process.setThreadPriority(paramInt1, paramInt2);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Log.w("OSUtilWrapper", "Failed setting priority for thread tid=" + paramInt1 + " prio=" + paramInt2 + " called from thread id " + Thread.currentThread().getId() + " thread name " + Thread.currentThread().getName());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.osutil.OSUtilWrapper
 * JD-Core Version:    0.6.2
 */