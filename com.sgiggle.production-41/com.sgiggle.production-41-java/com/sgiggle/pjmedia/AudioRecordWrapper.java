package com.sgiggle.pjmedia;

import android.media.AudioRecord;
import android.os.Build;
import android.os.Process;
import com.sgiggle.util.ClientCrashReporter;
import com.sgiggle.util.Log;
import java.nio.ByteBuffer;
import java.util.concurrent.Semaphore;

public class AudioRecordWrapper
{
  static final int TAG = 22;
  public static Semaphore audioSignalingSemaphore = new Semaphore(0);
  AudioRecord m_audioRecord;
  CaptureThread m_captureThread;
  boolean m_flag_stop;
  int m_frameSize;
  int m_pjmediaStreamPtr;
  int m_sampleRateInHz;
  int m_streamMode = StreamMode.INVALID.ordinal();

  public AudioRecordWrapper()
  {
  }

  public AudioRecordWrapper(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Log.v(22, "Creating AudioRecordWrapper. Sampling frequency: " + paramInt1 + " frame size: " + paramInt2);
    this.m_sampleRateInHz = paramInt1;
    this.m_frameSize = paramInt2;
    this.m_pjmediaStreamPtr = paramInt4;
    this.m_streamMode = paramInt3;
  }

  private boolean start()
  {
    this.m_flag_stop = false;
    int i = AudioRecord.getMinBufferSize(this.m_sampleRateInHz, 16, 2);
    int j = i * 3;
    if ((Build.MODEL.startsWith("YP-G")) && (Build.MANUFACTURER.compareToIgnoreCase("Samsung") == 0));
    for (int k = i * 10; ; k = j)
    {
      int m = AudioModeWrapper.getAudioSource();
      int i1;
      for (int n = 0; ; n = i1)
      {
        if (n < 5)
          i1 = n + 1;
        try
        {
          this.m_audioRecord = new AudioRecord(m, this.m_sampleRateInHz, 16, 2, k);
          if (this.m_audioRecord.getState() != 1)
            continue;
          if (this.m_audioRecord.getState() != 1)
          {
            ClientCrashReporter localClientCrashReporter1 = ClientCrashReporter.getInstance();
            Object[] arrayOfObject1 = new Object[6];
            arrayOfObject1[0] = Integer.valueOf(m);
            arrayOfObject1[1] = Integer.valueOf(this.m_sampleRateInHz);
            arrayOfObject1[2] = Integer.valueOf(16);
            arrayOfObject1[3] = Integer.valueOf(2);
            arrayOfObject1[4] = Integer.valueOf(k);
            arrayOfObject1[5] = Integer.valueOf(this.m_audioRecord.getState());
            localClientCrashReporter1.reportException(new IllegalStateException(String.format("Failed to initialze AudioRecord(%d, %d, %d, %d, %d). AudioRecord.State()=", arrayOfObject1)));
            return false;
          }
        }
        catch (Exception localException2)
        {
          do
            Log.e(22, "start exception catched, failed to create new AudioTrack: " + localException2.getMessage());
          while (i1 != 5);
          ClientCrashReporter localClientCrashReporter2 = ClientCrashReporter.getInstance();
          Object[] arrayOfObject2 = new Object[5];
          arrayOfObject2[0] = Integer.valueOf(m);
          arrayOfObject2[1] = Integer.valueOf(this.m_sampleRateInHz);
          arrayOfObject2[2] = Integer.valueOf(16);
          arrayOfObject2[3] = Integer.valueOf(2);
          arrayOfObject2[4] = Integer.valueOf(k);
          localClientCrashReporter2.reportException(new IllegalStateException(String.format("Construction of AudioRecord(%d, %d, %d, %d, %d) failed", arrayOfObject2), localException2));
          return false;
        }
        Log.d(22, "Creating AudioRecord audioSource=" + m + " sampling freq=" + this.m_sampleRateInHz + " buffer size=" + k / 2 + " samples (" + k * 1000 / 2 / this.m_sampleRateInHz + " msec)" + " minBufferSize=" + i / 2);
        if (Build.MODEL.compareToIgnoreCase("SAMSUNG-SGH-I727") == 0)
          Log.i(22, "Samsung Skyrocket: workaround corrupted audio system state");
        try
        {
          Thread.sleep(1000L);
          label428: this.m_audioRecord.startRecording();
          Log.v(22, "startRecording ended");
          this.m_captureThread = new CaptureThread();
          this.m_captureThread.start();
          return true;
        }
        catch (Exception localException1)
        {
          break label428;
        }
      }
    }
  }

  private void stop()
  {
    if (this.m_audioRecord == null)
    {
      Log.w(22, "stop was called but m_audioRecord was never initialized");
      return;
    }
    try
    {
      AudioModeWrapper.setMicMute(false);
      Log.v(22, "Calling stop");
      this.m_audioRecord.stop();
      this.m_flag_stop = true;
      Log.v(22, "Calling join");
      this.m_captureThread.join();
      Log.v(22, "Calling release");
      this.m_audioRecord.release();
      Log.v(22, "release ended");
      this.m_audioRecord = null;
      return;
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
    }
  }

  public int delay()
  {
    int i = this.m_sampleRateInHz;
    int j = 0;
    if (i > 0)
      j = 1000 * (this.m_frameSize / 2) / this.m_sampleRateInHz;
    return j;
  }

  public void release()
  {
  }

  public native int sendBytesToPJMedia(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2);

  class CaptureThread extends Thread
  {
    public CaptureThread()
    {
      super();
    }

    public void run()
    {
      Process.setThreadPriority(-14);
      if (AudioRecordWrapper.this.m_streamMode == AudioRecordWrapper.StreamMode.PLAYBACKANDRECORD.ordinal())
        AudioRecordWrapper.audioSignalingSemaphore.release();
      ByteBuffer localByteBuffer = ByteBuffer.allocateDirect(AudioRecordWrapper.this.m_frameSize);
      while ((!AudioRecordWrapper.this.m_flag_stop) && (AudioRecordWrapper.this.m_audioRecord.getRecordingState() == 3))
      {
        int i = AudioRecordWrapper.this.m_audioRecord.read(localByteBuffer, AudioRecordWrapper.this.m_frameSize);
        if (i < 0)
        {
          Log.v(22, "read error " + i);
        }
        else if (!AudioModeWrapper.isOffHook())
        {
          int j = AudioRecordWrapper.this.sendBytesToPJMedia(localByteBuffer, i, AudioRecordWrapper.this.m_pjmediaStreamPtr);
          if (j != AudioRecordWrapper.this.m_frameSize)
            Log.d(22, "Bailing, sendBytes returned " + j);
        }
      }
    }
  }

  private static enum StreamMode
  {
    static
    {
      PLAYBACKANDRECORD = new StreamMode("PLAYBACKANDRECORD", 2);
      INVALID = new StreamMode("INVALID", 3);
      StreamMode[] arrayOfStreamMode = new StreamMode[4];
      arrayOfStreamMode[0] = PLAYBACK;
      arrayOfStreamMode[1] = RECORD;
      arrayOfStreamMode[2] = PLAYBACKANDRECORD;
      arrayOfStreamMode[3] = INVALID;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.pjmedia.AudioRecordWrapper
 * JD-Core Version:    0.6.2
 */