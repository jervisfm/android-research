package com.sgiggle.pjmedia;

import android.media.AudioTrack;
import android.os.Process;
import com.sgiggle.util.ClientCrashReporter;
import com.sgiggle.util.Log;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class AudioTrackWrapper
{
  static final int TAG = 24;
  static AudioTrackWrapper m_instance;
  AudioTrack m_audioTrack;
  AtomicInteger m_bufferedPlaySamples;
  boolean m_flag_running = false;
  boolean m_flag_stop = false;
  long m_frameMSec;
  int m_frameSize;
  boolean m_mute;
  int m_pjmediaStreamPtr;
  int m_playPosition;
  PlaybackThread m_playbackThread;
  boolean m_req_start = false;
  boolean m_req_suspend = false;
  int m_sampleRateInHz;
  int m_streamMode = StreamMode.INVALID.ordinal();

  public AudioTrackWrapper()
  {
  }

  public AudioTrackWrapper(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.m_sampleRateInHz = paramInt1;
    this.m_pjmediaStreamPtr = paramInt4;
    this.m_frameSize = paramInt2;
    this.m_frameMSec = (1000 * this.m_frameSize / paramInt1 / 2);
    m_instance = this;
    this.m_bufferedPlaySamples = new AtomicInteger(0);
    this.m_playPosition = 0;
    this.m_streamMode = paramInt3;
    Log.d(24, "frame size " + this.m_frameSize + " bytes (" + this.m_frameMSec + " ms); sampling frequency " + paramInt1 + " Hz");
  }

  private boolean start()
  {
    if (this.m_audioTrack != null)
      Log.w(24, "Calling start while the playback in progress");
    this.m_flag_stop = false;
    int i = 2 * AudioTrack.getMinBufferSize(this.m_sampleRateInHz, 4, 2);
    int j = AudioModeWrapper.getStreamType();
    int k = 0;
    int m = i;
    while (k < 5)
      try
      {
        Log.d(24, "Creating AudioTrack streamType=" + j + " sampling freq=" + this.m_sampleRateInHz + " buffer size=" + m / 2 + " samples (" + m * 1000 / 2 / this.m_sampleRateInHz + " msec)");
        this.m_audioTrack = new AudioTrack(j, this.m_sampleRateInHz, 4, 2, m, 1);
        if (this.m_audioTrack.getState() == 1)
          break;
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = Integer.valueOf(this.m_audioTrack.getState());
        throw new RuntimeException(String.format("Unexpected AudioTrack state %d", arrayOfObject2));
      }
      catch (Exception localException)
      {
        Log.e(24, "start exception catched, failed to create new AudioTrack: " + localException.getMessage());
        int n = k + 1;
        if (n == 3)
          m = 4 * AudioTrack.getMinBufferSize(this.m_sampleRateInHz, 4, 2);
        if (n == 5)
        {
          ClientCrashReporter localClientCrashReporter = ClientCrashReporter.getInstance();
          Object[] arrayOfObject1 = new Object[5];
          arrayOfObject1[0] = Integer.valueOf(j);
          arrayOfObject1[1] = Integer.valueOf(this.m_sampleRateInHz);
          arrayOfObject1[2] = Integer.valueOf(4);
          arrayOfObject1[3] = Integer.valueOf(2);
          arrayOfObject1[4] = Integer.valueOf(m);
          localClientCrashReporter.reportException(new IllegalStateException(String.format("Construction of AudioTrack(%d, %d, %d, %d, %d) failed", arrayOfObject1), localException));
          return false;
        }
        k = n;
      }
    this.m_audioTrack.play();
    this.m_playbackThread = new PlaybackThread();
    this.m_playbackThread.start();
    return true;
  }

  private void stop()
  {
    if ((this.m_flag_stop) || (this.m_audioTrack == null))
      return;
    try
    {
      Log.v(24, "Calling stop");
      this.m_audioTrack.stop();
      this.m_flag_stop = true;
      Log.v(24, "Calling join");
      this.m_playbackThread.join();
      Log.v(24, "Calling release");
      this.m_audioTrack.release();
      Log.v(24, "release ended");
      this.m_audioTrack = null;
      return;
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
    }
  }

  public int delay()
  {
    int i = this.m_sampleRateInHz;
    int j = 0;
    if (i > 0)
      j = 1000 * this.m_bufferedPlaySamples.get() / this.m_sampleRateInHz;
    return j;
  }

  public native int getBytesFromPJMedia(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

  public void release()
  {
  }

  public void setSpeakerMute(boolean paramBoolean)
  {
    this.m_mute = paramBoolean;
  }

  class PlaybackThread extends Thread
  {
    public PlaybackThread()
    {
      super();
    }

    public void run()
    {
      Process.setThreadPriority(-14);
      byte[] arrayOfByte1 = new byte[AudioTrackWrapper.this.m_frameSize];
      byte[] arrayOfByte2 = new byte[AudioTrackWrapper.this.m_frameSize];
      if (AudioTrackWrapper.this.m_streamMode == AudioTrackWrapper.StreamMode.PLAYBACKANDRECORD.ordinal())
      {
        Log.d(24, "Waiting for record thread to start...");
        AudioRecordWrapper.audioSignalingSemaphore.acquireUninterruptibly();
      }
      Log.d(24, "Recording started!");
      int i = 0;
      int i1;
      if ((!AudioTrackWrapper.this.m_flag_stop) && (AudioTrackWrapper.this.m_audioTrack.getPlayState() == 3))
      {
        if (AudioModeWrapper.isOffHook())
          break label294;
        i1 = AudioTrackWrapper.this.getBytesFromPJMedia(arrayOfByte1, AudioTrackWrapper.this.m_frameSize, AudioTrackWrapper.this.m_pjmediaStreamPtr);
        if (i1 != AudioTrackWrapper.this.m_frameSize)
          Log.e(24, "Bailing, getBytes returned " + i1);
      }
      else
      {
        return;
      }
      int k;
      int m;
      if (AudioTrackWrapper.this.m_mute)
      {
        int i3 = AudioTrackWrapper.this.m_audioTrack.write(arrayOfByte2, 0, i1);
        k = i1;
        m = i3;
      }
      while (true)
      {
        int n = AudioTrackWrapper.this.m_audioTrack.getPlaybackHeadPosition();
        if (n < AudioTrackWrapper.this.m_playPosition)
          AudioTrackWrapper.this.m_playPosition = 0;
        AudioTrackWrapper.this.m_bufferedPlaySamples.addAndGet((m >> 1) - (n - AudioTrackWrapper.this.m_playPosition));
        AudioTrackWrapper.this.m_playPosition = n;
        i = k;
        break;
        int i2 = AudioTrackWrapper.this.m_audioTrack.write(arrayOfByte1, 0, i1);
        k = i1;
        m = i2;
        continue;
        label294: int j = AudioTrackWrapper.this.m_audioTrack.write(arrayOfByte2, 0, i);
        k = i;
        m = j;
      }
    }
  }

  private static enum StreamMode
  {
    static
    {
      PLAYBACKANDRECORD = new StreamMode("PLAYBACKANDRECORD", 2);
      INVALID = new StreamMode("INVALID", 3);
      StreamMode[] arrayOfStreamMode = new StreamMode[4];
      arrayOfStreamMode[0] = PLAYBACK;
      arrayOfStreamMode[1] = RECORD;
      arrayOfStreamMode[2] = PLAYBACKANDRECORD;
      arrayOfStreamMode[3] = INVALID;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.pjmedia.AudioTrackWrapper
 * JD-Core Version:    0.6.2
 */