package com.sgiggle.pjmedia;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import com.sgiggle.util.Log;

public class AudioWebRTC
{
  private static native void loadLibraries(String paramString);

  public static void updateContext(Context paramContext)
  {
    Log.d(143, "updateContext...");
    loadLibraries(paramContext.getApplicationInfo().dataDir);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.pjmedia.AudioWebRTC
 * JD-Core Version:    0.6.2
 */