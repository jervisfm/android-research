package com.sgiggle.production;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;

public class AbsoluteLayout extends ViewGroup
{
  public AbsoluteLayout(Context paramContext)
  {
    super(paramContext);
  }

  public AbsoluteLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public AbsoluteLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void addView(View paramView)
  {
    super.addView(paramView);
  }

  public void addView(View paramView, int paramInt)
  {
    super.addView(paramView, paramInt);
  }

  public void addView(View paramView, int paramInt1, int paramInt2)
  {
    super.addView(paramView, paramInt1, paramInt2);
  }

  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    super.addView(paramView, paramInt, paramLayoutParams);
  }

  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    super.addView(paramView, paramLayoutParams);
  }

  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }

  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new LayoutParams(paramLayoutParams);
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = getChildCount();
    for (int j = 0; j < i; j++)
    {
      View localView = getChildAt(j);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        int k = localLayoutParams.leftMargin;
        int m = localLayoutParams.topMargin;
        int n = localLayoutParams.width;
        int i1 = localLayoutParams.height;
        localView.layout(k, m, n + k, i1 + m);
      }
    }
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = getChildCount();
    int j = 0;
    int k = 0;
    int m = 0;
    int n;
    if (j < i)
    {
      View localView = getChildAt(j);
      if (localView.getVisibility() == 8)
        break label137;
      LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
      int i2 = localLayoutParams.leftMargin;
      int i3 = localLayoutParams.topMargin;
      int i4 = localLayoutParams.width;
      int i5 = localLayoutParams.height;
      if (i2 + i4 > m)
        m = i2 + i4;
      if (i3 + i5 <= k)
        break label137;
      n = i5 + i3;
    }
    for (int i1 = m; ; i1 = m)
    {
      j++;
      m = i1;
      k = n;
      break;
      setMeasuredDimension(m, k);
      return;
      label137: n = k;
    }
  }

  public class LayoutParams extends ViewGroup.MarginLayoutParams
  {
    public LayoutParams(int paramInt1, int arg3)
    {
      super(i);
    }

    public LayoutParams(Context paramAttributeSet, AttributeSet arg3)
    {
      super(localAttributeSet);
    }

    public LayoutParams(ViewGroup.LayoutParams arg2)
    {
      super();
    }

    public LayoutParams(ViewGroup.MarginLayoutParams arg2)
    {
      super();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.AbsoluteLayout
 * JD-Core Version:    0.6.2
 */