package com.sgiggle.production;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Resources;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.DismissVerificationWithOtherDeviceMessage;
import com.sgiggle.media_engine.MediaEngineMessage.OtherRegisteredDeviceMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;

class AccountVerifier
  implements DialogInterface.OnDismissListener
{
  private static final String TAG = "Tango.AccountVerifier";
  private static AccountVerifier s_me;
  private TangoApp m_application;
  private AlertDialog m_queryOtherAlert;

  public AccountVerifier(TangoApp paramTangoApp)
  {
    this.m_application = paramTangoApp;
  }

  static AccountVerifier getDefault()
  {
    return s_me;
  }

  static void init(TangoApp paramTangoApp)
  {
    s_me = new AccountVerifier(paramTangoApp);
  }

  private void replyToQueryForOtherDevice(boolean paramBoolean)
  {
    Log.d("Tango.AccountVerifier", "replyToQueryForOtherDevice(registeredOther=" + paramBoolean + ")");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.OtherRegisteredDeviceMessage(paramBoolean));
  }

  public void dismissQueryForOtherDeviceAlert()
  {
    if (this.m_queryOtherAlert != null)
    {
      Log.d("Tango.AccountVerifier", "Dismiss the existing Query-for-other-device alert...");
      AlertDialog localAlertDialog = this.m_queryOtherAlert;
      this.m_queryOtherAlert = null;
      localAlertDialog.dismiss();
    }
  }

  public void displayQueryOtherDeviceAlert(Context paramContext)
  {
    dismissQueryForOtherDeviceAlert();
    Resources localResources = this.m_application.getResources();
    String str1 = localResources.getString(2131296397);
    String str2 = localResources.getString(2131296398);
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
    localBuilder.setTitle(str1);
    localBuilder.setMessage(str2);
    localBuilder.setCancelable(false);
    localBuilder.setInverseBackgroundForced(true);
    localBuilder.setPositiveButton(2131296289, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        AccountVerifier.this.replyToQueryForOtherDevice(true);
        AccountVerifier.access$102(AccountVerifier.this, null);
      }
    });
    localBuilder.setNegativeButton(2131296290, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        AccountVerifier.this.replyToQueryForOtherDevice(false);
        AccountVerifier.access$102(AccountVerifier.this, null);
      }
    });
    this.m_queryOtherAlert = localBuilder.create();
    this.m_queryOtherAlert.setOnDismissListener(this);
    this.m_queryOtherAlert.show();
  }

  public void displayVerificationOtherDeviceNotice(Context paramContext)
  {
    Toast.makeText(TangoApp.getInstance(), paramContext.getResources().getString(2131296399), 1).show();
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DismissVerificationWithOtherDeviceMessage());
  }

  public void handleNewMessage(Message paramMessage, Activity paramActivity)
  {
    Log.v("Tango.AccountVerifier", "handleNewMessage(message=" + paramMessage + ")");
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35100:
      Log.v("Tango.AccountVerifier", "Display query of other device notice");
      displayQueryOtherDeviceAlert(paramActivity);
      return;
    case 35101:
    }
    Log.v("Tango.AccountVerifier", "Display veification other device notice");
    displayVerificationOtherDeviceNotice(paramActivity);
  }

  public void onDismiss(DialogInterface paramDialogInterface)
  {
    Log.d("Tango.AccountVerifier", "onDismiss() [DialogInterface]");
    if (paramDialogInterface == this.m_queryOtherAlert)
      replyToQueryForOtherDevice(false);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.AccountVerifier
 * JD-Core Version:    0.6.2
 */