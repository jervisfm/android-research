package com.sgiggle.production;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.Bundle;
import com.sgiggle.media_engine.MediaEngineMessage.ValidationRequiredEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;

public class ActivityBase extends Activity
{
  private static final boolean DBG = true;
  private static final String TAG = "Tango.ActivityBase";
  private static final boolean VDBG = true;
  protected Message m_firstMessage;

  public ActivityBase()
  {
    try
    {
      TangoApp.ensureInitialized();
      Log.v("Tango.ActivityBase", "ActivityBase()");
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      while (true)
        Log.e("Tango.ActivityBase", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  protected Message getFirstMessage()
  {
    return this.m_firstMessage;
  }

  void handleNewMessage(Message paramMessage)
  {
    Log.v("Tango.ActivityBase", "handleNewMessage(message=" + paramMessage + ").");
    switch (paramMessage.getType())
    {
    default:
      Log.w("Tango.ActivityBase", "handleNewMessage(): Unsupported message=" + paramMessage);
      return;
    case 35051:
      String str = ((SessionMessages.OptionalPayload)((MediaEngineMessage.ValidationRequiredEvent)paramMessage).payload()).getMessage();
      new ValidationRequiredDialog.Builder(this).create(str).show();
      return;
    case 35114:
    }
    new SMSRateLimitDialog.Builder(this).create().show();
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.ActivityBase", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    Object localObject = getLastNonConfigurationInstance();
    if (localObject == null)
    {
      this.m_firstMessage = MessageManager.getDefault().getMessageFromIntent(getIntent());
      Log.d("Tango.ActivityBase", "onCreate(): First time created: message=" + this.m_firstMessage);
      return;
    }
    this.m_firstMessage = ((ActivityConfiguration)localObject).firstMessage;
    Log.d("Tango.ActivityBase", "onCreate(): Restore activity: message=" + this.m_firstMessage);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Message localMessage = MessageManager.getDefault().getMessageFromIntent(paramIntent);
    Log.d("Tango.ActivityBase", "onNewIntent(): Message = " + localMessage);
    if (localMessage != null)
      handleNewMessage(localMessage);
  }

  protected void onResume()
  {
    Log.v("Tango.ActivityBase", "onResume()");
    super.onResume();
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_FOREGROUND);
  }

  public Object onRetainNonConfigurationInstance()
  {
    Log.v("Tango.ActivityBase", "onRetainNonConfigurationInstance(): message = " + this.m_firstMessage);
    ActivityConfiguration localActivityConfiguration = new ActivityConfiguration();
    localActivityConfiguration.firstMessage = this.m_firstMessage;
    return localActivityConfiguration;
  }

  protected void onUserLeaveHint()
  {
    Log.d("Tango.ActivityBase", "onUserLeaveHint()");
    super.onUserLeaveHint();
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
  }

  protected static class ActivityConfiguration
  {
    protected Message firstMessage;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ActivityBase
 * JD-Core Version:    0.6.2
 */