package com.sgiggle.production;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import com.sgiggle.messaging.Message;
import com.sgiggle.util.Log;
import java.util.Stack;

public class ActivityStack extends ActivityGroup
{
  private static final String TAG = "Tango.ActivityStack";
  private int classId = 0;
  protected Stack<String> stack = new Stack();

  public Activity currentActivity()
  {
    return getLocalActivityManager().getCurrentActivity();
  }

  public void finishFromChild(Activity paramActivity)
  {
    pop();
  }

  public Activity getActivity(int paramInt)
  {
    if (paramInt >= this.stack.size())
      return null;
    return getLocalActivityManager().getActivity((String)this.stack.get(paramInt));
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.stack.size() > 1))
      return true;
    if (paramInt == 82)
    {
      Log.i("Tango.ActivityStack", "menu key down handled by active activity");
      return currentActivity().onKeyDown(paramInt, paramKeyEvent);
    }
    Log.i("Tango.ActivityStack", "key down" + paramInt + " handled by super class");
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.stack.size() > 1))
    {
      Log.i("Tango.ActivityStack", "back key up handled by ActivityStack " + this.stack);
      return currentActivity().onKeyUp(paramInt, paramKeyEvent);
    }
    if (paramInt == 82)
    {
      Log.i("Tango.ActivityStack", "menu key up handled by active activity");
      return currentActivity().onKeyUp(paramInt, paramKeyEvent);
    }
    Log.i("Tango.ActivityStack", "key up" + paramInt + " handled by super class");
    return super.onKeyUp(paramInt, paramKeyEvent);
  }

  public void pop()
  {
    if (this.stack.size() == 1);
    LocalActivityManager localLocalActivityManager;
    do
    {
      return;
      localLocalActivityManager = getLocalActivityManager();
      Log.i("Tango.ActivityStack", "disappear " + (String)this.stack.peek());
      localLocalActivityManager.destroyActivity((String)this.stack.pop(), true);
    }
    while (this.stack.size() <= 0);
    Log.i("Tango.ActivityStack", "show " + (String)this.stack.peek());
    Intent localIntent = localLocalActivityManager.getActivity((String)this.stack.peek()).getIntent();
    setContentView(localLocalActivityManager.startActivity((String)this.stack.peek(), localIntent).getDecorView());
    Log.i("Tango.ActivityStack", getCurrentFocus() + " has focus, window has focus" + hasWindowFocus());
  }

  public void popToRoot()
  {
    while (this.stack.size() > 1)
      pop();
  }

  public void push(Class<?> paramClass)
  {
    Intent localIntent = new Intent();
    localIntent.setClass(this, paramClass);
    push(paramClass.getName() + this.classId, localIntent);
    this.classId = (1 + this.classId);
  }

  public void push(String paramString, Intent paramIntent)
  {
    Window localWindow = getLocalActivityManager().startActivity(paramString, paramIntent.addFlags(67108864));
    if (localWindow != null)
    {
      Log.i("Tango.ActivityStack", "push and show " + paramString);
      this.stack.push(paramString);
      setContentView(localWindow.getDecorView());
      return;
    }
    Log.d("Tango.ActivityStack", "can not start " + paramIntent);
  }

  public void pushWithMessage(Class<?> paramClass, Message paramMessage)
  {
    Intent localIntent = new Intent();
    localIntent.setClass(this, paramClass);
    if (paramMessage != null)
      MessageManager.getDefault().storeMessageInIntent(paramMessage, localIntent);
    push(paramClass.getName() + this.classId, localIntent);
    this.classId = (1 + this.classId);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ActivityStack
 * JD-Core Version:    0.6.2
 */