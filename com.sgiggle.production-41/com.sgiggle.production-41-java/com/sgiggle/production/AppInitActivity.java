package com.sgiggle.production;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import com.sgiggle.util.Log;
import com.sgiggle.util.LogReporter;

public class AppInitActivity extends Activity
{
  private static final int ACTIVITY_RESULT_LOG_SENT = 0;
  private static final int INTENT_DEFAULT = 0;
  private static final int INTENT_LOG_REPORT = 1;
  private static final String TAG = "Tango.AppInitActivity";
  private static AppInitActivity s_this;

  public static AppInitActivity getInstance()
  {
    return s_this;
  }

  public void ComposeAndSendEmail()
  {
    Log.v("Tango.AppInitActivity", "ComposerAndSendEmail()");
    String str1 = LogReporter.getLogEmail();
    StringBuilder localStringBuilder = new StringBuilder("Thank you for helping Tango collect this information!  We will investigate the logs and get back to you as soon as possible with our conclusions.\nPlease hit 'Send' to forward the logs to our team.");
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("message/rfc822");
    String str2 = getPackageName();
    localIntent.putExtra("android.intent.extra.STREAM", Uri.parse("content://" + str2 + ".tangocontentprovider/" + LogReporter.outFileName()));
    localIntent.putExtra("android.intent.extra.EMAIL", new String[] { str1 });
    localIntent.putExtra("android.intent.extra.SUBJECT", "Sending Tango log file");
    localIntent.putExtra("android.intent.extra.TEXT", localStringBuilder.toString());
    localIntent.addFlags(1);
    localIntent.addFlags(262144);
    try
    {
      startActivityForResult(localIntent, 0);
      Log.v("Tango.AppInitActivity", "Invites have been sent out.");
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Log.w("Tango.AppInitActivity", "Not activity was found for ACTION_SEND (for sending Invites)");
    }
  }

  protected int handleIntent(Intent paramIntent)
  {
    Uri localUri = paramIntent.getData();
    if (localUri != null)
    {
      String str = localUri.getScheme();
      if ((str != null) && (str.startsWith("tango")) && (LogReporter.enableUri(localUri.toString())))
        return 1;
    }
    return 0;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Log.i("Tango.AppInitActivity", "onActivityResult(): requestCode = " + paramInt1 + ", resultCode = " + paramInt2);
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.AppInitActivity", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    setContentView(2130903042);
    s_this = this;
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_RESUMING);
    LogReporter.setAppDataProvider(LogCollector.TangoAppData.class);
    Intent localIntent = getIntent();
    Uri localUri;
    if ("android.intent.action.VIEW".equals(localIntent.getAction()))
    {
      Log.v("Tango.AppInitActivity", "onCreate(): ... received: intent = [" + localIntent + "]");
      localUri = localIntent.getData();
      if (localUri != null)
        break label126;
      Log.i("Tango.AppInitActivity", "onCreate(): ... Intent 's uri is null. Do nothing.");
    }
    while (true)
    {
      return;
      label126: String str1 = localUri.getScheme();
      if ((str1 != null) && (str1.startsWith("tango")) && (localUri.toString().contains("://log")))
      {
        if (handleIntent(localIntent) == 0)
          LogReporter.restore();
      }
      else
      {
        Cursor localCursor = getContentResolver().query(localUri, new String[] { "_id", "data1", "data4", "data5" }, null, null, null);
        Object localObject;
        String str2;
        long l1;
        if ((localCursor != null) && (localCursor.moveToFirst()))
        {
          String str4 = localCursor.getString(1);
          localObject = localCursor.getString(2);
          long l3 = localCursor.getLong(3);
          Log.d("Tango.AppInitActivity", "onCreate(): ... found rawContact: peerJid = [" + str4 + "], peerName = [" + (String)localObject + "], deviceContactId = [" + l3 + "], _ID=[" + localCursor.getLong(0) + "]");
          str2 = str4;
          l1 = l3;
        }
        while (str2 != null)
        {
          Utils.UIContact localUIContact = ContactListActivity.findContact(str2);
          if (localUIContact != null)
          {
            String str3 = localUIContact.displayName();
            long l2 = localUIContact.m_deviceContactId;
            Log.v("Tango.AppInitActivity", "onCreate(): ... received: foundContact = [" + str3 + "]");
            localObject = str3;
            l1 = l2;
          }
          CallHandler.getDefault().sendCallMessage(str2, (String)localObject, l1);
          finish();
          return;
          l1 = -1L;
          localObject = "";
          str2 = null;
        }
      }
    }
  }

  protected void onDestroy()
  {
    Log.d("Tango.AppInitActivity", "onDestroy()");
    super.onDestroy();
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Log.d("Tango.AppInitActivity", "onNewIntent()");
    handleIntent(paramIntent);
  }

  protected void onPause()
  {
    super.onPause();
    if (!isFinishing())
    {
      Log.d("Tango.AppInitActivity", "onPause(): Force this splash screen to finish...");
      finish();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.AppInitActivity
 * JD-Core Version:    0.6.2
 */