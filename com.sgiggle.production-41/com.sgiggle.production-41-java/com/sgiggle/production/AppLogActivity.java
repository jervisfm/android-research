package com.sgiggle.production;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayAppLogEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplaySettingsMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.AppLogEntriesPayload;
import com.sgiggle.xmpp.SessionMessages.AppLogEntry;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class AppLogActivity extends ListActivity
{
  private static final int MENU_JUMP_BOTTOM = 3;
  private static final int MENU_JUMP_TOP = 2;
  private static final int MENU_SHARE = 1;
  private static final String TAG = "Tango.LogActivity";
  private static List<LogEntry> m_logEntries = new ArrayList();
  private LogEntryAdapter m_logEntryAdapter;
  private ListView m_logList;

  public AppLogActivity()
  {
    try
    {
      TangoApp.ensureInitialized();
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      Log.e("Tango.LogActivity", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  private String dump(boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Level localLevel = Level.V;
    Iterator localIterator = m_logEntries.iterator();
    Object localObject1 = localLevel;
    while (localIterator.hasNext())
    {
      LogEntry localLogEntry = (LogEntry)localIterator.next();
      Object localObject3;
      if (!paramBoolean)
      {
        localStringBuilder.append(localLogEntry.getText());
        localStringBuilder.append('\n');
        localObject3 = localObject1;
        localObject1 = localObject3;
      }
      else
      {
        Object localObject2 = localLogEntry.getLevel();
        if (localObject2 == null)
          localObject2 = localObject1;
        while (true)
        {
          localStringBuilder.append("<font color=\"");
          localStringBuilder.append(((Level)localObject1).getHexColor());
          localStringBuilder.append("\" face=\"sans-serif\"><b>");
          localStringBuilder.append(TextUtils.htmlEncode(localLogEntry.getText()));
          localStringBuilder.append("</b></font><br/>\n");
          localObject3 = localObject2;
          break;
          localObject1 = localObject2;
        }
      }
    }
    return localStringBuilder.toString();
  }

  private void jumpBottom()
  {
    this.m_logList.post(new Runnable()
    {
      public void run()
      {
        AppLogActivity.this.m_logList.setSelection(AppLogActivity.this.m_logEntryAdapter.getCount() - 1);
      }
    });
  }

  private void jumpTop()
  {
    this.m_logList.post(new Runnable()
    {
      public void run()
      {
        AppLogActivity.this.m_logList.setSelection(0);
      }
    });
  }

  private void share()
  {
    new Thread(new Runnable()
    {
      public void run()
      {
        String str = AppLogActivity.this.dump(false);
        Intent localIntent = new Intent("android.intent.action.SEND");
        localIntent.setType("text/plain");
        SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("MMM d, yyyy HH:mm:ss ZZZZ");
        localIntent.putExtra("android.intent.extra.SUBJECT", "Tango Log: " + localSimpleDateFormat.format(new Date()));
        localIntent.putExtra("android.intent.extra.TEXT", str);
        AppLogActivity.this.startActivity(Intent.createChooser(localIntent, "Share Tango Log ..."));
      }
    }).start();
  }

  static void storeAppLogEntries(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      Log.w("Tango.LogActivity", "storeAppLogEntries(): Unsupported message = [" + paramMessage + "]");
      return;
    case 35116:
    }
    List localList = ((SessionMessages.AppLogEntriesPayload)((MediaEngineMessage.DisplayAppLogEvent)paramMessage).payload()).getEntriesList();
    Log.d("Tango.LogActivity", "storeAppLogEntries(): # of entries = " + localList.size());
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = localList.iterator();
    if (localIterator.hasNext())
    {
      SessionMessages.AppLogEntry localAppLogEntry = (SessionMessages.AppLogEntry)localIterator.next();
      Level localLevel;
      switch (localAppLogEntry.getSeverity())
      {
      default:
        localLevel = Level.D;
      case 1:
      case 2:
      case 4:
      case 8:
      case 16:
      case 32:
      }
      while (true)
      {
        localArrayList.add(new LogEntry(localAppLogEntry.getText(), localLevel));
        break;
        localLevel = Level.V;
        continue;
        localLevel = Level.D;
        continue;
        localLevel = Level.I;
        continue;
        localLevel = Level.W;
        continue;
        localLevel = Level.E;
        continue;
        localLevel = Level.F;
      }
    }
    m_logEntries = localArrayList;
  }

  public void onBackPressed()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DisplaySettingsMessage());
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onContextItemSelected(paramMenuItem);
    case 2:
      Toast.makeText(this, "Jumping to top of log ...", 0).show();
      jumpTop();
      return true;
    case 3:
    }
    Toast.makeText(this, "Jumping to bottom of log ...", 0).show();
    jumpBottom();
    return true;
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.LogActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903090);
    this.m_logList = ((ListView)findViewById(16908298));
    this.m_logList.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener()
    {
      public void onCreateContextMenu(ContextMenu paramAnonymousContextMenu, View paramAnonymousView, ContextMenu.ContextMenuInfo paramAnonymousContextMenuInfo)
      {
        paramAnonymousContextMenu.setHeaderTitle("View Navigation");
        paramAnonymousContextMenu.add(0, 2, 0, "Jump to Top");
        paramAnonymousContextMenu.add(0, 3, 0, "Jump to Bottom");
      }
    });
    this.m_logEntryAdapter = new LogEntryAdapter(this, 2130903089, m_logEntries);
    setListAdapter(this.m_logEntryAdapter);
    setTitle("Tango Log (" + m_logEntries.size() + " entries)");
    Toast.makeText(this, "Loading " + m_logEntries.size() + " log entries...", 1).show();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    paramMenu.add(0, 1, 0, "Share").setIcon(17301586);
    return true;
  }

  protected void onDestroy()
  {
    Log.d("Tango.LogActivity", "onDestroy()");
    super.onDestroy();
    m_logEntries.clear();
    this.m_logEntryAdapter.clear();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 1:
    }
    share();
    return true;
  }

  static enum Level
  {
    private static Level[] byOrder;
    private int m_color;
    private String m_hexColor;
    private String m_title;
    private int m_value;

    static
    {
      D = new Level("D", 1, 1, "#00006C", "Debug");
      I = new Level("I", 2, 2, "#20831B", "Info");
      W = new Level("W", 3, 3, "#FD7916", "Warn");
      E = new Level("E", 4, 4, "#FD0010", "Error");
      F = new Level("F", 5, 5, "#ff0066", "Fatal");
      Level[] arrayOfLevel = new Level[6];
      arrayOfLevel[0] = V;
      arrayOfLevel[1] = D;
      arrayOfLevel[2] = I;
      arrayOfLevel[3] = W;
      arrayOfLevel[4] = E;
      arrayOfLevel[5] = F;
      $VALUES = arrayOfLevel;
      byOrder = new Level[6];
      byOrder[0] = V;
      byOrder[1] = D;
      byOrder[2] = I;
      byOrder[3] = W;
      byOrder[4] = E;
      byOrder[5] = F;
    }

    private Level(int paramInt, String paramString1, String paramString2)
    {
      this.m_value = paramInt;
      this.m_hexColor = paramString1;
      this.m_color = Color.parseColor(paramString1);
      this.m_title = paramString2;
    }

    public static Level getByOrder(int paramInt)
    {
      return byOrder[paramInt];
    }

    public int getColor()
    {
      return this.m_color;
    }

    public String getHexColor()
    {
      return this.m_hexColor;
    }

    public String getTitle(Context paramContext)
    {
      return this.m_title;
    }

    public int getValue()
    {
      return this.m_value;
    }
  }

  static class LogEntry
  {
    private AppLogActivity.Level level = AppLogActivity.Level.V;
    private String text = null;

    public LogEntry(String paramString, AppLogActivity.Level paramLevel)
    {
      this.text = paramString;
      this.level = paramLevel;
    }

    public AppLogActivity.Level getLevel()
    {
      return this.level;
    }

    public String getText()
    {
      return this.text;
    }
  }

  class LogEntryAdapter extends ArrayAdapter<AppLogActivity.LogEntry>
  {
    private Activity m_activity;
    private List<AppLogActivity.LogEntry> m_entries;
    private int m_resourceId;

    public LogEntryAdapter(int paramList, List<AppLogActivity.LogEntry> arg3)
    {
      super(i, localList);
      this.m_activity = paramList;
      this.m_resourceId = i;
      this.m_entries = localList;
    }

    public boolean areAllItemsEnabled()
    {
      return false;
    }

    public void clear()
    {
      this.m_entries = null;
    }

    public List<AppLogActivity.LogEntry> getLogEntries()
    {
      return this.m_entries;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      AppLogActivity.LogEntry localLogEntry = (AppLogActivity.LogEntry)this.m_entries.get(paramInt);
      if (paramView == null);
      for (TextView localTextView = (TextView)this.m_activity.getLayoutInflater().inflate(this.m_resourceId, null); ; localTextView = (TextView)paramView)
      {
        localTextView.setText(localLogEntry.getText());
        localTextView.setTextColor(localLogEntry.getLevel().getColor());
        return localTextView;
      }
    }

    public boolean isEnabled(int paramInt)
    {
      return false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.AppLogActivity
 * JD-Core Version:    0.6.2
 */