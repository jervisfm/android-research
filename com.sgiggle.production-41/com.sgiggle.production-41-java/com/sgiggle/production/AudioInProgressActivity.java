package com.sgiggle.production;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.format.DateUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.VideoView;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.media_engine.MediaEngineMessage.AcknowledgeCallErrorMessage;
import com.sgiggle.media_engine.MediaEngineMessage.AddVideoMessage;
import com.sgiggle.media_engine.MediaEngineMessage.AudioControlMessage;
import com.sgiggle.media_engine.MediaEngineMessage.AudioInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.CallErrorEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ClearMissedCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.FinishVideoRingbackMessage;
import com.sgiggle.media_engine.MediaEngineMessage.LikeVideoRingbackMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SendCallInvitationEvent;
import com.sgiggle.media_engine.MediaEngineMessage.SkipVideoRingbackMessage;
import com.sgiggle.media_engine.MediaEngineMessage.TerminateCallMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.pjmedia.AudioModeWrapper;
import com.sgiggle.pjmedia.SoundEffWrapper;
import com.sgiggle.production.dialog.TangoAlertDialog;
import com.sgiggle.production.factory.proximity.AbstractProximityFactory;
import com.sgiggle.production.factory.proximity.handler.AbstractProximityHandler;
import com.sgiggle.production.manager.ProximityManager;
import com.sgiggle.production.manager.ProximityManager.Listener;
import com.sgiggle.production.receiver.BluetoothButtonReceiver;
import com.sgiggle.production.receiver.BluetoothButtonReceiver.ButtonHandler;
import com.sgiggle.production.receiver.WifiLockReceiver;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;

public class AudioInProgressActivity extends ActivityBase
  implements View.OnClickListener, ProximityManager.Listener, DialogInterface.OnCancelListener
{
  private static final int DIALOG_CALL_ERROR = 3;
  private static final int DIALOG_CALL_ON_HOLD = 0;
  private static final int DIALOG_IGNORED_CALL = 2;
  private static final int DIALOG_LEAVE_MESSAGE = 1;
  private static final int FINISH_ACTIVITY = 6;
  private static final int FINISH_ACTIVITY_DELAY = 1000;
  private static final int REJECT_CALL_DELAY = 700;
  private static final int SEND_REJECT_CALL = 2;
  private static final int SHOW_IGNORED_CALL = 3;
  private static final int SHOW_IGNORED_CALL_DELAY = 500;
  private static final int SHOW_INCALL_CONTROLS = 1;
  private static final int SHOW_INCALL_CONTROLS_DELAY = 500;
  private static final String TAG = "Tango.AudioUI";
  private static final int TIME_UPDATE_INTERVAL = 250;
  private MediaEngineMessage.CallErrorEvent _callErrorEvent;
  private View answerCallControls;
  private Button m_answerCallBtn;
  private BluetoothButtonReceiver m_btBtnReceiver = new BluetoothButtonReceiver();
  private boolean m_callErrorDialogShowing = false;
  private boolean m_callOnHoldDialogShowing = false;
  private TextView m_callStatusTextView;
  private Button m_declineCallBtn;
  private TextView m_elapsedTimeTextView;
  private RelativeLayout m_endButton;
  private ImageView m_footerImage;
  private PowerManager.WakeLock m_fullWakeLock;
  private Handler m_handler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      if (AudioInProgressActivity.this.m_isDestroyed)
      {
        Log.d("Tango.AudioUI", "Handler: ignoring message " + paramAnonymousMessage + "; we're destroyed!");
        return;
      }
      Log.d("Tango.AudioUI", "handleMessage " + paramAnonymousMessage + " call state = " + AudioInProgressActivity.this.m_session.m_callState);
      switch (paramAnonymousMessage.what)
      {
      case 4:
      case 5:
      default:
        return;
      case 1:
        AudioInProgressActivity.this.m_inCallControls.setVisibility(0);
        return;
      case 2:
        CallHandler.getDefault().rejectIncomingCall();
        return;
      case 3:
        AudioInProgressActivity.this.showIgnoredCallAlert(AudioInProgressActivity.this.m_session.m_peerName);
        return;
      case 6:
      }
      AudioInProgressActivity.this.finish();
    }
  };
  private boolean m_ignoredCallDialogShowing = false;
  private View m_inCallControls;
  private LinearLayout m_inCallControlsRow2;
  private boolean m_isDestroyed = false;
  private boolean m_leaveMessageDialogShowing = false;
  private ViewGroup m_mainFrame;
  private ToggleButton m_muteButton;
  private TextView m_nameTextView;
  private String m_peerDisplayname;
  private ImageView m_photoImageView;
  private AbstractProximityFactory m_proximityFactory;
  private ProximityManager m_proximityManager;
  private LinearLayout m_rbButtons;
  private boolean m_rbFinishedPlaying;
  private RelativeLayout m_rbLayout;
  private VideoView m_rbPlayerView;
  private CallSession m_session;
  private ToggleButton m_speakerButton;
  private int m_textColorConnected;
  private int m_textColorEnded;
  private Handler m_timeHandler = new Handler();
  private LinearLayout m_topUserInfo;
  private Runnable m_updateTimeTask = new Runnable()
  {
    public void run()
    {
      if (AudioInProgressActivity.this.m_session.m_callStartTime > 0L);
      for (long l = (System.currentTimeMillis() - AudioInProgressActivity.this.m_session.m_callStartTime) / 1000L; ; l = 0L)
      {
        AudioInProgressActivity.this.updateElapsedTimeWidget(l);
        AudioInProgressActivity.this.m_timeHandler.postDelayed(this, 250L);
        return;
      }
    }
  };
  private ToggleButton m_videoButton;
  private WifiLockReceiver m_wifiLockReceiver;

  private void acceptIncomingCall()
  {
    Log.d("Tango.AudioUI", "LEFT_HANDLE: answer!");
    hideIncomingCallWidget();
    CallHandler.getDefault().answerIncomingCall();
    this.m_handler.sendEmptyMessageDelayed(1, 500L);
  }

  private void clearIgnoredCall()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ClearMissedCallMessage(this.m_session.getPeerAccountId()));
  }

  private Dialog createCallErrorDialog()
  {
    if (this._callErrorEvent == null)
      return null;
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage(Utils.getStringFromResource(this, ((SessionMessages.OptionalPayload)this._callErrorEvent.payload()).getMessage())).setCancelable(true).setPositiveButton(2131296295, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.AcknowledgeCallErrorMessage(((SessionMessages.OptionalPayload)AudioInProgressActivity.this._callErrorEvent.payload()).getMessage()));
      }
    }).setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.AcknowledgeCallErrorMessage(((SessionMessages.OptionalPayload)AudioInProgressActivity.this._callErrorEvent.payload()).getMessage()));
      }
    });
    return localBuilder.create();
  }

  private Dialog createCallOnHoldDialog()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage(2131296408).setCancelable(true).setPositiveButton(2131296409, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Intent localIntent = new Intent();
        localIntent.setAction("android.intent.action.VoIP_RESUME_CALL");
        localIntent.putExtra("ResumeType", 1);
        AudioInProgressActivity.this.sendBroadcast(localIntent);
      }
    }).setNegativeButton(2131296410, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        AudioInProgressActivity.this.hangUpCall();
        AudioInProgressActivity.this.moveTaskToBack(true);
      }
    });
    return localBuilder.create();
  }

  private Dialog createIgnoredCallDialog()
  {
    String str1 = getResources().getString(2131296327);
    String str2 = getResources().getString(2131296328);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = this.m_peerDisplayname;
    String str3 = String.format(str2, arrayOfObject);
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage(str3).setCancelable(true).setTitle(str1).setPositiveButton(2131296287, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Log.d("Tango.AudioUI", "Ignored-Call OK button clicked.");
        AudioInProgressActivity.this.clearIgnoredCall();
      }
    }).setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        Log.d("Tango.AudioUI", "onCancel(DialogInterface) Cancel the Ignored-Call dialog...");
        AudioInProgressActivity.this.clearIgnoredCall();
      }
    });
    return localBuilder.create();
  }

  private Dialog createLeaveMessageDialog()
  {
    this.m_leaveMessageDialogShowing = true;
    TangoAlertDialog localTangoAlertDialog = Utils.LeaveMessageDialogBuilder.create(this, this.m_session.m_peerName, false);
    localTangoAlertDialog.setExtraOnDismissListener(new DialogInterface.OnDismissListener()
    {
      public void onDismiss(DialogInterface paramAnonymousDialogInterface)
      {
        AudioInProgressActivity.access$1302(AudioInProgressActivity.this, false);
      }
    });
    return localTangoAlertDialog;
  }

  private void dismissKeyguard(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      getWindow().addFlags(4194304);
      getWindow().addFlags(524288);
      getWindow().addFlags(2097152);
      return;
    }
    getWindow().clearFlags(4194304);
    getWindow().clearFlags(524288);
    getWindow().clearFlags(2097152);
  }

  private void handleAudioInProgressEvent()
  {
    onAudioModeUpdated();
    this.m_callStatusTextView.setText(getResources().getString(2131296337));
    Bitmap localBitmap = this.m_session.getPeerPhoto();
    if (localBitmap != null)
      this.m_photoImageView.setImageBitmap(localBitmap);
    setVolumeControlStream(0);
  }

  private void handleCallErrorEvent(MediaEngineMessage.CallErrorEvent paramCallErrorEvent)
  {
    Log.d("Tango.AudioUI", "Handle Call Error Event: displaying call error dialog");
    this._callErrorEvent = paramCallErrorEvent;
    showDialog(3);
  }

  private void handleIncomingCallEvent()
  {
    dismissPendingIgnoredCallAlert();
    dismissPendingRecordVideoMessageAlert();
    dismissPendingCallErrorAlert();
    dismissPendingCallOnHoldAlert();
    showIncomingCallWidget();
    this.m_inCallControls.setVisibility(8);
    this.m_nameTextView.setText(this.m_session.m_peerName);
    this.m_callStatusTextView.setText(getResources().getString(2131296335));
    Bitmap localBitmap = this.m_session.getPeerPhoto();
    if (localBitmap != null)
      this.m_photoImageView.setImageBitmap(localBitmap);
  }

  private void handleSendCallInvitationEvent()
  {
    hideIncomingCallWidget();
    this.m_inCallControls.setVisibility(0);
    this.m_nameTextView.setText(this.m_session.m_peerName);
    this.m_callStatusTextView.setText(getResources().getString(2131296334));
    Bitmap localBitmap = this.m_session.getPeerPhoto();
    if (localBitmap != null)
      this.m_photoImageView.setImageBitmap(localBitmap);
    updateInCallBackground();
    updateInCallControls();
  }

  private void hangUpCall()
  {
    this.m_session.m_callState = CallSession.CallState.CALL_STATE_DISCONNECTING;
    updateInCallBackground();
    updateInCallControls();
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.TerminateCallMessage(this.m_session.getPeerAccountId()));
  }

  private void hideIncomingCallWidget()
  {
    this.answerCallControls.setVisibility(8);
  }

  private void likeVideoRingBack()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.LikeVideoRingbackMessage());
  }

  private void moveToBackgroundIfNeeded()
  {
    int i = getIntent().getIntExtra("com.sgiggle.production.tangoapp.EXTRA_APP_STATE", TangoApp.AppState.APP_STATE_FOREGROUND.ordinal());
    Log.d("Tango.AudioUI", "AppState (Before-call): " + i);
    if ((isFinishing()) && ((i == TangoApp.AppState.APP_STATE_BACKGROUND.ordinal()) || (i == TangoApp.AppState.APP_STATE_RESUMING.ordinal())))
      moveTaskToBack(true);
  }

  private void rejectCall()
  {
    this.m_callStatusTextView.setText(getResources().getString(2131296338));
    this.m_session.m_callState = CallSession.CallState.CALL_STATE_DISCONNECTED;
    updateInCallBackground();
    this.m_handler.sendEmptyMessageDelayed(2, 700L);
  }

  private void showIgnoredCallAlert(String paramString)
  {
    this.m_peerDisplayname = paramString;
    stopVideoRingback();
    showDialog(2);
  }

  private void showIncomingCallWidget()
  {
    this.answerCallControls.setVisibility(0);
    this.m_footerImage.setVisibility(0);
  }

  private void skipVideoRingBack()
  {
    MediaEngineMessage.SkipVideoRingbackMessage localSkipVideoRingbackMessage = new MediaEngineMessage.SkipVideoRingbackMessage(this.m_rbFinishedPlaying);
    MessageRouter.getInstance().postMessage("jingle", localSkipVideoRingbackMessage);
    stopVideoRingback();
  }

  private void updateElapsedTimeWidget(long paramLong)
  {
    if (paramLong == 0L)
    {
      this.m_elapsedTimeTextView.setText("");
      return;
    }
    this.m_elapsedTimeTextView.setText(DateUtils.formatElapsedTime(paramLong));
  }

  private void updateInCallBackground()
  {
    switch (21.$SwitchMap$com$sgiggle$production$CallSession$CallState[this.m_session.m_callState.ordinal()])
    {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    }
    this.m_mainFrame.setBackgroundResource(2130837518);
  }

  private void updateInCallControls()
  {
    switch (21.$SwitchMap$com$sgiggle$production$CallSession$CallState[this.m_session.m_callState.ordinal()])
    {
    case 1:
    default:
      return;
    case 2:
    case 3:
      this.m_endButton.setEnabled(true);
      this.m_videoButton.setEnabled(false);
      this.m_muteButton.setEnabled(true);
      this.m_speakerButton.setEnabled(true);
      return;
    case 4:
      this.m_endButton.setEnabled(true);
      this.m_videoButton.setEnabled(true);
      this.m_muteButton.setEnabled(true);
      this.m_speakerButton.setEnabled(true);
      return;
    case 5:
    case 6:
    }
    this.m_endButton.setEnabled(false);
    this.m_videoButton.setEnabled(false);
    this.m_muteButton.setEnabled(false);
    this.m_speakerButton.setEnabled(false);
  }

  public void dismissPendingCallErrorAlert()
  {
    if (this.m_callErrorDialogShowing);
    try
    {
      removeDialog(3);
      label12: this.m_callErrorDialogShowing = false;
      return;
    }
    catch (Exception localException)
    {
      break label12;
    }
  }

  public void dismissPendingCallOnHoldAlert()
  {
    if (this.m_callOnHoldDialogShowing);
    try
    {
      removeDialog(0);
      label12: this.m_callOnHoldDialogShowing = false;
      return;
    }
    catch (Exception localException)
    {
      break label12;
    }
  }

  public void dismissPendingIgnoredCallAlert()
  {
    if (this.m_ignoredCallDialogShowing);
    try
    {
      clearIgnoredCall();
      removeDialog(1);
      label16: this.m_ignoredCallDialogShowing = false;
      return;
    }
    catch (Exception localException)
    {
      break label16;
    }
  }

  public void dismissPendingRecordVideoMessageAlert()
  {
    if (this.m_leaveMessageDialogShowing);
    try
    {
      removeDialog(1);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  void handleNewMessage(com.sgiggle.messaging.Message paramMessage)
  {
    Log.d("Tango.AudioUI", "handleNewMessage(): Message = " + paramMessage);
    if (paramMessage == null)
    {
      TangoApp.getInstance().onActivityResumeAfterKilled(this);
      return;
    }
    int i = paramMessage.getType();
    switch (i)
    {
    default:
      Log.w("Tango.AudioUI", "handleNewMessage(): Unsupported message-type = [" + i + "]");
    case 35015:
    case 35021:
    case 35071:
    case 35073:
    case 35017:
    case 35023:
    case 35085:
    case 35019:
    case 35279:
    }
    while (true)
    {
      updateInCallBackground();
      updateInCallControls();
      return;
      MediaEngineMessage.SendCallInvitationEvent localSendCallInvitationEvent = (MediaEngineMessage.SendCallInvitationEvent)paramMessage;
      handleSendCallInvitationEvent();
      if ((((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).hasVideoRingback()) && (((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).getVideoRingback().length() > 0))
      {
        String str1 = ((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).getVideoRingback();
        String str2;
        if (((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).hasVideoRingbackPrologue())
        {
          str2 = ((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).getVideoRingbackPrologue();
          label250: if ((0xF & getResources().getConfiguration().screenLayout) < 4)
            break label310;
        }
        label310: for (int j = 1; ; j = 0)
        {
          if (j == 0)
            break label316;
          Log.d("Tango.AudioUI", "do not show the video ringback since we do not have a layout for tablet");
          MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.FinishVideoRingbackMessage());
          break;
          str2 = "";
          break label250;
        }
        label316: playVideoRingback(str2, str1);
        continue;
        dismissKeyguard(true);
        if (((TangoApp)getApplication()).isLaunchFromLockScreen())
        {
          ((TangoApp)getApplication()).setLaunchFromLockScreen(false);
          hideIncomingCallWidget();
          this.m_handler.sendEmptyMessageDelayed(1, 500L);
        }
        this.m_callStatusTextView.setText(getResources().getString(2131296336));
        this.m_videoButton.setVisibility(4);
        this.m_muteButton.setVisibility(4);
        this.m_speakerButton.setVisibility(4);
        continue;
        Log.d("Tango.AudioUI", "handleNewMessage(): Audio-In-Initialization: Reset call-timer.");
        stopVideoRingback();
        continue;
        if (this.m_session.m_callState == CallSession.CallState.CALL_STATE_DIALING)
        {
          this.m_callStatusTextView.setText(getResources().getString(2131296338));
          this.m_session.m_callState = CallSession.CallState.CALL_STATE_DISCONNECTED;
          this.m_handler.sendEmptyMessageDelayed(3, 500L);
        }
        else
        {
          clearIgnoredCall();
          finish();
          continue;
          if (!((TangoApp)getApplication()).isLaunchFromLockScreen())
            dismissKeyguard(true);
          handleIncomingCallEvent();
          continue;
          stopVideoRingback();
          MediaEngineMessage.AudioInProgressEvent localAudioInProgressEvent = (MediaEngineMessage.AudioInProgressEvent)paramMessage;
          CafeMgr.initVGoodStatus(((SessionMessages.MediaSessionPayload)localAudioInProgressEvent.payload()).getVgoodSupport(), ((SessionMessages.MediaSessionPayload)localAudioInProgressEvent.payload()).getVgoodsPurchased());
          Log.e("VGOOD", "-- X -- support:" + CafeMgr.vgoodSupport + ", purchased:" + CafeMgr.vgoodPurchased);
          handleAudioInProgressEvent();
          this.m_videoButton.setVisibility(0);
          this.m_muteButton.setVisibility(0);
          this.m_speakerButton.setVisibility(0);
          continue;
          stopVideoRingback();
          this.m_callStatusTextView.setText(getResources().getString(2131296338));
          this.m_session.m_callState = CallSession.CallState.CALL_STATE_DISCONNECTED;
          this.m_elapsedTimeTextView.setTextColor(this.m_textColorEnded);
          this.m_handler.sendEmptyMessageDelayed(6, 1000L);
          continue;
          stopVideoRingback();
          handleCallErrorEvent((MediaEngineMessage.CallErrorEvent)paramMessage);
          continue;
          if ((!this.m_isDestroyed) && (!isFinishing()))
          {
            stopVideoRingback();
            showDialog(1);
            dismissKeyguard(false);
          }
        }
      }
    }
  }

  void onAudioModeUpdated()
  {
    Log.d("Tango.AudioUI", "Updating audio button states | Mute Button on: " + this.m_session.m_muted + " | Mute on: " + AudioModeWrapper.getMicMute());
    this.m_speakerButton.setChecked(this.m_session.m_speakerOn);
    this.m_muteButton.setChecked(this.m_session.m_muted);
  }

  public void onBackPressed()
  {
    Log.d("Tango.AudioUI", "onBackPressed() Ignore the Back key.");
  }

  public void onCancel(DialogInterface paramDialogInterface)
  {
  }

  public void onClick(View paramView)
  {
    int i = paramView.getId();
    switch (i)
    {
    default:
      Log.w("Tango.AudioUI", "onClick: unexpected click: View " + paramView + ", id " + i);
      return;
    case 2131361821:
      Log.d("Tango.AudioUI", "onClick(hangup_button)");
      hangUpCall();
      return;
    case 2131361820:
      Log.d("Tango.AudioUI", "onClick(send_video_button)");
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.AddVideoMessage(this.m_session.getPeerAccountId()));
      return;
    case 2131361818:
      Log.d("Tango.AudioUI", "onClick(mute_button)");
      MessageRouter localMessageRouter2 = MessageRouter.getInstance();
      if (!this.m_session.m_muted);
      for (boolean bool2 = true; ; bool2 = false)
      {
        localMessageRouter2.postMessage("jingle", MediaEngineMessage.AudioControlMessage.createMessage(2, bool2));
        return;
      }
    case 2131361819:
    }
    Log.d("Tango.AudioUI", "onClick(speaker_button)");
    MessageRouter localMessageRouter1 = MessageRouter.getInstance();
    if (!this.m_session.m_speakerOn);
    for (boolean bool1 = true; ; bool1 = false)
    {
      localMessageRouter1.postMessage("jingle", MediaEngineMessage.AudioControlMessage.createMessage(1, bool1));
      return;
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.AudioUI", "onCreate()");
    if (!((TangoApp)getApplication()).isLaunchFromLockScreen())
      dismissKeyguard(true);
    super.onCreate(paramBundle);
    setContentView(2130903044);
    this.m_btBtnReceiver.setButtonHandler(new BluetoothButtonReceiver.ButtonHandler()
    {
      public void acceptCall()
      {
        AudioInProgressActivity.this.acceptIncomingCall();
      }

      public void rejectCall()
      {
        AudioInProgressActivity.this.hangUpCall();
      }
    });
    this.m_proximityFactory = AbstractProximityFactory.getInstance();
    this.m_fullWakeLock = ((PowerManager)getApplicationContext().getSystemService("power")).newWakeLock(268435482, "Tango.AudioUI");
    this.m_wifiLockReceiver = new WifiLockReceiver(this);
    this.m_session = CallHandler.getDefault().getCallSession();
    if (this.m_session == null)
    {
      Log.i("Tango.AudioUI", "onCreate(): Call session is null. End this screen.");
      dismissKeyguard(false);
      finish();
      moveToBackgroundIfNeeded();
      return;
    }
    TangoApp.getInstance().setAudioActivityInstance(this);
    this.m_topUserInfo = ((LinearLayout)findViewById(2131361803));
    this.answerCallControls = findViewById(2131361813);
    this.m_answerCallBtn = ((Button)findViewById(2131361814));
    this.m_answerCallBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        AudioInProgressActivity.this.acceptIncomingCall();
      }
    });
    this.m_declineCallBtn = ((Button)findViewById(2131361815));
    this.m_declineCallBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        AudioInProgressActivity.this.rejectCall();
      }
    });
    this.m_inCallControls = findViewById(2131361816);
    this.m_footerImage = ((ImageView)findViewById(2131361802));
    this.m_mainFrame = ((ViewGroup)findViewById(2131361800));
    this.m_callStatusTextView = ((TextView)findViewById(2131361807));
    this.m_elapsedTimeTextView = ((TextView)findViewById(2131361805));
    this.m_nameTextView = ((TextView)findViewById(2131361806));
    this.m_photoImageView = ((ImageView)findViewById(2131361804));
    this.m_rbPlayerView = ((VideoView)findViewById(2131361809));
    this.m_rbLayout = ((RelativeLayout)findViewById(2131361808));
    this.m_rbButtons = ((LinearLayout)findViewById(2131361810));
    Button localButton = (Button)findViewById(2131361811);
    ((ImageButton)findViewById(2131361812)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        AudioInProgressActivity.this.skipVideoRingBack();
      }
    });
    localButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        AudioInProgressActivity.this.likeVideoRingBack();
      }
    });
    this.m_inCallControlsRow2 = ((LinearLayout)findViewById(2131361817));
    this.m_videoButton = ((ToggleButton)findViewById(2131361820));
    this.m_videoButton.setOnClickListener(this);
    this.m_muteButton = ((ToggleButton)findViewById(2131361818));
    this.m_muteButton.setOnClickListener(this);
    this.m_speakerButton = ((ToggleButton)findViewById(2131361819));
    this.m_speakerButton.setOnClickListener(this);
    this.m_endButton = ((RelativeLayout)findViewById(2131361821));
    this.m_endButton.setOnClickListener(this);
    stopVideoRingback();
    this.m_textColorConnected = getResources().getColor(2131165209);
    this.m_textColorEnded = getResources().getColor(2131165210);
    this.m_elapsedTimeTextView.setTextColor(this.m_textColorConnected);
    this.m_proximityManager = new ProximityManager(this, this);
    if (this.m_session.m_callState == CallSession.CallState.CALL_STATE_ACTIVE)
      handleSendCallInvitationEvent();
    handleNewMessage(getFirstMessage());
    TangoApp.getInstance().enableKeyguard();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 0:
      this.m_callOnHoldDialogShowing = true;
      return createCallOnHoldDialog();
    case 1:
      return createLeaveMessageDialog();
    case 2:
      this.m_ignoredCallDialogShowing = true;
      return createIgnoredCallDialog();
    case 3:
    }
    this.m_callErrorDialogShowing = true;
    return createCallErrorDialog();
  }

  protected void onDestroy()
  {
    Log.d("Tango.AudioUI", "onDestroy()");
    super.onDestroy();
    if ((Build.VERSION.SDK_INT >= 8) && (((AudioManager)getSystemService("audio")).abandonAudioFocus(new AudioManager.OnAudioFocusChangeListener()
    {
      public void onAudioFocusChange(int paramAnonymousInt)
      {
      }
    }) != 1))
      Log.d("Tango.AudioUI", "onStop: could not abandon audiofocus");
    dismissPendingIgnoredCallAlert();
    TangoApp.getInstance().setAudioActivityInstance(null);
    this.m_isDestroyed = true;
    MessageRouter.getInstance().postMessage("jingle", MediaEngineMessage.AudioControlMessage.createMessage(1, false));
    moveToBackgroundIfNeeded();
  }

  public void onGrabbedStateChange(View paramView, int paramInt)
  {
    Log.v("Tango.AudioUI", "onGrabbedStateChange: grabbedState: " + paramInt);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.m_session.m_callState == CallSession.CallState.CALL_STATE_INCOMING)
      switch (paramInt)
      {
      default:
      case 24:
      case 25:
      }
    while (true)
    {
      return super.onKeyDown(paramInt, paramKeyEvent);
      Log.d("Tango.AudioUI", "On volume key pressed, silence the ringtone.");
      SoundEffWrapper.stop();
    }
  }

  protected void onPause()
  {
    Log.d("Tango.AudioUI", "onPause()");
    super.onPause();
    this.m_proximityManager.disable();
    this.m_timeHandler.removeCallbacks(this.m_updateTimeTask);
    unregisterReceiver(this.m_btBtnReceiver);
    if (isFinishing())
      dismissKeyguard(false);
    moveToBackgroundIfNeeded();
  }

  public void onProximityChanged(boolean paramBoolean)
  {
    Log.v("Tango.AudioUI", "proximityChanged(isTooClose:" + paramBoolean + ")");
    if (paramBoolean)
    {
      if (((this.m_session.m_callState == CallSession.CallState.CALL_STATE_ACTIVE) || (this.m_session.m_callState == CallSession.CallState.CALL_STATE_CONNECTING) || (this.m_session.m_callState == CallSession.CallState.CALL_STATE_DIALING)) && (!this.m_session.m_speakerOn))
        this.m_proximityFactory.getProximityHandler().handleProximityNear(this);
      return;
    }
    this.m_proximityFactory.getProximityHandler().handleProximityFar(this);
  }

  protected void onRestart()
  {
    Log.d("Tango.AudioUI", "onRestart()");
    super.onRestart();
    if (!isFinishing())
      MessageRouter.getInstance().postMessage("jingle", MediaEngineMessage.AudioControlMessage.createMessage(1, this.m_session.m_speakerOn));
  }

  protected void onResume()
  {
    Log.d("Tango.AudioUI", "onResume()");
    super.onResume();
    IntentFilter localIntentFilter = new IntentFilter("android.intent.action.VoIP_BLUETOOTH");
    registerReceiver(this.m_btBtnReceiver, localIntentFilter);
    if ((!isFinishing()) && (!((TangoApp)getApplication()).isLaunchFromLockScreen()) && (!this.m_leaveMessageDialogShowing))
      dismissKeyguard(true);
    this.m_proximityManager.enable();
    this.m_videoButton.setChecked(false);
    this.m_timeHandler.removeCallbacks(this.m_updateTimeTask);
    this.m_timeHandler.postDelayed(this.m_updateTimeTask, 250L);
    if (this.m_session.m_callOnHold)
      showDialog(0);
    if ((Build.VERSION.SDK_INT >= 8) && (((AudioManager)getSystemService("audio")).requestAudioFocus(new AudioManager.OnAudioFocusChangeListener()
    {
      public void onAudioFocusChange(int paramAnonymousInt)
      {
      }
    }
    , 2, 2) != 1))
      Log.d("Tango.AudioUI", "onResume: could not request audiofocus");
  }

  protected void onStart()
  {
    super.onStart();
    IntentFilter localIntentFilter = new IntentFilter();
    localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
    localIntentFilter.addAction("android.intent.action.SCREEN_ON");
    registerReceiver(this.m_wifiLockReceiver, localIntentFilter);
  }

  protected void onStop()
  {
    Log.d("Tango.AudioUI", "onStop()");
    super.onStop();
    this.m_proximityFactory.getProximityHandler().handleProximityFar(this);
    getWindow().addFlags(2097152);
    this.m_fullWakeLock.acquire(30000L);
    this.m_wifiLockReceiver.releaseWifiLock(this);
    try
    {
      unregisterReceiver(this.m_wifiLockReceiver);
      return;
    }
    catch (Exception localException)
    {
      Log.e("Tango.AudioUI", "got exception for unregisterReceiver()");
      localException.printStackTrace();
    }
  }

  public void playVideoRingback(String paramString1, final String paramString2)
  {
    this.m_rbLayout.setVisibility(0);
    this.m_rbButtons.setVisibility(0);
    this.m_topUserInfo.setVisibility(8);
    this.m_inCallControlsRow2.setVisibility(8);
    this.m_rbFinishedPlaying = false;
    boolean bool = paramString1.equals("");
    if (bool);
    for (String str = paramString2; ; str = paramString1)
    {
      Log.d("Tango.AudioUI", "video ringback to play " + str);
      this.m_rbPlayerView.setVideoPath(str);
      this.m_rbPlayerView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
      {
        public void onPrepared(MediaPlayer paramAnonymousMediaPlayer)
        {
          paramAnonymousMediaPlayer.setLooping(false);
        }
      });
      this.m_rbPlayerView.setOnErrorListener(new MediaPlayer.OnErrorListener()
      {
        public boolean onError(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt1, int paramAnonymousInt2)
        {
          Log.e("Tango.AudioUI", "Error occurs when playing video ringback, reason " + paramAnonymousInt1 + " extra " + paramAnonymousInt2);
          AudioInProgressActivity.this.stopVideoRingback();
          MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.FinishVideoRingbackMessage());
          return true;
        }
      });
      if (bool)
        this.m_rbPlayerView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
          public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
          {
            Log.d("Tango.AudioUI", "video ringback finished playing");
            AudioInProgressActivity.access$1402(AudioInProgressActivity.this, true);
            AudioInProgressActivity.this.m_rbButtons.setVisibility(8);
            MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.FinishVideoRingbackMessage());
          }
        });
      while (true)
      {
        this.m_rbPlayerView.start();
        return;
        this.m_rbPlayerView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
          public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
          {
            Log.d("Tango.AudioUI", "video ringback to play " + paramString2);
            AudioInProgressActivity.this.m_rbPlayerView.setVideoPath(paramString2);
            AudioInProgressActivity.this.m_rbPlayerView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
              public void onCompletion(MediaPlayer paramAnonymous2MediaPlayer)
              {
                Log.d("Tango.AudioUI", "video ringback finished playing");
                AudioInProgressActivity.access$1402(AudioInProgressActivity.this, true);
                AudioInProgressActivity.this.m_rbButtons.setVisibility(8);
                MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.FinishVideoRingbackMessage());
              }
            });
            AudioInProgressActivity.this.m_rbPlayerView.start();
          }
        });
      }
    }
  }

  public void stopVideoRingback()
  {
    this.m_rbPlayerView.stopPlayback();
    this.m_rbLayout.setVisibility(8);
    this.m_topUserInfo.setVisibility(0);
    this.m_inCallControlsRow2.setVisibility(0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.AudioInProgressActivity
 * JD-Core Version:    0.6.2
 */