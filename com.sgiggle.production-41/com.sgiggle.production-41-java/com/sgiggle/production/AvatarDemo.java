package com.sgiggle.production;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.cafe.vgood.CafeRenderer;
import com.sgiggle.cafe.vgood.CafeViewForCanvasRenderer;
import com.sgiggle.media_engine.MediaEngineMessage.AvatarBackgroundCleanupMessage;
import com.sgiggle.media_engine.MediaEngineMessage.AvatarRenderRequestEvent;
import com.sgiggle.media_engine.MediaEngineMessage.BackToAvatarProductCatalogMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayAvatarProductDetailEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayDemoAvatarEvent;
import com.sgiggle.media_engine.MediaEngineMessage.PurchaseAvatarMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ReportPurchaseResultEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ReportPurchasedAvatarMessage;
import com.sgiggle.media_engine.MediaEngineMessage.StartDemoAvatarMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.adapter.VGoodSelectorAdapter;
import com.sgiggle.production.avatar.AvatarRenderer;
import com.sgiggle.production.payments.BillingServiceManager;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.production.receiver.ScreenReceiver;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload;
import com.sgiggle.xmpp.SessionMessages.AvatarRenderRequestPayload;
import com.sgiggle.xmpp.SessionMessages.Price;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductDetailsPayload;
import com.sgiggle.xmpp.SessionMessages.PurchaseResultPayload;
import com.sgiggle.xmpp.SessionMessages.VGoodBundle;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AvatarDemo extends BillingSupportBaseActivity
  implements View.OnClickListener, AdapterView.OnItemClickListener
{
  private static final int HIDE_TIP_BUBBLE = 999;
  private static final String TAG = "AvatarDemo";
  private static final int TIP_WAIT_TIME = 4000;
  private static AvatarDemo s_instance;
  private View bottomBubble;
  private Handler handlerEvent = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        super.handleMessage(paramAnonymousMessage);
        return;
      case 999:
      }
      AvatarDemo.this.bottomBubble.setVisibility(8);
    }
  };
  private Handler handlerTimer = new Handler();
  private VGoodSelectorAdapter m_adapter;
  private boolean m_avatarPaused = false;
  private AvatarRenderer m_avatarRenderer;
  private CafeViewForCanvasRenderer m_avatarSurfaceView;
  private boolean m_avatarUpdated = false;
  private boolean m_btnClicked = false;
  private Button m_buy;
  private boolean m_is_incoming_call;
  private PhoneStateListener m_phoneStateListener = new PhoneStateListener()
  {
    public void onCallStateChanged(int paramAnonymousInt, String paramAnonymousString)
    {
      if (paramAnonymousInt == 1)
      {
        Log.d("AvatarDemo", "onCallStateChanged(int state, String incomingNumber) CALL_STATE_RINGING");
        AvatarDemo.access$102(AvatarDemo.this, true);
      }
      while (true)
      {
        super.onCallStateChanged(paramAnonymousInt, paramAnonymousString);
        return;
        if ((paramAnonymousInt == 0) || (paramAnonymousInt == 2))
        {
          Log.d("AvatarDemo", "onCallStateChanged(int state, String incomingNumber) CALL_STATE_IDLE || CALL_STATE_OFFHOOK");
          AvatarDemo.access$102(AvatarDemo.this, false);
        }
      }
    }
  };
  private PowerManager m_pm;
  SessionMessages.ProductCatalogEntry m_product;
  private String m_productMarketId;
  ProgressDialog m_progressDialog;
  private boolean m_purchased = false;
  private BroadcastReceiver m_receiver;
  private View m_selector;
  private Animation m_selectorIn;
  private ImageView m_wand;
  private Runnable taskHideTipBubble = new Runnable()
  {
    public void run()
    {
      android.os.Message localMessage = new android.os.Message();
      localMessage.what = 999;
      AvatarDemo.this.handlerEvent.sendMessage(localMessage);
    }
  };
  private View wandBubble;

  private static void clearActiveInstance(AvatarDemo paramAvatarDemo)
  {
    if (s_instance == paramAvatarDemo)
      s_instance = null;
  }

  public static AvatarDemo getActiveInstance()
  {
    return s_instance;
  }

  private void hideWand()
  {
    this.wandBubble.setVisibility(8);
    ((TextView)this.bottomBubble.findViewById(2131361827)).setText(2131296540);
    this.m_wand.setImageResource(2130837727);
    this.m_wand.setEnabled(false);
    this.m_selector.setVisibility(0);
  }

  private void markProductPurchased()
  {
    this.m_purchased = true;
    this.m_buy.setEnabled(false);
    this.m_buy.setText(2131296507);
  }

  private void onBuyClick()
  {
    if (this.m_progressDialog != null)
      return;
    showProgressDialog();
    if ((this.m_product.hasPrice()) && (this.m_product.getPrice().getValue() > 0.0F))
    {
      purchase(this.m_product);
      return;
    }
    purchaseFree(this.m_product);
  }

  private void onWandClick()
  {
  }

  private void purchaseFree(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    String str1 = paramProductCatalogEntry.getProductMarketId();
    String str2 = paramProductCatalogEntry.getExternalMarketId();
    long l = new Date().getTime();
    String str3 = String.valueOf(l);
    MediaEngineMessage.PurchaseAvatarMessage localPurchaseAvatarMessage = new MediaEngineMessage.PurchaseAvatarMessage(str1, str2, "Free avatar", 1, str3, l / 1000L, str3, false);
    MessageRouter.getInstance().postMessage("jingle", localPurchaseAvatarMessage);
  }

  private void reportPurchased(MediaEngineMessage.ReportPurchaseResultEvent paramReportPurchaseResultEvent)
  {
    this.m_avatarUpdated = false;
    markProductPurchased();
    this.m_avatarRenderer.onAvatarChanged(null);
    TangoApp.getInstance().setAvatarDemoActivity(null);
    MediaEngineMessage.ReportPurchasedAvatarMessage localReportPurchasedAvatarMessage = new MediaEngineMessage.ReportPurchasedAvatarMessage(this.m_product.getProductMarketId(), this.m_product.getExternalMarketId(), 1, new Date().getTime());
    MessageRouter.getInstance().postMessage("jingle", localReportPurchasedAvatarMessage);
    super.onBackPressed();
  }

  private static void setActiveInstance(AvatarDemo paramAvatarDemo)
  {
    s_instance = paramAvatarDemo;
  }

  public void confirmPurchaseFailed()
  {
  }

  public void goBack()
  {
    this.m_avatarUpdated = false;
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.BackToAvatarProductCatalogMessage());
    super.onBackPressed();
  }

  void handleDisplay(MediaEngineMessage.DisplayAvatarProductDetailEvent paramDisplayAvatarProductDetailEvent)
  {
    List localList = ((SessionMessages.ProductDetailsPayload)paramDisplayAvatarProductDetailEvent.payload()).getVgoodBundleList();
    Log.d("AvatarDemo", "avatars bundle size:" + Integer.toString(localList.size()));
    if (localList.size() <= 0);
    do
    {
      return;
      showProgressDialog();
      this.m_avatarUpdated = false;
      this.m_product = ((SessionMessages.ProductDetailsPayload)paramDisplayAvatarProductDetailEvent.payload()).getProduct();
      this.m_productMarketId = this.m_product.getProductMarketId();
      String str = this.m_product.getPrice().getLabel();
      ((TextView)findViewById(2131361832)).setText(((SessionMessages.ProductDetailsPayload)paramDisplayAvatarProductDetailEvent.payload()).getProduct().getProductName());
      this.m_buy.setText(str);
      this.m_buy.setVisibility(0);
      Constants.PurchaseState localPurchaseState = (Constants.PurchaseState)BillingServiceManager.getInstance().purchaseStateMap.get(((SessionMessages.ProductDetailsPayload)paramDisplayAvatarProductDetailEvent.payload()).getProduct().getProductMarketId());
      if ((((SessionMessages.ProductDetailsPayload)paramDisplayAvatarProductDetailEvent.payload()).getProduct().getPurchased()) || (localPurchaseState == Constants.PurchaseState.PURCHASED))
        markProductPurchased();
    }
    while (!((SessionMessages.ProductDetailsPayload)paramDisplayAvatarProductDetailEvent.payload()).getAllCached());
    SessionMessages.VGoodBundle localVGoodBundle = (SessionMessages.VGoodBundle)localList.get(0);
    if (localVGoodBundle != null)
      play(localVGoodBundle.getCinematic().getAssetPath(), localVGoodBundle.getCinematic().getAssetId());
    this.m_wand.setVisibility(8);
    this.m_wand.setImageResource(2130837727);
    this.wandBubble.setVisibility(8);
  }

  void handleNewMessage(com.sgiggle.messaging.Message paramMessage)
  {
    if (paramMessage == null)
    {
      TangoApp.getInstance().onActivityResumeAfterKilled(this);
      return;
    }
    MediaEngineMessage.ReportPurchaseResultEvent localReportPurchaseResultEvent;
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35200:
      localReportPurchaseResultEvent = (MediaEngineMessage.ReportPurchaseResultEvent)paramMessage;
      if (!((SessionMessages.PurchaseResultPayload)localReportPurchaseResultEvent.payload()).hasError())
        break;
    case 35247:
    case 35248:
    case 35245:
    }
    while (true)
    {
      hideProgressDialog();
      this.m_btnClicked = false;
      return;
      handleDisplay((MediaEngineMessage.DisplayAvatarProductDetailEvent)paramMessage);
      return;
      startAvatar((SessionMessages.AvatarControlPayload)((MediaEngineMessage.DisplayDemoAvatarEvent)paramMessage).payload());
      return;
      MediaEngineMessage.AvatarRenderRequestEvent localAvatarRenderRequestEvent = (MediaEngineMessage.AvatarRenderRequestEvent)paramMessage;
      if ((this.m_avatarRenderer == null) || (this.m_avatarPaused))
        break;
      this.m_avatarRenderer.handleRenderRequest(localAvatarRenderRequestEvent);
      if ((this.m_avatarUpdated) || (((SessionMessages.AvatarRenderRequestPayload)localAvatarRenderRequestEvent.payload()).getUpdatedTracks() == 0))
        break;
      this.handlerTimer.removeCallbacks(this.taskHideTipBubble);
      this.handlerTimer.postDelayed(this.taskHideTipBubble, 4000L);
      this.m_avatarUpdated = true;
      return;
      reportPurchased(localReportPurchaseResultEvent);
    }
  }

  void hideProgressDialog()
  {
    if (this.m_progressDialog != null)
    {
      this.m_progressDialog.dismiss();
      this.m_progressDialog = null;
    }
  }

  void onAnimationFinished()
  {
    this.m_selector.setEnabled(true);
  }

  public void onBackPressed()
  {
    this.m_avatarRenderer.onAvatarChanged(null);
    TangoApp.getInstance().setAvatarDemoActivity(null);
    goBack();
  }

  public void onClick(View paramView)
  {
    if (!this.m_btnClicked)
    {
      this.m_btnClicked = true;
      if (paramView.getId() != 2131361835)
        break label27;
      onWandClick();
    }
    label27: 
    while (paramView.getId() != 2131361833)
      return;
    onBuyClick();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    setContentView(2130903046);
    TangoApp.getInstance().setAvatarDemoActivity(this);
    Log.d("AvatarDemo", "onCreate(Bundle savedInstanceState) enter");
    this.m_receiver = null;
    this.m_pm = ((PowerManager)getSystemService("power"));
    this.m_avatarSurfaceView = ((CafeViewForCanvasRenderer)findViewById(2131361830));
    this.m_avatarSurfaceView.setZOrderMediaOverlay(true);
    this.m_avatarRenderer = new AvatarRenderer(0, true, null);
    this.m_avatarSurfaceView.getRenderer().addCafeViewRenderer(this.m_avatarRenderer);
    this.m_wand = ((ImageView)findViewById(2131361835));
    this.m_wand.setOnClickListener(this);
    this.m_wand.setVisibility(8);
    this.wandBubble = findViewById(2131361836);
    this.bottomBubble = findViewById(2131361837);
    this.wandBubble.findViewById(2131361826).setVisibility(8);
    this.bottomBubble.findViewById(2131361826).setVisibility(8);
    this.m_buy = ((Button)findViewById(2131361833));
    this.m_buy.setOnClickListener(this);
    findViewById(2131361834).bringToFront();
    findViewById(2131361831).bringToFront();
    this.m_selectorIn = AnimationUtils.loadAnimation(this, 2130968584);
    CafeMgr.InitEngine(this);
    handleNewMessage(getFirstMessage());
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    this.bottomBubble.setVisibility(8);
  }

  protected void onPause()
  {
    Log.d("AvatarDemo", "Received onPause Event");
    TangoApp.getInstance().setCurrentActivityInstance(null);
    super.onPause();
    this.m_avatarRenderer.stopAvatarAnimation();
    clearActiveInstance(this);
    this.m_avatarSurfaceView.onPause();
    CafeMgr.Pause();
    CafeMgr.FreeGraphics();
    CafeMgr.RenderClear();
    if ((isFinishing()) || (this.m_is_incoming_call))
      TangoApp.getInstance().setAvatarDemoActivity(null);
    this.m_avatarPaused = true;
    if (this.m_is_incoming_call)
    {
      goBack();
      return;
    }
    MediaEngineMessage.AvatarBackgroundCleanupMessage localAvatarBackgroundCleanupMessage = new MediaEngineMessage.AvatarBackgroundCleanupMessage();
    MessageRouter.getInstance().postMessage("jingle", localAvatarBackgroundCleanupMessage);
  }

  protected void onResume()
  {
    Log.d("AvatarDemo", "Received onResume Event");
    this.m_btnClicked = false;
    super.onResume();
    this.m_avatarSurfaceView.onResume();
    CafeMgr.Resume();
    this.m_avatarRenderer.startAvatar();
    setActiveInstance(this);
    this.m_avatarPaused = false;
    TangoApp.getInstance().setCurrentActivityInstance(this);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
  }

  protected void onStart()
  {
    super.onStart();
    this.m_is_incoming_call = false;
    TelephonyManager localTelephonyManager = (TelephonyManager)getSystemService("phone");
    if (localTelephonyManager != null)
      localTelephonyManager.listen(this.m_phoneStateListener, 32);
    Log.d("AvatarDemo", "onStart() enter");
    IntentFilter localIntentFilter = new IntentFilter("android.intent.action.SCREEN_ON");
    localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
    this.m_receiver = new ScreenReceiver();
    registerReceiver(this.m_receiver, localIntentFilter);
    if (this.m_buy.getText().toString().length() == 0)
      this.m_buy.setVisibility(8);
  }

  protected void onStop()
  {
    if (this.m_receiver != null)
    {
      unregisterReceiver(this.m_receiver);
      this.m_receiver = null;
    }
    TelephonyManager localTelephonyManager = (TelephonyManager)getSystemService("phone");
    if (localTelephonyManager != null)
    {
      localTelephonyManager.listen(this.m_phoneStateListener, 0);
      this.m_phoneStateListener = null;
      this.m_is_incoming_call = false;
    }
    super.onStop();
  }

  public void onUserLeaveHint()
  {
    Log.d("AvatarDemo", "Received onUserLeaveHint Event");
    MediaEngineMessage.AvatarBackgroundCleanupMessage localAvatarBackgroundCleanupMessage = new MediaEngineMessage.AvatarBackgroundCleanupMessage();
    MessageRouter.getInstance().postMessage("jingle", localAvatarBackgroundCleanupMessage);
    super.onUserLeaveHint();
  }

  void play(String paramString, long paramLong)
  {
    MediaEngineMessage.StartDemoAvatarMessage localStartDemoAvatarMessage = new MediaEngineMessage.StartDemoAvatarMessage(paramLong);
    MessageRouter.getInstance().postMessage("jingle", localStartDemoAvatarMessage);
  }

  public void purchaseProcessed()
  {
  }

  public void setBillingSupported(boolean paramBoolean)
  {
    Button localButton = this.m_buy;
    if ((paramBoolean) && (!this.m_purchased));
    for (boolean bool = true; ; bool = false)
    {
      localButton.setEnabled(bool);
      if (!paramBoolean)
        showDialog(2);
      return;
    }
  }

  public void setPurchesedProduct(String paramString, Constants.PurchaseState paramPurchaseState)
  {
    if (paramPurchaseState == Constants.PurchaseState.PURCHASED)
      markProductPurchased();
  }

  void showProgressDialog()
  {
    hideProgressDialog();
    this.m_progressDialog = new ProgressDialog(this);
    this.m_progressDialog.setMessage(getString(2131296508));
    this.m_progressDialog.setCanceledOnTouchOutside(false);
    this.m_progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        AvatarDemo.this.onBackPressed();
      }
    });
    this.m_progressDialog.show();
  }

  void showWand(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.m_wand.setVisibility(8);
      this.wandBubble.setVisibility(0);
      ((TextView)this.wandBubble.findViewById(2131361827)).setText(2131296539);
      this.m_selector.setVisibility(8);
    }
  }

  void startAvatar(SessionMessages.AvatarControlPayload paramAvatarControlPayload)
  {
    if (this.m_avatarRenderer != null)
    {
      this.m_avatarRenderer.onAvatarChanged(paramAvatarControlPayload);
      hideProgressDialog();
      ((TextView)this.bottomBubble.findViewById(2131361827)).setText(2131296540);
      this.bottomBubble.setVisibility(0);
      this.m_wand.setVisibility(8);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.AvatarDemo
 * JD-Core Version:    0.6.2
 */