package com.sgiggle.production;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.media_engine.MediaEngineMessage.AvatarProductCatalogEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DismissStoreBadgeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayAvatarProductDetailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayStoreMessage;
import com.sgiggle.media_engine.MediaEngineMessage.PurchaseAvatarMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ReportPurchaseResultEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.adapter.AvatarPaymentAdapter;
import com.sgiggle.production.adapter.AvatarPaymentAdapter.OnClickListener;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ErrorType;
import com.sgiggle.xmpp.SessionMessages.Price;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogPayload;
import com.sgiggle.xmpp.SessionMessages.PurchaseResultPayload;
import java.util.Date;
import java.util.List;

public class AvatarSubscriptionActivity extends BillingSupportBaseActivity
  implements AvatarPaymentAdapter.OnClickListener, AdapterView.OnItemClickListener
{
  private static final String TAG = "Tango.AvatarSub";
  private static AvatarSubscriptionActivity s_instance;
  private AvatarPaymentAdapter m_adapter;
  private View m_footer;
  private ProgressDialog m_progressDialog;

  private static void clearRunningInstance(AvatarSubscriptionActivity paramAvatarSubscriptionActivity)
  {
    Log.v("Tango.AvatarSub", "clearRunningInstance");
    if (s_instance == paramAvatarSubscriptionActivity)
      s_instance = null;
  }

  private void dismissNewBadge()
  {
    MediaEngineMessage.DismissStoreBadgeMessage localDismissStoreBadgeMessage = new MediaEngineMessage.DismissStoreBadgeMessage("product.category.avatar");
    MessageRouter.getInstance().postMessage("jingle", localDismissStoreBadgeMessage);
  }

  public static AvatarSubscriptionActivity getRunningInstance()
  {
    return s_instance;
  }

  private void hideErrorMessage()
  {
    findViewById(2131361844).setVisibility(8);
    findViewById(2131361845).setVisibility(0);
  }

  private void purchaseFree(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    String str1 = paramProductCatalogEntry.getProductMarketId();
    String str2 = paramProductCatalogEntry.getExternalMarketId();
    long l = new Date().getTime();
    String str3 = String.valueOf(l);
    MediaEngineMessage.PurchaseAvatarMessage localPurchaseAvatarMessage = new MediaEngineMessage.PurchaseAvatarMessage(str1, str2, "Free avatar", 1, str3, l / 1000L, str3, false);
    MessageRouter.getInstance().postMessage("jingle", localPurchaseAvatarMessage);
  }

  private void reportPurchased(MediaEngineMessage.ReportPurchaseResultEvent paramReportPurchaseResultEvent)
  {
    this.m_adapter.setProductPurchasedFlag(((SessionMessages.PurchaseResultPayload)paramReportPurchaseResultEvent.payload()).getProductMarketId());
  }

  private static void setRunningInstance(AvatarSubscriptionActivity paramAvatarSubscriptionActivity)
  {
    Log.v("Tango.AvatarSub", "setRunningInstance");
    s_instance = paramAvatarSubscriptionActivity;
  }

  private void showDemo(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DisplayAvatarProductDetailMessage(paramProductCatalogEntry));
  }

  private void showErrorMessage()
  {
    findViewById(2131361844).setVisibility(0);
    findViewById(2131361845).setVisibility(8);
  }

  public void confirmPurchaseFailed()
  {
  }

  protected View getFooterView()
  {
    return getLayoutInflater().inflate(2130903047, null);
  }

  public void goBack()
  {
    onBackPressed();
  }

  void handleNewMessage(Message paramMessage)
  {
    Log.v("Tango.AvatarSub", "handleNewMessage");
    if (paramMessage == null)
      return;
    switch (paramMessage.getType())
    {
    default:
      super.handleNewMessage(paramMessage);
      return;
    case 35246:
      Log.v("Tango.AvatarSub", "handleNewMessage:DISPLAY_AVATAR_PRODUCT_CATALOG_TYPE");
      onProductListChanged((MediaEngineMessage.AvatarProductCatalogEvent)paramMessage);
      dismissNewBadge();
      return;
    case 35200:
    }
    MediaEngineMessage.ReportPurchaseResultEvent localReportPurchaseResultEvent = (MediaEngineMessage.ReportPurchaseResultEvent)paramMessage;
    if (((SessionMessages.PurchaseResultPayload)localReportPurchaseResultEvent.payload()).hasError())
      showErrorMessage();
    while (true)
    {
      hideProgressDialog();
      return;
      reportPurchased(localReportPurchaseResultEvent);
    }
  }

  void hideProgressDialog()
  {
    if (this.m_progressDialog != null)
    {
      this.m_progressDialog.dismiss();
      this.m_progressDialog = null;
    }
  }

  public void onBackPressed()
  {
    MediaEngineMessage.DisplayStoreMessage localDisplayStoreMessage = new MediaEngineMessage.DisplayStoreMessage();
    MessageRouter.getInstance().postMessage("jingle", localDisplayStoreMessage);
  }

  public void onClick(int paramInt, SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    Log.v("Tango.AvatarSub", "onClick");
    if (paramInt == 2131361833)
      if (this.m_progressDialog == null);
    while (paramInt != 2131361843)
    {
      return;
      showProgressDialog();
      if ((paramProductCatalogEntry.hasPrice()) && (paramProductCatalogEntry.getPrice().getValue() > 0.0F))
      {
        purchase(paramProductCatalogEntry);
        return;
      }
      purchaseFree(paramProductCatalogEntry);
      return;
    }
    showDemo(paramProductCatalogEntry);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903049);
    this.m_adapter = new AvatarPaymentAdapter(this);
    this.m_adapter.setOnClickListener(this);
    ListView localListView = (ListView)findViewById(2131361845);
    localListView.setOnItemClickListener(this);
    this.m_footer = getFooterView();
    localListView.addFooterView(this.m_footer, null, false);
    localListView.setAdapter(this.m_adapter);
    localListView.setDividerHeight(0);
    localListView.setEmptyView(findViewById(16908292));
    handleNewMessage(getFirstMessage());
    setRunningInstance(this);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Log.v("Tango.AvatarSub", "onDestroy");
    clearRunningInstance(this);
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    showDemo((SessionMessages.ProductCatalogEntry)paramAdapterView.getItemAtPosition(paramInt));
  }

  protected void onPause()
  {
    TangoApp.getInstance().setCurrentActivityInstance(null);
    CafeMgr.StopAllSurprises();
    CafeMgr.Pause();
    CafeMgr.FreeEngine();
    super.onPause();
  }

  public void onProductListChanged(MediaEngineMessage.AvatarProductCatalogEvent paramAvatarProductCatalogEvent)
  {
    Log.v("Tango.AvatarSub", "onProductListChanged: Enter");
    if ((((SessionMessages.ProductCatalogPayload)paramAvatarProductCatalogEvent.payload()).hasError()) && (((SessionMessages.ProductCatalogPayload)paramAvatarProductCatalogEvent.payload()).getError() != SessionMessages.ErrorType.NONE_ERROR))
    {
      Log.v("Tango.AvatarSub", "onProductListChanged:evt.payload().hasError()");
      hideProgressDialog();
      showErrorMessage();
      return;
    }
    List localList = ((SessionMessages.ProductCatalogPayload)paramAvatarProductCatalogEvent.payload()).getEntryList();
    if (!localList.isEmpty())
      hideErrorMessage();
    this.m_adapter.setProducts(localList);
    if (((SessionMessages.ProductCatalogPayload)paramAvatarProductCatalogEvent.payload()).getAllCached())
    {
      Log.v("Tango.AvatarSub", "onProductListChanged:evt.payload().getAllCached()");
      ((ListView)findViewById(2131361845)).removeFooterView(this.m_footer);
      hideProgressDialog();
      return;
    }
    showProgressDialog();
  }

  protected void onResume()
  {
    super.onResume();
    Log.v("Tango.AvatarSub", "AvatarSubscriptionActivity::onResume::hashCode" + Integer.toString(hashCode()));
    TangoApp.getInstance().setCurrentActivityInstance(this);
  }

  public void purchaseProcessed()
  {
  }

  public void setBillingSupported(boolean paramBoolean)
  {
    this.m_adapter.setBillingSupported(paramBoolean);
    if (!paramBoolean)
      showDialog(2);
  }

  public void setPurchesedProduct(String paramString, Constants.PurchaseState paramPurchaseState)
  {
    this.m_adapter.notifyDataSetChanged();
  }

  void showProgressDialog()
  {
    Log.v("Tango.AvatarSub", "showProgressDialog");
    hideProgressDialog();
    this.m_progressDialog = new ProgressDialog(this);
    this.m_progressDialog.setMessage(getString(2131296510));
    this.m_progressDialog.setCanceledOnTouchOutside(false);
    this.m_progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        AvatarSubscriptionActivity.this.onBackPressed();
      }
    });
    this.m_progressDialog.show();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.AvatarSubscriptionActivity
 * JD-Core Version:    0.6.2
 */