package com.sgiggle.production;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.os.Handler;
import com.sgiggle.production.payments.BillingServiceManager;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.production.payments.Constants.ResponseCode;
import com.sgiggle.production.payments.PurchaseUtils;
import com.sgiggle.production.payments.ResponseHandler;
import com.sgiggle.production.payments.ResponseHandler.PurchaseObserver;
import com.sgiggle.production.service.BillingService.RequestPurchase;
import com.sgiggle.production.service.BillingService.RestoreTransactions;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import java.util.HashMap;

public abstract class BillingSupportBaseActivity extends ActivityBase
{
  protected static final int DIALOG_BILLING_NOT_SUPPORTED = 2;
  protected static final int DIALOG_SHOW_ERROR = 1;
  private static final String TAG = "billingSupportActivity";
  private boolean isActive = false;
  private BillingPurchaseObserver m_purchaseObserver = new BillingPurchaseObserver(null);

  public abstract void confirmPurchaseFailed();

  public abstract void goBack();

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 2:
      return PurchaseUtils.getBillingNotSupportedDialog(this);
    case 1:
    }
    return PurchaseUtils.getPurchaseFailedDialog(this, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        BillingSupportBaseActivity.this.confirmPurchaseFailed();
      }
    });
  }

  protected void onPause()
  {
    Log.i("billingSupportActivity", "onPuase");
    super.onPause();
    this.isActive = false;
    ((TangoApp)getApplication()).getResponseHandler().unregisterObserver(this.m_purchaseObserver);
    BillingServiceManager.getInstance().unbindService();
  }

  public void onPurchaseCancelled()
  {
  }

  protected void onResume()
  {
    Log.i("billingSupportActivity", "onResume");
    super.onResume();
    this.isActive = true;
    ((TangoApp)getApplication()).getResponseHandler().registerObserver(this.m_purchaseObserver);
    Object localObject = getParent();
    if (localObject == null)
      localObject = this;
    BillingServiceManager.getInstance().bindServiceAndSetContext((Context)localObject);
  }

  protected void purchase(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    String str1 = paramProductCatalogEntry.getProductMarketId();
    String str2 = paramProductCatalogEntry.getExternalMarketId();
    BillingServiceManager.getInstance().requestPurchase(str2, str1);
  }

  public abstract void purchaseProcessed();

  public abstract void setBillingSupported(boolean paramBoolean);

  public abstract void setPurchesedProduct(String paramString, Constants.PurchaseState paramPurchaseState);

  private class BillingPurchaseObserver
    implements ResponseHandler.PurchaseObserver
  {
    Handler m_handler = new Handler();

    private BillingPurchaseObserver()
    {
    }

    public void onBillingSupported(boolean paramBoolean)
    {
      Log.i("BillingService", "onBillingSupported:" + paramBoolean);
      BillingServiceManager.getInstance().setSupported(paramBoolean);
      BillingSupportBaseActivity.this.setBillingSupported(paramBoolean);
      if (paramBoolean)
        BillingServiceManager.getInstance().restoreTransactions();
    }

    public void onResponseCodeReceived(BillingService.RequestPurchase paramRequestPurchase, Constants.ResponseCode paramResponseCode)
    {
      Log.i("BillingService", "onResponseCodeReceived, responseCode:" + paramResponseCode);
      switch (BillingSupportBaseActivity.2.$SwitchMap$com$sgiggle$production$payments$Constants$ResponseCode[paramResponseCode.ordinal()])
      {
      default:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      }
      do
      {
        return;
        BillingSupportBaseActivity.this.purchaseProcessed();
        return;
        BillingSupportBaseActivity.this.setPurchesedProduct(null, Constants.PurchaseState.CANCELED);
        BillingSupportBaseActivity.this.onPurchaseCancelled();
        return;
        BillingSupportBaseActivity.this.setPurchesedProduct(null, Constants.PurchaseState.PURCHASE);
        return;
        BillingSupportBaseActivity.this.setPurchesedProduct(null, Constants.PurchaseState.PURCHASE);
      }
      while (!BillingSupportBaseActivity.this.isActive);
      BillingSupportBaseActivity.this.showDialog(1);
    }

    public void onResponseCodeReceived(BillingService.RestoreTransactions paramRestoreTransactions, Constants.ResponseCode paramResponseCode)
    {
      Log.i("BillingService", "Restore Transactions response received: " + paramResponseCode);
      if (paramResponseCode == Constants.ResponseCode.RESULT_OK)
      {
        TangoApp.setStoreTransactionRestored();
        Log.i("BillingService", "Completed restoring transactions");
        return;
      }
      Log.e("BillingService", "Restore Transactions Error: " + paramResponseCode);
    }

    public void postPurchaseStateChange(final Constants.PurchaseState paramPurchaseState, final String paramString1, final String paramString2, final long paramLong, String paramString3, final String paramString4, final String paramString5)
    {
      this.m_handler.post(new Runnable()
      {
        public void run()
        {
          BillingSupportBaseActivity.BillingPurchaseObserver.this.postPurchaseStateChange2(paramPurchaseState, paramString1, paramString2, paramLong, paramString4, paramString5, this.val$signature);
        }
      });
    }

    public void postPurchaseStateChange2(Constants.PurchaseState paramPurchaseState, String paramString1, String paramString2, long paramLong, String paramString3, String paramString4, String paramString5)
    {
      Log.i("BillingService", "postPurchaseStateChange: " + paramString3 + " at " + paramString1 + " state:" + paramPurchaseState);
      switch (BillingSupportBaseActivity.2.$SwitchMap$com$sgiggle$production$payments$Constants$PurchaseState[paramPurchaseState.ordinal()])
      {
      case 1:
      case 2:
      case 3:
      default:
      case 4:
      }
      while (true)
      {
        BillingSupportBaseActivity.this.setPurchesedProduct(paramString1, paramPurchaseState);
        return;
        BillingServiceManager.getInstance().purchaseStateMap.put(paramString1, paramPurchaseState);
      }
    }

    public void startBuyPageActivity(PendingIntent paramPendingIntent, Intent paramIntent)
    {
      try
      {
        TangoApp.getInstance().skipWelcomePageOnce();
        BillingSupportBaseActivity.this.startIntentSender(paramPendingIntent.getIntentSender(), paramIntent, 0, 0, 0);
        return;
      }
      catch (IntentSender.SendIntentException localSendIntentException)
      {
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.BillingSupportBaseActivity
 * JD-Core Version:    0.6.2
 */