package com.sgiggle.production;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.google.android.c2dm.C2DMBaseReceiver;
import com.sgiggle.util.Log;

public class C2dmReceiver extends C2DMBaseReceiver
{
  public static final String ACTION_TYPE_CALL = "offer-call";
  public static final String ACTION_TYPE_TC_NEW_MESSAGE = "newMessage";
  public static final String ACTION_TYPE_TC_VIDEOMAIL = "vm";
  private static final int MESSAGE_C2DM_ACTION_MESSAGE = 6;
  private static final int MESSAGE_C2DM_CALL = 4;
  private static final int MESSAGE_C2DM_CONVERSATION_MESSAGE = 7;
  private static final int MESSAGE_C2DM_ERROR = 3;
  private static final int MESSAGE_C2DM_MESSAGE = 5;
  private static final int MESSAGE_C2DM_REGISTERED = 1;
  private static final int MESSAGE_C2DM_UNREGISTERED = 2;
  private static final int PUSH_TYPE_ALERT_COUNT = 3;
  private static final int PUSH_TYPE_CALL = 0;
  private static final int PUSH_TYPE_MESSAGE = 1;
  private static final String TAG = "Tango.C2dmReceiver";
  private Handler m_handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        return;
      case 1:
        String str2 = (String)paramAnonymousMessage.obj;
        C2dmReceiver.this.handleRegistrationSuccess(str2);
        return;
      case 2:
        C2dmReceiver.this.handleUnregistration();
        return;
      case 3:
        String str1 = (String)paramAnonymousMessage.obj;
        C2dmReceiver.this.handleRegistrationError(str1);
        return;
      case 4:
        C2dmReceiver.CallPayload localCallPayload = (C2dmReceiver.CallPayload)paramAnonymousMessage.obj;
        C2dmReceiver.this.handleCallPush(localCallPayload.m_peerJid, localCallPayload.m_uniqueId, localCallPayload.m_sessionId, localCallPayload.m_swiftIp, localCallPayload.m_swiftTcpPort, localCallPayload.m_swiftUdpPort);
        return;
      case 5:
        C2dmReceiver.MessagePayload localMessagePayload = (C2dmReceiver.MessagePayload)paramAnonymousMessage.obj;
        C2dmReceiver.this.handleMessagePush(localMessagePayload.m_title, localMessagePayload.m_message);
        return;
      case 6:
        C2dmReceiver.ActionMessagePayload localActionMessagePayload = (C2dmReceiver.ActionMessagePayload)paramAnonymousMessage.obj;
        C2dmReceiver.this.handleActionMessagePush(localActionMessagePayload);
        return;
      case 7:
      }
      C2dmReceiver.ConversationMessagePayload localConversationMessagePayload = (C2dmReceiver.ConversationMessagePayload)paramAnonymousMessage.obj;
      C2dmReceiver.this.handleConversationMessagePush(localConversationMessagePayload);
    }
  };

  public C2dmReceiver()
  {
    super("tangodeveloper@gmail.com");
  }

  private void handleActionMessagePush(ActionMessagePayload paramActionMessagePayload)
  {
    Log.d("Tango.C2dmReceiver", "C2dm Action message received: title=" + paramActionMessagePayload.m_title + ", message=" + paramActionMessagePayload.m_message + " type " + paramActionMessagePayload.m_type);
    if (paramActionMessagePayload.m_type.equals("offer-call"))
    {
      CallMessagePayload localCallMessagePayload = (CallMessagePayload)paramActionMessagePayload;
      TangoApp.getInstance().onC2dmCallMessageReceived(localCallMessagePayload.m_title, localCallMessagePayload.m_message, localCallMessagePayload.m_type, localCallMessagePayload.m_firstName, localCallMessagePayload.m_lastName, localCallMessagePayload.m_accountId);
    }
  }

  private void handleCallPush(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, int paramInt2)
  {
    String str = "C2dm call received: peerJid=" + paramString1 + ", uniqueId=" + paramString2;
    if (paramString3 != null)
      str = str + ", sessionId=" + paramString3;
    if (paramString4 != null)
      str = str + ", swiftIp=" + paramString4 + ", swiftTcpPort=" + paramInt1 + ", swiftUdpPort=" + paramInt2;
    Log.d("Tango.C2dmReceiver", str);
    TangoApp.getInstance().onC2dmCallReceived(paramString1, paramString2, paramString3, paramString4, paramInt1, paramInt2);
  }

  private void handleConversationMessagePush(ConversationMessagePayload paramConversationMessagePayload)
  {
    Log.d("Tango.C2dmReceiver", "C2dm Conversation message received: type=" + paramConversationMessagePayload.m_messageType);
    TangoApp.getInstance().onC2dmConversationMessageReceived(paramConversationMessagePayload.m_messageType, paramConversationMessagePayload.m_messageId, paramConversationMessagePayload.m_peerAccountId, paramConversationMessagePayload.m_peerName, paramConversationMessagePayload.m_messageContent);
  }

  private void handleMessagePush(String paramString1, String paramString2)
  {
    Log.d("Tango.C2dmReceiver", "C2dm message received: title=" + paramString1 + ", message=" + paramString2);
    TangoApp.getInstance().onC2dmMessageReceived(paramString1, paramString2);
  }

  private void handleRegistrationError(String paramString)
  {
    Log.i("Tango.C2dmReceiver", "C2dm registration failed, should try again later:" + paramString);
  }

  private void handleRegistrationSuccess(String paramString)
  {
    Log.i("Tango.C2dmReceiver", "C2dm registration completed: registrationId=" + paramString);
    TangoApp.getInstance().onC2dmRegistrationIdReceived(paramString);
  }

  private void handleUnregistration()
  {
    Log.i("Tango.C2dmReceiver", "C2dm unregistration done, new messages from the authorized sender will be rejected.");
  }

  private String parseNameFromC2dmAlert(String paramString1, String paramString2)
  {
    Object localObject = "";
    if (paramString1.length() == 0)
      return localObject;
    Log.v("Tango.C2dmReceiver", "Retrieve " + paramString1 + " from " + paramString2);
    int i = paramString1.indexOf("-");
    int j = Integer.parseInt(paramString1.substring(0, i));
    int k = Integer.parseInt(paramString1.substring(i + 1));
    try
    {
      String str = paramString2.substring(j, k);
      localObject = str;
      Log.v("Tango.C2dmReceiver", "Name in C2dm Alert " + (String)localObject);
      return localObject;
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
    {
      while (true)
        Log.e("Tango.C2dmReceiver", "Failed in retriving " + paramString1 + " from " + paramString2);
    }
  }

  public void onCreate()
  {
    super.onCreate();
    try
    {
      TangoApp.ensureInitialized();
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      Log.e("Tango.C2dmReceiver", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  public void onError(Context paramContext, String paramString)
  {
    this.m_handler.sendMessage(this.m_handler.obtainMessage(3, paramString));
  }

  // ERROR //
  public void onMessage(Context paramContext, android.content.Intent paramIntent)
  {
    // Byte code:
    //   0: aload_2
    //   1: invokevirtual 287	android/content/Intent:getExtras	()Landroid/os/Bundle;
    //   4: astore_3
    //   5: aload_3
    //   6: ifnonnull +13 -> 19
    //   9: ldc 36
    //   11: ldc_w 289
    //   14: invokestatic 205	com/sgiggle/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   17: pop
    //   18: return
    //   19: aload_3
    //   20: ldc_w 291
    //   23: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   26: astore 4
    //   28: ldc_w 299
    //   31: aload 4
    //   33: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   36: ifeq +92 -> 128
    //   39: iconst_1
    //   40: istore 5
    //   42: ldc 36
    //   44: new 91	java/lang/StringBuilder
    //   47: dup
    //   48: invokespecial 93	java/lang/StringBuilder:<init>	()V
    //   51: ldc_w 301
    //   54: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: iload 5
    //   59: invokevirtual 167	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   62: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   65: invokestatic 124	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   68: pop
    //   69: iload 5
    //   71: tableswitch	default:+29 -> 100, 0:+74->145, 1:+227->298, 2:+29->100, 3:+274->345
    //   101: fload_2
    //   102: new 91	java/lang/StringBuilder
    //   105: dup
    //   106: invokespecial 93	java/lang/StringBuilder:<init>	()V
    //   109: ldc_w 303
    //   112: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: iload 5
    //   117: invokevirtual 167	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   120: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   123: invokestatic 205	com/sgiggle/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   126: pop
    //   127: return
    //   128: ldc_w 305
    //   131: aload 4
    //   133: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   136: ifeq +601 -> 737
    //   139: iconst_3
    //   140: istore 5
    //   142: goto -100 -> 42
    //   145: aload_3
    //   146: ldc_w 307
    //   149: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   152: astore 31
    //   154: aload_3
    //   155: ldc_w 309
    //   158: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   161: astore 32
    //   163: aload_3
    //   164: ldc_w 311
    //   167: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   170: astore 33
    //   172: aload_3
    //   173: ldc_w 313
    //   176: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   179: astore 34
    //   181: aload_3
    //   182: ldc_w 315
    //   185: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   188: invokestatic 244	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   191: istore 41
    //   193: iload 41
    //   195: istore 36
    //   197: aload_3
    //   198: ldc_w 317
    //   201: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   204: invokestatic 244	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   207: istore 43
    //   209: iload 43
    //   211: istore 39
    //   213: iload 36
    //   215: istore 38
    //   217: aload_0
    //   218: getfield 52	com/sgiggle/production/C2dmReceiver:m_handler	Landroid/os/Handler;
    //   221: aload_0
    //   222: getfield 52	com/sgiggle/production/C2dmReceiver:m_handler	Landroid/os/Handler;
    //   225: iconst_4
    //   226: new 319	com/sgiggle/production/C2dmReceiver$CallPayload
    //   229: dup
    //   230: aload_0
    //   231: aload 31
    //   233: aload 32
    //   235: aload 33
    //   237: aload 34
    //   239: iload 38
    //   241: iload 39
    //   243: invokespecial 321	com/sgiggle/production/C2dmReceiver$CallPayload:<init>	(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    //   246: invokevirtual 273	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
    //   249: invokevirtual 277	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   252: pop
    //   253: return
    //   254: astore 35
    //   256: iconst_0
    //   257: istore 36
    //   259: ldc 36
    //   261: new 91	java/lang/StringBuilder
    //   264: dup
    //   265: invokespecial 93	java/lang/StringBuilder:<init>	()V
    //   268: ldc_w 323
    //   271: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   274: aload_3
    //   275: invokevirtual 324	android/os/Bundle:toString	()Ljava/lang/String;
    //   278: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   281: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   284: invokestatic 254	com/sgiggle/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   287: pop
    //   288: iload 36
    //   290: istore 38
    //   292: iconst_0
    //   293: istore 39
    //   295: goto -78 -> 217
    //   298: aload_3
    //   299: ldc_w 326
    //   302: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   305: astore 28
    //   307: aload_3
    //   308: ldc_w 328
    //   311: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   314: astore 29
    //   316: aload_0
    //   317: getfield 52	com/sgiggle/production/C2dmReceiver:m_handler	Landroid/os/Handler;
    //   320: aload_0
    //   321: getfield 52	com/sgiggle/production/C2dmReceiver:m_handler	Landroid/os/Handler;
    //   324: iconst_5
    //   325: new 330	com/sgiggle/production/C2dmReceiver$MessagePayload
    //   328: dup
    //   329: aload_0
    //   330: aload 28
    //   332: aload 29
    //   334: invokespecial 332	com/sgiggle/production/C2dmReceiver$MessagePayload:<init>	(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;)V
    //   337: invokevirtual 273	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
    //   340: invokevirtual 277	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   343: pop
    //   344: return
    //   345: aload_3
    //   346: ldc_w 334
    //   349: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   352: astore 7
    //   354: aload_3
    //   355: ldc_w 326
    //   358: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   361: astore 8
    //   363: aload_3
    //   364: ldc_w 328
    //   367: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   370: astore 9
    //   372: ldc 36
    //   374: new 91	java/lang/StringBuilder
    //   377: dup
    //   378: invokespecial 93	java/lang/StringBuilder:<init>	()V
    //   381: ldc_w 336
    //   384: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   387: aload 7
    //   389: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   392: ldc_w 338
    //   395: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   398: aload 8
    //   400: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   403: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   406: invokestatic 229	com/sgiggle/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   409: pop
    //   410: ldc 8
    //   412: aload 7
    //   414: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   417: ifeq +114 -> 531
    //   420: aload_3
    //   421: ldc_w 340
    //   424: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   427: astore 21
    //   429: ldc 218
    //   431: astore 22
    //   433: aload_3
    //   434: ldc_w 342
    //   437: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   440: astore 23
    //   442: aload 23
    //   444: ldc_w 344
    //   447: invokevirtual 235	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   450: istore 24
    //   452: iload 24
    //   454: iconst_m1
    //   455: if_icmpeq +275 -> 730
    //   458: iload 24
    //   460: iconst_1
    //   461: iadd
    //   462: istore 27
    //   464: aload_0
    //   465: aload 23
    //   467: iconst_0
    //   468: iload 24
    //   470: invokevirtual 239	java/lang/String:substring	(II)Ljava/lang/String;
    //   473: aload 9
    //   475: invokespecial 346	com/sgiggle/production/C2dmReceiver:parseNameFromC2dmAlert	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   478: astore 22
    //   480: aload_0
    //   481: aload 23
    //   483: iload 27
    //   485: invokevirtual 247	java/lang/String:substring	(I)Ljava/lang/String;
    //   488: aload 9
    //   490: invokespecial 346	com/sgiggle/production/C2dmReceiver:parseNameFromC2dmAlert	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   493: astore 25
    //   495: aload_0
    //   496: getfield 52	com/sgiggle/production/C2dmReceiver:m_handler	Landroid/os/Handler;
    //   499: aload_0
    //   500: getfield 52	com/sgiggle/production/C2dmReceiver:m_handler	Landroid/os/Handler;
    //   503: bipush 6
    //   505: new 132	com/sgiggle/production/C2dmReceiver$CallMessagePayload
    //   508: dup
    //   509: aload_0
    //   510: aload 8
    //   512: aload 9
    //   514: aload 22
    //   516: aload 25
    //   518: aload 21
    //   520: invokespecial 349	com/sgiggle/production/C2dmReceiver$CallMessagePayload:<init>	(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   523: invokevirtual 273	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
    //   526: invokevirtual 277	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   529: pop
    //   530: return
    //   531: ldc 11
    //   533: aload 7
    //   535: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   538: ifne +13 -> 551
    //   541: ldc 14
    //   543: aload 7
    //   545: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   548: ifeq +149 -> 697
    //   551: aload_3
    //   552: ldc_w 351
    //   555: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   558: astore 11
    //   560: aload 11
    //   562: ifnonnull +125 -> 687
    //   565: ldc 14
    //   567: aload 7
    //   569: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   572: ifeq +109 -> 681
    //   575: iconst_1
    //   576: istore 18
    //   578: ldc 36
    //   580: new 91	java/lang/StringBuilder
    //   583: dup
    //   584: invokespecial 93	java/lang/StringBuilder:<init>	()V
    //   587: ldc_w 353
    //   590: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   593: iload 18
    //   595: invokevirtual 167	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   598: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   601: invokestatic 356	com/sgiggle/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   604: pop
    //   605: iload 18
    //   607: istore 12
    //   609: aload_3
    //   610: ldc_w 358
    //   613: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   616: astore 13
    //   618: aload_3
    //   619: ldc_w 328
    //   622: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   625: astore 14
    //   627: aload_3
    //   628: ldc_w 360
    //   631: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   634: astore 15
    //   636: aload_3
    //   637: ldc_w 362
    //   640: invokevirtual 297	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   643: astore 16
    //   645: aload_0
    //   646: getfield 52	com/sgiggle/production/C2dmReceiver:m_handler	Landroid/os/Handler;
    //   649: aload_0
    //   650: getfield 52	com/sgiggle/production/C2dmReceiver:m_handler	Landroid/os/Handler;
    //   653: bipush 7
    //   655: new 176	com/sgiggle/production/C2dmReceiver$ConversationMessagePayload
    //   658: dup
    //   659: aload_0
    //   660: iload 12
    //   662: aload 16
    //   664: aload 13
    //   666: aload 15
    //   668: aload 14
    //   670: invokespecial 365	com/sgiggle/production/C2dmReceiver$ConversationMessagePayload:<init>	(Lcom/sgiggle/production/C2dmReceiver;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   673: invokevirtual 273	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
    //   676: invokevirtual 277	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   679: pop
    //   680: return
    //   681: iconst_0
    //   682: istore 18
    //   684: goto -106 -> 578
    //   687: aload 11
    //   689: invokestatic 244	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   692: istore 12
    //   694: goto -85 -> 609
    //   697: ldc 36
    //   699: new 91	java/lang/StringBuilder
    //   702: dup
    //   703: invokespecial 93	java/lang/StringBuilder:<init>	()V
    //   706: ldc_w 367
    //   709: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   712: aload 7
    //   714: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   717: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   720: invokestatic 356	com/sgiggle/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   723: pop
    //   724: return
    //   725: astore 42
    //   727: goto -468 -> 259
    //   730: ldc 218
    //   732: astore 25
    //   734: goto -239 -> 495
    //   737: iconst_0
    //   738: istore 5
    //   740: goto -698 -> 42
    //
    // Exception table:
    //   from	to	target	type
    //   181	193	254	java/lang/Exception
    //   197	209	725	java/lang/Exception
  }

  public void onRegistered(Context paramContext, String paramString)
  {
    this.m_handler.sendMessage(this.m_handler.obtainMessage(1, paramString));
  }

  public void onUnregistered(Context paramContext)
  {
    this.m_handler.sendMessage(this.m_handler.obtainMessage(2));
  }

  private class ActionMessagePayload extends C2dmReceiver.MessagePayload
  {
    public String m_type;

    ActionMessagePayload(String paramString1, String paramString2, String arg4)
    {
      super(paramString1, paramString2);
      Object localObject1;
      if (localObject1 == null);
      for (Object localObject2 = ""; ; localObject2 = localObject1)
      {
        this.m_type = ((String)localObject2);
        return;
      }
    }
  }

  private class CallMessagePayload extends C2dmReceiver.ActionMessagePayload
  {
    public String m_accountId;
    public String m_firstName;
    public String m_lastName;

    CallMessagePayload(String paramString1, String paramString2, String paramString3, String paramString4, String arg6)
    {
      super(paramString1, paramString2, "offer-call");
      String str1;
      String str2;
      label38: Object localObject1;
      if (paramString3 == null)
      {
        str1 = "";
        this.m_firstName = str1;
        if (paramString4 != null)
          break label67;
        str2 = "";
        this.m_lastName = str2;
        if (localObject1 != null)
          break label74;
      }
      label67: label74: for (Object localObject2 = ""; ; localObject2 = localObject1)
      {
        this.m_accountId = ((String)localObject2);
        return;
        str1 = paramString3;
        break;
        str2 = paramString4;
        break label38;
      }
    }
  }

  private class CallPayload
  {
    public String m_peerJid;
    public String m_sessionId;
    public String m_swiftIp;
    public int m_swiftTcpPort;
    public int m_swiftUdpPort;
    public String m_uniqueId;

    CallPayload(String paramString1, String paramString2, String paramString3, String paramInt1, int paramInt2, int arg7)
    {
      this.m_peerJid = paramString1;
      this.m_uniqueId = paramString2;
      this.m_sessionId = paramString3;
      this.m_swiftIp = paramInt1;
      this.m_swiftTcpPort = paramInt2;
      int i;
      this.m_swiftUdpPort = i;
    }
  }

  private class ConversationMessagePayload
  {
    public String m_messageContent;
    public String m_messageId;
    public int m_messageType;
    public String m_peerAccountId;
    public String m_peerName;

    ConversationMessagePayload(int paramString1, String paramString2, String paramString3, String paramString4, String arg6)
    {
      this.m_messageType = paramString1;
      this.m_messageId = paramString2;
      this.m_peerAccountId = paramString3;
      this.m_peerName = paramString4;
      Object localObject;
      this.m_messageContent = localObject;
    }
  }

  private class MessagePayload
  {
    public String m_message;
    public String m_title;

    MessagePayload(String paramString1, String arg3)
    {
      String str;
      Object localObject1;
      if (paramString1 == null)
      {
        str = "";
        this.m_title = str;
        if (localObject1 != null)
          break label44;
      }
      label44: for (Object localObject2 = ""; ; localObject2 = localObject1)
      {
        this.m_message = ((String)localObject2);
        return;
        str = paramString1;
        break;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.C2dmReceiver
 * JD-Core Version:    0.6.2
 */