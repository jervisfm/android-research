package com.sgiggle.production;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Environment;
import com.sgiggle.localstorage.LocalStorage;
import com.sgiggle.media_engine.MediaEngineMessage.AvatarProductCatalogEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplaySettingsEvent;
import com.sgiggle.media_engine.MediaEngineMessage.LoginCompletedEvent;
import com.sgiggle.media_engine.MediaEngineMessage.UpdateTangoAlertsEvent;
import com.sgiggle.media_engine.MediaEngineMessage.VGoodProductCatalogEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.LoginCompletedPayload;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert.AlertSeverity;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert.Builder;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogPayload;
import com.sgiggle.xmpp.SessionMessages.RegisterUserPayload;
import com.sgiggle.xmpp.SessionMessages.UpdateAlertsPayload;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class CTANotifier
{
  private static final int STORAGE_FULL_ALERT = 3;
  private static final String TAG = "Tango.CTANotifier";
  private static CTANotifier s_me;
  private Context m_context;
  private List<SessionMessages.OperationalAlert> m_lastCtaAlertList = null;
  private HashMap<Integer, Long> m_mapRecentMsgs = new HashMap();

  private CTANotifier(Context paramContext)
  {
    this.m_context = paramContext;
  }

  public static List<SessionMessages.OperationalAlert> extractAlertList(Message paramMessage)
  {
    int i = paramMessage.getType();
    Object localObject = null;
    switch (i)
    {
    default:
    case 35011:
    case 35038:
    case 35049:
    case 35223:
    case 35246:
    }
    while (localObject != null)
    {
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = ((List)localObject).iterator();
      while (true)
        if (localIterator.hasNext())
        {
          SessionMessages.OperationalAlert localOperationalAlert = (SessionMessages.OperationalAlert)localIterator.next();
          if (localOperationalAlert.getType() == 3)
            localOperationalAlert = getStorageAlert();
          localArrayList.add(localOperationalAlert);
          continue;
          localObject = ((SessionMessages.LoginCompletedPayload)((MediaEngineMessage.LoginCompletedEvent)paramMessage).payload()).getAlertsList();
          break;
          localObject = ((SessionMessages.UpdateAlertsPayload)((MediaEngineMessage.UpdateTangoAlertsEvent)paramMessage).payload()).getAlertsList();
          break;
          localObject = ((SessionMessages.RegisterUserPayload)((MediaEngineMessage.DisplaySettingsEvent)paramMessage).payload()).getAlertsList();
          break;
          localObject = ((SessionMessages.ProductCatalogPayload)((MediaEngineMessage.VGoodProductCatalogEvent)paramMessage).payload()).getAlertsList();
          break;
          localObject = ((SessionMessages.ProductCatalogPayload)((MediaEngineMessage.AvatarProductCatalogEvent)paramMessage).payload()).getAlertsList();
          break;
        }
      localObject = localArrayList;
    }
    return localObject;
  }

  public static CTANotifier getDefault()
  {
    return s_me;
  }

  private static SessionMessages.OperationalAlert getStorageAlert()
  {
    TangoApp localTangoApp = TangoApp.getInstance();
    SessionMessages.OperationalAlert.Builder localBuilder = SessionMessages.OperationalAlert.newBuilder();
    String str1;
    if ((LocalStorage.isUsingExternalStorage(localTangoApp)) && (!"mounted".equals(Environment.getExternalStorageState())))
      str1 = localTangoApp.getResources().getString(2131296543);
    for (String str2 = localTangoApp.getResources().getString(2131296545); ; str2 = localTangoApp.getResources().getString(2131296544))
    {
      localBuilder.setType(3);
      localBuilder.setTitle(str1);
      localBuilder.setMessage(str2);
      return localBuilder.build();
      str1 = localTangoApp.getResources().getString(2131296543);
    }
  }

  public static void init(Context paramContext)
  {
    s_me = new CTANotifier(paramContext);
  }

  public boolean cancelAlert(int paramInt)
  {
    if (this.m_mapRecentMsgs.remove(Integer.valueOf(paramInt)) != null);
    for (boolean bool = true; ; bool = false)
    {
      TangoApp.getNotificationManager().cancel(getNotificationId(paramInt));
      return bool;
    }
  }

  protected void cancelRegistrationAlerts()
  {
    cancelAlert(SessionMessages.OperationalAlert.AlertSeverity.AS_REGISTRATION.ordinal());
    cancelAlert(SessionMessages.OperationalAlert.AlertSeverity.AS_REGISTRATION_EMAIL.ordinal());
  }

  protected void cancelValidationAlerts()
  {
    cancelAlert(SessionMessages.OperationalAlert.AlertSeverity.AS_VALIDATE_NUMBER.ordinal());
    cancelAlert(SessionMessages.OperationalAlert.AlertSeverity.AS_VALIDATE_EMAIL.ordinal());
    cancelAlert(SessionMessages.OperationalAlert.AlertSeverity.AS_VALIDATE_EMAIL_NUMBER.ordinal());
    cancelAlert(SessionMessages.OperationalAlert.AlertSeverity.AS_VALIDATE_REGISTRATION_EMAIL.ordinal());
  }

  public void cancelValidationAndRegistrationAlerts()
  {
    cancelValidationAlerts();
    cancelRegistrationAlerts();
  }

  public boolean checkAndUpdateMap(SessionMessages.OperationalAlert paramOperationalAlert)
  {
    if (!paramOperationalAlert.hasSeverity())
      return false;
    AppAlert localAppAlert = new AppAlert(paramOperationalAlert);
    Long localLong = (Long)this.m_mapRecentMsgs.get(Integer.valueOf(localAppAlert.CTA().getSeverity().ordinal()));
    if ((localLong == null) || (localAppAlert.isExpiredAfter(localLong.longValue())))
    {
      this.m_mapRecentMsgs.put(Integer.valueOf(localAppAlert.CTA().getSeverity().ordinal()), Long.valueOf(localAppAlert.getTime()));
      return true;
    }
    return false;
  }

  public List<SessionMessages.OperationalAlert> getLastCtaAlertList()
  {
    return this.m_lastCtaAlertList;
  }

  public int getNotificationId(int paramInt)
  {
    return paramInt + 1000;
  }

  public void publishCTA2NotificationBar(Message paramMessage)
  {
    Log.d("Tango.CTANotifier", "publishCTA2NotificationBar(list of CTA)");
    List localList = extractAlertList(paramMessage);
    HashSet localHashSet = new HashSet();
    if (localList != null)
    {
      Iterator localIterator2 = localList.iterator();
      while (localIterator2.hasNext())
      {
        SessionMessages.OperationalAlert localOperationalAlert = (SessionMessages.OperationalAlert)localIterator2.next();
        AppAlert localAppAlert = new AppAlert(localOperationalAlert);
        publishCTA2NotificationBar(localOperationalAlert);
        localHashSet.add(Integer.valueOf(localAppAlert.CTA().getSeverity().ordinal()));
      }
    }
    int i = 1;
    while (true)
      if (i != 0)
      {
        Iterator localIterator1 = this.m_mapRecentMsgs.keySet().iterator();
        Integer localInteger;
        do
        {
          if (!localIterator1.hasNext())
            break;
          localInteger = (Integer)localIterator1.next();
        }
        while ((localHashSet.contains(localInteger)) || (!cancelAlert(localInteger.intValue())));
        i = 1;
      }
      else
      {
        return;
        i = 0;
      }
  }

  public void publishCTA2NotificationBar(SessionMessages.OperationalAlert paramOperationalAlert)
  {
    Log.d("Tango.CTANotifier", "publishCTA2NotificationBar(tangoCTA)");
    if (!checkAndUpdateMap(paramOperationalAlert))
      return;
    Notification localNotification = new Notification(2130837675, paramOperationalAlert.getMessage(), System.currentTimeMillis());
    Intent localIntent = new Intent(this.m_context, TabActivityBase.class);
    localIntent.addFlags(268435456);
    localIntent.setAction("OPEN");
    PendingIntent localPendingIntent = PendingIntent.getActivity(this.m_context, 0, localIntent, 0);
    Log.d("Tango.CTANotifier", "Notification: title='" + paramOperationalAlert.getTitle() + "', message='" + paramOperationalAlert.getMessage() + "'");
    localNotification.setLatestEventInfo(this.m_context, paramOperationalAlert.getTitle(), paramOperationalAlert.getMessage(), localPendingIntent);
    localNotification.flags = (0x8 | localNotification.flags);
    TangoApp.getNotificationManager().notify(getNotificationId(paramOperationalAlert.getSeverity().ordinal()), localNotification);
  }

  public void saveLastCtaAlertList(Message paramMessage)
  {
    Log.d("Tango.CTANotifier", "saveLastCtaAlertList");
    this.m_lastCtaAlertList = extractAlertList(paramMessage);
  }

  public static class AppAlert
  {
    public final int MILLISECS_IN_24HRS = 86400000;
    private SessionMessages.OperationalAlert m_alertBuffer;
    private long m_time_ms = System.currentTimeMillis();

    public AppAlert(SessionMessages.OperationalAlert paramOperationalAlert)
    {
      this.m_alertBuffer = paramOperationalAlert;
    }

    public SessionMessages.OperationalAlert CTA()
    {
      return this.m_alertBuffer;
    }

    public long getTime()
    {
      return this.m_time_ms;
    }

    public boolean isEqual(AppAlert paramAppAlert)
    {
      return isEqual(paramAppAlert.CTA());
    }

    public boolean isEqual(SessionMessages.OperationalAlert paramOperationalAlert)
    {
      return (CTA().getSeverity() == paramOperationalAlert.getSeverity()) && (CTA().getTitle().equals(paramOperationalAlert.getTitle())) && (CTA().getMessage().equals(paramOperationalAlert.getMessage()));
    }

    public boolean isExpired()
    {
      return System.currentTimeMillis() - this.m_time_ms > 86400000L;
    }

    public boolean isExpiredAfter(long paramLong)
    {
      return this.m_time_ms - paramLong > 86400000L;
    }

    public boolean isRegistrationRequired()
    {
      SessionMessages.OperationalAlert.AlertSeverity localAlertSeverity = this.m_alertBuffer.getSeverity();
      return (localAlertSeverity == SessionMessages.OperationalAlert.AlertSeverity.AS_REGISTRATION) || (localAlertSeverity == SessionMessages.OperationalAlert.AlertSeverity.AS_REGISTRATION_EMAIL);
    }

    public boolean isUpgradeRequired()
    {
      return this.m_alertBuffer.getSeverity() == SessionMessages.OperationalAlert.AlertSeverity.AS_UPGRADE;
    }

    public boolean isValidationRequired()
    {
      SessionMessages.OperationalAlert.AlertSeverity localAlertSeverity = this.m_alertBuffer.getSeverity();
      return (localAlertSeverity == SessionMessages.OperationalAlert.AlertSeverity.AS_VALIDATE_NUMBER) || (localAlertSeverity == SessionMessages.OperationalAlert.AlertSeverity.AS_VALIDATE_EMAIL) || (localAlertSeverity == SessionMessages.OperationalAlert.AlertSeverity.AS_VALIDATE_EMAIL_NUMBER) || (localAlertSeverity == SessionMessages.OperationalAlert.AlertSeverity.AS_VALIDATE_REGISTRATION_EMAIL);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.CTANotifier
 * JD-Core Version:    0.6.2
 */