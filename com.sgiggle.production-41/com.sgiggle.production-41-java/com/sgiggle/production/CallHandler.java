package com.sgiggle.production;

import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.AcceptCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.Audio2WayAvatarInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AudioAvatarInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AudioControlMessage;
import com.sgiggle.media_engine.MediaEngineMessage.AudioInInitializationEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AudioInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AudioModeChangedEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AudioVideo2WayInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AudioVideoInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.CallReceivedEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InCallAlertEvent;
import com.sgiggle.media_engine.MediaEngineMessage.MakeCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RejectCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SendCallInvitationEvent;
import com.sgiggle.media_engine.MediaEngineMessage.TerminateCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.VideoAvatarInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.VideoModeChangedEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.pjmedia.AudioModeWrapper;
import com.sgiggle.production.receiver.VoIPStateListener;
import com.sgiggle.production.receiver.VoIPStateListener.Listener;
import com.sgiggle.production.vendor.htc.IntegrationConstants;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.AudioModePayload;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.VideoModePayload;

public class CallHandler
  implements VoIPStateListener.Listener
{
  private static final String TAG = "Tango.CallHandler";
  private static CallHandler s_me;
  private TangoApp m_application;
  private CallSession m_callSession;
  private int m_callState = 0;
  private PhoneStateListener m_phoneStateListener = new PhoneStateListener()
  {
    public void onCallStateChanged(int paramAnonymousInt, String paramAnonymousString)
    {
      if (CallHandler.this.m_callSession == null)
        Log.i("Tango.CallHandler", "onCallStateChanged(): Not in a Tango call. Do nothing.");
      label230: 
      do
      {
        do
        {
          do
          {
            return;
            if (paramAnonymousInt != 1)
              break;
          }
          while (GameInCallActivity.getRunningInstance() == null);
          CallHandler.this.m_application.setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
          return;
          if (paramAnonymousInt != 2)
            break label230;
          if (!IntegrationConstants.SUPPORT_CALL_WAITING)
            break;
        }
        while (CallHandler.this.m_callSession.m_callState != CallSession.CallState.CALL_STATE_ACTIVE);
        Log.i("Tango.CallHandler", "onCallStateChanged(): Put Tango call on hold...");
        CallHandler.this.m_callSession.m_callOnHold = true;
        MessageRouter.getInstance().postMessage("jingle", MediaEngineMessage.AudioControlMessage.createMessage(2, true));
        AudioModeWrapper.pstnSessionOffHook();
        CallHandler.this.m_application.setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
        Log.d("Tango.CallHandler", "Call-Waiting in effect. Old-muted = " + CallHandler.this.m_callSession.m_muted);
        return;
        String str = CallHandler.this.m_callSession.getPeerAccountId();
        Log.i("Tango.CallHandler", "On new telephone call: Terminate active Tango call with : " + str);
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.TerminateCallMessage(str));
        CallHandler.this.endCallSession();
        CallHandler.this.m_application.setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
        return;
      }
      while ((!CallHandler.this.m_callSession.m_callOnHold) || (paramAnonymousInt != 0) || (!IntegrationConstants.SUPPORT_CALL_WAITING));
      Log.d("Tango.CallHandler", "onCallStateChanged(): Is mute: " + CallHandler.this.m_callSession.m_muted);
      AudioModeWrapper.pstnSessionIdle();
      MessageRouter.getInstance().postMessage("jingle", MediaEngineMessage.AudioControlMessage.createMessage(2, CallHandler.this.m_callSession.m_muted));
      Log.d("Tango.CallHandler", "onCallStateChanged(): Is mute: " + CallHandler.this.m_callSession.m_muted);
      CallHandler.this.m_callSession.m_callOnHold = false;
    }
  };
  private TelephonyManager m_telephonyMgr;

  public CallHandler(TangoApp paramTangoApp)
  {
    this.m_application = paramTangoApp;
    this.m_telephonyMgr = ((TelephonyManager)this.m_application.getSystemService("phone"));
    if (IntegrationConstants.SUPPORT_CALL_WAITING)
    {
      VoIPStateListener localVoIPStateListener = new VoIPStateListener(this);
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addAction("android.intent.action.VoIP_RESUME_CALL");
      paramTangoApp.registerReceiver(localVoIPStateListener, localIntentFilter);
    }
  }

  private void broadcastCallState(int paramInt)
  {
    Log.d("Tango.CallHandler", "broadcastCallState(): new State = " + paramInt);
    if (this.m_callState == paramInt)
      Log.i("Tango.CallHandler", "broadcastCallState(): new State = current = " + paramInt + ". Do nothing.");
    do
    {
      return;
      this.m_callState = paramInt;
    }
    while (!IntegrationConstants.SUPPORT_CALL_WAITING);
    Intent localIntent = new Intent("android.intent.action.PS_CALL_STATE_CHANGED");
    localIntent.putExtra("state", this.m_callState);
    this.m_application.sendBroadcast(localIntent);
  }

  public static CallHandler getDefault()
  {
    return s_me;
  }

  static void init(TangoApp paramTangoApp)
  {
    s_me = new CallHandler(paramTangoApp);
  }

  public void answerIncomingCall()
  {
    if (this.m_callSession == null)
    {
      Log.w("Tango.CallHandler", "answerIncomingCall(): No current call session.");
      return;
    }
    broadcastCallState(2);
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.AcceptCallMessage(this.m_callSession.getPeerAccountId()));
  }

  public void endCallSession()
  {
    if (this.m_callSession != null)
    {
      Log.d("Tango.CallHandler", "endCallSession(): ... with peerAccountId=" + this.m_callSession.getPeerAccountId() + " m_callerInitVideoCall=" + this.m_callSession.m_callerInitVideoCall);
      broadcastCallState(0);
      if (this.m_telephonyMgr != null)
        this.m_telephonyMgr.listen(this.m_phoneStateListener, 0);
      this.m_callSession = null;
    }
  }

  public CallSession getCallSession()
  {
    Log.d("Tango.CallHandler", "getCallSession(): updating call session m_callSession=" + this.m_callSession);
    return this.m_callSession;
  }

  public void handleCSCallResume()
  {
    if (this.m_callSession != null)
    {
      Log.d("Tango.CallHandler", "CS Call resuming");
      String str = this.m_callSession.getPeerAccountId();
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.TerminateCallMessage(str));
      endCallSession();
      this.m_application.setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
    }
  }

  void rejectIncomingCall()
  {
    if (this.m_callSession == null)
    {
      Log.w("Tango.CallHandler", "rejectIncomingCall(): No current call session.");
      return;
    }
    broadcastCallState(0);
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.RejectCallMessage(this.m_callSession.getPeerAccountId()));
  }

  public void sendCallMessage(String paramString1, String paramString2, long paramLong)
  {
    sendCallMessage(paramString1, paramString2, paramLong, VideoMode.VIDEO_OFF);
  }

  public void sendCallMessage(String paramString1, String paramString2, long paramLong, VideoMode paramVideoMode)
  {
    if (((this.m_telephonyMgr != null) && (!IntegrationConstants.SUPPORT_CALL_WAITING) && (this.m_telephonyMgr.getCallState() != 0)) || ((paramVideoMode == VideoMode.VIDEO_ON) && (this.m_application.hasVideoActivity())))
    {
      String str = this.m_application.getString(2131296536);
      Toast.makeText(this.m_application, str, 0).show();
      return;
    }
    Log.d("Tango.CallHandler", "sendCallMessage(): peerName = " + paramString2 + " m_callerInitVideoCall = " + paramVideoMode);
    MessageRouter localMessageRouter = MessageRouter.getInstance();
    if (paramVideoMode == VideoMode.VIDEO_ON);
    for (boolean bool = true; ; bool = false)
    {
      localMessageRouter.postMessage("jingle", new MediaEngineMessage.MakeCallMessage(paramString1, paramString2, bool));
      return;
    }
  }

  CallSession startCallSession(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    case 35016:
    default:
    case 35017:
      while (true)
      {
        broadcastCallState(1);
        if (this.m_telephonyMgr != null)
          this.m_telephonyMgr.listen(this.m_phoneStateListener, 32);
        return this.m_callSession;
        if (this.m_callSession == null)
        {
          MediaEngineMessage.CallReceivedEvent localCallReceivedEvent = (MediaEngineMessage.CallReceivedEvent)paramMessage;
          Log.e("Tango.CallHandler", "startCallSession - CALL_RECEIVED_EVENT, peerName:" + ((SessionMessages.MediaSessionPayload)localCallReceivedEvent.payload()).getDisplayname() + " | peerAccountId:" + ((SessionMessages.MediaSessionPayload)localCallReceivedEvent.payload()).getAccountId());
          this.m_callSession = new CallSession((SessionMessages.MediaSessionPayload)localCallReceivedEvent.payload(), CallSession.CallState.CALL_STATE_INCOMING);
        }
      }
    case 35015:
    }
    MediaEngineMessage.SendCallInvitationEvent localSendCallInvitationEvent = (MediaEngineMessage.SendCallInvitationEvent)paramMessage;
    Log.d("Tango.CallHandler", "startCallSession - SEND_CALL_INVITATION_EVENT, peerName:" + ((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).getDisplayname() + " | peerAccountId:" + ((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).getAccountId());
    if (this.m_callSession != null)
      throw new RuntimeException("should be wrong, CallSession should be created by SEND_CALL_INVITATION_EVENT on the caller side");
    this.m_callSession = new CallSession((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload(), CallSession.CallState.CALL_STATE_DIALING);
    if ((((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).hasVideoMode()) && (((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).getVideoMode()));
    for (boolean bool = true; ; bool = false)
    {
      this.m_callSession.m_callerInitVideoCall = bool;
      if (!bool)
        break;
      this.m_callSession.m_cameraPosition = ((SessionMessages.MediaSessionPayload)localSendCallInvitationEvent.payload()).getCameraPosition();
      this.m_callSession.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.SEND;
      break;
    }
  }

  void updateCallSession(Message paramMessage)
  {
    if (this.m_callSession == null)
      Log.w("Tango.CallHandler", "updateCallSession(): No current call session. Ignore: " + paramMessage);
    MediaEngineMessage.AudioModeChangedEvent localAudioModeChangedEvent;
    do
    {
      MediaEngineMessage.AudioInInitializationEvent localAudioInInitializationEvent;
      do
      {
        return;
        switch (paramMessage.getType())
        {
        case 35033:
        default:
          return;
        case 35021:
          this.m_callSession.m_callState = CallSession.CallState.CALL_STATE_CONNECTING;
          return;
        case 35071:
          this.m_callSession.m_callStartTime = System.currentTimeMillis();
          localAudioInInitializationEvent = (MediaEngineMessage.AudioInInitializationEvent)paramMessage;
        case 35023:
        case 35083:
        case 35025:
        case 35069:
        case 35077:
        case 35079:
        case 35080:
        case 35089:
        case 35243:
        case 35244:
        case 35242:
        }
      }
      while (!((SessionMessages.MediaSessionPayload)localAudioInInitializationEvent.payload()).hasVideoMode());
      this.m_callSession.m_callerInitVideoCall = ((SessionMessages.MediaSessionPayload)localAudioInInitializationEvent.payload()).getVideoMode();
      Log.d("Tango.CallHandler", "m_callerInitVideoCall: " + this.m_callSession.m_callerInitVideoCall);
      return;
      this.m_callSession.m_callState = CallSession.CallState.CALL_STATE_ACTIVE;
      broadcastCallState(2);
      MediaEngineMessage.AudioInProgressEvent localAudioInProgressEvent = (MediaEngineMessage.AudioInProgressEvent)paramMessage;
      if (((SessionMessages.MediaSessionPayload)localAudioInProgressEvent.payload()).hasSpeakerOn())
      {
        this.m_callSession.m_speakerOn = ((SessionMessages.MediaSessionPayload)localAudioInProgressEvent.payload()).getSpeakerOn();
        Log.d("Tango.CallHandler", "AudioMode: AUDIO_IN_PROGRESS_EVENT: speakerOn=" + this.m_callSession.m_speakerOn);
      }
      if (((SessionMessages.MediaSessionPayload)localAudioInProgressEvent.payload()).hasMuted())
      {
        this.m_callSession.m_muted = ((SessionMessages.MediaSessionPayload)localAudioInProgressEvent.payload()).getMuted();
        Log.d("Tango.CallHandler", "Call state: " + this.m_callSession.m_callState);
        Log.d("Tango.CallHandler", "AudioMode: AUDIO_IN_PROGRESS_EVENT: muted=" + this.m_callSession.m_muted);
      }
      if (((SessionMessages.MediaSessionPayload)localAudioInProgressEvent.payload()).hasVideoMode())
      {
        this.m_callSession.m_callerInitVideoCall = ((SessionMessages.MediaSessionPayload)localAudioInProgressEvent.payload()).getVideoMode();
        Log.d("Tango.CallHandler", "m_callerInitVideoCall: " + this.m_callSession.m_callerInitVideoCall);
      }
      this.m_callSession.m_avatarDirection = SessionMessages.AvatarControlPayload.Direction.NONE;
      return;
      localAudioModeChangedEvent = (MediaEngineMessage.AudioModeChangedEvent)paramMessage;
      if (((SessionMessages.AudioModePayload)localAudioModeChangedEvent.payload()).hasSpeakeron())
      {
        this.m_callSession.m_speakerOn = ((SessionMessages.AudioModePayload)localAudioModeChangedEvent.payload()).getSpeakeron();
        Log.d("Tango.CallHandler", "AudioMode: AUDIO_MODE_CHANGED_EVENT: speakerOn=" + this.m_callSession.m_speakerOn);
      }
    }
    while (!((SessionMessages.AudioModePayload)localAudioModeChangedEvent.payload()).hasMuted());
    this.m_callSession.m_muted = ((SessionMessages.AudioModePayload)localAudioModeChangedEvent.payload()).getMuted();
    Log.d("Tango.CallHandler", "Call state: " + this.m_callSession.m_callState);
    Log.d("Tango.CallHandler", "AudioMode: AUDIO_MODE_CHANGED_EVENT: muted=" + this.m_callSession.m_muted);
    return;
    MediaEngineMessage.AudioVideoInProgressEvent localAudioVideoInProgressEvent = (MediaEngineMessage.AudioVideoInProgressEvent)paramMessage;
    this.m_callSession.m_videoDirection = ((SessionMessages.MediaSessionPayload)localAudioVideoInProgressEvent.payload()).getDirection();
    this.m_callSession.m_cameraPosition = ((SessionMessages.MediaSessionPayload)localAudioVideoInProgressEvent.payload()).getCameraPosition();
    this.m_callSession.m_avatarDirection = SessionMessages.AvatarControlPayload.Direction.NONE;
    this.m_callSession.updateVgoodSelectorData((SessionMessages.MediaSessionPayload)localAudioVideoInProgressEvent.payload());
    return;
    MediaEngineMessage.AudioVideo2WayInProgressEvent localAudioVideo2WayInProgressEvent = (MediaEngineMessage.AudioVideo2WayInProgressEvent)paramMessage;
    this.m_callSession.m_videoDirection = ((SessionMessages.MediaSessionPayload)localAudioVideo2WayInProgressEvent.payload()).getDirection();
    this.m_callSession.m_cameraPosition = ((SessionMessages.MediaSessionPayload)localAudioVideo2WayInProgressEvent.payload()).getCameraPosition();
    this.m_callSession.m_avatarDirection = SessionMessages.AvatarControlPayload.Direction.NONE;
    this.m_callSession.updateVgoodSelectorData((SessionMessages.MediaSessionPayload)localAudioVideo2WayInProgressEvent.payload());
    return;
    this.m_callSession.m_showLowBandwidth = true;
    return;
    this.m_callSession.m_showLowBandwidth = false;
    return;
    MediaEngineMessage.InCallAlertEvent localInCallAlertEvent = (MediaEngineMessage.InCallAlertEvent)paramMessage;
    this.m_callSession.setInCallAlertEvent(localInCallAlertEvent);
    return;
    MediaEngineMessage.VideoModeChangedEvent localVideoModeChangedEvent = (MediaEngineMessage.VideoModeChangedEvent)paramMessage;
    this.m_callSession.m_cameraPosition = ((SessionMessages.VideoModePayload)localVideoModeChangedEvent.payload()).getCameraPosition();
    return;
    MediaEngineMessage.VideoAvatarInProgressEvent localVideoAvatarInProgressEvent = (MediaEngineMessage.VideoAvatarInProgressEvent)paramMessage;
    if (((SessionMessages.AvatarControlPayload)localVideoAvatarInProgressEvent.payload()).getDirection() == SessionMessages.AvatarControlPayload.Direction.SENDER)
      this.m_callSession.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.SEND;
    while (true)
    {
      this.m_callSession.m_avatarDirection = ((SessionMessages.AvatarControlPayload)localVideoAvatarInProgressEvent.payload()).getDirection();
      return;
      if (((SessionMessages.AvatarControlPayload)localVideoAvatarInProgressEvent.payload()).getDirection() == SessionMessages.AvatarControlPayload.Direction.RECEIVER)
        this.m_callSession.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.RECEIVE;
    }
    MediaEngineMessage.Audio2WayAvatarInProgressEvent localAudio2WayAvatarInProgressEvent = (MediaEngineMessage.Audio2WayAvatarInProgressEvent)paramMessage;
    this.m_callSession.m_avatarDirection = ((SessionMessages.AvatarControlPayload)localAudio2WayAvatarInProgressEvent.payload()).getDirection();
    this.m_callSession.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.NONE;
    return;
    MediaEngineMessage.AudioAvatarInProgressEvent localAudioAvatarInProgressEvent = (MediaEngineMessage.AudioAvatarInProgressEvent)paramMessage;
    this.m_callSession.m_avatarDirection = ((SessionMessages.AvatarControlPayload)localAudioAvatarInProgressEvent.payload()).getDirection();
    this.m_callSession.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.NONE;
  }

  public static enum VideoMode
  {
    static
    {
      VIDEO_OFF = new VideoMode("VIDEO_OFF", 1);
      VideoMode[] arrayOfVideoMode = new VideoMode[2];
      arrayOfVideoMode[0] = VIDEO_ON;
      arrayOfVideoMode[1] = VIDEO_OFF;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.CallHandler
 * JD-Core Version:    0.6.2
 */