package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.DeleteCallLogMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayCallLogEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayContactDetailEvent;
import com.sgiggle.media_engine.MediaEngineMessage.UpdateCallLogEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ViewContactDetailMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.CallEntriesPayload;
import com.sgiggle.xmpp.SessionMessages.CallEntriesPayload.ResultType;
import com.sgiggle.xmpp.SessionMessages.CallEntry;
import com.sgiggle.xmpp.SessionMessages.CallEntry.Builder;
import com.sgiggle.xmpp.SessionMessages.CallEntry.CallType;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

public class CallLogActivity extends ActivityBase
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, RadioGroup.OnCheckedChangeListener
{
  private static final int DELETE_ALL_ID = 4;
  private static final int DELETE_ID = 7;
  private static final String PREF_VIEW_INDEX = "view_index";
  private static final String PREF_VIEW_TOP = "view_top";
  private static final int SHOW_CALL_LOG_FIRST_TIME = 1;
  private static final int SHOW_CALL_LOG_FIRST_TIME_DELAY = 15000;
  private static final String TAG = "Tango.CallLogUI";
  private static final boolean VDBG = true;
  private static List<CallLogEntry> m_logEntries = new ArrayList();
  private static boolean m_receivedEntriesFromServer = false;
  private CallLogAdapter m_adapter;
  private String m_dateFormat = null;
  private List<CallLogEntry> m_displayedEntries = m_logEntries;
  private TextView m_emptyView;
  private Handler m_handler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      if (CallLogActivity.this.m_isDestroyed)
      {
        Log.d("Tango.CallLogUI", "Handler: ignoring message " + paramAnonymousMessage + "; we're destroyed!");
        return;
      }
      switch (paramAnonymousMessage.what)
      {
      default:
        return;
      case 1:
      }
      CallLogActivity.this.doUpdateLogEntries();
    }
  };
  private boolean m_isDestroyed = false;
  private ListViewIgnoreBackKey m_listView;
  private View m_listWrapper;
  private CallLogEntry m_longClickedEntry = null;
  private SharedPreferences m_prefs;
  private ViewGroup m_progressView;
  private ViewBySelection m_viewBy = ViewBySelection.VIEW_BY_ALL;

  private void deleteAllCallLog()
  {
    MediaEngineMessage.DeleteCallLogMessage localDeleteCallLogMessage = new MediaEngineMessage.DeleteCallLogMessage();
    MessageRouter.getInstance().postMessage("jingle", localDeleteCallLogMessage);
  }

  private void deleteCallLog(CallLogEntry paramCallLogEntry)
  {
    MediaEngineMessage.DeleteCallLogMessage localDeleteCallLogMessage = new MediaEngineMessage.DeleteCallLogMessage(SessionMessages.CallEntry.newBuilder().setAccountId(paramCallLogEntry.m_accountId).setStartTime(paramCallLogEntry.m_when).build());
    MessageRouter.getInstance().postMessage("jingle", localDeleteCallLogMessage);
  }

  private void displayLogEntries(List<CallLogEntry> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      CallLogEntry localCallLogEntry = (CallLogEntry)localIterator.next();
      if ((this.m_viewBy != ViewBySelection.VIEW_BY_MISSED) || (localCallLogEntry.m_type == EntryType.ENTRY_TYPE_MISSED_CALL))
        localArrayList.add(localCallLogEntry);
    }
    Log.d("Tango.CallLogUI", "displayLogEntries(): Filtered " + localArrayList.size() + " / " + paramList.size());
    this.m_adapter = new CallLogAdapter(this, 2130903051, localArrayList);
    this.m_listView.setAdapter(this.m_adapter);
  }

  private void doUpdateLogEntries()
  {
    if (this.m_progressView.getVisibility() == 0)
    {
      this.m_listWrapper.setVisibility(0);
      this.m_listView.setEmptyView(this.m_emptyView);
      this.m_progressView.setVisibility(8);
    }
    this.m_displayedEntries = m_logEntries;
    displayLogEntries(this.m_displayedEntries);
    restoreScrollPositionInList();
  }

  private String getDateFormatString()
  {
    char[] arrayOfChar = DateFormat.getDateFormatOrder(getBaseContext());
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    if (i < arrayOfChar.length)
    {
      switch (arrayOfChar[i])
      {
      default:
      case 'd':
      case 'M':
      case 'y':
      }
      while (true)
      {
        i++;
        break;
        localStringBuilder.append("dd/");
        continue;
        localStringBuilder.append("MM/");
        continue;
        localStringBuilder.append("yy/");
      }
    }
    return localStringBuilder.substring(0, localStringBuilder.length() - 1);
  }

  private void onDeleteAllMenuSelected()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getParent());
    localBuilder.setTitle(2131296386);
    localBuilder.setMessage(2131296387);
    localBuilder.setCancelable(true);
    localBuilder.setPositiveButton(2131296287, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        CallLogActivity.this.deleteAllCallLog();
      }
    });
    localBuilder.setNegativeButton(2131296288, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
      }
    });
    AlertDialog localAlertDialog = localBuilder.create();
    localAlertDialog.setIcon(2130837650);
    localAlertDialog.show();
  }

  private void openContactDetailPage(MediaEngineMessage.DisplayContactDetailEvent paramDisplayContactDetailEvent)
  {
    ((ActivityStack)getParent()).pushWithMessage(ContactDetailActivity.class, paramDisplayContactDetailEvent);
  }

  private void restoreScrollPositionInList()
  {
    int i = this.m_prefs.getInt("view_index", -1);
    int j = this.m_prefs.getInt("view_top", 0);
    if (i != -1)
    {
      this.m_listView.setSelectionFromTop(i, j);
      Log.v("Tango.CallLogUI", "restoreScrollPositionInList: index=" + i + ", top=" + j);
    }
  }

  private void saveScrollPositionInList()
  {
    int i = this.m_listView.getFirstVisiblePosition();
    View localView = this.m_listView.getChildAt(0);
    if (localView == null);
    for (int j = 0; ; j = localView.getTop())
    {
      SharedPreferences.Editor localEditor = this.m_prefs.edit();
      localEditor.putInt("view_index", i);
      localEditor.putInt("view_top", j);
      localEditor.commit();
      Log.v("Tango.CallLogUI", "saveScrollPositionInList: index=" + i + ", top=" + j);
      return;
    }
  }

  private void showContactDetailPage(CallLogEntry paramCallLogEntry)
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ViewContactDetailMessage(paramCallLogEntry.convertToMessageContact()));
  }

  static void storeLogEntries(com.sgiggle.messaging.Message paramMessage, Context paramContext)
  {
    List localList;
    SessionMessages.CallEntriesPayload.ResultType localResultType;
    ArrayList localArrayList;
    label151: SessionMessages.CallEntry localCallEntry;
    EntryType localEntryType;
    label189: CallLogEntry localCallLogEntry;
    switch (paramMessage.getType())
    {
    default:
      Log.w("Tango.CallLogUI", "storeLogEntries(): Unsupported message = [" + paramMessage + "]");
      return;
    case 35092:
      MediaEngineMessage.DisplayCallLogEvent localDisplayCallLogEvent = (MediaEngineMessage.DisplayCallLogEvent)paramMessage;
      localList = ((SessionMessages.CallEntriesPayload)localDisplayCallLogEvent.payload()).getEntriesList();
      localResultType = ((SessionMessages.CallEntriesPayload)localDisplayCallLogEvent.payload()).getResultType();
      Log.d("Tango.CallLogUI", "storeLogEntries(): # of entries = " + localList.size() + ", result-type = " + localResultType);
      localArrayList = new ArrayList();
      Iterator localIterator = localList.iterator();
      if (!localIterator.hasNext())
        break label396;
      localCallEntry = (SessionMessages.CallEntry)localIterator.next();
      if (localCallEntry.getCallType() == SessionMessages.CallEntry.CallType.INBOUND_MISSED)
      {
        localEntryType = EntryType.ENTRY_TYPE_MISSED_CALL;
        localCallLogEntry = new CallLogEntry(localCallEntry.getNamePrefix(), localCallEntry.getFirstName(), localCallEntry.getMiddleName(), localCallEntry.getLastName(), localCallEntry.getNameSuffix(), localCallEntry.getDisplayName(), localEntryType, localCallEntry.getAccountId());
        if ((localCallEntry.getCallType() != SessionMessages.CallEntry.CallType.INBOUND_CONNECTED) && (localCallEntry.getCallType() != SessionMessages.CallEntry.CallType.INBOUND_MISSED))
          break label387;
      }
      break;
    case 35093:
    }
    label387: for (localCallLogEntry.m_direction = 0; ; localCallLogEntry.m_direction = 1)
    {
      localCallLogEntry.m_email = localCallEntry.getEmail();
      localCallLogEntry.m_phoneNumber = localCallEntry.getPhoneNumber();
      localCallLogEntry.m_when = localCallEntry.getStartTime();
      localCallLogEntry.m_duration = localCallEntry.getDuration();
      localCallLogEntry.m_deviceContactId = localCallEntry.getDeviceContactId();
      localArrayList.add(localCallLogEntry);
      break label151;
      MediaEngineMessage.UpdateCallLogEvent localUpdateCallLogEvent = (MediaEngineMessage.UpdateCallLogEvent)paramMessage;
      localList = ((SessionMessages.CallEntriesPayload)localUpdateCallLogEvent.payload()).getEntriesList();
      localResultType = ((SessionMessages.CallEntriesPayload)localUpdateCallLogEvent.payload()).getResultType();
      if (localResultType == SessionMessages.CallEntriesPayload.ResultType.SERVER);
      for (boolean bool = true; ; bool = false)
      {
        m_receivedEntriesFromServer = bool;
        break;
      }
      localEntryType = EntryType.ENTRY_TYPE_NORMAL_CALL;
      break label189;
    }
    label396: m_logEntries = localArrayList;
  }

  private void videoCall(CallLogEntry paramCallLogEntry)
  {
    CallHandler.getDefault().sendCallMessage(paramCallLogEntry.m_accountId, paramCallLogEntry.displayName(), paramCallLogEntry.m_deviceContactId, CallHandler.VideoMode.VIDEO_ON);
  }

  protected void handleNewMessage(com.sgiggle.messaging.Message paramMessage)
  {
    Log.d("Tango.CallLogUI", "handleNewMessage(): Message = " + paramMessage);
    if (paramMessage == null)
      return;
    switch (paramMessage.getType())
    {
    default:
      Log.w("Tango.CallLogUI", "handleNewMessage(): Unsupported message=" + paramMessage);
      return;
    case 35216:
    }
    openContactDetailPage((MediaEngineMessage.DisplayContactDetailEvent)paramMessage);
  }

  public void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt)
  {
    switch (paramInt)
    {
    default:
    case 2131361852:
    case 2131361853:
    }
    while (true)
    {
      displayLogEntries(this.m_displayedEntries);
      return;
      this.m_viewBy = ViewBySelection.VIEW_BY_ALL;
      continue;
      this.m_viewBy = ViewBySelection.VIEW_BY_MISSED;
    }
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    Log.d("Tango.CallLogUI", "onContextItemSelected()");
    boolean bool = true;
    switch (paramMenuItem.getItemId())
    {
    default:
      bool = super.onContextItemSelected(paramMenuItem);
    case 7:
    }
    while (true)
    {
      this.m_longClickedEntry = null;
      return bool;
      deleteCallLog(this.m_longClickedEntry);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.CallLogUI", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903052);
    this.m_prefs = getSharedPreferences("Tango.CallLogUI", 0);
    this.m_progressView = ((ViewGroup)findViewById(2131361988));
    this.m_listView = ((ListViewIgnoreBackKey)findViewById(2131361854));
    this.m_listWrapper = findViewById(2131361850);
    this.m_emptyView = ((TextView)findViewById(2131361855));
    this.m_listView.setOnItemClickListener(this);
    this.m_listView.setOnItemLongClickListener(this);
    this.m_listView.setItemsCanFocus(false);
    this.m_listView.setClickable(true);
    registerForContextMenu(this.m_listView);
    RadioGroup localRadioGroup = (RadioGroup)findViewById(2131361851);
    localRadioGroup.setOnCheckedChangeListener(this);
    localRadioGroup.check(2131361852);
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    Log.d("Tango.CallLogUI", "onCreateContextMenu()");
    if (this.m_longClickedEntry != null)
    {
      super.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
      paramContextMenu.setHeaderTitle(this.m_longClickedEntry.displayName());
      paramContextMenu.add(0, 7, 0, 2131296388);
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    paramMenu.add(0, 4, 0, 2131296386).setIcon(2130837666);
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    Log.d("Tango.CallLogUI", "onDestroy()");
    unregisterForContextMenu(this.m_listView);
    super.onDestroy();
    this.m_isDestroyed = true;
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    CallLogEntry localCallLogEntry = (CallLogEntry)((ListAdapter)paramAdapterView.getAdapter()).getItem(paramInt);
    Log.d("Tango.CallLogUI", "onItemClick(): Call to [" + localCallLogEntry.displayName() + "] (" + localCallLogEntry.m_accountId + ")");
    showContactDetailPage(localCallLogEntry);
  }

  public boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Log.d("Tango.CallLogUI", "onItemLongClick()");
    this.m_longClickedEntry = ((CallLogEntry)((ListAdapter)paramAdapterView.getAdapter()).getItem(paramInt));
    return false;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      Log.i("Tango.CallLogUI", "ignore back key down");
      return false;
    }
    if (paramInt == 82)
    {
      Log.i("Tango.CallLogUI", "open options menu");
      openOptionsMenu();
      return true;
    }
    Log.i("Tango.CallLogUI", "key down " + paramInt + " handled by super class");
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      Log.i("Tango.CallLogUI", "ignore back key up");
      return false;
    }
    if (paramInt == 82)
      return true;
    Log.i("Tango.CallLogUI", "key up " + paramInt + " handled by super class");
    return super.onKeyUp(paramInt, paramKeyEvent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 4:
    }
    onDeleteAllMenuSelected();
    return true;
  }

  protected void onPause()
  {
    Log.v("Tango.CallLogUI", "onPause()");
    super.onPause();
    saveScrollPositionInList();
  }

  protected void onResume()
  {
    Log.v("Tango.CallLogUI", "onResume()");
    super.onResume();
    this.m_dateFormat = getDateFormatString();
    if (this.m_displayedEntries != m_logEntries)
      updateLogEntries();
  }

  void updateLogEntries()
  {
    Log.v("Tango.CallLogUI", "updateLogEntries(): # of entries = " + m_logEntries.size());
    if (m_receivedEntriesFromServer)
    {
      this.m_handler.removeMessages(1);
      doUpdateLogEntries();
    }
    while (this.m_handler.hasMessages(1))
      return;
    this.m_handler.sendEmptyMessageDelayed(1, 15000L);
  }

  private class CallLogAdapter extends ArrayAdapter<CallLogActivity.CallLogEntry>
  {
    private RelativeLayout.LayoutParams layoutParamsHasDuration;
    private RelativeLayout.LayoutParams layoutParamsNoDuration;
    private List<CallLogActivity.CallLogEntry> m_entries;
    private final LayoutInflater m_inflater;
    private final Resources m_resources = getContext().getResources();
    private final int m_textViewResourceId;

    public CallLogAdapter(int paramList, List<CallLogActivity.CallLogEntry> arg3)
    {
      super(i, localList);
      this.m_textViewResourceId = i;
      this.m_inflater = LayoutInflater.from(paramList);
      this.m_entries = localList;
      this.layoutParamsHasDuration = new RelativeLayout.LayoutParams(-2, -2);
      this.layoutParamsHasDuration.addRule(10, -1);
      this.layoutParamsHasDuration.addRule(0, 2131361848);
      this.layoutParamsHasDuration.addRule(9, -1);
      this.layoutParamsNoDuration = new RelativeLayout.LayoutParams(-2, -2);
      this.layoutParamsNoDuration.addRule(15, -1);
      this.layoutParamsNoDuration.addRule(0, 2131361848);
      this.layoutParamsNoDuration.addRule(9, -1);
    }

    private String formatDuration(int paramInt)
    {
      int i = paramInt / 3600;
      int j = paramInt % 3600 / 60;
      int k = paramInt % 60;
      if (i == 0)
      {
        StringBuilder localStringBuilder3 = new StringBuilder().append(j).append(":");
        Object[] arrayOfObject3 = new Object[1];
        arrayOfObject3[0] = Integer.valueOf(k);
        return String.format("%02d", arrayOfObject3);
      }
      StringBuilder localStringBuilder1 = new StringBuilder().append(i).append(":");
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = Integer.valueOf(j);
      StringBuilder localStringBuilder2 = localStringBuilder1.append(String.format("%02d", arrayOfObject1)).append(":");
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Integer.valueOf(k);
      return String.format("%02d", arrayOfObject2);
    }

    private String formatWhen(long paramLong)
    {
      int i = TimeZone.getDefault().getOffset(paramLong);
      long l = 1000L * paramLong + i;
      GregorianCalendar localGregorianCalendar1 = new GregorianCalendar();
      localGregorianCalendar1.setTime(new Date());
      GregorianCalendar localGregorianCalendar2 = new GregorianCalendar();
      localGregorianCalendar2.setTimeInMillis(l);
      if ((localGregorianCalendar2.get(1) == localGregorianCalendar1.get(1)) && (localGregorianCalendar2.get(2) == localGregorianCalendar1.get(2)) && (localGregorianCalendar2.get(5) == localGregorianCalendar1.get(5)))
        return localeDateString(paramLong, "\n" + this.m_resources.getString(2131296382));
      localGregorianCalendar2.setTimeInMillis(l + 86400000L);
      if ((localGregorianCalendar2.get(1) == localGregorianCalendar1.get(1)) && (localGregorianCalendar2.get(2) == localGregorianCalendar1.get(2)) && (localGregorianCalendar2.get(5) == localGregorianCalendar1.get(5)))
        return localeDateString(paramLong, "\n" + this.m_resources.getString(2131296383));
      return localeDateString(paramLong, null);
    }

    private String localeDateString(long paramLong, String paramString)
    {
      if (paramString != null)
        return DateUtils.formatDateTime(CallLogActivity.this, 1000L * paramLong, 524289) + paramString;
      if (CallLogActivity.this.m_dateFormat == null)
      {
        Log.i("Tango.CallLogUI", "!!Got null m_dateFormat, this should not happen!!");
        CallLogActivity.access$202(CallLogActivity.this, CallLogActivity.this.getDateFormatString());
      }
      return new SimpleDateFormat(CallLogActivity.this.m_dateFormat).format(new Date(paramLong * 1000L));
    }

    public int getCount()
    {
      return this.m_entries.size();
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView1;
      ViewHolder localViewHolder1;
      if (paramView == null)
      {
        View localView2 = this.m_inflater.inflate(this.m_textViewResourceId, null);
        ViewHolder localViewHolder2 = new ViewHolder();
        localViewHolder2.name = ((TextView)localView2.findViewById(2131361806));
        localViewHolder2.when = ((TextView)localView2.findViewById(2131361848));
        localViewHolder2.duration = ((TextView)localView2.findViewById(2131361849));
        localViewHolder2.type = CallLogActivity.EntryType.ENTRY_TYPE_UNKNOWN;
        localView2.setTag(localViewHolder2);
        localView1 = localView2;
        localViewHolder1 = localViewHolder2;
      }
      CallLogActivity.CallLogEntry localCallLogEntry;
      while (true)
      {
        localCallLogEntry = (CallLogActivity.CallLogEntry)this.m_entries.get(paramInt);
        localViewHolder1.name.setText(localCallLogEntry.displayName());
        localViewHolder1.when.setText(formatWhen(localCallLogEntry.m_when));
        if ((localCallLogEntry.m_type != CallLogActivity.EntryType.ENTRY_TYPE_MISSED_CALL) || (localCallLogEntry.m_type == localViewHolder1.type))
          break;
        localViewHolder1.name.setTextColor(this.m_resources.getColor(2131165212));
        localViewHolder1.duration.setVisibility(8);
        localViewHolder1.type = localCallLogEntry.m_type;
        localViewHolder1.duration.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        localViewHolder1.duration.setCompoundDrawablePadding(0);
        return localView1;
        localViewHolder1 = (ViewHolder)paramView.getTag();
        localView1 = paramView;
      }
      if (localCallLogEntry.m_type != localViewHolder1.type)
      {
        localViewHolder1.name.setTextColor(this.m_resources.getColor(2131165211));
        localViewHolder1.type = localCallLogEntry.m_type;
        localViewHolder1.duration.setVisibility(0);
      }
      if (localCallLogEntry.m_direction == 1)
      {
        localViewHolder1.duration.setCompoundDrawablesWithIntrinsicBounds(0, 0, 2130837671, 0);
        localViewHolder1.duration.setCompoundDrawablePadding((int)(0.5F + 5.0F * CallLogActivity.this.getResources().getDisplayMetrics().density));
      }
      localViewHolder1.duration.setText(formatDuration(localCallLogEntry.m_duration));
      return localView1;
    }

    class ViewHolder
    {
      TextView duration;
      TextView name;
      CallLogActivity.EntryType type;
      TextView when;

      ViewHolder()
      {
      }
    }
  }

  static class CallLogEntry extends Utils.UIContact
  {
    static final int DIRECTION_INBOUND = 0;
    static final int DIRECTION_OUTBOUND = 1;
    public int m_direction;
    public int m_duration;
    public CallLogActivity.EntryType m_type;
    public long m_when;

    public CallLogEntry(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, CallLogActivity.EntryType paramEntryType, String paramString7)
    {
      super(paramString2, paramString3, paramString4, paramString5, paramString6);
      this.m_type = paramEntryType;
      this.m_accountId = paramString7;
    }
  }

  private static enum EntryType
  {
    static
    {
      ENTRY_TYPE_NORMAL_CALL = new EntryType("ENTRY_TYPE_NORMAL_CALL", 1);
      ENTRY_TYPE_MISSED_CALL = new EntryType("ENTRY_TYPE_MISSED_CALL", 2);
      EntryType[] arrayOfEntryType = new EntryType[3];
      arrayOfEntryType[0] = ENTRY_TYPE_UNKNOWN;
      arrayOfEntryType[1] = ENTRY_TYPE_NORMAL_CALL;
      arrayOfEntryType[2] = ENTRY_TYPE_MISSED_CALL;
    }
  }

  private static enum ViewBySelection
  {
    static
    {
      ViewBySelection[] arrayOfViewBySelection = new ViewBySelection[2];
      arrayOfViewBySelection[0] = VIEW_BY_ALL;
      arrayOfViewBySelection[1] = VIEW_BY_MISSED;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.CallLogActivity
 * JD-Core Version:    0.6.2
 */