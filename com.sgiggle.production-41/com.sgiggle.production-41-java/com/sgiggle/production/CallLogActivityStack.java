package com.sgiggle.production;

import android.content.Intent;
import android.os.Bundle;

public class CallLogActivityStack extends ActivityStack
{
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    push("CallLogActivity", new Intent(this, CallLogActivity.class));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.CallLogActivityStack
 * JD-Core Version:    0.6.2
 */