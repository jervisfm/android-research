package com.sgiggle.production;

import android.content.Context;
import android.graphics.Bitmap;
import com.sgiggle.contacts.ContactStore;
import com.sgiggle.media_engine.MediaEngineMessage.InCallAlertEvent;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.CameraPosition;
import com.sgiggle.xmpp.SessionMessages.InCallAlertPayload;
import com.sgiggle.xmpp.SessionMessages.InCallAlertPayload.Type;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.VGoodBundle;
import java.util.List;

public class CallSession
{
  private int mEmptySlotCount = 0;
  private List<SessionMessages.VGoodBundle> mVGoodData = null;
  public SessionMessages.AvatarControlPayload.Direction m_avatarDirection = SessionMessages.AvatarControlPayload.Direction.NONE;
  public boolean m_callOnHold = false;
  public long m_callStartTime = 0L;
  public CallState m_callState = CallState.CALL_STATE_UNDEFINED;
  public boolean m_callerInitVideoCall = false;
  public SessionMessages.CameraPosition m_cameraPosition = SessionMessages.CameraPosition.CAM_NONE;
  public String m_inCallAlertText;
  public String m_localName;
  public boolean m_muted = false;
  private String m_peerAccountId;
  public long m_peerContactId = -1L;
  public String m_peerName;
  private boolean m_pipSwapped = false;
  public boolean m_showAnimation = false;
  public boolean m_showInCallAlert = false;
  public boolean m_showLowBandwidth = false;
  public boolean m_speakerOn = false;
  public SessionMessages.MediaSessionPayload.Direction m_videoDirection = SessionMessages.MediaSessionPayload.Direction.NONE;

  public CallSession(SessionMessages.MediaSessionPayload paramMediaSessionPayload, CallState paramCallState)
  {
    this.m_peerAccountId = paramMediaSessionPayload.getAccountId();
    this.m_callState = paramCallState;
    this.m_peerName = paramMediaSessionPayload.getDisplayname();
    if (paramMediaSessionPayload.hasLocalDisplayname())
      this.m_localName = paramMediaSessionPayload.getLocalDisplayname();
    if (paramMediaSessionPayload.hasDeviceContactId())
      this.m_peerContactId = paramMediaSessionPayload.getDeviceContactId();
  }

  public CallSession(String paramString, CallState paramCallState)
  {
    this.m_peerAccountId = paramString;
    this.m_callState = paramCallState;
  }

  public int getEmptySlotCount()
  {
    return this.mEmptySlotCount;
  }

  public String getPeerAccountId()
  {
    return this.m_peerAccountId;
  }

  public Bitmap getPeerPhoto()
  {
    boolean bool = this.m_peerContactId < -1L;
    Bitmap localBitmap = null;
    if (bool)
      localBitmap = ContactStore.getPhotoByContactId(this.m_peerContactId);
    return localBitmap;
  }

  public List<SessionMessages.VGoodBundle> getVGoodData()
  {
    return this.mVGoodData;
  }

  public boolean isPipSwapped()
  {
    return this.m_pipSwapped;
  }

  public boolean pipSwappable()
  {
    return (this.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.BOTH) || (this.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.BOTH) || ((this.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND) && (this.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.RECEIVER)) || ((this.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.RECEIVE) && (this.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.SENDER));
  }

  public void resetPipSwapped()
  {
    this.m_pipSwapped = false;
  }

  public void setEmptySlotCount(int paramInt)
  {
    this.mEmptySlotCount = paramInt;
  }

  public void setInCallAlertEvent(MediaEngineMessage.InCallAlertEvent paramInCallAlertEvent)
  {
    boolean bool;
    SessionMessages.InCallAlertPayload.Type localType;
    Context localContext;
    if (((SessionMessages.InCallAlertPayload)paramInCallAlertEvent.payload()).getHide() == 0)
    {
      bool = true;
      this.m_showInCallAlert = bool;
      localType = ((SessionMessages.InCallAlertPayload)paramInCallAlertEvent.payload()).getType();
      localContext = TangoApp.getInstance().getApplicationContext();
      if (localType != SessionMessages.InCallAlertPayload.Type.SWITCH_CAMERA)
        break label86;
      String str2 = localContext.getString(2131296345);
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = this.m_peerName;
      this.m_inCallAlertText = String.format(str2, arrayOfObject2);
    }
    label86: 
    do
    {
      return;
      bool = false;
      break;
      if (localType == SessionMessages.InCallAlertPayload.Type.ANIMATION)
      {
        String str1 = localContext.getString(2131296380);
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = this.m_peerName;
        this.m_inCallAlertText = String.format(str1, arrayOfObject1);
        return;
      }
    }
    while ((localType != SessionMessages.InCallAlertPayload.Type.PASS_THROUGH) || (!((SessionMessages.InCallAlertPayload)paramInCallAlertEvent.payload()).hasText()));
    this.m_inCallAlertText = ((SessionMessages.InCallAlertPayload)paramInCallAlertEvent.payload()).getText();
  }

  public void setPipSwapped(boolean paramBoolean)
  {
    this.m_pipSwapped = paramBoolean;
  }

  public void setVGoodData(List<SessionMessages.VGoodBundle> paramList)
  {
    this.mVGoodData = paramList;
  }

  public void swapPip()
  {
    if (pipSwappable())
      if (this.m_pipSwapped)
        break label22;
    label22: for (boolean bool = true; ; bool = false)
    {
      this.m_pipSwapped = bool;
      return;
    }
  }

  public void updatePeerAccountId(String paramString)
  {
    this.m_peerAccountId = paramString;
  }

  public void updateVgoodSelectorData(SessionMessages.MediaSessionPayload paramMediaSessionPayload)
  {
    List localList = paramMediaSessionPayload.getVgoodBundleList();
    if (localList.size() > 0)
      this.mVGoodData = localList;
    this.mEmptySlotCount = paramMediaSessionPayload.getEmptySlotCount();
  }

  public static enum CallState
  {
    static
    {
      CALL_STATE_INCOMING = new CallState("CALL_STATE_INCOMING", 1);
      CALL_STATE_DIALING = new CallState("CALL_STATE_DIALING", 2);
      CALL_STATE_CONNECTING = new CallState("CALL_STATE_CONNECTING", 3);
      CALL_STATE_ACTIVE = new CallState("CALL_STATE_ACTIVE", 4);
      CALL_STATE_DISCONNECTING = new CallState("CALL_STATE_DISCONNECTING", 5);
      CALL_STATE_DISCONNECTED = new CallState("CALL_STATE_DISCONNECTED", 6);
      CallState[] arrayOfCallState = new CallState[7];
      arrayOfCallState[0] = CALL_STATE_UNDEFINED;
      arrayOfCallState[1] = CALL_STATE_INCOMING;
      arrayOfCallState[2] = CALL_STATE_DIALING;
      arrayOfCallState[3] = CALL_STATE_CONNECTING;
      arrayOfCallState[4] = CALL_STATE_ACTIVE;
      arrayOfCallState[5] = CALL_STATE_DISCONNECTING;
      arrayOfCallState[6] = CALL_STATE_DISCONNECTED;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.CallSession
 * JD-Core Version:    0.6.2
 */