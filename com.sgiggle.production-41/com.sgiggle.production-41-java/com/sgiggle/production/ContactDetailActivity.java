package com.sgiggle.production;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.AddFavoriteContactMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CancelContactDetailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayContactDetailEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayInviteContactEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InviteContactMessage;
import com.sgiggle.media_engine.MediaEngineMessage.OpenConversationMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RemoveFavoriteContactMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.util.ContactThumbnailLoader;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.CallEntry;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.Contact.Builder;
import com.sgiggle.xmpp.SessionMessages.ContactDetailPayload;
import com.sgiggle.xmpp.SessionMessages.ContactDetailPayload.Source;
import com.sgiggle.xmpp.SessionMessages.ContactsPayload;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class ContactDetailActivity extends ActivityBase
  implements View.OnClickListener
{
  private static final String TAG = "Tango.ContactDetail";
  private TextView callDuration;
  private TextView callInfo;
  private TextView callTime;
  private Button conversationButton;
  private Button favoriteButton;
  private Button inviteButton;
  private MediaEngineMessage.DisplayContactDetailEvent latestEvent;
  private InviteDialogCreater mInviteDialogCreater;
  private String m_dateFormat = null;
  private Utils.UIContact m_uiContact;
  private ImageView userImage;
  private TextView userName;
  private Button videoButton;
  private Button voiceButton;

  private String formatDuration(int paramInt)
  {
    int i = paramInt / 3600;
    int j = paramInt % 3600 / 60;
    int k = paramInt % 60;
    if (i == 0)
    {
      StringBuilder localStringBuilder3 = new StringBuilder().append(j).append(":");
      Object[] arrayOfObject3 = new Object[1];
      arrayOfObject3[0] = Integer.valueOf(k);
      return String.format("%02d", arrayOfObject3);
    }
    StringBuilder localStringBuilder1 = new StringBuilder().append(i).append(":");
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Integer.valueOf(j);
    StringBuilder localStringBuilder2 = localStringBuilder1.append(String.format("%02d", arrayOfObject1)).append(":");
    Object[] arrayOfObject2 = new Object[1];
    arrayOfObject2[0] = Integer.valueOf(k);
    return String.format("%02d", arrayOfObject2);
  }

  private String formatWhen(long paramLong)
  {
    GregorianCalendar localGregorianCalendar1 = new GregorianCalendar();
    localGregorianCalendar1.setTime(new Date());
    GregorianCalendar localGregorianCalendar2 = new GregorianCalendar();
    localGregorianCalendar2.setTimeInMillis(paramLong * 1000L);
    if ((localGregorianCalendar2.get(1) == localGregorianCalendar1.get(1)) && (localGregorianCalendar2.get(2) == localGregorianCalendar1.get(2)) && (localGregorianCalendar2.get(5) == localGregorianCalendar1.get(5)))
      return localeDateString(paramLong, "\n" + getResources().getString(2131296382));
    localGregorianCalendar2.setTimeInMillis(1000L * (86400L + paramLong));
    if ((localGregorianCalendar2.get(1) == localGregorianCalendar1.get(1)) && (localGregorianCalendar2.get(2) == localGregorianCalendar1.get(2)) && (localGregorianCalendar2.get(5) == localGregorianCalendar1.get(5)))
      return localeDateString(paramLong, "\n" + getResources().getString(2131296383));
    return localeDateString(paramLong, null);
  }

  private String getDateFormatString()
  {
    char[] arrayOfChar = DateFormat.getDateFormatOrder(getBaseContext());
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    if (i < arrayOfChar.length)
    {
      switch (arrayOfChar[i])
      {
      default:
      case 'd':
      case 'M':
      case 'y':
      }
      while (true)
      {
        i++;
        break;
        localStringBuilder.append("dd/");
        continue;
        localStringBuilder.append("MM/");
        continue;
        localStringBuilder.append("yy/");
      }
    }
    return localStringBuilder.substring(0, localStringBuilder.length() - 1);
  }

  private String localeDateString(long paramLong, String paramString)
  {
    if (paramString != null)
      return DateUtils.formatDateTime(this, 1000L * paramLong, 524289) + paramString;
    return new SimpleDateFormat(this.m_dateFormat).format(new Date(1000L * paramLong));
  }

  private void requestInvite2Tango(Utils.UIContact paramUIContact)
  {
    Log.i("Tango.ContactDetail", "contact detail show invite");
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(SessionMessages.Contact.newBuilder().setDeviceContactId(paramUIContact.m_deviceContactId).build());
    MediaEngineMessage.InviteContactMessage localInviteContactMessage = new MediaEngineMessage.InviteContactMessage(localArrayList);
    MessageRouter.getInstance().postMessage("jingle", localInviteContactMessage);
  }

  private void updateLayoutFromEvent(MediaEngineMessage.DisplayContactDetailEvent paramDisplayContactDetailEvent)
  {
    if (paramDisplayContactDetailEvent == null);
    do
    {
      return;
      this.m_uiContact = Utils.UIContact.convertFromMessageContact(((SessionMessages.ContactDetailPayload)paramDisplayContactDetailEvent.payload()).getContact());
    }
    while (this.m_uiContact == null);
    if ((this.m_uiContact.m_accountId != null) && (this.m_uiContact.m_accountId.length() > 0))
    {
      findViewById(2131361894).setVisibility(0);
      findViewById(2131361899).setVisibility(8);
      this.userName.setText(this.m_uiContact.displayName());
      if ((this.m_uiContact.m_deviceContactId == -1L) || (!this.m_uiContact.hasPhoto))
        break label418;
      Bitmap localBitmap = ContactThumbnailLoader.getInstance().getLoadedBitmap(this.m_uiContact);
      if (localBitmap == null)
        break label418;
      this.userImage.setImageBitmap(localBitmap);
    }
    label388: label413: label418: for (int i = 1; ; i = 0)
    {
      if (i == 0)
        this.userImage.setImageResource(2130837648);
      if (((SessionMessages.ContactDetailPayload)paramDisplayContactDetailEvent.payload()).hasEntry())
      {
        this.callInfo.setText(2131296542);
        this.callDuration.setText(formatDuration(((SessionMessages.ContactDetailPayload)paramDisplayContactDetailEvent.payload()).getEntry().getDuration()));
        this.callTime.setText(formatWhen(((SessionMessages.ContactDetailPayload)paramDisplayContactDetailEvent.payload()).getEntry().getStartTime()));
        label224: if (((((SessionMessages.ContactDetailPayload)paramDisplayContactDetailEvent.payload()).getSource() != SessionMessages.ContactDetailPayload.Source.FROM_RECENT_PAGE) && (((SessionMessages.ContactDetailPayload)paramDisplayContactDetailEvent.payload()).getSource() != SessionMessages.ContactDetailPayload.Source.FROM_MESSAGES_PAGE)) || (this.m_uiContact.m_deviceContactId != -1L))
          break label388;
        findViewById(2131361891).setVisibility(8);
        findViewById(2131361890).setVisibility(8);
        label294: checkFavoriteStat();
        if (!((SessionMessages.ContactDetailPayload)paramDisplayContactDetailEvent.payload()).hasAccountValidated())
          break label413;
      }
      for (boolean bool = ((SessionMessages.ContactDetailPayload)paramDisplayContactDetailEvent.payload()).getAccountValidated(); ; bool = true)
      {
        this.conversationButton.setEnabled(bool);
        return;
        findViewById(2131361894).setVisibility(8);
        findViewById(2131361899).setVisibility(0);
        break;
        this.callInfo.setText(2131296381);
        this.callDuration.setText("");
        this.callTime.setText("");
        break label224;
        findViewById(2131361891).setVisibility(0);
        findViewById(2131361890).setVisibility(0);
        break label294;
      }
    }
  }

  protected void checkFavoriteStat()
  {
    if (this.m_uiContact.m_favorite)
    {
      this.favoriteButton.setText(getResources().getString(2131296502));
      this.favoriteButton.setCompoundDrawablesWithIntrinsicBounds(2130837631, 0, 0, 0);
      return;
    }
    this.favoriteButton.setText(getResources().getString(2131296501));
    this.favoriteButton.setCompoundDrawablesWithIntrinsicBounds(2130837632, 0, 0, 0);
  }

  protected void handleNewMessage(Message paramMessage)
  {
    Log.d("Tango.ContactDetail", "handleNewMessage(): Message = " + paramMessage);
    if (paramMessage == null)
    {
      TangoApp.getInstance().onActivityResumeAfterKilled(this);
      return;
    }
    switch (paramMessage.getType())
    {
    default:
      Log.w("Tango.ContactDetail", "handleNewMessage(): Unsupported message=" + paramMessage);
      return;
    case 35091:
      SessionMessages.ContactsPayload localContactsPayload = (SessionMessages.ContactsPayload)((MediaEngineMessage.DisplayInviteContactEvent)paramMessage).payload();
      this.mInviteDialogCreater.doInvite2Tango(localContactsPayload);
      return;
    case 35216:
    }
    MediaEngineMessage.DisplayContactDetailEvent localDisplayContactDetailEvent = (MediaEngineMessage.DisplayContactDetailEvent)paramMessage;
    updateLayoutFromEvent(localDisplayContactDetailEvent);
    this.latestEvent = localDisplayContactDetailEvent;
  }

  public void onBackPressed()
  {
    Log.i("Tango.ContactDetail", "contact detail on back key");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelContactDetailMessage());
  }

  public void onClick(View paramView)
  {
    if (this.m_uiContact == null)
      return;
    int i = paramView.getId();
    Log.d("Tango.ContactDetail", "onClick(View " + paramView + ", id " + i + ")...");
    ArrayList localArrayList;
    switch (i)
    {
    case 2131361892:
    case 2131361893:
    case 2131361894:
    case 2131361897:
    case 2131361899:
    case 2131361900:
    default:
      return;
    case 2131361891:
      localArrayList = new ArrayList();
      localArrayList.add(SessionMessages.Contact.newBuilder().setAccountid(this.m_uiContact.m_accountId).build());
      if (this.m_uiContact.m_favorite)
      {
        MediaEngineMessage.RemoveFavoriteContactMessage localRemoveFavoriteContactMessage = new MediaEngineMessage.RemoveFavoriteContactMessage(localArrayList);
        MessageRouter.getInstance().postMessage("jingle", localRemoveFavoriteContactMessage);
      }
    case 2131361895:
    case 2131361896:
      for (this.m_uiContact.m_favorite = false; ; this.m_uiContact.m_favorite = true)
      {
        checkFavoriteStat();
        return;
        CallHandler.getDefault().sendCallMessage(this.m_uiContact.m_accountId, this.m_uiContact.displayName(), this.m_uiContact.m_deviceContactId, CallHandler.VideoMode.VIDEO_ON);
        return;
        CallHandler.getDefault().sendCallMessage(this.m_uiContact.m_accountId, this.m_uiContact.displayName(), this.m_uiContact.m_deviceContactId, CallHandler.VideoMode.VIDEO_OFF);
        return;
        MediaEngineMessage.AddFavoriteContactMessage localAddFavoriteContactMessage = new MediaEngineMessage.AddFavoriteContactMessage(localArrayList);
        MessageRouter.getInstance().postMessage("jingle", localAddFavoriteContactMessage);
      }
    case 2131361901:
      requestInvite2Tango(this.m_uiContact);
      return;
    case 2131361898:
    }
    MediaEngineMessage.OpenConversationMessage localOpenConversationMessage = new MediaEngineMessage.OpenConversationMessage(this.m_uiContact.convertToMessageContact());
    MessageRouter.getInstance().postMessage("jingle", localOpenConversationMessage);
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.ContactDetail", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903059);
    this.mInviteDialogCreater = new InviteDialogCreater(getParent(), LayoutInflater.from(getApplicationContext()));
    this.userName = ((TextView)findViewById(2131361884));
    this.userImage = ((ImageView)findViewById(2131361883));
    this.videoButton = ((Button)findViewById(2131361895));
    this.voiceButton = ((Button)findViewById(2131361896));
    this.favoriteButton = ((Button)findViewById(2131361891));
    this.inviteButton = ((Button)findViewById(2131361901));
    this.conversationButton = ((Button)findViewById(2131361898));
    this.callDuration = ((TextView)findViewById(2131361889));
    this.callTime = ((TextView)findViewById(2131361888));
    this.callInfo = ((TextView)findViewById(2131361886));
    this.videoButton.setOnClickListener(this);
    this.voiceButton.setOnClickListener(this);
    this.favoriteButton.setOnClickListener(this);
    this.inviteButton.setOnClickListener(this);
    this.conversationButton.setOnClickListener(this);
    this.m_dateFormat = getDateFormatString();
    StringBuilder localStringBuilder = new StringBuilder();
    String str = getString(2131296504);
    if ((str != null) && (str.length() > 0))
      localStringBuilder.append("<b>").append(str).append("</b>&nbsp;");
    localStringBuilder.append(getString(2131296505));
    ((TextView)findViewById(2131361903)).setText(Html.fromHtml(localStringBuilder.toString()));
    handleNewMessage(getFirstMessage());
  }

  protected void onResume()
  {
    super.onResume();
    this.m_dateFormat = getDateFormatString();
    if ((getFirstMessage() == null) || (getFirstMessage().getType() != 35216))
      return;
    boolean bool;
    if (this.m_uiContact != null)
      bool = this.m_uiContact.m_favorite;
    for (int i = 1; ; i = 0)
    {
      if (this.latestEvent != null)
        updateLayoutFromEvent(this.latestEvent);
      while (i != 0)
      {
        this.m_uiContact.m_favorite = bool;
        checkFavoriteStat();
        return;
        updateLayoutFromEvent((MediaEngineMessage.DisplayContactDetailEvent)getFirstMessage());
      }
      break;
      bool = false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ContactDetailActivity
 * JD-Core Version:    0.6.2
 */