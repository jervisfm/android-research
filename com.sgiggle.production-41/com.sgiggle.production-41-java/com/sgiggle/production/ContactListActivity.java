package com.sgiggle.production;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract.Contacts;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayContactDetailEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayInviteContactEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplaySettingsMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteContactMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteDisplayMainMessage;
import com.sgiggle.media_engine.MediaEngineMessage.LoginCompletedEvent;
import com.sgiggle.media_engine.MediaEngineMessage.RequestFilterContactMessage;
import com.sgiggle.media_engine.MediaEngineMessage.UpdateTangoUsersEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ViewContactDetailMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.adapter.ContactListAdapter;
import com.sgiggle.production.adapter.ContactListAdapter.ContactListSelection;
import com.sgiggle.production.screens.videomail.VideomailSharedPreferences;
import com.sgiggle.production.util.ContactThumbnailLoader;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.Contact.Builder;
import com.sgiggle.xmpp.SessionMessages.ContactsPayload;
import com.sgiggle.xmpp.SessionMessages.ContactsSource;
import com.sgiggle.xmpp.SessionMessages.LoginCompletedPayload;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ContactListActivity extends ActivityBase
  implements RadioGroup.OnCheckedChangeListener
{
  private static final int ADD_CONTACT_ID = 6;
  private static final boolean ALL_TOGGLE_AVAILABLE = false;
  private static final boolean DEBUG_EMAIL_PHONE_SELECTION = false;
  private static final ContactListAdapter.ContactListSelection DEFAULT_CONTACT_LIST_SELECTION = ContactListAdapter.ContactListSelection.TANGO;
  private static final int DIALOG_REFRESH_CONTACT = 0;
  private static final int PERFORM_ACCOUNT_SYNC = 2;
  private static final int PERFORM_ACCOUNT_SYNC_DELAY = 5000;
  private static final String PREF_VIEW_ALL_INDEX = "view_all_index";
  private static final String PREF_VIEW_ALL_TOP = "view_all_top";
  private static final String PREF_VIEW_TANGO_INDEX = "view_index";
  private static final String PREF_VIEW_TANGO_TOP = "view_top";
  private static final int REFRESH_CONTACTS_ID = 4;
  private static final int REQUEST_CODE_ADD_CONTACT = 0;
  private static final int SEARCH_ID = 5;
  private static final String SEARCH_INPUT_KEY_ROTATION = "*ffc";
  private static final int SHOW_CONTACTS_FIRST_TIME = 1;
  private static final int SHOW_CONTACTS_FIRST_TIME_DELAY = 15000;
  private static final String TAG = "Tango.ContactsActivity";
  private static final String USER_GROUP_SELECTION = "user_grp_selection";
  private static final boolean VDBG = true;
  private static List<Utils.UIContact> m_allContacts = new ArrayList();
  private static ContactStore.ContactOrderPair m_contactOrderPair = ContactStore.ContactOrderPair.getDefault();
  private static SessionMessages.ContactsSource m_sourceType = SessionMessages.ContactsSource.LOCAL_STORAGE_ONLY;
  private InviteDialogCreater mInviteDialogCreater;
  private LayoutInflater m_Inflater;
  private DisplayContactListAdapter m_adapter;
  private SessionMessages.OperationalAlert m_alert;
  private int m_alertCount;
  private RadioGroup m_contactListGroupRadioGroup;
  private List<Utils.UIContact> m_contacts;
  private Button m_emptyInviteButton;
  private String m_emptyTangoListText;
  private View m_emptyView;
  private TextView m_emptyViewText;
  private View m_footerView;
  private TextView m_footerViewCount;
  private ContactListAdapter.ContactListSelection m_groupSelection = ContactListAdapter.ContactListSelection.TANGO;
  private Handler m_handler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      if (ContactListActivity.this.m_isDestroyed)
      {
        Log.d("Tango.ContactsActivity", "Handler: ignoring message " + paramAnonymousMessage + "; we're destroyed!");
        return;
      }
      switch (paramAnonymousMessage.what)
      {
      default:
        return;
      case 1:
        ContactListActivity.this.doUpdateContacts();
        return;
      case 2:
      }
      ContactListActivity.this.performAccountSync();
    }
  };
  private boolean m_hasRefreshProgressDlg;
  private TextView m_header;
  private boolean m_isDestroyed = false;
  private ListViewIgnoreBackKey m_listView;
  private boolean m_onTouch = false;
  private SharedPreferences m_prefs;
  private String m_query;
  private int m_scrollState = 0;
  private TextView m_searchTitleView;
  private boolean m_syncAccountPending = true;

  private void createOrSyncContactsWithDevice()
  {
    String str = getString(2131296391);
    if (AccountManager.get(this).getAccountsByType(str).length > 0)
    {
      Log.d("Tango.ContactsActivity", "Tango sync account has been created. Sync contacts instead...");
      performAccountSync();
      return;
    }
    Log.i("Tango.ContactsActivity", "Create Tango sync account...");
    AccountManager.get(this).addAccount(str, null, null, null, this, new AccountManagerCallback()
    {
      public void run(AccountManagerFuture<Bundle> paramAnonymousAccountManagerFuture)
      {
        try
        {
          Bundle localBundle = (Bundle)paramAnonymousAccountManagerFuture.getResult();
          Log.d("Tango.ContactsActivity", "Account: [" + localBundle.getString("authAccount") + "] has been created. isDone = " + paramAnonymousAccountManagerFuture.isDone());
          ContactListActivity.this.m_handler.sendEmptyMessageDelayed(2, 5000L);
          return;
        }
        catch (OperationCanceledException localOperationCanceledException)
        {
          Log.e("Tango.ContactsActivity", "addAccount was canceled");
          return;
        }
        catch (IOException localIOException)
        {
          Log.e("Tango.ContactsActivity", "addAccount failed: " + localIOException);
          return;
        }
        catch (AuthenticatorException localAuthenticatorException)
        {
          Log.e("Tango.ContactsActivity", "addAccount failed: " + localAuthenticatorException);
        }
      }
    }
    , null);
  }

  private void displayContacts(List<Utils.UIContact> paramList)
  {
    Log.d("Tango.ContactsActivity", "displayContacts(): New list-size=" + paramList.size());
    while (true)
    {
      int i;
      synchronized (this.m_listView)
      {
        this.m_adapter = new DisplayContactListAdapter(2130903065, m_contactOrderPair, DEFAULT_CONTACT_LIST_SELECTION, this.m_Inflater);
        this.m_adapter.loadGroups(paramList, this.m_groupSelection);
        this.m_adapter.notifyDataSetChanged();
        i = this.m_adapter.getContactCount();
        this.m_listView.removeFooterView(this.m_footerView);
        this.m_listView.removeHeaderView(this.m_header);
        if (this.m_query == null)
        {
          if (i > 0)
          {
            if (this.m_groupSelection == ContactListAdapter.ContactListSelection.FAVORITE)
            {
              TextView localTextView2 = this.m_footerViewCount;
              Resources localResources2 = getResources();
              Object[] arrayOfObject2 = new Object[1];
              arrayOfObject2[0] = Integer.valueOf(i);
              localTextView2.setText(localResources2.getQuantityString(2131427332, i, arrayOfObject2));
              this.m_listView.addFooterView(this.m_footerView, null, false);
            }
          }
          else
          {
            this.m_listView.setFastScrollEnabled(false);
            this.m_listView.setAdapter(this.m_adapter);
            this.m_listView.setFastScrollEnabled(true);
            this.m_listView.forceOnSizeChanged();
            return;
          }
          TextView localTextView1 = this.m_footerViewCount;
          Resources localResources1 = getResources();
          Object[] arrayOfObject1 = new Object[1];
          arrayOfObject1[0] = Integer.valueOf(i);
          localTextView1.setText(localResources1.getQuantityString(2131427331, i, arrayOfObject1));
        }
      }
      TextView localTextView3 = this.m_header;
      Resources localResources3 = getResources();
      Object[] arrayOfObject3 = new Object[1];
      arrayOfObject3[0] = Integer.valueOf(i);
      localTextView3.setText(localResources3.getQuantityString(2131427330, i, arrayOfObject3));
      this.m_listView.addHeaderView(this.m_header, null, false);
    }
  }

  private void doUpdateContacts()
  {
    Log.v("Tango.ContactsActivity", "doUpdateContacts()");
    if (this.m_hasRefreshProgressDlg)
    {
      getParent().dismissDialog(0);
      this.m_hasRefreshProgressDlg = false;
    }
    List localList = m_allContacts;
    if (localList != null)
    {
      this.m_contacts = localList;
      this.m_searchTitleView.setVisibility(8);
      this.m_contactListGroupRadioGroup.setVisibility(0);
      updateEmptyView();
      displayContacts(localList);
      restoreScrollPositionInList();
    }
    if (this.m_syncAccountPending)
    {
      this.m_syncAccountPending = false;
      createOrSyncContactsWithDevice();
    }
  }

  static Utils.UIContact findContact(String paramString)
  {
    Iterator localIterator = m_allContacts.iterator();
    while (localIterator.hasNext())
    {
      Utils.UIContact localUIContact = (Utils.UIContact)localIterator.next();
      if (paramString.equals(localUIContact.m_accountId))
        return localUIContact;
    }
    return null;
  }

  private int getGroupSelectionType()
  {
    return this.m_prefs.getInt("user_grp_selection", -1);
  }

  public static List<Utils.UIContact> getTangoContacts()
  {
    if (m_sourceType != SessionMessages.ContactsSource.ADDRESS_BOOK_AND_CONTACT_FILTER)
    {
      Log.d("Tango.ContactsActivity", "getTangoContacts: Tango contacts not yet retrieved from Tango server.");
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = m_allContacts.iterator();
    while (localIterator.hasNext())
    {
      Utils.UIContact localUIContact = (Utils.UIContact)localIterator.next();
      if (!TextUtils.isEmpty(localUIContact.m_accountId))
        localArrayList.add(localUIContact);
    }
    return localArrayList;
  }

  private void handleSearch(String paramString)
  {
    Log.d("Tango.ContactsActivity", "handleSearch: [" + paramString + "]");
    List localList = m_allContacts;
    if (localList == null)
    {
      Log.d("Tango.ContactsActivity", "handleSearch: No contacts. Do nothing.");
      return;
    }
    ArrayList localArrayList = new ArrayList();
    String str = paramString.trim().toUpperCase();
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      Utils.UIContact localUIContact = (Utils.UIContact)localIterator.next();
      if (localUIContact.displayName().toUpperCase().indexOf(str) != -1)
        localArrayList.add(localUIContact);
    }
    this.m_searchTitleView.setVisibility(0);
    this.m_contactListGroupRadioGroup.setVisibility(8);
    this.m_searchTitleView.setText(String.format(getResources().getString(2131296341), new Object[] { paramString }));
    updateEmptyView(true);
    displayContacts(localArrayList);
  }

  private void hideTangoAlerts()
  {
    this.m_alertCount = 0;
    this.m_alert = null;
    displayTangoAlerts(null);
  }

  private void openContactDetailPage(MediaEngineMessage.DisplayContactDetailEvent paramDisplayContactDetailEvent)
  {
    Log.d("Tango.ContactsActivity", "open the contact detail page when receiving the event");
    ((ActivityStack)getParent()).pushWithMessage(ContactDetailActivity.class, paramDisplayContactDetailEvent);
  }

  private void performAccountSync()
  {
    Log.d("Tango.ContactsActivity", "performAccountSync() ...");
    String str = getString(2131296391);
    Account[] arrayOfAccount = AccountManager.get(this).getAccountsByType(str);
    if (arrayOfAccount.length > 0)
      ContentResolver.requestSync(arrayOfAccount[0], "com.android.contacts", new Bundle());
  }

  private void postRequestContactFilter()
  {
    this.m_syncAccountPending = true;
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.RequestFilterContactMessage());
  }

  private void requestInvite2Tango(Utils.UIContact paramUIContact)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(SessionMessages.Contact.newBuilder().setDeviceContactId(paramUIContact.m_deviceContactId).build());
    MediaEngineMessage.InviteContactMessage localInviteContactMessage = new MediaEngineMessage.InviteContactMessage(localArrayList);
    MessageRouter.getInstance().postMessage("jingle", localInviteContactMessage);
  }

  private void restoreGroupSelectionType()
  {
    int i = this.m_prefs.getInt("user_grp_selection", -1);
    if (i != -1)
    {
      if (i != ContactListAdapter.ContactListSelection.ALL.ordinal())
        break label66;
      this.m_groupSelection = ContactListAdapter.ContactListSelection.ALL;
    }
    while (true)
    {
      if (this.m_groupSelection == ContactListAdapter.ContactListSelection.ALL)
      {
        Log.d("Tango.ContactsActivity", "restoreGroupSelectionType: ALL is not available, forcing to TANGO");
        this.m_groupSelection = ContactListAdapter.ContactListSelection.TANGO;
        saveUserGroupSelectionType();
      }
      return;
      label66: if (i == ContactListAdapter.ContactListSelection.TANGO.ordinal())
        this.m_groupSelection = ContactListAdapter.ContactListSelection.TANGO;
      else
        this.m_groupSelection = ContactListAdapter.ContactListSelection.FAVORITE;
    }
  }

  private void restoreScrollPositionInList()
  {
    if ((this.m_onTouch) || (this.m_scrollState != 0))
      Log.v("Tango.ContactsActivity", "restoreScrollPositionInList/skip");
    while (true)
    {
      return;
      int m;
      int i;
      if (getGroupSelectionType() == ContactListAdapter.ContactListSelection.TANGO.ordinal())
      {
        int k = this.m_prefs.getInt("view_index", -1);
        m = this.m_prefs.getInt("view_top", 0);
        Log.v("Tango.ContactsActivity", "restoreScrollPositionInList/tango: index=" + k + ", top=" + m);
        i = k;
      }
      for (int j = m; i != -1; j = 0)
      {
        this.m_listView.setSelectionFromTop(i, j);
        return;
        i = -1;
      }
    }
  }

  private void saveScrollPositionInList()
  {
    int i = this.m_listView.getFirstVisiblePosition();
    View localView = this.m_listView.getChildAt(0);
    int j;
    SharedPreferences.Editor localEditor;
    int k;
    if (localView == null)
    {
      j = 0;
      localEditor = this.m_prefs.edit();
      k = getGroupSelectionType();
      if (k != ContactListAdapter.ContactListSelection.ALL.ordinal())
        break label126;
      localEditor.putInt("view_all_index", i);
      localEditor.putInt("view_all_top", j);
      Log.v("Tango.ContactsActivity", "saveScrollPositionInList/all: index=" + i + ", top=" + j);
    }
    while (true)
    {
      localEditor.commit();
      return;
      j = localView.getTop();
      break;
      label126: if (k == ContactListAdapter.ContactListSelection.TANGO.ordinal())
      {
        localEditor.putInt("view_index", i);
        localEditor.putInt("view_top", j);
        Log.v("Tango.ContactsActivity", "saveScrollPositionInList/tango: index=" + i + ", top=" + j);
      }
    }
  }

  private void saveUserGroupSelectionType()
  {
    SharedPreferences.Editor localEditor = this.m_prefs.edit();
    localEditor.putInt("user_grp_selection", this.m_groupSelection.ordinal());
    localEditor.commit();
  }

  private void showAddContactActivity()
  {
    Intent localIntent = new Intent("android.intent.action.INSERT", ContactsContract.Contacts.CONTENT_URI);
    try
    {
      startActivityForResult(localIntent, 0);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Log.w("Tango.ContactsActivity", "Not activity was found for ACTION_INSERT (for adding contact)");
    }
  }

  private void showContactDetailPage(Utils.UIContact paramUIContact)
  {
    Log.d("Tango.ContactsActivity", "send message to client core to open the detail page of a selected UI contact");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ViewContactDetailMessage(paramUIContact.convertToMessageContact()));
  }

  static void storeContacts(com.sgiggle.messaging.Message paramMessage, Context paramContext)
  {
    m_contactOrderPair = ContactStore.ContactOrderPair.getFromPhone(paramContext);
    switch (paramMessage.getType())
    {
    default:
      Log.w("Tango.ContactsActivity", "storeContacts(): Unsupported message = [" + paramMessage + "]");
      return;
    case 35037:
    }
    MediaEngineMessage.UpdateTangoUsersEvent localUpdateTangoUsersEvent = (MediaEngineMessage.UpdateTangoUsersEvent)paramMessage;
    List localList = ((SessionMessages.ContactsPayload)localUpdateTangoUsersEvent.payload()).getContactsList();
    m_sourceType = ((SessionMessages.ContactsPayload)localUpdateTangoUsersEvent.payload()).getSource();
    Log.d("Tango.ContactsActivity", "storeContacts(): # of entries = " + localList.size() + ", source-type = " + m_sourceType);
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
      localArrayList.add(Utils.UIContact.convertFromMessageContact((SessionMessages.Contact)localIterator.next()));
    Collections.sort(localArrayList, new Utils.ContactComparator(m_contactOrderPair.getSortOrder()));
    m_allContacts = localArrayList;
  }

  private void switchFooterFrame(boolean paramBoolean)
  {
    View localView = findViewById(2131361795);
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localView.setVisibility(i);
      return;
    }
  }

  private void updateEmptyView()
  {
    updateEmptyView(false);
  }

  private void updateEmptyView(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.m_emptyViewText.setText(getResources().getString(2131296342));
      this.m_emptyInviteButton.setVisibility(8);
      return;
    }
    if (this.m_groupSelection == ContactListAdapter.ContactListSelection.FAVORITE)
    {
      this.m_emptyViewText.setText(getResources().getString(2131296340));
      this.m_emptyInviteButton.setVisibility(8);
      return;
    }
    this.m_emptyViewText.setText(this.m_emptyTangoListText);
    this.m_emptyInviteButton.setVisibility(0);
  }

  public void displayTangoAlerts(com.sgiggle.messaging.Message paramMessage)
  {
    boolean bool;
    label13: TextView localTextView;
    if (paramMessage == null)
    {
      if (this.m_alertCount <= 0)
        break label171;
      bool = true;
      switchFooterFrame(bool);
      if (bool)
      {
        findViewById(2131361795).setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            if (ContactListActivity.this.m_alertCount > 0)
            {
              MediaEngineMessage.DisplaySettingsMessage localDisplaySettingsMessage = new MediaEngineMessage.DisplaySettingsMessage(ContactListActivity.this.m_alert);
              MessageRouter.getInstance().postMessage("jingle", localDisplaySettingsMessage);
              ContactListActivity.access$1302(ContactListActivity.this, 0);
              ContactListActivity.access$1402(ContactListActivity.this, null);
            }
          }
        });
        localTextView = (TextView)findViewById(2131361797);
        if (this.m_alertCount != 1)
          break label176;
      }
    }
    label171: label176: Resources localResources;
    int i;
    Object[] arrayOfObject;
    for (String str = this.m_alert.getTitle(); ; str = localResources.getQuantityString(2131427336, i, arrayOfObject))
    {
      SpannableString localSpannableString = new SpannableString(str);
      localSpannableString.setSpan(new UnderlineSpan(), 0, localSpannableString.length(), 0);
      localTextView.setText(localSpannableString);
      Log.v("Tango.ContactsActivity", "displayTangoAlerts()");
      return;
      this.m_alertCount = 0;
      this.m_alert = null;
      List localList = CTANotifier.extractAlertList(paramMessage);
      if ((localList == null) || (localList.size() <= 0))
        break;
      this.m_alertCount = localList.size();
      this.m_alert = ((SessionMessages.OperationalAlert)localList.get(0));
      break;
      bool = false;
      break label13;
      localResources = getResources();
      i = this.m_alertCount;
      arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(this.m_alertCount);
    }
  }

  protected void handleNewMessage(com.sgiggle.messaging.Message paramMessage)
  {
    Log.d("Tango.ContactsActivity", "handleNewMessage(): Message = " + paramMessage);
    if (paramMessage == null)
    {
      TangoApp.getInstance().onActivityResumeAfterKilled(this);
      return;
    }
    switch (paramMessage.getType())
    {
    default:
      Log.w("Tango.ContactsActivity", "handleNewMessage(): Unsupported message=" + paramMessage);
      return;
    case 35091:
      SessionMessages.ContactsPayload localContactsPayload = (SessionMessages.ContactsPayload)((MediaEngineMessage.DisplayInviteContactEvent)paramMessage).payload();
      this.mInviteDialogCreater.doInvite2Tango(localContactsPayload);
      return;
    case 35011:
      MediaEngineMessage.LoginCompletedEvent localLoginCompletedEvent = (MediaEngineMessage.LoginCompletedEvent)paramMessage;
      if (((SessionMessages.LoginCompletedPayload)localLoginCompletedEvent.payload()).hasSpecifiedEmptyListPrompt());
      for (String str = ((SessionMessages.LoginCompletedPayload)localLoginCompletedEvent.payload()).getSpecifiedEmptyListPrompt(); ; str = getResources().getString(2131296339))
      {
        this.m_emptyTangoListText = str;
        updateEmptyView();
        displayTangoAlerts(paramMessage);
        return;
      }
    case 35037:
      displayTangoAlerts(paramMessage);
      return;
    case 35088:
      hideTangoAlerts();
      return;
    case 35216:
    }
    openContactDetailPage((MediaEngineMessage.DisplayContactDetailEvent)paramMessage);
  }

  public void handleSearchIntent(Intent paramIntent)
  {
    Log.d("Tango.ContactsActivity", "handleSearchIntent " + paramIntent);
    if ("android.intent.action.SEARCH".equals(paramIntent.getAction()))
    {
      TabActivityBase localTabActivityBase = TangoApp.getInstance().getTabsActivityInstance();
      if (localTabActivityBase == null)
      {
        this.m_query = "";
        return;
      }
      localTabActivityBase.setContactSearchActivity(this);
      this.m_query = paramIntent.getStringExtra("query");
      if ((this.m_query != null) && (this.m_query.startsWith("*debug")) && (this.m_query.endsWith("#")))
      {
        boolean bool2 = this.m_query.equals("*debug1#");
        TangoApp.saveScreenLoggerOption(bool2);
        Log.i("Tango.ContactsActivity", "Set screen-logger = " + bool2);
      }
      do
        while (true)
        {
          handleSearch(this.m_query);
          return;
          if ((this.m_query == null) || (!this.m_query.startsWith("*vmailr")) || (!this.m_query.endsWith("#")))
            break;
          boolean bool1 = this.m_query.equals("*vmailr1#");
          VideomailSharedPreferences.setForceVideomailReplay(getApplicationContext(), bool1);
          Log.i("Tango.ContactsActivity", "Set forceVideomailReplay = " + bool1);
        }
      while ((this.m_query == null) || (!this.m_query.startsWith("*ffc")) || (!this.m_query.endsWith("#")));
      int i = this.m_query.indexOf("#");
      int j = "*ffc".length();
      if (i > j);
      while (true)
      {
        try
        {
          int m = Integer.parseInt(this.m_query.substring(j, i));
          k = m;
          TangoApp.saveFrontCameraRotationOption(k);
          Log.i("Tango.ContactsActivity", "Set camera rotation option = " + k);
        }
        catch (Exception localException)
        {
          Log.e("Tango.ContactsActivity", "Invalid rotation value" + localException);
        }
        int k = -1;
      }
    }
    this.m_contactListGroupRadioGroup.check(2131361908);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Log.w("Tango.ContactsActivity", "onActivityResult(): requestCode = " + paramInt1 + ", resultCode = " + paramInt2);
    switch (paramInt1)
    {
    default:
    case 0:
    }
    do
      return;
    while (paramInt2 != -1);
    postRequestContactFilter();
  }

  public void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt)
  {
    switch (paramInt)
    {
    case 2131361907:
    default:
    case 2131361906:
    case 2131361908:
    case 2131361909:
    }
    while (true)
    {
      if (this.m_contacts != null)
        displayContacts(this.m_contacts);
      updateEmptyView();
      saveUserGroupSelectionType();
      restoreScrollPositionInList();
      return;
      this.m_groupSelection = ContactListAdapter.ContactListSelection.ALL;
      continue;
      this.m_groupSelection = ContactListAdapter.ContactListSelection.TANGO;
      continue;
      this.m_groupSelection = ContactListAdapter.ContactListSelection.FAVORITE;
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.ContactsActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903060);
    setDefaultKeyMode(3);
    this.m_prefs = getSharedPreferences("Tango.ContactsActivity", 0);
    restoreGroupSelectionType();
    this.m_Inflater = LayoutInflater.from(getApplicationContext());
    this.mInviteDialogCreater = new InviteDialogCreater(getParent(), this.m_Inflater);
    this.m_listView = ((ListViewIgnoreBackKey)findViewById(2131361854));
    this.m_emptyView = findViewById(2131361855);
    this.m_emptyViewText = ((TextView)findViewById(2131361910));
    this.m_emptyInviteButton = ((Button)findViewById(2131361911));
    this.m_emptyInviteButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TabActivityBase localTabActivityBase = TangoApp.getInstance().getTabsActivityInstance();
        if (localTabActivityBase != null)
          localTabActivityBase.setForceSwitchTab();
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.InviteDisplayMainMessage());
      }
    });
    this.m_listView.setEmptyView(this.m_emptyView);
    this.m_emptyTangoListText = getResources().getString(2131296339);
    this.m_contactListGroupRadioGroup = ((RadioGroup)findViewById(2131361905));
    this.m_contactListGroupRadioGroup.setOnCheckedChangeListener(this);
    this.m_footerView = getLayoutInflater().inflate(2130903063, null);
    this.m_footerViewCount = ((TextView)this.m_footerView.findViewById(2131361913));
    updateEmptyView();
    this.m_searchTitleView = ((TextView)findViewById(2131361904));
    this.m_header = new TextView(this);
    this.m_header.setBackgroundColor(-16777216);
    this.m_header.setTextColor(-3355444);
    this.m_listView.setItemsCanFocus(false);
    this.m_listView.setClickable(true);
    this.m_listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        Log.d("Tango.ContactsActivity", "onClick(View " + paramAnonymousView + ", id " + paramAnonymousLong + ")...");
        while (true)
        {
          Utils.UIContact localUIContact;
          synchronized (ContactListActivity.this.m_listView)
          {
            if ((paramAnonymousView == ContactListActivity.this.m_header) || (paramAnonymousView == ContactListActivity.this.m_footerView))
            {
              Log.w("Tango.ContactsActivity", "onItemClick: header or footer was clicked, which should not happen!");
              return;
            }
            localUIContact = (Utils.UIContact)ContactListActivity.this.m_adapter.getItem(paramAnonymousInt - ContactListActivity.this.m_listView.getHeaderViewsCount());
            if (localUIContact != null)
            {
              if ((localUIContact.m_accountId != null) && (localUIContact.m_accountId.length() > 0))
                ContactListActivity.this.showContactDetailPage(localUIContact);
            }
            else
              return;
          }
          ContactListActivity.this.requestInvite2Tango(localUIContact);
        }
      }
    });
    handleSearchIntent(getIntent());
    this.m_listView.setOnScrollListener(new AbsListView.OnScrollListener()
    {
      public void onScroll(AbsListView paramAnonymousAbsListView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }

      public void onScrollStateChanged(AbsListView paramAnonymousAbsListView, int paramAnonymousInt)
      {
        if (paramAnonymousInt == 0)
        {
          ContactListActivity.this.saveScrollPositionInList();
          ContactListActivity.access$1002(ContactListActivity.this, 0);
          return;
        }
        ContactListActivity.access$1002(ContactListActivity.this, 2);
      }
    });
    this.m_listView.setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
      {
        ContactListActivity localContactListActivity = ContactListActivity.this;
        if ((paramAnonymousMotionEvent.getAction() == 2) || (paramAnonymousMotionEvent.getAction() == 0));
        for (boolean bool = true; ; bool = false)
        {
          ContactListActivity.access$1102(localContactListActivity, bool);
          return false;
        }
      }
    });
    if ((TangoApp.videomailSupported()) && (Build.VERSION.SDK_INT > 7))
      registerForContextMenu(this.m_listView);
  }

  public Dialog onCreateDialog(int paramInt)
  {
    Log.d("Tango.ContactsActivity", "onCreateDialog type = " + paramInt);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
    }
    ProgressDialog localProgressDialog = new ProgressDialog(getParent());
    localProgressDialog.setMessage(getString(2131296320));
    localProgressDialog.setIndeterminate(true);
    localProgressDialog.setCancelable(true);
    localProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        Log.d("Tango.ContactsActivity", "onCancel(DialogInterface) Cancel the progress dialog...");
        paramAnonymousDialogInterface.dismiss();
      }
    });
    return localProgressDialog;
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (this.m_query != null)
      return true;
    paramMenu.add(0, 4, 0, 2131296317).setIcon(2130837668);
    paramMenu.add(0, 5, 0, 2131296321).setIcon(2130837669);
    paramMenu.add(0, 6, 0, 2131296318).setIcon(2130837665);
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    Log.d("Tango.ContactsActivity", "onDestroy()");
    super.onDestroy();
    if (this.m_query != null)
    {
      TabActivityBase localTabActivityBase = TangoApp.getInstance().getTabsActivityInstance();
      if (localTabActivityBase != null)
        localTabActivityBase.setContactSearchActivity(null);
    }
    this.m_isDestroyed = true;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      Log.i("Tango.ContactsActivity", "ignore back key down");
      return false;
    }
    if (paramInt == 82)
    {
      Log.i("Tango.ContactsActivity", "open options menu");
      openOptionsMenu();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      if (this.m_query != null)
      {
        finish();
        return true;
      }
      Log.i("Tango.ContactsActivity", "ignore back key up");
      return false;
    }
    if (paramInt == 82)
      return true;
    return super.onKeyUp(paramInt, paramKeyEvent);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Log.d("Tango.ContactsActivity", "onNewIntent().");
    handleSearchIntent(paramIntent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 4:
      ContactThumbnailLoader.getInstance().clear();
      getParent().showDialog(0);
      this.m_hasRefreshProgressDlg = true;
      postRequestContactFilter();
      return true;
    case 5:
      onSearchRequested();
      return true;
    case 6:
    }
    showAddContactActivity();
    return true;
  }

  protected void onPause()
  {
    Log.v("Tango.ContactsActivity", "onPause()");
    super.onPause();
    ContactThumbnailLoader.getInstance().stop();
    if (this.m_query == null)
      saveScrollPositionInList();
  }

  protected void onResume()
  {
    Log.v("Tango.ContactsActivity", "onResume()");
    super.onResume();
    ContactThumbnailLoader.getInstance().start();
    if (this.m_query == null)
    {
      List localList = m_allContacts;
      if ((localList != null) && (localList != this.m_contacts))
      {
        Log.d("Tango.ContactsActivity", "onResume: Display new Tango contacts...");
        updateContacts();
      }
    }
    displayTangoAlerts(null);
    this.m_listView.requestFocus();
    this.m_listView.clearFocus();
  }

  public boolean onSearchRequested()
  {
    Log.v("Tango.ContactsActivity", "onSearchRequested()");
    return getParent().onSearchRequested();
  }

  void updateContacts()
  {
    Log.v("Tango.ContactsActivity", "updateContacts()");
    if (((this.m_groupSelection == ContactListAdapter.ContactListSelection.UNDEFINED) && (m_sourceType != SessionMessages.ContactsSource.ADDRESS_BOOK_AND_CONTACT_FILTER)) || ((this.m_groupSelection == ContactListAdapter.ContactListSelection.ALL) && (m_sourceType == SessionMessages.ContactsSource.LOCAL_STORAGE_ONLY)))
    {
      if (!this.m_hasRefreshProgressDlg)
      {
        getParent().showDialog(0);
        this.m_hasRefreshProgressDlg = true;
        this.m_handler.sendEmptyMessageDelayed(1, 15000L);
      }
      return;
    }
    this.m_handler.removeMessages(1);
    doUpdateContacts();
  }

  private class DisplayContactListAdapter extends ContactListAdapter
  {
    protected final int m_textViewResourceId;

    public DisplayContactListAdapter(int paramContactOrderPair, ContactStore.ContactOrderPair paramContactListSelection, ContactListAdapter.ContactListSelection paramLayoutInflater, LayoutInflater arg5)
    {
      super(paramLayoutInflater, localLayoutInflater);
      this.m_textViewResourceId = paramContactOrderPair;
    }

    protected void fillContactView(View paramView, Utils.UIContact paramUIContact)
    {
      ViewHolder localViewHolder = (ViewHolder)paramView.getTag();
      localViewHolder.name.setText(paramUIContact.displayName());
      localViewHolder.image.setTag(Integer.valueOf(paramUIContact.hashCode()));
      Button localButton;
      if (paramUIContact.m_deviceContactId != -1L)
      {
        if (!paramUIContact.hasPhoto)
          break label170;
        Bitmap localBitmap = ContactThumbnailLoader.getInstance().getLoadedBitmap(paramUIContact);
        if (localBitmap != null)
          localViewHolder.image.setImageBitmap(localBitmap);
      }
      else
      {
        localButton = (Button)paramView.findViewById(2131361918);
        if (paramUIContact.m_accountId.length() <= 0)
          break label182;
        localButton.setCompoundDrawablesWithIntrinsicBounds(2130837718, 0, 0, 0);
        localViewHolder.name.setTypeface(Typeface.DEFAULT_BOLD);
      }
      while (true)
      {
        localButton.setFocusable(false);
        localButton.setFocusableInTouchMode(false);
        localButton.setTag(paramUIContact);
        localButton.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Utils.UIContact localUIContact = (Utils.UIContact)paramAnonymousView.getTag();
            Log.i("Tango.ContactsActivity", "button clicked for " + localUIContact.displayName());
            if (localUIContact.m_accountId.length() > 0)
            {
              CallHandler.getDefault().sendCallMessage(localUIContact.m_accountId, localUIContact.displayName(), localUIContact.m_deviceContactId, CallHandler.VideoMode.VIDEO_ON);
              return;
            }
            ContactListActivity.this.requestInvite2Tango(localUIContact);
          }
        });
        return;
        localViewHolder.image.setImageResource(2130837648);
        ContactThumbnailLoader.getInstance().addLoadTask(paramUIContact, localViewHolder.image);
        break;
        label170: localViewHolder.image.setImageResource(2130837648);
        break;
        label182: localButton.setCompoundDrawablesWithIntrinsicBounds(2130837734, 0, 0, 0);
        localViewHolder.name.setTypeface(Typeface.DEFAULT);
      }
    }

    protected View getContactView(Utils.UIContact paramUIContact)
    {
      View localView = this.m_inflater.inflate(this.m_textViewResourceId, null);
      ViewHolder localViewHolder = new ViewHolder();
      localViewHolder.image = ((ImageView)localView.findViewById(2131361915));
      localViewHolder.name = ((TextView)localView.findViewById(2131361916));
      localView.setTag(localViewHolder);
      return localView;
    }

    class ViewHolder
    {
      ImageView image;
      TextView name;

      ViewHolder()
      {
      }
    }
  }

  class Section
  {
    private int m_count = 0;
    private Character m_displayLetter;
    private int m_headerIndex = -1;
    private final Character m_idLetter;
    private int m_startIndex = -1;

    public Section(Character arg2)
    {
      Object localObject;
      this.m_idLetter = localObject;
      this.m_displayLetter = localObject;
    }

    public void copyDisplayContentFrom(Section paramSection)
    {
      Log.d("Tango.ContactsActivity", "Section::copyDisplayContentFrom(): " + this.m_idLetter + " <= " + paramSection.m_idLetter);
      this.m_displayLetter = paramSection.m_displayLetter;
      this.m_count = paramSection.m_count;
      this.m_startIndex = paramSection.m_startIndex;
      this.m_headerIndex = paramSection.m_headerIndex;
    }

    public int getCount()
    {
      return this.m_count;
    }

    public int getHeaderIndex()
    {
      return this.m_headerIndex;
    }

    public char getIdLetter()
    {
      return this.m_idLetter.charValue();
    }

    public int getStartIndex()
    {
      return this.m_startIndex;
    }

    public void increment()
    {
      this.m_count = (1 + this.m_count);
    }

    public void reset()
    {
      this.m_displayLetter = this.m_idLetter;
      this.m_count = 0;
      this.m_startIndex = -1;
      this.m_headerIndex = -1;
    }

    public void setHeaderIndex(int paramInt)
    {
      this.m_headerIndex = paramInt;
    }

    public void setStartIndex(int paramInt)
    {
      this.m_startIndex = paramInt;
    }

    public String toDebugString()
    {
      return "{ " + this.m_idLetter + ": display=" + this.m_displayLetter + ", count=" + this.m_count + ", startIndex=" + this.m_startIndex + ", headerIndex=" + this.m_headerIndex + " }";
    }

    public String toString()
    {
      return this.m_displayLetter.toString();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ContactListActivity
 * JD-Core Version:    0.6.2
 */