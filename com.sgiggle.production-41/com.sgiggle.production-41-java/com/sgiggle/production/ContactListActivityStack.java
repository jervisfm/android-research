package com.sgiggle.production;

import android.app.Dialog;
import android.app.SearchManager;
import android.app.SearchManager.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import com.sgiggle.util.Log;
import java.util.Stack;

public class ContactListActivityStack extends ActivityStack
{
  private static final String TAG = "Tango.ContactListActivityStack";

  private void handleIntent(Intent paramIntent)
  {
    Log.d("Tango.ContactListActivityStack", "handleIntent");
    if ("android.intent.action.SEARCH".equals(paramIntent.getAction()))
      ((ContactListActivity)currentActivity()).handleSearchIntent(paramIntent);
  }

  public void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.ContactListActivityStack", "onCreat");
    super.onCreate(paramBundle);
    push("ContactListActivity", new Intent(this, ContactListActivity.class));
    handleIntent(getIntent());
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    return ((ContactListActivity)currentActivity()).onCreateDialog(paramInt);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Log.d("Tango.ContactListActivityStack", "onNewIntent " + paramIntent);
    setIntent(paramIntent);
    handleIntent(paramIntent);
  }

  public boolean onSearchRequested()
  {
    if (this.stack.size() > 1)
      return true;
    Log.v("Tango.ContactListActivityStack", "onSearchRequested()");
    final TabActivityBase localTabActivityBase = TangoApp.getInstance().getTabsActivityInstance();
    if (localTabActivityBase != null)
    {
      Log.d("Tango.ContactListActivityStack", "onSearchRequested: tabsUi=" + localTabActivityBase);
      localTabActivityBase.onSearchModeEntered(true);
      ((SearchManager)getSystemService("search")).setOnCancelListener(new SearchManager.OnCancelListener()
      {
        public void onCancel()
        {
          localTabActivityBase.onSearchModeEntered(false);
        }
      });
    }
    return super.onSearchRequested();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ContactListActivityStack
 * JD-Core Version:    0.6.2
 */