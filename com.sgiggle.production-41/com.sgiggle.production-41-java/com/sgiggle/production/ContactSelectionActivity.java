package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.contacts.ContactStore.ContactOrderPair.ContactOrder;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.MediaType;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class ContactSelectionActivity extends ActivityBase
  implements AdapterView.OnItemClickListener
{
  public static final int CONTACT_SELECTION_UNLIMITED = -1;
  private static final int DIALOG_REACHED_LIMIT = 0;
  public static final String EXTRA_FULL_SCREEN = "FullScreen";
  public static final boolean EXTRA_FULL_SCREEN_DEFAULT = false;
  public static final String EXTRA_LOCK_IN_PORTRAIT = "LockInPortrait";
  public static final boolean EXTRA_LOCK_IN_PORTRAIT_DEFAULT = false;
  public static final String EXTRA_SELECTED_CONTACTS = "SelectedContacts";
  public static final int REQUEST_CODE_SELECT_CONTACTS = 20;
  private static final int SEARCH_ID = 1;
  private static final String TAG = "Tango.ContactSelectionActivity";
  private static List<SessionMessages.Contact> s_latestContacts = null;
  private ContactArrayAdapter m_adapter;
  private CheckBox m_allButton;
  private View.OnClickListener m_cancelListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (ContactSelectionActivity.this.m_query != null)
      {
        ContactSelectionActivity.access$602(ContactSelectionActivity.this, null);
        ContactSelectionActivity.this.displayContacts(ContactSelectionActivity.this.m_contactItems);
        return;
      }
      ContactSelectionActivity.this.onContactsPicked(null);
    }
  };
  private List<ContactItem> m_contactItems = new ArrayList();
  private TextView m_emptyView;
  private TextView m_header;
  private ListView m_listView;
  private List<SessionMessages.Contact> m_localContacts = s_latestContacts;
  private Button m_okButton;
  private View.OnClickListener m_okListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = ContactSelectionActivity.this.m_contactItems.iterator();
      while (localIterator.hasNext())
      {
        ContactSelectionActivity.ContactItem localContactItem = (ContactSelectionActivity.ContactItem)localIterator.next();
        if (localContactItem.m_selected)
          localArrayList.add(localContactItem.m_contact);
      }
      ContactSelectionActivity.this.onContactsPicked(localArrayList);
    }
  };
  private String m_query;
  private View.OnClickListener m_selectAllListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (ContactSelectionActivity.this.m_selectedCount != ContactSelectionActivity.this.m_contactItems.size());
      for (boolean bool = true; ; bool = false)
      {
        Iterator localIterator = ContactSelectionActivity.this.m_contactItems.iterator();
        while (localIterator.hasNext())
        {
          ((ContactSelectionActivity.ContactItem)localIterator.next()).m_selected = bool;
          ContactSelectionActivity.this.m_adapter.notifyDataSetChanged();
        }
      }
      ContactSelectionActivity localContactSelectionActivity = ContactSelectionActivity.this;
      if (bool);
      for (int i = ContactSelectionActivity.this.m_contactItems.size(); ; i = 0)
      {
        ContactSelectionActivity.access$002(localContactSelectionActivity, i);
        ContactSelectionActivity.this.onCheckedItemChanged();
        return;
      }
    }
  };
  private ViewGroup m_selectAllPanel;
  private int m_selectLimit = -1;
  private int m_selectedCount = 0;
  private TextView m_titleView;

  private boolean checkSelectionLimitReached(ContactItem paramContactItem)
  {
    if ((this.m_selectLimit >= 0) && (this.m_selectedCount > this.m_selectLimit))
    {
      paramContactItem.m_selected = false;
      this.m_selectedCount -= 1;
      onSelectionLimitReached();
      return true;
    }
    return false;
  }

  private void displayContacts(List<ContactItem> paramList)
  {
    Log.d("Tango.ContactSelectionActivity", "displayContacts(): New list-size=" + paramList.size());
    int i;
    int j;
    if (this.m_query == null)
    {
      this.m_titleView.setText(getTitleString());
      this.m_emptyView.setText(getResources().getString(2131296339));
      this.m_selectAllPanel.setVisibility(0);
      i = 2131427329;
      this.m_adapter = new ContactArrayAdapter(this, 2130903085, paramList);
      this.m_listView.setAdapter(this.m_adapter);
      this.m_listView.setFastScrollEnabled(true);
      j = paramList.size();
      if (j <= 0)
        break label265;
      TextView localTextView2 = this.m_header;
      Resources localResources = getResources();
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Integer.valueOf(j);
      localTextView2.setText(localResources.getQuantityString(i, j, arrayOfObject2));
    }
    while (true)
    {
      if ((this.m_selectLimit < 0) || (j <= this.m_selectLimit))
        break label277;
      this.m_selectAllPanel.setVisibility(8);
      return;
      TextView localTextView1 = this.m_titleView;
      String str = getResources().getString(2131296341);
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = this.m_query;
      localTextView1.setText(String.format(str, arrayOfObject1));
      this.m_emptyView.setText(getResources().getString(2131296342));
      this.m_selectAllPanel.setVisibility(8);
      i = 2131427330;
      break;
      label265: this.m_header.setText("");
    }
    label277: this.m_selectAllPanel.setVisibility(0);
  }

  public static SessionMessages.MediaType getSupportedMediaType(Context paramContext)
  {
    if (Utils.isSmsIntentAvailable(paramContext))
    {
      Log.d("Tango.ContactSelectionActivity", "SMS supported, can ask for SMS and EMAIL contacts.");
      return SessionMessages.MediaType.SMS_EMAIL;
    }
    Log.d("Tango.ContactSelectionActivity", "SMS not supported, can ask for EMAIL contacts only.");
    return SessionMessages.MediaType.EMAIL;
  }

  private void handleSearch(String paramString)
  {
    Log.d("Tango.ContactSelectionActivity", "handleSearch(query=" + paramString + ")");
    ArrayList localArrayList = new ArrayList();
    String str = paramString.trim().toUpperCase();
    Iterator localIterator = this.m_contactItems.iterator();
    while (localIterator.hasNext())
    {
      ContactItem localContactItem = (ContactItem)localIterator.next();
      if ((Utils.startWithUpperCase(localContactItem.m_firstName, str)) || (Utils.startWithUpperCase(localContactItem.m_lastName, str)) || (Utils.startWithUpperCase(localContactItem.displayName(), str)))
        localArrayList.add(localContactItem);
    }
    displayContacts(localArrayList);
  }

  private void onCheckedItemChanged()
  {
    this.m_okButton.setText(getOkButtonString(this.m_selectedCount));
    Button localButton = this.m_okButton;
    boolean bool1;
    CheckBox localCheckBox;
    if (this.m_selectedCount > 0)
    {
      bool1 = true;
      localButton.setEnabled(bool1);
      localCheckBox = this.m_allButton;
      if (this.m_selectedCount != this.m_contactItems.size())
        break label70;
    }
    label70: for (boolean bool2 = true; ; bool2 = false)
    {
      localCheckBox.setChecked(bool2);
      return;
      bool1 = false;
      break;
    }
  }

  private void onContactsPicked(ArrayList<SessionMessages.Contact> paramArrayList)
  {
    if ((this.m_selectedCount == 0) || (paramArrayList == null))
      setResult(0);
    while (true)
    {
      finish();
      return;
      setResult(-1, new Intent().putExtra("SelectedContacts", paramArrayList));
    }
  }

  public static void setLatestContactList(List<SessionMessages.Contact> paramList)
  {
    s_latestContacts = paramList;
  }

  private void setupView()
  {
    setContentView(2130903086);
    this.m_allButton = ((CheckBox)findViewById(2131361974));
    this.m_allButton.setOnClickListener(this.m_selectAllListener);
    this.m_okButton = ((Button)findViewById(2131361976));
    this.m_okButton.setOnClickListener(this.m_okListener);
    ((Button)findViewById(2131361977)).setOnClickListener(this.m_cancelListener);
    setDefaultKeyMode(3);
    this.m_listView = ((ListView)findViewById(2131361854));
    this.m_emptyView = ((TextView)findViewById(2131361855));
    this.m_listView.setEmptyView(this.m_emptyView);
    this.m_titleView = ((TextView)findViewById(2131361841));
    this.m_selectAllPanel = ((ViewGroup)findViewById(2131361973));
    this.m_header = new TextView(this);
    this.m_listView.addHeaderView(this.m_header, null, false);
    this.m_listView.setOnItemClickListener(this);
    this.m_okButton.setText(getOkButtonString(0));
  }

  public void fillList()
  {
    if (!this.m_contactItems.isEmpty())
    {
      Log.d("Tango.ContactSelectionActivity", "setContacts ignored, since the list has already been set.");
      return;
    }
    Iterator localIterator = this.m_localContacts.iterator();
    while (localIterator.hasNext())
    {
      SessionMessages.Contact localContact = (SessionMessages.Contact)localIterator.next();
      this.m_contactItems.add(new ContactItem(localContact));
    }
    ContactStore.ContactOrderPair.ContactOrder localContactOrder = ContactStore.ContactOrderPair.getFromPhone(this).getSortOrder();
    Collections.sort(this.m_contactItems, new Utils.ContactComparator(localContactOrder));
    displayContacts(this.m_contactItems);
    onCheckedItemChanged();
  }

  protected abstract String getOkButtonString(int paramInt);

  protected int getSelectionLimit()
  {
    return -1;
  }

  protected String getSelectionLimitReachedString()
  {
    return "";
  }

  protected String getTitleString()
  {
    return getResources().getString(2131296297);
  }

  public void onBackPressed()
  {
    Log.v("Tango.ContactSelectionActivity", "onBackPressed()");
    if (this.m_query != null)
    {
      this.m_query = null;
      displayContacts(this.m_contactItems);
      return;
    }
    onContactsPicked(null);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Log.d("Tango.ContactSelectionActivity", "onCreate()");
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      if (localBundle.getBoolean("FullScreen", false))
      {
        getWindow().addFlags(1024);
        getWindow().clearFlags(2048);
      }
      if (localBundle.getBoolean("LockInPortrait", false))
        setRequestedOrientation(1);
    }
    setupView();
    if (this.m_localContacts == null)
      this.m_localContacts = new ArrayList();
    this.m_contactItems = new ArrayList();
    this.m_selectLimit = getSelectionLimit();
    fillList();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
    }
    AlertDialog localAlertDialog = new AlertDialog.Builder(this).create();
    localAlertDialog.setMessage(getSelectionLimitReachedString());
    localAlertDialog.setButton(getString(2131296287), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    });
    return localAlertDialog;
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (this.m_query != null)
      return true;
    paramMenu.add(0, 1, 0, 2131296321).setIcon(2130837669);
    return true;
  }

  protected void onDestroy()
  {
    s_latestContacts = null;
    super.onDestroy();
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ContactItem localContactItem = (ContactItem)((ListAdapter)paramAdapterView.getAdapter()).getItem(paramInt);
    boolean bool;
    int i;
    if (!localContactItem.m_selected)
    {
      bool = true;
      localContactItem.m_selected = bool;
      i = this.m_selectedCount;
      if (!localContactItem.m_selected)
        break label109;
    }
    label109: for (int j = 1; ; j = -1)
    {
      this.m_selectedCount = (i + j);
      if (!checkSelectionLimitReached(localContactItem))
      {
        onCheckedItemChanged();
        CheckBox localCheckBox = (CheckBox)paramView.findViewById(2131361972);
        if (localCheckBox != null)
          localCheckBox.setChecked(localContactItem.m_selected);
      }
      return;
      bool = false;
      break;
    }
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Log.d("Tango.ContactSelectionActivity", "onNewIntent()");
    if ("android.intent.action.SEARCH".equals(paramIntent.getAction()))
    {
      if (this.m_contactItems != null)
      {
        this.m_query = paramIntent.getStringExtra("query");
        handleSearch(this.m_query);
      }
      return;
    }
    super.onNewIntent(paramIntent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 1:
    }
    onSearchRequested();
    return true;
  }

  protected void onSelectionLimitReached()
  {
    showDialog(0);
  }

  private static class ContactArrayAdapter extends ArrayAdapter<ContactSelectionActivity.ContactItem>
    implements SectionIndexer
  {
    private HashMap<String, Integer> alphaIndexer;
    private final LayoutInflater m_Inflater;
    private List<ContactSelectionActivity.ContactItem> m_items;
    private final int m_textViewResourceId;
    private String[] sections;

    public ContactArrayAdapter(Context paramContext, int paramInt, List<ContactSelectionActivity.ContactItem> paramList)
    {
      super(paramInt, paramList);
      ContactStore.ContactOrderPair.ContactOrder localContactOrder = ContactStore.ContactOrderPair.getFromPhone(paramContext).getSortOrder();
      this.m_textViewResourceId = paramInt;
      this.m_Inflater = LayoutInflater.from(paramContext);
      this.m_items = paramList;
      this.alphaIndexer = new HashMap();
      for (int i = 0; i < this.m_items.size(); i++)
      {
        String str = ((ContactSelectionActivity.ContactItem)this.m_items.get(i)).compareName(localContactOrder).substring(0, 1).toUpperCase();
        if (!this.alphaIndexer.containsKey(str))
          this.alphaIndexer.put(str, Integer.valueOf(i));
      }
      ArrayList localArrayList = new ArrayList(this.alphaIndexer.keySet());
      Collections.sort(localArrayList);
      this.sections = new String[localArrayList.size()];
      localArrayList.toArray(this.sections);
    }

    public int getCount()
    {
      return this.m_items.size();
    }

    public int getPositionForSection(int paramInt)
    {
      return ((Integer)this.alphaIndexer.get(this.sections[paramInt])).intValue();
    }

    public int getSectionForPosition(int paramInt)
    {
      return 1;
    }

    public Object[] getSections()
    {
      return this.sections;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView1;
      ViewHolder localViewHolder1;
      ContactSelectionActivity.ContactItem localContactItem;
      if (paramView == null)
      {
        View localView2 = this.m_Inflater.inflate(this.m_textViewResourceId, null);
        ViewHolder localViewHolder2 = new ViewHolder();
        localViewHolder2.name = ((TextView)localView2.findViewById(2131361806));
        localViewHolder2.subLabel = ((TextView)localView2.findViewById(2131361971));
        localViewHolder2.checkBox = ((CheckBox)localView2.findViewById(2131361972));
        localView2.setTag(localViewHolder2);
        localView1 = localView2;
        localViewHolder1 = localViewHolder2;
        localContactItem = (ContactSelectionActivity.ContactItem)this.m_items.get(paramInt);
        localViewHolder1.name.setText(localContactItem.displayName());
        String str = localContactItem.m_contact.getAccountid();
        if ((str == null) || (str.length() <= 0))
          break label219;
        localViewHolder1.name.setTypeface(Typeface.DEFAULT_BOLD);
      }
      while (true)
      {
        localViewHolder1.subLabel.setText(localContactItem.m_subLabel);
        localViewHolder1.checkBox.setChecked(localContactItem.m_selected);
        localViewHolder1.checkBox.setTag(localContactItem);
        localViewHolder1.checkBox.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            ContactSelectionActivity.ContactItem localContactItem = (ContactSelectionActivity.ContactItem)paramAnonymousView.getTag();
            ContactSelectionActivity localContactSelectionActivity;
            boolean bool;
            if (localContactItem != null)
            {
              localContactSelectionActivity = (ContactSelectionActivity)ContactSelectionActivity.ContactArrayAdapter.this.getContext();
              if (localContactItem.m_selected)
                break label69;
              bool = true;
              localContactItem.m_selected = bool;
              if (!localContactItem.m_selected)
                break label75;
            }
            label69: label75: for (int i = 1; ; i = -1)
            {
              ContactSelectionActivity.access$012(localContactSelectionActivity, i);
              if (localContactSelectionActivity.checkSelectionLimitReached(localContactItem))
                break label81;
              localContactSelectionActivity.onCheckedItemChanged();
              return;
              bool = false;
              break;
            }
            label81: ((CheckBox)paramAnonymousView).setChecked(false);
          }
        });
        return localView1;
        localViewHolder1 = (ViewHolder)paramView.getTag();
        localView1 = paramView;
        break;
        label219: localViewHolder1.name.setTypeface(Typeface.DEFAULT);
      }
    }

    static class ViewHolder
    {
      CheckBox checkBox;
      TextView name;
      TextView subLabel;
    }
  }

  private class ContactItem extends Utils.UIContact
  {
    public SessionMessages.Contact m_contact;
    public boolean m_selected = false;
    public String m_subLabel;

    ContactItem(SessionMessages.Contact arg2)
    {
      super(localObject.getFirstname(), localObject.getMiddlename(), localObject.getLastname(), localObject.getNamesuffix(), localObject.getDisplayname());
      this.m_contact = localObject;
      this.m_email = localObject.getEmail();
      if (localObject.hasEmail())
        this.m_subLabel = localObject.getEmail();
      while (!localObject.hasPhoneNumber())
        return;
      this.m_subLabel = localObject.getPhoneNumber().getSubscriberNumber();
    }

    public String displayName()
    {
      if (TextUtils.isEmpty(this.m_displayName))
        return this.m_subLabel;
      return this.m_displayName;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ContactSelectionActivity
 * JD-Core Version:    0.6.2
 */