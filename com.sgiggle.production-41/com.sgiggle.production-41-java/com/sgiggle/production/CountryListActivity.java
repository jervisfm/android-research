package com.sgiggle.production;

import android.app.Activity;
import android.app.SearchManager;
import android.app.SearchManager.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.CountryCode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CountryListActivity extends Activity
  implements AdapterView.OnItemClickListener
{
  private static final String TAG = "Tango.CountryListActivity";
  private static List<CountryData> s_countryList;
  private static CountryData s_result;
  private static String s_selectedCountryId;
  private String m_query;
  private TextView m_searchTitleView;

  public static ArrayList<CountryData> buildSelectableCountriesFromList(List<SessionMessages.CountryCode> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      SessionMessages.CountryCode localCountryCode = (SessionMessages.CountryCode)localIterator.next();
      String str1 = localCountryCode.getCountryid();
      String str2 = localCountryCode.getCountrycodenumber();
      String str3 = localCountryCode.getCountryname();
      String str4 = localCountryCode.getCountryisocc();
      if ((str1 != null) && (str2 != null) && (str3 != null) && (str4 != null))
        localArrayList.add(new CountryData(str1, str2, str3, str4));
    }
    return localArrayList;
  }

  private void displayCountries(List<CountryData> paramList)
  {
    Iterator localIterator = paramList.iterator();
    int i = -1;
    while (localIterator.hasNext())
    {
      CountryData localCountryData = (CountryData)localIterator.next();
      i++;
      if (localCountryData.m_cid.equals(s_selectedCountryId))
        Log.v("Tango.CountryListActivity", "selectedCountry - Index = " + i);
    }
    for (int j = i; ; j = 0)
    {
      ListView localListView = (ListView)findViewById(2131361854);
      TextView localTextView = (TextView)findViewById(2131361855);
      localTextView.setText(getResources().getString(2131296343));
      localListView.setEmptyView(localTextView);
      localListView.setAdapter(new ArrayAdapter(this, 2130903074, paramList));
      localListView.setChoiceMode(1);
      if (j >= 0)
      {
        localListView.setItemChecked(j, true);
        localListView.setSelection(j);
      }
      localListView.setTextFilterEnabled(true);
      localListView.setOnItemClickListener(this);
      return;
    }
  }

  private void handleSearch(String paramString)
  {
    Log.d("Tango.CountryListActivity", "handleSearch: [" + paramString + "]");
    String str = paramString.trim().toUpperCase();
    Iterator localIterator = s_countryList.iterator();
    ArrayList localArrayList = new ArrayList();
    this.m_searchTitleView.setVisibility(0);
    this.m_searchTitleView.setText(String.format(getResources().getString(2131296341), new Object[] { paramString }));
    while (localIterator.hasNext())
    {
      CountryData localCountryData = (CountryData)localIterator.next();
      if (Utils.startWithUpperCase(localCountryData.m_name, str))
        localArrayList.add(localCountryData);
    }
    displayCountries(localArrayList);
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.CountryListActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903075);
    this.m_searchTitleView = ((TextView)findViewById(2131361904));
    Intent localIntent = getIntent();
    if ("android.intent.action.SEARCH".equals(localIntent.getAction()))
    {
      this.m_query = localIntent.getStringExtra("query");
      handleSearch(this.m_query);
      return;
    }
    setDefaultKeyMode(3);
    s_countryList = localIntent.getParcelableArrayListExtra("countries");
    s_selectedCountryId = localIntent.getStringExtra("selectedCountryId");
    displayCountries(s_countryList);
  }

  protected void onDestroy()
  {
    Log.d("Tango.CountryListActivity", "onDestroy()");
    if (this.m_query == null)
    {
      s_selectedCountryId = null;
      s_countryList = null;
      s_result = null;
    }
    super.onDestroy();
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    CountryData localCountryData = (CountryData)((ListAdapter)paramAdapterView.getAdapter()).getItem(paramInt);
    String str1 = localCountryData.m_cid;
    String str2 = localCountryData.m_code;
    String str3 = localCountryData.m_name;
    Log.v("Tango.CountryListActivity", "onItemClick: position = " + paramInt + ", Id = " + str1 + ", Country = " + str2 + " (" + str3 + ")");
    if (this.m_query == null)
    {
      Intent localIntent = new Intent();
      localIntent.putExtra("selectedCountry", localCountryData);
      setResult(-1, localIntent);
    }
    while (true)
    {
      finish();
      return;
      s_result = localCountryData;
    }
  }

  protected void onResume()
  {
    super.onResume();
    if ((this.m_query == null) && (s_result != null))
    {
      Intent localIntent = new Intent();
      localIntent.putExtra("selectedCountry", s_result);
      setResult(-1, localIntent);
      s_result = null;
      finish();
    }
  }

  public boolean onSearchRequested()
  {
    Log.v("Tango.CountryListActivity", "onSearchRequested()");
    if (this.m_query != null)
      return true;
    final TabActivityBase localTabActivityBase = (TabActivityBase)getParent();
    if (localTabActivityBase != null)
    {
      Log.d("Tango.CountryListActivity", "onSearchRequested: tabsUi=" + localTabActivityBase);
      localTabActivityBase.onSearchModeEntered(true);
      ((SearchManager)getSystemService("search")).setOnDismissListener(new SearchManager.OnDismissListener()
      {
        public void onDismiss()
        {
          localTabActivityBase.onSearchModeEntered(false);
        }
      });
    }
    return super.onSearchRequested();
  }

  public static class CountryData
    implements Parcelable
  {
    public static final Parcelable.Creator<CountryData> CREATOR = new Parcelable.Creator()
    {
      public CountryListActivity.CountryData createFromParcel(Parcel paramAnonymousParcel)
      {
        return new CountryListActivity.CountryData(paramAnonymousParcel, null);
      }

      public CountryListActivity.CountryData[] newArray(int paramAnonymousInt)
      {
        return new CountryListActivity.CountryData[paramAnonymousInt];
      }
    };
    public String m_cid;
    public String m_code;
    public String m_isocc;
    public String m_name;

    private CountryData(Parcel paramParcel)
    {
      String[] arrayOfString = new String[4];
      paramParcel.readStringArray(arrayOfString);
      this.m_cid = arrayOfString[0];
      this.m_code = arrayOfString[1];
      this.m_name = arrayOfString[2];
      this.m_isocc = arrayOfString[3];
    }

    CountryData(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      this.m_cid = paramString1;
      this.m_code = paramString2;
      this.m_name = paramString3;
      this.m_isocc = paramString4;
    }

    public int describeContents()
    {
      return 0;
    }

    public String toString()
    {
      Object[] arrayOfObject = new Object[2];
      if (this.m_code.startsWith("+"));
      for (String str = this.m_code; ; str = "+" + this.m_code)
      {
        arrayOfObject[0] = str;
        arrayOfObject[1] = this.m_name;
        return String.format("%5s  %s", arrayOfObject);
      }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      String[] arrayOfString = new String[4];
      arrayOfString[0] = this.m_cid;
      arrayOfString[1] = this.m_code;
      arrayOfString[2] = this.m_name;
      arrayOfString[3] = this.m_isocc;
      paramParcel.writeStringArray(arrayOfString);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.CountryListActivity
 * JD-Core Version:    0.6.2
 */