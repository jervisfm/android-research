package com.sgiggle.production;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.sgiggle.iphelper.IpHelper;
import com.sgiggle.media_engine.MediaEngineMessage.DisplaySMSSentEvent;
import com.sgiggle.media_engine.MediaEngineMessage.EmailValidationRequiredEvent;
import com.sgiggle.media_engine.MediaEngineMessage.EndStateNoChangeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.PushValidationRequiredEvent;
import com.sgiggle.media_engine.MediaEngineMessage.SMSValidationRequiredEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ValidationUserInputedCodeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ValidationUserNoCodeMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.BoolPayload;
import com.sgiggle.xmpp.SessionMessages.EmailVerificationPayload;
import com.sgiggle.xmpp.SessionMessages.SMSVerificationPayload;
import com.sgiggle.xmpp.SessionMessages.StringPayload;

public class EmailVerificationActivity extends ActivityBase
  implements View.OnClickListener, View.OnTouchListener
{
  private static final String TAG = "Tango.EmailVerificationActivity";
  private TextView m_WhatToDoLabel;
  private TextView m_notReceiveLabel;
  private TextView m_sentLabel;
  private Button m_submitButton;
  private TextView m_switchModeLink;
  private TextView m_userInfoLabel;
  private EditText m_verificationCodeEditText;
  private ViewMode m_viewMode = ViewMode.VERIFICATION_DEVICE_BY_EMAIL;

  protected void broadCastNoChange()
  {
    MediaEngineMessage.EndStateNoChangeMessage localEndStateNoChangeMessage = new MediaEngineMessage.EndStateNoChangeMessage();
    MessageRouter.getInstance().postMessage("jingle", localEndStateNoChangeMessage);
  }

  public void handleNewMessage(Message paramMessage)
  {
    Log.d("Tango.EmailVerificationActivity", "handleNewMessage(): Message = " + paramMessage);
    if (paramMessage == null)
    {
      TangoApp.getInstance().onActivityResumeAfterKilled(this);
      return;
    }
    switch (paramMessage.getType())
    {
    default:
      super.handleNewMessage(paramMessage);
      return;
    case 35052:
      SessionMessages.EmailVerificationPayload localEmailVerificationPayload = (SessionMessages.EmailVerificationPayload)((MediaEngineMessage.EmailValidationRequiredEvent)paramMessage).payload();
      boolean bool2 = localEmailVerificationPayload.getIsPhone();
      String str3 = localEmailVerificationPayload.getEmail();
      if (!TextUtils.isEmpty(localEmailVerificationPayload.getNormalizedNumber()));
      for (int i = 1; ; i = 0)
      {
        this.m_viewMode = ViewMode.VERIFICATION_DEVICE_BY_EMAIL;
        this.m_userInfoLabel.setVisibility(0);
        this.m_verificationCodeEditText.setVisibility(0);
        this.m_submitButton.setText(2131296638);
        this.m_sentLabel.setText(2131296628);
        this.m_userInfoLabel.setText(str3);
        this.m_WhatToDoLabel.setText(2131296629);
        this.m_notReceiveLabel.setText(2131296634);
        if ((!bool2) && ((IpHelper.isSmartPhone()) || (i == 0)))
          break;
        this.m_switchModeLink.setText(2131296635);
        return;
      }
      this.m_switchModeLink.setText(2131296639);
      return;
    case 35054:
      boolean bool1 = ((SessionMessages.BoolPayload)((MediaEngineMessage.PushValidationRequiredEvent)paramMessage).payload()).getValue();
      this.m_viewMode = ViewMode.VERIFICATION_DEVICE_BY_PUSH;
      this.m_userInfoLabel.setVisibility(8);
      this.m_userInfoLabel.setText(null);
      this.m_verificationCodeEditText.setVisibility(0);
      this.m_submitButton.setText(2131296638);
      this.m_WhatToDoLabel.setText(2131296632);
      this.m_notReceiveLabel.setText(2131296636);
      if (bool1)
      {
        this.m_sentLabel.setText(2131296630);
        this.m_switchModeLink.setText(2131296635);
        return;
      }
      this.m_sentLabel.setText(2131296631);
      this.m_switchModeLink.setText(2131296637);
      return;
    case 35056:
      String str2 = ((SessionMessages.SMSVerificationPayload)((MediaEngineMessage.SMSValidationRequiredEvent)paramMessage).payload()).getNormalizedNumber();
      this.m_viewMode = ViewMode.VERIFICATION_DEVICE_BY_SMS;
      this.m_userInfoLabel.setVisibility(0);
      this.m_verificationCodeEditText.setVisibility(0);
      this.m_submitButton.setText(2131296638);
      this.m_sentLabel.setText(2131296628);
      this.m_userInfoLabel.setText(str2);
      this.m_WhatToDoLabel.setText(2131296629);
      this.m_notReceiveLabel.setText(2131296634);
      this.m_switchModeLink.setText(2131296639);
      return;
    case 35119:
    }
    String str1 = ((SessionMessages.StringPayload)((MediaEngineMessage.DisplaySMSSentEvent)paramMessage).payload()).getText();
    this.m_viewMode = ViewMode.VERIFICATION_DEVICE_SMS_SENT;
    this.m_userInfoLabel.setVisibility(0);
    this.m_verificationCodeEditText.setVisibility(8);
    this.m_submitButton.setText(2131296287);
    this.m_sentLabel.setText(2131296641);
    this.m_userInfoLabel.setText(str1);
    this.m_WhatToDoLabel.setText(2131296642);
    this.m_notReceiveLabel.setText(2131296640);
    this.m_switchModeLink.setText(2131296639);
  }

  public void onBackPressed()
  {
    broadCastNoChange();
    super.onBackPressed();
  }

  public void onClick(View paramView)
  {
    int i = paramView.getId();
    Log.d("Tango.EmailVerificationActivity", "onClick(View " + paramView + ", id " + i + ")...");
    switch (i)
    {
    default:
      Log.w("Tango.EmailVerificationActivity", "onClick: unexpected click: View " + paramView + ", id " + i);
    case 2131361955:
    }
    do
    {
      return;
      switch (2.$SwitchMap$com$sgiggle$production$EmailVerificationActivity$ViewMode[this.m_viewMode.ordinal()])
      {
      default:
        Log.w("Tango.EmailVerificationActivity", "onClick: unexpected mode: View " + paramView + ", id " + i);
        return;
      case 1:
      case 2:
      case 3:
      case 4:
      }
    }
    while (this.m_verificationCodeEditText.getText().toString().length() == 0);
    MediaEngineMessage.ValidationUserInputedCodeMessage localValidationUserInputedCodeMessage = new MediaEngineMessage.ValidationUserInputedCodeMessage(this.m_verificationCodeEditText.getText().toString());
    MessageRouter.getInstance().postMessage("jingle", localValidationUserInputedCodeMessage);
    return;
    broadCastNoChange();
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.EmailVerificationActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903076);
    this.m_sentLabel = ((TextView)findViewById(2131361950));
    this.m_userInfoLabel = ((TextView)findViewById(2131361951));
    this.m_WhatToDoLabel = ((TextView)findViewById(2131361952));
    this.m_verificationCodeEditText = ((EditText)findViewById(2131361954));
    this.m_submitButton = ((Button)findViewById(2131361955));
    this.m_submitButton.setOnClickListener(this);
    this.m_notReceiveLabel = ((TextView)findViewById(2131361958));
    this.m_switchModeLink = ((TextView)findViewById(2131361957));
    this.m_switchModeLink.setMovementMethod(LinkMovementMethod.getInstance());
    this.m_switchModeLink.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        EmailVerificationActivity.this.onSwitchModeClicked();
      }
    });
    TangoApp.getInstance().setEmailVerificationActivityInstance(this);
    handleNewMessage(getFirstMessage());
  }

  protected void onDestroy()
  {
    Log.v("Tango.EmailVerificationActivity", "onDestroy()");
    super.onDestroy();
    TangoApp.getInstance().setEmailVerificationActivityInstance(null);
  }

  protected void onPause()
  {
    Log.v("Tango.EmailVerificationActivity", "onPause()");
    super.onPause();
    if (isFinishing())
      TangoApp.getInstance().setEmailVerificationActivityInstance(null);
  }

  protected void onSwitchModeClicked()
  {
    MediaEngineMessage.ValidationUserNoCodeMessage localValidationUserNoCodeMessage = new MediaEngineMessage.ValidationUserNoCodeMessage();
    MessageRouter.getInstance().postMessage("jingle", localValidationUserNoCodeMessage);
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    return false;
  }

  private static enum ViewMode
  {
    static
    {
      ViewMode[] arrayOfViewMode = new ViewMode[4];
      arrayOfViewMode[0] = VERIFICATION_DEVICE_BY_EMAIL;
      arrayOfViewMode[1] = VERIFICATION_DEVICE_BY_PUSH;
      arrayOfViewMode[2] = VERIFICATION_DEVICE_BY_SMS;
      arrayOfViewMode[3] = VERIFICATION_DEVICE_SMS_SENT;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.EmailVerificationActivity
 * JD-Core Version:    0.6.2
 */