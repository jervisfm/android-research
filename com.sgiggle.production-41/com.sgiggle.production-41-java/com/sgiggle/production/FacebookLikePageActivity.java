package com.sgiggle.production;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.CancelFacebookLikeMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;

public class FacebookLikePageActivity extends ActivityBase
{
  private static final String TAG = "Tango.FacebookLikePageActivity";
  private ViewGroup m_progressView;
  private WebView m_webView;

  public void onBackPressed()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelFacebookLikeMessage());
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.FacebookLikePageActivity", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    setContentView(2130903128);
    this.m_progressView = ((ViewGroup)findViewById(2131361988));
    this.m_webView = ((WebView)findViewById(2131362116));
    ((TextView)findViewById(2131361841)).setText(getResources().getString(2131296483));
    this.m_webView.getSettings().setJavaScriptEnabled(true);
    this.m_webView.setWebViewClient(new MyWebViewClient(null));
    this.m_webView.loadUrl("http://www.facebook.com/TangoMe");
  }

  protected void onDestroy()
  {
    Log.v("Tango.FacebookLikePageActivity", "onDestroy()");
    super.onDestroy();
  }

  private final class MyWebViewClient extends WebViewClient
  {
    private MyWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      Log.d("Tango.FacebookLikePageActivity", "Finished loading URL: " + paramString);
      FacebookLikePageActivity.this.m_webView.setVisibility(0);
      FacebookLikePageActivity.this.m_progressView.setVisibility(8);
      FacebookLikePageActivity.this.m_webView.requestFocus(130);
    }

    public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
    {
      FacebookLikePageActivity localFacebookLikePageActivity = FacebookLikePageActivity.this;
      Log.d("Tango.FacebookLikePageActivity", "onReceivedError: " + paramString1);
      Toast.makeText(localFacebookLikePageActivity, paramString1, 0).show();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.FacebookLikePageActivity
 * JD-Core Version:    0.6.2
 */