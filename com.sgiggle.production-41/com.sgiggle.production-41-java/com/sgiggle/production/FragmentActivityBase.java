package com.sgiggle.production;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.sgiggle.messaging.Message;
import com.sgiggle.util.Log;

public abstract class FragmentActivityBase extends FragmentActivity
{
  private static final boolean DBG = true;
  private static final String TAG = "Tango.FragmentActivityBase";
  private static final boolean VDBG = true;
  protected Message m_firstMessage;

  public FragmentActivityBase()
  {
    try
    {
      TangoApp.ensureInitialized();
      Log.v("Tango.FragmentActivityBase", "FragmentActivityBase()");
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      while (true)
        Log.e("Tango.FragmentActivityBase", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  protected boolean finishIfResumedAfterKilled()
  {
    return true;
  }

  protected Message getFirstMessage()
  {
    return this.m_firstMessage;
  }

  public void handleMessage(Message paramMessage)
  {
    Log.v("Tango.FragmentActivityBase", "handleNewMessage(message=" + paramMessage + ").");
  }

  void handleNewMessage(Message paramMessage)
  {
    handleMessage(paramMessage);
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.FragmentActivityBase", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    Object localObject = getLastCustomNonConfigurationInstance();
    if (localObject == null)
    {
      this.m_firstMessage = MessageManager.getDefault().getMessageFromIntent(getIntent());
      Log.d("Tango.FragmentActivityBase", "onCreate(): First time created: message=" + this.m_firstMessage);
    }
    while (true)
    {
      if ((this.m_firstMessage == null) && (shouldOnCreateIntentHaveMessage()) && (finishIfResumedAfterKilled()))
      {
        Log.d("Tango.FragmentActivityBase", "onCreate: activity was likely resumed after being killed, notifying TangoApp.");
        TangoApp.getInstance().onActivityResumeAfterKilled(this);
      }
      return;
      this.m_firstMessage = ((ActivityConfiguration)localObject).firstMessage;
      Log.d("Tango.FragmentActivityBase", "onCreate(): Restore activity: message=" + this.m_firstMessage);
    }
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Message localMessage = MessageManager.getDefault().getMessageFromIntent(paramIntent);
    Log.d("Tango.FragmentActivityBase", "onNewIntent(): Message = " + localMessage);
    if (localMessage != null)
      handleNewMessage(localMessage);
  }

  protected void onResume()
  {
    Log.v("Tango.FragmentActivityBase", "onResume()");
    super.onResume();
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_FOREGROUND);
  }

  public Object onRetainCustomNonConfigurationInstance()
  {
    Log.v("Tango.FragmentActivityBase", "onRetainCustomNonConfigurationInstance(): message = " + this.m_firstMessage);
    ActivityConfiguration localActivityConfiguration = new ActivityConfiguration();
    localActivityConfiguration.firstMessage = this.m_firstMessage;
    return localActivityConfiguration;
  }

  protected void onUserLeaveHint()
  {
    Log.d("Tango.FragmentActivityBase", "onUserLeaveHint()");
    super.onUserLeaveHint();
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
  }

  protected boolean shouldOnCreateIntentHaveMessage()
  {
    return true;
  }

  protected static class ActivityConfiguration
  {
    protected Message firstMessage;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.FragmentActivityBase
 * JD-Core Version:    0.6.2
 */