package com.sgiggle.production;

import com.sgiggle.media_engine.MediaEngineMessage.CancelGameCatalogMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DismissStoreBadgeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayGameCatalogEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayGameDemoMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogPayload;

public class GameCatalogActivity extends GenericProductCatalogActivity
{
  private void dismissNewBadge()
  {
    MediaEngineMessage.DismissStoreBadgeMessage localDismissStoreBadgeMessage = new MediaEngineMessage.DismissStoreBadgeMessage("product.category.game");
    MessageRouter.getInstance().postMessage("jingle", localDismissStoreBadgeMessage);
  }

  protected String getErrorMessage()
  {
    return getString(2131296516);
  }

  void handleNewMessage(Message paramMessage)
  {
    if (paramMessage == null)
      return;
    switch (paramMessage.getType())
    {
    default:
    case 35300:
    }
    while (true)
    {
      super.handleNewMessage(paramMessage);
      return;
      SessionMessages.ProductCatalogPayload localProductCatalogPayload = (SessionMessages.ProductCatalogPayload)((MediaEngineMessage.DisplayGameCatalogEvent)paramMessage).payload();
      onNewProductCatalogPayload(localProductCatalogPayload);
      if (localProductCatalogPayload.getAllCached())
        dismissNewBadge();
    }
  }

  public void onBackPressed()
  {
    MediaEngineMessage.CancelGameCatalogMessage localCancelGameCatalogMessage = new MediaEngineMessage.CancelGameCatalogMessage();
    MessageRouter.getInstance().postMessage("jingle", localCancelGameCatalogMessage);
  }

  protected void showDemo(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DisplayGameDemoMessage(paramProductCatalogEntry));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.GameCatalogActivity
 * JD-Core Version:    0.6.2
 */