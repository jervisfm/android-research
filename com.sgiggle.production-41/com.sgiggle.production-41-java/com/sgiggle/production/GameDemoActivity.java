package com.sgiggle.production;

import android.os.Bundle;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.cafe.vgood.CafeView;
import com.sgiggle.media_engine.MediaEngineMessage.CancelGameDemoMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayGameDemoEvent;
import com.sgiggle.media_engine.MediaEngineMessage.VGoodDemoAnimationTrackingMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.widget.HorizontalListView;
import com.sgiggle.xmpp.SessionMessages.ProductDetailsPayload;

public class GameDemoActivity extends GenericProductDemoActivity
{
  private boolean m_gameStarted;

  protected int getBottomBubbleTextResId()
  {
    return 2131296676;
  }

  protected int getRootView()
  {
    return 2130903080;
  }

  protected int getUpperBubbleTextResId()
  {
    return 2131296677;
  }

  void handleNewMessage(Message paramMessage)
  {
    if (paramMessage == null)
      return;
    switch (paramMessage.getType())
    {
    default:
    case 35301:
    }
    while (true)
    {
      super.handleNewMessage(paramMessage);
      return;
      onNewProductDetailPayload((SessionMessages.ProductDetailsPayload)((MediaEngineMessage.DisplayGameDemoEvent)paramMessage).payload());
    }
  }

  boolean isCafeViewFullScreen()
  {
    return true;
  }

  void onAnimationFinished()
  {
    this.m_gameStarted = false;
    super.onAnimationFinished();
  }

  protected void onAnimationStarted()
  {
    this.m_gameStarted = true;
    super.onAnimationStarted();
  }

  public void onBackPressed()
  {
    CafeMgr.StopAllSurprises();
    if (!this.m_gameStarted)
    {
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelGameDemoMessage());
      super.onBackPressed();
    }
    this.m_gameStarted = false;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.m_cafeView.setDeliverTouchToCafe(true);
    this.m_selector.setCenterSingleItem(true);
  }

  protected void play(String paramString, long paramLong)
  {
    CafeMgr.SetGodMode(TangoApp.g_screenLoggerEnabled);
    CafeMgr.LoadSurprise(paramString, "Surprise.Cafe");
    CafeMgr.StartSurprise(0, paramString, "Surprise.Cafe", "Main", 0L, paramLong, true);
    MediaEngineMessage.VGoodDemoAnimationTrackingMessage localVGoodDemoAnimationTrackingMessage = new MediaEngineMessage.VGoodDemoAnimationTrackingMessage(paramLong, this.m_productMarketId);
    MessageRouter.getInstance().postMessage("jingle", localVGoodDemoAnimationTrackingMessage);
    onAnimationStarted();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.GameDemoActivity
 * JD-Core Version:    0.6.2
 */