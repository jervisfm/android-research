package com.sgiggle.production;

import android.os.Bundle;
import android.view.Window;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.cafe.vgood.CafeView;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayGameInCallEvent;
import com.sgiggle.media_engine.MediaEngineMessage.GameModeOffMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload;
import com.sgiggle.xmpp.SessionMessages.VGoodBundle;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic;
import java.util.List;

public class GameInCallActivity extends ActivityBase
{
  private static final String TAG = "Tango.GameInCallActivity";
  private static GameInCallActivity s_instance;
  private CafeView m_cafeView;
  private long m_gameId;
  private String m_gamePath;
  private boolean m_resumed;

  private static void clearRunningInstance(GameInCallActivity paramGameInCallActivity)
  {
    if (s_instance == paramGameInCallActivity)
      s_instance = null;
  }

  public static GameInCallActivity getRunningInstance()
  {
    return s_instance;
  }

  private void handleGame(SessionMessages.VGoodBundle paramVGoodBundle)
  {
    this.m_gamePath = paramVGoodBundle.getCinematic().getAssetPath();
    this.m_gameId = paramVGoodBundle.getCinematic().getAssetId();
    if (this.m_resumed)
      playGame(this.m_gameId, this.m_gamePath);
  }

  private void playGame(long paramLong, String paramString)
  {
    CafeMgr.LoadSurprise(this.m_gamePath, "Surprise.Cafe");
    CafeMgr.StartSurprise(0, paramString, "Surprise.Cafe", "Main", 0L, paramLong, false);
  }

  private static void setRunningInstance(GameInCallActivity paramGameInCallActivity)
  {
    s_instance = paramGameInCallActivity;
  }

  void handleNewMessage(Message paramMessage)
  {
    if (paramMessage == null);
    List localList;
    do
    {
      return;
      switch (paramMessage.getType())
      {
      default:
        return;
      case 35194:
        onBackPressed();
        return;
      case 35302:
      }
      localList = ((SessionMessages.MediaSessionPayload)((MediaEngineMessage.DisplayGameInCallEvent)paramMessage).payload()).getVgoodBundleList();
    }
    while (localList.size() != 1);
    handleGame((SessionMessages.VGoodBundle)localList.get(0));
  }

  public void onBackPressed()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.GameModeOffMessage(false));
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().addFlags(4194304);
    getWindow().addFlags(524288);
    setContentView(2130903081);
    this.m_cafeView = ((CafeView)findViewById(2131361830));
    this.m_cafeView.setZOrderOnTop(true);
    this.m_cafeView.setDeliverTouchToCafe(true);
    handleNewMessage(getFirstMessage());
    TangoApp.getInstance().enableKeyguard();
  }

  protected void onDestroy()
  {
    super.onDestroy();
    clearRunningInstance(this);
  }

  protected void onPause()
  {
    super.onPause();
    this.m_cafeView.onPause();
    CafeMgr.StopAllSurprises();
    CafeMgr.Pause();
    CafeMgr.FreeEngine();
    this.m_resumed = false;
  }

  protected void onResume()
  {
    super.onResume();
    CafeMgr.InitEngine(this);
    CafeMgr.SetCallbacks();
    CafeMgr.SetGodMode(TangoApp.g_screenLoggerEnabled);
    if (this.m_gamePath != null)
      playGame(this.m_gameId, this.m_gamePath);
    this.m_cafeView.onResume();
    CafeMgr.Resume();
    setRunningInstance(this);
    this.m_resumed = true;
  }

  protected void onUserLeaveHint()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.GameModeOffMessage(true));
    super.onUserLeaveHint();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.GameInCallActivity
 * JD-Core Version:    0.6.2
 */