package com.sgiggle.production;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.sgiggle.production.adapter.VGoodsPaymentAdapter;
import com.sgiggle.production.adapter.VGoodsPaymentAdapter.OnClickListener;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogPayload;
import java.util.List;

public abstract class GenericProductCatalogActivity extends BillingSupportBaseActivity
  implements VGoodsPaymentAdapter.OnClickListener, AdapterView.OnItemClickListener
{
  private static GenericProductCatalogActivity s_instance;
  protected VGoodsPaymentAdapter m_adapter;
  private View m_footer;

  private static void clearRunningInstance(GenericProductCatalogActivity paramGenericProductCatalogActivity)
  {
    if (s_instance == paramGenericProductCatalogActivity)
      s_instance = null;
  }

  public static GenericProductCatalogActivity getRunningInstance()
  {
    return s_instance;
  }

  private void hideErrorMessage()
  {
    findViewById(2131361844).setVisibility(8);
    findViewById(2131361845).setVisibility(0);
  }

  private static void setRunningInstance(GenericProductCatalogActivity paramGenericProductCatalogActivity)
  {
    s_instance = paramGenericProductCatalogActivity;
  }

  private void showErrorMessage()
  {
    ((TextView)findViewById(2131362139)).setText(getErrorMessage());
    findViewById(2131361844).setVisibility(0);
    findViewById(2131361845).setVisibility(8);
  }

  public void confirmPurchaseFailed()
  {
  }

  protected String getErrorMessage()
  {
    return getString(2131296514);
  }

  protected View getFooterView()
  {
    return getLayoutInflater().inflate(2130903135, null);
  }

  public void goBack()
  {
    onBackPressed();
  }

  public void onClick(int paramInt, SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    if (paramInt == 2131361833)
      purchase(paramProductCatalogEntry);
    while (paramInt != 2131361843)
      return;
    showDemo(paramProductCatalogEntry);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903137);
    this.m_adapter = new VGoodsPaymentAdapter(this);
    this.m_adapter.setOnClickListener(this);
    ListView localListView = (ListView)findViewById(2131361845);
    localListView.setOnItemClickListener(this);
    this.m_footer = getFooterView();
    localListView.addFooterView(this.m_footer, null, false);
    localListView.setAdapter(this.m_adapter);
    localListView.setDividerHeight(0);
    localListView.setEmptyView(findViewById(16908292));
    handleNewMessage(getFirstMessage());
    setRunningInstance(this);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    clearRunningInstance(this);
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    showDemo((SessionMessages.ProductCatalogEntry)paramAdapterView.getItemAtPosition(paramInt));
  }

  protected void onNewProductCatalogPayload(SessionMessages.ProductCatalogPayload paramProductCatalogPayload)
  {
    if (paramProductCatalogPayload.hasError())
      showErrorMessage();
    do
    {
      return;
      List localList = paramProductCatalogPayload.getEntryList();
      if (!localList.isEmpty())
        hideErrorMessage();
      this.m_adapter.setProducts(localList);
    }
    while (!paramProductCatalogPayload.getAllCached());
    ((ListView)findViewById(2131361845)).removeFooterView(this.m_footer);
  }

  protected void onPause()
  {
    super.onPause();
  }

  protected void onResume()
  {
    super.onResume();
  }

  public void purchaseProcessed()
  {
  }

  public void setBillingSupported(boolean paramBoolean)
  {
    this.m_adapter.setBillingSupported(paramBoolean);
    if (!paramBoolean)
      showDialog(2);
  }

  public void setPurchesedProduct(String paramString, Constants.PurchaseState paramPurchaseState)
  {
    this.m_adapter.notifyDataSetChanged();
  }

  protected abstract void showDemo(SessionMessages.ProductCatalogEntry paramProductCatalogEntry);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.GenericProductCatalogActivity
 * JD-Core Version:    0.6.2
 */