package com.sgiggle.production;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.cafe.vgood.CafeView;
import com.sgiggle.media_engine.MediaEngineMessage.VGoodDemoAnimationTrackingMessage;
import com.sgiggle.media_engine.MediaEngineMessage.WandPressedMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.adapter.VGoodSelectorAdapter;
import com.sgiggle.production.payments.BillingServiceManager;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.production.widget.HorizontalListView;
import com.sgiggle.xmpp.SessionMessages.Price;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductDetailsPayload;
import com.sgiggle.xmpp.SessionMessages.VGoodBundle;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic;
import com.sgiggle.xmpp.SessionMessages.WandLocationType;
import java.util.HashMap;
import java.util.List;

public abstract class GenericProductDemoActivity extends BillingSupportBaseActivity
  implements View.OnClickListener, AdapterView.OnItemClickListener
{
  private static GenericProductDemoActivity s_instance;
  private View bottomBubble;
  private VGoodSelectorAdapter m_adapter;
  private boolean m_btnClicked = false;
  private Button m_buy;
  protected CafeView m_cafeView;
  SessionMessages.ProductCatalogEntry m_product;
  protected String m_productMarketId;
  ProgressDialog m_progressDialog;
  private boolean m_purchased = false;
  protected HorizontalListView m_selector;
  private Animation m_selectorIn;
  private ImageView m_wand;
  private View wandBubble;

  private static void clearRunningInstance(GenericProductDemoActivity paramGenericProductDemoActivity)
  {
    if (s_instance == paramGenericProductDemoActivity)
      s_instance = null;
  }

  public static GenericProductDemoActivity getRunningInstance()
  {
    return s_instance;
  }

  private void hideWand()
  {
    this.wandBubble.setVisibility(8);
    this.m_wand.setImageResource(2130837727);
    this.m_wand.setEnabled(false);
    this.m_selector.setVisibility(0);
  }

  private void markProductPurchased()
  {
    this.m_purchased = true;
    this.m_buy.setEnabled(false);
  }

  private void onBuyClick()
  {
    purchase(this.m_product);
  }

  private void onWandClick()
  {
    hideWand();
    this.m_selectorIn.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        ((TextView)GenericProductDemoActivity.this.bottomBubble.findViewById(2131361827)).setText(GenericProductDemoActivity.this.getBottomBubbleTextResId());
        GenericProductDemoActivity.this.bottomBubble.setVisibility(0);
      }

      public void onAnimationRepeat(Animation paramAnonymousAnimation)
      {
      }

      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
      }
    });
    this.m_selector.startAnimation(this.m_selectorIn);
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.WandPressedMessage(SessionMessages.WandLocationType.LOCATION_IN_DEMO));
    this.m_btnClicked = false;
  }

  private static void setRunningInstance(GenericProductDemoActivity paramGenericProductDemoActivity)
  {
    s_instance = paramGenericProductDemoActivity;
  }

  public void confirmPurchaseFailed()
  {
  }

  protected int getBottomBubbleTextResId()
  {
    return 2131296538;
  }

  protected int getRootView()
  {
    return 2130903082;
  }

  protected int getUpperBubbleTextResId()
  {
    return 2131296537;
  }

  public void goBack()
  {
    onBackPressed();
  }

  void handleNewMessage(Message paramMessage)
  {
    if (paramMessage == null)
      return;
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35229:
    }
    onAnimationComplete();
  }

  void hideProgressDialog()
  {
    if (this.m_progressDialog != null)
    {
      this.m_progressDialog.dismiss();
      this.m_progressDialog = null;
    }
  }

  boolean isCafeViewFullScreen()
  {
    return false;
  }

  protected void onAnimationComplete()
  {
    this.m_adapter.setEnabled(true);
    this.m_adapter.clearHighlight();
    if (isCafeViewFullScreen())
      findViewById(2131361923).setVisibility(0);
  }

  void onAnimationFinished()
  {
    this.m_selector.setEnabled(true);
  }

  protected void onAnimationStarted()
  {
    if (isCafeViewFullScreen())
      findViewById(2131361923).setVisibility(8);
  }

  public void onClick(View paramView)
  {
    if (!this.m_btnClicked)
    {
      this.m_btnClicked = true;
      if (paramView.getId() != 2131361835)
        break label26;
      onWandClick();
    }
    label26: 
    while (paramView.getId() != 2131361833)
      return;
    onBuyClick();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    setContentView(getRootView());
    this.m_selector = ((HorizontalListView)findViewById(2131361854));
    this.m_adapter = new VGoodSelectorAdapter(this);
    this.m_adapter.setEmptyCount(0);
    this.m_selector.setAdapter(this.m_adapter);
    this.m_selector.setOnItemClickListener(this);
    this.m_cafeView = ((CafeView)findViewById(2131361830));
    this.m_cafeView.setZOrderOnTop(true);
    this.m_wand = ((ImageView)findViewById(2131361835));
    this.m_wand.setOnClickListener(this);
    this.wandBubble = findViewById(2131361836);
    this.bottomBubble = findViewById(2131361837);
    this.wandBubble.findViewById(2131361826).setVisibility(8);
    this.bottomBubble.findViewById(2131361826).setVisibility(8);
    this.m_buy = ((Button)findViewById(2131361833));
    this.m_buy.setOnClickListener(this);
    findViewById(2131361834).bringToFront();
    findViewById(2131361831).bringToFront();
    this.m_selectorIn = AnimationUtils.loadAnimation(this, 2130968584);
    handleNewMessage(getFirstMessage());
  }

  protected void onDestroy()
  {
    super.onDestroy();
    clearRunningInstance(this);
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    SessionMessages.VGoodBundle localVGoodBundle = (SessionMessages.VGoodBundle)paramAdapterView.getItemAtPosition(paramInt);
    if ((localVGoodBundle != null) && (this.m_adapter.isEnabled()))
    {
      play(localVGoodBundle.getCinematic().getAssetPath(), localVGoodBundle.getCinematic().getAssetId());
      this.m_adapter.setEnabled(false);
      this.m_adapter.setHighlight(paramInt);
    }
    this.bottomBubble.setVisibility(8);
  }

  void onNewProductDetailPayload(SessionMessages.ProductDetailsPayload paramProductDetailsPayload)
  {
    List localList = paramProductDetailsPayload.getVgoodBundleList();
    this.m_adapter.setData(localList, false);
    showWand(paramProductDetailsPayload.getShowWand());
    this.m_product = paramProductDetailsPayload.getProduct();
    this.m_productMarketId = this.m_product.getProductMarketId();
    String str = this.m_product.getPrice().getLabel();
    ((TextView)findViewById(2131361832)).setText(paramProductDetailsPayload.getProduct().getProductName());
    this.m_buy.setText(str);
    Constants.PurchaseState localPurchaseState = (Constants.PurchaseState)BillingServiceManager.getInstance().purchaseStateMap.get(paramProductDetailsPayload.getProduct().getProductMarketId());
    if ((paramProductDetailsPayload.getProduct().getPurchased()) || (localPurchaseState == Constants.PurchaseState.PURCHASED))
      markProductPurchased();
    if (!paramProductDetailsPayload.getAllCached())
    {
      showProgressDialog();
      return;
    }
    hideProgressDialog();
  }

  protected void onPause()
  {
    super.onPause();
    this.m_cafeView.onPause();
    CafeMgr.StopAllSurprises();
    CafeMgr.Pause();
    CafeMgr.FreeEngine();
  }

  protected void onResume()
  {
    this.m_btnClicked = false;
    super.onResume();
    CafeMgr.InitEngine(this);
    CafeMgr.SetCallbacks();
    this.m_cafeView.onResume();
    CafeMgr.Resume();
    setRunningInstance(this);
  }

  protected void play(String paramString, long paramLong)
  {
    CafeMgr.LoadSurprise(paramString, "Surprise.Cafe");
    CafeMgr.StartSurprise(paramString, 0L, paramLong, true);
    MediaEngineMessage.VGoodDemoAnimationTrackingMessage localVGoodDemoAnimationTrackingMessage = new MediaEngineMessage.VGoodDemoAnimationTrackingMessage(paramLong, this.m_productMarketId);
    MessageRouter.getInstance().postMessage("jingle", localVGoodDemoAnimationTrackingMessage);
    onAnimationStarted();
  }

  public void purchaseProcessed()
  {
  }

  public void setBillingSupported(boolean paramBoolean)
  {
    Button localButton = this.m_buy;
    if ((paramBoolean) && (!this.m_purchased));
    for (boolean bool = true; ; bool = false)
    {
      localButton.setEnabled(bool);
      if (!paramBoolean)
        showDialog(2);
      return;
    }
  }

  public void setPurchesedProduct(String paramString, Constants.PurchaseState paramPurchaseState)
  {
    if (paramPurchaseState == Constants.PurchaseState.PURCHASED)
      markProductPurchased();
  }

  void showProgressDialog()
  {
    hideProgressDialog();
    this.m_progressDialog = new ProgressDialog(this);
    this.m_progressDialog.setMessage(getString(2131296547));
    this.m_progressDialog.setCanceledOnTouchOutside(false);
    this.m_progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        GenericProductDemoActivity.this.onBackPressed();
      }
    });
    this.m_progressDialog.show();
  }

  void showWand(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.m_wand.setVisibility(0);
      this.wandBubble.setVisibility(0);
      ((TextView)this.wandBubble.findViewById(2131361827)).setText(getUpperBubbleTextResId());
      this.m_selector.setVisibility(8);
      return;
    }
    hideWand();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.GenericProductDemoActivity
 * JD-Core Version:    0.6.2
 */