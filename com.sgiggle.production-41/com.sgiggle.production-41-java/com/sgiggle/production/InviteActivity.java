package com.sgiggle.production;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.InviteDisplayMainEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InviteEmailSelectionMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSMSSelectionMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteViaSnsMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.InviteOptionsPayload;
import com.sgiggle.xmpp.SessionMessages.InviteSendType;

public class InviteActivity extends ActivityBase
{
  private final String TAG = "Tango.InviteActivity";

  public void handleNewMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
    case 35039:
    }
    label153: label286: Button localButton3;
    label262: label274: 
    do
    {
      return;
      MediaEngineMessage.InviteDisplayMainEvent localInviteDisplayMainEvent = (MediaEngineMessage.InviteDisplayMainEvent)paramMessage;
      SessionMessages.InviteOptionsPayload localInviteOptionsPayload = (SessionMessages.InviteOptionsPayload)localInviteDisplayMainEvent.payload();
      String str1;
      String str2;
      Button localButton1;
      Button localButton2;
      if (((SessionMessages.InviteOptionsPayload)localInviteDisplayMainEvent.payload()).hasSpecifiedInvitePrompt())
      {
        str1 = ((SessionMessages.InviteOptionsPayload)localInviteDisplayMainEvent.payload()).getSpecifiedInvitePrompt();
        ((TextView)findViewById(2131361966)).setText(str1);
        SessionMessages.InviteSendType localInviteSendType = localInviteOptionsPayload.getEmailinvitetype();
        str2 = localInviteOptionsPayload.getSns();
        Log.v("Tango.InviteActivity", "sns type=" + str2);
        localButton1 = (Button)findViewById(2131361967);
        localButton1.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            MediaEngineMessage.InviteEmailSelectionMessage localInviteEmailSelectionMessage = new MediaEngineMessage.InviteEmailSelectionMessage();
            MessageRouter.getInstance().postMessage("jingle", localInviteEmailSelectionMessage);
          }
        });
        if (localInviteSendType != SessionMessages.InviteSendType.SERVER)
          break label262;
        localButton1.setEnabled(true);
        localButton2 = (Button)findViewById(2131361968);
        localButton2.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            MediaEngineMessage.InviteSMSSelectionMessage localInviteSMSSelectionMessage = new MediaEngineMessage.InviteSMSSelectionMessage();
            MessageRouter.getInstance().postMessage("jingle", localInviteSMSSelectionMessage);
          }
        });
        if (localInviteSendType != SessionMessages.InviteSendType.SERVER)
          break label274;
        localButton2.setEnabled(true);
      }
      while (true)
      {
        Log.v("Tango.InviteActivity", "try to show Weibo button");
        if (!str2.equals("weibo"))
          break label286;
        Log.v("Tango.InviteActivity", "show Weibo button");
        Button localButton4 = (Button)findViewById(2131361969);
        localButton4.setVisibility(0);
        localButton4.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            MediaEngineMessage.InviteViaSnsMessage localInviteViaSnsMessage = new MediaEngineMessage.InviteViaSnsMessage("weibo");
            MessageRouter.getInstance().postMessage("jingle", localInviteViaSnsMessage);
            Log.v("Tango.InviteActivity", "send InviteViaSnsMessage() to SM");
          }
        });
        return;
        str1 = getResources().getString(2131296300);
        break;
        localButton1.setEnabled(Utils.isEmailIntentAvailable(this));
        break label153;
        localButton2.setEnabled(Utils.isSmsIntentAvailable(this));
      }
      localButton3 = (Button)findViewById(2131361969);
    }
    while (localButton3.getVisibility() != 0);
    localButton3.setVisibility(4);
  }

  public void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.InviteActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903084);
  }

  protected void onDestroy()
  {
    Log.v("Tango.InviteActivity", "onDestroy()");
    super.onDestroy();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.InviteActivity
 * JD-Core Version:    0.6.2
 */