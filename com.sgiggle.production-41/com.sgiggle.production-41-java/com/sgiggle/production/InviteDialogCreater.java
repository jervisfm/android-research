package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.EndStateNoChangeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteDisplayMainMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteEmailComposerMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSMSSelectedMessage;
import com.sgiggle.media_engine.MediaEngineMessage.LeaveVideoMailMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.Contact.Builder;
import com.sgiggle.xmpp.SessionMessages.ContactsPayload;
import com.sgiggle.xmpp.SessionMessages.CountryCode;
import com.sgiggle.xmpp.SessionMessages.CountryCode.Builder;
import com.sgiggle.xmpp.SessionMessages.Invitee;
import com.sgiggle.xmpp.SessionMessages.Invitee.Builder;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber.Builder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InviteDialogCreater
{
  private Context mContext;
  private boolean mDialogShowing = false;
  private LayoutInflater mInflater;
  private boolean m_dialogEntryChosen;

  public InviteDialogCreater(Context paramContext, LayoutInflater paramLayoutInflater)
  {
    this.mContext = paramContext;
    this.mInflater = paramLayoutInflater;
  }

  private void addDismissListenerToDialog(AlertDialog paramAlertDialog)
  {
    paramAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
    {
      public void onDismiss(DialogInterface paramAnonymousDialogInterface)
      {
        if (!InviteDialogCreater.this.m_dialogEntryChosen)
          MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.EndStateNoChangeMessage());
        InviteDialogCreater.access$202(InviteDialogCreater.this, false);
      }
    });
    this.m_dialogEntryChosen = false;
  }

  private void sendInvite2Tango(SessionMessages.Contact paramContact)
  {
    if (!TextUtils.isEmpty(paramContact.getEmail()))
    {
      ArrayList localArrayList1 = new ArrayList();
      localArrayList1.add(SessionMessages.Invitee.newBuilder().setEmail(paramContact.getEmail()).setPhonenumber("").build());
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.InviteEmailComposerMessage(localArrayList1));
      return;
    }
    ArrayList localArrayList2 = new ArrayList();
    SessionMessages.PhoneNumber localPhoneNumber = paramContact.getPhoneNumber();
    localArrayList2.add(SessionMessages.Contact.newBuilder().setNameprefix(paramContact.getNameprefix()).setFirstname(paramContact.getFirstname()).setMiddlename(paramContact.getMiddlename()).setLastname(paramContact.getLastname()).setNamesuffix(paramContact.getNamesuffix()).setDisplayname(paramContact.getDisplayname()).setPhoneNumber(SessionMessages.PhoneNumber.newBuilder().setCountryCode(SessionMessages.CountryCode.newBuilder().setCountryname(localPhoneNumber.getCountryCode().getCountryname()).setCountrycodenumber(localPhoneNumber.getCountryCode().getCountrycodenumber()).setCountryid(localPhoneNumber.getCountryCode().getCountryid()).build()).setSubscriberNumber(localPhoneNumber.getSubscriberNumber()).build()).build());
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.InviteSMSSelectedMessage(localArrayList2));
  }

  private void showDialogForInviteeNoEmailNoNumber()
  {
    if (this.mDialogShowing)
      return;
    AlertDialog localAlertDialog = new AlertDialog.Builder(this.mContext).create();
    localAlertDialog.setTitle(2131296358);
    localAlertDialog.setMessage(this.mContext.getString(2131296359));
    localAlertDialog.setButton(this.mContext.getString(2131296288), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    });
    addDismissListenerToDialog(localAlertDialog);
    localAlertDialog.show();
    this.mDialogShowing = true;
  }

  private void showDialogInviteToTango(List<SessionMessages.Contact> paramList, boolean paramBoolean)
  {
    if (this.mDialogShowing)
      return;
    boolean bool1 = Utils.isSmsIntentAvailable(this.mContext);
    boolean bool2 = Utils.isEmailIntentAvailable(this.mContext);
    boolean bool3 = TangoApp.videomailSupported();
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.mContext);
    localBuilder.setTitle(2131296358);
    localBuilder.setCancelable(true);
    String str1 = this.mContext.getResources().getString(2131296360);
    String str2 = this.mContext.getResources().getString(2131296361);
    String str3 = this.mContext.getResources().getString(2131296362);
    String str4 = this.mContext.getResources().getString(2131296363);
    String str5 = this.mContext.getResources().getString(2131296364);
    String str6 = this.mContext.getResources().getString(2131296365);
    ArrayList localArrayList1 = new ArrayList();
    final ArrayList localArrayList2 = new ArrayList();
    Iterator localIterator = paramList.iterator();
    Object localObject1 = null;
    Object localObject2 = null;
    Object localObject3 = null;
    SessionMessages.Contact localContact4;
    SessionMessages.PhoneNumber localPhoneNumber;
    Object localObject4;
    Object localObject5;
    Object localObject6;
    if (localIterator.hasNext())
    {
      localContact4 = (SessionMessages.Contact)localIterator.next();
      String str7 = localContact4.getEmail();
      localPhoneNumber = localContact4.getPhoneNumber();
      if (!TextUtils.isEmpty(str7))
      {
        if (bool2)
        {
          localArrayList1.add(String.format(str6, new Object[] { str7 }));
          localArrayList2.add(localContact4);
        }
        if (localObject2 != null)
          break label882;
        localObject4 = str7;
        localObject5 = localObject3;
        localObject6 = localObject1;
      }
    }
    while (true)
    {
      localObject1 = localObject6;
      localObject3 = localObject5;
      localObject2 = localObject4;
      break;
      if ((bool1) && (localPhoneNumber != null) && (!TextUtils.isEmpty(localPhoneNumber.getSubscriberNumber())))
      {
        if (localObject1 == null)
          localObject1 = localPhoneNumber;
        Object localObject7;
        String str8;
        switch (4.$SwitchMap$com$sgiggle$xmpp$SessionMessages$PhoneType[localPhoneNumber.getType().ordinal()])
        {
        default:
          localObject7 = localObject3;
          str8 = str5;
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
          Object[] arrayOfObject = new Object[1];
          arrayOfObject[0] = localPhoneNumber.getSubscriberNumber();
          localArrayList1.add(String.format(str8, arrayOfObject));
          localArrayList2.add(localContact4);
          localObject4 = localObject2;
          localObject6 = localObject1;
          localObject5 = localObject7;
          break;
          if (localObject3 == null)
          {
            str8 = str1;
            localObject7 = localPhoneNumber;
            continue;
            localObject7 = localObject3;
            str8 = str2;
            continue;
            localObject7 = localObject3;
            str8 = str3;
            continue;
            localObject7 = localObject3;
            str8 = str4;
            continue;
            int i;
            SessionMessages.Contact.Builder localBuilder1;
            label620: final int m;
            final SessionMessages.Contact localContact1;
            final int k;
            if ((paramBoolean) && (bool3) && ((localObject2 != null) || ((bool1) && (localObject1 != null))))
            {
              i = 1;
              if (i == 0)
                break label816;
              localBuilder1 = SessionMessages.Contact.newBuilder();
              if ((paramList != null) && (paramList.size() > 0))
              {
                SessionMessages.Contact localContact3 = (SessionMessages.Contact)paramList.get(0);
                localBuilder1.setNameprefix(localContact3.getNameprefix());
                localBuilder1.setFirstname(localContact3.getFirstname());
                localBuilder1.setMiddlename(localContact3.getMiddlename());
                localBuilder1.setLastname(localContact3.getLastname());
                localBuilder1.setNamesuffix(localContact3.getNamesuffix());
                localBuilder1.setDisplayname(localContact3.getDisplayname());
              }
              if (localObject2 == null)
                break label784;
              localBuilder1.setEmail(localObject2);
              SessionMessages.Contact localContact2 = localBuilder1.build();
              int n = localArrayList1.size();
              localArrayList1.add(this.mContext.getString(2131296367));
              m = localArrayList1.size();
              localArrayList1.add(this.mContext.getString(2131296366));
              localContact1 = localContact2;
              k = n;
            }
            while (true)
            {
              String[] arrayOfString = (String[])localArrayList1.toArray(new String[0]);
              localBuilder.setSingleChoiceItems(new InviteListAdapter(this.mContext.getApplicationContext(), this.mInflater, 2130903064, arrayOfString), 0, new DialogInterface.OnClickListener()
              {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                  InviteDialogCreater.access$002(InviteDialogCreater.this, true);
                  if (paramAnonymousInt == m)
                  {
                    MediaEngineMessage.LeaveVideoMailMessage localLeaveVideoMailMessage = new MediaEngineMessage.LeaveVideoMailMessage(localContact1, true);
                    MessageRouter.getInstance().postMessage("jingle", localLeaveVideoMailMessage);
                  }
                  while (true)
                  {
                    paramAnonymousDialogInterface.dismiss();
                    return;
                    if (paramAnonymousInt == k)
                    {
                      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.EndStateNoChangeMessage());
                      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.InviteDisplayMainMessage());
                    }
                    else
                    {
                      InviteDialogCreater.this.sendInvite2Tango((SessionMessages.Contact)localArrayList2.get(paramAnonymousInt));
                    }
                  }
                }
              });
              AlertDialog localAlertDialog = localBuilder.create();
              addDismissListenerToDialog(localAlertDialog);
              localAlertDialog.show();
              this.mDialogShowing = true;
              return;
              i = 0;
              break;
              label784: if (localObject3 != null)
              {
                localBuilder1.setPhoneNumber(localObject3);
                break label620;
              }
              if (localObject1 == null)
                break label620;
              localBuilder1.setPhoneNumber(localObject1);
              break label620;
              label816: if (localArrayList1.size() == 0)
              {
                showDialogForInviteeNoEmailNoNumber();
                return;
              }
              int j = localArrayList1.size();
              localArrayList1.add(this.mContext.getString(2131296367));
              k = j;
              m = -1;
              localContact1 = null;
            }
          }
          else
          {
            localObject7 = localObject3;
            str8 = str1;
          }
        }
      }
      label882: localObject4 = localObject2;
      localObject5 = localObject3;
      localObject6 = localObject1;
    }
  }

  public void doInvite2Tango(SessionMessages.ContactsPayload paramContactsPayload)
  {
    List localList = paramContactsPayload.getContactsList();
    boolean bool = true;
    if (paramContactsPayload.hasAccountValidated())
      bool = paramContactsPayload.getAccountValidated();
    showDialogInviteToTango(localList, bool);
  }

  static class InviteListAdapter extends ArrayAdapter<String>
  {
    ViewHolder holder;
    private String[] mArray;
    private LayoutInflater mInflater;
    private int mLayoutViewResId;

    public InviteListAdapter(Context paramContext, LayoutInflater paramLayoutInflater, int paramInt, String[] paramArrayOfString)
    {
      super(paramInt, paramArrayOfString);
      this.mArray = paramArrayOfString;
      this.mInflater = paramLayoutInflater;
      this.mLayoutViewResId = paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView2;
      if (paramView == null)
      {
        localView2 = this.mInflater.inflate(this.mLayoutViewResId, null);
        this.holder = new ViewHolder();
        this.holder.name = ((TextView)localView2.findViewById(2131361914));
        localView2.setTag(this.holder);
      }
      for (View localView1 = localView2; ; localView1 = paramView)
      {
        this.holder.name.setText(this.mArray[paramInt]);
        return localView1;
        this.holder = ((ViewHolder)paramView.getTag());
      }
    }

    class ViewHolder
    {
      TextView name;

      ViewHolder()
      {
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.InviteDialogCreater
 * JD-Core Version:    0.6.2
 */