package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.contacts.ContactStore.ContactOrderPair.ContactOrder;
import com.sgiggle.media_engine.MediaEngineMessage.EndStateNoChangeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteDisplayMainEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InviteDisplayMainMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteEmailComposerEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InviteEmailComposerMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteEmailSelectionEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InviteEmailSelectionMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteEmailSendMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteRecommendedSelectedMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteRecommendedSelectionEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InviteRecommendedSelectionMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSMSInstructionEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSMSSelectedMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSMSSelectionEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSMSSelectionMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSMSSendMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteViaSnsMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.Contact.Builder;
import com.sgiggle.xmpp.SessionMessages.CountryCode;
import com.sgiggle.xmpp.SessionMessages.CountryCode.Builder;
import com.sgiggle.xmpp.SessionMessages.InviteEmailComposerPayload;
import com.sgiggle.xmpp.SessionMessages.InviteEmailSelectionPayload;
import com.sgiggle.xmpp.SessionMessages.InviteOptionsPayload;
import com.sgiggle.xmpp.SessionMessages.InviteSMSSelectedPayload;
import com.sgiggle.xmpp.SessionMessages.InviteSMSSelectionPayload;
import com.sgiggle.xmpp.SessionMessages.InviteSendType;
import com.sgiggle.xmpp.SessionMessages.Invitee;
import com.sgiggle.xmpp.SessionMessages.Invitee.Builder;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber.Builder;
import com.sgiggle.xmpp.SessionMessages.PhoneType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class InviteSelectionActivity extends ActivityBase
  implements AdapterView.OnItemClickListener
{
  private static final int DIALOG_INVITE_REACH_LIMIT = 0;
  private static final int REQUEST_CODE_INVITE_EMAIL = 0;
  private static final int REQUEST_CODE_INVITE_SMS = 1;
  private static final int SEARCH_ID = 4;
  private static final String TAG = "Tango.InviteSelectionActivity";
  private ContactArrayAdapter m_adapter;
  private CheckBox m_allButton;
  private final List<ContactItem> m_contactItems = new ArrayList();
  private boolean m_emailCapable = false;
  private TextView m_emptyView;
  private TextView m_header;
  private ListView m_listView;
  private TextView m_listViewPlaceholder;
  private View m_listViewWrapper;
  private String m_query;
  private String m_recommendationAlgo;
  private String m_recommendationAlgoForSelectedInvitees;
  private RadioButton m_recommendedButton;
  private String m_recommendedButtonLabel;
  private View.OnClickListener m_selectAllListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (InviteSelectionActivity.this.m_adapter == null)
        return;
      boolean bool;
      label42: InviteSelectionActivity localInviteSelectionActivity;
      if (!InviteSelectionActivity.this.m_adapter.isAllItemChecked())
      {
        bool = true;
        Iterator localIterator = InviteSelectionActivity.this.m_adapter.getDisplayedItems().iterator();
        InviteSelectionActivity.ContactItem localContactItem;
        do
        {
          if (!localIterator.hasNext())
            break;
          localContactItem = (InviteSelectionActivity.ContactItem)localIterator.next();
        }
        while (localContactItem.m_selected == bool);
        localContactItem.m_selected = bool;
        localInviteSelectionActivity = InviteSelectionActivity.this;
        if (!bool)
          break label106;
      }
      label106: for (int i = 1; ; i = -1)
      {
        InviteSelectionActivity.access$012(localInviteSelectionActivity, i);
        break label42;
        bool = false;
        break;
      }
      InviteSelectionActivity.this.m_adapter.notifyDataSetChanged();
      InviteSelectionActivity.this.onCheckedItemChanged();
    }
  };
  private ViewGroup m_selectAllPanel;
  private int m_selectedCount = 0;
  private final List<ContactItem> m_selectedInvitees = new ArrayList();
  private Button m_sendButton;
  private View.OnClickListener m_sendInvitesListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (InviteSelectionActivity.this.m_selectedCount == 0)
      {
        MediaEngineMessage.InviteDisplayMainMessage localInviteDisplayMainMessage = new MediaEngineMessage.InviteDisplayMainMessage();
        MessageRouter.getInstance().postMessage("jingle", localInviteDisplayMainMessage);
      }
      do
      {
        return;
        if (InviteSelectionActivity.this.m_viewType == InviteSelectionActivity.ViewType.VIEW_TYPE_INVITE_EMAIL)
        {
          ArrayList localArrayList1 = new ArrayList();
          Iterator localIterator1 = InviteSelectionActivity.this.m_contactItems.iterator();
          while (localIterator1.hasNext())
          {
            InviteSelectionActivity.ContactItem localContactItem1 = (InviteSelectionActivity.ContactItem)localIterator1.next();
            if (localContactItem1.m_selected)
            {
              Object[] arrayOfObject = new Object[4];
              arrayOfObject[0] = localContactItem1.m_email;
              arrayOfObject[1] = localContactItem1.m_phoneNumber;
              arrayOfObject[2] = localContactItem1.m_firstName;
              arrayOfObject[3] = localContactItem1.m_lastName;
              Log.v("Tango.InviteSelectionActivity", String.format("onClick(): Send email :: email=%s; phonenumber=%s; firstname=%s; lastname=%s", arrayOfObject));
              localArrayList1.add(SessionMessages.Invitee.newBuilder().setNameprefix(localContactItem1.m_namePrefix).setFirstname(localContactItem1.m_firstName).setMiddlename(localContactItem1.m_middleName).setLastname(localContactItem1.m_lastName).setNamesuffix(localContactItem1.m_nameSuffix).setDisplayname(localContactItem1.m_displayName).setPhonenumber(localContactItem1.m_phoneNumber).setEmail(localContactItem1.m_email).build());
            }
          }
          MediaEngineMessage.InviteEmailComposerMessage localInviteEmailComposerMessage = new MediaEngineMessage.InviteEmailComposerMessage(localArrayList1);
          MessageRouter.getInstance().postMessage("jingle", localInviteEmailComposerMessage);
          return;
        }
      }
      while ((InviteSelectionActivity.this.m_viewType != InviteSelectionActivity.ViewType.VIEW_TYPE_INVITE_SMS) && (InviteSelectionActivity.this.m_viewType != InviteSelectionActivity.ViewType.VIEW_TYPE_INVITE_RECOMMENDED));
      ArrayList localArrayList2 = new ArrayList();
      Iterator localIterator2 = InviteSelectionActivity.this.m_contactItems.iterator();
      while (localIterator2.hasNext())
      {
        InviteSelectionActivity.ContactItem localContactItem2 = (InviteSelectionActivity.ContactItem)localIterator2.next();
        if (localContactItem2.m_selected)
          localArrayList2.add(SessionMessages.Contact.newBuilder().setNameprefix(localContactItem2.m_namePrefix).setFirstname(localContactItem2.m_firstName).setMiddlename(localContactItem2.m_middleName).setLastname(localContactItem2.m_lastName).setNamesuffix(localContactItem2.m_nameSuffix).setDisplayname(localContactItem2.m_displayName).setPhoneNumber(SessionMessages.PhoneNumber.newBuilder().setCountryCode(SessionMessages.CountryCode.newBuilder().setCountryname("").setCountrycodenumber("").setCountryid("").build()).setSubscriberNumber(localContactItem2.m_phoneNumber).build()).build());
      }
      if (InviteSelectionActivity.this.m_viewType == InviteSelectionActivity.ViewType.VIEW_TYPE_INVITE_SMS);
      for (Object localObject = new MediaEngineMessage.InviteSMSSelectedMessage(localArrayList2); ; localObject = new MediaEngineMessage.InviteRecommendedSelectedMessage(localArrayList2, InviteSelectionActivity.this.m_recommendationAlgo))
      {
        MessageRouter.getInstance().postMessage("jingle", (Message)localObject);
        return;
      }
    }
  };
  private boolean m_smsCapable = false;
  private boolean m_startedAsList = false;
  private TextView m_titleView;
  private RadioGroup m_typeRadioGroup;
  private ViewType m_viewType = null;
  private boolean m_weiboCapable = false;

  private void displayContacts(List<ContactItem> paramList)
  {
    Log.d("Tango.InviteSelectionActivity", "displayContacts(): New list-size=" + paramList.size());
    int i;
    label84: boolean bool;
    if (this.m_query == null)
    {
      this.m_titleView.setVisibility(8);
      if (this.m_viewType == ViewType.VIEW_TYPE_INVITE_EMAIL)
      {
        this.m_emptyView.setText(getResources().getString(2131296304));
        this.m_selectAllPanel.setVisibility(0);
        i = 2131427329;
        this.m_adapter = new ContactArrayAdapter(this, 2130903085, paramList);
        this.m_listView.setAdapter(this.m_adapter);
        CheckBox localCheckBox = this.m_allButton;
        if (this.m_adapter.getCount() <= 0)
          break label381;
        bool = true;
        label129: localCheckBox.setEnabled(bool);
        this.m_listView.setFastScrollEnabled(false);
        this.m_listView.setFastScrollEnabled(true);
        int j = paramList.size();
        if (j <= 0)
          break label387;
        TextView localTextView2 = this.m_header;
        Resources localResources = getResources();
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = Integer.valueOf(j);
        localTextView2.setText(localResources.getQuantityString(i, j, arrayOfObject2));
        label208: if (j <= 500)
          break label399;
        this.m_selectAllPanel.setVisibility(8);
      }
    }
    while (true)
    {
      this.m_listViewPlaceholder.setVisibility(8);
      this.m_listViewWrapper.setVisibility(0);
      showProgressView(false);
      onCheckedItemChanged();
      return;
      if (this.m_viewType == ViewType.VIEW_TYPE_INVITE_SMS)
      {
        this.m_emptyView.setText(getResources().getString(2131296305));
        break;
      }
      this.m_emptyView.setText(getResources().getString(2131296306));
      break;
      TextView localTextView1 = this.m_titleView;
      String str = getResources().getString(2131296341);
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = this.m_query;
      localTextView1.setText(String.format(str, arrayOfObject1));
      this.m_emptyView.setText(getResources().getString(2131296342));
      this.m_selectAllPanel.setVisibility(8);
      i = 2131427330;
      this.m_titleView.setVisibility(0);
      break label84;
      label381: bool = false;
      break label129;
      label387: this.m_header.setText("");
      break label208;
      label399: this.m_selectAllPanel.setVisibility(0);
    }
  }

  private ViewType getSelectedType()
  {
    switch (this.m_typeRadioGroup.getCheckedRadioButtonId())
    {
    default:
      return null;
    case 2131361981:
      return ViewType.VIEW_TYPE_INVITE_EMAIL;
    case 2131361980:
      return ViewType.VIEW_TYPE_INVITE_SMS;
    case 2131361982:
    }
    return ViewType.VIEW_TYPE_INVITE_RECOMMENDED;
  }

  private String getSubLabelForSMSInvitee(SessionMessages.Contact paramContact)
  {
    String str;
    if (paramContact.getPhoneNumber().getType() == SessionMessages.PhoneType.PHONE_TYPE_HOME)
      str = getResources().getString(2131296310);
    while ((str != null) && (str.length() > 0))
    {
      return str + " " + paramContact.getPhoneNumber().getSubscriberNumber();
      if (paramContact.getPhoneNumber().getType() == SessionMessages.PhoneType.PHONE_TYPE_MAIN)
      {
        str = getResources().getString(2131296308);
      }
      else if (paramContact.getPhoneNumber().getType() == SessionMessages.PhoneType.PHONE_TYPE_WORK)
      {
        str = getResources().getString(2131296309);
      }
      else
      {
        SessionMessages.PhoneType localPhoneType1 = paramContact.getPhoneNumber().getType();
        SessionMessages.PhoneType localPhoneType2 = SessionMessages.PhoneType.PHONE_TYPE_MOBILE;
        str = null;
        if (localPhoneType1 == localPhoneType2)
          str = getResources().getString(2131296307);
      }
    }
    return paramContact.getPhoneNumber().getSubscriberNumber();
  }

  private void handleEmailComposerEvent(MediaEngineMessage.InviteEmailComposerEvent paramInviteEmailComposerEvent)
  {
    Log.d("Tango.InviteSelectionActivity", "handleEmailComposerEvent()");
    setVisible(this.m_startedAsList);
    int i = ((SessionMessages.InviteEmailComposerPayload)paramInviteEmailComposerEvent.payload()).getInviteeCount();
    Log.v("Tango.InviteSelectionActivity", "handleEmailComposerEvent(): nInviteeCount = " + i);
    if (i == 0)
      return;
    this.m_selectedInvitees.clear();
    String[] arrayOfString = new String[i];
    for (int j = 0; j < i; j++)
    {
      SessionMessages.Invitee localInvitee = ((SessionMessages.InviteEmailComposerPayload)paramInviteEmailComposerEvent.payload()).getInvitee(j);
      Object[] arrayOfObject = new Object[8];
      arrayOfObject[0] = localInvitee.getEmail();
      arrayOfObject[1] = localInvitee.getPhonenumber();
      arrayOfObject[2] = localInvitee.getNameprefix();
      arrayOfObject[3] = localInvitee.getFirstname();
      arrayOfObject[4] = localInvitee.getMiddlename();
      arrayOfObject[5] = localInvitee.getLastname();
      arrayOfObject[6] = localInvitee.getNamesuffix();
      arrayOfObject[7] = localInvitee.getDisplayname();
      Log.v("Tango.InviteSelectionActivity", String.format("handleEmailComposerEvent(): email=%s; phonenumber=%s; nameprefix=%s; firstname=%s; middlename=%s; lastname=%s; namesuffix=%s; displayname=%s", arrayOfObject));
      arrayOfString[j] = localInvitee.getEmail();
      ContactItem localContactItem = new ContactItem(localInvitee.getNameprefix(), localInvitee.getFirstname(), localInvitee.getMiddlename(), localInvitee.getLastname(), localInvitee.getNamesuffix(), localInvitee.getDisplayname(), localInvitee.getEmail());
      localContactItem.m_phoneNumber = localInvitee.getPhonenumber();
      localContactItem.m_selected = true;
      this.m_selectedInvitees.add(localContactItem);
    }
    String str1;
    if (((SessionMessages.InviteEmailComposerPayload)paramInviteEmailComposerEvent.payload()).hasSpecifiedSubject())
    {
      str1 = ((SessionMessages.InviteEmailComposerPayload)paramInviteEmailComposerEvent.payload()).getSpecifiedSubject();
      if (!((SessionMessages.InviteEmailComposerPayload)paramInviteEmailComposerEvent.payload()).hasSpecifiedContent())
        break label445;
    }
    label445: for (String str2 = ((SessionMessages.InviteEmailComposerPayload)paramInviteEmailComposerEvent.payload()).getSpecifiedContent(); ; str2 = getResources().getString(2131296301))
    {
      Spanned localSpanned = Html.fromHtml(str2);
      Intent localIntent = new Intent("android.intent.action.SEND");
      localIntent.putExtra("android.intent.extra.EMAIL", arrayOfString);
      localIntent.putExtra("android.intent.extra.SUBJECT", str1);
      localIntent.putExtra("android.intent.extra.TEXT", localSpanned);
      localIntent.setType("message/html");
      localIntent.addFlags(262144);
      try
      {
        startActivityForResult(localIntent, 0);
        Log.v("Tango.InviteSelectionActivity", "Invites have been sent out.");
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        Log.w("Tango.InviteSelectionActivity", "Not activity was found for ACTION_SEND (for sending Invites)");
        return;
      }
      str1 = getResources().getString(2131296303);
      break;
    }
  }

  private void handleEmailSelectionEvent(MediaEngineMessage.InviteEmailSelectionEvent paramInviteEmailSelectionEvent)
  {
    Log.d("Tango.InviteSelectionActivity", "handleInviteDisplayMainEvent()");
    if (this.m_typeRadioGroup.getCheckedRadioButtonId() != 2131361981)
      this.m_typeRadioGroup.check(2131361981);
    if ((this.m_viewType == ViewType.VIEW_TYPE_INVITE_EMAIL) && (!this.m_contactItems.isEmpty()))
      return;
    resetInviteSelectionUI();
    this.m_contactItems.clear();
    this.m_startedAsList = true;
    this.m_viewType = ViewType.VIEW_TYPE_INVITE_EMAIL;
    int i = ((SessionMessages.InviteEmailSelectionPayload)paramInviteEmailSelectionEvent.payload()).getContactCount();
    int j = 0;
    while (j < i)
    {
      SessionMessages.Contact localContact = ((SessionMessages.InviteEmailSelectionPayload)paramInviteEmailSelectionEvent.payload()).getContact(j);
      if (!localContact.hasEmail())
      {
        Log.w("Tango.InviteSelectionActivity", "Ignore Contact that has no emaill.");
        j++;
      }
      else
      {
        ContactItem localContactItem = new ContactItem(localContact.getNameprefix(), localContact.getFirstname(), localContact.getMiddlename(), localContact.getLastname(), localContact.getNamesuffix(), localContact.getDisplayname(), localContact.getEmail());
        localContactItem.m_subLabel = localContactItem.m_email;
        String str;
        label210: int k;
        if (localContact.hasPhoneNumber())
        {
          str = localContact.getPhoneNumber().getSubscriberNumber();
          localContactItem.m_phoneNumber = str;
          localContactItem.m_selected = ((SessionMessages.InviteEmailSelectionPayload)paramInviteEmailSelectionEvent.payload()).getSelected(j);
          k = this.m_selectedCount;
          if (!localContactItem.m_selected)
            break label282;
        }
        label282: for (int m = 1; ; m = 0)
        {
          this.m_selectedCount = (k + m);
          this.m_contactItems.add(localContactItem);
          break;
          str = "";
          break label210;
        }
      }
    }
    ContactStore.ContactOrderPair.ContactOrder localContactOrder = ContactStore.ContactOrderPair.getFromPhone(this).getSortOrder();
    Collections.sort(this.m_contactItems, new Utils.ContactComparator(localContactOrder));
    displayContacts(this.m_contactItems);
  }

  private boolean handleInviteDisplayMainEvent(MediaEngineMessage.InviteDisplayMainEvent paramInviteDisplayMainEvent)
  {
    Log.d("Tango.InviteSelectionActivity", "handleInviteDisplayMainEvent()");
    SessionMessages.InviteOptionsPayload localInviteOptionsPayload = (SessionMessages.InviteOptionsPayload)paramInviteDisplayMainEvent.payload();
    if (localInviteOptionsPayload.getEmailinvitetype() == SessionMessages.InviteSendType.SERVER)
    {
      this.m_emailCapable = true;
      if (localInviteOptionsPayload.getSmsinvitetype() != SessionMessages.InviteSendType.SERVER)
        break label121;
    }
    label121: for (this.m_smsCapable = true; ; this.m_smsCapable = Utils.isSmsIntentAvailable(this))
    {
      String str = localInviteOptionsPayload.getSns();
      Log.v("Tango.InviteSelectionActivity", "sns type=" + str);
      this.m_weiboCapable = str.equals("weibo");
      this.m_viewType = null;
      this.m_typeRadioGroup.clearCheck();
      updateTypeToggleVisibility();
      return true;
      this.m_emailCapable = Utils.isEmailIntentAvailable(this);
      break;
    }
  }

  private void handleRecommendedSelectionEvent(MediaEngineMessage.InviteRecommendedSelectionEvent paramInviteRecommendedSelectionEvent)
  {
    int i;
    List localList;
    if (this.m_viewType == ViewType.VIEW_TYPE_INVITE_RECOMMENDED)
    {
      i = ((SessionMessages.InviteSMSSelectionPayload)paramInviteRecommendedSelectionEvent.payload()).getContactsCount();
      localList = ((SessionMessages.InviteSMSSelectionPayload)paramInviteRecommendedSelectionEvent.payload()).getContactsList();
      if (!((SessionMessages.InviteSMSSelectionPayload)paramInviteRecommendedSelectionEvent.payload()).hasRecommendationAlgorithm())
        break label111;
    }
    for (this.m_recommendationAlgo = ((SessionMessages.InviteSMSSelectionPayload)paramInviteRecommendedSelectionEvent.payload()).getRecommendationAlgorithm(); ; this.m_recommendationAlgo = null)
    {
      Log.d("Tango.InviteSelectionActivity", "handleRecommendedSelectionEvent() - handleSMSSelectionEvent:" + i + " ,m_viewType:" + this.m_viewType);
      handleSMSSelectionEvent(2131361982, ViewType.VIEW_TYPE_INVITE_RECOMMENDED, i, localList);
      return;
      label111: Log.w("Tango.InviteSelectionActivity", "handleRecommendedSelectionEvent() - event does not have recommendation algorithm field");
    }
  }

  private void handleSMSInstructionEvent(MediaEngineMessage.InviteSMSInstructionEvent paramInviteSMSInstructionEvent)
  {
    setVisible(this.m_startedAsList);
    char c = Utils.getSmsContactSeparator();
    this.m_selectedInvitees.clear();
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = ((SessionMessages.InviteSMSSelectedPayload)paramInviteSMSInstructionEvent.payload()).getContactList().iterator();
    while (localIterator.hasNext())
    {
      SessionMessages.Contact localContact = (SessionMessages.Contact)localIterator.next();
      if (localStringBuilder.length() > 0)
        localStringBuilder.append(c);
      localStringBuilder.append(localContact.getPhoneNumber().getSubscriberNumber());
      ContactItem localContactItem = new ContactItem(localContact.getNameprefix(), localContact.getFirstname(), localContact.getMiddlename(), localContact.getLastname(), localContact.getNamesuffix(), localContact.getDisplayname(), localContact.getEmail());
      localContactItem.m_phoneNumber = localContact.getPhoneNumber().getSubscriberNumber();
      localContactItem.m_selected = true;
      this.m_selectedInvitees.add(localContactItem);
    }
    this.m_recommendationAlgoForSelectedInvitees = null;
    if (((SessionMessages.InviteSMSSelectedPayload)paramInviteSMSInstructionEvent.payload()).hasRecommendationAlgorithm())
      this.m_recommendationAlgoForSelectedInvitees = ((SessionMessages.InviteSMSSelectedPayload)paramInviteSMSInstructionEvent.payload()).getRecommendationAlgorithm();
    if (((SessionMessages.InviteSMSSelectedPayload)paramInviteSMSInstructionEvent.payload()).hasSpecifiedContent());
    for (String str = ((SessionMessages.InviteSMSSelectedPayload)paramInviteSMSInstructionEvent.payload()).getSpecifiedContent(); ; str = getResources().getString(2131296302))
    {
      sendSms(localStringBuilder.toString(), str, 1);
      return;
    }
  }

  private void handleSMSSelectionEvent(int paramInt1, ViewType paramViewType, int paramInt2, List<SessionMessages.Contact> paramList)
  {
    Log.d("Tango.InviteSelectionActivity", "handleSMSSelectionEvent view type:" + paramViewType);
    if ((this.m_typeRadioGroup.getCheckedRadioButtonId() != paramInt1) && (paramInt1 != 2131361982))
      this.m_typeRadioGroup.check(paramInt1);
    if ((this.m_viewType == ViewType.VIEW_TYPE_INVITE_SMS) && (!this.m_contactItems.isEmpty()))
      return;
    resetInviteSelectionUI();
    this.m_contactItems.clear();
    this.m_startedAsList = true;
    this.m_viewType = paramViewType;
    int i = 0;
    while (i < paramInt2)
    {
      SessionMessages.Contact localContact = (SessionMessages.Contact)paramList.get(i);
      if (!localContact.hasPhoneNumber())
      {
        Log.w("Tango.InviteSelectionActivity", "Ignore Contact without a phone-number.");
        i++;
      }
      else
      {
        ContactItem localContactItem = new ContactItem(localContact.getNameprefix(), localContact.getFirstname(), localContact.getMiddlename(), localContact.getLastname(), localContact.getNamesuffix(), localContact.getDisplayname());
        localContactItem.m_phoneNumber = localContact.getPhoneNumber().getSubscriberNumber();
        localContactItem.m_subLabel = getSubLabelForSMSInvitee(localContact);
        localContactItem.m_selected = false;
        int j = this.m_selectedCount;
        if (localContactItem.m_selected);
        for (int k = 1; ; k = 0)
        {
          this.m_selectedCount = (j + k);
          this.m_contactItems.add(localContactItem);
          break;
        }
      }
    }
    ContactStore.ContactOrderPair.ContactOrder localContactOrder = ContactStore.ContactOrderPair.getFromPhone(this).getSortOrder();
    Collections.sort(this.m_contactItems, new Utils.ContactComparator(localContactOrder));
    displayContacts(this.m_contactItems);
  }

  private void handleSMSSelectionEvent(MediaEngineMessage.InviteSMSSelectionEvent paramInviteSMSSelectionEvent)
  {
    Log.d("Tango.InviteSelectionActivity", "handleSMSSelectionEvent()");
    int i = ((SessionMessages.InviteSMSSelectionPayload)paramInviteSMSSelectionEvent.payload()).getContactsCount();
    List localList = ((SessionMessages.InviteSMSSelectionPayload)paramInviteSMSSelectionEvent.payload()).getContactsList();
    this.m_recommendationAlgo = null;
    handleSMSSelectionEvent(2131361980, ViewType.VIEW_TYPE_INVITE_SMS, i, localList);
  }

  private void handleSearch(String paramString)
  {
    Log.d("Tango.InviteSelectionActivity", "handleSearch(query=" + paramString + ")");
    ArrayList localArrayList = new ArrayList();
    String str = paramString.trim().toUpperCase();
    Iterator localIterator = this.m_contactItems.iterator();
    while (localIterator.hasNext())
    {
      ContactItem localContactItem = (ContactItem)localIterator.next();
      if ((Utils.startWithUpperCase(localContactItem.m_firstName, str)) || (Utils.startWithUpperCase(localContactItem.m_lastName, str)) || (Utils.startWithUpperCase(localContactItem.displayName(), str)))
        localArrayList.add(localContactItem);
    }
    displayContacts(localArrayList);
  }

  private boolean isSelectedTypeActive()
  {
    ViewType localViewType = getSelectedType();
    return ((localViewType == ViewType.VIEW_TYPE_INVITE_EMAIL) && (this.m_emailCapable)) || ((localViewType == ViewType.VIEW_TYPE_INVITE_RECOMMENDED) && (this.m_smsCapable)) || ((localViewType == ViewType.VIEW_TYPE_INVITE_SMS) && (this.m_smsCapable));
  }

  private void onCheckedItemChanged()
  {
    CheckBox localCheckBox;
    if (this.m_selectedCount == 0)
    {
      this.m_sendButton.setText(getResources().getString(2131296312));
      this.m_sendButton.setEnabled(false);
      localCheckBox = this.m_allButton;
      if ((this.m_adapter == null) || (!this.m_adapter.isAllItemChecked()))
        break label123;
    }
    label123: for (boolean bool = true; ; bool = false)
    {
      localCheckBox.setChecked(bool);
      return;
      Button localButton = this.m_sendButton;
      Resources localResources = getResources();
      int i = this.m_selectedCount;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(this.m_selectedCount);
      localButton.setText(localResources.getQuantityString(2131427328, i, arrayOfObject));
      this.m_sendButton.setEnabled(true);
      break;
    }
  }

  private boolean reachEmailSelectedLimit(ContactItem paramContactItem)
  {
    if (this.m_selectedCount > 500)
    {
      paramContactItem.m_selected = false;
      this.m_selectedCount -= 1;
      showDialog(0);
      return true;
    }
    return false;
  }

  private void resetInviteSelectionUI()
  {
    this.m_listView.setAdapter(null);
    this.m_contactItems.clear();
    this.m_allButton.setChecked(false);
    this.m_query = null;
    this.m_selectedCount = 0;
    this.m_viewType = null;
    onCheckedItemChanged();
  }

  private void sendSms(String paramString1, String paramString2, int paramInt)
  {
    Intent localIntent = Utils.getSmsIntent(paramString1, paramString2);
    try
    {
      startActivityForResult(localIntent, paramInt);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Log.w("Tango.InviteSelectionActivity", "Not activity was found for ACTION_VIEW (for sending SMS)");
    }
  }

  private void showProgressView(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      findViewById(2131361988).setVisibility(0);
      findViewById(2131361978).setVisibility(8);
      return;
    }
    findViewById(2131361988).setVisibility(8);
    findViewById(2131361978).setVisibility(0);
  }

  private void switchToType(ViewType paramViewType)
  {
    if (paramViewType == this.m_viewType)
    {
      Log.d("Tango.InviteSelectionActivity", "Ignoring switchToType, already in type: " + paramViewType);
      return;
    }
    Log.d("Tango.InviteSelectionActivity", "switchToType, to: " + paramViewType);
    switch (4.$SwitchMap$com$sgiggle$production$InviteSelectionActivity$ViewType[paramViewType.ordinal()])
    {
    default:
      return;
    case 1:
      if (this.m_emailCapable)
      {
        this.m_listViewWrapper.setVisibility(0);
        this.m_listViewPlaceholder.setVisibility(8);
        MediaEngineMessage.InviteEmailSelectionMessage localInviteEmailSelectionMessage = new MediaEngineMessage.InviteEmailSelectionMessage();
        MessageRouter.getInstance().postMessage("jingle", localInviteEmailSelectionMessage);
        return;
      }
      resetInviteSelectionUI();
      this.m_listViewWrapper.setVisibility(8);
      this.m_listViewPlaceholder.setText(2131296373);
      this.m_listViewPlaceholder.setVisibility(0);
      showProgressView(false);
      return;
    case 2:
      if (this.m_smsCapable)
      {
        this.m_listViewWrapper.setVisibility(0);
        this.m_listViewPlaceholder.setVisibility(8);
        MediaEngineMessage.InviteSMSSelectionMessage localInviteSMSSelectionMessage = new MediaEngineMessage.InviteSMSSelectionMessage();
        MessageRouter.getInstance().postMessage("jingle", localInviteSMSSelectionMessage);
        return;
      }
      resetInviteSelectionUI();
      this.m_listViewWrapper.setVisibility(8);
      this.m_listViewPlaceholder.setText(2131296372);
      this.m_listViewPlaceholder.setVisibility(0);
      showProgressView(false);
      return;
    case 3:
      this.m_viewType = ViewType.VIEW_TYPE_INVITE_RECOMMENDED;
      if (this.m_smsCapable)
      {
        this.m_listViewWrapper.setVisibility(0);
        this.m_listViewPlaceholder.setVisibility(8);
        MediaEngineMessage.InviteRecommendedSelectionMessage localInviteRecommendedSelectionMessage = new MediaEngineMessage.InviteRecommendedSelectionMessage();
        MessageRouter.getInstance().postMessage("jingle", localInviteRecommendedSelectionMessage);
        return;
      }
      resetInviteSelectionUI();
      this.m_listViewWrapper.setVisibility(8);
      this.m_listViewPlaceholder.setText(2131296374);
      this.m_listViewPlaceholder.setVisibility(0);
      showProgressView(false);
      return;
    case 4:
    }
    this.m_viewType = ViewType.VIEW_TYPE_INVITE_WEIBO;
    MediaEngineMessage.InviteViaSnsMessage localInviteViaSnsMessage = new MediaEngineMessage.InviteViaSnsMessage("weibo");
    MessageRouter.getInstance().postMessage("jingle", localInviteViaSnsMessage);
    Log.v("Tango.InviteSelectionActivity", "send InviteViaSnsMessage() to SM");
  }

  private void updateTypeToggleVisibility()
  {
    if (this.m_weiboCapable)
    {
      findViewById(2131361984).setVisibility(0);
      findViewById(2131361983).setVisibility(0);
      this.m_typeRadioGroup.setVisibility(0);
      if ((!this.m_emailCapable) || (this.m_smsCapable))
        break label109;
      if (this.m_typeRadioGroup.getCheckedRadioButtonId() != 2131361981)
      {
        this.m_typeRadioGroup.check(2131361981);
        switchToType(ViewType.VIEW_TYPE_INVITE_EMAIL);
      }
    }
    label109: 
    while (this.m_typeRadioGroup.getCheckedRadioButtonId() == 2131361980)
    {
      return;
      findViewById(2131361984).setVisibility(8);
      findViewById(2131361983).setVisibility(8);
      break;
    }
    this.m_typeRadioGroup.check(2131361980);
    switchToType(ViewType.VIEW_TYPE_INVITE_SMS);
  }

  protected void handleNewMessage(Message paramMessage)
  {
    Log.d("Tango.InviteSelectionActivity", "handleNewMessage(): Message = " + paramMessage);
    switch (paramMessage.getType())
    {
    default:
      Log.w("Tango.InviteSelectionActivity", "handleNewMessage(): Unsupported message=" + paramMessage);
      return;
    case 35039:
      handleInviteDisplayMainEvent((MediaEngineMessage.InviteDisplayMainEvent)paramMessage);
      return;
    case 35041:
      handleEmailSelectionEvent((MediaEngineMessage.InviteEmailSelectionEvent)paramMessage);
      return;
    case 35057:
      handleEmailComposerEvent((MediaEngineMessage.InviteEmailComposerEvent)paramMessage);
      return;
    case 35059:
      handleSMSSelectionEvent((MediaEngineMessage.InviteSMSSelectionEvent)paramMessage);
      return;
    case 35060:
      handleRecommendedSelectionEvent((MediaEngineMessage.InviteRecommendedSelectionEvent)paramMessage);
      return;
    case 35061:
    }
    handleSMSInstructionEvent((MediaEngineMessage.InviteSMSInstructionEvent)paramMessage);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Log.i("Tango.InviteSelectionActivity", "onActivityResult(): requestCode = " + paramInt1 + ", resultCode = " + paramInt2);
    switch (paramInt1)
    {
    default:
      return;
    case 0:
      ArrayList localArrayList2 = new ArrayList();
      Iterator localIterator2 = this.m_selectedInvitees.iterator();
      while (localIterator2.hasNext())
      {
        ContactItem localContactItem2 = (ContactItem)localIterator2.next();
        localArrayList2.add(SessionMessages.Invitee.newBuilder().setEmail(localContactItem2.m_email).setPhonenumber(localContactItem2.m_phoneNumber).setNameprefix(localContactItem2.m_namePrefix).setFirstname(localContactItem2.m_firstName).setMiddlename(localContactItem2.m_middleName).setLastname(localContactItem2.m_lastName).setNamesuffix(localContactItem2.m_nameSuffix).setDisplayname(localContactItem2.m_displayName).build());
      }
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.InviteEmailSendMessage(localArrayList2, true));
      return;
    case 1:
    }
    ArrayList localArrayList1 = new ArrayList();
    Iterator localIterator1 = this.m_selectedInvitees.iterator();
    while (localIterator1.hasNext())
    {
      ContactItem localContactItem1 = (ContactItem)localIterator1.next();
      localArrayList1.add(SessionMessages.Invitee.newBuilder().setPhonenumber(localContactItem1.m_phoneNumber).setEmail("").setNameprefix(localContactItem1.m_namePrefix).setFirstname(localContactItem1.m_firstName).setMiddlename(localContactItem1.m_middleName).setLastname(localContactItem1.m_lastName).setNamesuffix(localContactItem1.m_nameSuffix).setDisplayname(localContactItem1.m_displayName).build());
    }
    MediaEngineMessage.InviteSMSSendMessage localInviteSMSSendMessage = new MediaEngineMessage.InviteSMSSendMessage(localArrayList1, this.m_recommendationAlgoForSelectedInvitees, true);
    MessageRouter.getInstance().postMessage("jingle", localInviteSMSSendMessage);
  }

  public void onBackPressed()
  {
    Log.v("Tango.InviteSelectionActivity", "onBackPressed()");
    if (this.m_query != null)
    {
      this.m_query = null;
      displayContacts(this.m_contactItems);
      return;
    }
    super.onBackPressed();
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.InviteSelectionActivity", "onCreate()");
    this.m_contactItems.clear();
    this.m_selectedInvitees.clear();
    this.m_recommendationAlgo = null;
    this.m_recommendationAlgoForSelectedInvitees = null;
    super.onCreate(paramBundle);
    setContentView(2130903087);
    this.m_allButton = ((CheckBox)findViewById(2131361974));
    this.m_allButton.setOnClickListener(this.m_selectAllListener);
    this.m_allButton.setEnabled(false);
    this.m_sendButton = ((Button)findViewById(2131361976));
    this.m_sendButton.setOnClickListener(this.m_sendInvitesListener);
    setDefaultKeyMode(3);
    this.m_listView = ((ListView)findViewById(2131361854));
    this.m_emptyView = ((TextView)findViewById(2131361855));
    this.m_listView.setEmptyView(this.m_emptyView);
    this.m_titleView = ((TextView)findViewById(2131361841));
    this.m_selectAllPanel = ((ViewGroup)findViewById(2131361973));
    this.m_header = new TextView(this);
    this.m_listView.addHeaderView(this.m_header, null, false);
    this.m_listView.setOnItemClickListener(this);
    this.m_typeRadioGroup = ((RadioGroup)findViewById(2131361979));
    this.m_listViewWrapper = findViewById(2131361850);
    this.m_listViewPlaceholder = ((TextView)findViewById(2131361987));
    this.m_recommendedButton = ((RadioButton)findViewById(2131361982));
    if ((this.m_recommendedButton != null) && (this.m_recommendedButton.getText() != null))
      this.m_recommendedButtonLabel = this.m_recommendedButton.getText().toString();
    showProgressView(true);
    if (getFirstMessage() != null)
      handleNewMessage(getFirstMessage());
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
    }
    AlertDialog localAlertDialog = new AlertDialog.Builder(this).create();
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(500);
    localAlertDialog.setMessage(getString(2131296402, arrayOfObject));
    localAlertDialog.setButton(getString(2131296287), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    });
    return localAlertDialog;
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (this.m_query != null)
      return true;
    paramMenu.add(0, 4, 0, 2131296321).setIcon(2130837669);
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    Log.d("Tango.InviteSelectionActivity", "onDestroy()");
    super.onDestroy();
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.EndStateNoChangeMessage());
  }

  public void onEmailClick(View paramView)
  {
    this.m_typeRadioGroup.check(paramView.getId());
    switchToType(ViewType.VIEW_TYPE_INVITE_EMAIL);
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ContactItem localContactItem = (ContactItem)((ListAdapter)paramAdapterView.getAdapter()).getItem(paramInt);
    boolean bool;
    int i;
    if (!localContactItem.m_selected)
    {
      bool = true;
      localContactItem.m_selected = bool;
      i = this.m_selectedCount;
      if (!localContactItem.m_selected)
        break label119;
    }
    label119: for (int j = 1; ; j = -1)
    {
      this.m_selectedCount = (i + j);
      if ((this.m_viewType == ViewType.VIEW_TYPE_INVITE_SMS) || (!reachEmailSelectedLimit(localContactItem)))
      {
        onCheckedItemChanged();
        CheckBox localCheckBox = (CheckBox)paramView.findViewById(2131361972);
        if (localCheckBox != null)
          localCheckBox.setChecked(localContactItem.m_selected);
      }
      return;
      bool = false;
      break;
    }
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Log.d("Tango.InviteSelectionActivity", "onNewIntent()");
    if ("android.intent.action.SEARCH".equals(paramIntent.getAction()))
    {
      if (this.m_contactItems != null)
      {
        this.m_query = paramIntent.getStringExtra("query");
        handleSearch(this.m_query);
      }
      return;
    }
    super.onNewIntent(paramIntent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 4:
    }
    onSearchRequested();
    return true;
  }

  protected void onPause()
  {
    Log.d("Tango.InviteSelectionActivity", "onPause()");
    super.onPause();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    return isSelectedTypeActive();
  }

  public void onRecommendedClick(View paramView)
  {
    this.m_typeRadioGroup.check(paramView.getId());
    switchToType(ViewType.VIEW_TYPE_INVITE_RECOMMENDED);
  }

  protected void onResume()
  {
    Log.d("Tango.InviteSelectionActivity", "onResume()");
    super.onResume();
  }

  public boolean onSearchRequested()
  {
    if (!isSelectedTypeActive())
      return false;
    return super.onSearchRequested();
  }

  public void onSmsClick(View paramView)
  {
    this.m_typeRadioGroup.check(paramView.getId());
    switchToType(ViewType.VIEW_TYPE_INVITE_SMS);
  }

  protected void onStart()
  {
    Log.d("Tango.InviteSelectionActivity", "onStart()");
    super.onStart();
  }

  protected void onStop()
  {
    Log.d("Tango.InviteSelectionActivity", "onStop()");
    super.onStop();
  }

  public void onWeiboClick(View paramView)
  {
    this.m_typeRadioGroup.check(paramView.getId());
    switchToType(ViewType.VIEW_TYPE_INVITE_WEIBO);
  }

  public void setRecommendedBadgeCount(int paramInt)
  {
    Log.d("Tango.InviteSelectionActivity", "setRecommendedBadgeCount to count=" + paramInt);
    if (this.m_viewType != ViewType.VIEW_TYPE_INVITE_RECOMMENDED)
    {
      if (paramInt <= 0)
      {
        this.m_recommendedButton.setTypeface(null, 0);
        this.m_recommendedButton.setText(this.m_recommendedButtonLabel);
        return;
      }
      this.m_recommendedButton.setTypeface(null, 1);
      if (paramInt > 99)
      {
        this.m_recommendedButton.setText(this.m_recommendedButtonLabel + " (" + 2131296262 + ")");
        return;
      }
      this.m_recommendedButton.setText(this.m_recommendedButtonLabel + " (" + Integer.toString(paramInt) + ")");
      return;
    }
    this.m_recommendedButton.setTypeface(null, 0);
    this.m_recommendedButton.setText(this.m_recommendedButtonLabel);
  }

  private static class ContactArrayAdapter extends ArrayAdapter<InviteSelectionActivity.ContactItem>
    implements SectionIndexer
  {
    private HashMap<String, Integer> alphaIndexer;
    private final LayoutInflater m_Inflater;
    private List<InviteSelectionActivity.ContactItem> m_items;
    private final int m_textViewResourceId;
    private String[] sections;

    public ContactArrayAdapter(Context paramContext, int paramInt, List<InviteSelectionActivity.ContactItem> paramList)
    {
      super(paramInt, paramList);
      ContactStore.ContactOrderPair.ContactOrder localContactOrder = ContactStore.ContactOrderPair.getFromPhone(paramContext).getSortOrder();
      this.m_textViewResourceId = paramInt;
      this.m_Inflater = LayoutInflater.from(paramContext);
      this.m_items = paramList;
      this.alphaIndexer = new HashMap();
      for (int i = 0; i < this.m_items.size(); i++)
      {
        String str1 = ((InviteSelectionActivity.ContactItem)this.m_items.get(i)).compareName(localContactOrder);
        if (str1.length() == 0)
          str1 = " ";
        String str2 = str1.substring(0, 1).toUpperCase();
        if (!this.alphaIndexer.containsKey(str2))
          this.alphaIndexer.put(str2, Integer.valueOf(i));
      }
      ArrayList localArrayList = new ArrayList(this.alphaIndexer.keySet());
      Collections.sort(localArrayList);
      this.sections = new String[localArrayList.size()];
      localArrayList.toArray(this.sections);
    }

    public int getCount()
    {
      return this.m_items.size();
    }

    public List<InviteSelectionActivity.ContactItem> getDisplayedItems()
    {
      return this.m_items;
    }

    public int getPositionForSection(int paramInt)
    {
      return ((Integer)this.alphaIndexer.get(this.sections[paramInt])).intValue();
    }

    public int getSectionForPosition(int paramInt)
    {
      return 1;
    }

    public Object[] getSections()
    {
      return this.sections;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView1;
      ViewHolder localViewHolder1;
      if (paramView == null)
      {
        View localView2 = this.m_Inflater.inflate(this.m_textViewResourceId, null);
        ViewHolder localViewHolder2 = new ViewHolder();
        localViewHolder2.name = ((TextView)localView2.findViewById(2131361806));
        localViewHolder2.subLabel = ((TextView)localView2.findViewById(2131361971));
        localViewHolder2.checkBox = ((CheckBox)localView2.findViewById(2131361972));
        localView2.setTag(localViewHolder2);
        localView1 = localView2;
        localViewHolder1 = localViewHolder2;
      }
      while (true)
      {
        InviteSelectionActivity.ContactItem localContactItem = (InviteSelectionActivity.ContactItem)this.m_items.get(paramInt);
        localViewHolder1.name.setText(localContactItem.displayName());
        localViewHolder1.subLabel.setText(localContactItem.m_subLabel);
        localViewHolder1.checkBox.setChecked(localContactItem.m_selected);
        localViewHolder1.checkBox.setTag(localContactItem);
        localViewHolder1.checkBox.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            InviteSelectionActivity.ContactItem localContactItem = (InviteSelectionActivity.ContactItem)paramAnonymousView.getTag();
            InviteSelectionActivity localInviteSelectionActivity;
            boolean bool;
            if (localContactItem != null)
            {
              localInviteSelectionActivity = (InviteSelectionActivity)InviteSelectionActivity.ContactArrayAdapter.this.getContext();
              if (localContactItem.m_selected)
                break label79;
              bool = true;
              localContactItem.m_selected = bool;
              if (!localContactItem.m_selected)
                break label85;
            }
            label79: label85: for (int i = 1; ; i = -1)
            {
              InviteSelectionActivity.access$012(localInviteSelectionActivity, i);
              if ((localInviteSelectionActivity.m_viewType != InviteSelectionActivity.ViewType.VIEW_TYPE_INVITE_SMS) && (localInviteSelectionActivity.reachEmailSelectedLimit(localContactItem)))
                break label91;
              localInviteSelectionActivity.onCheckedItemChanged();
              return;
              bool = false;
              break;
            }
            label91: ((CheckBox)paramAnonymousView).setChecked(false);
          }
        });
        return localView1;
        localViewHolder1 = (ViewHolder)paramView.getTag();
        localView1 = paramView;
      }
    }

    public boolean isAllItemChecked()
    {
      if (this.m_items.size() == 0)
        return false;
      Iterator localIterator = this.m_items.iterator();
      while (localIterator.hasNext())
        if (!((InviteSelectionActivity.ContactItem)localIterator.next()).m_selected)
          return false;
      return true;
    }

    static class ViewHolder
    {
      CheckBox checkBox;
      TextView name;
      TextView subLabel;
    }
  }

  private class ContactItem extends Utils.UIContact
  {
    public boolean m_selected = false;
    public String m_subLabel;

    ContactItem(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String arg7)
    {
      super(paramString2, paramString3, paramString4, paramString5, str);
    }

    ContactItem(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String arg8)
    {
      super(paramString2, paramString3, paramString4, paramString5, paramString6);
      Object localObject;
      this.m_email = localObject;
    }

    public String displayName()
    {
      if (TextUtils.isEmpty(this.m_displayName))
        return this.m_subLabel;
      return this.m_displayName;
    }
  }

  private static enum ViewType
  {
    static
    {
      VIEW_TYPE_INVITE_RECOMMENDED = new ViewType("VIEW_TYPE_INVITE_RECOMMENDED", 2);
      VIEW_TYPE_INVITE_WEIBO = new ViewType("VIEW_TYPE_INVITE_WEIBO", 3);
      ViewType[] arrayOfViewType = new ViewType[4];
      arrayOfViewType[0] = VIEW_TYPE_INVITE_EMAIL;
      arrayOfViewType[1] = VIEW_TYPE_INVITE_SMS;
      arrayOfViewType[2] = VIEW_TYPE_INVITE_RECOMMENDED;
      arrayOfViewType[3] = VIEW_TYPE_INVITE_WEIBO;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.InviteSelectionActivity
 * JD-Core Version:    0.6.2
 */