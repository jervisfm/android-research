package com.sgiggle.production;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ListView;
import com.sgiggle.util.Log;

public class ListViewIgnoreBackKey extends ListView
{
  private static final String TAG = "Tango.ListViewIgnoreBackKey";

  public ListViewIgnoreBackKey(Context paramContext)
  {
    super(paramContext);
  }

  public ListViewIgnoreBackKey(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public ListViewIgnoreBackKey(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void forceOnSizeChanged()
  {
    int i = getMeasuredWidth();
    int j = getMeasuredHeight();
    super.onSizeChanged(i, j, i, j);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) || (paramInt == 82))
    {
      Log.i("Tango.ListViewIgnoreBackKey", "ignore back/menu key down");
      return false;
    }
    Log.i("Tango.ListViewIgnoreBackKey", "key down " + paramInt + " handled by super class");
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) || (paramInt == 82))
    {
      Log.i("Tango.ListViewIgnoreBackKey", "ignore back/menu key up");
      return false;
    }
    Log.i("Tango.ListViewIgnoreBackKey", "key up  " + paramInt + " handled by super class");
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ListViewIgnoreBackKey
 * JD-Core Version:    0.6.2
 */