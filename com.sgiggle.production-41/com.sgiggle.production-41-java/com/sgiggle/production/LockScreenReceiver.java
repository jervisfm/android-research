package com.sgiggle.production;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class LockScreenReceiver extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.LockScreenReceiver
 * JD-Core Version:    0.6.2
 */