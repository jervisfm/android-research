package com.sgiggle.production;

import android.content.Context;
import com.sgiggle.exception.TangoException;
import com.sgiggle.media_engine.MediaEngineMessage.LaunchLogReportEmailMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.LogReporter.AppData;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class LogCollector
{
  protected static boolean checkWriteOk(File paramFile)
  {
    try
    {
      File localFile = new File(paramFile, "tangolog.txt");
      BufferedWriter localBufferedWriter = new BufferedWriter(new FileWriter(localFile));
      localBufferedWriter.write("test");
      localBufferedWriter.newLine();
      localBufferedWriter.flush();
      localBufferedWriter.close();
      boolean bool = localFile.delete();
      return bool;
    }
    catch (Exception localException)
    {
    }
    return false;
  }

  public static String getStorageDirSafe(Context paramContext)
  {
    File localFile = paramContext.getFilesDir();
    if (checkWriteOk(localFile))
      return localFile.getAbsolutePath();
    return null;
  }

  static class ExceptionHandler
    implements Thread.UncaughtExceptionHandler
  {
    private Thread.UncaughtExceptionHandler m_defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();

    public void uncaughtException(Thread paramThread, Throwable paramThrowable)
    {
      if (!(paramThrowable instanceof TangoException));
      for (Object localObject = new TangoException(paramThrowable); ; localObject = paramThrowable)
      {
        this.m_defaultExceptionHandler.uncaughtException(paramThread, (Throwable)localObject);
        return;
      }
    }
  }

  public static class TangoAppData
    implements LogReporter.AppData
  {
    public String localStoragePath()
    {
      return LogCollector.getStorageDirSafe(TangoApp.getInstance());
    }

    public boolean scheduleEmail()
    {
      TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
      MediaEngineMessage.LaunchLogReportEmailMessage localLaunchLogReportEmailMessage = new MediaEngineMessage.LaunchLogReportEmailMessage();
      MessageRouter.getInstance().postMessage("jingle", localLaunchLogReportEmailMessage);
      return true;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.LogCollector
 * JD-Core Version:    0.6.2
 */