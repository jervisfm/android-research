package com.sgiggle.production;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.sgiggle.messaging.Message;
import com.sgiggle.util.Log;
import java.util.LinkedHashMap;

public final class MessageManager
{
  private static final boolean DBG = true;
  private static final int INSTANCE_ID_DEFAULT = 1;
  private static final int INSTANCE_ID_INVALID = 0;
  private static final int MESSAGE_ID_INVALID = 0;
  private static final String PREF_INSTANCE_ID = "instance_id";
  private static final String TAG = "Tango.MessageManager";
  private static final String TANGO_EXTRA_INSTANCE_ID = "me.tango.extra_instance_id";
  private static final String TANGO_EXTRA_MESSAGE_ID = "me.tango.extra_message_id";
  private static final boolean VDBG = true;
  private static MessageManager s_me;
  private TangoApp m_application;
  private final int m_instanceId;
  private final LinkedHashMap<Integer, Message> m_messageMap = new LinkedHashMap();
  private Integer m_nextMessageId = Integer.valueOf(0);

  public MessageManager(TangoApp paramTangoApp)
  {
    this.m_application = paramTangoApp;
    this.m_instanceId = generateInstanceId(this.m_application);
  }

  private int generateInstanceId(Context paramContext)
  {
    int i = getInstanceId(paramContext);
    if (i == 0);
    for (int j = 1; ; j = i + 1)
    {
      setInstanceId(paramContext, j);
      Log.v("Tango.MessageManager", "generateInstanceId(): new instanceId=" + j);
      return j;
    }
  }

  public static MessageManager getDefault()
  {
    return s_me;
  }

  private int getInstanceId(Context paramContext)
  {
    return paramContext.getSharedPreferences("Tango.MessageManager", 0).getInt("instance_id", 0);
  }

  public static void init(TangoApp paramTangoApp)
  {
    s_me = new MessageManager(paramTangoApp);
  }

  private void setInstanceId(Context paramContext, int paramInt)
  {
    paramContext.getSharedPreferences("Tango.MessageManager", 0).edit().putInt("instance_id", paramInt).commit();
  }

  public void clearAllPendingMessages()
  {
    synchronized (this.m_messageMap)
    {
      Log.i("Tango.MessageManager", "clearAllPendingMessages(): Clear all events: count:" + this.m_messageMap.size());
      this.m_messageMap.clear();
      return;
    }
  }

  public Message getMessageFromIntent(Intent paramIntent)
  {
    int i = paramIntent.getIntExtra("me.tango.extra_instance_id", 0);
    if (i == 0)
      return null;
    if (i != this.m_instanceId)
    {
      Log.v("Tango.MessageManager", "getMessageFromIntent: The intent is from an old instance=" + i + ". Current=" + this.m_instanceId);
      return null;
    }
    Integer localInteger = Integer.valueOf(paramIntent.getIntExtra("me.tango.extra_message_id", 0));
    if (localInteger.intValue() == 0)
      return null;
    synchronized (this.m_messageMap)
    {
      Message localMessage = (Message)this.m_messageMap.remove(localInteger);
      Log.v("Tango.MessageManager", "getMessageFromIntent: {#" + this.m_instanceId + ":" + localInteger + ", " + localMessage + "}");
      return localMessage;
    }
  }

  public void storeMessageInIntent(Message paramMessage, Intent paramIntent)
  {
    paramIntent.putExtra("me.tango.extra_instance_id", this.m_instanceId);
    synchronized (this.m_messageMap)
    {
      Integer localInteger = Integer.valueOf(1 + this.m_nextMessageId.intValue());
      this.m_nextMessageId = localInteger;
      this.m_messageMap.put(localInteger, paramMessage);
      paramIntent.putExtra("me.tango.extra_message_id", localInteger);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.MessageManager
 * JD-Core Version:    0.6.2
 */