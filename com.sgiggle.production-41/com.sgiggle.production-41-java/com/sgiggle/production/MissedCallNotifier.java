package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources;
import android.text.format.DateFormat;
import com.sgiggle.media_engine.MediaEngineMessage.CalleeMissedCallEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ClearMissedCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.MakeCallMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload;

class MissedCallNotifier
  implements DialogInterface.OnDismissListener
{
  private static final String TAG = "Tango.MissedCallNotifier";
  private static MissedCallNotifier s_me;
  private TangoApp m_application;
  private Utils.UIMissedCall m_lastMissedCall;
  private AlertDialog m_missedCallAlert;

  public MissedCallNotifier(TangoApp paramTangoApp)
  {
    this.m_application = paramTangoApp;
  }

  private void clearMissedCall()
  {
    if (this.m_lastMissedCall != null)
    {
      String str1 = this.m_lastMissedCall.getMissedJid();
      String str2 = this.m_lastMissedCall.getMissedName();
      Log.d("Tango.MissedCallNotifier", "clearMissedCall(): Dismiss " + str2 + " [" + str1 + "]");
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ClearMissedCallMessage(str1));
      this.m_lastMissedCall = null;
    }
  }

  private void dismissPendingMissedCallAlert()
  {
    if (this.m_missedCallAlert != null)
    {
      Log.d("Tango.MissedCallNotifier", "Dismiss the existing Missed-Call alert...");
      this.m_missedCallAlert.dismiss();
      this.m_missedCallAlert = null;
    }
  }

  static MissedCallNotifier getDefault()
  {
    return s_me;
  }

  static void init(TangoApp paramTangoApp)
  {
    s_me = new MissedCallNotifier(paramTangoApp);
  }

  private void makeCallback()
  {
    if (this.m_lastMissedCall != null)
    {
      String str1 = this.m_lastMissedCall.getMissedJid();
      String str2 = this.m_lastMissedCall.getMissedName();
      Log.d("Tango.MissedCallNotifier", "makeCallback(): to " + str2 + " [" + str1 + "]");
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.MakeCallMessage(str1, str2));
      this.m_lastMissedCall = null;
    }
  }

  private void showMissedCallAlert(Context paramContext, Utils.UIMissedCall paramUIMissedCall)
  {
    Log.d("Tango.MissedCallNotifier", "showMissedCallAlert()");
    dismissPendingMissedCallAlert();
    String str1 = paramUIMissedCall.getMissedName();
    long l = paramUIMissedCall.getMissedWhen();
    Resources localResources = paramContext.getResources();
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = localResources.getString(2131296313);
    arrayOfObject[1] = DateFormat.format("h:mm aa", l);
    String str2 = String.format("%s %s", arrayOfObject);
    String str3 = String.format(localResources.getString(2131296314), new Object[] { str1 });
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
    localBuilder.setTitle(str2);
    localBuilder.setMessage(str3);
    localBuilder.setPositiveButton(2131296315, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        MissedCallNotifier.this.makeCallback();
        MissedCallNotifier.access$102(MissedCallNotifier.this, null);
      }
    });
    localBuilder.setNegativeButton(2131296287, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        MissedCallNotifier.this.clearMissedCall();
        MissedCallNotifier.access$102(MissedCallNotifier.this, null);
      }
    });
    this.m_missedCallAlert = localBuilder.create();
    this.m_missedCallAlert.setOnDismissListener(this);
    this.m_missedCallAlert.show();
  }

  public void cancelLastAlert()
  {
    if (this.m_lastMissedCall != null)
    {
      Log.d("Tango.MissedCallNotifier", "cancelLastAlert()...");
      dismissPendingMissedCallAlert();
      this.m_lastMissedCall = null;
    }
  }

  public void dismissMissedCallNotification()
  {
    TangoApp.getNotificationManager().cancel(2);
  }

  public void displayPendingMissedCallAlert(Context paramContext)
  {
    if (this.m_lastMissedCall != null)
    {
      Log.d("Tango.MissedCallNotifier", "displayPendingMissedCallAlert()");
      TangoApp.getNotificationManager().cancel(2);
      showMissedCallAlert(paramContext, this.m_lastMissedCall);
    }
  }

  public void hidePendingMissedCallAlert()
  {
    if (this.m_missedCallAlert != null)
    {
      Log.d("Tango.MissedCallNotifier", "Hide the existing Missed-Call alert...");
      this.m_missedCallAlert.setOnDismissListener(null);
      this.m_missedCallAlert.dismiss();
      this.m_missedCallAlert = null;
    }
  }

  public boolean isLastEventEqualToMessage(Message paramMessage)
  {
    if ((paramMessage.getType() != 35113) || (this.m_lastMissedCall == null))
      return false;
    MediaEngineMessage.CalleeMissedCallEvent localCalleeMissedCallEvent = (MediaEngineMessage.CalleeMissedCallEvent)paramMessage;
    Utils.UIMissedCall localUIMissedCall = new Utils.UIMissedCall(((SessionMessages.MediaSessionPayload)localCalleeMissedCallEvent.payload()).getAccountId(), ((SessionMessages.MediaSessionPayload)localCalleeMissedCallEvent.payload()).getDisplayname(), 1000L * ((SessionMessages.MediaSessionPayload)localCalleeMissedCallEvent.payload()).getTimestamp());
    return this.m_lastMissedCall.isSame(localUIMissedCall);
  }

  public void notifyMissedCallInStatusBar()
  {
    Log.d("Tango.MissedCallNotifier", "notifyMissedCallInStatusBar()");
    String str1 = this.m_lastMissedCall.getMissedJid();
    String str2 = this.m_lastMissedCall.getMissedName();
    long l = this.m_lastMissedCall.getMissedWhen();
    TangoApp localTangoApp = this.m_application;
    Resources localResources = localTangoApp.getResources();
    String str3 = localResources.getString(2131296313);
    String str4 = localResources.getString(2131296313);
    String str5 = String.format(localResources.getString(2131296314), new Object[] { str2 });
    Intent localIntent = new Intent(localTangoApp, TabActivityBase.class);
    localIntent.putExtra("missedJid", str1);
    localIntent.putExtra("missedDisplayname", str2);
    localIntent.putExtra("missedWhen", l);
    localIntent.addFlags(805306368);
    PendingIntent localPendingIntent = PendingIntent.getActivity(localTangoApp, 0, localIntent, 134217728);
    Notification localNotification = new Notification(2130837675, str3, l);
    localNotification.setLatestEventInfo(localTangoApp, str4, str5, localPendingIntent);
    localNotification.flags = (0x10 | localNotification.flags);
    TangoApp.getNotificationManager().notify(2, localNotification);
  }

  public void onDismiss(DialogInterface paramDialogInterface)
  {
    Log.d("Tango.MissedCallNotifier", "onDismiss() [DialogInterface]");
    if (paramDialogInterface == this.m_missedCallAlert)
      clearMissedCall();
  }

  public boolean saveMissedCallMessage(Message paramMessage)
  {
    Log.d("Tango.MissedCallNotifier", "saveMissedCallMessage(" + paramMessage + ")");
    if (paramMessage.getType() != 35113)
    {
      Log.e("Tango.MissedCallNotifier", "Logic error: Message is NOT a missed-call.");
      return false;
    }
    MediaEngineMessage.CalleeMissedCallEvent localCalleeMissedCallEvent = (MediaEngineMessage.CalleeMissedCallEvent)paramMessage;
    this.m_lastMissedCall = new Utils.UIMissedCall(((SessionMessages.MediaSessionPayload)localCalleeMissedCallEvent.payload()).getAccountId(), ((SessionMessages.MediaSessionPayload)localCalleeMissedCallEvent.payload()).getDisplayname(), 1000L * ((SessionMessages.MediaSessionPayload)localCalleeMissedCallEvent.payload()).getTimestamp());
    return true;
  }

  public void saveMissedCallNotification(Utils.UIMissedCall paramUIMissedCall)
  {
    Log.d("Tango.MissedCallNotifier", "saveMissedCallNotification()");
    this.m_lastMissedCall = paramUIMissedCall;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.MissedCallNotifier
 * JD-Core Version:    0.6.2
 */