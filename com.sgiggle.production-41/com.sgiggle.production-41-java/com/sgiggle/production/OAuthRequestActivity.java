package com.sgiggle.production;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.SnsAuthResultMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SnsRequestAuthEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.StringPayload;

public class OAuthRequestActivity extends ActivityBase
{
  private static final String ACCESSDENIED = "access_denied";
  private static final String ACCESSTOKEN = "access_token";
  private static int DEFAUTL_EXPIRETIME = 0;
  private static final String EXPIRETIME = "expires_in";
  private static final String REFRESHTOKEN = "refresh_token";
  private static final String TAG = "Tango.OAuthRequestActivity";
  private boolean m_isFirstRun = true;
  private ViewGroup m_progressView;
  private WebView m_webView;

  private void parseOAuth(String paramString)
  {
    int i = 0;
    String[] arrayOfString;
    Object localObject1;
    String str1;
    Object localObject2;
    Object localObject5;
    Object localObject6;
    if (paramString.contains("access_token"))
    {
      arrayOfString = paramString.substring(paramString.indexOf("access_token")).split("&");
      localObject1 = new String();
      str1 = new String();
      localObject2 = new String();
      if (i < arrayOfString.length)
        if (arrayOfString[i].contains("access_token"))
        {
          String str3 = arrayOfString[i].substring(1 + arrayOfString[i].indexOf("="));
          Object localObject9 = localObject2;
          localObject5 = str3;
          localObject6 = localObject9;
        }
    }
    while (true)
    {
      i++;
      Object localObject7 = localObject6;
      localObject1 = localObject5;
      localObject2 = localObject7;
      break;
      if (arrayOfString[i].contains("expires_in"))
      {
        str1 = arrayOfString[i].substring(1 + arrayOfString[i].indexOf("="));
        Object localObject8 = localObject2;
        localObject5 = localObject1;
        localObject6 = localObject8;
      }
      else if (arrayOfString[i].contains("refresh_token"))
      {
        String str2 = arrayOfString[i].substring(1 + arrayOfString[i].indexOf("="));
        localObject5 = localObject1;
        localObject6 = str2;
        continue;
        do
          try
          {
            Integer.parseInt(str1);
            j = DEFAUTL_EXPIRETIME;
            this.m_webView.stopLoading();
            MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.SnsAuthResultMessage((String)localObject1, true, j, (String)localObject2, ""));
            return;
          }
          catch (Exception localException)
          {
            while (true)
            {
              localException = localException;
              Log.d("Tango.OAuthRequestActivity", "Parse int exception");
              int j = DEFAUTL_EXPIRETIME;
            }
          }
          finally
          {
          }
        while (!paramString.contains("access_denied"));
        this.m_webView.stopLoading();
        Log.d("Tango.OAuthRequestActivity", "User press the cancel button");
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.SnsAuthResultMessage("", false, 0, "", ""));
      }
      else
      {
        Object localObject4 = localObject2;
        localObject5 = localObject1;
        localObject6 = localObject4;
      }
    }
  }

  public void handleNewMessage(Message paramMessage)
  {
    Log.v("Tango.OAuthRequestActivity", "handleNewMessage id=" + paramMessage.getType());
    if (paramMessage.getType() != 35219)
      return;
    SessionMessages.StringPayload localStringPayload = (SessionMessages.StringPayload)((MediaEngineMessage.SnsRequestAuthEvent)paramMessage).payload();
    this.m_webView.loadUrl(localStringPayload.getText());
  }

  public void onBackPressed()
  {
    Log.d("Tango.OAuthRequestActivity", "onBackPressed() ");
    this.m_webView.stopLoading();
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.SnsAuthResultMessage("", false, 0, "", ""));
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.OAuthRequestActivity", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    setContentView(2130903120);
    this.m_progressView = ((ViewGroup)findViewById(2131361988));
    this.m_webView = ((WebView)findViewById(2131362116));
    this.m_webView.getSettings().setJavaScriptEnabled(true);
    this.m_webView.setWebViewClient(new MyWebViewClient(null));
    handleNewMessage(getFirstMessage());
  }

  protected void onDestroy()
  {
    Log.v("Tango.OAuthRequestActivity", "onDestroy()");
    super.onDestroy();
  }

  private final class MyWebViewClient extends WebViewClient
  {
    private MyWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      Log.d("Tango.OAuthRequestActivity", "Finished loading URL: " + paramString);
      if (OAuthRequestActivity.this.m_isFirstRun)
      {
        OAuthRequestActivity.this.m_webView.setVisibility(0);
        OAuthRequestActivity.this.m_progressView.setVisibility(8);
        OAuthRequestActivity.this.m_webView.requestFocus(130);
        OAuthRequestActivity.access$102(OAuthRequestActivity.this, false);
      }
    }

    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      Log.d("Tango.OAuthRequestActivity", "onPageStarted URL=" + paramString);
      OAuthRequestActivity.this.parseOAuth(paramString);
    }

    public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
    {
      OAuthRequestActivity localOAuthRequestActivity = OAuthRequestActivity.this;
      Log.d("Tango.OAuthRequestActivity", "onReceivedError: " + paramString1);
      Toast.makeText(localOAuthRequestActivity, paramString1, 0).show();
    }

    public void onReceivedSslError(WebView paramWebView, SslErrorHandler paramSslErrorHandler, SslError paramSslError)
    {
      paramSslErrorHandler.proceed();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.OAuthRequestActivity
 * JD-Core Version:    0.6.2
 */