package com.sgiggle.production;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.contacts.ContactStore.ContactOrderPair.ContactOrder;
import com.sgiggle.media_engine.MediaEngineMessage.ContactsDisplayMainMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ViewContactDetailMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ContactDetailPayload.Source;

public class PopupNotification extends Activity
{
  private static final String TAG = "Tango.PushMsgNotifier";
  private String m_peerAccountId;
  private String m_peerFirstname;
  private String m_peerLastname;

  private void createCallDialog()
  {
    Button localButton1 = (Button)findViewById(2131362026);
    localButton1.setText(getString(2131296405));
    localButton1.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PopupNotification.this.finish();
      }
    });
    Button localButton2 = (Button)findViewById(2131362025);
    localButton2.setText(getString(2131296316));
    localButton2.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PopupNotification.this.makeCall(PopupNotification.this.m_peerAccountId, PopupNotification.this.m_peerFirstname, PopupNotification.this.m_peerLastname);
        TangoApp.getNotificationManager().cancel(3);
        PopupNotification.this.finish();
      }
    });
  }

  private void createInformationalDialog()
  {
    ((Button)findViewById(2131362025)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TangoApp.getNotificationManager().cancel(3);
        PopupNotification.this.finish();
      }
    });
    ((Button)findViewById(2131362026)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PopupNotification.this.finish();
      }
    });
  }

  private boolean goodToCall()
  {
    boolean bool = true;
    if (CallHandler.getDefault().getCallSession() != null)
    {
      bool = false;
      Log.v("Tango.PushMsgNotifier", "goodToCall() in call");
    }
    return bool;
  }

  private void makeCall(String paramString1, String paramString2, String paramString3)
  {
    if (CallHandler.getDefault().getCallSession() != null)
      return;
    Log.d("Tango.PushMsgNotifier", "YFJ: makeCallback(): to " + paramString2 + " " + paramString3 + " [" + paramString1 + "]");
    TangoApp.getInstance().sendLoginRequestIfNeeded();
    TabActivityBase localTabActivityBase = TangoApp.getInstance().getTabsActivityInstance();
    String str1;
    String str2;
    if ((localTabActivityBase == null) || ("contacts".equals(localTabActivityBase.getCurrentTabTag())))
    {
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ContactsDisplayMainMessage());
      Log.d("Tango.PushMsgNotifier", "Send ContactsDisplayMainMessage");
      if (ContactStore.ContactOrderPair.getFromPhone(this).getDisplayOrder() != ContactStore.ContactOrderPair.ContactOrder.PRIMARY)
        break label234;
      str1 = paramString3;
      str2 = paramString2;
      label132: if (TextUtils.isEmpty(str2))
        break label280;
    }
    label280: for (String str3 = str2; ; str3 = null)
    {
      String str4;
      if (!TextUtils.isEmpty(str1))
        if (TextUtils.isEmpty(str2))
          str4 = str1;
      while (true)
      {
        Utils.UIContact localUIContact = new Utils.UIContact(null, paramString2, null, paramString3, null, str4);
        localUIContact.m_accountId = paramString1;
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ViewContactDetailMessage(localUIContact.convertToMessageContact(), SessionMessages.ContactDetailPayload.Source.FROM_YFJ_ACTION));
        TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_FOREGROUND);
        return;
        TangoApp.getInstance().getTabsActivityInstance().switchTabSafe("contacts");
        break;
        label234: str1 = paramString2;
        str2 = paramString3;
        break label132;
        str4 = str3 + " " + str1;
        continue;
        str4 = str3;
      }
    }
  }

  public void initPopup(String paramString1, String paramString2)
  {
    ((TextView)findViewById(2131362023)).setText(paramString1);
    ((TextView)findViewById(2131362024)).setText(paramString2);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    try
    {
      TangoApp.ensureInitialized();
      requestWindowFeature(1);
      setContentView(2130903100);
      initPopup(getIntent().getStringExtra("title"), getIntent().getStringExtra("body"));
      this.m_peerFirstname = null;
      this.m_peerLastname = null;
      this.m_peerAccountId = null;
      Bundle localBundle = getIntent().getBundleExtra("actioninfo");
      if ((localBundle != null) && ("offer-call".equals(localBundle.getString("actiontype"))) && (goodToCall()))
      {
        this.m_peerAccountId = localBundle.getString("accountid");
        this.m_peerLastname = localBundle.getString("lastname");
        this.m_peerFirstname = localBundle.getString("firstname");
        Log.v("Tango.PushMsgNotifier", "should display call dialog to  " + this.m_peerFirstname + " " + this.m_peerLastname + " " + this.m_peerAccountId + " " + this.m_peerAccountId);
        createCallDialog();
        return;
      }
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      while (true)
        Log.e("Tango.PushMsgNotifier", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
      createInformationalDialog();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.PopupNotification
 * JD-Core Version:    0.6.2
 */