package com.sgiggle.production;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.CancelAppStoreMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CancelFacebookLikeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CancelPostCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisablePostCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayPostCallScreenEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ForwardToPostCallContentMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.CallEntry;
import com.sgiggle.xmpp.SessionMessages.PostCallContentPayload;
import com.sgiggle.xmpp.SessionMessages.PostCallContentType;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import java.io.File;

public class PostCallActivity extends BillingSupportBaseActivity
{
  private static final int APPSTORE_DETAIL_REQUEST = 0;
  private static final int FACEBOOK_LIKE_BROWSER_REQUEST = 1;
  private static final String TAG = "Tango.PostCallActivity";
  TextView description;
  Button detailButton;
  ImageView detailIcon;
  TextView detailText;
  ImageView icon;
  private long mDeviceContactId;
  private SessionMessages.PostCallContentType mType;
  private boolean m_btnClicked = false;
  TextView title;
  private SessionMessages.ProductCatalogEntry upsellProduct;

  private Drawable getDrawable(String paramString)
  {
    if (new File(paramString).isDirectory())
      return null;
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inDensity = 240;
    localOptions.inTargetDensity = getResources().getDisplayMetrics().densityDpi;
    Bitmap localBitmap = BitmapFactory.decodeFile(paramString, localOptions);
    if (localBitmap == null)
      return null;
    BitmapDrawable localBitmapDrawable = new BitmapDrawable(getResources(), localBitmap);
    localBitmapDrawable.setTargetDensity(getResources().getDisplayMetrics().densityDpi);
    return localBitmapDrawable;
  }

  private void navigateToContentDetailPage()
  {
    if (this.mType != null)
    {
      TabActivityBase localTabActivityBase = TangoApp.getInstance().getTabsActivityInstance();
      if (localTabActivityBase != null)
        localTabActivityBase.setForceSwitchTab();
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ForwardToPostCallContentMessage(this.mType));
    }
  }

  private void onDisplayPostCallEvent(MediaEngineMessage.DisplayPostCallScreenEvent paramDisplayPostCallScreenEvent)
  {
    Log.v("Tango.PostCallActivity", "on display postcall event");
    this.mType = ((SessionMessages.PostCallContentPayload)paramDisplayPostCallScreenEvent.payload()).getContentType();
    this.mDeviceContactId = ((SessionMessages.PostCallContentPayload)paramDisplayPostCallScreenEvent.payload()).getCallEntry().getDeviceContactId();
    if (this.mDeviceContactId < 0L)
      this.mDeviceContactId = 0L;
    if (this.mType == SessionMessages.PostCallContentType.POSTCALL_VGOOD)
    {
      setContentView(2130903102);
      this.icon = ((ImageView)findViewById(2131362028));
      this.title = ((TextView)findViewById(2131362029));
      this.description = ((TextView)findViewById(2131362030));
      this.detailButton = ((Button)findViewById(2131362031));
      this.detailIcon = ((ImageView)findViewById(2131362033));
      this.detailText = ((TextView)findViewById(2131362034));
      if (this.mType != SessionMessages.PostCallContentType.POSTCALL_VGOOD)
        break label318;
      this.upsellProduct = ((SessionMessages.PostCallContentPayload)paramDisplayPostCallScreenEvent.payload()).getProduct();
      this.title.setText(this.upsellProduct.getProductName());
      this.title.setCompoundDrawablePadding((int)(0.5F + 5.0F * getResources().getDisplayMetrics().density));
      Drawable localDrawable = getDrawable(this.upsellProduct.getImagePath());
      this.title.setCompoundDrawablesWithIntrinsicBounds(localDrawable, null, null, null);
      this.detailButton.setEnabled(true);
    }
    while (true)
    {
      View localView1 = findViewById(2131362037);
      if (localView1 != null)
        localView1.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelPostCallMessage());
            PostCallActivity.this.finish();
          }
        });
      this.detailButton.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if (!PostCallActivity.this.m_btnClicked)
          {
            PostCallActivity.access$002(PostCallActivity.this, true);
            if (PostCallActivity.this.mType == SessionMessages.PostCallContentType.POSTCALL_VGOOD)
              PostCallActivity.this.purchase(PostCallActivity.this.upsellProduct);
            PostCallActivity.this.navigateToContentDetailPage();
          }
        }
      });
      View localView2 = findViewById(2131362036);
      if (localView2 != null)
        localView2.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DisablePostCallMessage(PostCallActivity.this.mType));
            PostCallActivity.this.finish();
          }
        });
      return;
      setContentView(2130903101);
      break;
      label318: if (this.mType == SessionMessages.PostCallContentType.POSTCALL_APPSTORE)
      {
        this.icon.setVisibility(8);
        this.icon.setImageResource(2130837738);
        this.title.setText(getResources().getString(2131296478));
        this.title.setCompoundDrawablePadding((int)(0.5F + 10.0F * getResources().getDisplayMetrics().density));
        this.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 2130837738);
        this.description.setText(getResources().getString(2131296479));
        this.detailText.setText(getResources().getString(2131296480));
      }
      else if (this.mType == SessionMessages.PostCallContentType.POSTCALL_FACEBOOK)
      {
        this.icon.setVisibility(8);
        this.title.setText(getResources().getString(2131296481));
        this.description.setText(getResources().getString(2131296482));
        this.detailIcon.setVisibility(0);
        this.detailText.setText(getResources().getString(2131296483));
      }
      else if (this.mType == SessionMessages.PostCallContentType.POSTCALL_INVITE)
      {
        this.icon.setVisibility(0);
        this.icon.setImageResource(2130837737);
        this.title.setText(getResources().getString(2131296486));
        this.description.setText(getResources().getString(2131296487));
        this.detailText.setText(getResources().getString(2131296488));
      }
    }
  }

  public void confirmPurchaseFailed()
  {
    onBackPressed();
  }

  public void displayAppStoreDetailPage()
  {
    Log.d("Tango.PostCallActivity", "displayAppStoreDetailPage(): enter");
    startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.sgiggle.production")), 0);
  }

  public void displayFacebookLikePage()
  {
    Log.d("Tango.PostCallActivity", "displayFacebookLikePage(): enter");
    startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse("http://www.facebook.com/TangoMe")), 1);
  }

  public void goBack()
  {
    onBackPressed();
  }

  void handleNewMessage(Message paramMessage)
  {
    Log.v("Tango.PostCallActivity", "handleNewMessage(message=" + paramMessage + ").");
    if ((paramMessage != null) && (paramMessage.getType() == 35191))
      onDisplayPostCallEvent((MediaEngineMessage.DisplayPostCallScreenEvent)paramMessage);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Log.d("Tango.PostCallActivity", "onActivityResult(): requestCode = " + paramInt1 + ", resultCode = " + paramInt2);
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (paramInt1 == 0)
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelAppStoreMessage());
    while (paramInt1 != 1)
      return;
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelFacebookLikeMessage());
  }

  public void onBackPressed()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelPostCallMessage());
    finish();
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.PostCallActivity", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    TangoApp.getInstance().setPostCallActivityInstance(this);
    setRequestedOrientation(1);
    getWindow().setGravity(17);
    getWindow().setLayout(-1, -2);
    getWindow().setBackgroundDrawable(new BitmapDrawable());
    if (getFirstMessage() == null)
    {
      Log.w("Tango.PostCallActivity", "PostCallActivity created without an event to init the layout");
      TangoApp.getInstance().onActivityResumeAfterKilled(this);
      return;
    }
    handleNewMessage(getFirstMessage());
  }

  protected void onDestroy()
  {
    Log.i("Tango.PostCallActivity", "onDestroy");
    super.onDestroy();
    TangoApp.getInstance().setPostCallActivityInstance(null);
  }

  protected void onPause()
  {
    Log.i("Tango.PostCallActivity", "onPause");
    super.onPause();
    if (isFinishing())
      TangoApp.getInstance().setPostCallActivityInstance(null);
  }

  public void onPurchaseCancelled()
  {
    onBackPressed();
  }

  protected void onResume()
  {
    this.m_btnClicked = false;
    Log.i("Tango.PostCallActivity", "onResume");
    super.onResume();
  }

  public void purchaseProcessed()
  {
    onBackPressed();
  }

  public void setBillingSupported(boolean paramBoolean)
  {
    if (this.mType == SessionMessages.PostCallContentType.POSTCALL_VGOOD)
      this.detailButton.setEnabled(paramBoolean);
  }

  public void setPurchesedProduct(String paramString, Constants.PurchaseState paramPurchaseState)
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.PostCallActivity
 * JD-Core Version:    0.6.2
 */