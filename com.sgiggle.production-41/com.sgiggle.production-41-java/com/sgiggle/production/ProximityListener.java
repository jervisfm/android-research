package com.sgiggle.production;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Build.VERSION;
import com.sgiggle.util.Log;

public final class ProximityListener
{
  private static final String TAG = "Tango.ProximityListener";
  private final float NEAR_VALUE = 3.0F;
  private boolean isSamsungMoment;
  private float m_farValue;
  private EventListener m_listener;
  private Sensor m_proximitySensor;
  private SensorEventListener m_sensorListener;
  private SensorManager m_sensorManager;

  public ProximityListener(Context paramContext, EventListener paramEventListener)
  {
    this.m_listener = paramEventListener;
    this.m_sensorManager = ((SensorManager)paramContext.getSystemService("sensor"));
    this.m_proximitySensor = this.m_sensorManager.getDefaultSensor(8);
    if (this.m_proximitySensor == null);
    do
    {
      return;
      this.m_farValue = this.m_proximitySensor.getMaximumRange();
      Log.v("Tango.ProximityListener", "MaximumRange " + this.m_farValue);
      this.m_sensorListener = new SensorEventListener()
      {
        public void onAccuracyChanged(Sensor paramAnonymousSensor, int paramAnonymousInt)
        {
        }

        public void onSensorChanged(SensorEvent paramAnonymousSensorEvent)
        {
          ProximityListener.this.onSensorEvent(paramAnonymousSensorEvent.values[0]);
        }
      };
    }
    while ((Build.MANUFACTURER.compareToIgnoreCase("Samsung") != 0) || (!Build.MODEL.equals("SPH-M900")));
    this.isSamsungMoment = true;
  }

  private static boolean isDeviceNexusOne2_2()
  {
    return (Build.VERSION.SDK_INT == 8) && (Build.MODEL.equals("Nexus One"));
  }

  private void onSensorEvent(float paramFloat)
  {
    if (this.isSamsungMoment)
      if (paramFloat == 1.0F)
        this.m_listener.proximityChanged(true);
    do
    {
      return;
      this.m_listener.proximityChanged(false);
      return;
      if ((paramFloat >= this.m_farValue) || (paramFloat > 3.0F))
        break;
    }
    while ((paramFloat == 1.0D) && (isDeviceNexusOne2_2()));
    this.m_listener.proximityChanged(true);
    return;
    this.m_listener.proximityChanged(false);
  }

  public void enable(boolean paramBoolean)
  {
    if (this.m_proximitySensor == null)
      return;
    if (paramBoolean)
    {
      this.m_sensorManager.registerListener(this.m_sensorListener, this.m_proximitySensor, 3);
      return;
    }
    this.m_sensorManager.unregisterListener(this.m_sensorListener);
  }

  public static abstract interface EventListener
  {
    public abstract void proximityChanged(boolean paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ProximityListener
 * JD-Core Version:    0.6.2
 */