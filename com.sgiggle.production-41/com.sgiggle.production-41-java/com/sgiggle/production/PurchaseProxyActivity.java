package com.sgiggle.production;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.sgiggle.production.payments.BillingServiceManager;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.production.payments.Constants.ResponseCode;
import com.sgiggle.production.payments.PurchaseUtils;
import com.sgiggle.production.payments.ResponseHandler;
import com.sgiggle.production.payments.ResponseHandler.PurchaseObserver;
import com.sgiggle.production.service.BillingService.RequestPurchase;
import com.sgiggle.production.service.BillingService.RestoreTransactions;
import com.sgiggle.util.Log;

public class PurchaseProxyActivity extends Activity
  implements ResponseHandler.PurchaseObserver, Handler.Callback
{
  private static final int DIALOG_BILLING_NOT_SUPPORTED = 2;
  private static final int DIALOG_SHOW_ERROR = 1;
  public static final String EXTRA_EXTERNAL_MARKET_ID = "PurchaseProxyActivity.EXTRA_EXTERNAL_MARKET_ID";
  public static final String EXTRA_PRODUCT_ID = "PurchaseProxyActivity.EXTRA_PRODUCT_ID";
  private static final int MSG_BUY = 2;
  private static final int MSG_SHOW_UI = 1;
  private static final int SHOW_UI_DELAY_MS = 1000;
  private static final String TAG = "PurchaseProxyActivity";
  private String mExternalMarketId;
  Handler mHandler = new Handler();
  private String mProductId;
  private boolean mResumed = false;
  private State mState = State.INVALID;

  private void buyProduct()
  {
    BillingServiceManager.getInstance().requestPurchase(this.mExternalMarketId, this.mProductId);
    setState(State.IN_STORE);
  }

  private Dialog getDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
      Dialog localDialog2 = PurchaseUtils.getPurchaseFailedDialog(this);
      localDialog2.setOnDismissListener(getDismissListener());
      return localDialog2;
    case 2:
    }
    Dialog localDialog1 = PurchaseUtils.getBillingNotSupportedDialog(this);
    localDialog1.setOnDismissListener(getDismissListener());
    return localDialog1;
  }

  private DialogInterface.OnDismissListener getDismissListener()
  {
    return new DialogInterface.OnDismissListener()
    {
      public void onDismiss(DialogInterface paramAnonymousDialogInterface)
      {
        PurchaseProxyActivity.this.onTransactionDone(false);
      }
    };
  }

  private void hideUi()
  {
    findViewById(2131361923).setVisibility(4);
  }

  private void onTransactionDone(boolean paramBoolean)
  {
    setState(State.DONE);
    if (paramBoolean);
    for (int i = -1; ; i = 0)
    {
      setResult(i);
      finish();
      return;
    }
  }

  private void showUi()
  {
    findViewById(2131361923).setVisibility(0);
  }

  public boolean handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default:
      return false;
    case 1:
      showUi();
      return true;
    case 2:
    }
    buyProduct();
    return true;
  }

  public void onBackPressed()
  {
    super.onBackPressed();
    if (this.mState == State.WAITING_FOR_CONFIRMATION)
      Toast.makeText(this, 2131296652, 0).show();
    setResult(0);
  }

  public void onBillingSupported(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      setState(State.STORE_AVAILABLE);
      this.mHandler.sendEmptyMessage(2);
      return;
    }
    setState(State.STORE_UNAVAILABLE);
    showDialog(2);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903103);
    hideUi();
    this.mProductId = getIntent().getStringExtra("PurchaseProxyActivity.EXTRA_PRODUCT_ID");
    this.mExternalMarketId = getIntent().getStringExtra("PurchaseProxyActivity.EXTRA_EXTERNAL_MARKET_ID");
    if ((this.mProductId == null) || (this.mExternalMarketId == null))
    {
      Log.w("PurchaseProxyActivity", "Missing product id or external market id.");
      setResult(0);
      finish();
      return;
    }
    Log.d("PurchaseProxyActivity", "Purchase:" + this.mProductId);
    BillingServiceManager.getInstance().bindServiceAndSetContext(this);
    setState(State.CONNECTING);
    this.mHandler = new Handler(this);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = getDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt);
  }

  protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
  {
    Dialog localDialog = getDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt, paramBundle);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    BillingServiceManager.getInstance().unbindService();
  }

  protected void onPause()
  {
    super.onPause();
    this.mResumed = false;
    ((TangoApp)getApplication()).getResponseHandler().unregisterObserver(this);
    this.mHandler.removeMessages(1);
  }

  public void onResponseCodeReceived(BillingService.RequestPurchase paramRequestPurchase, Constants.ResponseCode paramResponseCode)
  {
    Log.i("BillingService", "onResponseCodeReceived, responseCode:" + paramResponseCode);
    setState(State.STORE_FINISHED);
    switch (3.$SwitchMap$com$sgiggle$production$payments$Constants$ResponseCode[paramResponseCode.ordinal()])
    {
    default:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    }
    do
    {
      return;
      setState(State.WAITING_FOR_CONFIRMATION);
      return;
      onTransactionDone(false);
      return;
      onTransactionDone(false);
      return;
    }
    while (!this.mResumed);
    showDialog(1);
  }

  public void onResponseCodeReceived(BillingService.RestoreTransactions paramRestoreTransactions, Constants.ResponseCode paramResponseCode)
  {
  }

  protected void onResume()
  {
    super.onResume();
    this.mResumed = true;
    ((TangoApp)getApplication()).getResponseHandler().registerObserver(this);
    this.mHandler.sendEmptyMessageDelayed(1, 1000L);
  }

  public void postPurchaseStateChange(final Constants.PurchaseState paramPurchaseState, final String paramString1, final String paramString2, final long paramLong, String paramString3, final String paramString4, final String paramString5)
  {
    this.mHandler.post(new Runnable()
    {
      public void run()
      {
        PurchaseProxyActivity.this.postPurchaseStateChange2(paramPurchaseState, paramString1, paramString2, paramLong, paramString4, paramString5, this.val$signature);
      }
    });
  }

  public void postPurchaseStateChange2(Constants.PurchaseState paramPurchaseState, String paramString1, String paramString2, long paramLong, String paramString3, String paramString4, String paramString5)
  {
    if ((paramString1 == this.mProductId) && (paramPurchaseState == Constants.PurchaseState.PURCHASED))
    {
      onTransactionDone(true);
      return;
    }
    Log.w("PurchaseProxyActivity", "Got confirmation for different product:" + paramString3);
    onTransactionDone(false);
  }

  void setState(State paramState)
  {
    this.mState = paramState;
    updateUI();
  }

  public void startBuyPageActivity(PendingIntent paramPendingIntent, Intent paramIntent)
  {
    TangoApp.getInstance().skipWelcomePageOnce();
    try
    {
      startIntentSender(paramPendingIntent.getIntentSender(), paramIntent, 0, 0, 0);
      return;
    }
    catch (IntentSender.SendIntentException localSendIntentException)
    {
      onTransactionDone(false);
    }
  }

  void updateUI()
  {
    TextView localTextView = (TextView)findViewById(2131361841);
    switch (3.$SwitchMap$com$sgiggle$production$PurchaseProxyActivity$State[this.mState.ordinal()])
    {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    default:
      return;
    case 6:
      hideUi();
      return;
    case 7:
      localTextView.setText(2131296651);
      showUi();
      return;
    case 8:
    }
    hideUi();
  }

  private static enum State
  {
    static
    {
      CONNECTING = new State("CONNECTING", 1);
      STORE_AVAILABLE = new State("STORE_AVAILABLE", 2);
      STORE_UNAVAILABLE = new State("STORE_UNAVAILABLE", 3);
      IN_STORE = new State("IN_STORE", 4);
      STORE_FINISHED = new State("STORE_FINISHED", 5);
      WAITING_FOR_CONFIRMATION = new State("WAITING_FOR_CONFIRMATION", 6);
      DONE = new State("DONE", 7);
      State[] arrayOfState = new State[8];
      arrayOfState[0] = INVALID;
      arrayOfState[1] = CONNECTING;
      arrayOfState[2] = STORE_AVAILABLE;
      arrayOfState[3] = STORE_UNAVAILABLE;
      arrayOfState[4] = IN_STORE;
      arrayOfState[5] = STORE_FINISHED;
      arrayOfState[6] = WAITING_FOR_CONFIRMATION;
      arrayOfState[7] = DONE;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.PurchaseProxyActivity
 * JD-Core Version:    0.6.2
 */