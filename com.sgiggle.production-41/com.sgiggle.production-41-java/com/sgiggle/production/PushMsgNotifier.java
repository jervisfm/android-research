package com.sgiggle.production;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import com.sgiggle.contacts.ContactStore;
import com.sgiggle.production.manager.MediaManager;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationMessageFactory;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.UpdateConversationMessageNotificationPayload;
import java.util.Date;

public class PushMsgNotifier
{
  public static final String ACTION_CALL_ACCOUNT_ID = "accountid";
  public static final String ACTION_CALL_FIRST_NAME = "firstname";
  public static final String ACTION_CALL_LAST_NAME = "lastname";
  public static final String ACTION_INFO = "actioninfo";
  public static final String ACTION_PUSH_MESSAGE_CONVERSATION_ID = "conversationid";
  public static final String ACTION_SHOW_CONVERSATION = "tango.broadcast.show_conversation";
  public static final String ACTION_SHOW_CONVERSATION_LIST = "tango.broadcast.show_conversation_list";
  public static final String ACTION_TYPE = "actiontype";
  private static final String TAG = "Tango.PushMsgNotifier";
  private static PushMsgNotifier s_me;
  private TangoApp m_application;

  public PushMsgNotifier(TangoApp paramTangoApp)
  {
    this.m_application = paramTangoApp;
  }

  static PushMsgNotifier getDefault()
  {
    return s_me;
  }

  static void init(TangoApp paramTangoApp)
  {
    s_me = new PushMsgNotifier(paramTangoApp);
  }

  public static void updateConversationMessageNotificationInStatusBar(Context paramContext, SessionMessages.UpdateConversationMessageNotificationPayload paramUpdateConversationMessageNotificationPayload)
  {
    int i;
    MediaManager localMediaManager;
    if ((paramUpdateConversationMessageNotificationPayload.hasMessage()) && (ConversationMessage.isStatusError(paramUpdateConversationMessageNotificationPayload.getMessage().getSendStatus())))
    {
      i = 1;
      if (paramUpdateConversationMessageNotificationPayload.getUpdateNotification())
        break label85;
      Log.d("Tango.PushMsgNotifier", "updateConversationMessageNotificationInStatusBar() payload mentions not to update notification, skipping event");
      if (paramUpdateConversationMessageNotificationPayload.getPlayAlertSound())
      {
        localMediaManager = MediaManager.getInstance();
        if (i == 0)
          break label78;
      }
    }
    label78: for (int n = 2131099648; ; n = 2131099650)
    {
      localMediaManager.playAudioResourceAsNotification(paramContext, n, paramUpdateConversationMessageNotificationPayload.getMessage().getIsFromMe());
      return;
      i = 0;
      break;
    }
    label85: NotificationManager localNotificationManager = (NotificationManager)paramContext.getSystemService("notification");
    if (i != 0)
    {
      Intent localIntent1 = new Intent(paramContext, TabActivityBase.class);
      localIntent1.setAction("tango.broadcast.show_conversation_list");
      localIntent1.addFlags(805306368);
      PendingIntent localPendingIntent1 = PendingIntent.getActivity(paramContext, 0, localIntent1, 134217728);
      String str1 = paramContext.getResources().getString(2131296602);
      String str2 = paramContext.getResources().getString(2131296603);
      Notification localNotification1 = new Notification(2130837675, str2, new Date().getTime());
      localNotification1.tickerText = str2;
      localNotification1.setLatestEventInfo(paramContext, str1, str2, localPendingIntent1);
      localNotification1.defaults = (0x2 | localNotification1.defaults);
      localNotification1.flags = (0x10 | localNotification1.flags);
      if (paramUpdateConversationMessageNotificationPayload.getPlayAlertSound())
        localNotification1.sound = Utils.getResourceUri(2131099648);
      localNotificationManager.notify(7, localNotification1);
      return;
    }
    int j = paramUpdateConversationMessageNotificationPayload.getUnreadMessageCount();
    Log.d("Tango.PushMsgNotifier", "updateConversationMessageNotificationInStatusBar: unreadCount =" + j + ", conversation ID=" + paramUpdateConversationMessageNotificationPayload.getConversationId());
    if (j == 0)
    {
      Log.d("Tango.PushMsgNotifier", "Removing Conversation push message from notification bar");
      localNotificationManager.cancel(5);
      return;
    }
    Log.d("Tango.PushMsgNotifier", "Updating Conversation push message to notification bar, #messages =" + j);
    ConversationContact localConversationContact = new ConversationContact(paramUpdateConversationMessageNotificationPayload.getMessage().getPeer());
    ConversationMessage localConversationMessage = ConversationMessageFactory.create(paramUpdateConversationMessageNotificationPayload.getMessage(), localConversationContact);
    Intent localIntent2 = new Intent(paramContext, TabActivityBase.class);
    localIntent2.setAction("tango.broadcast.show_conversation");
    localIntent2.putExtra("conversationid", paramUpdateConversationMessageNotificationPayload.getConversationId());
    localIntent2.addFlags(805306368);
    PendingIntent localPendingIntent2 = PendingIntent.getActivity(paramContext, 0, localIntent2, 134217728);
    String str3 = localConversationContact.getDisplayName();
    String str4 = localConversationMessage.getText(true);
    long l1 = localConversationMessage.getTimestampMs();
    Uri localUri;
    String str5;
    Object localObject2;
    Notification.Builder localBuilder;
    if (paramUpdateConversationMessageNotificationPayload.getPlayAlertSound())
    {
      localUri = Utils.getResourceUri(2131099650);
      Resources localResources = paramContext.getResources();
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = localConversationContact.getDisplayName();
      arrayOfObject[1] = str4;
      str5 = localResources.getString(2131296601, arrayOfObject);
      if (Build.VERSION.SDK_INT < 11)
        break label793;
      if (!localConversationContact.getContact().getIsSystemAccount())
        break label675;
      localObject2 = BitmapFactory.decodeResource(paramContext.getResources(), 2130837649);
      int k = paramContext.getResources().getDimensionPixelSize(17104901);
      int m = paramContext.getResources().getDimensionPixelSize(17104902);
      if ((((Bitmap)localObject2).getWidth() != k) && (((Bitmap)localObject2).getHeight() != m))
        localObject2 = Bitmap.createScaledBitmap((Bitmap)localObject2, k, m, true);
      localBuilder = new Notification.Builder(paramContext);
      localBuilder.setContentIntent(localPendingIntent2).setSmallIcon(2130837675).setLargeIcon((Bitmap)localObject2).setTicker(str5).setNumber(j).setWhen(l1).setAutoCancel(true).setContentTitle(str3).setContentText(str4).setDefaults(2);
    }
    label675: Notification localNotification2;
    for (Object localObject1 = localBuilder.getNotification(); ; localObject1 = localNotification2)
    {
      if (localUri != null)
        ((Notification)localObject1).sound = localUri;
      localNotificationManager.notify(5, (Notification)localObject1);
      return;
      localUri = null;
      break;
      long l2 = localConversationContact.getContact().getDeviceContactId();
      if (l2 != -1L);
      while (true)
      {
        try
        {
          Bitmap localBitmap = ContactStore.getPhotoByContactId(l2);
          localObject2 = localBitmap;
          if (localObject2 != null)
            break;
          localObject2 = BitmapFactory.decodeResource(paramContext.getResources(), 2130837648);
        }
        catch (Exception localException)
        {
          Log.e("Tango.PushMsgNotifier", "updateConversationMessageNotificationInStatusBar(): cannot get photo for contactId=" + l2);
          localObject2 = null;
          continue;
        }
        Log.w("Tango.PushMsgNotifier", "updateConversationMessageNotificationInStatusBar(): cannot get photo for contactId=" + l2);
        localObject2 = null;
      }
      label793: localNotification2 = new Notification(2130837675, str4, l1);
      localNotification2.tickerText = str5;
      localNotification2.setLatestEventInfo(paramContext, str3, str4, localPendingIntent2);
      localNotification2.defaults = (0x2 | localNotification2.defaults);
      localNotification2.number = j;
      localNotification2.flags = (0x10 | localNotification2.flags);
    }
  }

  public void notifyPushMessageInStatusBar(String paramString1, String paramString2, Bundle paramBundle)
  {
    Log.d("Tango.PushMsgNotifier", "notifyPushMessageInStatusBar()");
    TangoApp localTangoApp = this.m_application;
    Resources localResources = localTangoApp.getResources();
    if (TextUtils.isEmpty(paramString1));
    for (String str = localResources.getString(2131296396); ; str = paramString1)
    {
      Intent localIntent = new Intent(TangoApp.getInstance(), PopupNotification.class);
      localIntent.putExtra("title", str);
      localIntent.putExtra("body", paramString2);
      localIntent.putExtra("actioninfo", paramBundle);
      localIntent.setFlags(268697600);
      PendingIntent localPendingIntent = PendingIntent.getActivity(localTangoApp, 0, localIntent, 134217728);
      Notification localNotification = new Notification(2130837675, str, 0L);
      localNotification.setLatestEventInfo(localTangoApp, str, paramString2, localPendingIntent);
      localNotification.flags = (0x10 | localNotification.flags);
      TangoApp.getNotificationManager().notify(3, localNotification);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.PushMsgNotifier
 * JD-Core Version:    0.6.2
 */