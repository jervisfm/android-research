package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import com.sgiggle.media_engine.MediaEngineMessage.StartSMSComposeMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.SMSComposerPayload;

public class QuerySendSMSDialogActivity extends ActivityBase
{
  private static final String TAG = "Tango.QuerySendSMSDialogActivity";
  static int VALIDATION_FAILED_DIALOG = 0;
  private MediaEngineMessage.StartSMSComposeMessage message;

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.QuerySendSMSDialogActivity", "onCreate()");
    super.onCreate(paramBundle);
    if (getFirstMessage() != null)
    {
      this.message = ((MediaEngineMessage.StartSMSComposeMessage)getFirstMessage());
      showDialog(VALIDATION_FAILED_DIALOG);
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Log.v("Tango.QuerySendSMSDialogActivity", "onCreateDialog()");
    return new Builder(this).create((SessionMessages.SMSComposerPayload)this.message.payload());
  }

  public static class Builder extends AlertDialog.Builder
  {
    private QuerySendSMSDialogActivity activity;
    private boolean askToSendSms;
    private SessionMessages.SMSComposerPayload payload;

    public Builder(Context paramContext)
    {
      super();
      this.activity = ((QuerySendSMSDialogActivity)paramContext);
    }

    private void postStartSMSComposeMessage(boolean paramBoolean)
    {
      MediaEngineMessage.StartSMSComposeMessage localStartSMSComposeMessage = new MediaEngineMessage.StartSMSComposeMessage(this.payload.getType(), paramBoolean, this.payload.getReceiversList(), this.payload.getInfo());
      MessageRouter.getInstance().postMessage("jingle", localStartSMSComposeMessage);
    }

    public AlertDialog create(SessionMessages.SMSComposerPayload paramSMSComposerPayload)
    {
      this.payload = paramSMSComposerPayload;
      this.askToSendSms = paramSMSComposerPayload.getAskToSendSms();
      String str1 = paramSMSComposerPayload.getDialogTitle();
      String str2 = paramSMSComposerPayload.getDialogMessage();
      setIcon(2130837650).setTitle(str1).setMessage(str2).setCancelable(true).setPositiveButton(2131296287, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          QuerySendSMSDialogActivity.Builder.this.postStartSMSComposeMessage(QuerySendSMSDialogActivity.Builder.this.askToSendSms);
          paramAnonymousDialogInterface.cancel();
        }
      });
      if (this.askToSendSms)
        setNegativeButton(2131296288, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            QuerySendSMSDialogActivity.Builder.this.postStartSMSComposeMessage(false);
            paramAnonymousDialogInterface.cancel();
          }
        });
      setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          QuerySendSMSDialogActivity.this.finish();
        }
      });
      return super.create();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.QuerySendSMSDialogActivity
 * JD-Core Version:    0.6.2
 */