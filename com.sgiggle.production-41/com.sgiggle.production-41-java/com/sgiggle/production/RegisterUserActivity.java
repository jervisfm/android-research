package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.android.Facebook;
import com.facebook.android.LoginButton;
import com.facebook.android.LoginButton.ButtonOnClickListener;
import com.facebook.android.SessionEvents;
import com.facebook.android.SessionEvents.AuthListener;
import com.facebook.android.SessionStore;
import com.sgiggle.contacts.Contact;
import com.sgiggle.contacts.ContactStore;
import com.sgiggle.corefacade.telephony.PhoneFormatter;
import com.sgiggle.iphelper.IpHelper;
import com.sgiggle.media_engine.MediaEngineMessage.AllowAccessAddressBookMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayRegisterUserEvent;
import com.sgiggle.media_engine.MediaEngineMessage.EndStateNoChangeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.FBDidLoginEvent;
import com.sgiggle.media_engine.MediaEngineMessage.FBDidLoginMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RegisterUserMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RegisterUserNoNetworkEvent;
import com.sgiggle.media_engine.MediaEngineMessage.SavePersonalInfoMessage;
import com.sgiggle.media_engine.MediaEngineMessage.StatsCollectorLogMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.CountryCode;
import com.sgiggle.xmpp.SessionMessages.KeyValuePair;
import com.sgiggle.xmpp.SessionMessages.KeyValuePair.Builder;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;
import com.sgiggle.xmpp.SessionMessages.RegisterUserPayload;
import com.sgiggle.xmpp.SessionMessages.RegistrationOptions;
import com.sgiggle.xmpp.SessionMessages.RegistrationOptions.PrefillContactInfo;
import com.sgiggle.xmpp.SessionMessages.RegistrationOptions.RegistrationLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RegisterUserActivity extends ActivityBase
  implements View.OnClickListener, View.OnTouchListener
{
  private static final int CHOOSE_COUNTRY_CODE_REQUEST = 0;
  private static final int FB_DEFAULT_AUTH_ACTIVITY_CODE = 32665;
  private static final boolean FORCE_FIXED_SIGNUP_BUTTONS = true;
  private static final String KEY_ACCOUNT_INFO_VALIDATION_ERROR_REASON = "account_info_validation_error_reason";
  private static final String KEY_ACCOUNT_SCREEN_UI = "account_screen_ui";
  private static final String KEY_DEVICE_ID = "deviceid";
  private static final String KEY_FB_FAIL_REASON = "fb_fail_reason";
  private static final String KEY_FB_INIT_REASON = "fb_init_reason";
  private static final String KEY_FB_VALID_SESSION = "fb_valid_session";
  private static final String KEY_NETWORK_ERROR_REASON = "network_error_reason";
  private static final String KEY_OPTIONAL_EMAIL_DIALOG_REASON = "optional_email_dialog_reason";
  private static final int MIN_PHONE_NUMBER_LENGTH = 5;
  private static final String PERSONAL_INFO_PREFIX = "personal_info_";
  private static final String REASON_ACCOUNT_INFO_VALIDATION_ERROR_EMAIL_INVALID = "email_invalid";
  private static final String REASON_ACCOUNT_INFO_VALIDATION_ERROR_EMPTY_PHONE = "empty_phone";
  private static final String REASON_ACCOUNT_INFO_VALIDATION_ERROR_PHONE_INVALID = "phone_invalid";
  private static final String REASON_FB_FAIL_FB_AUTH_FAILED = "fb_auth_failed";
  private static final String REASON_FB_FAIL_FB_AUTH_SUCCESS_SESSION_INVALID = "fb_auth_success_session_invalid";
  private static final String REASON_FB_INIT_FB_AUTH_SUCCESS = "fb_auth_success";
  private static final String REASON_FB_INIT_FB_SESSION_VALID_ON_INIT = "fb_session_valid_on_init";
  private static final String REASON_NETWORK_ERROR_NETWORK_TIMEOUT = "network_timeout";
  private static final String REASON_NETWORK_ERROR_NO_NETWORK = "no_network";
  private static final String REASON_OPTIONAL_EMAIL_DIALOG_ADD_EMAIL = "add_email";
  private static final String REASON_OPTIONAL_EMAIL_DIALOG_APPEARED = "dialog_appeared";
  private static final String REASON_OPTIONAL_EMAIL_DIALOG_SKIP_EMAIL = "skip_email";
  private static final String REGISTRATION_PREFIX = "reg_";
  private static final String TAG = "Tango.RegisterProfileUI";
  private static final String VALUE_ACCOUNT_INFO_VALIDATION_ERROR = "account_info_validation_error";
  private static final String VALUE_ATTEMPTING_FB_ME_QUERY = "attempting_fb_me_query";
  private static final String VALUE_ATTEMPTING_FB_ME_QUERY_FAILED = "fb_me_query_failed";
  private static final String VALUE_FB_LOGIN_CLICKED = "fb_login_clicked";
  private static final String VALUE_FB_ME_QUERY_COMPLETE_EVENT = "fb_me_query_complete_event";
  private static final String VALUE_OPTIONAL_EMAIL_DIALOG = "optional_email_dialog";
  private static final String VALUE_REGISTRATION_NETWORK_ERROR = "registration_network_error";
  private static final String VALUE_SCREEN_APPEARED = "screen_appeared";
  private static final String VALUE_SUBMIT_CLICKED = "submit_clicked";
  private static final String VALUE_VALIDATION_OK_REGISTERING = "validation_OK_registering";
  private FacebookLoginButton m_FBLoginButton;
  private boolean m_autoFill_Available = false;
  private String m_autoFill_Email;
  private String m_autoFill_Firstname;
  private String m_autoFill_Lastname;
  private String m_autoFill_SubscriberNumber;
  private Button m_bottomSignUpButton;
  private Button m_cancelButton;
  private String m_countryCode;
  private TextView m_countryCodeText;
  private String m_countryId;
  private String m_countryName;
  private EditText m_emailText;
  private LinearLayout m_fb_ll_remove_when_login = null;
  private String[] m_fb_permissions = { "email", "publish_actions", "user_birthday" };
  private LinearLayout m_fb_signup_bottom_ll;
  private LinearLayout m_fb_signup_orig_ll;
  private LinearLayout m_fb_signup_top_ll;
  private EditText m_firstNameEditText;
  private LinearLayout m_fixedSignUpButtonLayout;
  private String m_isoCountryCode;
  private EditText m_lastNameEditText;
  private View.OnFocusChangeListener m_onFocusChangeListener = new View.OnFocusChangeListener()
  {
    public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
    {
      if ((paramAnonymousView == RegisterUserActivity.this.m_phoneNumberEditText) && (!paramAnonymousBoolean) && (RegisterUserActivity.this.m_subscriberNumber != null) && (RegisterUserActivity.this.m_subscriberNumber.length() > 0) && (!RegisterUserActivity.this.m_subscriberNumber.equals(RegisterUserActivity.this.m_autoFill_SubscriberNumber)) && (RegisterUserActivity.this.m_prefillContactInfo == SessionMessages.RegistrationOptions.PrefillContactInfo.PREFILL_ENABLE))
      {
        RegisterUserActivity.this.clearAndResetAutoFillData();
        RegisterUserActivity.this.prepareAutoFillWithNumber(RegisterUserActivity.this.m_subscriberNumber);
      }
      while ((!paramAnonymousBoolean) || (!RegisterUserActivity.this.m_autoFill_Available) || (RegisterUserActivity.this.m_prefillContactInfo != SessionMessages.RegistrationOptions.PrefillContactInfo.PREFILL_ENABLE) || ((paramAnonymousView != RegisterUserActivity.this.m_firstNameEditText) && (paramAnonymousView != RegisterUserActivity.this.m_lastNameEditText) && (paramAnonymousView != RegisterUserActivity.this.m_emailText)))
        return;
      RegisterUserActivity.this.doAutoFillData();
    }
  };
  private EditText m_phoneNumberEditText;
  private TextWatcher m_phoneTextWatcher;
  private SessionMessages.RegistrationOptions.PrefillContactInfo m_prefillContactInfo = SessionMessages.RegistrationOptions.PrefillContactInfo.PREFILL_ENABLE;
  private ViewGroup m_privacyLayout;
  private LinearLayout m_profileBottomButtonsLayout;
  private ProgressBar m_progressBar;
  private ProgressDialog m_progressDialog;
  private LinearLayout m_reg_linear_layout;
  private RelativeLayout m_reg_root_layout;
  private LinearLayout m_registerBottomButtonsLayout;
  private SessionMessages.RegistrationOptions.RegistrationLayout m_registrationLayout = SessionMessages.RegistrationOptions.RegistrationLayout.TANGO_BOTTOM;
  private Button m_saveButton;
  private ArrayList<CountryListActivity.CountryData> m_selectableCountries;
  private Button m_signUpButton;
  private SignUpButtonLayoutListener m_signUpButtonLayoutListener;
  private CheckBox m_storeContacts;
  private String m_subscriberNumber;
  private TextView m_title;
  private ViewMode m_viewMode = ViewMode.VIEW_MODE_REGISTER;

  private void clearAndResetAutoFillData()
  {
    this.m_autoFill_Available = false;
    this.m_autoFill_SubscriberNumber = "";
    this.m_autoFill_Firstname = "";
    this.m_autoFill_Lastname = "";
    this.m_autoFill_Email = "";
  }

  private void closeProgressDialog()
  {
    if (this.m_progressDialog != null)
    {
      this.m_progressDialog.dismiss();
      this.m_progressDialog = null;
    }
  }

  private String displayCountryCode(String paramString1, String paramString2)
  {
    return this.m_isoCountryCode.toUpperCase() + " " + "+" + paramString1;
  }

  private void displayRegistrationNetworkFailure(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder().append("displayRegistrationNetworkFailure(networkFailureCause == ");
    String str1;
    String str2;
    label57: String str3;
    label72: AlertDialog.Builder localBuilder;
    if ((paramString == null) || (paramString.length() == 0))
    {
      str1 = "NO NETWORK)";
      Log.w("Tango.RegisterProfileUI", str1);
      if (this.m_viewMode != ViewMode.VIEW_MODE_REGISTER)
        break label171;
      str2 = "reg_";
      if ((paramString != null) && (paramString.length() != 0))
        break label178;
      str3 = "no_network";
      statsCollectorLog(str2, "registration_network_error", "network_error_reason", str3);
      closeProgressDialog();
      localBuilder = new AlertDialog.Builder(this);
      localBuilder.setTitle(2131296346);
      if ((paramString != null) && (paramString.length() != 0))
        break label185;
    }
    label171: label178: label185: for (int i = 2131296347; ; i = 2131296348)
    {
      localBuilder.setMessage(i);
      localBuilder.setCancelable(false);
      localBuilder.setPositiveButton(2131296349, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
        }
      });
      localBuilder.create().show();
      return;
      str1 = "TIMED OUT DUE TO NETWORK)";
      break;
      str2 = "personal_info_";
      break label57;
      str3 = "network_timeout";
      break label72;
    }
  }

  private void displaySaveFailed(saveFailedReason paramsaveFailedReason)
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setTitle(2131296350);
    String str = "";
    switch (8.$SwitchMap$com$sgiggle$production$RegisterUserActivity$saveFailedReason[paramsaveFailedReason.ordinal()])
    {
    default:
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      localBuilder.setMessage(str);
      localBuilder.setCancelable(false);
      localBuilder.setPositiveButton(2131296349, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
        }
      });
      localBuilder.create().show();
      return;
      str = getResources().getString(2131296351);
      continue;
      str = getResources().getString(2131296352);
      continue;
      str = getResources().getString(2131296353);
    }
  }

  private void doAutoFillData()
  {
    if (this.m_autoFill_Available)
    {
      Log.d("Tango.RegisterProfileUI", "Pre-populate Auto-Fill data...");
      if ((TextUtils.isEmpty(this.m_firstNameEditText.getText().toString())) && (TextUtils.isEmpty(this.m_lastNameEditText.getText().toString())))
      {
        this.m_firstNameEditText.setText(this.m_autoFill_Firstname);
        this.m_lastNameEditText.setText(this.m_autoFill_Lastname);
      }
      if (TextUtils.isEmpty(this.m_emailText.getText().toString()))
        this.m_emailText.setText(this.m_autoFill_Email);
      this.m_autoFill_Available = false;
    }
  }

  private void doFBLogin()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.FBDidLoginMessage(TangoApp.getInstance().getFacebook().getAccessToken(), TangoApp.getInstance().getFacebook().getAccessExpires() / 1000L));
    this.m_reg_linear_layout.removeView(this.m_fb_ll_remove_when_login);
    this.m_progressBar.setVisibility(0);
  }

  private void formatPhoneNumber()
  {
    String str = PhoneFormatter.format(this.m_subscriberNumber, this.m_countryCode);
    this.m_phoneNumberEditText.removeTextChangedListener(this.m_phoneTextWatcher);
    Editable localEditable = this.m_phoneNumberEditText.getText();
    localEditable.replace(0, localEditable.length(), str);
    this.m_phoneNumberEditText.addTextChangedListener(this.m_phoneTextWatcher);
  }

  private void initializeFacebook(SessionMessages.RegistrationOptions paramRegistrationOptions)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.addAll(paramRegistrationOptions.getFacebookUserAndFriendsPermissionsList());
    localArrayList.addAll(paramRegistrationOptions.getFacebookExtendedPermissionsList());
    localArrayList.addAll(paramRegistrationOptions.getFacebookOpenGraphPermissionsList());
    if (!localArrayList.isEmpty())
      this.m_fb_permissions = ((String[])localArrayList.toArray(new String[0]));
    SessionStore.restore(TangoApp.getInstance().getFacebook(), this);
    setupFBABTesting();
    if (this.m_FBLoginButton != null)
      this.m_FBLoginButton.init(this, TangoApp.getInstance().getFacebook(), this.m_fb_permissions, false);
    if (TangoApp.getInstance().getFacebook().isSessionValid())
    {
      if (this.m_viewMode == ViewMode.VIEW_MODE_REGISTER);
      for (String str = "reg_"; ; str = "personal_info_")
      {
        statsCollectorLog(str, "attempting_fb_me_query", "fb_init_reason", "fb_session_valid_on_init");
        doFBLogin();
        return;
      }
    }
    SessionEvents.addAuthListener(new SessionEvents.AuthListener()
    {
      public void onAuthFail(String paramAnonymousString)
      {
        RegisterUserActivity localRegisterUserActivity = RegisterUserActivity.this;
        if (RegisterUserActivity.this.m_viewMode == RegisterUserActivity.ViewMode.VIEW_MODE_REGISTER);
        for (String str = "reg_"; ; str = "personal_info_")
        {
          localRegisterUserActivity.statsCollectorLog(str, "fb_me_query_failed", "fb_fail_reason", "fb_auth_failed");
          return;
        }
      }

      public void onAuthSucceed()
      {
        if (TangoApp.getInstance().getFacebook().isSessionValid())
        {
          RegisterUserActivity localRegisterUserActivity2 = RegisterUserActivity.this;
          if (RegisterUserActivity.this.m_viewMode == RegisterUserActivity.ViewMode.VIEW_MODE_REGISTER);
          for (String str2 = "reg_"; ; str2 = "personal_info_")
          {
            localRegisterUserActivity2.statsCollectorLog(str2, "attempting_fb_me_query", "fb_init_reason", "fb_auth_success");
            RegisterUserActivity.this.doFBLogin();
            return;
          }
        }
        RegisterUserActivity localRegisterUserActivity1 = RegisterUserActivity.this;
        if (RegisterUserActivity.this.m_viewMode == RegisterUserActivity.ViewMode.VIEW_MODE_REGISTER);
        for (String str1 = "reg_"; ; str1 = "personal_info_")
        {
          localRegisterUserActivity1.statsCollectorLog(str1, "fb_me_query_failed", "fb_fail_reason", "fb_auth_success_session_invalid");
          return;
        }
      }
    });
  }

  private boolean isFacebookSupported()
  {
    if (this.m_registrationLayout == null)
      return false;
    return (this.m_registrationLayout == SessionMessages.RegistrationOptions.RegistrationLayout.TANGO_BOTTOM_AND_FB_BOTTOM) || (this.m_registrationLayout == SessionMessages.RegistrationOptions.RegistrationLayout.TANGO_BOTTOM_AND_FB_TOP);
  }

  private void populateFieldsFromEvent(SessionMessages.RegisterUserPayload paramRegisterUserPayload, boolean paramBoolean)
  {
    SessionMessages.Contact localContact = paramRegisterUserPayload.getContact();
    if ((TextUtils.isEmpty(this.m_firstNameEditText.getText().toString())) && (TextUtils.isEmpty(this.m_lastNameEditText.getText().toString())))
    {
      if (localContact.hasFirstname())
        this.m_firstNameEditText.setText(localContact.getFirstname());
      if (localContact.hasLastname())
        this.m_lastNameEditText.setText(localContact.getLastname());
    }
    if ((TextUtils.isEmpty(this.m_emailText.getText().toString())) && (localContact.hasEmail()))
      this.m_emailText.setText(localContact.getEmail());
    if ((localContact.hasPhoneNumber()) && (paramBoolean))
    {
      if (TextUtils.isEmpty(this.m_countryCodeText.getText().toString()))
      {
        SessionMessages.CountryCode localCountryCode = localContact.getPhoneNumber().getCountryCode();
        this.m_countryId = localCountryCode.getCountryid();
        this.m_countryCode = localCountryCode.getCountrycodenumber();
        this.m_countryName = localCountryCode.getCountryname();
        this.m_isoCountryCode = localCountryCode.getCountryisocc();
        this.m_countryCodeText.setText(displayCountryCode(this.m_countryCode, this.m_isoCountryCode));
      }
      if (TextUtils.isEmpty(this.m_phoneNumberEditText.getText().toString()))
      {
        this.m_subscriberNumber = localContact.getPhoneNumber().getSubscriberNumber();
        this.m_phoneNumberEditText.setText(this.m_subscriberNumber);
        this.m_autoFill_SubscriberNumber = this.m_subscriberNumber;
      }
    }
    this.m_selectableCountries = CountryListActivity.buildSelectableCountriesFromList(paramRegisterUserPayload.getCountryCodeList());
    this.m_storeContacts.setChecked(paramRegisterUserPayload.getStoreAddressBook());
  }

  private void prePopulatePhoneNumber()
  {
    TelephonyManager localTelephonyManager = (TelephonyManager)getSystemService("phone");
    if (localTelephonyManager != null)
    {
      String str = localTelephonyManager.getLine1Number();
      if ((str != null) && (str.length() > 0))
      {
        Log.d("Tango.RegisterProfileUI", "... prePopulatePhoneNumber: [" + str + "] [" + this.m_countryCode + "]");
        if (str.startsWith("+" + this.m_countryCode))
          str = str.substring(1 + this.m_countryCode.length());
        if (!TextUtils.isEmpty(this.m_phoneNumberEditText.getText().toString()))
          break label165;
        this.m_subscriberNumber = str;
        this.m_phoneNumberEditText.setText(this.m_subscriberNumber);
        prepareAutoFillWithNumber(this.m_subscriberNumber);
      }
    }
    while (true)
    {
      doAutoFillData();
      return;
      label165: prepareAutoFillWithNumber(this.m_phoneNumberEditText.getText().toString());
    }
  }

  private void prepareAutoFillWithNumber(String paramString)
  {
    Log.d("Tango.RegisterProfileUI", "prepareAutoFillWithNumber(" + paramString + ")");
    Contact localContact = ContactStore.getContactByNumber(paramString);
    if (localContact != null)
    {
      Log.d("Tango.RegisterProfileUI", "Prepare Auto-Fill: [" + localContact.firstName + " " + localContact.lastName + "]");
      this.m_autoFill_Available = true;
      this.m_autoFill_SubscriberNumber = paramString;
      this.m_autoFill_Firstname = localContact.firstName;
      this.m_autoFill_Lastname = localContact.lastName;
      if ((localContact.emailAddresses != null) && (localContact.emailAddresses.length > 0))
        this.m_autoFill_Email = localContact.emailAddresses[0];
    }
    else
    {
      return;
    }
    this.m_autoFill_Email = "";
  }

  private void setupFBABTesting()
  {
    this.m_reg_linear_layout.removeView(this.m_fb_signup_orig_ll);
    this.m_reg_linear_layout.removeView(this.m_fb_signup_top_ll);
    this.m_reg_linear_layout.removeView(this.m_fb_signup_bottom_ll);
    switch (8.$SwitchMap$com$sgiggle$xmpp$SessionMessages$RegistrationOptions$RegistrationLayout[this.m_registrationLayout.ordinal()])
    {
    default:
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      this.m_fixedSignUpButtonLayout = ((LinearLayout)findViewById(2131361959));
      this.m_signUpButton = ((Button)findViewById(2131361962));
      this.m_signUpButton.setOnClickListener(this);
      this.m_signUpButtonLayoutListener = new SignUpButtonLayoutListener(null);
      this.m_reg_root_layout.getViewTreeObserver().addOnGlobalLayoutListener(this.m_signUpButtonLayoutListener);
      toggleSignUpButton(isShowFixedSignUpButton());
      return;
      this.m_reg_linear_layout.addView(this.m_fb_signup_orig_ll);
      this.m_fb_ll_remove_when_login = this.m_fb_signup_orig_ll;
      continue;
      this.m_reg_linear_layout.addView(this.m_fb_signup_top_ll, 0);
      this.m_reg_linear_layout.addView(this.m_fb_signup_orig_ll);
      this.m_FBLoginButton = ((FacebookLoginButton)findViewById(2131361961));
      this.m_fb_ll_remove_when_login = this.m_fb_signup_top_ll;
      continue;
      this.m_reg_linear_layout.addView(this.m_fb_signup_orig_ll);
      this.m_reg_linear_layout.addView(this.m_fb_signup_bottom_ll);
      this.m_FBLoginButton = ((FacebookLoginButton)findViewById(2131361961));
      this.m_fb_ll_remove_when_login = this.m_fb_signup_bottom_ll;
    }
  }

  private void showProgressDialog()
  {
    if (this.m_viewMode == ViewMode.VIEW_MODE_REGISTER);
    for (int i = 2131296319; ; i = 2131296322)
    {
      this.m_progressDialog = ProgressDialog.show(this, "", getResources().getString(i), true);
      return;
    }
  }

  private void startSMSListener()
  {
    Log.v("Tango.RegisterProfileUI", "Start SMS Listener");
    TangoApp.getInstance().registerSMSReceiver();
    TangoApp.getInstance().scheduleSMSReceiverTimer();
  }

  private void statsCollectorLog(String paramString1, String paramString2)
  {
    statsCollectorLog(paramString1, paramString2, null, null);
  }

  private void statsCollectorLog(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(SessionMessages.KeyValuePair.newBuilder().setKey(paramString1 + "account_screen_ui").setValue(paramString2).build());
    localArrayList.add(SessionMessages.KeyValuePair.newBuilder().setKey("deviceid").setValue(IpHelper.getDevIDBase64()).build());
    SessionMessages.KeyValuePair.Builder localBuilder = SessionMessages.KeyValuePair.newBuilder().setKey("fb_valid_session");
    if (TangoApp.getInstance().getFacebook().isSessionValid());
    for (String str = "1"; ; str = "0")
    {
      localArrayList.add(localBuilder.setValue(str).build());
      if (paramString3 != null)
        localArrayList.add(SessionMessages.KeyValuePair.newBuilder().setKey(paramString3).setValue(paramString4).build());
      MediaEngineMessage.StatsCollectorLogMessage localStatsCollectorLogMessage = new MediaEngineMessage.StatsCollectorLogMessage(localArrayList);
      MessageRouter.getInstance().postMessage("jingle", localStatsCollectorLogMessage);
      return;
    }
  }

  private void stopCurrentSMSListener()
  {
    Log.v("Tango.RegisterProfileUI", "Stop current SMS Listener");
    TangoApp.getInstance().cancelSMSReceiverTimer();
    TangoApp.getInstance().unregisterSMSReceiver();
  }

  protected void doRegisterUser(ViewMode paramViewMode, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    String str1;
    String str2;
    if (this.m_viewMode == ViewMode.VIEW_MODE_REGISTER)
    {
      str1 = "reg_";
      statsCollectorLog(str1, "validation_OK_registering");
      str2 = Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry();
      if (paramViewMode != ViewMode.VIEW_MODE_REGISTER)
        break label137;
    }
    label137: for (Object localObject = new MediaEngineMessage.RegisterUserMessage(paramString1, paramString2, this.m_countryId, this.m_countryCode, this.m_isoCountryCode, this.m_countryName, paramString3, paramString4, str2, paramBoolean); ; localObject = new MediaEngineMessage.SavePersonalInfoMessage(paramString1, paramString2, this.m_countryId, this.m_countryCode, this.m_isoCountryCode, this.m_countryName, paramString3, paramString4, str2, paramBoolean))
    {
      stopCurrentSMSListener();
      startSMSListener();
      TangoApp.getInstance().startWakeupAlarmService();
      showProgressDialog();
      MessageRouter.getInstance().postMessage("jingle", (Message)localObject);
      return;
      str1 = "personal_info_";
      break;
    }
  }

  public void handleNewMessage(Message paramMessage)
  {
    Log.d("Tango.RegisterProfileUI", "handleNewMessage(): Message = " + paramMessage);
    if (paramMessage == null)
    {
      TangoApp.getInstance().onActivityResumeAfterKilled(this);
      return;
    }
    switch (paramMessage.getType())
    {
    default:
      super.handleNewMessage(paramMessage);
      return;
    case 35031:
    case 35320:
      this.m_viewMode = ViewMode.VIEW_MODE_REGISTER;
      this.m_title.setText(2131296268);
      this.m_privacyLayout.setVisibility(8);
      this.m_profileBottomButtonsLayout.setVisibility(8);
      this.m_progressBar.setVisibility(8);
      closeProgressDialog();
      if (paramMessage.getType() == 35031)
      {
        SessionMessages.RegisterUserPayload localRegisterUserPayload = (SessionMessages.RegisterUserPayload)((MediaEngineMessage.DisplayRegisterUserEvent)paramMessage).payload();
        SessionMessages.RegistrationOptions localRegistrationOptions = localRegisterUserPayload.getRegistrationOptions();
        this.m_registrationLayout = localRegistrationOptions.getRegistrationLayout();
        if (isFacebookSupported())
          initializeFacebook(localRegistrationOptions);
        this.m_prefillContactInfo = localRegistrationOptions.getPrefillContactInfo();
        populateFieldsFromEvent(localRegisterUserPayload, true);
        if (this.m_prefillContactInfo == SessionMessages.RegistrationOptions.PrefillContactInfo.PREFILL_ENABLE)
          prePopulatePhoneNumber();
        statsCollectorLog("reg_", "screen_appeared");
      }
      while (true)
      {
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.AllowAccessAddressBookMessage());
        return;
        populateFieldsFromEvent((SessionMessages.RegisterUserPayload)((MediaEngineMessage.FBDidLoginEvent)paramMessage).payload(), false);
        this.m_autoFill_Available = false;
        statsCollectorLog("reg_", "fb_me_query_complete_event");
      }
    case 35027:
      this.m_viewMode = ViewMode.VIEW_MODE_PROFILE;
      this.m_reg_linear_layout.removeView(this.m_fb_signup_orig_ll);
      this.m_reg_linear_layout.removeView(this.m_fb_signup_top_ll);
      this.m_reg_linear_layout.removeView(this.m_fb_signup_bottom_ll);
      this.m_title.setText(2131296269);
      this.m_privacyLayout.setVisibility(0);
      this.m_fixedSignUpButtonLayout.setVisibility(8);
      this.m_registerBottomButtonsLayout.setVisibility(8);
      this.m_profileBottomButtonsLayout.setVisibility(0);
      closeProgressDialog();
      populateFieldsFromEvent((SessionMessages.RegisterUserPayload)((MediaEngineMessage.DisplayRegisterUserEvent)paramMessage).payload(), true);
      statsCollectorLog("personal_info_", "screen_appeared");
      return;
    case 35090:
    }
    displayRegistrationNetworkFailure(((SessionMessages.OptionalPayload)((MediaEngineMessage.RegisterUserNoNetworkEvent)paramMessage).payload()).getMessage());
  }

  public boolean isInRegisterViewMode()
  {
    return this.m_viewMode == ViewMode.VIEW_MODE_REGISTER;
  }

  protected boolean isShowFixedSignUpButton()
  {
    return true;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Log.d("Tango.RegisterProfileUI", "onActivityResult(): requestCode = " + paramInt1 + ", resultCode = " + paramInt2);
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 0) && (paramInt2 == -1))
    {
      CountryListActivity.CountryData localCountryData = (CountryListActivity.CountryData)paramIntent.getParcelableExtra("selectedCountry");
      this.m_countryId = localCountryData.m_cid;
      this.m_countryCode = localCountryData.m_code;
      this.m_countryName = localCountryData.m_name;
      this.m_isoCountryCode = localCountryData.m_isocc;
      Log.v("Tango.RegisterProfileUI", "... Selected: Id = " + this.m_countryId + ", Country = " + this.m_countryCode + " (" + this.m_countryName + ")");
      this.m_countryCodeText.setText(displayCountryCode(this.m_countryCode, this.m_isoCountryCode));
      formatPhoneNumber();
    }
    if (paramInt1 == 32665)
      TangoApp.getInstance().getFacebook().authorizeCallback(paramInt1, paramInt2, paramIntent);
  }

  public void onBackPressed()
  {
    if (this.m_viewMode == ViewMode.VIEW_MODE_REGISTER)
    {
      TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
      super.onBackPressed();
      return;
    }
    Log.d("Tango.RegisterProfileUI", "onBackPressed(). Send 'No-State-Change' message...");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.EndStateNoChangeMessage());
  }

  public void onClick(View paramView)
  {
    int i = paramView.getId();
    Log.d("Tango.RegisterProfileUI", "onClick(View " + paramView + ", id " + i + ")...");
    switch (i)
    {
    default:
      Log.w("Tango.RegisterProfileUI", "onClick: unexpected click: View " + paramView + ", id " + i);
    case 2131362045:
      return;
    case 2131361962:
    case 2131362061:
    case 2131362063:
      String str1;
      final String str2;
      final String str3;
      final String str4;
      final String str5;
      final boolean bool;
      if (this.m_viewMode == ViewMode.VIEW_MODE_REGISTER)
      {
        str1 = "reg_";
        statsCollectorLog(str1, "submit_clicked");
        str2 = this.m_firstNameEditText.getText().toString();
        str3 = this.m_lastNameEditText.getText().toString();
        str4 = this.m_phoneNumberEditText.getText().toString();
        str5 = this.m_emailText.getText().toString();
        bool = this.m_storeContacts.isChecked();
        if (str4.length() != 0)
          break label272;
        if (this.m_viewMode != ViewMode.VIEW_MODE_REGISTER)
          break label265;
      }
      label265: for (String str9 = "reg_"; ; str9 = "personal_info_")
      {
        statsCollectorLog(str9, "account_info_validation_error", "account_info_validation_error_reason", "empty_phone");
        displaySaveFailed(saveFailedReason.PHONE_EMPTY);
        return;
        str1 = "personal_info_";
        break;
      }
      label272: int j = 0;
      int k = 0;
      while (j < str4.length())
      {
        if (Character.isDigit(str4.charAt(j)))
          k++;
        j++;
      }
      if (k < 5)
      {
        if (this.m_viewMode == ViewMode.VIEW_MODE_REGISTER);
        for (String str8 = "reg_"; ; str8 = "personal_info_")
        {
          statsCollectorLog(str8, "account_info_validation_error", "account_info_validation_error_reason", "phone_invalid");
          displaySaveFailed(saveFailedReason.PHONE_INVALID);
          return;
        }
      }
      if ((str5.length() > 0) && (!Utils.isEmailStringValid(str5)))
      {
        if (this.m_viewMode == ViewMode.VIEW_MODE_REGISTER);
        for (String str7 = "reg_"; ; str7 = "personal_info_")
        {
          statsCollectorLog(str7, "account_info_validation_error", "account_info_validation_error_reason", "email_invalid");
          displaySaveFailed(saveFailedReason.EMAIL_INVALID);
          return;
        }
      }
      if (str5.length() == 0)
      {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setTitle(2131296643);
        localBuilder.setMessage(2131296644);
        localBuilder.setNegativeButton(2131296645, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            RegisterUserActivity localRegisterUserActivity = RegisterUserActivity.this;
            if (RegisterUserActivity.this.m_viewMode == RegisterUserActivity.ViewMode.VIEW_MODE_REGISTER);
            for (String str = "reg_"; ; str = "personal_info_")
            {
              localRegisterUserActivity.statsCollectorLog(str, "optional_email_dialog", "optional_email_dialog_reason", "add_email");
              RegisterUserActivity.this.m_emailText.requestFocus();
              return;
            }
          }
        });
        localBuilder.setPositiveButton(2131296284, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            RegisterUserActivity localRegisterUserActivity = RegisterUserActivity.this;
            if (RegisterUserActivity.this.m_viewMode == RegisterUserActivity.ViewMode.VIEW_MODE_REGISTER);
            for (String str = "reg_"; ; str = "personal_info_")
            {
              localRegisterUserActivity.statsCollectorLog(str, "optional_email_dialog", "optional_email_dialog_reason", "skip_email");
              RegisterUserActivity.this.doRegisterUser(RegisterUserActivity.this.m_viewMode, str2, str3, str4, str5, bool);
              return;
            }
          }
        });
        if (this.m_viewMode == ViewMode.VIEW_MODE_REGISTER);
        for (String str6 = "reg_"; ; str6 = "personal_info_")
        {
          statsCollectorLog(str6, "optional_email_dialog", "optional_email_dialog_reason", "dialog_appeared");
          localBuilder.create().show();
          return;
        }
      }
      doRegisterUser(this.m_viewMode, str2, str3, str4, str5, bool);
      return;
    case 2131362064:
    }
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.EndStateNoChangeMessage());
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.RegisterProfileUI", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903107);
    this.m_reg_root_layout = ((RelativeLayout)findViewById(2131362041));
    this.m_reg_linear_layout = ((LinearLayout)findViewById(2131362043));
    this.m_fb_signup_orig_ll = ((LinearLayout)getLayoutInflater().inflate(2130903078, null));
    this.m_fb_signup_bottom_ll = ((LinearLayout)getLayoutInflater().inflate(2130903077, null));
    this.m_fb_signup_top_ll = ((LinearLayout)getLayoutInflater().inflate(2130903079, null));
    this.m_title = ((TextView)findViewById(2131361841));
    this.m_countryCodeText = ((TextView)findViewById(2131362045));
    this.m_countryCodeText.setOnClickListener(this);
    this.m_countryCodeText.setOnTouchListener(this);
    this.m_phoneNumberEditText = ((EditText)findViewById(2131362046));
    this.m_firstNameEditText = ((EditText)findViewById(2131362050));
    this.m_lastNameEditText = ((EditText)findViewById(2131362052));
    this.m_emailText = ((EditText)findViewById(2131362048));
    this.m_privacyLayout = ((ViewGroup)findViewById(2131362054));
    this.m_storeContacts = ((CheckBox)findViewById(2131362058));
    this.m_registerBottomButtonsLayout = ((LinearLayout)findViewById(2131362060));
    this.m_bottomSignUpButton = ((Button)findViewById(2131362061));
    this.m_bottomSignUpButton.setOnClickListener(this);
    this.m_profileBottomButtonsLayout = ((LinearLayout)findViewById(2131362062));
    this.m_saveButton = ((Button)findViewById(2131362063));
    this.m_saveButton.setOnClickListener(this);
    this.m_cancelButton = ((Button)findViewById(2131362064));
    this.m_cancelButton.setOnClickListener(this);
    this.m_progressBar = ((ProgressBar)findViewById(2131362065));
    setupFBABTesting();
    this.m_phoneTextWatcher = new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        RegisterUserActivity.access$002(RegisterUserActivity.this, RegisterUserActivity.this.m_phoneNumberEditText.getText().toString());
        RegisterUserActivity.this.formatPhoneNumber();
      }

      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }

      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }
    };
    this.m_phoneNumberEditText.addTextChangedListener(this.m_phoneTextWatcher);
    this.m_phoneNumberEditText.setOnFocusChangeListener(this.m_onFocusChangeListener);
    this.m_firstNameEditText.setOnFocusChangeListener(this.m_onFocusChangeListener);
    this.m_lastNameEditText.setOnFocusChangeListener(this.m_onFocusChangeListener);
    this.m_emailText.setOnFocusChangeListener(this.m_onFocusChangeListener);
    TangoApp.getInstance().setRegisterActivityInstance(this);
    handleNewMessage(getFirstMessage());
  }

  protected void onDestroy()
  {
    Log.v("Tango.RegisterProfileUI", "onDestroy()");
    super.onDestroy();
    closeProgressDialog();
    TangoApp.getInstance().setRegisterActivityInstance(null);
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 1)
    {
      if (paramView.getId() == 2131362045)
      {
        Intent localIntent = new Intent(this, CountryListActivity.class);
        localIntent.addFlags(262144);
        localIntent.putExtra("countries", this.m_selectableCountries);
        localIntent.putExtra("selectedCountryId", this.m_countryId);
        startActivityForResult(localIntent, 0);
      }
      return true;
    }
    return false;
  }

  public void onUserInteraction()
  {
    super.onUserInteraction();
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_FOREGROUND);
  }

  protected void toggleSignUpButton(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.m_fixedSignUpButtonLayout.setVisibility(0);
      this.m_registerBottomButtonsLayout.setVisibility(8);
      if ((this.m_registrationLayout == SessionMessages.RegistrationOptions.RegistrationLayout.TANGO_BOTTOM_AND_FB_TOP) && (this.m_fb_signup_top_ll != null))
        this.m_fb_signup_top_ll.setVisibility(0);
    }
    do
    {
      return;
      this.m_fixedSignUpButtonLayout.setVisibility(8);
      this.m_registerBottomButtonsLayout.setVisibility(0);
    }
    while ((this.m_registrationLayout != SessionMessages.RegistrationOptions.RegistrationLayout.TANGO_BOTTOM_AND_FB_TOP) || (this.m_fb_signup_top_ll == null));
    this.m_fb_signup_top_ll.setVisibility(8);
  }

  public static class FacebookLoginButton extends LoginButton
  {
    public FacebookLoginButton(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }

    public void init(RegisterUserActivity paramRegisterUserActivity, Facebook paramFacebook, String[] paramArrayOfString, boolean paramBoolean)
    {
      super.init(paramRegisterUserActivity, paramFacebook, paramArrayOfString, paramBoolean);
      setOnClickListener(new FacebookButtonOnClickListner());
    }

    protected class FacebookButtonOnClickListner extends LoginButton.ButtonOnClickListener
    {
      protected FacebookButtonOnClickListner()
      {
        super();
      }

      public void onClick(View paramView)
      {
        RegisterUserActivity localRegisterUserActivity = (RegisterUserActivity)RegisterUserActivity.FacebookLoginButton.this.mActivity;
        if (RegisterUserActivity.FacebookLoginButton.this.mActivity != null)
          if (localRegisterUserActivity.m_viewMode != RegisterUserActivity.ViewMode.VIEW_MODE_REGISTER)
            break label47;
        label47: for (String str = "reg_"; ; str = "personal_info_")
        {
          localRegisterUserActivity.statsCollectorLog(str, "fb_login_clicked");
          super.onClick(paramView);
          return;
        }
      }
    }
  }

  private class SignUpButtonLayoutListener
    implements ViewTreeObserver.OnGlobalLayoutListener
  {
    private SignUpButtonLayoutListener()
    {
    }

    public void onGlobalLayout()
    {
      RegisterUserActivity.this.toggleSignUpButton(RegisterUserActivity.this.isShowFixedSignUpButton());
    }
  }

  private static enum ViewMode
  {
    static
    {
      VIEW_MODE_PROFILE = new ViewMode("VIEW_MODE_PROFILE", 1);
      ViewMode[] arrayOfViewMode = new ViewMode[2];
      arrayOfViewMode[0] = VIEW_MODE_REGISTER;
      arrayOfViewMode[1] = VIEW_MODE_PROFILE;
    }
  }

  private static enum saveFailedReason
  {
    static
    {
      EMAIL_INVALID = new saveFailedReason("EMAIL_INVALID", 2);
      saveFailedReason[] arrayOfsaveFailedReason = new saveFailedReason[3];
      arrayOfsaveFailedReason[0] = PHONE_EMPTY;
      arrayOfsaveFailedReason[1] = PHONE_INVALID;
      arrayOfsaveFailedReason[2] = EMAIL_INVALID;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.RegisterUserActivity
 * JD-Core Version:    0.6.2
 */