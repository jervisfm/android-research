package com.sgiggle.production;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.sgiggle.util.Log;

public class RotatedTextView extends TextView
{
  private static final String TAG = "RotatedTextView";
  private boolean isBold = false;
  private DisplayMetrics m_displaymetrics = new DisplayMetrics();
  private TextView m_textView;

  public RotatedTextView(Context paramContext)
  {
    super(paramContext);
    this.m_textView = new TextView(paramContext);
  }

  public RotatedTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.m_textView = new TextView(paramContext, paramAttributeSet);
    for (int i = 0; i < paramAttributeSet.getAttributeCount(); i++)
      if ((paramAttributeSet.getAttributeName(i).equalsIgnoreCase("textStyle")) && (paramAttributeSet.getAttributeIntValue(i, 0) == 1))
        this.isBold = true;
  }

  public RotatedTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.m_textView = new TextView(paramContext, paramAttributeSet, paramInt);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    int i = this.m_textView.getWidth();
    Bitmap localBitmap = Bitmap.createBitmap(i, this.m_textView.getHeight(), Bitmap.Config.ARGB_8888);
    if (localBitmap == null)
    {
      Log.e("RotatedTextView", "can't create bitmap2");
      return;
    }
    Canvas localCanvas = new Canvas(localBitmap);
    this.m_textView.draw(localCanvas);
    Matrix localMatrix = new Matrix();
    localMatrix.setRotate(270.0F);
    localMatrix.postTranslate(0.0F, i);
    paramCanvas.drawBitmap(localBitmap, localMatrix, null);
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = this.m_textView.getMeasuredWidth();
    int j = this.m_textView.getMeasuredHeight();
    this.m_textView.layout(0, 0, i, j);
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    this.m_textView.measure(paramInt2, paramInt1);
    int i = this.m_textView.getMeasuredWidth();
    int j = this.m_textView.getMeasuredHeight();
    Activity localActivity = TangoApp.getInstance().getCurrentActivityInstance();
    if (localActivity != null)
    {
      localActivity.getWindowManager().getDefaultDisplay().getMetrics(this.m_displaymetrics);
      int k = this.m_displaymetrics.heightPixels;
      TextPaint localTextPaint = new TextPaint();
      String str = this.m_textView.getText().toString();
      localTextPaint.setTextSize(getTextSize());
      if (this.isBold)
        localTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
      float[] arrayOfFloat = new float[str.length()];
      localTextPaint.getTextWidths(str, 0, str.length(), arrayOfFloat);
      Rect localRect = new Rect();
      localTextPaint.getTextBounds(str, 0, str.length(), localRect);
      int m = 0;
      int n = 0;
      while (m < arrayOfFloat.length)
      {
        n = (int)(n + arrayOfFloat[m]);
        m++;
      }
      int i1 = localRect.bottom - localRect.top;
      if (((n > k) && (j < i1 * 2)) || (i > k) || ((n > 0.85D * k) && (j < i1 * 2)));
      for (int i2 = 1; ; i2 = 0)
      {
        if (i2 != 0)
          j *= 2;
        setMeasuredDimension(j, i);
        return;
      }
    }
    setMeasuredDimension(j, i);
  }

  public void setLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    super.setLayoutParams(paramLayoutParams);
    this.m_textView.setLayoutParams(paramLayoutParams);
  }

  public void setText(CharSequence paramCharSequence, TextView.BufferType paramBufferType)
  {
    if (this.m_textView != null)
    {
      this.m_textView.setText(paramCharSequence);
      requestLayout();
      invalidate();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.RotatedTextView
 * JD-Core Version:    0.6.2
 */