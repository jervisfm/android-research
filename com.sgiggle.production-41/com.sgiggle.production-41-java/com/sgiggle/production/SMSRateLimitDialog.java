package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import com.facebook.android.Facebook;
import com.sgiggle.iphelper.IpHelper;
import com.sgiggle.media_engine.MediaEngineMessage.EndStateNoChangeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.StatsCollectorLogMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.dialog.TangoAlertDialog;
import com.sgiggle.xmpp.SessionMessages.KeyValuePair;
import com.sgiggle.xmpp.SessionMessages.KeyValuePair.Builder;
import java.util.ArrayList;
import java.util.List;

public class SMSRateLimitDialog extends TangoAlertDialog
{
  private static final String KEY_DEVICE_ID = "deviceid";
  private static final String KEY_FB_VALID_SESSION = "fb_valid_session";
  private static final String KEY_SMS_VERIFICATION_DIALOG_UI = "sms_verification_dialog_ui";
  private static final String VALUE_SMS_VERIFICATION_DIALOG_UI_RATE_LIMITED_DIALOG_APPEARED = "rate_limited_dialog_appeared";
  private static final String VALUE_SMS_VERIFICATION_DIALOG_UI_RATE_LIMITED_OK = "rate_limited_OK";

  protected SMSRateLimitDialog(Context paramContext)
  {
    super(paramContext);
  }

  public static class Builder extends AlertDialog.Builder
  {
    public Builder(Context paramContext)
    {
      super();
    }

    private void statsCollectorLog(String paramString)
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(SessionMessages.KeyValuePair.newBuilder().setKey("sms_verification_dialog_ui").setValue(paramString).build());
      localArrayList.add(SessionMessages.KeyValuePair.newBuilder().setKey("deviceid").setValue(IpHelper.getDevIDBase64()).build());
      SessionMessages.KeyValuePair.Builder localBuilder = SessionMessages.KeyValuePair.newBuilder().setKey("fb_valid_session");
      if (TangoApp.getInstance().getFacebook().isSessionValid());
      for (String str = "1"; ; str = "0")
      {
        localArrayList.add(localBuilder.setValue(str).build());
        MediaEngineMessage.StatsCollectorLogMessage localStatsCollectorLogMessage = new MediaEngineMessage.StatsCollectorLogMessage(localArrayList);
        MessageRouter.getInstance().postMessage("jingle", localStatsCollectorLogMessage);
        return;
      }
    }

    public AlertDialog create()
    {
      setTitle(2131296626);
      setMessage(2131296627);
      setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          MediaEngineMessage.EndStateNoChangeMessage localEndStateNoChangeMessage = new MediaEngineMessage.EndStateNoChangeMessage();
          MessageRouter.getInstance().postMessage("jingle", localEndStateNoChangeMessage);
          SMSRateLimitDialog.Builder.this.statsCollectorLog("rate_limited_OK");
        }
      });
      setPositiveButton(2131296287, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          MediaEngineMessage.EndStateNoChangeMessage localEndStateNoChangeMessage = new MediaEngineMessage.EndStateNoChangeMessage();
          MessageRouter.getInstance().postMessage("jingle", localEndStateNoChangeMessage);
          SMSRateLimitDialog.Builder.this.statsCollectorLog("rate_limited_OK");
        }
      });
      statsCollectorLog("rate_limited_dialog_appeared");
      return super.create();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.SMSRateLimitDialog
 * JD-Core Version:    0.6.2
 */