package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.media_engine.MediaEngineMessage.CancelSelectContactMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SelectContactEvent;
import com.sgiggle.media_engine.MediaEngineMessage.SelectContactResultMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.actionbarcompat.ActionBarActivity;
import com.sgiggle.production.actionbarcompat.ActionBarHelper;
import com.sgiggle.production.adapter.ContactListAdapter.ContactListSelection;
import com.sgiggle.production.fragment.SelectContactFooterFragment;
import com.sgiggle.production.fragment.SelectContactFooterFragment.SelectContactFooterFragmentListener;
import com.sgiggle.production.fragment.SelectContactFragment;
import com.sgiggle.production.fragment.SelectContactFragment.SelectContactListener;
import com.sgiggle.production.fragment.SelectContactListFragment;
import com.sgiggle.production.util.ContactThumbnailLoader;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.SelectContactPayload;
import com.sgiggle.xmpp.SessionMessages.SelectContactType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class SelectContactActivity extends ActionBarActivity
  implements RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener, SelectContactFooterFragment.SelectContactFooterFragmentListener, SelectContactFragment.SelectContactListener
{
  private static final int DIALOG_MAX_SELECTION_REACHED = 0;
  public static final int FORWARD_MAX_SELECTION = 200;
  public static final int MAX_SELECTION_INFINITE = -1;
  private static final int MSG_SHOW_FOOTER_FRAGMENT = 0;
  private static final String TAG = "Tango.SelectContactActivity";
  private static SelectContactActivity s_instance;
  private ArrayList<ContactFilterWrapper> m_availableFilterWrappers;
  private ArrayList<ContactFilterWrapper> m_contactFilterWrappers;
  private HashMap<ContactListAdapter.ContactListSelection, ContactFilterWrapper> m_filterWrappersBySelection;
  private SelectContactFooterFragment m_footerFragment;
  private SelectContactFragmentAdapter m_fragmentAdapter;
  private Handler m_handler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage.what == 0)
        SelectContactActivity.this.showFooterFragment(true);
    }
  };
  private int m_maxSelection = -1;
  private boolean m_selectContactEventReceived = false;
  private RadioGroup m_selectContactFilterGroup;
  private SessionMessages.SelectContactPayload m_selectContactPayload;
  private Set<Utils.UIContact> m_selectedContacts;
  private boolean m_selectionDone = false;
  private ViewPager m_viewPager;

  private void cancelSelection()
  {
    if (this.m_selectionDone)
    {
      Log.d("Tango.SelectContactActivity", "cancelSelection: selection already done or canceled, ignoring call.");
      return;
    }
    this.m_selectionDone = true;
    sendCancelSelectContactMessage();
  }

  private static void clearRunningInstance(SelectContactActivity paramSelectContactActivity)
  {
    if (s_instance == paramSelectContactActivity)
      s_instance = null;
  }

  private SelectContactFragment getCurrentFragment()
  {
    int i = this.m_viewPager.getCurrentItem();
    if ((this.m_availableFilterWrappers != null) && (i < this.m_availableFilterWrappers.size()))
      return ((ContactFilterWrapper)this.m_availableFilterWrappers.get(i)).getSelectContactFragment(false);
    return null;
  }

  public static SelectContactActivity getRunningInstance()
  {
    return s_instance;
  }

  private void handleSelectContactEvent(com.sgiggle.messaging.Message paramMessage)
  {
    this.m_selectContactPayload = ((SessionMessages.SelectContactPayload)((MediaEngineMessage.SelectContactEvent)paramMessage).payload());
    ContactStore.ContactOrderPair localContactOrderPair = ContactStore.ContactOrderPair.getFromPhone(this);
    SessionMessages.SelectContactType localSelectContactType;
    boolean bool1;
    boolean bool2;
    boolean bool3;
    label145: Iterator localIterator1;
    if (this.m_selectContactPayload.hasMaxSelection())
    {
      this.m_maxSelection = this.m_selectContactPayload.getMaxSelection();
      Log.d("Tango.SelectContactActivity", "handleSelectContactEvent: maxSelection=" + this.m_maxSelection);
      localSelectContactType = this.m_selectContactPayload.getType();
      Log.d("Tango.SelectContactActivity", "handleSelectContactEvent: selectContactType=" + localSelectContactType);
      switch (3.$SwitchMap$com$sgiggle$xmpp$SessionMessages$SelectContactType[localSelectContactType.ordinal()])
      {
      default:
        bool1 = false;
        bool2 = false;
        bool3 = false;
        this.m_contactFilterWrappers.add(new ContactFilterWrapper(ContactListAdapter.ContactListSelection.ALL, 2131362067, 2131362068, bool3, localContactOrderPair));
        this.m_contactFilterWrappers.add(new ContactFilterWrapper(ContactListAdapter.ContactListSelection.TANGO, 2131362069, 2131362070, bool2, localContactOrderPair));
        this.m_contactFilterWrappers.add(new ContactFilterWrapper(ContactListAdapter.ContactListSelection.FAVORITE, 2131362071, 0, bool1, localContactOrderPair));
        localIterator1 = this.m_contactFilterWrappers.iterator();
      case 1:
      case 2:
      }
    }
    while (true)
    {
      if (!localIterator1.hasNext())
        break label476;
      ContactFilterWrapper localContactFilterWrapper5 = (ContactFilterWrapper)localIterator1.next();
      this.m_filterWrappersBySelection.put(localContactFilterWrapper5.getContactListSelection(), localContactFilterWrapper5);
      if (localContactFilterWrapper5.isAvailable())
      {
        this.m_availableFilterWrappers.add(localContactFilterWrapper5);
        continue;
        Log.d("Tango.SelectContactActivity", "handleSelectContactEvent: no max selection, consider infinite.");
        break;
        Resources localResources2 = getResources();
        int k = this.m_maxSelection;
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = Integer.valueOf(this.m_maxSelection);
        setTitle(localResources2.getQuantityString(2131427335, k, arrayOfObject2));
        bool1 = true;
        bool2 = true;
        bool3 = false;
        break label145;
        if ((this.m_maxSelection == -1) || (this.m_maxSelection > 200))
          this.m_maxSelection = 200;
        Resources localResources1 = getResources();
        int i = this.m_maxSelection;
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = Integer.valueOf(this.m_maxSelection);
        setTitle(localResources1.getQuantityString(2131427335, i, arrayOfObject1));
        bool1 = true;
        bool2 = true;
        bool3 = true;
        break label145;
      }
      findViewById(localContactFilterWrapper5.getFilterId()).setVisibility(8);
      if (localContactFilterWrapper5.getFilterSeparatorId() != 0)
        findViewById(localContactFilterWrapper5.getFilterSeparatorId()).setVisibility(8);
    }
    label476: int j = this.m_availableFilterWrappers.size();
    if (j > 0)
      this.m_selectContactFilterGroup.check(((ContactFilterWrapper)this.m_availableFilterWrappers.get(0)).getFilterId());
    this.m_viewPager.setOffscreenPageLimit(j);
    this.m_fragmentAdapter = new SelectContactFragmentAdapter(getSupportFragmentManager(), j);
    this.m_viewPager.setAdapter(this.m_fragmentAdapter);
    List localList = this.m_selectContactPayload.getContactsList();
    ContactFilterWrapper localContactFilterWrapper1 = (ContactFilterWrapper)this.m_filterWrappersBySelection.get(ContactListAdapter.ContactListSelection.ALL);
    ContactFilterWrapper localContactFilterWrapper2 = (ContactFilterWrapper)this.m_filterWrappersBySelection.get(ContactListAdapter.ContactListSelection.TANGO);
    ContactFilterWrapper localContactFilterWrapper3 = (ContactFilterWrapper)this.m_filterWrappersBySelection.get(ContactListAdapter.ContactListSelection.FAVORITE);
    Iterator localIterator2 = localList.iterator();
    while (localIterator2.hasNext())
    {
      SessionMessages.Contact localContact = (SessionMessages.Contact)localIterator2.next();
      Utils.UIContact localUIContact = Utils.UIContact.convertFromMessageContact(localContact);
      if (localContactFilterWrapper1.isAvailable())
        localContactFilterWrapper1.addContact(localUIContact);
      if ((localContactFilterWrapper2.isAvailable()) && (!TextUtils.isEmpty(localContact.getAccountid())))
        localContactFilterWrapper2.addContact(localUIContact);
      if ((localContactFilterWrapper3.isAvailable()) && (localContact.getFavorite()))
        localContactFilterWrapper3.addContact(localUIContact);
    }
    Iterator localIterator3 = this.m_availableFilterWrappers.iterator();
    while (localIterator3.hasNext())
    {
      ContactFilterWrapper localContactFilterWrapper4 = (ContactFilterWrapper)localIterator3.next();
      localContactFilterWrapper4.sortContacts();
      SelectContactFragment localSelectContactFragment = localContactFilterWrapper4.getSelectContactFragment(false);
      if (localSelectContactFragment != null)
      {
        Log.d("Tango.SelectContactActivity", "handleSelectContactEvent: give contacts for selection=" + localContactFilterWrapper4.getContactListSelection());
        localSelectContactFragment.setContacts(localContactFilterWrapper4.getContacts(), this.m_maxSelection);
      }
    }
    if (isMultiSelection())
    {
      this.m_footerFragment.setSelectContactType(localSelectContactType);
      this.m_handler.sendEmptyMessage(0);
    }
  }

  private boolean isMultiSelection()
  {
    return (this.m_maxSelection > 1) || (this.m_maxSelection == -1);
  }

  private void sendCancelSelectContactMessage()
  {
    Log.d("Tango.SelectContactActivity", "sendCancelSelectContactMessage");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelSelectContactMessage());
  }

  private void sendSelectContactResultMessage()
  {
    this.m_selectionDone = true;
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.m_selectedContacts.iterator();
    while (localIterator.hasNext())
      localArrayList.add(((Utils.UIContact)localIterator.next()).convertToMessageContact());
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.SelectContactResultMessage(this.m_selectContactPayload.getType(), localArrayList));
  }

  private static void setRunningInstance(SelectContactActivity paramSelectContactActivity)
  {
    s_instance = paramSelectContactActivity;
  }

  private void showFooterFragment(boolean paramBoolean)
  {
    Log.d("Tango.SelectContactActivity", "showFooterFragment: show=" + paramBoolean);
    FragmentTransaction localFragmentTransaction = getSupportFragmentManager().beginTransaction();
    localFragmentTransaction.setCustomAnimations(2130968585, 2130968589);
    if (paramBoolean)
      localFragmentTransaction.show(this.m_footerFragment);
    while (true)
    {
      localFragmentTransaction.commit();
      return;
      localFragmentTransaction.hide(this.m_footerFragment);
    }
  }

  public int getActionBarHomeIconResId()
  {
    return 2130837686;
  }

  public void handleMessage(com.sgiggle.messaging.Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35282:
    }
    if (this.m_selectContactEventReceived)
    {
      Log.d("Tango.SelectContactActivity", "Ignoring SELECT_CONTACT_EVENT, it was already received.");
      return;
    }
    this.m_selectContactEventReceived = true;
    handleSelectContactEvent(paramMessage);
    this.m_viewPager.setCurrentItem(0);
  }

  public void onBackPressed()
  {
    SelectContactFragment localSelectContactFragment = getCurrentFragment();
    if ((localSelectContactFragment != null) && (localSelectContactFragment.handleBackPressed()))
    {
      Log.d("Tango.SelectContactActivity", "BACK event handled by current fragment, catching event");
      return;
    }
    cancelSelection();
  }

  public void onCancelClicked()
  {
    cancelSelection();
  }

  public void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt)
  {
    if (!((RadioButton)paramRadioGroup.findViewById(paramInt)).isChecked());
    int i;
    ContactFilterWrapper localContactFilterWrapper;
    do
    {
      return;
      Iterator localIterator;
      while (!localIterator.hasNext())
      {
        localIterator = this.m_availableFilterWrappers.iterator();
        i = -1;
      }
      localContactFilterWrapper = (ContactFilterWrapper)localIterator.next();
      i++;
    }
    while ((localContactFilterWrapper.getFilterId() != paramInt) || (this.m_viewPager.getCurrentItem() == i));
    this.m_viewPager.setCurrentItem(i);
  }

  public void onContactSelectionChanged(Utils.UIContact paramUIContact, boolean paramBoolean)
  {
    if (!trySelectContacts(1, paramBoolean));
    do
    {
      return;
      if (paramBoolean)
        this.m_selectedContacts.add(paramUIContact);
      while ((this.m_maxSelection == 1) && (this.m_selectedContacts.size() != 0))
      {
        Log.d("Tango.SelectContactActivity", "onContactSelectionChanged: sending result.");
        sendSelectContactResultMessage();
        return;
        this.m_selectedContacts.remove(paramUIContact);
      }
    }
    while (!isMultiSelection());
    this.m_footerFragment.onSelectedCountChanged(this.m_selectedContacts.size());
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.SelectContactActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903108);
    this.m_selectContactFilterGroup = ((RadioGroup)findViewById(2131362066));
    this.m_selectContactFilterGroup.setOnCheckedChangeListener(this);
    this.m_viewPager = ((ViewPager)findViewById(2131362072));
    this.m_viewPager.setOnPageChangeListener(this);
    this.m_selectedContacts = new HashSet();
    this.m_availableFilterWrappers = new ArrayList();
    this.m_filterWrappersBySelection = new HashMap();
    this.m_contactFilterWrappers = new ArrayList();
    this.m_footerFragment = ((SelectContactFooterFragment)getSupportFragmentManager().findFragmentById(2131362073));
    showFooterFragment(false);
    if (getFirstMessage() != null)
      handleMessage(getFirstMessage());
    setRunningInstance(this);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
    }
    AlertDialog localAlertDialog = new AlertDialog.Builder(this).create();
    localAlertDialog.setMessage("");
    localAlertDialog.setButton(getString(2131296287), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    });
    return localAlertDialog;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    cancelSelection();
    clearRunningInstance(this);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
    case 16908332:
    }
    while (true)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      cancelSelection();
    }
  }

  public void onPageScrollStateChanged(int paramInt)
  {
  }

  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
  {
  }

  public void onPageSelected(int paramInt)
  {
    if ((this.m_selectContactFilterGroup != null) && (this.m_availableFilterWrappers != null))
    {
      ContactFilterWrapper localContactFilterWrapper = (ContactFilterWrapper)this.m_availableFilterWrappers.get(paramInt);
      this.m_selectContactFilterGroup.check(localContactFilterWrapper.getFilterId());
      SelectContactFragment localSelectContactFragment = localContactFilterWrapper.getSelectContactFragment(false);
      if (localSelectContactFragment != null)
        localSelectContactFragment.onSelected();
    }
  }

  protected void onPause()
  {
    super.onPause();
    ContactThumbnailLoader.getInstance().stop();
  }

  protected void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    getActionBarHelper().setDisplayHomeAsUpEnabled(true);
  }

  protected void onPrepareDialog(int paramInt, Dialog paramDialog)
  {
    switch (paramInt)
    {
    default:
      super.onPrepareDialog(paramInt, paramDialog);
      return;
    case 0:
    }
    AlertDialog localAlertDialog = (AlertDialog)paramDialog;
    Resources localResources = getResources();
    int i = this.m_maxSelection;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(this.m_maxSelection);
    localAlertDialog.setMessage(localResources.getQuantityString(2131427337, i, arrayOfObject));
  }

  protected void onResume()
  {
    super.onResume();
    ContactThumbnailLoader.getInstance().start();
  }

  public boolean onSearchRequested()
  {
    SelectContactFragment localSelectContactFragment = getCurrentFragment();
    if (localSelectContactFragment != null)
      return localSelectContactFragment.onSearchRequested();
    return super.onSearchRequested();
  }

  public void onSelectClicked()
  {
    sendSelectContactResultMessage();
  }

  public boolean trySelectContacts(int paramInt, boolean paramBoolean)
  {
    if (this.m_selectionDone)
    {
      Log.w("Tango.SelectContactActivity", "trySelectContacts with nbContacts=" + paramInt + " selected=" + paramBoolean + ". Selection was refused since user already validated a selection.");
      return false;
    }
    if ((!paramBoolean) || (this.m_maxSelection == -1))
      return true;
    if (paramInt + this.m_selectedContacts.size() <= this.m_maxSelection)
      return true;
    showDialog(0);
    return false;
  }

  private class ContactFilterWrapper
  {
    private boolean m_available = false;
    private ContactListAdapter.ContactListSelection m_contactListSelection;
    private ContactStore.ContactOrderPair m_contactOrderPair = ContactStore.ContactOrderPair.getDefault();
    private ArrayList<Utils.UIContact> m_contacts;
    private int m_filterId;
    private int m_filterSeparatorId = 0;
    private SelectContactFragment m_selectContactFragment;

    public ContactFilterWrapper(ContactListAdapter.ContactListSelection paramInt1, int paramInt2, int paramBoolean, boolean paramContactOrderPair, ContactStore.ContactOrderPair arg6)
    {
      this.m_contactListSelection = paramInt1;
      this.m_filterId = paramInt2;
      this.m_filterSeparatorId = paramBoolean;
      this.m_available = paramContactOrderPair;
      this.m_contacts = new ArrayList();
      Object localObject;
      this.m_contactOrderPair = localObject;
    }

    public void addContact(Utils.UIContact paramUIContact)
    {
      this.m_contacts.add(paramUIContact);
    }

    public ContactListAdapter.ContactListSelection getContactListSelection()
    {
      return this.m_contactListSelection;
    }

    public ArrayList<Utils.UIContact> getContacts()
    {
      return this.m_contacts;
    }

    public int getFilterId()
    {
      return this.m_filterId;
    }

    public int getFilterSeparatorId()
    {
      return this.m_filterSeparatorId;
    }

    public SelectContactFragment getSelectContactFragment(boolean paramBoolean)
    {
      Log.d("Tango.SelectContactActivity", "getSelectContactFragment: createIfNull=" + paramBoolean);
      if ((this.m_selectContactFragment == null) && (paramBoolean))
        switch (SelectContactActivity.3.$SwitchMap$com$sgiggle$production$adapter$ContactListAdapter$ContactListSelection[this.m_contactListSelection.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        }
      while (true)
      {
        return this.m_selectContactFragment;
        this.m_selectContactFragment = SelectContactListFragment.newInstance(this.m_contacts, this.m_contactListSelection, this.m_contactOrderPair, SelectContactActivity.this.m_maxSelection);
      }
    }

    public boolean isAvailable()
    {
      return this.m_available;
    }

    public void sortContacts()
    {
      if (this.m_contacts != null)
        Collections.sort(this.m_contacts, new Utils.ContactComparator(this.m_contactOrderPair.getSortOrder()));
    }
  }

  private class SelectContactFragmentAdapter extends FragmentPagerAdapter
  {
    private int m_numPages;

    public SelectContactFragmentAdapter(FragmentManager paramInt, int arg3)
    {
      super();
      int i;
      this.m_numPages = i;
    }

    public int getCount()
    {
      return this.m_numPages;
    }

    public Fragment getItem(int paramInt)
    {
      Log.d("Tango.SelectContactActivity", "SelectContactFragmentAdapter.getItem: position=" + paramInt);
      return ((SelectContactActivity.ContactFilterWrapper)SelectContactActivity.this.m_availableFilterWrappers.get(paramInt)).getSelectContactFragment(true);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.SelectContactActivity
 * JD-Core Version:    0.6.2
 */