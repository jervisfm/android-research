package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayPersonalInfoMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplaySettingsEvent;
import com.sgiggle.media_engine.MediaEngineMessage.NavigateBackMessage;
import com.sgiggle.media_engine.MediaEngineMessage.QueryProductCatalogMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RequestAppLogMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RequestFAQMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ValidationRequiredEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.adapter.AlertListAdapter;
import com.sgiggle.production.widget.FixedListView;
import com.sgiggle.production.widget.FixedListView.OnItemClickListener;
import com.sgiggle.util.Log;
import com.sgiggle.util.TangoEnvironment;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.CountryCode;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert.AlertSeverity;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert.Builder;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;
import com.sgiggle.xmpp.SessionMessages.RegisterUserPayload;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class SettingsActivity extends ActivityBase
  implements View.OnClickListener, FixedListView.OnItemClickListener
{
  private static final String TAG = "Tango.SettingsActivity";
  private static Message m_postponedForResume;
  private static SettingsActivity s_instance;
  private Button m_accountEdit;
  private TextView m_accountEmail;
  private TextView m_accountName;
  private TextView m_accountPhoneNumber;
  private View m_alertContainer;
  private View m_alertDivider;
  private FixedListView m_alertsList;
  private SessionMessages.OperationalAlert m_backgroundDataAlert;
  private SessionMessages.OperationalAlert m_c2dmAlert;
  private TextView m_copyrightVersion;
  private Button m_helpBtn;
  private String m_tips_url;
  private TextView m_underHoodHeader;
  private Button m_viewLog;

  private static void clearRunningInstance(SettingsActivity paramSettingsActivity)
  {
    if (s_instance == paramSettingsActivity)
      s_instance = null;
  }

  private void createAlertAdapterAndShowView(List<SessionMessages.OperationalAlert> paramList)
  {
    AlertListAdapter localAlertListAdapter = new AlertListAdapter(this, paramList);
    this.m_alertsList.setAdapter(localAlertListAdapter);
    this.m_alertContainer.setVisibility(0);
    this.m_alertDivider.setVisibility(0);
  }

  private SessionMessages.OperationalAlert createCTAforBackgroundDataAlert()
  {
    return SessionMessages.OperationalAlert.newBuilder().setTitle(getResources().getString(2131296424)).setMessage(getResources().getString(2131296425)).setSeverity(SessionMessages.OperationalAlert.AlertSeverity.AS_OK).build();
  }

  private SessionMessages.OperationalAlert createCTAforC2dmAlert()
  {
    return SessionMessages.OperationalAlert.newBuilder().setTitle(getResources().getString(2131296422)).setMessage(getResources().getString(2131296423)).setSeverity(SessionMessages.OperationalAlert.AlertSeverity.AS_OK).build();
  }

  private String getAppVersion()
  {
    String str = "";
    try
    {
      str = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
      if ((str == null) || (str.length() == 0))
        str = "1.0.NA";
      return str;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
        Log.e("Tango.SettingsActivity", "getAppVersion(): Tango package not found", localNameNotFoundException);
    }
  }

  private String getCopyrightAndVersionInfo()
  {
    String str1 = getResources().getString(2131296260);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = getAppVersion();
    String str2 = String.format(str1, arrayOfObject);
    String str3 = TangoEnvironment.getEnvironmentName();
    if (str3.length() == 0)
      return str2;
    return str2 + " " + "(" + str3 + ")";
  }

  public static SettingsActivity getRunningInstance()
  {
    return s_instance;
  }

  private void hideAlertsViewIfNoAlert()
  {
    if (this.m_alertsList.getChildCount() == 0)
    {
      this.m_alertContainer.setVisibility(8);
      this.m_alertDivider.setVisibility(8);
    }
  }

  private void launchAccountsAndSyncActivity()
  {
    Intent localIntent = new Intent("android.settings.SYNC_SETTINGS");
    localIntent.addFlags(262144);
    startActivity(localIntent);
  }

  private void launchRegistrationActivity()
  {
    MediaEngineMessage.DisplayPersonalInfoMessage localDisplayPersonalInfoMessage = new MediaEngineMessage.DisplayPersonalInfoMessage();
    MessageRouter.getInstance().postMessage("jingle", localDisplayPersonalInfoMessage);
  }

  private void removeRegistrationAndValidationCTAs()
  {
    if ((this.m_alertsList.getAdapter() != null) && (this.m_alertsList.getAdapter().getCount() > 0))
    {
      ListIterator localListIterator = ((AlertListAdapter)this.m_alertsList.getAdapter()).getDataSet().listIterator();
      while (localListIterator.hasNext())
      {
        SessionMessages.OperationalAlert localOperationalAlert = (SessionMessages.OperationalAlert)localListIterator.next();
        CTANotifier.AppAlert localAppAlert = new CTANotifier.AppAlert(localOperationalAlert);
        if ((localAppAlert.isRegistrationRequired()) || (localAppAlert.isValidationRequired()))
        {
          Log.d("Tango.SettingsActivity", "Remove Alert");
          ((AlertListAdapter)this.m_alertsList.getAdapter()).remove(localOperationalAlert);
        }
      }
    }
    for (int i = 1; ; i = 0)
    {
      if (i != 0)
        this.m_alertsList.refresh();
      hideAlertsViewIfNoAlert();
      return;
    }
  }

  public static void setMessage4resume(Message paramMessage)
  {
    m_postponedForResume = paramMessage;
  }

  private static void setRunningInstance(SettingsActivity paramSettingsActivity)
  {
    s_instance = paramSettingsActivity;
  }

  private void showHidePremiumSection(boolean paramBoolean)
  {
  }

  private void startAppLogActivity()
  {
    Intent localIntent = new Intent(this, AppLogActivity.class);
    localIntent.addFlags(262144);
    startActivity(localIntent);
  }

  private void startAppLogActivityViaStateMachine()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.RequestAppLogMessage());
  }

  private void startTipsActivityViaStateMachine()
  {
    MediaEngineMessage.RequestFAQMessage localRequestFAQMessage = new MediaEngineMessage.RequestFAQMessage();
    MessageRouter.getInstance().postMessage("jingle", localRequestFAQMessage);
  }

  private void startTipsScreenViaBrowser()
  {
    Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.m_tips_url));
    localIntent.setFlags(268435456);
    startActivity(localIntent);
  }

  private void startVideomailSubscriptionActivity()
  {
    MediaEngineMessage.QueryProductCatalogMessage localQueryProductCatalogMessage = new MediaEngineMessage.QueryProductCatalogMessage(1, "product.category.videomail");
    MessageRouter.getInstance().postMessage("jingle", localQueryProductCatalogMessage);
  }

  private void udpateBackgroundDataAlertInfo()
  {
    Log.d("Tango.SettingsActivity", "Check if we need to enable background data alert CTA");
    boolean bool;
    if (!((ConnectivityManager)getSystemService("connectivity")).getBackgroundDataSetting())
    {
      bool = true;
      if (Build.VERSION.SDK_INT >= 14);
      Log.d("Tango.SettingsActivity", "Show background data alert=" + bool);
      if (!bool)
        break label171;
      if ((this.m_alertsList.getAdapter() == null) || (this.m_alertsList.getAdapter().getCount() == 0))
        createAlertAdapterAndShowView(new ArrayList());
      AlertListAdapter localAlertListAdapter = (AlertListAdapter)this.m_alertsList.getAdapter();
      if (this.m_backgroundDataAlert != null)
      {
        localAlertListAdapter.remove(this.m_backgroundDataAlert);
        this.m_backgroundDataAlert = null;
      }
      this.m_backgroundDataAlert = createCTAforBackgroundDataAlert();
      localAlertListAdapter.add(this.m_backgroundDataAlert);
    }
    while (true)
    {
      this.m_alertsList.refresh();
      hideAlertsViewIfNoAlert();
      return;
      bool = false;
      break;
      label171: if ((this.m_alertsList.getAdapter() != null) && (this.m_backgroundDataAlert != null))
      {
        ((AlertListAdapter)this.m_alertsList.getAdapter()).remove(this.m_backgroundDataAlert);
        this.m_backgroundDataAlert = null;
      }
    }
  }

  private void updateAlertInfo(List<SessionMessages.OperationalAlert> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.addAll(paramList);
    if (localArrayList.size() > 0)
      createAlertAdapterAndShowView(localArrayList);
    updateC2dmAlertInfo();
    udpateBackgroundDataAlertInfo();
    hideAlertsViewIfNoAlert();
  }

  private void updateC2dmAlertInfo()
  {
    int i;
    if ((Utils.isDeviceAndroid22Plus()) && (Utils.getGoogleAccounts(this).length == 0))
    {
      i = 1;
      if (i == 0)
        break label120;
      if ((this.m_alertsList.getAdapter() == null) || (this.m_alertsList.getAdapter().getCount() == 0))
        createAlertAdapterAndShowView(new ArrayList());
      AlertListAdapter localAlertListAdapter = (AlertListAdapter)this.m_alertsList.getAdapter();
      if (this.m_c2dmAlert != null)
      {
        localAlertListAdapter.remove(this.m_c2dmAlert);
        this.m_c2dmAlert = null;
      }
      this.m_c2dmAlert = createCTAforC2dmAlert();
      localAlertListAdapter.add(this.m_c2dmAlert);
    }
    while (true)
    {
      this.m_alertsList.refresh();
      hideAlertsViewIfNoAlert();
      return;
      i = 0;
      break;
      label120: if ((this.m_alertsList.getAdapter() != null) && (this.m_c2dmAlert != null))
      {
        ((AlertListAdapter)this.m_alertsList.getAdapter()).remove(this.m_c2dmAlert);
        this.m_c2dmAlert = null;
      }
    }
  }

  private void updatePersonalInfo(MediaEngineMessage.DisplaySettingsEvent paramDisplaySettingsEvent)
  {
    SessionMessages.RegisterUserPayload localRegisterUserPayload = (SessionMessages.RegisterUserPayload)paramDisplaySettingsEvent.payload();
    SessionMessages.Contact localContact = localRegisterUserPayload.getContact();
    String str1;
    String str2;
    label39: String str3;
    if (localContact.hasDisplayname())
    {
      str1 = localContact.getDisplayname();
      if (!localContact.hasEmail())
        break label152;
      str2 = localContact.getEmail();
      if (!localContact.hasPhoneNumber())
        break label159;
      str3 = "+" + localContact.getPhoneNumber().getCountryCode().getCountrycodenumber() + " " + localContact.getPhoneNumber().getSubscriberNumber();
      label89: if ((TextUtils.equals(str1, str3)) || (TextUtils.equals(str1, str2)))
        break label166;
      this.m_accountName.setText(str1);
    }
    while (true)
    {
      this.m_accountEmail.setText(str2);
      this.m_accountPhoneNumber.setText(str3);
      showHidePremiumSection(localRegisterUserPayload.getRegistered());
      return;
      str1 = "";
      break;
      label152: str2 = "";
      break label39;
      label159: str3 = "";
      break label89;
      label166: this.m_accountName.setText("");
    }
  }

  public void applyPostponedEvent()
  {
    if (m_postponedForResume != null)
    {
      handleNewMessage(m_postponedForResume);
      m_postponedForResume = null;
    }
  }

  void handleNewMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35049:
      updatePersonalInfo((MediaEngineMessage.DisplaySettingsEvent)paramMessage);
      updateAlertInfo(CTANotifier.extractAlertList(paramMessage));
      return;
    case 35038:
      removeRegistrationAndValidationCTAs();
      updateAlertInfo(CTANotifier.extractAlertList(paramMessage));
      return;
    case 35088:
      Log.d("Tango.SettingsActivity", "Handle Validation-Result: clear registration/validation CTAs...");
      removeRegistrationAndValidationCTAs();
      return;
    case 35116:
      AppLogActivity.storeAppLogEntries(paramMessage);
      startAppLogActivity();
      return;
    case 35051:
      String str = ((SessionMessages.OptionalPayload)((MediaEngineMessage.ValidationRequiredEvent)paramMessage).payload()).getMessage();
      new ValidationRequiredDialog.Builder(this).create(str).show();
      return;
    case 35114:
    }
    new SMSRateLimitDialog.Builder(this).create().show();
  }

  public void onBackPressed()
  {
    MediaEngineMessage.NavigateBackMessage localNavigateBackMessage = new MediaEngineMessage.NavigateBackMessage();
    MessageRouter.getInstance().postMessage("jingle", localNavigateBackMessage);
  }

  public void onClick(View paramView)
  {
    if (paramView == this.m_helpBtn)
      startTipsScreenViaBrowser();
    do
    {
      return;
      if (paramView == this.m_viewLog)
      {
        startAppLogActivityViaStateMachine();
        return;
      }
      if (paramView == this.m_accountEdit)
      {
        launchRegistrationActivity();
        return;
      }
    }
    while (paramView.getId() != 2131362110);
    startVideomailSubscriptionActivity();
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.SettingsActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903112);
    this.m_tips_url = getString(2131296324);
    this.m_accountName = ((TextView)findViewById(2131362086));
    this.m_accountPhoneNumber = ((TextView)findViewById(2131362088));
    this.m_accountEmail = ((TextView)findViewById(2131362089));
    this.m_copyrightVersion = ((TextView)findViewById(2131362100));
    this.m_helpBtn = ((Button)findViewById(2131362097));
    this.m_underHoodHeader = ((TextView)findViewById(2131362098));
    this.m_viewLog = ((Button)findViewById(2131362099));
    this.m_accountEdit = ((Button)findViewById(2131362087));
    this.m_alertsList = ((FixedListView)findViewById(2131362096));
    this.m_alertContainer = findViewById(2131362094);
    this.m_alertDivider = findViewById(2131362093);
    this.m_helpBtn.setOnClickListener(this);
    this.m_viewLog.setOnClickListener(this);
    this.m_accountEdit.setOnClickListener(this);
    this.m_alertsList.setOnItemClickListener(this);
    findViewById(2131362110).setOnClickListener(this);
    if (!TangoApp.videomailSupported())
      findViewById(2131362109).setVisibility(8);
    this.m_copyrightVersion.setText(getCopyrightAndVersionInfo());
    Message localMessage = getFirstMessage();
    if (localMessage != null)
      handleNewMessage(localMessage);
    applyPostponedEvent();
    setRunningInstance(this);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    clearRunningInstance(this);
  }

  public void onItemClick(View paramView)
  {
    Object localObject = paramView.getTag();
    CTANotifier.AppAlert localAppAlert;
    if ((localObject instanceof CTANotifier.AppAlert))
    {
      localAppAlert = (CTANotifier.AppAlert)localObject;
      if (!localAppAlert.isUpgradeRequired())
        break label29;
      UpdateRequiredActivity.forceTangoUpdate(this);
    }
    label29: 
    do
    {
      return;
      if ((localAppAlert.isRegistrationRequired()) || (localAppAlert.isValidationRequired()))
      {
        launchRegistrationActivity();
        return;
      }
      if (getResources().getString(2131296422).equals(localAppAlert.CTA().getTitle()))
      {
        launchAccountsAndSyncActivity();
        return;
      }
    }
    while (!getResources().getString(2131296424).equals(localAppAlert.CTA().getTitle()));
    launchAccountsAndSyncActivity();
  }

  protected void onResume()
  {
    super.onResume();
    Log.d("Tango.SettingsActivity", "onResume()");
    applyPostponedEvent();
    updateC2dmAlertInfo();
    TextView localTextView = this.m_underHoodHeader;
    int i;
    Button localButton;
    if (TangoApp.g_screenLoggerEnabled)
    {
      i = 0;
      localTextView.setVisibility(i);
      localButton = this.m_viewLog;
      if (!TangoApp.g_screenLoggerEnabled)
        break label68;
    }
    label68: for (int j = 0; ; j = 8)
    {
      localButton.setVisibility(j);
      return;
      i = 8;
      break;
    }
  }

  public void startTipsActivity()
  {
    Intent localIntent = new Intent(this, TipsActivity.class);
    localIntent.addFlags(262144);
    startActivity(localIntent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.SettingsActivity
 * JD-Core Version:    0.6.2
 */