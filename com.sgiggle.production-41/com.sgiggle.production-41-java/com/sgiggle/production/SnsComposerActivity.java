package com.sgiggle.production;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.InviteDisplayMainMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSnsPublishMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteSnsPublishResultEvent;
import com.sgiggle.media_engine.MediaEngineMessage.SnsCancelProcessMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.SnsProcessResultPayload;
import com.sgiggle.xmpp.SessionMessages.SnsProcessResultPayload.Error;
import java.io.UnsupportedEncodingException;

public class SnsComposerActivity extends ActivityBase
{
  private static final String TAG = "Tango.SnsComposerActivity";
  private final int WEIBO_LIMITATION = 140;
  private boolean isProcessing = false;
  private EditText m_editText;

  private void cancelProcessing()
  {
    if (this.isProcessing)
    {
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.SnsCancelProcessMessage());
      this.isProcessing = false;
      return;
    }
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.InviteDisplayMainMessage());
  }

  private CharSequence trimGBK(int paramInt1, CharSequence paramCharSequence, int paramInt2)
    throws UnsupportedEncodingException
  {
    int i = paramInt1 + paramCharSequence.toString().getBytes("GBK").length - 280;
    Log.v("Tango.SnsComposerActivity", "extra = " + i);
    int j = paramCharSequence.length() - 1;
    int k = paramCharSequence.length();
    while (true)
    {
      if ((j <= 0) || (paramCharSequence.subSequence(j, k).toString().getBytes("GBK").length == i))
        return paramCharSequence.subSequence(paramInt2, j);
      j--;
    }
  }

  public void handleNewMessage(Message paramMessage)
  {
    Log.v("Tango.SnsComposerActivity", "handleNewMessage id=" + paramMessage.getType());
    switch (paramMessage.getType())
    {
    case 35219:
    default:
      return;
    case 35218:
      findViewById(2131361988).setVisibility(8);
      SessionMessages.SnsProcessResultPayload localSnsProcessResultPayload = (SessionMessages.SnsProcessResultPayload)((MediaEngineMessage.InviteSnsPublishResultEvent)paramMessage).payload();
      if (localSnsProcessResultPayload.getSuccess())
        Toast.makeText(this, 2131296495, 1).show();
      while (true)
      {
        MediaEngineMessage.InviteDisplayMainMessage localInviteDisplayMainMessage = new MediaEngineMessage.InviteDisplayMainMessage();
        MessageRouter.getInstance().postMessage("jingle", localInviteDisplayMainMessage);
        return;
        if (localSnsProcessResultPayload.getErrorType() == SessionMessages.SnsProcessResultPayload.Error.REPEAT_TEXT)
          Toast.makeText(this, 2131296496, 1).show();
        else
          Toast.makeText(this, 2131296497, 1).show();
      }
    case 35220:
    }
    findViewById(2131361988).setVisibility(0);
  }

  public void onBackPressed()
  {
    cancelProcessing();
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.SnsComposerActivity", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    setContentView(2130903119);
    this.m_editText = new MyEditText(this);
    this.m_editText.setTextColor(Color.rgb(33, 33, 33));
    this.m_editText.setText(2131296499);
    ((ScrollView)findViewById(2131362114)).addView(this.m_editText);
    Button localButton = (Button)findViewById(2131362115);
    localButton.setFocusableInTouchMode(true);
    localButton.requestFocus();
    localButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        MediaEngineMessage.InviteSnsPublishMessage localInviteSnsPublishMessage = new MediaEngineMessage.InviteSnsPublishMessage(SnsComposerActivity.this.m_editText.getText().toString());
        MessageRouter.getInstance().postMessage("jingle", localInviteSnsPublishMessage);
        SnsComposerActivity.access$202(SnsComposerActivity.this, true);
      }
    });
    EditText localEditText = this.m_editText;
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    arrayOfInputFilter[0] = new InputFilter()
    {
      public CharSequence filter(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, Spanned paramAnonymousSpanned, int paramAnonymousInt3, int paramAnonymousInt4)
      {
        try
        {
          int j = SnsComposerActivity.this.m_editText.getText().toString().getBytes("GBK").length;
          if (j >= 280)
            return "";
          if (j + paramAnonymousCharSequence.subSequence(paramAnonymousInt1, paramAnonymousInt2).toString().getBytes("GBK").length > 280)
          {
            CharSequence localCharSequence = SnsComposerActivity.this.trimGBK(j, paramAnonymousCharSequence, paramAnonymousInt1);
            return localCharSequence;
          }
          return paramAnonymousCharSequence;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
          Log.v("Tango.SnsComposerActivity", "filter(): can NOT support GBK encoding!");
          int i = SnsComposerActivity.this.m_editText.getText().toString().length();
          if (i >= 140)
            return "";
          if (i + paramAnonymousCharSequence.subSequence(paramAnonymousInt1, paramAnonymousInt2).length() > 140)
            return paramAnonymousCharSequence.subSequence(paramAnonymousInt1, paramAnonymousInt2 - (i + paramAnonymousCharSequence.subSequence(paramAnonymousInt1, paramAnonymousInt2).length() - 140));
        }
        return paramAnonymousCharSequence;
      }
    };
    localEditText.setFilters(arrayOfInputFilter);
    TangoApp.getInstance().setSnsActivityInstance(this);
  }

  protected void onDestroy()
  {
    Log.v("Tango.SnsComposerActivity", "onDestroy()");
    TangoApp.getInstance().setSnsActivityInstance(null);
    super.onDestroy();
  }

  private class MyEditText extends EditText
  {
    public MyEditText(Context arg2)
    {
      super();
    }

    public MyEditText(Context paramAttributeSet, AttributeSet paramInt, int arg4)
    {
      super(paramInt, i);
    }

    public boolean onTextContextMenuItem(int paramInt)
    {
      switch (paramInt)
      {
      default:
        return super.onTextContextMenuItem(paramInt);
      case 16908322:
      }
      ClipboardManager localClipboardManager = (ClipboardManager)getContext().getSystemService("clipboard");
      CharSequence localCharSequence = localClipboardManager.getText();
      try
      {
        int k = getText().toString().getBytes("GBK").length;
        if (k >= 280)
          return false;
        Log.v("Tango.SnsComposerActivity", "onTextContextMenuItem() paste event src=" + localCharSequence);
        if (k + localCharSequence.toString().getBytes("GBK").length > 280)
        {
          localClipboardManager.setText(SnsComposerActivity.this.trimGBK(k, localCharSequence, 0));
          Log.v("Tango.SnsComposerActivity", "final len=" + new StringBuilder().append(getText()).append(SnsComposerActivity.this.trimGBK(k, localCharSequence, 0).toString()).toString().getBytes("GBK").length);
        }
        boolean bool = super.onTextContextMenuItem(paramInt);
        return bool;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        Log.v("Tango.SnsComposerActivity", "filter(): can NOT support GBK encoding!");
        int i = getText().toString().length();
        if (i >= 140)
          return false;
        if (i + localCharSequence.length() > 140)
        {
          int j = i + localCharSequence.length() - 140;
          localClipboardManager.setText(localCharSequence.subSequence(0, localCharSequence.length() - j));
        }
      }
      return super.onTextContextMenuItem(paramInt);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.SnsComposerActivity
 * JD-Core Version:    0.6.2
 */