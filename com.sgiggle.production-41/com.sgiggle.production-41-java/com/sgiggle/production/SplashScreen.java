package com.sgiggle.production;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import com.sgiggle.util.Log;
import com.sgiggle.util.LogReporter;

public class SplashScreen extends Activity
  implements Handler.Callback
{
  private static final int DIALOG_INSTALL_FAILED = 0;
  private static final String ERROR_62_URL = "http://www.tango.me/error62";
  private static final String LOG_AUTHORITY_PREFIX = "log";
  private static final String LOG_ENABLED_AUTHORITY = "logenable";
  private static final int MSG_INIT = 0;
  private static final String SETCONFIGVAL_AUTHORITY_PREFIX = "setconfigval";
  private static final String TAG = "Tango.SplashScreen";
  private static final String TANGO_SCHEMA = "tango";
  private static final String WELCOME_SCREEN_AUTHORITY = "welcome_screen";
  Handler m_handler;
  private Exception m_initFailingException;

  private Dialog getDialog(int paramInt)
  {
    if (paramInt == 0)
    {
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
      localBuilder.setTitle(2131296646);
      localBuilder.setMessage(2131296647);
      localBuilder.setCancelable(false);
      localBuilder.setNeutralButton(2131296648, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Intent localIntent = new Intent("android.intent.action.VIEW");
          localIntent.setData(Uri.parse("http://www.tango.me/error62"));
          SplashScreen.this.startActivity(localIntent);
          SplashScreen.this.finish();
          throw new RuntimeException(SplashScreen.this.m_initFailingException.getMessage(), SplashScreen.this.m_initFailingException);
        }
      });
      return localBuilder.create();
    }
    return null;
  }

  private void handleCall(Uri paramUri)
  {
    Cursor localCursor = getContentResolver().query(paramUri, new String[] { "_id", "data1", "data4", "data5" }, null, null, null);
    Object localObject;
    String str1;
    long l1;
    if ((localCursor != null) && (localCursor.moveToFirst()))
    {
      String str3 = localCursor.getString(1);
      localObject = localCursor.getString(2);
      long l3 = localCursor.getInt(3);
      Log.d("Tango.SplashScreen", "onCreate(): ... found rawContact: peerJid = [" + str3 + "], peerName = [" + (String)localObject + "], deviceContactId = [" + l3 + "], _ID=[" + localCursor.getLong(0) + "]");
      str1 = str3;
      l1 = l3;
    }
    while (true)
    {
      if (str1 != null)
      {
        Utils.UIContact localUIContact = ContactListActivity.findContact(str1);
        if (localUIContact != null)
        {
          String str2 = localUIContact.displayName();
          long l2 = localUIContact.m_deviceContactId;
          Log.v("Tango.SplashScreen", "onCreate(): ... received: foundContact = [" + str2 + "]");
          localObject = str2;
          l1 = l2;
        }
        CallHandler.getDefault().sendCallMessage(str1, (String)localObject, l1);
        finish();
      }
      return;
      l1 = -1L;
      localObject = "";
      str1 = null;
    }
  }

  private boolean handleIntent(Intent paramIntent, boolean paramBoolean)
  {
    Uri localUri = paramIntent.getData();
    if (localUri != null)
    {
      String str1 = localUri.getScheme();
      if ((str1 != null) && (str1.startsWith("tango")))
      {
        String str2 = localUri.getAuthority();
        if ((str2 != null) && ((str2.startsWith("log")) || (str2.startsWith("setconfigval"))))
        {
          handleLog(localUri);
          if (str2.equals("logenable"))
          {
            Intent localIntent = new Intent(this, Startup.class);
            localIntent.addFlags(268435456);
            startActivity(localIntent);
          }
          finish();
          return false;
        }
        if ("welcome_screen".equals(str2))
        {
          handleWelcomeUri(localUri);
          return true;
        }
      }
      else if (!paramBoolean)
      {
        handleCall(localUri);
        return true;
      }
    }
    return true;
  }

  private void handleLog(Uri paramUri)
  {
    boolean bool1 = false;
    if (paramUri != null)
    {
      String str2 = paramUri.getScheme();
      bool1 = false;
      if (str2 != null)
      {
        boolean bool2 = str2.startsWith("tango");
        bool1 = false;
        if (bool2)
          bool1 = LogReporter.enableUri(paramUri.toString());
      }
    }
    StringBuilder localStringBuilder = new StringBuilder().append("LogReporter.enableUri() ");
    if (bool1);
    for (String str1 = "successful"; ; str1 = "failed")
    {
      Log.d("Tango.SplashScreen", str1);
      return;
    }
  }

  private void handleWelcomeUri(Uri paramUri)
  {
    WelcomeScreenActivity.handleWelcomeUrl(paramUri.toString());
  }

  private void init()
  {
    try
    {
      TangoApp.ensureInitialized();
      if (handleIntent(getIntent(), false))
        TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_RESUMING);
      return;
    }
    catch (Exception localException)
    {
      this.m_initFailingException = localException;
      showDialog(0);
    }
  }

  public boolean handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default:
    case 0:
    }
    while (true)
    {
      return false;
      init();
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903042);
    this.m_handler = new Handler(this);
    this.m_handler.sendEmptyMessageDelayed(0, 200L);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = getDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt);
  }

  protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
  {
    Dialog localDialog = getDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt, paramBundle);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    handleIntent(paramIntent, true);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.SplashScreen
 * JD-Core Version:    0.6.2
 */