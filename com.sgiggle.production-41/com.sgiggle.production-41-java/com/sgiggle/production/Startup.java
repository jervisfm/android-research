package com.sgiggle.production;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.sgiggle.util.Log;

public class Startup extends Activity
{
  private static final String TAG = "Startup";

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if ((TangoApp.isInitialized()) && (TangoApp.isInstallationOk()))
    {
      if (TangoApp.getInstance().getTabsActivityInstance() != null)
      {
        Log.d("Startup", "Second instance started, skipping...");
        finish();
        return;
      }
      Log.d("Startup", "Initialized. Asking to resume.");
      TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_RESUMING);
      return;
    }
    Log.d("Startup", "NOT initialized. Starting splash screen.");
    Intent localIntent = new Intent(this, SplashScreen.class);
    localIntent.addFlags(268435456);
    startActivity(localIntent);
  }

  protected void onRestart()
  {
    super.onRestart();
    Log.d("Startup", "We should never RE-ENTER this screen. Calling finish().");
    finish();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.Startup
 * JD-Core Version:    0.6.2
 */