package com.sgiggle.production;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayStoreEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.production.fragment.StoreCategoryListFragment;
import com.sgiggle.xmpp.SessionMessages.DisplayStorePayload;

public class StoreActivity extends FragmentActivityBase
{
  private StoreCategoryListFragment m_fragment;

  private void handleDisplayEvent(MediaEngineMessage.DisplayStoreEvent paramDisplayStoreEvent)
  {
    if (((SessionMessages.DisplayStorePayload)paramDisplayStoreEvent.payload()).hasError())
    {
      showErrorMessage();
      return;
    }
    showCatalog();
  }

  private void showCatalog()
  {
    findViewById(2131362117).setVisibility(0);
    findViewById(2131361844).setVisibility(8);
  }

  private void showErrorMessage()
  {
    findViewById(2131361844).setVisibility(0);
    findViewById(2131362117).setVisibility(8);
  }

  protected boolean finishIfResumedAfterKilled()
  {
    return false;
  }

  public void handleMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35264:
    }
    handleDisplayEvent((MediaEngineMessage.DisplayStoreEvent)paramMessage);
    this.m_fragment.handleMessage(paramMessage);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903121);
    this.m_fragment = ((StoreCategoryListFragment)getSupportFragmentManager().findFragmentById(2131362117));
    if (getFirstMessage() != null)
      handleMessage(getFirstMessage());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.StoreActivity
 * JD-Core Version:    0.6.2
 */