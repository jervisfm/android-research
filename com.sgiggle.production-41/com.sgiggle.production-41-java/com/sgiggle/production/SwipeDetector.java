package com.sgiggle.production;

import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class SwipeDetector
  implements View.OnTouchListener
{
  static final int BottomToTop = 3;
  static final int LeftToRight = 1;
  static final int RightToLeft = 0;
  static final int SWIPE_MIN_DISTANCE = 100;
  static final String TAG = "SwipeDetector";
  static final int TopToBottom = 2;
  private int REL_SWIPE_MIN_DISTANCE;
  private float downX;
  private float downY;
  private GestureHandler gestureHandler;
  private int m_Orientation;
  private float upX;
  private float upY;

  public SwipeDetector(GestureHandler paramGestureHandler, int paramInt, DisplayMetrics paramDisplayMetrics)
  {
    this.gestureHandler = paramGestureHandler;
    this.m_Orientation = paramInt;
    this.REL_SWIPE_MIN_DISTANCE = ((int)(100 * paramDisplayMetrics.densityDpi / 160.0F));
  }

  public void onBottomToTopSwipe()
  {
    this.gestureHandler.handleSwipe(3);
  }

  public void onLeftToRightSwipe()
  {
    this.gestureHandler.handleSwipe(1);
  }

  public void onRightToLeftSwipe()
  {
    this.gestureHandler.handleSwipe(0);
  }

  public void onTopToBottomSwipe()
  {
    this.gestureHandler.handleSwipe(2);
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    switch (paramMotionEvent.getAction())
    {
    default:
      return false;
    case 0:
      this.downX = paramMotionEvent.getX();
      this.downY = paramMotionEvent.getY();
      return true;
    case 1:
    }
    this.upX = paramMotionEvent.getX();
    this.upY = paramMotionEvent.getY();
    float f1 = this.downX - this.upX;
    float f2 = this.downY - this.upY;
    if (Math.abs(f1) > this.REL_SWIPE_MIN_DISTANCE)
    {
      if (f1 < 0.0F)
      {
        if (this.m_Orientation == 0)
          onTopToBottomSwipe();
        while (true)
        {
          return true;
          onLeftToRightSwipe();
        }
      }
      if (f1 > 0.0F)
      {
        if (this.m_Orientation == 0)
          onBottomToTopSwipe();
        while (true)
        {
          return true;
          onRightToLeftSwipe();
        }
      }
    }
    if (Math.abs(f2) > this.REL_SWIPE_MIN_DISTANCE)
    {
      if (f2 < 0.0F)
      {
        if (this.m_Orientation == 0)
          onRightToLeftSwipe();
        while (true)
        {
          return true;
          onTopToBottomSwipe();
        }
      }
      if (f2 > 0.0F)
      {
        if (this.m_Orientation == 0)
          onLeftToRightSwipe();
        while (true)
        {
          return true;
          onBottomToTopSwipe();
        }
      }
    }
    return true;
  }

  static abstract interface GestureHandler
  {
    public abstract void handleSwipe(int paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.SwipeDetector
 * JD-Core Version:    0.6.2
 */