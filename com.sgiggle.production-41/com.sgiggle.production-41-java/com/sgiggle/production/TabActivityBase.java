package com.sgiggle.production;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.LocalActivityManager;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.BadgeInviteEvent;
import com.sgiggle.media_engine.MediaEngineMessage.BadgeStoreEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ContactsDisplayMainMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplaySettingsMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayStoreEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayStoreMessage;
import com.sgiggle.media_engine.MediaEngineMessage.InviteDisplayMainMessage;
import com.sgiggle.media_engine.MediaEngineMessage.OpenConversationListMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RequestCallLogMessage;
import com.sgiggle.media_engine.MediaEngineMessage.UpdateConversationMessageNotificationEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ValidationRequiredEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.screens.tc.ConversationsActivity;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.BadgeInviteCountPayload;
import com.sgiggle.xmpp.SessionMessages.DisplayStorePayload;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;
import com.sgiggle.xmpp.SessionMessages.StoreBadgeCountPayload;
import com.sgiggle.xmpp.SessionMessages.UpdateConversationMessageNotificationPayload;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class TabActivityBase extends TabActivity
  implements TabHost.OnTabChangeListener
{
  public static final String CALL_LOG_TAB = "call_log";
  public static final String CONTACTS_TAB = "contacts";
  private static final int HELP_MENU_ID = 3;
  public static final String INVITE_TAB = "invite";
  private static final int SETTINGS_MENU_ID = 2;
  public static final String STORE_TAB = "store";
  public static final int TAB_ACTIVITY_FIRST_MENU_ID = 4;
  private static final String TAG = "Tango.TabsUI";
  public static final String THREADED_CONVERSATIONS_TAB = "threaded_conversations";
  private boolean forceSwtichTab = false;
  private ContactListActivity m_contactSearchActivity;
  private Message m_firstMessage;
  private int m_inviteBadgeCount = 0;
  private boolean m_isForegroundActivity = false;
  private boolean m_isInSearchMode = false;
  private boolean m_smsCapable = false;
  private View m_tabFooter;
  private TabHost m_tabHost;
  public HashMap<String, Integer> m_tabIndexByTag;

  public TabActivityBase()
  {
    try
    {
      TangoApp.ensureInitialized();
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      Log.e("Tango.TabsUI", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  private static View createTabView(Context paramContext, String paramString, int paramInt)
  {
    View localView = LayoutInflater.from(paramContext).inflate(2130903125, null);
    ((TextView)localView.findViewById(2131362124)).setText(paramString);
    ((ImageView)localView.findViewById(2131362122)).setImageResource(paramInt);
    return localView;
  }

  private Activity getCurrentTabActivity()
  {
    return getLocalActivityManager().getActivity(this.m_tabHost.getCurrentTabTag());
  }

  private ActivityStack getCurrentTabActivityStack()
  {
    return (ActivityStack)getLocalActivityManager().getActivity(this.m_tabHost.getCurrentTabTag());
  }

  private void handleMissedCallIntent(String paramString, Intent paramIntent)
  {
    Log.d("Tango.TabsUI", "handleMissedCallIntent(): missedJid=" + paramString);
    paramIntent.removeExtra("missedJid");
    Utils.UIMissedCall localUIMissedCall = new Utils.UIMissedCall(paramString, paramIntent.getStringExtra("missedDisplayname"), paramIntent.getLongExtra("missedWhen", 0L));
    MissedCallNotifier.getDefault().saveMissedCallNotification(localUIMissedCall);
    if (CallHandler.getDefault().getCallSession() != null)
    {
      Log.d("Tango.TabsUI", "handleMissedCallIntent(): A Tango call is in progresss. Finish this activity.");
      finish();
      return;
    }
    this.forceSwtichTab = true;
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.RequestCallLogMessage());
  }

  private void handleNotificationBar(String paramString, Intent paramIntent)
  {
    if ("OPEN".equals(paramString))
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DisplaySettingsMessage());
    do
    {
      return;
      if ("tango.broadcast.show_conversation".equals(paramString))
      {
        String str = (String)paramIntent.getCharSequenceExtra("conversationid");
        Log.d("Tango.TabsUI", "Switching to Conversation detail page with conversation id=" + str);
        TangoApp.getInstance().switchToConversationDetailIfPossible(str);
        return;
      }
    }
    while (!"tango.broadcast.show_conversation_list".equals(paramString));
    Log.d("Tango.TabsUI", "Switching to Conversation List");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.OpenConversationListMessage());
  }

  private void onHelpMenuSelected()
  {
    Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(getString(2131296324)));
    localIntent.setFlags(268435456);
    startActivity(localIntent);
  }

  private void onSettingsMenuSelected()
  {
    MediaEngineMessage.DisplaySettingsMessage localDisplaySettingsMessage = new MediaEngineMessage.DisplaySettingsMessage();
    MessageRouter.getInstance().postMessage("jingle", localDisplaySettingsMessage);
  }

  private void setBadgeCount(String paramString, int paramInt, boolean paramBoolean)
  {
    Log.d("Tango.TabsUI", "setBadgeCount for tab=" + paramString + " to count=" + paramInt);
    Integer localInteger = (Integer)this.m_tabIndexByTag.get(paramString);
    int i = this.m_tabHost.getTabWidget().getTabCount();
    if ((i == 0) || (localInteger == null))
    {
      Log.e("Tango.TabsUI", "setBadgeCount: could not find tab with tag=" + paramString);
      return;
    }
    int j = localInteger.intValue();
    if (j >= i)
    {
      Log.e("Tango.TabsUI", "setBadgeCount: tab position is invalid for tab=" + paramString);
      return;
    }
    TextView localTextView = (TextView)this.m_tabHost.getTabWidget().getChildTabViewAt(j).findViewById(2131362123);
    if ((paramBoolean) && (isForegroundActivity()));
    for (boolean bool = true; ; bool = false)
    {
      Utils.setBadgeCount(localTextView, paramInt, bool, this);
      return;
    }
  }

  private void setBadgeToError(String paramString, boolean paramBoolean)
  {
    setBadgeCount(paramString, -1, paramBoolean);
  }

  private void setupTab(Class<?> paramClass, String paramString1, String paramString2, int paramInt, View.OnTouchListener paramOnTouchListener)
  {
    int i = this.m_tabHost.getTabWidget().getTabCount();
    this.m_tabIndexByTag.put(paramString2, Integer.valueOf(i));
    View localView = createTabView(this.m_tabHost.getContext(), paramString1, paramInt);
    localView.setOnTouchListener(paramOnTouchListener);
    TabHost.TabSpec localTabSpec = this.m_tabHost.newTabSpec(paramString2).setIndicator(localView).setContent(new Intent().setClass(this, paramClass));
    this.m_tabHost.addTab(localTabSpec);
  }

  private void setupTabs()
  {
    this.m_tabIndexByTag = new HashMap();
    Resources localResources = getResources();
    setupTab(ContactListActivityStack.class, localResources.getString(2131296264), "contacts", 2130837682, null);
    setupTab(CallLogActivityStack.class, localResources.getString(2131296265), "call_log", 2130837681, null);
    setupTab(ConversationsActivity.class, localResources.getString(2131296548), "threaded_conversations", 2130837685, new View.OnTouchListener()
    {
      public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
      {
        if ((paramAnonymousMotionEvent.getAction() == 1) && ("threaded_conversations".equals(TabActivityBase.this.m_tabHost.getCurrentTabTag())))
          ((ConversationsActivity)TabActivityBase.this.getCurrentTabActivity()).scrollToRelevantItem();
        return false;
      }
    });
    setupTab(InviteSelectionActivity.class, localResources.getString(2131296266), "invite", 2130837683, null);
    setupTab(StoreActivity.class, localResources.getString(2131296267), "store", 2130837684, null);
  }

  void bringToForeground()
  {
    Log.d("Tango.TabsUI", "bringToForeground()...");
    Intent localIntent = new Intent(TangoApp.getInstance(), TabActivityBase.class);
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  public String getCurrentTabTag()
  {
    return this.m_tabHost.getCurrentTabTag();
  }

  void handleNewMessage(Message paramMessage)
  {
    Log.v("Tango.TabsUI", "handleNewMessage(message=" + paramMessage + ")");
    if (paramMessage == null)
      Log.i("Tango.TabsUI", "handleNewMessage(): Message is null. Do nothing.");
    do
    {
      do
      {
        do
        {
          return;
          switch (paramMessage.getType())
          {
          default:
            Log.w("Tango.TabsUI", "handleNewMessage(): Unsupported message:" + paramMessage);
            return;
          case 35037:
          case 35093:
          case 35091:
          case 35011:
          case 35051:
          case 35114:
          case 35092:
          case 35272:
          case 35283:
          case 35273:
          case 35274:
          case 35284:
          case 35039:
          case 35041:
          case 35057:
          case 35059:
          case 35060:
          case 35061:
          case 35267:
          case 35088:
          case 35216:
          case 35264:
          case 35038:
          case 35265:
          }
        }
        while (!"contacts".equals(this.m_tabHost.getCurrentTabTag()));
        ((ContactListActivity)((ContactListActivityStack)getCurrentTabActivityStack()).getActivity(0)).updateContacts();
        return;
      }
      while (!"call_log".equals(this.m_tabHost.getCurrentTabTag()));
      ((CallLogActivity)((CallLogActivityStack)getCurrentTabActivityStack()).getActivity(0)).updateLogEntries();
      return;
      if (this.m_contactSearchActivity != null)
      {
        this.m_contactSearchActivity.handleNewMessage(paramMessage);
        return;
      }
    }
    while (!"contacts".equals(this.m_tabHost.getCurrentTabTag()));
    ((ActivityBase)((ContactListActivityStack)getCurrentTabActivityStack()).getCurrentActivity()).handleNewMessage(paramMessage);
    return;
    Log.d("Tango.TabsUI", "LOGIN_COMPLETED_EVENT");
    CTANotifier.getDefault().publishCTA2NotificationBar(paramMessage);
    if (!"contacts".equals(this.m_tabHost.getCurrentTabTag()))
      switchTabSafe("contacts");
    if ("contacts".equals(this.m_tabHost.getCurrentTabTag()))
    {
      ContactListActivityStack localContactListActivityStack = (ContactListActivityStack)getCurrentTabActivityStack();
      localContactListActivityStack.popToRoot();
      ((ContactListActivity)localContactListActivityStack.getActivity(0)).handleNewMessage(paramMessage);
    }
    label694: label751: label757: 
    do
    {
      while (true)
      {
        Activity localActivity = getLocalActivityManager().getActivity(this.m_tabHost.getCurrentTabTag());
        if (localActivity == null)
          break;
        localActivity.closeOptionsMenu();
        return;
        String str = ((SessionMessages.OptionalPayload)((MediaEngineMessage.ValidationRequiredEvent)paramMessage).payload()).getMessage();
        new ValidationRequiredDialog.Builder(this).create(str).show();
        continue;
        new SMSRateLimitDialog.Builder(this).create().show();
        continue;
        if (this.forceSwtichTab)
        {
          switchTabSafe("call_log");
          this.forceSwtichTab = false;
        }
        if ("call_log".equals(this.m_tabHost.getCurrentTabTag()))
        {
          CallLogActivityStack localCallLogActivityStack = (CallLogActivityStack)getCurrentTabActivityStack();
          localCallLogActivityStack.popToRoot();
          ((CallLogActivity)localCallLogActivityStack.getActivity(0)).updateLogEntries();
          continue;
          switchTabSafe("threaded_conversations");
          if ("threaded_conversations".equals(this.m_tabHost.getCurrentTabTag()))
          {
            ConversationsActivity localConversationsActivity = (ConversationsActivity)getCurrentTabActivity();
            if (localConversationsActivity != null)
            {
              localConversationsActivity.handleMessage(paramMessage);
              continue;
              SessionMessages.UpdateConversationMessageNotificationPayload localUpdateConversationMessageNotificationPayload = (SessionMessages.UpdateConversationMessageNotificationPayload)((MediaEngineMessage.UpdateConversationMessageNotificationEvent)paramMessage).payload();
              int i1;
              if ((localUpdateConversationMessageNotificationPayload.hasLastUnreadMsgStatus()) && (ConversationMessage.isStatusError(localUpdateConversationMessageNotificationPayload.getLastUnreadMsgStatus())))
              {
                i1 = 1;
                if (((localUpdateConversationMessageNotificationPayload.getUpdateNotification()) || (!localUpdateConversationMessageNotificationPayload.getPlayAlertSound())) && ((!localUpdateConversationMessageNotificationPayload.getUpdateNotification()) || (localUpdateConversationMessageNotificationPayload.getUnreadMessageCount() <= 0)))
                  break label751;
              }
              for (boolean bool2 = true; ; bool2 = false)
              {
                if (i1 == 0)
                  break label757;
                setBadgeToError("threaded_conversations", bool2);
                break;
                i1 = 0;
                break label694;
              }
              setBadgeCount("threaded_conversations", localUpdateConversationMessageNotificationPayload.getUnreadConversationCount(), bool2);
              continue;
              if (!"threaded_conversations".equals(this.m_tabHost.getCurrentTabTag()))
                break;
              ((ConversationsActivity)getCurrentTabActivity()).handleMessage(paramMessage);
              return;
              if (this.forceSwtichTab)
              {
                switchTabSafe("invite");
                this.forceSwtichTab = false;
              }
              if ("invite".equals(this.m_tabHost.getCurrentTabTag()))
              {
                InviteSelectionActivity localInviteSelectionActivity3 = (InviteSelectionActivity)getCurrentTabActivity();
                if (localInviteSelectionActivity3 != null)
                {
                  localInviteSelectionActivity3.handleNewMessage(paramMessage);
                  localInviteSelectionActivity3.setRecommendedBadgeCount(this.m_inviteBadgeCount);
                  continue;
                  if ("invite".equals(this.m_tabHost.getCurrentTabTag()))
                  {
                    InviteSelectionActivity localInviteSelectionActivity2 = (InviteSelectionActivity)getCurrentTabActivity();
                    localInviteSelectionActivity2.handleNewMessage(paramMessage);
                    localInviteSelectionActivity2.setRecommendedBadgeCount(this.m_inviteBadgeCount);
                    continue;
                    if (this.m_smsCapable)
                    {
                      this.m_inviteBadgeCount = ((SessionMessages.BadgeInviteCountPayload)((MediaEngineMessage.BadgeInviteEvent)paramMessage).payload()).getBadgeCount();
                      int n = this.m_inviteBadgeCount;
                      if (this.m_inviteBadgeCount > 0);
                      for (boolean bool1 = true; ; bool1 = false)
                      {
                        setBadgeCount("invite", n, bool1);
                        if (!"invite".equals(this.m_tabHost.getCurrentTabTag()))
                          break;
                        InviteSelectionActivity localInviteSelectionActivity1 = (InviteSelectionActivity)getCurrentTabActivity();
                        if (localInviteSelectionActivity1 == null)
                          break;
                        localInviteSelectionActivity1.setRecommendedBadgeCount(this.m_inviteBadgeCount);
                        break;
                      }
                      CTANotifier.getDefault().cancelValidationAndRegistrationAlerts();
                      if ("contacts".equals(this.m_tabHost.getCurrentTabTag()))
                      {
                        ((ContactListActivity)((ContactListActivityStack)getCurrentTabActivityStack()).getActivity(0)).handleNewMessage(paramMessage);
                        continue;
                        if ("contacts".equals(this.m_tabHost.getCurrentTabTag()))
                        {
                          Log.v("Tango.TabsUI", "DISPLAY_CONTACT_DETAIL CONTACTS_TAB");
                          ((ContactListActivity)((ContactListActivityStack)getCurrentTabActivityStack()).getActivity(0)).handleNewMessage(paramMessage);
                        }
                        else if ("call_log".equals(this.m_tabHost.getCurrentTabTag()))
                        {
                          ((CallLogActivity)((CallLogActivityStack)getCurrentTabActivityStack()).getActivity(0)).handleNewMessage(paramMessage);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      if (this.forceSwtichTab)
      {
        switchTabSafe("store");
        this.forceSwtichTab = false;
      }
    }
    while (!"store".equals(this.m_tabHost.getCurrentTabTag()));
    StoreActivity localStoreActivity = (StoreActivity)getCurrentTabActivity();
    if (localStoreActivity != null)
      localStoreActivity.handleNewMessage(paramMessage);
    SessionMessages.DisplayStorePayload localDisplayStorePayload = (SessionMessages.DisplayStorePayload)((MediaEngineMessage.DisplayStoreEvent)paramMessage).payload();
    if (localDisplayStorePayload.getVgoodBadgeCount() > 0);
    for (int k = 0 + 1; ; k = 0)
    {
      if (localDisplayStorePayload.getGameBadgeCount() > 0)
        k++;
      if (localDisplayStorePayload.getAvatarBadgeCount() > 0);
      for (int m = k + 1; ; m = k)
      {
        setBadgeCount("store", m, false);
        break;
        updateTangoAlertFooter();
        return;
        SessionMessages.StoreBadgeCountPayload localStoreBadgeCountPayload = (SessionMessages.StoreBadgeCountPayload)((MediaEngineMessage.BadgeStoreEvent)paramMessage).payload();
        if (localStoreBadgeCountPayload.getVgoodBadgeCount() > 0);
        for (int i = 0 + 1; ; i = 0)
        {
          if (localStoreBadgeCountPayload.getGameBadgeCount() > 0)
            i++;
          if (localStoreBadgeCountPayload.getAvatarBadgeCount() > 0);
          for (int j = i + 1; ; j = i)
          {
            setBadgeCount("store", j, true);
            return;
          }
        }
      }
    }
  }

  boolean isForegroundActivity()
  {
    return this.m_isForegroundActivity;
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.TabsUI", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    Intent localIntent = getIntent();
    if ((0x100000 & localIntent.getFlags()) != 0)
    {
      Log.d("Tango.TabsUI", "onCreate(): The intent was re-launched from history.");
      TangoApp.getInstance().startInitScreenIfResumeFromKilled();
    }
    Object localObject = getLastNonConfigurationInstance();
    if (localObject == null)
    {
      this.m_firstMessage = MessageManager.getDefault().getMessageFromIntent(localIntent);
      Log.v("Tango.TabsUI", "onCreate(): First time created: message=" + this.m_firstMessage);
    }
    while (true)
    {
      setContentView(2130903126);
      this.m_tabFooter = findViewById(2131362125);
      this.m_tabHost = getTabHost();
      setupTabs();
      TangoApp.getInstance().setTabsActivityInstance(this);
      if (this.m_firstMessage == null)
        break;
      this.forceSwtichTab = true;
      handleNewMessage(this.m_firstMessage);
      return;
      this.m_firstMessage = ((ActivityConfiguration)localObject).firstMessage;
      Log.v("Tango.TabsUI", "onCreate(): Restore activity: message=" + this.m_firstMessage);
    }
    String str1 = localIntent.getAction();
    if (str1 != null)
    {
      handleNotificationBar(str1, localIntent);
      return;
    }
    String str2 = localIntent.getStringExtra("missedJid");
    if (str2 != null)
    {
      Log.d("Tango.TabsUI", "onCreate(): missedJid=" + str2);
      this.forceSwtichTab = true;
      handleMissedCallIntent(str2, localIntent);
      return;
    }
    Log.d("Tango.TabsUI", "onCreate(): Query UI State Machine for the latest UI...");
    TangoApp.getInstance().requestServiceForLatestUI();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    paramMenu.add(0, 2, 0, 2131296523).setIcon(2130837670);
    paramMenu.add(0, 3, 0, 2131296293).setIcon(2130837667);
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    Log.d("Tango.TabsUI", "onDestroy()");
    super.onDestroy();
    TangoApp.getInstance().setTabsActivityInstance(null);
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Log.d("Tango.TabsUI", "onNewIntent(intent=" + paramIntent + ")");
    if ("android.intent.action.SEARCH".equals(paramIntent.getAction()))
    {
      if ("contacts".equals(this.m_tabHost.getCurrentTabTag()))
      {
        localContactListActivityStack = (ContactListActivityStack)getCurrentTabActivityStack();
        localIntent = new Intent();
        localIntent.setClass(this, ContactListActivity.class);
        localIntent.setAction("android.intent.action.SEARCH");
        localIntent.putExtra("query", paramIntent.getStringExtra("query"));
        localContactListActivityStack.push(UUID.randomUUID().toString(), localIntent);
      }
      while (!"invite".equals(this.m_tabHost.getCurrentTabTag()))
      {
        ContactListActivityStack localContactListActivityStack;
        Intent localIntent;
        return;
      }
      ((InviteSelectionActivity)getCurrentTabActivity()).onNewIntent(paramIntent);
      return;
    }
    String str1 = paramIntent.getAction();
    if (str1 != null)
    {
      handleNotificationBar(str1, paramIntent);
      return;
    }
    String str2 = paramIntent.getStringExtra("missedJid");
    if (str2 != null)
    {
      Log.d("Tango.TabsUI", "onNewIntent(): missedJid=" + str2);
      this.forceSwtichTab = true;
      handleMissedCallIntent(str2, paramIntent);
      return;
    }
    Message localMessage = MessageManager.getDefault().getMessageFromIntent(paramIntent);
    if (localMessage == null)
    {
      Log.d("Tango.TabsUI", "onNewIntent(): Simply bring this Tabs activity to foreground.");
      return;
    }
    Log.d("Tango.TabsUI", "onNewIntent(): Message = " + localMessage);
    this.forceSwtichTab = true;
    handleNewMessage(localMessage);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2:
      onSettingsMenuSelected();
      return true;
    case 3:
    }
    onHelpMenuSelected();
    return true;
  }

  protected void onPause()
  {
    Log.v("Tango.TabsUI", "onPause()");
    super.onPause();
    this.m_isForegroundActivity = false;
  }

  protected void onResume()
  {
    Log.v("Tango.TabsUI", "onResume()");
    super.onResume();
    this.m_smsCapable = Utils.isSmsIntentAvailable(this);
    this.m_isForegroundActivity = true;
    TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_FOREGROUND);
    this.m_tabHost.setOnTabChangedListener(this);
  }

  public Object onRetainNonConfigurationInstance()
  {
    Log.v("Tango.TabsUI", "onRetainNonConfigurationInstance(): message = " + this.m_firstMessage);
    ActivityConfiguration localActivityConfiguration = new ActivityConfiguration(null);
    localActivityConfiguration.firstMessage = this.m_firstMessage;
    return localActivityConfiguration;
  }

  void onSearchModeEntered(boolean paramBoolean)
  {
    Log.d("Tango.TabsUI", "onSearchModeEntered(): inSearch=" + paramBoolean);
    this.m_isInSearchMode = paramBoolean;
  }

  protected void onStart()
  {
    Log.v("Tango.TabsUI", "onStart()");
    super.onStart();
  }

  protected void onStop()
  {
    Log.v("Tango.TabsUI", "onStop()");
    super.onStop();
  }

  public void onTabChanged(String paramString)
  {
    Log.d("Tango.TabsUI", "onTabChanged(tabId=" + paramString + ")");
    this.forceSwtichTab = false;
    Object localObject;
    if ("contacts".equals(paramString))
      localObject = new MediaEngineMessage.ContactsDisplayMainMessage();
    while (true)
    {
      updateTangoAlertFooter();
      if (localObject != null)
      {
        Log.v("Tango.TabsUI", "onTabChanged: Posting message: " + localObject);
        MessageRouter.getInstance().postMessage("jingle", (Message)localObject);
      }
      return;
      if ("call_log".equals(paramString))
      {
        MissedCallNotifier.getDefault().dismissMissedCallNotification();
        localObject = new MediaEngineMessage.RequestCallLogMessage();
      }
      else if ("invite".equals(paramString))
      {
        localObject = new MediaEngineMessage.InviteDisplayMainMessage();
      }
      else if ("threaded_conversations".equals(paramString))
      {
        localObject = new MediaEngineMessage.OpenConversationListMessage();
      }
      else
      {
        boolean bool = "store".equals(paramString);
        localObject = null;
        if (bool)
          localObject = new MediaEngineMessage.DisplayStoreMessage();
      }
    }
  }

  protected void onUserLeaveHint()
  {
    Log.d("Tango.TabsUI", "onUserLeaveHint()");
    super.onUserLeaveHint();
    if (!this.m_isInSearchMode)
      TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
  }

  void setContactSearchActivity(ContactListActivity paramContactListActivity)
  {
    this.m_contactSearchActivity = paramContactListActivity;
    this.m_isInSearchMode = false;
  }

  public void setForceSwitchTab()
  {
    this.forceSwtichTab = true;
  }

  boolean switchTabSafe(String paramString)
  {
    this.m_tabHost.setOnTabChangedListener(null);
    try
    {
      Log.d("Tango.TabsUI", "switchTabSafe !!");
      this.m_tabHost.setCurrentTabByTag(paramString);
      updateTangoAlertFooter();
      return this.m_tabHost.getCurrentTabTag().equals(paramString);
    }
    finally
    {
      this.m_tabHost.setOnTabChangedListener(this);
    }
  }

  public void updateTangoAlertFooter()
  {
    if (("contacts".equals(this.m_tabHost.getCurrentTabTag())) || ("invite".equals(this.m_tabHost.getCurrentTabTag())))
    {
      Log.d("Tango.TabsUI", "updateTangoAlertFooter: hiding footer, handled by screen already or should not be shown.");
      this.m_tabFooter.setVisibility(8);
      return;
    }
    List localList = CTANotifier.getDefault().getLastCtaAlertList();
    if ((localList == null) || (localList.size() == 0))
    {
      Log.d("Tango.TabsUI", "updateTangoAlertFooter: hiding footer, no alert to show.");
      this.m_tabFooter.setVisibility(8);
      return;
    }
    int i = localList.size();
    final SessionMessages.OperationalAlert localOperationalAlert = (SessionMessages.OperationalAlert)localList.get(0);
    Log.d("Tango.TabsUI", "updateTangoAlertFooter: updating footer, " + i + " alerts to show.");
    this.m_tabFooter.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        MediaEngineMessage.DisplaySettingsMessage localDisplaySettingsMessage = new MediaEngineMessage.DisplaySettingsMessage(localOperationalAlert);
        MessageRouter.getInstance().postMessage("jingle", localDisplaySettingsMessage);
      }
    });
    TextView localTextView = (TextView)this.m_tabFooter.findViewById(2131361797);
    if (i == 1);
    Resources localResources;
    Object[] arrayOfObject;
    for (String str = localOperationalAlert.getTitle(); ; str = localResources.getQuantityString(2131427336, i, arrayOfObject))
    {
      SpannableString localSpannableString = new SpannableString(str);
      localSpannableString.setSpan(new UnderlineSpan(), 0, localSpannableString.length(), 0);
      localTextView.setText(localSpannableString);
      this.m_tabFooter.setVisibility(0);
      return;
      localResources = getResources();
      arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(i);
    }
  }

  private class ActivityConfiguration
  {
    protected Message firstMessage;

    private ActivityConfiguration()
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.TabActivityBase
 * JD-Core Version:    0.6.2
 */