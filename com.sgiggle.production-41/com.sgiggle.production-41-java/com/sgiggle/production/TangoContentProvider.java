package com.sgiggle.production;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.sgiggle.util.Log;
import com.sgiggle.util.LogReporter;
import java.io.File;

public class TangoContentProvider extends ContentProvider
{
  private static final String TAG = "Tango.TangoContentProvider";

  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    return 0;
  }

  public String getType(Uri paramUri)
  {
    return null;
  }

  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    return null;
  }

  public boolean onCreate()
  {
    Log.d("Tango.TangoContentProvider", "onCreate");
    return true;
  }

  public ParcelFileDescriptor openFile(Uri paramUri, String paramString)
  {
    String str = paramUri.getLastPathSegment();
    if (str == null)
      return null;
    try
    {
      TangoApp.ensureInitialized();
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      try
      {
        while (str.equals(LogReporter.outFileName()))
        {
          ParcelFileDescriptor localParcelFileDescriptor2 = ParcelFileDescriptor.open(new File(LogReporter.getBinLogFilePath()), 268435456);
          Exception localException1;
          try
          {
            LogReporter.onEmailSent();
            return localParcelFileDescriptor2;
          }
          catch (Exception localException3)
          {
            localParcelFileDescriptor1 = localParcelFileDescriptor2;
            localException1 = localException3;
          }
          localException1.printStackTrace();
          return localParcelFileDescriptor1;
          localWrongTangoRuntimeVersionException = localWrongTangoRuntimeVersionException;
          Log.e("Tango.TangoContentProvider", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
        }
      }
      catch (Exception localException2)
      {
        while (true)
          ParcelFileDescriptor localParcelFileDescriptor1 = null;
      }
    }
    return null;
  }

  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    return null;
  }

  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    return 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.TangoContentProvider
 * JD-Core Version:    0.6.2
 */