package com.sgiggle.production;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.DisplaySettingsMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;

public class TipsActivity extends ActivityBase
{
  private static final String TAG = "Tango.TipsActivity";
  private ViewGroup m_progressView;
  private WebView m_webView;

  public void onBackPressed()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DisplaySettingsMessage());
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.TipsActivity", "onCreate(savedInstanceState=" + paramBundle + ")");
    super.onCreate(paramBundle);
    setContentView(2130903128);
    this.m_progressView = ((ViewGroup)findViewById(2131361988));
    this.m_webView = ((WebView)findViewById(2131362116));
    this.m_webView.getSettings().setJavaScriptEnabled(true);
    this.m_webView.setWebViewClient(new MyWebViewClient(null));
    String str = getResources().getString(2131296324);
    this.m_webView.loadUrl(str);
  }

  protected void onDestroy()
  {
    Log.v("Tango.TipsActivity", "onDestroy()");
    super.onDestroy();
  }

  private final class MyWebViewClient extends WebViewClient
  {
    private MyWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      Log.d("Tango.TipsActivity", "Finished loading URL: " + paramString);
      TipsActivity.this.m_webView.setVisibility(0);
      TipsActivity.this.m_progressView.setVisibility(8);
      TipsActivity.this.m_webView.requestFocus(130);
    }

    public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
    {
      TipsActivity localTipsActivity = TipsActivity.this;
      Log.d("Tango.TipsActivity", "onReceivedError: " + paramString1);
      Toast.makeText(localTipsActivity, paramString1, 0).show();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.TipsActivity
 * JD-Core Version:    0.6.2
 */