package com.sgiggle.production;

public final class UIConstants
{
  public static final int CALL_RECEIVED_NOTIFICATION_ID = 1;
  public static final int CONVERSATION_MESSAGE_ERROR_NOTIFICATION_ID = 7;
  public static final int CONVERSATION_MESSAGE_NOTIFICATION_ID = 5;
  public static final int CTA_ALERT_BASE_NOTIFICATION_ID = 1000;
  public static final long DEVICE_CONTACT_ID_UNKNOWN = -1L;
  public static final int EMAIL_INVITE_LIMIT = 500;
  public static final int INVISIBLE_NOTIFICATION_ID = 6;
  public static final int LOW_ON_SPACE_NOTIFICATION_ID = 4;
  public static final String MISSED_CALL_JID = "missedJid";
  public static final String MISSED_CALL_NAME = "missedDisplayname";
  public static final int MISSED_CALL_NOTIFICATION_ID = 2;
  public static final String MISSED_CALL_WHEN = "missedWhen";
  public static final int PUSH_MESSAGE_NOTIFICATION_ID = 3;
  public static final String TIPS_URL_FOR_LOW_MEMORY = "http://www.tango.me/android/lowmem";

  public static class CTA
  {
    public static final String ACTION_CLEAR = "CLEAR";
    public static final String ACTION_OPEN = "OPEN";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.UIConstants
 * JD-Core Version:    0.6.2
 */