package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.UpdateRequiredEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.UpdateRequiredPayload;

public class UpdateRequiredActivity extends ActivityBase
  implements View.OnClickListener
{
  private static final String TAG = "UpdateRequiredActivity";
  private static final String s_updateURI = "market://details?id=com.sgiggle.production";
  private String m_action;

  public static void forceTangoUpdate(Context paramContext)
  {
    try
    {
      Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.sgiggle.production"));
      localIntent.addFlags(268435456);
      localIntent.addFlags(262144);
      paramContext.startActivity(localIntent);
      return;
    }
    catch (Exception localException)
    {
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
      localBuilder.setTitle(2131296378);
      localBuilder.setMessage(2131296379);
      localBuilder.setCancelable(false);
      localBuilder.setPositiveButton(2131296349, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
        }
      });
      localBuilder.create().show();
    }
  }

  public static String getUpdateURI()
  {
    return "market://details?id=com.sgiggle.production";
  }

  protected void handleNewMessage(Message paramMessage)
  {
    Log.d("UpdateRequiredActivity", "handleNewMessage(): Message = " + paramMessage);
    switch (paramMessage.getType())
    {
    default:
    case 35075:
    }
    String str;
    do
    {
      return;
      MediaEngineMessage.UpdateRequiredEvent localUpdateRequiredEvent = (MediaEngineMessage.UpdateRequiredEvent)paramMessage;
      str = ((SessionMessages.UpdateRequiredPayload)localUpdateRequiredEvent.payload()).getMessage();
      this.m_action = ((SessionMessages.UpdateRequiredPayload)localUpdateRequiredEvent.payload()).getAction();
      Log.d("UpdateRequiredActivity", "handleNewMessage(): message = [" + str + "], action = [" + this.m_action + "]");
    }
    while (TextUtils.isEmpty(str));
    ((TextView)findViewById(2131361877)).setText(str);
  }

  public void onBackPressed()
  {
  }

  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131362131);
    try
    {
      startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.m_action)));
      return;
    }
    catch (Exception localException)
    {
      forceTangoUpdate(this);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("UpdateRequiredActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903129);
    setRequestedOrientation(1);
    ((Button)findViewById(2131362131)).setOnClickListener(this);
    handleNewMessage(getFirstMessage());
  }

  protected void onDestroy()
  {
    Log.d("UpdateRequiredActivity", "onDestroy()");
    super.onDestroy();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.UpdateRequiredActivity
 * JD-Core Version:    0.6.2
 */