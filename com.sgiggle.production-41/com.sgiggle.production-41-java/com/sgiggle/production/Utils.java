package com.sgiggle.production;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import com.sgiggle.contacts.ContactStore.ContactOrderPair.ContactOrder;
import com.sgiggle.media_engine.MediaEngineMessage.QueryLeaveMessageResultMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.dialog.TangoAlertDialog;
import com.sgiggle.production.dialog.TangoAlertDialog.Builder;
import com.sgiggle.production.util.BitmapLoaderThread.LoadableImage;
import com.sgiggle.util.Log;
import com.sgiggle.util.MailValidator;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.Contact.Builder;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber.Builder;
import com.sgiggle.xmpp.SessionMessages.PhoneType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class Utils
{
  private static final int SDK_VERSION_2_2 = 8;
  private static final String TAG = "Tango.Utils";
  public static final String TANGO_REGISTRATION_SUBMITTED = "Tango.Registration.Submitted";

  public static void acquirePartialWakeLock(Context paramContext, long paramLong, String paramString)
  {
    PowerManager localPowerManager = (PowerManager)paramContext.getSystemService("power");
    if (localPowerManager != null)
    {
      PowerManager.WakeLock localWakeLock = localPowerManager.newWakeLock(1, paramString);
      if (localWakeLock != null)
        localWakeLock.acquire(paramLong);
    }
  }

  public static void copyTextToClipboard(String paramString, Context paramContext)
  {
    if (TextUtils.isEmpty(paramString))
      return;
    if (Build.VERSION.SDK_INT >= 11)
    {
      ((android.content.ClipboardManager)paramContext.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("", paramString));
      return;
    }
    ((android.text.ClipboardManager)paramContext.getSystemService("clipboard")).setText(paramString);
  }

  public static void focusTextViewAndShowKeyboard(Context paramContext, TextView paramTextView)
  {
    paramTextView.requestFocus();
    ((InputMethodManager)paramContext.getSystemService("input_method")).showSoftInput(paramTextView, 1);
  }

  public static String getDisplayName(SessionMessages.Contact paramContact)
  {
    return getDisplayName(paramContact.getDisplayname(), paramContact.getFirstname(), paramContact.getLastname(), paramContact.getPhoneNumber().getSubscriberNumber(), paramContact.getEmail());
  }

  public static String getDisplayName(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    String str;
    if (!TextUtils.isEmpty(paramString1))
      str = paramString1;
    while (true)
    {
      if (str == null)
        str = "";
      return str;
      if ((!TextUtils.isEmpty(paramString2)) || (!TextUtils.isEmpty(paramString3)))
      {
        if (TextUtils.isEmpty(paramString2))
          str = paramString3;
        else if (TextUtils.isEmpty(paramString3))
          str = paramString2;
        else
          str = paramString2 + " " + paramString3;
      }
      else if (TextUtils.isEmpty(paramString4))
        str = paramString5;
      else
        str = paramString4;
    }
  }

  public static String[] getGoogleAccounts(Context paramContext)
  {
    ArrayList localArrayList = new ArrayList();
    for (Account localAccount : AccountManager.get(paramContext).getAccounts())
      if (localAccount.type.equals("com.google"))
        localArrayList.add(localAccount.name);
    String[] arrayOfString = new String[localArrayList.size()];
    localArrayList.toArray(arrayOfString);
    return arrayOfString;
  }

  public static CharSequence getRelativeTimeString(Context paramContext, long paramLong, String paramString)
  {
    Long localLong = Long.valueOf(new Date().getTime());
    if (localLong.longValue() - paramLong < 60000L)
      return paramString;
    try
    {
      CharSequence localCharSequence = DateUtils.getRelativeTimeSpanString(paramLong, localLong.longValue(), 60000L, 524288);
      return localCharSequence;
    }
    catch (Exception localException)
    {
      Log.e("Tango.Utils", "getRelativeTimeString: failed to call getRelativeTimeSpanString(). Falling back to simple formatting.");
    }
    return DateUtils.formatDateTime(paramContext, paramLong, 133137);
  }

  public static Uri getResourceUri(int paramInt)
  {
    return Uri.parse("android.resource://" + TangoApp.getInstance().getApplicationContext().getPackageName() + "/" + paramInt);
  }

  public static String getSmsAddressListFromContactList(List<SessionMessages.Contact> paramList)
  {
    if (paramList == null)
    {
      Log.w("Tango.Utils", "getSmsAddressListFromContactList: no contacts to build the string from");
      return "";
    }
    char c = getSmsContactSeparator();
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      SessionMessages.Contact localContact = (SessionMessages.Contact)localIterator.next();
      if (localStringBuilder.length() > 0)
        localStringBuilder.append(c);
      localStringBuilder.append(localContact.getPhoneNumber().getSubscriberNumber());
    }
    return localStringBuilder.toString();
  }

  public static char getSmsContactSeparator()
  {
    if ((Build.MANUFACTURER.equalsIgnoreCase("Samsung")) && ((Build.MODEL.equalsIgnoreCase("GT-I9000")) || (Build.MODEL.equalsIgnoreCase("GT-I9001")) || (Build.MODEL.equalsIgnoreCase("GT-i9003")) || (Build.MODEL.equalsIgnoreCase("GT-I9100")) || (Build.MODEL.equalsIgnoreCase("GT-I9100T")) || (Build.MODEL.equalsIgnoreCase("GT-I9103")) || (Build.MODEL.equalsIgnoreCase("GT-N7000")) || (Build.MODEL.equalsIgnoreCase("SAMSUNG-SGH-I717")) || (Build.MODEL.equalsIgnoreCase("SAMSUNG-SGH-I997")) || (Build.MODEL.equalsIgnoreCase("SAMSUNG-SGH-I727")) || (Build.MODEL.equalsIgnoreCase("SCH-i510")) || (Build.MODEL.equalsIgnoreCase("SCH-I535")) || (Build.MODEL.equalsIgnoreCase("SAMSUNG-SGH-I747")) || (Build.MODEL.equalsIgnoreCase("SGH-T999")) || (Build.MODEL.equalsIgnoreCase("GT-I9300")) || (Build.MODEL.equalsIgnoreCase("SGH-I997")) || (Build.MODEL.equalsIgnoreCase("SGH-i727")) || (Build.MODEL.equalsIgnoreCase("SGH-T959V")) || (Build.MODEL.equalsIgnoreCase("SGH-T989")) || (Build.MODEL.equalsIgnoreCase("SHW-M250K")) || (Build.MODEL.equalsIgnoreCase("SHW-M250L")) || (Build.MODEL.equalsIgnoreCase("SHW-M250S")) || (Build.MODEL.equalsIgnoreCase("SPH-D700")) || (Build.MODEL.equalsIgnoreCase("SPH-D710"))))
      return ',';
    return ';';
  }

  public static Intent getSmsIntent(String paramString1, String paramString2)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.putExtra("address", paramString1);
    localIntent.putExtra("sms_body", paramString2);
    localIntent.putExtra("android.intent.extra.TEXT", paramString2);
    localIntent.setData(Uri.parse("smsto:" + paramString1));
    localIntent.addFlags(262144);
    return localIntent;
  }

  public static String getStringFromResource(Context paramContext, String paramString)
  {
    int i = paramContext.getResources().getIdentifier(paramString, "string", paramContext.getPackageName());
    if (i == 0)
    {
      Log.w("Tango.Utils", "String [" + paramString + "] not found in resource.");
      return paramString;
    }
    return paramContext.getResources().getString(i);
  }

  public static void hideKeyboard(Context paramContext, TextView paramTextView)
  {
    ((InputMethodManager)paramContext.getSystemService("input_method")).hideSoftInputFromWindow(paramTextView.getWindowToken(), 0);
  }

  public static boolean isDeviceAndroid22Plus()
  {
    return Build.VERSION.SDK_INT >= 8;
  }

  public static boolean isDeviceCapableOfC2dm(Context paramContext)
  {
    return (Build.VERSION.SDK_INT >= 8) && (getGoogleAccounts(paramContext).length > 0);
  }

  public static boolean isEmailIntentAvailable(Context paramContext)
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("message/rfc822");
    return isIntentAvailable(paramContext, localIntent);
  }

  public static boolean isEmailStringValid(String paramString)
  {
    return MailValidator.isValid(paramString);
  }

  public static boolean isIntentAvailable(Context paramContext, Intent paramIntent)
  {
    return paramContext.getPackageManager().queryIntentActivities(paramIntent, 65536).size() > 0;
  }

  public static boolean isNetworkAvailable(Context paramContext)
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    if ((localNetworkInfo != null) && (localNetworkInfo.isAvailable()))
    {
      Log.v("Tango.Utils", "Network " + localNetworkInfo.getTypeName() + " is available");
      return true;
    }
    return false;
  }

  public static boolean isSmsIntentAvailable(Context paramContext)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setType("vnd.android-dir/mms-sms");
    return isIntentAvailable(paramContext, localIntent);
  }

  public static void markRegistrationFlag(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("Tango.Utils", 0);
    Log.v("Tango.Utils", "Set Tango.Registration.Submitted as true");
    localSharedPreferences.edit().putBoolean("Tango.Registration.Submitted", true).commit();
  }

  public static boolean readRegistrationFlag(Context paramContext)
  {
    boolean bool = paramContext.getSharedPreferences("Tango.Utils", 0).getBoolean("Tango.Registration.Submitted", false);
    Log.v("Tango.Utils", "Read Tango.Registration.Submitted as " + bool);
    return bool;
  }

  public static void setBadgeCount(TextView paramTextView, int paramInt, boolean paramBoolean, Context paramContext)
  {
    if (paramInt == 0)
    {
      paramTextView.setVisibility(8);
      return;
    }
    if (paramInt < 0)
      paramTextView.setText(2131296263);
    while (true)
    {
      paramTextView.setVisibility(0);
      if (!paramBoolean)
        break;
      paramTextView.startAnimation(AnimationUtils.loadAnimation(paramContext, 2130968583));
      return;
      if (paramInt > 99)
        paramTextView.setText(2131296262);
      else
        paramTextView.setText(Integer.toString(paramInt));
    }
  }

  public static boolean startWithUpperCase(String paramString1, String paramString2)
  {
    return (paramString1 != null) && (paramString1.length() > 0) && (paramString1.trim().toUpperCase().startsWith(paramString2));
  }

  public static class ContactComparator
    implements Comparator<Utils.IContactComparable>
  {
    private final ContactStore.ContactOrderPair.ContactOrder m_sortOrder;

    public ContactComparator()
    {
      this.m_sortOrder = ContactStore.ContactOrderPair.ContactOrder.PRIMARY;
    }

    public ContactComparator(ContactStore.ContactOrderPair.ContactOrder paramContactOrder)
    {
      this.m_sortOrder = paramContactOrder;
    }

    public int compare(Utils.IContactComparable paramIContactComparable1, Utils.IContactComparable paramIContactComparable2)
    {
      return paramIContactComparable1.compareName(this.m_sortOrder).compareToIgnoreCase(paramIContactComparable2.compareName(this.m_sortOrder));
    }
  }

  public static abstract interface IContactComparable
  {
    public abstract String compareName(ContactStore.ContactOrderPair.ContactOrder paramContactOrder);
  }

  public static class LeaveMessageDialogBuilder
  {
    public static TangoAlertDialog create(final Activity paramActivity, String paramString, boolean paramBoolean)
    {
      TangoAlertDialog.Builder localBuilder = new TangoAlertDialog.Builder(paramActivity);
      localBuilder.setMessage(Html.fromHtml(String.format(paramActivity.getResources().getString(2131296597), new Object[] { paramString }))).setCancelable(true).setDismissBeforeGoingToBackground(true).setPositiveButton(2131296289, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Utils.LeaveMessageDialogBuilder.postQueryLeaveMessageResultMessage(true);
          this.val$activity.finish();
        }
      }).setNegativeButton(2131296290, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Utils.LeaveMessageDialogBuilder.postQueryLeaveMessageResultMessage(false);
          if (this.val$finishActivityOnCancel)
            paramActivity.finish();
        }
      });
      return localBuilder.create();
    }

    private static void postQueryLeaveMessageResultMessage(boolean paramBoolean)
    {
      Log.d("Tango.Utils", " sending QueryLeaveMessageResultMessage: " + paramBoolean);
      MediaEngineMessage.QueryLeaveMessageResultMessage localQueryLeaveMessageResultMessage = new MediaEngineMessage.QueryLeaveMessageResultMessage(paramBoolean);
      MessageRouter.getInstance().postMessage("jingle", localQueryLeaveMessageResultMessage);
    }
  }

  public static class UIContact
    implements Utils.IContactComparable, BitmapLoaderThread.LoadableImage, Serializable
  {
    private static final long serialVersionUID = 1L;
    public transient boolean hasPhoto = true;
    public String m_accountId;
    public String m_countryCode;
    public String m_countryId;
    public String m_countryName;
    public long m_deviceContactId;
    public String m_displayName = "";
    public String m_email = "";
    public boolean m_favorite = false;
    public String m_firstName = "";
    public String m_lastName = "";
    public String m_middleName = "";
    public String m_namePrefix = "";
    public String m_nameSuffix = "";
    public String m_phoneNumber = "";
    public int m_phoneType = 7;
    public boolean m_selected = false;

    private UIContact()
    {
    }

    public UIContact(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
    {
      String str1;
      String str2;
      label95: String str3;
      label109: String str4;
      label124: String str5;
      if (paramString1 == null)
      {
        str1 = "";
        this.m_namePrefix = str1;
        if (paramString2 != null)
          break label167;
        str2 = "";
        this.m_firstName = str2;
        if (paramString3 != null)
          break label173;
        str3 = "";
        this.m_middleName = str3;
        if (paramString4 != null)
          break label179;
        str4 = "";
        this.m_lastName = str4;
        if (paramString5 != null)
          break label186;
        str5 = "";
        label139: this.m_nameSuffix = str5;
        if (paramString6 != null)
          break label193;
      }
      label167: label173: label179: label186: label193: for (String str6 = ""; ; str6 = paramString6)
      {
        this.m_displayName = str6;
        return;
        str1 = paramString1;
        break;
        str2 = paramString2;
        break label95;
        str3 = paramString3;
        break label109;
        str4 = paramString4;
        break label124;
        str5 = paramString5;
        break label139;
      }
    }

    public static UIContact convertFromMessageContact(SessionMessages.Contact paramContact)
    {
      UIContact localUIContact = new UIContact(paramContact.getNameprefix(), paramContact.getFirstname(), paramContact.getMiddlename(), paramContact.getLastname(), paramContact.getNamesuffix(), paramContact.getDisplayname());
      localUIContact.m_accountId = paramContact.getAccountid();
      localUIContact.m_deviceContactId = paramContact.getDeviceContactId();
      if (paramContact.hasPhoneNumber())
      {
        localUIContact.m_phoneNumber = paramContact.getPhoneNumber().getSubscriberNumber();
        localUIContact.m_phoneType = convertPhoneTypeToNative(paramContact.getPhoneNumber().getType());
      }
      if (paramContact.hasEmail())
        localUIContact.m_email = paramContact.getEmail();
      if (paramContact.hasFavorite())
        localUIContact.m_favorite = paramContact.getFavorite();
      return localUIContact;
    }

    public static int convertPhoneTypeToNative(SessionMessages.PhoneType paramPhoneType)
    {
      switch (Utils.1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$PhoneType[paramPhoneType.ordinal()])
      {
      default:
        return 7;
      case 1:
        return 2;
      case 2:
        return 1;
      case 3:
        return 3;
      case 4:
      }
      return 12;
    }

    public static SessionMessages.PhoneType convertPhoneTypeToProto(int paramInt)
    {
      switch (paramInt)
      {
      default:
        return SessionMessages.PhoneType.PHONE_TYPE_GENERIC;
      case 2:
        return SessionMessages.PhoneType.PHONE_TYPE_MOBILE;
      case 1:
        return SessionMessages.PhoneType.PHONE_TYPE_HOME;
      case 3:
        return SessionMessages.PhoneType.PHONE_TYPE_WORK;
      case 12:
      }
      return SessionMessages.PhoneType.PHONE_TYPE_MAIN;
    }

    public static UIContact getDummyContact()
    {
      return new UIContact();
    }

    public String compareName(ContactStore.ContactOrderPair.ContactOrder paramContactOrder)
    {
      String str;
      if (paramContactOrder == ContactStore.ContactOrderPair.ContactOrder.PRIMARY)
        if (!TextUtils.isEmpty(this.m_firstName))
          str = this.m_firstName;
      while (true)
      {
        if (TextUtils.isEmpty(str))
          str = this.m_displayName;
        return str;
        boolean bool2 = TextUtils.isEmpty(this.m_lastName);
        str = null;
        if (!bool2)
        {
          str = this.m_lastName;
          continue;
          if (!TextUtils.isEmpty(this.m_lastName))
          {
            str = this.m_lastName;
          }
          else
          {
            boolean bool1 = TextUtils.isEmpty(this.m_firstName);
            str = null;
            if (!bool1)
              str = this.m_firstName;
          }
        }
      }
    }

    public SessionMessages.Contact convertToMessageContact()
    {
      SessionMessages.Contact.Builder localBuilder = SessionMessages.Contact.newBuilder().setAccountid(this.m_accountId).setDeviceContactId(this.m_deviceContactId).setNameprefix(this.m_namePrefix).setFirstname(this.m_firstName).setMiddlename(this.m_middleName).setLastname(this.m_lastName).setNamesuffix(this.m_nameSuffix).setDisplayname(this.m_displayName).setFavorite(this.m_favorite);
      if (!TextUtils.isEmpty(this.m_email))
        localBuilder.setEmail(this.m_email);
      if (!TextUtils.isEmpty(this.m_phoneNumber))
        localBuilder.setPhoneNumber(SessionMessages.PhoneNumber.newBuilder().setSubscriberNumber(this.m_phoneNumber).setType(convertPhoneTypeToProto(this.m_phoneType)).build());
      return localBuilder.build();
    }

    public char dispNameFirstChar(ContactStore.ContactOrderPair.ContactOrder paramContactOrder)
    {
      if (!TextUtils.isEmpty(compareName(paramContactOrder)))
        return compareName(paramContactOrder).charAt(0);
      return ' ';
    }

    public String displayName()
    {
      return Utils.getDisplayName(this.m_displayName, this.m_firstName, this.m_lastName, this.m_phoneNumber, this.m_email);
    }

    public String firstName()
    {
      return this.m_firstName;
    }

    public Object getDataToLoadImage()
    {
      return Long.valueOf(this.m_deviceContactId);
    }

    public Object getImageId()
    {
      return Long.valueOf(this.m_deviceContactId);
    }

    public int getViewTag()
    {
      return hashCode();
    }

    public int hashCode()
    {
      return (int)this.m_deviceContactId;
    }

    public String lastName()
    {
      return this.m_lastName;
    }

    public String middleName()
    {
      return this.m_middleName;
    }

    public String namePrefix()
    {
      return this.m_namePrefix;
    }

    public String namesuffix()
    {
      return this.m_nameSuffix;
    }

    public void onLoadFailed()
    {
      this.hasPhoto = false;
    }

    public String phoneNumber()
    {
      return this.m_phoneNumber;
    }

    public boolean shouldHaveImage()
    {
      return this.hasPhoto;
    }
  }

  public static class UIMissedCall
  {
    private String m_missedJid;
    private String m_missedName;
    private long m_missedWhen;

    public UIMissedCall(String paramString1, String paramString2, long paramLong)
    {
      this.m_missedJid = paramString1;
      this.m_missedName = paramString2;
      this.m_missedWhen = paramLong;
    }

    public String getMissedJid()
    {
      return this.m_missedJid;
    }

    public String getMissedName()
    {
      return this.m_missedName;
    }

    public long getMissedWhen()
    {
      return this.m_missedWhen;
    }

    public boolean isSame(UIMissedCall paramUIMissedCall)
    {
      return (this.m_missedJid.equals(paramUIMissedCall.m_missedJid)) && (this.m_missedWhen == paramUIMissedCall.m_missedWhen);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.Utils
 * JD-Core Version:    0.6.2
 */