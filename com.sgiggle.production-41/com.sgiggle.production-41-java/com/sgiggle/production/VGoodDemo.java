package com.sgiggle.production;

import com.sgiggle.media_engine.MediaEngineMessage.BackToVGoodProductCatalogMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayProductDetailEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.xmpp.SessionMessages.ProductDetailsPayload;

public class VGoodDemo extends GenericProductDemoActivity
{
  void handleNewMessage(Message paramMessage)
  {
    if (paramMessage == null)
      return;
    switch (paramMessage.getType())
    {
    default:
    case 35224:
    }
    while (true)
    {
      super.handleNewMessage(paramMessage);
      return;
      onNewProductDetailPayload((SessionMessages.ProductDetailsPayload)((MediaEngineMessage.DisplayProductDetailEvent)paramMessage).payload());
    }
  }

  public void onBackPressed()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.BackToVGoodProductCatalogMessage());
    super.onBackPressed();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.VGoodDemo
 * JD-Core Version:    0.6.2
 */