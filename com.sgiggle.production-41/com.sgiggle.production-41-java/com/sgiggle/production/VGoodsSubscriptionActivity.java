package com.sgiggle.production;

import com.sgiggle.media_engine.MediaEngineMessage.DismissStoreBadgeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayProductDetailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.NavigateBackMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ReportPurchaseResultEvent;
import com.sgiggle.media_engine.MediaEngineMessage.VGoodProductCatalogEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.adapter.VGoodsPaymentAdapter;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogPayload;
import com.sgiggle.xmpp.SessionMessages.PurchaseResultPayload;

public class VGoodsSubscriptionActivity extends GenericProductCatalogActivity
{
  private void dismissNewBadge()
  {
    MediaEngineMessage.DismissStoreBadgeMessage localDismissStoreBadgeMessage = new MediaEngineMessage.DismissStoreBadgeMessage("product.category.vgood");
    MessageRouter.getInstance().postMessage("jingle", localDismissStoreBadgeMessage);
  }

  void handleNewMessage(Message paramMessage)
  {
    if (paramMessage == null)
      return;
    switch (paramMessage.getType())
    {
    default:
    case 35223:
    case 35200:
    }
    while (true)
    {
      super.handleNewMessage(paramMessage);
      return;
      SessionMessages.ProductCatalogPayload localProductCatalogPayload = (SessionMessages.ProductCatalogPayload)((MediaEngineMessage.VGoodProductCatalogEvent)paramMessage).payload();
      onNewProductCatalogPayload(localProductCatalogPayload);
      if (localProductCatalogPayload.getAllCached())
      {
        dismissNewBadge();
        continue;
        MediaEngineMessage.ReportPurchaseResultEvent localReportPurchaseResultEvent = (MediaEngineMessage.ReportPurchaseResultEvent)paramMessage;
        if (((SessionMessages.PurchaseResultPayload)localReportPurchaseResultEvent.payload()).hasPriceLabel())
        {
          String str1 = ((SessionMessages.PurchaseResultPayload)localReportPurchaseResultEvent.payload()).getProductMarketId();
          String str2 = ((SessionMessages.PurchaseResultPayload)localReportPurchaseResultEvent.payload()).getPriceLabel();
          this.m_adapter.markPurchased(str1, str2);
        }
      }
    }
  }

  public void onBackPressed()
  {
    MediaEngineMessage.NavigateBackMessage localNavigateBackMessage = new MediaEngineMessage.NavigateBackMessage();
    MessageRouter.getInstance().postMessage("jingle", localNavigateBackMessage);
  }

  protected void showDemo(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DisplayProductDetailMessage(paramProductCatalogEntry));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.VGoodsSubscriptionActivity
 * JD-Core Version:    0.6.2
 */