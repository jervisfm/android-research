package com.sgiggle.production;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import com.sgiggle.media_engine.MediaEngineMessage.AcknowledgeRegistrationErrorMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ValidationFailedEvent;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;

public class ValidationFailedDialogActivity extends Activity
{
  private static final String TAG = "Tango.ValidationFailedDialogActivity";
  static int VALIDATION_FAILED_DIALOG = 0;

  protected void onCreate(Bundle paramBundle)
  {
    Log.v("Tango.ValidationFailedDialogActivity", "onCreate()");
    super.onCreate(paramBundle);
    showDialog(VALIDATION_FAILED_DIALOG);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Log.v("Tango.ValidationFailedDialogActivity", "onCreateDialog()");
    return new Builder(this).create();
  }

  public static class Builder extends AlertDialog.Builder
  {
    protected ValidationFailedDialogActivity activity;

    public Builder(Context paramContext)
    {
      super();
      this.activity = ((ValidationFailedDialogActivity)paramContext);
    }

    public AlertDialog create()
    {
      Intent localIntent = this.activity.getIntent();
      String str = ((SessionMessages.OptionalPayload)((MediaEngineMessage.ValidationFailedEvent)MessageManager.getDefault().getMessageFromIntent(localIntent)).payload()).getMessage();
      setTitle(2131296346);
      setMessage(str);
      setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          MediaEngineMessage.AcknowledgeRegistrationErrorMessage localAcknowledgeRegistrationErrorMessage = new MediaEngineMessage.AcknowledgeRegistrationErrorMessage();
          MessageRouter.getInstance().postMessage("jingle", localAcknowledgeRegistrationErrorMessage);
          ValidationFailedDialogActivity.Builder.this.activity.finish();
        }
      });
      setNegativeButton(2131296349, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          MediaEngineMessage.AcknowledgeRegistrationErrorMessage localAcknowledgeRegistrationErrorMessage = new MediaEngineMessage.AcknowledgeRegistrationErrorMessage();
          MessageRouter.getInstance().postMessage("jingle", localAcknowledgeRegistrationErrorMessage);
          ValidationFailedDialogActivity.Builder.this.activity.finish();
        }
      });
      return super.create();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ValidationFailedDialogActivity
 * JD-Core Version:    0.6.2
 */