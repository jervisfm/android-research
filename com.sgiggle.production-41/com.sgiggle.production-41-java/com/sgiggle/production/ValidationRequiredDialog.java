package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import com.facebook.android.Facebook;
import com.sgiggle.iphelper.IpHelper;
import com.sgiggle.media_engine.MediaEngineMessage.EndStateNoChangeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SendValidationCodeRequestMessage;
import com.sgiggle.media_engine.MediaEngineMessage.StatsCollectorLogMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.dialog.TangoAlertDialog;
import com.sgiggle.xmpp.SessionMessages.KeyValuePair;
import com.sgiggle.xmpp.SessionMessages.KeyValuePair.Builder;
import com.sgiggle.xmpp.SessionMessages.ValidationCodeDeliveryPayload.ValidationCodeDeliveryType;
import java.util.ArrayList;
import java.util.List;

public class ValidationRequiredDialog extends TangoAlertDialog
{
  private static final String KEY_DEVICE_ID = "deviceid";
  private static final String KEY_FB_VALID_SESSION = "fb_valid_session";
  private static final String KEY_SMS_VERIFICATION_DIALOG_UI = "sms_verification_dialog_ui";
  private static final String VALUE_SMS_VERIFICATION_DIALOG_UI_VERIFICATION_DIALOG_APPEARED = "verification_dialog_appeared";
  private static final String VALUE_SMS_VERIFICATION_DIALOG_UI_VERIFICATION_REQUESTED = "verification_requested";
  private static final String VALUE_SMS_VERIFICATION_DIALOG_UI_VERIFICATION_SKIPPED = "verification_skipped";

  protected ValidationRequiredDialog(Context paramContext)
  {
    super(paramContext);
  }

  public static class Builder extends AlertDialog.Builder
  {
    protected Context m_context = null;

    public Builder(Context paramContext)
    {
      super();
      this.m_context = paramContext;
    }

    private void statsCollectorLog(String paramString)
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(SessionMessages.KeyValuePair.newBuilder().setKey("sms_verification_dialog_ui").setValue(paramString).build());
      localArrayList.add(SessionMessages.KeyValuePair.newBuilder().setKey("deviceid").setValue(IpHelper.getDevIDBase64()).build());
      SessionMessages.KeyValuePair.Builder localBuilder = SessionMessages.KeyValuePair.newBuilder().setKey("fb_valid_session");
      if (TangoApp.getInstance().getFacebook().isSessionValid());
      for (String str = "1"; ; str = "0")
      {
        localArrayList.add(localBuilder.setValue(str).build());
        MediaEngineMessage.StatsCollectorLogMessage localStatsCollectorLogMessage = new MediaEngineMessage.StatsCollectorLogMessage(localArrayList);
        MessageRouter.getInstance().postMessage("jingle", localStatsCollectorLogMessage);
        return;
      }
    }

    public AlertDialog create(String paramString)
    {
      setMessage(String.format(this.m_context.getResources().getString(2131296625), new Object[] { paramString }));
      setTitle(2131296624);
      setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          MediaEngineMessage.EndStateNoChangeMessage localEndStateNoChangeMessage = new MediaEngineMessage.EndStateNoChangeMessage();
          MessageRouter.getInstance().postMessage("jingle", localEndStateNoChangeMessage);
          ValidationRequiredDialog.Builder.this.statsCollectorLog("verification_skipped");
        }
      });
      setNegativeButton(2131296288, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          MediaEngineMessage.EndStateNoChangeMessage localEndStateNoChangeMessage = new MediaEngineMessage.EndStateNoChangeMessage();
          MessageRouter.getInstance().postMessage("jingle", localEndStateNoChangeMessage);
          ValidationRequiredDialog.Builder.this.statsCollectorLog("verification_skipped");
        }
      });
      setPositiveButton(2131296426, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          MediaEngineMessage.SendValidationCodeRequestMessage localSendValidationCodeRequestMessage = new MediaEngineMessage.SendValidationCodeRequestMessage(SessionMessages.ValidationCodeDeliveryPayload.ValidationCodeDeliveryType.SMS);
          MessageRouter.getInstance().postMessage("jingle", localSendValidationCodeRequestMessage);
          ValidationRequiredDialog.Builder.this.statsCollectorLog("verification_requested");
        }
      });
      statsCollectorLog("verification_dialog_appeared");
      return super.create();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ValidationRequiredDialog
 * JD-Core Version:    0.6.2
 */