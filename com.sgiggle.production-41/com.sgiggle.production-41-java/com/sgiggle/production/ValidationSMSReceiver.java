package com.sgiggle.production;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.telephony.SmsMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SendValidationCodeMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import java.net.URI;

class ValidationSMSReceiver extends BroadcastReceiver
{
  private static final String TAG = "Tango.ValidationSMSReceiver";

  private String extractValidationCode(String paramString1, String paramString2)
  {
    String str1 = paramString2.substring(paramString2.indexOf(paramString1));
    try
    {
      String str2 = new URI(str1).getQuery();
      String str3 = str2.substring(5 + str2.indexOf("code="));
      return str3;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    Log.d("Tango.ValidationSMSReceiver", "onReceive(): intent=" + paramIntent);
    Object[] arrayOfObject = (Object[])paramIntent.getExtras().get("pdus");
    String str1 = paramContext.getResources().getString(2131296259);
    Log.i("Tango.ValidationSMSReceiver", "Using validation_url: " + str1);
    for (int i = 0; i < arrayOfObject.length; i++)
    {
      String str2 = SmsMessage.createFromPdu((byte[])arrayOfObject[i]).getMessageBody();
      if (str2.contains(str1))
      {
        String str3 = extractValidationCode(str1, str2);
        if (str3 != null)
        {
          Log.d("Tango.ValidationSMSReceiver", "onReceive(): Validation code = [" + str3 + "]");
          MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.SendValidationCodeMessage(str3));
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.ValidationSMSReceiver
 * JD-Core Version:    0.6.2
 */