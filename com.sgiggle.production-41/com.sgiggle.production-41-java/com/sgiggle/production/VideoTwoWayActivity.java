package com.sgiggle.production;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.sgiggle.GLES20.GLRenderer;
import com.sgiggle.VideoCapture.VideoCaptureRaw;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.cafe.vgood.CafeRenderer;
import com.sgiggle.cafe.vgood.CafeViewForCanvasRenderer;
import com.sgiggle.cafe.vgood.VGoodButtonsBarView;
import com.sgiggle.cafe.vgood.VGoodRenderer;
import com.sgiggle.media_engine.MediaEngineMessage.AcknowledgeCallErrorMessage;
import com.sgiggle.media_engine.MediaEngineMessage.AddVideoMessage;
import com.sgiggle.media_engine.MediaEngineMessage.Audio2WayAvatarInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AudioAvatarInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AudioControlMessage;
import com.sgiggle.media_engine.MediaEngineMessage.AudioVideoInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AvatarErrorEvent;
import com.sgiggle.media_engine.MediaEngineMessage.AvatarRenderRequestEvent;
import com.sgiggle.media_engine.MediaEngineMessage.CallErrorEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ClearMissedCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayAnimationEvent;
import com.sgiggle.media_engine.MediaEngineMessage.GameErrorEvent;
import com.sgiggle.media_engine.MediaEngineMessage.InitiateVGoodMessage;
import com.sgiggle.media_engine.MediaEngineMessage.RemoveVideoMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SwitchAvatarEvent;
import com.sgiggle.media_engine.MediaEngineMessage.SwitchCameraMessage;
import com.sgiggle.media_engine.MediaEngineMessage.TerminateCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.VGoodErrorEvent;
import com.sgiggle.media_engine.MediaEngineMessage.VideoAvatarInProgressEvent;
import com.sgiggle.media_engine.MediaEngineMessage.WandPressedMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.avatar.AvatarRenderer;
import com.sgiggle.production.payments.BillingServiceManager;
import com.sgiggle.production.receiver.BluetoothButtonReceiver;
import com.sgiggle.production.receiver.BluetoothButtonReceiver.ButtonHandler;
import com.sgiggle.production.receiver.WifiLockReceiver;
import com.sgiggle.screen.ScreenLogger;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.AvatarErrorPayload;
import com.sgiggle.xmpp.SessionMessages.AvatarErrorPayload.Error;
import com.sgiggle.xmpp.SessionMessages.AvatarInfo;
import com.sgiggle.xmpp.SessionMessages.AvatarRenderRequestPayload;
import com.sgiggle.xmpp.SessionMessages.CameraPosition;
import com.sgiggle.xmpp.SessionMessages.ControlAnimationPayload;
import com.sgiggle.xmpp.SessionMessages.GameErrorPayload;
import com.sgiggle.xmpp.SessionMessages.GameErrorPayload.Error;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic.Type;
import com.sgiggle.xmpp.SessionMessages.VGoodErrorPayload;
import com.sgiggle.xmpp.SessionMessages.VGoodErrorPayload.Error;
import com.sgiggle.xmpp.SessionMessages.VGoodSupportType;
import com.sgiggle.xmpp.SessionMessages.WandLocationType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class VideoTwoWayActivity extends ActivityBase
  implements View.OnClickListener, DialogInterface.OnCancelListener
{
  private static final int DEBUG_INFO_UPDATE_INTERVAL = 500;
  private static final int DIALOG_CALL_ON_HOLD = 0;
  private static final int DIALOG_LEAVE_MESSAGE = 1;
  private static final String PREF_SWITCH_CAMERA_BUBBLE_DISMISSED = "switch_camera_bubble_dismissed";
  private static final int SHOW_IGNORED_CALL = 7;
  private static final int SHOW_IGNORED_CALL_DELAY = 500;
  private static final int SHOW_WAND_BUBBLE = 8;
  protected static final String TAG = "Tango.Video";
  private static final long THRESHOLD_SWITCH_CAMERA = 1000L;
  private static boolean s_showWand = false;
  private final int DISMISS_BUBBLE_DELAY = 5000;
  private final int DISMISS_BUBBLE_MSG = 0;
  private final int TOGGLE_BACK_CONTROL_BAR_DELAY = 15000;
  private final int TOGGLE_BACK_CONTROL_BAR_MSG = 1;
  private boolean animRestart = false;
  Animation.AnimationListener animationListener = new Animation.AnimationListener()
  {
    public void onAnimationEnd(Animation paramAnonymousAnimation)
    {
      if (!VideoTwoWayActivity.this.m_showVgoodBar)
        VideoTwoWayActivity.this.m_vgoodBtnLayout.setVisibility(8);
      VideoTwoWayActivity.this.setControlBarAnimation(false);
    }

    public void onAnimationRepeat(Animation paramAnonymousAnimation)
    {
    }

    public void onAnimationStart(Animation paramAnonymousAnimation)
    {
      if (VideoTwoWayActivity.this.m_showVgoodBar)
        VideoTwoWayActivity.this.restartControlBarTimer();
    }
  };
  Timer controlBarTimer = new Timer();
  private boolean hasControlBarAnimation = false;
  private boolean hasVGoodAnimation = false;
  private Animation mAnimationSlideInRight;
  private Animation mAnimationSlideInTop;
  private Animation mAnimationSlideOutRight;
  private Animation mAnimationSlideOutTop;
  protected AvatarRenderer mLocalAvatarRenderer = null;
  protected AvatarRenderer mRemoteAvatarRenderer = null;
  private VGoodRenderer mVGoodRenderer = null;
  private CafeViewForCanvasRenderer mVGoodSurfaceView = null;
  private TextView m_alertView = null;
  private BluetoothButtonReceiver m_btBtnReceiver = new BluetoothButtonReceiver();
  private RelativeLayout m_bubbleLayout;
  private boolean m_cafeInitialised = false;
  private TextView m_callStatusTextView;
  private int m_camera_count = VideoCaptureRaw.getCameraCount();
  protected int m_camera_type = -1;
  private boolean m_canCleanCafeOnPause = true;
  protected RelativeLayout m_controlsLayout;
  private Timer m_debugInfoTimer;
  protected TextView m_debugInfoView = null;
  private View m_endButton = null;
  private ImageView m_endImage = null;
  private ImageView m_endIndicator = null;
  private int m_endResId;
  private boolean m_firstVideoEvent = true;
  protected FrameLayout m_frameLayout;
  protected Handler m_handler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      if (VideoTwoWayActivity.this.m_isDestroyed)
        Log.d("Tango.Video", "Handler: ignoring message " + paramAnonymousMessage + "; we're destroyed!");
      do
      {
        return;
        Log.d("Tango.Video", "handleMessage " + paramAnonymousMessage + " call state = " + VideoTwoWayActivity.this.m_session.m_callState);
        switch (paramAnonymousMessage.what)
        {
        case 4:
        case 5:
        case 6:
        default:
          return;
        case 2:
          VideoTwoWayActivity.this.m_video_ui.updateLayout();
          return;
        case 3:
          VideoTwoWayActivity.this.m_video_ui.updateRendererSize(paramAnonymousMessage.arg1, paramAnonymousMessage.arg2);
          return;
        case 7:
          VideoTwoWayActivity.this.showIgnoredCallAlert(VideoTwoWayActivity.this.m_session.m_peerName);
          return;
        case 8:
        }
      }
      while (!VideoTwoWayActivity.this.hasSwitchCameraTipBubbleDismissed());
      VideoTwoWayActivity.this.setupWarningBubble(VideoTwoWayActivity.this.getString(2131296537), null, true, false, false);
    }
  };
  private AlertDialog m_ignoredCallAlert;
  private boolean m_isDestroyed = false;
  private boolean m_isInPortrait = false;
  private boolean m_isViewInitialized = false;
  private TextView m_lowBandwidthView = null;
  private View m_muteButton = null;
  private ImageView m_muteImage = null;
  private ImageView m_muteIndicator = null;
  private int m_muteResId;
  private TextView m_nameTextView;
  protected int m_orientation = -1;
  private SharedPreferences m_prefs;
  private boolean m_resumeFromAvatar = false;
  private boolean m_resumed;
  protected CallSession m_session;
  protected boolean m_showVgoodBar = false;
  private ImageButton m_switchCameraButton;
  protected RelativeLayout m_switchCameraLayout;
  private long m_timestampOld = 0L;
  private TextView m_userNameTxt;
  protected VGoodButtonsBarView m_vgoodBtnLayout = null;
  private View m_videoButton = null;
  private ImageView m_videoImage = null;
  private ImageView m_videoIndicator = null;
  private int m_videoResId;
  protected VideoUI m_video_ui;
  private ImageButton m_wandButton;
  private WifiLockReceiver m_wifiLockReceiver;
  TimerTask toggleControlBarTask;
  private Handler uihandler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        Log.w("Tango.Video", "uihandler message not handled");
      case 1:
        do
          return;
        while ((VideoTwoWayActivity.this.m_vgoodBtnLayout == null) || (VideoTwoWayActivity.this.m_vgoodBtnLayout.getVisibility() != 0));
        VideoTwoWayActivity.this.toggleWandBtn();
        return;
      case 0:
      }
      VideoTwoWayActivity.this.dismissBubbleView(false, true);
    }
  };

  private void addVGoodViewAndBringToFront()
  {
    Log.v("Tango.Video", "addGLViewAndBringToFront");
    if (GLRenderer.isSupported())
      this.m_video_ui.showCafe();
    do
    {
      return;
      AbsoluteLayout localAbsoluteLayout = (AbsoluteLayout)findViewById(2131362175);
      int i = localAbsoluteLayout.getMeasuredWidth();
      int j = localAbsoluteLayout.getMeasuredHeight();
      int k = getWindowManager().getDefaultDisplay().getWidth();
      int m = getWindowManager().getDefaultDisplay().getHeight();
      int n = Math.min(k, i);
      int i1 = Math.min(m, j);
      if (this.mVGoodSurfaceView == null)
      {
        Log.w("Tango.Video", "add surface view called before initialization");
        return;
      }
      FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(n, i1);
      this.mVGoodSurfaceView.setLayoutParams(localLayoutParams);
      this.m_frameLayout.addView(this.mVGoodSurfaceView);
      this.mVGoodSurfaceView.setZOrderMediaOverlay(true);
      this.m_switchCameraLayout.bringToFront();
      this.m_video_ui.bringToFront();
      if (!this.m_showVgoodBar)
        this.m_controlsLayout.bringToFront();
    }
    while ((!TangoApp.g_screenLoggerEnabled) || (this.m_debugInfoView == null));
    this.m_debugInfoView.bringToFront();
  }

  private void animationOnPause()
  {
    Log.v("VGOOD", "animationOnPause()");
    stopAndRemoveVGoodView();
  }

  private void animationOnResume()
  {
  }

  private void avatarOnPause()
  {
    Log.v("Avatar", "avatarOnPause()");
    this.m_video_ui.onAvatarPaused();
  }

  private void avatarOnResume()
  {
    Log.v("Tango.Video", "avatarOnResume()");
    if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.NONE)
      return;
    CafeMgr.Resume();
    this.m_video_ui.onAvatarResumed();
  }

  private void cleanUpAvatar()
  {
    if ((this.m_session != null) && (this.m_session.m_avatarDirection != SessionMessages.AvatarControlPayload.Direction.NONE))
      Log.e("Tango.Video", "avatar direction is not NONE, do not clean avatar here!");
    do
    {
      return;
      this.mRemoteAvatarRenderer.onAvatarChanged(null);
      this.mLocalAvatarRenderer.onAvatarChanged(null);
      this.m_video_ui.onCleanUpAvatar();
      CafeMgr.Reset();
    }
    while (this.m_vgoodBtnLayout == null);
    this.m_vgoodBtnLayout.hideCurrentPlayingAnimation();
    VGoodButtonsBarView localVGoodButtonsBarView = this.m_vgoodBtnLayout;
    if ((this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.BOTH) || (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND));
    for (boolean bool = true; ; bool = false)
    {
      localVGoodButtonsBarView.reenableAllButtons(bool);
      setFilterHightlightWithBackground();
      return;
    }
  }

  private void clearIgnoredCall()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ClearMissedCallMessage(this.m_session.getPeerAccountId()));
  }

  private Dialog createCallOnHoldDialog()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setMessage(2131296408).setCancelable(true).setPositiveButton(2131296409, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Intent localIntent = new Intent();
        localIntent.setAction("android.intent.action.VoIP_RESUME_CALL");
        localIntent.putExtra("ResumeType", 1);
        VideoTwoWayActivity.this.sendBroadcast(localIntent);
      }
    }).setNegativeButton(2131296410, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        VideoTwoWayActivity.this.hangUpCall();
        VideoTwoWayActivity.this.moveTaskToBack(true);
      }
    });
    return localBuilder.create();
  }

  private Dialog createLeaveMessageDialog()
  {
    getWindow().clearFlags(128);
    return Utils.LeaveMessageDialogBuilder.create(this, this.m_session.m_peerName, true);
  }

  private void createVGoodViewAndRenderer()
  {
    this.mVGoodRenderer = new VGoodRenderer(GLRenderer.isSupported());
    if (!GLRenderer.isSupported())
    {
      this.mVGoodSurfaceView = new CafeViewForCanvasRenderer(this, this.m_orientation);
      this.mVGoodSurfaceView.onResume();
      this.mVGoodSurfaceView.getRenderer().addCafeViewRenderer(this.mVGoodRenderer);
    }
  }

  private void dismissBubbleView(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramBoolean2)
    {
      this.m_bubbleLayout = ((RelativeLayout)findViewById(2131362200));
      if ((this.m_bubbleLayout != null) && (this.m_bubbleLayout.getVisibility() == 0))
      {
        this.m_bubbleLayout.setVisibility(8);
        SharedPreferences.Editor localEditor = this.m_prefs.edit();
        localEditor.putBoolean("switch_camera_bubble_dismissed", true);
        localEditor.commit();
      }
    }
    if (paramBoolean1)
    {
      this.m_bubbleLayout = ((RelativeLayout)findViewById(2131362132));
      if ((this.m_bubbleLayout != null) && (this.m_bubbleLayout.getVisibility() == 0))
        this.m_bubbleLayout.setVisibility(8);
      if (s_showWand)
      {
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.WandPressedMessage(SessionMessages.WandLocationType.LOCATION_IN_CALL));
        s_showWand = false;
      }
    }
  }

  private void dismissKeyguard(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      getWindow().addFlags(4194304);
      getWindow().addFlags(524288);
      return;
    }
    getWindow().clearFlags(4194304);
    getWindow().clearFlags(524288);
  }

  private void dismissVideoBubbleViewByAvatar()
  {
    Log.v("Tango.Video", "dismissVideoBubbleViewByAvatar");
    this.uihandler.removeMessages(0);
    this.m_bubbleLayout = ((RelativeLayout)findViewById(2131362200));
    if ((this.m_bubbleLayout != null) && (this.m_bubbleLayout.getVisibility() == 0))
    {
      this.m_bubbleLayout.setVisibility(8);
      Log.v("Tango.Video", "dismissVideoBubbleViewByAvatar() m_bubbleLayout.setVisibility(View.GONE) ");
    }
  }

  private void displayVGoodWarning()
  {
    if (CafeMgr.vgoodSupport == SessionMessages.VGoodSupportType.VGOOD_UNSUPPORTED_CLIENT)
    {
      Log.e("Tango.Video", "VGOOD_UNSUPPORTED_CLIENT");
      setupWarningBubble(getString(2131296528), getString(2131296529), true, false, true);
    }
    do
    {
      return;
      if (CafeMgr.vgoodSupport == SessionMessages.VGoodSupportType.VGOOD_UNSUPPORTED_PLATFORM)
      {
        Log.e("Tango.Video", "VGOOD_UNSUPPORTED_PLATFORM");
        setupWarningBubble(getString(2131296528), getString(2131296530), true, false, true);
        return;
      }
      if (CafeMgr.vgoodSupport == SessionMessages.VGoodSupportType.VGOOD_DOWNLOADING)
      {
        Log.e("Tango.Video", "VGOOD_DOWNLOADING");
        setupWarningBubble(getString(2131296528), getString(2131296531), true, false, true);
        return;
      }
    }
    while (CafeMgr.vgoodSupport != SessionMessages.VGoodSupportType.VGOOD_INVALID_STATE);
    Log.e("Tango.Video", "VGOOD_INVALID_STATE");
    setupWarningBubble(getString(2131296528), getString(2131296533), true, false, true);
  }

  private void handleAnimationEvent(com.sgiggle.messaging.Message paramMessage)
  {
    Log.v("Tango.Video", "handleAnimationEvent");
    MediaEngineMessage.DisplayAnimationEvent localDisplayAnimationEvent = (MediaEngineMessage.DisplayAnimationEvent)paramMessage;
    long l = ((SessionMessages.ControlAnimationPayload)localDisplayAnimationEvent.payload()).getAssetId();
    boolean bool = ((SessionMessages.ControlAnimationPayload)localDisplayAnimationEvent.payload()).getRestart();
    Log.w("VGOOD", "restart? " + this.animRestart);
    if ((bool == true) && (this.hasVGoodAnimation))
    {
      animationOnPause();
      animationOnResume();
      localInitiateVGoodMessage = new MediaEngineMessage.InitiateVGoodMessage(Integer.valueOf((int)l));
      MessageRouter.getInstance().postMessage("jingle", localInitiateVGoodMessage);
      this.animRestart = true;
    }
    while (this.hasVGoodAnimation)
    {
      MediaEngineMessage.InitiateVGoodMessage localInitiateVGoodMessage;
      return;
    }
    createVGoodViewAndRenderer();
    addVGoodViewAndBringToFront();
    this.hasVGoodAnimation = true;
    this.mVGoodRenderer.handleDisplayAnimationEvent(localDisplayAnimationEvent);
    this.m_vgoodBtnLayout.updateButtons(1);
    this.m_switchCameraLayout.bringToFront();
    this.m_video_ui.bringToFront();
    this.m_controlsLayout.bringToFront();
    this.m_vgoodBtnLayout.bringToFront();
    if ((TangoApp.g_screenLoggerEnabled) && (this.m_debugInfoView != null))
      this.m_debugInfoView.bringToFront();
    restartControlBarTimer();
  }

  private void handleAudioInInitializationEvent()
  {
  }

  private void handleAudioInProgressEvent()
  {
    cleanUpAvatar();
    this.m_callStatusTextView.setVisibility(4);
    this.m_nameTextView.setVisibility(4);
    setVolumeControlStream(0);
  }

  private void handleAvatarEvent(SessionMessages.AvatarControlPayload paramAvatarControlPayload, boolean paramBoolean)
  {
    this.m_camera_type = CameraToType(this.m_session.m_cameraPosition);
    SessionMessages.AvatarControlPayload.Direction localDirection = paramAvatarControlPayload.getDirection();
    if (TangoApp.getInstance().getAppRunningState() == TangoApp.AppState.APP_STATE_BACKGROUND)
    {
      Log.i("Tango.Video", "handleAvatarEvent  in background, avatar state shoulb be changed to : ,dir:" + localDirection);
      return;
    }
    Log.i("Tango.Video", "handleAvatarEvent avatar state will be changed to : ,dir:" + localDirection);
    this.m_session.m_avatarDirection = localDirection;
    if (this.m_session.m_avatarDirection != SessionMessages.AvatarControlPayload.Direction.NONE)
      dismissVideoBubbleViewByAvatar();
    if (!paramBoolean)
      this.m_session.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.NONE;
    while (true)
    {
      if (!this.m_session.pipSwappable())
        this.m_session.resetPipSwapped();
      stopAndRemoveVGoodView();
      this.mRemoteAvatarRenderer.onAvatarChanged(paramAvatarControlPayload);
      this.mLocalAvatarRenderer.onAvatarChanged(paramAvatarControlPayload);
      this.m_video_ui.onAvatarChanged();
      updateVGoodBar();
      if (paramBoolean)
        break;
      this.m_resumeFromAvatar = true;
      return;
      if (localDirection == SessionMessages.AvatarControlPayload.Direction.SENDER)
        this.m_session.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.RECEIVE;
      else if (localDirection == SessionMessages.AvatarControlPayload.Direction.RECEIVER)
        this.m_session.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.SEND;
      else
        this.m_session.m_videoDirection = SessionMessages.MediaSessionPayload.Direction.NONE;
    }
  }

  private void handleAvatarRenderRequest(com.sgiggle.messaging.Message paramMessage)
  {
    MediaEngineMessage.AvatarRenderRequestEvent localAvatarRenderRequestEvent = (MediaEngineMessage.AvatarRenderRequestEvent)paramMessage;
    if (((SessionMessages.AvatarRenderRequestPayload)localAvatarRenderRequestEvent.payload()).getIsLocal())
      if (this.mLocalAvatarRenderer != null)
        this.mLocalAvatarRenderer.handleRenderRequest(localAvatarRenderRequestEvent);
    while (this.mRemoteAvatarRenderer == null)
      return;
    this.mRemoteAvatarRenderer.handleRenderRequest(localAvatarRenderRequestEvent);
  }

  private void handleBandwidthEvent()
  {
    TextView localTextView = this.m_lowBandwidthView;
    if (this.m_session.m_showLowBandwidth);
    for (int i = 0; ; i = 4)
    {
      localTextView.setVisibility(i);
      return;
    }
  }

  private void handleCallDisconnectingEvent()
  {
    this.m_session.m_callState = CallSession.CallState.CALL_STATE_DISCONNECTED;
    this.mLocalAvatarRenderer.onSurfaceDestroyed();
    this.mRemoteAvatarRenderer.onSurfaceDestroyed();
    this.m_canCleanCafeOnPause = true;
    Log.i("Tango.Video", "handleCallDisconnectingEvent m_session.m_callState =" + this.m_session.m_callState);
    finish();
  }

  private void handleCallErrorEvent(MediaEngineMessage.CallErrorEvent paramCallErrorEvent)
  {
    if ((!this.m_isDestroyed) && (!isFinishing()))
    {
      this.m_session.m_callState = CallSession.CallState.CALL_STATE_DISCONNECTED;
      switchToPortrait();
      showCallErrorDialog(this, paramCallErrorEvent);
    }
  }

  private void handleInCallAlertEvent()
  {
    if (this.m_session.m_showInCallAlert)
    {
      this.m_alertView.setText(this.m_session.m_inCallAlertText, TextView.BufferType.NORMAL);
      this.m_alertView.setVisibility(0);
      return;
    }
    this.m_alertView.setVisibility(4);
  }

  private void handleMissedCallEvent()
  {
    if (this.m_session.m_callState == CallSession.CallState.CALL_STATE_DIALING)
    {
      this.m_callStatusTextView.setText(getResources().getString(2131296338));
      this.m_session.m_callState = CallSession.CallState.CALL_STATE_DISCONNECTED;
      this.m_handler.sendEmptyMessageDelayed(7, 500L);
      return;
    }
    clearIgnoredCall();
    finish();
  }

  private void handleSendCallAcceptedEvent()
  {
    dismissKeyguard(true);
    this.m_callStatusTextView.setText(getResources().getString(2131296336));
  }

  private void handleSendCallInvitationEvent()
  {
    dismissPendingIgnoredCallAlert();
    dismissBubbleView(false, true);
    handleVideoEvent();
    this.m_callStatusTextView.setText(getResources().getString(2131296334));
  }

  private void handleVideoEvent()
  {
    cleanUpAvatar();
    Log.d("Tango.Video", "handleVideoEvent: m_cameraPosition=" + this.m_session.m_cameraPosition + ", direction=" + this.m_session.m_videoDirection + ", m_callerInitVideoCall=" + this.m_session.m_callerInitVideoCall + ", m_callState=" + this.m_session.m_callState + ",m_firstVideoEvent=" + this.m_firstVideoEvent);
    if (!this.m_session.pipSwappable())
      this.m_session.resetPipSwapped();
    this.m_camera_type = CameraToType(this.m_session.m_cameraPosition);
    initViews();
    updateVGoodBar();
    if ((this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.BOTH) && (this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.SEND))
      setFilter(-1, -1);
  }

  private void handleVideoModeChangedEvent()
  {
    Log.v("Tango.Video", "handleVideoModeChangedEvent() cameraPos=" + this.m_session.m_cameraPosition);
    if ((this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.BOTH) && (this.m_video_ui.pipSwapSupported()))
    {
      if (this.m_session.m_cameraPosition != SessionMessages.CameraPosition.CAM_BACK)
        break label107;
      this.m_session.setPipSwapped(true);
    }
    while (true)
    {
      this.m_camera_type = CameraToType(this.m_session.m_cameraPosition);
      this.m_video_ui.onVideoModeChanged();
      updateVGoodBar();
      return;
      label107: if (this.m_session.m_cameraPosition == SessionMessages.CameraPosition.CAM_FRONT)
        this.m_session.setPipSwapped(false);
    }
  }

  private void hangUpCall()
  {
    Log.v("Tango.Video", "onClick(): Terminate call...");
    this.m_session.m_callState = CallSession.CallState.CALL_STATE_DISCONNECTING;
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.TerminateCallMessage(this.m_session.getPeerAccountId()));
  }

  private boolean hasSwitchCameraTipBubbleDismissed()
  {
    return this.m_prefs.getBoolean("switch_camera_bubble_dismissed", false);
  }

  private void initButtons()
  {
    Log.v("Tango.Video", "initButtons");
    this.m_frameLayout = ((FrameLayout)findViewById(2131362174));
    this.m_switchCameraLayout = ((RelativeLayout)findViewById(2131362193));
    this.m_controlsLayout = ((RelativeLayout)findViewById(2131362202));
    stopAndRemoveVGoodView();
    this.m_switchCameraLayout.bringToFront();
    this.m_video_ui.bringToFront();
    this.m_controlsLayout.bringToFront();
    this.m_vgoodBtnLayout = new VGoodButtonsBarView(this, this.m_orientation);
    boolean bool;
    FrameLayout.LayoutParams localLayoutParams;
    label198: int i;
    if (this.m_session != null)
    {
      VGoodButtonsBarView localVGoodButtonsBarView = this.m_vgoodBtnLayout;
      List localList = this.m_session.getVGoodData();
      if ((this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.BOTH) || (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND))
      {
        bool = true;
        localVGoodButtonsBarView.setData(localList, bool);
        this.m_vgoodBtnLayout.setEmptySlots(this.m_session.getEmptySlotCount());
      }
    }
    else
    {
      this.m_vgoodBtnLayout.setVisibility(8);
      if (this.m_orientation != 1)
        break label729;
      localLayoutParams = new FrameLayout.LayoutParams(-1, -2, 80);
      this.m_frameLayout.addView(this.m_vgoodBtnLayout, localLayoutParams);
      this.m_videoButton = findViewById(2131362203);
      this.m_videoImage = ((ImageView)findViewById(2131362204));
      this.m_videoIndicator = ((ImageView)findViewById(2131362205));
      this.m_videoButton.setOnClickListener(this);
      this.m_endButton = findViewById(2131362206);
      this.m_endImage = ((ImageView)findViewById(2131362207));
      this.m_endIndicator = ((ImageView)findViewById(2131362208));
      this.m_endButton.setOnClickListener(this);
      this.m_muteButton = findViewById(2131361818);
      this.m_muteImage = ((ImageView)findViewById(2131362209));
      this.m_muteIndicator = ((ImageView)findViewById(2131362210));
      this.m_muteButton.setOnClickListener(this);
      this.m_callStatusTextView = ((TextView)findViewById(2131361807));
      this.m_nameTextView = ((TextView)findViewById(2131361806));
      Log.i("Tango.Video", "call state " + this.m_session.m_callState);
      if (this.m_session.m_callState != CallSession.CallState.CALL_STATE_ACTIVE)
        break label744;
      i = 4;
      label427: if (this.m_callStatusTextView != null)
      {
        this.m_callStatusTextView.setVisibility(i);
        this.m_nameTextView.setVisibility(i);
        this.m_nameTextView.setText(this.m_session.m_peerName);
      }
      switch (11.$SwitchMap$com$sgiggle$production$CallSession$CallState[this.m_session.m_callState.ordinal()])
      {
      default:
        label508: if (this.m_orientation == 0)
        {
          this.m_muteResId = 2130837663;
          this.m_endResId = 2130837662;
          this.m_videoResId = 2130837664;
          this.m_lowBandwidthView = ((TextView)findViewById(2131362197));
          this.m_lowBandwidthView.setText(getString(2131296344), TextView.BufferType.NORMAL);
          this.m_lowBandwidthView.setVisibility(4);
          this.m_alertView = ((TextView)findViewById(2131362198));
          this.m_alertView.setVisibility(4);
        }
        break;
      case 1:
      case 2:
      case 3:
      }
    }
    while (true)
    {
      if (TangoApp.g_screenLoggerEnabled)
      {
        Log.d("Tango.Video", "initializeView(): Setup debug-info timer...");
        this.m_debugInfoView = ((TextView)findViewById(2131362199));
        this.m_debugInfoView.setVisibility(0);
        if (this.m_debugInfoTimer == null)
        {
          this.m_debugInfoTimer = new Timer("debugInfo");
          this.m_debugInfoTimer.schedule(new DebugInfoTimerTask(null), 0L, 500L);
        }
      }
      if (this.m_session.m_callState == CallSession.CallState.CALL_STATE_ACTIVE)
        setupSwitchCameraTipBubble();
      if ((TangoApp.g_screenLoggerEnabled) && (this.m_debugInfoView != null))
        this.m_debugInfoView.bringToFront();
      this.m_isViewInitialized = true;
      this.hasControlBarAnimation = false;
      return;
      bool = false;
      break;
      label729: localLayoutParams = new FrameLayout.LayoutParams(-2, -1, 5);
      break label198;
      label744: i = 0;
      break label427;
      this.m_callStatusTextView.setText(getResources().getString(2131296334));
      break label508;
      this.m_callStatusTextView.setText(getResources().getString(2131296336));
      break label508;
      this.m_callStatusTextView.setText(getResources().getString(2131296338));
      break label508;
      this.m_muteResId = 2130837657;
      this.m_endResId = 2130837656;
      this.m_videoResId = 2130837658;
      this.m_lowBandwidthView = ((TextView)findViewById(2131362212));
      this.m_lowBandwidthView.setText(getString(2131296344), TextView.BufferType.NORMAL);
      this.m_lowBandwidthView.setVisibility(4);
      this.m_alertView = ((TextView)findViewById(2131362213));
      this.m_alertView.setVisibility(4);
    }
  }

  private void initOnScreenButtons()
  {
    Log.v("Tango.Video", "initOnScreenButtons");
    this.m_switchCameraButton = ((ImageButton)findViewById(2131362160));
    this.m_wandButton = ((ImageButton)findViewById(2131362194));
    this.m_userNameTxt = ((TextView)findViewById(2131361884));
  }

  private void setControlBarAnimation(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.hasControlBarAnimation = true;
      this.m_vgoodBtnLayout.updateButtons(1);
    }
    do
    {
      return;
      this.hasControlBarAnimation = false;
    }
    while (this.hasVGoodAnimation);
    this.m_vgoodBtnLayout.updateButtons(0);
  }

  private void setupSwitchCameraTipBubble()
  {
    Log.v("Tango.Video", "setupSwitchCameraTipBubble");
    int i;
    if (!hasSwitchCameraTipBubbleDismissed())
    {
      if (this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.RECEIVE)
        break label53;
      i = 1;
      if (i == 0)
        break label58;
    }
    label53: label58: for (int j = 2131296389; ; j = 2131296390)
    {
      setupWarningBubble(null, getString(j), false, true, true);
      return;
      i = 0;
      break;
    }
  }

  public static void showCallErrorDialog(Context paramContext, MediaEngineMessage.CallErrorEvent paramCallErrorEvent)
  {
    String str = Utils.getStringFromResource(paramContext, ((SessionMessages.OptionalPayload)paramCallErrorEvent.payload()).getMessage());
    new AlertDialog.Builder(paramContext).setMessage(str).setPositiveButton(2131296295, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.AcknowledgeCallErrorMessage(((SessionMessages.OptionalPayload)this.val$evt.payload()).getMessage()));
      }
    }).setCancelable(false).create().show();
  }

  private void showIgnoredCallAlert(String paramString)
  {
    String str1 = getResources().getString(2131296327);
    String str2 = String.format(getResources().getString(2131296328), new Object[] { paramString });
    this.m_ignoredCallAlert = new AlertDialog.Builder(this).setTitle(str1).setMessage(str2).setPositiveButton(2131296287, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Log.d("Tango.Video", "Ignored-Call OK button clicked.");
        VideoTwoWayActivity.this.clearIgnoredCall();
        VideoTwoWayActivity.access$402(VideoTwoWayActivity.this, null);
      }
    }).setCancelable(true).setOnCancelListener(this).create();
    this.m_ignoredCallAlert.show();
  }

  private void stopAndRemoveVGoodView()
  {
    Log.v("VGOOD", "stopAndRemoveVGoodView()");
    if (!GLRenderer.isSupported())
      if (this.mVGoodSurfaceView != null)
      {
        this.mVGoodSurfaceView.setZOrderMediaOverlay(false);
        this.mVGoodSurfaceView.onPause();
        this.mVGoodSurfaceView.setVisibility(8);
        this.m_frameLayout.removeView(this.mVGoodSurfaceView);
        this.mVGoodSurfaceView = null;
      }
    while (true)
    {
      if (this.hasVGoodAnimation)
      {
        this.hasVGoodAnimation = false;
        this.mVGoodRenderer.stopAnimation();
      }
      return;
      this.m_video_ui.hideCafe();
    }
  }

  private void switchAvatar(SessionMessages.AvatarControlPayload paramAvatarControlPayload)
  {
    this.mRemoteAvatarRenderer.switchAvatar(paramAvatarControlPayload);
    this.mLocalAvatarRenderer.switchAvatar(paramAvatarControlPayload);
    if (this.mLocalAvatarRenderer.isAvatarActived())
      this.m_vgoodBtnLayout.refreshCurrentPlayingAnimation(paramAvatarControlPayload.getLocalAvatarInfo().getAvatarid());
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(SessionMessages.VGoodCinematic.Type.AVATAR);
    if ((this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.BOTH) || (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND));
    for (boolean bool = true; ; bool = false)
    {
      if (bool)
        localArrayList.add(SessionMessages.VGoodCinematic.Type.FILTER);
      this.m_vgoodBtnLayout.enableButtonByTypes(localArrayList, bool);
      return;
    }
  }

  private void switchCamera()
  {
    if (!this.hasControlBarAnimation)
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.SwitchCameraMessage(this.m_session.getPeerAccountId(), 0));
  }

  private void switchToPortrait()
  {
    this.m_isInPortrait = true;
    this.m_camera_type = 0;
    Log.i("Tango.Video", "change orientation to portrait before show error message");
    initViews();
  }

  private boolean throttleSwitchCamera()
  {
    if (System.currentTimeMillis() - this.m_timestampOld <= 1000L)
      return true;
    this.m_timestampOld = System.currentTimeMillis();
    return false;
  }

  private void toggleWandBtn()
  {
    Log.i("Tango.Video", "onClick(): Magic WAND...");
    cancelControlBarTimer();
    dismissBubbleView(true, true);
    if (this.hasControlBarAnimation)
      return;
    setControlBarAnimation(true);
    if (this.m_vgoodBtnLayout.getVisibility() == 0)
    {
      this.m_showVgoodBar = false;
      if (this.m_orientation == 0)
      {
        this.m_vgoodBtnLayout.startAnimation(this.mAnimationSlideOutTop);
        this.m_wandButton.setImageResource(2130837606);
      }
    }
    while (true)
    {
      this.m_vgoodBtnLayout.bringToFront();
      if ((!TangoApp.g_screenLoggerEnabled) || (this.m_debugInfoView == null))
        break;
      this.m_debugInfoView.bringToFront();
      return;
      this.m_vgoodBtnLayout.startAnimation(this.mAnimationSlideOutRight);
      this.m_wandButton.setImageResource(2130837605);
      continue;
      this.m_vgoodBtnLayout.setVisibility(0);
      this.m_showVgoodBar = true;
      if (this.m_orientation == 0)
      {
        this.m_vgoodBtnLayout.startAnimation(this.mAnimationSlideInTop);
        this.m_wandButton.setImageResource(2130837726);
      }
      else
      {
        this.m_vgoodBtnLayout.startAnimation(this.mAnimationSlideInRight);
        this.m_wandButton.setImageResource(2130837727);
      }
    }
  }

  private void updateButtons()
  {
    Log.v("Tango.Video", "updateButtons");
    if (!this.m_isViewInitialized);
    label289: 
    while (true)
    {
      return;
      this.m_muteImage.setImageResource(this.m_muteResId);
      if (this.m_session.m_muted)
        if (this.m_orientation == 0)
        {
          this.m_muteIndicator.setImageResource(2130837512);
          this.m_endImage.setImageResource(this.m_endResId);
          if (this.m_orientation != 0)
            break label223;
          this.m_endIndicator.setImageResource(2130837510);
          label83: this.m_videoImage.setImageResource(this.m_videoResId);
          if ((this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.BOTH) && (this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.SEND))
            break label249;
          Log.v("Tango.Video", "updateButtons, video on");
          if (this.m_orientation != 0)
            break label236;
          this.m_videoIndicator.setImageResource(2130837512);
        }
      while (true)
      {
        if ((this.m_session.m_callState != CallSession.CallState.CALL_STATE_ACTIVE) || (this.m_session.m_avatarDirection != SessionMessages.AvatarControlPayload.Direction.NONE))
          break label289;
        setupSwitchCameraTipBubble();
        return;
        this.m_muteIndicator.setImageResource(2130837513);
        break;
        if (this.m_orientation == 0)
        {
          this.m_muteIndicator.setImageResource(2130837510);
          break;
        }
        this.m_muteIndicator.setImageResource(2130837511);
        break;
        label223: this.m_endIndicator.setImageResource(2130837511);
        break label83;
        label236: this.m_videoIndicator.setImageResource(2130837513);
        continue;
        label249: Log.v("Tango.Video", "updateButtons, video off");
        if (this.m_orientation == 0)
          this.m_videoIndicator.setImageResource(2130837510);
        else
          this.m_videoIndicator.setImageResource(2130837511);
      }
    }
  }

  private void updateOnScreenButtons()
  {
    Log.v("Tango.Video", "updateOnScreenButtons");
    int i;
    if (this.m_session.m_callState == CallSession.CallState.CALL_STATE_ACTIVE)
    {
      i = 0;
      if (this.m_switchCameraButton != null)
      {
        if ((this.m_camera_count <= 1) || ((this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.SEND) && (this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.BOTH)))
          break label131;
        this.m_switchCameraButton.setOnClickListener(this);
        this.m_switchCameraButton.setVisibility(i);
      }
      label81: if (this.m_session.m_callState != CallSession.CallState.CALL_STATE_ACTIVE)
        break label143;
      this.m_videoButton.setEnabled(true);
    }
    while (true)
    {
      if (this.m_wandButton != null)
      {
        this.m_wandButton.setVisibility(i);
        this.m_wandButton.setOnClickListener(this);
      }
      return;
      i = 4;
      break;
      label131: this.m_switchCameraButton.setVisibility(8);
      break label81;
      label143: this.m_videoButton.setEnabled(false);
    }
  }

  private void updateVgoodSelectorData()
  {
    VGoodButtonsBarView localVGoodButtonsBarView;
    List localList;
    if ((this.m_vgoodBtnLayout != null) && (this.m_session != null))
    {
      localVGoodButtonsBarView = this.m_vgoodBtnLayout;
      localList = this.m_session.getVGoodData();
      if ((this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.BOTH) && (this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.SEND))
        break label76;
    }
    label76: for (boolean bool = true; ; bool = false)
    {
      localVGoodButtonsBarView.setData(localList, bool);
      this.m_vgoodBtnLayout.setEmptySlots(this.m_session.getEmptySlotCount());
      return;
    }
  }

  protected int CameraToType(SessionMessages.CameraPosition paramCameraPosition)
  {
    if (paramCameraPosition == SessionMessages.CameraPosition.CAM_BACK)
      return 1;
    if (paramCameraPosition == SessionMessages.CameraPosition.CAM_FRONT)
      return 2;
    return 0;
  }

  public void cancelControlBarTimer()
  {
    this.uihandler.removeMessages(1);
  }

  protected void cleanUpCafe()
  {
    if (this.m_cafeInitialised)
    {
      CafeMgr.StopAllSurprises();
      CafeMgr.Pause();
      CafeMgr.FreeEngine();
      this.m_cafeInitialised = false;
      this.mLocalAvatarRenderer.onCafeFreed();
      this.mRemoteAvatarRenderer.onCafeFreed();
    }
  }

  public void dismissPendingIgnoredCallAlert()
  {
    if (this.m_ignoredCallAlert != null)
    {
      Log.d("Tango.Video", "Dismiss the existing Ignored-Call alert...");
      clearIgnoredCall();
      this.m_ignoredCallAlert.dismiss();
      this.m_ignoredCallAlert = null;
    }
  }

  public void handleNewMessage(com.sgiggle.messaging.Message paramMessage)
  {
    Log.d("Tango.Video", "handleNewMessage(): Message = " + paramMessage);
    if (paramMessage == null)
    {
      Log.d("Tango.Video", "handleNewMessage(): message = null. Do nothing.");
      return;
    }
    switch (paramMessage.getType())
    {
    default:
    case 35015:
    case 35021:
    case 35071:
    case 35073:
    case 35023:
    case 35187:
    case 35025:
    case 35069:
    case 35077:
    case 35079:
    case 35080:
    case 35089:
    case 35085:
    case 35019:
    case 35279:
    case 35033:
    case 35242:
    case 35243:
    case 35244:
    case 35245:
    case 35194:
    case 35225:
    case 35250:
    case 35226:
    case 35251:
    }
    while (true)
    {
      updateButtons();
      return;
      handleSendCallInvitationEvent();
      continue;
      handleSendCallAcceptedEvent();
      continue;
      Log.d("Tango.Video", "handleNewMessage(): Audio-In-Initialization: Reset call-timer.");
      handleAudioInInitializationEvent();
      continue;
      handleMissedCallEvent();
      continue;
      handleAudioInProgressEvent();
      if (this.m_isViewInitialized)
      {
        Log.v("Tango.Video", "ignore updateButton for AUDIO_IN_PROGRESS_EVENT");
        return;
        toggleWandBtn();
        continue;
        MediaEngineMessage.AudioVideoInProgressEvent localAudioVideoInProgressEvent = (MediaEngineMessage.AudioVideoInProgressEvent)paramMessage;
        updateVgoodSelectorData();
        if (((SessionMessages.MediaSessionPayload)localAudioVideoInProgressEvent.payload()).getShowWand())
        {
          this.m_handler.sendEmptyMessageDelayed(8, 1000L);
          s_showWand = true;
        }
        handleVideoEvent();
        continue;
        updateVgoodSelectorData();
        handleVideoEvent();
        continue;
        handleBandwidthEvent();
        continue;
        handleInCallAlertEvent();
        continue;
        handleVideoModeChangedEvent();
        continue;
        handleCallDisconnectingEvent();
        continue;
        handleCallErrorEvent((MediaEngineMessage.CallErrorEvent)paramMessage);
        continue;
        if ((!this.m_isDestroyed) && (!isFinishing()))
        {
          switchToPortrait();
          showDialog(1);
          dismissKeyguard(false);
          continue;
          handleAnimationEvent(paramMessage);
          continue;
          handleAvatarEvent((SessionMessages.AvatarControlPayload)((MediaEngineMessage.AudioAvatarInProgressEvent)paramMessage).payload(), false);
          continue;
          handleAvatarEvent((SessionMessages.AvatarControlPayload)((MediaEngineMessage.VideoAvatarInProgressEvent)paramMessage).payload(), true);
          continue;
          handleAvatarEvent((SessionMessages.AvatarControlPayload)((MediaEngineMessage.Audio2WayAvatarInProgressEvent)paramMessage).payload(), false);
          continue;
          handleAvatarRenderRequest(paramMessage);
          continue;
          Log.w("Tango.Video", "VGOOD_ANIMATION_COMPLETE_TYPE " + paramMessage);
          if (this.animRestart)
          {
            Log.w("Tango.Video", "skip this animation complete event becuase of restart");
            this.animRestart = false;
            return;
          }
          this.m_vgoodBtnLayout.updateButtons(0);
          restartControlBarTimer();
          if (!this.hasVGoodAnimation)
          {
            Log.w("Tango.Video", "skip this animation complete event. Animation interrupted and stopped already");
            return;
          }
          stopAndRemoveVGoodView();
          continue;
          MediaEngineMessage.VGoodErrorEvent localVGoodErrorEvent = (MediaEngineMessage.VGoodErrorEvent)paramMessage;
          Log.i("Tango.Video", "VGOOD_ERROR_EVENT " + ((SessionMessages.VGoodErrorPayload)localVGoodErrorEvent.payload()).getError());
          if (((SessionMessages.VGoodErrorPayload)localVGoodErrorEvent.payload()).getError() == SessionMessages.VGoodErrorPayload.Error.UNSUPPORDED_BY_REMOTE);
          for (String str3 = getString(2131296529); ; str3 = getString(2131296546))
          {
            setupWarningBubble(null, str3, false, false, true);
            break;
          }
          MediaEngineMessage.AvatarErrorEvent localAvatarErrorEvent = (MediaEngineMessage.AvatarErrorEvent)paramMessage;
          Log.i("Tango.Video", "AVATAR_ERROR_TYPE " + ((SessionMessages.AvatarErrorPayload)localAvatarErrorEvent.payload()).getError());
          String str2;
          if (((SessionMessages.AvatarErrorPayload)localAvatarErrorEvent.payload()).getError() == SessionMessages.AvatarErrorPayload.Error.UNSUPPORDED_BY_REMOTE)
            str2 = getString(2131296529);
          while (true)
          {
            setupWarningBubble(null, str2, false, false, true);
            break;
            if (((SessionMessages.AvatarErrorPayload)localAvatarErrorEvent.payload()).getError() == SessionMessages.AvatarErrorPayload.Error.UNCACHED_BY_LOCAL)
              str2 = getString(2131296532);
            else
              str2 = getString(2131296522);
          }
          MediaEngineMessage.GameErrorEvent localGameErrorEvent = (MediaEngineMessage.GameErrorEvent)paramMessage;
          Log.i("Tango.Video", "GAME_ERROR_EVENT " + ((SessionMessages.GameErrorPayload)localGameErrorEvent.payload()).getError());
          String str1;
          if (((SessionMessages.GameErrorPayload)localGameErrorEvent.payload()).getError() == SessionMessages.GameErrorPayload.Error.UNSUPPORDED_BY_REMOTE)
            str1 = getString(2131296518);
          while (true)
          {
            setupWarningBubble(null, str1, false, false, true);
            break;
            if (((SessionMessages.GameErrorPayload)localGameErrorEvent.payload()).getError() == SessionMessages.GameErrorPayload.Error.REMOTE_FAILED)
            {
              str1 = getString(2131296517);
            }
            else if (((SessionMessages.GameErrorPayload)localGameErrorEvent.payload()).getError() == SessionMessages.GameErrorPayload.Error.LOCAL_FAILED)
            {
              str1 = getString(2131296520);
            }
            else if (((SessionMessages.GameErrorPayload)localGameErrorEvent.payload()).getError() == SessionMessages.GameErrorPayload.Error.REMOTE_TIMEOUT)
            {
              str1 = getString(2131296519);
            }
            else
            {
              Log.w("Tango.Video", "Unexpected GAME_ERROR_EVENT error type" + ((SessionMessages.GameErrorPayload)localGameErrorEvent.payload()).getError());
              str1 = getString(2131296521);
            }
          }
          switchAvatar((SessionMessages.AvatarControlPayload)((MediaEngineMessage.SwitchAvatarEvent)paramMessage).payload());
        }
      }
    }
  }

  public boolean hasControlBarAnimation()
  {
    return this.hasControlBarAnimation;
  }

  protected void initViews()
  {
    initViews(false);
  }

  protected void initViews(boolean paramBoolean)
  {
    Log.v("Tango.Video", "initViews");
    if ((paramBoolean) || (!this.m_isViewInitialized) || (this.m_resumed) || ((this.m_resumeFromAvatar) && (this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.NONE)))
    {
      this.m_resumed = false;
      this.m_resumeFromAvatar = false;
    }
    for (boolean bool = this.m_video_ui.initVideoViews(); ; bool = this.m_video_ui.changeViews())
    {
      if (bool)
      {
        initButtons();
        initOnScreenButtons();
      }
      updateButtons();
      updateOnScreenButtons();
      updateUserName();
      return;
    }
  }

  public void onAudioModeUpdated()
  {
    updateButtons();
  }

  public void onBackPressed()
  {
    Log.d("Tango.Video", "onBackPressed() Ignore the BACK key.");
    if (this.m_showVgoodBar)
      toggleWandBtn();
  }

  public void onCancel(DialogInterface paramDialogInterface)
  {
    if (paramDialogInterface == this.m_ignoredCallAlert)
    {
      Log.d("Tango.Video", "onCancel(DialogInterface) Cancel the Ignored-Call dialog...");
      clearIgnoredCall();
      this.m_ignoredCallAlert = null;
    }
  }

  public void onClick(View paramView)
  {
    boolean bool;
    if (paramView == this.m_muteButton)
    {
      Log.v("Tango.Video", "onClick(): Toggle mute button...");
      MessageRouter localMessageRouter = MessageRouter.getInstance();
      if (!this.m_session.m_muted)
      {
        bool = true;
        localMessageRouter.postMessage("jingle", MediaEngineMessage.AudioControlMessage.createMessage(2, bool));
      }
    }
    while (true)
    {
      updateButtons();
      return;
      bool = false;
      break;
      if (paramView == this.m_videoButton)
      {
        Log.v("Tango.Video", "onClick(): On video-button: direction=" + this.m_session.m_videoDirection);
        if ((this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.BOTH) || (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND))
          Log.v("Tango.Video", "onClick(): Remove video...");
        for (Object localObject = new MediaEngineMessage.RemoveVideoMessage(this.m_session.getPeerAccountId()); ; localObject = new MediaEngineMessage.AddVideoMessage(this.m_session.getPeerAccountId()))
        {
          MessageRouter.getInstance().postMessage("jingle", (com.sgiggle.messaging.Message)localObject);
          dismissBubbleView(false, true);
          break;
          Log.v("Tango.Video", "onClick(): Send video...");
        }
      }
      if (paramView == this.m_endButton)
      {
        hangUpCall();
      }
      else if (paramView == this.m_switchCameraButton)
      {
        Log.v("Tango.Video", "onClick(): Switch camera...");
        if (!throttleSwitchCamera())
          switchCamera();
        else
          Log.w("Tango.Video", "Throttling switch camera action...");
      }
      else if (paramView == this.m_wandButton)
      {
        toggleWandBtn();
      }
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.Video", "onCreate() savedInstanceState=" + paramBundle);
    getWindow().addFlags(2098304);
    dismissKeyguard(true);
    super.onCreate(paramBundle);
    this.m_btBtnReceiver.setButtonHandler(new BluetoothButtonReceiver.ButtonHandler()
    {
      public void acceptCall()
      {
      }

      public void rejectCall()
      {
        VideoTwoWayActivity.this.hangUpCall();
      }
    });
    this.m_session = CallHandler.getDefault().getCallSession();
    if (this.m_session == null)
    {
      Log.i("Tango.Video", "onCreate(): Call session is null. End this screen.");
      dismissKeyguard(false);
      finish();
      TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_RESUMING);
      return;
    }
    TangoApp.getInstance().setVideoActivityInstance(this);
    this.m_wifiLockReceiver = new WifiLockReceiver(this);
    setVolumeControlStream(0);
    this.m_prefs = getSharedPreferences("Tango.Video", 0);
    this.mRemoteAvatarRenderer = new AvatarRenderer(0, false, this.m_session, GLRenderer.isSupported());
    this.mLocalAvatarRenderer = new AvatarRenderer(1, true, this.m_session, GLRenderer.isSupported());
    handleNewMessage(getFirstMessage());
    this.mAnimationSlideInRight = AnimationUtils.loadAnimation(this, 2130968586);
    this.mAnimationSlideOutRight = AnimationUtils.loadAnimation(this, 2130968591);
    this.mAnimationSlideInTop = AnimationUtils.loadAnimation(this, 2130968587);
    this.mAnimationSlideOutTop = AnimationUtils.loadAnimation(this, 2130968592);
    this.mAnimationSlideInRight.setAnimationListener(this.animationListener);
    this.mAnimationSlideOutTop.setAnimationListener(this.animationListener);
    this.mAnimationSlideInTop.setAnimationListener(this.animationListener);
    this.mAnimationSlideOutRight.setAnimationListener(this.animationListener);
    BillingServiceManager.getInstance().checkBillingSupport(this);
    TangoApp.getInstance().enableKeyguard();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 0:
      return createCallOnHoldDialog();
    case 1:
    }
    return createLeaveMessageDialog();
  }

  protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
  {
    return onCreateDialog(paramInt);
  }

  protected void onDestroy()
  {
    Log.d("Tango.Video", "onDestroy()");
    super.onDestroy();
    cleanUpCafe();
    VideoCaptureRaw.setActivityHandler(null);
    dismissPendingIgnoredCallAlert();
    TangoApp.getInstance().clearVideoActivityInstance(this);
    this.m_isDestroyed = true;
    dismissKeyguard(false);
  }

  public void onGameInCallStart()
  {
    this.m_canCleanCafeOnPause = true;
  }

  protected void onPause()
  {
    Log.v("Tango.Video", "onPause()");
    super.onPause();
    unregisterReceiver(this.m_btBtnReceiver);
    animationOnPause();
    avatarOnPause();
    if ((this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.NONE) || (this.m_canCleanCafeOnPause == true))
      cleanUpCafe();
    if (this.m_debugInfoTimer != null)
    {
      Log.d("Tango.Video", "onPause(): Cancel debug-info timer....");
      this.m_debugInfoTimer.cancel();
      this.m_debugInfoTimer = null;
    }
  }

  protected void onRestart()
  {
    Log.d("Tango.Video", "onRestart()");
    super.onRestart();
    MessageRouter.getInstance().postMessage("jingle", MediaEngineMessage.AudioControlMessage.createMessage(1, this.m_session.m_speakerOn));
  }

  protected void onResume()
  {
    Log.d("Tango.Video", "onResume()");
    int i;
    boolean bool;
    if (TangoApp.getInstance().getAppRunningState() == TangoApp.AppState.APP_STATE_BACKGROUND)
    {
      i = 1;
      super.onResume();
      animationOnResume();
      if (GLRenderer.isSupported())
        break label161;
      bool = true;
      label39: this.m_canCleanCafeOnPause = bool;
      if (!this.m_cafeInitialised)
      {
        CafeMgr.InitEngine(this);
        CafeMgr.SetCallbacks();
        CafeMgr.Resume();
        this.m_cafeInitialised = true;
        this.mLocalAvatarRenderer.onCafeInitialized();
        this.mRemoteAvatarRenderer.onCafeInitialized();
      }
      IntentFilter localIntentFilter = new IntentFilter("android.intent.action.VoIP_BLUETOOTH");
      registerReceiver(this.m_btBtnReceiver, localIntentFilter);
      this.m_resumed = true;
      if (this.m_session.m_avatarDirection != SessionMessages.AvatarControlPayload.Direction.NONE)
        break label166;
      handleVideoEvent();
      updateVGoodBar();
    }
    while (true)
    {
      if (this.m_session.m_callOnHold)
        showDialog(0);
      if (this.m_isInPortrait)
        switchToPortrait();
      return;
      i = 0;
      break;
      label161: bool = false;
      break label39;
      label166: if (i == 0)
      {
        avatarOnResume();
        updateVGoodBar();
      }
    }
  }

  protected void onStart()
  {
    Log.d("Tango.Video", "onStart()");
    dismissKeyguard(true);
    IntentFilter localIntentFilter = new IntentFilter();
    localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
    localIntentFilter.addAction("android.intent.action.SCREEN_ON");
    registerReceiver(this.m_wifiLockReceiver, localIntentFilter);
    super.onStart();
  }

  protected void onStop()
  {
    Log.v("Tango.Video", "onStop()");
    super.onStop();
    dismissKeyguard(false);
    this.m_wifiLockReceiver.releaseWifiLock(this);
    try
    {
      unregisterReceiver(this.m_wifiLockReceiver);
      return;
    }
    catch (Exception localException)
    {
      Log.e("Tango.Video", "got exception for unregisterReceiver()");
      localException.printStackTrace();
    }
  }

  public void restartControlBarTimer()
  {
    cancelControlBarTimer();
    startControlBarTimer();
  }

  public void setFilter(int paramInt1, int paramInt2)
  {
  }

  protected void setFilterHightlightWithBackground()
  {
  }

  public void setupWarningBubble(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    StringBuilder localStringBuilder = new StringBuilder().append("setupBubble() m_orientation == Orientation.LANDSCAPE? ");
    boolean bool;
    label74: View localView;
    label132: TextView localTextView1;
    label195: TextView localTextView2;
    if (this.m_orientation == 0)
    {
      bool = true;
      Log.v("Tango.Video", bool);
      if (!paramBoolean1)
        break label311;
      this.m_bubbleLayout = ((RelativeLayout)findViewById(2131362132));
      this.m_bubbleLayout.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          VideoTwoWayActivity.this.dismissBubbleView(true, false);
        }
      });
      localView = this.m_bubbleLayout.findViewById(2131361826);
      this.m_bubbleLayout.setVisibility(0);
      if (!paramBoolean1)
      {
        if (!paramBoolean2)
          break label343;
        this.m_bubbleLayout.findViewById(2131361846).setVisibility(8);
        this.m_bubbleLayout.findViewById(2131362201).setVisibility(0);
      }
      localTextView1 = (TextView)this.m_bubbleLayout.findViewById(2131361827);
      if (paramString1 == null)
        break label375;
      localTextView1.setText(paramString1);
      localTextView1.setVisibility(0);
      localView.setVisibility(0);
      Log.v("Tango.Video", "setupBubble() bubbleTextTitle.setVisibility(View.VISIBLE) " + paramString1);
      localTextView2 = (TextView)this.m_bubbleLayout.findViewById(2131361828);
      if (paramString2 == null)
        break label392;
      localTextView2.setText(paramString2);
      localTextView2.setVisibility(0);
      Log.v("Tango.Video", "setupBubble() bubbleTextBody.setVisibility(View.VISIBLE) " + paramString2);
    }
    while (true)
    {
      this.m_bubbleLayout.bringToFront();
      if ((TangoApp.g_screenLoggerEnabled) && (this.m_debugInfoView != null))
        this.m_debugInfoView.bringToFront();
      if (paramBoolean3)
      {
        this.uihandler.removeMessages(0);
        this.uihandler.sendEmptyMessageDelayed(0, 5000L);
      }
      return;
      bool = false;
      break;
      label311: this.m_bubbleLayout = ((RelativeLayout)findViewById(2131362200));
      this.m_bubbleLayout.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          VideoTwoWayActivity.this.dismissBubbleView(false, true);
        }
      });
      break label74;
      label343: this.m_bubbleLayout.findViewById(2131361846).setVisibility(0);
      this.m_bubbleLayout.findViewById(2131362201).setVisibility(8);
      break label132;
      label375: localTextView1.setVisibility(8);
      localView.setVisibility(8);
      break label195;
      label392: localTextView2.setVisibility(8);
    }
  }

  public void startControlBarTimer()
  {
    this.uihandler.sendEmptyMessageDelayed(1, 15000L);
  }

  protected void updateUserName()
  {
    if (this.m_userNameTxt != null)
    {
      if (this.m_session.m_avatarDirection != SessionMessages.AvatarControlPayload.Direction.BOTH)
        break label78;
      this.m_userNameTxt.setVisibility(0);
      if (this.mRemoteAvatarRenderer.isInPIPMode())
        break label53;
      this.m_userNameTxt.setText(this.m_session.m_peerName);
    }
    label53: 
    while (this.mLocalAvatarRenderer.isInPIPMode())
      return;
    this.m_userNameTxt.setText(this.m_session.m_localName);
    return;
    label78: this.m_userNameTxt.setVisibility(8);
  }

  protected void updateVGoodBar()
  {
    ArrayList localArrayList;
    if ((this.mLocalAvatarRenderer.isAvatarActived()) && (this.mLocalAvatarRenderer.getAvatarInfo() != null))
    {
      this.m_vgoodBtnLayout.showCurrentPlayingAnimation(this.mLocalAvatarRenderer.getAvatarInfo().getAvatarid());
      if (this.m_session.m_avatarDirection != SessionMessages.AvatarControlPayload.Direction.NONE)
      {
        localArrayList = new ArrayList();
        localArrayList.add(SessionMessages.VGoodCinematic.Type.AVATAR);
        if ((this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.BOTH) && (this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.SEND))
          break label156;
      }
    }
    label156: for (boolean bool = true; ; bool = false)
    {
      if (bool)
        localArrayList.add(SessionMessages.VGoodCinematic.Type.FILTER);
      this.m_vgoodBtnLayout.enableButtonByTypes(localArrayList, bool);
      if (this.m_showVgoodBar)
      {
        this.m_vgoodBtnLayout.bringToFront();
        this.m_vgoodBtnLayout.setVisibility(0);
        restartControlBarTimer();
      }
      return;
      this.m_vgoodBtnLayout.hideCurrentPlayingAnimation();
      break;
    }
  }

  private class DebugInfoTimerTask extends TimerTask
  {
    private StringBuilder m_debugInfoText = new StringBuilder();
    private ScreenLogger m_screenLogger = new ScreenLogger();

    private DebugInfoTimerTask()
    {
    }

    public void run()
    {
      this.m_debugInfoText.setLength(0);
      Iterator localIterator = this.m_screenLogger.getAllParameters().entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        if (this.m_debugInfoText.length() > 0)
          this.m_debugInfoText.append("\n");
        this.m_debugInfoText.append((String)localEntry.getKey()).append(": ").append((String)localEntry.getValue());
      }
      if (VideoTwoWayActivity.this.m_debugInfoView != null)
        VideoTwoWayActivity.this.m_debugInfoView.post(new Runnable()
        {
          public void run()
          {
            VideoTwoWayActivity.this.m_debugInfoView.setText(VideoTwoWayActivity.DebugInfoTimerTask.this.m_debugInfoText.toString(), TextView.BufferType.NORMAL);
          }
        });
    }
  }

  public static abstract interface VideoUI
  {
    public abstract void bringToFront();

    public abstract boolean changeViews();

    public abstract void hideCafe();

    public abstract boolean initVideoViews();

    public abstract void onAvatarChanged();

    public abstract void onAvatarPaused();

    public abstract void onAvatarResumed();

    public abstract void onCleanUpAvatar();

    public abstract void onVideoModeChanged();

    public abstract boolean pipSwapSupported();

    public abstract void showCafe();

    public abstract void updateLayout();

    public abstract void updateRendererSize(int paramInt1, int paramInt2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.VideoTwoWayActivity
 * JD-Core Version:    0.6.2
 */