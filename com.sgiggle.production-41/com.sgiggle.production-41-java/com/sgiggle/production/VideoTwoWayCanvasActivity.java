package com.sgiggle.production;

import android.graphics.Bitmap.Config;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.sgiggle.VideoCapture.VideoCaptureRaw;
import com.sgiggle.VideoCapture.VideoView;
import com.sgiggle.VideoRenderer.VideoRenderer;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.cafe.vgood.CafeRenderer;
import com.sgiggle.cafe.vgood.CafeViewForCanvasRenderer;
import com.sgiggle.cafe.vgood.VGoodButtonsBarView;
import com.sgiggle.production.avatar.AvatarRenderer;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload.Direction;

public class VideoTwoWayCanvasActivity extends VideoTwoWayActivity
  implements VideoTwoWayActivity.VideoUI, SurfaceHolder.Callback
{
  private final int BUTTON_BACKGROUND = 6;
  private final int CAMERA_BACK_BIG = 0;
  private final int CAMERA_BACK_SMALL = 2;
  private final int CAMERA_FRONT_BIG = 1;
  private final int CAMERA_FRONT_SMALL = 3;
  private final int RENDERER_BIG = 4;
  private final int RENDERER_SMALL = 5;
  private CafeViewForCanvasRenderer mAvatarSurfaceView = null;
  private ImageView[] m_backgrounds = new ImageView[7];
  private RelativeLayout m_border;
  private RelativeLayout m_borderLayout;
  private Settings m_current = new Settings();
  private SurfaceHolder[] m_holders = new SurfaceHolder[6];
  private boolean m_htcNoSwap;
  private Settings m_next = new Settings();
  private Rect m_rendererRect;
  private Rect m_transparentRect;
  private int[] m_viewIndex = null;
  private SurfaceView[] m_views = new SurfaceView[6];

  private void addAvatarViewAndBringToFront()
  {
    if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.NONE)
      return;
    Log.v("Tango.Video", "addAvatarViewAndBringToFront");
    if (this.mAvatarSurfaceView == null)
    {
      Log.w("Tango.Video", "add surface view called before initialization");
      return;
    }
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.m_controlsLayout.measure(localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels);
    int m;
    int j;
    if (this.m_orientation == 0)
    {
      m = localDisplayMetrics.heightPixels;
      j = localDisplayMetrics.widthPixels - this.m_controlsLayout.getMeasuredWidth();
    }
    int i;
    for (int k = m; ; k = i)
    {
      Log.i("Tango.Video", "avatar surface view x" + 0 + " y " + 0 + " w " + j + " h " + k);
      FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(j, k);
      localLayoutParams.leftMargin = 0;
      localLayoutParams.topMargin = 0;
      this.mAvatarSurfaceView.setLayoutParams(localLayoutParams);
      this.m_frameLayout.addView(this.mAvatarSurfaceView);
      this.mAvatarSurfaceView.setZOrderMediaOverlay(true);
      this.m_switchCameraLayout.bringToFront();
      this.m_controlsLayout.bringToFront();
      if (this.m_showVgoodBar)
        this.m_vgoodBtnLayout.bringToFront();
      if ((!TangoApp.g_screenLoggerEnabled) || (this.m_debugInfoView == null))
        break;
      this.m_debugInfoView.bringToFront();
      return;
      i = localDisplayMetrics.heightPixels - this.m_controlsLayout.getMeasuredHeight();
      j = localDisplayMetrics.widthPixels;
    }
  }

  private void bringPIPVideoToFront()
  {
    Log.v("Tango.Video", "bringPIPToFront " + this.m_session.m_videoDirection);
    if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND)
    {
      this.m_views[this.m_current.m_camera_view_index].bringToFront();
      this.m_views[this.m_current.m_camera_view_index].setZOrderMediaOverlay(true);
    }
    while (true)
    {
      if (this.m_borderLayout != null)
        this.m_borderLayout.bringToFront();
      return;
      if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.RECEIVE)
      {
        this.m_views[this.m_current.m_renderer_view_index].bringToFront();
        this.m_views[this.m_current.m_renderer_view_index].setZOrderMediaOverlay(true);
      }
    }
  }

  private void createAvatarGLView()
  {
    CafeMgr.Reset();
    this.mAvatarSurfaceView = new CafeViewForCanvasRenderer(this, this.m_orientation);
    this.mAvatarSurfaceView.setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
      {
        Log.d("Tango.Video", "mAvatarSurfaceView on touch " + paramAnonymousMotionEvent.getAction());
        if (paramAnonymousMotionEvent.getAction() == 1)
        {
          int i = (int)paramAnonymousMotionEvent.getX();
          int j = (int)paramAnonymousMotionEvent.getY();
          Log.d("Tango.Video", "touch on " + i + " " + j);
          if (VideoView.getRect(VideoTwoWayCanvasActivity.this.m_orientation, 6).contains(i, j))
          {
            if ((VideoTwoWayCanvasActivity.this.m_session.pipSwappable()) && (VideoTwoWayCanvasActivity.this.m_video_ui.pipSwapSupported()))
              VideoTwoWayCanvasActivity.this.swapAvatar();
            return true;
          }
        }
        return true;
      }
    });
    this.mAvatarSurfaceView.onResume();
  }

  private int getCameraViewIndex(int paramInt1, int paramInt2)
  {
    int i = -1;
    if (paramInt2 == 1)
      if (paramInt1 == 0)
        i = 0;
    do
    {
      do
      {
        do
          return i;
        while (paramInt1 != 1);
        return 2;
      }
      while (paramInt2 != 2);
      if (paramInt1 == 0)
        return 1;
    }
    while (paramInt1 != 1);
    return 3;
  }

  private int getRectangleIndex(int paramInt)
  {
    int i = -1;
    if (paramInt == 0)
      i = 3;
    do
    {
      return i;
      if (paramInt == 1)
        return 4;
      if (paramInt == 2)
        return 7;
      if (paramInt == 3)
        return 8;
      if (paramInt == 4)
        return 5;
    }
    while (paramInt != 5);
    return 9;
  }

  private int getRendererViewIndex(int paramInt)
  {
    int i = -1;
    if (paramInt == 0)
      i = 4;
    while (paramInt != 1)
      return i;
    return 5;
  }

  private int getVideoViewIndex(int paramInt)
  {
    if (paramInt == 0)
      return 3;
    if (paramInt == 1)
      return 4;
    if (paramInt == 2)
      return 7;
    if (paramInt == 3)
      return 8;
    if (paramInt == 4)
      return 5;
    if (paramInt == 5)
      return 9;
    Log.w("Tango.Video", "getVideoViewIndex wrong index " + paramInt);
    return -1;
  }

  private void hideView()
  {
    Log.i("Tango.Video", "hideView");
    int i;
    if (this.m_current.m_camera_size == 0)
      i = this.m_current.m_camera_view_index;
    while (true)
    {
      int j;
      if (this.m_current.m_camera_size == 1)
        j = this.m_current.m_camera_view_index;
      while (true)
      {
        Log.v("Tango.Video", "hideView camera_view_ind=" + this.m_current.m_camera_view_index + " renderer_view_ind=" + this.m_current.m_renderer_view_index + " big=" + i + " small=" + j);
        if (j != -1)
        {
          this.m_views[j].setVisibility(8);
          this.m_border.setVisibility(8);
        }
        if (i != -1)
        {
          this.m_views[i].setVisibility(8);
          this.m_backgrounds[0].setVisibility(8);
          this.m_backgrounds[1].setVisibility(8);
          this.m_backgrounds[2].setVisibility(8);
          this.m_backgrounds[3].setVisibility(8);
          this.m_backgrounds[4].setVisibility(8);
          this.m_backgrounds[5].setVisibility(8);
          this.m_backgrounds[6].setVisibility(8);
        }
        return;
        if (this.m_current.m_renderer_size != 0)
          break label282;
        i = this.m_current.m_renderer_view_index;
        break;
        if (this.m_current.m_renderer_size == 1)
          j = this.m_current.m_renderer_view_index;
        else
          j = -1;
      }
      label282: i = -1;
    }
  }

  private void initBackground()
  {
    int i;
    if (this.m_next.m_camera_size == 0)
      if (this.m_camera_type == 1)
        i = 15;
    while (true)
    {
      for (int j = 0; j < 6; j++)
      {
        Rect localRect = VideoView.getRect(this.m_orientation, i + j);
        this.m_backgrounds[j].setBackgroundColor(-16777216);
        RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)this.m_backgrounds[j].getLayoutParams();
        localLayoutParams.leftMargin = localRect.left;
        localLayoutParams.topMargin = localRect.top;
        localLayoutParams.width = localRect.width();
        localLayoutParams.height = localRect.height();
        this.m_backgrounds[j].setLayoutParams(localLayoutParams);
      }
      i = 21;
      continue;
      if (this.m_next.m_renderer_size != 0)
        break;
      if (this.m_camera_type == 1)
        i = 27;
      else
        i = 33;
    }
  }

  private void initBorder()
  {
    Rect localRect = VideoView.getBorderRect(this.m_orientation, this.m_next.m_borderRectIndex);
    Log.v("Tango.Video", "initBorder: borderRect=" + localRect);
    RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)this.m_border.getLayoutParams();
    localLayoutParams.leftMargin = localRect.left;
    localLayoutParams.topMargin = localRect.top;
    localLayoutParams.width = localRect.width();
    localLayoutParams.height = localRect.height();
    this.m_border.setLayoutParams(localLayoutParams);
  }

  private void initButtonBackground()
  {
  }

  private void initCameraView(int paramInt)
  {
    int i = getVideoViewIndex(paramInt);
    if ((this.m_session.m_avatarDirection != SessionMessages.AvatarControlPayload.Direction.NONE) && ((i == 7) || (i == 8)));
    for (Rect localRect = VideoView.getRect(this.m_orientation, 10); ; localRect = VideoView.getRect(this.m_orientation, i))
    {
      ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)this.m_views[paramInt].getLayoutParams();
      localMarginLayoutParams.leftMargin = localRect.left;
      localMarginLayoutParams.topMargin = localRect.top;
      localMarginLayoutParams.width = localRect.width();
      localMarginLayoutParams.height = localRect.height();
      this.m_views[paramInt].setLayoutParams(localMarginLayoutParams);
      return;
    }
  }

  private void initRendererView(int paramInt)
  {
    int i = getVideoViewIndex(paramInt);
    Rect localRect = VideoView.getRect(this.m_orientation, i);
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)this.m_views[paramInt].getLayoutParams();
    localMarginLayoutParams.leftMargin = localRect.left;
    localMarginLayoutParams.topMargin = localRect.top;
    localMarginLayoutParams.width = localRect.width();
    localMarginLayoutParams.height = localRect.height();
    if (paramInt == 4)
    {
      this.m_transparentRect = VideoView.getRect(this.m_orientation, 10);
      this.m_rendererRect = new Rect(localRect);
    }
    this.m_views[paramInt].setLayoutParams(localMarginLayoutParams);
  }

  private void initViewId()
  {
    Log.i("Tango.Video", "initViewId");
    this.m_views[0] = ((SurfaceView)findViewById(2131362176));
    this.m_views[1] = ((SurfaceView)findViewById(2131362177));
    this.m_views[2] = ((SurfaceView)findViewById(2131362179));
    this.m_views[3] = ((SurfaceView)findViewById(2131362180));
    this.m_views[4] = ((SurfaceView)findViewById(2131362178));
    this.m_views[5] = ((SurfaceView)findViewById(2131362181));
    if (this.m_viewIndex == null)
    {
      this.m_viewIndex = new int[6];
      ViewGroup localViewGroup = (ViewGroup)this.m_views[0].getParent();
      this.m_viewIndex[0] = localViewGroup.indexOfChild(this.m_views[0]);
      this.m_viewIndex[1] = localViewGroup.indexOfChild(this.m_views[1]);
      this.m_viewIndex[2] = localViewGroup.indexOfChild(this.m_views[2]);
      this.m_viewIndex[3] = localViewGroup.indexOfChild(this.m_views[3]);
      this.m_viewIndex[4] = localViewGroup.indexOfChild(this.m_views[4]);
      this.m_viewIndex[5] = localViewGroup.indexOfChild(this.m_views[5]);
    }
    this.m_backgrounds[0] = ((ImageView)findViewById(2131362185));
    this.m_backgrounds[1] = ((ImageView)findViewById(2131362186));
    this.m_backgrounds[2] = ((ImageView)findViewById(2131362187));
    this.m_backgrounds[3] = ((ImageView)findViewById(2131362188));
    this.m_backgrounds[4] = ((ImageView)findViewById(2131362189));
    this.m_backgrounds[5] = ((ImageView)findViewById(2131362190));
    this.m_backgrounds[6] = ((ImageView)findViewById(2131362191));
    this.m_borderLayout = ((RelativeLayout)findViewById(2131362182));
    this.m_border = ((RelativeLayout)findViewById(2131362183));
    this.m_holders[0] = this.m_views[0].getHolder();
    this.m_holders[0].addCallback(this);
    this.m_holders[0].setType(3);
    this.m_holders[1] = this.m_views[1].getHolder();
    this.m_holders[1].addCallback(this);
    this.m_holders[1].setType(3);
    this.m_holders[2] = this.m_views[2].getHolder();
    this.m_holders[2].addCallback(this);
    this.m_holders[2].setType(3);
    this.m_holders[3] = this.m_views[3].getHolder();
    this.m_holders[3].addCallback(this);
    this.m_holders[3].setType(3);
    this.m_holders[4] = this.m_views[4].getHolder();
    this.m_holders[4].addCallback(this);
    if (this.m_next.m_isH264Renderer)
    {
      this.m_holders[4].setType(3);
      this.m_holders[5] = this.m_views[5].getHolder();
      this.m_holders[5].addCallback(this);
      if (!this.m_next.m_isH264Renderer)
        break label670;
      this.m_holders[5].setType(3);
    }
    while (true)
    {
      this.m_views[5].setZOrderMediaOverlay(true);
      this.m_borderLayout.bringToFront();
      return;
      this.m_holders[4].setType(0);
      this.m_holders[4].setFormat(-2);
      break;
      label670: this.m_holders[5].setType(0);
    }
  }

  private void initViewSize()
  {
    Settings.access$002(this.m_next, VideoRenderer.isH264Renderer());
    VideoView.setIsH264Renderer(this.m_next.m_isH264Renderer);
    Settings.access$302(this.m_next, -1);
    Settings.access$402(this.m_next, -1);
    Log.v("Tango.Video", "initViewSize vide " + this.m_session.m_videoDirection + " avatar direction " + this.m_session.m_avatarDirection);
    if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND)
      if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.NONE)
        Settings.access$302(this.m_next, 0);
    while (true)
    {
      if (this.m_session.isPipSwapped())
        swapViewSize();
      return;
      Settings.access$302(this.m_next, 1);
      continue;
      if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.RECEIVE)
      {
        Settings.access$402(this.m_next, 0);
      }
      else if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.BOTH)
      {
        Settings.access$302(this.m_next, 1);
        Settings.access$402(this.m_next, 0);
      }
    }
  }

  private void pauseAndRemoveAvatarView()
  {
    Log.v("Tango.Video", "pauseAndRemoveAvatarView()");
    if (this.mAvatarSurfaceView != null)
    {
      this.mAvatarSurfaceView.setZOrderMediaOverlay(false);
      this.mAvatarSurfaceView.onPause();
      this.mAvatarSurfaceView.setVisibility(8);
      this.m_frameLayout.removeView(this.mAvatarSurfaceView);
      this.mAvatarSurfaceView = null;
      this.mRemoteAvatarRenderer.onSurfaceDestroyed();
      this.mLocalAvatarRenderer.onSurfaceDestroyed();
    }
  }

  private void reinitVideoAvatarViews()
  {
    Log.v("Tango.Video", "reinitVideoAvatarViews");
    pauseAndRemoveAvatarView();
    super.initViews(true);
    createAvatarGLView();
    addAvatarViewAndBringToFront();
    resetAvatarRenderersList();
    if ((!this.mLocalAvatarRenderer.isInPIPMode()) && (!this.mRemoteAvatarRenderer.isInPIPMode()))
    {
      Log.d("Tango.Video", "bring pip video to front");
      bringPIPVideoToFront();
    }
  }

  private void resetAvatarRenderersList()
  {
    this.mAvatarSurfaceView.getRenderer().clearCafeViewRenderers();
    if (this.mLocalAvatarRenderer.needDrawPIPVideoBg())
    {
      this.mAvatarSurfaceView.getRenderer().addCafeViewRenderer(this.mRemoteAvatarRenderer);
      this.mRemoteAvatarRenderer.setUpdate(true);
      this.mRemoteAvatarRenderer.setClear(true);
      this.mAvatarSurfaceView.getRenderer().addCafeViewRenderer(this.mLocalAvatarRenderer);
      this.mLocalAvatarRenderer.setUpdate(false);
      this.mLocalAvatarRenderer.setClear(true);
      return;
    }
    if ((this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.RECEIVER) || ((this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.BOTH) && (this.mRemoteAvatarRenderer.isInPIPMode())))
    {
      this.mAvatarSurfaceView.getRenderer().addCafeViewRenderer(this.mLocalAvatarRenderer);
      this.mLocalAvatarRenderer.setUpdate(true);
      this.mLocalAvatarRenderer.setClear(true);
      this.mAvatarSurfaceView.getRenderer().addCafeViewRenderer(this.mRemoteAvatarRenderer);
      this.mRemoteAvatarRenderer.setUpdate(false);
      this.mRemoteAvatarRenderer.setClear(true);
      return;
    }
    if ((this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.SENDER) || ((this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.BOTH) && (this.mLocalAvatarRenderer.isInPIPMode())))
    {
      this.mAvatarSurfaceView.getRenderer().addCafeViewRenderer(this.mRemoteAvatarRenderer);
      this.mRemoteAvatarRenderer.setUpdate(true);
      this.mRemoteAvatarRenderer.setClear(true);
      this.mAvatarSurfaceView.getRenderer().addCafeViewRenderer(this.mLocalAvatarRenderer);
      this.mLocalAvatarRenderer.setUpdate(false);
      this.mLocalAvatarRenderer.setClear(true);
      return;
    }
    Log.e("Tango.Video", "render status are error, avatar direction = " + this.m_session.m_avatarDirection);
  }

  private int screenOrientationToViewOrientation(int paramInt)
  {
    int i = 1;
    if (paramInt == 0)
      i = 0;
    while (paramInt == i)
      return i;
    Log.v("Tango.Video", "screenOrientationToViewOrientation unexpected orientation " + paramInt);
    return -1;
  }

  private void setOrientation()
  {
    int i;
    StringBuilder localStringBuilder;
    if (this.m_camera_type == 1)
    {
      i = VideoView.getOrientation(0);
      Settings.access$502(this.m_next, viewOrientationToScreenOrientation(i));
      localStringBuilder = new StringBuilder().append("setOrientation() m_camera_type is ").append(this.m_camera_type).append(" orientation is ").append(i).append(" m_requestedOrientation is ");
      if (this.m_next.m_requestedOrientation != 0)
        break label127;
    }
    label127: for (String str = "LANDSCAPE"; ; str = "PORTRAIT")
    {
      Log.v("Tango.Video", str);
      Settings.access$602(this.m_next, this.m_camera_type);
      return;
      if (this.m_camera_type == 2)
      {
        i = VideoView.getOrientation(1);
        break;
      }
      i = 1;
      break;
    }
  }

  private boolean setViews(boolean paramBoolean)
  {
    Log.v("Tango.Video", "setViews " + this.m_session.m_videoDirection + " " + this.m_session.m_cameraPosition);
    setOrientation();
    if ((!paramBoolean) && (this.m_next.equals(this.m_current)))
    {
      if ((this.m_next.m_camera_size != 1) && (this.m_next.m_renderer_size != 1))
        this.m_border.setVisibility(8);
      Log.v("Tango.Video", "setViews: no configuration change, returning");
      return false;
    }
    hideView();
    String str;
    int i;
    if ((paramBoolean) || (this.m_current.m_requestedOrientation != this.m_next.m_requestedOrientation) || (this.m_next.m_isH264Renderer))
    {
      setRequestedOrientation(this.m_next.m_requestedOrientation);
      this.m_orientation = screenOrientationToViewOrientation(getRequestedOrientation());
      StringBuilder localStringBuilder = new StringBuilder().append("orientation changed to  ");
      if (this.m_orientation == 0)
      {
        str = "LANDSCAPE";
        Log.v("Tango.Video", str);
        if (this.m_orientation != 0)
          break label266;
        i = 2130903151;
        label223: setContentView(i);
        initViewId();
      }
    }
    for (boolean bool = true; ; bool = false)
    {
      updateLayout();
      showView();
      this.m_current = this.m_next.clone();
      return bool;
      str = "PORTRAIT";
      break;
      label266: i = 2130903152;
      break label223;
    }
  }

  private void showView()
  {
    int i = CameraToType(this.m_session.m_cameraPosition);
    Log.i("Tango.Video", "showView camera_size " + this.m_next.m_camera_size + " renderer_size " + this.m_next.m_renderer_size + " camera_type " + i);
    Settings.access$102(this.m_next, getCameraViewIndex(this.m_next.m_camera_size, i));
    Settings.access$202(this.m_next, getRendererViewIndex(this.m_next.m_renderer_size));
    int j;
    if (this.m_next.m_camera_size == 0)
      j = this.m_next.m_camera_view_index;
    while (true)
    {
      int k;
      if (this.m_next.m_camera_size == 1)
        k = this.m_next.m_camera_view_index;
      while (true)
      {
        label148: Log.v("Tango.Video", "showView camera_view_ind=" + this.m_next.m_camera_view_index + " renderer_view_ind=" + this.m_next.m_renderer_view_index + " big=" + j + " small=" + k);
        if (j != -1)
          this.m_views[j].setVisibility(0);
        int m;
        if (k != -1)
        {
          ViewGroup localViewGroup = (ViewGroup)this.m_views[k].getParent();
          if (localViewGroup.indexOfChild(this.m_views[k]) != this.m_viewIndex[k])
          {
            localViewGroup.removeView(this.m_views[k]);
            localViewGroup.addView(this.m_views[k], this.m_viewIndex[k]);
            this.m_views[k].setZOrderMediaOverlay(false);
          }
          this.m_views[k].setVisibility(0);
          if (!this.m_next.m_isH264Renderer)
            this.m_views[k].setOnClickListener(this);
          if ((this.m_next.m_isH264Renderer) || (k == this.m_next.m_renderer_view_index))
          {
            m = getRectangleIndex(k);
            label382: updateBorder(m);
            this.m_border.setVisibility(0);
          }
        }
        while (true)
        {
          if (j != -1)
          {
            initBackground();
            this.m_backgrounds[0].setVisibility(0);
            this.m_backgrounds[1].setVisibility(0);
            this.m_backgrounds[2].setVisibility(0);
            this.m_backgrounds[3].setVisibility(0);
            this.m_backgrounds[4].setVisibility(0);
            this.m_backgrounds[5].setVisibility(0);
            this.m_backgrounds[6].setVisibility(0);
          }
          return;
          if (this.m_next.m_renderer_size != 0)
            break label548;
          j = this.m_next.m_renderer_view_index;
          break;
          if (this.m_next.m_renderer_size != 1)
            break label542;
          k = this.m_next.m_renderer_view_index;
          break label148;
          m = 10;
          break label382;
          this.m_border.setVisibility(8);
        }
        label542: k = -1;
      }
      label548: j = -1;
    }
  }

  private void swapAvatar()
  {
    this.m_session.swapPip();
    if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.BOTH)
    {
      resetAvatarRenderersList();
      this.mLocalAvatarRenderer.setRenderView();
      this.mRemoteAvatarRenderer.setRenderView();
    }
    while (true)
    {
      super.updateVGoodBar();
      super.updateUserName();
      return;
      reinitVideoAvatarViews();
    }
  }

  private void swapView()
  {
    Log.v("Tango.Video", "swapView()");
    this.m_session.swapPip();
    swapViewSize();
    setViews(false);
  }

  private void swapViewSize()
  {
    Log.v("Tango.Video", "swapViewSize");
    if (this.m_next.m_camera_size == 0);
    do
    {
      Settings.access$302(this.m_next, 1);
      while (this.m_next.m_renderer_size == 0)
      {
        Settings.access$402(this.m_next, 1);
        return;
        if (this.m_next.m_camera_size == 1)
          Settings.access$302(this.m_next, 0);
      }
    }
    while (this.m_next.m_renderer_size != 1);
    Settings.access$402(this.m_next, 0);
  }

  private void updateBorder(int paramInt)
  {
    Settings.access$702(this.m_next, paramInt);
    initBorder();
  }

  private int viewOrientationToScreenOrientation(int paramInt)
  {
    int i = 1;
    if (paramInt == 0)
      i = 0;
    while (paramInt == i)
      return i;
    Log.v("Tango.Video", "viewOrientationToScreenOrientation unexpected orientation " + paramInt);
    return -1;
  }

  public void bringToFront()
  {
    Log.v("Tango.Video", "bringToFront");
    if (this.m_borderLayout != null)
      this.m_borderLayout.bringToFront();
  }

  public boolean changeViews()
  {
    Log.v("Tango.Video", "changeViews");
    initViewSize();
    return setViews(false);
  }

  public void hideCafe()
  {
  }

  public boolean initVideoViews()
  {
    Log.i("Tango.Video", "VideoTwoWayCanvasActivity.initViews()");
    initViewSize();
    return setViews(true);
  }

  public void onAvatarChanged()
  {
    reinitVideoAvatarViews();
  }

  public void onAvatarPaused()
  {
    pauseAndRemoveAvatarView();
  }

  public void onAvatarResumed()
  {
    reinitVideoAvatarViews();
  }

  public void onCleanUpAvatar()
  {
    pauseAndRemoveAvatarView();
  }

  public void onClick(View paramView)
  {
    Log.v("Tango.Video", "VideoTwoWayCanvasActivity.onClick");
    if ((paramView == this.m_views[2]) || (paramView == this.m_views[3]) || (paramView == this.m_views[5]))
    {
      Log.v("Tango.Video", "onClick(): Swap view...");
      if (!this.m_htcNoSwap)
      {
        if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.NONE)
          break label78;
        swapAvatar();
      }
    }
    while (true)
    {
      super.onClick(paramView);
      return;
      label78: swapView();
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.Video", "onCreate()");
    this.m_video_ui = this;
    VideoCaptureRaw.setActivityHandler(this.m_handler);
    VideoRenderer.setActivityHandler(this.m_handler);
    this.m_htcNoSwap = VideoRenderer.hasH264Renderer();
    super.onCreate(paramBundle);
  }

  protected void onDestroy()
  {
    Log.d("Tango.Video", "onDestroy()");
    VideoCaptureRaw.setActivityHandler(null);
    super.onDestroy();
  }

  public void onVideoModeChanged()
  {
    if (this.m_session.m_avatarDirection != SessionMessages.AvatarControlPayload.Direction.NONE)
    {
      reinitVideoAvatarViews();
      return;
    }
    super.initViews();
  }

  public boolean pipSwapSupported()
  {
    return !this.m_htcNoSwap;
  }

  public void showCafe()
  {
  }

  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    Log.v("Tango.Video", "surfaceChanged " + paramSurfaceHolder + " " + paramInt1 + " " + paramInt2 + "x" + paramInt3 + " surface: " + paramSurfaceHolder.getSurface());
    if ((paramSurfaceHolder == this.m_holders[4]) || (paramSurfaceHolder == this.m_holders[5]))
    {
      VideoRenderer.setPreviewDisplay(paramSurfaceHolder);
      if (!this.m_current.m_isH264Renderer)
      {
        if ((paramSurfaceHolder != this.m_holders[4]) || (this.m_session.m_videoDirection != SessionMessages.MediaSessionPayload.Direction.BOTH))
          break label150;
        VideoRenderer.setBitmapConfig(Bitmap.Config.ARGB_8888, this.m_rendererRect, this.m_transparentRect);
      }
    }
    while (true)
    {
      VideoRenderer.clear(-16777216);
      return;
      label150: VideoRenderer.setBitmapConfig(Bitmap.Config.RGB_565, this.m_rendererRect, null);
    }
  }

  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    Log.v("Tango.Video", "surfaceCreated " + paramSurfaceHolder + " surface: " + paramSurfaceHolder.getSurface());
    if ((paramSurfaceHolder == this.m_holders[0]) || (paramSurfaceHolder == this.m_holders[1]) || (paramSurfaceHolder == this.m_holders[2]) || (paramSurfaceHolder == this.m_holders[3]))
    {
      VideoCaptureRaw.setPreviewDisplay(paramSurfaceHolder);
      VideoCaptureRaw.staticResumeRecording();
    }
    if ((paramSurfaceHolder == this.m_holders[4]) || (paramSurfaceHolder == this.m_holders[5]))
      VideoRenderer.staticStartRenderer();
  }

  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    Log.v("Tango.Video", "surfaceDestroyed " + paramSurfaceHolder + " surface: " + paramSurfaceHolder.getSurface());
    if ((paramSurfaceHolder == this.m_holders[0]) || (paramSurfaceHolder == this.m_holders[1]) || (paramSurfaceHolder == this.m_holders[2]) || (paramSurfaceHolder == this.m_holders[3]))
    {
      VideoCaptureRaw.staticSuspendRecording();
      VideoCaptureRaw.setPreviewDisplay(null);
    }
    if ((paramSurfaceHolder == this.m_holders[4]) || (paramSurfaceHolder == this.m_holders[5]))
    {
      VideoRenderer.staticStopRenderer();
      VideoRenderer.setPreviewDisplay(null);
    }
  }

  public void updateLayout()
  {
    Log.v("Tango.Video", "updateLayout tid=" + Thread.currentThread().getId());
    if (this.m_views[0] == null)
    {
      Log.v("Tango.Video", "updateLayout: no views, returning");
      return;
    }
    if (this.m_next.m_isH264Renderer)
      VideoView.setRenderSize(192, 128);
    initCameraView(0);
    initCameraView(1);
    initRendererView(4);
    initCameraView(2);
    initCameraView(3);
    initRendererView(5);
    initBorder();
    initBackground();
    initButtonBackground();
  }

  public void updateRendererSize(int paramInt1, int paramInt2)
  {
    Log.d("Tango.Video", "updateRendererSize(" + paramInt1 + "," + paramInt2 + ")");
    if (this.m_views[0] == null)
      Log.v("Tango.Video", "updateRendererSize: no views, returning");
    while (VideoView.setRenderSize(paramInt1, paramInt2) != true)
      return;
    initRendererView(4);
    initCameraView(2);
    initCameraView(3);
    initRendererView(5);
    initBorder();
    initBackground();
    initButtonBackground();
  }

  class Settings
  {
    private int m_borderRectIndex = 6;
    private int m_camera_size = -1;
    private int m_camera_type = -1;
    private int m_camera_view_index = -1;
    private boolean m_isH264Renderer = false;
    private int m_renderer_size = -1;
    private int m_renderer_view_index = -1;
    private int m_requestedOrientation = -1;

    public Settings()
    {
    }

    public Settings clone()
    {
      Settings localSettings = new Settings(VideoTwoWayCanvasActivity.this);
      localSettings.m_camera_size = this.m_camera_size;
      localSettings.m_renderer_size = this.m_renderer_size;
      localSettings.m_camera_view_index = this.m_camera_view_index;
      localSettings.m_renderer_view_index = this.m_renderer_view_index;
      localSettings.m_borderRectIndex = this.m_borderRectIndex;
      localSettings.m_requestedOrientation = this.m_requestedOrientation;
      localSettings.m_camera_type = this.m_camera_type;
      localSettings.m_isH264Renderer = this.m_isH264Renderer;
      return localSettings;
    }

    public boolean equals(Settings paramSettings)
    {
      if (paramSettings.m_camera_size != this.m_camera_size)
        return false;
      if (paramSettings.m_renderer_size != this.m_renderer_size)
        return false;
      if (paramSettings.m_camera_view_index != this.m_camera_view_index)
        return false;
      if (paramSettings.m_renderer_view_index != this.m_renderer_view_index)
        return false;
      if (paramSettings.m_borderRectIndex != this.m_borderRectIndex)
        return false;
      if (paramSettings.m_requestedOrientation != this.m_requestedOrientation)
        return false;
      if (paramSettings.m_camera_type != this.m_camera_type)
        return false;
      return paramSettings.m_isH264Renderer == this.m_isH264Renderer;
    }

    public void print()
    {
      Log.v("Tango.Video", "equals " + this.m_camera_size + " " + this.m_renderer_size + " " + this.m_camera_view_index + " " + this.m_renderer_view_index + " " + this.m_borderRectIndex + " " + this.m_requestedOrientation + " " + this.m_camera_type + " " + this.m_isH264Renderer);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.VideoTwoWayCanvasActivity
 * JD-Core Version:    0.6.2
 */