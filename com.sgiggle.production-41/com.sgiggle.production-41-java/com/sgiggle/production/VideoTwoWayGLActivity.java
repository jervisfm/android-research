package com.sgiggle.production;

import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import com.sgiggle.GLES20.FilterManager;
import com.sgiggle.GLES20.GLCapture;
import com.sgiggle.GLES20.GLRenderer;
import com.sgiggle.GLES20.GLSurfaceViewEx;
import com.sgiggle.GLES20.VideoTwoWay;
import com.sgiggle.VideoCapture.VideoCaptureRaw;
import com.sgiggle.cafe.vgood.VGoodButtonsBarView;
import com.sgiggle.production.avatar.AvatarRenderer;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload.Direction;

public class VideoTwoWayGLActivity extends VideoTwoWayActivity
  implements VideoTwoWayActivity.VideoUI, SurfaceHolder.Callback, View.OnTouchListener
{
  private final int SURFACE_CAMERA = 0;
  private final int SURFACE_GL = 1;
  private int currentFilter;
  private int filterCount;
  private GLCapture glCapture;
  private GLRenderer glRenderer;
  private boolean mPaused;
  private SurfaceHolder[] m_holders = new SurfaceHolder[2];
  private SurfaceView[] m_views = new SurfaceView[2];
  private VideoTwoWay videoTwoWay;

  public void bringToFront()
  {
  }

  public boolean changeViews()
  {
    return changeViews(true);
  }

  public boolean changeViews(boolean paramBoolean)
  {
    int i = 3;
    int j;
    int k;
    if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND)
      if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.RECEIVER)
      {
        j = 1;
        k = -1;
      }
    while (true)
      if ((this.m_session.isPipSwapped()) && (this.m_session.pipSwappable()))
      {
        if (!paramBoolean)
          i = -1;
        this.videoTwoWay.setView(j, k, i);
        return false;
        j = -1;
        k = i;
        i = 1;
        continue;
        if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.RECEIVE)
        {
          if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.SENDER)
          {
            j = 4;
            k = -1;
            i = 0;
          }
          else
          {
            j = -1;
            k = i;
            i = 0;
          }
        }
        else if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.BOTH)
        {
          j = 1;
          k = i;
          i = 0;
        }
        else if (this.m_session.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.NONE)
        {
          if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.BOTH)
          {
            j = 4;
            k = -1;
            continue;
          }
          if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.SENDER)
          {
            j = -1;
            k = -1;
            i = 4;
            continue;
          }
          if (this.m_session.m_avatarDirection == SessionMessages.AvatarControlPayload.Direction.RECEIVER)
          {
            j = -1;
            k = -1;
          }
        }
      }
      else
      {
        if (!paramBoolean)
          j = -1;
        this.videoTwoWay.setView(i, k, j);
        return false;
        j = -1;
        k = -1;
        i = -1;
      }
  }

  public void hideCafe()
  {
    if (this.glRenderer != null)
      this.glRenderer.setRenderMode(0);
  }

  public boolean initVideoViews()
  {
    if (this.m_views[1] != null)
    {
      if (!this.mPaused)
        return changeViews();
      ((GLSurfaceViewEx)this.m_views[1]).onDetachedFromWindow();
      this.mPaused = false;
    }
    this.m_orientation = 1;
    setContentView(2130903154);
    if (this.glCapture == null)
    {
      this.m_views[0] = ((SurfaceView)findViewById(2131362176));
      this.m_views[0].setVisibility(0);
      this.m_holders[0] = this.m_views[0].getHolder();
      this.m_holders[0].setType(3);
      this.m_holders[0].addCallback(this);
    }
    this.m_views[1] = ((SurfaceView)findViewById(2131362214));
    this.m_views[1].setOnTouchListener(this);
    this.m_holders[1] = this.m_views[1].getHolder();
    this.m_holders[1].addCallback(this);
    this.m_views[1].setZOrderMediaOverlay(true);
    int i = getWindowManager().getDefaultDisplay().getWidth();
    int j = getWindowManager().getDefaultDisplay().getHeight();
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)this.m_views[1].getLayoutParams();
    localMarginLayoutParams.leftMargin = 0;
    localMarginLayoutParams.topMargin = 0;
    localMarginLayoutParams.width = i;
    localMarginLayoutParams.height = j;
    this.m_views[1].setLayoutParams(localMarginLayoutParams);
    this.videoTwoWay.setSurface((GLSurfaceViewEx)this.m_views[1]);
    changeViews();
    return true;
  }

  public void onAvatarChanged()
  {
    initViews();
    this.m_video_ui.showCafe();
  }

  public void onAvatarPaused()
  {
    hideCafe();
  }

  public void onAvatarResumed()
  {
    showCafe();
  }

  public void onCleanUpAvatar()
  {
    hideCafe();
  }

  protected void onCreate(Bundle paramBundle)
  {
    this.m_video_ui = this;
    VideoCaptureRaw.setActivityHandler(this.m_handler);
    this.glRenderer = GLRenderer.getInstance();
    if (GLRenderer.hasGLCapture())
      this.glCapture = GLCapture.getInstance();
    this.videoTwoWay = new VideoTwoWay();
    this.filterCount = FilterManager.install(this, "filters");
    this.currentFilter = -1;
    setFilter(this.currentFilter, -1);
    super.onCreate(paramBundle);
  }

  protected void onDestroy()
  {
    Log.v("Tango.Video", "onDestroy");
    VideoCaptureRaw.setActivityHandler(null);
    this.videoTwoWay.release();
    super.onDestroy();
  }

  protected void onPause()
  {
    if (this.m_views[1] != null)
    {
      ((GLSurfaceViewEx)this.m_views[1]).onPause();
      this.mPaused = true;
    }
    super.onPause();
  }

  protected void onResume()
  {
    if (TangoApp.getInstance().getAppRunningState() == TangoApp.AppState.APP_STATE_BACKGROUND)
      changeViews(false);
    if (this.m_views[1] != null)
      ((GLSurfaceViewEx)this.m_views[1]).onResume();
    super.onResume();
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() != 1)
      return true;
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    if (this.videoTwoWay.isInside(f1, f2) == true)
    {
      this.videoTwoWay.swapView();
      this.m_session.swapPip();
      super.updateUserName();
    }
    return true;
  }

  protected void onUserLeaveHint()
  {
    Log.v("Tango.Video", "onUserLeaveHint()");
    this.mLocalAvatarRenderer.stopAvatarAnimation();
    this.mRemoteAvatarRenderer.stopAvatarAnimation();
    cleanUpCafe();
    super.onUserLeaveHint();
  }

  public void onVideoModeChanged()
  {
    initViews();
  }

  public boolean pipSwapSupported()
  {
    return true;
  }

  public void setFilter(int paramInt1, int paramInt2)
  {
    int i = -1;
    int j;
    if (paramInt1 == this.currentFilter)
      j = i;
    while (true)
    {
      FilterManager.setFilter(i);
      if (this.m_vgoodBtnLayout != null)
        this.m_vgoodBtnLayout.setHightlightWithBackground(j);
      this.currentFilter = i;
      return;
      j = paramInt2;
      i = paramInt1;
    }
  }

  protected void setFilterHightlightWithBackground()
  {
    if ((this.m_vgoodBtnLayout != null) && (this.currentFilter != -1))
      this.m_vgoodBtnLayout.setFilterHightlightWithBackground(this.currentFilter);
  }

  public void showCafe()
  {
    if (this.glRenderer != null)
      this.glRenderer.setRenderMode(1);
  }

  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    Log.d("Tango.Video", "surfaceChanged " + paramSurfaceHolder + " " + paramInt1 + " " + paramInt2 + " " + paramInt3);
  }

  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    Log.d("Tango.Video", "surfaceCreated " + paramSurfaceHolder);
    if ((paramSurfaceHolder == this.m_holders[0]) || (this.glCapture != null))
    {
      VideoCaptureRaw.setPreviewDisplay(paramSurfaceHolder);
      VideoCaptureRaw.staticResumeRecording();
    }
  }

  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    Log.d("Tango.Video", "surfaceDestroyed " + paramSurfaceHolder);
    if ((paramSurfaceHolder == this.m_holders[0]) || (this.glCapture != null))
    {
      VideoCaptureRaw.staticSuspendRecording();
      VideoCaptureRaw.setPreviewDisplay(null);
    }
  }

  public void updateLayout()
  {
  }

  public void updateRendererSize(int paramInt1, int paramInt2)
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.VideoTwoWayGLActivity
 * JD-Core Version:    0.6.2
 */