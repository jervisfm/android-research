package com.sgiggle.production;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayProductInfoEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ProductInfoOkMessage;
import com.sgiggle.media_engine.MediaEngineMessage.WelcomeGoToMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.WelcomePayload;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WelcomeScreenActivity extends ActivityBase
{
  private static final String LINK_PATTERN = "tango.*?://welcome_screen/q\\?action=(.*)";
  private static final String TAG = "Tango.UI";
  private String m_url;
  private WebView m_webview;

  public static boolean handleWelcomeUrl(String paramString)
  {
    Matcher localMatcher = Pattern.compile("tango.*?://welcome_screen/q\\?action=(.*)").matcher(paramString);
    if (localMatcher.matches())
    {
      String str = localMatcher.group(1);
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.WelcomeGoToMessage(str));
      return true;
    }
    return false;
  }

  public void onBackPressed()
  {
    this.m_webview.setWebViewClient(null);
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ProductInfoOkMessage());
    super.onBackPressed();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    setContentView(2130903155);
    TextView localTextView = (TextView)findViewById(2131361841);
    localTextView.setText(2131296541);
    findViewById(2131362215).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        WelcomeScreenActivity.this.onBackPressed();
      }
    });
    this.m_webview = ((WebView)findViewById(2131362116));
    this.m_webview.getSettings().setJavaScriptEnabled(true);
    this.m_webview.getSettings().setPluginsEnabled(true);
    this.m_webview.setScrollBarStyle(33554432);
    this.m_webview.setWebViewClient(new WebViewClientCtr(null));
    Message localMessage = getFirstMessage();
    if (localMessage != null)
    {
      MediaEngineMessage.DisplayProductInfoEvent localDisplayProductInfoEvent = (MediaEngineMessage.DisplayProductInfoEvent)localMessage;
      localTextView.setText(((SessionMessages.WelcomePayload)localDisplayProductInfoEvent.payload()).getTitle());
      this.m_url = ((SessionMessages.WelcomePayload)localDisplayProductInfoEvent.payload()).getUrl();
      this.m_url += "?tango";
      this.m_webview.loadUrl(this.m_url);
    }
  }

  private class WebViewClientCtr extends WebViewClient
  {
    private WebViewClientCtr()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      WelcomeScreenActivity.this.findViewById(2131361798).setVisibility(8);
    }

    public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
    {
      Log.e("Tango.UI", "ERROR LOADING:" + paramString1 + " " + paramString2);
      if (!WelcomeScreenActivity.this.isFinishing())
        WelcomeScreenActivity.this.onBackPressed();
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      if (WelcomeScreenActivity.handleWelcomeUrl(paramString))
      {
        WelcomeScreenActivity.this.finish();
        return true;
      }
      if (paramString.startsWith("vnd.youtube:"))
        try
        {
          WelcomeScreenActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(paramString)));
          WelcomeScreenActivity.this.onBackPressed();
          return true;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
          while (true)
            Log.e("Tango.UI", "No youtube application installed");
        }
      return false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.WelcomeScreenActivity
 * JD-Core Version:    0.6.2
 */