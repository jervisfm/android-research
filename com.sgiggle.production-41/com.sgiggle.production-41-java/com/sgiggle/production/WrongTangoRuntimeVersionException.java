package com.sgiggle.production;

public class WrongTangoRuntimeVersionException extends Exception
{
  private static final long serialVersionUID = 1L;
  private String m_tangoCoreVersion;
  private String m_uiVersion;

  public WrongTangoRuntimeVersionException(String paramString1, String paramString2)
  {
    super(createMessage(paramString1, paramString2));
    this.m_uiVersion = paramString1;
    this.m_tangoCoreVersion = paramString2;
  }

  public WrongTangoRuntimeVersionException(String paramString1, String paramString2, Throwable paramThrowable)
  {
    super(createMessage(paramString1, paramString2), paramThrowable);
    this.m_uiVersion = paramString1;
    this.m_tangoCoreVersion = paramString2;
  }

  private static String createMessage(String paramString1, String paramString2)
  {
    return "Initialization failed due to UI/Runtime version mismatch: ui version=" + paramString1 + " core version=" + paramString2;
  }

  public String getTangoCoreVersion()
  {
    return this.m_tangoCoreVersion;
  }

  public String getUiVersion()
  {
    return this.m_uiVersion;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.WrongTangoRuntimeVersionException
 * JD-Core Version:    0.6.2
 */