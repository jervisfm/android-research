package com.sgiggle.production.actionbarcompat;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import com.sgiggle.production.FragmentActivityBase;

public abstract class ActionBarActivity extends FragmentActivityBase
{
  private static final int MSG_REFRESH_ACTION_BAR;
  private ActionBarHelper mActionBarHelper = ActionBarHelper.createInstance(this);
  private Handler m_handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        return;
      case 0:
      }
      ActionBarActivity.this.performRefreshActionBar();
    }
  };

  private void performRefreshActionBar()
  {
    if (isFinishing())
      return;
    setTitle(getTitle());
  }

  protected ActionBarHelper getActionBarHelper()
  {
    return this.mActionBarHelper;
  }

  public abstract int getActionBarHomeIconResId();

  public MenuInflater getMenuInflater()
  {
    return this.mActionBarHelper.getMenuInflater(super.getMenuInflater());
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mActionBarHelper.onCreate(paramBundle);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return false | this.mActionBarHelper.onCreateOptionsMenu(paramMenu) | super.onCreateOptionsMenu(paramMenu);
  }

  protected void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    this.mActionBarHelper.onPostCreate(paramBundle);
  }

  protected void onTitleChanged(CharSequence paramCharSequence, int paramInt)
  {
    this.mActionBarHelper.onTitleChanged(paramCharSequence, paramInt);
    super.onTitleChanged(paramCharSequence, paramInt);
  }

  public void refreshActionBar()
  {
    if (Build.VERSION.SDK_INT != 15)
      return;
    this.m_handler.removeMessages(0);
    this.m_handler.sendEmptyMessage(0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.actionbarcompat.ActionBarActivity
 * JD-Core Version:    0.6.2
 */