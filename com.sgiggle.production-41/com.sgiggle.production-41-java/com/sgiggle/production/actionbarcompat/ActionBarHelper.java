package com.sgiggle.production.actionbarcompat;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

public abstract class ActionBarHelper
{
  protected ActionBarActivity mActivity;

  protected ActionBarHelper(ActionBarActivity paramActionBarActivity)
  {
    this.mActivity = paramActionBarActivity;
  }

  public static ActionBarHelper createInstance(ActionBarActivity paramActionBarActivity)
  {
    if (Build.VERSION.SDK_INT >= 11)
      return new ActionBarHelperHoneycomb(paramActionBarActivity);
    return new ActionBarHelperBase(paramActionBarActivity);
  }

  public MenuInflater getMenuInflater(MenuInflater paramMenuInflater)
  {
    return paramMenuInflater;
  }

  public void onCreate(Bundle paramBundle)
  {
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public void onPostCreate(Bundle paramBundle)
  {
  }

  public void onTitleChanged(CharSequence paramCharSequence, int paramInt)
  {
  }

  public abstract void setDisplayHomeAsUpEnabled(boolean paramBoolean);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.actionbarcompat.ActionBarHelper
 * JD-Core Version:    0.6.2
 */