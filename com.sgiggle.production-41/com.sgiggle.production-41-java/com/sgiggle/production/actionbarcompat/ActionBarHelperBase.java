package com.sgiggle.production.actionbarcompat;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ActionBarHelperBase extends ActionBarHelper
{
  private static final String MENU_ATTR_ID = "id";
  private static final String MENU_ATTR_SHOW_AS_ACTION = "showAsAction";
  private static final String MENU_RES_NAMESPACE = "http://schemas.android.com/apk/res/android";
  protected Set<Integer> mActionItemIds = new HashSet();
  protected View m_upView;

  protected ActionBarHelperBase(ActionBarActivity paramActionBarActivity)
  {
    super(paramActionBarActivity);
  }

  private View addActionItemCompatFromMenuItem(final MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    ViewGroup localViewGroup = getActionBarCompat();
    if (localViewGroup == null)
      return null;
    ActionBarActivity localActionBarActivity = this.mActivity;
    int j;
    ImageButton localImageButton;
    Resources localResources;
    if (i == 16908332)
    {
      j = 2130771970;
      localImageButton = new ImageButton(localActionBarActivity, null, j);
      localResources = this.mActivity.getResources();
      if (i != 16908332)
        break label162;
    }
    label162: for (int k = 2131230727; ; k = 2131230726)
    {
      localImageButton.setLayoutParams(new ViewGroup.LayoutParams((int)localResources.getDimension(k), -1));
      localImageButton.setImageDrawable(paramMenuItem.getIcon());
      localImageButton.setScaleType(ImageView.ScaleType.CENTER);
      localImageButton.setContentDescription(paramMenuItem.getTitle());
      localImageButton.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          ActionBarHelperBase.this.mActivity.onMenuItemSelected(0, paramMenuItem);
        }
      });
      localImageButton.setOnLongClickListener(new View.OnLongClickListener()
      {
        public boolean onLongClick(View paramAnonymousView)
        {
          if (TextUtils.isEmpty(paramMenuItem.getTitle()))
            return false;
          Toast localToast = Toast.makeText(ActionBarHelperBase.this.mActivity, paramMenuItem.getTitle(), 0);
          localToast.setGravity(49, 0, ActionBarHelperBase.this.mActivity.getResources().getDimensionPixelSize(2131230725));
          localToast.show();
          return true;
        }
      });
      localViewGroup.addView(localImageButton);
      return localImageButton;
      j = 2130771969;
      break;
    }
  }

  private ViewGroup getActionBarCompat()
  {
    return (ViewGroup)this.mActivity.findViewById(2131361792);
  }

  private void setupActionBar()
  {
    ViewGroup localViewGroup = getActionBarCompat();
    if (localViewGroup == null)
      return;
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(0, -1);
    localLayoutParams.weight = 1.0F;
    final SimpleMenuItem localSimpleMenuItem = new SimpleMenuItem(new SimpleMenu(this.mActivity), 16908332, 0, this.mActivity.getTitle());
    localSimpleMenuItem.setIcon(this.mActivity.getActionBarHomeIconResId());
    addActionItemCompatFromMenuItem(localSimpleMenuItem);
    this.m_upView = localViewGroup.findViewById(2131361794);
    this.m_upView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ActionBarHelperBase.this.mActivity.onMenuItemSelected(0, localSimpleMenuItem);
      }
    });
    TextView localTextView = new TextView(this.mActivity, null, 2130771968);
    localTextView.setLayoutParams(localLayoutParams);
    localTextView.setText(this.mActivity.getTitle());
    localViewGroup.addView(localTextView);
  }

  public MenuInflater getMenuInflater(MenuInflater paramMenuInflater)
  {
    return new WrappedMenuInflater(this.mActivity, paramMenuInflater);
  }

  public void onCreate(Bundle paramBundle)
  {
    this.mActivity.requestWindowFeature(7);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    Iterator localIterator = this.mActionItemIds.iterator();
    while (localIterator.hasNext())
      paramMenu.findItem(((Integer)localIterator.next()).intValue()).setVisible(false);
    return true;
  }

  public void onPostCreate(Bundle paramBundle)
  {
    this.mActivity.getWindow().setFeatureInt(7, 2130903040);
    setupActionBar();
    SimpleMenu localSimpleMenu = new SimpleMenu(this.mActivity);
    this.mActivity.onCreatePanelMenu(0, localSimpleMenu);
    this.mActivity.onPrepareOptionsMenu(localSimpleMenu);
    for (int i = 0; i < localSimpleMenu.size(); i++)
    {
      SimpleMenuItem localSimpleMenuItem = (SimpleMenuItem)localSimpleMenu.getItem(i);
      if (this.mActionItemIds.contains(Integer.valueOf(localSimpleMenuItem.getItemId())))
        localSimpleMenuItem.setView(addActionItemCompatFromMenuItem(localSimpleMenuItem));
    }
  }

  public void onTitleChanged(CharSequence paramCharSequence, int paramInt)
  {
    TextView localTextView = (TextView)this.mActivity.findViewById(2131361793);
    if (localTextView != null)
      localTextView.setText(paramCharSequence);
  }

  public void setDisplayHomeAsUpEnabled(boolean paramBoolean)
  {
    View localView = this.m_upView;
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localView.setVisibility(i);
      return;
    }
  }

  private class WrappedMenuInflater extends MenuInflater
  {
    MenuInflater mInflater;

    public WrappedMenuInflater(Context paramMenuInflater, MenuInflater arg3)
    {
      super();
      Object localObject;
      this.mInflater = localObject;
    }

    // ERROR //
    private void loadActionBarMetadata(int paramInt)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 12	com/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater:this$0	Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;
      //   4: getfield 29	com/sgiggle/production/actionbarcompat/ActionBarHelperBase:mActivity	Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;
      //   7: invokevirtual 35	com/sgiggle/production/actionbarcompat/ActionBarActivity:getResources	()Landroid/content/res/Resources;
      //   10: iload_1
      //   11: invokevirtual 41	android/content/res/Resources:getXml	(I)Landroid/content/res/XmlResourceParser;
      //   14: astore 9
      //   16: aload 9
      //   18: invokeinterface 47 1 0
      //   23: istore 13
      //   25: iconst_0
      //   26: istore 14
      //   28: goto +220 -> 248
      //   31: aload 9
      //   33: invokeinterface 50 1 0
      //   38: istore 13
      //   40: goto +208 -> 248
      //   43: aload 9
      //   45: invokeinterface 54 1 0
      //   50: ldc 56
      //   52: invokevirtual 62	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   55: ifeq -24 -> 31
      //   58: aload 9
      //   60: ldc 64
      //   62: ldc 66
      //   64: iconst_0
      //   65: invokeinterface 70 4 0
      //   70: istore 15
      //   72: iload 15
      //   74: ifeq -43 -> 31
      //   77: aload 9
      //   79: ldc 64
      //   81: ldc 72
      //   83: iconst_m1
      //   84: invokeinterface 75 4 0
      //   89: istore 16
      //   91: iload 16
      //   93: iconst_2
      //   94: if_icmpeq +9 -> 103
      //   97: iload 16
      //   99: iconst_1
      //   100: if_icmpne -69 -> 31
      //   103: aload_0
      //   104: getfield 12	com/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater:this$0	Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;
      //   107: getfield 79	com/sgiggle/production/actionbarcompat/ActionBarHelperBase:mActionItemIds	Ljava/util/Set;
      //   110: iload 15
      //   112: invokestatic 85	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   115: invokeinterface 90 2 0
      //   120: pop
      //   121: goto -90 -> 31
      //   124: astore 12
      //   126: aload 9
      //   128: astore 4
      //   130: aload 12
      //   132: astore_3
      //   133: new 92	android/view/InflateException
      //   136: dup
      //   137: ldc 94
      //   139: aload_3
      //   140: invokespecial 97	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
      //   143: athrow
      //   144: astore 5
      //   146: aload 4
      //   148: ifnull +10 -> 158
      //   151: aload 4
      //   153: invokeinterface 101 1 0
      //   158: aload 5
      //   160: athrow
      //   161: iconst_1
      //   162: istore 14
      //   164: goto -133 -> 31
      //   167: aload 9
      //   169: ifnull +10 -> 179
      //   172: aload 9
      //   174: invokeinterface 101 1 0
      //   179: return
      //   180: astore 7
      //   182: aconst_null
      //   183: astore 4
      //   185: aload 7
      //   187: astore 8
      //   189: new 92	android/view/InflateException
      //   192: dup
      //   193: ldc 94
      //   195: aload 8
      //   197: invokespecial 97	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
      //   200: athrow
      //   201: astore 6
      //   203: aload 6
      //   205: astore 5
      //   207: aconst_null
      //   208: astore 4
      //   210: goto -64 -> 146
      //   213: astore 11
      //   215: aload 9
      //   217: astore 4
      //   219: aload 11
      //   221: astore 5
      //   223: goto -77 -> 146
      //   226: astore 10
      //   228: aload 9
      //   230: astore 4
      //   232: aload 10
      //   234: astore 8
      //   236: goto -47 -> 189
      //   239: astore_2
      //   240: aload_2
      //   241: astore_3
      //   242: aconst_null
      //   243: astore 4
      //   245: goto -112 -> 133
      //   248: iload 14
      //   250: ifne -83 -> 167
      //   253: iload 13
      //   255: tableswitch	default:+-224 -> 31, 1:+-94->161, 2:+-212->43
      //
      // Exception table:
      //   from	to	target	type
      //   16	25	124	org/xmlpull/v1/XmlPullParserException
      //   31	40	124	org/xmlpull/v1/XmlPullParserException
      //   43	72	124	org/xmlpull/v1/XmlPullParserException
      //   77	91	124	org/xmlpull/v1/XmlPullParserException
      //   103	121	124	org/xmlpull/v1/XmlPullParserException
      //   133	144	144	finally
      //   189	201	144	finally
      //   0	16	180	java/io/IOException
      //   0	16	201	finally
      //   16	25	213	finally
      //   31	40	213	finally
      //   43	72	213	finally
      //   77	91	213	finally
      //   103	121	213	finally
      //   16	25	226	java/io/IOException
      //   31	40	226	java/io/IOException
      //   43	72	226	java/io/IOException
      //   77	91	226	java/io/IOException
      //   103	121	226	java/io/IOException
      //   0	16	239	org/xmlpull/v1/XmlPullParserException
    }

    public void inflate(int paramInt, Menu paramMenu)
    {
      loadActionBarMetadata(paramInt);
      this.mInflater.inflate(paramInt, paramMenu);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.actionbarcompat.ActionBarHelperBase
 * JD-Core Version:    0.6.2
 */