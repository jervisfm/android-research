package com.sgiggle.production.actionbarcompat;

import android.app.ActionBar;

public class ActionBarHelperHoneycomb extends ActionBarHelper
{
  protected ActionBarHelperHoneycomb(ActionBarActivity paramActionBarActivity)
  {
    super(paramActionBarActivity);
  }

  public void setDisplayHomeAsUpEnabled(boolean paramBoolean)
  {
    this.mActivity.getActionBar().setDisplayHomeAsUpEnabled(paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.actionbarcompat.ActionBarHelperHoneycomb
 * JD-Core Version:    0.6.2
 */