package com.sgiggle.production.adapter;

import android.content.Context;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.production.CTANotifier.AppAlert;
import com.sgiggle.xmpp.SessionMessages.OperationalAlert;
import java.util.List;

public class AlertListAdapter extends ArrayAdapter<SessionMessages.OperationalAlert>
{
  public static final int KEY_TAG_APP_ALERT = 1;
  public static final int KEY_TAG_HOLDER;
  private final Context m_context;
  private List<SessionMessages.OperationalAlert> m_data;
  private final LayoutInflater m_inflater;

  public AlertListAdapter(Context paramContext, List<SessionMessages.OperationalAlert> paramList)
  {
    super(paramContext, 0, paramList);
    this.m_context = paramContext;
    this.m_data = paramList;
    this.m_inflater = ((LayoutInflater)this.m_context.getSystemService("layout_inflater"));
  }

  private Spanned getMessageWithLinks(String paramString)
  {
    Object localObject = Html.fromHtml(paramString);
    if (paramString.equals(localObject.toString()))
    {
      localObject = new SpannableString(paramString);
      Linkify.addLinks((Spannable)localObject, 1);
    }
    return localObject;
  }

  public int getCount()
  {
    if (this.m_data != null)
      return this.m_data.size();
    return 0;
  }

  public List<SessionMessages.OperationalAlert> getDataSet()
  {
    return this.m_data;
  }

  public SessionMessages.OperationalAlert getItem(int paramInt)
  {
    if ((this.m_data != null) && (paramInt < this.m_data.size()) && (paramInt >= 0))
      return (SessionMessages.OperationalAlert)this.m_data.get(paramInt);
    return null;
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null);
    for (View localView = this.m_inflater.inflate(2130903113, null); ; localView = paramView)
    {
      TextView localTextView1 = (TextView)localView.findViewById(2131362102);
      TextView localTextView2 = (TextView)localView.findViewById(2131362103);
      ImageView localImageView = (ImageView)localView.findViewById(2131362101);
      CTANotifier.AppAlert localAppAlert = new CTANotifier.AppAlert((SessionMessages.OperationalAlert)this.m_data.get(paramInt));
      localTextView1.setText(Html.fromHtml(localAppAlert.CTA().getTitle()));
      localTextView2.setText(getMessageWithLinks(localAppAlert.CTA().getMessage()));
      localTextView2.setOnTouchListener(new OpenLinksTouchListener(null));
      if ((localAppAlert.isRegistrationRequired()) || (localAppAlert.isValidationRequired()))
        localImageView.setImageResource(2130837636);
      while (true)
      {
        localView.setTag(localAppAlert);
        return localView;
        localImageView.setImageResource(2130837724);
      }
    }
  }

  private static class OpenLinksTouchListener
    implements View.OnTouchListener
  {
    public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
    {
      TextView localTextView = (TextView)paramView;
      ClickableSpan[] arrayOfClickableSpan;
      if ((paramMotionEvent.getAction() == 0) && ((localTextView.getText() instanceof Spanned)))
      {
        int i = (int)paramMotionEvent.getX();
        int j = (int)paramMotionEvent.getY();
        int k = i - localTextView.getTotalPaddingLeft();
        int m = j - localTextView.getTotalPaddingTop();
        int n = k + localTextView.getScrollX();
        int i1 = m + localTextView.getScrollY();
        Layout localLayout = localTextView.getLayout();
        int i2 = localLayout.getOffsetForHorizontal(localLayout.getLineForVertical(i1), n);
        if (i2 != localTextView.getText().length())
        {
          arrayOfClickableSpan = (ClickableSpan[])((Spanned)localTextView.getText()).getSpans(i2, i2, ClickableSpan.class);
          if (arrayOfClickableSpan.length == 0);
        }
      }
      try
      {
        arrayOfClickableSpan[0].onClick(localTextView);
        label146: return true;
        return false;
      }
      catch (Throwable localThrowable)
      {
        break label146;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.AlertListAdapter
 * JD-Core Version:    0.6.2
 */