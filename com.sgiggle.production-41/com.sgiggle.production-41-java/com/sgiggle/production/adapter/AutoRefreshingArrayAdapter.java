package com.sgiggle.production.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.ArrayAdapter;
import com.sgiggle.util.Log;

public abstract class AutoRefreshingArrayAdapter<T> extends ArrayAdapter<T>
{
  private static final int MSG_REFRESH = 1;
  private static final String TAG = "Tango.AutoRefreshingArrayAdapter";
  private Handler m_handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
      case 1:
      }
      do
      {
        return;
        Log.d("Tango.AutoRefreshingArrayAdapter", "Refreshing self.");
        AutoRefreshingArrayAdapter.this.refresh();
      }
      while (!AutoRefreshingArrayAdapter.this.m_isAutoRefreshing);
      AutoRefreshingArrayAdapter.this.m_handler.sendEmptyMessageDelayed(1, AutoRefreshingArrayAdapter.this.m_refreshIntervalMs);
    }
  };
  private boolean m_isAutoRefreshing = false;
  private long m_refreshIntervalMs = getRefreshIntervalMs();

  public AutoRefreshingArrayAdapter(Context paramContext, int paramInt)
  {
    super(paramContext, paramInt);
  }

  private void refresh()
  {
    notifyDataSetChanged();
  }

  protected abstract long getRefreshIntervalMs();

  public boolean isAutoRefreshing()
  {
    return this.m_isAutoRefreshing;
  }

  public void startAutoRefresh(boolean paramBoolean)
  {
    if (this.m_isAutoRefreshing)
      return;
    stopAutoRefresh();
    this.m_isAutoRefreshing = true;
    if (paramBoolean)
      refresh();
    this.m_handler.sendEmptyMessageDelayed(1, this.m_refreshIntervalMs);
  }

  public void stopAutoRefresh()
  {
    this.m_handler.removeMessages(1);
    this.m_isAutoRefreshing = false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.AutoRefreshingArrayAdapter
 * JD-Core Version:    0.6.2
 */