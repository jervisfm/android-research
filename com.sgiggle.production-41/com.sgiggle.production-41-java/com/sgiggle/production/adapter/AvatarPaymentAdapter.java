package com.sgiggle.production.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.production.payments.BillingServiceManager;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.xmpp.SessionMessages.Price;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry.Builder;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class AvatarPaymentAdapter extends ArrayAdapter<SessionMessages.ProductCatalogEntry>
  implements View.OnClickListener
{
  private HashMap<Long, SoftReference<Drawable>> iconsMap = new HashMap();
  private OnClickListener m_clickListener;
  private LayoutInflater m_inflater;
  private boolean m_isBillingSupported = false;
  private boolean m_isPurchaseEnabled = false;
  private int m_rowColorDark;
  private int m_rowColorLight;

  public AvatarPaymentAdapter(Context paramContext)
  {
    super(paramContext, 0);
    this.m_inflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    this.m_rowColorLight = paramContext.getResources().getColor(2131165218);
    this.m_rowColorDark = paramContext.getResources().getColor(2131165217);
    boolean bool = BillingServiceManager.getInstance().isSupported();
    this.m_isPurchaseEnabled = bool;
    this.m_isBillingSupported = bool;
  }

  private int getColorForPosition(int paramInt)
  {
    if (paramInt % 2 == 0)
      return this.m_rowColorLight;
    return this.m_rowColorDark;
  }

  private Drawable getDrawable(String paramString)
  {
    if (new File(paramString).isDirectory())
      return null;
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inDensity = 320;
    localOptions.inTargetDensity = getContext().getResources().getDisplayMetrics().densityDpi;
    Bitmap localBitmap = BitmapFactory.decodeFile(paramString, localOptions);
    if (localBitmap == null)
      return null;
    BitmapDrawable localBitmapDrawable = new BitmapDrawable(getContext().getResources(), localBitmap);
    localBitmapDrawable.setTargetDensity(getContext().getResources().getDisplayMetrics().densityDpi);
    return localBitmapDrawable;
  }

  private String getPriceText(SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    if (paramProductCatalogEntry.hasPrice())
      return paramProductCatalogEntry.getPrice().getLabel();
    return "Check price";
  }

  private void notifyItemClicked(int paramInt, SessionMessages.ProductCatalogEntry paramProductCatalogEntry)
  {
    if (this.m_clickListener != null)
      this.m_clickListener.onClick(paramInt, paramProductCatalogEntry);
  }

  private void setPurchaseEnabled(boolean paramBoolean)
  {
    this.m_isPurchaseEnabled = paramBoolean;
    notifyDataSetChanged();
  }

  public OnClickListener getOnClickListener()
  {
    return this.m_clickListener;
  }

  public SessionMessages.ProductCatalogEntry getProductItemFromMarketId(String paramString)
  {
    int i = getCount();
    for (int j = 0; j < i; j++)
    {
      SessionMessages.ProductCatalogEntry localProductCatalogEntry = (SessionMessages.ProductCatalogEntry)getItem(j);
      if (localProductCatalogEntry.getProductMarketId().equals(paramString))
        return localProductCatalogEntry;
    }
    return null;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView2;
    if (paramView == null)
    {
      localView2 = this.m_inflater.inflate(2130903048, null);
      ViewHolder localViewHolder2 = new ViewHolder();
      localViewHolder2.title = ((TextView)localView2.findViewById(2131361841));
      localViewHolder2.desc = ((TextView)localView2.findViewById(2131361842));
      localViewHolder2.demoBtn = ((Button)localView2.findViewById(2131361843));
      localViewHolder2.buyBtn = ((Button)localView2.findViewById(2131361833));
      localViewHolder2.marketing_icon = ((ImageView)localView2.findViewById(2131361840));
      localView2.setTag(localViewHolder2);
    }
    for (View localView1 = localView2; ; localView1 = paramView)
    {
      ViewHolder localViewHolder1 = (ViewHolder)localView1.getTag();
      SessionMessages.ProductCatalogEntry localProductCatalogEntry = (SessionMessages.ProductCatalogEntry)getItem(paramInt);
      localView1.setBackgroundColor(getColorForPosition(paramInt));
      localViewHolder1.title.setText(localProductCatalogEntry.getProductName());
      localViewHolder1.desc.setText(localProductCatalogEntry.getProductDescription());
      localViewHolder1.demoBtn.setTag(localProductCatalogEntry);
      localViewHolder1.demoBtn.setOnClickListener(this);
      localViewHolder1.buyBtn.setText(getPriceText(localProductCatalogEntry));
      localViewHolder1.buyBtn.setOnClickListener(this);
      localViewHolder1.buyBtn.setTag(localProductCatalogEntry);
      Constants.PurchaseState localPurchaseState = (Constants.PurchaseState)BillingServiceManager.getInstance().purchaseStateMap.get(localProductCatalogEntry.getProductMarketId());
      if ((!localProductCatalogEntry.getPurchased()) && (localProductCatalogEntry.hasPrice()) && (localProductCatalogEntry.getPrice().getValue() == 0.0F))
      {
        localViewHolder1.buyBtn.setEnabled(true);
        if (localProductCatalogEntry.getImagePath() == null)
          break label481;
        SoftReference localSoftReference = (SoftReference)this.iconsMap.get(Long.valueOf(localProductCatalogEntry.getProductId()));
        Drawable localDrawable;
        if (localSoftReference != null)
        {
          localDrawable = (Drawable)localSoftReference.get();
          if (localDrawable != null);
        }
        else
        {
          localDrawable = getDrawable(localProductCatalogEntry.getImagePath());
          if (localDrawable != null)
            this.iconsMap.put(Long.valueOf(localProductCatalogEntry.getProductId()), new SoftReference(localDrawable));
        }
        if (localDrawable == null)
          break label467;
        localViewHolder1.marketing_icon.setImageDrawable(localDrawable);
      }
      while (true)
      {
        if ((!localProductCatalogEntry.getPurchased()) && ((localPurchaseState == null) || (localPurchaseState != Constants.PurchaseState.PURCHASED)))
          break label495;
        localViewHolder1.buyBtn.setText(2131296507);
        return localView1;
        if ((localProductCatalogEntry.getPurchased()) || ((localPurchaseState != null) && (localPurchaseState == Constants.PurchaseState.PURCHASED)) || (!this.m_isPurchaseEnabled))
        {
          localViewHolder1.buyBtn.setEnabled(false);
          break;
        }
        localViewHolder1.buyBtn.setEnabled(true);
        break;
        label467: localViewHolder1.marketing_icon.setImageResource(2130837715);
        continue;
        label481: localViewHolder1.marketing_icon.setImageResource(2130837715);
      }
      label495: if ((localProductCatalogEntry.hasPrice()) && (localProductCatalogEntry.getPrice().getValue() > 0.0F))
      {
        localViewHolder1.buyBtn.setText(getPriceText(localProductCatalogEntry));
        return localView1;
      }
      localViewHolder1.buyBtn.setText(2131296509);
      return localView1;
    }
  }

  public boolean isBillingSupported()
  {
    return this.m_isBillingSupported;
  }

  public boolean isPurchaseEnabled()
  {
    return this.m_isPurchaseEnabled;
  }

  public void onClick(View paramView)
  {
    Object localObject = paramView.getTag();
    if ((localObject instanceof SessionMessages.ProductCatalogEntry))
    {
      SessionMessages.ProductCatalogEntry localProductCatalogEntry = (SessionMessages.ProductCatalogEntry)localObject;
      notifyItemClicked(paramView.getId(), localProductCatalogEntry);
    }
  }

  public void setBillingSupported(boolean paramBoolean)
  {
    this.m_isBillingSupported = paramBoolean;
    updateUIState();
  }

  public void setOnClickListener(OnClickListener paramOnClickListener)
  {
    this.m_clickListener = paramOnClickListener;
  }

  public void setProductPurchasedFlag(String paramString)
  {
    for (int i = 0; i < getCount(); i++)
    {
      SessionMessages.ProductCatalogEntry localProductCatalogEntry1 = (SessionMessages.ProductCatalogEntry)getItem(i);
      if (localProductCatalogEntry1.getProductMarketId().equals(paramString))
      {
        SessionMessages.ProductCatalogEntry localProductCatalogEntry2 = localProductCatalogEntry1.toBuilder().setPurchased(true).build();
        remove(localProductCatalogEntry1);
        insert(localProductCatalogEntry2, i);
      }
    }
    notifyDataSetChanged();
  }

  public void setProducts(List<SessionMessages.ProductCatalogEntry> paramList)
  {
    if (!paramList.isEmpty())
    {
      setNotifyOnChange(false);
      clear();
      ArrayList localArrayList = new ArrayList(paramList);
      Collections.sort(localArrayList, new Comparator()
      {
        public int compare(SessionMessages.ProductCatalogEntry paramAnonymousProductCatalogEntry1, SessionMessages.ProductCatalogEntry paramAnonymousProductCatalogEntry2)
        {
          return paramAnonymousProductCatalogEntry1.getSortOrder() - paramAnonymousProductCatalogEntry2.getSortOrder();
        }
      });
      Iterator localIterator = localArrayList.iterator();
      while (localIterator.hasNext())
      {
        SessionMessages.ProductCatalogEntry localProductCatalogEntry = (SessionMessages.ProductCatalogEntry)localIterator.next();
        if (localProductCatalogEntry.getCategoryKey().equals("product.category.avatar"))
          add(localProductCatalogEntry);
      }
      updateUIState();
      notifyDataSetChanged();
    }
  }

  public void updateUIState()
  {
    if (!this.m_isBillingSupported)
    {
      setPurchaseEnabled(false);
      return;
    }
    setPurchaseEnabled(true);
  }

  public static abstract interface OnClickListener
  {
    public abstract void onClick(int paramInt, SessionMessages.ProductCatalogEntry paramProductCatalogEntry);
  }

  static class ViewHolder
  {
    Button buyBtn;
    Button demoBtn;
    TextView desc;
    ImageView marketing_icon;
    TextView title;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.AvatarPaymentAdapter
 * JD-Core Version:    0.6.2
 */