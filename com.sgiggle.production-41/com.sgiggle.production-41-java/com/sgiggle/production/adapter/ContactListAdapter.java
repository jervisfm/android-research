package com.sgiggle.production.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.production.Utils.UIContact;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class ContactListAdapter extends BaseAdapter
  implements SectionIndexer
{
  private static final int COMPACT_LIST_LIMIT = 14;
  private static final int VIEW_TYPE_CONTACT = 1;
  private static final int VIEW_TYPE_DIVIDER;
  private HashMap<String, Integer> alphaIndexer = new HashMap();
  private HashMap<Integer, String> indexerPos = new HashMap();
  private ContactStore.ContactOrderPair m_contactOrderPair = ContactStore.ContactOrderPair.getDefault();
  protected final ContactListSelection m_defaultContactListSelection;
  protected LayoutInflater m_inflater;
  private boolean m_isSectionIndexerUpToDate = false;
  protected final List<Utils.UIContact> m_items = new ArrayList();
  private String[] sections;

  public ContactListAdapter(ContactStore.ContactOrderPair paramContactOrderPair, ContactListSelection paramContactListSelection, LayoutInflater paramLayoutInflater)
  {
    this.m_contactOrderPair = paramContactOrderPair;
    this.m_defaultContactListSelection = paramContactListSelection;
    this.m_inflater = paramLayoutInflater;
  }

  private View getDividerView(String paramString)
  {
    if (isCompactMode());
    for (View localView = this.m_inflater.inflate(2130903062, null); ; localView = this.m_inflater.inflate(2130903061, null))
    {
      ((TextView)localView.findViewById(2131361912)).setText(paramString);
      return localView;
    }
  }

  private boolean isCompactMode()
  {
    return this.m_items.size() < 14;
  }

  private void resetGroupIdxs()
  {
    this.m_items.clear();
    this.alphaIndexer.clear();
    this.indexerPos.clear();
  }

  public boolean areAllItemsEnabled()
  {
    return false;
  }

  protected abstract void fillContactView(View paramView, Utils.UIContact paramUIContact);

  public int getContactCount()
  {
    if (this.sections == null)
      return this.m_items.size();
    return this.m_items.size() - this.sections.length;
  }

  protected abstract View getContactView(Utils.UIContact paramUIContact);

  public int getCount()
  {
    return this.m_items.size();
  }

  public Object getItem(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= getCount()))
      return null;
    return this.m_items.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public int getItemViewType(int paramInt)
  {
    if (this.indexerPos.containsKey(Integer.valueOf(paramInt)))
      return 0;
    return 1;
  }

  public int getPositionForSection(int paramInt)
  {
    return ((Integer)this.alphaIndexer.get(this.sections[paramInt])).intValue();
  }

  public int getSectionForPosition(int paramInt)
  {
    return 0;
  }

  public Object[] getSections()
  {
    return this.sections;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (getItemViewType(paramInt) == 0)
    {
      if ((paramView != null) && (paramView.getTag() == null))
      {
        ((TextView)paramView.findViewById(2131361912)).setText((CharSequence)this.indexerPos.get(Integer.valueOf(paramInt)));
        return paramView;
      }
      return getDividerView((String)this.indexerPos.get(Integer.valueOf(paramInt)));
    }
    Utils.UIContact localUIContact = (Utils.UIContact)getItem(paramInt);
    if (paramView == null);
    for (View localView = getContactView(localUIContact); ; localView = paramView)
    {
      fillContactView(localView, localUIContact);
      return localView;
    }
  }

  public int getViewTypeCount()
  {
    return 2;
  }

  public boolean hasStableIds()
  {
    return true;
  }

  public boolean isEnabled(int paramInt)
  {
    return getItemViewType(paramInt) == 1;
  }

  public boolean isSectionIndexerUpToDate()
  {
    return this.m_isSectionIndexerUpToDate;
  }

  public void loadGroups(List<Utils.UIContact> paramList, ContactListSelection paramContactListSelection)
  {
    resetGroupIdxs();
    if (paramContactListSelection == ContactListSelection.UNDEFINED);
    for (ContactListSelection localContactListSelection = this.m_defaultContactListSelection; ; localContactListSelection = paramContactListSelection)
    {
      if (paramList.size() < 14)
      {
        Iterator localIterator = paramList.iterator();
        while (localIterator.hasNext())
        {
          Utils.UIContact localUIContact2 = (Utils.UIContact)localIterator.next();
          if (((localContactListSelection != ContactListSelection.TANGO) || (localUIContact2.m_accountId.length() != 0)) && ((localContactListSelection != ContactListSelection.FAVORITE) || (localUIContact2.m_favorite)))
            this.m_items.add(localUIContact2);
        }
        this.m_isSectionIndexerUpToDate = false;
        return;
      }
      int i = 0;
      if (i < paramList.size())
      {
        Utils.UIContact localUIContact1 = (Utils.UIContact)paramList.get(i);
        if ((localContactListSelection == ContactListSelection.TANGO) && (localUIContact1.m_accountId.length() == 0));
        while ((localContactListSelection == ContactListSelection.FAVORITE) && (!localUIContact1.m_favorite))
        {
          i++;
          break;
        }
        char c = localUIContact1.dispNameFirstChar(this.m_contactOrderPair.getSortOrder());
        if (c < 'A');
        for (String str1 = "#"; ; str1 = "" + c)
        {
          String str2 = str1.toUpperCase();
          if (!this.alphaIndexer.containsKey(str2))
          {
            this.alphaIndexer.put(str2, Integer.valueOf(this.m_items.size()));
            this.indexerPos.put(Integer.valueOf(this.m_items.size()), str2);
            this.m_items.add(Utils.UIContact.getDummyContact());
          }
          this.m_items.add(localUIContact1);
          break;
        }
      }
      ArrayList localArrayList = new ArrayList(this.alphaIndexer.keySet());
      Collections.sort(localArrayList);
      this.sections = new String[localArrayList.size()];
      localArrayList.toArray(this.sections);
      this.m_isSectionIndexerUpToDate = true;
      return;
    }
  }

  public static enum ContactListSelection
  {
    static
    {
      FAVORITE = new ContactListSelection("FAVORITE", 2);
      UNDEFINED = new ContactListSelection("UNDEFINED", 3);
      ContactListSelection[] arrayOfContactListSelection = new ContactListSelection[4];
      arrayOfContactListSelection[0] = ALL;
      arrayOfContactListSelection[1] = TANGO;
      arrayOfContactListSelection[2] = FAVORITE;
      arrayOfContactListSelection[3] = UNDEFINED;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.ContactListAdapter
 * JD-Core Version:    0.6.2
 */