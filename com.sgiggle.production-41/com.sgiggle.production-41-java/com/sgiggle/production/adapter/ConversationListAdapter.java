package com.sgiggle.production.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationSummary;
import com.sgiggle.production.widget.ConversationListItemView;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationSummary;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ConversationListAdapter extends AutoRefreshingArrayAdapter<ConversationSummary>
{
  private static final String TAG = "Tango.ConversationListAdapter";
  private Context m_context;
  private Map<String, ConversationSummary> m_conversationPerId;
  private final Comparator<ConversationSummary> m_mostRecentComparator;

  public ConversationListAdapter(Context paramContext)
  {
    super(paramContext, 17367043);
    this.m_context = paramContext;
    this.m_conversationPerId = new HashMap();
    this.m_mostRecentComparator = new Comparator()
    {
      public int compare(ConversationSummary paramAnonymousConversationSummary1, ConversationSummary paramAnonymousConversationSummary2)
      {
        long l1 = paramAnonymousConversationSummary1.getLastMessage().getTimestampMs();
        long l2 = paramAnonymousConversationSummary2.getLastMessage().getTimestampMs();
        if (l1 > l2)
          return -1;
        if (l1 == l2)
          return 0;
        return 1;
      }
    };
  }

  private int getFirstMessageIdIfUnread()
  {
    if ((getCount() > 0) && (((ConversationSummary)getItem(0)).getUnreadMessageCount() > 0))
      return ((ConversationSummary)getItem(0)).getLastMessage().getMessageId();
    return -1;
  }

  private void sortByMostRecent()
  {
    sort(this.m_mostRecentComparator);
  }

  public void add(ConversationSummary paramConversationSummary)
  {
    super.add(paramConversationSummary);
    this.m_conversationPerId.put(paramConversationSummary.getConversationId(), paramConversationSummary);
  }

  public void addOrUpdate(SessionMessages.ConversationSummary paramConversationSummary, ConversationContact paramConversationContact)
  {
    ConversationSummary localConversationSummary = (ConversationSummary)this.m_conversationPerId.get(paramConversationSummary.getConversationId());
    if (localConversationSummary == null)
      add(new ConversationSummary(paramConversationSummary, paramConversationContact));
    while (true)
    {
      sortByMostRecent();
      notifyDataSetChanged();
      return;
      localConversationSummary.setData(paramConversationSummary, paramConversationContact);
    }
  }

  public void clear()
  {
    super.clear();
    this.m_conversationPerId.clear();
  }

  public int getFirstConversationPosWithUnreadMessages()
  {
    int i = getCount();
    for (int j = 0; j < i; j++)
      if (((ConversationSummary)getItem(j)).getUnreadMessageCount() > 0)
        return j;
    return -1;
  }

  protected long getRefreshIntervalMs()
  {
    return 60000L;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null);
    for (ConversationListItemView localConversationListItemView = new ConversationListItemView(this.m_context); ; localConversationListItemView = (ConversationListItemView)paramView)
    {
      ConversationSummary localConversationSummary = (ConversationSummary)getItem(paramInt);
      localConversationListItemView.fill(this.m_context, localConversationSummary);
      return localConversationListItemView;
    }
  }

  public void remove(ConversationSummary paramConversationSummary)
  {
    super.remove(paramConversationSummary);
    this.m_conversationPerId.remove(paramConversationSummary.getConversationId());
  }

  public void remove(String paramString)
  {
    ConversationSummary localConversationSummary = (ConversationSummary)this.m_conversationPerId.get(paramString);
    if (localConversationSummary == null)
    {
      Log.w("Tango.ConversationListAdapter", "remove(): no conversation with ID " + paramString);
      return;
    }
    Log.d("Tango.ConversationListAdapter", "remove(): removed conversation with ID " + paramString);
    remove(localConversationSummary);
  }

  public boolean setData(List<ConversationSummary> paramList)
  {
    int i = getFirstMessageIdIfUnread();
    setNotifyOnChange(false);
    clear();
    if (paramList != null)
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
        add((ConversationSummary)localIterator.next());
      sortByMostRecent();
    }
    for (int j = getFirstMessageIdIfUnread(); ; j = -1)
    {
      setNotifyOnChange(true);
      notifyDataSetChanged();
      return (j != -1) && (i != j);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.ConversationListAdapter
 * JD-Core Version:    0.6.2
 */