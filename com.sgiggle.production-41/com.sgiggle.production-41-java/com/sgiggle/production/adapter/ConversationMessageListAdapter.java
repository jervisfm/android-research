package com.sgiggle.production.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.widget.MessageListCompoundItemViewPicture;
import com.sgiggle.production.widget.MessageListCompoundItemViewText;
import com.sgiggle.production.widget.MessageListCompoundItemViewVgood;
import com.sgiggle.production.widget.MessageListCompoundItemViewVideomail;
import com.sgiggle.production.widget.MessageListItemView;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import java.util.Iterator;
import java.util.List;

public class ConversationMessageListAdapter extends AutoRefreshingArrayAdapter<ConversationMessage>
{
  private static final String TAG = "Tango.ConversationMessageListAdapter";
  private static final int TYPE_MAX_COUNT = 5;
  private static final int TYPE_PICTURE = 4;
  private static final int TYPE_SIMPLE = 2;
  private static final int TYPE_TEXT = 0;
  private static final int TYPE_VGOOD_ANIM = 3;
  private static final int TYPE_VIDEOMAIL = 1;
  private Context m_context;
  private boolean m_isGroupConversation = false;

  public ConversationMessageListAdapter(Context paramContext)
  {
    super(paramContext, 17367043);
    setNotifyOnChange(true);
    this.m_context = paramContext;
  }

  public void clearData()
  {
    setData(null, false);
  }

  public MessageListItemView createMessageListItemView(SessionMessages.ConversationMessageType paramConversationMessageType)
  {
    switch (1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType[paramConversationMessageType.ordinal()])
    {
    default:
      Log.e("Tango.ConversationMessageListAdapter", "itemViewFactory: unsupported type " + paramConversationMessageType);
      return null;
    case 1:
      return new MessageListCompoundItemViewText(this.m_context);
    case 4:
      return new MessageListCompoundItemViewVgood(this.m_context);
    case 2:
      return new MessageListCompoundItemViewVideomail(this.m_context);
    case 3:
    }
    Log.d("Tango.ConversationMessageListAdapter", "createMessageListItemView IMAGE_MESSAGE");
    return new MessageListCompoundItemViewPicture(this.m_context);
  }

  public int getItemViewType(int paramInt)
  {
    SessionMessages.ConversationMessageType localConversationMessageType = ((ConversationMessage)getItem(paramInt)).getType();
    switch (1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType[localConversationMessageType.ordinal()])
    {
    default:
      Log.e("Tango.ConversationMessageListAdapter", "getItemViewType: unsupported type " + localConversationMessageType);
      return super.getItemViewType(paramInt);
    case 1:
      return 0;
    case 2:
      return 1;
    case 3:
      return 4;
    case 4:
    }
    return 3;
  }

  protected long getRefreshIntervalMs()
  {
    return 60000L;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    ConversationMessage localConversationMessage = (ConversationMessage)getItem(paramInt);
    if (paramView == null);
    for (MessageListItemView localMessageListItemView = createMessageListItemView(localConversationMessage.getType()); ; localMessageListItemView = (MessageListItemView)paramView)
    {
      localMessageListItemView.fill(localConversationMessage, this.m_isGroupConversation);
      return localMessageListItemView;
    }
  }

  public int getViewTypeCount()
  {
    return 5;
  }

  public void setData(List<ConversationMessage> paramList, boolean paramBoolean)
  {
    setNotifyOnChange(false);
    this.m_isGroupConversation = paramBoolean;
    clear();
    if (paramList != null)
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
        add((ConversationMessage)localIterator.next());
    }
    setNotifyOnChange(true);
    notifyDataSetChanged();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.ConversationMessageListAdapter
 * JD-Core Version:    0.6.2
 */