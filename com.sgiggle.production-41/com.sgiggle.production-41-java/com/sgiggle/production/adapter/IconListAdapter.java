package com.sgiggle.production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class IconListAdapter extends ArrayAdapter<IconListItem>
{
  private static final int mResource = 2130903083;
  protected LayoutInflater mInflater;

  public IconListAdapter(Context paramContext, List<IconListItem> paramList)
  {
    super(paramContext, 2130903083, paramList);
    this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null);
    for (View localView = this.mInflater.inflate(2130903083, paramViewGroup, false); ; localView = paramView)
    {
      ((TextView)localView.findViewById(2131361965)).setText(((IconListItem)getItem(paramInt)).getTitle());
      ((ImageView)localView.findViewById(2131361840)).setImageResource(((IconListItem)getItem(paramInt)).getResource());
      return localView;
    }
  }

  public static class IconListItem
  {
    private final int mResource;
    private final String mTitle;

    public IconListItem(String paramString, int paramInt)
    {
      this.mResource = paramInt;
      this.mTitle = paramString;
    }

    public int getResource()
    {
      return this.mResource;
    }

    public String getTitle()
    {
      return this.mTitle;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.IconListAdapter
 * JD-Core Version:    0.6.2
 */