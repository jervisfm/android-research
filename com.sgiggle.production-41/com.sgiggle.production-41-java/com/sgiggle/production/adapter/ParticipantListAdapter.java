package com.sgiggle.production.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.util.ContactThumbnailLoader;
import com.sgiggle.xmpp.SessionMessages.Contact;
import java.util.Iterator;
import java.util.List;

public class ParticipantListAdapter extends ArrayAdapter<ConversationContact>
{
  private LayoutInflater m_inflater;

  public ParticipantListAdapter(Context paramContext)
  {
    super(paramContext, 17367043);
    this.m_inflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
  }

  private void addData(List<ConversationContact> paramList, boolean paramBoolean)
  {
    if (paramBoolean)
      clear();
    if (paramList != null)
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
        add((ConversationContact)localIterator.next());
    }
  }

  public void addData(List<ConversationContact> paramList)
  {
    addData(paramList, false);
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    ConversationContact localConversationContact = (ConversationContact)getItem(paramInt);
    View localView;
    ImageView localImageView;
    Bitmap localBitmap;
    if (paramView == null)
    {
      localView = this.m_inflater.inflate(2130903097, null);
      localImageView = (ImageView)localView.findViewById(2131362015);
      if ((Long.valueOf(localConversationContact.getContact().getDeviceContactId()).longValue() == -1L) || (!localConversationContact.shouldHaveImage()))
        break label163;
      localBitmap = ContactThumbnailLoader.getInstance().getLoadedBitmap(localConversationContact);
      if (localBitmap == null)
        ContactThumbnailLoader.getInstance().addLoadTask(localConversationContact, localImageView);
    }
    while (true)
    {
      if (localBitmap != null)
      {
        localImageView.setImageBitmap(localBitmap);
        label104: if (paramInt != 0)
          break label153;
      }
      label153: for (String str = getContext().getString(2131296558); ; str = localConversationContact.getDisplayNameShort())
      {
        ((TextView)localView.findViewById(2131362016)).setText(str);
        return localView;
        localView = paramView;
        break;
        localImageView.setImageResource(2130837648);
        break label104;
      }
      label163: localBitmap = null;
    }
  }

  public void setData(List<ConversationContact> paramList)
  {
    addData(paramList, true);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.ParticipantListAdapter
 * JD-Core Version:    0.6.2
 */