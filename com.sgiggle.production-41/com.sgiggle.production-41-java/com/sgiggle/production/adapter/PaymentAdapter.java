package com.sgiggle.production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.sgiggle.production.model.Product;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.util.Log;
import java.util.Iterator;
import java.util.List;

public class PaymentAdapter extends ArrayAdapter<Product>
{
  private static final String TAG = "Tango.PaymentAdapter";
  private static final boolean TRIAL_SECTION_SUPPORTED = false;
  private static final int VIEW_TYPE_COUNT = 3;
  private static final int VIEW_TYPE_HEADER_SUBSCRIPTIONS = 2;
  private static final int VIEW_TYPE_HEADER_TRIAL = 1;
  private static final int VIEW_TYPE_NORMAL;
  private List<Product> m_data;
  private boolean m_enableAllItems = true;
  private LayoutInflater m_inflater;
  private PurchaseListener m_listener;
  private final PurchaseOnClickListener m_purchaseClickListener = new PurchaseOnClickListener();

  public PaymentAdapter(Context paramContext, List<Product> paramList, PurchaseListener paramPurchaseListener)
  {
    super(paramContext, 0, paramList);
    this.m_data = paramList;
    this.m_inflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    this.m_listener = paramPurchaseListener;
  }

  public boolean areAllItemsEnabled()
  {
    return false;
  }

  public void enableAllItems(boolean paramBoolean)
  {
    Log.d("Tango.PaymentAdapter", "Enable all items:" + paramBoolean);
    this.m_enableAllItems = paramBoolean;
    notifyDataSetChanged();
  }

  public int getCount()
  {
    if ((this.m_data != null) && (this.m_data.size() > 0))
      return 0 + this.m_data.size();
    return 1;
  }

  public Product getItem(int paramInt)
  {
    if ((this.m_data != null) && (this.m_data.size() > 0))
      return (Product)this.m_data.get(paramInt);
    return null;
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public int getItemViewType(int paramInt)
  {
    return 0;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView;
    ViewHolder localViewHolder1;
    if (paramView == null)
    {
      localView = this.m_inflater.inflate(2130903150, null);
      ViewHolder localViewHolder2 = new ViewHolder();
      localViewHolder2.m_paymentDescription = ((TextView)localView.findViewById(2131362172));
      localViewHolder2.m_paymentPlan = ((TextView)localView.findViewById(2131362171));
      localViewHolder2.m_error = ((TextView)localView.findViewById(2131361844));
      localViewHolder2.m_purchase = ((Button)localView.findViewById(2131362173));
      localViewHolder2.m_purchase.setOnClickListener(this.m_purchaseClickListener);
      localView.setTag(localViewHolder2);
      localViewHolder1 = localViewHolder2;
    }
    Product localProduct;
    while (true)
    {
      localProduct = getItem(paramInt);
      if (localProduct != null)
        break;
      if (paramInt == 0)
      {
        localViewHolder1.m_paymentDescription.setVisibility(8);
        localViewHolder1.m_paymentPlan.setVisibility(8);
        localViewHolder1.m_purchase.setVisibility(8);
        localViewHolder1.m_error.setVisibility(0);
        localViewHolder1.m_error.setText(2131296503);
      }
      return localView;
      localViewHolder1 = (ViewHolder)paramView.getTag();
      localView = paramView;
    }
    localViewHolder1.m_paymentDescription.setText(localProduct.getProductDescription());
    localViewHolder1.m_paymentPlan.setText(localProduct.getProductName());
    localViewHolder1.m_purchase.setTag(localProduct);
    localViewHolder1.m_paymentDescription.setVisibility(0);
    localViewHolder1.m_paymentPlan.setVisibility(0);
    localViewHolder1.m_purchase.setVisibility(0);
    localViewHolder1.m_error.setVisibility(8);
    if ((localProduct.getPurchaseState() == Constants.PurchaseState.PURCHASE) || (localProduct.getPurchaseState() == Constants.PurchaseState.CANCELED) || (localProduct.getPurchaseState() == Constants.PurchaseState.REFUNDED))
    {
      localViewHolder1.m_purchase.setEnabled(this.m_enableAllItems);
      if (localProduct.getPrice() != null)
        localViewHolder1.m_purchase.setText(localProduct.getFormattedPrice());
    }
    while (true)
    {
      return localView;
      localViewHolder1.m_purchase.setText("");
      continue;
      if (localProduct.getPurchaseState() == Constants.PurchaseState.PENDING)
      {
        localViewHolder1.m_purchase.setEnabled(false);
        localViewHolder1.m_purchase.setText("Pending");
      }
      else if (localProduct.getPurchaseState() == Constants.PurchaseState.PURCHASED)
      {
        localViewHolder1.m_purchase.setEnabled(false);
        localViewHolder1.m_purchase.setText("Purchased");
      }
    }
  }

  public int getViewTypeCount()
  {
    return 1;
  }

  public boolean isEnabled(int paramInt)
  {
    return (paramInt != 0) || (getItem(paramInt) != null);
  }

  public Product updateItemStatus(String paramString, Constants.PurchaseState paramPurchaseState)
  {
    if (this.m_data != null)
    {
      Iterator localIterator = this.m_data.iterator();
      while (localIterator.hasNext())
      {
        Product localProduct = (Product)localIterator.next();
        if (localProduct.getProductMarketId().equals(paramString))
        {
          localProduct.setPurchaseState(paramPurchaseState);
          notifyDataSetChanged();
          return localProduct;
        }
      }
    }
    return null;
  }

  public static abstract interface PurchaseListener
  {
    public abstract void onPurchase(Product paramProduct);
  }

  class PurchaseOnClickListener
    implements View.OnClickListener
  {
    PurchaseOnClickListener()
    {
    }

    public void onClick(View paramView)
    {
      Object localObject = paramView.getTag();
      Product localProduct = null;
      if (localObject != null)
      {
        boolean bool = paramView.getTag() instanceof Product;
        localProduct = null;
        if (bool)
          localProduct = (Product)paramView.getTag();
      }
      if ((localProduct != null) && (PaymentAdapter.this.m_listener != null))
        PaymentAdapter.this.m_listener.onPurchase(localProduct);
    }
  }

  static class ViewHolder
  {
    TextView m_error;
    TextView m_paymentDescription;
    TextView m_paymentPlan;
    Button m_purchase;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.PaymentAdapter
 * JD-Core Version:    0.6.2
 */