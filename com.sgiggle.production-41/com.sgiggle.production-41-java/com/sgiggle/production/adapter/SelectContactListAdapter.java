package com.sgiggle.production.adapter;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.production.Utils;
import com.sgiggle.production.Utils.UIContact;
import com.sgiggle.production.fragment.SelectContactFragment.SelectContactListener;
import com.sgiggle.production.util.ContactThumbnailLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SelectContactListAdapter extends ContactListAdapter
{
  private List<Utils.UIContact> m_allContacts;
  private ContactListFilter m_filter;
  private final Object m_filterResultLock = new Object();
  private SelectContactFragment.SelectContactListener m_listener;
  private boolean m_showCheckbox = true;

  public SelectContactListAdapter(ContactListAdapter.ContactListSelection paramContactListSelection, LayoutInflater paramLayoutInflater, ContactStore.ContactOrderPair paramContactOrderPair, SelectContactFragment.SelectContactListener paramSelectContactListener)
  {
    super(paramContactOrderPair, paramContactListSelection, paramLayoutInflater);
    this.m_listener = paramSelectContactListener;
  }

  protected void fillContactView(View paramView, Utils.UIContact paramUIContact)
  {
    ViewHolder localViewHolder = (ViewHolder)paramView.getTag();
    localViewHolder.m_name.setText(paramUIContact.displayName());
    localViewHolder.m_thumbnail.setTag(Integer.valueOf(paramUIContact.hashCode()));
    Bitmap localBitmap;
    if ((paramUIContact.m_deviceContactId != -1L) && (paramUIContact.hasPhoto))
    {
      localBitmap = ContactThumbnailLoader.getInstance().getLoadedBitmap(paramUIContact);
      if (localBitmap == null)
        ContactThumbnailLoader.getInstance().addLoadTask(paramUIContact, localViewHolder.m_thumbnail);
    }
    while (true)
    {
      if (localBitmap != null)
        localViewHolder.m_thumbnail.setImageBitmap(localBitmap);
      while (true)
      {
        localViewHolder.m_checkbox.setOnCheckedChangeListener(null);
        localViewHolder.m_checkbox.setChecked(paramUIContact.m_selected);
        localViewHolder.m_checkbox.setTag(paramUIContact);
        localViewHolder.m_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
          public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
          {
            Utils.UIContact localUIContact = (Utils.UIContact)paramAnonymousCompoundButton.getTag();
            if ((localUIContact != null) && (localUIContact.m_selected != paramAnonymousBoolean))
            {
              if (SelectContactListAdapter.this.m_listener.trySelectContacts(1, paramAnonymousBoolean))
              {
                localUIContact.m_selected = paramAnonymousBoolean;
                SelectContactListAdapter.this.m_listener.onContactSelectionChanged(localUIContact, paramAnonymousBoolean);
              }
            }
            else
              return;
            paramAnonymousCompoundButton.setOnCheckedChangeListener(null);
            paramAnonymousCompoundButton.setChecked(false);
            paramAnonymousCompoundButton.setOnCheckedChangeListener(this);
          }
        });
        if (!this.m_showCheckbox)
          break;
        localViewHolder.m_checkbox.setVisibility(0);
        return;
        localViewHolder.m_thumbnail.setImageResource(2130837648);
      }
      localViewHolder.m_checkbox.setVisibility(8);
      return;
      localBitmap = null;
    }
  }

  protected View getContactView(Utils.UIContact paramUIContact)
  {
    View localView = this.m_inflater.inflate(2130903111, null);
    ViewHolder localViewHolder = new ViewHolder();
    localViewHolder.m_thumbnail = ((ImageView)localView.findViewById(2131362080));
    localViewHolder.m_name = ((TextView)localView.findViewById(2131362081));
    localViewHolder.m_checkbox = ((CheckBox)localView.findViewById(2131362083));
    localView.setTag(localViewHolder);
    return localView;
  }

  public Filter getFilter(ContactListFilterListener paramContactListFilterListener)
  {
    if (this.m_filter == null)
      this.m_filter = new ContactListFilter(paramContactListFilterListener);
    return this.m_filter;
  }

  public void onItemClicked(View paramView, int paramInt, long paramLong)
  {
    CheckBox localCheckBox = (CheckBox)paramView.findViewById(2131362083);
    if (localCheckBox != null)
      if (localCheckBox.isChecked())
        break label35;
    label35: for (boolean bool = true; ; bool = false)
    {
      localCheckBox.setChecked(bool);
      return;
    }
  }

  public void setContacts(List<Utils.UIContact> paramList)
  {
    this.m_allContacts = paramList;
    loadGroups(this.m_allContacts, this.m_defaultContactListSelection);
  }

  public void setShowCheckbox(boolean paramBoolean)
  {
    if (this.m_showCheckbox != paramBoolean)
    {
      this.m_showCheckbox = paramBoolean;
      notifyDataSetChanged();
    }
  }

  private class ContactListFilter extends Filter
  {
    private SelectContactListAdapter.ContactListFilterListener m_listener;

    public ContactListFilter(SelectContactListAdapter.ContactListFilterListener arg2)
    {
      Object localObject;
      this.m_listener = localObject;
    }

    protected Filter.FilterResults performFiltering(CharSequence paramCharSequence)
    {
      Filter.FilterResults localFilterResults = new Filter.FilterResults();
      String str = paramCharSequence.toString().trim().toUpperCase();
      if ((paramCharSequence == null) || (paramCharSequence.length() == 0))
        synchronized (SelectContactListAdapter.this.m_filterResultLock)
        {
          localFilterResults.values = SelectContactListAdapter.this.m_allContacts;
          localFilterResults.count = SelectContactListAdapter.this.m_allContacts.size();
          return localFilterResults;
        }
      ArrayList localArrayList;
      synchronized (SelectContactListAdapter.this.m_filterResultLock)
      {
        localArrayList = new ArrayList();
        Iterator localIterator = SelectContactListAdapter.this.m_allContacts.iterator();
        while (localIterator.hasNext())
        {
          Utils.UIContact localUIContact = (Utils.UIContact)localIterator.next();
          if ((Utils.startWithUpperCase(localUIContact.m_firstName, str)) || (Utils.startWithUpperCase(localUIContact.m_lastName, str)) || (Utils.startWithUpperCase(localUIContact.displayName(), str)))
            localArrayList.add(localUIContact);
        }
      }
      localFilterResults.values = localArrayList;
      localFilterResults.count = localArrayList.size();
      return localFilterResults;
    }

    protected void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
    {
      synchronized (SelectContactListAdapter.this.m_filterResultLock)
      {
        ArrayList localArrayList = (ArrayList)paramFilterResults.values;
        SelectContactListAdapter.this.loadGroups(localArrayList, SelectContactListAdapter.this.m_defaultContactListSelection);
        SelectContactListAdapter.this.notifyDataSetChanged();
        if (this.m_listener != null)
          this.m_listener.onFilterDone(paramCharSequence);
        return;
      }
    }
  }

  public static abstract interface ContactListFilterListener
  {
    public abstract void onFilterDone(CharSequence paramCharSequence);
  }

  class ViewHolder
  {
    CheckBox m_checkbox;
    TextView m_name;
    ImageView m_thumbnail;

    ViewHolder()
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.SelectContactListAdapter
 * JD-Core Version:    0.6.2
 */