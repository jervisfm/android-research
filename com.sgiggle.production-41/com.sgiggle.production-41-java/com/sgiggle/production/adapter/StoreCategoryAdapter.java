package com.sgiggle.production.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class StoreCategoryAdapter extends BaseAdapter
{
  private Category[] m_categories = new Category[3];
  private int m_colorDark;
  private int m_colorLight;
  private LayoutInflater m_inflater;

  public StoreCategoryAdapter(Context paramContext)
  {
    this.m_categories[0] = new Category(0, 2131296672, 2131296673, 2130837678);
    this.m_categories[1] = new Category(1, 2131296674, 2131296675, 2130837677);
    this.m_categories[2] = new Category(2, 2131296678, 2131296679, 2130837676);
    this.m_inflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    this.m_colorLight = paramContext.getResources().getColor(2131165218);
    this.m_colorDark = paramContext.getResources().getColor(2131165217);
  }

  private int getColorForPosition(int paramInt)
  {
    if (paramInt % 2 == 0)
      return this.m_colorLight;
    return this.m_colorDark;
  }

  public int getCount()
  {
    return this.m_categories.length;
  }

  public Object getItem(int paramInt)
  {
    return this.m_categories[paramInt];
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null);
    for (View localView1 = this.m_inflater.inflate(2130903123, null); ; localView1 = paramView)
    {
      Category localCategory = (Category)getItem(paramInt);
      ((TextView)localView1.findViewById(2131361841)).setText(localCategory.m_resTitle);
      ((TextView)localView1.findViewById(2131361842)).setText(localCategory.m_resDesc);
      ((ImageView)localView1.findViewById(2131361840)).setImageResource(localCategory.m_resIcon);
      View localView2 = localView1.findViewById(2131362120);
      if (localCategory.m_isNewBadgeVisible);
      for (int i = 0; ; i = 8)
      {
        localView2.setVisibility(i);
        localView1.setBackgroundColor(getColorForPosition(paramInt));
        return localView1;
      }
    }
  }

  public void setNewBadge(int paramInt, boolean paramBoolean)
  {
    this.m_categories[paramInt].setNewBadge(paramBoolean);
    notifyDataSetChanged();
  }

  public static class Category
  {
    public static final int AVATARS = 2;
    public static final int GAMES = 1;
    public static final int SURPRISES;
    boolean m_isNewBadgeVisible;
    int m_resDesc;
    int m_resIcon;
    int m_resTitle;
    int m_type;

    public Category(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this.m_type = paramInt1;
      this.m_resTitle = paramInt2;
      this.m_resDesc = paramInt3;
      this.m_resIcon = paramInt4;
      this.m_isNewBadgeVisible = false;
    }

    public int getType()
    {
      return this.m_type;
    }

    public void setNewBadge(boolean paramBoolean)
    {
      this.m_isNewBadgeVisible = paramBoolean;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.StoreCategoryAdapter
 * JD-Core Version:    0.6.2
 */