package com.sgiggle.production.adapter;

import android.content.Context;
import com.sgiggle.production.TangoApp;
import java.util.ArrayList;
import java.util.List;

public class TcMediaSelectorAdapter extends IconListAdapter
{
  public static final int MAKE_AUDIO_CALL = 4;
  public static final int MAKE_VIDEO_CALL = 3;
  public static final int PICK_PHOTO = 1;
  public static final int TAKE_PHOTO = 0;
  public static final int TAKE_VIDEO = 2;

  public TcMediaSelectorAdapter(Context paramContext, boolean paramBoolean)
  {
    super(paramContext, getData(paramContext, paramBoolean));
  }

  protected static void addItem(List<IconListAdapter.IconListItem> paramList, String paramString, int paramInt1, int paramInt2)
  {
    paramList.add(new MediaListItem(paramString, paramInt1, paramInt2));
  }

  protected static List<IconListAdapter.IconListItem> getData(Context paramContext, boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList(4);
    addItem(localArrayList, paramContext.getString(2131296654), 2130837698, 0);
    addItem(localArrayList, paramContext.getString(2131296655), 2130837697, 1);
    if (TangoApp.videomailSupported())
      addItem(localArrayList, paramContext.getString(2131296656), 2130837699, 2);
    if (paramBoolean)
    {
      addItem(localArrayList, paramContext.getString(2131296657), 2130837696, 3);
      addItem(localArrayList, paramContext.getString(2131296658), 2130837695, 4);
    }
    return localArrayList;
  }

  public int buttonToCommand(int paramInt)
  {
    return ((MediaListItem)getItem(paramInt)).getCommand();
  }

  public static class MediaListItem extends IconListAdapter.IconListItem
  {
    private int mCommand;

    public MediaListItem(String paramString, int paramInt1, int paramInt2)
    {
      super(paramInt1);
      this.mCommand = paramInt2;
    }

    public int getCommand()
    {
      return this.mCommand;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.TcMediaSelectorAdapter
 * JD-Core Version:    0.6.2
 */