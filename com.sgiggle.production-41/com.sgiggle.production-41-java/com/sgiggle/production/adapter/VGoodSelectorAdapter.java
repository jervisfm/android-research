package com.sgiggle.production.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.sgiggle.xmpp.SessionMessages.VGoodBundle;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic.Type;
import com.sgiggle.xmpp.SessionMessages.VGoodSelectorImage;
import com.sgiggle.xmpp.SessionMessages.VGoodSelectorImage.ImageState;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class VGoodSelectorAdapter extends BaseAdapter
{
  private static final String TAG = "VGoodSelectorAdapter";
  public static final int[] filterImageResId = { 2130837653, 2130837652, 2130837654, 2130837651 };
  Drawable mEmptySlotDrawable;
  private Drawable mHighlightBackground;
  private boolean mIsReversed = false;
  private boolean mIsRotated = false;
  private boolean mShowEmptySlots = false;
  List<Boolean> m_buttonEnabled = new ArrayList();
  HashMap<Integer, Drawable> m_cache = new HashMap();
  Context m_context;
  List<SessionMessages.VGoodBundle> m_data = new ArrayList();
  int m_emptyCount;
  private boolean m_enabled = true;
  private int m_highlighted;
  private int m_highlightedWithBackground = -1;
  LayoutInflater m_inflater;

  public VGoodSelectorAdapter(Context paramContext)
  {
    this.m_context = paramContext;
    this.m_inflater = ((LayoutInflater)this.m_context.getSystemService("layout_inflater"));
  }

  private int getDataCount()
  {
    if (this.m_data != null)
      return this.m_data.size();
    return 0;
  }

  private Drawable getDrawable(String paramString)
  {
    if (new File(paramString).isDirectory())
      return null;
    Bitmap localBitmap = BitmapFactory.decodeFile(paramString);
    if ((localBitmap != null) && (this.mIsRotated))
      localBitmap = rotateBitmap(localBitmap);
    if (localBitmap == null)
      return null;
    return new BitmapDrawable(this.m_context.getResources(), localBitmap);
  }

  private Drawable getDrawableForPosition(int paramInt)
  {
    Drawable localDrawable = (Drawable)this.m_cache.get(Integer.valueOf(paramInt));
    if (localDrawable == null)
      if (!isEmptySlot(paramInt))
        break label47;
    label47: for (localDrawable = getEmptySlotDrawable(); ; localDrawable = getDrawable(((SessionMessages.VGoodBundle)this.m_data.get(paramInt)).getImageList()))
    {
      this.m_cache.put(Integer.valueOf(paramInt), localDrawable);
      return localDrawable;
    }
  }

  private Drawable getEmptySlotDrawable()
  {
    if (this.mEmptySlotDrawable == null)
    {
      Resources localResources = this.m_context.getResources();
      Bitmap localBitmap = BitmapFactory.decodeResource(localResources, 2130837775);
      if ((localBitmap != null) && (this.mIsRotated))
        localBitmap = rotateBitmap(localBitmap);
      this.mEmptySlotDrawable = new BitmapDrawable(localResources, localBitmap);
    }
    return this.mEmptySlotDrawable;
  }

  private Drawable getHighlightBackgroundDrawable()
  {
    if (this.mHighlightBackground == null)
    {
      Resources localResources = this.m_context.getResources();
      Bitmap localBitmap = BitmapFactory.decodeResource(localResources, 2130837517);
      if ((localBitmap != null) && (this.mIsRotated))
        localBitmap = rotateBitmap(localBitmap);
      this.mHighlightBackground = new BitmapDrawable(localResources, localBitmap);
    }
    return this.mHighlightBackground;
  }

  private int[] getStateSet(SessionMessages.VGoodSelectorImage.ImageState paramImageState)
  {
    int[] arrayOfInt = new int[0];
    switch (1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$VGoodSelectorImage$ImageState[paramImageState.ordinal()])
    {
    default:
      return arrayOfInt;
    case 1:
      return new int[] { -16842910 };
    case 2:
      return new int[] { 16842910, -16842919 };
    case 3:
      return new int[] { 16842910, 16842919 };
    case 4:
    }
    return new int[] { 16842912 };
  }

  private boolean isEmptySlot(int paramInt)
  {
    return paramInt >= getDataCount();
  }

  private Bitmap rotateBitmap(Bitmap paramBitmap)
  {
    Bitmap.Config localConfig = paramBitmap.getConfig();
    if (localConfig == null)
      localConfig = Bitmap.Config.ARGB_8888;
    int i = paramBitmap.getHeight();
    int j = paramBitmap.getWidth();
    Bitmap localBitmap = Bitmap.createBitmap(i, j, localConfig);
    localBitmap.setDensity(paramBitmap.getDensity());
    Canvas localCanvas = new Canvas(localBitmap);
    Matrix localMatrix = new Matrix();
    localMatrix.setTranslate(-j, 0.0F);
    localMatrix.postRotate(-90.0F);
    localCanvas.drawBitmap(paramBitmap, localMatrix, null);
    return localBitmap;
  }

  public void clearHighlight()
  {
    this.m_highlighted = -1;
    notifyDataSetChanged();
  }

  public void clearHightlightWithBackground()
  {
    this.m_highlightedWithBackground = -1;
    notifyDataSetChanged();
  }

  public void enableItem(int paramInt, boolean paramBoolean)
  {
    int i = getPos(paramInt);
    if (!isEmptySlot(i))
    {
      this.m_buttonEnabled.set(i, Boolean.valueOf(paramBoolean));
      this.m_cache.clear();
    }
  }

  public int getCount()
  {
    if (this.mShowEmptySlots)
      return getDataCount() + this.m_emptyCount;
    return getDataCount();
  }

  public Drawable getDrawable(List<SessionMessages.VGoodSelectorImage> paramList)
  {
    StateListDrawable localStateListDrawable = new StateListDrawable();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      SessionMessages.VGoodSelectorImage localVGoodSelectorImage = (SessionMessages.VGoodSelectorImage)localIterator.next();
      Drawable localDrawable = getDrawable(localVGoodSelectorImage.getPath());
      SessionMessages.VGoodSelectorImage.ImageState localImageState = localVGoodSelectorImage.getImageType();
      if (localDrawable != null)
        localStateListDrawable.addState(getStateSet(localImageState), localDrawable);
    }
    return localStateListDrawable;
  }

  public Object getItem(int paramInt)
  {
    int i = getPos(paramInt);
    if (!isEmptySlot(i))
      return (SessionMessages.VGoodBundle)this.m_data.get(i);
    return null;
  }

  public long getItemId(int paramInt)
  {
    return getPos(paramInt);
  }

  public int getPos(int paramInt)
  {
    if (this.mIsReversed)
      return getCount() - paramInt - 1;
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    int i = getPos(paramInt);
    View localView1;
    ImageView localImageView1;
    label111: int j;
    label123: int k;
    label135: boolean bool1;
    if (paramView == null)
    {
      View localView2 = this.m_inflater.inflate(2130903132, paramViewGroup, false);
      ImageView localImageView2 = (ImageView)localView2.findViewById(2131361796);
      localView1 = localView2;
      localImageView1 = localImageView2;
      if ((isEmptySlot(i)) || (((SessionMessages.VGoodBundle)this.m_data.get(i)).getCinematic().getType() != SessionMessages.VGoodCinematic.Type.FILTER))
        break label253;
      localImageView1.setImageResource(filterImageResId[((int)((SessionMessages.VGoodBundle)this.m_data.get(i)).getCinematic().getAssetId())]);
      if (i != this.m_highlighted)
        break label267;
      j = 1;
      if (i != this.m_highlightedWithBackground)
        break label273;
      k = 1;
      if ((j == 0) && (k == 0))
        break label279;
      bool1 = true;
      label148: localImageView1.setPressed(bool1);
      if (isEmptySlot(i))
        break label300;
    }
    label267: label273: label279: label291: label300: for (boolean bool2 = ((Boolean)this.m_buttonEnabled.get(i)).booleanValue(); ; bool2 = false)
    {
      boolean bool3;
      if (((this.m_enabled) && (bool2)) || (j != 0))
      {
        bool3 = true;
        label203: localImageView1.setEnabled(bool3);
        if (k == 0)
          break label291;
        localImageView1.setBackgroundDrawable(getHighlightBackgroundDrawable());
      }
      while (true)
      {
        localImageView1.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        return localView1;
        localImageView1 = (ImageView)paramView.findViewById(2131361796);
        localView1 = paramView;
        break;
        label253: localImageView1.setImageDrawable(getDrawableForPosition(i));
        break label111;
        j = 0;
        break label123;
        k = 0;
        break label135;
        bool1 = false;
        break label148;
        bool3 = false;
        break label203;
        localImageView1.setBackgroundDrawable(null);
      }
    }
  }

  public boolean isEnabled()
  {
    return this.m_enabled;
  }

  public boolean isItemEnabled(int paramInt)
  {
    int i = getPos(paramInt);
    if (!isEmptySlot(i))
      return ((Boolean)this.m_buttonEnabled.get(i)).booleanValue();
    return true;
  }

  public void setData(List<SessionMessages.VGoodBundle> paramList, boolean paramBoolean)
  {
    this.m_data.clear();
    this.m_cache.clear();
    this.m_highlighted = -1;
    this.mShowEmptySlots = true;
    if (paramList != null)
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        SessionMessages.VGoodBundle localVGoodBundle = (SessionMessages.VGoodBundle)localIterator.next();
        if ((localVGoodBundle.getImageCount() > 0) && ((paramBoolean) || (localVGoodBundle.getCinematic().getType() != SessionMessages.VGoodCinematic.Type.FILTER)))
        {
          this.m_data.add(localVGoodBundle);
          this.m_buttonEnabled.add(Boolean.valueOf(true));
        }
      }
    }
    setEnabled(true);
  }

  public void setEmptyCount(int paramInt)
  {
    this.m_emptyCount = Math.max(0, paramInt);
    notifyDataSetChanged();
  }

  public void setEnabled(boolean paramBoolean)
  {
    this.m_enabled = paramBoolean;
    notifyDataSetChanged();
  }

  public void setHighlight(int paramInt)
  {
    this.m_highlighted = paramInt;
    notifyDataSetChanged();
  }

  public void setHightlightWithBackground(int paramInt)
  {
    this.m_highlightedWithBackground = paramInt;
    notifyDataSetChanged();
  }

  public void setReversed(boolean paramBoolean)
  {
    this.mIsReversed = paramBoolean;
    notifyDataSetChanged();
  }

  public void setRotated(boolean paramBoolean)
  {
    this.mIsRotated = paramBoolean;
    notifyDataSetChanged();
  }

  public void setShowEmptySlots(boolean paramBoolean)
  {
    this.mShowEmptySlots = paramBoolean;
    notifyDataSetChanged();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.adapter.VGoodSelectorAdapter
 * JD-Core Version:    0.6.2
 */