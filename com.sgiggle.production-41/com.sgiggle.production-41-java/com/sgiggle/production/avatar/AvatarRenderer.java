package com.sgiggle.production.avatar;

import android.graphics.Rect;
import com.sgiggle.VideoCapture.VideoView;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.cafe.vgood.CafeRenderer.CafeViewRenderer;
import com.sgiggle.media_engine.MediaEngineMessage.AvatarRenderRequestEvent;
import com.sgiggle.production.CallSession;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload;
import com.sgiggle.xmpp.SessionMessages.AvatarControlPayload.Direction;
import com.sgiggle.xmpp.SessionMessages.AvatarInfo;
import com.sgiggle.xmpp.SessionMessages.AvatarRenderRequestPayload;
import com.sgiggle.xmpp.SessionMessages.MediaSessionPayload.Direction;

public class AvatarRenderer
  implements CafeRenderer.CafeViewRenderer
{
  private static final String TAG = "AVATAR_RENDERER";
  private boolean mAutoAdjustRotation = true;
  private SessionMessages.AvatarControlPayload mAvatarControlPayload = null;
  private CallSession mCallSession = null;
  private String mCurrentClip;
  private boolean mGLRendererMode = false;
  private boolean mIsCafeReady;
  private boolean mIsLocal;
  private volatile int mLastSurpriseId = -1;
  private boolean mRequireClear = true;
  private boolean mRequireUpdate = true;
  private boolean mRotate = true;
  private Rect mSurfaceRect = null;
  private int mViewId;

  public AvatarRenderer(int paramInt, boolean paramBoolean, CallSession paramCallSession)
  {
    this(paramInt, paramBoolean, paramCallSession, false);
  }

  public AvatarRenderer(int paramInt, boolean paramBoolean1, CallSession paramCallSession, boolean paramBoolean2)
  {
    this.mViewId = paramInt;
    this.mIsLocal = paramBoolean1;
    this.mCallSession = paramCallSession;
    this.mGLRendererMode = paramBoolean2;
    this.mIsCafeReady = false;
  }

  private boolean hasValidAvatarInfo()
  {
    if (this.mAvatarControlPayload == null)
      return false;
    SessionMessages.AvatarInfo localAvatarInfo = getAvatarInfo();
    return (localAvatarInfo != null) && (localAvatarInfo.getAnimation().length() > 0);
  }

  private boolean isCafeReady()
  {
    if (!this.mGLRendererMode)
      return true;
    return this.mIsCafeReady;
  }

  private boolean isRendererAvailable()
  {
    if (this.mGLRendererMode)
      return isCafeReady();
    return isSurfaceReady();
  }

  private boolean isSurfaceReady()
  {
    if (this.mGLRendererMode)
      return true;
    return (this.mSurfaceRect != null) && (this.mSurfaceRect.width() > 0) && (this.mSurfaceRect.height() > 0);
  }

  private boolean needRotate()
  {
    return ((this.mAutoAdjustRotation) && (this.mSurfaceRect.width() > this.mSurfaceRect.height())) || (this.mRotate);
  }

  private boolean needStartAvatar()
  {
    return (isAvatarActived()) && (isRendererAvailable()) && (hasValidAvatarInfo()) && (this.mLastSurpriseId <= 0);
  }

  private Rect surfaceToCafe(Rect paramRect)
  {
    return new Rect(paramRect.left, this.mSurfaceRect.height() - paramRect.top - paramRect.height(), paramRect.right, this.mSurfaceRect.height() - paramRect.top);
  }

  public SessionMessages.AvatarInfo getAvatarInfo()
  {
    if (this.mAvatarControlPayload == null)
      return null;
    if (this.mIsLocal)
      return this.mAvatarControlPayload.getLocalAvatarInfo();
    return this.mAvatarControlPayload.getRemoteAvatarInfo();
  }

  public void handleRenderRequest(MediaEngineMessage.AvatarRenderRequestEvent paramAvatarRenderRequestEvent)
  {
    if (paramAvatarRenderRequestEvent == null);
    String str2;
    do
    {
      return;
      if (this.mLastSurpriseId < 0)
      {
        Log.d("AVATAR_RENDERER", "ignore render request while surprise not started " + this);
        return;
      }
      String str1 = ((SessionMessages.AvatarRenderRequestPayload)paramAvatarRenderRequestEvent.payload()).getTrack();
      str2 = ((SessionMessages.AvatarRenderRequestPayload)paramAvatarRenderRequestEvent.payload()).getAnimation();
      Log.i("AVATAR_RENDERER", "HANDLE RENDER REQUEST ,track:" + str1 + " ,clip:" + str2);
    }
    while ((str2 == null) || (str2.equals(this.mCurrentClip)));
    this.mCurrentClip = str2;
    CafeMgr.PlayClip(this.mLastSurpriseId, str2, true);
    CafeMgr.forceUpdateAvatarTrack(this.mIsLocal);
  }

  public boolean isAvatarActived()
  {
    SessionMessages.AvatarControlPayload.Direction localDirection = SessionMessages.AvatarControlPayload.Direction.NONE;
    if (this.mCallSession != null)
      localDirection = this.mCallSession.m_avatarDirection;
    while ((localDirection == SessionMessages.AvatarControlPayload.Direction.BOTH) || ((this.mIsLocal) && (localDirection == SessionMessages.AvatarControlPayload.Direction.SENDER)) || ((!this.mIsLocal) && (localDirection == SessionMessages.AvatarControlPayload.Direction.RECEIVER)))
    {
      return true;
      if (this.mAvatarControlPayload != null)
        localDirection = this.mAvatarControlPayload.getDirection();
    }
    return false;
  }

  public boolean isInPIPMode()
  {
    if (this.mCallSession == null)
      return false;
    if ((isAvatarActived()) && (this.mCallSession.pipSwappable()))
    {
      if (this.mIsLocal)
        return !this.mCallSession.isPipSwapped();
      return this.mCallSession.isPipSwapped();
    }
    return false;
  }

  public boolean needDrawPIPVideoBg()
  {
    if (this.mCallSession == null)
      return false;
    if (!this.mIsLocal)
      return false;
    return (this.mCallSession.m_videoDirection == SessionMessages.MediaSessionPayload.Direction.SEND) && (!this.mCallSession.isPipSwapped());
  }

  public void onAvatarChanged(SessionMessages.AvatarControlPayload paramAvatarControlPayload)
  {
    this.mAvatarControlPayload = paramAvatarControlPayload;
    if (!isAvatarActived())
      stopAvatarAnimation();
    while (true)
    {
      CafeMgr.setAvatarSurpriseId(this.mIsLocal, this.mLastSurpriseId);
      return;
      if (needStartAvatar())
        startAvatar();
    }
  }

  public void onCafeFreed()
  {
    this.mIsCafeReady = false;
  }

  public void onCafeInitialized()
  {
    this.mIsCafeReady = true;
    if (needStartAvatar())
      startAvatar();
  }

  public void onSurfaceChanged(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.mGLRendererMode)
      Log.i("AVATAR_RENDERER", "GL2.0 should not call onSurfaceChanged ");
    do
    {
      return;
      Log.i("AVATAR_RENDERER", "onSurfaceChanged " + this.mViewId + " w " + paramInt1 + " h " + paramInt2 + " rotate " + paramBoolean1 + " autoAdjustRotation " + paramBoolean2);
      this.mRotate = paramBoolean1;
      this.mAutoAdjustRotation = paramBoolean2;
      this.mSurfaceRect = new Rect(0, 0, paramInt1, paramInt2);
      setRenderView();
    }
    while (!needStartAvatar());
    startAvatar();
  }

  public void onSurfaceCreated()
  {
    if (this.mGLRendererMode)
    {
      Log.i("AVATAR_RENDERER", "GL2.0 should not call onSurfaceCreated ");
      return;
    }
    onSurfaceDestroyed();
  }

  public void onSurfaceDestroyed()
  {
    Log.v("AVATAR_RENDERER", "onSurfaceDestroyed");
    this.mSurfaceRect = null;
    stopAvatarAnimation();
  }

  public void render()
  {
    boolean bool = isInPIPMode();
    if (this.mRequireUpdate)
      CafeMgr.updateAvatarTracksVisibility();
    CafeMgr.RenderView(this.mViewId, this.mRequireUpdate, this.mRequireClear, bool);
  }

  public void setClear(boolean paramBoolean)
  {
    this.mRequireClear = paramBoolean;
  }

  public void setRenderView()
  {
    if (this.mGLRendererMode)
      Log.i("AVATAR_RENDERER", "GL2.0 should not call onSurfaceChanged ");
    while (!isSurfaceReady())
      return;
    if (needRotate());
    for (int i = 90; ; i = 0)
    {
      int j = 1;
      if (needRotate())
        j = 0;
      if (!isAvatarActived())
      {
        if (needDrawPIPVideoBg())
        {
          Rect localRect2 = surfaceToCafe(VideoView.getBorderRect(j, 10));
          CafeMgr.SetRenderView(this.mViewId, 1 + localRect2.left, 1 + localRect2.top, localRect2.width() - 2, localRect2.height() - 2, i);
          CafeMgr.SetRenderClearColor(this.mViewId, 0.0F, 0.0F, 0.0F, 1.0F);
          return;
        }
        CafeMgr.SetRenderView(this.mViewId, this.mSurfaceRect.left, this.mSurfaceRect.top, this.mSurfaceRect.width(), this.mSurfaceRect.height(), i);
        CafeMgr.SetRenderClearColor(this.mViewId, 0.0F, 0.0F, 0.0F, 0.0F);
        return;
      }
      if (isInPIPMode())
      {
        Rect localRect1 = surfaceToCafe(VideoView.getBorderRect(j, 10));
        CafeMgr.SetRenderView(this.mViewId, localRect1.left, localRect1.top, localRect1.width(), localRect1.height(), i);
      }
      while (true)
      {
        CafeMgr.SetRenderClearColor(this.mViewId, 0.0F, 0.0F, 0.0F, 1.0F);
        return;
        Log.i("AVATAR_RENDERER", "set render view " + this.mViewId + " w " + this.mSurfaceRect.width() + " h " + this.mSurfaceRect.height());
        CafeMgr.SetRenderView(this.mViewId, this.mSurfaceRect.left, this.mSurfaceRect.top, this.mSurfaceRect.width(), this.mSurfaceRect.height(), i);
      }
    }
  }

  public void setUpdate(boolean paramBoolean)
  {
    this.mRequireUpdate = paramBoolean;
  }

  public void startAvatar()
  {
    if (!needStartAvatar())
    {
      Log.w("AVATAR_RENDERER", "start null avatar!");
      return;
    }
    CafeMgr.Resume();
    setRenderView();
    SessionMessages.AvatarInfo localAvatarInfo = getAvatarInfo();
    Log.i("AVATAR_RENDERER", "startAvatar animation = " + localAvatarInfo.getAnimation() + "mediaDir = " + localAvatarInfo.getMediaDir() + "mediaFile=" + localAvatarInfo.getMediaFile());
    String str1 = localAvatarInfo.getMediaDir();
    String str2 = localAvatarInfo.getMediaFile();
    String str3 = localAvatarInfo.getAnimation();
    CafeMgr.LoadSurprise(str1, str2);
    this.mLastSurpriseId = CafeMgr.StartAvatarSurpriseForClip(this.mViewId, str1, str2, str3, true, 0L, this.mIsLocal);
    CafeMgr.setAvatarSurpriseId(this.mIsLocal, this.mLastSurpriseId);
    Log.i("AVATAR_RENDERER", "SET surprise  " + this.mLastSurpriseId + " clip " + str3 + " started.");
  }

  public void stopAvatarAnimation()
  {
    Log.i("AVATAR_RENDERER", "stop surprise  " + this.mLastSurpriseId);
    if (this.mLastSurpriseId > 0)
    {
      CafeMgr.setAvatarSurpriseId(this.mIsLocal, -1);
      CafeMgr.StopSurprise(this.mLastSurpriseId);
      this.mLastSurpriseId = -1;
    }
  }

  public void switchAvatar(SessionMessages.AvatarControlPayload paramAvatarControlPayload)
  {
    Log.w("AVATAR_RENDERER", "switchAvatar");
    SessionMessages.AvatarControlPayload.Direction localDirection = paramAvatarControlPayload.getDirection();
    if (this.mIsLocal)
      if ((localDirection == SessionMessages.AvatarControlPayload.Direction.BOTH) || (localDirection == SessionMessages.AvatarControlPayload.Direction.SENDER))
      {
        SessionMessages.AvatarInfo localAvatarInfo2 = paramAvatarControlPayload.getLocalAvatarInfo();
        if ((localAvatarInfo2 != null) && ((getAvatarInfo() == null) || (localAvatarInfo2.getAvatarid() != getAvatarInfo().getAvatarid())))
        {
          stopAvatarAnimation();
          this.mAvatarControlPayload = paramAvatarControlPayload;
          startAvatar();
        }
      }
    SessionMessages.AvatarInfo localAvatarInfo1;
    do
    {
      do
        return;
      while ((localDirection != SessionMessages.AvatarControlPayload.Direction.BOTH) && (localDirection != SessionMessages.AvatarControlPayload.Direction.RECEIVER));
      localAvatarInfo1 = paramAvatarControlPayload.getRemoteAvatarInfo();
    }
    while ((localAvatarInfo1 == null) || ((getAvatarInfo() != null) && (localAvatarInfo1.getAvatarid() == this.mAvatarControlPayload.getRemoteAvatarInfo().getAvatarid())));
    stopAvatarAnimation();
    this.mAvatarControlPayload = paramAvatarControlPayload;
    startAvatar();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.avatar.AvatarRenderer
 * JD-Core Version:    0.6.2
 */