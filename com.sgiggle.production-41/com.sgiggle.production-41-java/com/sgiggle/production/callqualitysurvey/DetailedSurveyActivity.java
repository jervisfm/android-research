package com.sgiggle.production.callqualitysurvey;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.CallQualitySurveyDataMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions;
import com.sgiggle.media_engine.MediaEngineMessage.CancelPostCallMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.ActivityBase;
import com.sgiggle.production.TangoApp;
import com.sgiggle.util.Log;

public class DetailedSurveyActivity extends ActivityBase
{
  final int TAG = 86;

  private MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions getAnswer(int paramInt)
  {
    if (((CheckBox)findViewById(paramInt)).isChecked())
      return MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.AGREE;
    return MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED;
  }

  private void send()
  {
    String str = ((TextView)findViewById(2131361869)).getText().toString();
    MediaEngineMessage.CallQualitySurveyDataMessage localCallQualitySurveyDataMessage = new MediaEngineMessage.CallQualitySurveyDataMessage(-1, getAnswer(2131361861), MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, getAnswer(2131361862), getAnswer(2131361863), getAnswer(2131361865), getAnswer(2131361864), getAnswer(2131361866), MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, getAnswer(2131361867), str);
    MessageRouter.getInstance().postMessage("jingle", localCallQualitySurveyDataMessage);
    Toast.makeText(this, getResources().getString(2131296467), 1).show();
    finish();
  }

  public void onBackPressed()
  {
    Log.i(86, "onBackPressed");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelPostCallMessage());
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903053);
    TangoApp.getInstance().setPostCallActivityInstance(this);
    ((Button)findViewById(2131361858)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        DetailedSurveyActivity.this.send();
      }
    });
  }

  public void onDestroy()
  {
    super.onDestroy();
    TangoApp.getInstance().setPostCallActivityInstance(null);
  }

  public void onPause()
  {
    super.onPause();
    if (isFinishing())
      TangoApp.getInstance().setPostCallActivityInstance(null);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.callqualitysurvey.DetailedSurveyActivity
 * JD-Core Version:    0.6.2
 */