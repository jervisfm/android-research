package com.sgiggle.production.callqualitysurvey;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.CallQualitySurveyDataMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions;
import com.sgiggle.media_engine.MediaEngineMessage.CancelPostCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisablePostCallMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.ActivityBase;
import com.sgiggle.production.TangoApp;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.PostCallContentType;

public class QuickSurveyActivity extends ActivityBase
{
  private final int TAG = 86;
  private boolean m_spannedSurveyDetailsChosen = false;

  static
  {
    if (!QuickSurveyActivity.class.desiredAssertionStatus());
    for (boolean bool = true; ; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }

  private void onDontShowAgain()
  {
    this.m_spannedSurveyDetailsChosen = false;
    Log.v(86, "onDontShowAgain");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.DisablePostCallMessage(SessionMessages.PostCallContentType.POSTCALL_CALLQUALITY));
    finish();
  }

  private void onMoreDetails()
  {
    this.m_spannedSurveyDetailsChosen = true;
    startActivity(new Intent(this, DetailedSurveyActivity.class));
    finish();
  }

  private void onRating(int paramInt)
  {
    this.m_spannedSurveyDetailsChosen = false;
    int i;
    switch (paramInt)
    {
    default:
      Log.e(86, "Invalid rating value: " + paramInt);
      i = -2;
    case 2:
    case 1:
    case 0:
    }
    while (true)
    {
      MediaEngineMessage.CallQualitySurveyDataMessage localCallQualitySurveyDataMessage = new MediaEngineMessage.CallQualitySurveyDataMessage(i, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, MediaEngineMessage.CallQualitySurveyDataMessage.AgreeAnswerOptions.UNANSWERED, "");
      MessageRouter.getInstance().postMessage("jingle", localCallQualitySurveyDataMessage);
      Toast.makeText(this, getResources().getString(2131296467), 1).show();
      finish();
      return;
      i = 0;
      continue;
      i = 2;
      continue;
      i = 4;
    }
  }

  private void onSelectionMade(int paramInt)
  {
    assert ((paramInt >= 0) && (paramInt <= 4));
    if (paramInt == 3)
    {
      onMoreDetails();
      return;
    }
    if (paramInt == 4)
    {
      onDontShowAgain();
      return;
    }
    onRating(paramInt);
  }

  public void onCancel()
  {
    Log.i(86, "onCancel");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelPostCallMessage());
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.m_spannedSurveyDetailsChosen = false;
    setContentView(2130903054);
    TangoApp.getInstance().setPostCallActivityInstance(this);
    showDialog(0);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    RatingOptionsAdapter localRatingOptionsAdapter = new RatingOptionsAdapter(this);
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setTitle(getResources().getString(2131296457));
    localBuilder.setAdapter(localRatingOptionsAdapter, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        QuickSurveyActivity.this.onSelectionMade(paramAnonymousInt);
      }
    });
    localBuilder.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        QuickSurveyActivity.this.onCancel();
      }
    });
    return localBuilder.create();
  }

  protected void onDestroy()
  {
    Log.i(86, "onDestroy");
    super.onDestroy();
    if (!this.m_spannedSurveyDetailsChosen)
      TangoApp.getInstance().setPostCallActivityInstance(null);
  }

  protected void onPause()
  {
    Log.i(86, "onPause");
    super.onPause();
    if ((isFinishing()) && (!this.m_spannedSurveyDetailsChosen))
      TangoApp.getInstance().setPostCallActivityInstance(null);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.callqualitysurvey.QuickSurveyActivity
 * JD-Core Version:    0.6.2
 */