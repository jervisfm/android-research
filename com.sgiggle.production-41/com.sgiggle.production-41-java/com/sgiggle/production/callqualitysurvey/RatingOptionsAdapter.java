package com.sgiggle.production.callqualitysurvey;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RatingOptionsAdapter extends BaseAdapter
{
  static final int BAD = 2;
  static final int DONT_SHOW_AGAIN = 4;
  static final int EXCELLENT = 0;
  static final int FAIR = 1;
  private static final int[] MENU_ITEMS;
  static final int MORE_DETAILS = 3;
  private Context m_context;
  private final String[] m_longDescription;
  private final String[] m_shortDescription;

  static
  {
    if (!RatingOptionsAdapter.class.desiredAssertionStatus());
    for (boolean bool = true; ; bool = false)
    {
      $assertionsDisabled = bool;
      MENU_ITEMS = new int[] { 0, 1, 2, 3, 4 };
      return;
    }
  }

  public RatingOptionsAdapter(Context paramContext)
  {
    this.m_context = paramContext;
    String[] arrayOfString1 = new String[3];
    arrayOfString1[0] = paramContext.getString(2131296459);
    arrayOfString1[1] = paramContext.getString(2131296460);
    arrayOfString1[2] = paramContext.getString(2131296461);
    this.m_shortDescription = arrayOfString1;
    String[] arrayOfString2 = new String[3];
    arrayOfString2[0] = paramContext.getString(2131296462);
    arrayOfString2[1] = paramContext.getString(2131296463);
    arrayOfString2[2] = paramContext.getString(2131296464);
    this.m_longDescription = arrayOfString2;
  }

  private int getMaxShortDescriptionWidth(TextView paramTextView)
  {
    int i = 0;
    int j = 0;
    while (i < this.m_shortDescription.length)
    {
      Rect localRect = new Rect();
      paramTextView.getPaint().getTextBounds(this.m_shortDescription[i], 0, this.m_shortDescription[i].length(), localRect);
      j = Math.max(j, localRect.width());
      i++;
    }
    return j;
  }

  public int getCount()
  {
    return 5;
  }

  public Object getItem(int paramInt)
  {
    return Integer.valueOf(MENU_ITEMS[paramInt]);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    LayoutInflater localLayoutInflater = (LayoutInflater)this.m_context.getSystemService("layout_inflater");
    assert (paramInt < 5);
    if (paramInt < 3)
    {
      View localView2 = localLayoutInflater.inflate(2130903056, paramViewGroup, false);
      TextView localTextView2 = (TextView)localView2.findViewById(2131361871);
      int j = getMaxShortDescriptionWidth(localTextView2);
      ViewGroup.LayoutParams localLayoutParams = localTextView2.getLayoutParams();
      localLayoutParams.width = (10 + (j + (localTextView2.getPaddingLeft() + localTextView2.getPaddingRight())));
      localTextView2.setLayoutParams(localLayoutParams);
      localTextView2.setText(this.m_shortDescription[paramInt]);
      ((TextView)localView2.findViewById(2131361872)).setText(this.m_longDescription[paramInt]);
      return localView2;
    }
    View localView1 = localLayoutInflater.inflate(2130903055, paramViewGroup, false);
    TextView localTextView1 = (TextView)localView1.findViewById(2131361870);
    Resources localResources = this.m_context.getResources();
    if (paramInt == 3);
    for (int i = 2131296465; ; i = 2131296466)
    {
      localTextView1.setText(localResources.getString(i));
      return localView1;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.callqualitysurvey.RatingOptionsAdapter
 * JD-Core Version:    0.6.2
 */