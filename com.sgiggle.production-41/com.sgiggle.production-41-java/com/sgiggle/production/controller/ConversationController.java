package com.sgiggle.production.controller;

import android.content.Intent;
import com.sgiggle.messaging.Message;
import com.sgiggle.production.fragment.ConversationDetailFragment;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class ConversationController
{
  private static final String TAG = "Tango.ConversationController";
  private HashMap<SessionMessages.ConversationMessageType, ConversationMessageController> m_msgControllersByType = new HashMap();

  public ConversationController(ConversationDetailFragment paramConversationDetailFragment)
  {
    this.m_msgControllersByType.put(SessionMessages.ConversationMessageType.IMAGE_MESSAGE, new ConversationMessageControllerPicture(paramConversationDetailFragment));
    this.m_msgControllersByType.put(SessionMessages.ConversationMessageType.TEXT_MESSAGE, new ConversationMessageControllerText(paramConversationDetailFragment));
    this.m_msgControllersByType.put(SessionMessages.ConversationMessageType.VGOOD_MESSAGE, new ConversationMessageControllerVGood(paramConversationDetailFragment));
    this.m_msgControllersByType.put(SessionMessages.ConversationMessageType.VIDEO_MESSAGE, new ConversationMessageControllerVideo(paramConversationDetailFragment));
  }

  public boolean broadcastMessage(Message paramMessage)
  {
    Iterator localIterator = this.m_msgControllersByType.values().iterator();
    while (localIterator.hasNext())
      if (((ConversationMessageController)localIterator.next()).handleMessage(paramMessage))
        return true;
    return false;
  }

  public ConversationMessageController getMessageController(SessionMessages.ConversationMessageType paramConversationMessageType)
  {
    ConversationMessageController localConversationMessageController = (ConversationMessageController)this.m_msgControllersByType.get(paramConversationMessageType);
    if (localConversationMessageController == null)
    {
      Log.e("Tango.ConversationController", "getMessageController: cannot get controller of type=" + paramConversationMessageType);
      throw new UnsupportedOperationException("Not implemented! Cannot get controller of type=" + paramConversationMessageType);
    }
    return localConversationMessageController;
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Iterator localIterator = this.m_msgControllersByType.values().iterator();
    while (localIterator.hasNext())
      ((ConversationMessageController)localIterator.next()).onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onResume()
  {
    Iterator localIterator = this.m_msgControllersByType.values().iterator();
    while (localIterator.hasNext())
      ((ConversationMessageController)localIterator.next()).onResume();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.controller.ConversationController
 * JD-Core Version:    0.6.2
 */