package com.sgiggle.production.controller;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.ContextMenu;
import com.sgiggle.media_engine.MediaEngineMessage.ForwardMessageRequestMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SendConversationMessageMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.Utils;
import com.sgiggle.production.fragment.ConversationDetailFragment;
import com.sgiggle.production.fragment.DeleteConversationMessageDialog;
import com.sgiggle.production.manager.MediaManager;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationDetail;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.screens.tc.ConversationDetailActivity;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;
import com.sgiggle.xmpp.SessionMessages.SMSComposerType;
import java.util.ArrayList;
import java.util.List;

public abstract class ConversationMessageController
{
  protected static final int MENU_ID_BUY = 4;
  protected static final int MENU_ID_COPY_TEXT = 2;
  protected static final int MENU_ID_DELETE = 3;
  protected static final int MENU_ID_FORWARD = 5;
  protected static final int MENU_ID_RESEND = 0;
  protected static final int MENU_ID_VIEW = 1;
  private static final String TAG = "Tango.ConversationMessageController";
  protected ConversationDetailFragment m_conversationDetailFragment;

  public ConversationMessageController(ConversationDetailFragment paramConversationDetailFragment)
  {
    this.m_conversationDetailFragment = paramConversationDetailFragment;
  }

  private void doActionDeleteMessage(ConversationMessage paramConversationMessage)
  {
    DeleteConversationMessageDialog localDeleteConversationMessageDialog = DeleteConversationMessageDialog.newInstance(paramConversationMessage, getConversationId());
    localDeleteConversationMessageDialog.show(this.m_conversationDetailFragment.getFragmentManager(), localDeleteConversationMessageDialog.getClass().toString());
  }

  private void doActionForward(ConversationMessage paramConversationMessage)
  {
    MediaEngineMessage.ForwardMessageRequestMessage localForwardMessageRequestMessage = new MediaEngineMessage.ForwardMessageRequestMessage(paramConversationMessage.convertToSessionConversationMessage(getConversationId()));
    MessageRouter.getInstance().postMessage("jingle", localForwardMessageRequestMessage);
  }

  private void doActionResend(ConversationMessage paramConversationMessage)
  {
    MediaEngineMessage.SendConversationMessageMessage localSendConversationMessageMessage = new MediaEngineMessage.SendConversationMessageMessage(getConversationId(), paramConversationMessage.getMessageId(), paramConversationMessage.getType());
    MessageRouter.getInstance().postMessage("jingle", localSendConversationMessageMessage);
  }

  public abstract void createNewMessage(CreateMessageAction paramCreateMessageAction, Object[] paramArrayOfObject);

  public void doActionViewMessage(ConversationMessage paramConversationMessage)
  {
    Log.w("Tango.ConversationMessageController", "doActionViewMessage: cannot view message of type " + paramConversationMessage.getType());
  }

  protected ConversationDetailActivity getActivity()
  {
    return (ConversationDetailActivity)this.m_conversationDetailFragment.getActivity();
  }

  protected Context getContext()
  {
    return this.m_conversationDetailFragment.getActivity();
  }

  public ConversationDetail getConversationDetail()
  {
    return this.m_conversationDetailFragment.getConversationDetail();
  }

  protected String getConversationId()
  {
    return this.m_conversationDetailFragment.getConversationDetail().getConversationId();
  }

  protected Resources getResources()
  {
    return this.m_conversationDetailFragment.getResources();
  }

  public boolean handleMessage(Message paramMessage)
  {
    return false;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
  }

  public final void onContextItemSelected(int paramInt, ConversationMessage paramConversationMessage)
  {
    switch (paramInt)
    {
    case 2:
    case 4:
    default:
      onContextItemSelectedExtra(paramInt, paramConversationMessage);
      return;
    case 0:
      doActionResend(paramConversationMessage);
      return;
    case 1:
      doActionViewMessage(paramConversationMessage);
      return;
    case 5:
      doActionForward(paramConversationMessage);
      return;
    case 3:
    }
    doActionDeleteMessage(paramConversationMessage);
  }

  protected void onContextItemSelectedExtra(int paramInt, ConversationMessage paramConversationMessage)
  {
    throw new UnsupportedOperationException("Not implemented! menuItem not handled=" + paramInt);
  }

  public void onMessageStatusChanged(ConversationMessage paramConversationMessage)
  {
    if (paramConversationMessage.isStatusError())
    {
      MediaManager.getInstance().playAudioResourceAsNotification(getActivity(), 2131099648, paramConversationMessage.isFromSelf());
      if (((!paramConversationMessage.isStatusErrorPeerNotSupported()) && (!paramConversationMessage.isStatusErrorPeerPlatformNotSupported())) || (!onStatusErrorPeerNotSupported(paramConversationMessage)))
        this.m_conversationDetailFragment.showDeliveryError();
    }
    while (!paramConversationMessage.isStatusSent())
      return;
    MediaManager.getInstance().playAudioResourceAsNotification(getActivity(), 2131099649, true);
  }

  public void onResume()
  {
  }

  protected boolean onStatusErrorPeerNotSupported(ConversationMessage paramConversationMessage)
  {
    return false;
  }

  public final void populateContextMenu(ContextMenu paramContextMenu, ConversationMessage paramConversationMessage)
  {
    int i;
    if (paramConversationMessage.isStatusError())
    {
      i = 0 + 1;
      paramContextMenu.add(0, 0, i, 2131296581);
    }
    while (true)
    {
      int j = populateContextMenuExtra(paramContextMenu, paramConversationMessage, i);
      if ((supportsForward()) && ((!paramConversationMessage.isFromSelf()) || (paramConversationMessage.isStatusSent())))
      {
        j++;
        paramContextMenu.add(0, 5, j, 2131296580);
      }
      if ((!paramConversationMessage.isFromSelf()) || (!paramConversationMessage.isStatusSending()))
        paramContextMenu.add(0, 3, j + 1, 2131296582);
      return;
      i = 0;
    }
  }

  protected int populateContextMenuExtra(ContextMenu paramContextMenu, ConversationMessage paramConversationMessage, int paramInt)
  {
    return paramInt;
  }

  protected void showGenericSendSmsDialog(String paramString1, String paramString2, SessionMessages.SMSComposerType paramSMSComposerType, ConversationContact paramConversationContact)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramConversationContact.getContact());
    TangoApp.getInstance().showQuerySendSMSDialog(false, paramString1, paramString2, "", paramSMSComposerType, localArrayList);
  }

  protected void showNotSupportedByPeerGenericSendSmsDialog(ConversationContact paramConversationContact, ConversationMessage paramConversationMessage)
  {
    String str1 = getResources().getString(2131296585);
    Resources localResources2;
    Object[] arrayOfObject2;
    if (paramConversationMessage.isStatusErrorPeerNotSupported())
    {
      localResources2 = getResources();
      arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = paramConversationContact.getDisplayName();
    }
    Resources localResources1;
    Object[] arrayOfObject1;
    for (String str2 = localResources2.getString(2131296588, arrayOfObject2); ; str2 = localResources1.getString(2131296589, arrayOfObject1))
    {
      showGenericSendSmsDialog(str1, str2, SessionMessages.SMSComposerType.SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER, paramConversationContact);
      return;
      localResources1 = getResources();
      arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = paramConversationContact.getDisplayName();
    }
  }

  protected boolean showNotSupportedByPeerSendSmsDialog(ConversationContact paramConversationContact, String paramString, ConversationMessage paramConversationMessage)
  {
    String str1 = getResources().getString(2131296585);
    Resources localResources2;
    Object[] arrayOfObject2;
    if (paramConversationMessage.isStatusErrorPeerNotSupported())
    {
      localResources2 = getResources();
      arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = paramConversationContact.getDisplayName();
    }
    Resources localResources1;
    Object[] arrayOfObject1;
    for (String str2 = localResources2.getString(2131296586, arrayOfObject2); ; str2 = localResources1.getString(2131296587, arrayOfObject1))
    {
      return showSendSmsDialog(str1, str2, paramString, SessionMessages.SMSComposerType.SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER, paramConversationContact);
      localResources1 = getResources();
      arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = paramConversationContact.getDisplayName();
    }
  }

  protected boolean showSendSmsDialog(String paramString1, String paramString2, String paramString3, SessionMessages.SMSComposerType paramSMSComposerType, ConversationContact paramConversationContact)
  {
    SessionMessages.PhoneNumber localPhoneNumber = paramConversationContact.getContact().getPhoneNumber();
    if (localPhoneNumber == null);
    for (Object localObject = null; (!Utils.isSmsIntentAvailable(getContext())) || (TextUtils.isEmpty((CharSequence)localObject)); localObject = localPhoneNumber.getSubscriberNumber())
      return false;
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramConversationContact.getContact());
    TangoApp.getInstance().showQuerySendSMSDialog(true, paramString1, paramString2, paramString3, paramSMSComposerType, localArrayList);
    return true;
  }

  protected abstract boolean supportsForward();

  public static enum CreateMessageAction
  {
    static
    {
      CreateMessageAction[] arrayOfCreateMessageAction = new CreateMessageAction[2];
      arrayOfCreateMessageAction[0] = ACTION_NEW;
      arrayOfCreateMessageAction[1] = ACTION_PICK;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.controller.ConversationMessageController
 * JD-Core Version:    0.6.2
 */