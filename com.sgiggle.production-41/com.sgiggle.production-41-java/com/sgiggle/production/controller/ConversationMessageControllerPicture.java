package com.sgiggle.production.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.view.ContextMenu;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.CancelChoosePictureMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CancelPostProcessPictureMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CancelTakePictureMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ChoosePictureMessage;
import com.sgiggle.media_engine.MediaEngineMessage.PostProcessPictureMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SendConversationMessageMessage;
import com.sgiggle.media_engine.MediaEngineMessage.TakePictureMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ViewPictureMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.fragment.ConversationDetailFragment;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationDetail;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationMessagePicture;
import com.sgiggle.production.screens.picture.PicturePreviewActivity;
import com.sgiggle.production.screens.picture.PictureStorage;
import com.sgiggle.production.screens.tc.ConversationDetailActivity;
import com.sgiggle.production.util.BitmapOrientationDetector;
import com.sgiggle.production.util.BitmapTransformer;
import com.sgiggle.production.util.FileOperator;
import com.sgiggle.production.util.FilePathResolver;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import java.io.File;
import java.io.FileNotFoundException;

public class ConversationMessageControllerPicture extends ConversationMessageController
{
  private static final int CHOOSE_EXISTING_PHOTO_ACTIVITY_REQUEST_CODE = 200;
  private static final int PHOTO_PREVIEW_ACTIVITY_REQUEST_CODE = 300;
  private static final String TAG = "Tango.ConversationMessageControllerPicture";
  private static final int TAKE_PHOTO_ACTIVITY_REQUEST_CODE = 100;
  private boolean m_isChoosePictureInProgress = false;
  private boolean m_isPreviewInProcess = false;
  private boolean m_isTakePictureInProgress = false;
  private Uri m_oriPictureUri = null;
  private String m_processedPicturePath = null;

  public ConversationMessageControllerPicture(ConversationDetailFragment paramConversationDetailFragment)
  {
    super(paramConversationDetailFragment);
  }

  private void cancelChoosePicture()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelChoosePictureMessage());
  }

  private void cancelPostProcessPicture()
  {
    if (FileOperator.deleteFile(this.m_processedPicturePath))
      this.m_processedPicturePath = null;
    while (true)
    {
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelPostProcessPictureMessage());
      return;
      Log.e("Tango.ConversationMessageControllerPicture", "unable to delete image : " + this.m_processedPicturePath);
    }
  }

  private void cancelTakePicture()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CancelTakePictureMessage());
  }

  private void handleErrorInChoosingExistingPhoto()
  {
    cancelChoosePicture();
    showErrorInChoosingExistingPhoto();
  }

  private void handleErrorInTakingPhoto()
  {
    cancelTakePicture();
    showErrorInTakingPhoto();
  }

  private void handleResultFromCaptureImageActivity(String paramString, int paramInt)
  {
    if (paramInt == -1)
    {
      this.m_processedPicturePath = this.m_oriPictureUri.getPath();
      try
      {
        this.m_processedPicturePath = shrinkAndRotateBitmap(this.m_oriPictureUri, true);
        this.m_oriPictureUri = null;
        sendPhoto(paramString, this.m_processedPicturePath);
        return;
      }
      catch (Exception localException)
      {
        Log.e("Tango.ConversationMessageControllerPicture", "handleResultFromCaptureImageActivity got:" + localException.toString());
        localException.printStackTrace();
        handleErrorInTakingPhoto();
        return;
      }
    }
    if (paramInt == 0)
    {
      cancelTakePicture();
      Log.e("Tango.ConversationMessageControllerPicture", "The camera activity is canceled by user or crashed");
      return;
    }
    Log.e("Tango.ConversationMessageControllerPicture", "in handleResultFromCaptureImageActivity() gets unexpected resultCode:" + paramInt);
    handleErrorInTakingPhoto();
  }

  private void handleResultFromChooseExistingPhoto(int paramInt, Intent paramIntent)
  {
    if (paramInt == -1)
    {
      this.m_processedPicturePath = null;
      try
      {
        this.m_processedPicturePath = shrinkAndRotateBitmap(paramIntent.getData(), false);
        processPickupPicture();
        return;
      }
      catch (Exception localException)
      {
        Log.e("Tango.ConversationMessageControllerPicture", "handleResultFromChooseExistingPhoto() got exception:" + localException.toString());
        handleErrorInChoosingExistingPhoto();
        return;
      }
    }
    if (paramInt == 0)
    {
      cancelChoosePicture();
      Log.e("Tango.ConversationMessageControllerPicture", "The gallery activity is canceled by user or crashed");
      return;
    }
    Log.e("Tango.ConversationMessageControllerPicture", "handleResultFromChooseExistingPhoto() gets unexpected code : " + paramInt);
    handleErrorInChoosingExistingPhoto();
  }

  private void handleResultFromPhotoPreviewActivity(String paramString, int paramInt, Intent paramIntent)
  {
    this.m_isPreviewInProcess = false;
    if (paramInt == -1)
    {
      Uri localUri = Uri.parse(paramIntent.getStringExtra("result_uri"));
      if (localUri == null)
      {
        showErrorInChoosingExistingPhoto();
        cancelPostProcessPicture();
        return;
      }
      try
      {
        sendPhoto(paramString, localUri.getPath());
        return;
      }
      catch (Exception localException)
      {
        showErrorInChoosingExistingPhoto();
        cancelPostProcessPicture();
        return;
      }
    }
    if (paramInt == 0)
    {
      cancelPostProcessPicture();
      return;
    }
    Log.e("Tango.ConversationMessageControllerPicture", "handleResultFromPhotoPreviewActivity unknown resultCode : " + paramInt);
    cancelPostProcessPicture();
  }

  private boolean hasSDMounted()
  {
    if ("mounted".equals(Environment.getExternalStorageState()))
    {
      Log.d("Tango.ConversationMessageControllerPicture", "hasSDMounted() return true");
      return true;
    }
    Log.d("Tango.ConversationMessageControllerPicture", "hasSDMounted() return false");
    return false;
  }

  private void onChooseExistingPhotoClicked()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ChoosePictureMessage());
  }

  private void onTakePhotoClicked()
  {
    Log.d("Tango.ConversationMessageControllerPicture", "onTakePhotoClicked()");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.TakePictureMessage());
  }

  private void onViewPicture(String paramString, ConversationMessage paramConversationMessage)
  {
    startPictureViewer(paramString, (ConversationMessagePicture)paramConversationMessage);
  }

  private void processPickupPicture()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.PostProcessPictureMessage());
  }

  private static void renameFile(String paramString1, String paramString2)
  {
    new File(paramString1).renameTo(new File(paramString2));
  }

  private void sendPhoto(String paramString1, String paramString2)
    throws Exception
  {
    if (paramString1 == null)
    {
      showErrorInTakingPhoto();
      Log.e("Tango.ConversationMessageControllerPicture", "conversation id should be valid");
    }
    String str1 = PictureStorage.getTmpPicPath(getActivity(), "_sent", Boolean.valueOf(false));
    renameFile(paramString2, str1);
    String str2 = PictureStorage.getTmpPicPath(getActivity(), "_thumb", Boolean.valueOf(false));
    int i = getActivity().getResources().getDimensionPixelSize(2131230745);
    int j = getActivity().getResources().getDimensionPixelSize(2131230746);
    Bitmap localBitmap1 = BitmapTransformer.loadFile(str1);
    Bitmap localBitmap2 = BitmapTransformer.downscale(localBitmap1, i, j, true);
    int k = localBitmap2.getWidth();
    int m = localBitmap2.getHeight();
    if (!localBitmap1.isRecycled())
      localBitmap1.recycle();
    BitmapTransformer.saveFile(localBitmap2, str2);
    if (!localBitmap2.isRecycled())
      localBitmap2.recycle();
    Log.e("Tango.ConversationMessageControllerPicture", "sendPhoto, path:" + str1 + " Tpath:" + str2);
    MediaEngineMessage.SendConversationMessageMessage localSendConversationMessageMessage = new MediaEngineMessage.SendConversationMessageMessage(paramString1, SessionMessages.ConversationMessageType.IMAGE_MESSAGE, str1, str2, k, m, getResources().getString(2131296611));
    MessageRouter.getInstance().postMessage("jingle", localSendConversationMessageMessage);
  }

  private void showError(int paramInt)
  {
    Toast.makeText(getActivity(), getResources().getString(paramInt), 0).show();
  }

  private void showErrorInChoosingExistingPhoto()
  {
    showError(2131296660);
  }

  private void showErrorInTakingPhoto()
  {
    showError(2131296659);
  }

  private String shrinkAndRotateBitmap(Uri paramUri, boolean paramBoolean)
    throws Exception
  {
    int i = BitmapOrientationDetector.getOrientationOfImage(getActivity(), paramUri);
    String str1 = FilePathResolver.translateUriToFilePath(getActivity(), paramUri);
    Bitmap localBitmap1 = BitmapTransformer.loadFile(str1);
    if (localBitmap1 == null)
      localBitmap1 = BitmapTransformer.loadFile(getActivity(), paramUri);
    if (localBitmap1 == null)
      throw new FileNotFoundException();
    Bitmap localBitmap2 = BitmapTransformer.downscale(localBitmap1, 1280, 1280, false);
    if ((localBitmap1 != localBitmap2) && (!localBitmap1.isRecycled()))
      localBitmap1.recycle();
    Bitmap localBitmap3 = BitmapTransformer.rotate(localBitmap2, i);
    if ((localBitmap2 != localBitmap3) && (!localBitmap2.isRecycled()))
      localBitmap2.recycle();
    String str2 = PictureStorage.getTmpPicPath(getActivity(), "_ro", Boolean.valueOf(false));
    BitmapTransformer.saveFile(localBitmap3, str2);
    if (!localBitmap3.isRecycled())
      localBitmap3.recycle();
    if ((paramBoolean) && (!FileOperator.deleteFile(str1)))
      Log.e("Tango.ConversationMessageControllerPicture", "unable to remove file : " + str1);
    return str2;
  }

  private void startChooseExistingPhoto()
  {
    if (!this.m_isChoosePictureInProgress);
    try
    {
      Intent localIntent = new Intent();
      localIntent.addFlags(262144);
      localIntent.setType("image/*");
      localIntent.setAction("android.intent.action.GET_CONTENT");
      this.m_conversationDetailFragment.startActivityForResult(localIntent, 200);
      this.m_isChoosePictureInProgress = true;
      return;
    }
    catch (Exception localException)
    {
      Log.e("Tango.ConversationMessageControllerPicture", "in startChooseExistingPhoto() exception is caught:" + localException.toString());
      handleErrorInChoosingExistingPhoto();
    }
  }

  private void startPhotoPreview(String paramString)
  {
    if (paramString == null)
    {
      Log.e("Tango.ConversationMessageControllerPicture", "path is null, unable to start photo preview");
      return;
    }
    Intent localIntent = new Intent(getActivity(), PicturePreviewActivity.class);
    localIntent.putExtra("path", FileOperator.getUriFromPath(paramString).toString());
    this.m_conversationDetailFragment.startActivityForResult(localIntent, 300);
  }

  private void startPictureViewer(String paramString, ConversationMessagePicture paramConversationMessagePicture)
  {
    MediaEngineMessage.ViewPictureMessage localViewPictureMessage = new MediaEngineMessage.ViewPictureMessage(paramString, paramConversationMessagePicture.getMessageId());
    MessageRouter.getInstance().postMessage("jingle", localViewPictureMessage);
  }

  private void startTakePhoto()
  {
    Intent localIntent;
    if (!this.m_isTakePictureInProgress)
    {
      localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
      localIntent.addFlags(262144);
    }
    try
    {
      this.m_oriPictureUri = PictureStorage.getTmpPicUri(getActivity(), Boolean.valueOf(false));
      localIntent.putExtra("output", this.m_oriPictureUri);
      this.m_conversationDetailFragment.startActivityForResult(localIntent, 100);
      this.m_isTakePictureInProgress = true;
      return;
    }
    catch (Exception localException)
    {
      Log.e("Tango.ConversationMessageControllerPicture", "in startTakePhoto() exception is caught:" + localException.toString());
      handleErrorInTakingPhoto();
    }
  }

  public void createNewMessage(ConversationMessageController.CreateMessageAction paramCreateMessageAction, Object[] paramArrayOfObject)
  {
    switch (1.$SwitchMap$com$sgiggle$production$controller$ConversationMessageController$CreateMessageAction[paramCreateMessageAction.ordinal()])
    {
    default:
      Log.w("Tango.ConversationMessageControllerPicture", "createNewMessage: action not handled.");
      return;
    case 1:
      if (hasSDMounted())
      {
        onTakePhotoClicked();
        return;
      }
      Toast.makeText(getActivity(), 2131296614, 0).show();
      return;
    case 2:
    }
    if (hasSDMounted())
    {
      onChooseExistingPhotoClicked();
      return;
    }
    Toast.makeText(getActivity(), 2131296615, 0).show();
  }

  public void doActionViewMessage(ConversationMessage paramConversationMessage)
  {
    if ("mounted".equals(Environment.getExternalStorageState()))
    {
      onViewPicture(getConversationId(), paramConversationMessage);
      return;
    }
    Toast.makeText(getActivity(), 2131296616, 0).show();
  }

  public boolean handleMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      return false;
    case 35290:
      startTakePhoto();
      return true;
    case 35291:
      return true;
    case 35292:
      startChooseExistingPhoto();
      return true;
    case 293:
      return true;
    case 35294:
    }
    if (!this.m_isPreviewInProcess)
    {
      this.m_isPreviewInProcess = true;
      startPhotoPreview(this.m_processedPicturePath);
      return true;
    }
    return true;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
      return;
    case 100:
      this.m_isTakePictureInProgress = false;
      handleResultFromCaptureImageActivity(getConversationId(), paramInt2);
      return;
    case 200:
      Log.d("Tango.ConversationMessageControllerPicture", "onActivityResult case CHOOSE_EXISTING_PHOTO_ACTIVITY_REQUEST_CODE :" + paramInt2);
      this.m_isChoosePictureInProgress = false;
      handleResultFromChooseExistingPhoto(paramInt2, paramIntent);
      return;
    case 300:
    }
    handleResultFromPhotoPreviewActivity(getConversationId(), paramInt2, paramIntent);
  }

  public void onResume()
  {
    if (this.m_isChoosePictureInProgress)
    {
      Log.e("Tango.ConversationMessageControllerPicture", "onResume() should be called after m_isChoosePictureInProgress is cleared, the choosing picture app crashed possibly");
      cancelChoosePicture();
      this.m_isChoosePictureInProgress = false;
    }
  }

  protected boolean onStatusErrorPeerNotSupported(ConversationMessage paramConversationMessage)
  {
    ConversationContact localConversationContact = getConversationDetail().getFirstPeer();
    if (!showNotSupportedByPeerSendSmsDialog(localConversationContact, paramConversationMessage.getTextInSms(), paramConversationMessage))
      showNotSupportedByPeerGenericSendSmsDialog(localConversationContact, paramConversationMessage);
    return true;
  }

  protected int populateContextMenuExtra(ContextMenu paramContextMenu, ConversationMessage paramConversationMessage, int paramInt)
  {
    int i = paramInt + 1;
    paramContextMenu.add(0, 1, i, 2131296579);
    return i;
  }

  protected boolean supportsForward()
  {
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.controller.ConversationMessageControllerPicture
 * JD-Core Version:    0.6.2
 */