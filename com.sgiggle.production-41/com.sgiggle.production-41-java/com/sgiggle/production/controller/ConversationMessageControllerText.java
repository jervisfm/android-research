package com.sgiggle.production.controller;

import android.content.res.Resources;
import android.view.ContextMenu;
import com.sgiggle.media_engine.MediaEngineMessage.SendConversationMessageMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.Utils;
import com.sgiggle.production.fragment.ConversationDetailFragment;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationDetail;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;

public class ConversationMessageControllerText extends ConversationMessageController
{
  public ConversationMessageControllerText(ConversationDetailFragment paramConversationDetailFragment)
  {
    super(paramConversationDetailFragment);
  }

  private void doActionCopyText(ConversationMessage paramConversationMessage)
  {
    Utils.copyTextToClipboard(paramConversationMessage.getText(), getContext());
  }

  public void createNewMessage(ConversationMessageController.CreateMessageAction paramCreateMessageAction, Object[] paramArrayOfObject)
  {
    MediaEngineMessage.SendConversationMessageMessage localSendConversationMessageMessage = new MediaEngineMessage.SendConversationMessageMessage(getConversationId(), SessionMessages.ConversationMessageType.TEXT_MESSAGE, (String)paramArrayOfObject[0]);
    MessageRouter.getInstance().postMessage("jingle", localSendConversationMessageMessage);
  }

  public void onContextItemSelectedExtra(int paramInt, ConversationMessage paramConversationMessage)
  {
    switch (paramInt)
    {
    default:
      return;
    case 2:
    }
    doActionCopyText(paramConversationMessage);
  }

  protected boolean onStatusErrorPeerNotSupported(ConversationMessage paramConversationMessage)
  {
    ConversationContact localConversationContact = getConversationDetail().getFirstPeer();
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramConversationMessage.getText();
    if (!showNotSupportedByPeerSendSmsDialog(localConversationContact, localResources.getString(2131296590, arrayOfObject), paramConversationMessage))
      showNotSupportedByPeerGenericSendSmsDialog(localConversationContact, paramConversationMessage);
    return true;
  }

  protected int populateContextMenuExtra(ContextMenu paramContextMenu, ConversationMessage paramConversationMessage, int paramInt)
  {
    int i = paramInt + 1;
    paramContextMenu.add(0, 2, i, 2131296583);
    return i;
  }

  protected boolean supportsForward()
  {
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.controller.ConversationMessageControllerText
 * JD-Core Version:    0.6.2
 */