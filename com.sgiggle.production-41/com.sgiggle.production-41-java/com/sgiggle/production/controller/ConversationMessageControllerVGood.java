package com.sgiggle.production.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.view.ContextMenu;
import com.sgiggle.media_engine.MediaEngineMessage.SendConversationMessageMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.PurchaseProxyActivity;
import com.sgiggle.production.fragment.ConversationDetailFragment;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationDetail;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationMessageVGood;
import com.sgiggle.production.screens.tc.ConversationDetailActivity;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import java.util.Locale;
import java.util.Random;

public class ConversationMessageControllerVGood extends ConversationMessageController
{
  private Random m_random = new Random();

  public ConversationMessageControllerVGood(ConversationDetailFragment paramConversationDetailFragment)
  {
    super(paramConversationDetailFragment);
  }

  private void doActionBuy(ConversationMessage paramConversationMessage)
  {
    ConversationMessageVGood localConversationMessageVGood = (ConversationMessageVGood)paramConversationMessage;
    Intent localIntent = new Intent(getContext(), PurchaseProxyActivity.class);
    localIntent.putExtra("PurchaseProxyActivity.EXTRA_PRODUCT_ID", localConversationMessageVGood.getProductId());
    localIntent.putExtra("PurchaseProxyActivity.EXTRA_EXTERNAL_MARKET_ID", localConversationMessageVGood.getExternalMarketId());
    getActivity().startActivity(localIntent);
  }

  public void createNewMessage(ConversationMessageController.CreateMessageAction paramCreateMessageAction, Object[] paramArrayOfObject)
  {
    long l = 0xFFFFFFFF & this.m_random.nextLong();
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = paramArrayOfObject[0];
    arrayOfObject[1] = Long.valueOf(l);
    String str = String.format(localLocale, "%d:%d", arrayOfObject);
    MediaEngineMessage.SendConversationMessageMessage localSendConversationMessageMessage = new MediaEngineMessage.SendConversationMessageMessage(getConversationId(), SessionMessages.ConversationMessageType.VGOOD_MESSAGE, "", str);
    MessageRouter.getInstance().postMessage("jingle", localSendConversationMessageMessage);
  }

  public void doActionViewMessage(ConversationMessage paramConversationMessage)
  {
    this.m_conversationDetailFragment.hideIme();
    ConversationMessageVGood localConversationMessageVGood = (ConversationMessageVGood)paramConversationMessage;
    getActivity().startVGoodAnimation(localConversationMessageVGood.getVGoodPath(), localConversationMessageVGood.getVGoodId(), localConversationMessageVGood.getSeed());
  }

  public void onContextItemSelectedExtra(int paramInt, ConversationMessage paramConversationMessage)
  {
    switch (paramInt)
    {
    default:
      return;
    case 4:
    }
    doActionBuy(paramConversationMessage);
  }

  public void onMessageStatusChanged(ConversationMessage paramConversationMessage)
  {
    super.onMessageStatusChanged(paramConversationMessage);
    if ((!paramConversationMessage.isFromSelf()) || (paramConversationMessage.isStatusSent()))
    {
      ConversationMessageVGood localConversationMessageVGood = (ConversationMessageVGood)paramConversationMessage;
      getActivity().startVGoodAnimation(localConversationMessageVGood.getVGoodPath(), localConversationMessageVGood.getVGoodId(), localConversationMessageVGood.getSeed());
    }
  }

  protected boolean onStatusErrorPeerNotSupported(ConversationMessage paramConversationMessage)
  {
    ConversationContact localConversationContact = getConversationDetail().getFirstPeer();
    if (!showNotSupportedByPeerSendSmsDialog(localConversationContact, getResources().getString(2131296591), paramConversationMessage))
      showNotSupportedByPeerGenericSendSmsDialog(localConversationContact, paramConversationMessage);
    return true;
  }

  protected int populateContextMenuExtra(ContextMenu paramContextMenu, ConversationMessage paramConversationMessage, int paramInt)
  {
    int i = paramInt + 1;
    paramContextMenu.add(0, 1, i, 2131296579);
    if (((ConversationMessageVGood)paramConversationMessage).canBePurchased())
    {
      i++;
      paramContextMenu.add(0, 4, i, 2131296584);
    }
    return i;
  }

  protected boolean supportsForward()
  {
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.controller.ConversationMessageControllerVGood
 * JD-Core Version:    0.6.2
 */