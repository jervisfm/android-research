package com.sgiggle.production.controller;

import android.view.ContextMenu;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.LeaveVideoMailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.PlayVideoMessageMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.fragment.ConversationDetailFragment;
import com.sgiggle.production.model.ConversationDetail;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationMessageVideo;
import com.sgiggle.util.Log;

public class ConversationMessageControllerVideo extends ConversationMessageController
{
  private static final String TAG = "Tango.ConversationMessageControllerVideo";

  public ConversationMessageControllerVideo(ConversationDetailFragment paramConversationDetailFragment)
  {
    super(paramConversationDetailFragment);
  }

  public void createNewMessage(ConversationMessageController.CreateMessageAction paramCreateMessageAction, Object[] paramArrayOfObject)
  {
    MediaEngineMessage.LeaveVideoMailMessage localLeaveVideoMailMessage = new MediaEngineMessage.LeaveVideoMailMessage(this.m_conversationDetailFragment.getConversationDetail().convertPeersToContactList(), true);
    MessageRouter.getInstance().postMessage("jingle", localLeaveVideoMailMessage);
  }

  public void doActionViewMessage(ConversationMessage paramConversationMessage)
  {
    ConversationMessageVideo localConversationMessageVideo = (ConversationMessageVideo)paramConversationMessage;
    if (!localConversationMessageVideo.isLocalPlaybackAvailable())
    {
      Log.v("Tango.ConversationMessageControllerVideo", "Video play back not available");
      Toast.makeText(TangoApp.getInstance(), 2131296445, 0).show();
      return;
    }
    MediaEngineMessage.PlayVideoMessageMessage localPlayVideoMessageMessage = new MediaEngineMessage.PlayVideoMessageMessage(getConversationId(), localConversationMessageVideo.getMediaId(), paramConversationMessage.getMessageId(), paramConversationMessage.isFromSelf());
    MessageRouter.getInstance().postMessage("jingle", localPlayVideoMessageMessage);
  }

  protected boolean onStatusErrorPeerNotSupported(ConversationMessage paramConversationMessage)
  {
    showNotSupportedByPeerGenericSendSmsDialog(getConversationDetail().getFirstPeer(), paramConversationMessage);
    return true;
  }

  protected int populateContextMenuExtra(ContextMenu paramContextMenu, ConversationMessage paramConversationMessage, int paramInt)
  {
    int i = paramInt + 1;
    paramContextMenu.add(0, 1, i, 2131296579);
    return i;
  }

  protected boolean supportsForward()
  {
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.controller.ConversationMessageControllerVideo
 * JD-Core Version:    0.6.2
 */