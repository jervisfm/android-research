package com.sgiggle.production.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.util.Log;

public class PurchaseDatabase
{
  private static final String DATABASE_NAME = "purchase.db";
  private static final int DATABASE_VERSION = 1;
  private static final String[] HISTORY_COLUMNS = { "_id", "productId", "state", "purchaseTime", "developerPayload" };
  public static final String HISTORY_DEVELOPER_PAYLOAD_COL = "developerPayload";
  public static final String HISTORY_ORDER_ID_COL = "_id";
  public static final String HISTORY_PRODUCT_ID_COL = "productId";
  public static final String HISTORY_PURCHASE_TIME_COL = "purchaseTime";
  public static final String HISTORY_STATE_COL = "state";
  private static final String[] PURCHASED_COLUMNS = { "_id", "quantity" };
  private static final String PURCHASED_ITEMS_TABLE_NAME = "purchased";
  public static final String PURCHASED_PRODUCT_ID_COL = "_id";
  public static final String PURCHASED_QUANTITY_COL = "quantity";
  private static final String PURCHASE_HISTORY_TABLE_NAME = "history";
  private static final String TAG = "PurchaseDatabase";
  private DatabaseHelper mDatabaseHelper;
  private SQLiteDatabase mDb;

  public PurchaseDatabase(Context paramContext)
  {
    this.mDatabaseHelper = new DatabaseHelper(paramContext);
    this.mDb = this.mDatabaseHelper.getWritableDatabase();
  }

  private void insertOrder(String paramString1, String paramString2, Constants.PurchaseState paramPurchaseState, long paramLong, String paramString3)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", paramString1);
    localContentValues.put("productId", paramString2);
    localContentValues.put("state", Integer.valueOf(paramPurchaseState.ordinal()));
    localContentValues.put("purchaseTime", Long.valueOf(paramLong));
    localContentValues.put("developerPayload", paramString3);
    this.mDb.replace("history", null, localContentValues);
  }

  private void updatePurchasedItem(String paramString, int paramInt)
  {
    if (paramInt == 0)
    {
      this.mDb.delete("purchased", "_id=?", new String[] { paramString });
      return;
    }
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", paramString);
    localContentValues.put("quantity", Integer.valueOf(paramInt));
    this.mDb.replace("purchased", null, localContentValues);
  }

  public void close()
  {
    this.mDatabaseHelper.close();
  }

  public Cursor queryAllPurchasedItems()
  {
    return this.mDb.query("purchased", PURCHASED_COLUMNS, null, null, null, null, null);
  }

  public int updatePurchase(String paramString1, String paramString2, Constants.PurchaseState paramPurchaseState, long paramLong, String paramString3)
  {
    while (true)
    {
      int i;
      try
      {
        insertOrder(paramString1, paramString2, paramPurchaseState, paramLong, paramString3);
        Cursor localCursor = this.mDb.query("history", HISTORY_COLUMNS, "productId=?", new String[] { paramString2 }, null, null, null, null);
        if (localCursor == null)
        {
          j = 0;
          return j;
        }
        i = 0;
        try
        {
          if (localCursor.moveToNext())
          {
            Constants.PurchaseState localPurchaseState = Constants.PurchaseState.valueOf(localCursor.getInt(2));
            if (localPurchaseState == Constants.PurchaseState.PURCHASED)
              break label145;
            if (localPurchaseState != Constants.PurchaseState.REFUNDED)
              continue;
            break label145;
          }
          updatePurchasedItem(paramString2, i);
        }
        finally
        {
          if (localCursor != null)
            localCursor.close();
        }
      }
      finally
      {
      }
      label145: i++;
      continue;
      int j = i;
    }
  }

  private class DatabaseHelper extends SQLiteOpenHelper
  {
    public DatabaseHelper(Context arg2)
    {
      super("purchase.db", null, 1);
    }

    private void createPurchaseTable(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("CREATE TABLE history(_id TEXT PRIMARY KEY, state INTEGER, productId TEXT, developerPayload TEXT, purchaseTime INTEGER)");
      paramSQLiteDatabase.execSQL("CREATE TABLE purchased(_id TEXT PRIMARY KEY, quantity INTEGER)");
    }

    public void onCreate(SQLiteDatabase paramSQLiteDatabase)
    {
      createPurchaseTable(paramSQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
    {
      if (paramInt2 != 1)
      {
        Log.w("PurchaseDatabase", "Database upgrade from old: " + paramInt1 + " to: " + paramInt2);
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS history");
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS purchased");
        createPurchaseTable(paramSQLiteDatabase);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.database.PurchaseDatabase
 * JD-Core Version:    0.6.2
 */