package com.sgiggle.production.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.TangoApp.BeforeGoingToBackgroundCallback;
import com.sgiggle.util.Log;

public class TangoAlertDialog extends AlertDialog
  implements TangoApp.BeforeGoingToBackgroundCallback
{
  private static final String TAG = "Tango.TangoAlertDialog";
  private DialogInterface.OnDismissListener backgroundDismissListener;
  private boolean buttonClicked = false;
  private DialogInterface.OnDismissListener m_dismissListener;

  protected TangoAlertDialog(Context paramContext)
  {
    super(paramContext);
  }

  public void beforeGoingToBackground()
  {
    if (this.backgroundDismissListener != null)
    {
      this.backgroundDismissListener.onDismiss(this);
      dismiss();
    }
  }

  public boolean isButtonClicked()
  {
    return this.buttonClicked;
  }

  public void setBackgroundDismissListener(DialogInterface.OnDismissListener paramOnDismissListener)
  {
    this.backgroundDismissListener = paramOnDismissListener;
  }

  public void setButtonClicked(boolean paramBoolean)
  {
    this.buttonClicked = paramBoolean;
  }

  public void setExtraOnDismissListener(DialogInterface.OnDismissListener paramOnDismissListener)
  {
    this.m_dismissListener = paramOnDismissListener;
  }

  public static class Builder
  {
    private boolean m_cancelable = false;
    private Context m_context;
    private boolean m_dismissBeforeGoingToBackground = false;
    private CharSequence m_message;
    private String m_negativeButton;
    private DialogInterface.OnClickListener m_negativeButtonClickListener;
    private String m_positiveButton;
    private DialogInterface.OnClickListener m_positiveButtonClickListener;

    public Builder(Context paramContext)
    {
      this.m_context = paramContext;
    }

    public TangoAlertDialog create()
    {
      final TangoAlertDialog localTangoAlertDialog = new TangoAlertDialog(this.m_context);
      localTangoAlertDialog.setMessage(this.m_message);
      localTangoAlertDialog.setCancelable(this.m_cancelable);
      DialogInterface.OnClickListener local1 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Log.d("Tango.TangoAlertDialog", "execute button click listener");
          localTangoAlertDialog.setButtonClicked(true);
          if ((TangoAlertDialog.Builder.this.m_positiveButtonClickListener != null) && (paramAnonymousInt == -1))
            TangoAlertDialog.Builder.this.m_positiveButtonClickListener.onClick(localTangoAlertDialog, -1);
          while (true)
          {
            localTangoAlertDialog.dismiss();
            return;
            if ((TangoAlertDialog.Builder.this.m_negativeButtonClickListener != null) && (paramAnonymousInt == -2))
              TangoAlertDialog.Builder.this.m_negativeButtonClickListener.onClick(localTangoAlertDialog, -2);
          }
        }
      };
      if (this.m_positiveButton != null)
        localTangoAlertDialog.setButton(-1, this.m_positiveButton, local1);
      if (this.m_negativeButton != null)
        localTangoAlertDialog.setButton(-2, this.m_negativeButton, local1);
      DialogInterface.OnDismissListener local2 = new DialogInterface.OnDismissListener()
      {
        public void onDismiss(DialogInterface paramAnonymousDialogInterface)
        {
          if (!localTangoAlertDialog.isButtonClicked())
          {
            Log.d("Tango.TangoAlertDialog", "call extra dismiss listener since dialog is not dismissed by clicking button");
            if (TangoAlertDialog.Builder.this.m_negativeButtonClickListener == null)
              break label88;
            TangoAlertDialog.Builder.this.m_negativeButtonClickListener.onClick(localTangoAlertDialog, -2);
          }
          while (true)
          {
            localTangoAlertDialog.setButtonClicked(true);
            TangoApp.getInstance().removeBeforeBackgroundObserver(localTangoAlertDialog);
            if (localTangoAlertDialog.m_dismissListener != null)
              localTangoAlertDialog.m_dismissListener.onDismiss(paramAnonymousDialogInterface);
            return;
            label88: if (TangoAlertDialog.Builder.this.m_positiveButtonClickListener != null)
              TangoAlertDialog.Builder.this.m_positiveButtonClickListener.onClick(localTangoAlertDialog, -1);
          }
        }
      };
      if (this.m_dismissBeforeGoingToBackground)
      {
        localTangoAlertDialog.setBackgroundDismissListener(local2);
        TangoApp.getInstance().addBeforeBackgroundObserver(localTangoAlertDialog);
      }
      localTangoAlertDialog.setOnDismissListener(local2);
      return localTangoAlertDialog;
    }

    public Builder setCancelable(boolean paramBoolean)
    {
      this.m_cancelable = paramBoolean;
      return this;
    }

    public Builder setDismissBeforeGoingToBackground(boolean paramBoolean)
    {
      this.m_dismissBeforeGoingToBackground = paramBoolean;
      return this;
    }

    public Builder setMessage(int paramInt)
    {
      this.m_message = ((String)this.m_context.getText(paramInt));
      return this;
    }

    public Builder setMessage(CharSequence paramCharSequence)
    {
      this.m_message = paramCharSequence;
      return this;
    }

    public Builder setMessage(String paramString)
    {
      this.m_message = paramString;
      return this;
    }

    public Builder setNegativeButton(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m_negativeButton = ((String)this.m_context.getText(paramInt));
      this.m_negativeButtonClickListener = paramOnClickListener;
      return this;
    }

    public Builder setNegativeButton(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m_negativeButton = paramString;
      this.m_negativeButtonClickListener = paramOnClickListener;
      return this;
    }

    public Builder setPositiveButton(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m_positiveButton = ((String)this.m_context.getText(paramInt));
      this.m_positiveButtonClickListener = paramOnClickListener;
      return this;
    }

    public Builder setPositiveButton(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m_positiveButton = paramString;
      this.m_positiveButtonClickListener = paramOnClickListener;
      return this;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.dialog.TangoAlertDialog
 * JD-Core Version:    0.6.2
 */