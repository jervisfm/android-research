package com.sgiggle.production.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TangoDialog extends Dialog
{
  protected TangoDialog(Context paramContext)
  {
    this(paramContext, 2131558490);
  }

  protected TangoDialog(Context paramContext, int paramInt)
  {
    super(paramContext, paramInt);
  }

  protected TangoDialog(Context paramContext, boolean paramBoolean, DialogInterface.OnCancelListener paramOnCancelListener)
  {
    super(paramContext, paramBoolean, paramOnCancelListener);
  }

  public static class Builder
  {
    private boolean m_cancelable;
    private View m_contentView;
    private Context m_context;
    private LayoutInflater m_inflater;
    private CharSequence m_message;
    private String m_negativeButton;
    private DialogInterface.OnClickListener m_negativeButtonClickListener;
    private String m_positiveButton;
    private DialogInterface.OnClickListener m_positiveButtonClickListener;
    private String m_title;
    private View m_titleView;

    public Builder(Context paramContext)
    {
      this.m_context = paramContext;
    }

    public TangoDialog create()
    {
      if (this.m_inflater == null)
        this.m_inflater = ((LayoutInflater)this.m_context.getSystemService("layout_inflater"));
      final TangoDialog localTangoDialog = new TangoDialog(this.m_context, 2131558490);
      View localView = this.m_inflater.inflate(2130903127, null);
      localTangoDialog.addContentView(localView, new ViewGroup.LayoutParams(-2, -2));
      if (this.m_title != null)
      {
        ((TextView)localView.findViewById(2131361841)).setText(this.m_title);
        if (this.m_message == null)
          break label292;
        ((TextView)localView.findViewById(2131362127)).setText(this.m_message);
        label110: if (this.m_positiveButton == null)
          break label337;
        Button localButton2 = (Button)localView.findViewById(2131362128);
        localButton2.setText(this.m_positiveButton);
        if (this.m_positiveButtonClickListener != null)
          localButton2.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramAnonymousView)
            {
              TangoDialog.Builder.this.m_positiveButtonClickListener.onClick(localTangoDialog, -1);
              localTangoDialog.dismiss();
            }
          });
        label158: if (this.m_negativeButton == null)
          break label354;
        Button localButton1 = (Button)localView.findViewById(2131362129);
        localButton1.setText(this.m_negativeButton);
        if (this.m_negativeButtonClickListener != null)
          localButton1.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramAnonymousView)
            {
              TangoDialog.Builder.this.m_negativeButtonClickListener.onClick(localTangoDialog, -2);
              localTangoDialog.dismiss();
            }
          });
      }
      while (true)
      {
        localTangoDialog.setCancelable(this.m_cancelable);
        localTangoDialog.setContentView(localView, new ViewGroup.LayoutParams(-2, -2));
        localTangoDialog.getWindow().setLayout(-2, -2);
        return localTangoDialog;
        if (this.m_titleView == null)
          break;
        LinearLayout localLinearLayout1 = (LinearLayout)localView.findViewById(2131361880);
        localLinearLayout1.removeView(localView.findViewById(2131361841));
        localLinearLayout1.addView(this.m_titleView, 0, new ViewGroup.LayoutParams(-1, -2));
        break;
        label292: if (this.m_contentView == null)
          break label110;
        LinearLayout localLinearLayout2 = (LinearLayout)localView.findViewById(2131361880);
        localLinearLayout2.removeAllViews();
        localLinearLayout2.addView(this.m_contentView, new ViewGroup.LayoutParams(-1, -2));
        break label110;
        label337: ((Button)localView.findViewById(2131362128)).setVisibility(8);
        break label158;
        label354: ((Button)localView.findViewById(2131362129)).setVisibility(8);
      }
    }

    public Builder setCancelable(boolean paramBoolean)
    {
      this.m_cancelable = paramBoolean;
      return this;
    }

    public Builder setContentView(View paramView)
    {
      this.m_contentView = paramView;
      return this;
    }

    public Builder setCustomeTitleView(View paramView)
    {
      this.m_titleView = paramView;
      return this;
    }

    public Builder setMessage(int paramInt)
    {
      this.m_message = ((String)this.m_context.getText(paramInt));
      return this;
    }

    public Builder setMessage(CharSequence paramCharSequence)
    {
      this.m_message = paramCharSequence;
      return this;
    }

    public Builder setMessage(String paramString)
    {
      this.m_message = paramString;
      return this;
    }

    public Builder setNegativeButton(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m_negativeButton = ((String)this.m_context.getText(paramInt));
      this.m_negativeButtonClickListener = paramOnClickListener;
      return this;
    }

    public Builder setNegativeButton(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m_negativeButton = paramString;
      this.m_negativeButtonClickListener = paramOnClickListener;
      return this;
    }

    public Builder setPositiveButton(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m_positiveButton = ((String)this.m_context.getText(paramInt));
      this.m_positiveButtonClickListener = paramOnClickListener;
      return this;
    }

    public Builder setPositiveButton(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m_positiveButton = paramString;
      this.m_positiveButtonClickListener = paramOnClickListener;
      return this;
    }

    public Builder setTitle(String paramString)
    {
      this.m_title = paramString;
      return this;
    }

    public Builder setTtile(int paramInt)
    {
      this.m_title = ((String)this.m_context.getText(paramInt));
      return this;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.dialog.TangoDialog
 * JD-Core Version:    0.6.2
 */