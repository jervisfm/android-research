package com.sgiggle.production.factory.proximity;

import android.os.Build;
import com.sgiggle.production.factory.proximity.generic.GenericProximityFactory;
import com.sgiggle.production.factory.proximity.handler.AbstractProximityHandler;
import com.sgiggle.production.factory.proximity.htc.HTCProximityFactory;
import com.sgiggle.production.factory.proximity.samsung.SamsungProximityFactory;

public abstract class AbstractProximityFactory
{
  private static AbstractProximityFactory s_factory;

  public static AbstractProximityFactory getInstance()
  {
    String str;
    if (s_factory == null)
    {
      str = Build.MANUFACTURER;
      if ((str != null) && (str.length() != 0))
        break label31;
      s_factory = GenericProximityFactory.getInstance();
    }
    while (true)
    {
      return s_factory;
      label31: if (HTCProximityFactory.getManufacturer().compareToIgnoreCase(str) == 0)
        s_factory = HTCProximityFactory.getInstance();
      else if (SamsungProximityFactory.getManufacturer().compareToIgnoreCase(str) == 0)
        s_factory = SamsungProximityFactory.getInstance();
      else
        s_factory = GenericProximityFactory.getInstance();
    }
  }

  public static String getManufacturer()
  {
    return "";
  }

  public abstract AbstractProximityHandler getProximityHandler();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.factory.proximity.AbstractProximityFactory
 * JD-Core Version:    0.6.2
 */