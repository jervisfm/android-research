package com.sgiggle.production.factory.proximity.generic;

import com.sgiggle.production.factory.proximity.AbstractProximityFactory;
import com.sgiggle.production.factory.proximity.generic.handler.GenericProximityHandler;
import com.sgiggle.production.factory.proximity.handler.AbstractProximityHandler;

public class GenericProximityFactory extends AbstractProximityFactory
{
  private static GenericProximityFactory m_proximityFactory;
  private AbstractProximityHandler m_proximityHandler = new GenericProximityHandler();

  public static AbstractProximityFactory getInstance()
  {
    if (m_proximityFactory == null)
      m_proximityFactory = new GenericProximityFactory();
    return m_proximityFactory;
  }

  public static String getManufacturer()
  {
    return "";
  }

  public AbstractProximityHandler getProximityHandler()
  {
    return this.m_proximityHandler;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.factory.proximity.generic.GenericProximityFactory
 * JD-Core Version:    0.6.2
 */