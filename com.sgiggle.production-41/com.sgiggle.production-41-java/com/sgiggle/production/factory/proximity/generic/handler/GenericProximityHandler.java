package com.sgiggle.production.factory.proximity.generic.handler;

import android.app.Activity;
import android.view.View;
import com.sgiggle.production.factory.proximity.handler.AbstractProximityHandler;
import com.sgiggle.util.Log;

public class GenericProximityHandler extends AbstractProximityHandler
{
  private static final String TAG = "Tango.GenericProximityHandler";

  public void handleProximityFar(Activity paramActivity)
  {
    paramActivity.findViewById(2131361821).setClickable(true);
    paramActivity.findViewById(2131361820).setClickable(true);
    paramActivity.findViewById(2131361818).setClickable(true);
    paramActivity.findViewById(2131361819).setClickable(true);
    Log.i("Tango.GenericProximityHandler", "Unlocking screen and disabling ignore cheek presses");
  }

  public void handleProximityNear(Activity paramActivity)
  {
    paramActivity.findViewById(2131361821).setClickable(false);
    paramActivity.findViewById(2131361820).setClickable(false);
    paramActivity.findViewById(2131361818).setClickable(false);
    paramActivity.findViewById(2131361819).setClickable(false);
    Log.i("Tango.GenericProximityHandler", "Locking screen to ignore cheek presses");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.factory.proximity.generic.handler.GenericProximityHandler
 * JD-Core Version:    0.6.2
 */