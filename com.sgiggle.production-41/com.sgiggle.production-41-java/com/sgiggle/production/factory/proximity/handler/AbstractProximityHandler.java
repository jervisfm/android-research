package com.sgiggle.production.factory.proximity.handler;

import android.app.Activity;

public abstract class AbstractProximityHandler
{
  private static final long THRESHOLD = 500L;
  private long m_timestampCurrent = System.currentTimeMillis();
  private long m_timestampOld = System.currentTimeMillis();

  public abstract void handleProximityFar(Activity paramActivity);

  public abstract void handleProximityNear(Activity paramActivity);

  public boolean throttle()
  {
    return this.m_timestampCurrent - this.m_timestampOld <= 500L;
  }

  public void updateTimestamp()
  {
    this.m_timestampOld = this.m_timestampCurrent;
    this.m_timestampCurrent = System.currentTimeMillis();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.factory.proximity.handler.AbstractProximityHandler
 * JD-Core Version:    0.6.2
 */