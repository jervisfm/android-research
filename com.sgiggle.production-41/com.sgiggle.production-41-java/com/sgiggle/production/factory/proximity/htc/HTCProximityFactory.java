package com.sgiggle.production.factory.proximity.htc;

import android.os.Build;
import com.sgiggle.production.factory.proximity.AbstractProximityFactory;
import com.sgiggle.production.factory.proximity.generic.handler.GenericProximityHandler;
import com.sgiggle.production.factory.proximity.handler.AbstractProximityHandler;
import com.sgiggle.production.factory.proximity.htc.handler.HTCProximityHandler;

public class HTCProximityFactory extends AbstractProximityFactory
{
  private static final String HTC_BLISS = "HTC Bliss";
  private static final String HTC_NEXUS_ONE = "Nexus One";
  private static final String HTC_RHYME = "HTC Rhyme";
  private static final String HTC_RUBY = "HTC Ruby";
  private static final String HTC_RUNNYMEDE = "HTC Runnymede";
  private static final String HTC_SENSATION = "HTC Sensation";
  private static final String HTC_SENSATION_XL = "HTC Sensation XL";
  private static HTCProximityFactory m_proximityFactory;
  private AbstractProximityHandler m_proximityHandler;

  public static AbstractProximityFactory getInstance()
  {
    if (m_proximityFactory == null)
      m_proximityFactory = new HTCProximityFactory();
    return m_proximityFactory;
  }

  public static String getManufacturer()
  {
    return "HTC";
  }

  public AbstractProximityHandler getProximityHandler()
  {
    String str = Build.MODEL;
    if (this.m_proximityHandler != null)
      return this.m_proximityHandler;
    if ((str == null) || (str.length() == 0))
      this.m_proximityHandler = new GenericProximityHandler();
    while (true)
    {
      return this.m_proximityHandler;
      if (str.startsWith("HTC Sensation"))
        this.m_proximityHandler = new HTCProximityHandler();
      else if (str.startsWith("HTC Ruby"))
        this.m_proximityHandler = new HTCProximityHandler();
      else if (str.startsWith("Nexus One"))
        this.m_proximityHandler = new HTCProximityHandler();
      else if (str.startsWith("HTC Bliss"))
        this.m_proximityHandler = new HTCProximityHandler();
      else if (str.startsWith("HTC Rhyme"))
        this.m_proximityHandler = new HTCProximityHandler();
      else if (str.startsWith("HTC Sensation XL"))
        this.m_proximityHandler = new HTCProximityHandler();
      else if (str.startsWith("HTC Runnymede"))
        this.m_proximityHandler = new HTCProximityHandler();
      else
        this.m_proximityHandler = new GenericProximityHandler();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.factory.proximity.htc.HTCProximityFactory
 * JD-Core Version:    0.6.2
 */