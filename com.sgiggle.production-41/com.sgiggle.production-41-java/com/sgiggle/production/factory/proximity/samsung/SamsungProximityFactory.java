package com.sgiggle.production.factory.proximity.samsung;

import android.os.Build;
import com.sgiggle.production.factory.proximity.AbstractProximityFactory;
import com.sgiggle.production.factory.proximity.generic.handler.GenericProximityHandler;
import com.sgiggle.production.factory.proximity.handler.AbstractProximityHandler;
import com.sgiggle.production.factory.proximity.samsung.handler.SamsungProximityHandler;
import com.sgiggle.util.Log;

public class SamsungProximityFactory extends AbstractProximityFactory
{
  private static final String SAMSUNG_GT_I9103 = "GT-I9103";
  private static final String SAMSUNG_NEXUS_S = "Nexus S";
  private static final String TAG = "Tango.SamsungProximityFactory";
  private static SamsungProximityFactory m_proximityFactory;
  private AbstractProximityHandler m_proximityHandler;

  public static AbstractProximityFactory getInstance()
  {
    if (m_proximityFactory == null)
      m_proximityFactory = new SamsungProximityFactory();
    return m_proximityFactory;
  }

  public static String getManufacturer()
  {
    return "Samsung";
  }

  public AbstractProximityHandler getProximityHandler()
  {
    String str = Build.MODEL;
    Log.d("Tango.SamsungProximityFactory", "Model: " + str);
    if (this.m_proximityHandler != null)
      return this.m_proximityHandler;
    if ("Nexus S".compareToIgnoreCase(str) == 0)
      this.m_proximityHandler = new SamsungProximityHandler();
    while (true)
    {
      return this.m_proximityHandler;
      if ("GT-I9103".compareToIgnoreCase(str) == 0)
        this.m_proximityHandler = new SamsungProximityHandler();
      else
        this.m_proximityHandler = new GenericProximityHandler();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.factory.proximity.samsung.SamsungProximityFactory
 * JD-Core Version:    0.6.2
 */