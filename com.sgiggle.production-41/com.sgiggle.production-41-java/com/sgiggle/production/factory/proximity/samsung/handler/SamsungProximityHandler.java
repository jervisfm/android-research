package com.sgiggle.production.factory.proximity.samsung.handler;

import android.app.Activity;
import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import com.sgiggle.production.factory.proximity.handler.AbstractProximityHandler;
import com.sgiggle.util.Log;

public class SamsungProximityHandler extends AbstractProximityHandler
{
  private static final String TAG = "Tango.SamsungProximityHandler";
  private PowerManager.WakeLock m_proximityWakeLock;

  private void createNewWakeLock(Activity paramActivity)
  {
    if (this.m_proximityWakeLock == null)
    {
      Log.d("Tango.SamsungProximityHandler", "Creating new wake lock");
      this.m_proximityWakeLock = ((PowerManager)paramActivity.getApplicationContext().getSystemService("power")).newWakeLock(32, "Tango.SamsungProximityHandler");
    }
  }

  public void handleProximityFar(Activity paramActivity)
  {
    createNewWakeLock(paramActivity);
    updateTimestamp();
    if ((this.m_proximityWakeLock.isHeld()) && (!throttle()))
    {
      this.m_proximityWakeLock.release();
      Log.i("Tango.SamsungProximityHandler", "Released proximity wake lock");
    }
  }

  public void handleProximityNear(Activity paramActivity)
  {
    createNewWakeLock(paramActivity);
    updateTimestamp();
    if (!this.m_proximityWakeLock.isHeld())
    {
      this.m_proximityWakeLock.acquire();
      Log.i("Tango.SamsungProximityHandler", "Acquired proximity wake lock");
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.factory.proximity.samsung.handler.SamsungProximityHandler
 * JD-Core Version:    0.6.2
 */