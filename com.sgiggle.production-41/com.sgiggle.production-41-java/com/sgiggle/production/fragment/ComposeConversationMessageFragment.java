package com.sgiggle.production.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import com.sgiggle.media_engine.MediaEngineMessage.ComposingConversationMessage;
import com.sgiggle.media_engine.MediaEngineMessage.OpenConversationEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.Utils;
import com.sgiggle.production.adapter.VGoodSelectorAdapter;
import com.sgiggle.production.controller.ConversationMessageController.CreateMessageAction;
import com.sgiggle.production.widget.HorizontalListView;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.ConversationPayload;
import com.sgiggle.xmpp.SessionMessages.VGoodBundle;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic;
import java.util.List;

public class ComposeConversationMessageFragment extends Fragment
  implements View.OnClickListener, TextView.OnEditorActionListener, AdapterView.OnItemClickListener, View.OnFocusChangeListener
{
  private static final String TAG = "Tango.ComposeConversationMessageFragment";
  private ImageButton m_addMediaButton;
  Animation.AnimationListener m_inAnimationListener = new Animation.AnimationListener()
  {
    public void onAnimationEnd(Animation paramAnonymousAnimation)
    {
      if (ComposeConversationMessageFragment.this.m_switcher.getCurrentView().getId() == 2131361874)
        ComposeConversationMessageFragment.this.m_text.requestFocus();
    }

    public void onAnimationRepeat(Animation paramAnonymousAnimation)
    {
    }

    public void onAnimationStart(Animation paramAnonymousAnimation)
    {
    }
  };
  private ComposeConversationMessageFragmentListener m_listener;
  private HorizontalListView m_selector;
  private ImageButton m_sendButton;
  private ViewSwitcher m_switcher;
  private TextView m_text;
  private ImageButton m_vgoodButton;

  private void handleOpenConversationEvent(MediaEngineMessage.OpenConversationEvent paramOpenConversationEvent)
  {
    int i = ((SessionMessages.ConversationPayload)paramOpenConversationEvent.payload()).getEmptySlotCount();
    setSelectorVGoods(((SessionMessages.ConversationPayload)paramOpenConversationEvent.payload()).getVgoodBundleList(), i);
  }

  private void sendComposingConversationMessage()
  {
    Log.d("Tango.ComposeConversationMessageFragment", "sendComposingConversationMessage");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ComposingConversationMessage());
  }

  private void setSelectorVGoods(List<SessionMessages.VGoodBundle> paramList, int paramInt)
  {
    VGoodSelectorAdapter localVGoodSelectorAdapter = new VGoodSelectorAdapter(getActivity());
    localVGoodSelectorAdapter.setData(paramList, false);
    if (paramInt > 4);
    for (int i = 4; ; i = paramInt)
    {
      localVGoodSelectorAdapter.setEmptyCount(i);
      this.m_selector.setAdapter(localVGoodSelectorAdapter);
      return;
    }
  }

  public void focusText()
  {
    Utils.focusTextViewAndShowKeyboard(getActivity(), this.m_text);
  }

  public void handleMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35270:
    }
    handleOpenConversationEvent((MediaEngineMessage.OpenConversationEvent)paramMessage);
  }

  public void hideIme()
  {
    Utils.hideKeyboard(getActivity(), this.m_text);
  }

  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    try
    {
      this.m_listener = ((ComposeConversationMessageFragmentListener)paramActivity);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      String str = "onAttach: " + paramActivity.toString() + " must implement ComposeConversationMessageFragmentListener";
      Log.e("Tango.ComposeConversationMessageFragment", str);
      throw new ClassCastException(str);
    }
  }

  public void onClick(View paramView)
  {
    if (paramView == this.m_sendButton)
      if (this.m_text.getText().length() > 0)
      {
        this.m_listener.onComposeActionStarted();
        ComposeConversationMessageFragmentListener localComposeConversationMessageFragmentListener = this.m_listener;
        SessionMessages.ConversationMessageType localConversationMessageType = SessionMessages.ConversationMessageType.TEXT_MESSAGE;
        ConversationMessageController.CreateMessageAction localCreateMessageAction = ConversationMessageController.CreateMessageAction.ACTION_NEW;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = this.m_text.getText().toString();
        localComposeConversationMessageFragmentListener.createNewMessage(localConversationMessageType, localCreateMessageAction, arrayOfObject);
        this.m_text.setText("");
      }
    do
    {
      return;
      if (paramView == this.m_vgoodButton)
      {
        if (this.m_selector.getAdapter().getCount() > 0)
        {
          hideIme();
          int i = this.m_switcher.getHeight();
          if (this.m_selector.getHeight() < i)
          {
            ViewGroup.LayoutParams localLayoutParams = this.m_selector.getLayoutParams();
            localLayoutParams.height = i;
            this.m_selector.setLayoutParams(localLayoutParams);
          }
          this.m_switcher.showNext();
        }
        while (true)
        {
          this.m_listener.onComposeActionStarted();
          return;
          Toast.makeText(getActivity(), 2131296604, 0).show();
        }
      }
    }
    while (paramView != this.m_addMediaButton);
    this.m_listener.onComposeActionStarted();
    this.m_listener.onAddMediaClicked();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903057, paramViewGroup, false);
    this.m_text = ((TextView)localView.findViewById(2131361877));
    this.m_text.setOnEditorActionListener(this);
    this.m_text.setOnFocusChangeListener(this);
    this.m_sendButton = ((ImageButton)localView.findViewById(2131361878));
    this.m_sendButton.setOnClickListener(this);
    this.m_addMediaButton = ((ImageButton)localView.findViewById(2131361875));
    this.m_addMediaButton.setOnClickListener(this);
    this.m_vgoodButton = ((ImageButton)localView.findViewById(2131361876));
    this.m_vgoodButton.setOnClickListener(this);
    this.m_switcher = ((ViewSwitcher)localView.findViewById(2131361873));
    this.m_switcher.getInAnimation().setAnimationListener(this.m_inAnimationListener);
    this.m_selector = ((HorizontalListView)localView.findViewById(2131361879));
    this.m_selector.setOnItemClickListener(this);
    return localView;
  }

  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default:
      return false;
    case 4:
    }
    this.m_sendButton.performClick();
    return true;
  }

  public void onFocusChange(View paramView, boolean paramBoolean)
  {
    if ((paramView == this.m_text) && (paramBoolean))
    {
      sendComposingConversationMessage();
      this.m_listener.onComposeActionStarted();
    }
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    SessionMessages.VGoodBundle localVGoodBundle = (SessionMessages.VGoodBundle)paramAdapterView.getItemAtPosition(paramInt);
    if (localVGoodBundle != null)
    {
      long l = localVGoodBundle.getCinematic().getAssetId();
      this.m_switcher.showNext();
      this.m_listener.onComposeActionStarted();
      ComposeConversationMessageFragmentListener localComposeConversationMessageFragmentListener = this.m_listener;
      SessionMessages.ConversationMessageType localConversationMessageType = SessionMessages.ConversationMessageType.VGOOD_MESSAGE;
      ConversationMessageController.CreateMessageAction localCreateMessageAction = ConversationMessageController.CreateMessageAction.ACTION_NEW;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Long.valueOf(l);
      localComposeConversationMessageFragmentListener.createNewMessage(localConversationMessageType, localCreateMessageAction, arrayOfObject);
      return;
    }
    Toast.makeText(getActivity(), 2131296535, 0).show();
  }

  public void onStart()
  {
    super.onStart();
    this.m_switcher.requestFocus();
  }

  public static abstract interface ComposeConversationMessageFragmentListener
  {
    public abstract void createNewMessage(SessionMessages.ConversationMessageType paramConversationMessageType, ConversationMessageController.CreateMessageAction paramCreateMessageAction, Object[] paramArrayOfObject);

    public abstract void onAddMediaClicked();

    public abstract void onComposeActionStarted();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.ComposeConversationMessageFragment
 * JD-Core Version:    0.6.2
 */