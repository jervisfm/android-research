package com.sgiggle.production.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.RetrieveOfflineMessageStatus;
import java.lang.ref.WeakReference;

public class ConnectionStatusFragment extends Fragment
{
  private static final int FRAGMENT_VISIBILITY_CHANGE_DELAY_MS = 1000;
  private static final int MSG_HIDE_FRAGMENT = 1;
  private static final int MSG_SHOW_FRAGMENT = 0;
  private static final String TAG = "Tango.ConnectionStatusFragment";
  private static ConnectionStatus s_connectionStatus = ConnectionStatus.CONNECTED;
  private Handler m_handler = new UiHandler(this);
  private View m_progressBar;
  private TextView m_text;

  public static void setConnectionStatus(ConnectionStatus paramConnectionStatus)
  {
    s_connectionStatus = paramConnectionStatus;
  }

  public static void setConnectionStatus(SessionMessages.RetrieveOfflineMessageStatus paramRetrieveOfflineMessageStatus)
  {
    Log.d("Tango.ConnectionStatusFragment", "setConnectionStatus: new status=" + paramRetrieveOfflineMessageStatus);
    ConnectionStatus localConnectionStatus = ConnectionStatus.CONNECTED;
    switch (1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$RetrieveOfflineMessageStatus[paramRetrieveOfflineMessageStatus.ordinal()])
    {
    default:
    case 1:
    case 2:
    case 3:
    case 4:
    }
    while (true)
    {
      setConnectionStatus(localConnectionStatus);
      return;
      localConnectionStatus = ConnectionStatus.CONNECTED;
      continue;
      localConnectionStatus = ConnectionStatus.DISCONNECTED;
      continue;
      localConnectionStatus = ConnectionStatus.CONNECTING;
    }
  }

  private void setVisible(boolean paramBoolean)
  {
    Log.d("Tango.ConnectionStatusFragment", "setVisible: show=" + paramBoolean);
    if ((getView() == null) || (getActivity() == null) || (getActivity().isFinishing()))
    {
      Log.d("Tango.ConnectionStatusFragment", "setVisible: won't change visibility, activity is finishing or view was not created yet.");
      return;
    }
    FragmentTransaction localFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
    localFragmentTransaction.setCustomAnimations(2130968588, 2130968593);
    if (paramBoolean)
      localFragmentTransaction.show(this);
    while (true)
    {
      localFragmentTransaction.commitAllowingStateLoss();
      return;
      localFragmentTransaction.hide(this);
    }
  }

  private void setVisibleDelayed(boolean paramBoolean)
  {
    Log.d("Tango.ConnectionStatusFragment", "setVisibleDelayed: show=" + paramBoolean);
    int i;
    if (paramBoolean)
    {
      i = 1;
      if (!paramBoolean)
        break label77;
    }
    label77: for (int j = 0; ; j = 1)
    {
      this.m_handler.removeMessages(i);
      if (!this.m_handler.hasMessages(j))
        this.m_handler.sendEmptyMessageDelayed(j, 1000L);
      return;
      i = 0;
      break;
    }
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Log.d("Tango.ConnectionStatusFragment", "onCreateView");
    View localView = paramLayoutInflater.inflate(2130903058, paramViewGroup, false);
    this.m_text = ((TextView)localView.findViewById(2131361882));
    this.m_progressBar = localView.findViewById(2131361881);
    return localView;
  }

  public void onPause()
  {
    super.onPause();
    this.m_handler.removeMessages(1);
    this.m_handler.removeMessages(0);
  }

  public void onResume()
  {
    super.onResume();
    setVisible(false);
    updateConnectionStatus();
  }

  public void updateConnectionStatus()
  {
    if ((getView() == null) || (getActivity() == null) || (getActivity().isFinishing()))
    {
      Log.d("Tango.ConnectionStatusFragment", "setConnectionStatus: won't show fragment, activity is finishing or view was not created yet.");
      return;
    }
    int i;
    int j;
    int k;
    switch (1.$SwitchMap$com$sgiggle$production$fragment$ConnectionStatusFragment$ConnectionStatus[s_connectionStatus.ordinal()])
    {
    default:
      throw new UnsupportedOperationException("Connection status not supported: " + s_connectionStatus);
    case 1:
      i = 2131296618;
      j = 1;
      if (i > 0)
      {
        this.m_text.setText(i);
        View localView = this.m_progressBar;
        if (j != 0)
        {
          k = 0;
          label127: localView.setVisibility(k);
        }
      }
      else
      {
        if (s_connectionStatus == ConnectionStatus.CONNECTED)
          break label175;
      }
      break;
    case 2:
    case 3:
    }
    label175: for (boolean bool = true; ; bool = false)
    {
      setVisibleDelayed(bool);
      return;
      i = -1;
      j = 0;
      break;
      i = 2131296619;
      j = 0;
      break;
      k = 8;
      break label127;
    }
  }

  public static enum ConnectionStatus
  {
    static
    {
      CONNECTED = new ConnectionStatus("CONNECTED", 1);
      DISCONNECTED = new ConnectionStatus("DISCONNECTED", 2);
      ConnectionStatus[] arrayOfConnectionStatus = new ConnectionStatus[3];
      arrayOfConnectionStatus[0] = CONNECTING;
      arrayOfConnectionStatus[1] = CONNECTED;
      arrayOfConnectionStatus[2] = DISCONNECTED;
    }
  }

  private static class UiHandler extends Handler
  {
    WeakReference<ConnectionStatusFragment> m_fragment;

    public UiHandler(ConnectionStatusFragment paramConnectionStatusFragment)
    {
      this.m_fragment = new WeakReference(paramConnectionStatusFragment);
    }

    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default:
        throw new UnsupportedOperationException("Unhandled message type=" + paramMessage.what);
      case 0:
        if (this.m_fragment.get() != null)
          ((ConnectionStatusFragment)this.m_fragment.get()).setVisible(true);
        break;
      case 1:
      }
      do
        return;
      while (this.m_fragment.get() == null);
      ((ConnectionStatusFragment)this.m_fragment.get()).setVisible(false);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.ConnectionStatusFragment
 * JD-Core Version:    0.6.2
 */