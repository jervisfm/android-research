package com.sgiggle.production.fragment;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.ConversationMessageSendStatusEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DeleteSingleConversationMessageEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayConversationMessageEvent;
import com.sgiggle.media_engine.MediaEngineMessage.FinishSMSComposeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ForwardMessageResultEvent;
import com.sgiggle.media_engine.MediaEngineMessage.MessageUpdateReadStatusEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ShowMoreMessageEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ShowMoreMessageMessage;
import com.sgiggle.media_engine.MediaEngineMessage.StartSMSComposeEvent;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.CallHandler;
import com.sgiggle.production.CallHandler.VideoMode;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.Utils;
import com.sgiggle.production.adapter.ConversationMessageListAdapter;
import com.sgiggle.production.adapter.ParticipantListAdapter;
import com.sgiggle.production.controller.ConversationController;
import com.sgiggle.production.controller.ConversationMessageController;
import com.sgiggle.production.controller.ConversationMessageController.CreateMessageAction;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationDetail;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.widget.HorizontalListView;
import com.sgiggle.production.widget.ScrollToRefreshListView;
import com.sgiggle.production.widget.ScrollToRefreshListView.ScrollToRefreshListViewListener;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessagePayload;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.ConversationPayload;
import com.sgiggle.xmpp.SessionMessages.ConversationPayload.OpenConversationContext;
import com.sgiggle.xmpp.SessionMessages.ForwardMessageResultPayload;
import com.sgiggle.xmpp.SessionMessages.SMSComposerPayload;
import com.sgiggle.xmpp.SessionMessages.SMSComposerType;
import com.sgiggle.xmpp.SessionMessages.SwapReadStatusesPayload;
import java.util.List;

public class ConversationDetailFragment extends ListFragment
  implements ScrollToRefreshListView.ScrollToRefreshListViewListener, View.OnClickListener
{
  private static final String KEY_CONVERSATION_ID = "KEY_CONVERSATION_ID";
  private static final int MSG_SHOW_MORE = 0;
  private static final int MSG_SHOW_MORE_DELAY_MS = 1000;
  private static final int MSG_START_CONTINUOUS_ANIMATIONS = 1;
  private static final int MSG_START_CONTINUOUS_ANIMATIONS_DELAY_MS = 500;
  private static final int REQUEST_COMPOSE_SMS = 1;
  private static final String TAG = "Tango.ConversationDetailFragment";
  private ConversationController m_conversationController;
  private ConversationDetail m_conversationDetail;
  private ConversationMessageListAdapter m_conversationDetailAdapter;
  private String m_conversationId;
  private Toast m_deliveryErrorToast;
  private View m_emptyViewArrowsWrapper;
  private View m_emptyViewWrapper;
  private Handler m_handler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        throw new UnsupportedOperationException("handleMessage: unsupported message=" + paramAnonymousMessage.what);
      case 0:
        ConversationDetailFragment.this.postShowMoreMessageMessage();
        return;
      case 1:
      }
      ConversationDetailFragment.this.startContinuousAnimations();
    }
  };
  private boolean m_isStarted = false;
  private int m_latestConversationPayloadHashCode = 0;
  private ScrollToRefreshListView m_listView;
  private ConversationDetailFragmentListener m_listener;
  private HorizontalListView m_participantList;
  private ParticipantListAdapter m_participantListAdapter;
  private View m_participantListBar;
  private View m_progressView;
  private ConversationMessage m_selectedConversationMessage = null;
  private View m_tipComposeBubble;
  private TextView m_tipComposeText;
  private View m_tipWrapper;

  static
  {
    if (!ConversationDetailFragment.class.desiredAssertionStatus());
    for (boolean bool = true; ; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }

  private boolean fill(ConversationDetail paramConversationDetail)
  {
    Log.d("Tango.ConversationDetailFragment", "ConversationDetailFragment::fill(): data = " + paramConversationDetail);
    this.m_handler.removeMessages(0);
    if (this.m_conversationDetail != null)
    {
      ConversationMessage localConversationMessage1 = this.m_conversationDetail.getMostRecentMessage();
      ConversationMessage localConversationMessage2 = paramConversationDetail.getMostRecentMessage();
      if ((localConversationMessage2 == null) || (!localConversationMessage2.isFromSelf()) || ((localConversationMessage1 != null) && (localConversationMessage1.getMessageId() == localConversationMessage2.getMessageId())));
    }
    for (boolean bool = true; ; bool = false)
    {
      this.m_conversationDetail = paramConversationDetail;
      if (paramConversationDetail == null)
      {
        this.m_conversationDetailAdapter.clearData();
        this.m_participantListAdapter.setData(null);
      }
      while (true)
      {
        updateParticipantListBarVisibility();
        updateMenuItemsVisibility();
        this.m_listView.resetRefreshState(this.m_conversationDetail.getMoreMessageAvailable());
        return bool;
        this.m_conversationDetailAdapter.setData(paramConversationDetail.getConversationMessages(), paramConversationDetail.isGroupConversation());
        this.m_participantListAdapter.clear();
        if (paramConversationDetail.isGroupConversation())
        {
          getActivity().setTitle(2131296576);
          this.m_participantListAdapter.add(paramConversationDetail.getMyself());
          this.m_participantListAdapter.addData(paramConversationDetail.getConversationPeers());
        }
        else
        {
          ConversationContact localConversationContact = (ConversationContact)paramConversationDetail.getConversationPeers().get(0);
          getActivity().setTitle(localConversationContact.getDisplayName());
        }
      }
    }
  }

  private void handleConversationMessageSendStatusEvent(SessionMessages.ConversationMessage paramConversationMessage)
  {
    updateExistingConversationMessage(paramConversationMessage, true);
  }

  private void handleDeleteSingleConversationMessageEvent(SessionMessages.ConversationMessage paramConversationMessage)
  {
    ConversationMessage localConversationMessage = this.m_conversationDetail.deleteConversationMessage(paramConversationMessage.getMessageId());
    if (localConversationMessage == null)
      Log.w("Tango.ConversationDetailFragment", "handleDeleteSingleConversationMessageEvent: could not delete message with ID" + paramConversationMessage.getMessageId());
    do
    {
      return;
      this.m_conversationDetailAdapter.remove(localConversationMessage);
    }
    while (!this.m_conversationDetail.isEmpty());
    updateEmptyView();
  }

  private void handleDisplayConversationMessage(SessionMessages.ConversationMessage paramConversationMessage)
  {
    if (paramConversationMessage.getIsForUpdate())
      updateExistingConversationMessage(paramConversationMessage, false);
    ConversationMessage localConversationMessage;
    do
    {
      return;
      Log.d("Tango.ConversationDetailFragment", "handleDisplayConversationMessage: new message");
      localConversationMessage = this.m_conversationDetail.addConversationMessage(paramConversationMessage);
      this.m_conversationDetailAdapter.add(localConversationMessage);
      if (isTipLayerShown())
        hideTipLayer(true);
      onMessageStatusChanged(localConversationMessage);
    }
    while (!localConversationMessage.isFromSelf());
    onNewMessageFromSelf();
  }

  private void handleForwardMessageResultEvent(MediaEngineMessage.ForwardMessageResultEvent paramForwardMessageResultEvent)
  {
    SessionMessages.ForwardMessageResultPayload localForwardMessageResultPayload = (SessionMessages.ForwardMessageResultPayload)paramForwardMessageResultEvent.payload();
    switch (3.$SwitchMap$com$sgiggle$xmpp$SessionMessages$ForwardMessageResultType[localForwardMessageResultPayload.getType().ordinal()])
    {
    default:
      if (!$assertionsDisabled)
        throw new AssertionError();
    case 1:
      onForwardError();
      return;
    case 2:
      onForwardSuccess();
      return;
    case 3:
    }
    onForwardBySMSNeeded(localForwardMessageResultPayload);
  }

  private void handleOpenConversationPayload(SessionMessages.ConversationPayload paramConversationPayload)
  {
    Log.d("Tango.ConversationDetailFragment", "handleOpenConversationPayload(), conversationId=" + paramConversationPayload.getConversationId());
    boolean bool1 = fill(new ConversationDetail(paramConversationPayload));
    if ((paramConversationPayload.hasOpenConversationContext()) && (paramConversationPayload.getOpenConversationContext() == SessionMessages.ConversationPayload.OpenConversationContext.FAILED_OUTGOING_CALL));
    for (boolean bool2 = true; ; bool2 = false)
    {
      makeEmptyViewTransparent(bool2);
      updateTipLayer(bool2, true);
      if (bool1)
        onNewMessageFromSelf();
      return;
    }
  }

  private void handleReadStatusUpdateEvent(SessionMessages.ConversationMessage paramConversationMessage, int paramInt)
  {
    hideExistingMessageStatus(paramInt);
    updateExistingConversationMessage(paramConversationMessage, true);
  }

  private void handleShowMoreMessageEvent(SessionMessages.ConversationPayload paramConversationPayload)
  {
    Log.d("Tango.ConversationDetailFragment", "handleShowMoreMessageEvent()");
    this.m_conversationDetail.setMoreMessageAvailable(paramConversationPayload.getMoreMessageAvailable());
    List localList = paramConversationPayload.getMessageList();
    int i = localList.size();
    if (i > 0)
    {
      this.m_conversationDetail.addConversationMessagesAtFront(localList);
      this.m_conversationDetailAdapter.setData(this.m_conversationDetail.getConversationMessages(), this.m_conversationDetail.isGroupConversation());
    }
    this.m_listView.onRefreshDone(i, this.m_conversationDetail.getMoreMessageAvailable());
  }

  private void handleStartSMSComposeEvent(MediaEngineMessage.StartSMSComposeEvent paramStartSMSComposeEvent)
  {
    Intent localIntent = Utils.getSmsIntent(Utils.getSmsAddressListFromContactList(((SessionMessages.SMSComposerPayload)paramStartSMSComposeEvent.payload()).getReceiversList()), ((SessionMessages.SMSComposerPayload)paramStartSMSComposeEvent.payload()).getInfo());
    try
    {
      startActivityForResult(localIntent, 1);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Log.w("Tango.ConversationDetailFragment", "No activity was found for ACTION_VIEW (for sending SMS)");
    }
  }

  private boolean hasData()
  {
    return this.m_conversationDetail != null;
  }

  private void hideExistingMessageStatus(int paramInt)
  {
    ConversationMessage localConversationMessage = this.m_conversationDetail.updateMessage(paramInt, SessionMessages.ConversationMessageSendStatus.STATUS_READ);
    if (localConversationMessage == null)
    {
      Log.e("Tango.ConversationDetailFragment", "handleConversationMessageStatusPayload: message not found for Conversation #" + this.m_conversationDetail.getConversationId() + ", Message #" + paramInt);
      return;
    }
    this.m_conversationDetailAdapter.notifyDataSetChanged();
    onMessageStatusChanged(localConversationMessage);
  }

  public static ConversationDetailFragment newInstance(String paramString)
  {
    ConversationDetailFragment localConversationDetailFragment = new ConversationDetailFragment();
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("KEY_CONVERSATION_ID", paramString);
    localConversationDetailFragment.setArguments(localBundle);
    return localConversationDetailFragment;
  }

  private void onForwardBySMSNeeded(SessionMessages.ForwardMessageResultPayload paramForwardMessageResultPayload)
  {
    boolean bool = Utils.isSmsIntentAvailable(getActivity());
    String str1 = getResources().getString(2131296594);
    String str2;
    String str3;
    if (bool)
    {
      str2 = getResources().getString(2131296595);
      str3 = paramForwardMessageResultPayload.getMessage().getText();
      if ((str3 != null) && (str3.length() != 0))
        break label109;
    }
    label109: for (String str4 = paramForwardMessageResultPayload.getMessage().getTextIfNotSupport(); ; str4 = str3)
    {
      SessionMessages.SMSComposerType localSMSComposerType = SessionMessages.SMSComposerType.SMS_COMPOSER_FORWARD_MESSAGE;
      List localList = paramForwardMessageResultPayload.getSmsContactsList();
      TangoApp.getInstance().showQuerySendSMSDialog(bool, str1, str2, str4, localSMSComposerType, localList);
      return;
      str2 = getResources().getString(2131296596);
      break;
    }
  }

  private void onForwardError()
  {
    Toast.makeText(getActivity(), 2131296613, 1).show();
  }

  private void onForwardSuccess()
  {
    Toast.makeText(getActivity(), 2131296612, 1).show();
  }

  private void onMessageStatusChanged(ConversationMessage paramConversationMessage)
  {
    this.m_conversationController.getMessageController(paramConversationMessage.getType()).onMessageStatusChanged(paramConversationMessage);
  }

  private void onNewMessageFromSelf()
  {
    if (this.m_listView != null)
      this.m_listView.setSelectionToBottom();
  }

  private void postFinishSMSComposeMessage(boolean paramBoolean)
  {
    MediaEngineMessage.FinishSMSComposeMessage localFinishSMSComposeMessage = new MediaEngineMessage.FinishSMSComposeMessage(paramBoolean);
    MessageRouter.getInstance().postMessage("jingle", localFinishSMSComposeMessage);
  }

  private void postShowMoreMessageMessage()
  {
    int i = this.m_conversationDetail.getOldestMessageId();
    if (i == -1)
    {
      Log.w("Tango.ConversationDetailFragment", "Cannot post ShowMoreMessageMessage, previous message ID is invalid: " + i);
      return;
    }
    MediaEngineMessage.ShowMoreMessageMessage localShowMoreMessageMessage = new MediaEngineMessage.ShowMoreMessageMessage(this.m_conversationDetail.getConversationId(), this.m_conversationDetail.getOldestMessageId());
    MessageRouter.getInstance().postMessage("jingle", localShowMoreMessageMessage);
  }

  private void showProgressView(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.m_listView.setVisibility(4);
      this.m_progressView.setVisibility(0);
      updateParticipantListBarVisibility();
      return;
    }
    this.m_participantListBar.setVisibility(8);
    this.m_progressView.setVisibility(4);
    this.m_listView.setVisibility(0);
  }

  private void startContinuousAnimations()
  {
    if ((getView() == null) || (getActivity() == null) || (getActivity().isFinishing()))
      Log.d("Tango.ConversationDetailFragment", "startContinuousAnimations: won't start animations, activity is finishing or view was not created yet.");
    while (this.m_tipComposeBubble.getVisibility() != 0)
      return;
    Animation localAnimation = AnimationUtils.loadAnimation(TangoApp.getInstance().getApplicationContext(), 2130968581);
    this.m_tipComposeBubble.startAnimation(localAnimation);
  }

  private void startContinuousAnimationsDelayed()
  {
    if ((isResumed()) && (this.m_listener != null) && (this.m_listener.isComposeEnabled()) && (!this.m_handler.hasMessages(1)))
      this.m_handler.sendEmptyMessageDelayed(1, 500L);
  }

  private void stopContinuousAnimations()
  {
    if ((getView() == null) || (getActivity() == null) || (getActivity().isFinishing()))
    {
      Log.d("Tango.ConversationDetailFragment", "stopContinuousAnimations: won't stop animations, activity is finishing or view was not created yet.");
      return;
    }
    this.m_tipComposeBubble.setAnimation(null);
  }

  private void updateExistingConversationMessage(SessionMessages.ConversationMessage paramConversationMessage, boolean paramBoolean)
  {
    ConversationMessage localConversationMessage = this.m_conversationDetail.updateMessage(paramConversationMessage);
    if (localConversationMessage == null)
      Log.e("Tango.ConversationDetailFragment", "handleConversationMessageStatusPayload: message not found for Conversation #" + this.m_conversationDetail.getConversationId() + ", Message #" + paramConversationMessage.getMessageId());
    do
    {
      return;
      this.m_conversationDetailAdapter.notifyDataSetChanged();
    }
    while (!paramBoolean);
    onMessageStatusChanged(localConversationMessage);
  }

  private void updateMenuItemsVisibility()
  {
    if ((this.m_conversationDetail == null) || (this.m_conversationDetail.isSystemAccountConversation()))
    {
      this.m_listener.hideMenuItems();
      return;
    }
    this.m_listener.updateMenuItemsVisibility(this.m_conversationDetail.isGroupConversation());
  }

  private void updateParticipantListBarVisibility()
  {
    if ((this.m_participantListAdapter == null) || (this.m_participantListAdapter.isEmpty()))
    {
      this.m_participantListBar.setVisibility(8);
      return;
    }
    this.m_participantListBar.setVisibility(0);
  }

  private void updateTipLayer(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramBoolean1)
    {
      localTextView = this.m_tipComposeText;
      localResources = getResources();
      arrayOfObject = new Object[1];
      arrayOfObject[0] = this.m_conversationDetail.getFirstPeer().getDisplayNameShort();
      localTextView.setText(localResources.getString(2131296575, arrayOfObject));
      startContinuousAnimationsDelayed();
      this.m_tipWrapper.setVisibility(0);
    }
    while (this.m_tipWrapper.getVisibility() == 8)
    {
      TextView localTextView;
      Resources localResources;
      Object[] arrayOfObject;
      return;
    }
    if (paramBoolean2)
    {
      Animation localAnimation = AnimationUtils.loadAnimation(TangoApp.getInstance().getApplicationContext(), 2130968576);
      localAnimation.setAnimationListener(new Animation.AnimationListener()
      {
        public void onAnimationEnd(Animation paramAnonymousAnimation)
        {
          ConversationDetailFragment.this.stopContinuousAnimations();
          ConversationDetailFragment.this.m_tipWrapper.setVisibility(8);
          ConversationDetailFragment.this.makeEmptyViewTransparent(false);
        }

        public void onAnimationRepeat(Animation paramAnonymousAnimation)
        {
        }

        public void onAnimationStart(Animation paramAnonymousAnimation)
        {
        }
      });
      this.m_tipWrapper.startAnimation(localAnimation);
      return;
    }
    this.m_tipWrapper.setAnimation(null);
    this.m_tipWrapper.setVisibility(8);
    stopContinuousAnimations();
  }

  public void createNewMessage(SessionMessages.ConversationMessageType paramConversationMessageType, ConversationMessageController.CreateMessageAction paramCreateMessageAction, Object[] paramArrayOfObject)
  {
    this.m_conversationController.getMessageController(paramConversationMessageType).createNewMessage(paramCreateMessageAction, paramArrayOfObject);
  }

  public ConversationDetail getConversationDetail()
  {
    return this.m_conversationDetail;
  }

  public boolean handleMessage(com.sgiggle.messaging.Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      if (this.m_conversationController != null)
        return this.m_conversationController.broadcastMessage(paramMessage);
      break;
    case 35271:
      SessionMessages.ConversationMessage localConversationMessage4 = ((SessionMessages.ConversationMessagePayload)((MediaEngineMessage.DisplayConversationMessageEvent)paramMessage).payload()).getMessage();
      if (localConversationMessage4.getConversationId().equals(this.m_conversationId))
      {
        handleDisplayConversationMessage(localConversationMessage4);
        return true;
      }
      Log.d("Tango.ConversationDetailFragment", "handleMessage: Discarding DISPLAY_CONVERSATION_MESSAGE_EVENT, for conv #" + localConversationMessage4.getConversationId() + ", current is #" + this.m_conversationId);
      return false;
    case 35276:
      SessionMessages.ConversationMessage localConversationMessage3 = ((SessionMessages.ConversationMessagePayload)((MediaEngineMessage.ConversationMessageSendStatusEvent)paramMessage).payload()).getMessage();
      if (localConversationMessage3.getConversationId().equals(this.m_conversationId))
      {
        handleConversationMessageSendStatusEvent(localConversationMessage3);
        return true;
      }
      Log.d("Tango.ConversationDetailFragment", "handleMessage: Discarding CONVERSATION_MESSAGE_SEND_STATUS_EVENT, for conv #" + localConversationMessage3.getConversationId() + ", current is #" + this.m_conversationId);
      return false;
    case 35321:
      SessionMessages.ConversationMessage localConversationMessage2 = ((SessionMessages.SwapReadStatusesPayload)((MediaEngineMessage.MessageUpdateReadStatusEvent)paramMessage).payload()).getShowReadMessage();
      if (localConversationMessage2.getConversationId().equals(this.m_conversationId))
      {
        handleReadStatusUpdateEvent(localConversationMessage2, ((SessionMessages.SwapReadStatusesPayload)((MediaEngineMessage.MessageUpdateReadStatusEvent)paramMessage).payload()).getHideReadMessageId());
        return true;
      }
      Log.d("Tango.ConversationDetailFragment", "handleMessage: Discarding CONVERSATION_MESSAGE_SEND_STATUS_EVENT, for conv #" + localConversationMessage2.getConversationId() + ", current is #" + this.m_conversationId);
      return false;
    case 35277:
      SessionMessages.ConversationPayload localConversationPayload = (SessionMessages.ConversationPayload)((MediaEngineMessage.ShowMoreMessageEvent)paramMessage).payload();
      if (localConversationPayload.getConversationId().equals(this.m_conversationId))
      {
        handleShowMoreMessageEvent(localConversationPayload);
        return true;
      }
      Log.d("Tango.ConversationDetailFragment", "handleMessage: Discarding SHOW_MORE_MESSAGE_EVENT, for conv #" + localConversationPayload.getConversationId() + ", current is #" + this.m_conversationId);
      return false;
    case 35281:
      handleStartSMSComposeEvent((MediaEngineMessage.StartSMSComposeEvent)paramMessage);
      return false;
    case 35286:
      SessionMessages.ConversationMessage localConversationMessage1 = ((SessionMessages.ConversationMessagePayload)((MediaEngineMessage.DeleteSingleConversationMessageEvent)paramMessage).payload()).getMessage();
      if (localConversationMessage1.getConversationId().equals(this.m_conversationId))
      {
        handleDeleteSingleConversationMessageEvent(localConversationMessage1);
        return true;
      }
      Log.d("Tango.ConversationDetailFragment", "handleMessage: Discarding DELETE_SINGLE_CONVERSATION_MESSAGE_EVENT, for conv #" + localConversationMessage1.getConversationId() + ", current is #" + this.m_conversationId);
      return false;
    case 35280:
      handleForwardMessageResultEvent((MediaEngineMessage.ForwardMessageResultEvent)paramMessage);
      return true;
    }
    return false;
  }

  public void hideIme()
  {
    this.m_listener.hideIme();
  }

  public void hideTipLayer(boolean paramBoolean)
  {
    updateTipLayer(false, paramBoolean);
  }

  public boolean isCallEnabled()
  {
    return (this.m_conversationDetail != null) && (!this.m_conversationDetail.isSystemAccountConversation());
  }

  public boolean isTipLayerShown()
  {
    return (this.m_tipWrapper != null) && (this.m_tipWrapper.getVisibility() == 0);
  }

  public void makeEmptyViewTransparent(boolean paramBoolean)
  {
    updateEmptyView();
    View localView = this.m_emptyViewWrapper;
    if (paramBoolean);
    for (int i = 4; ; i = 0)
    {
      localView.setVisibility(i);
      return;
    }
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    Log.d("Tango.ConversationDetailFragment", "onActivityCreated()");
    super.onActivityCreated(paramBundle);
    this.m_conversationDetailAdapter = new ConversationMessageListAdapter(getActivity());
    this.m_participantListAdapter = new ParticipantListAdapter(getActivity());
    setListAdapter(this.m_conversationDetailAdapter);
    this.m_participantList.setAdapter(this.m_participantListAdapter);
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 1)
      postFinishSMSComposeMessage(true);
    while (this.m_conversationController == null)
      return;
    this.m_conversationController.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onAttach(Activity paramActivity)
  {
    Log.d("Tango.ConversationDetailFragment", "onAttach()");
    super.onAttach(paramActivity);
    try
    {
      this.m_listener = ((ConversationDetailFragmentListener)paramActivity);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      String str = "onAttach: " + paramActivity.toString() + " must implement ConversationDetailFragmentListener";
      Log.e("Tango.ConversationDetailFragment", str);
      throw new ClassCastException(str);
    }
  }

  public void onAudioCallClicked()
  {
    if (this.m_conversationDetail != null)
    {
      ConversationContact localConversationContact = this.m_conversationDetail.getFirstPeer();
      CallHandler.getDefault().sendCallMessage(localConversationContact.getContact().getAccountid(), localConversationContact.getDisplayName(), localConversationContact.getContact().getDeviceContactId(), CallHandler.VideoMode.VIDEO_OFF);
    }
  }

  public void onClick(View paramView)
  {
    if (paramView == this.m_tipComposeBubble)
      this.m_listener.onTipClicked();
    while (paramView != this.m_tipWrapper)
      return;
    hideTipLayer(true);
  }

  public void onComposeEnabled()
  {
    updateEmptyView();
    startContinuousAnimationsDelayed();
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    this.m_conversationController.getMessageController(this.m_selectedConversationMessage.getType()).onContextItemSelected(paramMenuItem.getItemId(), this.m_selectedConversationMessage);
    this.m_selectedConversationMessage = null;
    return true;
  }

  public void onConversationPayloadChanged()
  {
    Log.d("Tango.ConversationDetailFragment", "onConversationPayloadChanged, conversationId=" + this.m_conversationId);
    if (!this.m_isStarted)
      Log.w("Tango.ConversationDetailFragment", "onConversationPayloadChanged, fragment is not started, ignoring event");
    SessionMessages.ConversationPayload localConversationPayload;
    do
    {
      return;
      localConversationPayload = this.m_listener.getConversationPayload(this.m_conversationId);
    }
    while (localConversationPayload == null);
    int i = localConversationPayload.hashCode();
    if (i != this.m_latestConversationPayloadHashCode)
      handleOpenConversationPayload(localConversationPayload);
    while (true)
    {
      this.m_latestConversationPayloadHashCode = i;
      return;
      Log.d("Tango.ConversationDetailFragment", "onConversationPayloadChanged, payload has not changed, skipping");
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    this.m_conversationController = new ConversationController(this);
    super.onCreate(paramBundle);
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    if (paramView == this.m_listView)
    {
      paramContextMenu.setHeaderTitle(2131296578);
      AdapterView.AdapterContextMenuInfo localAdapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo)paramContextMenuInfo;
      this.m_selectedConversationMessage = ((ConversationMessage)this.m_conversationDetailAdapter.getItem(localAdapterContextMenuInfo.position - this.m_listView.getHeaderViewsCount()));
      this.m_conversationController.getMessageController(this.m_selectedConversationMessage.getType()).populateContextMenu(paramContextMenu, this.m_selectedConversationMessage);
    }
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Log.d("Tango.ConversationDetailFragment", "onCreateView()");
    this.m_conversationId = ((String)getArguments().getSerializable("KEY_CONVERSATION_ID"));
    View localView = paramLayoutInflater.inflate(2130903068, paramViewGroup, false);
    this.m_listView = ((ScrollToRefreshListView)localView.findViewById(16908298));
    this.m_listView.setEmptyView(localView.findViewById(16908292));
    this.m_listView.setListener(this);
    registerForContextMenu(this.m_listView);
    this.m_progressView = localView.findViewById(2131361988);
    this.m_participantList = ((HorizontalListView)localView.findViewById(2131361929));
    this.m_participantListBar = localView.findViewById(2131361928);
    this.m_emptyViewWrapper = localView.findViewById(2131361922);
    this.m_emptyViewArrowsWrapper = localView.findViewById(2131361924);
    this.m_tipWrapper = localView.findViewById(2131361930);
    this.m_tipComposeBubble = this.m_tipWrapper.findViewById(2131361931);
    this.m_tipComposeText = ((TextView)this.m_tipComposeBubble.findViewById(2131361932));
    this.m_tipWrapper.setOnClickListener(this);
    this.m_tipComposeBubble.setOnClickListener(this);
    hideTipLayer(false);
    makeEmptyViewTransparent(true);
    return localView;
  }

  public void onDestroy()
  {
    Log.d("Tango.ConversationDetailFragment", "onDestroy");
    super.onDestroy();
  }

  public void onDestroyView()
  {
    Log.d("Tango.ConversationDetailFragment", "onDestroyView");
    super.onDestroyView();
  }

  public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    ConversationMessage localConversationMessage = (ConversationMessage)paramListView.getItemAtPosition(paramInt);
    if (localConversationMessage.isStatusError())
    {
      paramListView.showContextMenuForChild(paramView);
      return;
    }
    this.m_conversationController.getMessageController(localConversationMessage.getType()).doActionViewMessage(localConversationMessage);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
    case 2131362216:
    }
    while (true)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      onVideoCallClicked();
    }
  }

  public void onPause()
  {
    super.onPause();
    this.m_conversationDetailAdapter.stopAutoRefresh();
  }

  public void onRefreshRequested()
  {
    this.m_handler.sendEmptyMessageDelayed(0, 1000L);
  }

  public void onResume()
  {
    super.onResume();
    if (!hasData());
    for (boolean bool = true; ; bool = false)
    {
      showProgressView(bool);
      updateParticipantListBarVisibility();
      updateMenuItemsVisibility();
      this.m_conversationDetailAdapter.startAutoRefresh(true);
      if (!"mounted".equals(Environment.getExternalStorageState()))
        Toast.makeText(getActivity(), 2131296616, 1).show();
      startContinuousAnimationsDelayed();
      if (this.m_conversationController != null)
        this.m_conversationController.onResume();
      return;
    }
  }

  public void onStart()
  {
    super.onStart();
    this.m_isStarted = true;
    onConversationPayloadChanged();
    if (hasData())
      this.m_listener.onLoadFinished(this.m_conversationDetail.getConversationMessageCount());
  }

  public void onStop()
  {
    this.m_isStarted = false;
    stopContinuousAnimations();
    super.onStop();
  }

  public void onVideoCallClicked()
  {
    if (this.m_conversationDetail != null)
    {
      ConversationContact localConversationContact = this.m_conversationDetail.getFirstPeer();
      CallHandler.getDefault().sendCallMessage(localConversationContact.getContact().getAccountid(), localConversationContact.getDisplayName(), localConversationContact.getContact().getDeviceContactId(), CallHandler.VideoMode.VIDEO_ON);
    }
  }

  public void showDeliveryError()
  {
    if (this.m_deliveryErrorToast == null)
      this.m_deliveryErrorToast = Toast.makeText(getActivity(), 2131296577, 1);
    this.m_deliveryErrorToast.show();
  }

  public void updateEmptyView()
  {
    if (this.m_listener.isComposeEnabled())
    {
      if (this.m_emptyViewArrowsWrapper.getVisibility() != 0)
      {
        this.m_emptyViewArrowsWrapper.startAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968577));
        this.m_emptyViewArrowsWrapper.setVisibility(0);
      }
      return;
    }
    this.m_emptyViewArrowsWrapper.setVisibility(4);
  }

  public static abstract interface ConversationDetailFragmentListener
  {
    public abstract SessionMessages.ConversationPayload getConversationPayload(String paramString);

    public abstract void hideIme();

    public abstract boolean hideMenuItems();

    public abstract boolean isComposeEnabled();

    public abstract void onLoadFinished(int paramInt);

    public abstract void onTipClicked();

    public abstract boolean updateMenuItemsVisibility(boolean paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.ConversationDetailFragment
 * JD-Core Version:    0.6.2
 */