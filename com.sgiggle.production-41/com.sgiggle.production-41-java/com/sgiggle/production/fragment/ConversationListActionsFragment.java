package com.sgiggle.production.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.sgiggle.util.Log;

public class ConversationListActionsFragment extends Fragment
  implements View.OnClickListener
{
  private static final String TAG = "Tango.ConversationListActionsFragment";
  private Button m_addTextButton;
  private ConversationListActionsFragmentListener m_listener;

  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    try
    {
      this.m_listener = ((ConversationListActionsFragmentListener)paramActivity);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      String str = "onAttach: " + paramActivity.toString() + " must implement ConversationListActionsFragmentListener";
      Log.e("Tango.ConversationListActionsFragment", str);
      throw new ClassCastException(str);
    }
  }

  public void onClick(View paramView)
  {
    if (paramView == this.m_addTextButton)
      this.m_listener.onAddTextClicked();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903070, paramViewGroup, false);
    this.m_addTextButton = ((Button)localView.findViewById(2131361933));
    this.m_addTextButton.setOnClickListener(this);
    return localView;
  }

  public static abstract interface ConversationListActionsFragmentListener
  {
    public abstract void onAddTextClicked();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.ConversationListActionsFragment
 * JD-Core Version:    0.6.2
 */