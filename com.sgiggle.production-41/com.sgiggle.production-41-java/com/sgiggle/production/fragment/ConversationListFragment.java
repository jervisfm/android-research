package com.sgiggle.production.fragment;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.DeleteConversationEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DeleteConversationMessage;
import com.sgiggle.media_engine.MediaEngineMessage.OpenConversationListEvent;
import com.sgiggle.media_engine.MediaEngineMessage.OpenConversationMessage;
import com.sgiggle.media_engine.MediaEngineMessage.UpdateConversationSummaryEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.adapter.ConversationListAdapter;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationSummary;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationSummary;
import com.sgiggle.xmpp.SessionMessages.ConversationSummaryItemPayload;
import com.sgiggle.xmpp.SessionMessages.ConversationSummaryListPayload;
import com.sgiggle.xmpp.SessionMessages.StringPayload;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConversationListFragment extends ListFragment
  implements View.OnClickListener
{
  private static final int ACTION_INDEX_DELETE = 1;
  private static final int ACTION_INDEX_OPEN = 0;
  private static final String KEY_CONVERSATION_SUMMARY_LIST_PAYLOAD = "KEY_CONVERSATION_SUMMARY_LIST_PAYLOAD";
  private static final String TAG = "Tango.ConversationListFragment";
  private ConversationListAdapter m_adapter;
  private SessionMessages.ConversationSummaryListPayload m_conversationSummaryListPayload;
  private ListView m_listView;
  private View m_listViewEmpty;
  private ConversationListActionsFragment.ConversationListActionsFragmentListener m_listener;
  private ConversationContact m_myself;
  private View m_progressView;
  private String m_selectedConversationId;

  private List<ConversationSummary> buildConversationSummaryFromPayload(SessionMessages.ConversationSummaryListPayload paramConversationSummaryListPayload)
  {
    this.m_myself = new ConversationContact(paramConversationSummaryListPayload.getMyself());
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramConversationSummaryListPayload.getConversationSummaryList().iterator();
    while (localIterator.hasNext())
      localArrayList.add(new ConversationSummary((SessionMessages.ConversationSummary)localIterator.next(), this.m_myself));
    return localArrayList;
  }

  private void handleConversationSummaryListPayload(SessionMessages.ConversationSummaryListPayload paramConversationSummaryListPayload)
  {
    Log.d("Tango.ConversationListFragment", "handleConversationSummaryListPayload()");
    boolean bool = this.m_adapter.setData(buildConversationSummaryFromPayload(paramConversationSummaryListPayload));
    showProgressView(false);
    if (bool)
      scrollToRelevantItem();
  }

  private void handleDeleteConversationEvent(MediaEngineMessage.DeleteConversationEvent paramDeleteConversationEvent)
  {
    String str = ((SessionMessages.StringPayload)paramDeleteConversationEvent.payload()).getText();
    this.m_adapter.remove(str);
  }

  private void handleUpdateConversationSummaryEvent(MediaEngineMessage.UpdateConversationSummaryEvent paramUpdateConversationSummaryEvent)
  {
    SessionMessages.ConversationSummary localConversationSummary = ((SessionMessages.ConversationSummaryItemPayload)paramUpdateConversationSummaryEvent.payload()).getSummary();
    this.m_adapter.addOrUpdate(localConversationSummary, this.m_myself);
  }

  private boolean hasData()
  {
    return this.m_conversationSummaryListPayload != null;
  }

  public static ConversationListFragment newInstance(SessionMessages.ConversationSummaryListPayload paramConversationSummaryListPayload)
  {
    ConversationListFragment localConversationListFragment = new ConversationListFragment();
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("KEY_CONVERSATION_SUMMARY_LIST_PAYLOAD", paramConversationSummaryListPayload);
    localConversationListFragment.setArguments(localBundle);
    return localConversationListFragment;
  }

  private void postDeleteConversationMessage()
  {
    Log.d("Tango.ConversationListFragment", "Deleting conversation with ID: " + this.m_selectedConversationId);
    MediaEngineMessage.DeleteConversationMessage localDeleteConversationMessage = new MediaEngineMessage.DeleteConversationMessage(this.m_selectedConversationId);
    MessageRouter.getInstance().postMessage("jingle", localDeleteConversationMessage);
  }

  private void postOpenConversationMessage(String paramString)
  {
    Log.d("Tango.ConversationListFragment", "Opening conversation with ID: " + paramString);
    MediaEngineMessage.OpenConversationMessage localOpenConversationMessage = new MediaEngineMessage.OpenConversationMessage(paramString);
    MessageRouter.getInstance().postMessage("jingle", localOpenConversationMessage);
  }

  private void scrollToPosition(final int paramInt)
  {
    Log.d("Tango.ConversationListFragment", "scrollToPosition=" + paramInt);
    int i = this.m_listView.getCount();
    if ((this.m_listView != null) && (i > 0) && (paramInt < i))
      this.m_listView.post(new Runnable()
      {
        public void run()
        {
          if (Build.VERSION.SDK_INT >= 8)
          {
            ConversationListFragment.this.m_listView.smoothScrollToPosition(paramInt);
            return;
          }
          ConversationListFragment.this.m_listView.setSelection(paramInt);
        }
      });
  }

  private void showProgressView(boolean paramBoolean)
  {
    if ((this.m_listView == null) || (this.m_progressView == null))
      return;
    if (paramBoolean)
    {
      this.m_listView.setVisibility(4);
      this.m_progressView.setVisibility(0);
      return;
    }
    this.m_progressView.setVisibility(4);
    this.m_listView.setVisibility(0);
  }

  public boolean handleMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      return false;
    case 35272:
      this.m_conversationSummaryListPayload = ((SessionMessages.ConversationSummaryListPayload)((MediaEngineMessage.OpenConversationListEvent)paramMessage).payload());
      handleConversationSummaryListPayload(this.m_conversationSummaryListPayload);
      return true;
    case 35273:
      handleUpdateConversationSummaryEvent((MediaEngineMessage.UpdateConversationSummaryEvent)paramMessage);
      return true;
    case 35274:
    }
    handleDeleteConversationEvent((MediaEngineMessage.DeleteConversationEvent)paramMessage);
    return true;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    this.m_adapter = new ConversationListAdapter(getActivity());
    setListAdapter(this.m_adapter);
    if (this.m_conversationSummaryListPayload != null)
      handleConversationSummaryListPayload(this.m_conversationSummaryListPayload);
  }

  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    try
    {
      this.m_listener = ((ConversationListActionsFragment.ConversationListActionsFragmentListener)paramActivity);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      String str = "onAttach: " + paramActivity.toString() + " must implement ConversationListActionsFragmentListener";
      Log.e("Tango.ConversationListFragment", str);
      throw new ClassCastException(str);
    }
  }

  public void onClick(View paramView)
  {
    if (paramView == this.m_listViewEmpty)
      this.m_listener.onAddTextClicked();
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
    case 0:
    case 1:
    }
    while (true)
    {
      return true;
      postOpenConversationMessage(this.m_selectedConversationId);
      continue;
      DeleteConversationDialog localDeleteConversationDialog = DeleteConversationDialog.newInstance(this);
      localDeleteConversationDialog.show(getActivity().getSupportFragmentManager(), localDeleteConversationDialog.getClass().toString());
    }
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    if (paramView == this.m_listView)
    {
      AdapterView.AdapterContextMenuInfo localAdapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo)paramContextMenuInfo;
      ConversationSummary localConversationSummary = (ConversationSummary)this.m_adapter.getItem(localAdapterContextMenuInfo.position);
      this.m_selectedConversationId = localConversationSummary.getConversationId();
      paramContextMenu.setHeaderTitle(localConversationSummary.getDisplayString());
      String[] arrayOfString = getResources().getStringArray(2131492866);
      for (int i = 0; i < arrayOfString.length; i++)
        paramContextMenu.add(0, i, i, arrayOfString[i]);
    }
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Log.d("Tango.ConversationListFragment", "onCreateView()");
    this.m_conversationSummaryListPayload = ((SessionMessages.ConversationSummaryListPayload)getArguments().getSerializable("KEY_CONVERSATION_SUMMARY_LIST_PAYLOAD"));
    View localView = paramLayoutInflater.inflate(2130903071, paramViewGroup, false);
    this.m_listView = ((ListView)localView.findViewById(16908298));
    this.m_listViewEmpty = localView.findViewById(16908292);
    this.m_listViewEmpty.setOnClickListener(this);
    this.m_listView.setEmptyView(this.m_listViewEmpty);
    if (!TangoApp.videomailSupported())
      ((TextView)localView.findViewById(2131361936)).setText(2131296570);
    this.m_progressView = localView.findViewById(2131361988);
    registerForContextMenu(this.m_listView);
    showProgressView(true);
    return localView;
  }

  public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    postOpenConversationMessage(((ConversationSummary)this.m_adapter.getItem(paramInt)).getConversationId());
  }

  public void onPause()
  {
    super.onPause();
    this.m_adapter.stopAutoRefresh();
  }

  public void onResume()
  {
    super.onResume();
    if (!hasData());
    for (boolean bool = true; ; bool = false)
    {
      showProgressView(bool);
      this.m_adapter.startAutoRefresh(true);
      return;
    }
  }

  public void scrollToRelevantItem()
  {
    int i;
    if ((this.m_adapter == null) || (this.m_listView == null))
    {
      i = -1;
      if (i <= 0)
        break label37;
    }
    while (true)
    {
      scrollToPosition(i);
      return;
      i = this.m_adapter.getFirstConversationPosWithUnreadMessages();
      break;
      label37: i = 0;
    }
  }

  private static class DeleteConversationDialog extends DialogFragment
  {
    private ConversationListFragment m_parent;

    private DeleteConversationDialog(ConversationListFragment paramConversationListFragment)
    {
      this.m_parent = paramConversationListFragment;
    }

    public static DeleteConversationDialog newInstance(ConversationListFragment paramConversationListFragment)
    {
      return new DeleteConversationDialog(paramConversationListFragment);
    }

    public Dialog onCreateDialog(Bundle paramBundle)
    {
      return new AlertDialog.Builder(getActivity()).setIcon(2130837650).setTitle(2131296559).setMessage(2131296560).setPositiveButton(2131296561, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          ConversationListFragment.this.postDeleteConversationMessage();
        }
      }).setNegativeButton(2131296288, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          ConversationListFragment.DeleteConversationDialog.this.dismiss();
        }
      }).create();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.ConversationListFragment
 * JD-Core Version:    0.6.2
 */