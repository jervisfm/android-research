package com.sgiggle.production.fragment;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.sgiggle.media_engine.MediaEngineMessage.DeleteSingleConversationMessageMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.model.ConversationMessage;

public class DeleteConversationMessageDialog extends DialogFragment
{
  private final String m_conversationId;
  private final ConversationMessage m_message;

  private DeleteConversationMessageDialog(ConversationMessage paramConversationMessage, String paramString)
  {
    this.m_message = paramConversationMessage;
    this.m_conversationId = paramString;
  }

  public static DeleteConversationMessageDialog newInstance(ConversationMessage paramConversationMessage, String paramString)
  {
    return new DeleteConversationMessageDialog(paramConversationMessage, paramString);
  }

  private void postDeleteSingleConversationMessageMessage()
  {
    MediaEngineMessage.DeleteSingleConversationMessageMessage localDeleteSingleConversationMessageMessage = new MediaEngineMessage.DeleteSingleConversationMessageMessage(this.m_message.convertToSessionConversationMessage(this.m_conversationId));
    MessageRouter.getInstance().postMessage("jingle", localDeleteSingleConversationMessageMessage);
  }

  public Dialog onCreateDialog(Bundle paramBundle)
  {
    return new AlertDialog.Builder(getActivity()).setIcon(2130837650).setTitle(2131296592).setMessage(2131296593).setPositiveButton(2131296561, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        DeleteConversationMessageDialog.this.postDeleteSingleConversationMessageMessage();
      }
    }).setNegativeButton(2131296288, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        DeleteConversationMessageDialog.this.dismiss();
      }
    }).create();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.DeleteConversationMessageDialog
 * JD-Core Version:    0.6.2
 */