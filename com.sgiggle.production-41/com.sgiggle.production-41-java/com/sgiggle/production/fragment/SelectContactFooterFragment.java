package com.sgiggle.production.fragment;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.SelectContactType;

public class SelectContactFooterFragment extends Fragment
  implements View.OnClickListener
{
  private static final String TAG = "Tango.SelectContactFooterFragment";
  private Button m_cancelButton;
  private SelectContactFooterFragmentListener m_listener;
  private Button m_selectButton;
  private SessionMessages.SelectContactType m_type;

  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    Log.d("Tango.SelectContactFooterFragment", "onAttach");
    try
    {
      this.m_listener = ((SelectContactFooterFragmentListener)paramActivity);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      String str = "onAttach: " + paramActivity.toString() + " must implement SelectContactFooterFragmentListener";
      Log.e("Tango.SelectContactFooterFragment", str);
      throw new ClassCastException(str);
    }
  }

  public void onClick(View paramView)
  {
    if (paramView == this.m_selectButton)
      this.m_listener.onSelectClicked();
    while (paramView != this.m_cancelButton)
      return;
    this.m_listener.onCancelClicked();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Log.d("Tango.SelectContactFooterFragment", "onCreateView");
    View localView = paramLayoutInflater.inflate(2130903109, paramViewGroup, false);
    this.m_selectButton = ((Button)localView.findViewById(2131362074));
    this.m_cancelButton = ((Button)localView.findViewById(2131362075));
    this.m_selectButton.setOnClickListener(this);
    this.m_cancelButton.setOnClickListener(this);
    return localView;
  }

  public void onSelectedCountChanged(int paramInt)
  {
    Log.d("Tango.SelectContactFooterFragment", "onSelectedCountChanged: count=" + paramInt + ", type=" + this.m_type);
    Object localObject = null;
    Button localButton;
    switch (1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$SelectContactType[this.m_type.ordinal()])
    {
    default:
      Log.w("Tango.SelectContactFooterFragment", "setType: unknown type " + this.m_type);
      if (localObject != null)
        this.m_selectButton.setText((CharSequence)localObject);
      localButton = this.m_selectButton;
      if (paramInt <= 0)
        break;
    case 1:
    case 2:
    }
    for (boolean bool = true; ; bool = false)
    {
      localButton.setEnabled(bool);
      return;
      if (paramInt <= 0)
      {
        localObject = getResources().getString(2131296566);
        break;
      }
      Resources localResources2 = getResources();
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Integer.valueOf(paramInt);
      localObject = localResources2.getString(2131296567, arrayOfObject2);
      break;
      if (paramInt <= 0)
      {
        localObject = getResources().getString(2131296620);
        break;
      }
      Resources localResources1 = getResources();
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = Integer.valueOf(paramInt);
      localObject = localResources1.getString(2131296621, arrayOfObject1);
      break;
    }
  }

  public void setSelectContactType(SessionMessages.SelectContactType paramSelectContactType)
  {
    Log.d("Tango.SelectContactFooterFragment", "setSelectContactType: contactType=" + paramSelectContactType);
    this.m_type = paramSelectContactType;
    onSelectedCountChanged(0);
  }

  public static abstract interface SelectContactFooterFragmentListener
  {
    public abstract void onCancelClicked();

    public abstract void onSelectClicked();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.SelectContactFooterFragment
 * JD-Core Version:    0.6.2
 */