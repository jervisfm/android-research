package com.sgiggle.production.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.production.Utils.UIContact;
import com.sgiggle.production.adapter.ContactListAdapter.ContactListSelection;
import com.sgiggle.util.Log;
import java.util.ArrayList;

public abstract class SelectContactFragment extends Fragment
{
  private static final String KEY_CONTACT_LIST = "KEY_CONTACT_LIST";
  private static final String KEY_CONTACT_LIST_CONTACT_ORDER_PAIR = "KEY_CONTACT_LIST_CONTACT_ORDER_PAIR";
  private static final String KEY_CONTACT_LIST_MAX_SELECTION = "KEY_CONTACT_LIST_MAX_SELECTION";
  private static final String KEY_CONTACT_LIST_SELECTION = "KEY_CONTACT_LIST_SELECTION";
  private static final String TAG = "Tango.SelectContactFragment";
  protected ContactListAdapter.ContactListSelection m_contactListSelection = ContactListAdapter.ContactListSelection.UNDEFINED;
  protected ContactStore.ContactOrderPair m_contactOrderPair = ContactStore.ContactOrderPair.getDefault();
  protected ArrayList<Utils.UIContact> m_contacts;
  private View m_dataView;
  protected int m_maxSelection = -1;
  private View m_progressView;

  public SelectContactFragment()
  {
  }

  public SelectContactFragment(ArrayList<Utils.UIContact> paramArrayList, ContactListAdapter.ContactListSelection paramContactListSelection, ContactStore.ContactOrderPair paramContactOrderPair, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder().append("SelectContactFragment: #contacts=");
    if (paramArrayList == null);
    for (int i = 0; ; i = paramArrayList.size())
    {
      Log.d("Tango.SelectContactFragment", i + ", sortOrder=" + paramContactOrderPair.getSortOrder() + ", displayOrder=" + paramContactOrderPair.getDisplayOrder() + ", maxSelection=" + paramInt);
      Bundle localBundle = new Bundle();
      localBundle.putSerializable("KEY_CONTACT_LIST", paramArrayList);
      localBundle.putSerializable("KEY_CONTACT_LIST_SELECTION", paramContactListSelection);
      localBundle.putSerializable("KEY_CONTACT_LIST_CONTACT_ORDER_PAIR", paramContactOrderPair);
      localBundle.putInt("KEY_CONTACT_LIST_MAX_SELECTION", paramInt);
      setArguments(localBundle);
      return;
    }
  }

  private boolean hasData()
  {
    return this.m_contacts != null;
  }

  private void showProgressView(boolean paramBoolean)
  {
    if ((this.m_dataView == null) || (this.m_progressView == null))
      return;
    if (paramBoolean)
    {
      this.m_dataView.setVisibility(4);
      this.m_progressView.setVisibility(0);
      return;
    }
    this.m_progressView.setVisibility(4);
    this.m_dataView.setVisibility(0);
  }

  protected abstract View createView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup);

  protected abstract int getDataViewResId();

  protected abstract int getProgressViewResId();

  public boolean handleBackPressed()
  {
    return false;
  }

  protected abstract void onContactsSet(ArrayList<Utils.UIContact> paramArrayList, int paramInt);

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Log.d("Tango.SelectContactFragment", "onCreateView()");
    this.m_contacts = ((ArrayList)getArguments().getSerializable("KEY_CONTACT_LIST"));
    this.m_contactListSelection = ((ContactListAdapter.ContactListSelection)getArguments().getSerializable("KEY_CONTACT_LIST_SELECTION"));
    this.m_contactOrderPair = ((ContactStore.ContactOrderPair)getArguments().getSerializable("KEY_CONTACT_LIST_CONTACT_ORDER_PAIR"));
    this.m_maxSelection = getArguments().getInt("KEY_CONTACT_LIST_MAX_SELECTION");
    View localView = createView(paramLayoutInflater, paramViewGroup);
    this.m_dataView = localView.findViewById(getDataViewResId());
    this.m_progressView = localView.findViewById(getProgressViewResId());
    showProgressView(true);
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    if (!hasData());
    for (boolean bool = true; ; bool = false)
    {
      showProgressView(bool);
      return;
    }
  }

  public boolean onSearchRequested()
  {
    return true;
  }

  public void onSelected()
  {
  }

  public void setContacts(ArrayList<Utils.UIContact> paramArrayList, int paramInt)
  {
    Log.d("Tango.SelectContactFragment", "setContacts: maxSelection=" + paramInt);
    this.m_contacts = paramArrayList;
    this.m_maxSelection = paramInt;
    onContactsSet(paramArrayList, paramInt);
    showProgressView(false);
  }

  public static abstract interface SelectContactListener
  {
    public abstract void onContactSelectionChanged(Utils.UIContact paramUIContact, boolean paramBoolean);

    public abstract boolean trySelectContacts(int paramInt, boolean paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.SelectContactFragment
 * JD-Core Version:    0.6.2
 */