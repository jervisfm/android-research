package com.sgiggle.production.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ViewSwitcher;
import com.sgiggle.contacts.ContactStore.ContactOrderPair;
import com.sgiggle.production.Utils;
import com.sgiggle.production.Utils.UIContact;
import com.sgiggle.production.adapter.ContactListAdapter.ContactListSelection;
import com.sgiggle.production.adapter.SelectContactListAdapter;
import com.sgiggle.production.adapter.SelectContactListAdapter.ContactListFilterListener;
import com.sgiggle.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class SelectContactListFragment extends SelectContactFragment
  implements AdapterView.OnItemClickListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener, SelectContactFragment.SelectContactListener, SelectContactListAdapter.ContactListFilterListener
{
  private static final boolean FAST_SCROLL_ENABLED = true;
  private static final String TAG = "Tango.SelectContactListFragment";
  private SelectContactListAdapter m_adapter;
  private ViewSwitcher m_headerSwitcher;
  private ListView m_listView;
  private SelectContactFragment.SelectContactListener m_listener = null;
  private ImageButton m_searchButton;
  private EditText m_searchText;
  private TextWatcher m_searchTextWatcher = new TextWatcher()
  {
    public void afterTextChanged(Editable paramAnonymousEditable)
    {
    }

    public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
    {
    }

    public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
    {
      if (SelectContactListFragment.this.m_adapter != null)
      {
        SelectContactListFragment.this.m_listView.setFastScrollEnabled(false);
        SelectContactListFragment.this.m_adapter.getFilter(SelectContactListFragment.this).filter(paramAnonymousCharSequence);
      }
    }
  };
  private CheckBox m_selectAllCheckbox;
  private View m_selectAllCheckboxText;

  public SelectContactListFragment()
  {
  }

  private SelectContactListFragment(ArrayList<Utils.UIContact> paramArrayList, ContactListAdapter.ContactListSelection paramContactListSelection, ContactStore.ContactOrderPair paramContactOrderPair, int paramInt)
  {
    super(paramArrayList, paramContactListSelection, paramContactOrderPair, paramInt);
  }

  public static SelectContactListFragment newInstance(ArrayList<Utils.UIContact> paramArrayList, ContactListAdapter.ContactListSelection paramContactListSelection, ContactStore.ContactOrderPair paramContactOrderPair, int paramInt)
  {
    return new SelectContactListFragment(paramArrayList, paramContactListSelection, paramContactOrderPair, paramInt);
  }

  private boolean searchShown()
  {
    return (this.m_headerSwitcher != null) && (this.m_headerSwitcher.getCurrentView().getId() == 2131362079);
  }

  private void showSearch(boolean paramBoolean)
  {
    int i = this.m_headerSwitcher.getCurrentView().getId();
    if (((paramBoolean) && (i == 2131362079)) || ((!paramBoolean) && (i == 2131362078)))
      return;
    this.m_headerSwitcher.showNext();
  }

  private void unselectAllCheckboxSafe()
  {
    this.m_selectAllCheckbox.setOnCheckedChangeListener(null);
    this.m_selectAllCheckbox.setChecked(false);
    this.m_selectAllCheckbox.setOnCheckedChangeListener(this);
  }

  protected View createView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup)
  {
    View localView = paramLayoutInflater.inflate(2130903110, paramViewGroup, false);
    this.m_listView = ((ListView)localView.findViewById(16908298));
    this.m_listView.setOnItemClickListener(this);
    this.m_listView.setEmptyView(localView.findViewById(16908292));
    this.m_searchButton = ((ImageButton)localView.findViewById(2131362076));
    this.m_searchButton.setOnClickListener(this);
    this.m_selectAllCheckbox = ((CheckBox)localView.findViewById(2131361974));
    this.m_selectAllCheckbox.setOnCheckedChangeListener(this);
    this.m_selectAllCheckboxText = localView.findViewById(2131361975);
    this.m_searchText = ((EditText)localView.findViewById(2131362079));
    this.m_searchText.addTextChangedListener(this.m_searchTextWatcher);
    this.m_headerSwitcher = ((ViewSwitcher)localView.findViewById(2131362077));
    Animation localAnimation = this.m_headerSwitcher.getInAnimation();
    if (localAnimation != null)
      localAnimation.setAnimationListener(new Animation.AnimationListener()
      {
        public void onAnimationEnd(Animation paramAnonymousAnimation)
        {
          if (SelectContactListFragment.this.searchShown())
          {
            SelectContactListFragment.this.onSearchStarting();
            return;
          }
          SelectContactListFragment.this.m_searchText.setText("");
          SelectContactListFragment.this.onSearchDone();
        }

        public void onAnimationRepeat(Animation paramAnonymousAnimation)
        {
        }

        public void onAnimationStart(Animation paramAnonymousAnimation)
        {
        }
      });
    return localView;
  }

  protected int getDataViewResId()
  {
    return 2131361880;
  }

  protected int getProgressViewResId()
  {
    return 2131361988;
  }

  public boolean handleBackPressed()
  {
    if (searchShown())
    {
      showSearch(false);
      return true;
    }
    return false;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    FragmentActivity localFragmentActivity = getActivity();
    try
    {
      this.m_listener = ((SelectContactFragment.SelectContactListener)localFragmentActivity);
      LayoutInflater localLayoutInflater = LayoutInflater.from(localFragmentActivity);
      this.m_adapter = new SelectContactListAdapter(this.m_contactListSelection, localLayoutInflater, this.m_contactOrderPair, this);
      this.m_listView.setAdapter(this.m_adapter);
      if (this.m_contacts != null)
      {
        Log.d("Tango.SelectContactListFragment", "onActivityCreated: maxSelection=" + this.m_maxSelection);
        onContactsSet(this.m_contacts, this.m_maxSelection);
        return;
      }
    }
    catch (ClassCastException localClassCastException)
    {
      String str = "onAttach: " + localFragmentActivity.toString() + " must implement SelectContactListener";
      Log.e("Tango.SelectContactListFragment", str);
      throw new ClassCastException(str);
    }
    Log.d("Tango.SelectContactListFragment", "onActivityCreated: no contacts set. onContactsSet() should be called shortly. m_maxSelection=" + this.m_maxSelection);
  }

  public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    int i = 0;
    int j;
    if (paramCompoundButton == this.m_selectAllCheckbox)
    {
      Iterator localIterator1 = this.m_contacts.iterator();
      j = 0;
      if (localIterator1.hasNext())
      {
        Utils.UIContact localUIContact2 = (Utils.UIContact)localIterator1.next();
        j++;
        if (localUIContact2.m_selected == paramBoolean)
          break label159;
      }
    }
    label159: for (int k = i + 1; ; k = i)
    {
      i = k;
      break;
      if ((j > 0) && (this.m_listener.trySelectContacts(i, paramBoolean)))
      {
        Iterator localIterator2 = this.m_contacts.iterator();
        while (localIterator2.hasNext())
        {
          Utils.UIContact localUIContact1 = (Utils.UIContact)localIterator2.next();
          if (localUIContact1.m_selected != paramBoolean)
          {
            localUIContact1.m_selected = paramBoolean;
            onContactSelectionChanged(localUIContact1, localUIContact1.m_selected);
          }
        }
        this.m_adapter.notifyDataSetChanged();
        return;
      }
      unselectAllCheckboxSafe();
      return;
    }
  }

  public void onClick(View paramView)
  {
    if (paramView == this.m_searchButton)
      if (searchShown())
        break label23;
    label23: for (boolean bool = true; ; bool = false)
    {
      showSearch(bool);
      return;
    }
  }

  public void onContactSelectionChanged(Utils.UIContact paramUIContact, boolean paramBoolean)
  {
    if ((!paramBoolean) && (this.m_selectAllCheckbox.isChecked()))
      unselectAllCheckboxSafe();
    onSearchDone();
    if (this.m_listener != null)
      this.m_listener.onContactSelectionChanged(paramUIContact, paramBoolean);
  }

  protected void onContactsSet(ArrayList<Utils.UIContact> paramArrayList, int paramInt)
  {
    int i;
    SelectContactListAdapter localSelectContactListAdapter;
    if (paramInt == 1)
    {
      i = 1;
      this.m_adapter.setContacts(paramArrayList);
      localSelectContactListAdapter = this.m_adapter;
      if (i != 0)
        break label76;
    }
    label76: for (boolean bool = true; ; bool = false)
    {
      localSelectContactListAdapter.setShowCheckbox(bool);
      this.m_adapter.notifyDataSetChanged();
      this.m_listView.setFastScrollEnabled(true);
      if (i == 0)
        break label82;
      this.m_selectAllCheckbox.setVisibility(4);
      this.m_selectAllCheckboxText.setVisibility(4);
      return;
      i = 0;
      break;
    }
    label82: this.m_selectAllCheckbox.setVisibility(0);
    this.m_selectAllCheckboxText.setVisibility(0);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Log.d("Tango.SelectContactListFragment", "onCreateView()");
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }

  public void onFilterDone(CharSequence paramCharSequence)
  {
    if (this.m_adapter.isSectionIndexerUpToDate())
      this.m_listView.setFastScrollEnabled(true);
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    this.m_adapter.onItemClicked(paramView, paramInt, paramLong);
  }

  public void onSearchDone()
  {
    Utils.hideKeyboard(getActivity(), this.m_searchText);
  }

  public boolean onSearchRequested()
  {
    showSearch(true);
    return true;
  }

  public void onSearchStarting()
  {
    Utils.focusTextViewAndShowKeyboard(getActivity(), this.m_searchText);
  }

  public void onSelected()
  {
    super.onSelected();
    if (this.m_selectAllCheckbox.isChecked())
    {
      Iterator localIterator = this.m_contacts.iterator();
      while (localIterator.hasNext())
        if (!((Utils.UIContact)localIterator.next()).m_selected)
          unselectAllCheckboxSafe();
    }
    if (this.m_adapter != null)
      this.m_adapter.notifyDataSetChanged();
  }

  public void setUserVisibleHint(boolean paramBoolean)
  {
    super.setUserVisibleHint(paramBoolean);
    if ((this.m_searchText != null) && (paramBoolean) && (!searchShown()))
      onSearchDone();
  }

  public boolean trySelectContacts(int paramInt, boolean paramBoolean)
  {
    if (this.m_listener != null)
      return this.m_listener.trySelectContacts(paramInt, paramBoolean);
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.SelectContactListFragment
 * JD-Core Version:    0.6.2
 */