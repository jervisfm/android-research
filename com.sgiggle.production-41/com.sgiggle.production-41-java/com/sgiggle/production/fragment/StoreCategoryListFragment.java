package com.sgiggle.production.fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayAvatarPaymentMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayGameCatalogMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayStoreEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayVgoodPaymentMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.adapter.StoreCategoryAdapter;
import com.sgiggle.production.adapter.StoreCategoryAdapter.Category;
import com.sgiggle.xmpp.SessionMessages.DisplayStorePayload;

public class StoreCategoryListFragment extends ListFragment
{
  private StoreCategoryAdapter m_adapter;

  private void handleDisplayEvent(MediaEngineMessage.DisplayStoreEvent paramDisplayStoreEvent)
  {
    if (((SessionMessages.DisplayStorePayload)paramDisplayStoreEvent.payload()).hasError())
      return;
    StoreCategoryAdapter localStoreCategoryAdapter1 = this.m_adapter;
    boolean bool1;
    boolean bool2;
    label62: StoreCategoryAdapter localStoreCategoryAdapter3;
    if (((SessionMessages.DisplayStorePayload)paramDisplayStoreEvent.payload()).getVgoodBadgeCount() > 0)
    {
      bool1 = true;
      localStoreCategoryAdapter1.setNewBadge(0, bool1);
      StoreCategoryAdapter localStoreCategoryAdapter2 = this.m_adapter;
      if (((SessionMessages.DisplayStorePayload)paramDisplayStoreEvent.payload()).getGameBadgeCount() <= 0)
        break label106;
      bool2 = true;
      localStoreCategoryAdapter2.setNewBadge(1, bool2);
      localStoreCategoryAdapter3 = this.m_adapter;
      if (((SessionMessages.DisplayStorePayload)paramDisplayStoreEvent.payload()).getAvatarBadgeCount() <= 0)
        break label112;
    }
    label106: label112: for (boolean bool3 = true; ; bool3 = false)
    {
      localStoreCategoryAdapter3.setNewBadge(2, bool3);
      return;
      bool1 = false;
      break;
      bool2 = false;
      break label62;
    }
  }

  Message getItemClickedMessage(StoreCategoryAdapter.Category paramCategory)
  {
    switch (paramCategory.getType())
    {
    default:
      throw new RuntimeException("Unsupported type!");
    case 0:
      return new MediaEngineMessage.DisplayVgoodPaymentMessage();
    case 1:
      return new MediaEngineMessage.DisplayGameCatalogMessage();
    case 2:
    }
    return new MediaEngineMessage.DisplayAvatarPaymentMessage();
  }

  public void handleMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35264:
    }
    handleDisplayEvent((MediaEngineMessage.DisplayStoreEvent)paramMessage);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2130903122, paramViewGroup, false);
  }

  public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    Message localMessage = getItemClickedMessage((StoreCategoryAdapter.Category)getListAdapter().getItem(paramInt));
    MessageRouter.getInstance().postMessage("jingle", localMessage);
  }

  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    this.m_adapter = new StoreCategoryAdapter(getActivity());
    setListAdapter(this.m_adapter);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.fragment.StoreCategoryListFragment
 * JD-Core Version:    0.6.2
 */