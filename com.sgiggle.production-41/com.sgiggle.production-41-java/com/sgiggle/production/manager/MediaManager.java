package com.sgiggle.production.manager;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.media.MediaPlayer;
import com.sgiggle.production.Utils;
import com.sgiggle.util.Log;

public class MediaManager
{
  private static final String TAG = "Tango.AudioManager";
  private static MediaManager s_instance;
  private MediaPlayer m_mediaPlayer;

  public static MediaManager getInstance()
  {
    if (s_instance == null)
      s_instance = new MediaManager();
    return s_instance;
  }

  public void playAudioResource(Context paramContext, int paramInt)
  {
    while (true)
    {
      try
      {
        if (this.m_mediaPlayer != null)
        {
          this.m_mediaPlayer.release();
          this.m_mediaPlayer = null;
        }
        Log.v("Tango.AudioManager", "playAudioResource " + paramInt);
        try
        {
          this.m_mediaPlayer = MediaPlayer.create(paramContext, paramInt);
          if (this.m_mediaPlayer == null)
          {
            Log.v("Tango.AudioManager", "MediaPlayer.create failed");
            return;
          }
        }
        catch (Resources.NotFoundException localNotFoundException)
        {
          Log.e("Tango.AudioManager", "playAudioResource: cannot find file with ID=" + paramInt, localNotFoundException);
          continue;
        }
      }
      finally
      {
      }
      this.m_mediaPlayer.start();
    }
  }

  public void playAudioResourceAsNotification(Context paramContext, int paramInt, boolean paramBoolean)
  {
    NotificationManager localNotificationManager = (NotificationManager)paramContext.getSystemService("notification");
    Notification localNotification = new Notification();
    localNotification.defaults = 0;
    if (!paramBoolean)
      localNotification.defaults = (0x2 | localNotification.defaults);
    localNotification.flags = (0x10 | localNotification.flags);
    localNotification.sound = Utils.getResourceUri(paramInt);
    localNotificationManager.notify(6, localNotification);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.manager.MediaManager
 * JD-Core Version:    0.6.2
 */