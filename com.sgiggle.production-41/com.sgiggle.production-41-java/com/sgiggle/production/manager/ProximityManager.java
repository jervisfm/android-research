package com.sgiggle.production.manager;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.sgiggle.util.Log;

public class ProximityManager
  implements SensorEventListener
{
  private static final float PROXIMITY_THRESHOLD = 5.0F;
  private static final String TAG = "Tango.ProximityManager";
  private boolean m_hasProximitySensor = false;
  private Listener m_listener;
  private float m_proximityMaximumRange;
  private Sensor m_proximitySensor;
  private SensorManager m_sensorManager;

  public ProximityManager(Context paramContext, Listener paramListener)
  {
    this.m_sensorManager = ((SensorManager)paramContext.getSystemService("sensor"));
    this.m_listener = paramListener;
    if (this.m_sensorManager == null)
    {
      this.m_hasProximitySensor = false;
      return;
    }
    this.m_proximitySensor = this.m_sensorManager.getDefaultSensor(8);
    if (this.m_proximitySensor == null)
    {
      this.m_hasProximitySensor = false;
      return;
    }
    this.m_hasProximitySensor = true;
    this.m_proximityMaximumRange = this.m_proximitySensor.getMaximumRange();
  }

  public void disable()
  {
    Log.i("Tango.ProximityManager", "Unegistered the proximity listener.");
    if ((this.m_hasProximitySensor) && (this.m_listener != null))
      this.m_sensorManager.unregisterListener(this);
  }

  public void enable()
  {
    if ((this.m_hasProximitySensor) && (this.m_listener != null))
    {
      Log.i("Tango.ProximityManager", "Registered the proximity listener to receive events.");
      this.m_sensorManager.registerListener(this, this.m_proximitySensor, 3);
    }
  }

  public void onAccuracyChanged(Sensor paramSensor, int paramInt)
  {
  }

  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    float f = paramSensorEvent.values[0];
    if ((f >= 0.0F) && (f < 5.0F) && (f < this.m_proximityMaximumRange))
    {
      this.m_listener.onProximityChanged(true);
      return;
    }
    this.m_listener.onProximityChanged(false);
  }

  public static abstract interface Listener
  {
    public abstract void onProximityChanged(boolean paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.manager.ProximityManager
 * JD-Core Version:    0.6.2
 */