package com.sgiggle.production.model;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.util.BitmapLoaderThread.LoadableImage;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.PhoneNumber;

public class ConversationContact
  implements BitmapLoaderThread.LoadableImage
{
  private SessionMessages.Contact m_contact;
  private String m_displayName;
  private String m_displayNameShort;
  private boolean m_hasPhoto = true;

  public ConversationContact(SessionMessages.Contact paramContact)
  {
    this.m_contact = paramContact;
    refreshDisplayNameShort();
  }

  private void refreshDisplayName()
  {
    SessionMessages.Contact localContact = this.m_contact;
    String str = null;
    if (localContact != null)
    {
      boolean bool1 = this.m_contact.getIsSystemAccount();
      str = null;
      if (!bool1)
      {
        boolean bool2 = TextUtils.isEmpty(this.m_contact.getDisplayname());
        str = null;
        if (!bool2)
          str = this.m_contact.getDisplayname();
      }
    }
    if (str == null)
      str = getDisplayNameShort();
    this.m_displayName = str;
  }

  private void refreshDisplayNameShort()
  {
    SessionMessages.Contact localContact = this.m_contact;
    String str = null;
    if (localContact != null)
    {
      if (!this.m_contact.getIsSystemAccount())
        break label61;
      str = TangoApp.getInstance().getApplicationContext().getResources().getString(2131296600);
    }
    while (true)
    {
      if (str == null)
        str = TangoApp.getInstance().getApplicationContext().getResources().getString(2131296562);
      this.m_displayNameShort = str;
      return;
      label61: if (!TextUtils.isEmpty(this.m_contact.getFirstname()))
      {
        str = this.m_contact.getFirstname();
      }
      else if (!TextUtils.isEmpty(this.m_contact.getLastname()))
      {
        str = this.m_contact.getLastname();
      }
      else if (!TextUtils.isEmpty(this.m_contact.getEmail()))
      {
        str = this.m_contact.getEmail();
      }
      else
      {
        boolean bool1 = this.m_contact.hasPhoneNumber();
        str = null;
        if (bool1)
        {
          boolean bool2 = TextUtils.isEmpty(this.m_contact.getPhoneNumber().getSubscriberNumber());
          str = null;
          if (!bool2)
            str = this.m_contact.getPhoneNumber().getSubscriberNumber();
        }
      }
    }
  }

  public SessionMessages.Contact getContact()
  {
    return this.m_contact;
  }

  public Object getDataToLoadImage()
  {
    return Long.valueOf(this.m_contact.getDeviceContactId());
  }

  public String getDisplayName()
  {
    if (this.m_displayName == null)
      refreshDisplayName();
    return this.m_displayName;
  }

  public String getDisplayNameShort()
  {
    if (this.m_displayNameShort == null)
      refreshDisplayNameShort();
    return this.m_displayNameShort;
  }

  public Object getImageId()
  {
    return Long.valueOf(this.m_contact.getDeviceContactId());
  }

  public int getViewTag()
  {
    return hashCode();
  }

  public boolean isSame(ConversationContact paramConversationContact)
  {
    return this.m_contact.getAccountid().equals(paramConversationContact.getContact().getAccountid());
  }

  public void onLoadFailed()
  {
    this.m_hasPhoto = false;
  }

  public boolean shouldHaveImage()
  {
    return this.m_hasPhoto;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationContact
 * JD-Core Version:    0.6.2
 */