package com.sgiggle.production.model;

import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationPayload;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ConversationDetail
{
  private static String TAG = "Tango.ConversationDetail";
  private Map<String, ConversationContact> m_contactsByAccountId;
  private String m_conversationId;
  private LinkedList<ConversationMessage> m_conversationMessages;
  private List<ConversationContact> m_conversationPeers;
  private boolean m_isGroupConversation = false;
  private boolean m_moreMessageAvailable = false;
  private ConversationContact m_myself;

  public ConversationDetail(SessionMessages.ConversationPayload paramConversationPayload)
  {
    this.m_moreMessageAvailable = paramConversationPayload.getMoreMessageAvailable();
    this.m_contactsByAccountId = new HashMap();
    this.m_conversationId = paramConversationPayload.getConversationId();
    this.m_myself = new ConversationContact(paramConversationPayload.getMyself());
    this.m_contactsByAccountId.put(this.m_myself.getContact().getAccountid(), this.m_myself);
    this.m_conversationPeers = new ArrayList();
    this.m_conversationPeers.add(new ConversationContact(paramConversationPayload.getPeer()));
    Iterator localIterator1 = this.m_conversationPeers.iterator();
    while (localIterator1.hasNext())
    {
      ConversationContact localConversationContact = (ConversationContact)localIterator1.next();
      this.m_contactsByAccountId.put(localConversationContact.getContact().getAccountid(), localConversationContact);
    }
    this.m_conversationMessages = new LinkedList();
    Iterator localIterator2 = paramConversationPayload.getMessageList().iterator();
    while (localIterator2.hasNext())
      addConversationMessage((SessionMessages.ConversationMessage)localIterator2.next());
  }

  private void addConversationMessage(ConversationMessage paramConversationMessage)
  {
    if (this.m_conversationMessages == null)
      this.m_conversationMessages = new LinkedList();
    this.m_conversationMessages.add(paramConversationMessage);
  }

  private ConversationMessage buildConversationMessage(SessionMessages.ConversationMessage paramConversationMessage)
  {
    String str = paramConversationMessage.getPeer().getAccountid();
    if ((!paramConversationMessage.getIsFromMe()) && (!this.m_contactsByAccountId.containsKey(str)))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Could not find accountId='").append(str).append("' (message ID='").append(paramConversationMessage.getMessageId()).append("', conv ID='").append(paramConversationMessage.getConversationId()).append("'). List of known peer account IDs for this conversation: ");
      Iterator localIterator = this.m_contactsByAccountId.values().iterator();
      while (localIterator.hasNext())
      {
        ConversationContact localConversationContact2 = (ConversationContact)localIterator.next();
        localStringBuilder.append("'").append(localConversationContact2.getContact().getAccountid()).append("', ");
      }
      localStringBuilder.append(" self accountId='").append(this.m_myself.getContact().getAccountid()).append("'");
      Log.e(TAG, localStringBuilder.toString());
    }
    if (paramConversationMessage.getIsFromMe());
    for (ConversationContact localConversationContact1 = this.m_myself; ; localConversationContact1 = (ConversationContact)this.m_contactsByAccountId.get(str))
      return ConversationMessageFactory.create(paramConversationMessage, localConversationContact1);
  }

  private ConversationMessage getMessageById(int paramInt)
  {
    Iterator localIterator = this.m_conversationMessages.iterator();
    while (localIterator.hasNext())
    {
      ConversationMessage localConversationMessage = (ConversationMessage)localIterator.next();
      if (localConversationMessage.getMessageId() == paramInt)
        return localConversationMessage;
    }
    return null;
  }

  public ConversationMessage addConversationMessage(SessionMessages.ConversationMessage paramConversationMessage)
  {
    ConversationMessage localConversationMessage = buildConversationMessage(paramConversationMessage);
    addConversationMessage(localConversationMessage);
    return localConversationMessage;
  }

  public void addConversationMessagesAtFront(List<SessionMessages.ConversationMessage> paramList)
  {
    if (paramList == null)
      return;
    LinkedList localLinkedList = new LinkedList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      localLinkedList.add(buildConversationMessage((SessionMessages.ConversationMessage)localIterator.next()));
    if (this.m_conversationMessages == null)
      this.m_conversationMessages = new LinkedList();
    this.m_conversationMessages.addAll(0, localLinkedList);
  }

  public List<SessionMessages.Contact> convertPeersToContactList()
  {
    ArrayList localArrayList = new ArrayList();
    if (this.m_conversationPeers != null)
    {
      Iterator localIterator = this.m_conversationPeers.iterator();
      while (localIterator.hasNext())
        localArrayList.add(((ConversationContact)localIterator.next()).getContact());
    }
    return localArrayList;
  }

  public ConversationMessage deleteConversationMessage(int paramInt)
  {
    if (this.m_conversationMessages == null)
      return null;
    Iterator localIterator = this.m_conversationMessages.iterator();
    while (localIterator.hasNext())
    {
      ConversationMessage localConversationMessage = (ConversationMessage)localIterator.next();
      if (localConversationMessage.getMessageId() == paramInt)
      {
        this.m_conversationMessages.remove(localConversationMessage);
        return localConversationMessage;
      }
    }
    return null;
  }

  public String getConversationId()
  {
    return this.m_conversationId;
  }

  public int getConversationMessageCount()
  {
    if (this.m_conversationMessages == null)
      return 0;
    return this.m_conversationMessages.size();
  }

  public List<ConversationMessage> getConversationMessages()
  {
    return this.m_conversationMessages;
  }

  public List<ConversationContact> getConversationPeers()
  {
    return this.m_conversationPeers;
  }

  public ConversationContact getFirstPeer()
  {
    if ((this.m_conversationPeers == null) || (this.m_conversationPeers.size() == 0))
      return null;
    return (ConversationContact)this.m_conversationPeers.get(0);
  }

  public boolean getMoreMessageAvailable()
  {
    return this.m_moreMessageAvailable;
  }

  public ConversationMessage getMostRecentMessage()
  {
    if ((this.m_conversationMessages == null) || (this.m_conversationMessages.size() == 0))
      return null;
    return (ConversationMessage)this.m_conversationMessages.getLast();
  }

  public ConversationContact getMyself()
  {
    return this.m_myself;
  }

  public int getOldestMessageId()
  {
    if ((this.m_conversationMessages == null) || (this.m_conversationMessages.size() == 0))
      return -1;
    return ((ConversationMessage)this.m_conversationMessages.get(0)).getMessageId();
  }

  public boolean isEmpty()
  {
    return (this.m_conversationMessages == null) || (this.m_conversationMessages.size() == 0);
  }

  public boolean isFromMySelf(ConversationMessage paramConversationMessage)
  {
    return paramConversationMessage.isFromSelf();
  }

  public boolean isGroupConversation()
  {
    return this.m_isGroupConversation;
  }

  public boolean isSystemAccountConversation()
  {
    Iterator localIterator = this.m_conversationPeers.iterator();
    while (localIterator.hasNext())
      if (!((ConversationContact)localIterator.next()).getContact().getIsSystemAccount())
        return false;
    return true;
  }

  public void setMoreMessageAvailable(boolean paramBoolean)
  {
    this.m_moreMessageAvailable = paramBoolean;
  }

  public ConversationMessage updateMessage(int paramInt, SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus)
  {
    ConversationMessage localConversationMessage;
    if (this.m_conversationMessages == null)
      localConversationMessage = null;
    do
    {
      return localConversationMessage;
      localConversationMessage = getMessageById(paramInt);
    }
    while (localConversationMessage == null);
    ConversationMessageFactory.updateStatus(localConversationMessage, paramConversationMessageSendStatus);
    return localConversationMessage;
  }

  public ConversationMessage updateMessage(SessionMessages.ConversationMessage paramConversationMessage)
  {
    ConversationMessage localConversationMessage;
    if (this.m_conversationMessages == null)
      localConversationMessage = null;
    do
    {
      return localConversationMessage;
      localConversationMessage = getMessageById(paramConversationMessage.getMessageId());
    }
    while (localConversationMessage == null);
    ConversationMessageFactory.update(localConversationMessage, paramConversationMessage);
    return localConversationMessage;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationDetail
 * JD-Core Version:    0.6.2
 */