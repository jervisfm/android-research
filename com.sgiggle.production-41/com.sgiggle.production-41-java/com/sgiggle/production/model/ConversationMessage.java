package com.sgiggle.production.model;

import android.content.Context;
import android.content.res.Resources;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage.Builder;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageLoadingStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.RobotMessageType;

public abstract class ConversationMessage
{
  public static final int MESSAGE_ID_UNKNOWN = -1;
  private ConversationContact m_fromContact;
  private boolean m_isFromSelf;
  private SessionMessages.ConversationMessageLoadingStatus m_loadingStatus;
  private int m_messageId;
  private long m_readStatusTimestampMs;
  private SessionMessages.ConversationMessageSendStatus m_status;
  protected String m_text;
  private String m_textInSms;
  private long m_timestampMs;
  private SessionMessages.ConversationMessageType m_type;

  protected ConversationMessage(int paramInt, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString, long paramLong1, boolean paramBoolean, ConversationContact paramConversationContact, SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus, SessionMessages.ConversationMessageLoadingStatus paramConversationMessageLoadingStatus, long paramLong2, SessionMessages.RobotMessageType paramRobotMessageType)
  {
    this.m_messageId = paramInt;
    this.m_type = paramConversationMessageType;
    this.m_text = paramString;
    this.m_timestampMs = paramLong1;
    this.m_isFromSelf = paramBoolean;
    this.m_fromContact = paramConversationContact;
    this.m_status = paramConversationMessageSendStatus;
    this.m_loadingStatus = paramConversationMessageLoadingStatus;
    this.m_readStatusTimestampMs = paramLong2;
  }

  public static boolean isStatusError(SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus)
  {
    return (!isStatusSent(paramConversationMessageSendStatus)) && (!isStatusSending(paramConversationMessageSendStatus));
  }

  public static boolean isStatusSending(SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus)
  {
    return (paramConversationMessageSendStatus == SessionMessages.ConversationMessageSendStatus.STATUS_INIT) || (paramConversationMessageSendStatus == SessionMessages.ConversationMessageSendStatus.STATUS_TRIMMING_VIDEO) || (paramConversationMessageSendStatus == SessionMessages.ConversationMessageSendStatus.STATUS_UPLOADING) || (paramConversationMessageSendStatus == SessionMessages.ConversationMessageSendStatus.STATUS_READY_TO_SEND) || (paramConversationMessageSendStatus == SessionMessages.ConversationMessageSendStatus.STATUS_SENDING);
  }

  public static boolean isStatusSent(SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus)
  {
    return paramConversationMessageSendStatus == SessionMessages.ConversationMessageSendStatus.STATUS_SENT;
  }

  public SessionMessages.ConversationMessage convertToSessionConversationMessage(String paramString)
  {
    return SessionMessages.ConversationMessage.newBuilder().setType(this.m_type).setConversationId(paramString).setMessageId(this.m_messageId).build();
  }

  public ConversationContact getFromContact()
  {
    return this.m_fromContact;
  }

  public int getMessageId()
  {
    return this.m_messageId;
  }

  public long getReadStatusTimestampMs()
  {
    return this.m_readStatusTimestampMs;
  }

  protected SessionMessages.ConversationMessageSendStatus getSendStatus()
  {
    return this.m_status;
  }

  public String getSummaryPrimaryText(Context paramContext, boolean paramBoolean)
  {
    if ((isStatusError()) && (paramBoolean))
      return paramContext.getResources().getString(2131296564);
    return getText(paramBoolean);
  }

  public String getSummarySecondaryText(Context paramContext, boolean paramBoolean)
  {
    return "";
  }

  public final String getText()
  {
    return getText(false);
  }

  public abstract String getText(boolean paramBoolean);

  public String getTextInSms()
  {
    return this.m_textInSms;
  }

  public long getTimestampMs()
  {
    return this.m_timestampMs;
  }

  public SessionMessages.ConversationMessageType getType()
  {
    return this.m_type;
  }

  public boolean isFromSelf()
  {
    return this.m_isFromSelf;
  }

  public boolean isLoadingStatusLoaded()
  {
    return this.m_loadingStatus == SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_THUMBNAIL_LOADED;
  }

  public boolean isLoadingStatusLoading()
  {
    return this.m_loadingStatus == SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_THUMBNAIL_LOADING;
  }

  public boolean isStatusError()
  {
    return (isFromSelf()) && (!isStatusSent(this.m_status)) && (!isStatusSending(this.m_status)) && (!isStatusRead()) && (!isStatusReadShow());
  }

  public boolean isStatusErrorPeerNotSupported()
  {
    return this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_ERROR_PEER_NOT_SUPPORT;
  }

  public boolean isStatusErrorPeerPlatformNotSupported()
  {
    return this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_ERROR_PEER_PLATFORM_NOT_SUPPORT;
  }

  public boolean isStatusRead()
  {
    return this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_READ;
  }

  public boolean isStatusReadShow()
  {
    return this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_READ_AND_SHOW_STATUS;
  }

  public boolean isStatusSending()
  {
    return (isFromSelf()) && (isStatusSending(this.m_status));
  }

  public boolean isStatusSent()
  {
    return isStatusSent(this.m_status);
  }

  public boolean isStatusUnknown()
  {
    return this.m_status == null;
  }

  public boolean isStatusUploaded()
  {
    return (this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_READY_TO_SEND) || (this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_SENDING) || (this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_SENT) || (this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_READ) || (this.m_status == SessionMessages.ConversationMessageSendStatus.STATUS_READ_AND_SHOW_STATUS);
  }

  public void setLoadingStatus(SessionMessages.ConversationMessageLoadingStatus paramConversationMessageLoadingStatus)
  {
    this.m_loadingStatus = paramConversationMessageLoadingStatus;
  }

  public void setReadStatusTimestampMs(long paramLong)
  {
    this.m_readStatusTimestampMs = paramLong;
  }

  public void setStatus(SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus)
  {
    this.m_status = paramConversationMessageSendStatus;
  }

  public void setTextInSms(String paramString)
  {
    this.m_textInSms = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationMessage
 * JD-Core Version:    0.6.2
 */