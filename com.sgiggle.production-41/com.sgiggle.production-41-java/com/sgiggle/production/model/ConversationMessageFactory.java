package com.sgiggle.production.model;

import android.text.TextUtils;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageLoadingStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.RobotMessageType;
import com.sgiggle.xmpp.SessionMessages.VGoodBundle;
import com.sgiggle.xmpp.SessionMessages.VGoodCinematic;
import com.sgiggle.xmpp.SessionMessages.VGoodSelectorImage;

public class ConversationMessageFactory
{
  private static boolean canBePurchased(SessionMessages.ConversationMessage paramConversationMessage)
  {
    if (!paramConversationMessage.hasProduct())
      return false;
    if (paramConversationMessage.getProduct().getPurchased())
      return false;
    long l = System.currentTimeMillis() / 1000L;
    if ((paramConversationMessage.getProduct().hasBeginTime()) && (l < paramConversationMessage.getProduct().getBeginTime()))
      return false;
    return (!paramConversationMessage.getProduct().hasEndTime()) || (paramConversationMessage.getProduct().getEndTime() >= l);
  }

  public static ConversationMessage create(SessionMessages.ConversationMessage paramConversationMessage, ConversationContact paramConversationContact)
  {
    switch (1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType[paramConversationMessage.getType().ordinal()])
    {
    default:
      throw new UnsupportedOperationException("Not implemented! Cannot create message of type=" + paramConversationMessage.getType());
    case 1:
      return createText(paramConversationMessage, paramConversationContact);
    case 2:
      return createVideoMail(paramConversationMessage, paramConversationContact);
    case 3:
      return createVGood(paramConversationMessage, paramConversationContact);
    case 4:
    }
    return createPicture(paramConversationMessage, paramConversationContact);
  }

  private static ConversationMessage createPicture(SessionMessages.ConversationMessage paramConversationMessage, ConversationContact paramConversationContact)
  {
    int i = paramConversationMessage.getMessageId();
    SessionMessages.ConversationMessageType localConversationMessageType = paramConversationMessage.getType();
    String str1 = paramConversationMessage.getText();
    long l1 = paramConversationMessage.getTimeSend();
    boolean bool = paramConversationMessage.getIsFromMe();
    SessionMessages.ConversationMessageSendStatus localConversationMessageSendStatus = paramConversationMessage.getSendStatus();
    SessionMessages.ConversationMessageLoadingStatus localConversationMessageLoadingStatus = paramConversationMessage.getLoadingStatus();
    long l2;
    int j;
    String str2;
    if (paramConversationMessage.hasTimePeerRead())
    {
      l2 = paramConversationMessage.getTimePeerRead();
      j = paramConversationMessage.getProgress();
      str2 = paramConversationMessage.getThumbnailPath();
      if (!paramConversationMessage.hasRobotMessageType())
        break label115;
    }
    label115: for (SessionMessages.RobotMessageType localRobotMessageType = paramConversationMessage.getRobotMessageType(); ; localRobotMessageType = null)
    {
      return new ConversationMessagePicture(i, localConversationMessageType, str1, l1, bool, paramConversationContact, localConversationMessageSendStatus, localConversationMessageLoadingStatus, l2, j, str2, localRobotMessageType);
      l2 = -1L;
      break;
    }
  }

  private static ConversationMessage createText(SessionMessages.ConversationMessage paramConversationMessage, ConversationContact paramConversationContact)
  {
    int i = paramConversationMessage.getMessageId();
    SessionMessages.ConversationMessageType localConversationMessageType = paramConversationMessage.getType();
    String str = paramConversationMessage.getText();
    long l1 = paramConversationMessage.getTimeSend();
    boolean bool = paramConversationMessage.getIsFromMe();
    SessionMessages.ConversationMessageSendStatus localConversationMessageSendStatus = paramConversationMessage.getSendStatus();
    SessionMessages.ConversationMessageLoadingStatus localConversationMessageLoadingStatus = paramConversationMessage.getLoadingStatus();
    long l2;
    if (paramConversationMessage.hasTimePeerRead())
    {
      l2 = paramConversationMessage.getTimePeerRead();
      if (!paramConversationMessage.hasRobotMessageType())
        break label99;
    }
    label99: for (SessionMessages.RobotMessageType localRobotMessageType = paramConversationMessage.getRobotMessageType(); ; localRobotMessageType = null)
    {
      return new ConversationMessageText(i, localConversationMessageType, str, l1, bool, paramConversationContact, localConversationMessageSendStatus, localConversationMessageLoadingStatus, l2, localRobotMessageType);
      l2 = -1L;
      break;
    }
  }

  private static ConversationMessage createVGood(SessionMessages.ConversationMessage paramConversationMessage, ConversationContact paramConversationContact)
  {
    int i = paramConversationMessage.getMessageId();
    SessionMessages.ConversationMessageType localConversationMessageType = paramConversationMessage.getType();
    String str1 = paramConversationMessage.getText();
    long l1 = paramConversationMessage.getTimeSend();
    boolean bool1 = paramConversationMessage.getIsFromMe();
    SessionMessages.ConversationMessageSendStatus localConversationMessageSendStatus = paramConversationMessage.getSendStatus();
    SessionMessages.ConversationMessageLoadingStatus localConversationMessageLoadingStatus = paramConversationMessage.getLoadingStatus();
    long l2;
    String str2;
    String str3;
    String str4;
    SessionMessages.RobotMessageType localRobotMessageType;
    label84: boolean bool2;
    String str5;
    if (paramConversationMessage.hasTimePeerRead())
    {
      l2 = paramConversationMessage.getTimePeerRead();
      str2 = paramConversationMessage.getMediaId();
      str3 = getVgoodPath(paramConversationMessage);
      str4 = getVgoodIconPath(paramConversationMessage);
      if (!paramConversationMessage.hasRobotMessageType())
        break label167;
      localRobotMessageType = paramConversationMessage.getRobotMessageType();
      bool2 = canBePurchased(paramConversationMessage);
      if (!paramConversationMessage.hasProduct())
        break label173;
      str5 = paramConversationMessage.getProduct().getProductMarketId();
      label106: if (!paramConversationMessage.hasProduct())
        break label179;
    }
    label167: label173: label179: for (String str6 = paramConversationMessage.getProduct().getExternalMarketId(); ; str6 = null)
    {
      return new ConversationMessageVGood(i, localConversationMessageType, str1, l1, bool1, paramConversationContact, localConversationMessageSendStatus, localConversationMessageLoadingStatus, l2, str2, str3, str4, localRobotMessageType, bool2, str5, str6);
      l2 = -1L;
      break;
      localRobotMessageType = null;
      break label84;
      str5 = null;
      break label106;
    }
  }

  private static ConversationMessage createVideoMail(SessionMessages.ConversationMessage paramConversationMessage, ConversationContact paramConversationContact)
  {
    int i = paramConversationMessage.getMessageId();
    SessionMessages.ConversationMessageType localConversationMessageType = paramConversationMessage.getType();
    String str1 = paramConversationMessage.getText();
    long l1 = paramConversationMessage.getTimeSend();
    boolean bool = paramConversationMessage.getIsFromMe();
    SessionMessages.ConversationMessageSendStatus localConversationMessageSendStatus = paramConversationMessage.getSendStatus();
    SessionMessages.ConversationMessageLoadingStatus localConversationMessageLoadingStatus = paramConversationMessage.getLoadingStatus();
    long l2;
    String str2;
    String str3;
    String str4;
    int j;
    String str5;
    String str6;
    if (paramConversationMessage.hasTimePeerRead())
    {
      l2 = paramConversationMessage.getTimePeerRead();
      str2 = paramConversationMessage.getMediaId();
      str3 = paramConversationMessage.getUrl();
      str4 = paramConversationMessage.getPath();
      j = paramConversationMessage.getDuration();
      str5 = paramConversationMessage.getThumbnailPath();
      str6 = paramConversationMessage.getThumbnailUrl();
      if (!paramConversationMessage.hasRobotMessageType())
        break label151;
    }
    label151: for (SessionMessages.RobotMessageType localRobotMessageType = paramConversationMessage.getRobotMessageType(); ; localRobotMessageType = null)
    {
      return new ConversationMessageVideo(i, localConversationMessageType, str1, l1, bool, paramConversationContact, localConversationMessageSendStatus, localConversationMessageLoadingStatus, l2, str2, str3, str4, j, str5, str6, localRobotMessageType, paramConversationMessage.getProgress());
      l2 = -1L;
      break;
    }
  }

  private static String getVgoodIconPath(SessionMessages.ConversationMessage paramConversationMessage)
  {
    if (!paramConversationMessage.hasVgoodBundle())
      return null;
    SessionMessages.VGoodBundle localVGoodBundle = paramConversationMessage.getVgoodBundle();
    if (localVGoodBundle.getImageCount() == 0)
      return null;
    return localVGoodBundle.getImage(0).getPath();
  }

  private static String getVgoodPath(SessionMessages.ConversationMessage paramConversationMessage)
  {
    SessionMessages.VGoodBundle localVGoodBundle = paramConversationMessage.getVgoodBundle();
    if (localVGoodBundle == null)
      return null;
    if (localVGoodBundle.getCinematic() == null)
      return null;
    return localVGoodBundle.getCinematic().getAssetPath();
  }

  public static void update(ConversationMessage paramConversationMessage, SessionMessages.ConversationMessage paramConversationMessage1)
  {
    if (paramConversationMessage1.hasSendStatus())
      paramConversationMessage.setStatus(paramConversationMessage1.getSendStatus());
    if (paramConversationMessage1.hasTimePeerRead())
      paramConversationMessage.setReadStatusTimestampMs(paramConversationMessage1.getTimePeerRead());
    switch (1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType[paramConversationMessage.getType().ordinal()])
    {
    default:
      throw new UnsupportedOperationException("Not implemented!");
    case 2:
      ConversationMessageVideo localConversationMessageVideo = (ConversationMessageVideo)paramConversationMessage;
      localConversationMessageVideo.setLoadingStatus(paramConversationMessage1.getLoadingStatus());
      if (!TextUtils.isEmpty(paramConversationMessage1.getThumbnailPath()))
        localConversationMessageVideo.setThumbnailLocalPath(paramConversationMessage1.getThumbnailPath());
      if (localConversationMessageVideo.isStatusSending())
        localConversationMessageVideo.setProgress(paramConversationMessage1.getProgress());
    case 1:
    case 3:
      return;
    case 4:
    }
    ConversationMessagePicture localConversationMessagePicture = (ConversationMessagePicture)paramConversationMessage;
    if (localConversationMessagePicture.isFromSelf())
    {
      updateSentPictureMessage(localConversationMessagePicture, paramConversationMessage1);
      return;
    }
    updateReceivedPictureMessage(localConversationMessagePicture, paramConversationMessage1);
  }

  private static void updateReceivedPictureMessage(ConversationMessagePicture paramConversationMessagePicture, SessionMessages.ConversationMessage paramConversationMessage)
  {
    SessionMessages.ConversationMessageLoadingStatus localConversationMessageLoadingStatus = paramConversationMessage.getLoadingStatus();
    if ((localConversationMessageLoadingStatus == SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_THUMBNAIL_LOADED) || (localConversationMessageLoadingStatus == SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_THUMBNAIL_ERROR) || (localConversationMessageLoadingStatus == SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_THUMBNAIL_LOADING))
    {
      paramConversationMessagePicture.setLoadingStatus(localConversationMessageLoadingStatus);
      paramConversationMessagePicture.setProgress(paramConversationMessage.getProgress());
      if (!TextUtils.isEmpty(paramConversationMessage.getThumbnailPath()))
        paramConversationMessagePicture.setThumbnailLocalPath(paramConversationMessage.getThumbnailPath());
    }
  }

  private static void updateSentPictureMessage(ConversationMessagePicture paramConversationMessagePicture, SessionMessages.ConversationMessage paramConversationMessage)
  {
    paramConversationMessagePicture.setLoadingStatus(paramConversationMessage.getLoadingStatus());
    paramConversationMessagePicture.setStatus(paramConversationMessage.getSendStatus());
    paramConversationMessagePicture.setProgress(paramConversationMessage.getProgress());
    paramConversationMessagePicture.setTextInSms(paramConversationMessage.getText());
    if (!TextUtils.isEmpty(paramConversationMessage.getThumbnailPath()))
      paramConversationMessagePicture.setThumbnailLocalPath(paramConversationMessage.getThumbnailPath());
  }

  public static void updateStatus(ConversationMessage paramConversationMessage, SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus)
  {
    paramConversationMessage.setStatus(paramConversationMessageSendStatus);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationMessageFactory
 * JD-Core Version:    0.6.2
 */