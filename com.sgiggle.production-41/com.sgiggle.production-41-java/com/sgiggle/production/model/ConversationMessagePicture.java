package com.sgiggle.production.model;

import android.content.Context;
import android.content.res.Resources;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.util.BitmapLoaderThread.LoadableImage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage.Builder;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageLoadingStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.RobotMessageType;

public class ConversationMessagePicture extends ConversationMessage
  implements BitmapLoaderThread.LoadableImage
{
  private int m_progress;
  private boolean m_thumbnailFailedToLoad = false;
  private String m_thumbnailLocalPath;

  protected ConversationMessagePicture(int paramInt1, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString1, long paramLong1, boolean paramBoolean, ConversationContact paramConversationContact, SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus, SessionMessages.ConversationMessageLoadingStatus paramConversationMessageLoadingStatus, long paramLong2, int paramInt2, String paramString2, SessionMessages.RobotMessageType paramRobotMessageType)
  {
    super(paramInt1, paramConversationMessageType, paramString1, paramLong1, paramBoolean, paramConversationContact, paramConversationMessageSendStatus, paramConversationMessageLoadingStatus, paramLong2, paramRobotMessageType);
    this.m_progress = paramInt2;
    this.m_thumbnailLocalPath = paramString2;
  }

  public SessionMessages.ConversationMessage convertToSessionConversationMessage(String paramString)
  {
    return SessionMessages.ConversationMessage.newBuilder().setType(getType()).setConversationId(paramString).setMessageId(getMessageId()).setTextIfNotSupport(TangoApp.getInstance().getApplicationContext().getResources().getString(2131296611)).build();
  }

  public Object getDataToLoadImage()
  {
    return this.m_thumbnailLocalPath;
  }

  public Object getImageId()
  {
    return getClass().getSimpleName() + getMessageId();
  }

  public int getProgress()
  {
    return this.m_progress;
  }

  public String getText(boolean paramBoolean)
  {
    Resources localResources = TangoApp.getInstance().getApplicationContext().getResources();
    if (paramBoolean);
    for (int i = 2131296609; ; i = 2131296610)
      return localResources.getString(i);
  }

  public String getThumbnailLocalPath()
  {
    return this.m_thumbnailLocalPath;
  }

  public int getViewTag()
  {
    return hashCode();
  }

  public void onLoadFailed()
  {
    this.m_thumbnailFailedToLoad = true;
  }

  public void setProgress(int paramInt)
  {
    this.m_progress = paramInt;
  }

  public void setThumbnailLocalPath(String paramString)
  {
    String str = this.m_thumbnailLocalPath;
    this.m_thumbnailLocalPath = paramString;
    if ((str == null) || (!str.equals(paramString)))
      this.m_thumbnailFailedToLoad = false;
  }

  public boolean shouldHaveImage()
  {
    return !this.m_thumbnailFailedToLoad;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationMessagePicture
 * JD-Core Version:    0.6.2
 */