package com.sgiggle.production.model;

import android.content.Context;
import android.content.res.Resources;
import com.sgiggle.production.TangoApp;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageLoadingStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.RobotMessageType;

public class ConversationMessageText extends ConversationMessage
{
  private static final String TAG = "Tango.ConversationMessageText";

  protected ConversationMessageText(int paramInt, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString, long paramLong1, boolean paramBoolean, ConversationContact paramConversationContact, SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus, SessionMessages.ConversationMessageLoadingStatus paramConversationMessageLoadingStatus, long paramLong2, SessionMessages.RobotMessageType paramRobotMessageType)
  {
    super(paramInt, paramConversationMessageType, buildMessageText(paramString, paramRobotMessageType, paramConversationContact.getContact().getIsSystemAccount()), paramLong1, paramBoolean, paramConversationContact, paramConversationMessageSendStatus, paramConversationMessageLoadingStatus, paramLong2, paramRobotMessageType);
  }

  protected static String buildMessageText(String paramString, SessionMessages.RobotMessageType paramRobotMessageType, boolean paramBoolean)
  {
    Context localContext = TangoApp.getInstance().getApplicationContext();
    String str = null;
    if (paramRobotMessageType != null)
    {
      str = null;
      if (paramBoolean)
        switch (1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$RobotMessageType[paramRobotMessageType.ordinal()])
        {
        default:
          Log.w("Tango.ConversationMessageText", "buildMessageText: unknown ROBOT message type=" + paramRobotMessageType);
        case 1:
        case 2:
        case 3:
        }
    }
    while (true)
    {
      if (str == null)
        str = paramString;
      return str;
      str = localContext.getResources().getString(2131296598);
      continue;
      str = localContext.getResources().getString(2131296599);
    }
  }

  public String getText(boolean paramBoolean)
  {
    return this.m_text;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationMessageText
 * JD-Core Version:    0.6.2
 */