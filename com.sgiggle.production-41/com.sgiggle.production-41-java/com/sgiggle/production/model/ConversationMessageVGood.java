package com.sgiggle.production.model;

import android.content.Context;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.util.BitmapLoaderThread.LoadableImage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageLoadingStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.RobotMessageType;
import java.util.Scanner;

public class ConversationMessageVGood extends ConversationMessage
  implements BitmapLoaderThread.LoadableImage
{
  private boolean m_canBePurchased = false;
  private String m_externalMarketId;
  private boolean m_iconFailedToLoad = false;
  private String m_mediaId;
  private String m_productId;
  private long m_seed;
  private String m_vgoodIconPath;
  private long m_vgoodId;
  private String m_vgoodPath;

  protected ConversationMessageVGood(int paramInt, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString1, long paramLong1, boolean paramBoolean1, ConversationContact paramConversationContact, SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus, SessionMessages.ConversationMessageLoadingStatus paramConversationMessageLoadingStatus, long paramLong2, String paramString2, String paramString3, String paramString4, SessionMessages.RobotMessageType paramRobotMessageType, boolean paramBoolean2, String paramString5, String paramString6)
  {
    super(paramInt, paramConversationMessageType, paramString1, paramLong1, paramBoolean1, paramConversationContact, paramConversationMessageSendStatus, paramConversationMessageLoadingStatus, paramLong2, paramRobotMessageType);
    this.m_mediaId = paramString2;
    this.m_vgoodPath = paramString3;
    this.m_vgoodIconPath = paramString4;
    Scanner localScanner = new Scanner(paramString2);
    localScanner.useDelimiter(":");
    long l1;
    if (localScanner.hasNextLong())
    {
      l1 = localScanner.nextLong();
      this.m_vgoodId = l1;
      if (!localScanner.hasNextLong())
        break label135;
    }
    label135: for (long l2 = localScanner.nextLong(); ; l2 = 0L)
    {
      this.m_seed = l2;
      this.m_canBePurchased = paramBoolean2;
      this.m_productId = paramString5;
      this.m_externalMarketId = paramString6;
      return;
      l1 = 0L;
      break;
    }
  }

  public boolean canBePurchased()
  {
    return this.m_canBePurchased;
  }

  public Object getDataToLoadImage()
  {
    return this.m_vgoodIconPath;
  }

  public String getExternalMarketId()
  {
    return this.m_externalMarketId;
  }

  public Object getImageId()
  {
    return this.m_vgoodIconPath;
  }

  public String getMediaId()
  {
    return this.m_mediaId;
  }

  public String getProductId()
  {
    return this.m_productId;
  }

  public long getSeed()
  {
    return this.m_seed;
  }

  public String getText(boolean paramBoolean)
  {
    return TangoApp.getInstance().getApplicationContext().getString(2131296605);
  }

  public String getVGoodIconPath()
  {
    return this.m_vgoodIconPath;
  }

  public long getVGoodId()
  {
    return this.m_vgoodId;
  }

  public String getVGoodPath()
  {
    return this.m_vgoodPath;
  }

  public int getViewTag()
  {
    return hashCode();
  }

  public void onLoadFailed()
  {
    this.m_iconFailedToLoad = true;
  }

  public boolean shouldHaveImage()
  {
    return !this.m_iconFailedToLoad;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationMessageVGood
 * JD-Core Version:    0.6.2
 */