package com.sgiggle.production.model;

import android.content.Context;
import android.content.res.Resources;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.screens.videomail.VideomailUtils;
import com.sgiggle.production.util.BitmapLoaderThread.LoadableImage;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage.Builder;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageLoadingStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.RobotMessageType;

public class ConversationMessageVideo extends ConversationMessage
  implements BitmapLoaderThread.LoadableImage
{
  private static final String TAG = "ConversationMessageVideo";
  private String m_durationDisplayString;
  private int m_durationSec;
  private boolean m_hasThumbnail = true;
  private String m_mediaId;
  private String m_mediaLocalPath;
  private String m_mediaUrl;
  private int m_progress;
  private boolean m_thumbnailFailedToLoad = false;
  private String m_thumbnailLocalPath;
  private String m_thumbnailRemoteUrl;

  protected ConversationMessageVideo(int paramInt1, SessionMessages.ConversationMessageType paramConversationMessageType, String paramString1, long paramLong1, boolean paramBoolean, ConversationContact paramConversationContact, SessionMessages.ConversationMessageSendStatus paramConversationMessageSendStatus, SessionMessages.ConversationMessageLoadingStatus paramConversationMessageLoadingStatus, long paramLong2, String paramString2, String paramString3, String paramString4, int paramInt2, String paramString5, String paramString6, SessionMessages.RobotMessageType paramRobotMessageType, int paramInt3)
  {
    super(paramInt1, paramConversationMessageType, paramString1, paramLong1, paramBoolean, paramConversationContact, paramConversationMessageSendStatus, paramConversationMessageLoadingStatus, paramLong2, paramRobotMessageType);
    this.m_mediaId = paramString2;
    this.m_mediaUrl = paramString3;
    this.m_mediaLocalPath = paramString4;
    this.m_durationSec = paramInt2;
    this.m_thumbnailLocalPath = paramString5;
    this.m_thumbnailRemoteUrl = paramString6;
    this.m_progress = paramInt3;
  }

  private void formatDurationDisplayString()
  {
    String str = TangoApp.getInstance().getApplicationContext().getString(2131296556);
    int i = this.m_durationSec / 60;
    int j = this.m_durationSec % 60;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Integer.valueOf(i);
    arrayOfObject[1] = Integer.valueOf(j);
    this.m_durationDisplayString = String.format(str, arrayOfObject);
  }

  public SessionMessages.ConversationMessage convertToSessionConversationMessage(String paramString)
  {
    return SessionMessages.ConversationMessage.newBuilder().setType(getType()).setConversationId(paramString).setMessageId(getMessageId()).setTextIfNotSupport(TangoApp.getInstance().getApplicationContext().getResources().getString(2131296430)).build();
  }

  public Object getDataToLoadImage()
  {
    return this.m_thumbnailLocalPath;
  }

  public long getDuration()
  {
    return this.m_durationSec;
  }

  public String getDurationDisplayString()
  {
    if (this.m_durationDisplayString == null)
      formatDurationDisplayString();
    return this.m_durationDisplayString;
  }

  public Object getImageId()
  {
    return getClass().getSimpleName() + getMessageId();
  }

  public String getMediaId()
  {
    return this.m_mediaId;
  }

  public String getMediaLocalPath()
  {
    return this.m_mediaLocalPath;
  }

  public String getMediaUrl()
  {
    return this.m_mediaUrl;
  }

  public int getProgress()
  {
    return this.m_progress;
  }

  public String getSummarySecondaryText(Context paramContext, boolean paramBoolean)
  {
    if ((isStatusError()) && (paramBoolean))
      return super.getSummarySecondaryText(paramContext, paramBoolean);
    return getDurationDisplayString();
  }

  public String getText(boolean paramBoolean)
  {
    Resources localResources = TangoApp.getInstance().getApplicationContext().getResources();
    if (paramBoolean);
    for (int i = 2131296554; ; i = 2131296555)
      return localResources.getString(i);
  }

  public String getThumbnailLocalPath()
  {
    return this.m_thumbnailLocalPath;
  }

  public String getThumbnailRemoteUrl()
  {
    return this.m_thumbnailRemoteUrl;
  }

  public int getViewTag()
  {
    return hashCode();
  }

  public boolean isLocalPlaybackAvailable()
  {
    boolean bool = true;
    if ((isFromSelf()) && (!VideomailUtils.isVideomailReplaySupported()) && (!isStatusUploaded()))
    {
      Log.v("ConversationMessageVideo", "Video Message can't be playbacked  playback " + VideomailUtils.isVideomailReplaySupported() + " upload status " + getSendStatus());
      bool = false;
    }
    return bool;
  }

  public void onLoadFailed()
  {
    this.m_thumbnailFailedToLoad = true;
  }

  public void setProgress(int paramInt)
  {
    this.m_progress = paramInt;
  }

  public void setThumbnailLocalPath(String paramString)
  {
    String str = this.m_thumbnailLocalPath;
    this.m_thumbnailLocalPath = paramString;
    if ((str == null) || (!str.equals(paramString)))
      this.m_thumbnailFailedToLoad = false;
  }

  public void setThumbnailRemoteUrl(String paramString)
  {
    String str = this.m_thumbnailRemoteUrl;
    this.m_thumbnailRemoteUrl = paramString;
    if ((!this.m_hasThumbnail) && (str != null) && (!str.equals(paramString)))
      this.m_hasThumbnail = true;
  }

  public boolean shouldHaveImage()
  {
    return !this.m_thumbnailFailedToLoad;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationMessageVideo
 * JD-Core Version:    0.6.2
 */