package com.sgiggle.production.model;

import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationSummary;

public class ConversationSummary
{
  private String m_conversationId;
  private String m_displayString;
  private ConversationMessage m_lastMessage;
  private ConversationContact m_peer;
  private int m_unreadMessageCount;

  public ConversationSummary(SessionMessages.ConversationSummary paramConversationSummary, ConversationContact paramConversationContact)
  {
    setData(paramConversationSummary, paramConversationContact);
  }

  public String getConversationId()
  {
    return this.m_conversationId;
  }

  public String getDisplayString()
  {
    return this.m_displayString;
  }

  public ConversationMessage getLastMessage()
  {
    return this.m_lastMessage;
  }

  public ConversationContact getPeer()
  {
    return this.m_peer;
  }

  public int getUnreadMessageCount()
  {
    return this.m_unreadMessageCount;
  }

  public void setData(SessionMessages.ConversationSummary paramConversationSummary, ConversationContact paramConversationContact)
  {
    this.m_peer = new ConversationContact(paramConversationSummary.getPeer());
    if (paramConversationSummary.getLast().getIsFromMe());
    for (ConversationContact localConversationContact = paramConversationContact; ; localConversationContact = new ConversationContact(paramConversationSummary.getPeer()))
    {
      this.m_lastMessage = ConversationMessageFactory.create(paramConversationSummary.getLast(), localConversationContact);
      this.m_conversationId = paramConversationSummary.getConversationId();
      this.m_unreadMessageCount = paramConversationSummary.getUnreadMessageCount();
      this.m_displayString = this.m_peer.getDisplayName();
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.ConversationSummary
 * JD-Core Version:    0.6.2
 */