package com.sgiggle.production.model;

import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

public abstract interface Model
{
  public abstract void build(InputStream paramInputStream)
    throws JSONException;

  public abstract void fromJSON(JSONObject paramJSONObject)
    throws JSONException;

  public abstract JSONObject toJSON()
    throws JSONException;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.Model
 * JD-Core Version:    0.6.2
 */