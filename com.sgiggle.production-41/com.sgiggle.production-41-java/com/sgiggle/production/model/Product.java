package com.sgiggle.production.model;

import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.xmpp.SessionMessages.Price;

public class Product
  implements Comparable<Product>
{
  private long m_beginTime;
  private String m_category;
  private String m_category_key;
  private long m_endTime;
  private int m_leaseDuration;
  private Managed m_managed;
  private int m_marketId;
  private SessionMessages.Price m_price;
  private String m_productDescription;
  private long m_productId;
  private String m_productMarketId;
  private String m_productName;
  private Constants.PurchaseState m_purchaseState;
  private String m_sku;

  public int compareTo(Product paramProduct)
  {
    if (this.m_leaseDuration > paramProduct.m_leaseDuration)
      return 1;
    if (this.m_leaseDuration < paramProduct.m_leaseDuration)
      return -1;
    return 0;
  }

  public long getBeginTime()
  {
    return this.m_beginTime;
  }

  public String getCategory()
  {
    return this.m_category;
  }

  public String getCategoryKey()
  {
    return this.m_category_key;
  }

  public long getEndTime()
  {
    return this.m_endTime;
  }

  public String getFormattedPrice()
  {
    String str = "";
    if ((this.m_price != null) && (this.m_price.hasValue()))
      str = this.m_price.getLabel();
    return str;
  }

  public int getLeaseDuration()
  {
    return this.m_leaseDuration;
  }

  public Managed getManaged()
  {
    return this.m_managed;
  }

  public int getMarketId()
  {
    return this.m_marketId;
  }

  public SessionMessages.Price getPrice()
  {
    return this.m_price;
  }

  public String getProductDescription()
  {
    return this.m_productDescription;
  }

  public long getProductId()
  {
    return this.m_productId;
  }

  public String getProductMarketId()
  {
    return this.m_productMarketId;
  }

  public String getProductName()
  {
    return this.m_productName;
  }

  public Constants.PurchaseState getPurchaseState()
  {
    return this.m_purchaseState;
  }

  public String getSku()
  {
    return this.m_sku;
  }

  public void setBeginTime(long paramLong)
  {
    this.m_beginTime = paramLong;
  }

  public void setCategory(String paramString)
  {
    this.m_category = paramString;
  }

  public void setCategoryKey(String paramString)
  {
    this.m_category_key = paramString;
  }

  public void setEndTime(long paramLong)
  {
    this.m_endTime = paramLong;
  }

  public void setLeaseDuration(int paramInt)
  {
    this.m_leaseDuration = paramInt;
  }

  public void setManaged(Managed paramManaged)
  {
    this.m_managed = paramManaged;
  }

  public void setMarketId(int paramInt)
  {
    this.m_marketId = paramInt;
  }

  public void setPrice(SessionMessages.Price paramPrice)
  {
    this.m_price = paramPrice;
  }

  public void setProductDescription(String paramString)
  {
    this.m_productDescription = paramString;
  }

  public void setProductId(long paramLong)
  {
    this.m_productId = paramLong;
  }

  public void setProductMarketId(String paramString)
  {
    this.m_productMarketId = paramString;
  }

  public void setProductName(String paramString)
  {
    this.m_productName = paramString;
  }

  public void setPurchaseState(Constants.PurchaseState paramPurchaseState)
  {
    this.m_purchaseState = paramPurchaseState;
  }

  public void setSku(String paramString)
  {
    this.m_sku = paramString;
  }

  public static enum Managed
  {
    static
    {
      Managed[] arrayOfManaged = new Managed[2];
      arrayOfManaged[0] = MANAGED;
      arrayOfManaged[1] = UNMANAGED;
    }

    public static Managed valueOf(int paramInt)
    {
      Managed[] arrayOfManaged = values();
      if ((paramInt < 0) || (paramInt >= arrayOfManaged.length))
        return UNMANAGED;
      return arrayOfManaged[paramInt];
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.Product
 * JD-Core Version:    0.6.2
 */