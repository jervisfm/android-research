package com.sgiggle.production.model;

public class VGoodPurchaseItem
{
  public static final String PRODUCT_1MONTH = "me.tango.tangodev.moments.pass.1month";
  public static final String PRODUCT_7DAYS = "me.tango.tangodev.moments.pass.7day";
  public static final String PRODUCT_FAKE = "android.test.purchased";
  private String m_description;
  private final String m_name;
  private final String m_productId;

  public VGoodPurchaseItem(String paramString1, String paramString2)
  {
    this(paramString1, paramString2, null);
  }

  public VGoodPurchaseItem(String paramString1, String paramString2, String paramString3)
  {
    this.m_productId = paramString1;
    this.m_name = paramString2;
    this.m_description = paramString3;
  }

  public String getDescription()
  {
    return this.m_description;
  }

  public String getName()
  {
    return this.m_name;
  }

  public String getProductId()
  {
    return this.m_productId;
  }

  public void setDescription(String paramString)
  {
    this.m_description = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.VGoodPurchaseItem
 * JD-Core Version:    0.6.2
 */