package com.sgiggle.production.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.json.JSONException;
import org.json.JSONObject;

public class VideomailResponse
  implements Model
{
  private static final String KEY_CODE = "code";
  private static final String KEY_ERROR = "error";
  private static final String KEY_ID = "video_mail_id";
  private static final String KEY_URL = "video_mail_url";
  private int m_code;
  private String m_error;
  private String m_videoMailID;
  private String m_videoMailURL;

  private static String convertStreamToString(InputStream paramInputStream)
  {
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(paramInputStream));
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      while (true)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
          break;
        localStringBuilder.append(str + "\n");
      }
    }
    catch (IOException localIOException2)
    {
      localIOException2 = localIOException2;
      localIOException2.printStackTrace();
      try
      {
        paramInputStream.close();
        while (true)
        {
          return localStringBuilder.toString();
          try
          {
            paramInputStream.close();
          }
          catch (IOException localIOException4)
          {
            localIOException4.printStackTrace();
          }
        }
      }
      catch (IOException localIOException3)
      {
        while (true)
          localIOException3.printStackTrace();
      }
    }
    finally
    {
    }
    try
    {
      paramInputStream.close();
      throw localObject;
    }
    catch (IOException localIOException1)
    {
      while (true)
        localIOException1.printStackTrace();
    }
  }

  public void build(InputStream paramInputStream)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject(convertStreamToString(paramInputStream));
    if (localJSONObject != null)
      fromJSON(localJSONObject);
  }

  public void fromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
    if (paramJSONObject.has("video_mail_id"))
      this.m_videoMailID = paramJSONObject.getString("video_mail_id");
    if (paramJSONObject.has("video_mail_url"))
      this.m_videoMailURL = paramJSONObject.getString("video_mail_url");
    if (paramJSONObject.has("code"))
      this.m_code = paramJSONObject.getInt("code");
    if (paramJSONObject.has("error"))
      this.m_error = paramJSONObject.getString("error");
  }

  public int getCode()
  {
    return this.m_code;
  }

  public String getErrorMessage()
  {
    return this.m_error;
  }

  public String getVideoMailID()
  {
    return this.m_videoMailID;
  }

  public String getVideoMailURL()
  {
    return this.m_videoMailURL;
  }

  public JSONObject toJSON()
    throws JSONException
  {
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.VideomailResponse
 * JD-Core Version:    0.6.2
 */