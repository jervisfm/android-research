package com.sgiggle.production.model;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VideomailTokenRequest
  implements Model
{
  private static final String KEY_ACCOUNT_ID = "account_id";
  private static final String KEY_CALLEES = "callees";
  private static final String KEY_DURATION = "duration";
  private static final String KEY_MIME_TYPE = "mime";
  private static final String KEY_SIZE = "size";
  private String m_accountId;
  private List<String> m_callees = new ArrayList();
  private long m_duration;
  private String m_mimeType;
  private long m_size;

  public void addCallee(String paramString)
  {
    this.m_callees.add(paramString);
  }

  public void build(InputStream paramInputStream)
    throws JSONException
  {
  }

  public void fromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
  }

  public String getAccountId()
  {
    return this.m_accountId;
  }

  public List<String> getCallees()
  {
    return this.m_callees;
  }

  public long getDuration()
  {
    return this.m_duration;
  }

  public String getMimeType()
  {
    return this.m_mimeType;
  }

  public long getSize()
  {
    return this.m_size;
  }

  public void setAccountId(String paramString)
  {
    this.m_accountId = paramString;
  }

  public void setDuration(long paramLong)
  {
    this.m_duration = paramLong;
  }

  public void setMimeType(String paramString)
  {
    this.m_mimeType = paramString;
  }

  public void setSize(long paramLong)
  {
    this.m_size = paramLong;
  }

  public JSONObject toJSON()
    throws JSONException
  {
    JSONObject localJSONObject1 = new JSONObject();
    localJSONObject1.put("account_id", this.m_accountId);
    localJSONObject1.put("mime", this.m_mimeType);
    localJSONObject1.put("size", this.m_size);
    localJSONObject1.put("duration", this.m_duration);
    localJSONObject1.put("size", this.m_size);
    JSONArray localJSONArray = new JSONArray();
    for (int i = 0; i < this.m_callees.size(); i++)
    {
      JSONObject localJSONObject2 = new JSONObject();
      localJSONObject2.put("account_id", this.m_callees.get(i));
      localJSONArray.put(localJSONObject2);
    }
    localJSONObject1.put("callees", localJSONArray);
    return localJSONObject1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.model.VideomailTokenRequest
 * JD-Core Version:    0.6.2
 */