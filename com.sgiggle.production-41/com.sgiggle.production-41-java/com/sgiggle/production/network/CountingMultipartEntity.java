package com.sgiggle.production.network;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.http.entity.mime.MultipartEntity;

public class CountingMultipartEntity extends MultipartEntity
{
  private ProgressListener m_listener;

  public CountingMultipartEntity(ProgressListener paramProgressListener)
  {
    this.m_listener = paramProgressListener;
  }

  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    super.writeTo(new CountingOutputStream(paramOutputStream, this.m_listener));
  }

  static class CountingOutputStream extends FilterOutputStream
  {
    CountingMultipartEntity.ProgressListener m_progressListener;
    long m_transferred;

    public CountingOutputStream(OutputStream paramOutputStream, CountingMultipartEntity.ProgressListener paramProgressListener)
    {
      super();
      this.m_progressListener = paramProgressListener;
      this.m_transferred = 0L;
    }

    public void write(int paramInt)
      throws IOException
    {
      this.out.write(paramInt);
      this.m_transferred = (1L + this.m_transferred);
      this.m_progressListener.transferred(this.m_transferred);
    }

    public void write(byte[] paramArrayOfByte)
      throws IOException
    {
      this.out.write(paramArrayOfByte);
      this.m_transferred += paramArrayOfByte.length;
      this.m_progressListener.transferred(this.m_transferred);
    }

    public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      this.out.write(paramArrayOfByte, paramInt1, paramInt2);
      this.m_transferred += paramInt2;
      this.m_progressListener.transferred(this.m_transferred);
    }
  }

  public static abstract interface ProgressListener
  {
    public abstract void transferred(long paramLong);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.CountingMultipartEntity
 * JD-Core Version:    0.6.2
 */