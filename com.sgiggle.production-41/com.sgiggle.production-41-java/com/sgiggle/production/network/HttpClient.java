package com.sgiggle.production.network;

import com.sgiggle.production.network.command.Command;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpUriRequest;

public abstract interface HttpClient
{
  public abstract HttpResponse execute(Command paramCommand)
    throws MethodNotSupportedException, ClientProtocolException, IOException;

  public abstract HttpResponse execute(HttpUriRequest paramHttpUriRequest)
    throws ClientProtocolException, IOException;

  public abstract void shutdown();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.HttpClient
 * JD-Core Version:    0.6.2
 */