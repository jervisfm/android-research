package com.sgiggle.production.network;

import android.content.Context;
import org.apache.http.auth.Credentials;

public abstract interface HttpConfiguration
{
  public static final String ENCODING_GZIP = "gzip";
  public static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
  public static final String USER_AGENT_BASE = "Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17";

  public abstract Credentials getAuthCredentials();

  public abstract int getHttpBufferSize();

  public abstract int getHttpConnectionTimeout();

  public abstract int getHttpDefaultMaxPerRoute();

  public abstract int getHttpMaxTotalConnections();

  public abstract int getHttpReadTimeout();

  public abstract int getHttpRetryCount();

  public abstract int getHttpRetryIntervalSeconds();

  public abstract String getUserAgent(Context paramContext);

  public abstract boolean isGZIPEnabled();

  public abstract boolean isPrettyDebugEnabled();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.HttpConfiguration
 * JD-Core Version:    0.6.2
 */