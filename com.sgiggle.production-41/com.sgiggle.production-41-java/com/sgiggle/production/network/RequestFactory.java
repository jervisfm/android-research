package com.sgiggle.production.network;

import com.sgiggle.production.network.command.Command;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.NameValuePair;
import org.apache.http.RequestLine;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;

public class RequestFactory
{
  private static String composeUrl(String paramString, List<NameValuePair> paramList)
  {
    if (paramList != null)
      return paramString + "?" + URLEncodedUtils.format(paramList, "UTF-8");
    return paramString;
  }

  private HttpUriRequest parseRequest(String paramString1, String paramString2)
    throws MethodNotSupportedException
  {
    if ("GET".equalsIgnoreCase(paramString1))
      return new HttpGet(paramString2);
    if ("POST".equalsIgnoreCase(paramString1))
      return new HttpPost(paramString2);
    if ("PUT".equalsIgnoreCase(paramString1))
      return new HttpPut(paramString2);
    if ("DELETE".equalsIgnoreCase(paramString1))
      return new HttpDelete(paramString2);
    if ("HEAD".equalsIgnoreCase(paramString1))
      return new HttpHead(paramString2);
    throw new MethodNotSupportedException("Invalid request");
  }

  public HttpUriRequest newHttpRequest(Command paramCommand)
    throws MethodNotSupportedException, UnsupportedEncodingException
  {
    String str1 = paramCommand.getMethod();
    String str2 = paramCommand.getUrl();
    Object localObject;
    if ("GET".equalsIgnoreCase(str1))
      localObject = new HttpGet(composeUrl(str2, paramCommand.getParameters()));
    while (true)
    {
      if (paramCommand.getHeaders() != null)
        ((HttpUriRequest)localObject).setHeaders(paramCommand.getHeaders());
      return localObject;
      HttpPost localHttpPost;
      if ("POST".equalsIgnoreCase(str1))
      {
        localHttpPost = new HttpPost(str2);
        if (paramCommand.getParameters() != null)
        {
          ((HttpPost)localHttpPost).setEntity(new URLEncodedEntity(paramCommand.getParameters()));
          localHttpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
        }
        if (paramCommand.getEntity() != null)
        {
          ((HttpPost)localHttpPost).setEntity(paramCommand.getEntity());
          localObject = localHttpPost;
        }
      }
      else if ("PUT".equalsIgnoreCase(str1))
      {
        localObject = new HttpPut(composeUrl(str2, paramCommand.getParameters()));
      }
      else if ("DELETE".equalsIgnoreCase(str1))
      {
        localObject = new HttpDelete(composeUrl(str2, paramCommand.getParameters()));
      }
      else if ("HEAD".equalsIgnoreCase(str1))
      {
        localObject = new HttpHead(composeUrl(str2, paramCommand.getParameters()));
      }
      else
      {
        throw new MethodNotSupportedException("Invalid request");
        localObject = localHttpPost;
      }
    }
  }

  public HttpUriRequest newHttpRequest(String paramString1, String paramString2)
    throws MethodNotSupportedException
  {
    return parseRequest(paramString1, paramString2);
  }

  public HttpUriRequest newHttpRequest(RequestLine paramRequestLine)
    throws MethodNotSupportedException
  {
    return parseRequest(paramRequestLine.getMethod(), paramRequestLine.getUri());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.RequestFactory
 * JD-Core Version:    0.6.2
 */