package com.sgiggle.production.network;

import android.content.Context;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;

public class TangoHttpClient
  implements HttpClient
{
  private static HttpClient _client;
  private final HttpConfiguration _config;
  private final HttpClient _httpClient;

  public TangoHttpClient(Context paramContext, HttpConfiguration paramHttpConfiguration)
  {
    this._config = paramHttpConfiguration;
    SchemeRegistry localSchemeRegistry = new SchemeRegistry();
    localSchemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
    localSchemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
    BasicHttpParams localBasicHttpParams = new BasicHttpParams();
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, this._config.getHttpConnectionTimeout());
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, this._config.getHttpReadTimeout());
    HttpConnectionParams.setSocketBufferSize(localBasicHttpParams, this._config.getHttpBufferSize());
    HttpProtocolParams.setUserAgent(localBasicHttpParams, this._config.getUserAgent(paramContext));
    ConnManagerParams.setMaxConnectionsPerRoute(localBasicHttpParams, new ConnPerRouteBean(this._config.getHttpDefaultMaxPerRoute()));
    ConnManagerParams.setMaxTotalConnections(localBasicHttpParams, this._config.getHttpMaxTotalConnections());
    this._httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(localBasicHttpParams, localSchemeRegistry), localBasicHttpParams);
    if (this._config.isGZIPEnabled())
    {
      ((DefaultHttpClient)this._httpClient).addRequestInterceptor(new HttpRequestInterceptor()
      {
        public void process(HttpRequest paramAnonymousHttpRequest, HttpContext paramAnonymousHttpContext)
        {
          if (!paramAnonymousHttpRequest.containsHeader("Accept-Encoding"))
            paramAnonymousHttpRequest.addHeader("Accept-Encoding", "gzip");
        }
      });
      ((DefaultHttpClient)this._httpClient).addResponseInterceptor(new HttpResponseInterceptor()
      {
        public void process(HttpResponse paramAnonymousHttpResponse, HttpContext paramAnonymousHttpContext)
        {
          Header localHeader = paramAnonymousHttpResponse.getEntity().getContentEncoding();
          HeaderElement[] arrayOfHeaderElement;
          int i;
          if (localHeader != null)
          {
            arrayOfHeaderElement = localHeader.getElements();
            i = arrayOfHeaderElement.length;
          }
          for (int j = 0; ; j++)
            if (j < i)
            {
              if (arrayOfHeaderElement[j].getName().equalsIgnoreCase("gzip"))
                paramAnonymousHttpResponse.setEntity(new TangoHttpClient.InflatingEntity(paramAnonymousHttpResponse.getEntity()));
            }
            else
              return;
        }
      });
    }
    if (this._config.getAuthCredentials() != null)
      ((DefaultHttpClient)this._httpClient).getCredentialsProvider().setCredentials(AuthScope.ANY, this._config.getAuthCredentials());
  }

  public static HttpClient getInstance(Context paramContext, HttpConfiguration paramHttpConfiguration)
  {
    try
    {
      if (_client == null)
        _client = new TangoHttpClient(paramContext, paramHttpConfiguration);
      for (HttpClient localHttpClient = _client; ; localHttpClient = _client)
        return localHttpClient;
    }
    finally
    {
    }
  }

  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException, ClientProtocolException
  {
    return this._httpClient.execute(paramHttpHost, paramHttpRequest, paramResponseHandler);
  }

  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return this._httpClient.execute(paramHttpHost, paramHttpRequest, paramResponseHandler, paramHttpContext);
  }

  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException, ClientProtocolException
  {
    return this._httpClient.execute(paramHttpUriRequest, paramResponseHandler);
  }

  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return this._httpClient.execute(paramHttpUriRequest, paramResponseHandler, paramHttpContext);
  }

  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException, ClientProtocolException
  {
    return this._httpClient.execute(paramHttpHost, paramHttpRequest);
  }

  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return this._httpClient.execute(paramHttpHost, paramHttpRequest, paramHttpContext);
  }

  public HttpResponse execute(HttpUriRequest paramHttpUriRequest)
    throws IOException, ClientProtocolException
  {
    return this._httpClient.execute(paramHttpUriRequest);
  }

  public HttpResponse execute(HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return this._httpClient.execute(paramHttpUriRequest, paramHttpContext);
  }

  public ClientConnectionManager getConnectionManager()
  {
    return this._httpClient.getConnectionManager();
  }

  public HttpParams getParams()
  {
    return this._httpClient.getParams();
  }

  public void shutdown()
  {
    this._httpClient.getConnectionManager().shutdown();
  }

  private static class InflatingEntity extends HttpEntityWrapper
  {
    public InflatingEntity(HttpEntity paramHttpEntity)
    {
      super();
    }

    public InputStream getContent()
      throws IOException
    {
      return new GZIPInputStream(this.wrappedEntity.getContent());
    }

    public long getContentLength()
    {
      return -1L;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.TangoHttpClient
 * JD-Core Version:    0.6.2
 */