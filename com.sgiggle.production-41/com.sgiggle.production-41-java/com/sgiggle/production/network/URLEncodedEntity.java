package com.sgiggle.production.network;

import java.io.UnsupportedEncodingException;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;

public class URLEncodedEntity extends StringEntity
{
  private static final String DEFAULT_ENCODING = "UTF-8";

  public URLEncodedEntity(List<NameValuePair> paramList)
    throws UnsupportedEncodingException
  {
    this(paramList, "UTF-8");
  }

  public URLEncodedEntity(List<NameValuePair> paramList, String paramString)
    throws UnsupportedEncodingException
  {
    super(composeURLEncodedEntity(paramList, paramString));
  }

  private static final String composeURLEncodedEntity(List<NameValuePair> paramList, String paramString)
  {
    if ((paramString != null) && (paramString.trim().length() > 0))
      return URLEncodedUtils.format(paramList, paramString);
    return URLEncodedUtils.format(paramList, "UTF-8");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.URLEncodedEntity
 * JD-Core Version:    0.6.2
 */