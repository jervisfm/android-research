package com.sgiggle.production.network.command;

import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;

public abstract interface Command
{
  public abstract HttpEntity getEntity();

  public abstract Header[] getHeaders();

  public abstract String getMethod();

  public abstract List<NameValuePair> getParameters();

  public abstract String getUrl();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.command.Command
 * JD-Core Version:    0.6.2
 */