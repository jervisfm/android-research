package com.sgiggle.production.network.command;

public class CommandFactory
{
  public static final int COMMAND_UPLOAD = 1;

  public Command getCommand(int paramInt)
  {
    switch (paramInt)
    {
    default:
      throw new UnsupportedOperationException("Unsupported command request for command: " + paramInt);
    case 1:
    }
    return new UploadCommand("/sdcard/output.3gp");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.command.CommandFactory
 * JD-Core Version:    0.6.2
 */