package com.sgiggle.production.network.command;

import com.sgiggle.production.network.CountingMultipartEntity;
import com.sgiggle.production.network.CountingMultipartEntity.ProgressListener;
import java.io.File;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

public class UploadCommand
  implements Command
{
  private File m_file;
  private CountingMultipartEntity.ProgressListener m_progressListener;
  private String m_url;

  public UploadCommand(String paramString)
  {
    if ((paramString == null) || (paramString.length() == 0))
      throw new IllegalArgumentException("Invalid filename");
    this.m_file = new File(paramString);
    if (!this.m_file.exists())
      throw new IllegalArgumentException("File does not exist");
  }

  public HttpEntity getEntity()
  {
    CountingMultipartEntity localCountingMultipartEntity = new CountingMultipartEntity(this.m_progressListener);
    try
    {
      localCountingMultipartEntity.addPart("type", new StringBody("videomessage"));
      localCountingMultipartEntity.addPart("file", new FileBody(this.m_file));
      return localCountingMultipartEntity;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  public Header[] getHeaders()
  {
    return null;
  }

  public String getMethod()
  {
    return "POST";
  }

  public List<NameValuePair> getParameters()
  {
    return null;
  }

  public String getUrl()
  {
    return this.m_url;
  }

  public void setProgressListener(CountingMultipartEntity.ProgressListener paramProgressListener)
  {
    this.m_progressListener = paramProgressListener;
  }

  public void setUrl(String paramString)
  {
    this.m_url = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.command.UploadCommand
 * JD-Core Version:    0.6.2
 */