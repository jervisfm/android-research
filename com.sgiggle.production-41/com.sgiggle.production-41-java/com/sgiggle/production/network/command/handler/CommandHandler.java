package com.sgiggle.production.network.command.handler;

import java.io.InputStream;

public abstract interface CommandHandler
{
  public abstract Object parseResponse(InputStream paramInputStream)
    throws Exception;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.command.handler.CommandHandler
 * JD-Core Version:    0.6.2
 */