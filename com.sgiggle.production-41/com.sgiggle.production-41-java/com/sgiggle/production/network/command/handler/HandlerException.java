package com.sgiggle.production.network.command.handler;

import java.io.IOException;

public class HandlerException extends IOException
{
  private static final long serialVersionUID = 5516990253039744177L;

  public HandlerException(String paramString)
  {
    super(paramString);
  }

  public HandlerException(String paramString, Throwable paramThrowable)
  {
    super(paramString);
    initCause(paramThrowable);
  }

  public String toString()
  {
    if (getCause() != null)
      return getLocalizedMessage() + ": " + getCause();
    return getLocalizedMessage();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.command.handler.HandlerException
 * JD-Core Version:    0.6.2
 */