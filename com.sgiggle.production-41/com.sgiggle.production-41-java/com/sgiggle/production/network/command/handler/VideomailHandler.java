package com.sgiggle.production.network.command.handler;

import com.sgiggle.production.model.VideomailResponse;
import java.io.InputStream;

public class VideomailHandler
  implements CommandHandler
{
  public Object parseResponse(InputStream paramInputStream)
    throws Exception
  {
    VideomailResponse localVideomailResponse = new VideomailResponse();
    localVideomailResponse.build(paramInputStream);
    return localVideomailResponse;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.command.handler.VideomailHandler
 * JD-Core Version:    0.6.2
 */