package com.sgiggle.production.network.executor;

import com.sgiggle.production.network.command.handler.CommandHandler;
import com.sgiggle.production.network.command.handler.HandlerException;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;

public class RemoteExecutor
{
  private final HttpClient _httpClient;

  public RemoteExecutor(HttpClient paramHttpClient)
  {
    this._httpClient = paramHttpClient;
  }

  public Object execute(HttpUriRequest paramHttpUriRequest, CommandHandler paramCommandHandler)
    throws HandlerException
  {
    Object localObject2;
    try
    {
      localHttpResponse = this._httpClient.execute(paramHttpUriRequest);
      if (localHttpResponse.getStatusLine().getStatusCode() != 200)
        throw new HandlerException("Unexepected server response " + localHttpResponse.getStatusLine() + " for " + paramHttpUriRequest.getRequestLine());
    }
    catch (HandlerException localHandlerException)
    {
      HttpResponse localHttpResponse;
      throw localHandlerException;
      InputStream localInputStream = localHttpResponse.getEntity().getContent();
      if (paramCommandHandler != null);
      try
      {
        localObject2 = paramCommandHandler.parseResponse(localInputStream);
        if (localInputStream != null)
        {
          localInputStream.close();
          break label218;
          return null;
        }
      }
      catch (Exception localException)
      {
        throw new HandlerException("Malformed response for " + paramHttpUriRequest.getRequestLine());
      }
      finally
      {
        if (localInputStream != null)
          localInputStream.close();
      }
    }
    catch (IOException localIOException)
    {
      throw new HandlerException("Problem reading remote response for " + paramHttpUriRequest.getRequestLine(), localIOException);
    }
    label218: return localObject2;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.executor.RemoteExecutor
 * JD-Core Version:    0.6.2
 */