package com.sgiggle.production.network.tango;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import com.sgiggle.production.network.HttpConfiguration;
import java.util.Locale;
import org.apache.http.auth.Credentials;

public class TangoHttpConfiguration
  implements HttpConfiguration
{
  private static String buildUserAgent(Context paramContext)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    String str1 = Build.VERSION.RELEASE;
    if (str1.length() > 0)
    {
      localStringBuffer.append(str1);
      localStringBuffer.append("; ");
      Locale localLocale = Locale.getDefault();
      String str2 = localLocale.getLanguage();
      if (str2 == null)
        break label166;
      localStringBuffer.append(str2.toLowerCase());
      String str5 = localLocale.getCountry();
      if (str5 != null)
      {
        localStringBuffer.append("-");
        localStringBuffer.append(str5.toLowerCase());
      }
    }
    while (true)
    {
      String str3 = Build.MODEL;
      if (str3.length() > 0)
      {
        localStringBuffer.append("; ");
        localStringBuffer.append(str3);
      }
      String str4 = Build.DISPLAY;
      if (str4.length() > 0)
      {
        localStringBuffer.append("Build/");
        localStringBuffer.append(str4);
      }
      return String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17", new Object[] { localStringBuffer });
      localStringBuffer.append("1.0");
      break;
      label166: localStringBuffer.append("en");
    }
  }

  public Credentials getAuthCredentials()
  {
    return null;
  }

  public int getHttpBufferSize()
  {
    return 16384;
  }

  public int getHttpConnectionTimeout()
  {
    return 20000;
  }

  public int getHttpDefaultMaxPerRoute()
  {
    return 5;
  }

  public int getHttpMaxTotalConnections()
  {
    return 1;
  }

  public int getHttpReadTimeout()
  {
    return getHttpConnectionTimeout();
  }

  public int getHttpRetryCount()
  {
    return 3;
  }

  public int getHttpRetryIntervalSeconds()
  {
    return getHttpConnectionTimeout();
  }

  public String getUserAgent(Context paramContext)
  {
    return buildUserAgent(paramContext);
  }

  public boolean isGZIPEnabled()
  {
    return true;
  }

  public boolean isPrettyDebugEnabled()
  {
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.network.tango.TangoHttpConfiguration
 * JD-Core Version:    0.6.2
 */