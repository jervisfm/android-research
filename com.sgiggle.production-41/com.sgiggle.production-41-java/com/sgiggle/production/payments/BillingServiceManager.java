package com.sgiggle.production.payments;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.service.BillingService;
import com.sgiggle.production.service.BillingService.BillingServiceBinder;
import com.sgiggle.production.service.BillingService.RequestPurchase;
import com.sgiggle.production.service.BillingService.RestoreTransactions;
import com.sgiggle.util.Log;
import java.util.HashMap;

public class BillingServiceManager
  implements ServiceConnection
{
  private static final String TAG = "BillingServiceMgr";
  private static BillingServiceManager mgr = null;
  private boolean isBinded = false;
  private boolean isCheckBillingServiceSupport = false;
  private boolean isSupported = false;
  private Context mContext;
  private BillingService m_billingService;
  private BillingPurchaseObserver m_purchaseObserver = new BillingPurchaseObserver(null);
  public HashMap<String, Constants.PurchaseState> purchaseStateMap = new HashMap();

  private void bindService()
  {
    Log.i("BillingServiceMgr", "bindService");
    if (this.isCheckBillingServiceSupport)
      TangoApp.getInstance().getResponseHandler().registerObserver(this.m_purchaseObserver);
    if (this.m_billingService == null)
      this.isBinded = this.mContext.bindService(new Intent(this.mContext, BillingService.class), this, 1);
  }

  public static BillingServiceManager getInstance()
  {
    if (mgr == null)
      mgr = new BillingServiceManager();
    return mgr;
  }

  public void bindServiceAndSetContext(Context paramContext)
  {
    this.mContext = paramContext;
    bindService();
  }

  public void checkBillingSupport(Context paramContext)
  {
    this.mContext = paramContext;
    this.isCheckBillingServiceSupport = true;
    bindService();
  }

  public boolean isBillingSupported()
  {
    return this.isSupported;
  }

  public boolean isSupported()
  {
    return this.isSupported;
  }

  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    Log.i("BillingServiceMgr", "onServiceConnected");
    if (this.isBinded)
    {
      this.m_billingService = ((BillingService.BillingServiceBinder)paramIBinder).getService();
      this.m_billingService.checkBillingSupported();
    }
  }

  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    Log.i("BillingServiceMgr", "onServiceDisconnected");
    this.m_billingService = null;
  }

  public boolean requestPurchase(String paramString1, String paramString2)
  {
    if (this.m_billingService != null)
      return this.m_billingService.requestPurchase(paramString1, paramString2);
    return false;
  }

  public void restoreTransactions()
  {
    Log.i("BillingServiceMgr", "RestoreTransactions!");
    if ((this.m_billingService != null) && (!TangoApp.areStoreTransactionRestored()))
    {
      this.m_billingService.restoreTransactions();
      return;
    }
    Log.e("BillingServiceMgr", "No billing service!");
  }

  public void setSupported(boolean paramBoolean)
  {
    Log.i("BillingServiceMgr", "service isSupported:" + paramBoolean);
    this.isSupported = paramBoolean;
  }

  public void unbindService()
  {
    Log.i("BillingServiceMgr", "unbindService");
    if (this.isCheckBillingServiceSupport)
      TangoApp.getInstance().getResponseHandler().unregisterObserver(this.m_purchaseObserver);
    if (this.isBinded);
    try
    {
      this.mContext.unbindService(this);
      label44: this.isBinded = false;
      this.m_billingService = null;
      this.mContext = null;
      return;
    }
    catch (Exception localException)
    {
      break label44;
    }
  }

  private class BillingPurchaseObserver
    implements ResponseHandler.PurchaseObserver
  {
    private BillingPurchaseObserver()
    {
    }

    public void onBillingSupported(boolean paramBoolean)
    {
      Log.i("BillingServiceMgr", "onBillingSupported:" + paramBoolean);
      BillingServiceManager.access$102(BillingServiceManager.this, paramBoolean);
      if (BillingServiceManager.this.isSupported)
        BillingServiceManager.getInstance().restoreTransactions();
      if (BillingServiceManager.this.isCheckBillingServiceSupport)
      {
        BillingServiceManager.this.unbindService();
        BillingServiceManager.access$202(BillingServiceManager.this, false);
      }
    }

    public void onResponseCodeReceived(BillingService.RequestPurchase paramRequestPurchase, Constants.ResponseCode paramResponseCode)
    {
    }

    public void onResponseCodeReceived(BillingService.RestoreTransactions paramRestoreTransactions, Constants.ResponseCode paramResponseCode)
    {
      Log.i("BillingService", "Restore Transactions response received: " + paramResponseCode);
      if (paramResponseCode == Constants.ResponseCode.RESULT_OK)
      {
        TangoApp.setStoreTransactionRestored();
        Log.i("BillingService", "Completed restoring transactions");
        return;
      }
      Log.e("BillingService", "Restore Transactions Error: " + paramResponseCode);
    }

    public void postPurchaseStateChange(Constants.PurchaseState paramPurchaseState, String paramString1, String paramString2, long paramLong, String paramString3, String paramString4, String paramString5)
    {
      Log.i("BillingService", "postPurchaseStateChange productId:" + paramString1 + ",state:" + paramPurchaseState);
      switch (BillingServiceManager.1.$SwitchMap$com$sgiggle$production$payments$Constants$PurchaseState[paramPurchaseState.ordinal()])
      {
      case 1:
      case 2:
      case 3:
      default:
        return;
      case 4:
      }
      BillingServiceManager.getInstance().purchaseStateMap.put(paramString1, paramPurchaseState);
    }

    public void startBuyPageActivity(PendingIntent paramPendingIntent, Intent paramIntent)
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.payments.BillingServiceManager
 * JD-Core Version:    0.6.2
 */