package com.sgiggle.production.payments;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import java.util.Locale;

public final class PurchaseUtils
{
  public static Dialog getBillingNotSupportedDialog(Context paramContext)
  {
    return getBillingNotSupportedDialog(paramContext, null);
  }

  public static Dialog getBillingNotSupportedDialog(final Context paramContext, DialogInterface.OnClickListener paramOnClickListener)
  {
    Uri localUri = Uri.parse(replaceLanguageAndRegion("http://market.android.com/support/bin/answer.py?answer=1050566&amp;hl=%lang%&amp;dl=%region%"));
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
    localBuilder.setTitle(2131296417).setIcon(17301624).setMessage(2131296418).setCancelable(false).setPositiveButton(2131296419, paramOnClickListener).setNegativeButton(2131296421, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Intent localIntent = new Intent("android.intent.action.VIEW", this.val$helpUri);
        paramContext.startActivity(localIntent);
      }
    });
    return localBuilder.create();
  }

  public static Dialog getPurchaseFailedDialog(Context paramContext)
  {
    return getPurchaseFailedDialog(paramContext, null);
  }

  public static Dialog getPurchaseFailedDialog(Context paramContext, DialogInterface.OnClickListener paramOnClickListener)
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
    localBuilder.setTitle(2131296415).setIcon(17301624).setMessage(2131296416).setNeutralButton(2131296420, paramOnClickListener).setCancelable(false);
    return localBuilder.create();
  }

  private static String replaceLanguageAndRegion(String paramString)
  {
    if ((paramString.contains("%lang%")) || (paramString.contains("%region%")))
    {
      Locale localLocale = Locale.getDefault();
      return paramString.replace("%lang%", localLocale.getLanguage().toLowerCase()).replace("%region%", localLocale.getCountry().toLowerCase());
    }
    return paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.payments.PurchaseUtils
 * JD-Core Version:    0.6.2
 */