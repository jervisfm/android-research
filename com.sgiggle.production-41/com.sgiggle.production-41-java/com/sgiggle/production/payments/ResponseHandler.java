package com.sgiggle.production.payments;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.sgiggle.production.service.BillingService.RequestPurchase;
import com.sgiggle.production.service.BillingService.RestoreTransactions;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ResponseHandler
{
  private List<WeakReference<PurchaseObserver>> m_observers = new CopyOnWriteArrayList();

  public void buyPageIntentResponse(PendingIntent paramPendingIntent, Intent paramIntent)
  {
    if (this.m_observers != null)
    {
      Iterator localIterator = this.m_observers.iterator();
      while (localIterator.hasNext())
      {
        WeakReference localWeakReference = (WeakReference)localIterator.next();
        if ((localWeakReference != null) && (localWeakReference.get() != null))
          ((PurchaseObserver)localWeakReference.get()).startBuyPageActivity(paramPendingIntent, paramIntent);
        else if ((localWeakReference == null) || (localWeakReference.get() == null))
          this.m_observers.remove(localWeakReference);
      }
    }
  }

  public void checkBillingSupported(boolean paramBoolean)
  {
    if (this.m_observers != null)
    {
      Iterator localIterator = this.m_observers.iterator();
      while (localIterator.hasNext())
      {
        WeakReference localWeakReference = (WeakReference)localIterator.next();
        if ((localWeakReference != null) && (localWeakReference.get() != null))
          ((PurchaseObserver)localWeakReference.get()).onBillingSupported(paramBoolean);
        else if ((localWeakReference == null) || (localWeakReference.get() == null))
          this.m_observers.remove(localWeakReference);
      }
    }
  }

  public void purchaseResponse(Context paramContext, final Constants.PurchaseState paramPurchaseState, final String paramString1, final String paramString2, final long paramLong, String paramString3, final String paramString4, final String paramString5)
  {
    if (this.m_observers != null)
    {
      Iterator localIterator = this.m_observers.iterator();
      while (localIterator.hasNext())
      {
        WeakReference localWeakReference = (WeakReference)localIterator.next();
        if ((localWeakReference != null) && (localWeakReference.get() != null))
          new Thread(new Runnable()
          {
            public void run()
            {
              try
              {
                this.val$observer.postPurchaseStateChange(paramPurchaseState, paramString1, paramString2, paramLong, paramString4, paramString5, this.val$signature);
                return;
              }
              finally
              {
              }
            }
          }).start();
        else if ((localWeakReference == null) || (localWeakReference.get() == null))
          this.m_observers.remove(localWeakReference);
      }
    }
  }

  public void registerObserver(PurchaseObserver paramPurchaseObserver)
  {
    if (paramPurchaseObserver != null)
      this.m_observers.add(new WeakReference(paramPurchaseObserver));
  }

  public void responseCodeReceived(BillingService.RequestPurchase paramRequestPurchase, Constants.ResponseCode paramResponseCode)
  {
    if (this.m_observers != null)
    {
      Iterator localIterator = this.m_observers.iterator();
      while (localIterator.hasNext())
      {
        WeakReference localWeakReference = (WeakReference)localIterator.next();
        if ((localWeakReference != null) && (localWeakReference.get() != null))
          ((PurchaseObserver)localWeakReference.get()).onResponseCodeReceived(paramRequestPurchase, paramResponseCode);
        else if ((localWeakReference == null) || (localWeakReference.get() == null))
          this.m_observers.remove(localWeakReference);
      }
    }
  }

  public void responseCodeReceived(BillingService.RestoreTransactions paramRestoreTransactions, Constants.ResponseCode paramResponseCode)
  {
    if (this.m_observers != null)
    {
      Iterator localIterator = this.m_observers.iterator();
      while (localIterator.hasNext())
      {
        WeakReference localWeakReference = (WeakReference)localIterator.next();
        if ((localWeakReference != null) && (localWeakReference.get() != null))
          ((PurchaseObserver)localWeakReference.get()).onResponseCodeReceived(paramRestoreTransactions, paramResponseCode);
        else if ((localWeakReference == null) || (localWeakReference.get() == null))
          this.m_observers.remove(localWeakReference);
      }
    }
  }

  public boolean unregisterObserver(PurchaseObserver paramPurchaseObserver)
  {
    if (paramPurchaseObserver != null)
    {
      Iterator localIterator = this.m_observers.iterator();
      while (localIterator.hasNext())
      {
        WeakReference localWeakReference = (WeakReference)localIterator.next();
        if ((localWeakReference != null) && (localWeakReference.get() != null) && (localWeakReference.get() == paramPurchaseObserver))
          return this.m_observers.remove(localWeakReference);
        if ((localWeakReference == null) || (localWeakReference.get() == null))
          this.m_observers.remove(localWeakReference);
      }
    }
    return false;
  }

  public static abstract interface PurchaseObserver
  {
    public abstract void onBillingSupported(boolean paramBoolean);

    public abstract void onResponseCodeReceived(BillingService.RequestPurchase paramRequestPurchase, Constants.ResponseCode paramResponseCode);

    public abstract void onResponseCodeReceived(BillingService.RestoreTransactions paramRestoreTransactions, Constants.ResponseCode paramResponseCode);

    public abstract void postPurchaseStateChange(Constants.PurchaseState paramPurchaseState, String paramString1, String paramString2, long paramLong, String paramString3, String paramString4, String paramString5);

    public abstract void startBuyPageActivity(PendingIntent paramPendingIntent, Intent paramIntent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.payments.ResponseHandler
 * JD-Core Version:    0.6.2
 */