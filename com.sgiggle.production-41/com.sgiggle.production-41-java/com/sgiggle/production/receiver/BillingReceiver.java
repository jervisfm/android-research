package com.sgiggle.production.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.sgiggle.production.payments.Constants.ResponseCode;
import com.sgiggle.production.service.BillingService;
import com.sgiggle.util.Log;

public class BillingReceiver extends BroadcastReceiver
{
  private static final String TAG = "BillingReceiver";

  private void checkResponseCode(Context paramContext, long paramLong, int paramInt)
  {
    Intent localIntent = new Intent("com.android.vending.billing.RESPONSE_CODE");
    localIntent.setClass(paramContext, BillingService.class);
    localIntent.putExtra("request_id", paramLong);
    localIntent.putExtra("response_code", paramInt);
    paramContext.startService(localIntent);
  }

  private void notify(Context paramContext, String paramString)
  {
    Intent localIntent = new Intent("com.sgiggle.production.tango.GET_PURCHASE_INFORMATION");
    localIntent.setClass(paramContext, BillingService.class);
    localIntent.putExtra("notification_id", paramString);
    paramContext.startService(localIntent);
  }

  private void purchaseStateChanged(Context paramContext, String paramString1, String paramString2)
  {
    Intent localIntent = new Intent("com.android.vending.billing.PURCHASE_STATE_CHANGED");
    localIntent.setClass(paramContext, BillingService.class);
    localIntent.putExtra("inapp_signed_data", paramString1);
    localIntent.putExtra("inapp_signature", paramString2);
    paramContext.startService(localIntent);
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    String str1 = paramIntent.getAction();
    if ("com.android.vending.billing.PURCHASE_STATE_CHANGED".equals(str1))
    {
      purchaseStateChanged(paramContext, paramIntent.getStringExtra("inapp_signed_data"), paramIntent.getStringExtra("inapp_signature"));
      return;
    }
    if ("com.android.vending.billing.IN_APP_NOTIFY".equals(str1))
    {
      String str2 = paramIntent.getStringExtra("notification_id");
      Log.i("BillingReceiver", "Notify ID: " + str2);
      notify(paramContext, str2);
      return;
    }
    if ("com.android.vending.billing.RESPONSE_CODE".equals(str1))
    {
      checkResponseCode(paramContext, paramIntent.getLongExtra("request_id", -1L), paramIntent.getIntExtra("response_code", Constants.ResponseCode.RESULT_ERROR.ordinal()));
      return;
    }
    Log.w("BillingReceiver", "Unexpected action: " + str1);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.receiver.BillingReceiver
 * JD-Core Version:    0.6.2
 */