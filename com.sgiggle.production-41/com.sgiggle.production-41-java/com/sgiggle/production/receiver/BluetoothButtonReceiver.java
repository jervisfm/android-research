package com.sgiggle.production.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BluetoothButtonReceiver extends BroadcastReceiver
{
  private ButtonHandler m_handler;

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    int i;
    if (paramIntent.getAction().equals("android.intent.action.VoIP_BLUETOOTH"))
    {
      i = paramIntent.getIntExtra("Event", -1);
      if (i != 1)
        break label42;
      if (this.m_handler != null)
        this.m_handler.acceptCall();
    }
    label42: 
    while ((i != 3) || (this.m_handler == null))
      return;
    this.m_handler.rejectCall();
  }

  public void setButtonHandler(ButtonHandler paramButtonHandler)
  {
    this.m_handler = paramButtonHandler;
  }

  public static abstract interface ButtonHandler
  {
    public abstract void acceptCall();

    public abstract void rejectCall();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.receiver.BluetoothButtonReceiver
 * JD-Core Version:    0.6.2
 */