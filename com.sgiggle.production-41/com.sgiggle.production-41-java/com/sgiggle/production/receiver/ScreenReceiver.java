package com.sgiggle.production.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ScreenReceiver extends BroadcastReceiver
{
  public static boolean wasScreenOn = true;

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent.getAction().equals("android.intent.action.SCREEN_OFF"))
      wasScreenOn = false;
    while (!paramIntent.getAction().equals("android.intent.action.SCREEN_ON"))
      return;
    wasScreenOn = true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.receiver.ScreenReceiver
 * JD-Core Version:    0.6.2
 */