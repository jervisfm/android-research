package com.sgiggle.production.receiver;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.TangoApp.AppState;
import com.sgiggle.production.WrongTangoRuntimeVersionException;
import com.sgiggle.util.Log;

public class SystemDaemon extends BroadcastReceiver
{
  private static final String TAG = "Tango.SystemDaemon";

  private void performAccountSync(Context paramContext)
  {
    TangoApp.getInstance().sendLoginRequestIfNeeded();
    Log.d("Tango.SystemDaemon", "performAccountSync(): Request sync ...");
    String str = paramContext.getString(2131296391);
    Account[] arrayOfAccount = AccountManager.get(paramContext).getAccountsByType(str);
    if (arrayOfAccount.length > 0)
      ContentResolver.requestSync(arrayOfAccount[0], "com.android.contacts", new Bundle());
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    try
    {
      TangoApp.ensureInitialized();
      Log.d("Tango.SystemDaemon", "onReceive(): intent" + paramIntent);
      str = paramIntent.getAction();
      if ("android.intent.action.BOOT_COMPLETED".equals(str))
      {
        TangoApp.getInstance().setAppRunningState(TangoApp.AppState.APP_STATE_BACKGROUND);
        return;
      }
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      String str;
      do
        while (true)
          Log.e("Tango.SystemDaemon", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
      while (!"com.android.contacts.im.VoIP.SYNC".equals(str));
      performAccountSync(paramContext);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.receiver.SystemDaemon
 * JD-Core Version:    0.6.2
 */