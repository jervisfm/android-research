package com.sgiggle.production.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.WrongTangoRuntimeVersionException;
import com.sgiggle.util.Log;

public class TimerReceiver extends BroadcastReceiver
{
  public static final String ACTION_STOP_SMS_LISTENER = "tango.service.STOP.SMS.LISTENER";
  private static final String TAG = "TimerReceiver";

  private void stopSMSListener()
  {
    Log.v("TimerReceiver", "Stop SMS Listener due to timer");
    TangoApp.getInstance().unregisterSMSReceiver();
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    try
    {
      TangoApp.ensureInitialized();
      Log.d("TimerReceiver", "onStartCommand(): intent = " + paramIntent);
      if ((paramIntent != null) && ("tango.service.STOP.SMS.LISTENER".equals(paramIntent.getAction())))
        stopSMSListener();
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      while (true)
        Log.e("TimerReceiver", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.receiver.TimerReceiver
 * JD-Core Version:    0.6.2
 */