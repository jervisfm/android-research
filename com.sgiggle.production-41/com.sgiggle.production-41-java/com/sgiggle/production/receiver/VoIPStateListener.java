package com.sgiggle.production.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class VoIPStateListener extends BroadcastReceiver
{
  private Listener m_listener;

  public VoIPStateListener(Listener paramListener)
  {
    this.m_listener = paramListener;
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (("android.intent.action.VoIP_RESUME_CALL".equals(paramIntent.getAction())) && (2 == paramIntent.getIntExtra("ResumeType", -1)))
      this.m_listener.handleCSCallResume();
  }

  public static abstract interface Listener
  {
    public abstract void handleCSCallResume();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.receiver.VoIPStateListener
 * JD-Core Version:    0.6.2
 */