package com.sgiggle.production.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Build;
import android.os.Build.VERSION;
import com.sgiggle.util.Log;
import java.lang.reflect.Field;

public class WifiLockReceiver extends BroadcastReceiver
{
  private static final String TAG = "Tango.WifiLockReceiver";
  private WifiManager.WifiLock m_wifiLock;

  public WifiLockReceiver(Context paramContext)
  {
    WifiManager localWifiManager = (WifiManager)paramContext.getSystemService("wifi");
    if (Build.VERSION.SDK_INT >= 9)
      try
      {
        Field localField = Class.forName("android.net.wifi.WifiManager").getField("WIFI_MODE_FULL_HIGH_PERF");
        this.m_wifiLock = localWifiManager.createWifiLock(localField.getInt(localField), "Tango.WifiLockReceiver");
        return;
      }
      catch (Exception localException)
      {
        Log.e("Tango.WifiLockReceiver", "Error acquiring wifi lock using full high performance mode. Falling back to regular mode...");
        this.m_wifiLock = localWifiManager.createWifiLock(1, "Tango.WifiLockReceiver");
        return;
      }
    this.m_wifiLock = localWifiManager.createWifiLock(1, "Tango.WifiLockReceiver");
  }

  public void acquireWifiLock(Context paramContext)
  {
    if (!this.m_wifiLock.isHeld())
    {
      this.m_wifiLock.acquire();
      Log.i("Tango.WifiLockReceiver", "Acquired wifi lock");
    }
    if (Build.MANUFACTURER.compareToIgnoreCase("HTC") == 0)
    {
      Log.d("Tango.WifiLockReceiver", "Sending broadcast for htc wifi - change wifi mode to true");
      Intent localIntent = new Intent();
      localIntent.setAction("com.htc.intent.action.CHANGE_WIFI_MODE");
      localIntent.putExtra("comeIn", true);
      paramContext.sendBroadcast(localIntent);
    }
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if ("android.intent.action.SCREEN_OFF".equals(paramIntent.getAction()))
      acquireWifiLock(paramContext);
    while (!"android.intent.action.SCREEN_ON".equals(paramIntent.getAction()))
      return;
    releaseWifiLock(paramContext);
  }

  public void releaseWifiLock(Context paramContext)
  {
    if (this.m_wifiLock.isHeld())
    {
      this.m_wifiLock.release();
      Log.i("Tango.WifiLockReceiver", "Released wifi lock");
    }
    if (Build.MANUFACTURER.compareToIgnoreCase("HTC") == 0)
    {
      Log.d("Tango.WifiLockReceiver", "Sending broadcast for htc wifi - change wifi mode to false");
      Intent localIntent = new Intent();
      localIntent.setAction("com.htc.intent.action.CHANGE_WIFI_MODE");
      localIntent.putExtra("comeIn", false);
      paramContext.sendBroadcast(localIntent);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.receiver.WifiLockReceiver
 * JD-Core Version:    0.6.2
 */