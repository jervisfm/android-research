package com.sgiggle.production.screens.picture;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import com.sgiggle.util.Log;

public class ImageViewControl extends ImageView
  implements View.OnTouchListener
{
  private static final int DURATION_OF_DOUBLE_TAP_IN_MS = 500;
  private static final int DURATION_OF_TRANSFORMING_ANIMATION_IN_MS = 300;
  private static final int MAX_SCALE = 4;
  private static final String TAG = "Tango.ImageViewControl";
  static final int TOUCH_MODE_DRAG = 1;
  static final int TOUCH_MODE_NONE = 0;
  static final int TOUCH_MODE_ZOOM = 2;
  private static final float ZOOM_IN_RATIO = 3.0F;
  Handler m_bubbleTapEventHandler = new Handler();
  Matrix m_curMatrix = new Matrix();
  float m_distBeforeZooming = 0.0F;
  MotionEvent m_firstTapEvent = null;
  private boolean m_isUriChanged = false;
  private RectF m_mappedImageBound = new RectF();
  Matrix m_matrixBeforeZoomingPanning = new Matrix();
  Point m_midPointBeforeZooming = new Point();
  private float m_minScale;
  private RectF m_oriImageBound;
  Point m_pointBeforePanning = new Point();
  private float m_scaleFittingScreen = 0.0F;
  Runnable m_tapEventBubbler = new Runnable()
  {
    public void run()
    {
      ImageViewControl.this.bubbleTapEvent();
    }
  };
  int m_touchMode = 0;
  private Uri m_uri;

  public ImageViewControl(Context paramContext)
  {
    super(paramContext);
    setOnTouchListener(this);
  }

  public ImageViewControl(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setOnTouchListener(this);
  }

  public ImageViewControl(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setOnTouchListener(this);
  }

  private void adjustMatrix(Matrix paramMatrix)
  {
    paramMatrix.mapRect(this.m_mappedImageBound, this.m_oriImageBound);
    float f1 = this.m_mappedImageBound.width() / this.m_oriImageBound.width();
    float f2;
    if (f1 < this.m_minScale)
      f2 = this.m_minScale / f1;
    while (true)
    {
      if (f2 != 1.0F)
      {
        paramMatrix.postScale(f2, f2);
        paramMatrix.mapRect(this.m_mappedImageBound, this.m_oriImageBound);
      }
      int i = getWidth();
      int j = getHeight();
      float f3;
      if (this.m_mappedImageBound.width() >= i)
        if (this.m_mappedImageBound.left > 0.0F)
          f3 = -this.m_mappedImageBound.left;
      while (true)
      {
        label123: float f4;
        if (this.m_mappedImageBound.height() >= j)
          if (this.m_mappedImageBound.top > 0.0F)
            f4 = -this.m_mappedImageBound.top;
        while (true)
        {
          paramMatrix.postTranslate(f3, f4);
          return;
          if (f1 <= 4.0F * this.m_scaleFittingScreen)
            break label303;
          f2 = 4.0F * this.m_scaleFittingScreen / f1;
          break;
          if (this.m_mappedImageBound.right >= i)
            break label297;
          f3 = i - this.m_mappedImageBound.right;
          break label123;
          f3 = i / 2 - this.m_mappedImageBound.centerX();
          break label123;
          if (this.m_mappedImageBound.bottom < j)
          {
            f4 = j - this.m_mappedImageBound.bottom;
            continue;
            f4 = j / 2 - this.m_mappedImageBound.centerY();
          }
          else
          {
            f4 = 0.0F;
          }
        }
        label297: f3 = 0.0F;
      }
      label303: f2 = 1.0F;
    }
  }

  private void beginDetectingDoubleTapEvent(MotionEvent paramMotionEvent)
  {
    this.m_bubbleTapEventHandler.postDelayed(this.m_tapEventBubbler, 500L);
    this.m_firstTapEvent = paramMotionEvent;
  }

  private void bubbleTapEvent()
  {
    ((Activity)getContext()).onTouchEvent(this.m_firstTapEvent);
    this.m_firstTapEvent = null;
  }

  private void cancelDetectingDoubleTapEvent()
  {
    this.m_bubbleTapEventHandler.removeCallbacks(this.m_tapEventBubbler);
    this.m_firstTapEvent = null;
  }

  private float getCurScale()
  {
    return getMatrixValues(this.m_curMatrix)[0];
  }

  private float getDistBetweenTwoFingers(MotionEvent paramMotionEvent)
  {
    float f1 = paramMotionEvent.getX(0) - paramMotionEvent.getX(1);
    float f2 = paramMotionEvent.getY(0) - paramMotionEvent.getY(1);
    return FloatMath.sqrt(f1 * f1 + f2 * f2);
  }

  private static float[] getMatrixValues(Matrix paramMatrix)
  {
    float[] arrayOfFloat = new float[9];
    paramMatrix.getValues(arrayOfFloat);
    return arrayOfFloat;
  }

  private void getMidPointOfTwoFingers(Point paramPoint, MotionEvent paramMotionEvent)
  {
    float f1 = paramMotionEvent.getX(0) + paramMotionEvent.getX(1);
    float f2 = paramMotionEvent.getY(0) + paramMotionEvent.getY(1);
    paramPoint.set((int)f1 / 2, (int)f2 / 2);
  }

  private void handleTapEvent(MotionEvent paramMotionEvent)
  {
    if (this.m_firstTapEvent != null)
    {
      onDoubleTapEvent(paramMotionEvent);
      cancelDetectingDoubleTapEvent();
      return;
    }
    beginDetectingDoubleTapEvent(paramMotionEvent);
  }

  private void initForZoomingAndPanning(int paramInt1, int paramInt2)
  {
    if ((paramInt1 == 0) || (paramInt2 == 0));
    while (this.m_uri == null)
      return;
    Drawable localDrawable = getDrawable();
    if (localDrawable == null)
    {
      Log.d("Tango.ImageViewControl", "image was not loaded successfully");
      return;
    }
    int i = localDrawable.getIntrinsicWidth();
    int j = localDrawable.getIntrinsicHeight();
    this.m_oriImageBound = new RectF(0.0F, 0.0F, i - 1, j - 1);
    if (i * paramInt2 > j * paramInt1);
    for (this.m_scaleFittingScreen = (paramInt1 / i); ; this.m_scaleFittingScreen = (paramInt2 / j))
    {
      this.m_minScale = this.m_scaleFittingScreen;
      if (this.m_minScale > 1.0F)
        this.m_minScale = 1.0F;
      if (!this.m_isUriChanged)
        break;
      this.m_isUriChanged = false;
      scaleImageAndAdjust(this.m_scaleFittingScreen);
      return;
    }
    adjustMatrix(this.m_curMatrix);
    setMatrix(this.m_curMatrix);
  }

  private void onDoubleTapEvent(MotionEvent paramMotionEvent)
  {
    float f1 = getCurScale();
    if (Math.abs(f1 - this.m_scaleFittingScreen) / f1 < 0.01D);
    for (float f2 = 3.0F * this.m_scaleFittingScreen; ; f2 = this.m_scaleFittingScreen)
    {
      Matrix localMatrix = new Matrix();
      localMatrix.set(this.m_curMatrix);
      float f3 = f2 / f1;
      localMatrix.postScale(f3, f3, paramMotionEvent.getX(), paramMotionEvent.getY());
      adjustMatrix(localMatrix);
      transformImageAnimated(this.m_curMatrix, localMatrix);
      return;
    }
  }

  private void scaleImageAndAdjust(float paramFloat)
  {
    Matrix localMatrix = new Matrix();
    localMatrix.postScale(paramFloat, paramFloat);
    adjustMatrix(localMatrix);
    setMatrix(localMatrix);
  }

  private void setMatrix(Matrix paramMatrix)
  {
    this.m_curMatrix = paramMatrix;
    setImageMatrix(paramMatrix);
  }

  private void transformImageAnimated(Matrix paramMatrix1, Matrix paramMatrix2)
  {
    float[] arrayOfFloat1 = getMatrixValues(paramMatrix1);
    float[] arrayOfFloat2 = getMatrixValues(paramMatrix2);
    AccelerateDecelerateInterpolator localAccelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
    final long l = System.currentTimeMillis();
    final Matrix localMatrix = new Matrix();
    final float f1 = arrayOfFloat1[0];
    final float f2 = arrayOfFloat1[4];
    final float f3 = arrayOfFloat2[0];
    final float f4 = arrayOfFloat2[4];
    final float f5 = arrayOfFloat1[2];
    final float f6 = arrayOfFloat1[5];
    post(new Runnable()
    {
      public void run()
      {
        float f1 = (float)(System.currentTimeMillis() - l) / 300.0F;
        if (f1 > 1.0F)
          f1 = 1.0F;
        float f2 = f1.getInterpolation(f1);
        float f3 = f3 + f2 * (f2 - f3);
        float f4 = f4 + f2 * (f5 - f4);
        float f5 = this.val$fromTransX + f2 * (f6 - this.val$fromTransX);
        float f6 = this.val$fromTransY + f2 * (localMatrix - this.val$fromTransY);
        jdField_this.reset();
        jdField_this.postScale(f3, f4);
        jdField_this.postTranslate(f5, f6);
        ImageViewControl.this.setMatrix(jdField_this);
        if (f1 < 1.0F)
          this.val$thisImageView.post(this);
      }
    });
  }

  public void loadImageToMemory()
  {
    Uri localUri = this.m_uri;
    Object localObject1 = null;
    if (localUri != null);
    try
    {
      Bitmap localBitmap = BitmapFactory.decodeFile(this.m_uri.getPath());
      localObject1 = localBitmap;
      if (localObject1 != null)
      {
        setImageBitmap(localObject1);
        Log.d("Tango.ImageViewControl", "loadImageToMemory() success " + this.m_uri);
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder().append("loadImageToMemory() bitmap is empty for file ");
      if (this.m_uri == null);
      for (Object localObject2 = " unkown uri"; ; localObject2 = this.m_uri)
      {
        Log.w("Tango.ImageViewControl", localObject2);
        return;
      }
    }
    catch (Exception localException)
    {
      while (true)
        localObject1 = null;
    }
  }

  public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    initForZoomingAndPanning(paramInt1, paramInt2);
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (this.m_uri == null)
      return false;
    switch (0xFF & paramMotionEvent.getAction())
    {
    case 3:
    case 4:
    default:
    case 0:
    case 5:
    case 1:
    case 6:
    case 2:
    }
    while (true)
    {
      setMatrix(this.m_curMatrix);
      return true;
      this.m_matrixBeforeZoomingPanning.set(this.m_curMatrix);
      this.m_pointBeforePanning.set((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
      this.m_touchMode = 1;
      continue;
      this.m_distBeforeZooming = getDistBetweenTwoFingers(paramMotionEvent);
      if (this.m_distBeforeZooming > 10.0F)
      {
        this.m_matrixBeforeZoomingPanning.set(this.m_curMatrix);
        getMidPointOfTwoFingers(this.m_midPointBeforeZooming, paramMotionEvent);
        this.m_touchMode = 2;
        continue;
        this.m_touchMode = 0;
        if ((getContext() instanceof Activity))
        {
          DisplayMetrics localDisplayMetrics = new DisplayMetrics();
          ((Activity)getContext()).getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
          int i = localDisplayMetrics.densityDpi;
          float f3 = StrictMath.abs(paramMotionEvent.getX() - this.m_pointBeforePanning.x) / i;
          float f4 = StrictMath.abs(paramMotionEvent.getY() - this.m_pointBeforePanning.y) / i;
          if ((f3 < 0.1D) && (f4 < 0.1D))
          {
            handleTapEvent(paramMotionEvent);
            return true;
            this.m_touchMode = 0;
            continue;
            if (this.m_touchMode == 1)
            {
              this.m_curMatrix.set(this.m_matrixBeforeZoomingPanning);
              this.m_curMatrix.postTranslate(paramMotionEvent.getX() - this.m_pointBeforePanning.x, paramMotionEvent.getY() - this.m_pointBeforePanning.y);
              adjustMatrix(this.m_curMatrix);
            }
            else if (this.m_touchMode == 2)
            {
              float f1 = getDistBetweenTwoFingers(paramMotionEvent);
              if (f1 > 10.0F)
              {
                this.m_curMatrix.set(this.m_matrixBeforeZoomingPanning);
                float f2 = f1 / this.m_distBeforeZooming;
                this.m_curMatrix.postScale(f2, f2, this.m_midPointBeforeZooming.x, this.m_midPointBeforeZooming.y);
                adjustMatrix(this.m_curMatrix);
              }
            }
          }
        }
      }
    }
  }

  public void setImageURI(Uri paramUri)
  {
    if ((this.m_uri == null) && (paramUri == null));
    do
    {
      return;
      if ((this.m_uri != null) && (paramUri != null))
        break;
      this.m_isUriChanged = true;
      unloadImageFromMemory();
      this.m_uri = paramUri;
      loadImageToMemory();
    }
    while (!this.m_isUriChanged);
    initForZoomingAndPanning(getWidth(), getHeight());
    return;
    if (!this.m_uri.equals(paramUri));
    for (boolean bool = true; ; bool = false)
    {
      this.m_isUriChanged = bool;
      break;
    }
  }

  public void unloadImageFromMemory()
  {
    Drawable localDrawable = getDrawable();
    if ((localDrawable != null) && ((localDrawable instanceof BitmapDrawable)))
    {
      Bitmap localBitmap = ((BitmapDrawable)localDrawable).getBitmap();
      if (localBitmap != null)
        localBitmap.recycle();
      setImageBitmap(null);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.picture.ImageViewControl
 * JD-Core Version:    0.6.2
 */