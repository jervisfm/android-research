package com.sgiggle.production.screens.picture;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Toast;
import com.sgiggle.production.ActivityBase;
import java.io.File;

public class PicturePreviewActivity extends ActivityBase
{
  public static final String PATH = "path";
  public static final String RESULT_TITLE = "result_title";
  public static final String RESULT_URI = "result_uri";
  private String m_photoPath;

  private boolean doesFileExist(String paramString)
  {
    if (paramString == null)
      return false;
    return new File(paramString).exists();
  }

  public void onCancel(View paramView)
  {
    setResult(0);
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903098);
    String str = getIntent().getStringExtra("path");
    if ((str == null) || (!doesFileExist(Uri.parse(str).getPath())))
    {
      setResult(0);
      finish();
      return;
    }
    this.m_photoPath = str;
  }

  public void onFilter(View paramView)
  {
    Toast.makeText(this, "Filtering...", 0).show();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      onCancel(null);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onSend(View paramView)
  {
    if (this.m_photoPath == null)
      return;
    Intent localIntent = new Intent();
    localIntent.putExtra("result_uri", this.m_photoPath.toString());
    localIntent.putExtra("result_title", "");
    if (doesFileExist(Uri.parse(this.m_photoPath).getPath()));
    for (int i = -1; ; i = 0)
    {
      setResult(i, localIntent);
      finish();
      return;
    }
  }

  public void onStart()
  {
    super.onStart();
    ImageViewControl localImageViewControl = (ImageViewControl)findViewById(2131362017);
    localImageViewControl.setImageURI(Uri.parse(this.m_photoPath));
    View localView = findViewById(2131362018);
    localImageViewControl.getLayoutParams().height = (getWindowManager().getDefaultDisplay().getHeight() - localView.getLayoutParams().height);
  }

  public void onStop()
  {
    super.onStop();
    ((ImageViewControl)findViewById(2131362017)).unloadImageFromMemory();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.picture.PicturePreviewActivity
 * JD-Core Version:    0.6.2
 */