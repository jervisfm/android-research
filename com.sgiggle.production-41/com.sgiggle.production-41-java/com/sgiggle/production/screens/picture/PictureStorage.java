package com.sgiggle.production.screens.picture;

import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Environment;
import com.sgiggle.localstorage.LocalStorage;
import com.sgiggle.util.Log;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PictureStorage
{
  private static final String DEFAULT_PIC_DIR = "DCIM";
  private static final String TAG = "PictureStorage";
  private static final String TANGO_DIR = "Tango";
  private static int s_imgCount = 0;

  public static File getPrivatePicDirectory(Context paramContext)
  {
    return new File(LocalStorage.getCacheDir(paramContext));
  }

  public static File getSharedGalleryDirectory()
  {
    if (Build.VERSION.SDK_INT >= 8)
      return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Tango");
    File localFile = Environment.getExternalStorageDirectory();
    return new File(localFile.getAbsolutePath() + File.separator + "DCIM" + File.separator + "Tango");
  }

  public static File getTmpOutputPictureFile(Context paramContext, Boolean paramBoolean)
    throws Exception
  {
    return getTmpOutputPictureFile(paramContext, "", paramBoolean);
  }

  public static File getTmpOutputPictureFile(Context paramContext, String paramString, Boolean paramBoolean)
    throws Exception
  {
    if (paramBoolean.booleanValue());
    for (File localFile1 = getSharedGalleryDirectory(); ; localFile1 = getPrivatePicDirectory(paramContext))
    {
      Log.d("PictureStorage", localFile1.toString());
      if ((localFile1.exists()) || (localFile1.mkdirs()))
        break;
      Log.d("PictureStorage", "failed to create directory");
      throw new Exception("failed to create directory");
    }
    if (paramBoolean.booleanValue());
    for (String str1 = "IMG_"; ; str1 = "picturetmp_")
    {
      String str2 = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
      File localFile2 = new File(localFile1.getPath() + File.separator + str1 + str2 + "_" + s_imgCount + "_" + paramString + ".jpg");
      s_imgCount = 1 + s_imgCount;
      return localFile2;
    }
  }

  public static String getTmpPicPath(Context paramContext, Boolean paramBoolean)
    throws Exception
  {
    return getTmpPicPath(paramContext, "", paramBoolean);
  }

  public static String getTmpPicPath(Context paramContext, String paramString, Boolean paramBoolean)
    throws Exception
  {
    return getTmpPicUri(paramContext, paramString, paramBoolean).getPath();
  }

  public static Uri getTmpPicUri(Context paramContext, Boolean paramBoolean)
    throws Exception
  {
    return getTmpPicUri(paramContext, "", paramBoolean);
  }

  public static Uri getTmpPicUri(Context paramContext, String paramString, Boolean paramBoolean)
    throws Exception
  {
    return Uri.fromFile(getTmpOutputPictureFile(paramContext, paramString, paramBoolean));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.picture.PictureStorage
 * JD-Core Version:    0.6.2
 */