package com.sgiggle.production.screens.picture;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.CancelViewPictureMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ConversationMessageSendStatusEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayConversationMessageEvent;
import com.sgiggle.media_engine.MediaEngineMessage.FinishSMSComposeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ForwardMessageRequestMessage;
import com.sgiggle.media_engine.MediaEngineMessage.StartSMSComposeEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ViewPictureEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.ActivityBase;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.Utils;
import com.sgiggle.production.util.FileOperator;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage.Builder;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageLoadingStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessagePayload;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageSendStatus;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.SMSComposerPayload;
import java.io.File;

public class PictureViewerActivity extends ActivityBase
{
  private static final int DURATION_OF_BAR_BEING_VISIBLE = 5000;
  private static final int MENU_ITEM_ID_FORWARD = 0;
  private static final int MENU_ITEM_ID_SAVE_TO_PHONE = 1;
  private static final int REQUEST_CODE_SMS_OLD_CLIENT = 1;
  private static final String TAG = "Tango.PictureViewerActivity";
  private static PictureViewerActivity s_instance;
  Animation m_FadeOutAnimation;
  private View m_actionBar;
  Runnable m_barHider = new Runnable()
  {
    public void run()
    {
      PictureViewerActivity.this.hideBar(true);
    }
  };
  Button m_btnDoMoreOperations;
  private String m_conversationId;
  private String m_imagePath;
  ImageViewControl m_imageView;
  private boolean m_isBarVisible = true;
  boolean m_isForwardEnabled = false;
  private int m_messageId;
  ProgressBar m_progressBar;
  Handler m_timeoutHandler = new Handler();

  static
  {
    if (!PictureViewerActivity.class.desiredAssertionStatus());
    for (boolean bool = true; ; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }

  private void back()
  {
    MediaEngineMessage.CancelViewPictureMessage localCancelViewPictureMessage = new MediaEngineMessage.CancelViewPictureMessage();
    MessageRouter.getInstance().postMessage("jingle", localCancelViewPictureMessage);
  }

  private void cancelTimerTriggerHidingBar()
  {
    this.m_timeoutHandler.removeCallbacks(this.m_barHider);
  }

  private void changeImage(String paramString)
  {
    if (this.m_imagePath == paramString)
      return;
    this.m_imagePath = paramString;
    if ((this.m_imagePath != null) && (new File(this.m_imagePath).exists()))
    {
      this.m_imageView.setImageURI(Uri.parse(this.m_imagePath));
      startTimerTriggerHidingBar();
      return;
    }
    this.m_imageView.setImageURI(null);
  }

  private static void clearRunningInstance(PictureViewerActivity paramPictureViewerActivity)
  {
    if (s_instance == paramPictureViewerActivity)
      s_instance = null;
  }

  private boolean copyFile()
    throws Exception
  {
    String str = PictureStorage.getTmpPicPath(this, Boolean.valueOf(true));
    if (FileOperator.copyFile(this.m_imagePath, str))
    {
      new SingleMediaScanner(this, str);
      return true;
    }
    return false;
  }

  private String getMessageMediaPath(SessionMessages.ConversationMessage paramConversationMessage)
  {
    if (TextUtils.isEmpty(paramConversationMessage.getPath()))
      return null;
    return paramConversationMessage.getPath();
  }

  public static PictureViewerActivity getRunningInstance()
  {
    return s_instance;
  }

  private void handleDownloadingError()
  {
    Toast.makeText(this, 2131296669, 1).show();
    back();
  }

  private void handleStartSMSComposeEvent(MediaEngineMessage.StartSMSComposeEvent paramStartSMSComposeEvent)
  {
    String str1 = Utils.getSmsAddressListFromContactList(((SessionMessages.SMSComposerPayload)paramStartSMSComposeEvent.payload()).getReceiversList());
    String str2 = ((SessionMessages.SMSComposerPayload)paramStartSMSComposeEvent.payload()).getInfo();
    Log.w("Tango.PictureViewerActivity", "handleStartSMSComposeEvent() " + str2);
    Intent localIntent = Utils.getSmsIntent(str1, str2);
    try
    {
      startActivityForResult(localIntent, 1);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Log.w("Tango.PictureViewerActivity", "No activity was found for ACTION_VIEW (for sending SMS)");
    }
  }

  private void hideBar(boolean paramBoolean)
  {
    cancelTimerTriggerHidingBar();
    this.m_isBarVisible = false;
    if (paramBoolean)
    {
      this.m_actionBar.startAnimation(this.m_FadeOutAnimation);
      return;
    }
    this.m_actionBar.setVisibility(4);
  }

  private void postFinishSMSComposeMessage(boolean paramBoolean)
  {
    MediaEngineMessage.FinishSMSComposeMessage localFinishSMSComposeMessage = new MediaEngineMessage.FinishSMSComposeMessage(paramBoolean);
    MessageRouter.getInstance().postMessage("jingle", localFinishSMSComposeMessage);
  }

  private static void setRunningInstance(PictureViewerActivity paramPictureViewerActivity)
  {
    s_instance = paramPictureViewerActivity;
  }

  private void showBar()
  {
    this.m_actionBar.clearAnimation();
    this.m_isBarVisible = true;
    this.m_actionBar.setVisibility(0);
  }

  private void startTimerTriggerHidingBar()
  {
    this.m_timeoutHandler.postDelayed(this.m_barHider, 5000L);
  }

  private void toggleBar()
  {
    if (this.m_isBarVisible)
    {
      hideBar(false);
      return;
    }
    showBar();
  }

  private void updateImageAndStatusFromMessage(SessionMessages.ConversationMessage paramConversationMessage)
  {
    String str = getMessageMediaPath(paramConversationMessage);
    int i = paramConversationMessage.getProgress();
    SessionMessages.ConversationMessageLoadingStatus localConversationMessageLoadingStatus = paramConversationMessage.getLoadingStatus();
    if (localConversationMessageLoadingStatus == SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_MEDIA_ERROR)
    {
      handleDownloadingError();
      return;
    }
    if ((localConversationMessageLoadingStatus == SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_MEDIA_LOADED) || (localConversationMessageLoadingStatus == SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_MEDIA_LOADING))
      updateUiWithImageAndStatus(str, localConversationMessageLoadingStatus, i);
    if ((!paramConversationMessage.getPeer().getIsSystemAccount()) && ((!paramConversationMessage.getIsFromMe()) || (paramConversationMessage.getSendStatus() == SessionMessages.ConversationMessageSendStatus.STATUS_SENT)));
    for (boolean bool = true; ; bool = false)
    {
      this.m_isForwardEnabled = bool;
      return;
    }
  }

  private void updateUiWithImageAndStatus(String paramString, SessionMessages.ConversationMessageLoadingStatus paramConversationMessageLoadingStatus, int paramInt)
  {
    changeImage(paramString);
    Button localButton = this.m_btnDoMoreOperations;
    boolean bool;
    ProgressBar localProgressBar;
    if (paramString != null)
    {
      bool = true;
      localButton.setEnabled(bool);
      localProgressBar = this.m_progressBar;
      if (paramConversationMessageLoadingStatus != SessionMessages.ConversationMessageLoadingStatus.STATUS_CONTENT_MEDIA_LOADING)
        break label63;
    }
    label63: for (int i = 0; ; i = 4)
    {
      localProgressBar.setVisibility(i);
      this.m_progressBar.setProgress(paramInt);
      return;
      bool = false;
      break;
    }
  }

  public void handleMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      if (!$assertionsDisabled)
        throw new AssertionError();
      break;
    case 35271:
      updateImageAndStatusFromMessage(((SessionMessages.ConversationMessagePayload)((MediaEngineMessage.DisplayConversationMessageEvent)paramMessage).payload()).getMessage());
    case 35281:
    case 35276:
    }
    SessionMessages.ConversationMessage localConversationMessage;
    do
    {
      return;
      handleStartSMSComposeEvent((MediaEngineMessage.StartSMSComposeEvent)paramMessage);
      return;
      localConversationMessage = ((SessionMessages.ConversationMessagePayload)((MediaEngineMessage.ConversationMessageSendStatusEvent)paramMessage).payload()).getMessage();
    }
    while (localConversationMessage.getSendStatus() != SessionMessages.ConversationMessageSendStatus.STATUS_READY_TO_SEND);
    changeImage(getMessageMediaPath(localConversationMessage));
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 1)
    {
      Log.w("Tango.PictureViewerActivity", "onActivityResult() resultCode = REQUEST_CODE_SMS_OLD_CLIENT");
      postFinishSMSComposeMessage(true);
    }
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    startTimerTriggerHidingBar();
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onContextItemSelected(paramMenuItem);
    case 0:
      onForwardClicked();
      return true;
    case 1:
    }
    onSaveToPhoneClicked();
    return true;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903099);
    this.m_imageView = ((ImageViewControl)findViewById(2131362017));
    this.m_actionBar = findViewById(2131362018);
    this.m_FadeOutAnimation = AnimationUtils.loadAnimation(this, 2130968579);
    this.m_FadeOutAnimation.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        PictureViewerActivity.this.m_actionBar.setVisibility(4);
      }

      public void onAnimationRepeat(Animation paramAnonymousAnimation)
      {
      }

      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
      }
    });
    this.m_btnDoMoreOperations = ((Button)findViewById(2131362021));
    this.m_btnDoMoreOperations.setEnabled(false);
    this.m_progressBar = ((ProgressBar)findViewById(2131361881));
    if (getFirstMessage() != null)
    {
      SessionMessages.ConversationMessage localConversationMessage = ((SessionMessages.ConversationMessagePayload)((MediaEngineMessage.ViewPictureEvent)getFirstMessage()).payload()).getMessage();
      this.m_conversationId = localConversationMessage.getConversationId();
      this.m_messageId = localConversationMessage.getMessageId();
      updateImageAndStatusFromMessage(localConversationMessage);
      setRunningInstance(this);
    }
    TangoApp.getInstance().sendMessageAck();
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    super.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
    if (this.m_isForwardEnabled)
      paramContextMenu.add(0, 0, 0, getResources().getString(2131296665));
    paramContextMenu.add(0, 1, 0, getResources().getString(2131296666));
    paramContextMenu.setHeaderTitle(2131296664);
    cancelTimerTriggerHidingBar();
  }

  protected void onDestroy()
  {
    super.onDestroy();
    clearRunningInstance(this);
  }

  public void onForwardClicked()
  {
    MediaEngineMessage.ForwardMessageRequestMessage localForwardMessageRequestMessage = new MediaEngineMessage.ForwardMessageRequestMessage(SessionMessages.ConversationMessage.newBuilder().setType(SessionMessages.ConversationMessageType.IMAGE_MESSAGE).setConversationId(this.m_conversationId).setMessageId(this.m_messageId).setTextIfNotSupport(TangoApp.getInstance().getApplicationContext().getResources().getString(2131296611)).build());
    MessageRouter.getInstance().postMessage("jingle", localForwardMessageRequestMessage);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      back();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onMoreOperationsClicked(View paramView)
  {
    registerForContextMenu(paramView);
    openContextMenu(paramView);
    unregisterForContextMenu(paramView);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    TangoApp.getInstance().sendMessageAck();
  }

  public void onSaveToPhoneClicked()
  {
    try
    {
      boolean bool2 = copyFile();
      bool1 = bool2;
      if (bool1)
      {
        i = 2131296667;
        Toast.makeText(this, i, 0).show();
        return;
      }
    }
    catch (Exception localException)
    {
      while (true)
      {
        boolean bool1 = false;
        continue;
        int i = 2131296668;
      }
    }
  }

  public void onStart()
  {
    super.onStart();
    this.m_imageView.loadImageToMemory();
    showBar();
  }

  public void onStop()
  {
    super.onStop();
    this.m_imageView.unloadImageFromMemory();
    cancelTimerTriggerHidingBar();
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 1)
      toggleBar();
    return super.onTouchEvent(paramMotionEvent);
  }

  public class SingleMediaScanner
    implements MediaScannerConnection.MediaScannerConnectionClient
  {
    private String m_fileName;
    private MediaScannerConnection m_mediaScannerConnection;

    public SingleMediaScanner(Context paramString, String arg3)
    {
      Object localObject;
      this.m_fileName = localObject;
      this.m_mediaScannerConnection = new MediaScannerConnection(paramString, this);
      this.m_mediaScannerConnection.connect();
    }

    public void onMediaScannerConnected()
    {
      this.m_mediaScannerConnection.scanFile(this.m_fileName, null);
    }

    public void onScanCompleted(String paramString, Uri paramUri)
    {
      this.m_mediaScannerConnection.disconnect();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.picture.PictureViewerActivity
 * JD-Core Version:    0.6.2
 */