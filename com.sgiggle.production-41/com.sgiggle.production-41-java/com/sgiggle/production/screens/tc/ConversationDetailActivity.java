package com.sgiggle.production.screens.tc;

import android.app.AlertDialog.Builder;
import android.app.KeyguardManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;
import com.sgiggle.cafe.vgood.CafeMgr;
import com.sgiggle.cafe.vgood.CafeView;
import com.sgiggle.media_engine.MediaEngineMessage.AdvertisementShownMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CloseConversationMessage;
import com.sgiggle.media_engine.MediaEngineMessage.OpenConversationEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ShowAdvertisementEvent;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.actionbarcompat.ActionBarActivity;
import com.sgiggle.production.actionbarcompat.ActionBarHelper;
import com.sgiggle.production.adapter.TcMediaSelectorAdapter;
import com.sgiggle.production.controller.ConversationMessageController.CreateMessageAction;
import com.sgiggle.production.fragment.ComposeConversationMessageFragment;
import com.sgiggle.production.fragment.ComposeConversationMessageFragment.ComposeConversationMessageFragmentListener;
import com.sgiggle.production.fragment.ConnectionStatusFragment;
import com.sgiggle.production.fragment.ConversationDetailFragment;
import com.sgiggle.production.fragment.ConversationDetailFragment.ConversationDetailFragmentListener;
import com.sgiggle.production.util.ContactThumbnailLoader;
import com.sgiggle.production.util.FileImageLoader;
import com.sgiggle.production.util.HttpImageLoader;
import com.sgiggle.production.widget.BetterViewPager;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.ConversationPayload;
import com.sgiggle.xmpp.SessionMessages.ShowAdvertisementPayload;
import java.util.ArrayList;
import java.util.HashMap;

public class ConversationDetailActivity extends ActionBarActivity
  implements ComposeConversationMessageFragment.ComposeConversationMessageFragmentListener, ConversationDetailFragment.ConversationDetailFragmentListener
{
  private static final int FOCUS_COMPOSE_FRAGMENT_TEXT_DELAY_MS = 100;
  private static final int MSG_HANDLE_COMPOSE_FRAGMENT_SHOWN = 1;
  private static final int MSG_SHOW_COMPOSE_FRAGMENT = 0;
  private static final String TAG = "Tango.ConversationDetailActivity";
  private static ConversationDetailActivity s_instance;
  private boolean m_cafeAnimationInProgress = false;
  private CafeView m_cafeView;
  private ComposeConversationMessageFragment m_composeFragment;
  private ConnectionStatusFragment m_connectionStatusFragment;
  private ConversationDetailFragmentAdapter m_fragmentAdapter;
  private Handler m_handler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
      case 0:
      case 1:
      }
      while (true)
      {
        return;
        int j = ((Integer)paramAnonymousMessage.obj).intValue();
        ConversationDetailActivity.this.showComposeFragment(true, j);
        return;
        ConversationDetailActivity.access$102(ConversationDetailActivity.this, true);
        int i = ((Integer)paramAnonymousMessage.obj).intValue();
        boolean bool = false;
        ConversationDetailFragment localConversationDetailFragment = ConversationDetailActivity.this.getCurrentConversationFragment();
        if ((localConversationDetailFragment == null) || (!localConversationDetailFragment.isAdded()))
          Log.d("Tango.ConversationDetailActivity", "Handler: dumping MSG_HANDLE_COMPOSE_FRAGMENT_SHOWN, there is no fragment yet, or it was detached from activity.");
        while ((!bool) && (i == 0) && (ConversationDetailActivity.this.m_composeFragment != null) && (ConversationDetailActivity.this.m_composeFragment.isAdded()))
        {
          ConversationDetailActivity.this.m_composeFragment.focusText();
          return;
          localConversationDetailFragment.onComposeEnabled();
          bool = localConversationDetailFragment.isTipLayerShown();
        }
      }
    }
  };
  private boolean m_isCafePaused = true;
  private boolean m_isComposeEnabled = false;
  private boolean m_isConversationClosed = false;
  private boolean m_isForegroundActivity = false;
  private MenuItem m_menuVideoCall;
  private final boolean m_supportsSwipingBetweenConversations = false;
  private BetterViewPager m_viewPager;

  public static void clearRunningInstance()
  {
    s_instance = null;
  }

  private static void clearRunningInstance(ConversationDetailActivity paramConversationDetailActivity)
  {
    if (s_instance == paramConversationDetailActivity)
      s_instance = null;
  }

  private void closeConversation(boolean paramBoolean)
  {
    if ((this.m_isConversationClosed) && (!paramBoolean))
    {
      Log.d("Tango.ConversationDetailActivity", "closeConversation: force=" + paramBoolean + " Conversation already closed, ignoring.");
      return;
    }
    Log.d("Tango.ConversationDetailActivity", "closeConversation: force=" + paramBoolean);
    sendCloseConversationMessage();
    this.m_isConversationClosed = true;
  }

  private ConversationDetailFragment getCurrentConversationFragment()
  {
    return (ConversationDetailFragment)getSupportFragmentManager().findFragmentById(2131361921);
  }

  public static ConversationDetailActivity getRunningInstance()
  {
    return s_instance;
  }

  private void onMediaSelected(int paramInt)
  {
    ConversationDetailFragment localConversationDetailFragment = getCurrentConversationFragment();
    if ((localConversationDetailFragment == null) || (!localConversationDetailFragment.isAdded()))
      return;
    switch (paramInt)
    {
    default:
      Log.e("Tango.ConversationDetailActivity", "onMediaSelected: unhandled type=" + paramInt);
      return;
    case 0:
      localConversationDetailFragment.createNewMessage(SessionMessages.ConversationMessageType.IMAGE_MESSAGE, ConversationMessageController.CreateMessageAction.ACTION_NEW, new Object[0]);
      return;
    case 1:
      localConversationDetailFragment.createNewMessage(SessionMessages.ConversationMessageType.IMAGE_MESSAGE, ConversationMessageController.CreateMessageAction.ACTION_PICK, new Object[0]);
      return;
    case 2:
      localConversationDetailFragment.createNewMessage(SessionMessages.ConversationMessageType.VIDEO_MESSAGE, ConversationMessageController.CreateMessageAction.ACTION_NEW, new Object[0]);
      return;
    case 3:
      onVideoCallClicked();
      return;
    case 4:
    }
    onAudioCallClicked();
  }

  private void onPauseVGood()
  {
    stopVGoodAnimation();
    this.m_cafeView.onPause();
    CafeMgr.Pause();
    this.m_isCafePaused = true;
    CafeMgr.FreeEngine();
  }

  private void onResumeVGood()
  {
    CafeMgr.InitEngine(this);
    CafeMgr.SetCallbacks();
    this.m_cafeView.onResume();
    CafeMgr.Resume();
    this.m_isCafePaused = false;
  }

  private void sendCloseConversationMessage()
  {
    Log.d("Tango.ConversationDetailActivity", "sendCloseConversationMessage");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.CloseConversationMessage());
  }

  private static void setRunningInstance(ConversationDetailActivity paramConversationDetailActivity)
  {
    s_instance = paramConversationDetailActivity;
  }

  private void showComposeFragment(boolean paramBoolean)
  {
    showComposeFragment(paramBoolean, -1);
  }

  private void showComposeFragment(boolean paramBoolean, int paramInt)
  {
    if (isFinishing())
    {
      Log.d("Tango.ConversationDetailActivity", "showComposeFragment: won't show fragment, activity is finishing.");
      return;
    }
    FragmentTransaction localFragmentTransaction = getSupportFragmentManager().beginTransaction();
    localFragmentTransaction.setCustomAnimations(2130968585, 2130968589);
    Animation localAnimation = AnimationUtils.loadAnimation(this, 2130968585);
    if (paramBoolean)
    {
      localFragmentTransaction.show(this.m_composeFragment);
      this.m_handler.removeMessages(1);
      android.os.Message localMessage = this.m_handler.obtainMessage(1, Integer.valueOf(paramInt));
      this.m_handler.sendMessageDelayed(localMessage, 100L + localAnimation.getDuration());
    }
    while (true)
    {
      localFragmentTransaction.commitAllowingStateLoss();
      return;
      localFragmentTransaction.hide(this.m_composeFragment);
    }
  }

  private void showMediaSelectorDialog()
  {
    ConversationDetailFragment localConversationDetailFragment = getCurrentConversationFragment();
    boolean bool = false;
    if (localConversationDetailFragment != null)
      bool = localConversationDetailFragment.isCallEnabled();
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setTitle(2131296653);
    final TcMediaSelectorAdapter localTcMediaSelectorAdapter = new TcMediaSelectorAdapter(this, bool);
    localBuilder.setAdapter(localTcMediaSelectorAdapter, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ConversationDetailActivity.this.onMediaSelected(localTcMediaSelectorAdapter.buttonToCommand(paramAnonymousInt));
        paramAnonymousDialogInterface.dismiss();
      }
    });
    localBuilder.show();
  }

  public void createNewMessage(SessionMessages.ConversationMessageType paramConversationMessageType, ConversationMessageController.CreateMessageAction paramCreateMessageAction, Object[] paramArrayOfObject)
  {
    ConversationDetailFragment localConversationDetailFragment = getCurrentConversationFragment();
    if (localConversationDetailFragment == null)
    {
      Log.d("Tango.ConversationDetailActivity", "createNewMessage asked, but no conversation is selected. Dumping message...");
      return;
    }
    localConversationDetailFragment.createNewMessage(paramConversationMessageType, paramCreateMessageAction, paramArrayOfObject);
  }

  public int getActionBarHomeIconResId()
  {
    return 2130837686;
  }

  public SessionMessages.ConversationPayload getConversationPayload(String paramString)
  {
    Log.d("Tango.ConversationDetailActivity", "getConversationPayload for conversation id=" + paramString);
    return this.m_fragmentAdapter.getConversationPayload(paramString);
  }

  public void handleMessage(com.sgiggle.messaging.Message paramMessage)
  {
    ConversationDetailFragment localConversationDetailFragment = getCurrentConversationFragment();
    boolean bool = false;
    if (localConversationDetailFragment != null)
      bool = localConversationDetailFragment.handleMessage(paramMessage);
    this.m_composeFragment.handleMessage(paramMessage);
    if (!bool)
    {
      switch (paramMessage.getType())
      {
      default:
        Log.w("Tango.ConversationDetailActivity", "handleMessage: message " + paramMessage.getType() + " was not handled by fragment or activity");
      case 35270:
      case 35284:
      case 35278:
      case 35229:
      case 35288:
      }
      while (true)
      {
        TangoApp.getInstance().sendMessageAck();
        return;
        SessionMessages.ConversationPayload localConversationPayload = (SessionMessages.ConversationPayload)((MediaEngineMessage.OpenConversationEvent)paramMessage).payload();
        this.m_fragmentAdapter.onNewConversationPayload(localConversationPayload);
        if (localConversationDetailFragment != null)
        {
          localConversationDetailFragment.onConversationPayloadChanged();
          continue;
          if (this.m_connectionStatusFragment != null)
          {
            this.m_connectionStatusFragment.updateConnectionStatus();
            continue;
            Toast.makeText(this, 2131296445, 1).show();
            continue;
            stopVGoodAnimation();
            continue;
            if (!this.m_cafeAnimationInProgress)
            {
              MediaEngineMessage.ShowAdvertisementEvent localShowAdvertisementEvent = (MediaEngineMessage.ShowAdvertisementEvent)paramMessage;
              long l = ((SessionMessages.ShowAdvertisementPayload)localShowAdvertisementEvent.payload()).getId();
              if (startVGoodAnimation(((SessionMessages.ShowAdvertisementPayload)localShowAdvertisementEvent.payload()).getPath(), l, 0L))
                MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.AdvertisementShownMessage());
            }
          }
        }
      }
    }
    TangoApp.getInstance().sendMessageAck();
  }

  public void hideIme()
  {
    this.m_composeFragment.hideIme();
  }

  public boolean hideMenuItems()
  {
    if (this.m_menuVideoCall == null)
      return false;
    this.m_menuVideoCall.setVisible(false);
    return true;
  }

  public boolean isComposeEnabled()
  {
    return this.m_isComposeEnabled;
  }

  public boolean isForegroundActivity()
  {
    return this.m_isForegroundActivity;
  }

  public void onAddMediaClicked()
  {
    showMediaSelectorDialog();
  }

  public void onAudioCallClicked()
  {
    ConversationDetailFragment localConversationDetailFragment = getCurrentConversationFragment();
    if (localConversationDetailFragment == null)
    {
      Log.d("Tango.ConversationDetailActivity", "onVideoCallClicked clicked but no conversation is selected. Dumping message...");
      return;
    }
    localConversationDetailFragment.onAudioCallClicked();
  }

  public void onBackPressed()
  {
    closeConversation(true);
  }

  public void onComposeActionStarted()
  {
    ConversationDetailFragment localConversationDetailFragment = getCurrentConversationFragment();
    if (localConversationDetailFragment == null)
    {
      Log.d("Tango.ConversationDetailActivity", "Compose clicked, but no conversation is selected. Dumping message...");
      return;
    }
    localConversationDetailFragment.hideTipLayer(true);
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.ConversationDetailActivity", "onCreate()");
    super.onCreate(paramBundle);
    setRunningInstance(this);
    setContentView(2130903066);
    this.m_fragmentAdapter = new ConversationDetailFragmentAdapter(getSupportFragmentManager());
    this.m_viewPager = ((BetterViewPager)findViewById(2131361921));
    this.m_viewPager.setAdapter(this.m_fragmentAdapter);
    this.m_viewPager.setScrollableChildResId(2131361929);
    this.m_composeFragment = ((ComposeConversationMessageFragment)getSupportFragmentManager().findFragmentById(2131361920));
    showComposeFragment(false);
    this.m_cafeView = ((CafeView)findViewById(2131361830));
    this.m_cafeView.setZOrderOnTop(true);
    this.m_connectionStatusFragment = ((ConnectionStatusFragment)getSupportFragmentManager().findFragmentById(2131361919));
    if (getFirstMessage() != null)
      handleMessage(getFirstMessage());
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131623936, paramMenu);
    this.m_menuVideoCall = paramMenu.findItem(2131362216);
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    clearRunningInstance(this);
    if (s_instance == null)
      closeConversation(false);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 82)
      return true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onLoadFinished(int paramInt)
  {
    this.m_handler.sendMessage(this.m_handler.obtainMessage(0, Integer.valueOf(paramInt)));
    refreshActionBar();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
    case 16908332:
    }
    while (true)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      closeConversation(true);
    }
  }

  protected void onPause()
  {
    super.onPause();
    ContactThumbnailLoader.getInstance().stop();
    HttpImageLoader.getInstance().stop();
    FileImageLoader.getInstance().stop();
    onPauseVGood();
    this.m_isForegroundActivity = false;
  }

  protected void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    getActionBarHelper().setDisplayHomeAsUpEnabled(true);
  }

  protected void onResume()
  {
    this.m_isConversationClosed = false;
    super.onResume();
    ContactThumbnailLoader.getInstance().start();
    HttpImageLoader.getInstance().start();
    FileImageLoader.getInstance().start();
    onResumeVGood();
    this.m_isForegroundActivity = true;
  }

  public void onTipClicked()
  {
    this.m_composeFragment.focusText();
  }

  public void onVideoCallClicked()
  {
    ConversationDetailFragment localConversationDetailFragment = getCurrentConversationFragment();
    if (localConversationDetailFragment == null)
    {
      Log.d("Tango.ConversationDetailActivity", "onVideoCallClicked clicked but no conversation is selected. Dumping message...");
      return;
    }
    localConversationDetailFragment.onVideoCallClicked();
  }

  public boolean startVGoodAnimation(String paramString, long paramLong1, long paramLong2)
  {
    if ((this.m_cafeAnimationInProgress) || (this.m_isCafePaused))
      return false;
    if (TextUtils.isEmpty(paramString))
    {
      Toast.makeText(this, 2131296604, 0).show();
      return false;
    }
    if (((KeyguardManager)getSystemService("keyguard")).inKeyguardRestrictedInputMode())
      return false;
    this.m_cafeAnimationInProgress = true;
    this.m_cafeView.setVisibility(0);
    CafeMgr.LoadSurprise(paramString, "Surprise.Cafe");
    CafeMgr.StartSurprise(paramString, paramLong2, paramLong1, true);
    return true;
  }

  public void stopVGoodAnimation()
  {
    if (this.m_cafeAnimationInProgress)
    {
      this.m_cafeAnimationInProgress = false;
      this.m_cafeView.setVisibility(4);
      CafeMgr.StopAllSurprises();
      CafeMgr.Reset();
    }
  }

  public boolean updateMenuItemsVisibility(boolean paramBoolean)
  {
    if (this.m_menuVideoCall == null)
      return false;
    MenuItem localMenuItem = this.m_menuVideoCall;
    boolean bool = false;
    if (!paramBoolean)
      bool = true;
    localMenuItem.setVisible(bool);
    return true;
  }

  public class ConversationDetailFragmentAdapter extends FragmentStatePagerAdapter
  {
    private ArrayList<String> m_conversationIds = new ArrayList();
    private HashMap<String, SessionMessages.ConversationPayload> m_conversationPayloads = new HashMap();

    public ConversationDetailFragmentAdapter(FragmentManager arg2)
    {
      super();
    }

    public SessionMessages.ConversationPayload getConversationPayload(String paramString)
    {
      Log.d("Tango.ConversationDetailActivity", "getConversationPayload: conversationId=" + paramString);
      return (SessionMessages.ConversationPayload)this.m_conversationPayloads.get(paramString);
    }

    public int getCount()
    {
      if (this.m_conversationIds == null)
        return 0;
      return this.m_conversationIds.size();
    }

    public Fragment getItem(int paramInt)
    {
      String str = (String)this.m_conversationIds.get(paramInt);
      Log.d("Tango.ConversationDetailActivity", "ConversationDetailFragmentAdapter: position=" + paramInt + " conversationId=" + str);
      return ConversationDetailFragment.newInstance(str);
    }

    public int getItemPosition(Object paramObject)
    {
      return -2;
    }

    public void onNewConversationPayload(SessionMessages.ConversationPayload paramConversationPayload)
    {
      String str = paramConversationPayload.getConversationId();
      boolean bool = this.m_conversationIds.contains(str);
      Log.d("Tango.ConversationDetailActivity", "onNewConversationPayload: conversationId=" + str + " isConversationIdKnown=" + bool);
      this.m_conversationPayloads.clear();
      this.m_conversationIds.clear();
      if (bool)
        this.m_conversationIds.add(str);
      this.m_conversationPayloads.put(str, paramConversationPayload);
      if (!bool)
      {
        Log.d("Tango.ConversationDetailActivity", "onNewConversationPayload: refreshing data");
        this.m_conversationIds.add(str);
        notifyDataSetChanged();
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.tc.ConversationDetailActivity
 * JD-Core Version:    0.6.2
 */