package com.sgiggle.production.screens.tc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import com.sgiggle.media_engine.MediaEngineMessage.OpenConversationListEvent;
import com.sgiggle.media_engine.MediaEngineMessage.SelectContactRequestMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.FragmentActivityBase;
import com.sgiggle.production.fragment.ConnectionStatusFragment;
import com.sgiggle.production.fragment.ConversationListActionsFragment.ConversationListActionsFragmentListener;
import com.sgiggle.production.fragment.ConversationListFragment;
import com.sgiggle.production.fragment.VideomailListFragment;
import com.sgiggle.production.util.ContactThumbnailLoader;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ConversationSummaryListPayload;
import com.sgiggle.xmpp.SessionMessages.SelectContactType;

public class ConversationsActivity extends FragmentActivityBase
  implements RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener, ConversationListActionsFragment.ConversationListActionsFragmentListener
{
  private static final String TAG = "Tango.ConversationsActivity";
  private final boolean USE_CONVERSATION_TOGGLE = false;
  private ConnectionStatusFragment m_connectionStatusFragment;
  private RadioGroup m_conversationFilterGroup;
  private final int[] m_conversationFilterIds = { 2131361947, 2131361948 };
  private SessionMessages.ConversationSummaryListPayload m_conversationSummaryListPayload = null;
  private ConversationFragmentAdapter m_fragmentAdapter;
  private int m_nbConversationFilters;
  private ViewPager m_viewPager;

  private SessionMessages.ConversationSummaryListPayload getConversationSummaryListPayload()
  {
    if (this.m_conversationSummaryListPayload != null)
      return this.m_conversationSummaryListPayload;
    if ((this.m_firstMessage instanceof MediaEngineMessage.OpenConversationListEvent))
      return (SessionMessages.ConversationSummaryListPayload)((MediaEngineMessage.OpenConversationListEvent)this.m_firstMessage).payload();
    return null;
  }

  private Fragment getCurrentFragment()
  {
    return getSupportFragmentManager().findFragmentById(2131361921);
  }

  private int getPagePositionFromFilterId(int paramInt)
  {
    int i = this.m_conversationFilterIds.length;
    for (int j = 0; j < i; j++)
      if (this.m_conversationFilterIds[j] == paramInt)
        return j;
    return -1;
  }

  private void sendSelectContactRequestMessage()
  {
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.SelectContactRequestMessage(SessionMessages.SelectContactType.SELECT_TO_START_CONVERSATION));
  }

  protected boolean finishIfResumedAfterKilled()
  {
    return false;
  }

  public void handleMessage(Message paramMessage)
  {
    Fragment localFragment = getCurrentFragment();
    boolean bool1;
    switch (paramMessage.getType())
    {
    default:
      boolean bool2 = localFragment instanceof ConversationListFragment;
      bool1 = false;
      if (bool2)
        bool1 = ((ConversationListFragment)localFragment).handleMessage(paramMessage);
      break;
    case 35272:
    case 35284:
    }
    while (true)
    {
      if (!bool1)
        Log.w("Tango.ConversationsActivity", "handleMessage: message " + paramMessage.getType() + " was not handled by fragment");
      return;
      this.m_conversationSummaryListPayload = ((SessionMessages.ConversationSummaryListPayload)((MediaEngineMessage.OpenConversationListEvent)paramMessage).payload());
      if ((localFragment instanceof ConversationListFragment))
        ((ConversationListFragment)localFragment).handleMessage(paramMessage);
      this.m_viewPager.setCurrentItem(getPagePositionFromFilterId(2131361947));
      bool1 = true;
      continue;
      ConnectionStatusFragment localConnectionStatusFragment = this.m_connectionStatusFragment;
      bool1 = false;
      if (localConnectionStatusFragment != null)
      {
        this.m_connectionStatusFragment.updateConnectionStatus();
        bool1 = false;
      }
    }
  }

  public void onAddTextClicked()
  {
    sendSelectContactRequestMessage();
  }

  public void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt)
  {
    if (!((RadioButton)paramRadioGroup.findViewById(paramInt)).isChecked());
    while (true)
    {
      return;
      for (int i = 0; i < this.m_nbConversationFilters; i++)
        if ((this.m_conversationFilterIds[i] == paramInt) && (this.m_viewPager.getCurrentItem() != i))
        {
          this.m_viewPager.setCurrentItem(i);
          return;
        }
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.ConversationsActivity", "onCreate()");
    super.onCreate(paramBundle);
    setContentView(2130903073);
    this.m_conversationFilterGroup = ((RadioGroup)findViewById(2131361946));
    this.m_conversationFilterGroup.setVisibility(8);
    this.m_nbConversationFilters = 1;
    this.m_fragmentAdapter = new ConversationFragmentAdapter(getSupportFragmentManager(), this.m_nbConversationFilters);
    this.m_viewPager = ((ViewPager)findViewById(2131361921));
    this.m_viewPager.setAdapter(this.m_fragmentAdapter);
    this.m_viewPager.setOnPageChangeListener(this);
    this.m_connectionStatusFragment = ((ConnectionStatusFragment)getSupportFragmentManager().findFragmentById(2131361919));
  }

  public void onPageScrollStateChanged(int paramInt)
  {
  }

  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
  {
  }

  public void onPageSelected(int paramInt)
  {
    if (this.m_conversationFilterGroup != null)
      this.m_conversationFilterGroup.check(this.m_conversationFilterIds[paramInt]);
  }

  protected void onPause()
  {
    super.onPause();
    ContactThumbnailLoader.getInstance().stop();
  }

  protected void onResume()
  {
    super.onResume();
    ContactThumbnailLoader.getInstance().start();
  }

  public void scrollToRelevantItem()
  {
    Fragment localFragment = getCurrentFragment();
    if ((localFragment instanceof ConversationListFragment))
      ((ConversationListFragment)localFragment).scrollToRelevantItem();
  }

  private class ConversationFragmentAdapter extends FragmentPagerAdapter
  {
    private int m_numPages;

    public ConversationFragmentAdapter(FragmentManager paramInt, int arg3)
    {
      super();
      int i;
      this.m_numPages = i;
    }

    public int getCount()
    {
      return this.m_numPages;
    }

    public Fragment getItem(int paramInt)
    {
      switch (ConversationsActivity.this.m_conversationFilterIds[paramInt])
      {
      default:
        Log.e("Tango.ConversationsActivity", "ConversationFragmentAdapter.getItem(): invalid position #" + paramInt);
        return null;
      case 2131361947:
        return ConversationListFragment.newInstance(ConversationsActivity.this.getConversationSummaryListPayload());
      case 2131361948:
      }
      return new VideomailListFragment();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.tc.ConversationsActivity
 * JD-Core Version:    0.6.2
 */