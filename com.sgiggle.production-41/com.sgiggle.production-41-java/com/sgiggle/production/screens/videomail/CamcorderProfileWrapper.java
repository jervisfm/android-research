package com.sgiggle.production.screens.videomail;

import android.media.CamcorderProfile;

public class CamcorderProfileWrapper
{
  public int audioBitRate;
  public int audioChannels;
  public int audioCodec;
  public int audioSampleRate;
  public int duration;
  public int fileFormat;
  public int quality;
  public int videoBitRate;
  public int videoCodec;
  public int videoFrameHeight;
  public int videoFrameRate;
  public int videoFrameWidth;

  public void setFromCamcorderProfile(CamcorderProfile paramCamcorderProfile)
  {
    this.audioBitRate = paramCamcorderProfile.audioBitRate;
    this.audioChannels = paramCamcorderProfile.audioChannels;
    this.audioCodec = paramCamcorderProfile.audioCodec;
    this.audioSampleRate = paramCamcorderProfile.audioSampleRate;
    this.duration = paramCamcorderProfile.duration;
    this.fileFormat = paramCamcorderProfile.fileFormat;
    this.quality = paramCamcorderProfile.quality;
    this.videoBitRate = paramCamcorderProfile.videoBitRate;
    this.videoCodec = paramCamcorderProfile.videoCodec;
    this.videoFrameRate = paramCamcorderProfile.videoFrameRate;
    this.videoFrameWidth = paramCamcorderProfile.videoFrameWidth;
    this.videoFrameHeight = paramCamcorderProfile.videoFrameHeight;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.CamcorderProfileWrapper
 * JD-Core Version:    0.6.2
 */