package com.sgiggle.production.screens.videomail;

public class CameraHardwareException extends Exception
{
  private static final long serialVersionUID = 1L;

  public CameraHardwareException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.CameraHardwareException
 * JD-Core Version:    0.6.2
 */