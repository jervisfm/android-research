package com.sgiggle.production.screens.videomail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.media.MediaRecorder.OnInfoListener;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.sgiggle.VideoCapture.CameraWrapper;
import com.sgiggle.VideoCapture.CameraWrapper.CameraInfo;
import com.sgiggle.media_engine.MediaEngineMessage.RecordVideoMailEvent;
import com.sgiggle.media_engine.MediaEngineMessage.StartRecordingVideoMailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.StopRecordingVideoMailMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.ActivityBase;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.screens.videomail.util.RecorderHelper;
import com.sgiggle.production.widget.RotateButton;
import com.sgiggle.production.widget.Timer.Callback;
import com.sgiggle.production.widget.VideoPreviewLayout;
import com.sgiggle.production.widget.VideoPreviewLayout.OnSizeChangedListener;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.RecordVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.StopRecordingVideoMailPayload.Type;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RecordVideomailActivity extends ActivityBase
  implements SurfaceHolder.Callback, VideoPreviewLayout.OnSizeChangedListener, MediaRecorder.OnErrorListener, MediaRecorder.OnInfoListener, View.OnClickListener, Timer.Callback, SendVideomailActivityHelper.SendVideomailActivityHelperListener
{
  private static final int AUDIO_BIT_RATE = 0;
  private static final int AUDIO_CODEC = 0;
  private static final int CAMERA_ID_UNKNOWN = -1;
  private static final float DEFAULT_CAMERA_BRIGHTNESS = 0.7F;
  private static final int DEFAULT_MAX_RECORDING_DURATION = 30000;
  private static final String FILE_EXTENSION = ".mp4";
  private static final int FILE_FORMAT = 2;
  private static final int FRAME_RATE = 30;
  public static final String KEY_CALLEE_ACCOUNT_ID = "com.sgiggle.production.screens.videomail.RecordVideomailNewActivity.KEY_ACCOUNT_ID";
  private static final long LOW_STORAGE_THRESHOLD = 3145728L;
  private static final String MIME_TYPE = "video/mp4";
  private static final int MSG_CLEAR_SCREEN_DELAY = 0;
  private static final int MSG_HIDE_PROGRESS_VIEW = 8;
  private static final int MSG_KEEP_SCREEN_ON_A_WHILE = 5;
  private static final int MSG_RECONNECT_CAMERA = 2;
  private static final int MSG_SET_PREVIEW_UI = 4;
  private static final int MSG_TURN_REC_BUTTON_LIGHT_OFF = 7;
  private static final int MSG_TURN_REC_BUTTON_LIGHT_ON = 6;
  private static final int RECONNECT_CAMERA_DELAY = 2000;
  private static final int REC_LIGHT_FIRST_TOGGLE_DELAY = 1000;
  private static final int REC_LIGHT_ON_DURATION = 500;
  private static final int SCREEN_DELAY = 120000;
  private static boolean SHOW_TIMER_AUTO_DISMISS_ALERT = false;
  private static final String TAG = "Tango.RecordVideomailActivity";
  private static final long THRESHOLD_RECORD = 1000L;
  private static final long THRESHOLD_SWITCH_CAMERA = 1000L;
  private static final String THUMBNAIL_FILE_EXTENSION = ".jpeg";
  private static final int TIMER_AUTO_DISMISS_ALERT_DURATION = 5000;
  private static final int TIMER_PERMANENT_ALERT_DURATION = 5000;
  private static final int VIDEO_BIT_RATE = 0;
  private static final int VIDEO_CODEC = 0;
  private static final int VIDEO_FRAME_HEIGHT = 240;
  private static final int VIDEO_FRAME_WIDTH = 320;
  private static final boolean isGingerbreadOrHigher;
  private static final boolean isIcsOrHigher;
  private static final boolean isSupportCamcorderProfile;
  private TextView m_alertMessage;
  private FrameLayout m_alertNotificaiton;
  private boolean m_alwaysShowTimerAlert = false;
  private List<SessionMessages.Contact> m_calleesList;
  private CamcorderProfile m_camcorderProfile;
  private CameraWrapper m_cameraDevice;
  private int m_cameraDisplayOrientation = 0;
  private int m_cameraId = 0;
  private int[] m_cameraMap = { 0, 0 };
  private Camera.Parameters m_cameraParameters;
  private Button m_cancel;
  private com.sgiggle.production.widget.Timer m_countdownTimer;
  private String m_currentFilename;
  private int m_currentRequestedOrientation;
  private String m_currentThumbnailFilename;
  private ProgressBar m_fileUploadProgress;
  private LinearLayout m_fileUploader;
  private final Handler m_handler = new MainHandler(null);
  private int m_hideAutoDismissTimerAlertAt;
  private boolean m_isMaxDurationReached = false;
  private boolean m_isPausing = false;
  private boolean m_isRecording = false;
  private boolean m_isReviewInProgress = false;
  private boolean m_isStopping = false;
  private boolean m_isVideomailReplaySupported = false;
  private int[] m_layout_orientation = { 0, 0 };
  private long m_maxRawVideoSize = calculateMaxRawVideoSize(30000);
  private int m_maxRecordingDuration = 30000;
  private MediaRecorder m_mediaRecorder;
  private int m_numberOfCameras;
  private int m_orientation = -1;
  private int m_orientationCompensation = 0;
  private RecorderOrientationListener m_orientationListener;
  private PrepareCameraThread m_prepareCameraThread;
  private VideoPreviewLayout m_previewLayout;
  private CamcorderProfileWrapper m_profile = new CamcorderProfileWrapper();
  private View m_progressView;
  private Drawable m_recBtnOffDrawable;
  private Drawable m_recBtnOnDrawable;
  private Button m_record;
  private java.util.Timer m_recordButtonTimer = new java.util.Timer();
  private int m_rotationHint = 0;
  private Button m_send;
  private SendVideomailActivityHelper m_sendHelper;
  private int m_showAutoDismissTimerAlertAt;
  private int m_showPermanentTimerAlertAt;
  private SurfaceHolder m_surfaceHolder = null;
  private SurfaceView m_surfaceView;
  private RotateButton m_switchCamera;
  private long m_timestampLastCameraSwitch = 0L;
  private long m_timestampLastRecord = 0L;
  private TextView m_uploadingMessage;
  private Object m_videomailSenderLock = new Object();

  static
  {
    boolean bool1;
    boolean bool2;
    label24: boolean bool3;
    label49: int i;
    label63: int j;
    label77: int k;
    if (Build.VERSION.SDK_INT >= 9)
    {
      bool1 = true;
      isGingerbreadOrHigher = bool1;
      if (Build.VERSION.SDK_INT < 14)
        break label122;
      bool2 = true;
      isIcsOrHigher = bool2;
      if ((Build.VERSION.SDK_INT < 8) || (Build.MODEL.equals("LG-MS910")))
        break label127;
      bool3 = true;
      isSupportCamcorderProfile = bool3;
      if (!isGingerbreadOrHigher)
        break label132;
      i = 32000;
      AUDIO_BIT_RATE = i;
      if (!isGingerbreadOrHigher)
        break label139;
      j = 500000;
      VIDEO_BIT_RATE = j;
      if (Build.VERSION.SDK_INT < 8)
        break label146;
      k = 2;
      label93: VIDEO_CODEC = k;
      if (!isGingerbreadOrHigher)
        break label152;
    }
    label132: label139: label146: label152: for (int m = 3; ; m = 1)
    {
      AUDIO_CODEC = m;
      SHOW_TIMER_AUTO_DISMISS_ALERT = false;
      return;
      bool1 = false;
      break;
      label122: bool2 = false;
      break label24;
      label127: bool3 = false;
      break label49;
      i = 12200;
      break label63;
      j = 384000;
      break label77;
      k = 3;
      break label93;
    }
  }

  private void calcCameraDisplayOrientation()
  {
    CameraWrapper.CameraInfo localCameraInfo = new CameraWrapper.CameraInfo();
    CameraWrapper.getCameraInfo(this.m_cameraMap[this.m_cameraId], localCameraInfo);
    this.m_cameraDisplayOrientation = RecorderHelper.getSurfaceRotation(localCameraInfo.facing);
    Log.v("Tango.RecordVideomailActivity", "set display orientation:" + this.m_cameraDisplayOrientation);
  }

  private static long calculateMaxRawVideoSize(int paramInt)
  {
    return 5184000L * (paramInt / 1000L);
  }

  private void cancelSend()
  {
    synchronized (this.m_videomailSenderLock)
    {
      if (this.m_sendHelper != null)
        this.m_sendHelper.cancel();
      this.m_sendHelper = null;
      ((TangoApp)getApplication()).setSendVideomailActivityHelper(null);
      return;
    }
  }

  private void cleanupFiles()
  {
    if (this.m_currentFilename != null)
    {
      deleteVideoFile(this.m_currentFilename);
      this.m_currentFilename = null;
    }
  }

  private void closeCamera()
  {
    Log.v("Tango.RecordVideomailActivity", "closeCamera");
    if (this.m_cameraDevice == null)
    {
      Log.d("Tango.RecordVideomailActivity", "already stopped.");
      return;
    }
    this.m_cameraDevice.lock();
    releaseCamera();
    this.m_cameraDevice = null;
  }

  private void deleteVideoFile(String paramString)
  {
    if (new File(paramString).delete())
    {
      Log.d("Tango.RecordVideomailActivity", "File deleted: " + paramString);
      return;
    }
    Log.w("Tango.RecordVideomailActivity", "Could not delete (or already deleted): " + paramString);
  }

  private static long getAvailableStorage()
  {
    StatFs localStatFs = new StatFs(Environment.getExternalStorageDirectory().toString());
    return localStatFs.getAvailableBlocks() * localStatFs.getBlockSize();
  }

  private int getDisplayRotation()
  {
    int i;
    int j;
    if (Build.VERSION.SDK_INT >= 8)
    {
      i = getWindowManager().getDefaultDisplay().getRotation();
      switch (i)
      {
      default:
        j = 0;
      case 1:
      case 2:
      case 3:
      }
    }
    while (true)
    {
      Log.v("Tango.RecordVideomailActivity", "Display Rotation Degree: " + j);
      return j;
      i = getWindowManager().getDefaultDisplay().getOrientation();
      break;
      j = 90;
      continue;
      j = 180;
      continue;
      j = 270;
    }
  }

  public static int getDisplayRotation(Activity paramActivity)
  {
    switch (paramActivity.getWindowManager().getDefaultDisplay().getRotation())
    {
    default:
      return 0;
    case 0:
      return 0;
    case 1:
      return 90;
    case 2:
      return 180;
    case 3:
    }
    return 270;
  }

  private void getIdealResolution()
  {
    setDefaultProfile();
    if (this.m_cameraDevice == null)
      return;
    if (Build.VERSION.SDK_INT >= 8)
    {
      List localList = this.m_cameraDevice.getParameters().getSupportedPreviewSizes();
      if (localList != null)
      {
        Collections.sort(localList, new CameraSizeComparator(null));
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
          Camera.Size localSize = (Camera.Size)localIterator.next();
          long l = 30 * (localSize.width * localSize.height) * (this.m_maxRecordingDuration / 1000);
          if ((localSize.width < localSize.height) || (l > this.m_maxRawVideoSize))
            break;
          this.m_profile.videoFrameWidth = localSize.width;
          this.m_profile.videoFrameHeight = localSize.height;
        }
      }
      Log.w("Tango.RecordVideomailActivity", "Device does not have any valid supported preview sizes. Defaulting to width=" + this.m_profile.videoFrameWidth + " height=" + this.m_profile.videoFrameHeight);
    }
    CameraWrapper.CameraInfo localCameraInfo = new CameraWrapper.CameraInfo();
    CameraWrapper.getCameraInfo(this.m_cameraMap[this.m_cameraId], localCameraInfo);
    RecorderHelper.getProfile(this.m_profile, localCameraInfo.facing);
  }

  private int getRecordDurationInSeconds()
  {
    int i = Math.round((float)this.m_countdownTimer.getDuration() / 1000.0F);
    Log.d("Tango.RecordVideomailActivity", "Record duration: " + i + "s");
    return i;
  }

  private String getStoragePath()
  {
    Date localDate = new Date();
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("MMddyyyyHHmmss");
    String str1 = "android_" + localSimpleDateFormat.format(localDate) + ".mp4";
    String str2 = "android_" + localSimpleDateFormat.format(localDate) + ".jpeg";
    File localFile1;
    if (("mounted".equals(Environment.getExternalStorageState())) && (getAvailableStorage() > 3145728L))
      localFile1 = new File(Environment.getExternalStorageDirectory(), str1);
    for (File localFile2 = new File(Environment.getExternalStorageDirectory(), str2); ; localFile2 = new File(getFilesDir(), str2))
    {
      this.m_currentFilename = localFile1.toString();
      Log.v("Tango.RecordVideomailActivity", "Local File Name: " + this.m_currentFilename);
      this.m_currentThumbnailFilename = localFile2.toString();
      this.m_currentFilename = localFile1.toString();
      Log.v("Tango.RecordVideomailActivity", "Local File Name: " + this.m_currentFilename);
      return localFile1.toString();
      localFile1 = new File(getFilesDir(), str1);
    }
  }

  private void handleRecordingError(Exception paramException)
  {
    if (paramException == null)
      Log.e("Tango.RecordVideomailActivity", "handlerRecordingError: Serious error happened during recording.");
    while (true)
    {
      Toast.makeText(this, 2131296440, 1).show();
      return;
      Log.e("Tango.RecordVideomailActivity", "handlerRecordingError: Serious error happened during recording.", paramException);
    }
  }

  private void initVideoPreferences()
  {
    if (isSupportCamcorderProfile)
      if (!isGingerbreadOrHigher)
        break label47;
    label47: for (CamcorderProfile localCamcorderProfile = CamcorderProfile.get(this.m_cameraMap[this.m_cameraId], 1); ; localCamcorderProfile = CamcorderProfile.get(1))
    {
      this.m_camcorderProfile = localCamcorderProfile;
      this.m_profile.setFromCamcorderProfile(this.m_camcorderProfile);
      getIdealResolution();
      return;
    }
  }

  private void initializeRecorder()
  {
    Log.v("Tango.RecordVideomailActivity", "initializeRecorder");
    if (this.m_cameraDevice == null)
      return;
    if (this.m_surfaceHolder == null)
    {
      Log.v("Tango.RecordVideomailActivity", "Surface holder is null. Wait for surface change");
      return;
    }
    this.m_isMaxDurationReached = false;
    if ((Build.MANUFACTURER.equalsIgnoreCase("Samsung")) && ((Build.MODEL.equalsIgnoreCase("GT-I9000")) || (Build.MODEL.startsWith("SCH-I800")) || (Build.MODEL.startsWith("SPH-D700")) || (Build.MODEL.equals("SAMSUNG-SGH-I997")) || (Build.MODEL.startsWith("SCH-I405"))))
      this.m_cameraDevice.stopPreview();
    this.m_mediaRecorder = new MediaRecorder();
    this.m_cameraDevice.unlock();
    this.m_mediaRecorder.setCamera(this.m_cameraDevice.getCamera());
    CameraWrapper.CameraInfo localCameraInfo;
    if (Build.MANUFACTURER.equalsIgnoreCase("SHARP"))
    {
      this.m_mediaRecorder.setAudioSource(1);
      this.m_mediaRecorder.setVideoSource(1);
      if (!isSupportCamcorderProfile)
        break label764;
      setToCamcorderProfile();
      this.m_mediaRecorder.setProfile(this.m_camcorderProfile);
      this.m_mediaRecorder.setMaxDuration(this.m_maxRecordingDuration);
      this.m_mediaRecorder.setOutputFile(getStoragePath());
      this.m_mediaRecorder.setPreviewDisplay(this.m_surfaceHolder.getSurface());
      if (this.m_orientation == -1)
        break label910;
      localCameraInfo = new CameraWrapper.CameraInfo();
      CameraWrapper.getCameraInfo(this.m_cameraMap[this.m_cameraId], localCameraInfo);
    }
    label910: for (int i = RecorderHelper.getRecorderRotation(localCameraInfo.orientation, this.m_orientation, localCameraInfo.facing); ; i = 0)
    {
      this.m_rotationHint = i;
      if (this.m_rotationHint % 90 != 0)
      {
        Log.w("Tango.RecordVideomailActivity", "Rotation Hint = " + this.m_rotationHint + " is NOT a multiple of 90!!!");
        this.m_rotationHint = (90 * ((90 + this.m_rotationHint) / 90));
        Log.i("Tango.RecordVideomailActivity", "Normalized Rotation Hint to = " + this.m_rotationHint);
      }
      boolean bool = RecorderHelper.isRotated();
      if ((isGingerbreadOrHigher) && (!Build.MANUFACTURER.equalsIgnoreCase("Samsung")) && (!bool))
      {
        Log.d("Tango.RecordVideomailActivity", "Orientation Hint: " + i);
        this.m_mediaRecorder.setOrientationHint(i);
      }
      for (int j = 1; ; j = 0)
        while (true)
        {
          if ((this.m_isVideomailReplaySupported) && (j == 0))
          {
            Log.d("Tango.RecordVideomailActivity", "Orientation Hint (for replay): " + i);
            this.m_mediaRecorder.setOrientationHint(i);
          }
          try
          {
            this.m_mediaRecorder.prepare();
            this.m_mediaRecorder.setOnErrorListener(this);
            this.m_mediaRecorder.setOnInfoListener(this);
            Log.v("Tango.RecordVideomailActivity", "MediaRecorder fileFormat: " + this.m_profile.fileFormat + "  quality: " + this.m_profile.quality + "  duration: " + this.m_profile.duration);
            Log.v("Tango.RecordVideomailActivity", "MediaRecorder audioCodec: " + this.m_profile.audioCodec + "  audioChannels: " + this.m_profile.audioChannels + "  audioSampleRate: " + this.m_profile.audioSampleRate + "  audioBitRate: " + this.m_profile.audioBitRate);
            Log.v("Tango.RecordVideomailActivity", "MediaRecorder videoCodec: " + this.m_profile.videoCodec + "  videoFrameSize: " + this.m_profile.videoFrameWidth + "x" + this.m_profile.videoFrameHeight + "  videoFrameRate: " + this.m_profile.videoFrameRate + "  videoBitRate: " + this.m_profile.videoBitRate);
            return;
            this.m_mediaRecorder.setAudioSource(5);
            break;
            label764: this.m_mediaRecorder.setOutputFormat(this.m_profile.fileFormat);
            this.m_mediaRecorder.setAudioEncoder(this.m_profile.audioCodec);
            this.m_mediaRecorder.setVideoEncoder(this.m_profile.videoCodec);
            this.m_mediaRecorder.setVideoSize(this.m_profile.videoFrameWidth, this.m_profile.videoFrameHeight);
            this.m_mediaRecorder.setVideoFrameRate(this.m_profile.videoFrameRate);
          }
          catch (IOException localIOException)
          {
            Log.e("Tango.RecordVideomailActivity", "prepare failed for " + this.m_currentFilename, localIOException);
            this.m_isRecording = false;
            updateRecordUi(this.m_isRecording);
            releaseMediaRecorder();
            throw new RuntimeException(localIOException);
          }
        }
    }
  }

  private void initializeView()
  {
    this.m_surfaceView = ((SurfaceView)findViewById(2131362159));
    this.m_record = ((Button)findViewById(2131362040));
    this.m_send = ((Button)findViewById(2131362038));
    this.m_cancel = ((Button)findViewById(2131362039));
    this.m_switchCamera = ((RotateButton)findViewById(2131362160));
    this.m_countdownTimer = ((com.sgiggle.production.widget.Timer)findViewById(2131362161));
    this.m_fileUploader = ((LinearLayout)findViewById(2131362142));
    this.m_fileUploadProgress = ((ProgressBar)findViewById(2131362143));
    this.m_alertNotificaiton = ((FrameLayout)findViewById(2131362140));
    this.m_alertMessage = ((TextView)findViewById(2131362141));
    this.m_uploadingMessage = ((TextView)findViewById(2131362144));
    this.m_progressView = findViewById(2131361988);
    this.m_record.setEnabled(false);
    this.m_record.setOnClickListener(this);
    this.m_cancel.setOnClickListener(this);
    this.m_switchCamera.setOnClickListener(this);
    this.m_countdownTimer.setTimerCallback(this);
    if (this.m_isVideomailReplaySupported)
      this.m_send.setVisibility(8);
    while (true)
    {
      SurfaceHolder localSurfaceHolder = this.m_surfaceView.getHolder();
      localSurfaceHolder.addCallback(this);
      localSurfaceHolder.setType(3);
      ((View)this.m_record.getParent()).post(new Runnable()
      {
        public void run()
        {
          Rect localRect = new Rect();
          RecordVideomailActivity.this.m_record.getHitRect(localRect);
          localRect.right = (20 + localRect.right);
          localRect.left -= 20;
          localRect.bottom = (5 + localRect.bottom);
          localRect.top -= 5;
          ((View)RecordVideomailActivity.this.m_record.getParent()).setTouchDelegate(new TouchDelegate(localRect, RecordVideomailActivity.this.m_record));
        }
      });
      return;
      this.m_send.setVisibility(0);
      this.m_send.setOnClickListener(this);
    }
  }

  private boolean isSending()
  {
    return this.m_sendHelper != null;
  }

  private void keepScreenOn()
  {
    this.m_handler.removeMessages(0);
    getWindow().addFlags(128);
  }

  private void keepScreenOnAwhile()
  {
    this.m_handler.removeMessages(0);
    getWindow().addFlags(128);
    this.m_handler.sendEmptyMessageDelayed(0, 120000L);
  }

  private void onRecordFinished()
  {
    if (this.m_isVideomailReplaySupported)
    {
      stopRecording(false);
      reviewVideoMail();
      return;
    }
    stopRecording(true);
  }

  private void openCamera(int paramInt)
    throws CameraHardwareException
  {
    if ((this.m_cameraDevice != null) && (this.m_cameraId != paramInt))
    {
      this.m_cameraDevice.release();
      this.m_cameraDevice = null;
      this.m_cameraId = -1;
    }
    if (this.m_cameraDevice == null)
    {
      try
      {
        Log.v("Tango.RecordVideomailActivity", "open camera " + this.m_cameraMap[paramInt]);
        this.m_cameraDevice = CameraWrapper.open(this.m_cameraMap[paramInt]);
        if (this.m_cameraDevice == null)
          throw new CameraHardwareException(new Throwable("CameraWrapper swallowed our runtime exception and returned a null camera device!"));
      }
      catch (RuntimeException localRuntimeException)
      {
        Log.e("Tango.RecordVideomailActivity", "fail to connect Camera", localRuntimeException);
        throw new CameraHardwareException(localRuntimeException);
      }
      this.m_cameraId = paramInt;
      this.m_cameraParameters = this.m_cameraDevice.getParameters();
      return;
    }
    try
    {
      this.m_cameraDevice.reconnect();
      Log.d("Tango.RecordVideomailActivity", "reconnect camera " + this.m_cameraMap[paramInt]);
      if (!isGingerbreadOrHigher)
      {
        this.m_cameraParameters.set("recording-size-width", 320);
        this.m_cameraParameters.set("recording-size-height", 240);
      }
      this.m_cameraDevice.setParameters(this.m_cameraParameters);
      return;
    }
    catch (IOException localIOException)
    {
      Log.e("Tango.RecordVideomailActivity", "reconnect failed");
      throw new CameraHardwareException(localIOException);
    }
  }

  private boolean prepareCamera()
  {
    if (this.m_surfaceView.getVisibility() == 0)
      this.m_surfaceView.setVisibility(4);
    this.m_progressView.setVisibility(0);
    this.m_record.setEnabled(false);
    try
    {
      startPrepareCameraThread();
      return true;
    }
    catch (Exception localException)
    {
      Log.w("Tango.RecordVideomailActivity", "prepareCamera() failed: " + localException);
    }
    return false;
  }

  private void releaseCamera()
  {
    this.m_cameraDevice.stopPreview();
    this.m_cameraDevice.release();
  }

  private void releaseMediaRecorder()
  {
    Log.v("Tango.RecordVideomailActivity", "Releasing media recorder.");
    if (this.m_mediaRecorder != null)
    {
      this.m_mediaRecorder.reset();
      this.m_mediaRecorder.release();
      this.m_mediaRecorder = null;
    }
    if (this.m_cameraDevice != null)
      this.m_cameraDevice.lock();
  }

  private void resetRecButtonLight()
  {
    this.m_handler.removeMessages(6);
    this.m_handler.removeMessages(7);
    setRecButtonLightOn(false);
  }

  private void resetScreenOn()
  {
    this.m_handler.removeMessages(0);
    getWindow().clearFlags(128);
  }

  private void resizeForPreviewAspectRatio()
  {
    if (Build.VERSION.SDK_INT > 7)
      calcCameraDisplayOrientation();
    if ((this.m_profile.videoFrameWidth != 0) && (this.m_profile.videoFrameHeight != 0))
    {
      VideoPreviewLayout localVideoPreviewLayout2 = this.m_previewLayout;
      if (this.m_cameraDisplayOrientation % 180 == 0);
      for (double d2 = this.m_profile.videoFrameWidth / this.m_profile.videoFrameHeight; ; d2 = this.m_profile.videoFrameHeight / this.m_profile.videoFrameWidth)
      {
        localVideoPreviewLayout2.setAspectRatio(d2);
        return;
      }
    }
    VideoPreviewLayout localVideoPreviewLayout1 = this.m_previewLayout;
    if (this.m_cameraDisplayOrientation % 180 == 0);
    for (double d1 = 1.333333333333333D; ; d1 = 0.75D)
    {
      localVideoPreviewLayout1.setAspectRatio(d1);
      return;
    }
  }

  private void restartPreview()
  {
    this.m_progressView.setVisibility(0);
    this.m_record.setEnabled(false);
    new Thread(new Runnable()
    {
      public void run()
      {
        try
        {
          RecordVideomailActivity.this.m_cameraDevice.stopPreview();
          RecordVideomailActivity.this.m_cameraDevice.startPreview();
          RecordVideomailActivity.this.m_handler.sendEmptyMessage(8);
          return;
        }
        catch (Exception localException)
        {
          Log.w("Tango.RecordVideomailActivity", "restartPreview(): " + localException);
          RecordVideomailActivity.this.m_handler.sendEmptyMessageDelayed(2, 2000L);
        }
      }
    }).start();
  }

  private void sendStartRecordingMessage()
  {
    MediaEngineMessage.StartRecordingVideoMailMessage localStartRecordingVideoMailMessage = new MediaEngineMessage.StartRecordingVideoMailMessage();
    MessageRouter.getInstance().postMessage("jingle", localStartRecordingVideoMailMessage);
  }

  private void sendStopRecordingMessage(SessionMessages.StopRecordingVideoMailPayload.Type paramType)
  {
    MediaEngineMessage.StopRecordingVideoMailMessage localStopRecordingVideoMailMessage = new MediaEngineMessage.StopRecordingVideoMailMessage(paramType);
    MessageRouter.getInstance().postMessage("jingle", localStopRecordingVideoMailMessage);
  }

  private void setActivityLayout()
  {
    setActivityLayout(this.m_layout_orientation[this.m_cameraId]);
  }

  private void setActivityLayout(int paramInt)
  {
    Log.v("Tango.RecordVideomailActivity", "New layout orientation: " + paramInt);
    if (paramInt != getRequestedOrientation())
      setRequestedOrientation(paramInt);
    int i;
    boolean bool;
    if (paramInt == 0)
      if (useFlippedLandscapeLayout())
      {
        this.m_recBtnOnDrawable = getResources().getDrawable(2130837531);
        this.m_recBtnOffDrawable = getResources().getDrawable(2130837529);
        i = 2130903146;
        bool = false;
      }
    while (true)
    {
      setContentView(i);
      ((RotateButton)findViewById(2131362160)).lockUntilLayoutOrientationReached(bool);
      this.m_currentRequestedOrientation = paramInt;
      return;
      i = 2130903147;
      this.m_recBtnOnDrawable = getResources().getDrawable(2130837530);
      this.m_recBtnOffDrawable = getResources().getDrawable(2130837528);
      bool = true;
      continue;
      this.m_recBtnOnDrawable = getResources().getDrawable(2130837530);
      this.m_recBtnOffDrawable = getResources().getDrawable(2130837528);
      i = 2130903148;
      bool = false;
    }
  }

  // ERROR //
  private void setCameraParameters()
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_0
    //   2: getfield 289	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraDevice	Lcom/sgiggle/VideoCapture/CameraWrapper;
    //   5: invokevirtual 516	com/sgiggle/VideoCapture/CameraWrapper:getParameters	()Landroid/hardware/Camera$Parameters;
    //   8: putfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   11: aload_0
    //   12: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   15: aload_0
    //   16: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   19: getfield 557	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameWidth	I
    //   22: aload_0
    //   23: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   26: getfield 560	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameHeight	I
    //   29: invokevirtual 1123	android/hardware/Camera$Parameters:setPreviewSize	(II)V
    //   32: aload_0
    //   33: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   36: invokevirtual 1127	java/lang/Object:getClass	()Ljava/lang/Class;
    //   39: astore 16
    //   41: aload 16
    //   43: ldc_w 1129
    //   46: aconst_null
    //   47: checkcast 1131	[Ljava/lang/Class;
    //   50: invokevirtual 1137	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   53: astore 17
    //   55: aload 17
    //   57: astore_2
    //   58: iconst_2
    //   59: anewarray 1133	java/lang/Class
    //   62: astore 19
    //   64: aload 19
    //   66: iconst_0
    //   67: getstatic 1143	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   70: aastore
    //   71: aload 19
    //   73: iconst_1
    //   74: getstatic 1143	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   77: aastore
    //   78: aload 16
    //   80: ldc_w 1145
    //   83: aload 19
    //   85: invokevirtual 1137	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   88: astore 20
    //   90: aload 20
    //   92: astore 4
    //   94: aload_2
    //   95: astore_3
    //   96: aload_0
    //   97: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   100: invokevirtual 1127	java/lang/Object:getClass	()Ljava/lang/Class;
    //   103: ldc_w 1147
    //   106: aconst_null
    //   107: checkcast 1131	[Ljava/lang/Class;
    //   110: invokevirtual 1137	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   113: astore 15
    //   115: aload 15
    //   117: astore 6
    //   119: aload_3
    //   120: ifnull +499 -> 619
    //   123: aload 4
    //   125: ifnull +494 -> 619
    //   128: ldc 72
    //   130: new 394	java/lang/StringBuilder
    //   133: dup
    //   134: invokespecial 395	java/lang/StringBuilder:<init>	()V
    //   137: ldc_w 1149
    //   140: invokevirtual 401	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   143: aload_0
    //   144: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   147: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   150: invokevirtual 404	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   153: invokevirtual 408	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   156: invokestatic 447	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   159: pop
    //   160: aload_0
    //   161: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   164: aload_0
    //   165: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   168: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   171: aload_0
    //   172: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   175: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   178: invokestatic 1155	com/sgiggle/VideoCapture/VideoCaptureRaw:initFPSRange	(Landroid/hardware/Camera$Parameters;II)Ljava/util/List;
    //   181: astore 13
    //   183: aload 13
    //   185: invokeinterface 1158 1 0
    //   190: ifne +99 -> 289
    //   193: aload 13
    //   195: invokeinterface 1161 1 0
    //   200: iconst_2
    //   201: if_icmpne +88 -> 289
    //   204: aload_0
    //   205: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   208: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   211: aload 13
    //   213: iconst_0
    //   214: invokeinterface 1164 2 0
    //   219: checkcast 1139	java/lang/Integer
    //   222: invokevirtual 1167	java/lang/Integer:intValue	()I
    //   225: sipush 1000
    //   228: idiv
    //   229: if_icmpge +334 -> 563
    //   232: aload_0
    //   233: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   236: aload 13
    //   238: iconst_0
    //   239: invokeinterface 1164 2 0
    //   244: checkcast 1139	java/lang/Integer
    //   247: invokevirtual 1167	java/lang/Integer:intValue	()I
    //   250: sipush 1000
    //   253: idiv
    //   254: putfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   257: ldc 72
    //   259: new 394	java/lang/StringBuilder
    //   262: dup
    //   263: invokespecial 395	java/lang/StringBuilder:<init>	()V
    //   266: ldc_w 1169
    //   269: invokevirtual 401	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   272: aload_0
    //   273: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   276: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   279: invokevirtual 404	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   282: invokevirtual 408	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   285: invokestatic 447	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   288: pop
    //   289: aload_0
    //   290: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   293: ldc_w 1171
    //   296: invokevirtual 1174	android/hardware/Camera$Parameters:setWhiteBalance	(Ljava/lang/String;)V
    //   299: getstatic 664	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   302: ldc_w 1176
    //   305: invokevirtual 670	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   308: ifeq +77 -> 385
    //   311: getstatic 186	android/os/Build:MODEL	Ljava/lang/String;
    //   314: ldc_w 1178
    //   317: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   320: ifne +39 -> 359
    //   323: getstatic 186	android/os/Build:MODEL	Ljava/lang/String;
    //   326: ldc_w 1180
    //   329: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   332: ifne +27 -> 359
    //   335: getstatic 186	android/os/Build:MODEL	Ljava/lang/String;
    //   338: ldc_w 1182
    //   341: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   344: ifne +15 -> 359
    //   347: getstatic 186	android/os/Build:MODEL	Ljava/lang/String;
    //   350: ldc_w 672
    //   353: invokevirtual 677	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   356: ifeq +29 -> 385
    //   359: aload_0
    //   360: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   363: ldc_w 993
    //   366: sipush 320
    //   369: invokevirtual 997	android/hardware/Camera$Parameters:set	(Ljava/lang/String;I)V
    //   372: aload_0
    //   373: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   376: ldc_w 999
    //   379: sipush 240
    //   382: invokevirtual 997	android/hardware/Camera$Parameters:set	(Ljava/lang/String;I)V
    //   385: getstatic 664	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   388: ldc_w 1184
    //   391: invokevirtual 670	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   394: ifeq +52 -> 446
    //   397: getstatic 186	android/os/Build:MODEL	Ljava/lang/String;
    //   400: ldc_w 1186
    //   403: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   406: ifeq +40 -> 446
    //   409: getstatic 177	android/os/Build$VERSION:SDK_INT	I
    //   412: bipush 14
    //   414: if_icmplt +32 -> 446
    //   417: aload_0
    //   418: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   421: sipush 640
    //   424: putfield 557	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameWidth	I
    //   427: aload_0
    //   428: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   431: sipush 480
    //   434: putfield 560	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameHeight	I
    //   437: aload_0
    //   438: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   441: bipush 15
    //   443: putfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   446: aload_0
    //   447: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   450: invokevirtual 1189	android/hardware/Camera$Parameters:getSupportedFocusModes	()Ljava/util/List;
    //   453: astore 8
    //   455: getstatic 179	com/sgiggle/production/screens/videomail/RecordVideomailActivity:isGingerbreadOrHigher	Z
    //   458: ifeq +297 -> 755
    //   461: aload 8
    //   463: ldc_w 1191
    //   466: invokeinterface 1194 2 0
    //   471: ifeq +258 -> 729
    //   474: aload_0
    //   475: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   478: ldc_w 1191
    //   481: invokevirtual 1197	android/hardware/Camera$Parameters:setFocusMode	(Ljava/lang/String;)V
    //   484: getstatic 664	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   487: ldc_w 1199
    //   490: invokevirtual 670	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   493: ifeq +28 -> 521
    //   496: getstatic 186	android/os/Build:MODEL	Ljava/lang/String;
    //   499: ldc_w 1201
    //   502: invokevirtual 677	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   505: ifeq +16 -> 521
    //   508: aload_0
    //   509: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   512: sipush 1280
    //   515: sipush 960
    //   518: invokevirtual 1204	android/hardware/Camera$Parameters:setPictureSize	(II)V
    //   521: aload_0
    //   522: getfield 289	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraDevice	Lcom/sgiggle/VideoCapture/CameraWrapper;
    //   525: aload_0
    //   526: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   529: invokevirtual 1003	com/sgiggle/VideoCapture/CameraWrapper:setParameters	(Landroid/hardware/Camera$Parameters;)V
    //   532: aload_0
    //   533: aload_0
    //   534: getfield 289	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraDevice	Lcom/sgiggle/VideoCapture/CameraWrapper;
    //   537: invokevirtual 516	com/sgiggle/VideoCapture/CameraWrapper:getParameters	()Landroid/hardware/Camera$Parameters;
    //   540: putfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   543: return
    //   544: astore_1
    //   545: aconst_null
    //   546: astore_2
    //   547: aload_2
    //   548: astore_3
    //   549: aconst_null
    //   550: astore 4
    //   552: goto -456 -> 96
    //   555: astore 5
    //   557: aconst_null
    //   558: astore 6
    //   560: goto -441 -> 119
    //   563: aload_0
    //   564: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   567: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   570: aload 13
    //   572: iconst_1
    //   573: invokeinterface 1164 2 0
    //   578: checkcast 1139	java/lang/Integer
    //   581: invokevirtual 1167	java/lang/Integer:intValue	()I
    //   584: sipush 1000
    //   587: idiv
    //   588: if_icmple -331 -> 257
    //   591: aload_0
    //   592: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   595: aload 13
    //   597: iconst_1
    //   598: invokeinterface 1164 2 0
    //   603: checkcast 1139	java/lang/Integer
    //   606: invokevirtual 1167	java/lang/Integer:intValue	()I
    //   609: sipush 1000
    //   612: idiv
    //   613: putfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   616: goto -359 -> 257
    //   619: aload 6
    //   621: ifnull +59 -> 680
    //   624: aload_0
    //   625: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   628: aload_0
    //   629: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   632: aload_0
    //   633: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   636: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   639: invokestatic 1208	com/sgiggle/VideoCapture/VideoCaptureRaw:initFrameRate	(Landroid/hardware/Camera$Parameters;I)I
    //   642: putfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   645: ldc 72
    //   647: new 394	java/lang/StringBuilder
    //   650: dup
    //   651: invokespecial 395	java/lang/StringBuilder:<init>	()V
    //   654: ldc_w 1210
    //   657: invokevirtual 401	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   660: aload_0
    //   661: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   664: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   667: invokevirtual 404	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   670: invokevirtual 408	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   673: invokestatic 447	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   676: pop
    //   677: goto -388 -> 289
    //   680: aload_0
    //   681: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   684: aload_0
    //   685: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   688: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   691: invokevirtual 1213	android/hardware/Camera$Parameters:setPreviewFrameRate	(I)V
    //   694: ldc 72
    //   696: new 394	java/lang/StringBuilder
    //   699: dup
    //   700: invokespecial 395	java/lang/StringBuilder:<init>	()V
    //   703: ldc_w 1215
    //   706: invokevirtual 401	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   709: aload_0
    //   710: getfield 261	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_profile	Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
    //   713: getfield 819	com/sgiggle/production/screens/videomail/CamcorderProfileWrapper:videoFrameRate	I
    //   716: invokevirtual 404	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   719: invokevirtual 408	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   722: invokestatic 447	com/sgiggle/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   725: pop
    //   726: goto -437 -> 289
    //   729: aload 8
    //   731: ldc_w 1171
    //   734: invokeinterface 1194 2 0
    //   739: ifeq -255 -> 484
    //   742: aload_0
    //   743: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   746: ldc_w 1171
    //   749: invokevirtual 1197	android/hardware/Camera$Parameters:setFocusMode	(Ljava/lang/String;)V
    //   752: goto -268 -> 484
    //   755: aload 8
    //   757: ldc_w 1171
    //   760: invokeinterface 1194 2 0
    //   765: ifeq -281 -> 484
    //   768: aload_0
    //   769: getfield 986	com/sgiggle/production/screens/videomail/RecordVideomailActivity:m_cameraParameters	Landroid/hardware/Camera$Parameters;
    //   772: ldc_w 1171
    //   775: invokevirtual 1197	android/hardware/Camera$Parameters:setFocusMode	(Ljava/lang/String;)V
    //   778: goto -294 -> 484
    //   781: astore 9
    //   783: ldc 72
    //   785: ldc_w 1217
    //   788: invokestatic 626	com/sgiggle/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   791: pop
    //   792: aload 9
    //   794: invokevirtual 1220	java/lang/Exception:printStackTrace	()V
    //   797: goto -265 -> 532
    //   800: astore 18
    //   802: goto -255 -> 547
    //
    // Exception table:
    //   from	to	target	type
    //   32	55	544	java/lang/Exception
    //   96	115	555	java/lang/Exception
    //   521	532	781	java/lang/Exception
    //   58	90	800	java/lang/Exception
  }

  private void setDefaultProfile()
  {
    this.m_profile.fileFormat = 2;
    this.m_profile.audioBitRate = AUDIO_BIT_RATE;
    this.m_profile.videoBitRate = VIDEO_BIT_RATE;
    this.m_profile.audioCodec = AUDIO_CODEC;
    this.m_profile.videoCodec = VIDEO_CODEC;
    this.m_profile.videoFrameRate = 30;
    this.m_profile.videoFrameWidth = 320;
    this.m_profile.videoFrameHeight = 240;
  }

  private void setPreviewDisplay(SurfaceHolder paramSurfaceHolder)
  {
    try
    {
      this.m_cameraDevice.setPreviewDisplay(paramSurfaceHolder);
      return;
    }
    catch (Throwable localThrowable)
    {
      closeCamera();
      throw new RuntimeException("setPreviewDisplay failed", localThrowable);
    }
  }

  private void setRecButtonLightOn(boolean paramBoolean)
  {
    if (paramBoolean);
    for (Drawable localDrawable = this.m_recBtnOnDrawable; ; localDrawable = this.m_recBtnOffDrawable)
    {
      this.m_record.setCompoundDrawablesWithIntrinsicBounds(localDrawable, null, null, null);
      return;
    }
  }

  private void setToCamcorderProfile()
  {
    this.m_camcorderProfile.audioBitRate = this.m_profile.audioBitRate;
    this.m_camcorderProfile.audioChannels = this.m_profile.audioChannels;
    this.m_camcorderProfile.audioCodec = this.m_profile.audioCodec;
    this.m_camcorderProfile.audioSampleRate = this.m_profile.audioSampleRate;
    this.m_camcorderProfile.duration = this.m_profile.duration;
    this.m_camcorderProfile.fileFormat = this.m_profile.fileFormat;
    this.m_camcorderProfile.quality = this.m_profile.quality;
    this.m_camcorderProfile.videoBitRate = this.m_profile.videoBitRate;
    this.m_camcorderProfile.videoCodec = this.m_profile.videoCodec;
    this.m_camcorderProfile.videoFrameRate = this.m_profile.videoFrameRate;
    this.m_camcorderProfile.videoFrameWidth = this.m_profile.videoFrameWidth;
    this.m_camcorderProfile.videoFrameHeight = this.m_profile.videoFrameHeight;
  }

  private void showFileUploader()
  {
    this.m_fileUploader.setVisibility(0);
  }

  private void startPrepareCameraThread()
  {
    Log.d("Tango.RecordVideomailActivity", "prepareCamera(): opening camera and start preview");
    if (this.m_prepareCameraThread == null)
    {
      this.m_prepareCameraThread = new PrepareCameraThread();
      this.m_prepareCameraThread.start();
    }
  }

  private void startRecording()
  {
    Log.i("Tango.RecordVideomailActivity", "startRecording");
    this.m_send.setEnabled(false);
    if (this.m_numberOfCameras > 1)
      this.m_switchCamera.setVisibility(8);
    try
    {
      initializeRecorder();
      if (this.m_mediaRecorder == null)
      {
        Log.e("Tango.RecordVideomailActivity", "Failed to initialize media recorder");
        handleRecordingError(null);
        return;
      }
    }
    catch (Exception localException)
    {
      this.m_isRecording = false;
      updateRecordUi(this.m_isRecording);
      releaseMediaRecorder();
      Log.e("Tango.RecordVideomailActivity", "Failed to initialize media recorder");
      handleRecordingError(localException);
      return;
    }
    try
    {
      this.m_mediaRecorder.start();
      this.m_countdownTimer.setOrientation(this.m_orientationCompensation);
      this.m_countdownTimer.start();
      this.m_isRecording = true;
      updateRecordUi(this.m_isRecording);
      keepScreenOn();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      this.m_isRecording = false;
      updateRecordUi(this.m_isRecording);
      Log.e("Tango.RecordVideomailActivity", "Unable to start recording", localRuntimeException);
      releaseMediaRecorder();
      handleRecordingError(localRuntimeException);
    }
  }

  private void startSend()
  {
    Log.d("Tango.RecordVideomailActivity", "startSend()");
    File localFile;
    if ((this.m_currentFilename != null) && (this.m_currentFilename.length() > 0))
    {
      localFile = new File(this.m_currentFilename);
      if (!localFile.exists());
    }
    for (long l1 = localFile.length(); ; l1 = 0L)
    {
      if (l1 == 0L)
      {
        onUploadFailedFileNotFound();
        Log.w("Tango.RecordVideomailActivity", "uploadFile(): contentLength = 0. No need to request for upload token.");
        return;
      }
      String str1 = this.m_currentFilename;
      String str2 = this.m_currentThumbnailFilename;
      List localList = this.m_calleesList;
      int i = getRecordDurationInSeconds();
      long l2 = System.currentTimeMillis() / 1000L;
      if (RecorderHelper.isRotated());
      for (int j = this.m_rotationHint; ; j = 0)
      {
        this.m_sendHelper = new SendVideomailActivityHelper(this, str1, str2, localList, i, "video/mp4", l1, l2, j, RecorderHelper.isFlipped(), true, true, this);
        ((TangoApp)getApplication()).setSendVideomailActivityHelper(this.m_sendHelper);
        this.m_sendHelper.send();
        return;
      }
    }
  }

  private void stopPrepareCameraThread()
  {
    if (this.m_prepareCameraThread != null)
    {
      Log.d("Tango.RecordVideomailActivity", "stopPrepareCameraThread()");
      this.m_prepareCameraThread.requestExitAndWait();
      this.m_prepareCameraThread = null;
    }
  }

  private void stopRecording()
  {
    stopRecording(true);
  }

  private void stopRecording(boolean paramBoolean)
  {
    Log.i("Tango.RecordVideomailActivity", "stopRecording()");
    this.m_isStopping = true;
    if (this.m_isRecording)
    {
      this.m_countdownTimer.stop();
      this.m_mediaRecorder.setOnErrorListener(null);
      this.m_mediaRecorder.setOnInfoListener(null);
    }
    try
    {
      this.m_mediaRecorder.stop();
      this.m_isRecording = false;
      if (paramBoolean)
        this.m_handler.sendEmptyMessage(5);
      releaseMediaRecorder();
      if (paramBoolean)
      {
        restartPreview();
        this.m_handler.sendEmptyMessage(4);
      }
      this.m_isStopping = false;
      updateRecordUi(false);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      while (true)
        Log.e("Tango.RecordVideomailActivity", "stopRecording: failed to stop media recorder", localRuntimeException);
    }
  }

  private void switchCamera(int paramInt)
  {
    if (paramInt == 0)
      Log.d("Tango.RecordVideomailActivity", "Camera: BACK");
    while (this.m_isPausing)
    {
      return;
      if (paramInt == 1)
      {
        Log.d("Tango.RecordVideomailActivity", "Camera: FRONT");
      }
      else
      {
        Log.d("Tango.RecordVideomailActivity", "Camera: OTHER");
        switchCamera((paramInt + 1) % this.m_numberOfCameras);
        return;
      }
    }
    this.m_cameraId = paramInt;
    if (this.m_isRecording)
    {
      stopRecording();
      sendStopRecordingMessage(SessionMessages.StopRecordingVideoMailPayload.Type.STOP);
    }
    closeCamera();
    initVideoPreferences();
    if (this.m_layout_orientation[this.m_cameraId] != getRequestedOrientation())
    {
      this.m_surfaceView.setVisibility(8);
      this.m_switchCamera.setVisibility(8);
      setActivityLayout();
      this.m_previewLayout = ((VideoPreviewLayout)findViewById(2131362157));
      this.m_previewLayout.setOnSizeChangedListener(this);
      initializeView();
      this.m_switchCamera.setVisibility(0);
    }
    resizeForPreviewAspectRatio();
    prepareCamera();
  }

  private boolean throttleRecord()
  {
    if (System.currentTimeMillis() - this.m_timestampLastRecord <= 1000L)
      return true;
    this.m_timestampLastRecord = System.currentTimeMillis();
    return false;
  }

  private boolean throttleSwitchCamera()
  {
    if (System.currentTimeMillis() - this.m_timestampLastCameraSwitch <= 1000L)
      return true;
    this.m_timestampLastCameraSwitch = System.currentTimeMillis();
    return false;
  }

  private void updatePerCameraLayoutOrientation()
  {
    this.m_layout_orientation[0] = 0;
    this.m_layout_orientation[1] = 0;
    if ((Build.VERSION.SDK_INT < 8) && (Build.MANUFACTURER.equalsIgnoreCase("Samsung")) && (!Build.MODEL.equals("Nexus S")) && (!Build.MODEL.equals("GT-P1010")) && (!Build.MODEL.equals("SPH-M920")) && (!Build.MODEL.equals("SGH-T959V")) && (!Build.MODEL.equals("SCH-I510")) && (!Build.MODEL.equals("SGH-T839")) && (!Build.MODEL.equals("SAMSUNG-SGH-I997")))
      this.m_layout_orientation[1] = 1;
  }

  private boolean useFlippedLandscapeLayout()
  {
    return true;
  }

  public boolean isReviewInProgress()
  {
    return this.m_isReviewInProgress;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    synchronized (this.m_videomailSenderLock)
    {
      if ((this.m_sendHelper != null) && (this.m_sendHelper.handleOnActivityResult(paramInt1, paramInt2, paramIntent)))
        return;
      if (paramInt1 != 1)
        return;
      this.m_isReviewInProgress = false;
      switch (paramInt2)
      {
      default:
        Log.w("Tango.RecordVideomailActivity", "Unexpected result (logic error or previous activity crashed), code: " + paramInt2);
        sendStopRecordingMessage(SessionMessages.StopRecordingVideoMailPayload.Type.ABORT);
        finish();
        return;
      case 2:
      case 3:
      case 4:
      }
    }
    this.m_countdownTimer.hide();
    this.m_countdownTimer.reset();
    this.m_handler.sendEmptyMessage(4);
    this.m_alertNotificaiton.setVisibility(8);
    updateRecordUi(this.m_isRecording);
    return;
    sendStopRecordingMessage(SessionMessages.StopRecordingVideoMailPayload.Type.ABORT);
    finish();
  }

  public void onBackPressed()
  {
    if (this.m_isRecording)
      stopRecording(false);
    cleanupFiles();
    super.onBackPressed();
    sendStopRecordingMessage(SessionMessages.StopRecordingVideoMailPayload.Type.ABORT);
  }

  public void onBeforeSmsDialogShown()
  {
    if ((this.m_currentRequestedOrientation == 0) && (useFlippedLandscapeLayout()))
      setActivityLayout(1);
  }

  public void onClick(View paramView)
  {
    if (this.m_isStopping)
      Log.d("Tango.RecordVideomailActivity", "Stop in progress, ignoring event.");
    do
    {
      do
      {
        return;
        if (paramView != this.m_record)
          break;
      }
      while (isSending());
      if (throttleRecord())
      {
        Log.w("Tango.RecordVideomailActivity", "Throttling record action...");
        return;
      }
      this.m_alertNotificaiton.setVisibility(8);
      if (this.m_isRecording)
      {
        sendStopRecordingMessage(SessionMessages.StopRecordingVideoMailPayload.Type.STOP);
        onRecordFinished();
        return;
      }
      sendStartRecordingMessage();
      updateRecordUi(true);
      startRecording();
      return;
      if (paramView == this.m_send)
      {
        this.m_send.setEnabled(false);
        startSend();
        return;
      }
      if (paramView == this.m_cancel)
      {
        this.m_alertNotificaiton.setVisibility(8);
        if (this.m_isRecording)
        {
          sendStopRecordingMessage(SessionMessages.StopRecordingVideoMailPayload.Type.STOP);
          stopRecording(false);
        }
        cancelSend();
        cleanupFiles();
        sendStopRecordingMessage(SessionMessages.StopRecordingVideoMailPayload.Type.ABORT);
        finish();
        return;
      }
    }
    while (paramView != this.m_switchCamera);
    if (throttleSwitchCamera())
    {
      Log.w("Tango.RecordVideomailActivity", "Throttling switch camera action...");
      return;
    }
    switchCamera((1 + this.m_cameraId) % this.m_numberOfCameras);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }

  protected void onCreate(Bundle paramBundle)
  {
    Log.d("Tango.RecordVideomailActivity", "onCreate()");
    requestWindowFeature(1);
    getWindow().addFlags(1024);
    getWindow().clearFlags(2048);
    getWindow().getAttributes().screenBrightness = 0.7F;
    super.onCreate(paramBundle);
    MediaEngineMessage.RecordVideoMailEvent localRecordVideoMailEvent = (MediaEngineMessage.RecordVideoMailEvent)getFirstMessage();
    if ((localRecordVideoMailEvent != null) && (localRecordVideoMailEvent.payload() != null))
      this.m_calleesList = ((SessionMessages.RecordVideoMailPayload)localRecordVideoMailEvent.payload()).getCalleesList();
    if ((Build.MANUFACTURER.equalsIgnoreCase("samsung")) && ((Build.MODEL.equals("SHW-M110S")) || (Build.MODEL.equals("SAMSUNG-SGH-I997"))))
    {
      this.m_numberOfCameras = 1;
      this.m_cameraId = 0;
      if (this.m_numberOfCameras > 2)
        this.m_numberOfCameras = 2;
      Log.d("Tango.RecordVideomailActivity", "Number of cameras: " + this.m_numberOfCameras);
      initVideoPreferences();
      this.m_isVideomailReplaySupported = VideomailUtils.isVideomailReplaySupported();
      updatePerCameraLayoutOrientation();
      setActivityLayout();
      this.m_previewLayout = ((VideoPreviewLayout)findViewById(2131362157));
      this.m_previewLayout.setOnSizeChangedListener(this);
      initializeView();
      resizeForPreviewAspectRatio();
      if (this.m_numberOfCameras <= 1)
        break label468;
      this.m_switchCamera.setVisibility(0);
      label242: if ((localRecordVideoMailEvent == null) || (localRecordVideoMailEvent.payload() == null) || (!((SessionMessages.RecordVideoMailPayload)localRecordVideoMailEvent.payload()).hasMaxRecordingDuration()) || (((SessionMessages.RecordVideoMailPayload)localRecordVideoMailEvent.payload()).getMaxRecordingDuration() <= 0))
        break label480;
      this.m_maxRecordingDuration = (1000 * ((SessionMessages.RecordVideoMailPayload)localRecordVideoMailEvent.payload()).getMaxRecordingDuration());
      this.m_maxRawVideoSize = calculateMaxRawVideoSize(this.m_maxRecordingDuration);
      Log.d("Tango.RecordVideomailActivity", "Max recording duration (ms): " + this.m_maxRecordingDuration);
      label342: this.m_countdownTimer.setMaxTimerDuration(this.m_maxRecordingDuration);
      this.m_countdownTimer.setStartCallbackAt(0L);
      if (this.m_maxRecordingDuration > 10000)
        break label492;
      this.m_alwaysShowTimerAlert = true;
    }
    while (true)
    {
      this.m_orientationListener = new RecorderOrientationListener(this);
      ((TangoApp)getApplication()).setRecordVideomailActivityInstance(this);
      return;
      this.m_numberOfCameras = CameraWrapper.getNumberOfCameras();
      CameraWrapper.CameraInfo localCameraInfo = new CameraWrapper.CameraInfo();
      for (int i = 0; ; i++)
      {
        if (i >= this.m_numberOfCameras)
          break label466;
        CameraWrapper.getCameraInfo(i, localCameraInfo);
        if (localCameraInfo.facing == 1)
        {
          this.m_cameraId = 1;
          this.m_cameraMap[1] = i;
          break;
        }
      }
      label466: break;
      label468: this.m_switchCamera.setVisibility(8);
      break label242;
      label480: Log.e("Tango.RecordVideomailActivity", "Max recording duration MISSING or INVALID, check state machine. Default value will be used: 30000");
      break label342;
      label492: this.m_showAutoDismissTimerAlertAt = (this.m_maxRecordingDuration / 2);
      this.m_hideAutoDismissTimerAlertAt = (5000 + this.m_showAutoDismissTimerAlertAt);
      this.m_showPermanentTimerAlertAt = (this.m_maxRecordingDuration - 5000);
    }
  }

  protected void onDestroy()
  {
    Log.d("Tango.RecordVideomailActivity", "onDestroy()");
    super.onDestroy();
    cancelSend();
    this.m_handler.removeMessages(0);
    this.m_handler.removeMessages(2);
    this.m_handler.removeMessages(4);
    this.m_handler.removeMessages(5);
    ((TangoApp)getApplication()).setRecordVideomailActivityInstance(null);
  }

  public void onDetached(boolean paramBoolean)
  {
    Log.d("Tango.RecordVideomailActivity", "onDetached");
    if (paramBoolean)
    {
      this.m_send.setEnabled(true);
      this.m_cancel.setEnabled(true);
    }
    synchronized (this.m_videomailSenderLock)
    {
      this.m_sendHelper = null;
      return;
    }
  }

  public void onError(MediaRecorder paramMediaRecorder, int paramInt1, int paramInt2)
  {
    handleRecordingError(null);
    if (paramInt1 == 1)
    {
      Log.e("Tango.RecordVideomailActivity", "onError - Error received: MEDIA_RECORDER_ERROR_UNKNOWN - stopping recording");
      stopRecording();
    }
  }

  public void onInfo(MediaRecorder paramMediaRecorder, int paramInt1, int paramInt2)
  {
    if (paramInt1 == 800)
    {
      Log.w("Tango.RecordVideomailActivity", "onInfo: MEDIA_RECORDER_INFO_MAX_DURATION_REACHED - stopping recording");
      if (this.m_isRecording)
      {
        this.m_isMaxDurationReached = true;
        this.m_countdownTimer.stopAtMaxDuration();
        TextView localTextView = this.m_alertMessage;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = com.sgiggle.production.widget.Timer.formatTime(this.m_maxRecordingDuration);
        localTextView.setText(getString(2131296438, arrayOfObject));
        this.m_alertNotificaiton.setVisibility(0);
        onRecordFinished();
      }
    }
  }

  protected void onPause()
  {
    Log.d("Tango.RecordVideomailActivity", "onPause()");
    super.onPause();
    this.m_isPausing = true;
    if (this.m_isRecording)
      stopRecording(false);
    stopPrepareCameraThread();
    resetScreenOn();
    closeCamera();
    this.m_orientationListener.disable();
  }

  protected void onResume()
  {
    Log.d("Tango.RecordVideomailActivity", "onResume()");
    super.onResume();
    this.m_isPausing = false;
    this.m_orientationListener.enable();
    initVideoPreferences();
    resizeForPreviewAspectRatio();
    if ((this.m_prepareCameraThread == null) && (!prepareCamera()))
    {
      Log.w("Tango.RecordVideomailActivity", "onResume(): failed with preparing camera");
      return;
    }
    keepScreenOnAwhile();
  }

  public void onSendSuccess()
  {
  }

  public void onSizeChanged()
  {
  }

  public void onSmsDialogDismissed()
  {
  }

  public void onTimerStopped()
  {
    resetRecButtonLight();
  }

  public void onTimerUpdated(long paramLong1, long paramLong2)
  {
    boolean bool;
    if (paramLong2 > 0L)
    {
      if (paramLong1 >= 1000L)
      {
        this.m_handler.removeMessages(6);
        this.m_handler.removeMessages(7);
        this.m_handler.sendEmptyMessage(6);
        this.m_handler.sendEmptyMessageDelayed(7, 500L);
      }
      if (!this.m_alwaysShowTimerAlert)
        break label135;
      bool = true;
    }
    while (true)
      if (bool)
      {
        int i = (int)((999L + paramLong2) / 1000L);
        Resources localResources = getResources();
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(i);
        String str = localResources.getQuantityString(2131427333, i, arrayOfObject);
        this.m_alertMessage.setText(str);
        this.m_alertNotificaiton.setVisibility(0);
        return;
        label135: if (paramLong1 >= this.m_showAutoDismissTimerAlertAt)
        {
          if (paramLong1 < this.m_hideAutoDismissTimerAlertAt)
          {
            bool = SHOW_TIMER_AUTO_DISMISS_ALERT;
            continue;
          }
          if (paramLong1 >= this.m_showPermanentTimerAlertAt)
            bool = true;
        }
      }
      else
      {
        this.m_alertNotificaiton.setVisibility(8);
        return;
        bool = false;
      }
  }

  public void onUploadFailedFileNotFound()
  {
    Log.d("Tango.RecordVideomailActivity", "Upload failed, file not found.");
    Toast.makeText(this, 2131296441, 1).show();
  }

  public void onUploadPreExecute()
  {
    this.m_uploadingMessage.setText(2131296444);
  }

  public void onUploadProgress(int paramInt)
  {
    Log.d("Tango.RecordVideomailActivity", "Upload in progress " + paramInt + "%");
    this.m_fileUploadProgress.setProgress(paramInt);
  }

  public void onUploadRequested()
  {
    Log.d("Tango.RecordVideomailActivity", "Upload about to start...");
    this.m_alertNotificaiton.setVisibility(8);
    showFileUploader();
  }

  public void onUploadWillRetry(int paramInt)
  {
    Log.d("Tango.RecordVideomailActivity", "Upload failed, will retry in " + paramInt + "s");
    TextView localTextView = this.m_uploadingMessage;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    localTextView.setText(getString(2131296442, arrayOfObject));
  }

  public void onUserInteraction()
  {
    super.onUserInteraction();
    if (!this.m_isRecording)
      keepScreenOnAwhile();
  }

  protected void reviewVideoMail()
  {
    Intent localIntent = new Intent(this, ViewRecordedVideoActivity.class);
    localIntent.putExtra("videoFilename", this.m_currentFilename);
    localIntent.putExtra("videoThumbnailFilename", this.m_currentThumbnailFilename);
    localIntent.putExtra("rotateOnServer", RecorderHelper.isRotated());
    localIntent.putExtra("rotationHint", this.m_rotationHint);
    localIntent.putExtra("duration", getRecordDurationInSeconds());
    localIntent.putExtra("flipped", RecorderHelper.isFlipped());
    localIntent.putExtra("mimeType", "video/mp4");
    localIntent.putExtra("timeCreated", System.currentTimeMillis() / 1000L);
    localIntent.putExtra("maxDurationReached", this.m_isMaxDurationReached);
    localIntent.putExtra("maxDuration", this.m_maxRecordingDuration);
    MediaEngineMessage.RecordVideoMailEvent localRecordVideoMailEvent = (MediaEngineMessage.RecordVideoMailEvent)getFirstMessage();
    if (localRecordVideoMailEvent != null);
    for (SessionMessages.RecordVideoMailPayload localRecordVideoMailPayload = (SessionMessages.RecordVideoMailPayload)localRecordVideoMailEvent.payload(); ; localRecordVideoMailPayload = null)
    {
      localIntent.putExtra("recordVideoMailPayload", localRecordVideoMailPayload);
      localIntent.addFlags(262144);
      this.m_isReviewInProgress = true;
      startActivityForResult(localIntent, 1);
      return;
    }
  }

  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramSurfaceHolder.getSurface() == null)
      Log.d("Tango.RecordVideomailActivity", "holder.getSurface() == null");
    do
    {
      return;
      this.m_surfaceHolder = paramSurfaceHolder;
    }
    while ((this.m_isPausing) || (this.m_cameraDevice == null));
    if (paramSurfaceHolder.isCreating())
    {
      setPreviewDisplay(paramSurfaceHolder);
      return;
    }
    restartPreview();
  }

  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
  }

  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    this.m_surfaceHolder = null;
  }

  protected void updateRecordUi(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.m_send.setEnabled(false);
      this.m_cancel.setEnabled(false);
      return;
    }
    this.m_recordButtonTimer.cancel();
    this.m_recordButtonTimer = new java.util.Timer();
    resetRecButtonLight();
    this.m_send.setEnabled(true);
    this.m_cancel.setEnabled(true);
  }

  private class CameraSizeComparator
    implements Comparator<Camera.Size>
  {
    private CameraSizeComparator()
    {
    }

    public int compare(Camera.Size paramSize1, Camera.Size paramSize2)
    {
      return paramSize1.width * paramSize1.height - paramSize2.width * paramSize2.height;
    }
  }

  private class MainHandler extends Handler
  {
    private MainHandler()
    {
    }

    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      case 1:
      case 3:
      default:
        Log.v("Tango.RecordVideomailActivity", "Unhandled message: " + paramMessage.what);
        return;
      case 0:
        RecordVideomailActivity.this.getWindow().clearFlags(128);
        return;
      case 2:
        RecordVideomailActivity.this.prepareCamera();
        return;
      case 4:
        if (RecordVideomailActivity.this.m_numberOfCameras > 1)
          RecordVideomailActivity.this.m_switchCamera.setVisibility(0);
        RecordVideomailActivity.this.updateRecordUi(false);
        return;
      case 5:
        RecordVideomailActivity.this.keepScreenOnAwhile();
        return;
      case 8:
        RecordVideomailActivity.this.m_progressView.setVisibility(8);
        RecordVideomailActivity.this.m_surfaceView.setVisibility(0);
        RecordVideomailActivity.this.m_record.setEnabled(true);
        if (RecordVideomailActivity.this.m_numberOfCameras > 1)
          RecordVideomailActivity.this.m_switchCamera.setVisibility(0);
        RecordVideomailActivity.this.stopPrepareCameraThread();
        return;
      case 6:
        RecordVideomailActivity.this.setRecButtonLightOn(true);
        return;
      case 7:
      }
      RecordVideomailActivity.this.setRecButtonLightOn(false);
    }
  }

  class PrepareCameraThread extends Thread
  {
    private boolean m_stopPreviewThread = false;

    PrepareCameraThread()
    {
      super();
    }

    public void requestExitAndWait()
    {
      this.m_stopPreviewThread = true;
      try
      {
        join();
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        Log.w("Tango.RecordVideomailActivity", "requestExitAndWait() failed: " + localInterruptedException);
      }
    }

    public void run()
    {
      try
      {
        if (!this.m_stopPreviewThread)
          RecordVideomailActivity.this.openCamera(RecordVideomailActivity.this.m_cameraMap[RecordVideomailActivity.this.m_cameraId]);
        while (!this.m_stopPreviewThread)
        {
          RecordVideomailActivity.this.getIdealResolution();
          RecordVideomailActivity.this.runOnUiThread(new Runnable()
          {
            public void run()
            {
              RecordVideomailActivity.this.resizeForPreviewAspectRatio();
            }
          });
          RecordVideomailActivity.this.setPreviewDisplay(RecordVideomailActivity.this.m_surfaceHolder);
          if (Build.VERSION.SDK_INT > 7)
          {
            RecordVideomailActivity.this.calcCameraDisplayOrientation();
            if ((RecordVideomailActivity.isGingerbreadOrHigher) || ((Build.VERSION.SDK_INT == 8) && (RecordVideomailActivity.this.m_cameraDisplayOrientation != 0)))
              RecordVideomailActivity.this.m_cameraDevice.setDisplayOrientation(RecordVideomailActivity.this.m_cameraDisplayOrientation);
          }
          RecordVideomailActivity.this.setCameraParameters();
          RecordVideomailActivity.this.m_cameraDevice.startPreview();
          RecordVideomailActivity.this.m_handler.sendEmptyMessage(8);
          Log.d("Tango.RecordVideomailActivity", "Camera opened and preview started successfully");
          return;
          Log.i("Tango.RecordVideomailActivity", "PrepareCameraThread is requested to stop running");
        }
      }
      catch (Throwable localThrowable)
      {
        RecordVideomailActivity.this.closeCamera();
        Log.w("Tango.RecordVideomailActivity", "PrepareCameraThrea.run() failed: " + localThrowable);
        return;
      }
      Log.i("Tango.RecordVideomailActivity", "PrepareCameraThread is requested to stop running");
    }
  }

  private class RecorderOrientationListener extends OrientationEventListener
  {
    public RecorderOrientationListener(Context arg2)
    {
      super();
    }

    public void enable()
    {
      super.enable();
      onOrientationChanged(0);
    }

    public void onOrientationChanged(int paramInt)
    {
      if (RecordVideomailActivity.this.m_isRecording);
      int i;
      do
      {
        do
          return;
        while (paramInt == -1);
        RecordVideomailActivity.access$1402(RecordVideomailActivity.this, 90 * ((paramInt + 45) / 90) % 360);
        i = RecordVideomailActivity.this.m_orientation + RecordVideomailActivity.this.getDisplayRotation();
      }
      while (RecordVideomailActivity.this.m_orientationCompensation == i);
      RecordVideomailActivity.access$1602(RecordVideomailActivity.this, i);
      RecordVideomailActivity.this.m_switchCamera.setDegree(RecordVideomailActivity.this.m_orientationCompensation);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.RecordVideomailActivity
 * JD-Core Version:    0.6.2
 */