package com.sgiggle.production.screens.videomail;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.CancelVideoMailReceiversSelectionMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ConfirmVideoMailNonTangoNotificationMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DismissUploadVideoMailFinishedMessage;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayVideoMailNonTangoNotificationEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayVideoMailReceiversEvent;
import com.sgiggle.media_engine.MediaEngineMessage.UploadVideoMailFinishedEvent;
import com.sgiggle.media_engine.MediaEngineMessage.VideoMailReceiversMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.ContactSelectionActivity;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.Utils;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.VideoMailNonTangoNotificationPayload;
import com.sgiggle.xmpp.SessionMessages.VideoMailReceiversPayload;
import java.util.ArrayList;
import java.util.List;

public class SendVideomailActivityHelper
  implements VideomailUploader.VideomailUploaderListener
{
  private static final int REQUEST_CODE_SEND_SMS = 10;
  private static final String TAG = "SendVideomailActivityHelper";
  private Activity m_activity;
  private List<SessionMessages.Contact> m_callees;
  private Context m_context;
  private int m_duration;
  private long m_fileSize;
  private String m_filename;
  private boolean m_flipped;
  private boolean m_fullscreen;
  private boolean m_isSelectingContacts = false;
  private SendVideomailActivityHelperListener m_listener;
  private boolean m_lockInPortrait;
  private String m_mimeType;
  private List<SessionMessages.Contact> m_nonTangoContactsSentTo;
  private int m_rotationHint;
  private boolean m_showMessageOnSent = true;
  private String m_thumbnailFilename;
  private long m_timeCreated;
  private String m_videomailId;
  private VideomailUploader m_videomailUploader;

  public SendVideomailActivityHelper(Activity paramActivity, String paramString1, String paramString2, List<SessionMessages.Contact> paramList, int paramInt1, String paramString3, long paramLong1, long paramLong2, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, SendVideomailActivityHelperListener paramSendVideomailActivityHelperListener)
  {
    this.m_context = paramActivity;
    this.m_activity = paramActivity;
    this.m_filename = paramString1;
    this.m_thumbnailFilename = paramString2;
    this.m_callees = paramList;
    this.m_duration = paramInt1;
    this.m_mimeType = paramString3;
    this.m_fileSize = paramLong1;
    this.m_timeCreated = paramLong2;
    this.m_rotationHint = paramInt2;
    this.m_flipped = paramBoolean1;
    this.m_fullscreen = paramBoolean2;
    this.m_lockInPortrait = paramBoolean3;
    this.m_listener = paramSendVideomailActivityHelperListener;
  }

  private void cancelUpload()
  {
    if (this.m_videomailUploader != null)
      this.m_videomailUploader.cancel();
    if (this.m_isSelectingContacts)
    {
      postCancelVideoMailReceiversSelectionMessage();
      this.m_isSelectingContacts = false;
    }
    this.m_videomailUploader = null;
    this.m_videomailId = null;
    ((TangoApp)this.m_activity.getApplication()).setVideomailStateMachineUploadListener(null);
  }

  private void detachListener(boolean paramBoolean)
  {
    if (this.m_listener != null)
    {
      SendVideomailActivityHelperListener localSendVideomailActivityHelperListener = this.m_listener;
      this.m_listener = null;
      localSendVideomailActivityHelperListener.onDetached(paramBoolean);
    }
  }

  private void handleDisplayVideoMailNonTangoNotificationEvent(MediaEngineMessage.DisplayVideoMailNonTangoNotificationEvent paramDisplayVideoMailNonTangoNotificationEvent)
  {
    this.m_nonTangoContactsSentTo = ((SessionMessages.VideoMailNonTangoNotificationPayload)paramDisplayVideoMailNonTangoNotificationEvent.payload()).getCalleesList();
    String str1 = ((SessionMessages.VideoMailNonTangoNotificationPayload)paramDisplayVideoMailNonTangoNotificationEvent.payload()).getNonTangoUrl();
    final String str2 = Utils.getSmsAddressListFromContactList(this.m_nonTangoContactsSentTo);
    final String str3 = this.m_context.getResources().getString(2131296430, new Object[] { str1 });
    Dialog localDialog = VideomailUtils.createSendBySmsWarningDialog(this.m_context);
    if (localDialog == null)
    {
      sendSms(str2, str3, 10);
      return;
    }
    if (this.m_listener != null)
      this.m_listener.onBeforeSmsDialogShown();
    localDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
    {
      public void onDismiss(DialogInterface paramAnonymousDialogInterface)
      {
        SendVideomailActivityHelper.this.sendSms(str2, str3, 10);
        if (SendVideomailActivityHelper.this.m_listener != null)
          SendVideomailActivityHelper.this.m_listener.onSmsDialogDismissed();
      }
    });
    localDialog.show();
  }

  private void handleUploadVideoMailFinishedEvent(MediaEngineMessage.UploadVideoMailFinishedEvent paramUploadVideoMailFinishedEvent)
  {
    postDismissUploadVideoMailFinishedMessage();
    if (this.m_showMessageOnSent)
      Toast.makeText(this.m_context, 2131296439, 1).show();
    if (this.m_listener != null)
      this.m_listener.onSendSuccess();
    detachListener(false);
  }

  private void postCancelVideoMailReceiversSelectionMessage()
  {
    MediaEngineMessage.CancelVideoMailReceiversSelectionMessage localCancelVideoMailReceiversSelectionMessage = new MediaEngineMessage.CancelVideoMailReceiversSelectionMessage();
    MessageRouter.getInstance().postMessage("jingle", localCancelVideoMailReceiversSelectionMessage);
  }

  private void postConfirmVideoMailNonTangoNotificationMessage(boolean paramBoolean)
  {
    MediaEngineMessage.ConfirmVideoMailNonTangoNotificationMessage localConfirmVideoMailNonTangoNotificationMessage = new MediaEngineMessage.ConfirmVideoMailNonTangoNotificationMessage(this.m_videomailId, this.m_nonTangoContactsSentTo, paramBoolean);
    MessageRouter.getInstance().postMessage("jingle", localConfirmVideoMailNonTangoNotificationMessage);
  }

  private void postDismissUploadVideoMailFinishedMessage()
  {
    MediaEngineMessage.DismissUploadVideoMailFinishedMessage localDismissUploadVideoMailFinishedMessage = new MediaEngineMessage.DismissUploadVideoMailFinishedMessage(this.m_videomailId);
    MessageRouter.getInstance().postMessage("jingle", localDismissUploadVideoMailFinishedMessage);
  }

  private void postVideoMailReceiversMessage()
  {
    MediaEngineMessage.VideoMailReceiversMessage localVideoMailReceiversMessage = new MediaEngineMessage.VideoMailReceiversMessage(ContactSelectionActivity.getSupportedMediaType(this.m_context));
    MessageRouter.getInstance().postMessage("jingle", localVideoMailReceiversMessage);
  }

  private void sendSms(String paramString1, String paramString2, int paramInt)
  {
    Intent localIntent = Utils.getSmsIntent(paramString1, paramString2);
    try
    {
      this.m_activity.startActivityForResult(localIntent, paramInt);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Log.w("SendVideomailActivityHelper", "Not activity was found for ACTION_VIEW (for sending SMS)");
    }
  }

  private void startUpload(List<SessionMessages.Contact> paramList)
  {
    if (this.m_listener != null)
      this.m_listener.onUploadRequested();
    this.m_videomailUploader = new VideomailUploader(this.m_context, ((SessionMessages.Contact)paramList.get(0)).getAccountid(), this.m_filename, this.m_thumbnailFilename, paramList, this.m_duration, this.m_mimeType, this.m_fileSize, this.m_timeCreated, this.m_rotationHint, this.m_flipped, this);
    ((TangoApp)this.m_activity.getApplication()).setVideomailStateMachineUploadListener(this.m_videomailUploader);
    this.m_videomailUploader.start();
  }

  public void cancel()
  {
    cancelUpload();
    detachListener(true);
  }

  public void handleMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
    case 35161:
      do
        return;
      while (this.m_isSelectingContacts);
      this.m_isSelectingContacts = true;
      ContactSelectionActivity.setLatestContactList(((SessionMessages.VideoMailReceiversPayload)((MediaEngineMessage.DisplayVideoMailReceiversEvent)paramMessage).payload()).getCalleesList());
      Intent localIntent = new Intent(this.m_activity, VideomailContactSelectionActivity.class);
      localIntent.putExtra("FullScreen", this.m_fullscreen);
      localIntent.putExtra("LockInPortrait", this.m_lockInPortrait);
      localIntent.addFlags(262144);
      this.m_activity.startActivityForResult(localIntent, 20);
      return;
    case 35162:
      handleDisplayVideoMailNonTangoNotificationEvent((MediaEngineMessage.DisplayVideoMailNonTangoNotificationEvent)paramMessage);
      return;
    case 35173:
    }
    handleUploadVideoMailFinishedEvent((MediaEngineMessage.UploadVideoMailFinishedEvent)paramMessage);
  }

  public boolean handleOnActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 20)
    {
      switch (paramInt2)
      {
      default:
      case -1:
      case 0:
      }
      while (true)
      {
        this.m_isSelectingContacts = false;
        return true;
        startUpload((ArrayList)paramIntent.getSerializableExtra("SelectedContacts"));
        continue;
        postCancelVideoMailReceiversSelectionMessage();
        detachListener(true);
      }
    }
    if (paramInt1 == 10)
    {
      this.m_showMessageOnSent = false;
      postConfirmVideoMailNonTangoNotificationMessage(true);
      return true;
    }
    return false;
  }

  public void onUploadDetached()
  {
    Log.d("SendVideomailActivityHelper", "onUploadDetached");
  }

  public void onUploadFailedFileNotFound()
  {
    if (this.m_listener != null)
      this.m_listener.onUploadFailedFileNotFound();
  }

  public void onUploadPreExecute()
  {
    if (this.m_listener != null)
      this.m_listener.onUploadPreExecute();
  }

  public void onUploadProgress(int paramInt)
  {
    if (this.m_listener != null)
      this.m_listener.onUploadProgress(paramInt);
  }

  public void onUploadSuccess()
  {
    Log.d("SendVideomailActivityHelper", "Upload succeeded!");
  }

  public void onUploadVideoMailIdReceived(String paramString)
  {
    this.m_videomailId = paramString;
  }

  public void onUploadWillRetry(int paramInt)
  {
    if (this.m_listener != null)
      this.m_listener.onUploadWillRetry(paramInt);
  }

  public void send()
  {
    cancelUpload();
    this.m_showMessageOnSent = true;
    if ((this.m_callees == null) || (this.m_callees.size() == 0))
    {
      postVideoMailReceiversMessage();
      return;
    }
    startUpload(this.m_callees);
  }

  public static abstract interface SendVideomailActivityHelperListener
  {
    public abstract void onBeforeSmsDialogShown();

    public abstract void onDetached(boolean paramBoolean);

    public abstract void onSendSuccess();

    public abstract void onSmsDialogDismissed();

    public abstract void onUploadFailedFileNotFound();

    public abstract void onUploadPreExecute();

    public abstract void onUploadProgress(int paramInt);

    public abstract void onUploadRequested();

    public abstract void onUploadWillRetry(int paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.SendVideomailActivityHelper
 * JD-Core Version:    0.6.2
 */