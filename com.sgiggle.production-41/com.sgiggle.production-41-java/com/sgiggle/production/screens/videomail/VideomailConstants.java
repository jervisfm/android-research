package com.sgiggle.production.screens.videomail;

public class VideomailConstants
{
  public static final String FORWARD_VIDEO_MAIL_OPERATION = "ForwardVideoMail";
  public static final String STATE_MACHINE_FOLDER_ID_OUTBOX = "_outbox";
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.VideomailConstants
 * JD-Core Version:    0.6.2
 */