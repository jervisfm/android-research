package com.sgiggle.production.screens.videomail;

import android.content.res.Resources;
import com.sgiggle.production.ContactSelectionActivity;

public class VideomailContactSelectionActivity extends ContactSelectionActivity
{
  public static final int CONTACT_SELECTION_LIMIT = 200;

  protected String getOkButtonString(int paramInt)
  {
    if (paramInt == 0)
      return getResources().getString(2131296453);
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    return localResources.getQuantityString(2131427334, paramInt, arrayOfObject);
  }

  protected int getSelectionLimit()
  {
    return 200;
  }

  protected String getSelectionLimitReachedString()
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(getSelectionLimit());
    return getString(2131296454, arrayOfObject);
  }

  protected String getTitleString()
  {
    return getResources().getString(2131296452);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.VideomailContactSelectionActivity
 * JD-Core Version:    0.6.2
 */