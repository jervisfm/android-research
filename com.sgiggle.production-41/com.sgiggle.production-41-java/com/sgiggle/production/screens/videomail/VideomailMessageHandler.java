package com.sgiggle.production.screens.videomail;

import android.content.Context;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.VideoMailResultEvent;
import com.sgiggle.messaging.Message;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.OperationErrorPayload;

public class VideomailMessageHandler
{
  private static final String TAG = "Tango.VideomailMessageHandler";

  public static boolean handleMessage(Context paramContext, Message paramMessage)
  {
    if ((paramContext == null) || (paramMessage == null))
    {
      Log.e("Tango.VideomailMessageHandler", "handleMessage: invalid parameters.");
      return false;
    }
    switch (paramMessage.getType())
    {
    default:
      return false;
    case 35155:
    }
    MediaEngineMessage.VideoMailResultEvent localVideoMailResultEvent = (MediaEngineMessage.VideoMailResultEvent)paramMessage;
    if ((localVideoMailResultEvent == null) || (localVideoMailResultEvent.payload() == null))
    {
      Log.e("Tango.VideomailMessageHandler", "handleVideomailError: VideoMailResultEvent invalid.");
      return false;
    }
    SessionMessages.OperationErrorPayload localOperationErrorPayload = (SessionMessages.OperationErrorPayload)localVideoMailResultEvent.payload();
    String str1;
    String str2;
    boolean bool;
    if (localOperationErrorPayload.hasReason())
    {
      str1 = localOperationErrorPayload.getReason();
      if (!localOperationErrorPayload.hasOperation())
        break label176;
      str2 = localOperationErrorPayload.getOperation();
      if (!"ForwardVideoMail".equals(str2))
        break label186;
      Toast.makeText(paramContext, 2131296443, 0).show();
      bool = true;
    }
    while (true)
    {
      Log.e("Tango.VideomailMessageHandler", "handleVideomailError: Error received. Operation: " + str2 + " Reason: " + str1);
      return bool;
      str1 = "UNKNOWN";
      break;
      label176: str2 = "UNKNOWN";
      bool = false;
      continue;
      label186: bool = false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.VideomailMessageHandler
 * JD-Core Version:    0.6.2
 */