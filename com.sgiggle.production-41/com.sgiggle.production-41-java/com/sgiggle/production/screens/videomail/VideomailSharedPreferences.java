package com.sgiggle.production.screens.videomail;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class VideomailSharedPreferences
{
  private static final boolean FORCE_VIDEOMAIL_REPLAY_DEFAULT = false;
  private static final String KEY_FORCE_VIDEOMAIL_REPLAY = "KEY_FORCE_VIDEOMAIL_REPLAY";
  private static final String KEY_SHOW_SEND_BY_SMS_WARNING_DIALOG = "SHOW_SEND_BY_SMS_WARNING_DIALOG";
  private static final String SHARED_PREFERENCES_NAME = "com.sgiggle.production.screens.videomail.VideomailSharedPreferences";
  private static final boolean SHOW_SEND_BY_SMS_WARNING_DIALOG_DEFAULT = true;

  private static SharedPreferences.Editor getEditor(Context paramContext)
  {
    return getSharedPreferences(paramContext).edit();
  }

  public static boolean getForceVideomailReplay(Context paramContext)
  {
    return getSharedPreferences(paramContext).getBoolean("KEY_FORCE_VIDEOMAIL_REPLAY", false);
  }

  private static SharedPreferences getSharedPreferences(Context paramContext)
  {
    return paramContext.getSharedPreferences("com.sgiggle.production.screens.videomail.VideomailSharedPreferences", 0);
  }

  public static boolean getShowSendBySmsWarningDialog(Context paramContext)
  {
    return getSharedPreferences(paramContext).getBoolean("SHOW_SEND_BY_SMS_WARNING_DIALOG", true);
  }

  public static void setForceVideomailReplay(Context paramContext, boolean paramBoolean)
  {
    getEditor(paramContext).putBoolean("KEY_FORCE_VIDEOMAIL_REPLAY", paramBoolean).commit();
  }

  public static void setShowSendBySmsWarningDialog(Context paramContext, boolean paramBoolean)
  {
    getEditor(paramContext).putBoolean("SHOW_SEND_BY_SMS_WARNING_DIALOG", paramBoolean).commit();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.VideomailSharedPreferences
 * JD-Core Version:    0.6.2
 */