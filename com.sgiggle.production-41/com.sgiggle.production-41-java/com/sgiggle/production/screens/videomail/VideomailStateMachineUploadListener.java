package com.sgiggle.production.screens.videomail;

import com.sgiggle.messaging.Message;

public abstract interface VideomailStateMachineUploadListener
{
  public abstract void onUploadTokenReceived(Message paramMessage);

  public abstract void onVideomailErrorEvent(Message paramMessage);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.VideomailStateMachineUploadListener
 * JD-Core Version:    0.6.2
 */