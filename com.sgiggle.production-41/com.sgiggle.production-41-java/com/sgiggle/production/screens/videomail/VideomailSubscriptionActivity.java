package com.sgiggle.production.screens.videomail;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayProductCatalogEvent;
import com.sgiggle.media_engine.MediaEngineMessage.ProductCatalogFinishedMessage;
import com.sgiggle.media_engine.MediaEngineMessage.VMailAttemptPurchaseTrackingMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.ActivityBase;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.adapter.PaymentAdapter;
import com.sgiggle.production.adapter.PaymentAdapter.PurchaseListener;
import com.sgiggle.production.database.PurchaseDatabase;
import com.sgiggle.production.model.Product;
import com.sgiggle.production.model.Product.Managed;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.production.payments.Constants.ResponseCode;
import com.sgiggle.production.payments.PurchaseUtils;
import com.sgiggle.production.payments.ResponseHandler;
import com.sgiggle.production.payments.ResponseHandler.PurchaseObserver;
import com.sgiggle.production.service.BillingService;
import com.sgiggle.production.service.BillingService.BillingServiceBinder;
import com.sgiggle.production.service.BillingService.RequestPurchase;
import com.sgiggle.production.service.BillingService.RestoreTransactions;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogEntry;
import com.sgiggle.xmpp.SessionMessages.ProductCatalogPayload;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class VideomailSubscriptionActivity extends ActivityBase
  implements ServiceConnection, View.OnClickListener
{
  private static final String DB_INITIALIZED = "db_initialized";
  private static final int DIALOG_BILLING_NOT_SUPPORTED = 1;
  private static final int DIALOG_PURCHASE_FAILED = 0;
  public static final String KEY_BUNDLE = "com.sgiggle.production.screens.videomail.BUNDLE";
  private static final int MSG_SHOW_DIALOG_BILLING_NOT_SUPPORTED = 2;
  private static final int MSG_SHOW_DIALOG_PURCHASE_FAILED = 1;
  private static final int MSG_UPDATE_PRODUCT_CATALOG = 0;
  private static final String TAG = "Tango.VideomailSubscriptionActivity";
  private static PaymentAdapter m_adapter;
  private static ArrayList<Product> m_catalog = new ArrayList();
  private BillingService m_billingService;
  private Handler m_handler = new Handler()
  {
    public void handleMessage(android.os.Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage.what == 0)
      {
        if (VideomailSubscriptionActivity.m_adapter != null)
        {
          VideomailSubscriptionActivity.m_adapter.clear();
          if (((paramAnonymousMessage.obj instanceof List)) && (paramAnonymousMessage.obj != null))
          {
            Iterator localIterator = ((List)paramAnonymousMessage.obj).iterator();
            while (localIterator.hasNext())
            {
              Product localProduct = (Product)localIterator.next();
              VideomailSubscriptionActivity.m_adapter.add(localProduct);
            }
          }
        }
      }
      else
      {
        if (paramAnonymousMessage.what != 1)
          break label97;
        VideomailSubscriptionActivity.this.showDialog(0);
      }
      label97: 
      while ((paramAnonymousMessage.what != 2) || (VideomailSubscriptionActivity.this.m_isDialogBillingNotSupportedShowing))
        return;
      VideomailSubscriptionActivity.access$102(VideomailSubscriptionActivity.this, true);
      VideomailSubscriptionActivity.this.showDialog(1);
    }
  };
  private boolean m_isBillingServiceBound;
  private boolean m_isDialogBillingNotSupportedShowing = false;
  private BillingPurchaseObserver m_observer;
  private PurchaseDatabase m_purchaseDatabase;
  private TextView m_termosOfService;
  private UpdateItems m_updateItems = new UpdateItems(null);

  private static ArrayList<Product> getProductCatalog(com.sgiggle.messaging.Message paramMessage)
  {
    if (paramMessage == null)
    {
      Log.w("Tango.VideomailSubscriptionActivity", "Event is null");
      return null;
    }
    if (!(paramMessage instanceof MediaEngineMessage.DisplayProductCatalogEvent))
    {
      Log.w("Tango.VideomailSubscriptionActivity", "Event is not instance of DisplayProductCatalogEvent");
      return null;
    }
    SessionMessages.ProductCatalogPayload localProductCatalogPayload = (SessionMessages.ProductCatalogPayload)((MediaEngineMessage.DisplayProductCatalogEvent)paramMessage).payload();
    if (localProductCatalogPayload == null)
    {
      Log.e("Tango.VideomailSubscriptionActivity", "Payload is null");
      return null;
    }
    int i = localProductCatalogPayload.getEntryCount();
    Log.d("Tango.VideomailSubscriptionActivity", "Received " + localProductCatalogPayload.getEntryCount() + " products to sell");
    ArrayList localArrayList = new ArrayList();
    List localList = localProductCatalogPayload.getEntryList();
    for (int j = 0; j < i; j++)
    {
      SessionMessages.ProductCatalogEntry localProductCatalogEntry = (SessionMessages.ProductCatalogEntry)localList.get(j);
      Product localProduct = new Product();
      localProduct.setManaged(Product.Managed.MANAGED);
      localProduct.setProductMarketId(localProductCatalogEntry.getProductMarketId());
      localProduct.setProductId(localProductCatalogEntry.getProductId());
      localProduct.setSku(localProductCatalogEntry.getSKU());
      localProduct.setProductName(localProductCatalogEntry.getProductName());
      localProduct.setProductDescription(localProductCatalogEntry.getProductDescription());
      localProduct.setCategoryKey(localProductCatalogEntry.getCategoryKey());
      localProduct.setCategory(localProductCatalogEntry.getCategory());
      localProduct.setMarketId(localProductCatalogEntry.getMarketId());
      localProduct.setBeginTime(localProductCatalogEntry.getBeginTime());
      localProduct.setEndTime(localProductCatalogEntry.getEndTime());
      localProduct.setLeaseDuration(localProductCatalogEntry.getLeaseDuration());
      localProduct.setPurchaseState(Constants.PurchaseState.PURCHASE);
      localProduct.setPrice(localProductCatalogEntry.getPrice());
      localArrayList.add(localProduct);
    }
    return localArrayList;
  }

  private void restoreDatabase()
  {
    if (!getPreferences(0).getBoolean("db_initialized", false))
      this.m_billingService.restoreTransactions();
  }

  public static void updateProductCatalog(com.sgiggle.messaging.Message paramMessage)
  {
    Log.d("Tango.VideomailSubscriptionActivity", "Add products in to adapter");
    ArrayList localArrayList = getProductCatalog(paramMessage);
    if (localArrayList != null)
    {
      m_catalog.clear();
      m_catalog.addAll(localArrayList);
      Collections.sort(m_catalog);
      if (m_adapter != null)
        m_adapter.notifyDataSetChanged();
      Log.d("Tango.VideomailSubscriptionActivity", "Added " + localArrayList.size() + " products");
    }
  }

  public void onClick(View paramView)
  {
    if (paramView == this.m_termosOfService)
    {
      Intent localIntent = new Intent("android.intent.action.VIEW");
      localIntent.setData(Uri.parse("http://www.tango.me/terms-of-use/"));
      startActivity(localIntent);
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    setContentView(2130903149);
    this.m_termosOfService = ((TextView)findViewById(2131362170));
    this.m_termosOfService.setOnClickListener(this);
    this.m_observer = new BillingPurchaseObserver(null);
    this.m_purchaseDatabase = new PurchaseDatabase(this);
    ListView localListView = (ListView)findViewById(2131362169);
    m_adapter = new PaymentAdapter(this, m_catalog, new ItemPurchaseListener(null));
    localListView.setAdapter(m_adapter);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      Log.w("Tango.VideomailSubscriptionActivity", "Unable to create dialog. Invalid ID: " + paramInt);
      return super.onCreateDialog(paramInt);
    case 0:
      return PurchaseUtils.getPurchaseFailedDialog(this);
    case 1:
    }
    return PurchaseUtils.getBillingNotSupportedDialog(this);
  }

  protected void onDestroy()
  {
    this.m_purchaseDatabase.close();
    if (this.m_isBillingServiceBound)
    {
      unbindService(this);
      this.m_isBillingServiceBound = false;
    }
    ((TangoApp)getApplication()).getResponseHandler().unregisterObserver(this.m_observer);
    this.m_handler = null;
    super.onDestroy();
  }

  protected void onPause()
  {
    super.onPause();
    if (isFinishing())
    {
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ProductCatalogFinishedMessage());
      if (this.m_isBillingServiceBound)
      {
        unbindService(this);
        this.m_isBillingServiceBound = false;
      }
    }
  }

  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    this.m_billingService = ((BillingService.BillingServiceBinder)paramIBinder).getService();
    boolean bool = this.m_billingService.checkBillingSupported();
    Log.d("Tango.VideomailSubscriptionActivity", "Billing Supported: " + bool);
    if ((!bool) && (this.m_handler != null))
    {
      android.os.Message localMessage = this.m_handler.obtainMessage(2);
      this.m_handler.sendMessage(localMessage);
    }
    m_adapter.enableAllItems(bool);
  }

  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    this.m_billingService = null;
  }

  protected void onStart()
  {
    super.onStart();
    ((TangoApp)getApplication()).getResponseHandler().registerObserver(this.m_observer);
    if (!this.m_isBillingServiceBound)
      bindService(new Intent(this, BillingService.class), this, 1);
    this.m_isBillingServiceBound = true;
  }

  private class BillingPurchaseObserver
    implements ResponseHandler.PurchaseObserver
  {
    private BillingPurchaseObserver()
    {
    }

    public void onBillingSupported(boolean paramBoolean)
    {
      if (paramBoolean)
        VideomailSubscriptionActivity.this.restoreDatabase();
      while (true)
      {
        VideomailSubscriptionActivity.m_adapter.enableAllItems(paramBoolean);
        return;
        Log.w("Tango.VideomailSubscriptionActivity", "Billing is not supported");
        if (VideomailSubscriptionActivity.this.m_handler != null)
        {
          android.os.Message localMessage = VideomailSubscriptionActivity.this.m_handler.obtainMessage(2);
          VideomailSubscriptionActivity.this.m_handler.sendMessage(localMessage);
        }
      }
    }

    public void onResponseCodeReceived(BillingService.RequestPurchase paramRequestPurchase, Constants.ResponseCode paramResponseCode)
    {
      Log.i("Tango.VideomailSubscriptionActivity", "Purchase response received");
      if (paramResponseCode == Constants.ResponseCode.RESULT_OK)
      {
        Log.i("Tango.VideomailSubscriptionActivity", "Purchase was successfully sent to the server");
        if (VideomailSubscriptionActivity.m_adapter != null)
          VideomailSubscriptionActivity.m_adapter.updateItemStatus(paramRequestPurchase.m_productId, Constants.PurchaseState.PENDING);
      }
      do
      {
        do
        {
          return;
          if (paramResponseCode != Constants.ResponseCode.RESULT_USER_CANCELED)
            break;
          Log.i("Tango.VideomailSubscriptionActivity", "Purchase cancelled by user");
        }
        while (VideomailSubscriptionActivity.m_adapter == null);
        VideomailSubscriptionActivity.m_adapter.updateItemStatus(paramRequestPurchase.m_productId, Constants.PurchaseState.CANCELED);
        return;
        Log.i("Tango.VideomailSubscriptionActivity", "Purchase failed");
        if (VideomailSubscriptionActivity.m_adapter != null)
          VideomailSubscriptionActivity.m_adapter.updateItemStatus(paramRequestPurchase.m_productId, Constants.PurchaseState.PURCHASE);
      }
      while (VideomailSubscriptionActivity.this.m_handler == null);
      android.os.Message localMessage = VideomailSubscriptionActivity.this.m_handler.obtainMessage(1);
      VideomailSubscriptionActivity.this.m_handler.sendMessage(localMessage);
    }

    public void onResponseCodeReceived(BillingService.RestoreTransactions paramRestoreTransactions, Constants.ResponseCode paramResponseCode)
    {
      Log.i("Tango.VideomailSubscriptionActivity", "Restore Transactions response received");
      if (paramResponseCode == Constants.ResponseCode.RESULT_OK)
      {
        SharedPreferences.Editor localEditor = VideomailSubscriptionActivity.this.getPreferences(0).edit();
        localEditor.putBoolean("db_initialized", true);
        localEditor.commit();
        Log.i("Tango.VideomailSubscriptionActivity", "Completed restoring transactions");
        return;
      }
      Log.e("Tango.VideomailSubscriptionActivity", "Restore Transactions Error: " + paramResponseCode);
    }

    public void postPurchaseStateChange(Constants.PurchaseState paramPurchaseState, String paramString1, String paramString2, long paramLong, String paramString3, String paramString4, String paramString5)
    {
      if ((VideomailSubscriptionActivity.this.m_handler != null) && (VideomailSubscriptionActivity.this.m_updateItems != null))
      {
        VideomailSubscriptionActivity.this.m_updateItems.setParams(paramString1, paramPurchaseState);
        VideomailSubscriptionActivity.this.m_handler.post(VideomailSubscriptionActivity.this.m_updateItems);
      }
      if (paramPurchaseState == Constants.PurchaseState.PURCHASED)
        VideomailSubscriptionActivity.this.finish();
      while ((paramPurchaseState == Constants.PurchaseState.CANCELED) || (paramPurchaseState != Constants.PurchaseState.REFUNDED))
        return;
    }

    public void startBuyPageActivity(PendingIntent paramPendingIntent, Intent paramIntent)
    {
      Log.i("Tango.VideomailSubscriptionActivity", "Launching purchase activity");
      try
      {
        TangoApp.getInstance().skipWelcomePageOnce();
        VideomailSubscriptionActivity.this.startIntentSender(paramPendingIntent.getIntentSender(), paramIntent, 0, 0, 0);
        return;
      }
      catch (IntentSender.SendIntentException localSendIntentException)
      {
        Log.e("Tango.VideomailSubscriptionActivity", "Error starting activity", localSendIntentException);
      }
    }
  }

  private class ItemPurchaseListener
    implements PaymentAdapter.PurchaseListener
  {
    private ItemPurchaseListener()
    {
    }

    public void onPurchase(Product paramProduct)
    {
      Log.i("Tango.VideomailSubscriptionActivity", "Purchase item: " + paramProduct.getProductName() + " MarketId: " + paramProduct.getProductMarketId());
      MediaEngineMessage.VMailAttemptPurchaseTrackingMessage localVMailAttemptPurchaseTrackingMessage = new MediaEngineMessage.VMailAttemptPurchaseTrackingMessage(paramProduct.getProductMarketId());
      MessageRouter.getInstance().postMessage("jingle", localVMailAttemptPurchaseTrackingMessage);
      if (!VideomailSubscriptionActivity.this.m_billingService.requestPurchase(paramProduct.getProductMarketId(), null))
        Log.e("Tango.VideomailSubscriptionActivity", "Unable to purchase. Check if billing is supported");
    }
  }

  private class UpdateItems
    implements Runnable
  {
    private String m_itemId;
    private Constants.PurchaseState m_state;

    private UpdateItems()
    {
    }

    public void run()
    {
      if (VideomailSubscriptionActivity.m_adapter != null)
        VideomailSubscriptionActivity.m_adapter.updateItemStatus(this.m_itemId, this.m_state);
    }

    public void setParams(String paramString, Constants.PurchaseState paramPurchaseState)
    {
      this.m_itemId = paramString;
      this.m_state = paramPurchaseState;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.VideomailSubscriptionActivity
 * JD-Core Version:    0.6.2
 */