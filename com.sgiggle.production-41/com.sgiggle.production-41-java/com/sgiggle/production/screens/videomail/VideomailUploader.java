package com.sgiggle.production.screens.videomail;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Handler;
import com.sgiggle.media_engine.MediaEngineMessage.FinishUploadVideoMailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.SendConversationMessageMessage;
import com.sgiggle.media_engine.MediaEngineMessage.UploadVideoMailResponseEvent;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.model.VideomailResponse;
import com.sgiggle.production.network.CountingMultipartEntity.ProgressListener;
import com.sgiggle.production.network.RequestFactory;
import com.sgiggle.production.network.TangoHttpClient;
import com.sgiggle.production.network.command.UploadCommand;
import com.sgiggle.production.network.command.handler.HandlerException;
import com.sgiggle.production.network.command.handler.VideomailHandler;
import com.sgiggle.production.network.executor.RemoteExecutor;
import com.sgiggle.production.network.tango.TangoHttpConfiguration;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;
import com.sgiggle.xmpp.SessionMessages.UploadVideoMailResponsePayload;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.client.methods.HttpUriRequest;

public class VideomailUploader
  implements VideomailStateMachineUploadListener, CountingMultipartEntity.ProgressListener
{
  private static final int MSG_RETRY_UPLOAD = 1;
  private static final int MSG_RETRY_UPLOAD_TOKEN = 3;
  private static final String TAG = "VideomailUploader";
  private static final int UPLOAD_RETRY_BACKOFF = 5000;
  private Context m_context;
  private String m_conversationId = null;
  private int m_duration = 0;
  private long m_fileSize = 0L;
  private String m_filename;
  private final Handler m_handler = new MainHandler(null);
  private boolean m_isFinished = false;
  private VideomailUploaderListener m_listener;
  private int m_retryTimeout = 0;
  private int m_retryUploadTokenTimeout = 0;
  private int m_rotationHint;
  private String m_thumbnailFilename;
  private MediaEngineMessage.SendConversationMessageMessage m_uploadVideoMailMessage;
  private HttpUriRequest m_uriRequest;
  private String m_videomailId;
  private String m_videomailUrl;

  public VideomailUploader(Context paramContext, String paramString1, String paramString2, String paramString3, List<SessionMessages.Contact> paramList, int paramInt1, String paramString4, long paramLong1, long paramLong2, int paramInt2, boolean paramBoolean, VideomailUploaderListener paramVideomailUploaderListener)
  {
    this.m_context = paramContext;
    this.m_conversationId = paramString1;
    this.m_filename = paramString2;
    this.m_thumbnailFilename = paramString3;
    this.m_rotationHint = paramInt2;
    this.m_duration = paramInt1;
    this.m_listener = paramVideomailUploaderListener;
    this.m_fileSize = paramLong1;
    this.m_uploadVideoMailMessage = new MediaEngineMessage.SendConversationMessageMessage(paramString1, SessionMessages.ConversationMessageType.VIDEO_MESSAGE, this.m_filename, this.m_thumbnailFilename, (int)paramLong1, paramInt1, paramLong2, paramInt2, VideomailUtils.isVideomailReplaySupported());
    Log.v("VideomailUploader", "rotationHint=" + paramInt2 + " isFlipped=" + paramBoolean);
  }

  private void detachListener()
  {
    if (this.m_listener != null)
    {
      VideomailUploaderListener localVideomailUploaderListener = this.m_listener;
      this.m_listener = null;
      localVideomailUploaderListener.onUploadDetached();
    }
  }

  public static Bitmap rotate(Bitmap paramBitmap, int paramInt)
  {
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getHeight(), paramBitmap.getWidth(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    Matrix localMatrix = new Matrix();
    localMatrix.setRotate(paramInt, paramBitmap.getWidth() / 2, paramBitmap.getHeight() / 2);
    localCanvas.drawBitmap(paramBitmap, localMatrix, new Paint());
    return localBitmap;
  }

  private void sendFinishUploadMessage()
  {
    if (this.m_videomailId == null)
      return;
    MediaEngineMessage.SendConversationMessageMessage localSendConversationMessageMessage = new MediaEngineMessage.SendConversationMessageMessage(this.m_conversationId, SessionMessages.ConversationMessageType.VIDEO_MESSAGE, this.m_videomailId, (int)this.m_fileSize, this.m_duration);
    MessageRouter.getInstance().postMessage("jingle", localSendConversationMessageMessage);
    MediaEngineMessage.FinishUploadVideoMailMessage localFinishUploadVideoMailMessage = new MediaEngineMessage.FinishUploadVideoMailMessage(this.m_videomailId);
    MessageRouter.getInstance().postMessage("jingle", localFinishUploadVideoMailMessage);
  }

  private void sendUploadMessage()
  {
    Log.v("VideomailUploader", "uploadFile(): Posting message: " + this.m_uploadVideoMailMessage);
    MessageRouter.getInstance().postMessage("jingle", this.m_uploadVideoMailMessage);
  }

  public void cancel()
  {
    this.m_isFinished = true;
    this.m_handler.removeMessages(1);
    this.m_handler.removeMessages(3);
    if (this.m_videomailId == null)
      return;
    if (this.m_uriRequest != null);
    try
    {
      this.m_uriRequest.abort();
      detachListener();
      return;
    }
    catch (UnsupportedOperationException localUnsupportedOperationException)
    {
      while (true)
        Log.w("VideomailUploader", "Exception while aborting request: ", localUnsupportedOperationException);
    }
  }

  public boolean isFinished()
  {
    return this.m_isFinished;
  }

  public void onUploadTokenReceived(com.sgiggle.messaging.Message paramMessage)
  {
    MediaEngineMessage.UploadVideoMailResponseEvent localUploadVideoMailResponseEvent = (MediaEngineMessage.UploadVideoMailResponseEvent)paramMessage;
    if ((localUploadVideoMailResponseEvent != null) && (!this.m_isFinished))
    {
      this.m_videomailUrl = ((SessionMessages.UploadVideoMailResponsePayload)localUploadVideoMailResponseEvent.payload()).getVideoMailUrl();
      this.m_videomailId = ((SessionMessages.UploadVideoMailResponsePayload)localUploadVideoMailResponseEvent.payload()).getVideoMailId();
      if (this.m_listener != null)
        this.m_listener.onUploadVideoMailIdReceived(this.m_videomailId);
      UploadFileTask localUploadFileTask = new UploadFileTask(null);
      String[] arrayOfString = new String[1];
      arrayOfString[0] = this.m_filename;
      localUploadFileTask.execute(arrayOfString);
    }
  }

  public void onVideomailErrorEvent(com.sgiggle.messaging.Message paramMessage)
  {
    int i = this.m_retryUploadTokenTimeout / 1000;
    Log.w("VideomailUploader", "Unable to retrieve upload token. Retry in " + i + " seconds...");
    this.m_handler.sendEmptyMessageDelayed(3, this.m_retryUploadTokenTimeout);
    if (this.m_listener != null)
      this.m_listener.onUploadWillRetry(i);
  }

  public boolean start()
  {
    if (this.m_isFinished)
    {
      Log.e("VideomailUploader", "Uploader did not start, it's already been used. Create a new object an try again.");
      return false;
    }
    Bitmap localBitmap;
    if (Build.VERSION.SDK_INT >= 8)
    {
      localBitmap = ThumbnailUtils.createVideoThumbnail(this.m_filename, 3);
      if (localBitmap == null)
        break label153;
      if ((this.m_rotationHint > 0) && (!VideomailUtils.isVideomailReplaySupported()))
      {
        Log.v("VideomailUploader", "Rotate bitmap for " + this.m_rotationHint);
        localBitmap = rotate(localBitmap, this.m_rotationHint);
      }
    }
    while (true)
    {
      try
      {
        FileOutputStream localFileOutputStream = new FileOutputStream(this.m_thumbnailFilename);
        localBitmap.compress(Bitmap.CompressFormat.JPEG, 100, localFileOutputStream);
        sendUploadMessage();
        return true;
      }
      catch (Exception localException)
      {
        Log.w("VideomailUploader", "Error saving video thumbnail at " + this.m_thumbnailFilename);
        continue;
      }
      label153: Log.e("VideomailUploader", "Failed to generate thumbnail from " + this.m_filename);
    }
  }

  public void transferred(long paramLong)
  {
    int i = (int)(0.5F + 100.0F * ((float)paramLong / (float)this.m_fileSize));
    Log.d("VideomailUploader", "bytes: " + paramLong + "\tpercentage: " + i);
    if (this.m_listener != null)
      this.m_listener.onUploadProgress(i);
  }

  private class MainHandler extends Handler
  {
    private MainHandler()
    {
    }

    public void handleMessage(android.os.Message paramMessage)
    {
      switch (paramMessage.what)
      {
      case 2:
      default:
        Log.v("VideomailUploader", "Unhandled message: " + paramMessage.what);
        return;
      case 1:
        VideomailUploader.access$112(VideomailUploader.this, 5000);
        VideomailUploader.UploadFileTask localUploadFileTask = new VideomailUploader.UploadFileTask(VideomailUploader.this, null);
        String[] arrayOfString = new String[1];
        arrayOfString[0] = VideomailUploader.this.m_filename;
        localUploadFileTask.execute(arrayOfString);
        return;
      case 3:
      }
      VideomailUploader.access$412(VideomailUploader.this, 5000);
      VideomailUploader.this.sendUploadMessage();
    }
  }

  private class UploadFileTask extends AsyncTask<String, Void, Integer>
  {
    final int RESULT_CODE_BAD_RESPONSE_ERROR = -3;
    final int RESULT_CODE_FILE_DOES_NOT_EXIST = -5;
    final int RESULT_CODE_INVALID_URL_ERROR = -2;
    final int RESULT_CODE_NETWORK_ERROR = -1;
    final int RESULT_CODE_OK = 0;
    final int RESULT_CODE_UNKNOWN_ERROR = -4;

    private UploadFileTask()
    {
    }

    protected Integer doInBackground(String[] paramArrayOfString)
    {
      if ((paramArrayOfString[0] == null) || (!new File(paramArrayOfString[0]).exists()))
      {
        Log.e("VideomailUploader", "UploadFileTask: error uploading file: file does not exist");
        return Integer.valueOf(-5);
      }
      RemoteExecutor localRemoteExecutor = new RemoteExecutor(new TangoHttpClient(VideomailUploader.this.m_context, new TangoHttpConfiguration()));
      RequestFactory localRequestFactory = new RequestFactory();
      try
      {
        UploadCommand localUploadCommand = new UploadCommand(paramArrayOfString[0]);
        if ((VideomailUploader.this.m_videomailUrl == null) || (VideomailUploader.this.m_videomailUrl.length() == 0))
          return Integer.valueOf(-2);
        localUploadCommand.setUrl(VideomailUploader.this.m_videomailUrl);
        localUploadCommand.setProgressListener(VideomailUploader.this);
        VideomailUploader.access$902(VideomailUploader.this, localRequestFactory.newHttpRequest(localUploadCommand));
        VideomailResponse localVideomailResponse = (VideomailResponse)localRemoteExecutor.execute(VideomailUploader.this.m_uriRequest, new VideomailHandler());
        if ((localVideomailResponse != null) && (localVideomailResponse.getCode() == 200))
          return Integer.valueOf(0);
        Log.e("VideomailUploader", "UploadFileTask: error uploading file: " + localVideomailResponse.getErrorMessage());
        Integer localInteger = Integer.valueOf(-3);
        return localInteger;
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        Log.e("VideomailUploader", "UploadFileTask: error uploading file", localIllegalArgumentException);
        return Integer.valueOf(-5);
      }
      catch (HandlerException localHandlerException)
      {
        Log.e("VideomailUploader", "UploadFileTask: error uploading file", localHandlerException);
        return Integer.valueOf(-4);
      }
      catch (MethodNotSupportedException localMethodNotSupportedException)
      {
        Log.e("VideomailUploader", "UploadFileTask: error uploading file", localMethodNotSupportedException);
        return Integer.valueOf(-4);
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        Log.e("VideomailUploader", "UploadFileTask: error uploading file", localUnsupportedEncodingException);
      }
      return Integer.valueOf(-4);
    }

    protected void onPostExecute(Integer paramInteger)
    {
      if (VideomailUploader.this.m_isFinished);
      do
      {
        return;
        switch (paramInteger.intValue())
        {
        default:
          return;
        case -5:
          VideomailUploader.access$1002(VideomailUploader.this, true);
          VideomailUploader.this.sendFinishUploadMessage();
          if (VideomailUploader.this.m_listener != null)
            VideomailUploader.this.m_listener.onUploadFailedFileNotFound();
          VideomailUploader.this.detachListener();
          return;
        case 0:
          VideomailUploader.access$1002(VideomailUploader.this, true);
          VideomailUploader.this.sendFinishUploadMessage();
          if (VideomailUploader.this.m_listener != null)
            VideomailUploader.this.m_listener.onUploadSuccess();
          VideomailUploader.this.detachListener();
          return;
        case -4:
        case -3:
        case -2:
        case -1:
        }
        Log.e("VideomailUploader", "UploadFileTask: error during upload. Result code: " + paramInteger);
        VideomailUploader.this.m_handler.sendEmptyMessageDelayed(1, VideomailUploader.this.m_retryTimeout);
      }
      while (VideomailUploader.this.m_listener == null);
      VideomailUploader.this.m_listener.onUploadWillRetry(VideomailUploader.this.m_retryTimeout / 1000);
    }

    protected void onPreExecute()
    {
      if (VideomailUploader.this.m_listener != null)
        VideomailUploader.this.m_listener.onUploadPreExecute();
    }
  }

  public static abstract interface VideomailUploaderListener
  {
    public abstract void onUploadDetached();

    public abstract void onUploadFailedFileNotFound();

    public abstract void onUploadPreExecute();

    public abstract void onUploadProgress(int paramInt);

    public abstract void onUploadSuccess();

    public abstract void onUploadVideoMailIdReceived(String paramString);

    public abstract void onUploadWillRetry(int paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.VideomailUploader
 * JD-Core Version:    0.6.2
 */