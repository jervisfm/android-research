package com.sgiggle.production.screens.videomail;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build;
import android.os.Build.VERSION;
import com.sgiggle.production.TangoApp;
import com.sgiggle.util.Log;

public class VideomailUtils
{
  private static final String TAG = "VideomailUtils";

  public static Dialog createSendBySmsWarningDialog(Context paramContext)
  {
    if (VideomailSharedPreferences.getShowSendBySmsWarningDialog(paramContext))
    {
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
      localBuilder.setTitle(2131296449).setMessage(2131296450).setIcon(17301659).setCancelable(false).setPositiveButton(2131296287, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface.dismiss();
        }
      }).setNegativeButton(2131296451, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          VideomailSharedPreferences.setShowSendBySmsWarningDialog(this.val$context, false);
        }
      });
      return localBuilder.create();
    }
    Log.d("VideomailUtils", "createSendBySmsWarningDialog: no dialog created based on preferences.");
    return null;
  }

  public static boolean isVideomailReplaySupported()
  {
    if (Build.VERSION.SDK_INT < 9)
      return false;
    if (((Build.MODEL.equals("Galaxy Nexus")) && (Build.MANUFACTURER.equals("samsung"))) || ((Build.MODEL.equals("Nexus S")) && (Build.MANUFACTURER.equals("samsung"))) || ((Build.MODEL.equals("DROIDX")) && (Build.MANUFACTURER.equals("motorola"))) || ((Build.MODEL.equals("LG-P970")) && (Build.MANUFACTURER.equals("LGE"))) || ((Build.MODEL.equals("HTC Sensation 4G")) && (Build.MANUFACTURER.equals("HTC"))) || ((Build.MODEL.equals("T-Mobile G2")) && (Build.MANUFACTURER.equals("HTC"))))
      return true;
    if (VideomailSharedPreferences.getForceVideomailReplay(TangoApp.getInstance().getApplicationContext()))
    {
      Log.d("VideomailUtils", "isVideomailReplaySupported: forced to true");
      return true;
    }
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.VideomailUtils
 * JD-Core Version:    0.6.2
 */