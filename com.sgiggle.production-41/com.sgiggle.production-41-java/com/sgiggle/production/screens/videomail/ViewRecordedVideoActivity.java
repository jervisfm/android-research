package com.sgiggle.production.screens.videomail;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.widget.Timer;
import com.sgiggle.production.widget.VideomailMediaController;
import com.sgiggle.production.widget.VideomailMediaController.Mode;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.RecordVideoMailPayload;
import java.io.File;
import java.util.List;

public class ViewRecordedVideoActivity extends ViewVideoBaseActivity
  implements SendVideomailActivityHelper.SendVideomailActivityHelperListener
{
  public static final String EXTRA_DURATION = "duration";
  public static final String EXTRA_FLIPPED = "flipped";
  public static final String EXTRA_MAX_DURATION = "maxDuration";
  public static final String EXTRA_MAX_DURATION_REACHED = "maxDurationReached";
  public static final String EXTRA_MIME_TYPE = "mimeType";
  public static final String EXTRA_RECORD_VIDEOMAIL_PAYLOAD = "recordVideoMailPayload";
  public static final String EXTRA_ROTATE_ON_SERVER = "rotateOnServer";
  public static final String EXTRA_ROTATION_HINT = "rotationHint";
  public static final String EXTRA_TIME_CREATED = "timeCreated";
  public static final String EXTRA_VIDEO_FILENAME = "videoFilename";
  public static final String EXTRA_VIDEO_THUMBNAIL_FILENAME = "videoThumbnailFilename";
  private static final int MAX_VIDEO_PREPARE_RETRY = 0;
  public static final int RESULT_FILE_NOT_FOUND = 4;
  public static final int RESULT_RETAKE = 2;
  public static final int RESULT_SENT_CANCELED = 3;
  public static final int REVIEW_AND_SEND_REQUEST = 1;
  private static final String TAG = "ViewRecordedVideoActivity";
  private LinearLayout m_alertMaxDurationReached;
  private LinearLayout m_alertOrientationWarning;
  private List<SessionMessages.Contact> m_calleesList;
  private long m_contentLength = 0L;
  private int m_duration = 0;
  private ProgressBar m_fileUploadProgress;
  private LinearLayout m_fileUploader;
  private String m_filename = null;
  private boolean m_isFlipped = false;
  private int m_maxDuration = 0;
  private boolean m_maxDurationReached = false;
  private String m_mimeType;
  private boolean m_rotateOnServer = false;
  private int m_rotationHint = 0;
  private SendVideomailActivityHelper m_sendHelper;
  private boolean m_showOrientationWarning = false;
  private String m_thumbnailFilename = null;
  private long m_timeCreated = 0L;
  private TextView m_uploadingMessage;
  private Object m_videomailSenderLock = new Object();

  private void cancelSend()
  {
    synchronized (this.m_videomailSenderLock)
    {
      if (this.m_sendHelper != null)
        this.m_sendHelper.cancel();
      this.m_sendHelper = null;
      ((TangoApp)getApplication()).setSendVideomailActivityHelper(null);
      return;
    }
  }

  private void deleteVideoFile()
  {
    if (this.m_filename == null)
      return;
    if (new File(this.m_filename).delete())
    {
      Log.d("ViewRecordedVideoActivity", "File deleted: " + this.m_filename);
      return;
    }
    Log.w("ViewRecordedVideoActivity", "Could not delete (or already deleted): " + this.m_filename);
  }

  private void initAlertMessages()
  {
    RelativeLayout localRelativeLayout = (RelativeLayout)findViewById(2131361964);
    this.m_fileUploader = ((LinearLayout)getLayoutInflater().inflate(2130903139, null));
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
    localLayoutParams.addRule(10);
    this.m_fileUploadProgress = ((ProgressBar)this.m_fileUploader.findViewById(2131362143));
    this.m_uploadingMessage = ((TextView)this.m_fileUploader.findViewById(2131362144));
    this.m_fileUploader.setVisibility(8);
    localRelativeLayout.addView(this.m_fileUploader, localLayoutParams);
    if (this.m_showOrientationWarning)
    {
      this.m_alertOrientationWarning = ((LinearLayout)getLayoutInflater().inflate(2130903138, null));
      ((TextView)this.m_alertOrientationWarning.findViewById(2131362141)).setText(2131296448);
      this.m_alertOrientationWarning.setVisibility(8);
      localRelativeLayout.addView(this.m_alertOrientationWarning, localLayoutParams);
    }
    if (this.m_maxDurationReached)
    {
      this.m_alertMaxDurationReached = ((LinearLayout)getLayoutInflater().inflate(2130903138, null));
      TextView localTextView = (TextView)this.m_alertMaxDurationReached.findViewById(2131362141);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Timer.formatTime(this.m_maxDuration);
      localTextView.setText(getString(2131296438, arrayOfObject));
      this.m_alertMaxDurationReached.setVisibility(8);
      localRelativeLayout.addView(this.m_alertMaxDurationReached, localLayoutParams);
    }
  }

  private void showAlert(AlertType paramAlertType)
  {
    int k;
    int i;
    int j;
    switch (1.$SwitchMap$com$sgiggle$production$screens$videomail$ViewRecordedVideoActivity$AlertType[paramAlertType.ordinal()])
    {
    default:
      k = 8;
      i = 8;
      j = 8;
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      this.m_fileUploader.setVisibility(j);
      if (this.m_alertMaxDurationReached != null)
        this.m_alertMaxDurationReached.setVisibility(i);
      if (this.m_alertOrientationWarning != null)
        this.m_alertOrientationWarning.setVisibility(k);
      return;
      k = 8;
      i = 8;
      j = 0;
      continue;
      k = 8;
      j = 8;
      i = 0;
      continue;
      i = 8;
      j = 8;
      k = 0;
    }
  }

  private void startSend()
  {
    Log.d("ViewRecordedVideoActivity", "startSend()");
    if (this.m_contentLength == 0L)
    {
      Log.w("ViewRecordedVideoActivity", "uploadFile(): contentLength = 0. No need to request for upload.");
      onNoVideoError();
      return;
    }
    String str1 = this.m_filename;
    String str2 = this.m_thumbnailFilename;
    List localList = this.m_calleesList;
    int i = this.m_duration;
    String str3 = this.m_mimeType;
    long l1 = this.m_contentLength;
    long l2 = this.m_timeCreated;
    if (this.m_rotateOnServer);
    for (int j = this.m_rotationHint; ; j = 0)
    {
      this.m_sendHelper = new SendVideomailActivityHelper(this, str1, str2, localList, i, str3, l1, l2, j, this.m_isFlipped, true, false, this);
      ((TangoApp)getApplication()).setSendVideomailActivityHelper(this.m_sendHelper);
      this.m_sendHelper.send();
      return;
    }
  }

  public VideomailMediaController createVideoMediaController()
  {
    return new VideomailMediaController(this, true, VideomailMediaController.Mode.REVIEW_RECORDED_VIDEOMAIL);
  }

  protected int getMaxPrepareRetry()
  {
    return 0;
  }

  protected boolean isForcePreviewEnabled()
  {
    return true;
  }

  protected boolean isLoadingIndicatorEnabled()
  {
    return false;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Log.i("ViewRecordedVideoActivity", "onActivityResult(): requestCode = " + paramInt1 + ", resultCode = " + paramInt2);
    synchronized (this.m_videomailSenderLock)
    {
      if ((this.m_sendHelper != null) && (this.m_sendHelper.handleOnActivityResult(paramInt1, paramInt2, paramIntent)))
        return;
      return;
    }
  }

  public void onBackPressed()
  {
    setResult(2);
    super.onBackPressed();
  }

  public void onBeforeSmsDialogShown()
  {
  }

  protected void onCancelRequested()
  {
    setResult(3);
    finish();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    this.m_filename = localBundle.getString("videoFilename");
    this.m_thumbnailFilename = localBundle.getString("videoThumbnailFilename");
    this.m_rotateOnServer = localBundle.getBoolean("rotateOnServer", false);
    this.m_rotationHint = localBundle.getInt("rotationHint", 0);
    this.m_duration = localBundle.getInt("duration", 0);
    this.m_isFlipped = localBundle.getBoolean("flipped", false);
    this.m_mimeType = localBundle.getString("mimeType");
    this.m_timeCreated = localBundle.getLong("timeCreated", 0L);
    this.m_maxDurationReached = localBundle.getBoolean("maxDurationReached", false);
    if (this.m_maxDurationReached)
      this.m_maxDuration = localBundle.getInt("maxDuration");
    m_uri = this.m_filename;
    this.m_calleesList = ((SessionMessages.RecordVideoMailPayload)localBundle.getSerializable("recordVideoMailPayload")).getCalleesList();
    boolean bool;
    if ((this.m_rotationHint != 0) && (this.m_rotateOnServer) && (!VideomailUtils.isVideomailReplaySupported()))
    {
      bool = true;
      this.m_showOrientationWarning = bool;
      if (this.m_showOrientationWarning)
        setRequestedOrientation(1);
      initAlertMessages();
      if (!this.m_maxDurationReached)
        break label276;
      showAlert(AlertType.MAX_DURATION);
    }
    while (true)
    {
      if ((this.m_filename != null) && (this.m_filename.length() > 0))
      {
        m_receivedUri = true;
        File localFile = new File(this.m_filename);
        if (localFile.exists())
          this.m_contentLength = localFile.length();
      }
      if (this.m_contentLength == 0L)
        onNoVideoError();
      return;
      bool = false;
      break;
      label276: if (this.m_showOrientationWarning)
        showAlert(AlertType.ORIENTATION_WARNING);
    }
  }

  protected void onDestroy()
  {
    super.onDestroy();
    cancelSend();
  }

  public void onDetached(boolean paramBoolean)
  {
  }

  protected void onMediaPlayerError()
  {
    Toast.makeText(this, 2131296447, 1).show();
  }

  protected void onPlayRequested()
  {
    if (this.m_showOrientationWarning);
    for (AlertType localAlertType = AlertType.ORIENTATION_WARNING; ; localAlertType = AlertType.NONE)
    {
      showAlert(localAlertType);
      super.onPlayRequested();
      return;
    }
  }

  protected void onRetakeRequested()
  {
    setResult(2);
    finish();
  }

  protected void onSendRequested()
  {
    synchronized (this.m_videomailSenderLock)
    {
      cancelSend();
      startSend();
      return;
    }
  }

  public void onSendSuccess()
  {
    showAlert(AlertType.NONE);
  }

  public void onSmsDialogDismissed()
  {
  }

  public void onUploadFailedFileNotFound()
  {
    Log.d("ViewRecordedVideoActivity", "Upload failed, file not found.");
    Toast.makeText(this, 2131296441, 1).show();
    setResult(4);
    finish();
  }

  public void onUploadPreExecute()
  {
    this.m_uploadingMessage.setText(2131296444);
  }

  public void onUploadProgress(int paramInt)
  {
    Log.d("ViewRecordedVideoActivity", "Upload in progress " + paramInt + "%");
    this.m_fileUploadProgress.setProgress(paramInt);
  }

  public void onUploadRequested()
  {
    Log.d("ViewRecordedVideoActivity", "Upload about to start...");
    this.m_controller.setReviewSendState();
    showAlert(AlertType.FILE_UPLOADER);
    this.m_uploadingMessage.setText(2131296444);
  }

  public void onUploadWillRetry(int paramInt)
  {
    Log.d("ViewRecordedVideoActivity", "Upload failed, will retry in " + paramInt + "s");
    TextView localTextView = this.m_uploadingMessage;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    localTextView.setText(getString(2131296442, arrayOfObject));
  }

  protected boolean startPlaybackOnFirstLaunch()
  {
    return false;
  }

  private static enum AlertType
  {
    static
    {
      FILE_UPLOADER = new AlertType("FILE_UPLOADER", 1);
      MAX_DURATION = new AlertType("MAX_DURATION", 2);
      ORIENTATION_WARNING = new AlertType("ORIENTATION_WARNING", 3);
      AlertType[] arrayOfAlertType = new AlertType[4];
      arrayOfAlertType[0] = NONE;
      arrayOfAlertType[1] = FILE_UPLOADER;
      arrayOfAlertType[2] = MAX_DURATION;
      arrayOfAlertType[3] = ORIENTATION_WARNING;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.ViewRecordedVideoActivity
 * JD-Core Version:    0.6.2
 */