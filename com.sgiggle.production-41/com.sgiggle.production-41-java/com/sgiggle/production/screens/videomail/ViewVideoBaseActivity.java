package com.sgiggle.production.screens.videomail;

import android.app.Dialog;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.sgiggle.production.ActivityBase;
import com.sgiggle.production.widget.BetterVideoView;
import com.sgiggle.production.widget.BetterVideoView.BetterVideoViewListener;
import com.sgiggle.production.widget.VideomailMediaController;
import com.sgiggle.production.widget.VideomailMediaController.Callback;
import com.sgiggle.util.Log;

public abstract class ViewVideoBaseActivity extends ActivityBase
  implements BetterVideoView.BetterVideoViewListener
{
  private static final int CLEAR_SCREEN_DELAY = 2;
  private static final int FETCH_URL_MAX_RETRY_COUNT = 5;
  public static final String KEY_FIRST_LAUNCH = "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_FIRST_LAUNCH";
  private static final String KEY_VIDEO_PLAYING = "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_PLAYING";
  private static final String KEY_VIDEO_POSITION = "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_POSITION";
  public static final String KEY_VIDEO_SOURCE = "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_SOURCE";
  private static final int MSG_SHOW_ERROR = 0;
  private static final int MSG_UPDATE_VIDEO_PLAYBACK = 1;
  private static final int SCREEN_DELAY = 60000;
  private static final String TAG = "Tango.ViewVideoBaseActivity";
  public static boolean m_receivedUri = false;
  public static String m_uri = null;
  protected VideomailMediaController m_controller;
  private ControllerCallBack m_controllerCallback = new ControllerCallBack(null);
  private int m_fetchUrlRetryCount = 0;
  private volatile boolean m_firstLaunch = true;
  private final Handler m_handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        Log.w("Tango.ViewVideoBaseActivity", "invalid message: " + paramAnonymousMessage);
      case 0:
      case 1:
        do
          return;
        while (ViewVideoBaseActivity.this.m_isDestroyed);
        if (ViewVideoBaseActivity.this.m_fetchUrlRetryCount == 5)
        {
          ViewVideoBaseActivity.this.onNoVideoError();
          return;
        }
        ViewVideoBaseActivity.this.updateVideomailPlayback();
        ViewVideoBaseActivity.access$208(ViewVideoBaseActivity.this);
        return;
      case 2:
      }
      ViewVideoBaseActivity.this.getWindow().clearFlags(128);
    }
  };
  private boolean m_isDestroyed = false;
  private ProgressBar m_loadingIndicator;
  private int m_videoPosition = 0;
  protected BetterVideoView m_videoView;
  private boolean m_wasPlaying = false;

  private void keepScreenOn()
  {
    this.m_handler.removeMessages(2);
    getWindow().addFlags(128);
  }

  private void keepScreenOnAwhile()
  {
    this.m_handler.removeMessages(2);
    getWindow().addFlags(128);
    this.m_handler.sendEmptyMessageDelayed(2, 60000L);
  }

  private void resetScreenOn()
  {
    this.m_handler.removeMessages(2);
    getWindow().clearFlags(128);
  }

  private void updateVideomailPlayback()
  {
    if (m_receivedUri)
    {
      this.m_handler.removeMessages(1);
      if ((m_uri != null) && (!this.m_videoView.isPlaying()))
      {
        i = this.m_videoView.getCurrentPosition();
        this.m_videoView.stopPlayback();
        this.m_videoView.setVideoURI(Uri.parse(m_uri));
        this.m_videoView.seekTo(i);
        if ((this.m_wasPlaying) || (this.m_firstLaunch))
        {
          if (!startPlaybackOnFirstLaunch())
            break label100;
          this.m_videoView.start();
        }
      }
    }
    label100: 
    while ((!this.m_firstLaunch) || (m_receivedUri))
    {
      do
      {
        int i;
        while (true)
        {
          this.m_firstLaunch = false;
          return;
          if ((this.m_firstLaunch) && (isForcePreviewEnabled()))
            this.m_videoView.forcePreview();
        }
      }
      while (m_uri != null);
      onNoVideoError();
      return;
    }
    this.m_handler.sendEmptyMessageDelayed(1, 1000L);
  }

  public abstract VideomailMediaController createVideoMediaController();

  protected abstract int getMaxPrepareRetry();

  protected boolean isForcePreviewEnabled()
  {
    return false;
  }

  protected boolean isLoadingIndicatorEnabled()
  {
    return true;
  }

  public void onBackPressed()
  {
    super.onBackPressed();
  }

  protected void onCancelRequested()
  {
    Log.d("Tango.ViewVideoBaseActivity", "onCancelRequested");
  }

  public void onCompletion()
  {
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (!this.m_videoView.isPlaying())
      this.m_videoView.pause();
    super.onConfigurationChanged(paramConfiguration);
  }

  protected void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    getWindow().addFlags(1024);
    getWindow().clearFlags(2048);
    super.onCreate(paramBundle);
    this.m_fetchUrlRetryCount = 0;
    setContentView(2130903144);
    this.m_controller = createVideoMediaController();
    this.m_controller.setEnabled(false);
    this.m_controller.setCallback(this.m_controllerCallback);
    this.m_videoView = ((BetterVideoView)findViewById(2131362146));
    this.m_videoView.setVideomailMediaController(this.m_controller);
    this.m_videoView.init(this, getMaxPrepareRetry());
    this.m_loadingIndicator = ((ProgressBar)findViewById(2131362065));
    if (paramBundle != null)
    {
      m_uri = paramBundle.getString("com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_SOURCE");
      if (m_uri == null)
        break label219;
      this.m_videoView.setVideoURI(Uri.parse(m_uri));
    }
    while (true)
    {
      this.m_videoPosition = paramBundle.getInt("com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_POSITION", 0);
      boolean bool = paramBundle.getBoolean("com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_PLAYING");
      this.m_firstLaunch = paramBundle.getBoolean("com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_FIRST_LAUNCH");
      this.m_videoView.seekTo(this.m_videoPosition);
      if (bool)
        this.m_videoView.start();
      setVolumeControlStream(3);
      ((AudioManager)getSystemService("audio")).setSpeakerphoneOn(true);
      return;
      label219: onNoVideoError();
    }
  }

  protected void onDeleteRequested()
  {
    Log.d("Tango.ViewVideoBaseActivity", "onDeleteRequested");
  }

  protected void onDestroy()
  {
    super.onDestroy();
    this.m_isDestroyed = true;
    m_uri = null;
    m_receivedUri = false;
  }

  public void onError()
  {
    Log.e("Tango.ViewVideoBaseActivity", "An error occured playing URI: '" + m_uri + "'");
    onMediaPlayerError();
  }

  protected abstract void onMediaPlayerError();

  protected void onNoVideoError()
  {
    Toast.makeText(this, 2131296446, 1).show();
    finish();
  }

  protected void onPause()
  {
    super.onPause();
    this.m_wasPlaying = this.m_videoView.isPlaying();
    this.m_videoPosition = this.m_videoView.getCurrentPosition();
    Log.i("Tango.ViewVideoBaseActivity", "Release media resources");
    this.m_videoView.stopPlayback();
    Log.d("Tango.ViewVideoBaseActivity", "onPause :::: video playback state :::: was playing: " + this.m_wasPlaying);
    resetScreenOn();
    if (isFinishing())
    {
      this.m_isDestroyed = true;
      m_uri = null;
      m_receivedUri = false;
    }
  }

  protected void onPauseRequested()
  {
    Log.d("Tango.ViewVideoBaseActivity", "onPauseRequested");
    keepScreenOnAwhile();
  }

  protected void onPlayRequested()
  {
    Log.d("Tango.ViewVideoBaseActivity", "onPlayRequested");
    keepScreenOn();
  }

  protected void onPrepareDialog(int paramInt, Dialog paramDialog)
  {
    if (this.m_videoView != null)
      this.m_videoView.pause();
    onPauseRequested();
    super.onPrepareDialog(paramInt, paramDialog);
  }

  public void onPrepared()
  {
    if (isLoadingIndicatorEnabled())
      this.m_loadingIndicator.setVisibility(8);
    Log.v("Tango.ViewVideoBaseActivity", "onPrepared()");
    this.m_videoView.seekTo(this.m_videoPosition);
  }

  protected void onReplyRequested()
  {
    Log.d("Tango.ViewVideoBaseActivity", "onReplyRequested");
  }

  protected void onRestartRequested()
  {
    Log.d("Tango.ViewVideoBaseActivity", "onRestartRequested");
    this.m_videoView.stopPlayback();
    if (m_uri != null)
    {
      this.m_videoView.setVideoURI(Uri.parse(m_uri));
      return;
    }
    onNoVideoError();
  }

  protected void onResume()
  {
    super.onResume();
    if (isLoadingIndicatorEnabled())
      this.m_loadingIndicator.setVisibility(0);
    updateVideomailPlayback();
    Log.d("Tango.ViewVideoBaseActivity", "onResume :::: video playback state :::: was playing: " + this.m_wasPlaying + ", video position (ms): " + this.m_videoPosition);
    this.m_videoView.seekTo(this.m_videoPosition);
    if (this.m_wasPlaying)
      this.m_videoView.start();
    keepScreenOnAwhile();
  }

  protected void onRetakeRequested()
  {
    Log.d("Tango.ViewVideoBaseActivity", "onRetakeRequested");
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_POSITION", this.m_videoView.getCurrentPosition());
    paramBundle.putBoolean("com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_PLAYING", this.m_videoView.isPlaying());
    paramBundle.putString("com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_SOURCE", m_uri);
    paramBundle.putBoolean("com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_FIRST_LAUNCH", this.m_firstLaunch);
  }

  protected void onSendRequested()
  {
    Log.d("Tango.ViewVideoBaseActivity", "onSendRequested");
  }

  public void onUserInteraction()
  {
    super.onUserInteraction();
    if (!this.m_videoView.isPlaying())
      keepScreenOnAwhile();
  }

  protected boolean startPlaybackOnFirstLaunch()
  {
    return true;
  }

  private class ControllerCallBack
    implements VideomailMediaController.Callback
  {
    private ControllerCallBack()
    {
    }

    public void onCancel()
    {
      ViewVideoBaseActivity.this.onCancelRequested();
    }

    public void onDeleteMessage()
    {
      ViewVideoBaseActivity.this.onDeleteRequested();
    }

    public void onPause()
    {
      ViewVideoBaseActivity.this.onPauseRequested();
    }

    public void onPlay()
    {
      ViewVideoBaseActivity.this.onPlayRequested();
    }

    public void onReply()
    {
      ViewVideoBaseActivity.this.onReplyRequested();
    }

    public void onRestart()
    {
      ViewVideoBaseActivity.this.onRestartRequested();
    }

    public void onRetake()
    {
      ViewVideoBaseActivity.this.onRetakeRequested();
    }

    public void onSend()
    {
      ViewVideoBaseActivity.this.onSendRequested();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.ViewVideoBaseActivity
 * JD-Core Version:    0.6.2
 */