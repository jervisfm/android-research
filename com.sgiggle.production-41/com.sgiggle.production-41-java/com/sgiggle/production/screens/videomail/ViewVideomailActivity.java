package com.sgiggle.production.screens.videomail;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.sgiggle.media_engine.MediaEngineMessage.AcknowledgeCallErrorMessage;
import com.sgiggle.media_engine.MediaEngineMessage.CallErrorEvent;
import com.sgiggle.media_engine.MediaEngineMessage.DeleteVideoMailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.FinishPlayVideoMailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.FinishPlayVideoMessageMessage;
import com.sgiggle.media_engine.MediaEngineMessage.FinishSMSComposeMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ForwardMessageRequestMessage;
import com.sgiggle.media_engine.MediaEngineMessage.ForwardMessageResultEvent;
import com.sgiggle.media_engine.MediaEngineMessage.MakeCallMessage;
import com.sgiggle.media_engine.MediaEngineMessage.PausePlayVideoMailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.PlayVideoMailEvent;
import com.sgiggle.media_engine.MediaEngineMessage.PlayVideoMailMessage;
import com.sgiggle.media_engine.MediaEngineMessage.PlayVideoMessageEvent;
import com.sgiggle.media_engine.MediaEngineMessage.StartPlayVideoMailEvent;
import com.sgiggle.media_engine.MediaEngineMessage.StartSMSComposeEvent;
import com.sgiggle.media_engine.MediaEngineMessage.StartSMSComposeMessage;
import com.sgiggle.messaging.Message;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.Utils;
import com.sgiggle.production.widget.BetterVideoView;
import com.sgiggle.production.widget.VideomailMediaController;
import com.sgiggle.production.widget.VideomailMediaController.Mode;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;
import com.sgiggle.xmpp.SessionMessages.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessagePayload;
import com.sgiggle.xmpp.SessionMessages.ForwardMessageResultPayload;
import com.sgiggle.xmpp.SessionMessages.OptionalPayload;
import com.sgiggle.xmpp.SessionMessages.PlayVideoMailPayload;
import com.sgiggle.xmpp.SessionMessages.SMSComposerPayload;
import com.sgiggle.xmpp.SessionMessages.SMSComposerType;
import com.sgiggle.xmpp.SessionMessages.VideoMailDownloadURLPayload;
import com.sgiggle.xmpp.SessionMessages.VideoMailEntry;
import java.util.List;

public class ViewVideomailActivity extends ViewVideoBaseActivity
{
  private static final int DIALOG_DELETE_VIDEOMAIL = 0;
  private static final int DIALOG_REPLY = 1;
  private static final int MAX_VIDEO_PREPARE_RETRY = 3;
  private static final int REQUEST_CODE_FORWARD_SMS = 1;
  private static final String TAG = "ViewVideomailActivity";
  private SessionMessages.ConversationMessage m_conversationMessage = null;
  private boolean m_isCallEnabled = true;
  private boolean m_playedSuccessfully = false;
  private boolean m_showMessageOnSent = true;
  private SessionMessages.VideoMailEntry m_videomailEntry = null;
  private VideomailType m_videomailType;

  public static void clearOldPlaybackData()
  {
    m_uri = null;
    m_receivedUri = false;
  }

  private Dialog createDeleteVideomailDialog()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setTitle(2131296411).setMessage(2131296412).setIcon(17301642).setPositiveButton(2131296413, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        if (ViewVideomailActivity.this.m_videoView.isPlaying())
          ViewVideomailActivity.this.m_videoView.stopPlayback();
        ViewVideomailActivity.this.postPauseMessage();
        ViewVideomailActivity.this.postDeleteMessage();
        ViewVideomailActivity.this.onPlaybackFinished();
      }
    }).setNegativeButton(2131296414, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    });
    return localBuilder.create();
  }

  private Dialog createReplyDialog()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    localBuilder.setCancelable(true);
    localBuilder.setTitle(2131296427);
    if (this.m_isCallEnabled)
      localBuilder.setItems(2131492864, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          switch (paramAnonymousInt)
          {
          default:
            return;
          case 0:
            ViewVideomailActivity.this.postCallbackMessage();
            return;
          case 1:
          }
          ViewVideomailActivity.this.startForward();
        }
      });
    while (true)
    {
      return localBuilder.create();
      localBuilder.setItems(2131492865, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          switch (paramAnonymousInt)
          {
          default:
            return;
          case 0:
          }
          ViewVideomailActivity.this.startForward();
        }
      });
    }
  }

  private void handleCallErrorEvent(MediaEngineMessage.CallErrorEvent paramCallErrorEvent)
  {
    Log.d("ViewVideomailActivity", "Handle Call Error Event: displaying call error dialog");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.AcknowledgeCallErrorMessage(((SessionMessages.OptionalPayload)paramCallErrorEvent.payload()).getMessage()));
    showCallErrorDialog(this, paramCallErrorEvent);
  }

  private void handleForwardMessageResultEvent(MediaEngineMessage.ForwardMessageResultEvent paramForwardMessageResultEvent)
  {
    SessionMessages.ForwardMessageResultPayload localForwardMessageResultPayload = (SessionMessages.ForwardMessageResultPayload)paramForwardMessageResultEvent.payload();
    switch (7.$SwitchMap$com$sgiggle$xmpp$SessionMessages$ForwardMessageResultType[localForwardMessageResultPayload.getType().ordinal()])
    {
    default:
      return;
    case 1:
      onForwardError();
      return;
    case 2:
      onForwardSuccess();
      return;
    case 3:
    }
    onForwardBySmsNeeded(localForwardMessageResultPayload.getSmsContactsList(), localForwardMessageResultPayload.getViewUrl());
  }

  private void handleStartSMSComposeEvent(MediaEngineMessage.StartSMSComposeEvent paramStartSMSComposeEvent)
  {
    String str = Utils.getSmsAddressListFromContactList(((SessionMessages.SMSComposerPayload)paramStartSMSComposeEvent.payload()).getReceiversList());
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ((SessionMessages.SMSComposerPayload)paramStartSMSComposeEvent.payload()).getInfo();
    sendSms(str, localResources.getString(2131296430, arrayOfObject), 1);
  }

  private boolean isCallEnabled()
  {
    boolean bool = true;
    if ((this.m_videomailType == VideomailType.STANDALONE_VIDEOMAIL) && (!"_outbox".equals(this.m_videomailEntry.getFolder())) && (this.m_videomailEntry.hasCaller()))
    {
      Log.d("ViewVideomailActivity", "Has a caller");
      if ((this.m_videomailEntry.getCaller().hasAccountid()) && (TextUtils.isEmpty(this.m_videomailEntry.getCaller().getAccountid())))
        bool = false;
      do
      {
        return bool;
        if (!this.m_videomailEntry.getCaller().hasAccountid())
          return false;
      }
      while (!this.m_videomailEntry.getCaller().getAccountid().equals("mC5mPUPZh1ZsQP2zhN8s-g"));
      return false;
    }
    return false;
  }

  private void onForwardBySmsNeeded(final List<SessionMessages.Contact> paramList, final String paramString)
  {
    Dialog localDialog = VideomailUtils.createSendBySmsWarningDialog(this);
    if (localDialog == null)
    {
      postStartSMSComposeMessage(paramList, paramString, true);
      return;
    }
    localDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
    {
      public void onDismiss(DialogInterface paramAnonymousDialogInterface)
      {
        ViewVideomailActivity.this.postStartSMSComposeMessage(paramList, paramString, true);
      }
    });
    localDialog.show();
  }

  private void onForwardError()
  {
    Toast.makeText(this, 2131296443, 1).show();
  }

  private void onForwardSuccess()
  {
    if (this.m_showMessageOnSent)
      Toast.makeText(this, 2131296439, 1).show();
  }

  private void onPlaybackFinished()
  {
    if (this.m_videomailType == VideomailType.STANDALONE_VIDEOMAIL)
    {
      MediaEngineMessage.FinishPlayVideoMailMessage localFinishPlayVideoMailMessage = new MediaEngineMessage.FinishPlayVideoMailMessage(this.m_videomailEntry.getFolder(), this.m_videomailEntry.getVideoMailId(), this.m_playedSuccessfully);
      MessageRouter.getInstance().postMessage("jingle", localFinishPlayVideoMailMessage);
      return;
    }
    MediaEngineMessage.FinishPlayVideoMessageMessage localFinishPlayVideoMessageMessage = new MediaEngineMessage.FinishPlayVideoMessageMessage();
    MessageRouter.getInstance().postMessage("jingle", localFinishPlayVideoMessageMessage);
  }

  private void postCallbackMessage()
  {
    MediaEngineMessage.MakeCallMessage localMakeCallMessage = new MediaEngineMessage.MakeCallMessage(this.m_videomailEntry.getCaller().getAccountid(), Utils.getDisplayName(this.m_videomailEntry.getCaller()));
    MessageRouter.getInstance().postMessage("jingle", localMakeCallMessage);
  }

  private void postDeleteMessage()
  {
    MediaEngineMessage.DeleteVideoMailMessage localDeleteVideoMailMessage = new MediaEngineMessage.DeleteVideoMailMessage(this.m_videomailEntry.getFolder(), this.m_videomailEntry.getVideoMailId());
    MessageRouter.getInstance().postMessage("jingle", localDeleteVideoMailMessage);
  }

  private void postFinishSMSComposeMessage(boolean paramBoolean)
  {
    MediaEngineMessage.FinishSMSComposeMessage localFinishSMSComposeMessage = new MediaEngineMessage.FinishSMSComposeMessage(paramBoolean);
    MessageRouter.getInstance().postMessage("jingle", localFinishSMSComposeMessage);
  }

  private void postForwardMessageRequestMessage()
  {
    MediaEngineMessage.ForwardMessageRequestMessage localForwardMessageRequestMessage = new MediaEngineMessage.ForwardMessageRequestMessage(this.m_conversationMessage);
    MessageRouter.getInstance().postMessage("jingle", localForwardMessageRequestMessage);
  }

  private void postPauseMessage()
  {
    MediaEngineMessage.PausePlayVideoMailMessage localPausePlayVideoMailMessage = new MediaEngineMessage.PausePlayVideoMailMessage(this.m_videomailEntry.getFolder(), this.m_videomailEntry.getVideoMailId());
    MessageRouter.getInstance().postMessage("jingle", localPausePlayVideoMailMessage);
  }

  private void postPlayMessage()
  {
    MediaEngineMessage.PlayVideoMailMessage localPlayVideoMailMessage = new MediaEngineMessage.PlayVideoMailMessage(this.m_videomailEntry.getFolder(), this.m_videomailEntry.getVideoMailId());
    MessageRouter.getInstance().postMessage("jingle", localPlayVideoMailMessage);
  }

  private void postStartSMSComposeMessage(List<SessionMessages.Contact> paramList, String paramString, boolean paramBoolean)
  {
    MediaEngineMessage.StartSMSComposeMessage localStartSMSComposeMessage = new MediaEngineMessage.StartSMSComposeMessage(SessionMessages.SMSComposerType.SMS_COMPOSER_FORWARD_MESSAGE, paramBoolean, paramList, paramString);
    MessageRouter.getInstance().postMessage("jingle", localStartSMSComposeMessage);
  }

  private void sendSms(String paramString1, String paramString2, int paramInt)
  {
    Intent localIntent = Utils.getSmsIntent(paramString1, paramString2);
    try
    {
      startActivityForResult(localIntent, paramInt);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Log.w("ViewVideomailActivity", "Not activity was found for ACTION_VIEW (for sending SMS)");
    }
  }

  private void startForward()
  {
    this.m_showMessageOnSent = true;
    postForwardMessageRequestMessage();
  }

  public static void startPlayback(Message paramMessage)
  {
    MediaEngineMessage.StartPlayVideoMailEvent localStartPlayVideoMailEvent = (MediaEngineMessage.StartPlayVideoMailEvent)paramMessage;
    if ((localStartPlayVideoMailEvent != null) && (localStartPlayVideoMailEvent.payload() != null))
      m_uri = ((SessionMessages.VideoMailDownloadURLPayload)localStartPlayVideoMailEvent.payload()).getVideoMailUrl();
    m_receivedUri = true;
  }

  public VideomailMediaController createVideoMediaController()
  {
    return new VideomailMediaController(this, true, VideomailMediaController.Mode.VIEW_VIDEOMAIL);
  }

  protected int getMaxPrepareRetry()
  {
    return 3;
  }

  public void handleMessage(Message paramMessage)
  {
    switch (paramMessage.getType())
    {
    default:
      return;
    case 35280:
      handleForwardMessageResultEvent((MediaEngineMessage.ForwardMessageResultEvent)paramMessage);
      return;
    case 35281:
      handleStartSMSComposeEvent((MediaEngineMessage.StartSMSComposeEvent)paramMessage);
      return;
    case 35019:
    }
    handleCallErrorEvent((MediaEngineMessage.CallErrorEvent)paramMessage);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 1)
    {
      this.m_showMessageOnSent = false;
      postFinishSMSComposeMessage(true);
    }
  }

  public void onBackPressed()
  {
    super.onBackPressed();
    onPlaybackFinished();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getFirstMessage() == null)
    {
      Log.w("ViewVideomailActivity", "ViewVideomailActivity created without an event to init the layout");
      TangoApp.getInstance().onActivityResumeAfterKilled(this);
      return;
    }
    ((TangoApp)getApplication()).setVideoPlayerActivityInstance(this);
    if ((getFirstMessage() instanceof MediaEngineMessage.PlayVideoMailEvent))
    {
      this.m_videomailEntry = ((SessionMessages.PlayVideoMailPayload)((MediaEngineMessage.PlayVideoMailEvent)getFirstMessage()).payload()).getVideoMail();
      this.m_videomailType = VideomailType.STANDALONE_VIDEOMAIL;
      this.m_isCallEnabled = isCallEnabled();
      this.m_playedSuccessfully = false;
      return;
    }
    this.m_conversationMessage = ((SessionMessages.ConversationMessagePayload)((MediaEngineMessage.PlayVideoMessageEvent)getFirstMessage()).payload()).getMessage();
    String str = this.m_conversationMessage.getPath();
    if (str.length() > 0);
    for (m_uri = str; ; m_uri = this.m_conversationMessage.getUrl())
    {
      m_receivedUri = true;
      this.m_videomailType = VideomailType.CONVERSATION_VIDEO_MESSAGE;
      break;
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      Log.w("ViewVideomailActivity", "Unsupported dialog id: " + paramInt);
      return super.onCreateDialog(paramInt);
    case 0:
      return createDeleteVideomailDialog();
    case 1:
    }
    return createReplyDialog();
  }

  protected void onDeleteRequested()
  {
    super.onDeleteRequested();
    showDialog(0);
  }

  protected void onDestroy()
  {
    Log.d("ViewVideomailActivity", "onDestroy()");
    ((TangoApp)getApplication()).setVideoPlayerActivityInstance(null);
    super.onDestroy();
    onPlaybackFinished();
  }

  protected void onMediaPlayerError()
  {
    Toast.makeText(this, 2131296445, 1).show();
    this.m_playedSuccessfully = false;
    onPlaybackFinished();
  }

  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    Log.d("ViewVideomailActivity", "onNewIntent: activity will restore.");
  }

  protected void onNoVideoError()
  {
    super.onNoVideoError();
    this.m_playedSuccessfully = false;
    onPlaybackFinished();
  }

  protected void onPause()
  {
    super.onPause();
    if (isFinishing())
      ((TangoApp)getApplication()).setVideoPlayerActivityInstance(null);
  }

  protected void onPauseRequested()
  {
    super.onPauseRequested();
    if (this.m_videomailType == VideomailType.STANDALONE_VIDEOMAIL)
      postPauseMessage();
  }

  protected void onPlayRequested()
  {
    super.onPlayRequested();
    if (this.m_videomailType == VideomailType.STANDALONE_VIDEOMAIL)
      postPlayMessage();
  }

  public void onPrepared()
  {
    this.m_playedSuccessfully = true;
    super.onPrepared();
  }

  protected void onReplyRequested()
  {
    super.onReplyRequested();
    showDialog(1);
  }

  public void showCallErrorDialog(Context paramContext, MediaEngineMessage.CallErrorEvent paramCallErrorEvent)
  {
    String str = Utils.getStringFromResource(paramContext, ((SessionMessages.OptionalPayload)paramCallErrorEvent.payload()).getMessage());
    new AlertDialog.Builder(paramContext).setMessage(str).setPositiveButton(2131296295, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    }).setCancelable(false).create().show();
  }

  private static enum VideomailType
  {
    static
    {
      CONVERSATION_VIDEO_MESSAGE = new VideomailType("CONVERSATION_VIDEO_MESSAGE", 1);
      VideomailType[] arrayOfVideomailType = new VideomailType[2];
      arrayOfVideomailType[0] = STANDALONE_VIDEOMAIL;
      arrayOfVideomailType[1] = CONVERSATION_VIDEO_MESSAGE;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.ViewVideomailActivity
 * JD-Core Version:    0.6.2
 */