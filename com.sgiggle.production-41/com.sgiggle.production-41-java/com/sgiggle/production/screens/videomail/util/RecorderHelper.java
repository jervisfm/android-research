package com.sgiggle.production.screens.videomail.util;

import android.os.Build;
import android.os.Build.VERSION;
import com.sgiggle.production.screens.videomail.CamcorderProfileWrapper;
import com.sgiggle.util.Log;

public class RecorderHelper
{
  private static final String TAG = "Tango.RecordHelper";
  private static final boolean isGingerbreadOrHigher;
  private static final boolean isIcsOrHigher;
  private static boolean m_flip = false;
  private static boolean m_rotate = true;

  static
  {
    boolean bool1;
    if (Build.VERSION.SDK_INT >= 9)
    {
      bool1 = true;
      isGingerbreadOrHigher = bool1;
      if (Build.VERSION.SDK_INT < 14)
        break label42;
    }
    label42: for (boolean bool2 = true; ; bool2 = false)
    {
      isIcsOrHigher = bool2;
      return;
      bool1 = false;
      break;
    }
  }

  public static void getProfile(CamcorderProfileWrapper paramCamcorderProfileWrapper, int paramInt)
  {
    Log.d("Tango.RecordHelper", "getProfile(): (CURRENT) width = " + paramCamcorderProfileWrapper.videoFrameWidth + "; height = " + paramCamcorderProfileWrapper.videoFrameHeight + "; framerate = " + paramCamcorderProfileWrapper.videoFrameRate + "; bitrate = " + paramCamcorderProfileWrapper.videoBitRate);
    if (isIcsOrHigher)
      if (Build.MANUFACTURER.equalsIgnoreCase("HTC"))
        if (Build.MODEL.startsWith("HTC Sensation"))
        {
          paramCamcorderProfileWrapper.videoFrameWidth = 320;
          paramCamcorderProfileWrapper.videoFrameHeight = 240;
        }
    label812: 
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      return;
                      if (!Build.MANUFACTURER.equalsIgnoreCase("motorola"))
                        break;
                    }
                    while ((!Build.MODEL.equals("DROID RAZR")) && (!Build.MODEL.equals("DROID4")));
                    paramCamcorderProfileWrapper.videoFrameWidth = 640;
                    paramCamcorderProfileWrapper.videoFrameHeight = 480;
                    paramCamcorderProfileWrapper.videoFrameRate = 15;
                    paramCamcorderProfileWrapper.videoBitRate = 767000;
                    paramCamcorderProfileWrapper.audioBitRate = 56000;
                    return;
                    if (!Build.MANUFACTURER.equalsIgnoreCase("samsung"))
                      break;
                  }
                  while ((!Build.MODEL.equals("GT-P3113")) && (!Build.MODEL.equals("GT-I9100")) && (!Build.MODEL.equals("GT-I9300")) && (!Build.MODEL.equals("SCH-I925")));
                  paramCamcorderProfileWrapper.videoFrameWidth = 640;
                  paramCamcorderProfileWrapper.videoFrameHeight = 480;
                  paramCamcorderProfileWrapper.videoFrameRate = 15;
                  paramCamcorderProfileWrapper.videoBitRate = 767000;
                  return;
                }
                while ((!Build.MANUFACTURER.equalsIgnoreCase("sony ericsson")) || (!Build.MODEL.equals("LT22i")));
                paramCamcorderProfileWrapper.videoFrameWidth = 640;
                paramCamcorderProfileWrapper.videoFrameHeight = 480;
                paramCamcorderProfileWrapper.videoFrameRate = 30;
                paramCamcorderProfileWrapper.videoBitRate = 767000;
                return;
                if (!isGingerbreadOrHigher)
                  break label812;
                if ((Build.MANUFACTURER.equalsIgnoreCase("lge")) && (Build.MODEL.equals("LG-P925")))
                {
                  paramCamcorderProfileWrapper.videoFrameWidth = 640;
                  paramCamcorderProfileWrapper.videoFrameHeight = 480;
                  return;
                }
                if (!Build.MANUFACTURER.equalsIgnoreCase("samsung"))
                  break;
                if (Build.MODEL.equals("SHW-M110S"))
                {
                  paramCamcorderProfileWrapper.videoFrameRate = 15;
                  paramCamcorderProfileWrapper.videoFrameWidth = 592;
                  paramCamcorderProfileWrapper.videoFrameHeight = 480;
                  paramCamcorderProfileWrapper.videoBitRate = 767000;
                  paramCamcorderProfileWrapper.videoCodec = 2;
                  return;
                }
                if (Build.MODEL.equals("SPH-P100"))
                {
                  paramCamcorderProfileWrapper.videoFrameRate = 15;
                  paramCamcorderProfileWrapper.videoFrameWidth = 640;
                  paramCamcorderProfileWrapper.videoFrameHeight = 480;
                  paramCamcorderProfileWrapper.videoBitRate = 767000;
                  paramCamcorderProfileWrapper.videoCodec = 2;
                  return;
                }
                if ((Build.MODEL.equals("GT-I9003")) || (Build.MODEL.equals("GT-I9003L")))
                {
                  paramCamcorderProfileWrapper.videoFrameRate = 15;
                  paramCamcorderProfileWrapper.videoFrameWidth = 640;
                  paramCamcorderProfileWrapper.videoFrameHeight = 480;
                  paramCamcorderProfileWrapper.videoBitRate = 767000;
                  paramCamcorderProfileWrapper.videoCodec = 3;
                  return;
                }
                if ((Build.MODEL.equals("GT-I9000")) && (Build.VERSION.RELEASE.compareTo("2.3.2") > 0) && (paramInt == 1))
                {
                  paramCamcorderProfileWrapper.videoFrameWidth = 320;
                  paramCamcorderProfileWrapper.videoFrameHeight = 320;
                  return;
                }
                if (((Build.MODEL.equals("GT-P6200")) || (Build.MODEL.equals("GT-P6210"))) && (Build.VERSION.SDK_INT >= 13) && (paramInt == 1))
                {
                  paramCamcorderProfileWrapper.videoFrameWidth = 640;
                  paramCamcorderProfileWrapper.videoFrameHeight = 480;
                  paramCamcorderProfileWrapper.videoFrameRate = 15;
                  return;
                }
                if (Build.MODEL.equals("GT-S5830"))
                {
                  paramCamcorderProfileWrapper.videoFrameWidth = 320;
                  paramCamcorderProfileWrapper.videoFrameHeight = 240;
                  paramCamcorderProfileWrapper.videoFrameRate = 10;
                  return;
                }
              }
              while (!Build.MODEL.equals("SCH-I405"));
              paramCamcorderProfileWrapper.videoFrameWidth = 320;
              paramCamcorderProfileWrapper.videoFrameHeight = 240;
              paramCamcorderProfileWrapper.videoFrameRate = 15;
              return;
              if (!Build.MANUFACTURER.equalsIgnoreCase("motorola"))
                break;
              if (Build.MODEL.equals("DROID3"))
              {
                paramCamcorderProfileWrapper.videoFrameWidth = 320;
                paramCamcorderProfileWrapper.videoFrameHeight = 240;
                return;
              }
            }
            while (!Build.MODEL.equals("DROIDX"));
            paramCamcorderProfileWrapper.videoFrameWidth = 320;
            paramCamcorderProfileWrapper.videoFrameHeight = 240;
            return;
            if (!Build.MANUFACTURER.equalsIgnoreCase("HTC"))
              break;
            if (Build.MODEL.startsWith("HTC Sensation"))
            {
              paramCamcorderProfileWrapper.videoBitRate = 700000;
              return;
            }
          }
          while (!Build.MODEL.equalsIgnoreCase("HTC PG09410"));
          paramCamcorderProfileWrapper.videoFrameWidth = 640;
          paramCamcorderProfileWrapper.videoFrameHeight = 480;
          paramCamcorderProfileWrapper.videoFrameRate = 15;
          return;
        }
        while ((!Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) || (!Build.MODEL.equals("M886")));
        paramCamcorderProfileWrapper.videoCodec = 3;
        return;
        if (!Build.MANUFACTURER.equalsIgnoreCase("samsung"))
          break;
        if ((Build.MODEL.equals("SAMSUNG-SGH-I897")) || (Build.MODEL.startsWith("GT-I9000")))
        {
          paramCamcorderProfileWrapper.videoFrameRate = 15;
          paramCamcorderProfileWrapper.videoFrameWidth = 592;
          paramCamcorderProfileWrapper.videoFrameHeight = 480;
          paramCamcorderProfileWrapper.videoBitRate = 767000;
          paramCamcorderProfileWrapper.videoCodec = 2;
          return;
        }
        if (Build.MODEL.equals("SPH-M920"))
        {
          paramCamcorderProfileWrapper.videoBitRate = 400000;
          paramCamcorderProfileWrapper.videoCodec = 3;
          paramCamcorderProfileWrapper.fileFormat = 1;
          paramCamcorderProfileWrapper.videoFrameWidth = 352;
          paramCamcorderProfileWrapper.videoFrameHeight = 288;
          return;
        }
        if ((Build.MODEL.equals("GT-P1010")) || (Build.MODEL.equals("GT-I9003")) || (Build.MODEL.equals("GT-I9003L")))
        {
          paramCamcorderProfileWrapper.videoCodec = 3;
          return;
        }
      }
      while (!Build.MODEL.equals("YP-GB1"));
      paramCamcorderProfileWrapper.videoFrameWidth = 640;
      paramCamcorderProfileWrapper.videoFrameHeight = 480;
      return;
      if ((Build.MANUFACTURER.equalsIgnoreCase("lge")) && (Build.MODEL.equals("LG-P970")))
      {
        paramCamcorderProfileWrapper.videoFrameWidth = 320;
        paramCamcorderProfileWrapper.videoFrameHeight = 240;
        return;
      }
    }
    while ((!Build.MANUFACTURER.equalsIgnoreCase("motorola")) || (!Build.MODEL.equals("Droid")));
    paramCamcorderProfileWrapper.videoFrameWidth = 320;
    paramCamcorderProfileWrapper.videoFrameHeight = 240;
    paramCamcorderProfileWrapper.audioBitRate = 16000;
    paramCamcorderProfileWrapper.audioCodec = 3;
  }

  public static int getRecorderRotation(int paramInt1, int paramInt2, int paramInt3)
  {
    m_rotate = true;
    m_flip = false;
    int i;
    if (isIcsOrHigher)
      if (paramInt3 == 1)
      {
        i = (360 + (paramInt1 - paramInt2)) % 360;
        if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.startsWith("HTC Runnymede")))
        {
          i = (360 - i) % 360;
          m_rotate = false;
        }
      }
    while (true)
    {
      Log.d("Tango.RecordHelper", "getRecorderRotation: Captured media must be rotated by: " + i + " (m_rotate=" + m_rotate + ", m_flip=" + m_flip + ")");
      return i;
      i = (paramInt1 + paramInt2) % 360;
      if ((Build.MANUFACTURER.equals("HTC")) && (Build.MODEL.startsWith("HTC Runnymede")))
      {
        m_rotate = false;
        continue;
        if (isGingerbreadOrHigher)
        {
          if (paramInt3 == 1)
          {
            i = (360 + (paramInt1 - paramInt2)) % 360;
            Log.e("Tango.RecordHelper", "Original captured media angle: " + i);
            if (Build.MANUFACTURER.equals("HTC"))
            {
              i = (paramInt1 + paramInt2) % 360;
              Log.v("Tango.RecordHelper", "Adjusted original captured media angle: " + i);
              if (Build.MODEL.equals("PC36100"))
              {
                if (Build.VERSION.RELEASE.compareTo("2.3.5") < 0)
                {
                  i = (i + 90) % 360;
                  m_flip = true;
                }
                else
                {
                  i = (540 - i) % 360;
                }
              }
              else if (Build.MODEL.startsWith("HTC Flyer"))
              {
                m_flip = true;
              }
              else if ((Build.MODEL.startsWith("HTC Incredible S")) && (Build.VERSION.RELEASE.compareTo("2.3.5") < 0))
              {
                i = (i + 90) % 360;
                m_flip = true;
                m_rotate = false;
              }
              else if ((Build.MODEL.equals("HTC Glacier")) || (Build.MODEL.equals("HTC Desire S")))
              {
                i = (i + 90) % 360;
                m_rotate = true;
                m_flip = true;
              }
              else if ((Build.MODEL.equals("ADR6400L")) || (Build.MODEL.equals("HTC Rhyme S510b")))
              {
                i = (540 - i) % 360;
                m_rotate = true;
              }
              else if (Build.MODEL.equalsIgnoreCase("HTC PG09410"))
              {
                i = (360 - i) % 360;
                m_rotate = true;
              }
              else
              {
                i = (i + 180) % 360;
                m_rotate = false;
              }
            }
            else if (Build.MANUFACTURER.equalsIgnoreCase("samsung"))
            {
              if ((Build.MODEL.equals("GT-I9003")) || (Build.MODEL.equals("GT-I9003L")))
              {
                i = (630 - i) % 360;
                m_flip = true;
              }
              if ((Build.MODEL.equals("GT-I9000")) && (Build.VERSION.RELEASE.compareTo("2.3.3") > 0))
                m_flip = true;
            }
            else if (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI"))
            {
              if (Build.MODEL.equals("M886"))
                if (Build.VERSION.RELEASE.compareTo("2.3.5") < 0)
                {
                  i = (450 - i) % 360;
                  m_flip = true;
                }
                else
                {
                  m_flip = false;
                }
            }
            else if (Build.MANUFACTURER.equalsIgnoreCase("LGE"))
            {
              if (Build.MODEL.equals("LG-P999"))
                m_rotate = false;
              else if (Build.MODEL.equals("LG-E739"))
                i = (i + 270) % 360;
            }
            else if ((Build.MANUFACTURER.equalsIgnoreCase("ZTE")) && (Build.MODEL.equals("ZTE-N910")))
            {
              i = (i + 90) % 360;
            }
          }
          else
          {
            i = (paramInt1 + paramInt2) % 360;
            if (Build.MANUFACTURER.equals("HTC"))
            {
              if ((!Build.MODEL.equals("PC36100")) && (!Build.MODEL.equals("Nexus One")) && (!Build.MODEL.equals("ADR6300")) && (!Build.MODEL.equals("T-Mobile G2")) && (!Build.MODEL.equals("HTC Glacier")) && (!Build.MODEL.equals("HTC Rhyme S510b")) && (!Build.MODEL.equals("HTC Desire S")))
                m_rotate = false;
            }
            else if (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI"))
            {
              if ((Build.MODEL.equals("M886")) && (Build.VERSION.RELEASE.compareTo("2.3.5") < 0))
                i = (i + 180) % 360;
            }
            else if (Build.MANUFACTURER.equalsIgnoreCase("LGE"))
            {
              if (Build.MODEL.equals("LG-P999"))
                m_rotate = false;
              else if (Build.MODEL.equals("LG-E739"))
                i = (i + 90) % 360;
            }
            else if (Build.MANUFACTURER.equalsIgnoreCase("samsung"))
            {
              if (Build.MODEL.equals("SAMSUNG-SGH-I897"))
                i = (paramInt2 + 90) % 360;
            }
            else if ((Build.MANUFACTURER.equalsIgnoreCase("ZTE")) && (Build.MODEL.equals("ZTE-N910")))
              i = (i + 90) % 360;
          }
        }
        else
        {
          i = (paramInt2 + 90) % 360;
          if (paramInt3 == 1)
            if ((Build.MODEL.equals("ADR6400L")) || ((Build.MANUFACTURER.equalsIgnoreCase("lge")) && (Build.MODEL.equals("LG-P970"))))
            {
              m_flip = true;
            }
            else if (Build.MANUFACTURER.equalsIgnoreCase("samsung"))
            {
              if ((Build.MODEL.startsWith("GT-I9000")) || (Build.MODEL.equals("GT-P1010")) || (Build.MODEL.equals("SCH-I800")))
              {
                i = (i + 270) % 360;
                m_flip = true;
              }
              else if (Build.MODEL.equals("YP-GB1"))
              {
                m_flip = true;
              }
            }
            else if (Build.MANUFACTURER.equalsIgnoreCase("PANTECH"))
            {
              i = (i + 270) % 360;
              m_flip = true;
            }
            else if ((Build.MANUFACTURER.equalsIgnoreCase("LGE")) && ((Build.MODEL.startsWith("VS910 4G")) || (Build.MODEL.equals("LG-P925"))))
            {
              m_flip = true;
            }
        }
      }
    }
  }

  public static int getSurfaceRotation(int paramInt)
  {
    int i;
    if (isGingerbreadOrHigher)
      if (paramInt == 1)
        if ((Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) && (Build.MODEL.equals("M886")) && (Build.VERSION.RELEASE.compareTo("2.3.5") < 0))
          i = 270;
    boolean bool1;
    do
    {
      boolean bool8;
      do
      {
        boolean bool7;
        do
        {
          boolean bool6;
          do
          {
            boolean bool5;
            do
            {
              boolean bool4;
              do
              {
                boolean bool3;
                do
                {
                  boolean bool2;
                  do
                  {
                    do
                    {
                      int j;
                      do
                      {
                        boolean bool10;
                        do
                        {
                          boolean bool9;
                          do
                          {
                            boolean bool12;
                            do
                            {
                              boolean bool11;
                              do
                              {
                                return i;
                                bool11 = Build.MANUFACTURER.equalsIgnoreCase("samsung");
                                i = 0;
                              }
                              while (!bool11);
                              bool12 = Build.MODEL.startsWith("GT-I9000");
                              i = 0;
                            }
                            while (!bool12);
                            if (Build.VERSION.RELEASE.compareTo("2.3.3") >= 0)
                              return 360;
                            return 270;
                            bool9 = Build.MANUFACTURER.equalsIgnoreCase("HUAWEI");
                            i = 0;
                          }
                          while (!bool9);
                          bool10 = Build.MODEL.equals("M886");
                          i = 0;
                        }
                        while (!bool10);
                        j = Build.VERSION.RELEASE.compareTo("2.3.5");
                        i = 0;
                      }
                      while (j >= 0);
                      return 180;
                      i = 0;
                    }
                    while (paramInt != 1);
                    if (!Build.MANUFACTURER.equalsIgnoreCase("Samsung"))
                      break;
                    bool2 = Build.MODEL.equals("SPH-M920");
                    i = 0;
                  }
                  while (bool2);
                  bool3 = Build.MODEL.equals("SGH-T959V");
                  i = 0;
                }
                while (bool3);
                bool4 = Build.MODEL.equals("SCH-I510");
                i = 0;
              }
              while (bool4);
              bool5 = Build.MODEL.equals("SGH-T839");
              i = 0;
            }
            while (bool5);
            bool6 = Build.MODEL.equals("SAMSUNG-SGH-I997");
            i = 0;
          }
          while (bool6);
          bool7 = Build.MODEL.equals("YP-GB1");
          i = 0;
        }
        while (bool7);
        bool8 = Build.MODEL.equals("YP-G1");
        i = 0;
      }
      while (bool8);
      return 270;
      bool1 = Build.MANUFACTURER.equalsIgnoreCase("PANTECH");
      i = 0;
    }
    while (!bool1);
    return 270;
  }

  public static boolean isFlipped()
  {
    return m_flip;
  }

  public static boolean isRotated()
  {
    return m_rotate;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.screens.videomail.util.RecorderHelper
 * JD-Core Version:    0.6.2
 */