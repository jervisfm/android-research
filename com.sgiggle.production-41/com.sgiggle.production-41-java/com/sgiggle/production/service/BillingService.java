package com.sgiggle.production.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.vending.billing.IMarketBillingService;
import com.android.vending.billing.IMarketBillingService.Stub;
import com.sgiggle.media_engine.MediaEngineMessage.PurchaseAttemptMessage;
import com.sgiggle.media_engine.MediaEngineMessage.PurchaseVGoodMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.WrongTangoRuntimeVersionException;
import com.sgiggle.production.payments.Constants;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.production.payments.Constants.ResponseCode;
import com.sgiggle.production.payments.ResponseHandler;
import com.sgiggle.production.util.Security;
import com.sgiggle.production.util.Security.VerifiedPurchase;
import com.sgiggle.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class BillingService extends Service
  implements ServiceConnection
{
  public static final String TAG = "BillingService";
  private static Queue<BillingRequest> m_pendingRequests = new LinkedList();
  private static HashMap<Long, BillingRequest> m_sentRequests = new HashMap();
  private boolean m_isRestoreOperationDone = false;
  private IMarketBillingService m_marketBillingService;
  private boolean m_marketBindResult = false;
  private String m_packagePrefix;
  private ResponseHandler m_responseHandler;
  private final IBinder m_serviceBinder = new BillingServiceBinder();

  private String addPackagePrefix(String paramString)
  {
    return this.m_packagePrefix + paramString;
  }

  private boolean bindToMarketBillingService()
  {
    try
    {
      this.m_marketBindResult = bindService(new Intent("com.android.vending.billing.MarketBillingService.BIND"), this, 1);
      if (this.m_marketBindResult)
      {
        Log.i("BillingService", "Service bind successful");
        return true;
      }
      Log.e("BillingService", "Could not bind to the MarketBillingService");
      return false;
    }
    catch (SecurityException localSecurityException)
    {
      while (true)
        Log.e("BillingService", "Security Exception: ", localSecurityException);
    }
  }

  private void checkResponseCode(long paramLong, Constants.ResponseCode paramResponseCode)
  {
    BillingRequest localBillingRequest = (BillingRequest)m_sentRequests.get(Long.valueOf(paramLong));
    if (localBillingRequest != null)
    {
      Log.d("BillingService", localBillingRequest.getClass().getSimpleName() + ": " + paramResponseCode);
      localBillingRequest.responseCodeReceived(paramResponseCode);
    }
    m_sentRequests.remove(Long.valueOf(paramLong));
  }

  private boolean confirmNotifications(int paramInt, String[] paramArrayOfString)
  {
    return new ConfirmNotifications(paramInt, paramArrayOfString).runRequest();
  }

  private boolean getPurchaseInformation(int paramInt, String[] paramArrayOfString)
  {
    return new GetPurchaseInformation(paramInt, paramArrayOfString).runRequest();
  }

  private void purchaseStateChanged(int paramInt, String paramString1, String paramString2)
  {
    ArrayList localArrayList1 = Security.verifyPurchase(paramString1, paramString2);
    if (localArrayList1 == null)
      return;
    ArrayList localArrayList2 = new ArrayList();
    Iterator localIterator = localArrayList1.iterator();
    Security.VerifiedPurchase localVerifiedPurchase;
    String str1;
    String str2;
    if (localIterator.hasNext())
    {
      localVerifiedPurchase = (Security.VerifiedPurchase)localIterator.next();
      if (localVerifiedPurchase.notificationId != null)
        localArrayList2.add(localVerifiedPurchase.notificationId);
      str1 = removePackagePrefix(localVerifiedPurchase.productId);
      str2 = localVerifiedPurchase.developerPayload;
      if (str2 != null)
        break label211;
    }
    label211: for (String str3 = str1; ; str3 = str2)
    {
      if (localVerifiedPurchase.purchaseState == Constants.PurchaseState.PURCHASED)
        recordVGoodPurchased(str1, str3, paramString1, paramString2, localVerifiedPurchase.orderId, localVerifiedPurchase.purchaseTime);
      ResponseHandler localResponseHandler = this.m_responseHandler;
      Constants.PurchaseState localPurchaseState = localVerifiedPurchase.purchaseState;
      String str4 = localVerifiedPurchase.orderId;
      long l = localVerifiedPurchase.purchaseTime;
      localResponseHandler.purchaseResponse(this, localPurchaseState, str3, str4, l, null, paramString1, paramString2);
      break;
      if (!localArrayList2.isEmpty())
        confirmNotifications(paramInt, (String[])localArrayList2.toArray(new String[localArrayList2.size()]));
      this.m_isRestoreOperationDone = false;
      return;
    }
  }

  private void recordPurchasedAttempt(String paramString)
  {
    MediaEngineMessage.PurchaseAttemptMessage localPurchaseAttemptMessage = new MediaEngineMessage.PurchaseAttemptMessage(paramString);
    MessageRouter.getInstance().postMessage("jingle", localPurchaseAttemptMessage);
  }

  private void recordVGoodPurchased(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, long paramLong)
  {
    MediaEngineMessage.PurchaseVGoodMessage localPurchaseVGoodMessage = new MediaEngineMessage.PurchaseVGoodMessage(paramString1, paramString2, paramString3, 1, paramString5, paramLong / 1000L, paramString4, this.m_isRestoreOperationDone);
    MessageRouter.getInstance().postMessage("jingle", localPurchaseVGoodMessage);
  }

  private String removePackagePrefix(String paramString)
  {
    if (paramString.startsWith(this.m_packagePrefix))
      return paramString.substring(this.m_packagePrefix.length());
    return paramString;
  }

  private void runPendingRequests()
  {
    int i = -1;
    while (true)
    {
      BillingRequest localBillingRequest = (BillingRequest)m_pendingRequests.peek();
      if (localBillingRequest == null)
        break label56;
      if (!localBillingRequest.runIfConnected())
        break;
      m_pendingRequests.remove();
      if (i < localBillingRequest.getStartId())
        i = localBillingRequest.getStartId();
    }
    bindToMarketBillingService();
    label56: 
    while (i < 0)
      return;
    Log.i("BillingService", "stopping service, startId: " + i);
    stopSelf(i);
  }

  public boolean checkBillingSupported()
  {
    boolean bool = new CheckBillingSupported().runRequest();
    if (!bool)
      this.m_responseHandler.checkBillingSupported(false);
    return bool;
  }

  public void handleCommand(Intent paramIntent, int paramInt)
  {
    if (paramIntent == null)
    {
      Log.i("BillingService", "BillingService started with null intent!");
      if ((m_sentRequests.isEmpty()) && (m_pendingRequests.isEmpty()))
        stopSelfResult(paramInt);
    }
    while (true)
    {
      return;
      String str = paramIntent.getAction();
      Log.i("BillingService", "handleCommand() action: " + str);
      if ("com.sgiggle.production.tango.CONFIRM_NOTIFICATION".equals(str))
        confirmNotifications(paramInt, paramIntent.getStringArrayExtra("notification_id"));
      while ((m_sentRequests.isEmpty()) && (m_pendingRequests.isEmpty()))
      {
        stopSelfResult(paramInt);
        return;
        if ("com.sgiggle.production.tango.GET_PURCHASE_INFORMATION".equals(str))
          getPurchaseInformation(paramInt, new String[] { paramIntent.getStringExtra("notification_id") });
        else if ("com.android.vending.billing.PURCHASE_STATE_CHANGED".equals(str))
          purchaseStateChanged(paramInt, paramIntent.getStringExtra("inapp_signed_data"), paramIntent.getStringExtra("inapp_signature"));
        else if ("com.android.vending.billing.RESPONSE_CODE".equals(str))
          checkResponseCode(paramIntent.getLongExtra("request_id", -1L), Constants.ResponseCode.valueOf(paramIntent.getIntExtra("response_code", Constants.ResponseCode.RESULT_ERROR.ordinal())));
      }
    }
  }

  public IBinder onBind(Intent paramIntent)
  {
    return this.m_serviceBinder;
  }

  public void onCreate()
  {
    super.onCreate();
    try
    {
      TangoApp.ensureInitialized();
      this.m_responseHandler = ((TangoApp)getApplication()).getResponseHandler();
      this.m_packagePrefix = (TangoApp.getInstance().getApplicationContext().getPackageName() + ".");
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      while (true)
        Log.e("BillingService", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  public void onDestroy()
  {
    super.onDestroy();
    try
    {
      if (this.m_marketBindResult)
      {
        unbindService(this);
        this.m_marketBindResult = false;
      }
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Log.e("BillingService", "Error while unbinding from service: ", localIllegalArgumentException);
    }
  }

  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    Log.i("BillingService", "MarketBillingService connected");
    this.m_marketBillingService = IMarketBillingService.Stub.asInterface(paramIBinder);
    runPendingRequests();
  }

  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    Log.w("BillingService", "Billing service disconnected");
    this.m_marketBillingService = null;
  }

  public void onStart(Intent paramIntent, int paramInt)
  {
    handleCommand(paramIntent, paramInt);
  }

  public boolean requestPurchase(String paramString1, String paramString2)
  {
    Log.i("BillingService", "requestPurchase: " + paramString2 + " at " + paramString1);
    recordPurchasedAttempt(paramString2);
    return new RequestPurchase(addPackagePrefix(paramString1), paramString2).runRequest();
  }

  public boolean restoreTransactions()
  {
    return new RestoreTransactions().runRequest();
  }

  public Bundle sendBillingRequestSafe(Bundle paramBundle)
    throws RemoteException
  {
    try
    {
      Bundle localBundle2 = this.m_marketBillingService.sendBillingRequest(paramBundle);
      return localBundle2;
    }
    catch (NullPointerException localNullPointerException)
    {
      Log.e("BillingService", "MarketBillingService failed:" + localNullPointerException);
      Bundle localBundle1 = new Bundle();
      localBundle1.putInt("RESPONSE_CODE", Constants.ResponseCode.RESULT_SERVICE_UNAVAILABLE.ordinal());
      localBundle1.putLong("REQUEST_ID", Constants.BILLING_RESPONSE_INVALID_REQUEST_ID);
      return localBundle1;
    }
  }

  public void setContext(Context paramContext)
  {
    attachBaseContext(paramContext);
  }

  abstract class BillingRequest
  {
    protected long m_requestId;
    private final int m_startId;

    public BillingRequest(int arg2)
    {
      int i;
      this.m_startId = i;
    }

    public int getStartId()
    {
      return this.m_startId;
    }

    protected void logResponseCode(String paramString, Bundle paramBundle)
    {
      Constants.ResponseCode localResponseCode = Constants.ResponseCode.valueOf(paramBundle.getInt("RESPONSE_CODE"));
      Log.e("BillingService", paramString + " received " + localResponseCode.toString());
    }

    protected Bundle makeRequestBundle(String paramString)
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("BILLING_REQUEST", paramString);
      localBundle.putInt("API_VERSION", 1);
      localBundle.putString("PACKAGE_NAME", BillingService.this.getPackageName());
      return localBundle;
    }

    protected void onRemoteException(RemoteException paramRemoteException)
    {
      Log.w("BillingService", "remote billing service crashed");
      BillingService.access$202(BillingService.this, null);
    }

    protected void responseCodeReceived(Constants.ResponseCode paramResponseCode)
    {
    }

    protected abstract long run()
      throws RemoteException;

    public boolean runIfConnected()
    {
      if (BillingService.this.m_marketBillingService != null)
        try
        {
          this.m_requestId = run();
          Log.d("BillingService", "request id: " + this.m_requestId);
          if (this.m_requestId >= 0L)
            BillingService.m_sentRequests.put(Long.valueOf(this.m_requestId), this);
          return true;
        }
        catch (RemoteException localRemoteException)
        {
          onRemoteException(localRemoteException);
        }
      return false;
    }

    public boolean runRequest()
    {
      if (runIfConnected())
        return true;
      if (BillingService.this.bindToMarketBillingService())
      {
        BillingService.m_pendingRequests.add(this);
        return true;
      }
      return false;
    }
  }

  public class BillingServiceBinder extends Binder
  {
    public BillingServiceBinder()
    {
    }

    public BillingService getService()
    {
      return BillingService.this;
    }
  }

  public class CheckBillingSupported extends BillingService.BillingRequest
  {
    public CheckBillingSupported()
    {
      super(-1);
    }

    protected long run()
      throws RemoteException
    {
      Bundle localBundle = makeRequestBundle("CHECK_BILLING_SUPPORTED");
      int i = BillingService.this.sendBillingRequestSafe(localBundle).getInt("RESPONSE_CODE");
      Log.i("BillingService", "CheckBillingSupported response code: " + Constants.ResponseCode.valueOf(i));
      if (i == Constants.ResponseCode.RESULT_OK.ordinal());
      for (boolean bool = true; ; bool = false)
      {
        BillingService.this.m_responseHandler.checkBillingSupported(bool);
        return Constants.BILLING_RESPONSE_INVALID_REQUEST_ID;
      }
    }
  }

  public class ConfirmNotifications extends BillingService.BillingRequest
  {
    final String[] m_notifyIds;

    public ConfirmNotifications(int paramArrayOfString, String[] arg3)
    {
      super(paramArrayOfString);
      Object localObject;
      this.m_notifyIds = localObject;
    }

    protected long run()
      throws RemoteException
    {
      Bundle localBundle1 = makeRequestBundle("CONFIRM_NOTIFICATIONS");
      localBundle1.putStringArray("NOTIFY_IDS", this.m_notifyIds);
      Bundle localBundle2 = BillingService.this.sendBillingRequestSafe(localBundle1);
      logResponseCode("confirmNotifications", localBundle2);
      return localBundle2.getLong("REQUEST_ID", Constants.BILLING_RESPONSE_INVALID_REQUEST_ID);
    }
  }

  public class GetPurchaseInformation extends BillingService.BillingRequest
  {
    long m_nonce;
    final String[] m_notifyIds;

    public GetPurchaseInformation(int paramArrayOfString, String[] arg3)
    {
      super(paramArrayOfString);
      Object localObject;
      this.m_notifyIds = localObject;
    }

    protected void onRemoteException(RemoteException paramRemoteException)
    {
      super.onRemoteException(paramRemoteException);
      Security.removeNonce(this.m_nonce);
    }

    protected long run()
      throws RemoteException
    {
      this.m_nonce = Security.generateNonce();
      Bundle localBundle1 = makeRequestBundle("GET_PURCHASE_INFORMATION");
      localBundle1.putLong("NONCE", this.m_nonce);
      localBundle1.putStringArray("NOTIFY_IDS", this.m_notifyIds);
      Bundle localBundle2 = BillingService.this.sendBillingRequestSafe(localBundle1);
      logResponseCode("getPurchaseInformation", localBundle2);
      return localBundle2.getLong("REQUEST_ID", Constants.BILLING_RESPONSE_INVALID_REQUEST_ID);
    }
  }

  public class RequestPurchase extends BillingService.BillingRequest
  {
    public final String m_developerPayload;
    public final String m_productId;

    public RequestPurchase(String arg2)
    {
      this(str, null);
    }

    public RequestPurchase(String paramString1, String arg3)
    {
      super(-1);
      this.m_productId = paramString1;
      Object localObject;
      this.m_developerPayload = localObject;
    }

    protected void responseCodeReceived(Constants.ResponseCode paramResponseCode)
    {
      BillingService.this.m_responseHandler.responseCodeReceived(this, paramResponseCode);
    }

    protected long run()
      throws RemoteException
    {
      Bundle localBundle1 = makeRequestBundle("REQUEST_PURCHASE");
      localBundle1.putString("ITEM_ID", this.m_productId);
      if (this.m_developerPayload != null)
        localBundle1.putString("DEVELOPER_PAYLOAD", this.m_developerPayload);
      Bundle localBundle2 = BillingService.this.sendBillingRequestSafe(localBundle1);
      PendingIntent localPendingIntent = (PendingIntent)localBundle2.getParcelable("PURCHASE_INTENT");
      if (localPendingIntent == null)
      {
        Log.e("BillingService", "Error with requestPurchase");
        return Constants.BILLING_RESPONSE_INVALID_REQUEST_ID;
      }
      Intent localIntent = new Intent();
      BillingService.this.m_responseHandler.buyPageIntentResponse(localPendingIntent, localIntent);
      return localBundle2.getLong("REQUEST_ID", Constants.BILLING_RESPONSE_INVALID_REQUEST_ID);
    }
  }

  public class RestoreTransactions extends BillingService.BillingRequest
  {
    long m_nonce;

    public RestoreTransactions()
    {
      super(-1);
    }

    protected void onRemoteException(RemoteException paramRemoteException)
    {
      super.onRemoteException(paramRemoteException);
      Security.removeNonce(this.m_nonce);
    }

    protected void responseCodeReceived(Constants.ResponseCode paramResponseCode)
    {
      BillingService.this.m_responseHandler.responseCodeReceived(this, paramResponseCode);
    }

    protected long run()
      throws RemoteException
    {
      this.m_nonce = Security.generateNonce();
      Bundle localBundle1 = makeRequestBundle("RESTORE_TRANSACTIONS");
      localBundle1.putLong("NONCE", this.m_nonce);
      Bundle localBundle2 = BillingService.this.sendBillingRequestSafe(localBundle1);
      logResponseCode("restoreTransactions", localBundle2);
      if (Constants.ResponseCode.valueOf(localBundle2.getInt("RESPONSE_CODE")) == Constants.ResponseCode.RESULT_OK)
        BillingService.access$502(BillingService.this, true);
      return localBundle2.getLong("REQUEST_ID", Constants.BILLING_RESPONSE_INVALID_REQUEST_ID);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.service.BillingService
 * JD-Core Version:    0.6.2
 */