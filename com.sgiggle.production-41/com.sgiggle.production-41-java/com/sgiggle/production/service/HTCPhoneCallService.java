package com.sgiggle.production.service;

import com.htc.lockscreen.idlescreen.phonecall.IdlePhoneCallService;
import com.sgiggle.media_engine.MediaEngineMessage.TerminateCallMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.CallHandler;
import com.sgiggle.production.CallSession;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.WrongTangoRuntimeVersionException;
import com.sgiggle.util.Log;

public class HTCPhoneCallService extends IdlePhoneCallService
{
  private static final String TAG = "Tango.HTCPhoneCallService";

  public void answerCall(int paramInt)
  {
    Log.d("Tango.HTCPhoneCallService", "answerCall(): " + paramInt);
    CallHandler.getDefault().answerIncomingCall();
  }

  public void onCreate()
  {
    super.onCreate();
    try
    {
      TangoApp.ensureInitialized();
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      Log.e("Tango.HTCPhoneCallService", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  public void rejectCall(int paramInt)
  {
    Log.d("Tango.HTCPhoneCallService", "rejectCall(): " + paramInt);
    CallSession localCallSession = CallHandler.getDefault().getCallSession();
    if (localCallSession != null)
      MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.TerminateCallMessage(localCallSession.getPeerAccountId()));
  }

  public void sendMessage(int paramInt)
  {
    Log.d("Tango.HTCPhoneCallService", "sendMessage(): " + paramInt);
  }

  public void silentCall(int paramInt)
  {
    Log.d("Tango.HTCPhoneCallService", "silentCall(): " + paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.service.HTCPhoneCallService
 * JD-Core Version:    0.6.2
 */