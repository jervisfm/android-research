package com.sgiggle.production.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import com.sgiggle.media_engine.MediaEngineMessage.KeepTangoPushNotificationAliveMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.Utils;
import com.sgiggle.production.WrongTangoRuntimeVersionException;
import com.sgiggle.util.Log;

public class MessageService extends Service
{
  private static final String ACTION_KEEPALIVE = "tango.service.KEEP_ALIVE";
  public static final String ACTION_STOP = "tango.service.STOP";
  private static final long KEEP_ALIVE_INTERVAL = 480000L;
  private static final String PREF_STARTED = "isStarted";
  private static final String TAG = "Tango.MessageService";
  private static final long WAKE_LOCK_ACQUIRED_TIMEOUT = 15000L;
  private SharedPreferences m_prefs;
  private boolean m_started;

  private void keepAlive()
  {
    if (!Utils.readRegistrationFlag(this))
    {
      Log.d("Tango.MessageService", "Tango registration not sent, stop Alarm Service");
      setKeepAlives(false);
      return;
    }
    if (!Utils.isNetworkAvailable(this))
    {
      Log.d("Tango.MessageService", "Network not available, don't send heartbeat");
      return;
    }
    Log.d("Tango.MessageService", "keepAlive(): acquire wake-lock: Timeout=15000");
    Utils.acquirePartialWakeLock(this, 15000L, "Tango.MessageService");
    MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.KeepTangoPushNotificationAliveMessage());
  }

  private void setKeepAlives(boolean paramBoolean)
  {
    Log.d("Tango.MessageService", "setKeepAlives(): bStart = " + paramBoolean + " m_started " + this.m_started);
    if (this.m_started == paramBoolean)
      return;
    Intent localIntent = new Intent();
    localIntent.setClass(this, getClass());
    localIntent.setAction("tango.service.KEEP_ALIVE");
    PendingIntent localPendingIntent = PendingIntent.getService(this, 0, localIntent, 0);
    AlarmManager localAlarmManager = (AlarmManager)getSystemService("alarm");
    if (paramBoolean)
      localAlarmManager.setRepeating(0, 480000L + System.currentTimeMillis(), 480000L, localPendingIntent);
    while (true)
    {
      setStarted(paramBoolean);
      return;
      localAlarmManager.cancel(localPendingIntent);
    }
  }

  private void setStarted(boolean paramBoolean)
  {
    this.m_prefs.edit().putBoolean("isStarted", paramBoolean).commit();
    this.m_started = paramBoolean;
  }

  private boolean wasStarted()
  {
    return this.m_prefs.getBoolean("isStarted", false);
  }

  public IBinder onBind(Intent paramIntent)
  {
    Log.d("Tango.MessageService", "onBind()");
    return null;
  }

  public void onCreate()
  {
    try
    {
      TangoApp.ensureInitialized();
      Log.d("Tango.MessageService", "onCreate()");
      super.onCreate();
      this.m_prefs = getSharedPreferences("Tango.MessageService", 0);
      if (wasStarted())
      {
        Log.d("Tango.MessageService", "onCreate(): Service was crashed. Cleanup...");
        setKeepAlives(false);
      }
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      while (true)
        Log.e("Tango.MessageService", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  public void onDestroy()
  {
    Log.d("Tango.MessageService", "onDestroy() started = " + this.m_started);
    super.onDestroy();
    if (this.m_started)
      setKeepAlives(false);
  }

  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    Log.d("Tango.MessageService", "onStartCommand(): intent = " + paramIntent);
    if ((paramIntent != null) && ("tango.service.KEEP_ALIVE".equals(paramIntent.getAction())))
    {
      keepAlive();
      return 1;
    }
    if ((paramIntent != null) && ("tango.service.STOP".equals(paramIntent.getAction())) && (this.m_started))
    {
      setKeepAlives(false);
      stopSelf();
      return 1;
    }
    setKeepAlives(true);
    return 1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.service.MessageService
 * JD-Core Version:    0.6.2
 */