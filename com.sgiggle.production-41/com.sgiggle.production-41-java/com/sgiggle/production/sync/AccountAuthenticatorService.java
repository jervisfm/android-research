package com.sgiggle.production.sync;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.NetworkErrorException;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import com.sgiggle.util.Log;

public class AccountAuthenticatorService extends Service
{
  private static final String TAG = "Tango.AccountAuthenticatorService";
  private static AccountAuthenticatorImpl s_accountAuthenticator = null;

  private AccountAuthenticatorImpl getAuthenticator()
  {
    if (s_accountAuthenticator == null)
      s_accountAuthenticator = new AccountAuthenticatorImpl(this);
    return s_accountAuthenticator;
  }

  public IBinder onBind(Intent paramIntent)
  {
    boolean bool = paramIntent.getAction().equals("android.accounts.AccountAuthenticator");
    IBinder localIBinder = null;
    if (bool)
      localIBinder = getAuthenticator().getIBinder();
    return localIBinder;
  }

  private static class AccountAuthenticatorImpl extends AbstractAccountAuthenticator
  {
    private Context m_context;

    public AccountAuthenticatorImpl(Context paramContext)
    {
      super();
      this.m_context = paramContext;
    }

    public Bundle addAccount(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString1, String paramString2, String[] paramArrayOfString, Bundle paramBundle)
      throws NetworkErrorException
    {
      Bundle localBundle = new Bundle();
      Intent localIntent = new Intent(this.m_context, LoginActivity.class);
      localIntent.putExtra("accountAuthenticatorResponse", paramAccountAuthenticatorResponse);
      localBundle.putParcelable("intent", localIntent);
      return localBundle;
    }

    public Bundle confirmCredentials(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, Bundle paramBundle)
    {
      Log.d("Tango.AccountAuthenticatorService", "confirmCredentials");
      return null;
    }

    public Bundle editProperties(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString)
    {
      Log.d("Tango.AccountAuthenticatorService", "editProperties");
      return null;
    }

    public Bundle getAuthToken(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
      throws NetworkErrorException
    {
      Log.d("Tango.AccountAuthenticatorService", "getAuthToken");
      return null;
    }

    public String getAuthTokenLabel(String paramString)
    {
      Log.d("Tango.AccountAuthenticatorService", "getAuthTokenLabel");
      return null;
    }

    public Bundle hasFeatures(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String[] paramArrayOfString)
      throws NetworkErrorException
    {
      Log.d("Tango.AccountAuthenticatorService", "hasFeatures: " + paramArrayOfString);
      return null;
    }

    public Bundle updateCredentials(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
    {
      Log.d("Tango.AccountAuthenticatorService", "updateCredentials");
      return null;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.sync.AccountAuthenticatorService
 * JD-Core Version:    0.6.2
 */