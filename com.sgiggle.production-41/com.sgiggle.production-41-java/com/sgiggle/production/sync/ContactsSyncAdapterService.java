package com.sgiggle.production.sync;

import android.accounts.Account;
import android.accounts.OperationCanceledException;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.StatusUpdates;
import android.text.TextUtils;
import com.sgiggle.production.ContactListActivity;
import com.sgiggle.production.TangoApp;
import com.sgiggle.production.Utils.UIContact;
import com.sgiggle.production.WrongTangoRuntimeVersionException;
import com.sgiggle.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class ContactsSyncAdapterService extends Service
{
  private static final String CUSTOM_IM_PROTOCOL = "TangoSyncAdapter";
  private static final String IM_HANDLE_PREFIX = "im_handle_";
  private static final String TAG = "Tango.ContactsSyncAdapterService";
  private static ContentResolver m_contentResolver = null;
  private static SyncAdapterImpl s_syncAdapter = null;

  private static void addContact(Context paramContext, Account paramAccount, Utils.UIContact paramUIContact)
  {
    String str1 = paramUIContact.displayName();
    String str2 = paramUIContact.m_accountId;
    Log.d("Tango.ContactsSyncAdapterService", "ADD: peerName=" + str1 + ", peerAid = [" + str2 + "]");
    ArrayList localArrayList = new ArrayList();
    ContentProviderOperation.Builder localBuilder1 = ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build());
    localBuilder1.withValue("account_name", paramAccount.name);
    localBuilder1.withValue("account_type", paramAccount.type);
    localBuilder1.withValue("sync1", str2);
    localArrayList.add(localBuilder1.build());
    ContentProviderOperation.Builder localBuilder2 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
    localBuilder2.withValueBackReference("raw_contact_id", 0);
    localBuilder2.withValue("mimetype", "vnd.android.cursor.item/name");
    localBuilder2.withValue("data3", paramUIContact.m_lastName);
    localBuilder2.withValue("data2", paramUIContact.m_firstName);
    localArrayList.add(localBuilder2.build());
    if (!TextUtils.isEmpty(paramUIContact.m_phoneNumber))
    {
      ContentProviderOperation.Builder localBuilder4 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
      localBuilder4.withValueBackReference("raw_contact_id", 0);
      localBuilder4.withValue("mimetype", "vnd.android.cursor.item/phone_v2");
      localBuilder4.withValue("data2", Integer.valueOf(paramUIContact.m_phoneType));
      localBuilder4.withValue("data1", paramUIContact.m_phoneNumber);
      localArrayList.add(localBuilder4.build());
    }
    ContentProviderOperation.Builder localBuilder3 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
    localBuilder3.withValueBackReference("raw_contact_id", 0);
    localBuilder3.withValue("mimetype", "com.android.htccontacts/chat_capability");
    localBuilder3.withValue("data1", str2);
    localBuilder3.withValue("data2", paramContext.getString(2131296395));
    String str3;
    if (TextUtils.isEmpty(paramUIContact.m_phoneNumber))
      str3 = paramUIContact.m_email;
    while (true)
    {
      localBuilder3.withValue("data3", str3);
      localBuilder3.withValue("data4", str1);
      localBuilder3.withValue("data5", String.valueOf(paramUIContact.m_deviceContactId));
      localArrayList.add(localBuilder3.build());
      try
      {
        ContentProviderResult[] arrayOfContentProviderResult = m_contentResolver.applyBatch("com.android.contacts", localArrayList);
        if (arrayOfContentProviderResult.length > 0)
        {
          ContentProviderResult localContentProviderResult = arrayOfContentProviderResult[0];
          long l = ContentUris.parseId(localContentProviderResult.uri);
          Log.d("Tango.ContactsSyncAdapterService", "... (Added) contact: " + str1 + ", uri = " + localContentProviderResult.uri);
          localArrayList.clear();
          updateContactStatus(paramContext, localArrayList, str2, l, paramContext.getString(2131296407));
          if (localArrayList.size() > 0)
            m_contentResolver.applyBatch("com.android.contacts", localArrayList);
        }
        return;
        str3 = paramUIContact.m_phoneNumber;
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
    }
  }

  private static void deleteContact(Context paramContext, Account paramAccount, ArrayList<ContentProviderOperation> paramArrayList, long paramLong)
  {
    Log.d("Tango.ContactsSyncAdapterService", "DELETE: rawContactId=" + paramLong);
    ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("account_name", paramAccount.name).appendQueryParameter("account_type", paramAccount.type).appendQueryParameter("caller_is_syncadapter", "true").build());
    String[] arrayOfString = new String[1];
    arrayOfString[0] = String.valueOf(paramLong);
    localBuilder.withSelection("_id=?", arrayOfString);
    paramArrayList.add(localBuilder.build());
  }

  private SyncAdapterImpl getSyncAdapter()
  {
    if (s_syncAdapter == null)
      s_syncAdapter = new SyncAdapterImpl(this);
    return s_syncAdapter;
  }

  private static void performSync(Context paramContext, Account paramAccount, Bundle paramBundle, String paramString, ContentProviderClient paramContentProviderClient, SyncResult paramSyncResult)
    throws OperationCanceledException
  {
    Log.i("Tango.ContactsSyncAdapterService", "performSync: " + paramAccount);
    List localList = ContactListActivity.getTangoContacts();
    if (localList == null)
      Log.i("Tango.ContactsSyncAdapterService", "performSync: Tango contacts are not yet available. Do nothing.");
    ArrayList localArrayList;
    do
    {
      return;
      m_contentResolver = paramContext.getContentResolver();
      HashMap localHashMap1 = new HashMap();
      HashMap localHashMap2 = new HashMap();
      Uri localUri = ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("account_name", paramAccount.name).appendQueryParameter("account_type", paramAccount.type).appendQueryParameter("caller_is_syncadapter", "true").build();
      Cursor localCursor = m_contentResolver.query(localUri, new String[] { "_id", "sync1" }, "deleted<>1", null, null);
      while ((localCursor != null) && (localCursor.moveToNext()))
      {
        localHashMap1.put(localCursor.getString(1), Long.valueOf(localCursor.getLong(0)));
        localHashMap2.put(localCursor.getString(1), Long.valueOf(localCursor.getLong(0)));
      }
      if (localCursor != null)
        localCursor.close();
      localArrayList = new ArrayList();
      while (true)
      {
        String str;
        try
        {
          Iterator localIterator1 = localList.iterator();
          if (!localIterator1.hasNext())
            break;
          Utils.UIContact localUIContact = (Utils.UIContact)localIterator1.next();
          str = localUIContact.m_accountId;
          if (localHashMap1.get(str) == null)
          {
            addContact(paramContext, paramAccount, localUIContact);
            continue;
          }
        }
        catch (Exception localException)
        {
          localException.printStackTrace();
          return;
        }
        updateContactStatus(paramContext, localArrayList, str, ((Long)localHashMap1.get(str)).longValue(), paramContext.getString(2131296407));
        localHashMap2.remove(str);
      }
      if (localArrayList.size() > 0)
        m_contentResolver.applyBatch("com.android.contacts", localArrayList);
      localArrayList.clear();
      Iterator localIterator2 = localHashMap2.entrySet().iterator();
      while (localIterator2.hasNext())
        deleteContact(paramContext, paramAccount, localArrayList, ((Long)((Map.Entry)localIterator2.next()).getValue()).longValue());
    }
    while (localArrayList.size() <= 0);
    m_contentResolver.applyBatch("com.android.contacts", localArrayList);
  }

  private static void updateContactStatus(Context paramContext, ArrayList<ContentProviderOperation> paramArrayList, String paramString1, long paramLong, String paramString2)
  {
    Log.d("Tango.ContactsSyncAdapterService", "UPDATE: rawContactId = " + paramLong + ", peerAid = " + paramString1);
    String str = paramContext.getPackageName();
    Uri localUri = Uri.withAppendedPath(ContentUris.withAppendedId(ContactsContract.RawContacts.CONTENT_URI, paramLong), "entity");
    Cursor localCursor = m_contentResolver.query(localUri, new String[] { "sourceid", "data_id", "mimetype" }, null, null, null);
    try
    {
      while (localCursor.moveToNext())
        if ((!localCursor.isNull(1)) && (localCursor.getString(2).equals("com.android.htccontacts/chat_capability")))
        {
          ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newInsert(ContactsContract.StatusUpdates.CONTENT_URI);
          localBuilder.withValue("presence_data_id", Long.valueOf(localCursor.getLong(1)));
          localBuilder.withValue("protocol", Integer.valueOf(-1));
          localBuilder.withValue("custom_protocol", "TangoSyncAdapter");
          localBuilder.withValue("im_account", str);
          localBuilder.withValue("im_handle", "im_handle_" + paramString1);
          localBuilder.withValue("mode", Integer.valueOf(10));
          localBuilder.withValue("status_res_package", str);
          localBuilder.withValue("status_label", Integer.valueOf(2131296257));
          localBuilder.withValue("status_icon", Integer.valueOf(2130837716));
          paramArrayList.add(localBuilder.build());
        }
    }
    finally
    {
      localCursor.close();
    }
  }

  public IBinder onBind(Intent paramIntent)
  {
    return getSyncAdapter().getSyncAdapterBinder();
  }

  public void onCreate()
  {
    super.onCreate();
    try
    {
      TangoApp.ensureInitialized();
      return;
    }
    catch (WrongTangoRuntimeVersionException localWrongTangoRuntimeVersionException)
    {
      Log.e("Tango.ContactsSyncAdapterService", "Initialization failed: " + localWrongTangoRuntimeVersionException.toString());
    }
  }

  private static class SyncAdapterImpl extends AbstractThreadedSyncAdapter
  {
    private Context m_context;

    public SyncAdapterImpl(Context paramContext)
    {
      super(true);
      this.m_context = paramContext;
    }

    public void onPerformSync(Account paramAccount, Bundle paramBundle, String paramString, ContentProviderClient paramContentProviderClient, SyncResult paramSyncResult)
    {
      try
      {
        ContactsSyncAdapterService.performSync(this.m_context, paramAccount, paramBundle, paramString, paramContentProviderClient, paramSyncResult);
        return;
      }
      catch (OperationCanceledException localOperationCanceledException)
      {
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.sync.ContactsSyncAdapterService
 * JD-Core Version:    0.6.2
 */