package com.sgiggle.production.sync;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.os.AsyncTask;
import android.os.Bundle;
import com.sgiggle.util.Log;

public class LoginActivity extends AccountAuthenticatorActivity
{
  private static final String TAG = "Tango.sync.LoginActivity";

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903124);
    String str = getString(2131296392);
    showDialog(0);
    if ((paramBundle == null) && (str.length() > 0) && (str.length() > 0))
      new LoginTask(null).execute(new String[] { str, str });
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    ProgressDialog localProgressDialog = new ProgressDialog(this);
    localProgressDialog.setMessage(getString(2131296394));
    localProgressDialog.setIndeterminate(true);
    localProgressDialog.setCancelable(false);
    localProgressDialog.setOwnerActivity(this);
    return localProgressDialog;
  }

  private class LoginTask extends AsyncTask<String, Void, Boolean>
  {
    private LoginTask()
    {
    }

    public Boolean doInBackground(String[] paramArrayOfString)
    {
      String str1 = paramArrayOfString[0];
      String str2 = paramArrayOfString[1];
      try
      {
        Thread.sleep(2000L);
        str3 = LoginActivity.this.getString(2131296391);
        if (AccountManager.get(LoginActivity.this).getAccountsByType(str3).length > 0)
        {
          Log.d("Tango.sync.LoginActivity", "Tango sync account has been created. Do nothing.");
          return Boolean.valueOf(true);
        }
      }
      catch (Exception localException)
      {
        String str3;
        while (true)
          localException.printStackTrace();
        Account localAccount = new Account(str1, str3);
        if (AccountManager.get(LoginActivity.this).addAccountExplicitly(localAccount, str2, null))
        {
          ContentResolver.setIsSyncable(localAccount, "com.android.contacts", 1);
          ContentResolver.setSyncAutomatically(localAccount, "com.android.contacts", true);
          Bundle localBundle = new Bundle();
          localBundle.putString("authAccount", localAccount.name);
          localBundle.putString("accountType", localAccount.type);
          LoginActivity.this.setAccountAuthenticatorResult(localBundle);
          return Boolean.valueOf(true);
        }
      }
      return Boolean.valueOf(false);
    }

    public void onPostExecute(Boolean paramBoolean)
    {
      LoginActivity.this.removeDialog(0);
      if (paramBoolean.booleanValue())
        LoginActivity.this.finish();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.sync.LoginActivity
 * JD-Core Version:    0.6.2
 */