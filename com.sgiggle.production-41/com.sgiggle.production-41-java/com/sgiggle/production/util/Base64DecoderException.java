package com.sgiggle.production.util;

public class Base64DecoderException extends Exception
{
  private static final long serialVersionUID = 1L;

  public Base64DecoderException()
  {
  }

  public Base64DecoderException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.Base64DecoderException
 * JD-Core Version:    0.6.2
 */