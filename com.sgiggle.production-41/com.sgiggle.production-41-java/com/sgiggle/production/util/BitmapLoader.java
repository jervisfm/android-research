package com.sgiggle.production.util;

import android.graphics.Bitmap;
import android.widget.ImageView;
import java.lang.ref.SoftReference;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

abstract class BitmapLoader
{
  private ConcurrentHashMap<Object, SoftReference<Bitmap>> m_bitmaps = new ConcurrentHashMap();
  private BitmapLoaderThread m_loadBitmapThread;

  public void addLoadTask(BitmapLoaderThread.LoadableImage paramLoadableImage, ImageView paramImageView)
  {
    this.m_loadBitmapThread.queueTask(paramLoadableImage, paramImageView);
  }

  public void clear()
  {
    this.m_bitmaps.clear();
  }

  protected abstract BitmapLoaderThread createLoadBitmapThread();

  protected ConcurrentHashMap<Object, SoftReference<Bitmap>> getBitmaps()
  {
    return this.m_bitmaps;
  }

  public Bitmap getLoadedBitmap(BitmapLoaderThread.LoadableImage paramLoadableImage)
  {
    if (this.m_bitmaps == null)
      return null;
    SoftReference localSoftReference = (SoftReference)this.m_bitmaps.get(paramLoadableImage.getImageId());
    if (localSoftReference == null)
      return null;
    if ((Bitmap)localSoftReference.get() == null)
      this.m_bitmaps.remove(paramLoadableImage.getImageId());
    return (Bitmap)localSoftReference.get();
  }

  public void start()
  {
    this.m_loadBitmapThread = createLoadBitmapThread();
    this.m_loadBitmapThread.stop = false;
    this.m_loadBitmapThread.setPriority(1);
    this.m_loadBitmapThread.start();
  }

  public void stop()
  {
    this.m_loadBitmapThread.stop = true;
    this.m_loadBitmapThread.interrupt();
    this.m_loadBitmapThread.taskQueue.clear();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.BitmapLoader
 * JD-Core Version:    0.6.2
 */