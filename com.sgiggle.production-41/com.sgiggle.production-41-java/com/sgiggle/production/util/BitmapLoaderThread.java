package com.sgiggle.production.util;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.sgiggle.util.Log;
import java.lang.ref.SoftReference;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

public abstract class BitmapLoaderThread extends Thread
{
  private static final String TAG = "Tango.BitmapLoaderThread";
  protected ConcurrentHashMap<Object, SoftReference<Bitmap>> m_imageMap;
  public boolean stop = true;
  public Stack<LoadBitmapRunnable> taskQueue = new Stack();
  public Set<Object> taskedQueueIds = new HashSet();

  public BitmapLoaderThread(ConcurrentHashMap<Object, SoftReference<Bitmap>> paramConcurrentHashMap)
  {
    this.m_imageMap = paramConcurrentHashMap;
  }

  protected abstract Bitmap loadImage(Object paramObject);

  public boolean queueTask(LoadableImage paramLoadableImage, ImageView paramImageView)
  {
    synchronized (this.taskQueue)
    {
      Object localObject2 = paramLoadableImage.getImageId();
      if (this.taskedQueueIds.contains(localObject2))
      {
        Log.d("Tango.BitmapLoaderThread", "queueTask: ignoring, task already queued for id=" + localObject2);
        return false;
      }
      this.taskedQueueIds.add(localObject2);
      LoadBitmapRunnable localLoadBitmapRunnable = new LoadBitmapRunnable(paramLoadableImage, paramImageView);
      this.taskQueue.push(localLoadBitmapRunnable);
      this.taskQueue.notify();
      return true;
    }
  }

  // ERROR //
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 36	com/sgiggle/production/util/BitmapLoaderThread:stop	Z
    //   4: ifne +81 -> 85
    //   7: aload_0
    //   8: getfield 29	com/sgiggle/production/util/BitmapLoaderThread:taskQueue	Ljava/util/Stack;
    //   11: invokevirtual 100	java/util/Stack:size	()I
    //   14: ifne +22 -> 36
    //   17: aload_0
    //   18: getfield 29	com/sgiggle/production/util/BitmapLoaderThread:taskQueue	Ljava/util/Stack;
    //   21: astore 6
    //   23: aload 6
    //   25: monitorenter
    //   26: aload_0
    //   27: getfield 29	com/sgiggle/production/util/BitmapLoaderThread:taskQueue	Ljava/util/Stack;
    //   30: invokevirtual 103	java/lang/Object:wait	()V
    //   33: aload 6
    //   35: monitorexit
    //   36: aload_0
    //   37: getfield 29	com/sgiggle/production/util/BitmapLoaderThread:taskQueue	Ljava/util/Stack;
    //   40: invokevirtual 100	java/util/Stack:size	()I
    //   43: ifeq +34 -> 77
    //   46: aload_0
    //   47: getfield 29	com/sgiggle/production/util/BitmapLoaderThread:taskQueue	Ljava/util/Stack;
    //   50: astore_3
    //   51: aload_3
    //   52: monitorenter
    //   53: aload_0
    //   54: getfield 29	com/sgiggle/production/util/BitmapLoaderThread:taskQueue	Ljava/util/Stack;
    //   57: invokevirtual 106	java/util/Stack:pop	()Ljava/lang/Object;
    //   60: checkcast 81	com/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable
    //   63: astore 5
    //   65: aload_3
    //   66: monitorexit
    //   67: aload 5
    //   69: ifnull +8 -> 77
    //   72: aload 5
    //   74: invokevirtual 108	com/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable:run	()V
    //   77: invokestatic 112	java/lang/Thread:interrupted	()Z
    //   80: istore_2
    //   81: iload_2
    //   82: ifeq -82 -> 0
    //   85: return
    //   86: astore 7
    //   88: aload 6
    //   90: monitorexit
    //   91: aload 7
    //   93: athrow
    //   94: astore_1
    //   95: return
    //   96: astore 4
    //   98: aload_3
    //   99: monitorexit
    //   100: aload 4
    //   102: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   26	36	86	finally
    //   88	91	86	finally
    //   0	26	94	java/lang/InterruptedException
    //   36	53	94	java/lang/InterruptedException
    //   72	77	94	java/lang/InterruptedException
    //   77	81	94	java/lang/InterruptedException
    //   91	94	94	java/lang/InterruptedException
    //   100	103	94	java/lang/InterruptedException
    //   53	67	96	finally
    //   98	100	96	finally
  }

  public class LoadBitmapRunnable
    implements Runnable
  {
    private BitmapLoaderThread.LoadableImage m_model;
    private ImageView m_view;

    public LoadBitmapRunnable(BitmapLoaderThread.LoadableImage paramImageView, ImageView arg3)
    {
      this.m_model = paramImageView;
      Object localObject;
      this.m_view = localObject;
      this.m_view.setTag(Integer.valueOf(this.m_model.getViewTag()));
    }

    public void run()
    {
      Object localObject = this.m_model.getImageId();
      final Bitmap localBitmap = BitmapLoaderThread.this.loadImage(this.m_model.getDataToLoadImage());
      if (localBitmap == null)
        this.m_model.onLoadFailed();
      while (true)
      {
        BitmapLoaderThread.this.taskedQueueIds.remove(localObject);
        do
        {
          return;
          BitmapLoaderThread.this.m_imageMap.put(localObject, new SoftReference(localBitmap));
        }
        while ((this.m_view == null) || (this.m_view.getTag() == null));
        if (((Integer)this.m_view.getTag()).intValue() == this.m_model.getViewTag())
          this.m_view.post(new Runnable()
          {
            public void run()
            {
              BitmapLoaderThread.LoadBitmapRunnable.this.m_view.setImageBitmap(localBitmap);
            }
          });
      }
    }
  }

  public static abstract interface LoadableImage
  {
    public abstract Object getDataToLoadImage();

    public abstract Object getImageId();

    public abstract int getViewTag();

    public abstract void onLoadFailed();

    public abstract boolean shouldHaveImage();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.BitmapLoaderThread
 * JD-Core Version:    0.6.2
 */