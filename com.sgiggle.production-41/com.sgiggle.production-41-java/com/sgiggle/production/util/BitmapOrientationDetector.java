package com.sgiggle.production.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.ExifInterface;
import android.net.Uri;
import com.sgiggle.util.Log;
import java.io.IOException;

public class BitmapOrientationDetector
{
  private static final String TAG = "BitmapOrientationDetector";

  private static int exifOrientationToDegrees(int paramInt)
  {
    if (paramInt == 6)
      return 90;
    if (paramInt == 3)
      return 180;
    if (paramInt == 8)
      return 270;
    return 0;
  }

  public static int getOrientationOfImage(Context paramContext, Uri paramUri)
  {
    if (paramUri.getScheme().equals("content"))
    {
      String[] arrayOfString = { "orientation" };
      Cursor localCursor = paramContext.getContentResolver().query(paramUri, arrayOfString, null, null, null);
      if (localCursor.moveToFirst())
        return localCursor.getInt(0);
    }
    else if (paramUri.getScheme().equals("file"))
    {
      try
      {
        int i = exifOrientationToDegrees(new ExifInterface(paramUri.getPath()).getAttributeInt("Orientation", 1));
        return i;
      }
      catch (IOException localIOException)
      {
        Log.e("BitmapOrientationDetector", "Error checking exif", localIOException);
      }
    }
    return 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.BitmapOrientationDetector
 * JD-Core Version:    0.6.2
 */