package com.sgiggle.production.util;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BitmapTransformer
{
  private static final int BITMAP_PIXEL_LIMIT_FOR_MEMORY = 1500000;
  private static final int JPEG_FILE_QUALITY = 30;
  private static final String TAG = "com.sgiggle.production.util.BitmapTransformer";

  public static Bitmap downscale(Bitmap paramBitmap, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    if (paramBitmap == null)
      return null;
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    if (paramInt1 > i);
    for (int k = i; ; k = paramInt1)
    {
      if (paramInt2 > j);
      for (int m = j; ; m = paramInt2)
      {
        float f1 = k / i;
        float f2 = m / j;
        if (paramBoolean)
          if (f1 < f2)
            f1 = f2;
        Bitmap localBitmap;
        while (true)
        {
          int n = (int)(0.5F + f1 * i);
          int i1 = (int)(0.5F + f1 * j);
          if (n < 1)
            n = 1;
          if (i1 < 1)
            i1 = 1;
          localBitmap = Bitmap.createScaledBitmap(paramBitmap, n, i1, true);
          if (!paramBoolean)
            break;
          return Bitmap.createBitmap(localBitmap, (n - k) / 2, (i1 - m) / 2, k, m);
          if (f1 >= f2)
            f1 = f2;
        }
        return localBitmap;
      }
    }
  }

  public static Bitmap loadFile(Context paramContext, final Uri paramUri)
  {
    return loadFile(new IBitmapStreamSource()
    {
      public InputStream getBitmapInputStream()
        throws IOException
      {
        return this.val$context.getContentResolver().openInputStream(paramUri);
      }
    });
  }

  // ERROR //
  public static Bitmap loadFile(IBitmapStreamSource paramIBitmapStreamSource)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokeinterface 54 1 0
    //   6: astore_3
    //   7: new 56	android/graphics/BitmapFactory$Options
    //   10: dup
    //   11: invokespecial 57	android/graphics/BitmapFactory$Options:<init>	()V
    //   14: astore 4
    //   16: aload 4
    //   18: iconst_1
    //   19: putfield 61	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   22: aload_3
    //   23: aconst_null
    //   24: aload 4
    //   26: invokestatic 67	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   29: pop
    //   30: aload 4
    //   32: getfield 70	android/graphics/BitmapFactory$Options:outWidth	I
    //   35: aload 4
    //   37: getfield 73	android/graphics/BitmapFactory$Options:outHeight	I
    //   40: imul
    //   41: istore 6
    //   43: iconst_1
    //   44: istore 7
    //   46: iload 6
    //   48: ldc 7
    //   50: if_icmple +18 -> 68
    //   53: iload 6
    //   55: iconst_4
    //   56: idiv
    //   57: istore 6
    //   59: iload 7
    //   61: iconst_2
    //   62: imul
    //   63: istore 7
    //   65: goto -19 -> 46
    //   68: aload 4
    //   70: iconst_0
    //   71: putfield 61	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   74: aload 4
    //   76: iload 7
    //   78: putfield 76	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   81: aload_3
    //   82: invokevirtual 81	java/io/InputStream:close	()V
    //   85: aload_0
    //   86: invokeinterface 54 1 0
    //   91: astore 8
    //   93: aload 8
    //   95: aconst_null
    //   96: aload 4
    //   98: invokestatic 67	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   101: astore 9
    //   103: aload 9
    //   105: astore_2
    //   106: aload 8
    //   108: invokevirtual 81	java/io/InputStream:close	()V
    //   111: aload_2
    //   112: areturn
    //   113: astore_1
    //   114: aconst_null
    //   115: astore_2
    //   116: aload_1
    //   117: invokevirtual 84	java/io/IOException:printStackTrace	()V
    //   120: aload_2
    //   121: areturn
    //   122: astore_1
    //   123: goto -7 -> 116
    //
    // Exception table:
    //   from	to	target	type
    //   0	43	113	java/io/IOException
    //   53	59	113	java/io/IOException
    //   68	103	113	java/io/IOException
    //   106	111	122	java/io/IOException
  }

  public static Bitmap loadFile(String paramString)
  {
    return loadFile(new IBitmapStreamSource()
    {
      public InputStream getBitmapInputStream()
        throws IOException
      {
        return new FileInputStream(new File(this.val$path));
      }
    });
  }

  public static Bitmap rotate(Bitmap paramBitmap, int paramInt)
  {
    if (paramBitmap == null)
      return null;
    Matrix localMatrix = new Matrix();
    if (paramInt != 0)
      localMatrix.preRotate(paramInt);
    return Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), localMatrix, false);
  }

  // ERROR //
  public static void saveFile(Bitmap paramBitmap, String paramString)
    throws java.lang.Exception
  {
    // Byte code:
    //   0: new 108	java/io/FileOutputStream
    //   3: dup
    //   4: aload_1
    //   5: invokespecial 109	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   8: astore_2
    //   9: aload_0
    //   10: getstatic 115	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   13: bipush 30
    //   15: aload_2
    //   16: invokevirtual 119	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   19: pop
    //   20: aload_2
    //   21: ifnull +7 -> 28
    //   24: aload_2
    //   25: invokevirtual 120	java/io/FileOutputStream:close	()V
    //   28: return
    //   29: astore 8
    //   31: ldc 13
    //   33: new 122	java/lang/StringBuilder
    //   36: dup
    //   37: invokespecial 123	java/lang/StringBuilder:<init>	()V
    //   40: ldc 125
    //   42: invokevirtual 129	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: aload_1
    //   46: invokevirtual 129	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   52: invokestatic 139	com/sgiggle/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   55: pop
    //   56: return
    //   57: astore 11
    //   59: aconst_null
    //   60: astore_2
    //   61: aload 11
    //   63: astore_3
    //   64: aload_3
    //   65: athrow
    //   66: astore 4
    //   68: aload_2
    //   69: ifnull +7 -> 76
    //   72: aload_2
    //   73: invokevirtual 120	java/io/FileOutputStream:close	()V
    //   76: aload 4
    //   78: athrow
    //   79: astore 5
    //   81: ldc 13
    //   83: new 122	java/lang/StringBuilder
    //   86: dup
    //   87: invokespecial 123	java/lang/StringBuilder:<init>	()V
    //   90: ldc 125
    //   92: invokevirtual 129	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   95: aload_1
    //   96: invokevirtual 129	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   99: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   102: invokestatic 139	com/sgiggle/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   105: pop
    //   106: goto -30 -> 76
    //   109: astore 10
    //   111: aload 10
    //   113: astore 4
    //   115: aconst_null
    //   116: astore_2
    //   117: goto -49 -> 68
    //   120: astore_3
    //   121: goto -57 -> 64
    //
    // Exception table:
    //   from	to	target	type
    //   24	28	29	java/lang/Exception
    //   0	9	57	java/lang/Exception
    //   9	20	66	finally
    //   64	66	66	finally
    //   72	76	79	java/lang/Exception
    //   0	9	109	finally
    //   9	20	120	java/lang/Exception
  }

  public static abstract interface IBitmapStreamSource
  {
    public abstract InputStream getBitmapInputStream()
      throws IOException;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.BitmapTransformer
 * JD-Core Version:    0.6.2
 */