package com.sgiggle.production.util;

import android.graphics.Bitmap;
import com.sgiggle.contacts.ContactStore;
import com.sgiggle.util.Log;
import java.lang.ref.SoftReference;
import java.util.concurrent.ConcurrentHashMap;

public class ContactThumbnailLoader extends BitmapLoader
{
  private static final String TAG = "Tango.ContactThumbnailLoader";
  private static ContactThumbnailLoader s_instance;

  public static ContactThumbnailLoader getInstance()
  {
    if (s_instance == null)
      s_instance = new ContactThumbnailLoader();
    return s_instance;
  }

  public void clear()
  {
    Log.d("Tango.ContactThumbnailLoader", "clear()");
    super.clear();
  }

  protected BitmapLoaderThread createLoadBitmapThread()
  {
    return new LoadThumbnailThread(getBitmaps());
  }

  public void start()
  {
    Log.d("Tango.ContactThumbnailLoader", "start()");
    super.start();
  }

  public void stop()
  {
    Log.d("Tango.ContactThumbnailLoader", "stop()");
    super.stop();
  }

  private class LoadThumbnailThread extends BitmapLoaderThread
  {
    public LoadThumbnailThread()
    {
      super();
    }

    protected Bitmap loadImage(Object paramObject)
    {
      return ContactStore.getPhotoByContactId(((Long)paramObject).longValue());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.ContactThumbnailLoader
 * JD-Core Version:    0.6.2
 */