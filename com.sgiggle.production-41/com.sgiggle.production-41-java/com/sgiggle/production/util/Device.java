package com.sgiggle.production.util;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.util.UUID;

public class Device
{
  public static int convertDpToPixel(int paramInt, Activity paramActivity)
  {
    return paramInt * getDpi(paramActivity) / 160;
  }

  public static int getDpi(Activity paramActivity)
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    paramActivity.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    return localDisplayMetrics.densityDpi;
  }

  public static String getUniqueDeviceId(Context paramContext)
  {
    TelephonyManager localTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
    String str1 = "" + localTelephonyManager.getDeviceId();
    String str2 = "" + localTelephonyManager.getSimSerialNumber();
    return new UUID(("" + Settings.Secure.getString(paramContext.getContentResolver(), "android_id")).hashCode(), str1.hashCode() << 32 | str2.hashCode()).toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.Device
 * JD-Core Version:    0.6.2
 */