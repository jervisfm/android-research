package com.sgiggle.production.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.sgiggle.util.Log;
import java.lang.ref.SoftReference;
import java.util.concurrent.ConcurrentHashMap;

public class FileImageLoader extends BitmapLoader
{
  private static final String TAG = "Tango.FileImageLoader";
  private static FileImageLoader s_instance;

  public static FileImageLoader getInstance()
  {
    if (s_instance == null)
      s_instance = new FileImageLoader();
    return s_instance;
  }

  protected BitmapLoaderThread createLoadBitmapThread()
  {
    return new LoadFileImageThread(getBitmaps());
  }

  private class LoadFileImageThread extends BitmapLoaderThread
  {
    public LoadFileImageThread()
    {
      super();
    }

    protected Bitmap loadImage(Object paramObject)
    {
      String str = (String)paramObject;
      if (TextUtils.isEmpty(str))
      {
        Log.d("Tango.FileImageLoader", "loadImageFromModel: Empty path, ignoring.");
        return null;
      }
      Log.d("Tango.FileImageLoader", "loadImageFromModel: Loading image from path=" + str);
      try
      {
        Bitmap localBitmap = BitmapFactory.decodeFile(str);
        return localBitmap;
      }
      catch (Exception localException)
      {
        Log.d("Tango.FileImageLoader", "loadImageFromModel: Failed to load image: " + localException);
      }
      return null;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.FileImageLoader
 * JD-Core Version:    0.6.2
 */