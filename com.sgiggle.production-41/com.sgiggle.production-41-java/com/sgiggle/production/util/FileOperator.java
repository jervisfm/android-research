package com.sgiggle.production.util;

import android.net.Uri;
import com.sgiggle.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class FileOperator
{
  private static final int BUF_SIZE = 1024;

  public static boolean copyFile(String paramString1, String paramString2)
  {
    FileInputStream localFileInputStream;
    FileOutputStream localFileOutputStream;
    try
    {
      localFileInputStream = new FileInputStream(paramString1);
      localFileOutputStream = new FileOutputStream(paramString2);
      byte[] arrayOfByte = new byte[1024];
      while (true)
      {
        int i = localFileInputStream.read(arrayOfByte);
        if (i == -1)
          break;
        localFileOutputStream.write(arrayOfByte, 0, i);
      }
    }
    catch (Exception localException)
    {
      Log.e("FileOperator", localException.getMessage());
      return false;
    }
    localFileInputStream.close();
    localFileOutputStream.flush();
    localFileOutputStream.close();
    return true;
  }

  public static boolean deleteFile(String paramString)
  {
    if (paramString == null)
      return false;
    File localFile = new File(paramString);
    if (localFile.exists())
      return localFile.delete();
    return false;
  }

  public static Uri getUriFromPath(String paramString)
  {
    return Uri.fromFile(new File(paramString));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.FileOperator
 * JD-Core Version:    0.6.2
 */