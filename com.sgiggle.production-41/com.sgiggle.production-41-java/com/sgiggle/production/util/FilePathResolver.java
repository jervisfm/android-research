package com.sgiggle.production.util;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;

public class FilePathResolver
{
  public static String getPathFromMediaGallery(Activity paramActivity, Uri paramUri)
  {
    Cursor localCursor = paramActivity.managedQuery(paramUri, new String[] { "_data" }, null, null, null);
    if (localCursor == null)
      return null;
    int i = localCursor.getColumnIndexOrThrow("_data");
    localCursor.moveToFirst();
    return localCursor.getString(i);
  }

  public static String translateUriToFilePath(Activity paramActivity, Uri paramUri)
  {
    String str = getPathFromMediaGallery(paramActivity, paramUri);
    if (str != null)
      return str;
    return paramUri.getPath();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.FilePathResolver
 * JD-Core Version:    0.6.2
 */