package com.sgiggle.production.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.sgiggle.util.Log;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ConcurrentHashMap;

public class HttpImageLoader extends BitmapLoader
{
  private static final String TAG = "Tango.HttpImageLoader";
  private static HttpImageLoader s_instance;

  public static HttpImageLoader getInstance()
  {
    if (s_instance == null)
      s_instance = new HttpImageLoader();
    return s_instance;
  }

  public void clear()
  {
    Log.d("Tango.HttpImageLoader", "clear()");
    super.clear();
  }

  protected BitmapLoaderThread createLoadBitmapThread()
  {
    return new LoadHttpImageThread(getBitmaps());
  }

  public void start()
  {
    Log.d("Tango.HttpImageLoader", "start()");
    super.start();
  }

  public void stop()
  {
    Log.d("Tango.HttpImageLoader", "stop()");
    super.stop();
  }

  private class LoadHttpImageThread extends BitmapLoaderThread
  {
    private static final int INITIAL_DELAY_BETWEEN_ATTEMPTS_MS = 1500;
    private static final int NB_ATTEMPTS_MAX = 5;

    public LoadHttpImageThread()
    {
      super();
    }

    private Bitmap loadImageFromUrl(String paramString)
    {
      try
      {
        Log.d("Tango.HttpImageLoader", "loadImageFromUrl: loading image START from URL=" + paramString);
        Bitmap localBitmap2 = BitmapFactory.decodeStream(new URL(paramString).openConnection().getInputStream());
        localBitmap1 = localBitmap2;
        Log.d("Tango.HttpImageLoader", "loadImageFromUrl: loading image DONE from URL=" + paramString);
        return localBitmap1;
      }
      catch (Exception localException)
      {
        while (true)
        {
          Log.e("Tango.HttpImageLoader", "loadImageFromUrl: Could not fetch image from URL=" + paramString, localException);
          Bitmap localBitmap1 = null;
        }
      }
    }

    protected Bitmap loadImage(Object paramObject)
    {
      Bitmap localBitmap = null;
      String str = (String)paramObject;
      if (TextUtils.isEmpty(str))
      {
        Log.d("Tango.HttpImageLoader", "loadImageFromModel: URL is empty, ignoring.");
        return null;
      }
      Log.d("Tango.HttpImageLoader", "loadImageFromModel: loading image START from URL=" + str);
      int i = 0;
      int j = 1500;
      int k = 1;
      while (k != 0)
      {
        i++;
        localBitmap = loadImageFromUrl(str);
        if (localBitmap != null)
          return localBitmap;
        if (i == 5)
        {
          Log.d("Tango.HttpImageLoader", "loadImageFromModel: attempt #" + i + " failed, won't try loading again URL:" + str);
          k = 0;
        }
        else
        {
          try
          {
            Log.d("Tango.HttpImageLoader", "loadImageFromModel: attempt #" + i + " failed, waiting " + j + "ms before retry for URL:" + str);
            Thread.sleep(j);
            j *= 2;
          }
          catch (InterruptedException localInterruptedException)
          {
          }
        }
      }
      return localBitmap;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.HttpImageLoader
 * JD-Core Version:    0.6.2
 */