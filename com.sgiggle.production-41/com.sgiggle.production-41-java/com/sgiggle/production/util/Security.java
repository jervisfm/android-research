package com.sgiggle.production.util;

import android.text.TextUtils;
import com.sgiggle.production.payments.Constants.PurchaseState;
import com.sgiggle.util.Log;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Security
{
  private static final String KEY_FACTORY_ALGORITHM = "RSA";
  private static final SecureRandom RANDOM = new SecureRandom();
  private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
  private static final String TAG = "Security";
  private static HashSet<Long> sKnownNonces = new HashSet();

  public static long generateNonce()
  {
    long l = RANDOM.nextLong();
    sKnownNonces.add(Long.valueOf(l));
    return l;
  }

  public static PublicKey generatePublicKey(String paramString)
  {
    try
    {
      byte[] arrayOfByte = Base64.decode(paramString);
      PublicKey localPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(arrayOfByte));
      return localPublicKey;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      throw new RuntimeException(localNoSuchAlgorithmException);
    }
    catch (InvalidKeySpecException localInvalidKeySpecException)
    {
      Log.e("Security", "Invalid key specification.");
      throw new IllegalArgumentException(localInvalidKeySpecException);
    }
    catch (Base64DecoderException localBase64DecoderException)
    {
      Log.e("Security", "Base64 decoding failed.");
      throw new IllegalArgumentException(localBase64DecoderException);
    }
  }

  public static boolean isNonceKnown(long paramLong)
  {
    return sKnownNonces.contains(Long.valueOf(paramLong));
  }

  public static void removeNonce(long paramLong)
  {
    sKnownNonces.remove(Long.valueOf(paramLong));
  }

  public static boolean verify(PublicKey paramPublicKey, String paramString1, String paramString2)
  {
    Log.i("Security", "signature: " + paramString2);
    try
    {
      Signature localSignature = Signature.getInstance("SHA1withRSA");
      localSignature.initVerify(paramPublicKey);
      localSignature.update(paramString1.getBytes());
      if (!localSignature.verify(Base64.decode(paramString2)))
      {
        Log.e("Security", "Signature verification failed.");
        return false;
      }
      return true;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      Log.e("Security", "NoSuchAlgorithmException.");
      return false;
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      while (true)
        Log.e("Security", "Invalid key specification.");
    }
    catch (SignatureException localSignatureException)
    {
      while (true)
        Log.e("Security", "Signature exception.");
    }
    catch (Base64DecoderException localBase64DecoderException)
    {
      while (true)
        Log.e("Security", "Base64 decoding failed.");
    }
  }

  public static ArrayList<VerifiedPurchase> verifyPurchase(String paramString1, String paramString2)
  {
    if (paramString1 == null)
    {
      Log.e("Security", "data is null");
      return null;
    }
    Log.i("Security", "signedData: " + paramString1);
    boolean bool1;
    long l1;
    JSONArray localJSONArray;
    int j;
    label120: ArrayList localArrayList;
    if (!TextUtils.isEmpty(paramString2))
    {
      boolean bool3 = verify(generatePublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArKXV6pVXcaAGaLvj+9F3cSR3tCruAKT0qMYQhz1CnGSZ9Tex1t00GNbOJzmH+4SQI+N1Gij0XGdY4CsS2r6/cpPQMq60HJtb1Zr4UUA8QqPbsQdXAqZA/VO7uVtnVWi1KLUhyAsDxj8BmAr//WSlMniJY/lLdT1k4MLlDHEzYBlXuEGzZhLI7mWex5CvKzymPe1uNCuq55h1WiTtfajlI9tuGnUlgAey4NzU5omMapp+dJrcQrFoNFFZMs6ETBdHxAzu3NdNuSMS5nXjPA6rP7FbZv4DgN/wvPNs2AKaTb+LIRoyjI4ikNCyagkk2oYI9e4iz81FHtd1DJ6+UfiqEwIDAQAB"), paramString1, paramString2);
      if (!bool3)
      {
        Log.w("Security", "signature does not match data.");
        return null;
      }
      bool1 = bool3;
      try
      {
        JSONObject localJSONObject1 = new JSONObject(paramString1);
        l1 = localJSONObject1.optLong("nonce");
        localJSONArray = localJSONObject1.optJSONArray("orders");
        if (localJSONArray == null)
          break label331;
        int i = localJSONArray.length();
        j = i;
        if (!isNonceKnown(l1))
        {
          Log.w("Security", "Nonce not found: " + l1);
          return null;
        }
      }
      catch (JSONException localJSONException1)
      {
        return null;
      }
      localArrayList = new ArrayList();
    }
    for (int k = 0; ; k++)
      if (k < j)
      {
        try
        {
          JSONObject localJSONObject2 = localJSONArray.getJSONObject(k);
          Constants.PurchaseState localPurchaseState = Constants.PurchaseState.valueOf(localJSONObject2.getInt("purchaseState"));
          String str1 = localJSONObject2.getString("productId");
          long l2 = localJSONObject2.getLong("purchaseTime");
          String str2 = localJSONObject2.optString("orderId", "");
          boolean bool2 = localJSONObject2.has("notificationId");
          String str3 = null;
          if (bool2)
            str3 = localJSONObject2.getString("notificationId");
          String str4 = localJSONObject2.optString("developerPayload", null);
          if ((localPurchaseState == Constants.PurchaseState.PURCHASED) && (!bool1))
            continue;
          localArrayList.add(new VerifiedPurchase(localPurchaseState, str3, str1, str2, l2, str4));
        }
        catch (JSONException localJSONException2)
        {
          Log.e("Security", "JSON exception: ", localJSONException2);
          return null;
        }
      }
      else
      {
        removeNonce(l1);
        return localArrayList;
        label331: j = 0;
        break label120;
        bool1 = false;
        break;
      }
  }

  public static class VerifiedPurchase
  {
    public String developerPayload;
    public String notificationId;
    public String orderId;
    public String productId;
    public Constants.PurchaseState purchaseState;
    public long purchaseTime;

    public VerifiedPurchase(Constants.PurchaseState paramPurchaseState, String paramString1, String paramString2, String paramString3, long paramLong, String paramString4)
    {
      this.purchaseState = paramPurchaseState;
      this.notificationId = paramString1;
      this.productId = paramString2;
      this.orderId = paramString3;
      this.purchaseTime = paramLong;
      this.developerPayload = paramString4;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.util.Security
 * JD-Core Version:    0.6.2
 */