package com.sgiggle.production.vendor.htc;

import android.os.Build;

public class IntegrationConstants
{
  public static final String ACTION_CHANGE_WIFI_MODE = "com.htc.intent.action.CHANGE_WIFI_MODE";
  public static final String ACTION_PS_AUDIO_STATE_CHANGED = "android.intent.action.PS_AUDIO_STATE_CHANGED";
  public static final String ACTION_PS_CALL_STATE_CHANGED = "android.intent.action.PS_CALL_STATE_CHANGED";
  public static final String ACTION_SCREEN_OFF = "android.intent.action.SCREEN_OFF";
  public static final String ACTION_SCREEN_ON = "android.intent.action.SCREEN_ON";
  public static final String ACTION_VoIP_BLUETOOTH = "android.intent.action.VoIP_BLUETOOTH";
  public static final String ACTION_VoIP_HEADSET = "android.intent.action.VoIP_HEADSET";
  public static final String ACTION_VoIP_PRESENCE_SYNC = "com.android.contacts.im.VoIP.SYNC";
  public static final String ACTION_VoIP_RESUME_CALL = "android.intent.action.VoIP_RESUME_CALL";
  public static final int BUTTON_ACCEPT = 1;
  public static final int BUTTON_REJECT = 3;
  public static final int CS_CALL_RESUME = 2;
  public static final String HTC_LOCKSCREEN_CLASS = "com.android.phone.VoIPIncomingCallArc";
  public static final String HTC_LOCKSCREEN_PACKAGE = "com.android.phone";
  public static final String KEY_MODE = "comeIn";
  public static final String MIMETYPE_CHAT_CAPABILITY = "com.android.htccontacts/chat_capability";
  public static final String PARAM_AUDIO_SPEAKER = "AudioType";
  public static final String PARAM_CALL_TYPE = "call_type";
  public static final String PARAM_EVENT = "Event";
  public static final String PARAM_LOCKSCREEN_RESULT = "result";
  public static final String PARAM_PS_CALLER_AVATAR = "photo";
  public static final String PARAM_PS_CALLER_ID = "id";
  public static final String PARAM_PS_CALLER_NAME = "name";
  public static final String PARAM_PS_CALL_STATE = "state";
  public static final String PARAM_RESUME_TYPE = "ResumeType";
  public static final String PARAM_STATE = "state";
  public static final int PRESENCE_AVAILABLE_VIDEO = 10;
  public static final int PS_CALL_RESUME = 1;
  public static final boolean SUPPORT_CALL_WAITING = false;
  public static final int VoIP_CALL_ACCEPTED_AUDIO = 1;
  public static final int VoIP_CALL_ACCEPTED_VIDEO = 2;
  public static final int VoIP_CALL_REJECTED = 3;
  public static final int VoIP_CALL_TYPE_AUDIO = 1;
  public static final int VoIP_CALL_TYPE_VIDEO = 2;

  static
  {
    if ((Build.MANUFACTURER.compareToIgnoreCase("HTC") == 0) && ((Build.MODEL.startsWith("HTC Sensation")) || (Build.MODEL.startsWith("HTC Bliss")) || (Build.MODEL.startsWith("HTC Rhyme")) || (Build.MODEL.startsWith("HTC Runnymede")) || (Build.MODEL.startsWith("HTC Sensation XL"))));
    for (boolean bool = true; ; bool = false)
    {
      SUPPORT_CALL_WAITING = bool;
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.vendor.htc.IntegrationConstants
 * JD-Core Version:    0.6.2
 */