package com.sgiggle.production.widget;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.widget.MediaController;
import android.widget.VideoView;
import com.sgiggle.util.Log;

public class BetterVideoView extends VideoView
  implements SurfaceHolder.Callback, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener
{
  private static final int MSG_RETRY_VIDEO_PREPARE = 2;
  private static final int MSG_STOP_SHORT_PLAYBACK = 1;
  private static final int RETRY_PREPARE_DELAY = 2000;
  private static final int SHORT_PLAYBACK_DURATION = 500;
  private static final String TAG = "Tango.BetterVideoView";
  private Handler m_handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
      case 1:
        do
        {
          return;
          Log.d("Tango.BetterVideoView", "Stopping short playback");
          BetterVideoView.this.abortShortPlayback();
          BetterVideoView.access$102(BetterVideoView.this, false);
          BetterVideoView.this.pauseAt(BetterVideoView.this.m_positionBeforeShortPlay);
        }
        while (BetterVideoView.this.m_videomailMediaController == null);
        BetterVideoView.this.m_videomailMediaController.setProgressUiFrozen(false);
        return;
      case 2:
      }
      BetterVideoView.access$508(BetterVideoView.this);
      Log.d("Tango.BetterVideoView", "Retrying playback - Attempt " + BetterVideoView.this.m_videoPrepareRetryCount + "/" + BetterVideoView.this.m_maxVideoPrepareRetry);
      BetterVideoView.this.setVideoUriInternal(BetterVideoView.this.m_videoUri);
    }
  };
  private BetterVideoViewListener m_listener;
  private int m_maxVideoPrepareRetry = 0;
  private boolean m_playShortlyWhenReady = false;
  private int m_positionBeforeShortPlay = 0;
  private boolean m_shouldBePlaying = false;
  private boolean m_surfacePrepared = false;
  private int m_videoPrepareRetryCount = 0;
  private boolean m_videoPrepared = false;
  private Uri m_videoUri = null;
  private VideomailMediaController m_videomailMediaController;

  public BetterVideoView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    getHolder().addCallback(this);
    setOnErrorListener(this);
    setOnCompletionListener(this);
    setOnPreparedListener(this);
  }

  private void abortShortPlayback()
  {
    this.m_playShortlyWhenReady = false;
  }

  private void checkPlayShortly()
  {
    if ((!this.m_playShortlyWhenReady) || (isPlaying()))
      abortShortPlayback();
    while ((!this.m_videoPrepared) || (!this.m_surfacePrepared))
      return;
    abortShortPlayback();
    Log.d("Tango.BetterVideoView", "Starting short playback for few ms.");
    this.m_positionBeforeShortPlay = getCurrentPosition();
    int i = this.m_positionBeforeShortPlay - 500;
    if (i < 0)
      i = 0;
    if (this.m_videomailMediaController != null)
      this.m_videomailMediaController.setProgressUiFrozen(true);
    seekTo(i);
    start();
    this.m_handler.sendEmptyMessageDelayed(1, 500L);
  }

  private void pauseAt(int paramInt)
  {
    int i = getDuration();
    if (paramInt > i);
    while (true)
    {
      seekTo(i);
      pause();
      return;
      if (paramInt < 0)
        i = 0;
      else
        i = paramInt;
    }
  }

  private void resetRetryCount()
  {
    this.m_handler.removeMessages(2);
    this.m_videoPrepareRetryCount = 0;
  }

  private void setVideoPrepared(boolean paramBoolean)
  {
    this.m_videoPrepared = paramBoolean;
    checkPlayShortly();
  }

  private void setVideoUriInternal(Uri paramUri)
  {
    this.m_videoUri = paramUri;
    super.setVideoURI(paramUri);
  }

  public void forcePreview()
  {
    this.m_playShortlyWhenReady = true;
    checkPlayShortly();
  }

  public void init(BetterVideoViewListener paramBetterVideoViewListener, int paramInt)
  {
    this.m_listener = paramBetterVideoViewListener;
    this.m_maxVideoPrepareRetry = paramInt;
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    this.m_shouldBePlaying = false;
    setVideoPrepared(false);
    this.m_videomailMediaController.finishedPlaying();
    if (this.m_listener != null)
      this.m_listener.onCompletion();
  }

  public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    setVideoPrepared(false);
    String str = "";
    switch (paramInt1)
    {
    default:
    case 200:
    case 100:
    case 1:
    }
    while (true)
    {
      Log.e("Tango.BetterVideoView", "Error occurred during playback: " + str);
      if (this.m_videoUri == null)
        break label164;
      if (this.m_videoPrepareRetryCount >= this.m_maxVideoPrepareRetry)
        break;
      Log.e("Tango.BetterVideoView", "Retrying video prepare in 2000 ms");
      this.m_handler.sendEmptyMessageDelayed(2, 2000L);
      return true;
      str = "The video is streamed and its container is not valid for progressive playback i.e the video's index (e.g moov atom) is not at the start of the file.";
      continue;
      str = "Media server died. In this case, the application must release the MediaPlayer object and instantiate a new one.";
      continue;
      str = "Unspecified media player error.";
    }
    Log.e("Tango.BetterVideoView", "Video prepare failed " + this.m_videoPrepareRetryCount + " times. Max reached, won't retry.");
    label164: if (this.m_listener != null)
      this.m_listener.onError();
    return true;
  }

  public void onPrepared(MediaPlayer paramMediaPlayer)
  {
    setVideoPrepared(true);
    paramMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
    {
      public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
      {
        BetterVideoView.this.m_videomailMediaController.showAndStay();
      }
    });
    paramMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener()
    {
      public void onSeekComplete(MediaPlayer paramAnonymousMediaPlayer)
      {
        BetterVideoView.this.m_videomailMediaController.setInitialSeekComplete();
      }
    });
    Log.d("Tango.BetterVideoView", "Duration: " + getDuration());
    this.m_videomailMediaController.setEnabled(true);
    this.m_videomailMediaController.showAndStayIfShort();
    if (this.m_listener != null)
      this.m_listener.onPrepared();
    if (this.m_shouldBePlaying)
      start();
  }

  public void pause()
  {
    this.m_shouldBePlaying = false;
    abortShortPlayback();
    super.pause();
  }

  public void setMediaController(MediaController paramMediaController)
  {
    if (paramMediaController != this.m_videomailMediaController)
      this.m_videomailMediaController = null;
    super.setMediaController(paramMediaController);
  }

  public void setVideoURI(Uri paramUri)
  {
    Log.d("Tango.BetterVideoView", "Video URI: " + paramUri);
    resetRetryCount();
    setVideoUriInternal(paramUri);
  }

  public void setVideomailMediaController(VideomailMediaController paramVideomailMediaController)
  {
    this.m_videomailMediaController = paramVideomailMediaController;
    setMediaController(paramVideomailMediaController);
  }

  public void start()
  {
    this.m_shouldBePlaying = true;
    super.start();
  }

  public void stopPlayback()
  {
    this.m_shouldBePlaying = false;
    abortShortPlayback();
    resetRetryCount();
    super.stopPlayback();
  }

  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    this.m_surfacePrepared = true;
    checkPlayShortly();
  }

  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
  }

  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    this.m_surfacePrepared = false;
    abortShortPlayback();
  }

  public static abstract interface BetterVideoViewListener
  {
    public abstract void onCompletion();

    public abstract void onError();

    public abstract void onPrepared();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.BetterVideoView
 * JD-Core Version:    0.6.2
 */