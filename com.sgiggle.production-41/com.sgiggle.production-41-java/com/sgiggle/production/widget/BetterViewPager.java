package com.sgiggle.production.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class BetterViewPager extends ViewPager
{
  private int m_scrollableChildResId = 0;

  public BetterViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.m_scrollableChildResId > 0)
    {
      View localView = findViewById(this.m_scrollableChildResId);
      if (localView != null)
      {
        Rect localRect = new Rect();
        localView.getHitRect(localRect);
        if (localRect.contains((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY()))
          return false;
      }
    }
    return super.onInterceptTouchEvent(paramMotionEvent);
  }

  public void resetScrollableChildId()
  {
    this.m_scrollableChildResId = 0;
  }

  public void setScrollableChildResId(int paramInt)
  {
    this.m_scrollableChildResId = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.BetterViewPager
 * JD-Core Version:    0.6.2
 */