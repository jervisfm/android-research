package com.sgiggle.production.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

public class ButtonDontInheritPress extends Button
{
  public ButtonDontInheritPress(Context paramContext)
  {
    super(paramContext);
  }

  public ButtonDontInheritPress(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public ButtonDontInheritPress(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void setPressed(boolean paramBoolean)
  {
    if ((paramBoolean) && ((getParent() instanceof View)) && (((View)getParent()).isPressed()))
      return;
    super.setPressed(paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.ButtonDontInheritPress
 * JD-Core Version:    0.6.2
 */