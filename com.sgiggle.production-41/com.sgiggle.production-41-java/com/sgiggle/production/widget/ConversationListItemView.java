package com.sgiggle.production.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sgiggle.production.Utils;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationSummary;
import com.sgiggle.production.util.ContactThumbnailLoader;
import com.sgiggle.xmpp.SessionMessages.Contact;

public class ConversationListItemView extends RelativeLayout
{
  private final int COLOR_READ;
  private final int COLOR_UNREAD;
  private final String JUST_NOW_LABEL;
  private TextView m_badge;
  private ImageView m_contactThumbnail;
  private Context m_context;
  private View m_statusIcon;
  private TextView m_summary;
  private TextView m_summarySecondary;
  private TextView m_timestamp;
  private TextView m_title;

  public ConversationListItemView(Context paramContext)
  {
    this(paramContext, null);
  }

  public ConversationListItemView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public ConversationListItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.m_context = paramContext;
    LayoutInflater.from(paramContext).inflate(2130903072, this);
    this.COLOR_READ = this.m_context.getResources().getColor(2131165235);
    this.COLOR_UNREAD = this.m_context.getResources().getColor(2131165234);
    this.JUST_NOW_LABEL = this.m_context.getString(2131296563);
    this.m_contactThumbnail = ((ImageView)findViewById(2131361937));
    this.m_title = ((TextView)findViewById(2131361938));
    this.m_timestamp = ((TextView)findViewById(2131361939));
    this.m_summary = ((TextView)findViewById(2131361943));
    this.m_summarySecondary = ((TextView)findViewById(2131361944));
    this.m_statusIcon = findViewById(2131361940);
    this.m_badge = ((TextView)findViewById(2131361945));
  }

  public void fill(Context paramContext, ConversationSummary paramConversationSummary)
  {
    ConversationMessage localConversationMessage = paramConversationSummary.getLastMessage();
    boolean bool1;
    int i;
    label44: int j;
    if (paramConversationSummary.getUnreadMessageCount() > 0)
    {
      bool1 = true;
      String str1 = localConversationMessage.getSummaryPrimaryText(paramContext, bool1);
      String str2 = localConversationMessage.getSummarySecondaryText(paramContext, bool1);
      if (!bool1)
        break label174;
      i = this.COLOR_UNREAD;
      this.m_title.setText(paramConversationSummary.getDisplayString());
      this.m_summary.setText(str1);
      this.m_summary.setTextColor(i);
      this.m_summarySecondary.setText(str2);
      this.m_timestamp.setText(Utils.getRelativeTimeString(getContext(), localConversationMessage.getTimestampMs(), this.JUST_NOW_LABEL));
      if ((!localConversationMessage.isStatusError()) || (!bool1))
        break label183;
      j = 1;
      label119: if (j == 0)
        break label189;
      this.m_badge.setVisibility(8);
      this.m_statusIcon.setVisibility(0);
    }
    ConversationContact localConversationContact;
    while (true)
    {
      localConversationContact = paramConversationSummary.getPeer();
      if (!localConversationContact.getContact().getIsSystemAccount())
        break label214;
      this.m_contactThumbnail.setImageResource(2130837649);
      return;
      bool1 = false;
      break;
      label174: i = this.COLOR_READ;
      break label44;
      label183: j = 0;
      break label119;
      label189: this.m_statusIcon.setVisibility(8);
      Utils.setBadgeCount(this.m_badge, paramConversationSummary.getUnreadMessageCount(), false, paramContext);
    }
    label214: boolean bool2 = Long.valueOf(localConversationContact.getContact().getDeviceContactId()).longValue() < -1L;
    Bitmap localBitmap = null;
    if (bool2)
    {
      boolean bool3 = localConversationContact.shouldHaveImage();
      localBitmap = null;
      if (bool3)
      {
        localBitmap = ContactThumbnailLoader.getInstance().getLoadedBitmap(localConversationContact);
        if (localBitmap == null)
          ContactThumbnailLoader.getInstance().addLoadTask(localConversationContact, this.m_contactThumbnail);
      }
    }
    if (localBitmap != null)
    {
      this.m_contactThumbnail.setImageBitmap(localBitmap);
      return;
    }
    this.m_contactThumbnail.setImageResource(2130837648);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.ConversationListItemView
 * JD-Core Version:    0.6.2
 */