package com.sgiggle.production.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

public class FileUploader extends FrameLayout
{
  private static final int UPDATE_PROGRESS = 1;
  private Handler m_handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage.what == 1)
      {
        FileUploader.this.m_progressBar.setProgress(paramAnonymousMessage.arg1);
        FileUploader.this.requestLayout();
      }
    }
  };
  private ProgressBar m_progressBar;

  public FileUploader(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  protected void dispatchDraw(Canvas paramCanvas)
  {
    paramCanvas.save();
    float f1 = getWidth();
    float f2 = getHeight();
    paramCanvas.translate(this.m_progressBar.getHeight() + -f1 / 2.0F, 0.0F);
    paramCanvas.rotate(-90.0F, f1 / 2.0F, f2 / 2.0F);
    super.dispatchDraw(paramCanvas);
    paramCanvas.restore();
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.m_progressBar = ((ProgressBar)findViewById(2131362143));
    this.m_progressBar.setMax(100);
  }

  public void reset()
  {
    this.m_progressBar.setProgress(0);
  }

  public void updateProgress(int paramInt)
  {
    Message localMessage = this.m_handler.obtainMessage();
    localMessage.arg1 = paramInt;
    localMessage.what = 1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.FileUploader
 * JD-Core Version:    0.6.2
 */