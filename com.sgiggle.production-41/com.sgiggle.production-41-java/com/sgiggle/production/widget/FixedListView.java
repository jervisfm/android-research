package com.sgiggle.production.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class FixedListView extends LinearLayout
  implements View.OnClickListener
{
  private Adapter m_adapter;
  private OnItemClickListener m_itemClickListener;

  public FixedListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(null);
  }

  public FixedListView(Context paramContext, Adapter paramAdapter)
  {
    super(paramContext);
    init(paramAdapter);
  }

  private void fillViews()
  {
    if ((this.m_adapter == null) || (this.m_adapter.getCount() <= 0))
      removeAllViews();
    while (true)
    {
      return;
      removeAllViews();
      int i = this.m_adapter.getCount();
      for (int j = 0; j < i; j++)
      {
        View localView = this.m_adapter.getView(j, null, this);
        if (localView != null)
        {
          int k = (int)(0.5F + 5.0F * getContext().getResources().getDisplayMetrics().density);
          addView(localView);
          localView.setOnClickListener(this);
          localView.setBackgroundResource(17301602);
          localView.setPadding(k, k, k, k);
          if (j < i - 1)
            addView(getSeperator());
        }
      }
    }
  }

  private View getSeperator()
  {
    View localView = new View(getContext());
    localView.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
    localView.setBackgroundColor(-7829368);
    return localView;
  }

  private void init(Adapter paramAdapter)
  {
    this.m_adapter = paramAdapter;
    setOrientation(1);
    fillViews();
  }

  public Adapter getAdapter()
  {
    return this.m_adapter;
  }

  public void onClick(View paramView)
  {
    if (this.m_itemClickListener != null)
      this.m_itemClickListener.onItemClick(paramView);
  }

  public void refresh()
  {
    fillViews();
  }

  public void setAdapter(Adapter paramAdapter)
  {
    this.m_adapter = paramAdapter;
    fillViews();
  }

  public void setOnItemClickListener(OnItemClickListener paramOnItemClickListener)
  {
    this.m_itemClickListener = paramOnItemClickListener;
  }

  public static abstract interface OnItemClickListener
  {
    public abstract void onItemClick(View paramView);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.FixedListView
 * JD-Core Version:    0.6.2
 */