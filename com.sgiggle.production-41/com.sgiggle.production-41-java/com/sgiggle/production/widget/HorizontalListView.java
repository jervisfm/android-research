package com.sgiggle.production.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListAdapter;
import android.widget.Scroller;
import com.sgiggle.cafe.vgood.VGoodButtonsBarView.ScrollListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class HorizontalListView extends AdapterView<ListAdapter>
{
  protected ListAdapter mAdapter;
  public boolean mAlwaysOverrideTouch = true;
  private boolean mCenterSingleItem = false;
  protected int mCurrentX;
  private boolean mDataChanged = false;
  private DataSetObserver mDataObserver = new DataSetObserver()
  {
    public void onChanged()
    {
      synchronized (HorizontalListView.this)
      {
        HorizontalListView.access$002(HorizontalListView.this, true);
        HorizontalListView.this.invalidate();
        HorizontalListView.this.requestLayout();
        return;
      }
    }

    public void onInvalidated()
    {
      HorizontalListView.this.reset();
      HorizontalListView.this.invalidate();
      HorizontalListView.this.requestLayout();
    }
  };
  private int mDisplayOffset = 0;
  private GestureDetector mGesture;
  private int mLeftViewIndex = -1;
  private int mMaxX = 2147483647;
  private boolean mMeasureHeightForVisibleOnly = true;
  protected int mNextX;
  private GestureDetector.OnGestureListener mOnGesture = new GestureDetector.SimpleOnGestureListener()
  {
    public boolean onDown(MotionEvent paramAnonymousMotionEvent)
    {
      return HorizontalListView.this.onDown(paramAnonymousMotionEvent);
    }

    public boolean onFling(MotionEvent paramAnonymousMotionEvent1, MotionEvent paramAnonymousMotionEvent2, float paramAnonymousFloat1, float paramAnonymousFloat2)
    {
      return HorizontalListView.this.onFling(paramAnonymousMotionEvent1, paramAnonymousMotionEvent2, paramAnonymousFloat1, paramAnonymousFloat2);
    }

    public void onLongPress(MotionEvent paramAnonymousMotionEvent)
    {
      Rect localRect = new Rect();
      int i = HorizontalListView.this.getChildCount();
      for (int j = 0; ; j++)
      {
        if (j < i)
        {
          View localView = HorizontalListView.this.getChildAt(j);
          int k = localView.getLeft();
          int m = localView.getRight();
          localRect.set(k, localView.getTop(), m, localView.getBottom());
          if (!localRect.contains((int)paramAnonymousMotionEvent.getX(), (int)paramAnonymousMotionEvent.getY()))
            continue;
          if (HorizontalListView.this.mOnItemLongClicked != null)
            HorizontalListView.this.mOnItemLongClicked.onItemLongClick(HorizontalListView.this, localView, j + (1 + HorizontalListView.this.mLeftViewIndex), HorizontalListView.this.mAdapter.getItemId(j + (1 + HorizontalListView.this.mLeftViewIndex)));
        }
        return;
      }
    }

    public boolean onScroll(MotionEvent paramAnonymousMotionEvent1, MotionEvent paramAnonymousMotionEvent2, float paramAnonymousFloat1, float paramAnonymousFloat2)
    {
      synchronized (HorizontalListView.this)
      {
        HorizontalListView localHorizontalListView2 = HorizontalListView.this;
        localHorizontalListView2.mNextX += (int)paramAnonymousFloat1;
        HorizontalListView.this.requestLayout();
        return true;
      }
    }

    public boolean onSingleTapConfirmed(MotionEvent paramAnonymousMotionEvent)
    {
      Rect localRect = new Rect();
      for (int i = 0; ; i++)
      {
        if (i < HorizontalListView.this.getChildCount())
        {
          View localView = HorizontalListView.this.getChildAt(i);
          int j = localView.getLeft();
          int k = localView.getRight();
          localRect.set(j, localView.getTop(), k, localView.getBottom());
          if (!localRect.contains((int)paramAnonymousMotionEvent.getX(), (int)paramAnonymousMotionEvent.getY()))
            continue;
          if (HorizontalListView.this.mOnItemClicked != null)
            HorizontalListView.this.mOnItemClicked.onItemClick(HorizontalListView.this, localView, i + (1 + HorizontalListView.this.mLeftViewIndex), HorizontalListView.this.mAdapter.getItemId(i + (1 + HorizontalListView.this.mLeftViewIndex)));
          if (HorizontalListView.this.mOnItemSelected != null)
            HorizontalListView.this.mOnItemSelected.onItemSelected(HorizontalListView.this, localView, i + (1 + HorizontalListView.this.mLeftViewIndex), HorizontalListView.this.mAdapter.getItemId(i + (1 + HorizontalListView.this.mLeftViewIndex)));
        }
        return true;
      }
    }
  };
  private AdapterView.OnItemClickListener mOnItemClicked;
  private AdapterView.OnItemLongClickListener mOnItemLongClicked;
  private AdapterView.OnItemSelectedListener mOnItemSelected;
  private View mPressedChild;
  private Queue<View> mRemovedViewQueue = new LinkedList();
  private int mRightViewIndex = 0;
  protected Scroller mScroller;
  private float mSpacing;
  private VGoodButtonsBarView.ScrollListener scrollListener;

  public HorizontalListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initView();
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, new int[] { 16843027 });
    this.mSpacing = localTypedArray.getDimension(0, 0.0F);
    localTypedArray.recycle();
  }

  private void addAndMeasureChild(View paramView, int paramInt)
  {
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    if (localLayoutParams == null)
      localLayoutParams = new ViewGroup.LayoutParams(-1, -1);
    addViewInLayout(paramView, paramInt, localLayoutParams, true);
    paramView.measure(View.MeasureSpec.makeMeasureSpec(getWidth(), -2147483648), View.MeasureSpec.makeMeasureSpec(getHeight(), -2147483648));
  }

  private void fillList(int paramInt)
  {
    View localView1 = getChildAt(getChildCount() - 1);
    if (localView1 != null);
    for (int i = localView1.getRight(); ; i = 0)
    {
      fillListRight(i, paramInt);
      View localView2 = getChildAt(0);
      if (localView2 != null);
      for (int j = localView2.getLeft(); ; j = 0)
      {
        fillListLeft(j, paramInt);
        return;
      }
    }
  }

  private void fillListLeft(int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    while ((i + paramInt2 > 0) && (this.mLeftViewIndex >= 0))
    {
      View localView = this.mAdapter.getView(this.mLeftViewIndex, (View)this.mRemovedViewQueue.poll(), this);
      addAndMeasureChild(localView, 0);
      i -= localView.getMeasuredWidth() + (int)this.mSpacing;
      this.mLeftViewIndex -= 1;
      this.mDisplayOffset -= localView.getMeasuredWidth() + (int)this.mSpacing;
    }
  }

  private void fillListRight(int paramInt1, int paramInt2)
  {
    int j;
    for (int i = paramInt1; (i + paramInt2 < getWidth()) && (this.mRightViewIndex < this.mAdapter.getCount()); i = j)
    {
      View localView = this.mAdapter.getView(this.mRightViewIndex, (View)this.mRemovedViewQueue.poll(), this);
      addAndMeasureChild(localView, -1);
      j = i + (localView.getMeasuredWidth() + (int)this.mSpacing);
      if (this.mRightViewIndex == this.mAdapter.getCount() - 1)
        this.mMaxX = (j + this.mCurrentX - getWidth());
      if (this.mMaxX < 0)
        this.mMaxX = 0;
      this.mRightViewIndex = (1 + this.mRightViewIndex);
    }
  }

  private void initView()
  {
    try
    {
      this.mLeftViewIndex = -1;
      this.mRightViewIndex = 0;
      this.mDisplayOffset = 0;
      this.mCurrentX = 0;
      this.mNextX = 0;
      this.mMaxX = 2147483647;
      this.mScroller = new Scroller(getContext());
      this.mGesture = new GestureDetector(getContext(), this.mOnGesture);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void positionItems(int paramInt)
  {
    int i = getChildCount();
    if ((this.mCenterSingleItem) && (i == 1))
    {
      View localView2 = getChildAt(0);
      int i1 = localView2.getMeasuredWidth();
      int i2 = (getWidth() - i1) / 2;
      int i3 = getHeight() - localView2.getMeasuredHeight();
      localView2.layout(i2, i3 / 2, i1 + i2, localView2.getMeasuredHeight() + i3 / 2);
    }
    while (true)
    {
      return;
      if (i > 0)
      {
        this.mDisplayOffset = (paramInt + this.mDisplayOffset);
        int j = this.mDisplayOffset;
        for (int k = 0; k < getChildCount(); k++)
        {
          View localView1 = getChildAt(k);
          int m = localView1.getMeasuredWidth() + (int)this.mSpacing;
          int n = getHeight() - localView1.getMeasuredHeight();
          localView1.layout(j, n / 2, j + m, localView1.getMeasuredHeight() + n / 2);
          j += m;
        }
      }
    }
  }

  private void removeNonVisibleItems(int paramInt)
  {
    for (View localView1 = getChildAt(0); (localView1 != null) && (paramInt + localView1.getRight() <= 0); localView1 = getChildAt(0))
    {
      this.mDisplayOffset += localView1.getMeasuredWidth() + (int)this.mSpacing;
      this.mRemovedViewQueue.offer(localView1);
      removeViewInLayout(localView1);
      this.mLeftViewIndex = (1 + this.mLeftViewIndex);
    }
    for (View localView2 = getChildAt(getChildCount() - 1); (localView2 != null) && (paramInt + localView2.getLeft() >= getWidth()); localView2 = getChildAt(getChildCount() - 1))
    {
      this.mRemovedViewQueue.offer(localView2);
      removeViewInLayout(localView2);
      this.mRightViewIndex -= 1;
    }
  }

  protected void dispatchSetPressed(boolean paramBoolean)
  {
    if (this.mPressedChild != null)
    {
      this.mPressedChild.setPressed(paramBoolean);
      if (!paramBoolean)
        this.mPressedChild = null;
    }
  }

  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    switch (paramMotionEvent.getAction())
    {
    default:
    case 0:
    case 1:
    }
    while (true)
    {
      return this.mGesture.onTouchEvent(paramMotionEvent) | super.dispatchTouchEvent(paramMotionEvent);
      if (this.scrollListener != null)
      {
        this.scrollListener.startScrolling();
        continue;
        if (this.scrollListener != null)
          this.scrollListener.isGoingToStop();
      }
    }
  }

  protected View findChild(float paramFloat1, float paramFloat2)
  {
    Rect localRect = new Rect();
    for (int i = 0; i < getChildCount(); i++)
    {
      View localView = getChildAt(i);
      int j = localView.getLeft();
      int k = localView.getRight();
      localRect.set(j, localView.getTop(), k, localView.getBottom());
      if (localRect.contains((int)paramFloat1, (int)paramFloat2))
        return localView;
    }
    return null;
  }

  public ListAdapter getAdapter()
  {
    return this.mAdapter;
  }

  public View getSelectedView()
  {
    return null;
  }

  protected boolean onDown(MotionEvent paramMotionEvent)
  {
    this.mScroller.forceFinished(true);
    return true;
  }

  protected boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    try
    {
      this.mScroller.fling(this.mNextX, 0, (int)-paramFloat1, 0, 0, this.mMaxX, 0, 0);
      requestLayout();
      return true;
    }
    finally
    {
    }
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    try
    {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      ListAdapter localListAdapter = this.mAdapter;
      if (localListAdapter == null);
      while (true)
      {
        return;
        if (this.mDataChanged)
        {
          int j = this.mCurrentX;
          initView();
          removeAllViewsInLayout();
          this.mNextX = j;
          this.mDataChanged = false;
        }
        if (this.mScroller.computeScrollOffset())
          this.mNextX = this.mScroller.getCurrX();
        if (this.mNextX <= 0)
        {
          this.mNextX = 0;
          this.mScroller.forceFinished(true);
        }
        if (this.mNextX >= this.mMaxX)
        {
          this.mNextX = this.mMaxX;
          this.mScroller.forceFinished(true);
        }
        int i = this.mCurrentX - this.mNextX;
        removeNonVisibleItems(i);
        fillList(i);
        positionItems(i);
        this.mCurrentX = this.mNextX;
        if (!this.mScroller.isFinished())
          post(new Runnable()
          {
            public void run()
            {
              HorizontalListView.this.requestLayout();
            }
          });
      }
    }
    finally
    {
    }
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    int m;
    if (View.MeasureSpec.getMode(paramInt2) != 1073741824)
    {
      if (!this.mMeasureHeightForVisibleOnly)
        break label184;
      int i3 = getChildCount();
      if (i3 <= 0)
        break label126;
      int i4 = 0;
      int i5 = 0;
      while (i4 < i3)
      {
        View localView4 = getChildAt(i4);
        localView4.measure(0, 0);
        if (localView4.getMeasuredHeight() > i5)
          i5 = localView4.getMeasuredHeight();
        i4++;
      }
      m = i5;
    }
    label184: 
    while (true)
    {
      if (View.MeasureSpec.getMode(paramInt2) == -2147483648)
      {
        int n = View.MeasureSpec.getSize(paramInt2);
        if (n < m)
          m = n;
      }
      setMeasuredDimension(getMeasuredWidth(), m);
      return;
      label126: if ((getAdapter() != null) && (getAdapter().getCount() > 0))
      {
        View localView3 = getAdapter().getView(1, null, this);
        localView3.measure(0, 0);
        if (localView3.getMeasuredHeight() > 0)
        {
          m = localView3.getMeasuredHeight();
          continue;
          HashMap localHashMap = new HashMap();
          int i = getAdapter().getCount();
          int j = 0;
          int k = 0;
          if (j < i)
          {
            int i1 = getAdapter().getItemViewType(j);
            View localView1 = (View)localHashMap.get(Integer.valueOf(i1));
            View localView2 = getAdapter().getView(j, localView1, this);
            localHashMap.put(Integer.valueOf(i1), localView2);
            localView2.measure(0, 0);
            if (localView2.getMeasuredHeight() > k);
            for (int i2 = localView2.getMeasuredHeight(); ; i2 = k)
            {
              j++;
              k = i2;
              break;
            }
          }
          m = k;
        }
      }
      else
      {
        m = 0;
      }
    }
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((paramMotionEvent.getAction() == 0) && (this.mPressedChild == null))
    {
      this.mPressedChild = findChild(paramMotionEvent.getX(), paramMotionEvent.getY());
      setPressed(true);
    }
    if ((paramMotionEvent.getAction() == 1) && (this.mPressedChild != null))
      setPressed(false);
    return super.onTouchEvent(paramMotionEvent);
  }

  public void reset()
  {
    try
    {
      initView();
      removeAllViewsInLayout();
      requestLayout();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void scrollTo(int paramInt)
  {
    try
    {
      this.mScroller.startScroll(this.mNextX, 0, paramInt - this.mNextX, 0);
      requestLayout();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void setAdapter(ListAdapter paramListAdapter)
  {
    if (this.mAdapter != null)
      this.mAdapter.unregisterDataSetObserver(this.mDataObserver);
    this.mAdapter = paramListAdapter;
    this.mAdapter.registerDataSetObserver(this.mDataObserver);
    reset();
  }

  public void setCenterSingleItem(boolean paramBoolean)
  {
    this.mCenterSingleItem = paramBoolean;
  }

  public void setHeightMeasureMode(boolean paramBoolean)
  {
    if (this.mMeasureHeightForVisibleOnly != paramBoolean)
    {
      this.mMeasureHeightForVisibleOnly = paramBoolean;
      requestLayout();
    }
  }

  public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener)
  {
    this.mOnItemClicked = paramOnItemClickListener;
  }

  public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener paramOnItemLongClickListener)
  {
    this.mOnItemLongClicked = paramOnItemLongClickListener;
  }

  public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener paramOnItemSelectedListener)
  {
    this.mOnItemSelected = paramOnItemSelectedListener;
  }

  public void setScrollListener(VGoodButtonsBarView.ScrollListener paramScrollListener)
  {
    this.scrollListener = paramScrollListener;
  }

  public void setSelection(int paramInt)
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.HorizontalListView
 * JD-Core Version:    0.6.2
 */