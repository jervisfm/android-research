package com.sgiggle.production.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.ViewContactDetailMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.model.ConversationContact;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.util.ContactThumbnailLoader;
import com.sgiggle.util.Log;
import com.sgiggle.xmpp.SessionMessages.Contact;

public abstract class MessageListCompoundItemView extends MessageListItemView
{
  private static final String TAG = "Tango.MessageListCompoundItemView";
  protected TextView m_contactName;
  protected ImageView m_contactThumbnail;
  protected View m_contactThumbnailWrapper;
  protected LinearLayout m_messageContentPositioner;
  protected View m_messageContentWrapper;
  protected ImageView m_statusIcon;
  protected View m_statusProgress;
  protected View m_statusWrapper;

  public MessageListCompoundItemView(Context paramContext)
  {
    this(paramContext, null);
  }

  public MessageListCompoundItemView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public MessageListCompoundItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void fill(ConversationMessage paramConversationMessage, boolean paramBoolean)
  {
    super.fill(paramConversationMessage, paramBoolean);
    int i;
    if (paramConversationMessage.isFromSelf())
    {
      this.m_messageContentPositioner.setGravity(5);
      this.m_timestamp.setGravity(5);
      this.m_statusRead.setGravity(5);
      this.m_messageContentWrapper.setBackgroundResource(2130837772);
      this.m_contactThumbnail.setVisibility(8);
      this.m_contactThumbnailWrapper.setVisibility(4);
      this.m_contactName.setVisibility(4);
      this.m_statusRead.setVisibility(8);
      if (!paramConversationMessage.isFromSelf())
        break label375;
      if (!paramConversationMessage.isStatusSending())
        break label329;
      this.m_statusIcon.setImageResource(2130837707);
      this.m_statusProgress.setVisibility(0);
      i = 1;
    }
    while (true)
    {
      if (i == 0)
        break label381;
      this.m_statusWrapper.setVisibility(0);
      return;
      this.m_messageContentPositioner.setGravity(3);
      this.m_timestamp.setGravity(3);
      this.m_messageContentWrapper.setBackgroundResource(2130837771);
      final ConversationContact localConversationContact = paramConversationMessage.getFromContact();
      if (localConversationContact.getContact().getIsSystemAccount())
      {
        this.m_contactThumbnail.setImageResource(2130837649);
        this.m_contactThumbnail.setVisibility(0);
        this.m_contactThumbnailWrapper.setVisibility(0);
        this.m_contactName.setText(paramConversationMessage.getFromContact().getDisplayNameShort());
        this.m_contactName.setVisibility(0);
        break;
      }
      boolean bool1 = Long.valueOf(localConversationContact.getContact().getDeviceContactId()).longValue() < -1L;
      Bitmap localBitmap = null;
      if (bool1)
      {
        boolean bool2 = localConversationContact.shouldHaveImage();
        localBitmap = null;
        if (bool2)
        {
          localBitmap = ContactThumbnailLoader.getInstance().getLoadedBitmap(localConversationContact);
          if (localBitmap == null)
            ContactThumbnailLoader.getInstance().addLoadTask(localConversationContact, this.m_contactThumbnail);
        }
      }
      if (localBitmap != null)
        this.m_contactThumbnail.setImageBitmap(localBitmap);
      while (true)
      {
        this.m_contactThumbnail.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Log.d("Tango.MessageListCompoundItemView", "fill: jumping to contact detail.");
            MessageRouter.getInstance().postMessage("jingle", new MediaEngineMessage.ViewContactDetailMessage(localConversationContact.getContact()));
          }
        });
        break;
        this.m_contactThumbnail.setImageResource(2130837648);
      }
      label329: if (paramConversationMessage.isStatusError())
      {
        this.m_statusIcon.setImageResource(2130837706);
        this.m_statusProgress.setVisibility(8);
        i = 1;
      }
      else
      {
        if (paramConversationMessage.isStatusReadShow())
          this.m_statusRead.setVisibility(0);
        label375: i = 0;
      }
    }
    label381: this.m_statusWrapper.setVisibility(8);
  }

  protected abstract int getContentResId();

  protected void inflateLayout(LayoutInflater paramLayoutInflater)
  {
    paramLayoutInflater.inflate(2130903091, this);
    ViewGroup localViewGroup = (ViewGroup)findViewById(2131361998);
    paramLayoutInflater.inflate(getContentResId(), localViewGroup);
  }

  protected void initViews()
  {
    Log.d("Tango.MessageListCompoundItemView", "initViews");
    super.initViews();
    this.m_messageContentPositioner = ((LinearLayout)findViewById(2131361996));
    this.m_messageContentWrapper = findViewById(2131361997);
    this.m_statusWrapper = findViewById(2131361999);
    this.m_statusIcon = ((ImageView)this.m_statusWrapper.findViewById(2131362000));
    this.m_statusProgress = this.m_statusWrapper.findViewById(2131362001);
    this.m_contactName = ((TextView)findViewById(2131361994));
    this.m_contactThumbnailWrapper = findViewById(2131361993);
    this.m_contactThumbnail = ((ImageView)findViewById(2131361995));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.MessageListCompoundItemView
 * JD-Core Version:    0.6.2
 */