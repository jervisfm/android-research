package com.sgiggle.production.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationMessagePicture;
import com.sgiggle.production.util.FileImageLoader;

public class MessageListCompoundItemViewPicture extends MessageListCompoundItemView
{
  protected ProgressBar m_downloadProgressBar;
  protected ImageView m_thumbnail;
  protected ProgressBar m_uploadProgressBar;

  public MessageListCompoundItemViewPicture(Context paramContext)
  {
    this(paramContext, null);
  }

  public MessageListCompoundItemViewPicture(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public MessageListCompoundItemViewPicture(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void fill(ConversationMessage paramConversationMessage, boolean paramBoolean)
  {
    super.fill(paramConversationMessage, paramBoolean);
    ConversationMessagePicture localConversationMessagePicture = (ConversationMessagePicture)paramConversationMessage;
    Bitmap localBitmap;
    if ((!TextUtils.isEmpty(localConversationMessagePicture.getThumbnailLocalPath())) && (localConversationMessagePicture.shouldHaveImage()))
    {
      localBitmap = FileImageLoader.getInstance().getLoadedBitmap(localConversationMessagePicture);
      if (localBitmap == null)
        FileImageLoader.getInstance().addLoadTask(localConversationMessagePicture, this.m_thumbnail);
    }
    while (true)
    {
      this.m_thumbnail.setImageBitmap(localBitmap);
      ProgressBar localProgressBar1 = this.m_uploadProgressBar;
      int i;
      ProgressBar localProgressBar2;
      if (localConversationMessagePicture.isStatusSending())
      {
        i = 0;
        localProgressBar1.setVisibility(i);
        localProgressBar2 = this.m_downloadProgressBar;
        if (!localConversationMessagePicture.isLoadingStatusLoading())
          break label137;
      }
      label137: for (int j = 0; ; j = 4)
      {
        localProgressBar2.setVisibility(j);
        this.m_uploadProgressBar.setProgress(localConversationMessagePicture.getProgress());
        this.m_downloadProgressBar.setProgress(localConversationMessagePicture.getProgress());
        return;
        i = 4;
        break;
      }
      localBitmap = null;
    }
  }

  protected int getContentResId()
  {
    return 2130903092;
  }

  protected void initViews()
  {
    super.initViews();
    this.m_thumbnail = ((ImageView)findViewById(2131362004));
    this.m_thumbnail.setImageBitmap(null);
    this.m_uploadProgressBar = ((ProgressBar)findViewById(2131362007));
    this.m_downloadProgressBar = ((ProgressBar)findViewById(2131362005));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.MessageListCompoundItemViewPicture
 * JD-Core Version:    0.6.2
 */