package com.sgiggle.production.widget;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import com.sgiggle.production.model.ConversationMessage;

public class MessageListCompoundItemViewText extends MessageListCompoundItemView
{
  protected TextView m_text;

  public MessageListCompoundItemViewText(Context paramContext)
  {
    this(paramContext, null);
  }

  public MessageListCompoundItemViewText(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public MessageListCompoundItemViewText(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private static void ignoreReleaseIfWindowNotFocused(TextView paramTextView)
  {
    if (paramTextView.getAutoLinkMask() > 0)
      paramTextView.setOnTouchListener(new View.OnTouchListener()
      {
        public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
        {
          return (paramAnonymousMotionEvent.getAction() == 1) && (!this.val$textView.hasWindowFocus());
        }
      });
  }

  public void fill(ConversationMessage paramConversationMessage, boolean paramBoolean)
  {
    super.fill(paramConversationMessage, paramBoolean);
    this.m_text.setText(paramConversationMessage.getText());
  }

  protected int getContentResId()
  {
    return 2130903093;
  }

  protected void initViews()
  {
    super.initViews();
    this.m_text = ((TextView)findViewById(2131362008));
    if (Build.VERSION.SDK_INT == 16)
      ignoreReleaseIfWindowNotFocused(this.m_text);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.MessageListCompoundItemViewText
 * JD-Core Version:    0.6.2
 */