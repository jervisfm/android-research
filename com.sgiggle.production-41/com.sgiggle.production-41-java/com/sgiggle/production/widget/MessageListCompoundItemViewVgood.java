package com.sgiggle.production.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.media_engine.MediaEngineMessage.DisplayVgoodPaymentMessage;
import com.sgiggle.messaging.MessageRouter;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationMessageVGood;
import com.sgiggle.production.util.FileImageLoader;

public class MessageListCompoundItemViewVgood extends MessageListCompoundItemView
  implements View.OnClickListener
{
  protected TextView m_buyLink;
  protected View m_buyWrapper;
  protected String m_productId;
  protected ImageView m_thumbnail;

  public MessageListCompoundItemViewVgood(Context paramContext)
  {
    this(paramContext, null);
  }

  public MessageListCompoundItemViewVgood(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public MessageListCompoundItemViewVgood(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private void setDefaultImage()
  {
    this.m_thumbnail.setImageResource(2130837679);
  }

  public void fill(ConversationMessage paramConversationMessage, boolean paramBoolean)
  {
    super.fill(paramConversationMessage, paramBoolean);
    ConversationMessageVGood localConversationMessageVGood = (ConversationMessageVGood)paramConversationMessage;
    View localView;
    if (localConversationMessageVGood.getVGoodIconPath() != null)
    {
      Bitmap localBitmap = FileImageLoader.getInstance().getLoadedBitmap(localConversationMessageVGood);
      if (localBitmap != null)
      {
        this.m_thumbnail.setImageBitmap(localBitmap);
        localView = this.m_buyWrapper;
        if (!localConversationMessageVGood.canBePurchased())
          break label121;
      }
    }
    label121: for (int i = 0; ; i = 8)
    {
      localView.setVisibility(i);
      this.m_buyLink.setText(2131296607);
      this.m_productId = localConversationMessageVGood.getProductId();
      return;
      if (localConversationMessageVGood.shouldHaveImage())
      {
        setDefaultImage();
        FileImageLoader.getInstance().addLoadTask(localConversationMessageVGood, this.m_thumbnail);
        break;
      }
      setDefaultImage();
      break;
      setDefaultImage();
      break;
    }
  }

  protected int getContentResId()
  {
    return 2130903094;
  }

  protected void initViews()
  {
    super.initViews();
    this.m_thumbnail = ((ImageView)findViewById(2131362004));
    this.m_buyWrapper = findViewById(2131362010);
    this.m_buyLink = ((TextView)findViewById(2131361833));
    this.m_buyLink.setOnClickListener(this);
  }

  public void onClick(View paramView)
  {
    MediaEngineMessage.DisplayVgoodPaymentMessage localDisplayVgoodPaymentMessage = new MediaEngineMessage.DisplayVgoodPaymentMessage();
    MessageRouter.getInstance().postMessage("jingle", localDisplayVgoodPaymentMessage);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.MessageListCompoundItemViewVgood
 * JD-Core Version:    0.6.2
 */