package com.sgiggle.production.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.production.model.ConversationMessageVideo;
import com.sgiggle.production.util.FileImageLoader;

public class MessageListCompoundItemViewVideomail extends MessageListCompoundItemView
{
  protected View m_playButton;
  protected View m_spinner;
  protected ImageView m_thumbnail;
  protected View m_thumbnail_wrapper;
  protected ProgressBar m_uploadProgressBar;

  public MessageListCompoundItemViewVideomail(Context paramContext)
  {
    this(paramContext, null);
  }

  public MessageListCompoundItemViewVideomail(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public MessageListCompoundItemViewVideomail(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void fill(ConversationMessage paramConversationMessage, boolean paramBoolean)
  {
    super.fill(paramConversationMessage, paramBoolean);
    ConversationMessageVideo localConversationMessageVideo = (ConversationMessageVideo)paramConversationMessage;
    boolean bool1 = TextUtils.isEmpty(localConversationMessageVideo.getThumbnailLocalPath());
    Bitmap localBitmap = null;
    if (!bool1)
    {
      boolean bool2 = localConversationMessageVideo.shouldHaveImage();
      localBitmap = null;
      if (bool2)
      {
        localBitmap = FileImageLoader.getInstance().getLoadedBitmap(localConversationMessageVideo);
        if (localBitmap == null)
          FileImageLoader.getInstance().addLoadTask(localConversationMessageVideo, this.m_thumbnail);
      }
    }
    this.m_thumbnail.setImageBitmap(localBitmap);
    ProgressBar localProgressBar;
    if (localConversationMessageVideo.isLoadingStatusLoading())
    {
      this.m_spinner.setVisibility(0);
      this.m_playButton.setVisibility(4);
      localProgressBar = this.m_uploadProgressBar;
      if (!localConversationMessageVideo.isStatusSending())
        break label153;
    }
    label153: for (int i = 0; ; i = 4)
    {
      localProgressBar.setVisibility(i);
      this.m_uploadProgressBar.setProgress(localConversationMessageVideo.getProgress());
      return;
      this.m_spinner.setVisibility(4);
      this.m_playButton.setVisibility(0);
      break;
    }
  }

  protected int getContentResId()
  {
    return 2130903095;
  }

  protected void initViews()
  {
    super.initViews();
    this.m_thumbnail_wrapper = findViewById(2131362003);
    this.m_thumbnail = ((ImageView)findViewById(2131362004));
    this.m_playButton = findViewById(2131362006);
    this.m_spinner = findViewById(2131362011);
    this.m_uploadProgressBar = ((ProgressBar)findViewById(2131362007));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.MessageListCompoundItemViewVideomail
 * JD-Core Version:    0.6.2
 */