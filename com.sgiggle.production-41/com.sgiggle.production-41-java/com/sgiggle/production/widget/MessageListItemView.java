package com.sgiggle.production.widget;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sgiggle.production.Utils;
import com.sgiggle.production.model.ConversationMessage;

public abstract class MessageListItemView extends RelativeLayout
{
  private final String JUST_NOW_LABEL;
  protected Context m_context;
  protected TextView m_statusRead;
  protected TextView m_timestamp;

  public MessageListItemView(Context paramContext)
  {
    this(paramContext, null);
  }

  public MessageListItemView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public MessageListItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.m_context = paramContext;
    this.JUST_NOW_LABEL = this.m_context.getString(2131296563);
    inflateLayout(LayoutInflater.from(paramContext));
    initViews();
  }

  public void fill(ConversationMessage paramConversationMessage, boolean paramBoolean)
  {
    String str = "<b>" + getContext().getString(2131296617) + "</b>";
    if (paramConversationMessage.getReadStatusTimestampMs() != -1L)
      str = str + ": " + Utils.getRelativeTimeString(getContext(), paramConversationMessage.getReadStatusTimestampMs(), this.JUST_NOW_LABEL);
    this.m_statusRead.setText(Html.fromHtml(str));
    this.m_timestamp.setText(Utils.getRelativeTimeString(getContext(), paramConversationMessage.getTimestampMs(), this.JUST_NOW_LABEL));
  }

  protected abstract void inflateLayout(LayoutInflater paramLayoutInflater);

  protected void initViews()
  {
    this.m_timestamp = ((TextView)findViewById(2131361992));
    this.m_statusRead = ((TextView)findViewById(2131362002));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.MessageListItemView
 * JD-Core Version:    0.6.2
 */