package com.sgiggle.production.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgiggle.production.model.ConversationMessage;
import com.sgiggle.xmpp.SessionMessages.ConversationMessageType;

public class MessageListSimpleItemView extends MessageListItemView
{
  protected TextView m_duration;
  protected ImageView m_icon;
  protected TextView m_text;

  public MessageListSimpleItemView(Context paramContext)
  {
    this(paramContext, null);
  }

  public MessageListSimpleItemView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public MessageListSimpleItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void fill(ConversationMessage paramConversationMessage, boolean paramBoolean)
  {
    super.fill(paramConversationMessage, paramBoolean);
    SessionMessages.ConversationMessageType localConversationMessageType = paramConversationMessage.getType();
    String str = paramConversationMessage.getText();
    1.$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType[localConversationMessageType.ordinal()];
    if (-1 > 0)
    {
      this.m_icon.setImageResource(-1);
      this.m_icon.setVisibility(0);
    }
    while (true)
    {
      this.m_text.setText(str);
      return;
      this.m_icon.setVisibility(4);
    }
  }

  protected void inflateLayout(LayoutInflater paramLayoutInflater)
  {
    paramLayoutInflater.inflate(2130903096, this);
  }

  protected void initViews()
  {
    super.initViews();
    this.m_icon = ((ImageView)findViewById(2131362012));
    this.m_text = ((TextView)findViewById(2131361877));
    this.m_duration = ((TextView)findViewById(2131362013));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.MessageListSimpleItemView
 * JD-Core Version:    0.6.2
 */