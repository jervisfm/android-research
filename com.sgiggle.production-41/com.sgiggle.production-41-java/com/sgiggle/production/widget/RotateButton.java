package com.sgiggle.production.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;

public class RotateButton extends Button
{
  private static final int ANIMATION_SPEED = 180;
  private long m_animationEndTime;
  private long m_animationStartTime;
  private int m_currentAngle;
  private boolean m_lockUntilLayoutOrientationReached = false;
  private boolean m_rotateCW;
  private int m_startAngle;
  private int m_targetAngle;

  public RotateButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public void draw(Canvas paramCanvas)
  {
    int j;
    int m;
    int n;
    if (this.m_currentAngle != this.m_targetAngle)
    {
      long l = AnimationUtils.currentAnimationTimeMillis();
      if (l >= this.m_animationEndTime)
        break label147;
      j = (int)(l - this.m_animationStartTime);
      int k = this.m_startAngle;
      if (!this.m_rotateCW)
        break label124;
      m = k + j * 180 / 1000;
      if (m < 0)
        break label132;
      n = m % 360;
      label74: this.m_currentAngle = n;
      invalidate();
    }
    while (true)
    {
      int i = paramCanvas.getSaveCount();
      paramCanvas.rotate(-this.m_currentAngle, getWidth() / 2, getHeight() / 2);
      super.draw(paramCanvas);
      paramCanvas.restoreToCount(i);
      return;
      label124: j = -j;
      break;
      label132: n = 360 + m % 360;
      break label74;
      label147: this.m_currentAngle = this.m_targetAngle;
    }
  }

  public void lockUntilLayoutOrientationReached(boolean paramBoolean)
  {
    this.m_lockUntilLayoutOrientationReached = paramBoolean;
  }

  public void setDegree(int paramInt)
  {
    int i;
    if (paramInt >= 0)
    {
      i = paramInt % 360;
      if (!this.m_lockUntilLayoutOrientationReached)
        break label40;
      if (i == 0)
        break label35;
    }
    label35: label40: 
    while (i == this.m_targetAngle)
    {
      return;
      i = 360 + paramInt % 360;
      break;
      this.m_lockUntilLayoutOrientationReached = false;
    }
    this.m_targetAngle = i;
    this.m_startAngle = this.m_currentAngle;
    this.m_animationStartTime = AnimationUtils.currentAnimationTimeMillis();
    int j = this.m_targetAngle - this.m_currentAngle;
    if (j >= 0)
    {
      if (j > 180)
        j -= 360;
      if (j < 0)
        break label144;
    }
    label144: for (boolean bool = true; ; bool = false)
    {
      this.m_rotateCW = bool;
      this.m_animationEndTime = (this.m_animationStartTime + 1000 * Math.abs(j) / 180);
      invalidate();
      return;
      j += 360;
      break;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.RotateButton
 * JD-Core Version:    0.6.2
 */