package com.sgiggle.production.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import com.sgiggle.util.Log;

public class ScrollToRefreshListView extends ListView
  implements AbsListView.OnScrollListener
{
  private static final String TAG = "Tango.HistoryListView";
  private View m_headerViewContent;
  private int m_headerViewHeight = 0;
  private boolean m_isRefreshing = false;
  private ScrollToRefreshListViewListener m_listener;
  private AbsListView.OnScrollListener m_onScrollListener;
  private int m_originalTranscriptMode = 0;
  private int m_previousTotal = 0;
  private boolean m_refreshEnabled = false;

  public ScrollToRefreshListView(Context paramContext)
  {
    super(paramContext);
    initialize(paramContext);
  }

  public ScrollToRefreshListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initialize(paramContext);
  }

  public ScrollToRefreshListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initialize(paramContext);
  }

  private void initialize(Context paramContext)
  {
    this.m_originalTranscriptMode = getTranscriptMode();
    View localView = ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130903106, this, false);
    this.m_headerViewContent = localView.findViewById(2131361880);
    addHeaderView(localView, null, false);
    measureView(localView);
    this.m_headerViewHeight = localView.getMeasuredHeight();
    resetRefreshState(false);
    super.setOnScrollListener(this);
  }

  private void measureView(View paramView)
  {
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    if (localLayoutParams == null)
      localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
    int i = ViewGroup.getChildMeasureSpec(0, 0, localLayoutParams.width);
    int j = localLayoutParams.height;
    if (j > 0);
    for (int k = View.MeasureSpec.makeMeasureSpec(j, 1073741824); ; k = View.MeasureSpec.makeMeasureSpec(0, 0))
    {
      paramView.measure(i, k);
      return;
    }
  }

  public void onRefreshDone(int paramInt, boolean paramBoolean)
  {
    this.m_isRefreshing = false;
    setRefreshEnabled(paramBoolean);
    int i = getCount();
    int j = paramInt + 1;
    int k = this.m_headerViewHeight;
    if (j < i)
    {
      Log.d("Tango.HistoryListView", "setSelectionFromTop to: " + j);
      setSelectionFromTop(j, k);
    }
  }

  public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt1 == 0) && (this.m_refreshEnabled) && (!this.m_isRefreshing) && (paramInt3 != 0) && (paramInt3 != this.m_previousTotal))
    {
      this.m_isRefreshing = true;
      this.m_previousTotal = paramInt3;
      if (this.m_listener != null)
      {
        Log.d("Tango.HistoryListView", "onScroll: requesting refresh");
        this.m_listener.onRefreshRequested();
      }
    }
    if (this.m_onScrollListener != null)
      this.m_onScrollListener.onScroll(paramAbsListView, paramInt1, paramInt2, paramInt3);
    if (this.m_originalTranscriptMode == 1)
    {
      if ((paramInt2 != 0) && (paramInt2 != paramInt3) && (paramInt1 + paramInt2 != paramInt3))
        break label128;
      if (getTranscriptMode() != 2)
        setTranscriptMode(2);
    }
    label128: 
    while (getTranscriptMode() == 0)
      return;
    setTranscriptMode(0);
  }

  public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
  {
    if (this.m_onScrollListener != null)
      this.m_onScrollListener.onScrollStateChanged(paramAbsListView, paramInt);
  }

  public void resetRefreshState(boolean paramBoolean)
  {
    setRefreshEnabled(paramBoolean);
    this.m_previousTotal = 0;
    this.m_isRefreshing = false;
  }

  public void setListener(ScrollToRefreshListViewListener paramScrollToRefreshListViewListener)
  {
    this.m_listener = paramScrollToRefreshListViewListener;
  }

  public void setOnScrollListener(AbsListView.OnScrollListener paramOnScrollListener)
  {
    this.m_onScrollListener = paramOnScrollListener;
  }

  public void setRefreshEnabled(boolean paramBoolean)
  {
    Log.d("Tango.HistoryListView", "setRefreshAvailable to " + paramBoolean);
    if (paramBoolean)
      this.m_headerViewContent.setVisibility(0);
    while (true)
    {
      this.m_refreshEnabled = paramBoolean;
      return;
      this.m_headerViewContent.setVisibility(4);
    }
  }

  public void setSelectionToBottom()
  {
    int i = getCount();
    if (i > 0)
      setSelection(i - 1);
  }

  public static abstract interface ScrollToRefreshListViewListener
  {
    public abstract void onRefreshRequested();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.ScrollToRefreshListView
 * JD-Core Version:    0.6.2
 */