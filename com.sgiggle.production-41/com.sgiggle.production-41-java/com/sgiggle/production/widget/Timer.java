package com.sgiggle.production.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.sgiggle.util.Log;

public class Timer extends FrameLayout
{
  private static final int MESSAGE_UPDATE_COUNTER = 0;
  private static final String TAG = "Tango.Timer";
  private Callback m_callback;
  private long m_duration;
  private long m_maxDuration = 30000L;
  private int m_orientation;
  private TextView m_recordingTime;
  private long m_startCallbackAt = 0L;
  private long m_startTime;
  private Handler m_timerHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default:
        return;
      case 0:
      }
      Timer.this.updateTimer();
    }
  };

  public Timer(Context paramContext)
  {
    this(paramContext, null);
  }

  public Timer(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public Timer(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public static String formatTime(long paramLong)
  {
    long l1 = paramLong / 1000L;
    long l2 = l1 / 60L;
    long l3 = l1 % 60L;
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Long.valueOf(l3);
    String str1 = String.format("%02d", arrayOfObject1);
    Object[] arrayOfObject2 = new Object[1];
    arrayOfObject2[0] = Long.valueOf(l2);
    String str2 = String.format("%02d", arrayOfObject2);
    return str2 + ":" + str1;
  }

  private void updateTimer()
  {
    long l1 = SystemClock.uptimeMillis() - this.m_startTime;
    this.m_duration = l1;
    this.m_recordingTime.setText(formatTime(this.m_duration));
    if ((this.m_duration > this.m_startCallbackAt) && (this.m_callback != null))
      this.m_callback.onTimerUpdated(l1, this.m_maxDuration - this.m_duration);
    if (this.m_duration >= this.m_maxDuration)
    {
      this.m_timerHandler.removeMessages(0);
      return;
    }
    long l2 = 1000L - l1 % 1000L;
    this.m_timerHandler.sendEmptyMessageDelayed(0, l2);
  }

  public long getDuration()
  {
    return this.m_duration;
  }

  public long getMaxTimerDuration()
  {
    return this.m_maxDuration;
  }

  public void hide()
  {
    this.m_recordingTime.setVisibility(8);
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.m_recordingTime = ((TextView)findViewById(2131362162));
    reset();
  }

  public void reset()
  {
    this.m_recordingTime.setText(formatTime(0L));
    this.m_recordingTime.setTextColor(-1);
  }

  public void setMaxTimerDuration(long paramLong)
  {
    this.m_maxDuration = paramLong;
  }

  public void setOrientation(int paramInt)
  {
    if (paramInt % 90 == 0)
    {
      this.m_orientation = (paramInt % 360);
      if (this.m_orientation < 0)
        this.m_orientation = (360 + this.m_orientation);
      return;
    }
    Log.e("Tango.Timer", "Invalid orientation: " + paramInt);
  }

  public void setStartCallbackAt(long paramLong)
  {
    this.m_startCallbackAt = paramLong;
  }

  public void setTimerCallback(Callback paramCallback)
  {
    this.m_callback = paramCallback;
  }

  public void start()
  {
    reset();
    this.m_startTime = SystemClock.uptimeMillis();
    updateTimer();
    this.m_recordingTime.setVisibility(0);
  }

  public void stop()
  {
    this.m_timerHandler.removeMessages(0);
    Log.d("Tango.Timer", "Timer stopped at " + this.m_duration + "ms");
    if (this.m_callback != null)
      this.m_callback.onTimerStopped();
  }

  public void stopAtMaxDuration()
  {
    stop();
    this.m_duration = this.m_maxDuration;
    this.m_recordingTime.setText(formatTime(this.m_duration));
  }

  public static abstract interface Callback
  {
    public abstract void onTimerStopped();

    public abstract void onTimerUpdated(long paramLong1, long paramLong2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.Timer
 * JD-Core Version:    0.6.2
 */