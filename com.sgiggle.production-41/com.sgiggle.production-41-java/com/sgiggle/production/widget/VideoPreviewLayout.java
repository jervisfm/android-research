package com.sgiggle.production.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class VideoPreviewLayout extends ViewGroup
{
  private double m_aspectRatio = 1.333333333333333D;
  private final DisplayMetrics m_displayMetrics = new DisplayMetrics();
  private FrameLayout m_frame;
  private OnSizeChangedListener m_sizeListener;

  public VideoPreviewLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    ((Activity)paramContext).getWindowManager().getDefaultDisplay().getMetrics(this.m_displayMetrics);
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.m_frame = ((FrameLayout)findViewById(2131362158));
    if (this.m_frame == null)
      throw new IllegalStateException("Must provide a child frame layout with id as \"preview_frame\"");
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = getWidth();
    int j = getHeight();
    FrameLayout localFrameLayout = this.m_frame;
    int k = localFrameLayout.getPaddingLeft() + localFrameLayout.getPaddingRight();
    int m = localFrameLayout.getPaddingBottom() + localFrameLayout.getPaddingTop();
    int n = j - m;
    int i1 = i - k;
    if (i1 > n * this.m_aspectRatio)
      i1 = (int)(0.5D + n * this.m_aspectRatio);
    while (true)
    {
      int i2 = i1 + k;
      int i3 = n + m;
      int i4 = (paramInt3 - paramInt1 - i2) / 2;
      int i5 = (paramInt4 - paramInt2 - i3) / 2;
      this.m_frame.measure(View.MeasureSpec.makeMeasureSpec(i2, 1073741824), View.MeasureSpec.makeMeasureSpec(i3, 1073741824));
      this.m_frame.layout(paramInt1 + i4, paramInt2 + i5, paramInt3 - i4, paramInt4 - i5);
      if (this.m_sizeListener != null)
        this.m_sizeListener.onSizeChanged();
      return;
      n = (int)(0.5D + i1 / this.m_aspectRatio);
    }
  }

  public void setAspectRatio(double paramDouble)
  {
    if (paramDouble <= 0.0D)
      throw new IllegalArgumentException("Invalid ratio");
    if (this.m_aspectRatio != paramDouble)
    {
      this.m_aspectRatio = paramDouble;
      requestLayout();
    }
  }

  public void setOnSizeChangedListener(OnSizeChangedListener paramOnSizeChangedListener)
  {
    this.m_sizeListener = paramOnSizeChangedListener;
  }

  public static abstract interface OnSizeChangedListener
  {
    public abstract void onSizeChanged();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.VideoPreviewLayout
 * JD-Core Version:    0.6.2
 */