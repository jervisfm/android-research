package com.sgiggle.production.widget;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.sgiggle.util.Log;

public class VideomailMediaController extends MediaController
  implements View.OnClickListener
{
  private static final int HIDE_CONTROLLER = 0;
  private static final String TAG = "Tango.VideomailMediaController";
  private static final int TIMEOUT = 4000;
  private static final int UPDATE_PROGRESS = 1;
  private View m_anchor;
  private Button m_btnCancel;
  private Button m_btnDelete;
  private Button m_btnPlayPause;
  private Button m_btnReply;
  private Button m_btnRetake;
  private Button m_btnSend;
  private Callback m_callback;
  private boolean m_canSeek = isSeekEnabled();
  private TextView m_elapsedTime;
  private boolean m_enabled = true;
  private Handler m_handler;
  private LayoutInflater m_inflater;
  private boolean m_isInitialSeekComplete;
  private boolean m_isProgressUiFrozen = false;
  private View m_mediaControllerView;
  private Mode m_mode;
  private MediaController.MediaPlayerControl m_player;
  private ProgressBar m_progress;
  private View m_seekBarWrapper;
  private SeekbarListener m_seekListener = new SeekbarListener(null);
  private boolean m_showAlways = false;
  private boolean m_showing = true;
  private TextView m_totalTime;

  public VideomailMediaController(Context paramContext)
  {
    super(paramContext);
    if (!this.m_canSeek);
    for (boolean bool = true; ; bool = false)
    {
      this.m_isInitialSeekComplete = bool;
      this.m_mode = Mode.VIEW_VIDEOMAIL;
      this.m_handler = new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          switch (paramAnonymousMessage.what)
          {
          default:
          case 0:
          case 1:
          }
          int i;
          do
          {
            do
              return;
            while (VideomailMediaController.this.m_showAlways);
            VideomailMediaController.this.hide(false);
            return;
            i = VideomailMediaController.this.setProgress();
          }
          while (!VideomailMediaController.this.m_showing);
          sendMessageDelayed(obtainMessage(1), 1000 - i % 1000);
        }
      };
      this.m_inflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
      this.m_mediaControllerView = this.m_inflater.inflate(2130903145, null);
      return;
    }
  }

  public VideomailMediaController(Context paramContext, boolean paramBoolean, Mode paramMode)
  {
    this(paramContext);
    this.m_showAlways = paramBoolean;
    this.m_mode = paramMode;
  }

  private void buildAndAttachControllerView()
  {
    if ((this.m_anchor != null) && ((this.m_anchor instanceof RelativeLayout)))
    {
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
      localLayoutParams.addRule(12);
      if (((RelativeLayout)this.m_anchor).getChildAt(1) == this.m_mediaControllerView)
        ((RelativeLayout)this.m_anchor).removeViewAt(1);
      ((RelativeLayout)this.m_anchor).addView(this.m_mediaControllerView, 1, localLayoutParams);
      initControllerView(this.m_mediaControllerView);
      if (!this.m_enabled)
        hide(true);
    }
  }

  private void delayHide()
  {
    Message localMessage = this.m_handler.obtainMessage(0);
    if (localMessage != null)
    {
      this.m_handler.removeMessages(0);
      this.m_handler.sendMessageDelayed(localMessage, 4000L);
    }
  }

  private String formatTime(int paramInt)
  {
    int i = paramInt / 1000;
    int j = i / 60;
    int k = i % 60;
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Integer.valueOf(k);
    String str1 = String.format("%02d", arrayOfObject1);
    Object[] arrayOfObject2 = new Object[1];
    arrayOfObject2[0] = Integer.valueOf(j);
    String str2 = String.format("%02d", arrayOfObject2);
    return str2 + ":" + str1;
  }

  private void hide(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.m_handler.removeMessages(0);
      this.m_mediaControllerView.setVisibility(8);
    }
    if (this.m_showing);
    try
    {
      this.m_handler.removeMessages(1);
      slideOut();
      this.m_showing = false;
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      while (true)
        Log.w("Tango.VideomailMediaController", "MediaController already removed");
    }
  }

  private void initControllerView(View paramView)
  {
    this.m_btnPlayPause = ((Button)paramView.findViewById(2131362156));
    this.m_btnReply = ((Button)paramView.findViewById(2131362153));
    this.m_btnDelete = ((Button)paramView.findViewById(2131362154));
    this.m_btnSend = ((Button)paramView.findViewById(2131362020));
    this.m_btnRetake = ((Button)paramView.findViewById(2131362155));
    this.m_btnCancel = ((Button)paramView.findViewById(2131362019));
    this.m_progress = ((ProgressBar)paramView.findViewById(2131362150));
    this.m_seekBarWrapper = paramView.findViewById(2131362148);
    if ((this.m_progress instanceof SeekBar))
    {
      SeekBar localSeekBar = (SeekBar)this.m_progress;
      localSeekBar.setOnSeekBarChangeListener(this.m_seekListener);
      localSeekBar.setEnabled(this.m_canSeek);
    }
    this.m_totalTime = ((TextView)paramView.findViewById(2131362152));
    this.m_elapsedTime = ((TextView)paramView.findViewById(2131362151));
    this.m_btnPlayPause.setOnClickListener(this);
    if (this.m_mode == Mode.REVIEW_RECORDED_VIDEOMAIL)
    {
      this.m_btnRetake.setOnClickListener(this);
      this.m_btnSend.setOnClickListener(this);
      this.m_btnCancel.setOnClickListener(this);
      this.m_btnReply.setVisibility(8);
      this.m_btnDelete.setVisibility(8);
      this.m_btnRetake.setVisibility(0);
      this.m_btnSend.setVisibility(0);
      this.m_btnReply = null;
      this.m_btnDelete = null;
    }
    while (true)
    {
      setDurationAndPosition(0, 0);
      return;
      this.m_btnDelete.setVisibility(8);
      this.m_btnDelete = null;
      this.m_btnReply.setVisibility(0);
      this.m_btnReply.setOnClickListener(this);
      this.m_btnRetake.setVisibility(8);
      this.m_btnSend.setVisibility(8);
      this.m_btnRetake = null;
      this.m_btnSend = null;
    }
  }

  private static boolean isSeekEnabled()
  {
    return (!Build.MANUFACTURER.equalsIgnoreCase("Samsung")) || (!Build.MODEL.equals("SPH-D700"));
  }

  private void setDurationAndPosition(int paramInt1, int paramInt2)
  {
    if (this.m_totalTime != null)
      this.m_totalTime.setText(formatTime(paramInt1));
    if (this.m_elapsedTime != null)
      this.m_elapsedTime.setText(formatTime(paramInt2));
  }

  private int setProgress()
  {
    if (this.m_player == null)
      return 0;
    if (this.m_isProgressUiFrozen)
    {
      Log.d("Tango.VideomailMediaController", "Progress UI is locked, seekbar and time won't be updated until unlocked");
      return 0;
    }
    int i = this.m_player.getCurrentPosition();
    int j = this.m_player.getDuration();
    if (this.m_progress != null)
    {
      if (j > 0)
      {
        long l = ()Math.floor((999L + 1000L * i) / j);
        if (j - i < 1000)
          l = 1000L;
        this.m_progress.setMax(1000);
        this.m_progress.setProgress((int)l);
      }
      int k = this.m_player.getBufferPercentage();
      this.m_progress.setSecondaryProgress(k * 10);
    }
    setDurationAndPosition(j, i);
    updatePlayIcon();
    return i;
  }

  private void slideIn()
  {
    if (this.m_mediaControllerView != null)
    {
      TranslateAnimation localTranslateAnimation = new TranslateAnimation(1, 0.0F, 1, 0.0F, 1, 1.0F, 1, 0.0F);
      localTranslateAnimation.setFillAfter(true);
      localTranslateAnimation.setDuration(1000L);
      this.m_mediaControllerView.setAnimation(localTranslateAnimation);
      this.m_mediaControllerView.setVisibility(0);
    }
  }

  private void slideOut()
  {
    if (this.m_mediaControllerView != null)
    {
      TranslateAnimation localTranslateAnimation = new TranslateAnimation(1, 0.0F, 1, 0.0F, 1, 0.0F, 1, 1.0F);
      localTranslateAnimation.setFillAfter(true);
      localTranslateAnimation.setDuration(1000L);
      this.m_mediaControllerView.setAnimation(localTranslateAnimation);
      this.m_mediaControllerView.setVisibility(8);
    }
  }

  public void finishedPlaying()
  {
    if (this.m_canSeek)
      this.m_player.seekTo(0);
    updatePlayIcon();
    if (this.m_callback != null)
    {
      if (!this.m_canSeek)
        this.m_callback.onRestart();
      this.m_callback.onPause();
    }
  }

  public int getDuration()
  {
    MediaController.MediaPlayerControl localMediaPlayerControl = this.m_player;
    int i = 0;
    if (localMediaPlayerControl != null)
      i = this.m_player.getDuration();
    return i;
  }

  public int getElapsedTime()
  {
    MediaController.MediaPlayerControl localMediaPlayerControl = this.m_player;
    int i = 0;
    if (localMediaPlayerControl != null)
      i = this.m_player.getCurrentPosition();
    return i;
  }

  public void hide()
  {
    if (!this.m_isInitialSeekComplete);
    while (this.m_showAlways)
      return;
    hide(true);
  }

  public boolean isShowing()
  {
    return this.m_showing;
  }

  public void onClick(View paramView)
  {
    delayHide();
    if (paramView == this.m_btnPlayPause)
      togglePlay();
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              return;
              if (paramView != this.m_btnReply)
                break;
            }
            while (this.m_callback == null);
            this.m_callback.onReply();
            return;
            if (paramView != this.m_btnDelete)
              break;
          }
          while (this.m_callback == null);
          this.m_callback.onDeleteMessage();
          return;
          if (paramView != this.m_btnRetake)
            break;
        }
        while (this.m_callback == null);
        this.m_callback.onRetake();
        return;
        if (paramView != this.m_btnSend)
          break;
        if (this.m_player.isPlaying())
          this.m_player.pause();
      }
      while (this.m_callback == null);
      this.m_callback.onSend();
      return;
    }
    while ((paramView != this.m_btnCancel) || (this.m_callback == null));
    this.m_callback.onCancel();
  }

  public void onFinishInflate()
  {
    if (this.m_mediaControllerView != null)
      initControllerView(this.m_mediaControllerView);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    show(4000);
    return true;
  }

  public void setAnchorView(View paramView)
  {
    this.m_anchor = paramView;
    removeAllViews();
    buildAndAttachControllerView();
  }

  public void setCallback(Callback paramCallback)
  {
    this.m_callback = paramCallback;
  }

  public void setEnabled(boolean paramBoolean)
  {
    if (this.m_btnReply != null)
      this.m_btnReply.setEnabled(paramBoolean);
    if (this.m_btnDelete != null)
      this.m_btnDelete.setEnabled(paramBoolean);
    if (this.m_btnPlayPause != null)
      this.m_btnPlayPause.setEnabled(paramBoolean);
  }

  public void setInitialSeekComplete()
  {
    this.m_isInitialSeekComplete = true;
  }

  public void setMediaPlayer(MediaController.MediaPlayerControl paramMediaPlayerControl)
  {
    this.m_player = paramMediaPlayerControl;
  }

  public void setProgressUiFrozen(boolean paramBoolean)
  {
    if (this.m_isProgressUiFrozen == paramBoolean);
    do
    {
      return;
      this.m_isProgressUiFrozen = paramBoolean;
    }
    while (this.m_isProgressUiFrozen);
    setProgress();
  }

  public void setReviewPlayState()
  {
    if (this.m_mode != Mode.REVIEW_RECORDED_VIDEOMAIL)
    {
      Log.e("Tango.VideomailMediaController", "Logic error: mode not supported.");
      return;
    }
    this.m_seekBarWrapper.setVisibility(0);
    this.m_btnSend.setEnabled(true);
    this.m_btnPlayPause.setVisibility(0);
    this.m_btnRetake.setVisibility(0);
    this.m_btnCancel.setVisibility(4);
  }

  public void setReviewSendState()
  {
    if (this.m_mode != Mode.REVIEW_RECORDED_VIDEOMAIL)
    {
      Log.e("Tango.VideomailMediaController", "Logic error: mode not supported.");
      return;
    }
    this.m_seekBarWrapper.setVisibility(4);
    this.m_btnSend.setEnabled(false);
    this.m_btnPlayPause.setVisibility(4);
    this.m_btnRetake.setVisibility(4);
    this.m_btnCancel.setVisibility(0);
  }

  public void show()
  {
    show(4000, true);
  }

  public void show(int paramInt, boolean paramBoolean)
  {
    if (!this.m_enabled);
    Message localMessage;
    do
    {
      do
      {
        return;
        if (!this.m_showing)
          slideIn();
        if ((!this.m_showing) && (this.m_anchor != null))
        {
          setProgress();
          if (this.m_btnPlayPause != null)
            this.m_btnPlayPause.requestFocus();
          this.m_showing = true;
        }
        this.m_handler.sendEmptyMessage(1);
      }
      while (this.m_showAlways);
      localMessage = this.m_handler.obtainMessage(0);
      this.m_handler.removeMessages(0);
    }
    while ((!paramBoolean) || (paramInt == 0));
    this.m_handler.sendMessageDelayed(localMessage, paramInt);
  }

  public void showAndStay()
  {
    show(4000, false);
  }

  public void showAndStayIfShort()
  {
    if ((this.m_player == null) || (this.m_player.getDuration() > 4000));
    for (boolean bool = true; ; bool = false)
    {
      show(4000, bool);
      return;
    }
  }

  public void togglePlay()
  {
    if (this.m_player.isPlaying())
    {
      this.m_player.pause();
      if (this.m_callback != null)
        this.m_callback.onPause();
    }
    while (true)
    {
      updatePlayIcon();
      return;
      this.m_player.start();
      showAndStayIfShort();
      if (this.m_callback != null)
        this.m_callback.onPlay();
    }
  }

  public void updatePlayIcon()
  {
    Button localButton = this.m_btnPlayPause;
    if (this.m_player.isPlaying());
    for (int i = 2130837579; ; i = 2130837583)
    {
      localButton.setBackgroundResource(i);
      return;
    }
  }

  public static abstract interface Callback
  {
    public abstract void onCancel();

    public abstract void onDeleteMessage();

    public abstract void onPause();

    public abstract void onPlay();

    public abstract void onReply();

    public abstract void onRestart();

    public abstract void onRetake();

    public abstract void onSend();
  }

  public static enum Mode
  {
    static
    {
      REVIEW_RECORDED_VIDEOMAIL = new Mode("REVIEW_RECORDED_VIDEOMAIL", 1);
      Mode[] arrayOfMode = new Mode[2];
      arrayOfMode[0] = VIEW_VIDEOMAIL;
      arrayOfMode[1] = REVIEW_RECORDED_VIDEOMAIL;
    }
  }

  private class SeekbarListener
    implements SeekBar.OnSeekBarChangeListener
  {
    private SeekbarListener()
    {
    }

    public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
    {
      if ((!paramBoolean) || (!VideomailMediaController.this.m_canSeek))
        return;
      long l = VideomailMediaController.this.m_player.getDuration() * paramInt / 1000L;
      VideomailMediaController.this.m_player.seekTo((int)l);
      VideomailMediaController.this.m_elapsedTime.setText(VideomailMediaController.this.formatTime((int)l));
    }

    public void onStartTrackingTouch(SeekBar paramSeekBar)
    {
      VideomailMediaController.this.m_handler.removeMessages(1);
      VideomailMediaController.this.m_handler.removeMessages(0);
    }

    public void onStopTrackingTouch(SeekBar paramSeekBar)
    {
      VideomailMediaController.this.setProgress();
      VideomailMediaController.this.updatePlayIcon();
      VideomailMediaController.this.m_handler.sendEmptyMessage(1);
      VideomailMediaController.this.show();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.VideomailMediaController
 * JD-Core Version:    0.6.2
 */