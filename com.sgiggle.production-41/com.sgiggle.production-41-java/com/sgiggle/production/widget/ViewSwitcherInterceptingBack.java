package com.sgiggle.production.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ViewSwitcher;

public class ViewSwitcherInterceptingBack extends ViewSwitcher
{
  public ViewSwitcherInterceptingBack(Context paramContext)
  {
    super(paramContext);
  }

  public ViewSwitcherInterceptingBack(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public boolean dispatchKeyEventPreIme(KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getKeyCode() == 4) && (getDisplayedChild() != 0))
    {
      showPrevious();
      return true;
    }
    return super.dispatchKeyEventPreIme(paramKeyEvent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.production.widget.ViewSwitcherInterceptingBack
 * JD-Core Version:    0.6.2
 */