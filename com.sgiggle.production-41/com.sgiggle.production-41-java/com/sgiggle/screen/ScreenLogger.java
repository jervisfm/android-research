package com.sgiggle.screen;

import com.sgiggle.util.Log;
import java.util.LinkedHashMap;

public class ScreenLogger
{
  private static final String TAG = "Tango.ScreenLogger";

  public ScreenLogger()
  {
    Log.d("Tango.ScreenLogger", "ScreenLogger(): ENTER.");
  }

  public native LinkedHashMap<String, String> getAllParameters();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.screen.ScreenLogger
 * JD-Core Version:    0.6.2
 */