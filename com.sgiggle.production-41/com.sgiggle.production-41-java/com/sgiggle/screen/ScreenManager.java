package com.sgiggle.screen;

import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public class ScreenManager
{
  private static final String TAG = "ScreenManager";
  private static Context context;
  private static int mode = 1;
  private static PowerManager pm;
  private static PowerManager.WakeLock wl;

  public static void disableAutoOff()
  {
  }

  public static void enableAutoOff()
  {
  }

  public static void keepOn()
  {
  }

  public static void normal()
  {
  }

  private static void release()
  {
  }

  public static void updateContext(Context paramContext)
  {
  }

  private static abstract interface Mode
  {
    public static final int DISABLE_AUTOOFF = 3;
    public static final int ENABLE_AUTOOFF = 2;
    public static final int KEEPON = 0;
    public static final int NORMAL = 1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.screen.ScreenManager
 * JD-Core Version:    0.6.2
 */