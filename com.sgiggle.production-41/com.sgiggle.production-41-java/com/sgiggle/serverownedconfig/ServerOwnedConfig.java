package com.sgiggle.serverownedconfig;

import com.sgiggle.util.Log;

public class ServerOwnedConfig
{
  public static final String CRASH_REPORT_FORM_KEY = "crashreport.form.key";
  public static final String CRASH_REPORT_FORM_URI = "crashreport.form.uri";
  public static final String CRASH_REPORT_LOGCAT_ARGS = "crashreport.logcatargs";
  static final int TAG = 102;

  public static int getInt32(String paramString, int paramInt)
  {
    int i = nativeGetInt32(paramString, paramInt);
    Log.v(102, "Server owned config requested " + paramString + "=" + i);
    return i;
  }

  public static String getString(String paramString1, String paramString2)
  {
    String str = nativeGetString(paramString1, paramString2);
    Log.v(102, "Server owned config requested " + paramString1 + "=" + str);
    return str;
  }

  private static native int nativeGetInt32(String paramString, int paramInt);

  private static native String nativeGetString(String paramString1, String paramString2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.serverownedconfig.ServerOwnedConfig
 * JD-Core Version:    0.6.2
 */