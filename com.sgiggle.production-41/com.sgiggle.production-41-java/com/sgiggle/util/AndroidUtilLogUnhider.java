package com.sgiggle.util;

import android.util.Log;

class AndroidUtilLogUnhider
{
  static final int ASSERT = 7;
  static final int DEBUG = 3;
  static final int ERROR = 6;
  static final int INFO = 4;
  static final int VERBOSE = 2;
  static final int WARN = 5;

  public static void println(int paramInt, String paramString1, String paramString2)
  {
    Log.println(paramInt, paramString1, paramString2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.util.AndroidUtilLogUnhider
 * JD-Core Version:    0.6.2
 */