package com.sgiggle.util;

import android.app.Application;
import android.text.TextUtils;
import com.sgiggle.serverownedconfig.ServerOwnedConfig;
import org.acra.ACRA;
import org.acra.ACRAConfiguration;
import org.acra.ErrorReporter;

public class ClientCrashReporter
{
  static final String TAG = "ClientCrashReporter";
  private static ClientCrashReporter s_me = null;
  private boolean m_isAcraInitialized = false;

  public static ClientCrashReporter getInstance()
  {
    try
    {
      if (s_me == null)
        s_me = new ClientCrashReporter();
      ClientCrashReporter localClientCrashReporter = s_me;
      return localClientCrashReporter;
    }
    finally
    {
    }
  }

  public void init(Application paramApplication)
  {
    String str1 = ServerOwnedConfig.getString("crashreport.form.uri", "");
    String str2 = ServerOwnedConfig.getString("crashreport.form.key", "");
    String str3 = ServerOwnedConfig.getString("crashreport.logcatargs", "-t|200|-v|threadtime");
    if ((!TextUtils.isEmpty(str1)) || (!TextUtils.isEmpty(str2)))
    {
      Log.i("ClientCrashReporter", "ACRA is enabled formUri=" + str1 + " formKey=" + str2);
      ACRAConfiguration localACRAConfiguration = ACRA.getConfig();
      if (!TextUtils.isEmpty(str1))
        localACRAConfiguration.setFormUri(str1);
      if (!TextUtils.isEmpty(str2))
        localACRAConfiguration.setFormKey(str2);
      localACRAConfiguration.setLogcatArguments(str3.split("\\|"));
      ACRA.init(paramApplication);
      this.m_isAcraInitialized = true;
      return;
    }
    Log.i("ClientCrashReporter", "ACRA is disabled");
  }

  public void reportException(Exception paramException)
  {
    if (this.m_isAcraInitialized)
      ACRA.getErrorReporter().handleException(paramException);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.util.ClientCrashReporter
 * JD-Core Version:    0.6.2
 */