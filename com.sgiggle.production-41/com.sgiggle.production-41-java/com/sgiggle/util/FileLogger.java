package com.sgiggle.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class FileLogger
{
  private static String TAG = "FileLogger";
  private static FileWriter m_fileStream = null;
  private static BufferedWriter m_out = null;

  public static void close()
  {
    try
    {
      if (m_out != null)
        m_out.close();
      if (m_fileStream != null)
        m_fileStream.close();
      Log.i(TAG, "FileLogger closed");
      return;
    }
    catch (Exception localException)
    {
      while (true)
        Log.v(TAG, "Failed to close FileLogger");
    }
    finally
    {
    }
  }

  public static void init()
  {
    try
    {
      if (m_fileStream == null)
      {
        m_fileStream = new FileWriter("/sdcard/networkout.txt", false);
        m_out = new BufferedWriter(m_fileStream);
        Log.i(TAG, "FileLogger created");
      }
      return;
    }
    catch (Exception localException)
    {
      while (true)
        Log.e(TAG, "Failed to open FileLogger " + localException.getMessage());
    }
    finally
    {
    }
  }

  public static void toFile(String paramString)
  {
    try
    {
      BufferedWriter localBufferedWriter = m_out;
      if (localBufferedWriter == null);
      while (true)
      {
        return;
        Log.w(TAG, "dump to FileLogger " + paramString);
        GregorianCalendar localGregorianCalendar = new GregorianCalendar();
        int i = localGregorianCalendar.get(10);
        int j = localGregorianCalendar.get(12);
        int k = localGregorianCalendar.get(13);
        String str = Integer.toString(i) + ":" + Integer.toString(j) + " " + Integer.toString(k) + " ";
        m_out.write(str + paramString);
        m_out.newLine();
        m_out.flush();
      }
    }
    catch (Exception localException)
    {
      while (true)
        Log.w(TAG, "dump to FileLogger error " + localException.getMessage());
    }
    finally
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.util.FileLogger
 * JD-Core Version:    0.6.2
 */