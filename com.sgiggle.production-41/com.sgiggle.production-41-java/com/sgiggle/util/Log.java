package com.sgiggle.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Log extends LogModule
{
  public static final int debug = 2;
  public static final int error = 16;
  public static final int fatal = 32;
  public static final int info = 4;
  private static boolean m_useTangoLogs = false;
  public static final int trace = 1;
  public static final int warn = 8;

  protected static StringBuilder addExceptionInfo(String paramString, Throwable paramThrowable)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramString != null)
      localStringBuilder.append(paramString);
    if (paramThrowable != null)
    {
      StringWriter localStringWriter = new StringWriter();
      PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
      paramThrowable.printStackTrace(localPrintWriter);
      String str = localStringWriter.toString();
      localPrintWriter.close();
      localStringBuilder.append(str);
    }
    return localStringBuilder;
  }

  public static native boolean addWriter(String paramString);

  public static int d(int paramInt, String paramString)
  {
    logTangoOrSystem(2, paramInt, paramString);
    return 1;
  }

  public static int d(int paramInt, String paramString, Throwable paramThrowable)
  {
    logTangoOrSystem(2, paramInt, addExceptionInfo(paramString, paramThrowable).toString());
    return 1;
  }

  public static int d(String paramString1, String paramString2)
  {
    return d(118, paramString1 + " : " + paramString2);
  }

  public static int d(String paramString1, String paramString2, Throwable paramThrowable)
  {
    return d(118, paramString1 + " : " + paramString2, paramThrowable);
  }

  public static int e(int paramInt, String paramString)
  {
    logTangoOrSystem(16, paramInt, paramString);
    return 1;
  }

  public static int e(int paramInt, String paramString, Throwable paramThrowable)
  {
    logTangoOrSystem(16, paramInt, addExceptionInfo(paramString, paramThrowable).toString());
    return 1;
  }

  public static int e(String paramString1, String paramString2)
  {
    return e(118, paramString1 + " : " + paramString2);
  }

  public static int e(String paramString1, String paramString2, Throwable paramThrowable)
  {
    return e(118, paramString1 + " : " + paramString2, paramThrowable);
  }

  public static native boolean gzCompressFile(String paramString1, String paramString2, int paramInt);

  public static int i(int paramInt, String paramString)
  {
    logTangoOrSystem(4, paramInt, paramString);
    return 1;
  }

  public static int i(int paramInt, String paramString, Throwable paramThrowable)
  {
    logTangoOrSystem(4, paramInt, addExceptionInfo(paramString, paramThrowable).toString());
    return 1;
  }

  public static int i(String paramString1, String paramString2)
  {
    return i(118, paramString1 + " : " + paramString2);
  }

  public static int i(String paramString1, String paramString2, Throwable paramThrowable)
  {
    return i(118, paramString1 + " : " + paramString2, paramThrowable);
  }

  public static native void log(int paramInt1, int paramInt2, String paramString);

  public static void log(int paramInt, String paramString)
  {
    log(4, paramInt, paramString);
  }

  private static void logTangoOrSystem(int paramInt1, int paramInt2, String paramString)
  {
    if (m_useTangoLogs)
    {
      log(paramInt1, paramInt2, paramString);
      return;
    }
    AndroidUtilLogUnhider.println(severity2androidPriority(paramInt1), "Tango", "moduleId: " + paramInt2 + " : " + paramString);
  }

  public static native boolean removeWriter(String paramString);

  public static void setUseTangoLogs(boolean paramBoolean)
  {
    m_useTangoLogs = paramBoolean;
  }

  private static int severity2androidPriority(int paramInt)
  {
    switch (paramInt)
    {
    case 16:
    default:
      return 6;
    case 1:
      return 2;
    case 2:
      return 3;
    case 4:
      return 4;
    case 8:
      return 5;
    case 32:
    }
    return 7;
  }

  public static int v(int paramInt, String paramString)
  {
    logTangoOrSystem(1, paramInt, paramString);
    return 1;
  }

  public static int v(int paramInt, String paramString, Throwable paramThrowable)
  {
    logTangoOrSystem(1, paramInt, addExceptionInfo(paramString, paramThrowable).toString());
    return 1;
  }

  public static int v(String paramString1, String paramString2)
  {
    return v(118, paramString1 + " : " + paramString2);
  }

  public static int v(String paramString1, String paramString2, Throwable paramThrowable)
  {
    return v(118, paramString1 + " : " + paramString2, paramThrowable);
  }

  public static int w(int paramInt, String paramString)
  {
    logTangoOrSystem(8, paramInt, paramString);
    return 1;
  }

  public static int w(int paramInt, String paramString, Throwable paramThrowable)
  {
    logTangoOrSystem(8, paramInt, addExceptionInfo(paramString, paramThrowable).toString());
    return 1;
  }

  public static int w(String paramString1, String paramString2)
  {
    return w(118, paramString1 + " : " + paramString2);
  }

  public static int w(String paramString1, String paramString2, Throwable paramThrowable)
  {
    return w(118, paramString1 + " : " + paramString2, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.util.Log
 * JD-Core Version:    0.6.2
 */