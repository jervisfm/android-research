package com.sgiggle.util;

public class LogReporter
{
  private static String TAG = "LogReporter";
  private static Class<? extends AppData> s_provider;

  public static native boolean enableUri(String paramString);

  private static AppData getAppData()
  {
    try
    {
      AppData localAppData = (AppData)s_provider.newInstance();
      return localAppData;
    }
    catch (Exception localException)
    {
      Log.v(TAG, "Failed to get an instance of AppData: " + localException.toString());
    }
    return null;
  }

  public static native String getBinLogFilePath();

  public static native String getLogEmail();

  public static String localStoragePath()
  {
    AppData localAppData = getAppData();
    if (localAppData == null)
      return null;
    return localAppData.localStoragePath();
  }

  public static native void onEmailSent();

  public static native String outFileName();

  public static void releaseInstance()
  {
    s_provider = null;
    releaseNativeLogReporter();
  }

  private static native void releaseNativeLogReporter();

  public static native boolean restore();

  public static boolean scheduleEmail()
  {
    AppData localAppData = getAppData();
    if (localAppData == null)
      return false;
    return localAppData.scheduleEmail();
  }

  public static void setAppDataProvider(Class<? extends AppData> paramClass)
  {
    s_provider = paramClass;
  }

  public static abstract interface AppData
  {
    public abstract String localStoragePath();

    public abstract boolean scheduleEmail();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.util.LogReporter
 * JD-Core Version:    0.6.2
 */