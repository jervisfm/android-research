package com.sgiggle.util;

public class MailValidator
{
  public static boolean isValid(String paramString)
  {
    return nativeIsValid(paramString);
  }

  public static native boolean nativeIsValid(String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     com.sgiggle.util.MailValidator
 * JD-Core Version:    0.6.2
 */