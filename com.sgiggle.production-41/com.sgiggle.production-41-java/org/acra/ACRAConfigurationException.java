package org.acra;

public class ACRAConfigurationException extends Exception
{
  private static final long serialVersionUID = -7355339673505996110L;

  public ACRAConfigurationException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.ACRAConfigurationException
 * JD-Core Version:    0.6.2
 */