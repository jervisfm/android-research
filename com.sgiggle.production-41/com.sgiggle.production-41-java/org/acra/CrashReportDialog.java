package org.acra;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.IOException;
import org.acra.collector.CrashReportData;
import org.acra.util.ToastSender;

public final class CrashReportDialog extends Activity
{
  String mReportFileName;
  private SharedPreferences prefs;
  private EditText userComment;
  private EditText userEmail;

  protected void cancelNotification()
  {
    ((NotificationManager)getSystemService("notification")).cancel(666);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mReportFileName = getIntent().getStringExtra("REPORT_FILE_NAME");
    Log.d(ACRA.LOG_TAG, "Opening CrashReportDialog for " + this.mReportFileName);
    if (this.mReportFileName == null)
      finish();
    requestWindowFeature(3);
    LinearLayout localLinearLayout1 = new LinearLayout(this);
    localLinearLayout1.setOrientation(1);
    localLinearLayout1.setPadding(10, 10, 10, 10);
    localLinearLayout1.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    ScrollView localScrollView = new ScrollView(this);
    localLinearLayout1.addView(localScrollView, new LinearLayout.LayoutParams(-1, -1, 1.0F));
    TextView localTextView1 = new TextView(this);
    localTextView1.setText(getText(ACRA.getConfig().resDialogText()));
    localScrollView.addView(localTextView1, -1, -1);
    int i = ACRA.getConfig().resDialogCommentPrompt();
    if (i != 0)
    {
      TextView localTextView2 = new TextView(this);
      localTextView2.setText(getText(i));
      localTextView2.setPadding(localTextView2.getPaddingLeft(), 10, localTextView2.getPaddingRight(), localTextView2.getPaddingBottom());
      localLinearLayout1.addView(localTextView2, new LinearLayout.LayoutParams(-1, -2));
      this.userComment = new EditText(this);
      this.userComment.setLines(2);
      localLinearLayout1.addView(this.userComment, new LinearLayout.LayoutParams(-1, -2));
    }
    int j = ACRA.getConfig().resDialogEmailPrompt();
    if (j != 0)
    {
      TextView localTextView3 = new TextView(this);
      localTextView3.setText(getText(j));
      localTextView3.setPadding(localTextView3.getPaddingLeft(), 10, localTextView3.getPaddingRight(), localTextView3.getPaddingBottom());
      localLinearLayout1.addView(localTextView3, new LinearLayout.LayoutParams(-1, -2));
      this.userEmail = new EditText(this);
      this.userEmail.setSingleLine();
      this.userEmail.setInputType(33);
      this.prefs = getSharedPreferences(ACRA.getConfig().sharedPreferencesName(), ACRA.getConfig().sharedPreferencesMode());
      this.userEmail.setText(this.prefs.getString("acra.user.email", ""));
      localLinearLayout1.addView(this.userEmail, new LinearLayout.LayoutParams(-1, -2));
    }
    LinearLayout localLinearLayout2 = new LinearLayout(this);
    localLinearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    localLinearLayout2.setPadding(localLinearLayout2.getPaddingLeft(), 10, localLinearLayout2.getPaddingRight(), localLinearLayout2.getPaddingBottom());
    Button localButton1 = new Button(this);
    localButton1.setText(17039379);
    localButton1.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        String str1;
        if (CrashReportDialog.this.userComment != null)
          str1 = CrashReportDialog.this.userComment.getText().toString();
        while (true)
        {
          String str2;
          CrashReportPersister localCrashReportPersister;
          if ((CrashReportDialog.this.prefs != null) && (CrashReportDialog.this.userEmail != null))
          {
            str2 = CrashReportDialog.this.userEmail.getText().toString();
            SharedPreferences.Editor localEditor = CrashReportDialog.this.prefs.edit();
            localEditor.putString("acra.user.email", str2);
            localEditor.commit();
            localCrashReportPersister = new CrashReportPersister(CrashReportDialog.this.getApplicationContext());
          }
          try
          {
            Log.d(ACRA.LOG_TAG, "Add user comment to " + CrashReportDialog.this.mReportFileName);
            CrashReportData localCrashReportData = localCrashReportPersister.load(CrashReportDialog.this.mReportFileName);
            localCrashReportData.put(ReportField.USER_COMMENT, str1);
            localCrashReportData.put(ReportField.USER_EMAIL, str2);
            localCrashReportPersister.store(localCrashReportData, CrashReportDialog.this.mReportFileName);
            Log.v(ACRA.LOG_TAG, "About to start SenderWorker from CrashReportDialog");
            ACRA.getErrorReporter().startSendingReports(false, true);
            int i = ACRA.getConfig().resDialogOkToast();
            if (i != 0)
              ToastSender.sendToast(CrashReportDialog.this.getApplicationContext(), i, 1);
            CrashReportDialog.this.finish();
            return;
            str1 = "";
            continue;
            str2 = "";
          }
          catch (IOException localIOException)
          {
            while (true)
              Log.w(ACRA.LOG_TAG, "User comment not added: ", localIOException);
          }
        }
      }
    });
    localLinearLayout2.addView(localButton1, new LinearLayout.LayoutParams(-1, -2, 1.0F));
    Button localButton2 = new Button(this);
    localButton2.setText(17039369);
    localButton2.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ACRA.getErrorReporter().deletePendingNonApprovedReports(false);
        CrashReportDialog.this.finish();
      }
    });
    localLinearLayout2.addView(localButton2, new LinearLayout.LayoutParams(-1, -2, 1.0F));
    localLinearLayout1.addView(localLinearLayout2, new LinearLayout.LayoutParams(-1, -2));
    setContentView(localLinearLayout1);
    int k = ACRA.getConfig().resDialogTitle();
    if (k != 0)
      setTitle(k);
    getWindow().setFeatureDrawableResource(3, ACRA.getConfig().resDialogIcon());
    cancelNotification();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.CrashReportDialog
 * JD-Core Version:    0.6.2
 */