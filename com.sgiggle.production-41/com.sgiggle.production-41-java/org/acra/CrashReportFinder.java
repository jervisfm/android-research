package org.acra;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FilenameFilter;

final class CrashReportFinder
{
  private final Context context;

  public CrashReportFinder(Context paramContext)
  {
    this.context = paramContext;
  }

  public String[] getCrashReportFiles()
  {
    if (this.context == null)
    {
      Log.e(ACRA.LOG_TAG, "Trying to get ACRA reports but ACRA is not initialized.");
      return new String[0];
    }
    File localFile = this.context.getFilesDir();
    if (localFile == null)
    {
      Log.w(ACRA.LOG_TAG, "Application files directory does not exist! The application may not be installed correctly. Please try reinstalling.");
      return new String[0];
    }
    Log.d(ACRA.LOG_TAG, "Looking for error files in " + localFile.getAbsolutePath());
    String[] arrayOfString = localFile.list(new FilenameFilter()
    {
      public boolean accept(File paramAnonymousFile, String paramAnonymousString)
      {
        return paramAnonymousString.endsWith(".stacktrace");
      }
    });
    if (arrayOfString == null)
      return new String[0];
    return arrayOfString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.CrashReportFinder
 * JD-Core Version:    0.6.2
 */