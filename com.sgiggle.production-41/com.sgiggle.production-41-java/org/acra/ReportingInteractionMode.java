package org.acra;

public enum ReportingInteractionMode
{
  static
  {
    NOTIFICATION = new ReportingInteractionMode("NOTIFICATION", 1);
    TOAST = new ReportingInteractionMode("TOAST", 2);
    DIALOG = new ReportingInteractionMode("DIALOG", 3);
    ReportingInteractionMode[] arrayOfReportingInteractionMode = new ReportingInteractionMode[4];
    arrayOfReportingInteractionMode[0] = SILENT;
    arrayOfReportingInteractionMode[1] = NOTIFICATION;
    arrayOfReportingInteractionMode[2] = TOAST;
    arrayOfReportingInteractionMode[3] = DIALOG;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.ReportingInteractionMode
 * JD-Core Version:    0.6.2
 */