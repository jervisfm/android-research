package org.acra.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.TYPE})
public @interface ReportsCrashes
{
  public abstract String[] additionalDropBoxTags();

  public abstract String[] additionalSharedPreferences();

  public abstract String applicationLogFile();

  public abstract int applicationLogFileLines();

  public abstract int connectionTimeout();

  public abstract ReportField[] customReportContent();

  public abstract boolean deleteOldUnsentReportsOnApplicationStart();

  public abstract boolean deleteUnapprovedReportsOnApplicationStart();

  public abstract int dropboxCollectionMinutes();

  public abstract String[] excludeMatchingSharedPreferencesKeys();

  public abstract boolean forceCloseDialogAfterToast();

  public abstract String formKey();

  public abstract String formUri();

  public abstract String formUriBasicAuthLogin();

  public abstract String formUriBasicAuthPassword();

  public abstract boolean includeDropBoxSystemTags();

  public abstract String[] logcatArguments();

  public abstract boolean logcatFilterByPid();

  public abstract String mailTo();

  public abstract int maxNumberOfRequestRetries();

  public abstract ReportingInteractionMode mode();

  public abstract int resDialogCommentPrompt();

  public abstract int resDialogEmailPrompt();

  public abstract int resDialogIcon();

  public abstract int resDialogOkToast();

  public abstract int resDialogText();

  public abstract int resDialogTitle();

  public abstract int resNotifIcon();

  public abstract int resNotifText();

  public abstract int resNotifTickerText();

  public abstract int resNotifTitle();

  public abstract int resToastText();

  public abstract boolean sendReportsInDevMode();

  public abstract int sharedPreferencesMode();

  public abstract String sharedPreferencesName();

  public abstract int socketTimeout();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.annotation.ReportsCrashes
 * JD-Core Version:    0.6.2
 */