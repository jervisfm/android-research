package org.acra.collector;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.acra.ACRA;

final class DumpSysCollector
{
  public static String collectMemInfo()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add("dumpsys");
      localArrayList.add("meminfo");
      localArrayList.add(Integer.toString(android.os.Process.myPid()));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[])localArrayList.toArray(new String[localArrayList.size()])).getInputStream()), 8192);
      while (true)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
          return localStringBuilder.toString();
        localStringBuilder.append(str);
        localStringBuilder.append("\n");
      }
    }
    catch (IOException localIOException)
    {
      while (true)
        Log.e(ACRA.LOG_TAG, "DumpSysCollector.meminfo could not retrieve data", localIOException);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.collector.DumpSysCollector
 * JD-Core Version:    0.6.2
 */