package org.acra.collector;

import android.util.SparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class MediaCodecListCollector
{
  private static final String[] AAC_TYPES;
  private static final String[] AVC_TYPES;
  private static final String COLOR_FORMAT_PREFIX = "COLOR_";
  private static final String[] H263_TYPES;
  private static final String[] MPEG4_TYPES = { "mp4", "mpeg4", "MP4", "MPEG4" };
  private static Class<?> codecCapabilitiesClass;
  private static Field colorFormatsField;
  private static Method getCapabilitiesForTypeMethod;
  private static Method getCodecInfoAtMethod;
  private static Method getNameMethod;
  private static Method getSupportedTypesMethod;
  private static Method isEncoderMethod;
  private static Field levelField;
  private static SparseArray<String> mAACProfileValues;
  private static SparseArray<String> mAVCLevelValues;
  private static SparseArray<String> mAVCProfileValues;
  private static SparseArray<String> mColorFormatValues;
  private static SparseArray<String> mH263LevelValues;
  private static SparseArray<String> mH263ProfileValues;
  private static SparseArray<String> mMPEG4LevelValues;
  private static SparseArray<String> mMPEG4ProfileValues;
  private static Class<?> mediaCodecInfoClass;
  private static Class<?> mediaCodecListClass;
  private static Field profileField;
  private static Field profileLevelsField;

  static
  {
    AVC_TYPES = new String[] { "avc", "h264", "AVC", "H264" };
    H263_TYPES = new String[] { "h263", "H263" };
    AAC_TYPES = new String[] { "aac", "AAC" };
    mediaCodecListClass = null;
    getCodecInfoAtMethod = null;
    mediaCodecInfoClass = null;
    getNameMethod = null;
    isEncoderMethod = null;
    getSupportedTypesMethod = null;
    getCapabilitiesForTypeMethod = null;
    codecCapabilitiesClass = null;
    colorFormatsField = null;
    profileLevelsField = null;
    profileField = null;
    levelField = null;
    mColorFormatValues = new SparseArray();
    mAVCLevelValues = new SparseArray();
    mAVCProfileValues = new SparseArray();
    mH263LevelValues = new SparseArray();
    mH263ProfileValues = new SparseArray();
    mMPEG4LevelValues = new SparseArray();
    mMPEG4ProfileValues = new SparseArray();
    mAACProfileValues = new SparseArray();
    try
    {
      mediaCodecListClass = Class.forName("android.media.MediaCodecList");
      Class localClass1 = mediaCodecListClass;
      Class[] arrayOfClass = new Class[1];
      arrayOfClass[0] = Integer.TYPE;
      getCodecInfoAtMethod = localClass1.getMethod("getCodecInfoAt", arrayOfClass);
      mediaCodecInfoClass = Class.forName("android.media.MediaCodecInfo");
      getNameMethod = mediaCodecInfoClass.getMethod("getName", new Class[0]);
      isEncoderMethod = mediaCodecInfoClass.getMethod("isEncoder", new Class[0]);
      getSupportedTypesMethod = mediaCodecInfoClass.getMethod("getSupportedTypes", new Class[0]);
      getCapabilitiesForTypeMethod = mediaCodecInfoClass.getMethod("getCapabilitiesForType", new Class[] { String.class });
      codecCapabilitiesClass = Class.forName("android.media.MediaCodecInfo$CodecCapabilities");
      colorFormatsField = codecCapabilitiesClass.getField("colorFormats");
      profileLevelsField = codecCapabilitiesClass.getField("profileLevels");
      Field[] arrayOfField1 = codecCapabilitiesClass.getFields();
      int i = arrayOfField1.length;
      int j = 0;
      Class localClass2;
      Field[] arrayOfField2;
      int k;
      if (j < i)
      {
        Field localField1 = arrayOfField1[j];
        if ((Modifier.isStatic(localField1.getModifiers())) && (Modifier.isFinal(localField1.getModifiers())) && (localField1.getName().startsWith("COLOR_")))
          mColorFormatValues.put(localField1.getInt(null), localField1.getName());
      }
      else
      {
        localClass2 = Class.forName("android.media.MediaCodecInfo$CodecProfileLevel");
        arrayOfField2 = localClass2.getFields();
        k = arrayOfField2.length;
      }
      for (int m = 0; ; m++)
        if (m < k)
        {
          Field localField2 = arrayOfField2[m];
          if ((Modifier.isStatic(localField2.getModifiers())) && (Modifier.isFinal(localField2.getModifiers())))
            if (localField2.getName().startsWith("AVCLevel"))
              mAVCLevelValues.put(localField2.getInt(null), localField2.getName());
            else if (localField2.getName().startsWith("AVCProfile"))
              mAVCProfileValues.put(localField2.getInt(null), localField2.getName());
            else if (localField2.getName().startsWith("H263Level"))
              mH263LevelValues.put(localField2.getInt(null), localField2.getName());
            else if (localField2.getName().startsWith("H263Profile"))
              mH263ProfileValues.put(localField2.getInt(null), localField2.getName());
            else if (localField2.getName().startsWith("MPEG4Level"))
              mMPEG4LevelValues.put(localField2.getInt(null), localField2.getName());
            else if (localField2.getName().startsWith("MPEG4Profile"))
              mMPEG4ProfileValues.put(localField2.getInt(null), localField2.getName());
            else if (localField2.getName().startsWith("AAC"))
              mAACProfileValues.put(localField2.getInt(null), localField2.getName());
        }
        else
        {
          profileField = localClass2.getField("profile");
          levelField = localClass2.getField("level");
          return;
          j++;
          break;
        }
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
    }
    catch (SecurityException localSecurityException)
    {
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
    }
  }

  public static String collecMediaCodecList()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((mediaCodecListClass != null) && (mediaCodecInfoClass != null));
    try
    {
      int i = ((Integer)mediaCodecListClass.getMethod("getCodecCount", new Class[0]).invoke(null, new Object[0])).intValue();
      for (int j = 0; j < i; j++)
      {
        localStringBuilder.append("\n");
        Method localMethod = getCodecInfoAtMethod;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(j);
        Object localObject = localMethod.invoke(null, arrayOfObject);
        localStringBuilder.append(j).append(": ").append(getNameMethod.invoke(localObject, new Object[0])).append("\n");
        localStringBuilder.append("isEncoder: ").append(isEncoderMethod.invoke(localObject, new Object[0])).append("\n");
        String[] arrayOfString = (String[])getSupportedTypesMethod.invoke(localObject, new Object[0]);
        localStringBuilder.append("Supported types: ").append(Arrays.toString(arrayOfString)).append("\n");
        int k = arrayOfString.length;
        for (int m = 0; m < k; m++)
          localStringBuilder.append(collectCapabilitiesForType(localObject, arrayOfString[m]));
        localStringBuilder.append("\n");
      }
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      return localStringBuilder.toString();
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      break label250;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      label250: break label250;
    }
  }

  private static String collectCapabilitiesForType(Object paramObject, String paramString)
    throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Object localObject = getCapabilitiesForTypeMethod.invoke(paramObject, new Object[] { paramString });
    int[] arrayOfInt = (int[])colorFormatsField.get(localObject);
    if (arrayOfInt.length > 0)
    {
      localStringBuilder.append(paramString).append(" color formats:");
      for (int m = 0; m < arrayOfInt.length; m++)
      {
        localStringBuilder.append((String)mColorFormatValues.get(arrayOfInt[m]));
        if (m < arrayOfInt.length - 1)
          localStringBuilder.append(',');
      }
      localStringBuilder.append("\n");
    }
    Object[] arrayOfObject = (Object[])profileLevelsField.get(localObject);
    if (arrayOfObject.length > 0)
    {
      localStringBuilder.append(paramString).append(" profile levels:");
      int i = 0;
      if (i < arrayOfObject.length)
      {
        CodecType localCodecType = identifyCodecType(paramObject);
        int j = profileField.getInt(arrayOfObject[i]);
        int k = levelField.getInt(arrayOfObject[i]);
        if (localCodecType == null)
          localStringBuilder.append(j).append('-').append(k);
        switch (1.$SwitchMap$org$acra$collector$MediaCodecListCollector$CodecType[localCodecType.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
          if (i < arrayOfObject.length - 1)
            localStringBuilder.append(',');
          i++;
          break;
          localStringBuilder.append(j).append((String)mAVCProfileValues.get(j)).append('-').append((String)mAVCLevelValues.get(k));
          continue;
          localStringBuilder.append((String)mH263ProfileValues.get(j)).append('-').append((String)mH263LevelValues.get(k));
          continue;
          localStringBuilder.append((String)mMPEG4ProfileValues.get(j)).append('-').append((String)mMPEG4LevelValues.get(k));
          continue;
          localStringBuilder.append((String)mAACProfileValues.get(j));
        }
      }
      localStringBuilder.append("\n");
    }
    return "\n";
  }

  private static CodecType identifyCodecType(Object paramObject)
    throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
  {
    String str = (String)getNameMethod.invoke(paramObject, new Object[0]);
    String[] arrayOfString1 = AVC_TYPES;
    int i = arrayOfString1.length;
    for (int j = 0; j < i; j++)
      if (str.contains(arrayOfString1[j]))
        return CodecType.AVC;
    String[] arrayOfString2 = H263_TYPES;
    int k = arrayOfString2.length;
    for (int m = 0; m < k; m++)
      if (str.contains(arrayOfString2[m]))
        return CodecType.H263;
    String[] arrayOfString3 = MPEG4_TYPES;
    int n = arrayOfString3.length;
    for (int i1 = 0; i1 < n; i1++)
      if (str.contains(arrayOfString3[i1]))
        return CodecType.MPEG4;
    String[] arrayOfString4 = AAC_TYPES;
    int i2 = arrayOfString4.length;
    for (int i3 = 0; i3 < i2; i3++)
      if (str.contains(arrayOfString4[i3]))
        return CodecType.AAC;
    return null;
  }

  private static enum CodecType
  {
    static
    {
      AAC = new CodecType("AAC", 3);
      CodecType[] arrayOfCodecType = new CodecType[4];
      arrayOfCodecType[0] = AVC;
      arrayOfCodecType[1] = H263;
      arrayOfCodecType[2] = MPEG4;
      arrayOfCodecType[3] = AAC;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.collector.MediaCodecListCollector
 * JD-Core Version:    0.6.2
 */