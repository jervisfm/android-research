package org.acra.sender;

import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.acra.ACRA;
import org.acra.ACRAConfiguration;
import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.util.HttpRequest;

public class HttpPostSender
  implements ReportSender
{
  private final Uri mFormUri;
  private final Map<ReportField, String> mMapping;

  public HttpPostSender(String paramString, Map<ReportField, String> paramMap)
  {
    this.mFormUri = Uri.parse(paramString);
    this.mMapping = paramMap;
  }

  private static boolean isNull(String paramString)
  {
    return (paramString == null) || ("ACRA-NULL-STRING".equals(paramString));
  }

  private Map<String, String> remap(Map<ReportField, String> paramMap)
  {
    ReportField[] arrayOfReportField1 = ACRA.getConfig().customReportContent();
    if (arrayOfReportField1.length == 0)
      arrayOfReportField1 = ACRA.DEFAULT_REPORT_FIELDS;
    HashMap localHashMap = new HashMap(paramMap.size());
    ReportField[] arrayOfReportField2 = arrayOfReportField1;
    int i = arrayOfReportField2.length;
    int j = 0;
    if (j < i)
    {
      ReportField localReportField = arrayOfReportField2[j];
      if ((this.mMapping == null) || (this.mMapping.get(localReportField) == null))
        localHashMap.put(localReportField.toString(), paramMap.get(localReportField));
      while (true)
      {
        j++;
        break;
        localHashMap.put(this.mMapping.get(localReportField), paramMap.get(localReportField));
      }
    }
    return localHashMap;
  }

  public void send(CrashReportData paramCrashReportData)
    throws ReportSenderException
  {
    try
    {
      Map localMap = remap(paramCrashReportData);
      URL localURL = new URL(this.mFormUri.toString());
      Log.d(ACRA.LOG_TAG, "Connect to " + localURL.toString());
      String str1;
      if (isNull(ACRA.getConfig().formUriBasicAuthLogin()))
      {
        str1 = null;
        if (!isNull(ACRA.getConfig().formUriBasicAuthPassword()))
          break label158;
      }
      label158: String str2;
      for (Object localObject = null; ; localObject = str2)
      {
        HttpRequest localHttpRequest = new HttpRequest();
        localHttpRequest.setConnectionTimeOut(ACRA.getConfig().connectionTimeout());
        localHttpRequest.setSocketTimeOut(ACRA.getConfig().socketTimeout());
        localHttpRequest.setMaxNrRetries(ACRA.getConfig().maxNumberOfRequestRetries());
        localHttpRequest.setLogin(str1);
        localHttpRequest.setPassword((String)localObject);
        localHttpRequest.sendPost(localURL, localMap);
        return;
        str1 = ACRA.getConfig().formUriBasicAuthLogin();
        break;
        str2 = ACRA.getConfig().formUriBasicAuthPassword();
      }
    }
    catch (IOException localIOException)
    {
      throw new ReportSenderException("Error while sending report to Http Post Form.", localIOException);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.sender.HttpPostSender
 * JD-Core Version:    0.6.2
 */