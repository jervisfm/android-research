package org.acra.sender;

import org.acra.collector.CrashReportData;

public abstract interface ReportSender
{
  public abstract void send(CrashReportData paramCrashReportData)
    throws ReportSenderException;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.sender.ReportSender
 * JD-Core Version:    0.6.2
 */