package org.acra.sender;

public class ReportSenderException extends Exception
{
  public ReportSenderException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.sender.ReportSenderException
 * JD-Core Version:    0.6.2
 */