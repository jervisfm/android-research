package org.acra.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import org.acra.ACRA;

public final class PackageManagerWrapper
{
  private final Context context;

  public PackageManagerWrapper(Context paramContext)
  {
    this.context = paramContext;
  }

  public PackageInfo getPackageInfo()
  {
    PackageManager localPackageManager = this.context.getPackageManager();
    if (localPackageManager == null)
      return null;
    try
    {
      PackageInfo localPackageInfo = localPackageManager.getPackageInfo(this.context.getPackageName(), 0);
      return localPackageInfo;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      Log.v(ACRA.LOG_TAG, "Failed to find PackageInfo for current App : " + this.context.getPackageName());
      return null;
    }
    catch (RuntimeException localRuntimeException)
    {
    }
    return null;
  }

  public boolean hasPermission(String paramString)
  {
    PackageManager localPackageManager = this.context.getPackageManager();
    if (localPackageManager == null)
      return false;
    try
    {
      int i = localPackageManager.checkPermission(paramString, this.context.getPackageName());
      return i == 0;
    }
    catch (RuntimeException localRuntimeException)
    {
    }
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.acra.util.PackageManagerWrapper
 * JD-Core Version:    0.6.2
 */