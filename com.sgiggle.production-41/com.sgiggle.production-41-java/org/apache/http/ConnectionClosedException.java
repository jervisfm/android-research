package org.apache.http;

import java.io.IOException;

public class ConnectionClosedException extends IOException
{
  private static final long serialVersionUID = 617550366255636674L;

  public ConnectionClosedException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.ConnectionClosedException
 * JD-Core Version:    0.6.2
 */