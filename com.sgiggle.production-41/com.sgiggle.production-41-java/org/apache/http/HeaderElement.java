package org.apache.http;

public abstract interface HeaderElement
{
  public abstract String getName();

  public abstract NameValuePair getParameter(int paramInt);

  public abstract NameValuePair getParameterByName(String paramString);

  public abstract int getParameterCount();

  public abstract NameValuePair[] getParameters();

  public abstract String getValue();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.HeaderElement
 * JD-Core Version:    0.6.2
 */