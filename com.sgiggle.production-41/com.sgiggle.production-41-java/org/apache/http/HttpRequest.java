package org.apache.http;

public abstract interface HttpRequest extends HttpMessage
{
  public abstract RequestLine getRequestLine();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.HttpRequest
 * JD-Core Version:    0.6.2
 */