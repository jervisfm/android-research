package org.apache.http;

import java.io.IOException;

public class MalformedChunkCodingException extends IOException
{
  private static final long serialVersionUID = 2158560246948994524L;

  public MalformedChunkCodingException()
  {
  }

  public MalformedChunkCodingException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.MalformedChunkCodingException
 * JD-Core Version:    0.6.2
 */