package org.apache.http;

public class MethodNotSupportedException extends HttpException
{
  private static final long serialVersionUID = 3365359036840171201L;

  public MethodNotSupportedException(String paramString)
  {
    super(paramString);
  }

  public MethodNotSupportedException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.MethodNotSupportedException
 * JD-Core Version:    0.6.2
 */