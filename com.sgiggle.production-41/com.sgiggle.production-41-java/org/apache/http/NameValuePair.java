package org.apache.http;

public abstract interface NameValuePair
{
  public abstract String getName();

  public abstract String getValue();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.NameValuePair
 * JD-Core Version:    0.6.2
 */