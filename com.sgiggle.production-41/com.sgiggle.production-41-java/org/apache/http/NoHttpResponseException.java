package org.apache.http;

import java.io.IOException;

public class NoHttpResponseException extends IOException
{
  private static final long serialVersionUID = -7658940387386078766L;

  public NoHttpResponseException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.NoHttpResponseException
 * JD-Core Version:    0.6.2
 */