package org.apache.http;

public class ParseException extends RuntimeException
{
  private static final long serialVersionUID = -7288819855864183578L;

  public ParseException()
  {
  }

  public ParseException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.ParseException
 * JD-Core Version:    0.6.2
 */