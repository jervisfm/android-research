package org.apache.http;

import java.util.Locale;

public abstract interface ReasonPhraseCatalog
{
  public abstract String getReason(int paramInt, Locale paramLocale);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.ReasonPhraseCatalog
 * JD-Core Version:    0.6.2
 */