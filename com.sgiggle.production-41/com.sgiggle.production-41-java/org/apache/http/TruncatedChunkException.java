package org.apache.http;

public class TruncatedChunkException extends MalformedChunkCodingException
{
  private static final long serialVersionUID = -23506263930279460L;

  public TruncatedChunkException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.TruncatedChunkException
 * JD-Core Version:    0.6.2
 */