package org.apache.http;

public class UnsupportedHttpVersionException extends ProtocolException
{
  private static final long serialVersionUID = -1348448090193107031L;

  public UnsupportedHttpVersionException()
  {
  }

  public UnsupportedHttpVersionException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.UnsupportedHttpVersionException
 * JD-Core Version:    0.6.2
 */