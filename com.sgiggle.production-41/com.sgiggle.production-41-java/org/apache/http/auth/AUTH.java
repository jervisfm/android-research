package org.apache.http.auth;

import org.apache.http.annotation.Immutable;

@Immutable
public final class AUTH
{
  public static final String PROXY_AUTH = "Proxy-Authenticate";
  public static final String PROXY_AUTH_RESP = "Proxy-Authorization";
  public static final String WWW_AUTH = "WWW-Authenticate";
  public static final String WWW_AUTH_RESP = "Authorization";
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.auth.AUTH
 * JD-Core Version:    0.6.2
 */