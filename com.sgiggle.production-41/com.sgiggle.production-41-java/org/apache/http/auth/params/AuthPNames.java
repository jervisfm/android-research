package org.apache.http.auth.params;

public abstract interface AuthPNames
{
  public static final String CREDENTIAL_CHARSET = "http.auth.credential-charset";
  public static final String PROXY_AUTH_PREF = "http.auth.proxy-scheme-pref";
  public static final String TARGET_AUTH_PREF = "http.auth.target-scheme-pref";
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.auth.params.AuthPNames
 * JD-Core Version:    0.6.2
 */