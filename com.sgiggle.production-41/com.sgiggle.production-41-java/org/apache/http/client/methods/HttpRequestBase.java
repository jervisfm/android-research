package org.apache.http.client.methods;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.http.ProtocolVersion;
import org.apache.http.RequestLine;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.client.utils.CloneUtils;
import org.apache.http.conn.ClientConnectionRequest;
import org.apache.http.conn.ConnectionReleaseTrigger;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.message.BasicRequestLine;
import org.apache.http.message.HeaderGroup;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

@NotThreadSafe
public abstract class HttpRequestBase extends AbstractHttpMessage
  implements HttpUriRequest, AbortableHttpRequest, Cloneable
{
  private Lock abortLock = new ReentrantLock();
  private boolean aborted;
  private ClientConnectionRequest connRequest;
  private ConnectionReleaseTrigger releaseTrigger;
  private URI uri;

  public void abort()
  {
    this.abortLock.lock();
    try
    {
      boolean bool = this.aborted;
      if (bool);
      ConnectionReleaseTrigger localConnectionReleaseTrigger;
      do
      {
        return;
        this.aborted = true;
        ClientConnectionRequest localClientConnectionRequest = this.connRequest;
        localConnectionReleaseTrigger = this.releaseTrigger;
        this.abortLock.unlock();
        if (localClientConnectionRequest != null)
          localClientConnectionRequest.abortRequest();
      }
      while (localConnectionReleaseTrigger == null);
      try
      {
        localConnectionReleaseTrigger.abortConnection();
        return;
      }
      catch (IOException localIOException)
      {
      }
    }
    finally
    {
      this.abortLock.unlock();
    }
  }

  public Object clone()
    throws CloneNotSupportedException
  {
    HttpRequestBase localHttpRequestBase = (HttpRequestBase)super.clone();
    localHttpRequestBase.abortLock = new ReentrantLock();
    localHttpRequestBase.aborted = false;
    localHttpRequestBase.releaseTrigger = null;
    localHttpRequestBase.connRequest = null;
    localHttpRequestBase.headergroup = ((HeaderGroup)CloneUtils.clone(this.headergroup));
    localHttpRequestBase.params = ((HttpParams)CloneUtils.clone(this.params));
    return localHttpRequestBase;
  }

  public abstract String getMethod();

  public ProtocolVersion getProtocolVersion()
  {
    return HttpProtocolParams.getVersion(getParams());
  }

  public RequestLine getRequestLine()
  {
    String str1 = getMethod();
    ProtocolVersion localProtocolVersion = getProtocolVersion();
    URI localURI = getURI();
    String str2 = null;
    if (localURI != null)
      str2 = localURI.toASCIIString();
    if ((str2 == null) || (str2.length() == 0))
      str2 = "/";
    return new BasicRequestLine(str1, str2, localProtocolVersion);
  }

  public URI getURI()
  {
    return this.uri;
  }

  public boolean isAborted()
  {
    return this.aborted;
  }

  public void setConnectionRequest(ClientConnectionRequest paramClientConnectionRequest)
    throws IOException
  {
    this.abortLock.lock();
    try
    {
      if (this.aborted)
        throw new IOException("Request already aborted");
    }
    finally
    {
      this.abortLock.unlock();
    }
    this.releaseTrigger = null;
    this.connRequest = paramClientConnectionRequest;
    this.abortLock.unlock();
  }

  public void setReleaseTrigger(ConnectionReleaseTrigger paramConnectionReleaseTrigger)
    throws IOException
  {
    this.abortLock.lock();
    try
    {
      if (this.aborted)
        throw new IOException("Request already aborted");
    }
    finally
    {
      this.abortLock.unlock();
    }
    this.connRequest = null;
    this.releaseTrigger = paramConnectionReleaseTrigger;
    this.abortLock.unlock();
  }

  public void setURI(URI paramURI)
  {
    this.uri = paramURI;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.client.methods.HttpRequestBase
 * JD-Core Version:    0.6.2
 */