package org.apache.http.client.methods;

import java.net.URI;
import org.apache.http.HttpRequest;

public abstract interface HttpUriRequest extends HttpRequest
{
  public abstract void abort()
    throws UnsupportedOperationException;

  public abstract String getMethod();

  public abstract URI getURI();

  public abstract boolean isAborted();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.client.methods.HttpUriRequest
 * JD-Core Version:    0.6.2
 */