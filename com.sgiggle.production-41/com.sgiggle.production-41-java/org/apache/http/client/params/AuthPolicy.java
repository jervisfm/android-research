package org.apache.http.client.params;

import org.apache.http.annotation.Immutable;

@Immutable
public final class AuthPolicy
{
  public static final String BASIC = "Basic";
  public static final String DIGEST = "Digest";
  public static final String NTLM = "NTLM";
  public static final String SPNEGO = "negotiate";
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.client.params.AuthPolicy
 * JD-Core Version:    0.6.2
 */