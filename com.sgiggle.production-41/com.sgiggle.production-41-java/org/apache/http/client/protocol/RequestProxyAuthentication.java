package org.apache.http.client.protocol;

import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.annotation.Immutable;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.ContextAwareAuthScheme;
import org.apache.http.auth.Credentials;
import org.apache.http.conn.HttpRoutedConnection;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.protocol.HttpContext;

@Immutable
public class RequestProxyAuthentication
  implements HttpRequestInterceptor
{
  private final Log log = LogFactory.getLog(getClass());

  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    if (paramHttpRequest == null)
      throw new IllegalArgumentException("HTTP request may not be null");
    if (paramHttpContext == null)
      throw new IllegalArgumentException("HTTP context may not be null");
    if (paramHttpRequest.containsHeader("Proxy-Authorization"));
    AuthState localAuthState;
    AuthScheme localAuthScheme;
    Credentials localCredentials;
    do
    {
      do
      {
        HttpRoutedConnection localHttpRoutedConnection;
        do
        {
          return;
          localHttpRoutedConnection = (HttpRoutedConnection)paramHttpContext.getAttribute("http.connection");
          if (localHttpRoutedConnection == null)
          {
            this.log.debug("HTTP connection not set in the context");
            return;
          }
        }
        while (localHttpRoutedConnection.getRoute().isTunnelled());
        localAuthState = (AuthState)paramHttpContext.getAttribute("http.auth.proxy-scope");
        if (localAuthState == null)
        {
          this.log.debug("Proxy auth state not set in the context");
          return;
        }
        localAuthScheme = localAuthState.getAuthScheme();
      }
      while (localAuthScheme == null);
      localCredentials = localAuthState.getCredentials();
      if (localCredentials == null)
      {
        this.log.debug("User credentials not available");
        return;
      }
    }
    while ((localAuthState.getAuthScope() == null) && (localAuthScheme.isConnectionBased()));
    while (true)
    {
      try
      {
        if (!(localAuthScheme instanceof ContextAwareAuthScheme))
          break label244;
        localObject = ((ContextAwareAuthScheme)localAuthScheme).authenticate(localCredentials, paramHttpRequest, paramHttpContext);
        paramHttpRequest.addHeader((Header)localObject);
        return;
      }
      catch (AuthenticationException localAuthenticationException)
      {
      }
      if (!this.log.isErrorEnabled())
        break;
      this.log.error("Proxy authentication error: " + localAuthenticationException.getMessage());
      return;
      label244: Header localHeader = localAuthScheme.authenticate(localCredentials, paramHttpRequest);
      Object localObject = localHeader;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.client.protocol.RequestProxyAuthentication
 * JD-Core Version:    0.6.2
 */