package org.apache.http.client.protocol;

import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.annotation.Immutable;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthState;
import org.apache.http.client.AuthCache;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.protocol.HttpContext;

@Immutable
public class ResponseAuthCache
  implements HttpResponseInterceptor
{
  private final Log log = LogFactory.getLog(getClass());

  private void cache(AuthCache paramAuthCache, HttpHost paramHttpHost, AuthState paramAuthState)
  {
    AuthScheme localAuthScheme = paramAuthState.getAuthScheme();
    if (paramAuthState.getAuthScope() != null)
    {
      if (paramAuthState.getCredentials() != null)
      {
        if (this.log.isDebugEnabled())
          this.log.debug("Caching '" + localAuthScheme.getSchemeName() + "' auth scheme for " + paramHttpHost);
        paramAuthCache.put(paramHttpHost, localAuthScheme);
      }
    }
    else
      return;
    paramAuthCache.remove(paramHttpHost);
  }

  private boolean isCachable(AuthState paramAuthState)
  {
    AuthScheme localAuthScheme = paramAuthState.getAuthScheme();
    if ((localAuthScheme == null) || (!localAuthScheme.isComplete()))
      return false;
    String str = localAuthScheme.getSchemeName();
    return (str.equalsIgnoreCase("Basic")) || (str.equalsIgnoreCase("Digest"));
  }

  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    if (paramHttpResponse == null)
      throw new IllegalArgumentException("HTTP request may not be null");
    if (paramHttpContext == null)
      throw new IllegalArgumentException("HTTP context may not be null");
    Object localObject = (AuthCache)paramHttpContext.getAttribute("http.auth.auth-cache");
    HttpHost localHttpHost1 = (HttpHost)paramHttpContext.getAttribute("http.target_host");
    AuthState localAuthState1 = (AuthState)paramHttpContext.getAttribute("http.auth.target-scope");
    if ((localHttpHost1 != null) && (localAuthState1 != null) && (isCachable(localAuthState1)))
    {
      if (localObject == null)
      {
        localObject = new BasicAuthCache();
        paramHttpContext.setAttribute("http.auth.auth-cache", localObject);
      }
      cache((AuthCache)localObject, localHttpHost1, localAuthState1);
    }
    HttpHost localHttpHost2 = (HttpHost)paramHttpContext.getAttribute("http.proxy_host");
    AuthState localAuthState2 = (AuthState)paramHttpContext.getAttribute("http.auth.proxy-scope");
    if ((localHttpHost2 != null) && (localAuthState2 != null) && (isCachable(localAuthState2)))
    {
      if (localObject == null)
      {
        localObject = new BasicAuthCache();
        paramHttpContext.setAttribute("http.auth.auth-cache", localObject);
      }
      cache((AuthCache)localObject, localHttpHost2, localAuthState2);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.client.protocol.ResponseAuthCache
 * JD-Core Version:    0.6.2
 */