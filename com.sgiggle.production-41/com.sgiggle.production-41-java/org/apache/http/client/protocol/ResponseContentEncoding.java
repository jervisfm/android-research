package org.apache.http.client.protocol;

import java.io.IOException;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.annotation.Immutable;
import org.apache.http.client.entity.DeflateDecompressingEntity;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.protocol.HttpContext;

@Immutable
public class ResponseContentEncoding
  implements HttpResponseInterceptor
{
  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpEntity localHttpEntity = paramHttpResponse.getEntity();
    HeaderElement localHeaderElement;
    String str;
    if (localHttpEntity != null)
    {
      Header localHeader = localHttpEntity.getContentEncoding();
      if (localHeader != null)
      {
        HeaderElement[] arrayOfHeaderElement = localHeader.getElements();
        if (arrayOfHeaderElement.length < 0)
        {
          localHeaderElement = arrayOfHeaderElement[0];
          str = localHeaderElement.getName().toLowerCase(Locale.US);
          if ((!"gzip".equals(str)) && (!"x-gzip".equals(str)))
            break label100;
          paramHttpResponse.setEntity(new GzipDecompressingEntity(paramHttpResponse.getEntity()));
        }
      }
    }
    label100: 
    do
    {
      return;
      if ("deflate".equals(str))
      {
        paramHttpResponse.setEntity(new DeflateDecompressingEntity(paramHttpResponse.getEntity()));
        return;
      }
    }
    while ("identity".equals(str));
    throw new HttpException("Unsupported Content-Coding: " + localHeaderElement.getName());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.client.protocol.ResponseContentEncoding
 * JD-Core Version:    0.6.2
 */