package org.apache.http.client.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Stack;
import org.apache.http.HttpHost;
import org.apache.http.annotation.Immutable;

@Immutable
public class URIUtils
{
  public static URI createURI(String paramString1, String paramString2, int paramInt, String paramString3, String paramString4, String paramString5)
    throws URISyntaxException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramString2 != null)
    {
      if (paramString1 != null)
      {
        localStringBuilder.append(paramString1);
        localStringBuilder.append("://");
      }
      localStringBuilder.append(paramString2);
      if (paramInt > 0)
      {
        localStringBuilder.append(':');
        localStringBuilder.append(paramInt);
      }
    }
    if ((paramString3 == null) || (!paramString3.startsWith("/")))
      localStringBuilder.append('/');
    if (paramString3 != null)
      localStringBuilder.append(paramString3);
    if (paramString4 != null)
    {
      localStringBuilder.append('?');
      localStringBuilder.append(paramString4);
    }
    if (paramString5 != null)
    {
      localStringBuilder.append('#');
      localStringBuilder.append(paramString5);
    }
    return new URI(localStringBuilder.toString());
  }

  public static HttpHost extractHost(URI paramURI)
  {
    if (paramURI == null)
      return null;
    boolean bool = paramURI.isAbsolute();
    HttpHost localHttpHost = null;
    int i;
    int j;
    if (bool)
    {
      i = paramURI.getPort();
      str1 = paramURI.getHost();
      if (str1 == null)
      {
        str1 = paramURI.getAuthority();
        if (str1 != null)
        {
          j = str1.indexOf('@');
          if (j >= 0)
            if (str1.length() <= j + 1)
              break label163;
        }
      }
    }
    label163: for (String str1 = str1.substring(j + 1); ; str1 = null)
    {
      if (str1 != null)
      {
        int k = str1.indexOf(':');
        if (k >= 0)
        {
          if (k + 1 < str1.length())
            i = Integer.parseInt(str1.substring(k + 1));
          str1 = str1.substring(0, k);
        }
      }
      String str2 = paramURI.getScheme();
      localHttpHost = null;
      if (str1 != null)
        localHttpHost = new HttpHost(str1, i, str2);
      return localHttpHost;
    }
  }

  private static String normalizePath(String paramString)
  {
    if (paramString == null)
      return null;
    for (int i = 0; ; i++)
      if ((i >= paramString.length()) || (paramString.charAt(i) != '/'))
      {
        if (i > 1)
          paramString = paramString.substring(i - 1);
        return paramString;
      }
  }

  private static URI removeDotSegments(URI paramURI)
  {
    String str1 = paramURI.getPath();
    if ((str1 == null) || (str1.indexOf("/.") == -1))
      return paramURI;
    String[] arrayOfString = str1.split("/");
    Stack localStack = new Stack();
    int i = 0;
    if (i < arrayOfString.length)
    {
      if ((arrayOfString[i].length() == 0) || (".".equals(arrayOfString[i])));
      while (true)
      {
        i++;
        break;
        if ("..".equals(arrayOfString[i]))
        {
          if (!localStack.isEmpty())
            localStack.pop();
        }
        else
          localStack.push(arrayOfString[i]);
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = localStack.iterator();
    while (localIterator.hasNext())
    {
      String str2 = (String)localIterator.next();
      localStringBuilder.append('/').append(str2);
    }
    try
    {
      URI localURI = new URI(paramURI.getScheme(), paramURI.getAuthority(), localStringBuilder.toString(), paramURI.getQuery(), paramURI.getFragment());
      return localURI;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      throw new IllegalArgumentException(localURISyntaxException);
    }
  }

  public static URI resolve(URI paramURI, String paramString)
  {
    return resolve(paramURI, URI.create(paramString));
  }

  public static URI resolve(URI paramURI1, URI paramURI2)
  {
    if (paramURI1 == null)
      throw new IllegalArgumentException("Base URI may nor be null");
    if (paramURI2 == null)
      throw new IllegalArgumentException("Reference URI may nor be null");
    String str1 = paramURI2.toString();
    if (str1.startsWith("?"))
      return resolveReferenceStartingWithQueryString(paramURI1, paramURI2);
    if (str1.length() == 0);
    for (int i = 1; ; i = 0)
    {
      if (i != 0)
        paramURI2 = URI.create("#");
      URI localURI = paramURI1.resolve(paramURI2);
      if (i != 0)
      {
        String str2 = localURI.toString();
        localURI = URI.create(str2.substring(0, str2.indexOf('#')));
      }
      return removeDotSegments(localURI);
    }
  }

  private static URI resolveReferenceStartingWithQueryString(URI paramURI1, URI paramURI2)
  {
    String str = paramURI1.toString();
    if (str.indexOf('?') > -1)
      str = str.substring(0, str.indexOf('?'));
    return URI.create(str + paramURI2.toString());
  }

  public static URI rewriteURI(URI paramURI, HttpHost paramHttpHost)
    throws URISyntaxException
  {
    return rewriteURI(paramURI, paramHttpHost, false);
  }

  public static URI rewriteURI(URI paramURI, HttpHost paramHttpHost, boolean paramBoolean)
    throws URISyntaxException
  {
    if (paramURI == null)
      throw new IllegalArgumentException("URI may nor be null");
    if (paramHttpHost != null)
    {
      String str4 = paramHttpHost.getSchemeName();
      String str5 = paramHttpHost.getHostName();
      int i = paramHttpHost.getPort();
      String str6 = normalizePath(paramURI.getRawPath());
      String str7 = paramURI.getRawQuery();
      if (paramBoolean);
      for (String str8 = null; ; str8 = paramURI.getRawFragment())
        return createURI(str4, str5, i, str6, str7, str8);
    }
    String str1 = normalizePath(paramURI.getRawPath());
    String str2 = paramURI.getRawQuery();
    if (paramBoolean);
    for (String str3 = null; ; str3 = paramURI.getRawFragment())
      return createURI(null, null, -1, str1, str2, str3);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.client.utils.URIUtils
 * JD-Core Version:    0.6.2
 */