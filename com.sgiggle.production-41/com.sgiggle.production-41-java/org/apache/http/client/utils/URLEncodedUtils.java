package org.apache.http.client.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.annotation.Immutable;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

@Immutable
public class URLEncodedUtils
{
  public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
  private static final String NAME_VALUE_SEPARATOR = "=";
  private static final String PARAMETER_SEPARATOR = "&";

  private static String decode(String paramString1, String paramString2)
  {
    String str;
    if (paramString2 != null)
      str = paramString2;
    try
    {
      while (true)
      {
        return URLDecoder.decode(paramString1, str);
        str = "ISO-8859-1";
      }
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new IllegalArgumentException(localUnsupportedEncodingException);
    }
  }

  private static String encode(String paramString1, String paramString2)
  {
    String str;
    if (paramString2 != null)
      str = paramString2;
    try
    {
      while (true)
      {
        return URLEncoder.encode(paramString1, str);
        str = "ISO-8859-1";
      }
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new IllegalArgumentException(localUnsupportedEncodingException);
    }
  }

  public static String format(List<? extends NameValuePair> paramList, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    if (localIterator.hasNext())
    {
      NameValuePair localNameValuePair = (NameValuePair)localIterator.next();
      String str1 = encode(localNameValuePair.getName(), paramString);
      String str2 = localNameValuePair.getValue();
      if (str2 != null);
      for (String str3 = encode(str2, paramString); ; str3 = "")
      {
        if (localStringBuilder.length() > 0)
          localStringBuilder.append("&");
        localStringBuilder.append(str1);
        localStringBuilder.append("=");
        localStringBuilder.append(str3);
        break;
      }
    }
    return localStringBuilder.toString();
  }

  public static boolean isEncoded(HttpEntity paramHttpEntity)
  {
    Header localHeader = paramHttpEntity.getContentType();
    if (localHeader != null)
    {
      HeaderElement[] arrayOfHeaderElement = localHeader.getElements();
      if (arrayOfHeaderElement.length > 0)
        return arrayOfHeaderElement[0].getName().equalsIgnoreCase("application/x-www-form-urlencoded");
      return false;
    }
    return false;
  }

  public static List<NameValuePair> parse(URI paramURI, String paramString)
  {
    Object localObject = Collections.emptyList();
    String str = paramURI.getRawQuery();
    if ((str != null) && (str.length() > 0))
    {
      localObject = new ArrayList();
      parse((List)localObject, new Scanner(str), paramString);
    }
    return localObject;
  }

  public static List<NameValuePair> parse(HttpEntity paramHttpEntity)
    throws IOException
  {
    Object localObject = Collections.emptyList();
    Header localHeader = paramHttpEntity.getContentType();
    String str1 = null;
    String str2 = null;
    if (localHeader != null)
    {
      HeaderElement[] arrayOfHeaderElement = localHeader.getElements();
      int i = arrayOfHeaderElement.length;
      str1 = null;
      str2 = null;
      if (i > 0)
      {
        HeaderElement localHeaderElement = arrayOfHeaderElement[0];
        str2 = localHeaderElement.getName();
        NameValuePair localNameValuePair = localHeaderElement.getParameterByName("charset");
        str1 = null;
        if (localNameValuePair != null)
          str1 = localNameValuePair.getValue();
      }
    }
    if ((str2 != null) && (str2.equalsIgnoreCase("application/x-www-form-urlencoded")))
    {
      String str3 = EntityUtils.toString(paramHttpEntity, "ASCII");
      if ((str3 != null) && (str3.length() > 0))
      {
        localObject = new ArrayList();
        parse((List)localObject, new Scanner(str3), str1);
      }
    }
    return localObject;
  }

  public static void parse(List<NameValuePair> paramList, Scanner paramScanner, String paramString)
  {
    paramScanner.useDelimiter("&");
    while (paramScanner.hasNext())
    {
      String[] arrayOfString = paramScanner.next().split("=");
      if ((arrayOfString.length == 0) || (arrayOfString.length > 2))
        throw new IllegalArgumentException("bad parameter");
      String str1 = decode(arrayOfString[0], paramString);
      int i = arrayOfString.length;
      String str2 = null;
      if (i == 2)
        str2 = decode(arrayOfString[1], paramString);
      paramList.add(new BasicNameValuePair(str1, str2));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.client.utils.URLEncodedUtils
 * JD-Core Version:    0.6.2
 */