package org.apache.http.conn;

import org.apache.http.annotation.Immutable;

@Immutable
public class ConnectionPoolTimeoutException extends ConnectTimeoutException
{
  private static final long serialVersionUID = -7898874842020245128L;

  public ConnectionPoolTimeoutException()
  {
  }

  public ConnectionPoolTimeoutException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.conn.ConnectionPoolTimeoutException
 * JD-Core Version:    0.6.2
 */