package org.apache.http.conn.params;

@Deprecated
public abstract interface ConnManagerPNames
{

  @Deprecated
  public static final String MAX_CONNECTIONS_PER_ROUTE = "http.conn-manager.max-per-route";

  @Deprecated
  public static final String MAX_TOTAL_CONNECTIONS = "http.conn-manager.max-total";

  @Deprecated
  public static final String TIMEOUT = "http.conn-manager.timeout";
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.conn.params.ConnManagerPNames
 * JD-Core Version:    0.6.2
 */