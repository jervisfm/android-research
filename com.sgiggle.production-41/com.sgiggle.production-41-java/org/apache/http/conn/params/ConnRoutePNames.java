package org.apache.http.conn.params;

public abstract interface ConnRoutePNames
{
  public static final String DEFAULT_PROXY = "http.route.default-proxy";
  public static final String FORCED_ROUTE = "http.route.forced-route";
  public static final String LOCAL_ADDRESS = "http.route.local-address";
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.conn.params.ConnRoutePNames
 * JD-Core Version:    0.6.2
 */