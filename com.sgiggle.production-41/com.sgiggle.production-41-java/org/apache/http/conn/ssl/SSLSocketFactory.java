package org.apache.http.conn.ssl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.HostNameResolver;
import org.apache.http.conn.scheme.LayeredSchemeSocketFactory;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.params.HttpParams;

@ThreadSafe
public class SSLSocketFactory
  implements LayeredSchemeSocketFactory, LayeredSocketFactory
{
  public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER = new AllowAllHostnameVerifier();
  public static final X509HostnameVerifier BROWSER_COMPATIBLE_HOSTNAME_VERIFIER = new BrowserCompatHostnameVerifier();
  public static final String SSL = "SSL";
  public static final String SSLV2 = "SSLv2";
  public static final X509HostnameVerifier STRICT_HOSTNAME_VERIFIER = new StrictHostnameVerifier();
  public static final String TLS = "TLS";
  private volatile X509HostnameVerifier hostnameVerifier;
  private final HostNameResolver nameResolver;
  private final javax.net.ssl.SSLSocketFactory socketfactory;

  private SSLSocketFactory()
  {
    this(createDefaultSSLContext());
  }

  @Deprecated
  public SSLSocketFactory(String paramString1, KeyStore paramKeyStore1, String paramString2, KeyStore paramKeyStore2, SecureRandom paramSecureRandom, HostNameResolver paramHostNameResolver)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    this(createSSLContext(paramString1, paramKeyStore1, paramString2, paramKeyStore2, paramSecureRandom, null), paramHostNameResolver);
  }

  public SSLSocketFactory(String paramString1, KeyStore paramKeyStore1, String paramString2, KeyStore paramKeyStore2, SecureRandom paramSecureRandom, TrustStrategy paramTrustStrategy, X509HostnameVerifier paramX509HostnameVerifier)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    this(createSSLContext(paramString1, paramKeyStore1, paramString2, paramKeyStore2, paramSecureRandom, paramTrustStrategy), paramX509HostnameVerifier);
  }

  public SSLSocketFactory(String paramString1, KeyStore paramKeyStore1, String paramString2, KeyStore paramKeyStore2, SecureRandom paramSecureRandom, X509HostnameVerifier paramX509HostnameVerifier)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    this(createSSLContext(paramString1, paramKeyStore1, paramString2, paramKeyStore2, paramSecureRandom, null), paramX509HostnameVerifier);
  }

  public SSLSocketFactory(KeyStore paramKeyStore)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    this("TLS", null, null, paramKeyStore, null, null, BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
  }

  public SSLSocketFactory(KeyStore paramKeyStore, String paramString)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    this("TLS", paramKeyStore, paramString, null, null, null, BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
  }

  public SSLSocketFactory(KeyStore paramKeyStore1, String paramString, KeyStore paramKeyStore2)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    this("TLS", paramKeyStore1, paramString, paramKeyStore2, null, null, BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
  }

  public SSLSocketFactory(SSLContext paramSSLContext)
  {
    this(paramSSLContext, BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
  }

  @Deprecated
  public SSLSocketFactory(SSLContext paramSSLContext, HostNameResolver paramHostNameResolver)
  {
    this.socketfactory = paramSSLContext.getSocketFactory();
    this.hostnameVerifier = BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
    this.nameResolver = paramHostNameResolver;
  }

  public SSLSocketFactory(SSLContext paramSSLContext, X509HostnameVerifier paramX509HostnameVerifier)
  {
    this.socketfactory = paramSSLContext.getSocketFactory();
    this.hostnameVerifier = paramX509HostnameVerifier;
    this.nameResolver = null;
  }

  public SSLSocketFactory(TrustStrategy paramTrustStrategy)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    this("TLS", null, null, null, null, paramTrustStrategy, BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
  }

  public SSLSocketFactory(TrustStrategy paramTrustStrategy, X509HostnameVerifier paramX509HostnameVerifier)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    this("TLS", null, null, null, null, paramTrustStrategy, paramX509HostnameVerifier);
  }

  private static SSLContext createDefaultSSLContext()
  {
    try
    {
      SSLContext localSSLContext = createSSLContext("TLS", null, null, null, null, null);
      return localSSLContext;
    }
    catch (Exception localException)
    {
      throw new IllegalStateException("Failure initializing default SSL context", localException);
    }
  }

  private static SSLContext createSSLContext(String paramString1, KeyStore paramKeyStore1, String paramString2, KeyStore paramKeyStore2, SecureRandom paramSecureRandom, TrustStrategy paramTrustStrategy)
    throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException
  {
    if (paramString1 == null)
      paramString1 = "TLS";
    KeyManagerFactory localKeyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    if (paramString2 != null);
    KeyManager[] arrayOfKeyManager;
    TrustManager[] arrayOfTrustManager;
    for (char[] arrayOfChar = paramString2.toCharArray(); ; arrayOfChar = null)
    {
      localKeyManagerFactory.init(paramKeyStore1, arrayOfChar);
      arrayOfKeyManager = localKeyManagerFactory.getKeyManagers();
      TrustManagerFactory localTrustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
      localTrustManagerFactory.init(paramKeyStore2);
      arrayOfTrustManager = localTrustManagerFactory.getTrustManagers();
      if ((arrayOfTrustManager == null) || (paramTrustStrategy == null))
        break;
      for (int i = 0; i < arrayOfTrustManager.length; i++)
      {
        TrustManager localTrustManager = arrayOfTrustManager[i];
        if ((localTrustManager instanceof X509TrustManager))
          arrayOfTrustManager[i] = new TrustManagerDecorator((X509TrustManager)localTrustManager, paramTrustStrategy);
      }
    }
    SSLContext localSSLContext = SSLContext.getInstance(paramString1);
    localSSLContext.init(arrayOfKeyManager, arrayOfTrustManager, paramSecureRandom);
    return localSSLContext;
  }

  public static SSLSocketFactory getSocketFactory()
  {
    return new SSLSocketFactory();
  }

  @Deprecated
  public Socket connectSocket(Socket paramSocket, String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2, HttpParams paramHttpParams)
    throws IOException, UnknownHostException, ConnectTimeoutException
  {
    InetSocketAddress localInetSocketAddress;
    if (paramInetAddress == null)
    {
      localInetSocketAddress = null;
      if (paramInt2 <= 0);
    }
    else
    {
      if (paramInt2 < 0)
        paramInt2 = 0;
      localInetSocketAddress = new InetSocketAddress(paramInetAddress, paramInt2);
    }
    if (this.nameResolver != null);
    for (InetAddress localInetAddress = this.nameResolver.resolve(paramString); ; localInetAddress = InetAddress.getByName(paramString))
      return connectSocket(paramSocket, new InetSocketAddress(localInetAddress, paramInt1), localInetSocketAddress, paramHttpParams);
  }

  // ERROR //
  public Socket connectSocket(Socket paramSocket, InetSocketAddress paramInetSocketAddress1, InetSocketAddress paramInetSocketAddress2, HttpParams paramHttpParams)
    throws IOException, UnknownHostException, ConnectTimeoutException
  {
    // Byte code:
    //   0: aload_2
    //   1: ifnonnull +13 -> 14
    //   4: new 185	java/lang/IllegalArgumentException
    //   7: dup
    //   8: ldc 187
    //   10: invokespecial 190	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   13: athrow
    //   14: aload 4
    //   16: ifnonnull +13 -> 29
    //   19: new 185	java/lang/IllegalArgumentException
    //   22: dup
    //   23: ldc 192
    //   25: invokespecial 190	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   28: athrow
    //   29: aload_1
    //   30: ifnull +156 -> 186
    //   33: aload_1
    //   34: astore 5
    //   36: aload_3
    //   37: ifnull +19 -> 56
    //   40: aload 5
    //   42: aload 4
    //   44: invokestatic 198	org/apache/http/params/HttpConnectionParams:getSoReuseaddr	(Lorg/apache/http/params/HttpParams;)Z
    //   47: invokevirtual 204	java/net/Socket:setReuseAddress	(Z)V
    //   50: aload 5
    //   52: aload_3
    //   53: invokevirtual 208	java/net/Socket:bind	(Ljava/net/SocketAddress;)V
    //   56: aload 4
    //   58: invokestatic 212	org/apache/http/params/HttpConnectionParams:getConnectionTimeout	(Lorg/apache/http/params/HttpParams;)I
    //   61: istore 6
    //   63: aload 4
    //   65: invokestatic 215	org/apache/http/params/HttpConnectionParams:getSoTimeout	(Lorg/apache/http/params/HttpParams;)I
    //   68: istore 7
    //   70: aload 5
    //   72: iload 7
    //   74: invokevirtual 219	java/net/Socket:setSoTimeout	(I)V
    //   77: aload 5
    //   79: aload_2
    //   80: iload 6
    //   82: invokevirtual 223	java/net/Socket:connect	(Ljava/net/SocketAddress;I)V
    //   85: aload_2
    //   86: invokevirtual 226	java/net/InetSocketAddress:toString	()Ljava/lang/String;
    //   89: astore 9
    //   91: aload_2
    //   92: invokevirtual 230	java/net/InetSocketAddress:getPort	()I
    //   95: istore 10
    //   97: new 232	java/lang/StringBuilder
    //   100: dup
    //   101: invokespecial 233	java/lang/StringBuilder:<init>	()V
    //   104: ldc 235
    //   106: invokevirtual 239	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   109: iload 10
    //   111: invokevirtual 242	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   114: invokevirtual 243	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   117: astore 11
    //   119: aload 9
    //   121: aload 11
    //   123: invokevirtual 247	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   126: ifeq +22 -> 148
    //   129: aload 9
    //   131: iconst_0
    //   132: aload 9
    //   134: invokevirtual 250	java/lang/String:length	()I
    //   137: aload 11
    //   139: invokevirtual 250	java/lang/String:length	()I
    //   142: isub
    //   143: invokevirtual 254	java/lang/String:substring	(II)Ljava/lang/String;
    //   146: astore 9
    //   148: aload 5
    //   150: instanceof 256
    //   153: ifeq +81 -> 234
    //   156: aload 5
    //   158: checkcast 256	javax/net/ssl/SSLSocket
    //   161: astore 12
    //   163: aload_0
    //   164: getfield 90	org/apache/http/conn/ssl/SSLSocketFactory:hostnameVerifier	Lorg/apache/http/conn/ssl/X509HostnameVerifier;
    //   167: ifnull +16 -> 183
    //   170: aload_0
    //   171: getfield 90	org/apache/http/conn/ssl/SSLSocketFactory:hostnameVerifier	Lorg/apache/http/conn/ssl/X509HostnameVerifier;
    //   174: aload 9
    //   176: aload 12
    //   178: invokeinterface 262 3 0
    //   183: aload 12
    //   185: areturn
    //   186: new 200	java/net/Socket
    //   189: dup
    //   190: invokespecial 263	java/net/Socket:<init>	()V
    //   193: astore 5
    //   195: goto -159 -> 36
    //   198: astore 8
    //   200: new 162	org/apache/http/conn/ConnectTimeoutException
    //   203: dup
    //   204: new 232	java/lang/StringBuilder
    //   207: dup
    //   208: invokespecial 233	java/lang/StringBuilder:<init>	()V
    //   211: ldc_w 265
    //   214: invokevirtual 239	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   217: aload_2
    //   218: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   221: ldc_w 270
    //   224: invokevirtual 239	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   227: invokevirtual 243	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   230: invokespecial 271	org/apache/http/conn/ConnectTimeoutException:<init>	(Ljava/lang/String;)V
    //   233: athrow
    //   234: aload_0
    //   235: getfield 88	org/apache/http/conn/ssl/SSLSocketFactory:socketfactory	Ljavax/net/ssl/SSLSocketFactory;
    //   238: aload 5
    //   240: aload 9
    //   242: iload 10
    //   244: iconst_1
    //   245: invokevirtual 277	javax/net/ssl/SSLSocketFactory:createSocket	(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    //   248: checkcast 256	javax/net/ssl/SSLSocket
    //   251: astore 12
    //   253: goto -90 -> 163
    //   256: astore 13
    //   258: aload 12
    //   260: invokevirtual 280	javax/net/ssl/SSLSocket:close	()V
    //   263: aload 13
    //   265: athrow
    //   266: astore 14
    //   268: goto -5 -> 263
    //
    // Exception table:
    //   from	to	target	type
    //   70	85	198	java/net/SocketTimeoutException
    //   170	183	256	java/io/IOException
    //   258	263	266	java/lang/Exception
  }

  public Socket createLayeredSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
    throws IOException, UnknownHostException
  {
    SSLSocket localSSLSocket = (SSLSocket)this.socketfactory.createSocket(paramSocket, paramString, paramInt, paramBoolean);
    if (this.hostnameVerifier != null)
      this.hostnameVerifier.verify(paramString, localSSLSocket);
    return localSSLSocket;
  }

  @Deprecated
  public Socket createSocket()
    throws IOException
  {
    return this.socketfactory.createSocket();
  }

  @Deprecated
  public Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
    throws IOException, UnknownHostException
  {
    return createLayeredSocket(paramSocket, paramString, paramInt, paramBoolean);
  }

  public Socket createSocket(HttpParams paramHttpParams)
    throws IOException
  {
    return this.socketfactory.createSocket();
  }

  public X509HostnameVerifier getHostnameVerifier()
  {
    return this.hostnameVerifier;
  }

  public boolean isSecure(Socket paramSocket)
    throws IllegalArgumentException
  {
    if (paramSocket == null)
      throw new IllegalArgumentException("Socket may not be null");
    if (!(paramSocket instanceof SSLSocket))
      throw new IllegalArgumentException("Socket not created by this factory");
    if (paramSocket.isClosed())
      throw new IllegalArgumentException("Socket is closed");
    return true;
  }

  @Deprecated
  public void setHostnameVerifier(X509HostnameVerifier paramX509HostnameVerifier)
  {
    if (paramX509HostnameVerifier == null)
      throw new IllegalArgumentException("Hostname verifier may not be null");
    this.hostnameVerifier = paramX509HostnameVerifier;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.conn.ssl.SSLSocketFactory
 * JD-Core Version:    0.6.2
 */