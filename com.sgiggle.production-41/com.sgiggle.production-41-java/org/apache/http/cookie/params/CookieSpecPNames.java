package org.apache.http.cookie.params;

public abstract interface CookieSpecPNames
{
  public static final String DATE_PATTERNS = "http.protocol.cookie-datepatterns";
  public static final String SINGLE_COOKIE_HEADER = "http.protocol.single-cookie-header";
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.cookie.params.CookieSpecPNames
 * JD-Core Version:    0.6.2
 */