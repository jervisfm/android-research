package org.apache.http.impl;

import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpConnection;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ParseException;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.TokenIterator;
import org.apache.http.message.BasicTokenIterator;
import org.apache.http.protocol.HttpContext;

public class DefaultConnectionReuseStrategy
  implements ConnectionReuseStrategy
{
  protected TokenIterator createTokenIterator(HeaderIterator paramHeaderIterator)
  {
    return new BasicTokenIterator(paramHeaderIterator);
  }

  public boolean keepAlive(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
  {
    if (paramHttpResponse == null)
      throw new IllegalArgumentException("HTTP response may not be null.");
    if (paramHttpContext == null)
      throw new IllegalArgumentException("HTTP context may not be null.");
    HttpConnection localHttpConnection = (HttpConnection)paramHttpContext.getAttribute("http.connection");
    if ((localHttpConnection != null) && (!localHttpConnection.isOpen()))
      return false;
    HttpEntity localHttpEntity = paramHttpResponse.getEntity();
    ProtocolVersion localProtocolVersion = paramHttpResponse.getStatusLine().getProtocolVersion();
    if ((localHttpEntity != null) && (localHttpEntity.getContentLength() < 0L) && ((!localHttpEntity.isChunked()) || (localProtocolVersion.lessEquals(HttpVersion.HTTP_1_0))))
      return false;
    HeaderIterator localHeaderIterator = paramHttpResponse.headerIterator("Connection");
    if (!localHeaderIterator.hasNext())
      localHeaderIterator = paramHttpResponse.headerIterator("Proxy-Connection");
    if (localHeaderIterator.hasNext())
      try
      {
        TokenIterator localTokenIterator = createTokenIterator(localHeaderIterator);
        int i = 0;
        while (localTokenIterator.hasNext())
        {
          String str = localTokenIterator.nextToken();
          if ("Close".equalsIgnoreCase(str))
            return false;
          boolean bool = "Keep-Alive".equalsIgnoreCase(str);
          if (bool)
            i = 1;
        }
        if (i != 0)
          return true;
      }
      catch (ParseException localParseException)
      {
        return false;
      }
    return !localProtocolVersion.lessEquals(HttpVersion.HTTP_1_0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.DefaultConnectionReuseStrategy
 * JD-Core Version:    0.6.2
 */