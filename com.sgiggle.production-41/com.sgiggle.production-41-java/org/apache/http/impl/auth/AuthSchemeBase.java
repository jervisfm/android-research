package org.apache.http.impl.auth;

import org.apache.http.FormattedHeader;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.ContextAwareAuthScheme;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.CharArrayBuffer;

@NotThreadSafe
public abstract class AuthSchemeBase
  implements ContextAwareAuthScheme
{
  private boolean proxy;

  public Header authenticate(Credentials paramCredentials, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws AuthenticationException
  {
    return authenticate(paramCredentials, paramHttpRequest);
  }

  public boolean isProxy()
  {
    return this.proxy;
  }

  protected abstract void parseChallenge(CharArrayBuffer paramCharArrayBuffer, int paramInt1, int paramInt2)
    throws MalformedChallengeException;

  public void processChallenge(Header paramHeader)
    throws MalformedChallengeException
  {
    if (paramHeader == null)
      throw new IllegalArgumentException("Header may not be null");
    String str1 = paramHeader.getName();
    CharArrayBuffer localCharArrayBuffer;
    int i;
    if (str1.equalsIgnoreCase("WWW-Authenticate"))
    {
      this.proxy = false;
      if (!(paramHeader instanceof FormattedHeader))
        break label137;
      localCharArrayBuffer = ((FormattedHeader)paramHeader).getBuffer();
      i = ((FormattedHeader)paramHeader).getValuePos();
    }
    while (true)
    {
      if ((i >= localCharArrayBuffer.length()) || (!HTTP.isWhitespace(localCharArrayBuffer.charAt(i))))
        break label183;
      i++;
      continue;
      if (str1.equalsIgnoreCase("Proxy-Authenticate"))
      {
        this.proxy = true;
        break;
      }
      throw new MalformedChallengeException("Unexpected header name: " + str1);
      label137: String str2 = paramHeader.getValue();
      if (str2 == null)
        throw new MalformedChallengeException("Header value is null");
      localCharArrayBuffer = new CharArrayBuffer(str2.length());
      localCharArrayBuffer.append(str2);
      i = 0;
    }
    label183: int j = i;
    while ((i < localCharArrayBuffer.length()) && (!HTTP.isWhitespace(localCharArrayBuffer.charAt(i))))
      i++;
    String str3 = localCharArrayBuffer.substring(j, i);
    if (!str3.equalsIgnoreCase(getSchemeName()))
      throw new MalformedChallengeException("Invalid scheme identifier: " + str3);
    parseChallenge(localCharArrayBuffer, i, localCharArrayBuffer.length());
  }

  public String toString()
  {
    return getSchemeName();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.auth.AuthSchemeBase
 * JD-Core Version:    0.6.2
 */