package org.apache.http.impl.auth;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.util.CharArrayBuffer;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.Oid;

public class NegotiateScheme extends AuthSchemeBase
{
  private static final String KERBEROS_OID = "1.2.840.113554.1.2.2";
  private static final String SPNEGO_OID = "1.3.6.1.5.5.2";
  private GSSContext gssContext = null;
  private final Log log = LogFactory.getLog(getClass());
  private Oid negotiationOid = null;
  private final SpnegoTokenGenerator spengoGenerator;
  private State state = State.UNINITIATED;
  private final boolean stripPort;
  private byte[] token;

  public NegotiateScheme()
  {
    this(null, false);
  }

  public NegotiateScheme(SpnegoTokenGenerator paramSpnegoTokenGenerator)
  {
    this(paramSpnegoTokenGenerator, false);
  }

  public NegotiateScheme(SpnegoTokenGenerator paramSpnegoTokenGenerator, boolean paramBoolean)
  {
    this.spengoGenerator = paramSpnegoTokenGenerator;
    this.stripPort = paramBoolean;
  }

  @Deprecated
  public Header authenticate(Credentials paramCredentials, HttpRequest paramHttpRequest)
    throws AuthenticationException
  {
    return authenticate(paramCredentials, paramHttpRequest, null);
  }

  // ERROR //
  public Header authenticate(Credentials paramCredentials, HttpRequest paramHttpRequest, org.apache.http.protocol.HttpContext paramHttpContext)
    throws AuthenticationException
  {
    // Byte code:
    //   0: aload_2
    //   1: ifnonnull +13 -> 14
    //   4: new 76	java/lang/IllegalArgumentException
    //   7: dup
    //   8: ldc 78
    //   10: invokespecial 81	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   13: athrow
    //   14: aload_0
    //   15: getfield 58	org/apache/http/impl/auth/NegotiateScheme:state	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   18: getstatic 84	org/apache/http/impl/auth/NegotiateScheme$State:CHALLENGE_RECEIVED	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   21: if_acmpeq +13 -> 34
    //   24: new 86	java/lang/IllegalStateException
    //   27: dup
    //   28: ldc 88
    //   30: invokespecial 89	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   33: athrow
    //   34: aload_0
    //   35: invokevirtual 93	org/apache/http/impl/auth/NegotiateScheme:isProxy	()Z
    //   38: ifeq +79 -> 117
    //   41: ldc 95
    //   43: astore 6
    //   45: aload_3
    //   46: aload 6
    //   48: invokeinterface 101 2 0
    //   53: checkcast 103	org/apache/http/HttpHost
    //   56: astore 7
    //   58: aload 7
    //   60: ifnonnull +64 -> 124
    //   63: new 66	org/apache/http/auth/AuthenticationException
    //   66: dup
    //   67: ldc 105
    //   69: invokespecial 106	org/apache/http/auth/AuthenticationException:<init>	(Ljava/lang/String;)V
    //   72: athrow
    //   73: astore 5
    //   75: aload_0
    //   76: getstatic 109	org/apache/http/impl/auth/NegotiateScheme$State:FAILED	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   79: putfield 58	org/apache/http/impl/auth/NegotiateScheme:state	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   82: aload 5
    //   84: invokevirtual 113	org/ietf/jgss/GSSException:getMajor	()I
    //   87: bipush 9
    //   89: if_icmpeq +13 -> 102
    //   92: aload 5
    //   94: invokevirtual 113	org/ietf/jgss/GSSException:getMajor	()I
    //   97: bipush 8
    //   99: if_icmpne +556 -> 655
    //   102: new 115	org/apache/http/auth/InvalidCredentialsException
    //   105: dup
    //   106: aload 5
    //   108: invokevirtual 119	org/ietf/jgss/GSSException:getMessage	()Ljava/lang/String;
    //   111: aload 5
    //   113: invokespecial 122	org/apache/http/auth/InvalidCredentialsException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   116: athrow
    //   117: ldc 124
    //   119: astore 6
    //   121: goto -76 -> 45
    //   124: aload_0
    //   125: getfield 62	org/apache/http/impl/auth/NegotiateScheme:stripPort	Z
    //   128: ifne +343 -> 471
    //   131: aload 7
    //   133: invokevirtual 127	org/apache/http/HttpHost:getPort	()I
    //   136: ifle +335 -> 471
    //   139: aload 7
    //   141: invokevirtual 130	org/apache/http/HttpHost:toHostString	()Ljava/lang/String;
    //   144: astore 8
    //   146: aload_0
    //   147: getfield 47	org/apache/http/impl/auth/NegotiateScheme:log	Lorg/apache/commons/logging/Log;
    //   150: invokeinterface 135 1 0
    //   155: ifeq +32 -> 187
    //   158: aload_0
    //   159: getfield 47	org/apache/http/impl/auth/NegotiateScheme:log	Lorg/apache/commons/logging/Log;
    //   162: new 137	java/lang/StringBuilder
    //   165: dup
    //   166: invokespecial 138	java/lang/StringBuilder:<init>	()V
    //   169: ldc 140
    //   171: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   174: aload 8
    //   176: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: invokevirtual 147	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   182: invokeinterface 151 2 0
    //   187: aload_0
    //   188: new 153	org/ietf/jgss/Oid
    //   191: dup
    //   192: ldc 11
    //   194: invokespecial 154	org/ietf/jgss/Oid:<init>	(Ljava/lang/String;)V
    //   197: putfield 51	org/apache/http/impl/auth/NegotiateScheme:negotiationOid	Lorg/ietf/jgss/Oid;
    //   200: iconst_0
    //   201: istore 9
    //   203: aload_0
    //   204: invokevirtual 158	org/apache/http/impl/auth/NegotiateScheme:getManager	()Lorg/ietf/jgss/GSSManager;
    //   207: astore 14
    //   209: aload_0
    //   210: aload 14
    //   212: aload 14
    //   214: new 137	java/lang/StringBuilder
    //   217: dup
    //   218: invokespecial 138	java/lang/StringBuilder:<init>	()V
    //   221: ldc 160
    //   223: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   226: aload 8
    //   228: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   231: invokevirtual 147	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   234: getstatic 165	org/ietf/jgss/GSSName:NT_HOSTBASED_SERVICE	Lorg/ietf/jgss/Oid;
    //   237: invokevirtual 171	org/ietf/jgss/GSSManager:createName	(Ljava/lang/String;Lorg/ietf/jgss/Oid;)Lorg/ietf/jgss/GSSName;
    //   240: aload_0
    //   241: getfield 51	org/apache/http/impl/auth/NegotiateScheme:negotiationOid	Lorg/ietf/jgss/Oid;
    //   244: invokeinterface 175 2 0
    //   249: aload_0
    //   250: getfield 51	org/apache/http/impl/auth/NegotiateScheme:negotiationOid	Lorg/ietf/jgss/Oid;
    //   253: aconst_null
    //   254: iconst_0
    //   255: invokevirtual 179	org/ietf/jgss/GSSManager:createContext	(Lorg/ietf/jgss/GSSName;Lorg/ietf/jgss/Oid;Lorg/ietf/jgss/GSSCredential;I)Lorg/ietf/jgss/GSSContext;
    //   258: putfield 49	org/apache/http/impl/auth/NegotiateScheme:gssContext	Lorg/ietf/jgss/GSSContext;
    //   261: aload_0
    //   262: getfield 49	org/apache/http/impl/auth/NegotiateScheme:gssContext	Lorg/ietf/jgss/GSSContext;
    //   265: iconst_1
    //   266: invokeinterface 185 2 0
    //   271: aload_0
    //   272: getfield 49	org/apache/http/impl/auth/NegotiateScheme:gssContext	Lorg/ietf/jgss/GSSContext;
    //   275: iconst_1
    //   276: invokeinterface 188 2 0
    //   281: iload 9
    //   283: ifeq +105 -> 388
    //   286: aload_0
    //   287: getfield 47	org/apache/http/impl/auth/NegotiateScheme:log	Lorg/apache/commons/logging/Log;
    //   290: ldc 190
    //   292: invokeinterface 151 2 0
    //   297: aload_0
    //   298: new 153	org/ietf/jgss/Oid
    //   301: dup
    //   302: ldc 8
    //   304: invokespecial 154	org/ietf/jgss/Oid:<init>	(Ljava/lang/String;)V
    //   307: putfield 51	org/apache/http/impl/auth/NegotiateScheme:negotiationOid	Lorg/ietf/jgss/Oid;
    //   310: aload_0
    //   311: invokevirtual 158	org/apache/http/impl/auth/NegotiateScheme:getManager	()Lorg/ietf/jgss/GSSManager;
    //   314: astore 13
    //   316: aload_0
    //   317: aload 13
    //   319: aload 13
    //   321: new 137	java/lang/StringBuilder
    //   324: dup
    //   325: invokespecial 138	java/lang/StringBuilder:<init>	()V
    //   328: ldc 160
    //   330: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   333: aload 8
    //   335: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   338: invokevirtual 147	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   341: getstatic 165	org/ietf/jgss/GSSName:NT_HOSTBASED_SERVICE	Lorg/ietf/jgss/Oid;
    //   344: invokevirtual 171	org/ietf/jgss/GSSManager:createName	(Ljava/lang/String;Lorg/ietf/jgss/Oid;)Lorg/ietf/jgss/GSSName;
    //   347: aload_0
    //   348: getfield 51	org/apache/http/impl/auth/NegotiateScheme:negotiationOid	Lorg/ietf/jgss/Oid;
    //   351: invokeinterface 175 2 0
    //   356: aload_0
    //   357: getfield 51	org/apache/http/impl/auth/NegotiateScheme:negotiationOid	Lorg/ietf/jgss/Oid;
    //   360: aconst_null
    //   361: iconst_0
    //   362: invokevirtual 179	org/ietf/jgss/GSSManager:createContext	(Lorg/ietf/jgss/GSSName;Lorg/ietf/jgss/Oid;Lorg/ietf/jgss/GSSCredential;I)Lorg/ietf/jgss/GSSContext;
    //   365: putfield 49	org/apache/http/impl/auth/NegotiateScheme:gssContext	Lorg/ietf/jgss/GSSContext;
    //   368: aload_0
    //   369: getfield 49	org/apache/http/impl/auth/NegotiateScheme:gssContext	Lorg/ietf/jgss/GSSContext;
    //   372: iconst_1
    //   373: invokeinterface 185 2 0
    //   378: aload_0
    //   379: getfield 49	org/apache/http/impl/auth/NegotiateScheme:gssContext	Lorg/ietf/jgss/GSSContext;
    //   382: iconst_1
    //   383: invokeinterface 188 2 0
    //   388: aload_0
    //   389: getfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   392: ifnonnull +10 -> 402
    //   395: aload_0
    //   396: iconst_0
    //   397: newarray byte
    //   399: putfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   402: aload_0
    //   403: aload_0
    //   404: getfield 49	org/apache/http/impl/auth/NegotiateScheme:gssContext	Lorg/ietf/jgss/GSSContext;
    //   407: aload_0
    //   408: getfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   411: iconst_0
    //   412: aload_0
    //   413: getfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   416: arraylength
    //   417: invokeinterface 196 4 0
    //   422: putfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   425: aload_0
    //   426: getfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   429: ifnonnull +83 -> 512
    //   432: aload_0
    //   433: getstatic 109	org/apache/http/impl/auth/NegotiateScheme$State:FAILED	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   436: putfield 58	org/apache/http/impl/auth/NegotiateScheme:state	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   439: new 66	org/apache/http/auth/AuthenticationException
    //   442: dup
    //   443: ldc 198
    //   445: invokespecial 106	org/apache/http/auth/AuthenticationException:<init>	(Ljava/lang/String;)V
    //   448: athrow
    //   449: astore 4
    //   451: aload_0
    //   452: getstatic 109	org/apache/http/impl/auth/NegotiateScheme$State:FAILED	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   455: putfield 58	org/apache/http/impl/auth/NegotiateScheme:state	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   458: new 66	org/apache/http/auth/AuthenticationException
    //   461: dup
    //   462: aload 4
    //   464: invokevirtual 199	java/io/IOException:getMessage	()Ljava/lang/String;
    //   467: invokespecial 106	org/apache/http/auth/AuthenticationException:<init>	(Ljava/lang/String;)V
    //   470: athrow
    //   471: aload 7
    //   473: invokevirtual 202	org/apache/http/HttpHost:getHostName	()Ljava/lang/String;
    //   476: astore 8
    //   478: goto -332 -> 146
    //   481: astore 10
    //   483: aload 10
    //   485: invokevirtual 113	org/ietf/jgss/GSSException:getMajor	()I
    //   488: iconst_2
    //   489: if_icmpne +20 -> 509
    //   492: aload_0
    //   493: getfield 47	org/apache/http/impl/auth/NegotiateScheme:log	Lorg/apache/commons/logging/Log;
    //   496: ldc 204
    //   498: invokeinterface 151 2 0
    //   503: iconst_1
    //   504: istore 9
    //   506: goto -225 -> 281
    //   509: aload 10
    //   511: athrow
    //   512: aload_0
    //   513: getfield 60	org/apache/http/impl/auth/NegotiateScheme:spengoGenerator	Lorg/apache/http/impl/auth/SpnegoTokenGenerator;
    //   516: ifnull +35 -> 551
    //   519: aload_0
    //   520: getfield 51	org/apache/http/impl/auth/NegotiateScheme:negotiationOid	Lorg/ietf/jgss/Oid;
    //   523: invokevirtual 205	org/ietf/jgss/Oid:toString	()Ljava/lang/String;
    //   526: ldc 8
    //   528: invokevirtual 211	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   531: ifeq +20 -> 551
    //   534: aload_0
    //   535: aload_0
    //   536: getfield 60	org/apache/http/impl/auth/NegotiateScheme:spengoGenerator	Lorg/apache/http/impl/auth/SpnegoTokenGenerator;
    //   539: aload_0
    //   540: getfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   543: invokeinterface 217 2 0
    //   548: putfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   551: aload_0
    //   552: getstatic 220	org/apache/http/impl/auth/NegotiateScheme$State:TOKEN_GENERATED	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   555: putfield 58	org/apache/http/impl/auth/NegotiateScheme:state	Lorg/apache/http/impl/auth/NegotiateScheme$State;
    //   558: new 207	java/lang/String
    //   561: dup
    //   562: aload_0
    //   563: getfield 192	org/apache/http/impl/auth/NegotiateScheme:token	[B
    //   566: iconst_0
    //   567: invokestatic 226	org/apache/commons/codec/binary/Base64:encodeBase64	([BZ)[B
    //   570: invokespecial 229	java/lang/String:<init>	([B)V
    //   573: astore 11
    //   575: aload_0
    //   576: getfield 47	org/apache/http/impl/auth/NegotiateScheme:log	Lorg/apache/commons/logging/Log;
    //   579: invokeinterface 135 1 0
    //   584: ifeq +37 -> 621
    //   587: aload_0
    //   588: getfield 47	org/apache/http/impl/auth/NegotiateScheme:log	Lorg/apache/commons/logging/Log;
    //   591: new 137	java/lang/StringBuilder
    //   594: dup
    //   595: invokespecial 138	java/lang/StringBuilder:<init>	()V
    //   598: ldc 231
    //   600: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   603: aload 11
    //   605: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   608: ldc 233
    //   610: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   613: invokevirtual 147	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   616: invokeinterface 151 2 0
    //   621: new 235	org/apache/http/message/BasicHeader
    //   624: dup
    //   625: ldc 237
    //   627: new 137	java/lang/StringBuilder
    //   630: dup
    //   631: invokespecial 138	java/lang/StringBuilder:<init>	()V
    //   634: ldc 239
    //   636: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   639: aload 11
    //   641: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   644: invokevirtual 147	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   647: invokespecial 242	org/apache/http/message/BasicHeader:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   650: astore 12
    //   652: aload 12
    //   654: areturn
    //   655: aload 5
    //   657: invokevirtual 113	org/ietf/jgss/GSSException:getMajor	()I
    //   660: bipush 13
    //   662: if_icmpne +18 -> 680
    //   665: new 115	org/apache/http/auth/InvalidCredentialsException
    //   668: dup
    //   669: aload 5
    //   671: invokevirtual 119	org/ietf/jgss/GSSException:getMessage	()Ljava/lang/String;
    //   674: aload 5
    //   676: invokespecial 122	org/apache/http/auth/InvalidCredentialsException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   679: athrow
    //   680: aload 5
    //   682: invokevirtual 113	org/ietf/jgss/GSSException:getMajor	()I
    //   685: bipush 10
    //   687: if_icmpeq +23 -> 710
    //   690: aload 5
    //   692: invokevirtual 113	org/ietf/jgss/GSSException:getMajor	()I
    //   695: bipush 19
    //   697: if_icmpeq +13 -> 710
    //   700: aload 5
    //   702: invokevirtual 113	org/ietf/jgss/GSSException:getMajor	()I
    //   705: bipush 20
    //   707: if_icmpne +18 -> 725
    //   710: new 66	org/apache/http/auth/AuthenticationException
    //   713: dup
    //   714: aload 5
    //   716: invokevirtual 119	org/ietf/jgss/GSSException:getMessage	()Ljava/lang/String;
    //   719: aload 5
    //   721: invokespecial 243	org/apache/http/auth/AuthenticationException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   724: athrow
    //   725: new 66	org/apache/http/auth/AuthenticationException
    //   728: dup
    //   729: aload 5
    //   731: invokevirtual 119	org/ietf/jgss/GSSException:getMessage	()Ljava/lang/String;
    //   734: invokespecial 106	org/apache/http/auth/AuthenticationException:<init>	(Ljava/lang/String;)V
    //   737: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   34	41	73	org/ietf/jgss/GSSException
    //   45	58	73	org/ietf/jgss/GSSException
    //   63	73	73	org/ietf/jgss/GSSException
    //   124	146	73	org/ietf/jgss/GSSException
    //   146	187	73	org/ietf/jgss/GSSException
    //   187	200	73	org/ietf/jgss/GSSException
    //   286	388	73	org/ietf/jgss/GSSException
    //   388	402	73	org/ietf/jgss/GSSException
    //   402	449	73	org/ietf/jgss/GSSException
    //   471	478	73	org/ietf/jgss/GSSException
    //   483	503	73	org/ietf/jgss/GSSException
    //   509	512	73	org/ietf/jgss/GSSException
    //   512	551	73	org/ietf/jgss/GSSException
    //   551	621	73	org/ietf/jgss/GSSException
    //   621	652	73	org/ietf/jgss/GSSException
    //   34	41	449	java/io/IOException
    //   45	58	449	java/io/IOException
    //   63	73	449	java/io/IOException
    //   124	146	449	java/io/IOException
    //   146	187	449	java/io/IOException
    //   187	200	449	java/io/IOException
    //   203	281	449	java/io/IOException
    //   286	388	449	java/io/IOException
    //   388	402	449	java/io/IOException
    //   402	449	449	java/io/IOException
    //   471	478	449	java/io/IOException
    //   483	503	449	java/io/IOException
    //   509	512	449	java/io/IOException
    //   512	551	449	java/io/IOException
    //   551	621	449	java/io/IOException
    //   621	652	449	java/io/IOException
    //   203	281	481	org/ietf/jgss/GSSException
  }

  protected GSSManager getManager()
  {
    return GSSManager.getInstance();
  }

  public String getParameter(String paramString)
  {
    if (paramString == null)
      throw new IllegalArgumentException("Parameter name may not be null");
    return null;
  }

  public String getRealm()
  {
    return null;
  }

  public String getSchemeName()
  {
    return "Negotiate";
  }

  public boolean isComplete()
  {
    return (this.state == State.TOKEN_GENERATED) || (this.state == State.FAILED);
  }

  public boolean isConnectionBased()
  {
    return true;
  }

  protected void parseChallenge(CharArrayBuffer paramCharArrayBuffer, int paramInt1, int paramInt2)
    throws MalformedChallengeException
  {
    String str = paramCharArrayBuffer.substringTrimmed(paramInt1, paramInt2);
    if (this.log.isDebugEnabled())
      this.log.debug("Received challenge '" + str + "' from the auth server");
    if (this.state == State.UNINITIATED)
    {
      this.token = new Base64().decode(str.getBytes());
      this.state = State.CHALLENGE_RECEIVED;
      return;
    }
    this.log.debug("Authentication already attempted");
    this.state = State.FAILED;
  }

  static enum State
  {
    static
    {
      CHALLENGE_RECEIVED = new State("CHALLENGE_RECEIVED", 1);
      TOKEN_GENERATED = new State("TOKEN_GENERATED", 2);
      FAILED = new State("FAILED", 3);
      State[] arrayOfState = new State[4];
      arrayOfState[0] = UNINITIATED;
      arrayOfState[1] = CHALLENGE_RECEIVED;
      arrayOfState[2] = TOKEN_GENERATED;
      arrayOfState[3] = FAILED;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.auth.NegotiateScheme
 * JD-Core Version:    0.6.2
 */