package org.apache.http.impl.auth;

import java.io.IOException;

public abstract interface SpnegoTokenGenerator
{
  public abstract byte[] generateSpnegoDERObject(byte[] paramArrayOfByte)
    throws IOException;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.auth.SpnegoTokenGenerator
 * JD-Core Version:    0.6.2
 */