package org.apache.http.impl.auth;

import org.apache.http.annotation.Immutable;

@Immutable
public class UnsupportedDigestAlgorithmException extends RuntimeException
{
  private static final long serialVersionUID = 319558534317118022L;

  public UnsupportedDigestAlgorithmException()
  {
  }

  public UnsupportedDigestAlgorithmException(String paramString)
  {
    super(paramString);
  }

  public UnsupportedDigestAlgorithmException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.auth.UnsupportedDigestAlgorithmException
 * JD-Core Version:    0.6.2
 */