package org.apache.http.impl.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import org.apache.http.annotation.GuardedBy;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieIdentityComparator;

@ThreadSafe
public class BasicCookieStore
  implements CookieStore, Serializable
{
  private static final long serialVersionUID = -7581093305228232025L;

  @GuardedBy("this")
  private final TreeSet<Cookie> cookies = new TreeSet(new CookieIdentityComparator());

  public void addCookie(Cookie paramCookie)
  {
    if (paramCookie != null);
    try
    {
      this.cookies.remove(paramCookie);
      if (!paramCookie.isExpired(new Date()))
        this.cookies.add(paramCookie);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void addCookies(Cookie[] paramArrayOfCookie)
  {
    if (paramArrayOfCookie != null);
    try
    {
      int i = paramArrayOfCookie.length;
      for (int j = 0; j < i; j++)
        addCookie(paramArrayOfCookie[j]);
      return;
    }
    finally
    {
    }
  }

  public void clear()
  {
    try
    {
      this.cookies.clear();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public boolean clearExpired(Date paramDate)
  {
    boolean bool2;
    if (paramDate == null)
      bool2 = false;
    while (true)
    {
      return bool2;
      boolean bool1 = false;
      try
      {
        Iterator localIterator = this.cookies.iterator();
        while (localIterator.hasNext())
          if (((Cookie)localIterator.next()).isExpired(paramDate))
          {
            localIterator.remove();
            bool1 = true;
          }
        bool2 = bool1;
      }
      finally
      {
      }
    }
  }

  public List<Cookie> getCookies()
  {
    try
    {
      ArrayList localArrayList = new ArrayList(this.cookies);
      return localArrayList;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public String toString()
  {
    try
    {
      String str = this.cookies.toString();
      return str;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.client.BasicCookieStore
 * JD-Core Version:    0.6.2
 */