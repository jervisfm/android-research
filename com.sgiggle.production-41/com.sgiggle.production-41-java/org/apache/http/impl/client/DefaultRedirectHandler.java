package org.apache.http.impl.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.RequestLine;
import org.apache.http.StatusLine;
import org.apache.http.annotation.Immutable;
import org.apache.http.client.RedirectHandler;
import org.apache.http.protocol.HttpContext;

@Deprecated
@Immutable
public class DefaultRedirectHandler
  implements RedirectHandler
{
  private static final String REDIRECT_LOCATIONS = "http.protocol.redirect-locations";
  private final Log log = LogFactory.getLog(getClass());

  // ERROR //
  public java.net.URI getLocationURI(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws org.apache.http.ProtocolException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull +13 -> 14
    //   4: new 38	java/lang/IllegalArgumentException
    //   7: dup
    //   8: ldc 40
    //   10: invokespecial 43	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   13: athrow
    //   14: aload_1
    //   15: ldc 45
    //   17: invokeinterface 51 2 0
    //   22: astore_3
    //   23: aload_3
    //   24: ifnonnull +40 -> 64
    //   27: new 34	org/apache/http/ProtocolException
    //   30: dup
    //   31: new 53	java/lang/StringBuilder
    //   34: dup
    //   35: invokespecial 54	java/lang/StringBuilder:<init>	()V
    //   38: ldc 56
    //   40: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: aload_1
    //   44: invokeinterface 64 1 0
    //   49: invokevirtual 67	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   52: ldc 69
    //   54: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: invokespecial 74	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;)V
    //   63: athrow
    //   64: aload_3
    //   65: invokeinterface 79 1 0
    //   70: astore 4
    //   72: aload_0
    //   73: getfield 30	org/apache/http/impl/client/DefaultRedirectHandler:log	Lorg/apache/commons/logging/Log;
    //   76: invokeinterface 85 1 0
    //   81: ifeq +37 -> 118
    //   84: aload_0
    //   85: getfield 30	org/apache/http/impl/client/DefaultRedirectHandler:log	Lorg/apache/commons/logging/Log;
    //   88: new 53	java/lang/StringBuilder
    //   91: dup
    //   92: invokespecial 54	java/lang/StringBuilder:<init>	()V
    //   95: ldc 87
    //   97: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   100: aload 4
    //   102: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: ldc 89
    //   107: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   113: invokeinterface 93 2 0
    //   118: new 95	java/net/URI
    //   121: dup
    //   122: aload 4
    //   124: invokespecial 96	java/net/URI:<init>	(Ljava/lang/String;)V
    //   127: astore 5
    //   129: aload_1
    //   130: invokeinterface 100 1 0
    //   135: astore 6
    //   137: aload 5
    //   139: invokevirtual 103	java/net/URI:isAbsolute	()Z
    //   142: ifne +161 -> 303
    //   145: aload 6
    //   147: ldc 105
    //   149: invokeinterface 111 2 0
    //   154: ifeq +72 -> 226
    //   157: new 34	org/apache/http/ProtocolException
    //   160: dup
    //   161: new 53	java/lang/StringBuilder
    //   164: dup
    //   165: invokespecial 54	java/lang/StringBuilder:<init>	()V
    //   168: ldc 113
    //   170: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   173: aload 5
    //   175: invokevirtual 67	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   178: ldc 115
    //   180: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   186: invokespecial 74	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;)V
    //   189: athrow
    //   190: astore 18
    //   192: new 34	org/apache/http/ProtocolException
    //   195: dup
    //   196: new 53	java/lang/StringBuilder
    //   199: dup
    //   200: invokespecial 54	java/lang/StringBuilder:<init>	()V
    //   203: ldc 117
    //   205: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   208: aload 4
    //   210: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   216: aload 18
    //   218: invokespecial 120	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   221: astore 19
    //   223: aload 19
    //   225: athrow
    //   226: aload_2
    //   227: ldc 122
    //   229: invokeinterface 128 2 0
    //   234: checkcast 130	org/apache/http/HttpHost
    //   237: astore 13
    //   239: aload 13
    //   241: ifnonnull +13 -> 254
    //   244: new 132	java/lang/IllegalStateException
    //   247: dup
    //   248: ldc 134
    //   250: invokespecial 135	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   253: athrow
    //   254: aload_2
    //   255: ldc 137
    //   257: invokeinterface 128 2 0
    //   262: checkcast 139	org/apache/http/HttpRequest
    //   265: astore 14
    //   267: new 95	java/net/URI
    //   270: dup
    //   271: aload 14
    //   273: invokeinterface 143 1 0
    //   278: invokeinterface 148 1 0
    //   283: invokespecial 96	java/net/URI:<init>	(Ljava/lang/String;)V
    //   286: aload 13
    //   288: iconst_1
    //   289: invokestatic 154	org/apache/http/client/utils/URIUtils:rewriteURI	(Ljava/net/URI;Lorg/apache/http/HttpHost;Z)Ljava/net/URI;
    //   292: aload 5
    //   294: invokestatic 158	org/apache/http/client/utils/URIUtils:resolve	(Ljava/net/URI;Ljava/net/URI;)Ljava/net/URI;
    //   297: astore 17
    //   299: aload 17
    //   301: astore 5
    //   303: aload 6
    //   305: ldc 160
    //   307: invokeinterface 163 2 0
    //   312: ifeq +185 -> 497
    //   315: aload_2
    //   316: ldc 12
    //   318: invokeinterface 128 2 0
    //   323: checkcast 165	org/apache/http/impl/client/RedirectLocations
    //   326: astore 7
    //   328: aload 7
    //   330: ifnonnull +22 -> 352
    //   333: new 165	org/apache/http/impl/client/RedirectLocations
    //   336: dup
    //   337: invokespecial 166	org/apache/http/impl/client/RedirectLocations:<init>	()V
    //   340: astore 7
    //   342: aload_2
    //   343: ldc 12
    //   345: aload 7
    //   347: invokeinterface 170 3 0
    //   352: aload 5
    //   354: invokevirtual 173	java/net/URI:getFragment	()Ljava/lang/String;
    //   357: ifnull +126 -> 483
    //   360: new 130	org/apache/http/HttpHost
    //   363: dup
    //   364: aload 5
    //   366: invokevirtual 176	java/net/URI:getHost	()Ljava/lang/String;
    //   369: aload 5
    //   371: invokevirtual 180	java/net/URI:getPort	()I
    //   374: aload 5
    //   376: invokevirtual 183	java/net/URI:getScheme	()Ljava/lang/String;
    //   379: invokespecial 186	org/apache/http/HttpHost:<init>	(Ljava/lang/String;ILjava/lang/String;)V
    //   382: astore 9
    //   384: aload 5
    //   386: aload 9
    //   388: iconst_1
    //   389: invokestatic 154	org/apache/http/client/utils/URIUtils:rewriteURI	(Ljava/net/URI;Lorg/apache/http/HttpHost;Z)Ljava/net/URI;
    //   392: astore 12
    //   394: aload 12
    //   396: astore 8
    //   398: aload 7
    //   400: aload 8
    //   402: invokevirtual 190	org/apache/http/impl/client/RedirectLocations:contains	(Ljava/net/URI;)Z
    //   405: ifeq +85 -> 490
    //   408: new 192	org/apache/http/client/CircularRedirectException
    //   411: dup
    //   412: new 53	java/lang/StringBuilder
    //   415: dup
    //   416: invokespecial 54	java/lang/StringBuilder:<init>	()V
    //   419: ldc 194
    //   421: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   424: aload 8
    //   426: invokevirtual 67	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   429: ldc 89
    //   431: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   434: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   437: invokespecial 195	org/apache/http/client/CircularRedirectException:<init>	(Ljava/lang/String;)V
    //   440: athrow
    //   441: astore 15
    //   443: new 34	org/apache/http/ProtocolException
    //   446: dup
    //   447: aload 15
    //   449: invokevirtual 198	java/net/URISyntaxException:getMessage	()Ljava/lang/String;
    //   452: aload 15
    //   454: invokespecial 120	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   457: astore 16
    //   459: aload 16
    //   461: athrow
    //   462: astore 10
    //   464: new 34	org/apache/http/ProtocolException
    //   467: dup
    //   468: aload 10
    //   470: invokevirtual 198	java/net/URISyntaxException:getMessage	()Ljava/lang/String;
    //   473: aload 10
    //   475: invokespecial 120	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   478: astore 11
    //   480: aload 11
    //   482: athrow
    //   483: aload 5
    //   485: astore 8
    //   487: goto -89 -> 398
    //   490: aload 7
    //   492: aload 8
    //   494: invokevirtual 202	org/apache/http/impl/client/RedirectLocations:add	(Ljava/net/URI;)V
    //   497: aload 5
    //   499: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   118	129	190	java/net/URISyntaxException
    //   267	299	441	java/net/URISyntaxException
    //   360	394	462	java/net/URISyntaxException
  }

  public boolean isRedirectRequested(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
  {
    if (paramHttpResponse == null)
      throw new IllegalArgumentException("HTTP response may not be null");
    switch (paramHttpResponse.getStatusLine().getStatusCode())
    {
    case 304:
    case 305:
    case 306:
    default:
      return false;
    case 301:
    case 302:
    case 307:
      String str = ((HttpRequest)paramHttpContext.getAttribute("http.request")).getRequestLine().getMethod();
      return (str.equalsIgnoreCase("GET")) || (str.equalsIgnoreCase("HEAD"));
    case 303:
    }
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.client.DefaultRedirectHandler
 * JD-Core Version:    0.6.2
 */