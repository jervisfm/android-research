package org.apache.http.impl.client;

import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.RequestLine;
import org.apache.http.StatusLine;
import org.apache.http.annotation.Immutable;
import org.apache.http.client.RedirectStrategy;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

@Immutable
public class DefaultRedirectStrategy
  implements RedirectStrategy
{
  public static final String REDIRECT_LOCATIONS = "http.protocol.redirect-locations";
  private final Log log = LogFactory.getLog(getClass());

  protected URI createLocationURI(String paramString)
    throws ProtocolException
  {
    try
    {
      URI localURI = new URI(paramString);
      return localURI;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      throw new ProtocolException("Invalid redirect URI: " + paramString, localURISyntaxException);
    }
  }

  // ERROR //
  public URI getLocationURI(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    // Byte code:
    //   0: aload_2
    //   1: ifnonnull +13 -> 14
    //   4: new 60	java/lang/IllegalArgumentException
    //   7: dup
    //   8: ldc 62
    //   10: invokespecial 63	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   13: athrow
    //   14: aload_2
    //   15: ldc 65
    //   17: invokeinterface 71 2 0
    //   22: astore 4
    //   24: aload 4
    //   26: ifnonnull +40 -> 66
    //   29: new 33	org/apache/http/ProtocolException
    //   32: dup
    //   33: new 42	java/lang/StringBuilder
    //   36: dup
    //   37: invokespecial 43	java/lang/StringBuilder:<init>	()V
    //   40: ldc 73
    //   42: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: aload_2
    //   46: invokeinterface 77 1 0
    //   51: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   54: ldc 82
    //   56: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: invokevirtual 53	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   62: invokespecial 83	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;)V
    //   65: athrow
    //   66: aload 4
    //   68: invokeinterface 88 1 0
    //   73: astore 5
    //   75: aload_0
    //   76: getfield 29	org/apache/http/impl/client/DefaultRedirectStrategy:log	Lorg/apache/commons/logging/Log;
    //   79: invokeinterface 94 1 0
    //   84: ifeq +37 -> 121
    //   87: aload_0
    //   88: getfield 29	org/apache/http/impl/client/DefaultRedirectStrategy:log	Lorg/apache/commons/logging/Log;
    //   91: new 42	java/lang/StringBuilder
    //   94: dup
    //   95: invokespecial 43	java/lang/StringBuilder:<init>	()V
    //   98: ldc 96
    //   100: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: aload 5
    //   105: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   108: ldc 98
    //   110: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   113: invokevirtual 53	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   116: invokeinterface 102 2 0
    //   121: aload_0
    //   122: aload 5
    //   124: invokevirtual 104	org/apache/http/impl/client/DefaultRedirectStrategy:createLocationURI	(Ljava/lang/String;)Ljava/net/URI;
    //   127: astore 6
    //   129: aload_2
    //   130: invokeinterface 108 1 0
    //   135: astore 7
    //   137: aload 6
    //   139: invokevirtual 111	java/net/URI:isAbsolute	()Z
    //   142: ifne +111 -> 253
    //   145: aload 7
    //   147: ldc 113
    //   149: invokeinterface 119 2 0
    //   154: ifeq +36 -> 190
    //   157: new 33	org/apache/http/ProtocolException
    //   160: dup
    //   161: new 42	java/lang/StringBuilder
    //   164: dup
    //   165: invokespecial 43	java/lang/StringBuilder:<init>	()V
    //   168: ldc 121
    //   170: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   173: aload 6
    //   175: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   178: ldc 123
    //   180: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: invokevirtual 53	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   186: invokespecial 83	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;)V
    //   189: athrow
    //   190: aload_3
    //   191: ldc 125
    //   193: invokeinterface 131 2 0
    //   198: checkcast 133	org/apache/http/HttpHost
    //   201: astore 12
    //   203: aload 12
    //   205: ifnonnull +13 -> 218
    //   208: new 135	java/lang/IllegalStateException
    //   211: dup
    //   212: ldc 137
    //   214: invokespecial 138	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   217: athrow
    //   218: new 37	java/net/URI
    //   221: dup
    //   222: aload_1
    //   223: invokeinterface 144 1 0
    //   228: invokeinterface 149 1 0
    //   233: invokespecial 40	java/net/URI:<init>	(Ljava/lang/String;)V
    //   236: aload 12
    //   238: iconst_1
    //   239: invokestatic 155	org/apache/http/client/utils/URIUtils:rewriteURI	(Ljava/net/URI;Lorg/apache/http/HttpHost;Z)Ljava/net/URI;
    //   242: aload 6
    //   244: invokestatic 159	org/apache/http/client/utils/URIUtils:resolve	(Ljava/net/URI;Ljava/net/URI;)Ljava/net/URI;
    //   247: astore 14
    //   249: aload 14
    //   251: astore 6
    //   253: aload 7
    //   255: ldc 161
    //   257: invokeinterface 164 2 0
    //   262: ifeq +173 -> 435
    //   265: aload_3
    //   266: ldc 11
    //   268: invokeinterface 131 2 0
    //   273: checkcast 166	org/apache/http/impl/client/RedirectLocations
    //   276: astore 8
    //   278: aload 8
    //   280: ifnonnull +22 -> 302
    //   283: new 166	org/apache/http/impl/client/RedirectLocations
    //   286: dup
    //   287: invokespecial 167	org/apache/http/impl/client/RedirectLocations:<init>	()V
    //   290: astore 8
    //   292: aload_3
    //   293: ldc 11
    //   295: aload 8
    //   297: invokeinterface 171 3 0
    //   302: aload 6
    //   304: invokevirtual 174	java/net/URI:getFragment	()Ljava/lang/String;
    //   307: ifnull +114 -> 421
    //   310: aload 6
    //   312: new 133	org/apache/http/HttpHost
    //   315: dup
    //   316: aload 6
    //   318: invokevirtual 177	java/net/URI:getHost	()Ljava/lang/String;
    //   321: aload 6
    //   323: invokevirtual 181	java/net/URI:getPort	()I
    //   326: aload 6
    //   328: invokevirtual 184	java/net/URI:getScheme	()Ljava/lang/String;
    //   331: invokespecial 187	org/apache/http/HttpHost:<init>	(Ljava/lang/String;ILjava/lang/String;)V
    //   334: iconst_1
    //   335: invokestatic 155	org/apache/http/client/utils/URIUtils:rewriteURI	(Ljava/net/URI;Lorg/apache/http/HttpHost;Z)Ljava/net/URI;
    //   338: astore 11
    //   340: aload 11
    //   342: astore 9
    //   344: aload 8
    //   346: aload 9
    //   348: invokevirtual 191	org/apache/http/impl/client/RedirectLocations:contains	(Ljava/net/URI;)Z
    //   351: ifeq +77 -> 428
    //   354: new 193	org/apache/http/client/CircularRedirectException
    //   357: dup
    //   358: new 42	java/lang/StringBuilder
    //   361: dup
    //   362: invokespecial 43	java/lang/StringBuilder:<init>	()V
    //   365: ldc 195
    //   367: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   370: aload 9
    //   372: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   375: ldc 98
    //   377: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   380: invokevirtual 53	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   383: invokespecial 196	org/apache/http/client/CircularRedirectException:<init>	(Ljava/lang/String;)V
    //   386: athrow
    //   387: astore 13
    //   389: new 33	org/apache/http/ProtocolException
    //   392: dup
    //   393: aload 13
    //   395: invokevirtual 199	java/net/URISyntaxException:getMessage	()Ljava/lang/String;
    //   398: aload 13
    //   400: invokespecial 56	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   403: athrow
    //   404: astore 10
    //   406: new 33	org/apache/http/ProtocolException
    //   409: dup
    //   410: aload 10
    //   412: invokevirtual 199	java/net/URISyntaxException:getMessage	()Ljava/lang/String;
    //   415: aload 10
    //   417: invokespecial 56	org/apache/http/ProtocolException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   420: athrow
    //   421: aload 6
    //   423: astore 9
    //   425: goto -81 -> 344
    //   428: aload 8
    //   430: aload 9
    //   432: invokevirtual 203	org/apache/http/impl/client/RedirectLocations:add	(Ljava/net/URI;)V
    //   435: aload 6
    //   437: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   218	249	387	java/net/URISyntaxException
    //   310	340	404	java/net/URISyntaxException
  }

  public HttpUriRequest getRedirect(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    URI localURI = getLocationURI(paramHttpRequest, paramHttpResponse, paramHttpContext);
    if (paramHttpRequest.getRequestLine().getMethod().equalsIgnoreCase("HEAD"))
      return new HttpHead(localURI);
    return new HttpGet(localURI);
  }

  public boolean isRedirected(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    if (paramHttpResponse == null)
      throw new IllegalArgumentException("HTTP response may not be null");
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    String str = paramHttpRequest.getRequestLine().getMethod();
    Header localHeader = paramHttpResponse.getFirstHeader("location");
    switch (i)
    {
    case 304:
    case 305:
    case 306:
    default:
      return false;
    case 302:
      return ((str.equalsIgnoreCase("GET")) || (str.equalsIgnoreCase("HEAD"))) && (localHeader != null);
    case 301:
    case 307:
      return (str.equalsIgnoreCase("GET")) || (str.equalsIgnoreCase("HEAD"));
    case 303:
    }
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.client.DefaultRedirectStrategy
 * JD-Core Version:    0.6.2
 */