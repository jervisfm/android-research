package org.apache.http.impl.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.RequestLine;
import org.apache.http.StatusLine;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.client.AuthenticationHandler;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.NonRepeatableRequestException;
import org.apache.http.client.RedirectException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.RedirectStrategy;
import org.apache.http.client.RequestDirector;
import org.apache.http.client.UserTokenHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.ManagedClientConnection;
import org.apache.http.conn.routing.BasicRouteDirector;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRouteDirector;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.util.EntityUtils;

@NotThreadSafe
public class DefaultRequestDirector
  implements RequestDirector
{
  protected final ClientConnectionManager connManager;
  private int execCount;
  protected final HttpProcessor httpProcessor;
  protected final ConnectionKeepAliveStrategy keepAliveStrategy;
  private final Log log;
  protected ManagedClientConnection managedConn;
  private int maxRedirects;
  protected final HttpParams params;
  protected final AuthenticationHandler proxyAuthHandler;
  protected final AuthState proxyAuthState;
  private int redirectCount;

  @Deprecated
  protected final RedirectHandler redirectHandler = null;
  protected final RedirectStrategy redirectStrategy;
  protected final HttpRequestExecutor requestExec;
  protected final HttpRequestRetryHandler retryHandler;
  protected final ConnectionReuseStrategy reuseStrategy;
  protected final HttpRoutePlanner routePlanner;
  protected final AuthenticationHandler targetAuthHandler;
  protected final AuthState targetAuthState;
  protected final UserTokenHandler userTokenHandler;
  private HttpHost virtualHost;

  public DefaultRequestDirector(Log paramLog, HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    if (paramLog == null)
      throw new IllegalArgumentException("Log may not be null.");
    if (paramHttpRequestExecutor == null)
      throw new IllegalArgumentException("Request executor may not be null.");
    if (paramClientConnectionManager == null)
      throw new IllegalArgumentException("Client connection manager may not be null.");
    if (paramConnectionReuseStrategy == null)
      throw new IllegalArgumentException("Connection reuse strategy may not be null.");
    if (paramConnectionKeepAliveStrategy == null)
      throw new IllegalArgumentException("Connection keep alive strategy may not be null.");
    if (paramHttpRoutePlanner == null)
      throw new IllegalArgumentException("Route planner may not be null.");
    if (paramHttpProcessor == null)
      throw new IllegalArgumentException("HTTP protocol processor may not be null.");
    if (paramHttpRequestRetryHandler == null)
      throw new IllegalArgumentException("HTTP request retry handler may not be null.");
    if (paramRedirectStrategy == null)
      throw new IllegalArgumentException("Redirect strategy may not be null.");
    if (paramAuthenticationHandler1 == null)
      throw new IllegalArgumentException("Target authentication handler may not be null.");
    if (paramAuthenticationHandler2 == null)
      throw new IllegalArgumentException("Proxy authentication handler may not be null.");
    if (paramUserTokenHandler == null)
      throw new IllegalArgumentException("User token handler may not be null.");
    if (paramHttpParams == null)
      throw new IllegalArgumentException("HTTP parameters may not be null");
    this.log = paramLog;
    this.requestExec = paramHttpRequestExecutor;
    this.connManager = paramClientConnectionManager;
    this.reuseStrategy = paramConnectionReuseStrategy;
    this.keepAliveStrategy = paramConnectionKeepAliveStrategy;
    this.routePlanner = paramHttpRoutePlanner;
    this.httpProcessor = paramHttpProcessor;
    this.retryHandler = paramHttpRequestRetryHandler;
    this.redirectStrategy = paramRedirectStrategy;
    this.targetAuthHandler = paramAuthenticationHandler1;
    this.proxyAuthHandler = paramAuthenticationHandler2;
    this.userTokenHandler = paramUserTokenHandler;
    this.params = paramHttpParams;
    this.managedConn = null;
    this.execCount = 0;
    this.redirectCount = 0;
    this.maxRedirects = this.params.getIntParameter("http.protocol.max-redirects", 100);
    this.targetAuthState = new AuthState();
    this.proxyAuthState = new AuthState();
  }

  @Deprecated
  public DefaultRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectHandler paramRedirectHandler, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    this(localLog, paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, localDefaultRedirectStrategyAdaptor, paramAuthenticationHandler1, paramAuthenticationHandler2, paramUserTokenHandler, paramHttpParams);
  }

  private void abortConnection()
  {
    ManagedClientConnection localManagedClientConnection = this.managedConn;
    if (localManagedClientConnection != null)
      this.managedConn = null;
    try
    {
      localManagedClientConnection.abortConnection();
    }
    catch (IOException localIOException1)
    {
      try
      {
        while (true)
        {
          localManagedClientConnection.releaseConnection();
          return;
          localIOException1 = localIOException1;
          if (this.log.isDebugEnabled())
            this.log.debug(localIOException1.getMessage(), localIOException1);
        }
      }
      catch (IOException localIOException2)
      {
        this.log.debug("Error releasing connection", localIOException2);
      }
    }
  }

  private void invalidateAuthIfSuccessful(AuthState paramAuthState)
  {
    AuthScheme localAuthScheme = paramAuthState.getAuthScheme();
    if ((localAuthScheme != null) && (localAuthScheme.isConnectionBased()) && (localAuthScheme.isComplete()) && (paramAuthState.getCredentials() != null))
      paramAuthState.invalidate();
  }

  private void processChallenges(Map<String, Header> paramMap, AuthState paramAuthState, AuthenticationHandler paramAuthenticationHandler, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws MalformedChallengeException, AuthenticationException
  {
    AuthScheme localAuthScheme = paramAuthState.getAuthScheme();
    if (localAuthScheme == null)
    {
      localAuthScheme = paramAuthenticationHandler.selectScheme(paramMap, paramHttpResponse, paramHttpContext);
      paramAuthState.setAuthScheme(localAuthScheme);
    }
    String str = localAuthScheme.getSchemeName();
    Header localHeader = (Header)paramMap.get(str.toLowerCase(Locale.ENGLISH));
    if (localHeader == null)
      throw new AuthenticationException(str + " authorization challenge expected, but not found");
    localAuthScheme.processChallenge(localHeader);
    this.log.debug("Authorization challenge processed");
  }

  private void tryConnect(RoutedRequest paramRoutedRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpRoute localHttpRoute = paramRoutedRequest.getRoute();
    int i = 0;
    while (true)
    {
      i++;
      try
      {
        if (!this.managedConn.isOpen())
          this.managedConn.open(localHttpRoute, paramHttpContext, this.params);
        while (true)
        {
          establishRoute(localHttpRoute, paramHttpContext);
          return;
          this.managedConn.setSocketTimeout(HttpConnectionParams.getSoTimeout(this.params));
        }
      }
      catch (IOException localIOException1)
      {
      }
      try
      {
        this.managedConn.close();
        label75: if (this.retryHandler.retryRequest(localIOException1, i, paramHttpContext))
        {
          if (this.log.isInfoEnabled())
            this.log.info("I/O exception (" + localIOException1.getClass().getName() + ") caught when connecting to the target host: " + localIOException1.getMessage());
          if (this.log.isDebugEnabled())
            this.log.debug(localIOException1.getMessage(), localIOException1);
          this.log.info("Retrying connect");
          continue;
        }
        throw localIOException1;
      }
      catch (IOException localIOException2)
      {
        break label75;
      }
    }
  }

  private HttpResponse tryExecute(RoutedRequest paramRoutedRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    RequestWrapper localRequestWrapper = paramRoutedRequest.getRequest();
    HttpRoute localHttpRoute = paramRoutedRequest.getRoute();
    Object localObject = null;
    while (true)
    {
      this.execCount = (1 + this.execCount);
      localRequestWrapper.incrementExecCount();
      if (!localRequestWrapper.isRepeatable())
      {
        this.log.debug("Cannot retry non-repeatable request");
        if (localObject != null)
          throw new NonRepeatableRequestException("Cannot retry request with a non-repeatable request entity.  The cause lists the reason the original request failed.", (Throwable)localObject);
        throw new NonRepeatableRequestException("Cannot retry request with a non-repeatable request entity.");
      }
      try
      {
        if (!this.managedConn.isOpen())
        {
          if (!localHttpRoute.isTunnelled())
          {
            this.log.debug("Reopening the direct connection.");
            this.managedConn.open(localHttpRoute, paramHttpContext, this.params);
          }
        }
        else
        {
          if (this.log.isDebugEnabled())
            this.log.debug("Attempt " + this.execCount + " to execute request");
          return this.requestExec.execute(localRequestWrapper, this.managedConn, paramHttpContext);
        }
        this.log.debug("Proxied connection. Need to start over.");
        return null;
      }
      catch (IOException localIOException1)
      {
        this.log.debug("Closing the connection.");
      }
      try
      {
        this.managedConn.close();
        label225: if (this.retryHandler.retryRequest(localIOException1, localRequestWrapper.getExecCount(), paramHttpContext))
        {
          if (this.log.isInfoEnabled())
            this.log.info("I/O exception (" + localIOException1.getClass().getName() + ") caught when processing request: " + localIOException1.getMessage());
          if (this.log.isDebugEnabled())
            this.log.debug(localIOException1.getMessage(), localIOException1);
          this.log.info("Retrying request");
          localObject = localIOException1;
          continue;
        }
        throw localIOException1;
      }
      catch (IOException localIOException2)
      {
        break label225;
      }
    }
  }

  private void updateAuthState(AuthState paramAuthState, HttpHost paramHttpHost, CredentialsProvider paramCredentialsProvider)
  {
    if (!paramAuthState.isValid())
      return;
    String str = paramHttpHost.getHostName();
    int i = paramHttpHost.getPort();
    if (i < 0)
      i = this.connManager.getSchemeRegistry().getScheme(paramHttpHost).getDefaultPort();
    AuthScheme localAuthScheme = paramAuthState.getAuthScheme();
    AuthScope localAuthScope = new AuthScope(str, i, localAuthScheme.getRealm(), localAuthScheme.getSchemeName());
    if (this.log.isDebugEnabled())
      this.log.debug("Authentication scope: " + localAuthScope);
    Credentials localCredentials = paramAuthState.getCredentials();
    if (localCredentials == null)
    {
      localCredentials = paramCredentialsProvider.getCredentials(localAuthScope);
      if (this.log.isDebugEnabled())
      {
        if (localCredentials == null)
          break label181;
        this.log.debug("Found credentials");
      }
    }
    while (true)
    {
      paramAuthState.setAuthScope(localAuthScope);
      paramAuthState.setCredentials(localCredentials);
      return;
      label181: this.log.debug("Credentials not found");
      continue;
      if (localAuthScheme.isComplete())
      {
        this.log.debug("Authentication failed");
        localCredentials = null;
      }
    }
  }

  private RequestWrapper wrapRequest(HttpRequest paramHttpRequest)
    throws ProtocolException
  {
    if ((paramHttpRequest instanceof HttpEntityEnclosingRequest))
      return new EntityEnclosingRequestWrapper((HttpEntityEnclosingRequest)paramHttpRequest);
    return new RequestWrapper(paramHttpRequest);
  }

  protected HttpRequest createConnectRequest(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
  {
    HttpHost localHttpHost = paramHttpRoute.getTargetHost();
    String str = localHttpHost.getHostName();
    int i = localHttpHost.getPort();
    if (i < 0)
      i = this.connManager.getSchemeRegistry().getScheme(localHttpHost.getSchemeName()).getDefaultPort();
    StringBuilder localStringBuilder = new StringBuilder(6 + str.length());
    localStringBuilder.append(str);
    localStringBuilder.append(':');
    localStringBuilder.append(Integer.toString(i));
    return new BasicHttpRequest("CONNECT", localStringBuilder.toString(), HttpProtocolParams.getVersion(this.params));
  }

  protected boolean createTunnelToProxy(HttpRoute paramHttpRoute, int paramInt, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    throw new HttpException("Proxy chains are not supported.");
  }

  protected boolean createTunnelToTarget(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpHost localHttpHost1 = paramHttpRoute.getProxyHost();
    HttpHost localHttpHost2 = paramHttpRoute.getTargetHost();
    HttpResponse localHttpResponse = null;
    int i = 0;
    while (true)
      if (i == 0)
      {
        i = 1;
        if (!this.managedConn.isOpen())
          this.managedConn.open(paramHttpRoute, paramHttpContext, this.params);
        HttpRequest localHttpRequest = createConnectRequest(paramHttpRoute, paramHttpContext);
        localHttpRequest.setParams(this.params);
        paramHttpContext.setAttribute("http.target_host", localHttpHost2);
        paramHttpContext.setAttribute("http.proxy_host", localHttpHost1);
        paramHttpContext.setAttribute("http.connection", this.managedConn);
        paramHttpContext.setAttribute("http.auth.target-scope", this.targetAuthState);
        paramHttpContext.setAttribute("http.auth.proxy-scope", this.proxyAuthState);
        paramHttpContext.setAttribute("http.request", localHttpRequest);
        this.requestExec.preProcess(localHttpRequest, this.httpProcessor, paramHttpContext);
        localHttpResponse = this.requestExec.execute(localHttpRequest, this.managedConn, paramHttpContext);
        localHttpResponse.setParams(this.params);
        HttpRequestExecutor localHttpRequestExecutor = this.requestExec;
        HttpProcessor localHttpProcessor = this.httpProcessor;
        localHttpRequestExecutor.postProcess(localHttpResponse, localHttpProcessor, paramHttpContext);
        if (localHttpResponse.getStatusLine().getStatusCode() < 200)
          throw new HttpException("Unexpected response to CONNECT request: " + localHttpResponse.getStatusLine());
        CredentialsProvider localCredentialsProvider = (CredentialsProvider)paramHttpContext.getAttribute("http.auth.credentials-provider");
        if ((localCredentialsProvider != null) && (HttpClientParams.isAuthenticating(this.params)))
          if (this.proxyAuthHandler.isAuthenticationRequested(localHttpResponse, paramHttpContext))
          {
            this.log.debug("Proxy requested authentication");
            Map localMap = this.proxyAuthHandler.getChallenges(localHttpResponse, paramHttpContext);
            try
            {
              processChallenges(localMap, this.proxyAuthState, this.proxyAuthHandler, localHttpResponse, paramHttpContext);
              updateAuthState(this.proxyAuthState, localHttpHost1, localCredentialsProvider);
              if (this.proxyAuthState.getCredentials() == null)
                continue;
              if (!this.reuseStrategy.keepAlive(localHttpResponse, paramHttpContext))
                break label548;
              this.log.debug("Connection kept alive");
              EntityUtils.consume(localHttpResponse.getEntity());
              i = 0;
            }
            catch (AuthenticationException localAuthenticationException)
            {
              while (!this.log.isWarnEnabled());
              this.log.warn("Authentication error: " + localAuthenticationException.getMessage());
            }
          }
      }
      else
      {
        if (localHttpResponse.getStatusLine().getStatusCode() <= 299)
          break;
        HttpEntity localHttpEntity = localHttpResponse.getEntity();
        if (localHttpEntity != null)
          localHttpResponse.setEntity(new BufferedHttpEntity(localHttpEntity));
        this.managedConn.close();
        throw new TunnelRefusedException("CONNECT refused by proxy: " + localHttpResponse.getStatusLine(), localHttpResponse);
        label548: this.managedConn.close();
        i = 0;
        continue;
        this.proxyAuthState.setAuthScope(null);
      }
    this.managedConn.markReusable();
    return false;
  }

  protected HttpRoute determineRoute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    if (paramHttpHost == null)
      paramHttpHost = (HttpHost)paramHttpRequest.getParams().getParameter("http.default-host");
    if (paramHttpHost == null)
      throw new IllegalStateException("Target host must not be null, or set in parameters.");
    return this.routePlanner.determineRoute(paramHttpHost, paramHttpRequest, paramHttpContext);
  }

  protected void establishRoute(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    BasicRouteDirector localBasicRouteDirector = new BasicRouteDirector();
    HttpRoute localHttpRoute = this.managedConn.getRoute();
    int i = localBasicRouteDirector.nextStep(paramHttpRoute, localHttpRoute);
    switch (i)
    {
    default:
      throw new IllegalStateException("Unknown step indicator " + i + " from RouteDirector.");
    case 1:
    case 2:
      this.managedConn.open(paramHttpRoute, paramHttpContext, this.params);
    case 0:
    case 3:
    case 4:
    case 5:
      while (i <= 0)
      {
        return;
        boolean bool2 = createTunnelToTarget(paramHttpRoute, paramHttpContext);
        this.log.debug("Tunnel to target created.");
        this.managedConn.tunnelTarget(bool2, this.params);
        continue;
        int j = localHttpRoute.getHopCount() - 1;
        boolean bool1 = createTunnelToProxy(paramHttpRoute, j, paramHttpContext);
        this.log.debug("Tunnel to proxy created.");
        this.managedConn.tunnelProxy(paramHttpRoute.getHopTarget(j), bool1, this.params);
        continue;
        this.managedConn.layerProtocol(paramHttpContext, this.params);
      }
    case -1:
    }
    throw new HttpException("Unable to establish route: planned = " + paramHttpRoute + "; current = " + localHttpRoute);
  }

  // ERROR //
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_2
    //   2: invokespecial 687	org/apache/http/impl/client/DefaultRequestDirector:wrapRequest	(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    //   5: astore 4
    //   7: aload 4
    //   9: aload_0
    //   10: getfield 110	org/apache/http/impl/client/DefaultRequestDirector:params	Lorg/apache/http/params/HttpParams;
    //   13: invokevirtual 688	org/apache/http/impl/client/RequestWrapper:setParams	(Lorg/apache/http/params/HttpParams;)V
    //   16: aload_0
    //   17: aload_1
    //   18: aload 4
    //   20: aload_3
    //   21: invokevirtual 689	org/apache/http/impl/client/DefaultRequestDirector:determineRoute	(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/conn/routing/HttpRoute;
    //   24: astore 5
    //   26: aload_0
    //   27: aload_2
    //   28: invokeinterface 619 1 0
    //   33: ldc_w 691
    //   36: invokeinterface 624 2 0
    //   41: checkcast 381	org/apache/http/HttpHost
    //   44: putfield 693	org/apache/http/impl/client/DefaultRequestDirector:virtualHost	Lorg/apache/http/HttpHost;
    //   47: aload_0
    //   48: getfield 693	org/apache/http/impl/client/DefaultRequestDirector:virtualHost	Lorg/apache/http/HttpHost;
    //   51: ifnull +57 -> 108
    //   54: aload_0
    //   55: getfield 693	org/apache/http/impl/client/DefaultRequestDirector:virtualHost	Lorg/apache/http/HttpHost;
    //   58: invokevirtual 387	org/apache/http/HttpHost:getPort	()I
    //   61: iconst_m1
    //   62: if_icmpne +46 -> 108
    //   65: aload_1
    //   66: invokevirtual 387	org/apache/http/HttpHost:getPort	()I
    //   69: istore 38
    //   71: iload 38
    //   73: iconst_m1
    //   74: if_icmpeq +34 -> 108
    //   77: new 381	org/apache/http/HttpHost
    //   80: dup
    //   81: aload_0
    //   82: getfield 693	org/apache/http/impl/client/DefaultRequestDirector:virtualHost	Lorg/apache/http/HttpHost;
    //   85: invokevirtual 384	org/apache/http/HttpHost:getHostName	()Ljava/lang/String;
    //   88: iload 38
    //   90: aload_0
    //   91: getfield 693	org/apache/http/impl/client/DefaultRequestDirector:virtualHost	Lorg/apache/http/HttpHost;
    //   94: invokevirtual 457	org/apache/http/HttpHost:getSchemeName	()Ljava/lang/String;
    //   97: invokespecial 696	org/apache/http/HttpHost:<init>	(Ljava/lang/String;ILjava/lang/String;)V
    //   100: astore 39
    //   102: aload_0
    //   103: aload 39
    //   105: putfield 693	org/apache/http/impl/client/DefaultRequestDirector:virtualHost	Lorg/apache/http/HttpHost;
    //   108: new 261	org/apache/http/impl/client/RoutedRequest
    //   111: dup
    //   112: aload 4
    //   114: aload 5
    //   116: invokespecial 699	org/apache/http/impl/client/RoutedRequest:<init>	(Lorg/apache/http/impl/client/RequestWrapper;Lorg/apache/http/conn/routing/HttpRoute;)V
    //   119: astore 6
    //   121: iconst_0
    //   122: istore 7
    //   124: iconst_0
    //   125: istore 8
    //   127: aconst_null
    //   128: astore 9
    //   130: iload 8
    //   132: ifne +646 -> 778
    //   135: aload 6
    //   137: invokevirtual 322	org/apache/http/impl/client/RoutedRequest:getRequest	()Lorg/apache/http/impl/client/RequestWrapper;
    //   140: astore 18
    //   142: aload 6
    //   144: invokevirtual 265	org/apache/http/impl/client/RoutedRequest:getRoute	()Lorg/apache/http/conn/routing/HttpRoute;
    //   147: astore 19
    //   149: aload_3
    //   150: ldc_w 701
    //   153: invokeinterface 550 2 0
    //   158: astore 20
    //   160: aload_0
    //   161: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   164: ifnonnull +128 -> 292
    //   167: aload_0
    //   168: getfield 90	org/apache/http/impl/client/DefaultRequestDirector:connManager	Lorg/apache/http/conn/ClientConnectionManager;
    //   171: aload 19
    //   173: aload 20
    //   175: invokeinterface 705 3 0
    //   180: astore 32
    //   182: aload_2
    //   183: instanceof 707
    //   186: ifeq +14 -> 200
    //   189: aload_2
    //   190: checkcast 707	org/apache/http/client/methods/AbortableHttpRequest
    //   193: aload 32
    //   195: invokeinterface 711 2 0
    //   200: aload_0
    //   201: getfield 110	org/apache/http/impl/client/DefaultRequestDirector:params	Lorg/apache/http/params/HttpParams;
    //   204: invokestatic 717	org/apache/http/conn/params/ConnManagerParams:getTimeout	(Lorg/apache/http/params/HttpParams;)J
    //   207: lstore 33
    //   209: aload_0
    //   210: aload 32
    //   212: lload 33
    //   214: getstatic 723	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   217: invokeinterface 729 4 0
    //   222: putfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   225: aload_0
    //   226: getfield 110	org/apache/http/impl/client/DefaultRequestDirector:params	Lorg/apache/http/params/HttpParams;
    //   229: invokestatic 732	org/apache/http/params/HttpConnectionParams:isStaleCheckingEnabled	(Lorg/apache/http/params/HttpParams;)Z
    //   232: ifeq +60 -> 292
    //   235: aload_0
    //   236: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   239: invokeinterface 268 1 0
    //   244: ifeq +48 -> 292
    //   247: aload_0
    //   248: getfield 86	org/apache/http/impl/client/DefaultRequestDirector:log	Lorg/apache/commons/logging/Log;
    //   251: ldc_w 734
    //   254: invokeinterface 255 2 0
    //   259: aload_0
    //   260: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   263: invokeinterface 737 1 0
    //   268: ifeq +24 -> 292
    //   271: aload_0
    //   272: getfield 86	org/apache/http/impl/client/DefaultRequestDirector:log	Lorg/apache/commons/logging/Log;
    //   275: ldc_w 739
    //   278: invokeinterface 255 2 0
    //   283: aload_0
    //   284: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   287: invokeinterface 289 1 0
    //   292: aload_2
    //   293: instanceof 707
    //   296: ifeq +16 -> 312
    //   299: aload_2
    //   300: checkcast 707	org/apache/http/client/methods/AbortableHttpRequest
    //   303: aload_0
    //   304: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   307: invokeinterface 743 2 0
    //   312: aload_0
    //   313: aload 6
    //   315: aload_3
    //   316: invokespecial 745	org/apache/http/impl/client/DefaultRequestDirector:tryConnect	(Lorg/apache/http/impl/client/RoutedRequest;Lorg/apache/http/protocol/HttpContext;)V
    //   319: aload 18
    //   321: invokevirtual 748	org/apache/http/impl/client/RequestWrapper:resetHeaders	()V
    //   324: aload_0
    //   325: aload 18
    //   327: aload 19
    //   329: invokevirtual 751	org/apache/http/impl/client/DefaultRequestDirector:rewriteRequestURI	(Lorg/apache/http/impl/client/RequestWrapper;Lorg/apache/http/conn/routing/HttpRoute;)V
    //   332: aload_0
    //   333: getfield 693	org/apache/http/impl/client/DefaultRequestDirector:virtualHost	Lorg/apache/http/HttpHost;
    //   336: astore 22
    //   338: aload 22
    //   340: ifnonnull +10 -> 350
    //   343: aload 19
    //   345: invokevirtual 456	org/apache/http/conn/routing/HttpRoute:getTargetHost	()Lorg/apache/http/HttpHost;
    //   348: astore 22
    //   350: aload 19
    //   352: invokevirtual 496	org/apache/http/conn/routing/HttpRoute:getProxyHost	()Lorg/apache/http/HttpHost;
    //   355: astore 23
    //   357: aload_3
    //   358: ldc_w 506
    //   361: aload 22
    //   363: invokeinterface 512 3 0
    //   368: aload_3
    //   369: ldc_w 514
    //   372: aload 23
    //   374: invokeinterface 512 3 0
    //   379: aload_3
    //   380: ldc_w 516
    //   383: aload_0
    //   384: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   387: invokeinterface 512 3 0
    //   392: aload_3
    //   393: ldc_w 518
    //   396: aload_0
    //   397: getfield 131	org/apache/http/impl/client/DefaultRequestDirector:targetAuthState	Lorg/apache/http/auth/AuthState;
    //   400: invokeinterface 512 3 0
    //   405: aload_3
    //   406: ldc_w 520
    //   409: aload_0
    //   410: getfield 133	org/apache/http/impl/client/DefaultRequestDirector:proxyAuthState	Lorg/apache/http/auth/AuthState;
    //   413: invokeinterface 512 3 0
    //   418: aload_0
    //   419: getfield 88	org/apache/http/impl/client/DefaultRequestDirector:requestExec	Lorg/apache/http/protocol/HttpRequestExecutor;
    //   422: aload 18
    //   424: aload_0
    //   425: getfield 98	org/apache/http/impl/client/DefaultRequestDirector:httpProcessor	Lorg/apache/http/protocol/HttpProcessor;
    //   428: aload_3
    //   429: invokevirtual 526	org/apache/http/protocol/HttpRequestExecutor:preProcess	(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/protocol/HttpContext;)V
    //   432: aload_0
    //   433: aload 6
    //   435: aload_3
    //   436: invokespecial 753	org/apache/http/impl/client/DefaultRequestDirector:tryExecute	(Lorg/apache/http/impl/client/RoutedRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    //   439: astore 9
    //   441: aload 9
    //   443: ifnull -313 -> 130
    //   446: aload_0
    //   447: getfield 110	org/apache/http/impl/client/DefaultRequestDirector:params	Lorg/apache/http/params/HttpParams;
    //   450: astore 24
    //   452: aload 9
    //   454: aload 24
    //   456: invokeinterface 529 2 0
    //   461: aload_0
    //   462: getfield 88	org/apache/http/impl/client/DefaultRequestDirector:requestExec	Lorg/apache/http/protocol/HttpRequestExecutor;
    //   465: astore 25
    //   467: aload_0
    //   468: getfield 98	org/apache/http/impl/client/DefaultRequestDirector:httpProcessor	Lorg/apache/http/protocol/HttpProcessor;
    //   471: astore 26
    //   473: aload 25
    //   475: aload 9
    //   477: aload 26
    //   479: aload_3
    //   480: invokevirtual 533	org/apache/http/protocol/HttpRequestExecutor:postProcess	(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/protocol/HttpContext;)V
    //   483: aload_0
    //   484: getfield 92	org/apache/http/impl/client/DefaultRequestDirector:reuseStrategy	Lorg/apache/http/ConnectionReuseStrategy;
    //   487: aload 9
    //   489: aload_3
    //   490: invokeinterface 575 3 0
    //   495: istore 7
    //   497: iload 7
    //   499: ifeq +115 -> 614
    //   502: aload_0
    //   503: getfield 94	org/apache/http/impl/client/DefaultRequestDirector:keepAliveStrategy	Lorg/apache/http/conn/ConnectionKeepAliveStrategy;
    //   506: aload 9
    //   508: aload_3
    //   509: invokeinterface 759 3 0
    //   514: lstore 27
    //   516: aload_0
    //   517: getfield 86	org/apache/http/impl/client/DefaultRequestDirector:log	Lorg/apache/commons/logging/Log;
    //   520: invokeinterface 163 1 0
    //   525: ifeq +75 -> 600
    //   528: lload 27
    //   530: lconst_0
    //   531: lcmp
    //   532: ifle +428 -> 960
    //   535: new 235	java/lang/StringBuilder
    //   538: dup
    //   539: invokespecial 236	java/lang/StringBuilder:<init>	()V
    //   542: ldc_w 761
    //   545: invokevirtual 240	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   548: lload 27
    //   550: invokevirtual 764	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   553: ldc_w 766
    //   556: invokevirtual 240	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   559: getstatic 723	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   562: invokevirtual 417	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   565: invokevirtual 245	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   568: astore 29
    //   570: aload_0
    //   571: getfield 86	org/apache/http/impl/client/DefaultRequestDirector:log	Lorg/apache/commons/logging/Log;
    //   574: new 235	java/lang/StringBuilder
    //   577: dup
    //   578: invokespecial 236	java/lang/StringBuilder:<init>	()V
    //   581: ldc_w 768
    //   584: invokevirtual 240	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   587: aload 29
    //   589: invokevirtual 240	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   592: invokevirtual 245	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   595: invokeinterface 255 2 0
    //   600: aload_0
    //   601: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   604: lload 27
    //   606: getstatic 723	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   609: invokeinterface 772 4 0
    //   614: aload_0
    //   615: aload 6
    //   617: aload 9
    //   619: aload_3
    //   620: invokevirtual 776	org/apache/http/impl/client/DefaultRequestDirector:handleResponse	(Lorg/apache/http/impl/client/RoutedRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/impl/client/RoutedRequest;
    //   623: astore 30
    //   625: aload 30
    //   627: ifnonnull +202 -> 829
    //   630: iconst_1
    //   631: istore 8
    //   633: aload_0
    //   634: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   637: ifnull -507 -> 130
    //   640: aload 20
    //   642: ifnonnull -512 -> 130
    //   645: aload_0
    //   646: getfield 108	org/apache/http/impl/client/DefaultRequestDirector:userTokenHandler	Lorg/apache/http/client/UserTokenHandler;
    //   649: aload_3
    //   650: invokeinterface 782 2 0
    //   655: astore 31
    //   657: aload_3
    //   658: ldc_w 701
    //   661: aload 31
    //   663: invokeinterface 512 3 0
    //   668: aload 31
    //   670: ifnull -540 -> 130
    //   673: aload_0
    //   674: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   677: aload 31
    //   679: invokeinterface 785 2 0
    //   684: goto -554 -> 130
    //   687: astore 13
    //   689: new 787	java/io/InterruptedIOException
    //   692: dup
    //   693: ldc_w 789
    //   696: invokespecial 790	java/io/InterruptedIOException:<init>	(Ljava/lang/String;)V
    //   699: astore 14
    //   701: aload 14
    //   703: aload 13
    //   705: invokevirtual 794	java/io/InterruptedIOException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   708: pop
    //   709: aload 14
    //   711: athrow
    //   712: astore 35
    //   714: new 787	java/io/InterruptedIOException
    //   717: dup
    //   718: invokespecial 795	java/io/InterruptedIOException:<init>	()V
    //   721: astore 36
    //   723: aload 36
    //   725: aload 35
    //   727: invokevirtual 794	java/io/InterruptedIOException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   730: pop
    //   731: aload 36
    //   733: athrow
    //   734: astore 12
    //   736: aload_0
    //   737: invokespecial 796	org/apache/http/impl/client/DefaultRequestDirector:abortConnection	()V
    //   740: aload 12
    //   742: athrow
    //   743: astore 21
    //   745: aload_0
    //   746: getfield 86	org/apache/http/impl/client/DefaultRequestDirector:log	Lorg/apache/commons/logging/Log;
    //   749: invokeinterface 163 1 0
    //   754: ifeq +17 -> 771
    //   757: aload_0
    //   758: getfield 86	org/apache/http/impl/client/DefaultRequestDirector:log	Lorg/apache/commons/logging/Log;
    //   761: aload 21
    //   763: invokevirtual 797	org/apache/http/impl/client/TunnelRefusedException:getMessage	()Ljava/lang/String;
    //   766: invokeinterface 255 2 0
    //   771: aload 21
    //   773: invokevirtual 801	org/apache/http/impl/client/TunnelRefusedException:getResponse	()Lorg/apache/http/HttpResponse;
    //   776: astore 9
    //   778: aload 9
    //   780: ifnull +28 -> 808
    //   783: aload 9
    //   785: invokeinterface 581 1 0
    //   790: ifnull +18 -> 808
    //   793: aload 9
    //   795: invokeinterface 581 1 0
    //   800: invokeinterface 806 1 0
    //   805: ifne +108 -> 913
    //   808: iload 7
    //   810: ifeq +12 -> 822
    //   813: aload_0
    //   814: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   817: invokeinterface 613 1 0
    //   822: aload_0
    //   823: invokevirtual 807	org/apache/http/impl/client/DefaultRequestDirector:releaseConnection	()V
    //   826: aload 9
    //   828: areturn
    //   829: iload 7
    //   831: ifeq +45 -> 876
    //   834: aload 9
    //   836: invokeinterface 581 1 0
    //   841: invokestatic 587	org/apache/http/util/EntityUtils:consume	(Lorg/apache/http/HttpEntity;)V
    //   844: aload_0
    //   845: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   848: invokeinterface 613 1 0
    //   853: aload 30
    //   855: invokevirtual 265	org/apache/http/impl/client/RoutedRequest:getRoute	()Lorg/apache/http/conn/routing/HttpRoute;
    //   858: aload 6
    //   860: invokevirtual 265	org/apache/http/impl/client/RoutedRequest:getRoute	()Lorg/apache/http/conn/routing/HttpRoute;
    //   863: invokevirtual 811	org/apache/http/conn/routing/HttpRoute:equals	(Ljava/lang/Object;)Z
    //   866: ifne +102 -> 968
    //   869: aload_0
    //   870: invokevirtual 807	org/apache/http/impl/client/DefaultRequestDirector:releaseConnection	()V
    //   873: goto +95 -> 968
    //   876: aload_0
    //   877: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   880: invokeinterface 289 1 0
    //   885: aload_0
    //   886: aload_0
    //   887: getfield 133	org/apache/http/impl/client/DefaultRequestDirector:proxyAuthState	Lorg/apache/http/auth/AuthState;
    //   890: invokespecial 813	org/apache/http/impl/client/DefaultRequestDirector:invalidateAuthIfSuccessful	(Lorg/apache/http/auth/AuthState;)V
    //   893: aload_0
    //   894: aload_0
    //   895: getfield 131	org/apache/http/impl/client/DefaultRequestDirector:targetAuthState	Lorg/apache/http/auth/AuthState;
    //   898: invokespecial 813	org/apache/http/impl/client/DefaultRequestDirector:invalidateAuthIfSuccessful	(Lorg/apache/http/auth/AuthState;)V
    //   901: goto -48 -> 853
    //   904: astore 11
    //   906: aload_0
    //   907: invokespecial 796	org/apache/http/impl/client/DefaultRequestDirector:abortConnection	()V
    //   910: aload 11
    //   912: athrow
    //   913: aload 9
    //   915: invokeinterface 581 1 0
    //   920: astore 16
    //   922: new 815	org/apache/http/conn/BasicManagedEntity
    //   925: dup
    //   926: aload 16
    //   928: aload_0
    //   929: getfield 112	org/apache/http/impl/client/DefaultRequestDirector:managedConn	Lorg/apache/http/conn/ManagedClientConnection;
    //   932: iload 7
    //   934: invokespecial 818	org/apache/http/conn/BasicManagedEntity:<init>	(Lorg/apache/http/HttpEntity;Lorg/apache/http/conn/ManagedClientConnection;Z)V
    //   937: astore 17
    //   939: aload 9
    //   941: aload 17
    //   943: invokeinterface 603 2 0
    //   948: aload 9
    //   950: areturn
    //   951: astore 10
    //   953: aload_0
    //   954: invokespecial 796	org/apache/http/impl/client/DefaultRequestDirector:abortConnection	()V
    //   957: aload 10
    //   959: athrow
    //   960: ldc_w 820
    //   963: astore 29
    //   965: goto -395 -> 570
    //   968: aload 30
    //   970: astore 6
    //   972: goto -339 -> 633
    //
    // Exception table:
    //   from	to	target	type
    //   135	200	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   200	209	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   209	225	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   225	292	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   292	312	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   312	319	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   319	338	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   343	350	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   350	441	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   446	497	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   502	528	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   535	570	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   570	600	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   600	614	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   614	625	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   633	640	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   645	668	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   673	684	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   714	734	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   745	771	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   771	778	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   783	808	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   813	822	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   822	826	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   834	853	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   853	873	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   876	901	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   913	948	687	org/apache/http/impl/conn/ConnectionShutdownException
    //   209	225	712	java/lang/InterruptedException
    //   135	200	734	org/apache/http/HttpException
    //   200	209	734	org/apache/http/HttpException
    //   209	225	734	org/apache/http/HttpException
    //   225	292	734	org/apache/http/HttpException
    //   292	312	734	org/apache/http/HttpException
    //   312	319	734	org/apache/http/HttpException
    //   319	338	734	org/apache/http/HttpException
    //   343	350	734	org/apache/http/HttpException
    //   350	441	734	org/apache/http/HttpException
    //   446	497	734	org/apache/http/HttpException
    //   502	528	734	org/apache/http/HttpException
    //   535	570	734	org/apache/http/HttpException
    //   570	600	734	org/apache/http/HttpException
    //   600	614	734	org/apache/http/HttpException
    //   614	625	734	org/apache/http/HttpException
    //   633	640	734	org/apache/http/HttpException
    //   645	668	734	org/apache/http/HttpException
    //   673	684	734	org/apache/http/HttpException
    //   714	734	734	org/apache/http/HttpException
    //   745	771	734	org/apache/http/HttpException
    //   771	778	734	org/apache/http/HttpException
    //   783	808	734	org/apache/http/HttpException
    //   813	822	734	org/apache/http/HttpException
    //   822	826	734	org/apache/http/HttpException
    //   834	853	734	org/apache/http/HttpException
    //   853	873	734	org/apache/http/HttpException
    //   876	901	734	org/apache/http/HttpException
    //   913	948	734	org/apache/http/HttpException
    //   312	319	743	org/apache/http/impl/client/TunnelRefusedException
    //   135	200	904	java/io/IOException
    //   200	209	904	java/io/IOException
    //   209	225	904	java/io/IOException
    //   225	292	904	java/io/IOException
    //   292	312	904	java/io/IOException
    //   312	319	904	java/io/IOException
    //   319	338	904	java/io/IOException
    //   343	350	904	java/io/IOException
    //   350	441	904	java/io/IOException
    //   446	497	904	java/io/IOException
    //   502	528	904	java/io/IOException
    //   535	570	904	java/io/IOException
    //   570	600	904	java/io/IOException
    //   600	614	904	java/io/IOException
    //   614	625	904	java/io/IOException
    //   633	640	904	java/io/IOException
    //   645	668	904	java/io/IOException
    //   673	684	904	java/io/IOException
    //   714	734	904	java/io/IOException
    //   745	771	904	java/io/IOException
    //   771	778	904	java/io/IOException
    //   783	808	904	java/io/IOException
    //   813	822	904	java/io/IOException
    //   822	826	904	java/io/IOException
    //   834	853	904	java/io/IOException
    //   853	873	904	java/io/IOException
    //   876	901	904	java/io/IOException
    //   913	948	904	java/io/IOException
    //   135	200	951	java/lang/RuntimeException
    //   200	209	951	java/lang/RuntimeException
    //   209	225	951	java/lang/RuntimeException
    //   225	292	951	java/lang/RuntimeException
    //   292	312	951	java/lang/RuntimeException
    //   312	319	951	java/lang/RuntimeException
    //   319	338	951	java/lang/RuntimeException
    //   343	350	951	java/lang/RuntimeException
    //   350	441	951	java/lang/RuntimeException
    //   446	497	951	java/lang/RuntimeException
    //   502	528	951	java/lang/RuntimeException
    //   535	570	951	java/lang/RuntimeException
    //   570	600	951	java/lang/RuntimeException
    //   600	614	951	java/lang/RuntimeException
    //   614	625	951	java/lang/RuntimeException
    //   633	640	951	java/lang/RuntimeException
    //   645	668	951	java/lang/RuntimeException
    //   673	684	951	java/lang/RuntimeException
    //   714	734	951	java/lang/RuntimeException
    //   745	771	951	java/lang/RuntimeException
    //   771	778	951	java/lang/RuntimeException
    //   783	808	951	java/lang/RuntimeException
    //   813	822	951	java/lang/RuntimeException
    //   822	826	951	java/lang/RuntimeException
    //   834	853	951	java/lang/RuntimeException
    //   853	873	951	java/lang/RuntimeException
    //   876	901	951	java/lang/RuntimeException
    //   913	948	951	java/lang/RuntimeException
  }

  protected RoutedRequest handleResponse(RoutedRequest paramRoutedRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpRoute localHttpRoute1 = paramRoutedRequest.getRoute();
    RequestWrapper localRequestWrapper1 = paramRoutedRequest.getRequest();
    HttpParams localHttpParams = localRequestWrapper1.getParams();
    if ((HttpClientParams.isRedirecting(localHttpParams)) && (this.redirectStrategy.isRedirected(localRequestWrapper1, paramHttpResponse, paramHttpContext)))
    {
      if (this.redirectCount >= this.maxRedirects)
        throw new RedirectException("Maximum redirects (" + this.maxRedirects + ") exceeded");
      this.redirectCount = (1 + this.redirectCount);
      this.virtualHost = null;
      HttpUriRequest localHttpUriRequest = this.redirectStrategy.getRedirect(localRequestWrapper1, paramHttpResponse, paramHttpContext);
      localHttpUriRequest.setHeaders(localRequestWrapper1.getOriginal().getAllHeaders());
      URI localURI = localHttpUriRequest.getURI();
      if (localURI.getHost() == null)
        throw new ProtocolException("Redirect URI does not specify a valid host name: " + localURI);
      HttpHost localHttpHost3 = new HttpHost(localURI.getHost(), localURI.getPort(), localURI.getScheme());
      this.targetAuthState.setAuthScope(null);
      this.proxyAuthState.setAuthScope(null);
      if (!localHttpRoute1.getTargetHost().equals(localHttpHost3))
      {
        this.targetAuthState.invalidate();
        AuthScheme localAuthScheme = this.proxyAuthState.getAuthScheme();
        if ((localAuthScheme != null) && (localAuthScheme.isConnectionBased()))
          this.proxyAuthState.invalidate();
      }
      RequestWrapper localRequestWrapper2 = wrapRequest(localHttpUriRequest);
      localRequestWrapper2.setParams(localHttpParams);
      HttpRoute localHttpRoute2 = determineRoute(localHttpHost3, localRequestWrapper2, paramHttpContext);
      RoutedRequest localRoutedRequest = new RoutedRequest(localRequestWrapper2, localHttpRoute2);
      if (this.log.isDebugEnabled())
        this.log.debug("Redirecting to '" + localURI + "' via " + localHttpRoute2);
      return localRoutedRequest;
    }
    CredentialsProvider localCredentialsProvider = (CredentialsProvider)paramHttpContext.getAttribute("http.auth.credentials-provider");
    if ((localCredentialsProvider != null) && (HttpClientParams.isAuthenticating(localHttpParams)))
    {
      if (this.targetAuthHandler.isAuthenticationRequested(paramHttpResponse, paramHttpContext))
      {
        HttpHost localHttpHost2 = (HttpHost)paramHttpContext.getAttribute("http.target_host");
        if (localHttpHost2 == null)
          localHttpHost2 = localHttpRoute1.getTargetHost();
        this.log.debug("Target requested authentication");
        Map localMap2 = this.targetAuthHandler.getChallenges(paramHttpResponse, paramHttpContext);
        try
        {
          processChallenges(localMap2, this.targetAuthState, this.targetAuthHandler, paramHttpResponse, paramHttpContext);
          updateAuthState(this.targetAuthState, localHttpHost2, localCredentialsProvider);
          if (this.targetAuthState.getCredentials() != null)
            return paramRoutedRequest;
        }
        catch (AuthenticationException localAuthenticationException2)
        {
          while (!this.log.isWarnEnabled());
          this.log.warn("Authentication error: " + localAuthenticationException2.getMessage());
          return null;
        }
        return null;
      }
      this.targetAuthState.setAuthScope(null);
      if (this.proxyAuthHandler.isAuthenticationRequested(paramHttpResponse, paramHttpContext))
      {
        HttpHost localHttpHost1 = localHttpRoute1.getProxyHost();
        this.log.debug("Proxy requested authentication");
        Map localMap1 = this.proxyAuthHandler.getChallenges(paramHttpResponse, paramHttpContext);
        try
        {
          processChallenges(localMap1, this.proxyAuthState, this.proxyAuthHandler, paramHttpResponse, paramHttpContext);
          updateAuthState(this.proxyAuthState, localHttpHost1, localCredentialsProvider);
          if (this.proxyAuthState.getCredentials() != null)
            return paramRoutedRequest;
        }
        catch (AuthenticationException localAuthenticationException1)
        {
          while (!this.log.isWarnEnabled());
          this.log.warn("Authentication error: " + localAuthenticationException1.getMessage());
          return null;
        }
        return null;
      }
      this.proxyAuthState.setAuthScope(null);
    }
    return null;
  }

  protected void releaseConnection()
  {
    try
    {
      this.managedConn.releaseConnection();
      this.managedConn = null;
      return;
    }
    catch (IOException localIOException)
    {
      while (true)
        this.log.debug("IOException releasing connection", localIOException);
    }
  }

  protected void rewriteRequestURI(RequestWrapper paramRequestWrapper, HttpRoute paramHttpRoute)
    throws ProtocolException
  {
    try
    {
      URI localURI = paramRequestWrapper.getURI();
      if ((paramHttpRoute.getProxyHost() != null) && (!paramHttpRoute.isTunnelled()))
      {
        if (!localURI.isAbsolute())
          paramRequestWrapper.setURI(URIUtils.rewriteURI(localURI, paramHttpRoute.getTargetHost()));
      }
      else if (localURI.isAbsolute())
      {
        paramRequestWrapper.setURI(URIUtils.rewriteURI(localURI, null));
        return;
      }
    }
    catch (URISyntaxException localURISyntaxException)
    {
      throw new ProtocolException("Invalid URI: " + paramRequestWrapper.getRequestLine().getUri(), localURISyntaxException);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.client.DefaultRequestDirector
 * JD-Core Version:    0.6.2
 */