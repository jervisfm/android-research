package org.apache.http.impl.conn;

import org.apache.http.annotation.Immutable;

@Immutable
public class ConnectionShutdownException extends IllegalStateException
{
  private static final long serialVersionUID = 5868657401162844497L;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.conn.ConnectionShutdownException
 * JD-Core Version:    0.6.2
 */