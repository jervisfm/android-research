package org.apache.http.impl.conn;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.annotation.GuardedBy;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ClientConnectionOperator;
import org.apache.http.conn.ClientConnectionRequest;
import org.apache.http.conn.ManagedClientConnection;
import org.apache.http.conn.OperatedClientConnection;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.RouteTracker;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.params.HttpParams;

@ThreadSafe
public class SingleClientConnManager
  implements ClientConnectionManager
{
  public static final String MISUSE_MESSAGE = "Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.";
  protected final boolean alwaysShutDown;
  protected final ClientConnectionOperator connOperator;

  @GuardedBy("this")
  protected long connectionExpiresTime;
  protected volatile boolean isShutDown;

  @GuardedBy("this")
  protected long lastReleaseTime;
  private final Log log = LogFactory.getLog(getClass());

  @GuardedBy("this")
  protected ConnAdapter managedConn;
  protected final SchemeRegistry schemeRegistry;

  @GuardedBy("this")
  protected PoolEntry uniquePoolEntry;

  public SingleClientConnManager()
  {
    this(SchemeRegistryFactory.createDefault());
  }

  public SingleClientConnManager(SchemeRegistry paramSchemeRegistry)
  {
    if (paramSchemeRegistry == null)
      throw new IllegalArgumentException("Scheme registry must not be null.");
    this.schemeRegistry = paramSchemeRegistry;
    this.connOperator = createConnectionOperator(paramSchemeRegistry);
    this.uniquePoolEntry = new PoolEntry();
    this.managedConn = null;
    this.lastReleaseTime = -1L;
    this.alwaysShutDown = false;
    this.isShutDown = false;
  }

  @Deprecated
  public SingleClientConnManager(HttpParams paramHttpParams, SchemeRegistry paramSchemeRegistry)
  {
    this(paramSchemeRegistry);
  }

  protected final void assertStillUp()
    throws IllegalStateException
  {
    if (this.isShutDown)
      throw new IllegalStateException("Manager is shut down.");
  }

  public void closeExpiredConnections()
  {
    try
    {
      if (System.currentTimeMillis() >= this.connectionExpiresTime)
        closeIdleConnections(0L, TimeUnit.MILLISECONDS);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    try
    {
      assertStillUp();
      if (paramTimeUnit == null)
        throw new IllegalArgumentException("Time unit must not be null.");
    }
    finally
    {
    }
    if ((this.managedConn == null) && (this.uniquePoolEntry.connection.isOpen()))
    {
      long l1 = System.currentTimeMillis() - paramTimeUnit.toMillis(paramLong);
      long l2 = this.lastReleaseTime;
      if (l2 > l1);
    }
    try
    {
      this.uniquePoolEntry.close();
      return;
    }
    catch (IOException localIOException)
    {
      while (true)
        this.log.debug("Problem closing idle connection.", localIOException);
    }
  }

  protected ClientConnectionOperator createConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    return new DefaultClientConnectionOperator(paramSchemeRegistry);
  }

  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }

  public ManagedClientConnection getConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    if (paramHttpRoute == null)
      try
      {
        throw new IllegalArgumentException("Route may not be null.");
      }
      finally
      {
      }
    assertStillUp();
    if (this.log.isDebugEnabled())
      this.log.debug("Get connection for route " + paramHttpRoute);
    if (this.managedConn != null)
      throw new IllegalStateException("Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.");
    int i = 0;
    closeExpiredConnections();
    int j;
    if (this.uniquePoolEntry.connection.isOpen())
    {
      RouteTracker localRouteTracker = this.uniquePoolEntry.tracker;
      if (localRouteTracker != null)
      {
        boolean bool = localRouteTracker.toRoute().equals(paramHttpRoute);
        if (bool);
      }
      else
      {
        j = 1;
      }
    }
    while (true)
    {
      if (j != 0)
        i = 1;
      try
      {
        this.uniquePoolEntry.shutdown();
        if (i != 0)
          this.uniquePoolEntry = new PoolEntry();
        this.managedConn = new ConnAdapter(this.uniquePoolEntry, paramHttpRoute);
        ConnAdapter localConnAdapter = this.managedConn;
        return localConnAdapter;
        i = 0;
        j = 0;
        continue;
        i = 1;
        j = 0;
      }
      catch (IOException localIOException)
      {
        while (true)
          this.log.debug("Problem shutting down connection.", localIOException);
      }
    }
  }

  public SchemeRegistry getSchemeRegistry()
  {
    return this.schemeRegistry;
  }

  public void releaseConnection(ManagedClientConnection paramManagedClientConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    try
    {
      assertStillUp();
      if (!(paramManagedClientConnection instanceof ConnAdapter))
        throw new IllegalArgumentException("Connection class mismatch, connection not obtained from this manager.");
    }
    finally
    {
    }
    if (this.log.isDebugEnabled())
      this.log.debug("Releasing connection " + paramManagedClientConnection);
    ConnAdapter localConnAdapter = (ConnAdapter)paramManagedClientConnection;
    AbstractPoolEntry localAbstractPoolEntry = localConnAdapter.poolEntry;
    if (localAbstractPoolEntry == null);
    while (true)
    {
      return;
      ClientConnectionManager localClientConnectionManager = localConnAdapter.getManager();
      if ((localClientConnectionManager != null) && (localClientConnectionManager != this))
        throw new IllegalArgumentException("Connection not obtained from this manager.");
      try
      {
        if ((localConnAdapter.isOpen()) && ((this.alwaysShutDown) || (!localConnAdapter.isMarkedReusable())))
        {
          if (this.log.isDebugEnabled())
            this.log.debug("Released connection open but not reusable.");
          localConnAdapter.shutdown();
        }
        localConnAdapter.detach();
        this.managedConn = null;
        this.lastReleaseTime = System.currentTimeMillis();
        if (paramLong > 0L)
          this.connectionExpiresTime = (paramTimeUnit.toMillis(paramLong) + this.lastReleaseTime);
        else
          this.connectionExpiresTime = 9223372036854775807L;
      }
      catch (IOException localIOException)
      {
        if (this.log.isDebugEnabled())
          this.log.debug("Exception shutting down released connection.", localIOException);
        localConnAdapter.detach();
        this.managedConn = null;
        this.lastReleaseTime = System.currentTimeMillis();
        if (paramLong > 0L)
          this.connectionExpiresTime = (paramTimeUnit.toMillis(paramLong) + this.lastReleaseTime);
        else
          this.connectionExpiresTime = 9223372036854775807L;
      }
      finally
      {
        localConnAdapter.detach();
        this.managedConn = null;
        this.lastReleaseTime = System.currentTimeMillis();
        if (paramLong <= 0L);
      }
    }
    for (this.connectionExpiresTime = (paramTimeUnit.toMillis(paramLong) + this.lastReleaseTime); ; this.connectionExpiresTime = 9223372036854775807L)
      throw localObject2;
  }

  public final ClientConnectionRequest requestConnection(final HttpRoute paramHttpRoute, final Object paramObject)
  {
    return new ClientConnectionRequest()
    {
      public void abortRequest()
      {
      }

      public ManagedClientConnection getConnection(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        return SingleClientConnManager.this.getConnection(paramHttpRoute, paramObject);
      }
    };
  }

  @Deprecated
  protected void revokeConnection()
  {
    try
    {
      ConnAdapter localConnAdapter = this.managedConn;
      if (localConnAdapter == null);
      while (true)
      {
        return;
        this.managedConn.detach();
        try
        {
          this.uniquePoolEntry.shutdown();
        }
        catch (IOException localIOException)
        {
          this.log.debug("Problem while shutting down connection.", localIOException);
        }
      }
    }
    finally
    {
    }
  }

  // ERROR //
  public void shutdown()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: putfield 87	org/apache/http/impl/conn/SingleClientConnManager:isShutDown	Z
    //   7: aload_0
    //   8: getfield 79	org/apache/http/impl/conn/SingleClientConnManager:managedConn	Lorg/apache/http/impl/conn/SingleClientConnManager$ConnAdapter;
    //   11: ifnull +10 -> 21
    //   14: aload_0
    //   15: getfield 79	org/apache/http/impl/conn/SingleClientConnManager:managedConn	Lorg/apache/http/impl/conn/SingleClientConnManager$ConnAdapter;
    //   18: invokevirtual 235	org/apache/http/impl/conn/SingleClientConnManager$ConnAdapter:detach	()V
    //   21: aload_0
    //   22: getfield 77	org/apache/http/impl/conn/SingleClientConnManager:uniquePoolEntry	Lorg/apache/http/impl/conn/SingleClientConnManager$PoolEntry;
    //   25: ifnull +10 -> 35
    //   28: aload_0
    //   29: getfield 77	org/apache/http/impl/conn/SingleClientConnManager:uniquePoolEntry	Lorg/apache/http/impl/conn/SingleClientConnManager$PoolEntry;
    //   32: invokevirtual 201	org/apache/http/impl/conn/SingleClientConnManager$PoolEntry:shutdown	()V
    //   35: aload_0
    //   36: aconst_null
    //   37: putfield 77	org/apache/http/impl/conn/SingleClientConnManager:uniquePoolEntry	Lorg/apache/http/impl/conn/SingleClientConnManager$PoolEntry;
    //   40: aload_0
    //   41: monitorexit
    //   42: return
    //   43: astore_3
    //   44: aload_0
    //   45: getfield 55	org/apache/http/impl/conn/SingleClientConnManager:log	Lorg/apache/commons/logging/Log;
    //   48: ldc 251
    //   50: aload_3
    //   51: invokeinterface 145 3 0
    //   56: aload_0
    //   57: aconst_null
    //   58: putfield 77	org/apache/http/impl/conn/SingleClientConnManager:uniquePoolEntry	Lorg/apache/http/impl/conn/SingleClientConnManager$PoolEntry;
    //   61: goto -21 -> 40
    //   64: astore_1
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_1
    //   68: athrow
    //   69: astore_2
    //   70: aload_0
    //   71: aconst_null
    //   72: putfield 77	org/apache/http/impl/conn/SingleClientConnManager:uniquePoolEntry	Lorg/apache/http/impl/conn/SingleClientConnManager$PoolEntry;
    //   75: aload_2
    //   76: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   21	35	43	java/io/IOException
    //   2	21	64	finally
    //   35	40	64	finally
    //   56	61	64	finally
    //   70	77	64	finally
    //   21	35	69	finally
    //   44	56	69	finally
  }

  protected class ConnAdapter extends AbstractPooledConnAdapter
  {
    protected ConnAdapter(SingleClientConnManager.PoolEntry paramHttpRoute, HttpRoute arg3)
    {
      super(paramHttpRoute);
      markReusable();
      Object localObject;
      paramHttpRoute.route = localObject;
    }
  }

  protected class PoolEntry extends AbstractPoolEntry
  {
    protected PoolEntry()
    {
      super(null);
    }

    protected void close()
      throws IOException
    {
      shutdownEntry();
      if (this.connection.isOpen())
        this.connection.close();
    }

    protected void shutdown()
      throws IOException
    {
      shutdownEntry();
      if (this.connection.isOpen())
        this.connection.shutdown();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.conn.SingleClientConnManager
 * JD-Core Version:    0.6.2
 */