package org.apache.http.impl.conn.tsccm;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;

@Deprecated
public class RefQueueWorker
  implements Runnable
{
  protected final RefQueueHandler refHandler;
  protected final ReferenceQueue<?> refQueue;
  protected volatile Thread workerThread;

  public RefQueueWorker(ReferenceQueue<?> paramReferenceQueue, RefQueueHandler paramRefQueueHandler)
  {
    if (paramReferenceQueue == null)
      throw new IllegalArgumentException("Queue must not be null.");
    if (paramRefQueueHandler == null)
      throw new IllegalArgumentException("Handler must not be null.");
    this.refQueue = paramReferenceQueue;
    this.refHandler = paramRefQueueHandler;
  }

  public void run()
  {
    if (this.workerThread == null)
      this.workerThread = Thread.currentThread();
    while (this.workerThread == Thread.currentThread())
      try
      {
        Reference localReference = this.refQueue.remove();
        this.refHandler.handleReference(localReference);
      }
      catch (InterruptedException localInterruptedException)
      {
      }
  }

  public void shutdown()
  {
    Thread localThread = this.workerThread;
    if (localThread != null)
    {
      this.workerThread = null;
      localThread.interrupt();
    }
  }

  public String toString()
  {
    return "RefQueueWorker::" + this.workerThread;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.conn.tsccm.RefQueueWorker
 * JD-Core Version:    0.6.2
 */