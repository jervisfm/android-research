package org.apache.http.impl.cookie;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.http.client.utils.Punycode;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieAttributeHandler;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.cookie.MalformedCookieException;
import org.apache.http.cookie.SetCookie;

public class PublicSuffixFilter
  implements CookieAttributeHandler
{
  private Set<String> exceptions;
  private Set<String> suffixes;
  private final CookieAttributeHandler wrapped;

  public PublicSuffixFilter(CookieAttributeHandler paramCookieAttributeHandler)
  {
    this.wrapped = paramCookieAttributeHandler;
  }

  private boolean isForPublicSuffix(Cookie paramCookie)
  {
    String str1 = paramCookie.getDomain();
    if (str1.startsWith("."))
      str1 = str1.substring(1);
    String str2 = Punycode.toUnicode(str1);
    if ((this.exceptions != null) && (this.exceptions.contains(str2)))
      return false;
    if (this.suffixes == null)
      return false;
    if (this.suffixes.contains(str2))
      return true;
    if (str2.startsWith("*."))
      str2 = str2.substring(2);
    int i = str2.indexOf('.');
    if (i == -1);
    while (true)
    {
      return false;
      str2 = "*" + str2.substring(i);
      if (str2.length() > 0)
        break;
    }
  }

  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    if (isForPublicSuffix(paramCookie))
      return false;
    return this.wrapped.match(paramCookie, paramCookieOrigin);
  }

  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    this.wrapped.parse(paramSetCookie, paramString);
  }

  public void setExceptions(Collection<String> paramCollection)
  {
    this.exceptions = new HashSet(paramCollection);
  }

  public void setPublicSuffixes(Collection<String> paramCollection)
  {
    this.suffixes = new HashSet(paramCollection);
  }

  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    this.wrapped.validate(paramCookie, paramCookieOrigin);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.cookie.PublicSuffixFilter
 * JD-Core Version:    0.6.2
 */