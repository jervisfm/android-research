package org.apache.http.impl.entity;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpException;
import org.apache.http.HttpMessage;
import org.apache.http.ParseException;
import org.apache.http.ProtocolException;
import org.apache.http.entity.ContentLengthStrategy;
import org.apache.http.params.HttpParams;

public class LaxContentLengthStrategy
  implements ContentLengthStrategy
{
  public long determineLength(HttpMessage paramHttpMessage)
    throws HttpException
  {
    if (paramHttpMessage == null)
      throw new IllegalArgumentException("HTTP message may not be null");
    boolean bool = paramHttpMessage.getParams().isParameterTrue("http.protocol.strict-transfer-encoding");
    Header localHeader1 = paramHttpMessage.getFirstHeader("Transfer-Encoding");
    Header localHeader2 = paramHttpMessage.getFirstHeader("Content-Length");
    if (localHeader1 != null)
    {
      HeaderElement[] arrayOfHeaderElement;
      while (true)
      {
        int k;
        try
        {
          arrayOfHeaderElement = localHeader1.getElements();
          if (!bool)
            break;
          k = 0;
          int m = arrayOfHeaderElement.length;
          if (k >= m)
            break;
          String str = arrayOfHeaderElement[k].getName();
          if ((str != null) && (str.length() > 0) && (!str.equalsIgnoreCase("chunked")) && (!str.equalsIgnoreCase("identity")))
            throw new ProtocolException("Unsupported transfer encoding: " + str);
        }
        catch (ParseException localParseException)
        {
          ProtocolException localProtocolException = new ProtocolException("Invalid Transfer-Encoding header value: " + localHeader1, localParseException);
          throw localProtocolException;
        }
        k++;
      }
      int j = arrayOfHeaderElement.length;
      if ("identity".equalsIgnoreCase(localHeader1.getValue()))
        return -1L;
      if ((j > 0) && ("chunked".equalsIgnoreCase(arrayOfHeaderElement[(j - 1)].getName())))
        return -2L;
      if (bool)
        throw new ProtocolException("Chunk-encoding must be the last one applied");
      return -1L;
    }
    if (localHeader2 != null)
    {
      long l1 = -1L;
      Header[] arrayOfHeader = paramHttpMessage.getHeaders("Content-Length");
      if ((bool) && (arrayOfHeader.length > 1))
        throw new ProtocolException("Multiple content length headers");
      int i = arrayOfHeader.length - 1;
      while (true)
      {
        Header localHeader3;
        if (i >= 0)
          localHeader3 = arrayOfHeader[i];
        try
        {
          long l2 = Long.parseLong(localHeader3.getValue());
          l1 = l2;
          if (l1 < 0L)
            break;
          return l1;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          if (bool)
            throw new ProtocolException("Invalid content length: " + localHeader3.getValue());
          i--;
        }
      }
      return -1L;
    }
    return -1L;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.entity.LaxContentLengthStrategy
 * JD-Core Version:    0.6.2
 */