package org.apache.http.impl.entity;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpMessage;
import org.apache.http.HttpVersion;
import org.apache.http.ProtocolException;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.ContentLengthStrategy;

public class StrictContentLengthStrategy
  implements ContentLengthStrategy
{
  public long determineLength(HttpMessage paramHttpMessage)
    throws HttpException
  {
    if (paramHttpMessage == null)
      throw new IllegalArgumentException("HTTP message may not be null");
    Header localHeader1 = paramHttpMessage.getFirstHeader("Transfer-Encoding");
    Header localHeader2 = paramHttpMessage.getFirstHeader("Content-Length");
    if (localHeader1 != null)
    {
      String str2 = localHeader1.getValue();
      if ("chunked".equalsIgnoreCase(str2))
      {
        if (paramHttpMessage.getProtocolVersion().lessEquals(HttpVersion.HTTP_1_0))
          throw new ProtocolException("Chunked transfer encoding not allowed for " + paramHttpMessage.getProtocolVersion());
        return -2L;
      }
      if ("identity".equalsIgnoreCase(str2))
        return -1L;
      throw new ProtocolException("Unsupported transfer encoding: " + str2);
    }
    if (localHeader2 != null)
    {
      String str1 = localHeader2.getValue();
      try
      {
        long l = Long.parseLong(str1);
        return l;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw new ProtocolException("Invalid content length: " + str1);
      }
    }
    return -1L;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.entity.StrictContentLengthStrategy
 * JD-Core Version:    0.6.2
 */