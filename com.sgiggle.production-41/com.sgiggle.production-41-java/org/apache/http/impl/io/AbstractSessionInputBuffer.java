package org.apache.http.impl.io;

import java.io.IOException;
import java.io.InputStream;
import org.apache.http.io.BufferInfo;
import org.apache.http.io.HttpTransportMetrics;
import org.apache.http.io.SessionInputBuffer;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.CharArrayBuffer;

public abstract class AbstractSessionInputBuffer
  implements SessionInputBuffer, BufferInfo
{
  private boolean ascii = true;
  private byte[] buffer;
  private int bufferlen;
  private int bufferpos;
  private String charset = "US-ASCII";
  private InputStream instream;
  private ByteArrayBuffer linebuffer = null;
  private int maxLineLen = -1;
  private HttpTransportMetricsImpl metrics;
  private int minChunkLimit = 512;

  private int lineFromLineBuffer(CharArrayBuffer paramCharArrayBuffer)
    throws IOException
  {
    int i = this.linebuffer.length();
    if (i > 0)
    {
      if (this.linebuffer.byteAt(i - 1) == 10)
      {
        i--;
        this.linebuffer.setLength(i);
      }
      if ((i > 0) && (this.linebuffer.byteAt(i - 1) == 13))
      {
        int k = i - 1;
        this.linebuffer.setLength(k);
      }
    }
    int j = this.linebuffer.length();
    if (this.ascii)
      paramCharArrayBuffer.append(this.linebuffer, 0, j);
    while (true)
    {
      this.linebuffer.clear();
      return j;
      String str = new String(this.linebuffer.buffer(), 0, j, this.charset);
      j = str.length();
      paramCharArrayBuffer.append(str);
    }
  }

  private int lineFromReadBuffer(CharArrayBuffer paramCharArrayBuffer, int paramInt)
    throws IOException
  {
    int i = this.bufferpos;
    this.bufferpos = (paramInt + 1);
    if ((paramInt > 0) && (this.buffer[(paramInt - 1)] == 13))
      paramInt--;
    int j = paramInt - i;
    if (this.ascii)
    {
      paramCharArrayBuffer.append(this.buffer, i, j);
      return j;
    }
    String str = new String(this.buffer, i, j, this.charset);
    paramCharArrayBuffer.append(str);
    return str.length();
  }

  private int locateLF()
  {
    for (int i = this.bufferpos; i < this.bufferlen; i++)
      if (this.buffer[i] == 10)
        return i;
    return -1;
  }

  public int available()
  {
    return capacity() - length();
  }

  public int capacity()
  {
    return this.buffer.length;
  }

  protected HttpTransportMetricsImpl createTransportMetrics()
  {
    return new HttpTransportMetricsImpl();
  }

  protected int fillBuffer()
    throws IOException
  {
    if (this.bufferpos > 0)
    {
      int m = this.bufferlen - this.bufferpos;
      if (m > 0)
        System.arraycopy(this.buffer, this.bufferpos, this.buffer, 0, m);
      this.bufferpos = 0;
      this.bufferlen = m;
    }
    int i = this.bufferlen;
    int j = this.buffer.length - i;
    int k = this.instream.read(this.buffer, i, j);
    if (k == -1)
      return -1;
    this.bufferlen = (i + k);
    this.metrics.incrementBytesTransferred(k);
    return k;
  }

  public HttpTransportMetrics getMetrics()
  {
    return this.metrics;
  }

  protected boolean hasBufferedData()
  {
    return this.bufferpos < this.bufferlen;
  }

  protected void init(InputStream paramInputStream, int paramInt, HttpParams paramHttpParams)
  {
    if (paramInputStream == null)
      throw new IllegalArgumentException("Input stream may not be null");
    if (paramInt <= 0)
      throw new IllegalArgumentException("Buffer size may not be negative or zero");
    if (paramHttpParams == null)
      throw new IllegalArgumentException("HTTP parameters may not be null");
    this.instream = paramInputStream;
    this.buffer = new byte[paramInt];
    this.bufferpos = 0;
    this.bufferlen = 0;
    this.linebuffer = new ByteArrayBuffer(paramInt);
    this.charset = HttpProtocolParams.getHttpElementCharset(paramHttpParams);
    if ((this.charset.equalsIgnoreCase("US-ASCII")) || (this.charset.equalsIgnoreCase("ASCII")));
    for (boolean bool = true; ; bool = false)
    {
      this.ascii = bool;
      this.maxLineLen = paramHttpParams.getIntParameter("http.connection.max-line-length", -1);
      this.minChunkLimit = paramHttpParams.getIntParameter("http.connection.min-chunk-limit", 512);
      this.metrics = createTransportMetrics();
      return;
    }
  }

  public int length()
  {
    return this.bufferlen - this.bufferpos;
  }

  public int read()
    throws IOException
  {
    while (!hasBufferedData())
      if (fillBuffer() == -1)
        return -1;
    byte[] arrayOfByte = this.buffer;
    int i = this.bufferpos;
    this.bufferpos = (i + 1);
    return 0xFF & arrayOfByte[i];
  }

  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    if (paramArrayOfByte == null)
      return 0;
    return read(paramArrayOfByte, 0, paramArrayOfByte.length);
  }

  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (paramArrayOfByte == null)
      return 0;
    if (hasBufferedData())
    {
      int k = Math.min(paramInt2, this.bufferlen - this.bufferpos);
      System.arraycopy(this.buffer, this.bufferpos, paramArrayOfByte, paramInt1, k);
      this.bufferpos = (k + this.bufferpos);
      return k;
    }
    if (paramInt2 > this.minChunkLimit)
    {
      int j = this.instream.read(paramArrayOfByte, paramInt1, paramInt2);
      if (j > 0)
        this.metrics.incrementBytesTransferred(j);
      return j;
    }
    while (!hasBufferedData())
      if (fillBuffer() == -1)
        return -1;
    int i = Math.min(paramInt2, this.bufferlen - this.bufferpos);
    System.arraycopy(this.buffer, this.bufferpos, paramArrayOfByte, paramInt1, i);
    this.bufferpos = (i + this.bufferpos);
    return i;
  }

  public int readLine(CharArrayBuffer paramCharArrayBuffer)
    throws IOException
  {
    if (paramCharArrayBuffer == null)
      throw new IllegalArgumentException("Char array buffer may not be null");
    int i = 0;
    int j = 1;
    while (j != 0)
    {
      int k = locateLF();
      if (k != -1)
      {
        if (this.linebuffer.isEmpty())
          return lineFromReadBuffer(paramCharArrayBuffer, k);
        j = 0;
        int n = k + 1 - this.bufferpos;
        this.linebuffer.append(this.buffer, this.bufferpos, n);
        this.bufferpos = (k + 1);
      }
      while ((this.maxLineLen > 0) && (this.linebuffer.length() >= this.maxLineLen))
      {
        throw new IOException("Maximum line length limit exceeded");
        if (hasBufferedData())
        {
          int m = this.bufferlen - this.bufferpos;
          this.linebuffer.append(this.buffer, this.bufferpos, m);
          this.bufferpos = this.bufferlen;
        }
        i = fillBuffer();
        if (i == -1)
          j = 0;
      }
    }
    if ((i == -1) && (this.linebuffer.isEmpty()))
      return -1;
    return lineFromLineBuffer(paramCharArrayBuffer);
  }

  public String readLine()
    throws IOException
  {
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(64);
    if (readLine(localCharArrayBuffer) != -1)
      return localCharArrayBuffer.toString();
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.io.AbstractSessionInputBuffer
 * JD-Core Version:    0.6.2
 */