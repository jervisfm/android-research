package org.apache.http.impl.io;

import org.apache.http.io.HttpTransportMetrics;

public class HttpTransportMetricsImpl
  implements HttpTransportMetrics
{
  private long bytesTransferred = 0L;

  public long getBytesTransferred()
  {
    return this.bytesTransferred;
  }

  public void incrementBytesTransferred(long paramLong)
  {
    this.bytesTransferred = (paramLong + this.bytesTransferred);
  }

  public void reset()
  {
    this.bytesTransferred = 0L;
  }

  public void setBytesTransferred(long paramLong)
  {
    this.bytesTransferred = paramLong;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.io.HttpTransportMetricsImpl
 * JD-Core Version:    0.6.2
 */