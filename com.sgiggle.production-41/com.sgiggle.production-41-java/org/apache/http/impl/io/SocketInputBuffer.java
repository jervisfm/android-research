package org.apache.http.impl.io;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import org.apache.http.io.EofSensor;
import org.apache.http.params.HttpParams;

public class SocketInputBuffer extends AbstractSessionInputBuffer
  implements EofSensor
{
  private static final Class SOCKET_TIMEOUT_CLASS = SocketTimeoutExceptionClass();
  private boolean eof;
  private final Socket socket;

  public SocketInputBuffer(Socket paramSocket, int paramInt, HttpParams paramHttpParams)
    throws IOException
  {
    if (paramSocket == null)
      throw new IllegalArgumentException("Socket may not be null");
    this.socket = paramSocket;
    this.eof = false;
    if (paramInt < 0)
      paramInt = paramSocket.getReceiveBufferSize();
    if (paramInt < 1024)
      paramInt = 1024;
    init(paramSocket.getInputStream(), paramInt, paramHttpParams);
  }

  private static Class SocketTimeoutExceptionClass()
  {
    try
    {
      Class localClass = Class.forName("java.net.SocketTimeoutException");
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
    }
    return null;
  }

  private static boolean isSocketTimeoutException(InterruptedIOException paramInterruptedIOException)
  {
    if (SOCKET_TIMEOUT_CLASS != null)
      return SOCKET_TIMEOUT_CLASS.isInstance(paramInterruptedIOException);
    return true;
  }

  protected int fillBuffer()
    throws IOException
  {
    int i = super.fillBuffer();
    if (i == -1);
    for (boolean bool = true; ; bool = false)
    {
      this.eof = bool;
      return i;
    }
  }

  public boolean isDataAvailable(int paramInt)
    throws IOException
  {
    boolean bool1 = hasBufferedData();
    int i;
    if (!bool1)
      i = this.socket.getSoTimeout();
    try
    {
      this.socket.setSoTimeout(paramInt);
      fillBuffer();
      boolean bool2 = hasBufferedData();
      bool1 = bool2;
      return bool1;
    }
    catch (InterruptedIOException localInterruptedIOException)
    {
      if (!isSocketTimeoutException(localInterruptedIOException))
        throw localInterruptedIOException;
    }
    finally
    {
      this.socket.setSoTimeout(i);
    }
    this.socket.setSoTimeout(i);
    return bool1;
  }

  public boolean isEof()
  {
    return this.eof;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.impl.io.SocketInputBuffer
 * JD-Core Version:    0.6.2
 */