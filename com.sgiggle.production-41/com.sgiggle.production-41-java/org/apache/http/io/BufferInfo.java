package org.apache.http.io;

public abstract interface BufferInfo
{
  public abstract int available();

  public abstract int capacity();

  public abstract int length();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.io.BufferInfo
 * JD-Core Version:    0.6.2
 */