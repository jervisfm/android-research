package org.apache.http.io;

public abstract interface EofSensor
{
  public abstract boolean isEof();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.io.EofSensor
 * JD-Core Version:    0.6.2
 */