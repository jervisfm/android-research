package org.apache.http.message;

import org.apache.http.Header;
import org.apache.http.HttpVersion;
import org.apache.http.ParseException;
import org.apache.http.ProtocolVersion;
import org.apache.http.RequestLine;
import org.apache.http.StatusLine;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.CharArrayBuffer;

public class BasicLineParser
  implements LineParser
{
  public static final BasicLineParser DEFAULT = new BasicLineParser();
  protected final ProtocolVersion protocol;

  public BasicLineParser()
  {
    this(null);
  }

  public BasicLineParser(ProtocolVersion paramProtocolVersion)
  {
    if (paramProtocolVersion == null)
      paramProtocolVersion = HttpVersion.HTTP_1_1;
    this.protocol = paramProtocolVersion;
  }

  public static final Header parseHeader(String paramString, LineParser paramLineParser)
    throws ParseException
  {
    if (paramString == null)
      throw new IllegalArgumentException("Value to parse may not be null");
    if (paramLineParser == null)
      paramLineParser = DEFAULT;
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    return paramLineParser.parseHeader(localCharArrayBuffer);
  }

  public static final ProtocolVersion parseProtocolVersion(String paramString, LineParser paramLineParser)
    throws ParseException
  {
    if (paramString == null)
      throw new IllegalArgumentException("Value to parse may not be null.");
    if (paramLineParser == null)
      paramLineParser = DEFAULT;
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    return paramLineParser.parseProtocolVersion(localCharArrayBuffer, new ParserCursor(0, paramString.length()));
  }

  public static final RequestLine parseRequestLine(String paramString, LineParser paramLineParser)
    throws ParseException
  {
    if (paramString == null)
      throw new IllegalArgumentException("Value to parse may not be null.");
    if (paramLineParser == null)
      paramLineParser = DEFAULT;
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    return paramLineParser.parseRequestLine(localCharArrayBuffer, new ParserCursor(0, paramString.length()));
  }

  public static final StatusLine parseStatusLine(String paramString, LineParser paramLineParser)
    throws ParseException
  {
    if (paramString == null)
      throw new IllegalArgumentException("Value to parse may not be null.");
    if (paramLineParser == null)
      paramLineParser = DEFAULT;
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    return paramLineParser.parseStatusLine(localCharArrayBuffer, new ParserCursor(0, paramString.length()));
  }

  protected ProtocolVersion createProtocolVersion(int paramInt1, int paramInt2)
  {
    return this.protocol.forVersion(paramInt1, paramInt2);
  }

  protected RequestLine createRequestLine(String paramString1, String paramString2, ProtocolVersion paramProtocolVersion)
  {
    return new BasicRequestLine(paramString1, paramString2, paramProtocolVersion);
  }

  protected StatusLine createStatusLine(ProtocolVersion paramProtocolVersion, int paramInt, String paramString)
  {
    return new BasicStatusLine(paramProtocolVersion, paramInt, paramString);
  }

  public boolean hasProtocolVersion(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    if (paramCharArrayBuffer == null)
      throw new IllegalArgumentException("Char array buffer may not be null");
    if (paramParserCursor == null)
      throw new IllegalArgumentException("Parser cursor may not be null");
    int i = paramParserCursor.getPos();
    String str = this.protocol.getProtocol();
    int j = str.length();
    if (paramCharArrayBuffer.length() < j + 4)
      return false;
    if (i < 0)
      i = paramCharArrayBuffer.length() - 4 - j;
    while (4 + (i + j) > paramCharArrayBuffer.length())
    {
      return false;
      if (i == 0)
        while ((i < paramCharArrayBuffer.length()) && (HTTP.isWhitespace(paramCharArrayBuffer.charAt(i))))
          i++;
    }
    boolean bool = true;
    int k = 0;
    if ((bool) && (k < j))
    {
      if (paramCharArrayBuffer.charAt(i + k) == str.charAt(k));
      for (bool = true; ; bool = false)
      {
        k++;
        break;
      }
    }
    if (bool)
      if (paramCharArrayBuffer.charAt(i + j) != '/')
        break label195;
    label195: for (bool = true; ; bool = false)
      return bool;
  }

  public Header parseHeader(CharArrayBuffer paramCharArrayBuffer)
    throws ParseException
  {
    return new BufferedHeader(paramCharArrayBuffer);
  }

  // ERROR //
  public ProtocolVersion parseProtocolVersion(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
    throws ParseException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull +13 -> 14
    //   4: new 35	java/lang/IllegalArgumentException
    //   7: dup
    //   8: ldc 104
    //   10: invokespecial 40	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   13: athrow
    //   14: aload_2
    //   15: ifnonnull +13 -> 28
    //   18: new 35	java/lang/IllegalArgumentException
    //   21: dup
    //   22: ldc 106
    //   24: invokespecial 40	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   27: athrow
    //   28: aload_0
    //   29: getfield 29	org/apache/http/message/BasicLineParser:protocol	Lorg/apache/http/ProtocolVersion;
    //   32: invokevirtual 113	org/apache/http/ProtocolVersion:getProtocol	()Ljava/lang/String;
    //   35: astore_3
    //   36: aload_3
    //   37: invokevirtual 48	java/lang/String:length	()I
    //   40: istore 4
    //   42: aload_2
    //   43: invokevirtual 109	org/apache/http/message/ParserCursor:getPos	()I
    //   46: istore 5
    //   48: aload_2
    //   49: invokevirtual 135	org/apache/http/message/ParserCursor:getUpperBound	()I
    //   52: istore 6
    //   54: aload_0
    //   55: aload_1
    //   56: aload_2
    //   57: invokevirtual 139	org/apache/http/message/BasicLineParser:skipWhitespace	(Lorg/apache/http/util/CharArrayBuffer;Lorg/apache/http/message/ParserCursor;)V
    //   60: aload_2
    //   61: invokevirtual 109	org/apache/http/message/ParserCursor:getPos	()I
    //   64: istore 7
    //   66: iconst_4
    //   67: iload 7
    //   69: iload 4
    //   71: iadd
    //   72: iadd
    //   73: iload 6
    //   75: if_icmple +37 -> 112
    //   78: new 33	org/apache/http/ParseException
    //   81: dup
    //   82: new 141	java/lang/StringBuffer
    //   85: dup
    //   86: invokespecial 142	java/lang/StringBuffer:<init>	()V
    //   89: ldc 144
    //   91: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   94: aload_1
    //   95: iload 5
    //   97: iload 6
    //   99: invokevirtual 151	org/apache/http/util/CharArrayBuffer:substring	(II)Ljava/lang/String;
    //   102: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   105: invokevirtual 154	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   108: invokespecial 155	org/apache/http/ParseException:<init>	(Ljava/lang/String;)V
    //   111: athrow
    //   112: iconst_1
    //   113: istore 8
    //   115: iconst_0
    //   116: istore 9
    //   118: iload 8
    //   120: ifeq +43 -> 163
    //   123: iload 9
    //   125: iload 4
    //   127: if_icmpge +36 -> 163
    //   130: aload_1
    //   131: iload 7
    //   133: iload 9
    //   135: iadd
    //   136: invokevirtual 118	org/apache/http/util/CharArrayBuffer:charAt	(I)C
    //   139: aload_3
    //   140: iload 9
    //   142: invokevirtual 125	java/lang/String:charAt	(I)C
    //   145: if_icmpne +12 -> 157
    //   148: iconst_1
    //   149: istore 8
    //   151: iinc 9 1
    //   154: goto -36 -> 118
    //   157: iconst_0
    //   158: istore 8
    //   160: goto -9 -> 151
    //   163: iload 8
    //   165: ifeq +20 -> 185
    //   168: aload_1
    //   169: iload 7
    //   171: iload 4
    //   173: iadd
    //   174: invokevirtual 118	org/apache/http/util/CharArrayBuffer:charAt	(I)C
    //   177: bipush 47
    //   179: if_icmpne +45 -> 224
    //   182: iconst_1
    //   183: istore 8
    //   185: iload 8
    //   187: ifne +43 -> 230
    //   190: new 33	org/apache/http/ParseException
    //   193: dup
    //   194: new 141	java/lang/StringBuffer
    //   197: dup
    //   198: invokespecial 142	java/lang/StringBuffer:<init>	()V
    //   201: ldc 144
    //   203: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   206: aload_1
    //   207: iload 5
    //   209: iload 6
    //   211: invokevirtual 151	org/apache/http/util/CharArrayBuffer:substring	(II)Ljava/lang/String;
    //   214: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   217: invokevirtual 154	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   220: invokespecial 155	org/apache/http/ParseException:<init>	(Ljava/lang/String;)V
    //   223: athrow
    //   224: iconst_0
    //   225: istore 8
    //   227: goto -42 -> 185
    //   230: iload 7
    //   232: iload 4
    //   234: iconst_1
    //   235: iadd
    //   236: iadd
    //   237: istore 10
    //   239: aload_1
    //   240: bipush 46
    //   242: iload 10
    //   244: iload 6
    //   246: invokevirtual 159	org/apache/http/util/CharArrayBuffer:indexOf	(III)I
    //   249: istore 11
    //   251: iload 11
    //   253: iconst_m1
    //   254: if_icmpne +37 -> 291
    //   257: new 33	org/apache/http/ParseException
    //   260: dup
    //   261: new 141	java/lang/StringBuffer
    //   264: dup
    //   265: invokespecial 142	java/lang/StringBuffer:<init>	()V
    //   268: ldc 161
    //   270: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   273: aload_1
    //   274: iload 5
    //   276: iload 6
    //   278: invokevirtual 151	org/apache/http/util/CharArrayBuffer:substring	(II)Ljava/lang/String;
    //   281: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   284: invokevirtual 154	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   287: invokespecial 155	org/apache/http/ParseException:<init>	(Ljava/lang/String;)V
    //   290: athrow
    //   291: aload_1
    //   292: iload 10
    //   294: iload 11
    //   296: invokevirtual 164	org/apache/http/util/CharArrayBuffer:substringTrimmed	(II)Ljava/lang/String;
    //   299: invokestatic 170	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   302: istore 13
    //   304: iload 11
    //   306: iconst_1
    //   307: iadd
    //   308: istore 14
    //   310: aload_1
    //   311: bipush 32
    //   313: iload 14
    //   315: iload 6
    //   317: invokevirtual 159	org/apache/http/util/CharArrayBuffer:indexOf	(III)I
    //   320: istore 15
    //   322: iload 15
    //   324: iconst_m1
    //   325: if_icmpne +7 -> 332
    //   328: iload 6
    //   330: istore 15
    //   332: aload_1
    //   333: iload 14
    //   335: iload 15
    //   337: invokevirtual 164	org/apache/http/util/CharArrayBuffer:substringTrimmed	(II)Ljava/lang/String;
    //   340: invokestatic 170	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   343: istore 17
    //   345: aload_2
    //   346: iload 15
    //   348: invokevirtual 173	org/apache/http/message/ParserCursor:updatePos	(I)V
    //   351: aload_0
    //   352: iload 13
    //   354: iload 17
    //   356: invokevirtual 175	org/apache/http/message/BasicLineParser:createProtocolVersion	(II)Lorg/apache/http/ProtocolVersion;
    //   359: areturn
    //   360: astore 12
    //   362: new 33	org/apache/http/ParseException
    //   365: dup
    //   366: new 141	java/lang/StringBuffer
    //   369: dup
    //   370: invokespecial 142	java/lang/StringBuffer:<init>	()V
    //   373: ldc 177
    //   375: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   378: aload_1
    //   379: iload 5
    //   381: iload 6
    //   383: invokevirtual 151	org/apache/http/util/CharArrayBuffer:substring	(II)Ljava/lang/String;
    //   386: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   389: invokevirtual 154	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   392: invokespecial 155	org/apache/http/ParseException:<init>	(Ljava/lang/String;)V
    //   395: athrow
    //   396: astore 16
    //   398: new 33	org/apache/http/ParseException
    //   401: dup
    //   402: new 141	java/lang/StringBuffer
    //   405: dup
    //   406: invokespecial 142	java/lang/StringBuffer:<init>	()V
    //   409: ldc 179
    //   411: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   414: aload_1
    //   415: iload 5
    //   417: iload 6
    //   419: invokevirtual 151	org/apache/http/util/CharArrayBuffer:substring	(II)Ljava/lang/String;
    //   422: invokevirtual 147	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   425: invokevirtual 154	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   428: invokespecial 155	org/apache/http/ParseException:<init>	(Ljava/lang/String;)V
    //   431: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   291	304	360	java/lang/NumberFormatException
    //   332	345	396	java/lang/NumberFormatException
  }

  public RequestLine parseRequestLine(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
    throws ParseException
  {
    if (paramCharArrayBuffer == null)
      throw new IllegalArgumentException("Char array buffer may not be null");
    if (paramParserCursor == null)
      throw new IllegalArgumentException("Parser cursor may not be null");
    int i = paramParserCursor.getPos();
    int j = paramParserCursor.getUpperBound();
    int k;
    int m;
    try
    {
      skipWhitespace(paramCharArrayBuffer, paramParserCursor);
      k = paramParserCursor.getPos();
      m = paramCharArrayBuffer.indexOf(32, k, j);
      if (m < 0)
        throw new ParseException("Invalid request line: " + paramCharArrayBuffer.substring(i, j));
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
    {
      throw new ParseException("Invalid request line: " + paramCharArrayBuffer.substring(i, j));
    }
    String str1 = paramCharArrayBuffer.substringTrimmed(k, m);
    paramParserCursor.updatePos(m);
    skipWhitespace(paramCharArrayBuffer, paramParserCursor);
    int n = paramParserCursor.getPos();
    int i1 = paramCharArrayBuffer.indexOf(32, n, j);
    if (i1 < 0)
      throw new ParseException("Invalid request line: " + paramCharArrayBuffer.substring(i, j));
    String str2 = paramCharArrayBuffer.substringTrimmed(n, i1);
    paramParserCursor.updatePos(i1);
    ProtocolVersion localProtocolVersion = parseProtocolVersion(paramCharArrayBuffer, paramParserCursor);
    skipWhitespace(paramCharArrayBuffer, paramParserCursor);
    if (!paramParserCursor.atEnd())
      throw new ParseException("Invalid request line: " + paramCharArrayBuffer.substring(i, j));
    RequestLine localRequestLine = createRequestLine(str1, str2, localProtocolVersion);
    return localRequestLine;
  }

  public StatusLine parseStatusLine(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
    throws ParseException
  {
    if (paramCharArrayBuffer == null)
      throw new IllegalArgumentException("Char array buffer may not be null");
    if (paramParserCursor == null)
      throw new IllegalArgumentException("Parser cursor may not be null");
    int i = paramParserCursor.getPos();
    int j = paramParserCursor.getUpperBound();
    ProtocolVersion localProtocolVersion;
    int m;
    String str1;
    while (true)
    {
      int n;
      try
      {
        localProtocolVersion = parseProtocolVersion(paramCharArrayBuffer, paramParserCursor);
        skipWhitespace(paramCharArrayBuffer, paramParserCursor);
        int k = paramParserCursor.getPos();
        m = paramCharArrayBuffer.indexOf(32, k, j);
        if (m < 0)
          m = j;
        str1 = paramCharArrayBuffer.substringTrimmed(k, m);
        n = 0;
        if (n >= str1.length())
          break;
        if (!Character.isDigit(str1.charAt(n)))
          throw new ParseException("Status line contains invalid status code: " + paramCharArrayBuffer.substring(i, j));
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        throw new ParseException("Invalid status line: " + paramCharArrayBuffer.substring(i, j));
      }
      n++;
    }
    while (true)
    {
      try
      {
        int i1 = Integer.parseInt(str1);
        int i2 = m;
        if (i2 < j)
        {
          str2 = paramCharArrayBuffer.substringTrimmed(i2, j);
          return createStatusLine(localProtocolVersion, i1, str2);
        }
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw new ParseException("Status line contains invalid status code: " + paramCharArrayBuffer.substring(i, j));
      }
      String str2 = "";
    }
  }

  protected void skipWhitespace(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    int i = paramParserCursor.getPos();
    int j = paramParserCursor.getUpperBound();
    while ((i < j) && (HTTP.isWhitespace(paramCharArrayBuffer.charAt(i))))
      i++;
    paramParserCursor.updatePos(i);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.message.BasicLineParser
 * JD-Core Version:    0.6.2
 */