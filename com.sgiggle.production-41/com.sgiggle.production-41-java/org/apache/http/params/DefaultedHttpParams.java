package org.apache.http.params;

public final class DefaultedHttpParams extends AbstractHttpParams
{
  private final HttpParams defaults;
  private final HttpParams local;

  public DefaultedHttpParams(HttpParams paramHttpParams1, HttpParams paramHttpParams2)
  {
    if (paramHttpParams1 == null)
      throw new IllegalArgumentException("HTTP parameters may not be null");
    this.local = paramHttpParams1;
    this.defaults = paramHttpParams2;
  }

  public HttpParams copy()
  {
    return new DefaultedHttpParams(this.local.copy(), this.defaults);
  }

  public HttpParams getDefaults()
  {
    return this.defaults;
  }

  public Object getParameter(String paramString)
  {
    Object localObject = this.local.getParameter(paramString);
    if ((localObject == null) && (this.defaults != null))
      localObject = this.defaults.getParameter(paramString);
    return localObject;
  }

  public boolean removeParameter(String paramString)
  {
    return this.local.removeParameter(paramString);
  }

  public HttpParams setParameter(String paramString, Object paramObject)
  {
    return this.local.setParameter(paramString, paramObject);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.params.DefaultedHttpParams
 * JD-Core Version:    0.6.2
 */