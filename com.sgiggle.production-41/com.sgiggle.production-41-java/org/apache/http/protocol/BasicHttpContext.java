package org.apache.http.protocol;

import java.util.HashMap;
import java.util.Map;

public class BasicHttpContext
  implements HttpContext
{
  private Map map = null;
  private final HttpContext parentContext;

  public BasicHttpContext()
  {
    this(null);
  }

  public BasicHttpContext(HttpContext paramHttpContext)
  {
    this.parentContext = paramHttpContext;
  }

  public Object getAttribute(String paramString)
  {
    if (paramString == null)
      throw new IllegalArgumentException("Id may not be null");
    Map localMap = this.map;
    Object localObject = null;
    if (localMap != null)
      localObject = this.map.get(paramString);
    if ((localObject == null) && (this.parentContext != null))
      return this.parentContext.getAttribute(paramString);
    return localObject;
  }

  public Object removeAttribute(String paramString)
  {
    if (paramString == null)
      throw new IllegalArgumentException("Id may not be null");
    if (this.map != null)
      return this.map.remove(paramString);
    return null;
  }

  public void setAttribute(String paramString, Object paramObject)
  {
    if (paramString == null)
      throw new IllegalArgumentException("Id may not be null");
    if (this.map == null)
      this.map = new HashMap();
    this.map.put(paramString, paramObject);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.protocol.BasicHttpContext
 * JD-Core Version:    0.6.2
 */