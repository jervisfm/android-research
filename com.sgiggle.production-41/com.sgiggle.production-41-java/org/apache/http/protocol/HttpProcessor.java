package org.apache.http.protocol;

import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponseInterceptor;

public abstract interface HttpProcessor extends HttpRequestInterceptor, HttpResponseInterceptor
{
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.protocol.HttpProcessor
 * JD-Core Version:    0.6.2
 */