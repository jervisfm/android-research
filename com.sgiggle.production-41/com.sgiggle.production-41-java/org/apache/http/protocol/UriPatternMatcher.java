package org.apache.http.protocol;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class UriPatternMatcher
{
  private final Map map = new HashMap();

  public Object lookup(String paramString)
  {
    if (paramString == null)
      try
      {
        throw new IllegalArgumentException("Request URI may not be null");
      }
      finally
      {
      }
    int i = paramString.indexOf("?");
    if (i != -1)
      paramString = paramString.substring(0, i);
    Object localObject1 = this.map.get(paramString);
    if (localObject1 == null)
    {
      Object localObject2 = null;
      Iterator localIterator = this.map.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if ((matchUriRequestPattern(str, paramString)) && ((localObject2 == null) || (localObject2.length() < str.length()) || ((localObject2.length() == str.length()) && (str.endsWith("*")))))
        {
          Object localObject3 = this.map.get(str);
          localObject1 = localObject3;
          localObject2 = str;
        }
      }
    }
    return localObject1;
  }

  protected boolean matchUriRequestPattern(String paramString1, String paramString2)
  {
    if (paramString1.equals("*"))
      return true;
    return ((paramString1.endsWith("*")) && (paramString2.startsWith(paramString1.substring(0, paramString1.length() - 1)))) || ((paramString1.startsWith("*")) && (paramString2.endsWith(paramString1.substring(1, paramString1.length()))));
  }

  public void register(String paramString, Object paramObject)
  {
    if (paramString == null)
      try
      {
        throw new IllegalArgumentException("URI request pattern may not be null");
      }
      finally
      {
      }
    this.map.put(paramString, paramObject);
  }

  public void setHandlers(Map paramMap)
  {
    if (paramMap == null)
      try
      {
        throw new IllegalArgumentException("Map of handlers may not be null");
      }
      finally
      {
      }
    this.map.clear();
    this.map.putAll(paramMap);
  }

  public void setObjects(Map paramMap)
  {
    if (paramMap == null)
      try
      {
        throw new IllegalArgumentException("Map of handlers may not be null");
      }
      finally
      {
      }
    this.map.clear();
    this.map.putAll(paramMap);
  }

  public void unregister(String paramString)
  {
    if (paramString == null);
    while (true)
    {
      return;
      try
      {
        this.map.remove(paramString);
      }
      finally
      {
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.protocol.UriPatternMatcher
 * JD-Core Version:    0.6.2
 */