package org.apache.http.util;

import java.lang.reflect.Method;

public final class ExceptionUtils
{
  private static final Method INIT_CAUSE_METHOD = getInitCauseMethod();
  static Class class$java$lang$Throwable;

  static Class class$(String paramString)
  {
    try
    {
      Class localClass = Class.forName(paramString);
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  private static Method getInitCauseMethod()
  {
    try
    {
      Class[] arrayOfClass = new Class[1];
      Class localClass1;
      Class localClass2;
      if (class$java$lang$Throwable == null)
      {
        localClass1 = class$("java.lang.Throwable");
        class$java$lang$Throwable = localClass1;
        arrayOfClass[0] = localClass1;
        if (class$java$lang$Throwable != null)
          break label56;
        localClass2 = class$("java.lang.Throwable");
        class$java$lang$Throwable = localClass2;
      }
      while (true)
      {
        return localClass2.getMethod("initCause", arrayOfClass);
        localClass1 = class$java$lang$Throwable;
        break;
        label56: localClass2 = class$java$lang$Throwable;
      }
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
    }
    return null;
  }

  public static void initCause(Throwable paramThrowable1, Throwable paramThrowable2)
  {
    if (INIT_CAUSE_METHOD != null);
    try
    {
      INIT_CAUSE_METHOD.invoke(paramThrowable1, new Object[] { paramThrowable2 });
      return;
    }
    catch (Exception localException)
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.http.util.ExceptionUtils
 * JD-Core Version:    0.6.2
 */