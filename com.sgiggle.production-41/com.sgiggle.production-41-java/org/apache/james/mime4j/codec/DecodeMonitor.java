package org.apache.james.mime4j.codec;

public class DecodeMonitor
{
  public static final DecodeMonitor SILENT = new DecodeMonitor();
  public static final DecodeMonitor STRICT = new DecodeMonitor()
  {
    public boolean isListening()
    {
      return true;
    }

    public boolean warn(String paramAnonymousString1, String paramAnonymousString2)
    {
      return true;
    }
  };

  public boolean isListening()
  {
    return false;
  }

  public boolean warn(String paramString1, String paramString2)
  {
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.james.mime4j.codec.DecodeMonitor
 * JD-Core Version:    0.6.2
 */