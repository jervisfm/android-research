package org.apache.james.mime4j.io;

public abstract interface LineNumberSource
{
  public abstract int getLineNumber();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.james.mime4j.io.LineNumberSource
 * JD-Core Version:    0.6.2
 */