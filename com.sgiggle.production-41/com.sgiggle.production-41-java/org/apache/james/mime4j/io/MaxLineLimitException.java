package org.apache.james.mime4j.io;

import java.io.IOException;

public class MaxLineLimitException extends IOException
{
  private static final long serialVersionUID = 1855987166990764426L;

  public MaxLineLimitException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.james.mime4j.io.MaxLineLimitException
 * JD-Core Version:    0.6.2
 */