package org.apache.james.mime4j.stream;

public abstract interface BodyDescriptor extends ContentDescriptor
{
  public abstract String getBoundary();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.james.mime4j.stream.BodyDescriptor
 * JD-Core Version:    0.6.2
 */