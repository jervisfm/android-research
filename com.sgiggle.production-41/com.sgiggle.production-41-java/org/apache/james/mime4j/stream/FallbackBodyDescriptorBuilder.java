package org.apache.james.mime4j.stream;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.util.MimeUtil;

class FallbackBodyDescriptorBuilder
  implements BodyDescriptorBuilder
{
  private static final String DEFAULT_MEDIA_TYPE = "text";
  private static final String DEFAULT_MIME_TYPE = "text/plain";
  private static final String DEFAULT_SUB_TYPE = "plain";
  private static final String EMAIL_MESSAGE_MIME_TYPE = "message/rfc822";
  private static final String MEDIA_TYPE_MESSAGE = "message";
  private static final String MEDIA_TYPE_TEXT = "text";
  private static final String SUB_TYPE_EMAIL = "rfc822";
  private static final String US_ASCII = "us-ascii";
  private String boundary;
  private String charset;
  private long contentLength;
  private String mediaType;
  private String mimeType;
  private final DecodeMonitor monitor;
  private final String parentMimeType;
  private String subType;
  private String transferEncoding;

  public FallbackBodyDescriptorBuilder()
  {
    this(null, null);
  }

  public FallbackBodyDescriptorBuilder(String paramString, DecodeMonitor paramDecodeMonitor)
  {
    this.parentMimeType = paramString;
    if (paramDecodeMonitor != null);
    for (DecodeMonitor localDecodeMonitor = paramDecodeMonitor; ; localDecodeMonitor = DecodeMonitor.SILENT)
    {
      this.monitor = localDecodeMonitor;
      reset();
      return;
    }
  }

  private void parseContentType(Field paramField)
    throws MimeException
  {
    if ((paramField instanceof RawField));
    String str3;
    HashMap localHashMap;
    String str1;
    String str2;
    for (RawField localRawField = (RawField)paramField; ; localRawField = new RawField(str1, str2))
    {
      RawBody localRawBody = RawFieldParser.DEFAULT.parseRawBody(localRawField);
      str3 = localRawBody.getValue();
      localHashMap = new HashMap();
      Iterator localIterator = localRawBody.getParams().iterator();
      while (localIterator.hasNext())
      {
        NameValuePair localNameValuePair = (NameValuePair)localIterator.next();
        localHashMap.put(localNameValuePair.getName().toLowerCase(Locale.US), localNameValuePair.getValue());
      }
      str1 = paramField.getName();
      str2 = paramField.getBody();
    }
    String str4 = null;
    String str5 = null;
    if (str3 != null)
    {
      str3 = str3.toLowerCase().trim();
      int i = str3.indexOf('/');
      str4 = null;
      str5 = null;
      int j = 0;
      if (i != -1)
      {
        str5 = str3.substring(0, i).trim();
        int k = i + 1;
        str4 = str3.substring(k).trim();
        int m = str5.length();
        j = 0;
        if (m > 0)
        {
          int n = str4.length();
          j = 0;
          if (n > 0)
          {
            str3 = str5 + "/" + str4;
            j = 1;
          }
        }
      }
      if (j == 0)
      {
        str3 = null;
        str5 = null;
        str4 = null;
      }
    }
    String str6 = (String)localHashMap.get("boundary");
    if ((str3 != null) && (((str3.startsWith("multipart/")) && (str6 != null)) || (!str3.startsWith("multipart/"))))
    {
      this.mimeType = str3;
      this.mediaType = str5;
      this.subType = str4;
    }
    if (MimeUtil.isMultipart(this.mimeType))
      this.boundary = str6;
    String str7 = (String)localHashMap.get("charset");
    this.charset = null;
    if (str7 != null)
    {
      String str8 = str7.trim();
      if (str8.length() > 0)
        this.charset = str8;
    }
  }

  public Field addField(RawField paramRawField)
    throws MimeException
  {
    String str1 = paramRawField.getName().toLowerCase(Locale.US);
    if ((str1.equals("content-transfer-encoding")) && (this.transferEncoding == null))
    {
      String str4 = paramRawField.getBody();
      if (str4 != null)
      {
        String str5 = str4.trim().toLowerCase(Locale.US);
        if (str5.length() > 0)
          this.transferEncoding = str5;
      }
    }
    while (true)
    {
      return null;
      if ((str1.equals("content-length")) && (this.contentLength == -1L))
      {
        String str2 = paramRawField.getBody();
        if (str2 != null)
        {
          String str3 = str2.trim();
          try
          {
            this.contentLength = Long.parseLong(str3.trim());
          }
          catch (NumberFormatException localNumberFormatException)
          {
          }
          if (this.monitor.warn("Invalid content length: " + str3, "ignoring Content-Length header"))
            throw new MimeException("Invalid Content-Length header: " + str3);
        }
      }
      else if ((str1.equals("content-type")) && (this.mimeType == null))
      {
        parseContentType(paramRawField);
      }
    }
  }

  public BodyDescriptor build()
  {
    String str1 = this.mimeType;
    String str2 = this.mediaType;
    String str3 = this.subType;
    String str4 = this.charset;
    String str5;
    if (str1 == null)
    {
      if (MimeUtil.isSameMimeType("multipart/digest", this.parentMimeType))
      {
        str1 = "message/rfc822";
        str2 = "message";
        str3 = "rfc822";
      }
    }
    else
    {
      if ((str4 == null) && ("text".equals(str2)))
        str4 = "us-ascii";
      str5 = this.boundary;
      if (this.transferEncoding == null)
        break label116;
    }
    label116: for (String str6 = this.transferEncoding; ; str6 = "7bit")
    {
      return new BasicBodyDescriptor(str1, str2, str3, str5, str4, str6, this.contentLength);
      str1 = "text/plain";
      str2 = "text";
      str3 = "plain";
      break;
    }
  }

  public BodyDescriptorBuilder newChild()
  {
    return new FallbackBodyDescriptorBuilder(this.mimeType, this.monitor);
  }

  public void reset()
  {
    this.mimeType = null;
    this.subType = null;
    this.mediaType = null;
    this.boundary = null;
    this.charset = null;
    this.transferEncoding = null;
    this.contentLength = -1L;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.sgiggle.production-41\classes_dex2jar.jar
 * Qualified Name:     org.apache.james.mime4j.stream.FallbackBodyDescriptorBuilder
 * JD-Core Version:    0.6.2
 */