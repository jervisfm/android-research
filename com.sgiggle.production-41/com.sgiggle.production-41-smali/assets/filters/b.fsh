// holga
precision mediump float;
varying vec2 vTextureCoord;
uniform sampler2D sTexture1;
void main() {
    vec4 c4 = texture2D(sTexture1, vTextureCoord);
    const float ratio = 0.99;
    float c = 0.257 * c4.x + 0.504 * c4.y + 0.098 * c4.z;
    c = 2.0 * c - 0.2;
    vec4 s4 = vec4(c, c, c, 1.0) / ratio;
    vec2 coord = vTextureCoord;
    gl_FragColor = s4 * sin( coord.x * 3.14) * sin( coord.y * 3.14);
}
