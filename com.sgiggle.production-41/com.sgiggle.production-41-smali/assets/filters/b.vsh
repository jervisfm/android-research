uniform mat4 uMVPMatrix;
uniform vec2 uCRatio;
attribute vec4 aPosition;
attribute vec2 aTextureCoord;
varying vec2 vTextureCoord;
void main() {
    vec4 scaledPos = aPosition;
    scaledPos.xy = scaledPos.xy * uCRatio;
    gl_Position = uMVPMatrix * scaledPos;
    vTextureCoord = aTextureCoord;
}
