// russian mafia
precision mediump float;
varying vec2 vTextureCoord;
uniform sampler2D sTexture1;
void main() {
    vec4 c4 = texture2D(sTexture1, vTextureCoord);
    float c = 0.257 * c4.x + 0.504 * c4.y + 0.098 * c4.z + 0.0625;
    float d = step(0.3,c);
    float e = step(0.6,c);
    float f = 0.0;
    if (d > 0.0)
        f += 0.95;
    if (e > 0.0)
        f += 0.05;
    gl_FragColor = vec4(f, f, f, 1.0);
}
