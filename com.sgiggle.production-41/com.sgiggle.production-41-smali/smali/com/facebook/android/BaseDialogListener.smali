.class public abstract Lcom/facebook/android/BaseDialogListener;
.super Ljava/lang/Object;
.source "BaseDialogListener.java"

# interfaces
.implements Lcom/facebook/android/Facebook$DialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public onError(Lcom/facebook/android/DialogError;)V
    .locals 0
    .parameter

    .prologue
    .line 18
    invoke-virtual {p1}, Lcom/facebook/android/DialogError;->printStackTrace()V

    .line 19
    return-void
.end method

.method public onFacebookError(Lcom/facebook/android/FacebookError;)V
    .locals 0
    .parameter

    .prologue
    .line 14
    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->printStackTrace()V

    .line 15
    return-void
.end method
