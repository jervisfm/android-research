.class public Lcom/facebook/android/LoginButton$ButtonOnClickListener;
.super Ljava/lang/Object;
.source "LoginButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/android/LoginButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ButtonOnClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/facebook/android/LoginButton;


# direct methods
.method public constructor <init>(Lcom/facebook/android/LoginButton;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 85
    iget-object v0, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    #getter for: Lcom/facebook/android/LoginButton;->mFb:Lcom/facebook/android/Facebook;
    invoke-static {v0}, Lcom/facebook/android/LoginButton;->access$100(Lcom/facebook/android/LoginButton;)Lcom/facebook/android/Facebook;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    #getter for: Lcom/facebook/android/LoginButton;->mIsLogoutAllowed:Z
    invoke-static {v0}, Lcom/facebook/android/LoginButton;->access$200(Lcom/facebook/android/LoginButton;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-static {}, Lcom/facebook/android/SessionEvents;->onLogoutBegin()V

    .line 87
    new-instance v0, Lcom/facebook/android/AsyncFacebookRunner;

    iget-object v1, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    #getter for: Lcom/facebook/android/LoginButton;->mFb:Lcom/facebook/android/Facebook;
    invoke-static {v1}, Lcom/facebook/android/LoginButton;->access$100(Lcom/facebook/android/LoginButton;)Lcom/facebook/android/Facebook;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/android/AsyncFacebookRunner;-><init>(Lcom/facebook/android/Facebook;)V

    .line 88
    iget-object v1, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    invoke-virtual {v1}, Lcom/facebook/android/LoginButton;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/android/LoginButton$LogoutRequestListener;

    iget-object v3, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    invoke-direct {v2, v3, v5}, Lcom/facebook/android/LoginButton$LogoutRequestListener;-><init>(Lcom/facebook/android/LoginButton;Lcom/facebook/android/LoginButton$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/android/AsyncFacebookRunner;->logout(Landroid/content/Context;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 93
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    #getter for: Lcom/facebook/android/LoginButton;->mFb:Lcom/facebook/android/Facebook;
    invoke-static {v0}, Lcom/facebook/android/LoginButton;->access$100(Lcom/facebook/android/LoginButton;)Lcom/facebook/android/Facebook;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    iget-object v1, v1, Lcom/facebook/android/LoginButton;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    #getter for: Lcom/facebook/android/LoginButton;->mPermissions:[Ljava/lang/String;
    invoke-static {v2}, Lcom/facebook/android/LoginButton;->access$400(Lcom/facebook/android/LoginButton;)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/facebook/android/LoginButton$LoginDialogListener;

    iget-object v4, p0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;->this$0:Lcom/facebook/android/LoginButton;

    invoke-direct {v3, v4, v5}, Lcom/facebook/android/LoginButton$LoginDialogListener;-><init>(Lcom/facebook/android/LoginButton;Lcom/facebook/android/LoginButton$1;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/android/Facebook;->authorize(Landroid/app/Activity;[Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;)V

    goto :goto_0
.end method
