.class final Lcom/facebook/android/LoginButton$LoginDialogListener;
.super Ljava/lang/Object;
.source "LoginButton.java"

# interfaces
.implements Lcom/facebook/android/Facebook$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/android/LoginButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LoginDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/facebook/android/LoginButton;


# direct methods
.method private constructor <init>(Lcom/facebook/android/LoginButton;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/facebook/android/LoginButton$LoginDialogListener;->this$0:Lcom/facebook/android/LoginButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/facebook/android/LoginButton;Lcom/facebook/android/LoginButton$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/facebook/android/LoginButton$LoginDialogListener;-><init>(Lcom/facebook/android/LoginButton;)V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    .prologue
    .line 110
    const-string v0, "Action Canceled"

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->onLoginError(Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public onComplete(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 98
    invoke-static {}, Lcom/facebook/android/SessionEvents;->onLoginSuccess()V

    .line 99
    return-void
.end method

.method public onError(Lcom/facebook/android/DialogError;)V
    .locals 1
    .parameter

    .prologue
    .line 106
    invoke-virtual {p1}, Lcom/facebook/android/DialogError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->onLoginError(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public onFacebookError(Lcom/facebook/android/FacebookError;)V
    .locals 1
    .parameter

    .prologue
    .line 102
    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->onLoginError(Ljava/lang/String;)V

    .line 103
    return-void
.end method
