.class Lcom/facebook/android/LoginButton$SessionListener;
.super Ljava/lang/Object;
.source "LoginButton.java"

# interfaces
.implements Lcom/facebook/android/SessionEvents$AuthListener;
.implements Lcom/facebook/android/SessionEvents$LogoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/android/LoginButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/facebook/android/LoginButton;


# direct methods
.method private constructor <init>(Lcom/facebook/android/LoginButton;)V
    .locals 0
    .parameter

    .prologue
    .line 126
    iput-object p1, p0, Lcom/facebook/android/LoginButton$SessionListener;->this$0:Lcom/facebook/android/LoginButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/facebook/android/LoginButton;Lcom/facebook/android/LoginButton$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/facebook/android/LoginButton$SessionListener;-><init>(Lcom/facebook/android/LoginButton;)V

    return-void
.end method


# virtual methods
.method public onAuthFail(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 137
    return-void
.end method

.method public onAuthSucceed()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/facebook/android/LoginButton$SessionListener;->this$0:Lcom/facebook/android/LoginButton;

    #getter for: Lcom/facebook/android/LoginButton;->mIsLogoutAllowed:Z
    invoke-static {v0}, Lcom/facebook/android/LoginButton;->access$200(Lcom/facebook/android/LoginButton;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/facebook/android/LoginButton$SessionListener;->this$0:Lcom/facebook/android/LoginButton;

    const v1, 0x7f020088

    invoke-virtual {v0, v1}, Lcom/facebook/android/LoginButton;->setBackgroundResource(I)V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/LoginButton$SessionListener;->this$0:Lcom/facebook/android/LoginButton;

    #getter for: Lcom/facebook/android/LoginButton;->mFb:Lcom/facebook/android/Facebook;
    invoke-static {v0}, Lcom/facebook/android/LoginButton;->access$100(Lcom/facebook/android/LoginButton;)Lcom/facebook/android/Facebook;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/android/LoginButton$SessionListener;->this$0:Lcom/facebook/android/LoginButton;

    invoke-virtual {v1}, Lcom/facebook/android/LoginButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/android/SessionStore;->save(Lcom/facebook/android/Facebook;Landroid/content/Context;)Z

    .line 134
    return-void
.end method

.method public onLogoutBegin()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public onLogoutFinish()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/facebook/android/LoginButton$SessionListener;->this$0:Lcom/facebook/android/LoginButton;

    invoke-virtual {v0}, Lcom/facebook/android/LoginButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/SessionStore;->clear(Landroid/content/Context;)V

    .line 144
    iget-object v0, p0, Lcom/facebook/android/LoginButton$SessionListener;->this$0:Lcom/facebook/android/LoginButton;

    const v1, 0x7f020074

    invoke-virtual {v0, v1}, Lcom/facebook/android/LoginButton;->setBackgroundResource(I)V

    .line 145
    return-void
.end method
