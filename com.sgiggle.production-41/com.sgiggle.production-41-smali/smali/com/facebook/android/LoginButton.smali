.class public Lcom/facebook/android/LoginButton;
.super Landroid/widget/ImageButton;
.source "LoginButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/android/LoginButton$1;,
        Lcom/facebook/android/LoginButton$SessionListener;,
        Lcom/facebook/android/LoginButton$LogoutRequestListener;,
        Lcom/facebook/android/LoginButton$LoginDialogListener;,
        Lcom/facebook/android/LoginButton$ButtonOnClickListener;
    }
.end annotation


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field private mFb:Lcom/facebook/android/Facebook;

.field private mHandler:Landroid/os/Handler;

.field private mIsLogoutAllowed:Z

.field private mPermissions:[Ljava/lang/String;

.field private mSessionListener:Lcom/facebook/android/LoginButton$SessionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Lcom/facebook/android/LoginButton$SessionListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/android/LoginButton$SessionListener;-><init>(Lcom/facebook/android/LoginButton;Lcom/facebook/android/LoginButton$1;)V

    iput-object v0, p0, Lcom/facebook/android/LoginButton;->mSessionListener:Lcom/facebook/android/LoginButton$SessionListener;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance v0, Lcom/facebook/android/LoginButton$SessionListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/android/LoginButton$SessionListener;-><init>(Lcom/facebook/android/LoginButton;Lcom/facebook/android/LoginButton$1;)V

    iput-object v0, p0, Lcom/facebook/android/LoginButton;->mSessionListener:Lcom/facebook/android/LoginButton$SessionListener;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v0, Lcom/facebook/android/LoginButton$SessionListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/android/LoginButton$SessionListener;-><init>(Lcom/facebook/android/LoginButton;Lcom/facebook/android/LoginButton$1;)V

    iput-object v0, p0, Lcom/facebook/android/LoginButton;->mSessionListener:Lcom/facebook/android/LoginButton$SessionListener;

    .line 52
    return-void
.end method

.method static synthetic access$100(Lcom/facebook/android/LoginButton;)Lcom/facebook/android/Facebook;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/facebook/android/LoginButton;->mFb:Lcom/facebook/android/Facebook;

    return-object v0
.end method

.method static synthetic access$200(Lcom/facebook/android/LoginButton;)Z
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/facebook/android/LoginButton;->mIsLogoutAllowed:Z

    return v0
.end method

.method static synthetic access$400(Lcom/facebook/android/LoginButton;)[Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/facebook/android/LoginButton;->mPermissions:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/facebook/android/LoginButton;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/facebook/android/LoginButton;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public init(Landroid/app/Activity;Lcom/facebook/android/Facebook;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/facebook/android/LoginButton;->init(Landroid/app/Activity;Lcom/facebook/android/Facebook;[Ljava/lang/String;Z)V

    .line 56
    return-void
.end method

.method public init(Landroid/app/Activity;Lcom/facebook/android/Facebook;[Ljava/lang/String;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lcom/facebook/android/LoginButton;->mActivity:Landroid/app/Activity;

    .line 62
    iput-object p2, p0, Lcom/facebook/android/LoginButton;->mFb:Lcom/facebook/android/Facebook;

    .line 63
    iput-object p3, p0, Lcom/facebook/android/LoginButton;->mPermissions:[Ljava/lang/String;

    .line 64
    iput-boolean p4, p0, Lcom/facebook/android/LoginButton;->mIsLogoutAllowed:Z

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/LoginButton;->mHandler:Landroid/os/Handler;

    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/android/LoginButton;->setBackgroundColor(I)V

    .line 68
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/android/LoginButton;->setAdjustViewBounds(Z)V

    .line 69
    invoke-virtual {p2}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/android/LoginButton;->mIsLogoutAllowed:Z

    if-eqz v0, :cond_1

    const v0, 0x7f020088

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/android/LoginButton;->setBackgroundResource(I)V

    .line 72
    invoke-virtual {p0}, Lcom/facebook/android/LoginButton;->drawableStateChanged()V

    .line 74
    iget-object v0, p0, Lcom/facebook/android/LoginButton;->mSessionListener:Lcom/facebook/android/LoginButton$SessionListener;

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->addAuthListener(Lcom/facebook/android/SessionEvents$AuthListener;)V

    .line 75
    iget-boolean v0, p0, Lcom/facebook/android/LoginButton;->mIsLogoutAllowed:Z

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/facebook/android/LoginButton;->mSessionListener:Lcom/facebook/android/LoginButton$SessionListener;

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->addLogoutListener(Lcom/facebook/android/SessionEvents$LogoutListener;)V

    .line 79
    :cond_0
    new-instance v0, Lcom/facebook/android/LoginButton$ButtonOnClickListener;

    invoke-direct {v0, p0}, Lcom/facebook/android/LoginButton$ButtonOnClickListener;-><init>(Lcom/facebook/android/LoginButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/android/LoginButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void

    .line 69
    :cond_1
    const v0, 0x7f020074

    goto :goto_0
.end method
