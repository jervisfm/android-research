.class public Lcom/facebook/android/SessionEvents;
.super Ljava/lang/Object;
.source "SessionEvents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/android/SessionEvents$LogoutListener;,
        Lcom/facebook/android/SessionEvents$AuthListener;
    }
.end annotation


# static fields
.field private static mAuthListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/android/SessionEvents$AuthListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mLogoutListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/android/SessionEvents$LogoutListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    .line 25
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    return-void
.end method

.method public static addAuthListener(Lcom/facebook/android/SessionEvents$AuthListener;)V
    .locals 1
    .parameter

    .prologue
    .line 37
    sget-object v0, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method

.method public static addLogoutListener(Lcom/facebook/android/SessionEvents$LogoutListener;)V
    .locals 1
    .parameter

    .prologue
    .line 61
    sget-object v0, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public static onLoginError(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 83
    sget-object v0, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/SessionEvents$AuthListener;

    .line 84
    invoke-interface {v0, p0}, Lcom/facebook/android/SessionEvents$AuthListener;->onAuthFail(Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :cond_0
    return-void
.end method

.method public static onLoginSuccess()V
    .locals 2

    .prologue
    .line 77
    sget-object v0, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/SessionEvents$AuthListener;

    .line 78
    invoke-interface {v0}, Lcom/facebook/android/SessionEvents$AuthListener;->onAuthSucceed()V

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method

.method public static onLogoutBegin()V
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/SessionEvents$LogoutListener;

    .line 90
    invoke-interface {v0}, Lcom/facebook/android/SessionEvents$LogoutListener;->onLogoutBegin()V

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method

.method public static onLogoutFinish()V
    .locals 2

    .prologue
    .line 95
    sget-object v0, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/SessionEvents$LogoutListener;

    .line 96
    invoke-interface {v0}, Lcom/facebook/android/SessionEvents$LogoutListener;->onLogoutFinish()V

    goto :goto_0

    .line 98
    :cond_0
    return-void
.end method

.method public static removeAuthListener(Lcom/facebook/android/SessionEvents$AuthListener;)V
    .locals 1
    .parameter

    .prologue
    .line 49
    sget-object v0, Lcom/facebook/android/SessionEvents;->mAuthListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public static removeLogoutListener(Lcom/facebook/android/SessionEvents$LogoutListener;)V
    .locals 1
    .parameter

    .prologue
    .line 73
    sget-object v0, Lcom/facebook/android/SessionEvents;->mLogoutListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method
