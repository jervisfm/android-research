.class public Lcom/htc/lockscreen/telephony/PhoneState;
.super Ljava/lang/Object;
.source "PhoneState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CALL_STATE_IDLE:I = 0x0

.field public static final CALL_STATE_OFFHOOK:I = 0x2

.field public static final CALL_STATE_RING:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/htc/lockscreen/telephony/PhoneState;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_SENDMSG:I = 0x2

.field public static final FLAG_SILENT:I = 0x1


# instance fields
.field public mCallState:I

.field public mCallType:Ljava/lang/String;

.field public mDisplayNumber:Ljava/lang/String;

.field public mEventDesp:Ljava/lang/String;

.field public mEventIcon:Landroid/graphics/Bitmap;

.field public mFlag:I

.field public mHint:Ljava/lang/String;

.field public mId:I

.field public mLocation:Ljava/lang/String;

.field public mName:Ljava/lang/String;

.field public mOpName:Ljava/lang/String;

.field public mPackageName:Ljava/lang/String;

.field public mPhoto:Landroid/graphics/Bitmap;

.field public mSNIcon:Landroid/graphics/Bitmap;

.field public mSNStatus:Ljava/lang/String;

.field public mService:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 242
    new-instance v0, Lcom/htc/lockscreen/telephony/PhoneState$1;

    invoke-direct {v0}, Lcom/htc/lockscreen/telephony/PhoneState$1;-><init>()V

    .line 241
    sput-object v0, Lcom/htc/lockscreen/telephony/PhoneState;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 9
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .parameter "callState"
    .parameter "opName"
    .parameter "callType"
    .parameter "phone"
    .parameter "name"
    .parameter "displayNumber"
    .parameter "location"
    .parameter "hint"
    .parameter "snStatus"
    .parameter "snIcon"
    .parameter "eventDesp"
    .parameter "eventIcon"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallState:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mOpName:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallType:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mDisplayNumber:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mLocation:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mHint:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNStatus:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventDesp:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventIcon:Landroid/graphics/Bitmap;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPackageName:Ljava/lang/String;

    .line 32
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mId:I

    .line 33
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mService:Landroid/content/ComponentName;

    .line 34
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mFlag:I

    .line 48
    iput p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallState:I

    .line 49
    iput-object p2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mOpName:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallType:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    .line 52
    iput-object p5, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mName:Ljava/lang/String;

    .line 53
    iput-object p6, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mDisplayNumber:Ljava/lang/String;

    .line 54
    iput-object p7, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mLocation:Ljava/lang/String;

    .line 55
    iput-object p8, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mHint:Ljava/lang/String;

    .line 56
    iput-object p9, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNStatus:Ljava/lang/String;

    .line 57
    iput-object p10, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    .line 58
    iput-object p11, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventDesp:Ljava/lang/String;

    .line 59
    iput-object p12, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventIcon:Landroid/graphics/Bitmap;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallState:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mOpName:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallType:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mDisplayNumber:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mLocation:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mHint:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNStatus:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventDesp:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventIcon:Landroid/graphics/Bitmap;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPackageName:Ljava/lang/String;

    .line 32
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mId:I

    .line 33
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mService:Landroid/content/ComponentName;

    .line 34
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mFlag:I

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPackageName:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .parameter "parcel"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallState:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mOpName:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallType:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mDisplayNumber:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mLocation:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mHint:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNStatus:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventDesp:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventIcon:Landroid/graphics/Bitmap;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPackageName:Ljava/lang/String;

    .line 32
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mId:I

    .line 33
    iput-object v1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mService:Landroid/content/ComponentName;

    .line 34
    iput v2, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mFlag:I

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPackageName:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mId:I

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallState:I

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mOpName:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallType:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mName:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mDisplayNumber:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mLocation:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mHint:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNStatus:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventDesp:Ljava/lang/String;

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mFlag:I

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    .line 81
    sget-object v0, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    .line 84
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_1

    .line 85
    sget-object v0, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    .line 88
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_2

    .line 89
    sget-object v0, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventIcon:Landroid/graphics/Bitmap;

    .line 92
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_3

    .line 93
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mService:Landroid/content/ComponentName;

    .line 95
    :cond_3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    return v0
.end method

.method public getBirthdayInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventDesp:Ljava/lang/String;

    return-object v0
.end method

.method public getCallState()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallState:I

    return v0
.end method

.method public getDisplayNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mDisplayNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getFlag()I
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mFlag:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mId:I

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneComponent()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mService:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getPhoto()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSocailState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getSocialIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public setBirthdayInfo(Ljava/lang/String;)V
    .locals 0
    .parameter "birthday"

    .prologue
    .line 194
    iput-object p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventDesp:Ljava/lang/String;

    .line 195
    return-void
.end method

.method public setCallState(I)V
    .locals 0
    .parameter "callState"

    .prologue
    .line 230
    iput p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallState:I

    .line 231
    return-void
.end method

.method public setDisplayNumber(Ljava/lang/String;)V
    .locals 0
    .parameter "number"

    .prologue
    .line 162
    iput-object p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mDisplayNumber:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public setFlag(I)V
    .locals 0
    .parameter "flag"

    .prologue
    .line 222
    iput p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mFlag:I

    .line 223
    return-void
.end method

.method public setId(I)V
    .locals 0
    .parameter "id"

    .prologue
    .line 206
    iput p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mId:I

    .line 207
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .parameter "location"

    .prologue
    .line 186
    iput-object p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mLocation:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .parameter "name"

    .prologue
    .line 170
    iput-object p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mName:Ljava/lang/String;

    .line 171
    return-void
.end method

.method public setPhoneComponent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "packageName"
    .parameter "className"

    .prologue
    .line 214
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mService:Landroid/content/ComponentName;

    .line 215
    return-void
.end method

.method public setPhoto(Landroid/graphics/Bitmap;)V
    .locals 0
    .parameter "photo"

    .prologue
    .line 154
    iput-object p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    .line 155
    return-void
.end method

.method public setSocailState(Ljava/lang/String;)V
    .locals 0
    .parameter "social"

    .prologue
    .line 178
    iput-object p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNStatus:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setSocialIcon(Landroid/graphics/Bitmap;)V
    .locals 0
    .parameter "icon"

    .prologue
    .line 146
    iput-object p1, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    .line 147
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .parameter "parcel"
    .parameter "flags"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    iget v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mOpName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mCallType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mDisplayNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mHint:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventDesp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    iget v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mFlag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mPhoto:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 121
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mSNIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    .line 128
    :goto_1
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 129
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mEventIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    .line 136
    :goto_2
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mService:Landroid/content/ComponentName;

    if-eqz v0, :cond_3

    .line 137
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    iget-object v0, p0, Lcom/htc/lockscreen/telephony/PhoneState;->mService:Landroid/content/ComponentName;

    invoke-virtual {v0, p1, p2}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    .line 143
    :goto_3
    return-void

    .line 117
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 133
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 141
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3
.end method
