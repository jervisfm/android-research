.class Lcom/lge/secondcamera/DualCamera;
.super Ljava/lang/Object;
.source "LG_Revo_CameraEngine.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "dualcamera_library_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native setCamera(I)V
.end method


# virtual methods
.method public SelectCamera(I)Landroid/hardware/Camera;
    .locals 1
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/lge/secondcamera/DualCamera;->setCamera(I)V

    .line 52
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    return-object v0
.end method
