.class public final Lcom/lge/secondcamera/LG_Revo_CameraEngine;
.super Ljava/lang/Object;
.source "LG_Revo_CameraEngine.java"


# instance fields
.field private mCam:Landroid/hardware/Camera;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method


# virtual methods
.method public CloseCamera(Landroid/hardware/Camera;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    invoke-virtual {p1}, Landroid/hardware/Camera;->release()V

    .line 34
    return-void
.end method

.method public OpenCamera(I)Landroid/hardware/Camera;
    .locals 1
    .parameter

    .prologue
    .line 25
    new-instance v0, Lcom/lge/secondcamera/DualCamera;

    invoke-direct {v0}, Lcom/lge/secondcamera/DualCamera;-><init>()V

    .line 27
    invoke-virtual {v0, p1}, Lcom/lge/secondcamera/DualCamera;->SelectCamera(I)Landroid/hardware/Camera;

    move-result-object v0

    return-object v0
.end method
