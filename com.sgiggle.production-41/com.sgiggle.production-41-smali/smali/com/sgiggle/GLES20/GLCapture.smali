.class public Lcom/sgiggle/GLES20/GLCapture;
.super Ljava/lang/Object;
.source "GLCapture.java"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# static fields
.field private static final GL_TEXTURE_EXTERNAL_OES:I = 0x8d65

.field private static final METHOD_GET_TIMESTAMP:I = 0x0

.field private static final METHOD_RELEASE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GLCapture"

.field private static mInstance:Lcom/sgiggle/GLES20/GLCapture;


# instance fields
.field private mAvailable:Z

.field private mCamera:Landroid/hardware/Camera;

.field private mGLRenderer:Lcom/sgiggle/GLES20/GLRenderer;

.field private mHeight:I

.field private mMethods:[Ljava/lang/reflect/Method;

.field private mNullObjects:[Ljava/lang/Object;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTextures:[I

.field private mTimestamp:J

.field private mTransform:[F

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->hasGLCapture()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    new-instance v0, Lcom/sgiggle/GLES20/GLCapture;

    invoke-direct {v0}, Lcom/sgiggle/GLES20/GLCapture;-><init>()V

    sput-object v0, Lcom/sgiggle/GLES20/GLCapture;->mInstance:Lcom/sgiggle/GLES20/GLCapture;

    .line 135
    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mTextures:[I

    .line 26
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mTransform:[F

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/reflect/Method;

    iput-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mMethods:[Ljava/lang/reflect/Method;

    .line 32
    new-array v0, v1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mNullObjects:[Ljava/lang/Object;

    .line 37
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->getInstance()Lcom/sgiggle/GLES20/GLRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mGLRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    .line 39
    :try_start_0
    const-string v0, "android.graphics.SurfaceTexture"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 40
    iget-object v2, p0, Lcom/sgiggle/GLES20/GLCapture;->mMethods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    const-string v4, "getTimestamp"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    aput-object v0, v2, v3

    .line 41
    iget-object v2, p0, Lcom/sgiggle/GLES20/GLCapture;->mMethods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    const-string v4, "release"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    aput-object v0, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getInstance()Lcom/sgiggle/GLES20/GLCapture;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sgiggle/GLES20/GLCapture;->mInstance:Lcom/sgiggle/GLES20/GLCapture;

    return-object v0
.end method

.method private native render([FIIIJ)V
.end method

.method private declared-synchronized setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    .locals 1
    .parameter

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mCamera:Landroid/hardware/Camera;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 113
    :try_start_1
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 118
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized draw()V
    .locals 7

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mAvailable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 97
    :goto_0
    monitor-exit p0

    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mAvailable:Z

    .line 84
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 85
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/sgiggle/GLES20/GLCapture;->mTransform:[F

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 86
    const-wide/16 v1, 0x0

    .line 87
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mMethods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    aget-object v0, v0, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    .line 89
    :try_start_2
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mMethods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    iget-object v3, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v4, p0, Lcom/sgiggle/GLES20/GLCapture;->mNullObjects:[Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-wide v0

    .line 90
    const-wide/32 v2, 0xf4240

    :try_start_3
    div-long/2addr v0, v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-wide v5, v0

    .line 96
    :goto_1
    :try_start_4
    iget-object v1, p0, Lcom/sgiggle/GLES20/GLCapture;->mTransform:[F

    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mTextures:[I

    const/4 v2, 0x0

    aget v2, v0, v2

    iget v3, p0, Lcom/sgiggle/GLES20/GLCapture;->mWidth:I

    iget v4, p0, Lcom/sgiggle/GLES20/GLCapture;->mHeight:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLCapture;->render([FIIIJ)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 91
    :catch_0
    move-exception v0

    move-wide v0, v1

    :goto_2
    move-wide v5, v0

    .line 92
    goto :goto_1

    .line 94
    :cond_1
    :try_start_5
    iget-wide v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mTimestamp:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-wide v5, v0

    goto :goto_1

    .line 91
    :catch_1
    move-exception v2

    goto :goto_2
.end method

.method public declared-synchronized init()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 52
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/GLES20/GLCapture;->mTextures:[I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 53
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 54
    const v0, 0x8d65

    iget-object v1, p0, Lcom/sgiggle/GLES20/GLCapture;->mTextures:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 55
    const v0, 0x8d65

    const/16 v1, 0x2801

    const/16 v2, 0x2601

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 56
    const v0, 0x8d65

    const/16 v1, 0x2800

    const/16 v2, 0x2601

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 57
    const v0, 0x8d65

    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 58
    const v0, 0x8d65

    const/16 v1, 0x2803

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 59
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/sgiggle/GLES20/GLCapture;->mTextures:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 60
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 61
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-direct {p0, v0}, Lcom/sgiggle/GLES20/GLCapture;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    monitor-exit p0

    return v3

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 2
    .parameter

    .prologue
    .line 122
    monitor-enter p0

    .line 123
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mAvailable:Z

    .line 124
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mMethods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 125
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mTimestamp:J

    .line 127
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mGLRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/GLRenderer;->requestRender()V

    .line 129
    return-void

    .line 127
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized setCamera(Landroid/hardware/Camera;II)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sgiggle/GLES20/GLCapture;->mCamera:Landroid/hardware/Camera;

    .line 102
    iput p2, p0, Lcom/sgiggle/GLES20/GLCapture;->mWidth:I

    .line 103
    iput p3, p0, Lcom/sgiggle/GLES20/GLCapture;->mHeight:I

    .line 105
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-direct {p0, v0}, Lcom/sgiggle/GLES20/GLCapture;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :cond_0
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized uninit()V
    .locals 3

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mMethods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    aget-object v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 69
    :try_start_1
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mMethods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, Lcom/sgiggle/GLES20/GLCapture;->mNullObjects:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 74
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mAvailable:Z

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/GLES20/GLCapture;->mCamera:Landroid/hardware/Camera;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
