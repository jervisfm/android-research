.class Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;
.super Ljava/lang/Object;
.source "GLSurfaceViewEx.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$EGLConfigChooser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/GLES20/GLSurfaceViewEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConfigChooser"
.end annotation


# static fields
.field private static EGL_OPENGL_ES2_BIT:I

.field private static s_configAttribs2:[I


# instance fields
.field protected mAlphaSize:I

.field protected mBlueSize:I

.field protected mDepthSize:I

.field protected mGreenSize:I

.field protected mRedSize:I

.field protected mStencilSize:I

.field private mValue:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 82
    sput v3, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->EGL_OPENGL_ES2_BIT:I

    .line 83
    const/16 v0, 0x9

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3024

    aput v2, v0, v1

    const/4 v1, 0x1

    aput v3, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x3023

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v3, v0, v1

    const/16 v1, 0x3022

    aput v1, v0, v3

    const/4 v1, 0x5

    aput v3, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3040

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->EGL_OPENGL_ES2_BIT:I

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3038

    aput v2, v0, v1

    sput-object v0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->s_configAttribs2:[I

    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mValue:[I

    .line 70
    iput p1, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mRedSize:I

    .line 71
    iput p2, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mGreenSize:I

    .line 72
    iput p3, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mBlueSize:I

    .line 73
    iput p4, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mAlphaSize:I

    .line 74
    iput p5, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mDepthSize:I

    .line 75
    iput p6, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mStencilSize:I

    .line 76
    return-void
.end method

.method private findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mValue:[I

    invoke-interface {p1, p2, p3, p4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mValue:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 152
    :goto_0
    return v0

    :cond_0
    move v0, p5

    goto :goto_0
.end method

.method private printConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x21

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 167
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    .line 202
    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "EGL_BUFFER_SIZE"

    aput-object v2, v1, v8

    const-string v2, "EGL_ALPHA_SIZE"

    aput-object v2, v1, v9

    const-string v2, "EGL_BLUE_SIZE"

    aput-object v2, v1, v10

    const/4 v2, 0x3

    const-string v3, "EGL_GREEN_SIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "EGL_RED_SIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "EGL_DEPTH_SIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "EGL_STENCIL_SIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "EGL_CONFIG_CAVEAT"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "EGL_CONFIG_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "EGL_LEVEL"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "EGL_MAX_PBUFFER_HEIGHT"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "EGL_MAX_PBUFFER_PIXELS"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "EGL_MAX_PBUFFER_WIDTH"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "EGL_NATIVE_RENDERABLE"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "EGL_NATIVE_VISUAL_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "EGL_NATIVE_VISUAL_TYPE"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "EGL_PRESERVED_RESOURCES"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "EGL_SAMPLES"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "EGL_SAMPLE_BUFFERS"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "EGL_SURFACE_TYPE"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "EGL_TRANSPARENT_TYPE"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "EGL_TRANSPARENT_RED_VALUE"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "EGL_TRANSPARENT_GREEN_VALUE"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "EGL_TRANSPARENT_BLUE_VALUE"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "EGL_BIND_TO_TEXTURE_RGB"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "EGL_BIND_TO_TEXTURE_RGBA"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "EGL_MIN_SWAP_INTERVAL"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string v3, "EGL_MAX_SWAP_INTERVAL"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "EGL_LUMINANCE_SIZE"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "EGL_ALPHA_MASK_SIZE"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string v3, "EGL_COLOR_BUFFER_TYPE"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "EGL_RENDERABLE_TYPE"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string v3, "EGL_CONFORMANT"

    aput-object v3, v1, v2

    .line 237
    new-array v2, v9, [I

    move v3, v8

    .line 238
    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_2

    .line 239
    aget v4, v0, v3

    .line 240
    aget-object v5, v1, v3

    .line 241
    invoke-interface {p1, p2, p3, v4, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 242
    const-string v4, "GLSurfaceViewEx"

    const-string v6, "  %s: %d\n"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v5, v7, v8

    aget v5, v2, v8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 245
    :cond_1
    :goto_1
    invoke-interface {p1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    const/16 v5, 0x3000

    if-eq v4, v5, :cond_0

    goto :goto_1

    .line 248
    :cond_2
    return-void

    .line 167
    nop

    :array_0
    .array-data 0x4
        0x20t 0x30t 0x0t 0x0t
        0x21t 0x30t 0x0t 0x0t
        0x22t 0x30t 0x0t 0x0t
        0x23t 0x30t 0x0t 0x0t
        0x24t 0x30t 0x0t 0x0t
        0x25t 0x30t 0x0t 0x0t
        0x26t 0x30t 0x0t 0x0t
        0x27t 0x30t 0x0t 0x0t
        0x28t 0x30t 0x0t 0x0t
        0x29t 0x30t 0x0t 0x0t
        0x2at 0x30t 0x0t 0x0t
        0x2bt 0x30t 0x0t 0x0t
        0x2ct 0x30t 0x0t 0x0t
        0x2dt 0x30t 0x0t 0x0t
        0x2et 0x30t 0x0t 0x0t
        0x2ft 0x30t 0x0t 0x0t
        0x30t 0x30t 0x0t 0x0t
        0x31t 0x30t 0x0t 0x0t
        0x32t 0x30t 0x0t 0x0t
        0x33t 0x30t 0x0t 0x0t
        0x34t 0x30t 0x0t 0x0t
        0x37t 0x30t 0x0t 0x0t
        0x36t 0x30t 0x0t 0x0t
        0x35t 0x30t 0x0t 0x0t
        0x39t 0x30t 0x0t 0x0t
        0x3at 0x30t 0x0t 0x0t
        0x3bt 0x30t 0x0t 0x0t
        0x3ct 0x30t 0x0t 0x0t
        0x3dt 0x30t 0x0t 0x0t
        0x3et 0x30t 0x0t 0x0t
        0x3ft 0x30t 0x0t 0x0t
        0x40t 0x30t 0x0t 0x0t
        0x42t 0x30t 0x0t 0x0t
    .end array-data
.end method

.method private printConfigs(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 157
    array-length v0, p3

    .line 158
    const-string v1, "GLSurfaceViewEx"

    const-string v2, "%d configurations"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v6

    .line 159
    :goto_0
    if-ge v1, v0, :cond_0

    .line 160
    const-string v2, "GLSurfaceViewEx"

    const-string v3, "Configuration %d:\n"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    aget-object v2, p3, v1

    invoke-direct {p0, p1, p2, v2}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->printConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    :cond_0
    return-void
.end method


# virtual methods
.method public chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 96
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 97
    sget-object v2, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->s_configAttribs2:[I

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 99
    aget v4, v5, v4

    .line 101
    if-gtz v4, :cond_0

    .line 102
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No configs match configSpec"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 108
    sget-object v2, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->s_configAttribs2:[I

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 115
    invoke-virtual {p0, p1, p2, v3}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    return-object v0
.end method

.method public chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 120
    array-length v6, p3

    move v7, v5

    :goto_0
    if-ge v7, v6, :cond_2

    aget-object v3, p3, v7

    .line 121
    const/16 v4, 0x3025

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    .line 123
    const/16 v4, 0x3026

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    .line 127
    iget v1, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mDepthSize:I

    if-lt v8, v1, :cond_0

    iget v1, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mStencilSize:I

    if-ge v0, v1, :cond_1

    .line 120
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 131
    :cond_1
    const/16 v4, 0x3024

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    .line 133
    const/16 v4, 0x3023

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    .line 135
    const/16 v4, 0x3022

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v10

    .line 137
    const/16 v4, 0x3021

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    .line 140
    iget v1, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mRedSize:I

    if-ne v8, v1, :cond_0

    iget v1, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mGreenSize:I

    if-ne v9, v1, :cond_0

    iget v1, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mBlueSize:I

    if-ne v10, v1, :cond_0

    iget v1, p0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;->mAlphaSize:I

    if-ne v0, v1, :cond_0

    move-object v0, v3

    .line 143
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
