.class public Lcom/sgiggle/GLES20/GLSurfaceViewEx;
.super Landroid/opengl/GLSurfaceView;
.source "GLSurfaceViewEx.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/GLES20/GLSurfaceViewEx$1;,
        Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;,
        Lcom/sgiggle/GLES20/GLSurfaceViewEx$ContextFactory;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DEFAULT_DEPTH:I = 0x10

.field private static final DEFAULT_STENCIL:I = 0x0

.field private static final DEFAULT_TRANSLUCENT:Z = true

.field private static final TAG:Ljava/lang/String; = "GLSurfaceViewEx"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14
    invoke-static {p0, p1}, Lcom/sgiggle/GLES20/GLSurfaceViewEx;->checkEglError(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V

    return-void
.end method

.method private static checkEglError(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 62
    :goto_0
    invoke-interface {p1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 63
    const-string v1, "GLSurfaceViewEx"

    const-string v2, "%s: EGL error: 0x%x"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :cond_0
    return-void
.end method

.method public static init(Landroid/opengl/GLSurfaceView;Landroid/opengl/GLSurfaceView$Renderer;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 31
    const/4 v0, 0x1

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/sgiggle/GLES20/GLSurfaceViewEx;->init(Landroid/opengl/GLSurfaceView;Landroid/opengl/GLSurfaceView$Renderer;ZII)V

    .line 32
    return-void
.end method

.method public static init(Landroid/opengl/GLSurfaceView;Landroid/opengl/GLSurfaceView$Renderer;ZII)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x5

    const/4 v7, 0x0

    const/16 v1, 0x8

    .line 35
    new-instance v0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ContextFactory;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ContextFactory;-><init>(Lcom/sgiggle/GLES20/GLSurfaceViewEx$1;)V

    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView;->setEGLContextFactory(Landroid/opengl/GLSurfaceView$EGLContextFactory;)V

    .line 36
    if-eqz p2, :cond_0

    .line 37
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v2, -0x3

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 38
    new-instance v0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;-><init>(IIIIII)V

    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    .line 42
    :goto_0
    invoke-virtual {p0, p1}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 43
    invoke-virtual {p0, v7}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 44
    return-void

    .line 40
    :cond_0
    new-instance v0, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;

    const/4 v2, 0x6

    move v1, v4

    move v3, v4

    move v4, v7

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLSurfaceViewEx$ConfigChooser;-><init>(IIIIII)V

    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    goto :goto_0
.end method


# virtual methods
.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onDetachedFromWindow()V

    .line 28
    return-void
.end method
