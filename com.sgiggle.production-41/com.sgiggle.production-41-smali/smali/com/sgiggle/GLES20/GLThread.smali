.class public Lcom/sgiggle/GLES20/GLThread;
.super Ljava/lang/Object;
.source "GLThread.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native init()Z
.end method

.method public static native loadLibraries()I
.end method

.method public static native requestRender()V
.end method

.method public static native setSurface(Landroid/view/Surface;II)V
.end method

.method public static native uninit()V
.end method

.method public static native unloadLibraries()V
.end method
