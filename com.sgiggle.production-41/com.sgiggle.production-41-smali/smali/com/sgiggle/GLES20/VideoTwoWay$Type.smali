.class public interface abstract Lcom/sgiggle/GLES20/VideoTwoWay$Type;
.super Ljava/lang/Object;
.source "VideoTwoWay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/GLES20/VideoTwoWay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Type"
.end annotation


# static fields
.field public static final BORDER:I = 0x2

.field public static final CAFE:I = 0x3

.field public static final CAFE_ALTERNATE:I = 0x4

.field public static final INVALID:I = -0x1

.field public static final LAST:I = 0x6

.field public static final PLAYBACK:I = 0x0

.field public static final PREVIEW:I = 0x1

.field public static final TEST:I = 0x5
