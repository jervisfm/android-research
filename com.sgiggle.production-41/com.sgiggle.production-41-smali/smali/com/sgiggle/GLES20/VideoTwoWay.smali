.class public Lcom/sgiggle/GLES20/VideoTwoWay;
.super Ljava/lang/Object;
.source "VideoTwoWay.java"

# interfaces
.implements Lcom/sgiggle/GLES20/Renderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/GLES20/VideoTwoWay$Type;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoTwoWay"


# instance fields
.field private glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

.field private native_object:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    invoke-static {}, Lcom/sgiggle/GLES20/VideoTwoWay;->create()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    .line 10
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->getInstance()Lcom/sgiggle/GLES20/GLRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    .line 11
    return-void
.end method

.method private static native create()I
.end method

.method private static native destroy(I)V
.end method

.method private native isInside(IFF)Z
.end method

.method private native nativeDraw(I)V
.end method

.method private native nativeInit(III)V
.end method

.method private native nativeUninit(I)V
.end method

.method private native setView1(II)V
.end method

.method private native setView2(III)V
.end method

.method private native setView3(IIII)V
.end method

.method private native swapView(I)V
.end method


# virtual methods
.method public declared-synchronized draw()V
    .locals 1

    .prologue
    .line 36
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    if-eqz v0, :cond_0

    .line 37
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-direct {p0, v0}, Lcom/sgiggle/GLES20/VideoTwoWay;->nativeDraw(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :cond_0
    monitor-exit p0

    return-void

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized init(II)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 24
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    if-eqz v0, :cond_0

    .line 25
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-direct {p0, v0, p1, p2}, Lcom/sgiggle/GLES20/VideoTwoWay;->nativeInit(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    :cond_0
    monitor-exit p0

    return-void

    .line 24
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isInside(FF)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 77
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-direct {p0, v0, p1, p2}, Lcom/sgiggle/GLES20/VideoTwoWay;->isInside(IFF)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized release()V
    .locals 1

    .prologue
    .line 14
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-static {v0}, Lcom/sgiggle/GLES20/VideoTwoWay;->destroy(I)V

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16
    monitor-exit p0

    return-void

    .line 14
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSurface(Lcom/sgiggle/GLES20/GLSurfaceViewEx;)V
    .locals 1
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    invoke-virtual {v0, p1, p0}, Lcom/sgiggle/GLES20/GLRenderer;->init(Landroid/opengl/GLSurfaceView;Lcom/sgiggle/GLES20/Renderer;)V

    .line 20
    return-void
.end method

.method public setView(I)V
    .locals 1
    .parameter

    .prologue
    .line 55
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-direct {p0, v0, p1}, Lcom/sgiggle/GLES20/VideoTwoWay;->setView1(II)V

    .line 57
    iget-object v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/GLRenderer;->requestRender()V

    .line 58
    return-void
.end method

.method public setView(II)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-direct {p0, v0, p1, p2}, Lcom/sgiggle/GLES20/VideoTwoWay;->setView2(III)V

    .line 63
    iget-object v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/GLRenderer;->requestRender()V

    .line 64
    return-void
.end method

.method public setView(III)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/sgiggle/GLES20/VideoTwoWay;->setView3(IIII)V

    .line 69
    iget-object v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/GLRenderer;->requestRender()V

    .line 70
    return-void
.end method

.method public swapView()V
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-direct {p0, v0}, Lcom/sgiggle/GLES20/VideoTwoWay;->swapView(I)V

    .line 74
    return-void
.end method

.method public declared-synchronized uninit()V
    .locals 1

    .prologue
    .line 30
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    if-eqz v0, :cond_0

    .line 31
    iget v0, p0, Lcom/sgiggle/GLES20/VideoTwoWay;->native_object:I

    invoke-direct {p0, v0}, Lcom/sgiggle/GLES20/VideoTwoWay;->nativeUninit(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    :cond_0
    monitor-exit p0

    return-void

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
