.class public Lcom/sgiggle/VideoCapture/CameraWrapper;
.super Ljava/lang/Object;
.source "CameraWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;
    }
.end annotation


# static fields
.field private static final CLASS_CAMERA_INFO:I = 0x0

.field private static final FIELD_FACING:I = 0x0

.field private static final FIELD_ORIENTATION:I = 0x1

.field private static final METHOD_ADD_CALLBACK_BUFFER:I = 0x0

.field private static final METHOD_DELL_OPEN:I = 0xa

.field private static final METHOD_GET_CAMERA_INFO:I = 0x4

.field private static final METHOD_GET_FRONT_FACING_CAMERA:I = 0x3

.field private static final METHOD_GET_NUMBER_OF_CAMERAS:I = 0x5

.field private static final METHOD_HUAWEI_GET_CAMERA_NUM:I = 0x9

.field private static final METHOD_HUAWEI_GET_CURRENT_CAMERA:I = 0x8

.field private static final METHOD_HUAWEI_SET_CURRENT_CAMERA:I = 0x7

.field private static final METHOD_MOTOROLA_GET_FRONT_CAMERA:I = 0xb

.field private static final METHOD_OPEN:I = 0x6

.field private static final METHOD_RECONNECT:I = 0xc

.field private static final METHOD_SET_DISPLAY_ORIENTATION:I = 0x2

.field private static final METHOD_SET_PREVIEW_CALLBACK_WITH_BUFFER:I = 0x1

.field public static final TAG:Ljava/lang/String; = "CameraWrapper"

.field private static arg1:[Ljava/lang/Object;

.field private static camera:Landroid/hardware/Camera;

.field private static camera_count:I

.field private static classes:[Ljava/lang/Class;

.field private static fields:[Ljava/lang/reflect/Field;

.field private static hasBack:Z

.field private static hasFront:Z

.field private static instance:Lcom/sgiggle/VideoCapture/CameraWrapper;

.field public static isDell:Z

.field public static isDellStreak7:Z

.field public static isGingerbread:Z

.field public static isHuawei:Z

.field public static isLG:Z

.field public static isMotorola:Z

.field public static isPantech:Z

.field public static isSamsung:Z

.field public static isSamsungTablet:Z

.field public static isSprint:Z

.field private static methods:[Ljava/lang/reflect/Method;

.field private static oem_camera:Ljava/lang/Object;

.field private static screenHeight:I

.field private static screenWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 57
    new-array v0, v1, [Ljava/lang/Class;

    sput-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->classes:[Ljava/lang/Class;

    .line 58
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/reflect/Method;

    sput-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    .line 59
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/reflect/Field;

    sput-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->fields:[Ljava/lang/reflect/Field;

    .line 60
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    .line 77
    new-instance v0, Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-direct {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;-><init>()V

    sput-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->instance:Lcom/sgiggle/VideoCapture/CameraWrapper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-static {}, Lcom/sgiggle/VideoCapture/CameraWrapper;->init()V

    .line 95
    return-void
.end method

.method public static getCameraInfo(ILcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x5a

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isGingerbread:Z

    if-eqz v0, :cond_1

    .line 133
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->classes:[Ljava/lang/Class;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    .line 134
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 135
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->fields:[Ljava/lang/reflect/Field;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    iput v1, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    .line 137
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->fields:[Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    iput v0, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    .line 138
    const-string v0, "CameraWrapper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " facing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " orientation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 143
    :cond_1
    sget v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera_count:I

    if-ne v0, v2, :cond_3

    .line 144
    if-nez p0, :cond_0

    .line 145
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z

    if-eqz v0, :cond_2

    .line 146
    iput v1, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    .line 147
    iput v4, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    goto :goto_0

    .line 148
    :cond_2
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasFront:Z

    if-eqz v0, :cond_0

    .line 149
    iput v2, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    .line 150
    iput v1, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    goto :goto_0

    .line 153
    :cond_3
    sget v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera_count:I

    if-ne v0, v3, :cond_0

    .line 154
    if-nez p0, :cond_4

    .line 155
    iput v1, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    .line 156
    iput v4, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    goto :goto_0

    .line 157
    :cond_4
    if-ne p0, v2, :cond_0

    .line 158
    iput v2, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    .line 159
    iput v1, p1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    goto :goto_0
.end method

.method public static getNumberOfCameras()I
    .locals 1

    .prologue
    .line 170
    sget v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera_count:I

    return v0
.end method

.method private static init()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 307
    invoke-static {}, Lcom/sgiggle/VideoCapture/CameraWrapper;->initClass()V

    .line 308
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    .line 309
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isGingerbread:Z

    .line 332
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sgiggle/VideoCapture/CameraWrapper;->initCameraCount()V

    .line 333
    return-void

    .line 310
    :cond_1
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-M820-BST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 312
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isSamsung:Z

    goto :goto_0

    .line 313
    :cond_2
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 314
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isHuawei:Z

    goto :goto_0

    .line 315
    :cond_3
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    if-eqz v0, :cond_4

    .line 316
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isSprint:Z

    goto :goto_0

    .line 317
    :cond_4
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    if-eqz v0, :cond_5

    .line 318
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isDell:Z

    goto :goto_0

    .line 319
    :cond_5
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v1, 0xb

    aget-object v0, v0, v1

    if-eqz v0, :cond_6

    .line 320
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isMotorola:Z

    goto :goto_0

    .line 321
    :cond_6
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Dell Inc."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Dell Streak 7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 322
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isDellStreak7:Z

    goto :goto_0

    .line 323
    :cond_7
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P990"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-SU660"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "VS910 4G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P925"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P920"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-SU760"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P970"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-KU5900"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LGL85C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 325
    :cond_8
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isLG:Z

    .line 326
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "VS910 4G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    new-instance v0, Lcom/lge/secondcamera/LG_Revo_CameraEngine;

    invoke-direct {v0}, Lcom/lge/secondcamera/LG_Revo_CameraEngine;-><init>()V

    sput-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->oem_camera:Ljava/lang/Object;

    goto/16 :goto_0

    .line 330
    :cond_9
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 331
    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->isPantech:Z

    goto/16 :goto_0
.end method

.method private static initCameraCount()V
    .locals 7

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 410
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isGingerbread:Z

    if-eqz v0, :cond_2

    .line 413
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 414
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 488
    :cond_0
    :goto_0
    sput v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera_count:I

    .line 489
    return-void

    .line 415
    :catch_0
    move-exception v0

    .line 416
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    move v0, v5

    goto :goto_0

    .line 418
    :cond_2
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isSamsung:Z

    if-eqz v0, :cond_3

    .line 419
    add-int/lit8 v0, v5, 0x1

    .line 420
    sput-boolean v4, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z

    .line 421
    invoke-static {v4}, Lcom/sgiggle/VideoCapture/CameraWrapper;->openCameraSafe(I)Landroid/hardware/Camera;

    move-result-object v1

    .line 422
    if-eqz v1, :cond_0

    .line 424
    :try_start_1
    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V

    .line 425
    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 426
    add-int/lit8 v0, v0, 0x1

    .line 427
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasFront:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 430
    :goto_1
    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    goto :goto_0

    .line 432
    :cond_3
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isHuawei:Z

    if-eqz v0, :cond_8

    .line 433
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_7

    .line 434
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    if-eqz v0, :cond_6

    .line 437
    :try_start_2
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 438
    if-eqz v0, :cond_1

    .line 439
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-result v0

    .line 440
    if-ne v0, v4, :cond_5

    .line 441
    :try_start_3
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 442
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 443
    if-nez v1, :cond_4

    .line 444
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 452
    :catch_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 453
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_0

    .line 445
    :cond_4
    if-ne v1, v4, :cond_0

    .line 446
    const/4 v1, 0x1

    :try_start_4
    sput-boolean v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasFront:Z

    goto :goto_0

    .line 447
    :cond_5
    if-ne v0, v3, :cond_0

    .line 448
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z

    .line 449
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasFront:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 457
    :cond_6
    sput-boolean v4, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z

    move v0, v4

    goto/16 :goto_0

    .line 461
    :cond_7
    :try_start_5
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    .line 462
    new-instance v1, Ljava/io/FileInputStream;

    new-instance v2, Ljava/io/File;

    const-string v3, "/sys/camera/sensor"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 463
    if-eqz v1, :cond_1

    .line 464
    invoke-virtual {v0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 465
    const-string v2, "OUTER_CAMERA"

    invoke-virtual {v0, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_c

    .line 466
    add-int/lit8 v2, v5, 0x1

    .line 467
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z

    .line 469
    :goto_3
    const-string v3, "INNER_CAMERA"

    invoke-virtual {v0, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "true"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_b

    .line 470
    add-int/lit8 v0, v2, 0x1

    .line 471
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasFront:Z

    .line 473
    :goto_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 475
    :catch_2
    move-exception v0

    .line 477
    sput-boolean v4, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z

    move v0, v4

    .line 478
    goto/16 :goto_0

    .line 480
    :cond_8
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isSprint:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isDell:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isMotorola:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isDellStreak7:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isLG:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isPantech:Z

    if-eqz v0, :cond_a

    .line 482
    :cond_9
    sput-boolean v4, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z

    .line 483
    sput-boolean v4, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasFront:Z

    move v0, v3

    goto/16 :goto_0

    .line 486
    :cond_a
    sput-boolean v4, Lcom/sgiggle/VideoCapture/CameraWrapper;->hasBack:Z

    move v0, v4

    goto/16 :goto_0

    .line 452
    :catch_3
    move-exception v0

    move v1, v5

    goto/16 :goto_2

    .line 428
    :catch_4
    move-exception v2

    goto/16 :goto_1

    :cond_b
    move v0, v2

    goto :goto_4

    :cond_c
    move v2, v5

    goto :goto_3
.end method

.method private static initClass()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 340
    :try_start_0
    const-string v0, "android.hardware.Camera"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 341
    invoke-virtual {v0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    move v1, v5

    .line 342
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 344
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "addCallbackBuffer"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 345
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    aget-object v4, v0, v1

    aput-object v4, v2, v3

    .line 342
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 346
    :cond_1
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "setPreviewCallbackWithBuffer"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    .line 347
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    aget-object v4, v0, v1

    aput-object v4, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 366
    :catch_0
    move-exception v0

    .line 369
    :cond_2
    :try_start_1
    const-string v0, "android.hardware.Camera$CameraInfo"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 370
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->classes:[Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 371
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->fields:[Ljava/lang/reflect/Field;

    const/4 v2, 0x0

    const-string v3, "facing"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    aput-object v3, v1, v2

    .line 372
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->fields:[Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    const-string v3, "orientation"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    aput-object v0, v1, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 376
    :goto_2
    :try_start_2
    const-string v0, "com.sprint.hardware.twinCamDevice.FrontFacingCamera"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 377
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v2, 0x3

    const-string v3, "getFrontFacingCamera"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    aput-object v0, v1, v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 383
    :goto_3
    :try_start_3
    const-string v0, "android.hardware.HtcFrontFacingCamera"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 384
    invoke-virtual {v0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    move v2, v5

    .line 385
    :goto_4
    array-length v3, v1

    if-ge v2, v3, :cond_c

    .line 387
    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "setMirrorMode"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    .line 388
    sget-object v3, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v4, 0x3

    const-string v5, "getCamera"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    aput-object v5, v3, v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 385
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 348
    :cond_4
    :try_start_4
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "setDisplayOrientation"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_5

    .line 349
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x2

    aget-object v4, v0, v1

    aput-object v4, v2, v3

    goto/16 :goto_1

    .line 350
    :cond_5
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getCameraInfo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_6

    .line 351
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x4

    aget-object v4, v0, v1

    aput-object v4, v2, v3

    goto/16 :goto_1

    .line 352
    :cond_6
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getNumberOfCameras"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_7

    .line 353
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x5

    aget-object v4, v0, v1

    aput-object v4, v2, v3

    goto/16 :goto_1

    .line 354
    :cond_7
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->toGenericString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "public static android.hardware.Camera android.hardware.Camera.open(int)"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_8

    .line 355
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x6

    aget-object v4, v0, v1

    aput-object v4, v2, v3

    goto/16 :goto_1

    .line 356
    :cond_8
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "setCurrentCamera"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_9

    .line 357
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v3, 0x7

    aget-object v4, v0, v1

    aput-object v4, v2, v3

    goto/16 :goto_1

    .line 358
    :cond_9
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getCurrentCamera"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_a

    .line 359
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v3, 0x8

    aget-object v4, v0, v1

    aput-object v4, v2, v3

    goto/16 :goto_1

    .line 360
    :cond_a
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getCameraNum"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_b

    .line 361
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v3, 0x9

    aget-object v4, v0, v1

    aput-object v4, v2, v3

    goto/16 :goto_1

    .line 362
    :cond_b
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "reconnect"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 363
    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v3, 0xc

    aget-object v4, v0, v1

    aput-object v4, v2, v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_1

    .line 391
    :catch_1
    move-exception v0

    .line 394
    :cond_c
    :try_start_5
    const-string v0, "com.dell.android.hardware.CameraExtensions"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 395
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v2, 0xa

    const-string v3, "open"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    aput-object v0, v1, v2
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 399
    :goto_5
    :try_start_6
    const-string v0, "com.motorola.hardware.frontcamera.FrontCamera"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 400
    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v2, 0xb

    const-string v3, "getFrontCamera"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    aput-object v0, v1, v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 403
    :goto_6
    return-void

    .line 401
    :catch_2
    move-exception v0

    goto :goto_6

    .line 396
    :catch_3
    move-exception v0

    goto :goto_5

    .line 378
    :catch_4
    move-exception v0

    goto/16 :goto_3

    .line 373
    :catch_5
    move-exception v0

    goto/16 :goto_2
.end method

.method public static open(I)Lcom/sgiggle/VideoCapture/CameraWrapper;
    .locals 1
    .parameter

    .prologue
    .line 103
    invoke-static {p0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->openCameraSafe(I)Landroid/hardware/Camera;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    .line 104
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 105
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->instance:Lcom/sgiggle/VideoCapture/CameraWrapper;

    .line 107
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static openCameraSafe(I)Landroid/hardware/Camera;
    .locals 6
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 492
    .line 493
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isGingerbread:Z

    if-eqz v0, :cond_2

    .line 495
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 496
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    const/4 v1, 0x0

    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 647
    sget-boolean v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->isSamsungTablet:Z

    if-eqz v1, :cond_1

    .line 648
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 649
    const-string v2, "cam_mode"

    invoke-virtual {v1, v2, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 650
    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 653
    :cond_1
    return-object v0

    .line 497
    :catch_0
    move-exception v0

    .line 498
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 499
    goto :goto_0

    .line 500
    :cond_2
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isSamsung:Z

    if-eqz v0, :cond_4

    .line 502
    :try_start_1
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 506
    :goto_1
    if-eqz v0, :cond_0

    .line 507
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 508
    if-nez p0, :cond_3

    .line 509
    const-string v2, "camera-id"

    invoke-virtual {v1, v2, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 512
    :goto_2
    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0

    .line 503
    :catch_1
    move-exception v0

    .line 504
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    goto :goto_1

    .line 511
    :cond_3
    const-string v2, "camera-id"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    goto :goto_2

    .line 514
    :cond_4
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isSprint:Z

    if-eqz v0, :cond_6

    .line 515
    if-nez p0, :cond_5

    .line 517
    :try_start_2
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_0

    .line 518
    :catch_2
    move-exception v0

    .line 519
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 520
    goto :goto_0

    .line 523
    :cond_5
    :try_start_3
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 524
    :catch_3
    move-exception v0

    .line 525
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 526
    goto :goto_0

    .line 528
    :cond_6
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isHuawei:Z

    if-eqz v0, :cond_9

    .line 529
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_7

    .line 531
    :try_start_4
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 532
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    const/4 v1, 0x0

    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_e

    .line 536
    :goto_3
    :try_start_5
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    move-result-object v0

    .line 555
    :goto_4
    if-eqz v0, :cond_0

    if-ne p0, v5, :cond_0

    .line 556
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 557
    const-string v2, "mirror"

    const-string v3, "enable"

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto/16 :goto_0

    .line 537
    :catch_4
    move-exception v0

    .line 538
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 539
    goto :goto_4

    .line 541
    :cond_7
    if-nez p0, :cond_8

    .line 543
    :try_start_6
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    move-result-object v0

    goto :goto_4

    .line 544
    :catch_5
    move-exception v0

    .line 545
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 546
    goto :goto_4

    .line 549
    :cond_8
    :try_start_7
    invoke-static {}, Landroid/hardware/CameraSlave;->open()Landroid/hardware/CameraSlave;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    move-result-object v0

    goto :goto_4

    .line 550
    :catch_6
    move-exception v0

    .line 551
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    goto :goto_4

    .line 560
    :cond_9
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isDell:Z

    if-eqz v0, :cond_a

    .line 562
    :try_start_8
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 563
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    const/4 v1, 0x0

    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    goto/16 :goto_0

    .line 564
    :catch_7
    move-exception v0

    .line 565
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 566
    goto/16 :goto_0

    .line 567
    :cond_a
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isMotorola:Z

    if-eqz v0, :cond_c

    .line 568
    if-nez p0, :cond_b

    .line 570
    :try_start_9
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    move-result-object v0

    goto/16 :goto_0

    .line 571
    :catch_8
    move-exception v0

    .line 572
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 573
    goto/16 :goto_0

    .line 576
    :cond_b
    :try_start_a
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v1, 0xb

    aget-object v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_9

    goto/16 :goto_0

    .line 577
    :catch_9
    move-exception v0

    .line 578
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 579
    goto/16 :goto_0

    .line 581
    :cond_c
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isDellStreak7:Z

    if-eqz v0, :cond_e

    .line 583
    :try_start_b
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a

    move-result-object v0

    .line 587
    :goto_5
    if-eqz v0, :cond_0

    .line 588
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 589
    if-nez p0, :cond_d

    .line 590
    const-string v2, "camera-sensor"

    invoke-virtual {v1, v2, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 593
    :goto_6
    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto/16 :goto_0

    .line 584
    :catch_a
    move-exception v0

    .line 585
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    goto :goto_5

    .line 592
    :cond_d
    const-string v2, "camera-sensor"

    invoke-virtual {v1, v2, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    goto :goto_6

    .line 595
    :cond_e
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isLG:Z

    if-eqz v0, :cond_19

    .line 596
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "VS910 4G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 597
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->oem_camera:Ljava/lang/Object;

    if-eqz v0, :cond_1b

    .line 599
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->oem_camera:Ljava/lang/Object;

    check-cast v0, Lcom/lge/secondcamera/LG_Revo_CameraEngine;

    .line 600
    invoke-virtual {v0, p0}, Lcom/lge/secondcamera/LG_Revo_CameraEngine;->OpenCamera(I)Landroid/hardware/Camera;

    move-result-object v0

    goto/16 :goto_0

    .line 605
    :cond_f
    :try_start_c
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_b

    move-result-object v0

    .line 609
    :goto_7
    if-eqz v0, :cond_0

    .line 610
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 611
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "LG-P970"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "LG-KU5900"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "LGL85C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 612
    :cond_10
    const-string v2, "lge-camera"

    if-nez p0, :cond_11

    move v3, v4

    :goto_8
    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 613
    const-string v2, "video-input"

    if-nez p0, :cond_12

    move v3, v4

    :goto_9
    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 627
    :goto_a
    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto/16 :goto_0

    .line 606
    :catch_b
    move-exception v0

    .line 607
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    goto :goto_7

    :cond_11
    move v3, v5

    .line 612
    goto :goto_8

    :cond_12
    move v3, v5

    .line 613
    goto :goto_9

    .line 615
    :cond_13
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "LG-P925"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_14

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "LG-P920"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_14

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "LG-SU760"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 616
    :cond_14
    const/16 v2, 0x280

    const/16 v3, 0x1e0

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 617
    const-string v2, "lge-camera"

    if-nez p0, :cond_15

    move v3, v4

    :goto_b
    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 618
    const-string v2, "s3d-supported"

    const-string v3, "false"

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    const-string v2, "camera-index"

    if-nez p0, :cond_16

    move v3, v4

    :goto_c
    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    goto :goto_a

    :cond_15
    move v3, v5

    .line 617
    goto :goto_b

    :cond_16
    move v3, v5

    .line 619
    goto :goto_c

    .line 622
    :cond_17
    if-nez p0, :cond_18

    .line 623
    const-string v2, "camera-sensor"

    invoke-virtual {v1, v2, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    goto :goto_a

    .line 625
    :cond_18
    const-string v2, "camera-sensor"

    invoke-virtual {v1, v2, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    goto :goto_a

    .line 630
    :cond_19
    sget-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isPantech:Z

    if-eqz v0, :cond_1a

    .line 632
    :try_start_d
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 633
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    const/4 v1, 0x0

    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_c

    goto/16 :goto_0

    .line 634
    :catch_c
    move-exception v0

    .line 635
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    .line 636
    goto/16 :goto_0

    .line 638
    :cond_1a
    if-nez p0, :cond_1b

    .line 640
    :try_start_e
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_d

    move-result-object v0

    goto/16 :goto_0

    .line 641
    :catch_d
    move-exception v0

    .line 642
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1b
    move-object v0, v3

    goto/16 :goto_0

    .line 533
    :catch_e
    move-exception v0

    goto/16 :goto_3
.end method

.method public static updateContext(Landroid/content/Context;)V
    .locals 4
    .parameter

    .prologue
    const/16 v3, 0x400

    const/16 v2, 0x258

    .line 295
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 296
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    sput v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->screenWidth:I

    .line 297
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    sput v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->screenHeight:I

    .line 298
    sget v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->screenWidth:I

    if-ne v0, v3, :cond_0

    sget v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->screenHeight:I

    if-eq v0, v2, :cond_1

    :cond_0
    sget v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->screenHeight:I

    if-eq v0, v3, :cond_1

    sget v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->screenWidth:I

    if-ne v0, v2, :cond_2

    .line 299
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->isSamsungTablet:Z

    .line 300
    :cond_2
    return-void
.end method


# virtual methods
.method public final addCallbackBuffer([B)V
    .locals 3
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 236
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-void

    .line 239
    :cond_0
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 240
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    .locals 1
    .parameter

    .prologue
    .line 257
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 258
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 259
    :cond_0
    return-void
.end method

.method public final cancelAutoFocus()V
    .locals 1

    .prologue
    .line 262
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 263
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 264
    :cond_0
    return-void
.end method

.method public getCamera()Landroid/hardware/Camera;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    return-object v0
.end method

.method public getParameters()Landroid/hardware/Camera$Parameters;
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 175
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 176
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lock()V
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 268
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->lock()V

    .line 269
    :cond_0
    return-void
.end method

.method public final reconnect()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0xc

    .line 280
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 289
    :goto_0
    return-void

    .line 283
    :cond_0
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/16 v1, 0xc

    aget-object v1, v0, v1

    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 284
    :catch_0
    move-exception v0

    .line 285
    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_1

    .line 286
    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 287
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final release()V
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 112
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 114
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    .line 115
    return-void
.end method

.method public final setDisplayOrientation(I)V
    .locals 3
    .parameter

    .prologue
    const/4 v1, 0x2

    .line 190
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 198
    :goto_0
    return-void

    .line 193
    :cond_0
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 194
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setParameters(Landroid/hardware/Camera$Parameters;)V
    .locals 1
    .parameter

    .prologue
    .line 180
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 181
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 182
    :cond_0
    return-void
.end method

.method public final setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 1
    .parameter

    .prologue
    .line 210
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 211
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 212
    :cond_0
    return-void
.end method

.method public final setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 3
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 219
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 220
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 229
    :goto_0
    return-void

    .line 224
    :cond_0
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 225
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->methods:[Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    sget-object v1, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    sget-object v2, Lcom/sgiggle/VideoCapture/CameraWrapper;->arg1:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .parameter

    .prologue
    .line 202
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 203
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 204
    :catch_0
    move-exception v0

    .line 205
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public final startPreview()V
    .locals 1

    .prologue
    .line 247
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 249
    :cond_0
    return-void
.end method

.method public final stopPreview()V
    .locals 1

    .prologue
    .line 252
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 253
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 254
    :cond_0
    return-void
.end method

.method public final unlock()V
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 273
    sget-object v0, Lcom/sgiggle/VideoCapture/CameraWrapper;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->unlock()V

    .line 274
    :cond_0
    return-void
.end method
