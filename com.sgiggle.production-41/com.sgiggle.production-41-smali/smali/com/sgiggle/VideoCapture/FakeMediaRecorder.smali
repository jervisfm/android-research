.class public Lcom/sgiggle/VideoCapture/FakeMediaRecorder;
.super Ljava/lang/Object;
.source "FakeMediaRecorder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FakeMediaRecorder"


# instance fields
.field private fd:Ljava/io/FileDescriptor;

.field private filename:Ljava/lang/String;

.field private isStarted:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, "capture"

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->filename:Ljava/lang/String;

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->isStarted:Z

    return-void
.end method

.method private native nativeStart(Ljava/io/FileDescriptor;Ljava/lang/String;)I
.end method

.method private native nativeStop(Ljava/io/FileDescriptor;)I
.end method


# virtual methods
.method public setInputFile(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->filename:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public setOutputFile(Ljava/io/FileDescriptor;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->fd:Ljava/io/FileDescriptor;

    .line 23
    return-void
.end method

.method public start()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->fd:Ljava/io/FileDescriptor;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "fd is not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->filename:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 29
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "filename is not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->fd:Ljava/io/FileDescriptor;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->filename:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->nativeStart(Ljava/io/FileDescriptor;Ljava/lang/String;)I

    move-result v0

    .line 31
    if-eqz v0, :cond_2

    .line 32
    const-string v0, "FakeMediaRecorder"

    const-string v1, "nativeStart failed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "nativeStart failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->isStarted:Z

    .line 36
    return-void
.end method

.method public stop()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->isStarted:Z

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->fd:Ljava/io/FileDescriptor;

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->nativeStop(Ljava/io/FileDescriptor;)I

    move-result v0

    .line 42
    if-eqz v0, :cond_1

    .line 43
    const-string v0, "FakeMediaRecorder"

    const-string v1, "nativeStop failed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :cond_1
    return-void
.end method
