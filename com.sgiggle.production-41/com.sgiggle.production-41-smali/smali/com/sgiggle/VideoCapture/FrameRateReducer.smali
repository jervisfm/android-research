.class Lcom/sgiggle/VideoCapture/FrameRateReducer;
.super Ljava/lang/Object;
.source "FrameRateReducer.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "FrameRateReducer"


# instance fields
.field private m_head:I

.field private m_rateDenom:I

.field private m_rateNum:I

.field private m_rejectedCount:I

.field private m_selectedCount:I

.field private m_selectionQueue:[Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/FrameRateReducer;-><init>(I)V

    .line 19
    return-void
.end method

.method public constructor <init>(I)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput v3, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateNum:I

    iput v3, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateDenom:I

    .line 22
    new-array v0, p1, [Z

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectionQueue:[Z

    move v0, v2

    .line 24
    :goto_0
    if-ge v0, p1, :cond_0

    .line 25
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectionQueue:[Z

    aput-boolean v3, v1, v0

    .line 24
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    iput p1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    .line 29
    iput v2, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    .line 30
    iput v2, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_head:I

    .line 31
    return-void
.end method


# virtual methods
.method public nextFrame()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 41
    iget v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit16 v0, v0, 0x2710

    iget v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    iget v2, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    div-int/2addr v0, v1

    iget v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateNum:I

    mul-int/lit16 v1, v1, 0x2710

    iget v2, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateDenom:I

    div-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 42
    iget v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    mul-int/lit16 v1, v1, 0x2710

    iget v2, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    iget v3, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    div-int/2addr v1, v2

    iget v2, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateNum:I

    mul-int/lit16 v2, v2, 0x2710

    iget v3, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateDenom:I

    div-int/2addr v2, v3

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 45
    const-string v2, "FrameRateReducer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "next rate called m_selectedCount "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " m_rejectedCount "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " posHypErr "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " negHypErr "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " m_rateNum "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateNum:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " m_rateDenom "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateDenom:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    if-ge v0, v1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectionQueue:[Z

    iget v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_head:I

    aget-boolean v0, v0, v1

    if-nez v0, :cond_2

    .line 50
    iget v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    .line 51
    iget v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    sub-int/2addr v0, v5

    iput v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    .line 52
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectionQueue:[Z

    iget v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_head:I

    aput-boolean v5, v0, v1

    move v0, v5

    .line 63
    :goto_0
    const-string v1, "FrameRateReducer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "decided "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " m_selectedCount "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " m_rejectedCount "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_head:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectionQueue:[Z

    array-length v2, v2

    rem-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_head:I

    .line 66
    return v0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectionQueue:[Z

    iget v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_head:I

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_1

    .line 57
    iget v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    sub-int/2addr v0, v5

    iput v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectedCount:I

    .line 58
    iget v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rejectedCount:I

    .line 59
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_selectionQueue:[Z

    iget v1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_head:I

    aput-boolean v6, v0, v1

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v5

    goto :goto_0
.end method

.method public setRate(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 34
    const-string v0, "FrameRateReducer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting rate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    iput p1, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateNum:I

    .line 36
    iput p2, p0, Lcom/sgiggle/VideoCapture/FrameRateReducer;->m_rateDenom:I

    .line 37
    return-void
.end method
