.class Lcom/sgiggle/VideoCapture/PreprocessorThread;
.super Ljava/lang/Thread;
.source "PreprocessorThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "PreprocessorThread"


# instance fields
.field private m_queue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;",
            ">;"
        }
    .end annotation
.end field

.field private m_shouldBeRunning:Z

.field private m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;


# direct methods
.method public constructor <init>(Lcom/sgiggle/VideoCapture/VideoCaptureRaw;)V
    .locals 2
    .parameter

    .prologue
    .line 32
    const-string v0, "PreprocessorThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 13
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_queue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 33
    const-string v0, "PreprocessorThread"

    const-string v1, "PreprocessorThread created"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_shouldBeRunning:Z

    .line 35
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    .line 36
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/PreprocessorThread;->start()V

    .line 37
    return-void
.end method

.method private native captureCallback([BIIJ)V
.end method

.method private processFrame([BJ)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraWidth()I

    move-result v2

    iget-object v0, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraHeight()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/VideoCapture/PreprocessorThread;->captureCallback([BIIJ)V

    .line 70
    return-void
.end method


# virtual methods
.method public offer(Lcom/sgiggle/VideoCapture/TimestamperThread;[BJ)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 42
    iget-object v6, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_queue:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v0, Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;-><init>(Lcom/sgiggle/VideoCapture/PreprocessorThread;Lcom/sgiggle/VideoCapture/TimestamperThread;[BJ)V

    invoke-virtual {v6, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method public queueSize()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_queue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->size()I

    move-result v0

    return v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 74
    const-string v0, "PreprocessorThread"

    const-string v1, "Preprocessor thread started"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->registerPrThread()V

    .line 76
    :goto_0
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_shouldBeRunning:Z

    if-eqz v0, :cond_0

    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_queue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    iget-object v1, v0, Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;->data:[B

    iget-wide v2, v0, Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;->timestamp:J

    invoke-direct {p0, v1, v2, v3}, Lcom/sgiggle/VideoCapture/PreprocessorThread;->processFrame([BJ)V

    .line 92
    iget-object v1, v0, Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    iget-object v0, v0, Lcom/sgiggle/VideoCapture/PreprocessorThread$TimestampedFrame;->data:[B

    invoke-virtual {v1, v0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->returnBufferToCamera([B)V

    goto :goto_0

    .line 96
    :cond_0
    const-string v0, "PreprocessorThread"

    const-string v1, "Preprocessor thread terminated"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return-void

    .line 83
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public stopPreprocessor()V
    .locals 2

    .prologue
    .line 51
    const-string v0, "PreprocessorThread"

    const-string v1, "stopPreprocessor"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/PreprocessorThread;->m_shouldBeRunning:Z

    .line 53
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/PreprocessorThread;->interrupt()V

    .line 61
    return-void
.end method
