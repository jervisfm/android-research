.class Lcom/sgiggle/VideoCapture/TimestampPredictor;
.super Ljava/lang/Object;
.source "TimestampPredictor.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "TimestampPredictor"


# instance fields
.field private final B:D

.field private final D:D

.field private final K:D

.field private final KB:J

.field private final KD:J

.field private final KN:J

.field private final N:I

.field private final Q:I

.field private m_head:I

.field private m_history:[I

.field private m_numConseqOutliers:I

.field private m_warmupIterLeft:I

.field private t0:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0xa

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput v2, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->N:I

    .line 61
    const/high16 v0, 0x40

    iput v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->Q:I

    .line 62
    const-wide v0, 0x4046800000000000L

    iput-wide v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->B:D

    .line 63
    const-wide v0, 0x4071d00000000000L

    iput-wide v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->D:D

    .line 64
    const-wide v0, 0x3f53dc013dc013dcL

    iput-wide v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->K:D

    .line 65
    const-wide/32 v0, 0x37dac

    iput-wide v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->KB:J

    .line 66
    const-wide/32 v0, 0x161bed

    iput-wide v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->KD:J

    .line 67
    const-wide/32 v0, 0xc698

    iput-wide v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->KN:J

    .line 69
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_history:[I

    .line 70
    iput v3, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    .line 71
    iput v2, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_warmupIterLeft:I

    .line 72
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->t0:J

    .line 73
    iput v3, p0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_numConseqOutliers:I

    .line 76
    const-string v0, "TimestampPredictor"

    const-string v1, "Intialized: Q=4194304; B=45.0; D=285.0; K=0.0012121212121212121; KB=228780; KD=1448941; KN=50840"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return-void
.end method


# virtual methods
.method public push(J)J
    .locals 23
    .parameter

    .prologue
    .line 81
    .line 84
    const/4 v3, 0x0

    .line 86
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->t0:J

    move-wide v4, v0

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 87
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sgiggle/VideoCapture/TimestampPredictor;->t0:J

    .line 89
    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->t0:J

    move-wide v4, v0

    sub-long v4, p1, v4

    long-to-int v4, v4

    .line 92
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_warmupIterLeft:I

    move v5, v0

    if-lez v5, :cond_1

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_history:[I

    move-object v3, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    move v5, v0

    aput v4, v3, v5

    .line 95
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_warmupIterLeft:I

    move v3, v0

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    move v0, v3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_warmupIterLeft:I

    .line 96
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    move v3, v0

    add-int/lit8 v3, v3, 0x1

    rem-int/lit8 v3, v3, 0xa

    move v0, v3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    move-wide/from16 v3, p1

    .line 163
    :goto_0
    return-wide v3

    .line 99
    :cond_1
    const-wide/16 v5, 0x0

    .line 100
    const-wide/16 v7, 0x0

    .line 103
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    move v9, v0

    .line 104
    const/4 v10, 0x0

    move/from16 v20, v10

    move-wide v10, v5

    move/from16 v5, v20

    move-wide/from16 v21, v7

    move-wide/from16 v6, v21

    :goto_1
    const/16 v8, 0xa

    if-ge v5, v8, :cond_2

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_history:[I

    move-object v8, v0

    add-int v12, v9, v5

    rem-int/lit8 v12, v12, 0xa

    aget v8, v8, v12

    int-to-long v12, v8

    add-long/2addr v10, v12

    .line 106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_history:[I

    move-object v8, v0

    add-int v12, v9, v5

    rem-int/lit8 v12, v12, 0xa

    aget v8, v8, v12

    mul-int/2addr v8, v5

    int-to-long v12, v8

    add-long/2addr v6, v12

    .line 104
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 111
    :cond_2
    const-wide/32 v12, 0x161bed

    mul-long/2addr v12, v10

    const-wide/32 v14, 0x37dac

    mul-long/2addr v14, v6

    sub-long/2addr v12, v14

    const-wide/32 v14, 0x400000

    div-long/2addr v12, v14

    .line 112
    const-wide/32 v14, -0x37dac

    mul-long/2addr v10, v14

    const-wide/32 v14, 0xc698

    mul-long v5, v14, v6

    add-long/2addr v5, v10

    const-wide/16 v7, 0x80

    mul-long/2addr v5, v7

    const-wide/32 v7, 0x400000

    div-long/2addr v5, v7

    long-to-int v5, v5

    .line 115
    mul-int/lit8 v6, v5, 0xa

    div-int/lit16 v6, v6, 0x80

    int-to-long v6, v6

    add-long/2addr v6, v12

    .line 116
    mul-int/lit8 v8, v5, 0xa

    div-int/lit16 v8, v8, 0x80

    int-to-long v10, v8

    add-long/2addr v10, v12

    int-to-long v14, v4

    sub-long/2addr v10, v14

    long-to-int v8, v10

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    .line 119
    const-wide/16 v10, 0x0

    .line 120
    const/4 v14, 0x0

    move/from16 v20, v14

    move-wide v14, v10

    move/from16 v10, v20

    :goto_2
    const/16 v11, 0xa

    if-ge v10, v11, :cond_3

    .line 121
    mul-int v11, v5, v10

    div-int/lit16 v11, v11, 0x80

    move v0, v11

    int-to-long v0, v0

    move-wide/from16 v16, v0

    add-long v16, v16, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_history:[I

    move-object v11, v0

    add-int v18, v9, v10

    rem-int/lit8 v18, v18, 0xa

    aget v11, v11, v18

    move v0, v11

    int-to-long v0, v0

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    add-long v14, v14, v16

    .line 120
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 124
    :cond_3
    int-to-long v9, v8

    add-long/2addr v9, v14

    const-wide/16 v11, 0xb

    div-long/2addr v9, v11

    .line 127
    int-to-long v11, v5

    cmp-long v9, v9, v11

    if-lez v9, :cond_4

    .line 128
    const/4 v3, 0x1

    .line 133
    :cond_4
    mul-int/lit16 v8, v8, 0x80

    if-le v8, v5, :cond_5

    .line 134
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_numConseqOutliers:I

    move v5, v0

    add-int/lit8 v5, v5, 0x1

    move v0, v5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_numConseqOutliers:I

    .line 137
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_numConseqOutliers:I

    move v5, v0

    const/4 v8, 0x1

    if-ne v5, v8, :cond_6

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_history:[I

    move-object v4, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    move v5, v0

    long-to-int v8, v6

    aput v8, v4, v5

    .line 148
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    move v4, v0

    add-int/lit8 v4, v4, 0x1

    rem-int/lit8 v4, v4, 0xa

    move v0, v4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    .line 150
    if-nez v3, :cond_8

    .line 151
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->t0:J

    move-wide v3, v0

    add-long/2addr v3, v6

    goto/16 :goto_0

    .line 140
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_numConseqOutliers:I

    move v5, v0

    const/4 v8, 0x1

    if-le v5, v8, :cond_7

    .line 142
    const/4 v3, 0x1

    goto :goto_3

    .line 144
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_history:[I

    move-object v5, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    move v8, v0

    aput v4, v5, v8

    .line 145
    const/4 v4, 0x0

    move v0, v4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_numConseqOutliers:I

    goto :goto_3

    .line 155
    :cond_8
    const/16 v3, 0xa

    move v0, v3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_warmupIterLeft:I

    .line 156
    const/4 v3, 0x0

    move v0, v3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_numConseqOutliers:I

    .line 157
    const/4 v3, 0x0

    move v0, v3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sgiggle/VideoCapture/TimestampPredictor;->m_head:I

    .line 158
    const-wide/16 v3, -0x1

    move-wide v0, v3

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sgiggle/VideoCapture/TimestampPredictor;->t0:J

    move-wide/from16 v3, p1

    goto/16 :goto_0
.end method
