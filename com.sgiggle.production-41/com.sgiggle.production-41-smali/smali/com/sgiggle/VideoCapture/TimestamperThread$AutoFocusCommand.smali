.class Lcom/sgiggle/VideoCapture/TimestamperThread$AutoFocusCommand;
.super Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;
.source "TimestamperThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/VideoCapture/TimestamperThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AutoFocusCommand"
.end annotation


# instance fields
.field private m_autoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

.field final synthetic this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;


# direct methods
.method public constructor <init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;ZLandroid/hardware/Camera$AutoFocusCallback;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$AutoFocusCommand;->this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;

    .line 338
    const-string v0, "Auto Focus"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V

    .line 339
    iput-object p4, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$AutoFocusCommand;->m_autoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    .line 340
    return-void
.end method


# virtual methods
.method public commandImpl()V
    .locals 4

    .prologue
    .line 344
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$AutoFocusCommand;->this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;

    #getter for: Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;
    invoke-static {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->access$000(Lcom/sgiggle/VideoCapture/TimestamperThread;)Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraWrapper()Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$AutoFocusCommand;->m_autoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    :goto_0
    return-void

    .line 345
    :catch_0
    move-exception v0

    .line 346
    const-string v1, "TimestamperThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "autoFocus failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
