.class Lcom/sgiggle/VideoCapture/TimestamperThread$ReturnBufferToCameraCommand;
.super Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;
.source "TimestamperThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/VideoCapture/TimestamperThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ReturnBufferToCameraCommand"
.end annotation


# instance fields
.field private m_buffer:[B

.field final synthetic this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;


# direct methods
.method public constructor <init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z[B)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 325
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$ReturnBufferToCameraCommand;->this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;

    .line 326
    const-string v0, "Return buffer"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V

    .line 327
    iput-object p4, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$ReturnBufferToCameraCommand;->m_buffer:[B

    .line 328
    return-void
.end method


# virtual methods
.method public commandImpl()V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$ReturnBufferToCameraCommand;->this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$ReturnBufferToCameraCommand;->m_buffer:[B

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/TimestamperThread;->returnBufferToCamera([B)V

    .line 332
    return-void
.end method
