.class Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;
.super Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;
.source "TimestamperThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/VideoCapture/TimestamperThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StartRecordingCommand"
.end annotation


# instance fields
.field private m_address:I

.field private m_data:I

.field final synthetic this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;


# direct methods
.method public constructor <init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;ZII)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 311
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;->this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;

    .line 312
    const-string v0, "Start recording"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V

    .line 313
    iput p4, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;->m_address:I

    .line 314
    iput p5, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;->m_data:I

    .line 315
    return-void
.end method


# virtual methods
.method public commandImpl()V
    .locals 3

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    iget v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;->m_address:I

    iget v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;->m_data:I

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->doStartRecording(II)Z

    .line 319
    return-void
.end method
