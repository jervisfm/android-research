.class abstract Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;
.super Ljava/lang/Object;
.source "TimestamperThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/VideoCapture/TimestamperThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "TimestamperCommand"
.end annotation


# instance fields
.field private doneSemaphore:Ljava/util/concurrent/Semaphore;

.field protected m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

.field private name:Ljava/lang/String;

.field final synthetic this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;


# direct methods
.method public constructor <init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->this$0:Lcom/sgiggle/VideoCapture/TimestamperThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    if-eqz p4, :cond_0

    .line 284
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->doneSemaphore:Ljava/util/concurrent/Semaphore;

    .line 287
    :goto_0
    iput-object p2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->name:Ljava/lang/String;

    .line 288
    iput-object p3, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    .line 289
    return-void

    .line 286
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->doneSemaphore:Ljava/util/concurrent/Semaphore;

    goto :goto_0
.end method


# virtual methods
.method abstract commandImpl()V
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->name:Ljava/lang/String;

    return-object v0
.end method

.method public notifyDone()V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->doneSemaphore:Ljava/util/concurrent/Semaphore;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->doneSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 296
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 0

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->commandImpl()V

    .line 276
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->notifyDone()V

    .line 278
    return-void
.end method

.method public waitForDone()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->doneSemaphore:Ljava/util/concurrent/Semaphore;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->doneSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 303
    :goto_0
    return-void

    .line 302
    :cond_0
    const-string v0, "TimestamperThread"

    const-string v1, "Trying to waiting for done on a non synchronized command. Will exit immediatly."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
