.class Lcom/sgiggle/VideoCapture/TimestamperThread;
.super Ljava/lang/Thread;
.source "TimestamperThread.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/VideoCapture/TimestamperThread$AutoFocusCommand;,
        Lcom/sgiggle/VideoCapture/TimestamperThread$ReturnBufferToCameraCommand;,
        Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;,
        Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "TimestamperThread"


# instance fields
.field final PREVIEW_CALLBACK_BUFFER_COUNT:I

.field private final PREVIEW_INTERVAL:I

.field private m_buffers:[[B

.field private m_commandHandler:Landroid/os/Handler;

.field private m_frameRateReducer:Lcom/sgiggle/VideoCapture/FrameRateReducer;

.field private m_initializationSemaphore:Ljava/util/concurrent/Semaphore;

.field private m_preprocessorThread:Lcom/sgiggle/VideoCapture/PreprocessorThread;

.field private m_slowDownToPreprocessor:Z

.field private m_time_previewed:J

.field private m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;


# direct methods
.method public constructor <init>(Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Lcom/sgiggle/VideoCapture/PreprocessorThread;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 45
    const-string v0, "TimestamperThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x5

    iput v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->PREVIEW_CALLBACK_BUFFER_COUNT:I

    .line 36
    new-instance v0, Lcom/sgiggle/VideoCapture/FrameRateReducer;

    invoke-direct {v0}, Lcom/sgiggle/VideoCapture/FrameRateReducer;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_frameRateReducer:Lcom/sgiggle/VideoCapture/FrameRateReducer;

    .line 37
    iput-boolean v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_slowDownToPreprocessor:Z

    .line 40
    const/16 v0, 0x32

    iput v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->PREVIEW_INTERVAL:I

    .line 46
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    .line 47
    iput-object p2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_preprocessorThread:Lcom/sgiggle/VideoCapture/PreprocessorThread;

    .line 50
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_initializationSemaphore:Ljava/util/concurrent/Semaphore;

    .line 51
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->start()V

    .line 52
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_initializationSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/VideoCapture/TimestamperThread;)Lcom/sgiggle/VideoCapture/VideoCaptureRaw;
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    return-object v0
.end method


# virtual methods
.method public addCallbackBuffers()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 58
    const-string v0, "TimestamperThread"

    const-string v1, "addCallbackBuffers"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraWrapper()Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    .line 61
    if-nez v0, :cond_0

    .line 62
    const-string v0, "TimestamperThread"

    const-string v1, "CameraWrapper is null"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 68
    new-instance v2, Landroid/graphics/PixelFormat;

    invoke-direct {v2}, Landroid/graphics/PixelFormat;-><init>()V

    .line 69
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v1

    invoke-static {v1, v2}, Landroid/graphics/PixelFormat;->getPixelFormatInfo(ILandroid/graphics/PixelFormat;)V

    .line 72
    const/4 v1, 0x5

    new-array v1, v1, [[B

    iput-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_buffers:[[B

    move v1, v6

    .line 73
    :goto_1
    iget-object v3, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_buffers:[[B

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 74
    iget-object v3, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_buffers:[[B

    iget-object v4, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v4}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v5}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraHeight()I

    move-result v5

    mul-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/PixelFormat;->bitsPerPixel:I

    mul-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x8

    new-array v4, v4, [B

    aput-object v4, v3, v1

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 78
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    move v1, v6

    .line 79
    :goto_2
    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_buffers:[[B

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 80
    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_buffers:[[B

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/sgiggle/VideoCapture/CameraWrapper;->addCallbackBuffer([B)V

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 82
    :cond_2
    invoke-virtual {v0, p0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    goto :goto_0
.end method

.method public autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    .locals 3
    .parameter

    .prologue
    .line 162
    new-instance v0, Lcom/sgiggle/VideoCapture/TimestamperThread$AutoFocusCommand;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sgiggle/VideoCapture/TimestamperThread$AutoFocusCommand;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;ZLandroid/hardware/Camera$AutoFocusCallback;)V

    .line 164
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v1, p0, :cond_0

    .line 165
    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->run()V

    .line 168
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_commandHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 10
    .parameter
    .parameter

    .prologue
    const/16 v9, 0x1c

    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 174
    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 180
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getGlRenderer()Lcom/sgiggle/GLES20/GLRenderer;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    .line 182
    iget-wide v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_time_previewed:J

    sub-long v2, v6, v2

    const-wide/16 v4, 0x32

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 183
    array-length v3, p1

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraWidth()I

    move-result v4

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraHeight()I

    move-result v5

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/GLES20/GLRenderer;->render(I[BIII)V

    .line 184
    iput-wide v6, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_time_previewed:J

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_preprocessorThread:Lcom/sgiggle/VideoCapture/PreprocessorThread;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/PreprocessorThread;->queueSize()I

    move-result v0

    .line 194
    const/4 v2, 0x2

    if-gt v0, v2, :cond_2

    .line 195
    iget-boolean v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_slowDownToPreprocessor:Z

    if-eqz v2, :cond_1

    .line 196
    const-string v2, "TimestamperThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preprocessor queue size is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Leave slow down mode."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_1
    iput-boolean v8, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_slowDownToPreprocessor:Z

    .line 201
    :cond_2
    const/4 v2, 0x5

    if-lt v0, v2, :cond_3

    .line 202
    iput-boolean v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_slowDownToPreprocessor:Z

    .line 203
    const-string v2, "TimestamperThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preprocessor queue size is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Go to slow down mode."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_3
    iget-boolean v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_slowDownToPreprocessor:Z

    if-eqz v2, :cond_8

    .line 207
    const/16 v2, 0x1e

    mul-int/lit8 v0, v0, 0x3

    sub-int v0, v2, v0

    .line 210
    if-gez v0, :cond_4

    move v0, v8

    .line 212
    :cond_4
    if-le v0, v9, :cond_5

    .line 213
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_frameRateReducer:Lcom/sgiggle/VideoCapture/FrameRateReducer;

    invoke-virtual {v0, v1, v1}, Lcom/sgiggle/VideoCapture/FrameRateReducer;->setRate(II)V

    .line 218
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_frameRateReducer:Lcom/sgiggle/VideoCapture/FrameRateReducer;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/FrameRateReducer;->nextFrame()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 223
    :goto_1
    if-nez v0, :cond_7

    .line 224
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_preprocessorThread:Lcom/sgiggle/VideoCapture/PreprocessorThread;

    invoke-virtual {v0, p0, p1, v6, v7}, Lcom/sgiggle/VideoCapture/PreprocessorThread;->offer(Lcom/sgiggle/VideoCapture/TimestamperThread;[BJ)V

    .line 231
    :goto_2
    return-void

    .line 215
    :cond_5
    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_frameRateReducer:Lcom/sgiggle/VideoCapture/FrameRateReducer;

    invoke-virtual {v2, v0, v9}, Lcom/sgiggle/VideoCapture/FrameRateReducer;->setRate(II)V

    goto :goto_0

    :cond_6
    move v0, v8

    .line 218
    goto :goto_1

    .line 226
    :cond_7
    invoke-virtual {p0, p1}, Lcom/sgiggle/VideoCapture/TimestamperThread;->returnBufferToCamera([B)V

    goto :goto_2

    :cond_8
    move v0, v8

    goto :goto_1
.end method

.method public resumeRecording()V
    .locals 4

    .prologue
    .line 133
    const-string v0, "TimestamperThread"

    const-string v1, "resumeRecording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    new-instance v0, Lcom/sgiggle/VideoCapture/TimestamperThread$3;

    const-string v1, "StopRecording"

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sgiggle/VideoCapture/TimestamperThread$3;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V

    .line 142
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_commandHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 144
    return-void
.end method

.method public returnBufferToCamera([B)V
    .locals 4
    .parameter

    .prologue
    .line 239
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_commandHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sgiggle/VideoCapture/TimestamperThread$ReturnBufferToCameraCommand;

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3, p1}, Lcom/sgiggle/VideoCapture/TimestamperThread$ReturnBufferToCameraCommand;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z[B)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 250
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraWrapper()Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_1

    .line 246
    invoke-virtual {v0, p1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->addCallbackBuffer([B)V

    goto :goto_0

    .line 248
    :cond_1
    const-string v0, "TimestamperThread"

    const-string v1, "returnBufferToCamera: cameraWrapper is null"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 253
    const-string v0, "TimestamperThread"

    const-string v1, "run"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->registerPrThread()V

    .line 257
    const/16 v0, -0xc

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 259
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 261
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_commandHandler:Landroid/os/Handler;

    .line 262
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_initializationSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 264
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 265
    return-void
.end method

.method public startRecording(II)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 89
    const-string v0, "TimestamperThread"

    const-string v1, "startRecording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    new-instance v0, Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    const/4 v3, 0x1

    move-object v1, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/VideoCapture/TimestamperThread$StartRecordingCommand;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;ZII)V

    .line 92
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_time_previewed:J

    .line 93
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_commandHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 94
    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->waitForDone()V

    .line 95
    return-void
.end method

.method public stopRecording()V
    .locals 4

    .prologue
    .line 101
    const-string v0, "TimestamperThread"

    const-string v1, "stopRecording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v0, Lcom/sgiggle/VideoCapture/TimestamperThread$1;

    const-string v1, "StopRecording"

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sgiggle/VideoCapture/TimestamperThread$1;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V

    .line 110
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_commandHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 111
    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->waitForDone()V

    .line 112
    return-void
.end method

.method public stopThread()V
    .locals 4

    .prologue
    .line 148
    const-string v0, "TimestamperThread"

    const-string v1, "stopThread"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    new-instance v0, Lcom/sgiggle/VideoCapture/TimestamperThread$4;

    const-string v1, "StopThread"

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sgiggle/VideoCapture/TimestamperThread$4;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V

    .line 157
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_commandHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 158
    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->waitForDone()V

    .line 159
    return-void
.end method

.method public suspendRecording()V
    .locals 4

    .prologue
    .line 117
    const-string v0, "TimestamperThread"

    const-string v1, "suspendRecording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    new-instance v0, Lcom/sgiggle/VideoCapture/TimestamperThread$2;

    const-string v1, "StopRecording"

    iget-object v2, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_videoCaptureRaw:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sgiggle/VideoCapture/TimestamperThread$2;-><init>(Lcom/sgiggle/VideoCapture/TimestamperThread;Ljava/lang/String;Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Z)V

    .line 126
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/TimestamperThread;->m_commandHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 127
    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread$TimestamperCommand;->waitForDone()V

    .line 128
    return-void
.end method
