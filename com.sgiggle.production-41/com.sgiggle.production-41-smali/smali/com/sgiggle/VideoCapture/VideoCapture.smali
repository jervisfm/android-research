.class public Lcom/sgiggle/VideoCapture/VideoCapture;
.super Ljava/lang/Object;
.source "VideoCapture.java"

# interfaces
.implements Landroid/media/MediaRecorder$OnErrorListener;
.implements Landroid/media/MediaRecorder$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/VideoCapture/VideoCapture$VideoFormat;,
        Lcom/sgiggle/VideoCapture/VideoCapture$VideoSize;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoCapture"

.field public static sInstance:Lcom/sgiggle/VideoCapture/VideoCapture;


# instance fields
.field private camera:Landroid/hardware/Camera;

.field private clientFd:Ljava/io/FileDescriptor;

.field private fakeRecorder:Lcom/sgiggle/VideoCapture/FakeMediaRecorder;

.field private filename:Ljava/lang/String;

.field private frameRate:I

.field private height:I

.field private holder:Landroid/view/SurfaceHolder;

.field private isCreateData:Z

.field private isPreviewing:Z

.field private isRecording:Z

.field private isUseCamera:Z

.field private isUseFakeMediaRecorder:Z

.field private isUseNative:Z

.field private recorder:Landroid/media/MediaRecorder;

.field private serverFd:Ljava/io/FileDescriptor;

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCapture;->sInstance:Lcom/sgiggle/VideoCapture/VideoCapture;

    .line 369
    const-string v0, "S1"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 370
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseCamera:Z

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseNative:Z

    .line 28
    iput-boolean v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseFakeMediaRecorder:Z

    .line 29
    iput-boolean v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isCreateData:Z

    .line 34
    const/16 v0, 0xb0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->width:I

    .line 35
    const/16 v0, 0x90

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->height:I

    .line 36
    const/16 v0, 0xa

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->frameRate:I

    .line 58
    sput-object p0, Lcom/sgiggle/VideoCapture/VideoCapture;->sInstance:Lcom/sgiggle/VideoCapture/VideoCapture;

    .line 59
    return-void
.end method

.method private closeCamera()V
    .locals 2

    .prologue
    .line 223
    const-string v0, "VideoCapture"

    const-string v1, "closeCamera"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->lock()V

    .line 225
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 226
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 227
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    .line 228
    return-void
.end method

.method private closeRecorder()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 274
    const-string v0, "VideoCapture"

    const-string v1, "closeRecorder"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseNative:Z

    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/VideoCapture;->callNativeStop()V

    .line 278
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseFakeMediaRecorder:Z

    if-eqz v0, :cond_1

    .line 279
    iput-object v2, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->fakeRecorder:Lcom/sgiggle/VideoCapture/FakeMediaRecorder;

    .line 285
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 282
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 283
    iput-object v2, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    goto :goto_0
.end method

.method private getFD(Ljava/lang/String;)Ljava/io/FileDescriptor;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 320
    .line 323
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 324
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    const/high16 v1, 0x3000

    :try_start_1
    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 335
    :goto_0
    return-object v0

    .line 325
    :catch_0
    move-exception v0

    .line 326
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v2

    .line 327
    goto :goto_0

    .line 331
    :catch_1
    move-exception v0

    .line 332
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v0, v2

    .line 333
    goto :goto_0
.end method

.method private native nativeSetup(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;)I
.end method

.method private native nativeStart(Ljava/io/FileDescriptor;Ljava/lang/String;)I
.end method

.method private native nativeStop(Ljava/io/FileDescriptor;)I
.end method

.method private setupCamera()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 206
    const-string v0, "VideoCapture"

    const-string v1, "setupCamera"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :try_start_0
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    .line 209
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->lock()V

    .line 210
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 211
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 212
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->holder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 213
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->unlock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    return-void

    .line 214
    :catch_0
    move-exception v0

    .line 215
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 216
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 217
    iput-object v2, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    .line 218
    throw v0
.end method

.method private setupRecorder(Ljava/lang/String;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 231
    const-string v0, "VideoCapture"

    const-string v1, "setupRecorder"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isCreateData:Z

    if-ne v0, v2, :cond_0

    .line 233
    iput-boolean v2, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseNative:Z

    .line 234
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->filename:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sgiggle/VideoCapture/VideoCapture;->callNativeStart(Ljava/lang/String;)V

    .line 244
    :goto_0
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseFakeMediaRecorder:Z

    if-eqz v0, :cond_2

    .line 245
    new-instance v0, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;

    invoke-direct {v0}, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->fakeRecorder:Lcom/sgiggle/VideoCapture/FakeMediaRecorder;

    .line 246
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->fakeRecorder:Lcom/sgiggle/VideoCapture/FakeMediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->serverFd:Ljava/io/FileDescriptor;

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->setOutputFile(Ljava/io/FileDescriptor;)V

    .line 247
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->fakeRecorder:Lcom/sgiggle/VideoCapture/FakeMediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->filename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->setInputFile(Ljava/lang/String;)V

    .line 271
    :goto_1
    return-void

    .line 236
    :cond_0
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_1

    .line 237
    iput-boolean v3, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseNative:Z

    .line 238
    invoke-direct {p0, p1}, Lcom/sgiggle/VideoCapture/VideoCapture;->getFD(Ljava/lang/String;)Ljava/io/FileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->serverFd:Ljava/io/FileDescriptor;

    goto :goto_0

    .line 240
    :cond_1
    iput-boolean v2, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseNative:Z

    .line 241
    invoke-virtual {p0, p1}, Lcom/sgiggle/VideoCapture/VideoCapture;->callNativeStart(Ljava/lang/String;)V

    goto :goto_0

    .line 250
    :cond_2
    :try_start_0
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    .line 251
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseCamera:Z

    if-eqz v0, :cond_3

    .line 252
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setCamera(Landroid/hardware/Camera;)V

    .line 254
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 256
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 257
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->width:I

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->height:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    .line 258
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->frameRate:I

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    .line 259
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 261
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->serverFd:Ljava/io/FileDescriptor;

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/io/FileDescriptor;)V

    .line 262
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->holder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setPreviewDisplay(Landroid/view/Surface;)V

    .line 263
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 264
    :catch_0
    move-exception v0

    .line 265
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 266
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    .line 267
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    .line 268
    throw v0
.end method

.method private startRecorder()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 288
    const-string v0, "VideoCapture"

    const-string v1, "startRecorder"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseFakeMediaRecorder:Z

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->fakeRecorder:Lcom/sgiggle/VideoCapture/FakeMediaRecorder;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->start()V

    .line 296
    :goto_0
    return-void

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, p0}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    .line 293
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, p0}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 294
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->start()V

    goto :goto_0
.end method

.method private stopRecorder()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 299
    const-string v0, "VideoCapture"

    const-string v1, "stopRecorder"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseFakeMediaRecorder:Z

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->fakeRecorder:Lcom/sgiggle/VideoCapture/FakeMediaRecorder;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/FakeMediaRecorder;->stop()V

    .line 307
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v2}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    .line 304
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v2}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 305
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    goto :goto_0
.end method


# virtual methods
.method public callNativeStart(Ljava/lang/String;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 342
    new-instance v0, Ljava/io/FileDescriptor;

    invoke-direct {v0}, Ljava/io/FileDescriptor;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->serverFd:Ljava/io/FileDescriptor;

    .line 343
    new-instance v0, Ljava/io/FileDescriptor;

    invoke-direct {v0}, Ljava/io/FileDescriptor;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->clientFd:Ljava/io/FileDescriptor;

    .line 344
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->serverFd:Ljava/io/FileDescriptor;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->clientFd:Ljava/io/FileDescriptor;

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/VideoCapture/VideoCapture;->nativeSetup(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;)I

    move-result v0

    .line 345
    if-eqz v0, :cond_0

    .line 346
    new-instance v0, Ljava/io/IOException;

    const-string v1, "nativeSetup failed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->clientFd:Ljava/io/FileDescriptor;

    invoke-direct {p0, v0, p1}, Lcom/sgiggle/VideoCapture/VideoCapture;->nativeStart(Ljava/io/FileDescriptor;Ljava/lang/String;)I

    move-result v0

    .line 351
    if-eqz v0, :cond_1

    .line 352
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->clientFd:Ljava/io/FileDescriptor;

    .line 353
    new-instance v0, Ljava/io/IOException;

    const-string v1, "nativeStart failed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :cond_1
    return-void
.end method

.method public callNativeStop()V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->clientFd:Ljava/io/FileDescriptor;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->clientFd:Ljava/io/FileDescriptor;

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/VideoCapture;->nativeStop(Ljava/io/FileDescriptor;)I

    .line 360
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->clientFd:Ljava/io/FileDescriptor;

    .line 362
    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaRecorder;II)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 311
    const-string v0, "VideoCapture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    return-void
.end method

.method public onInfo(Landroid/media/MediaRecorder;II)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 316
    const-string v0, "VideoCapture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onInfo"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    return-void
.end method

.method public setCaptureFile(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_1
    if-nez p1, :cond_2

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isCreateData:Z

    .line 89
    :goto_0
    return-void

    .line 86
    :cond_2
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->filename:Ljava/lang/String;

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isCreateData:Z

    goto :goto_0
.end method

.method public setCapturedFile(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    if-eqz v0, :cond_1

    .line 67
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1
    if-nez p1, :cond_2

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseFakeMediaRecorder:Z

    .line 74
    :goto_0
    return-void

    .line 71
    :cond_2
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->filename:Ljava/lang/String;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isUseFakeMediaRecorder:Z

    goto :goto_0
.end method

.method public setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    if-eqz v0, :cond_1

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_1
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->holder:Landroid/view/SurfaceHolder;

    .line 100
    return-void
.end method

.method public setVideoFormat(I)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-eqz v0, :cond_0

    .line 137
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    return-void
.end method

.method public setVideoFrameRate(I)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-eqz v0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iput p1, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->frameRate:I

    .line 111
    return-void
.end method

.method public setVideoSize(I)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-eqz v0, :cond_0

    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 127
    :goto_0
    return-void

    .line 123
    :pswitch_0
    const/16 v0, 0xb0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->width:I

    const/16 v0, 0x90

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->height:I

    goto :goto_0

    .line 124
    :pswitch_1
    const/16 v0, 0x160

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->width:I

    const/16 v0, 0x120

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->height:I

    goto :goto_0

    .line 125
    :pswitch_2
    const/16 v0, 0x2e4

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->width:I

    const/16 v0, 0x1e0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->height:I

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public startPreview()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already previewing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-eqz v0, :cond_1

    .line 148
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already recording"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCapture;->setupCamera()V

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    .line 152
    return-void
.end method

.method public startRecording(Ljava/lang/String;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-eqz v0, :cond_0

    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already recording"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    if-eqz v0, :cond_1

    .line 175
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCapture;->closeCamera()V

    .line 178
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sgiggle/VideoCapture/VideoCapture;->setupRecorder(Ljava/lang/String;)V

    .line 179
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCapture;->startRecorder()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    .line 184
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 181
    throw v0
.end method

.method public stopPreview()V
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    if-nez v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCapture;->closeCamera()V

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    goto :goto_0
.end method

.method public stopRecording()V
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    if-nez v0, :cond_0

    .line 203
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCapture;->stopRecorder()V

    .line 194
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCapture;->closeRecorder()V

    .line 196
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isPreviewing:Z

    if-eqz v0, :cond_1

    .line 198
    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCapture;->setupCamera()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCapture;->isRecording:Z

    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    goto :goto_1
.end method
