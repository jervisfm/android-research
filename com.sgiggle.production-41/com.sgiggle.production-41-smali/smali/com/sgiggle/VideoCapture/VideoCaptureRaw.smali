.class public Lcom/sgiggle/VideoCapture/VideoCaptureRaw;
.super Ljava/lang/Object;
.source "VideoCaptureRaw.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;,
        Lcom/sgiggle/VideoCapture/VideoCaptureRaw$CameraDevice;,
        Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Type;
    }
.end annotation


# static fields
.field private static final CAMERA_HEIGHT:I = 0xf0

.field private static final CAMERA_WIDTH:I = 0x140

.field private static final CAPTURE_HEIGHT:I = 0x80

.field private static final CAPTURE_VGA_HEIGHT:I = 0x1a0

.field private static final CAPTURE_VGA_WIDTH:I = 0x270

.field private static final CAPTURE_WIDTH:I = 0xc0

.field public static final COMMAND_START:I = 0x0

.field public static final COMMAND_STOP:I = 0x1

.field private static final HVGA_HEIGHT:I = 0x140

.field private static final HVGA_WIDTH:I = 0x1e0

.field public static final MESSAGE_UPDATE_LAYOUT:I = 0x2

.field private static final RATIO_CAPTURE:F = 1.5f

.field private static final ROTATION_TRANSFORM_MIRROR_ROTATE270_CLIP:I = 0x3

.field private static final ROTATION_TRANSFORM_MIRROR_UPSIDEDOWN:I = 0x1

.field private static final ROTATION_TRANSFORM_NONE:I = 0x0

.field private static final ROTATION_TRANSFORM_ROTATE270_MIRROR_CLIP:I = 0x4

.field private static final ROTATION_TRANSFORM_UPSIDEDOWN:I = 0x2

.field private static final SizeComparator:Ljava/util/Comparator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "VideoCaptureRaw"

.field private static final VGA_HEIGHT:I = 0x1e0

.field private static final VGA_WIDTH:I = 0x280

.field private static _holder:[Landroid/view/SurfaceHolder;

.field private static cameraIds:[I

.field private static cameraSizes:[Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation
.end field

.field private static camera_height:[I

.field private static camera_width:[I

.field public static capture_height:[I

.field public static capture_rotation:[I

.field public static capture_width:[I

.field private static final crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

.field private static current_camera:I

.field private static hasBackCamera:Z

.field private static hasFrontCamera:Z

.field private static isGetPreviewSizeInitialized:[Z

.field private static isGingerbread:Z

.field private static isInitialized:Z

.field private static isSamsungTablet10Dot1:Z

.field private static isSamsungTablet7Inch:Z

.field private static preprocessor:Lcom/sgiggle/VideoCapture/PreprocessorThread;

.field private static sInstance:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

.field private static screenHeight:I

.field private static screenWidth:I

.field private static sm:Landroid/hardware/SensorManager;

.field private static timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

.field private static ui_handler:Landroid/os/Handler;

.field private static userFrontCameraRotation:I

.field private static wm:Landroid/view/WindowManager;


# instance fields
.field private _stopped:Z

.field private _suspended:Z

.field private accSensor:Landroid/hardware/Sensor;

.field private acc_x:F

.field private acc_y:F

.field private acc_z:F

.field private camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

.field private frameRate:I

.field private glCapture:Lcom/sgiggle/GLES20/GLCapture;

.field private glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

.field private glRendererHasCapture:Z

.field private isFocusing:Z

.field private isFoundPeak:Z

.field private isSupportAutoFocus:Z

.field private maxHeight:I

.field private maxWidth:I

.field private threshold_high:F

.field private threshold_low:F

.field private threshold_stable:F

.field private type:I

.field private value_peak:F

.field private value_prev:F

.field private weight_now:F

.field private weight_prev:F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 38
    new-array v0, v4, [Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    aput-object v1, v0, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sInstance:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    .line 39
    new-array v0, v4, [Landroid/view/SurfaceHolder;

    aput-object v1, v0, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_holder:[Landroid/view/SurfaceHolder;

    .line 40
    sput v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    .line 42
    new-array v0, v4, [Z

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGetPreviewSizeInitialized:[Z

    .line 57
    new-array v0, v4, [Ljava/util/List;

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraSizes:[Ljava/util/List;

    .line 81
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    .line 82
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    .line 83
    new-array v0, v4, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    .line 84
    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    .line 85
    new-array v0, v4, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    .line 88
    new-array v0, v4, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraIds:[I

    .line 95
    sput v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->userFrontCameraRotation:I

    .line 906
    new-instance v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$1;

    invoke-direct {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$1;-><init>()V

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->SizeComparator:Ljava/util/Comparator;

    .line 935
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    new-instance v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    const/16 v2, 0xc0

    const/16 v3, 0x80

    invoke-direct {v1, v2, v3}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;-><init>(II)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    const/16 v2, 0xf0

    const/16 v3, 0xa0

    invoke-direct {v1, v2, v3}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;-><init>(II)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    const/16 v2, 0x140

    const/16 v3, 0xd6

    invoke-direct {v1, v2, v3}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;-><init>(II)V

    aput-object v1, v0, v4

    const/4 v1, 0x3

    new-instance v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    const/16 v3, 0x180

    const/16 v4, 0x100

    invoke-direct {v2, v3, v4}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;-><init>(II)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    const/16 v3, 0x1e0

    const/16 v4, 0x140

    invoke-direct {v2, v3, v4}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;-><init>(II)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    const/16 v3, 0x240

    const/16 v4, 0x180

    invoke-direct {v2, v3, v4}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;-><init>(II)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    .line 1708
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    .line 1709
    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    move v1, v5

    .line 1711
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 1712
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sgiggle.VideoCaptureTest"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v6

    .line 1717
    :goto_1
    if-eqz v0, :cond_0

    .line 1718
    const-string v0, "VideoCapture"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 1719
    :cond_0
    return-void

    .line 1711
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v5

    goto :goto_1

    .line 42
    :array_0
    .array-data 0x1
        0x0t
        0x0t
    .end array-data

    .line 81
    nop

    :array_1
    .array-data 0x4
        0x40t 0x1t 0x0t 0x0t
        0x40t 0x1t 0x0t 0x0t
    .end array-data

    .line 82
    :array_2
    .array-data 0x4
        0xf0t 0x0t 0x0t 0x0t
        0xf0t 0x0t 0x0t 0x0t
    .end array-data

    .line 83
    :array_3
    .array-data 0x4
        0xc0t 0x0t 0x0t 0x0t
        0xc0t 0x0t 0x0t 0x0t
    .end array-data

    .line 84
    :array_4
    .array-data 0x4
        0x80t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t
    .end array-data

    .line 85
    :array_5
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 88
    :array_6
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0xfft 0xfft 0xfft 0xfft
    .end array-data
.end method

.method public constructor <init>(III)V
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const/16 v0, 0xa

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->frameRate:I

    .line 90
    iput-boolean v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_suspended:Z

    .line 91
    iput-boolean v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_stopped:Z

    .line 93
    const/16 v0, 0xc0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxWidth:I

    .line 94
    const/16 v0, 0x80

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxHeight:I

    .line 118
    const v0, 0x3dcccccd

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_now:F

    .line 119
    const/high16 v0, 0x3f80

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_now:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_prev:F

    .line 120
    const/high16 v0, 0x4000

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->threshold_high:F

    .line 121
    const v0, 0x3e4ccccd

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->threshold_low:F

    .line 122
    const/high16 v0, 0x3f00

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->threshold_stable:F

    .line 149
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VideoCaptureRaw(type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-direct {p0, p2, p3}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initCameraSize(II)V

    .line 154
    if-ne p1, v4, :cond_2

    .line 155
    iput v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    .line 159
    :goto_0
    const-class v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    monitor-enter v0

    .line 160
    :try_start_0
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sInstance:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aput-object p0, v1, v2

    .line 161
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->getInstance()Lcom/sgiggle/GLES20/GLRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    .line 165
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->hasGLCapture()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-static {}, Lcom/sgiggle/GLES20/GLCapture;->getInstance()Lcom/sgiggle/GLES20/GLCapture;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    .line 167
    :cond_0
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isCaptureSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    iput-boolean v4, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRendererHasCapture:Z

    .line 170
    :cond_1
    return-void

    .line 157
    :cond_2
    iput v4, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static checkCamera(I)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 1046
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 1047
    sput-boolean v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    .line 1048
    :cond_0
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initCameraIds()V

    .line 1049
    if-ne p0, v2, :cond_1

    .line 1050
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasBackCamera:Z

    .line 1053
    :goto_0
    return v0

    .line 1051
    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    .line 1052
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasFrontCamera:Z

    goto :goto_0

    .line 1053
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCameraCount()I
    .locals 2

    .prologue
    .line 1061
    const/4 v0, 0x0

    .line 1062
    sget-boolean v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasBackCamera:Z

    if-eqz v1, :cond_0

    .line 1063
    add-int/lit8 v0, v0, 0x1

    .line 1064
    :cond_0
    sget-boolean v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasFrontCamera:Z

    if-eqz v1, :cond_1

    .line 1065
    add-int/lit8 v0, v0, 0x1

    .line 1066
    :cond_1
    return v0
.end method

.method public static getPreviewSize(III)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 967
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPreviewSize() cameraType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v5, v0, p0

    .line 971
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, p0

    .line 973
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraIds:[I

    aget v0, v0, p0

    .line 975
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGetPreviewSizeInitialized:[Z

    aget-boolean v1, v1, p0

    if-nez v1, :cond_2

    .line 977
    invoke-static {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->openCameraSafe(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 978
    if-eqz v0, :cond_1

    .line 980
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "PC36100"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 983
    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 984
    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 986
    :cond_0
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraSizes:[Ljava/util/List;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    aput-object v2, v1, p0

    .line 987
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 988
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraSizes:[Ljava/util/List;

    aget-object v0, v0, p0

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->SizeComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move v1, v5

    .line 990
    :goto_0
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraSizes:[Ljava/util/List;

    aget-object v0, v0, p0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 992
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraSizes:[Ljava/util/List;

    aget-object v0, v0, p0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 993
    const-string v2, "VideoCaptureRaw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preview size of camera "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 996
    :cond_1
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGetPreviewSizeInitialized:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p0

    .line 999
    :cond_2
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraSizes:[Ljava/util/List;

    aget-object v0, v0, p0

    if-eqz v0, :cond_4

    move v1, v5

    .line 1001
    :goto_1
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraSizes:[Ljava/util/List;

    aget-object v0, v0, p0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1003
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraSizes:[Ljava/util/List;

    aget-object v0, v0, p0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1005
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    if-lt v2, p1, :cond_7

    iget v2, v0, Landroid/hardware/Camera$Size;->height:I

    if-lt v2, p2, :cond_7

    .line 1006
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    aput v2, v1, p0

    .line 1007
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    aput v0, v1, p0

    .line 1012
    :cond_3
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v0, v0, p0

    if-lez v0, :cond_4

    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v0, v0, p0

    if-lez v0, :cond_4

    .line 1014
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v0, v0, p0

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v1, v1, p0

    invoke-static {v0, v1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setPreviewSize(II)I

    move-result v0

    .line 1016
    if-ltz v0, :cond_8

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    array-length v1, v1

    if-ge v0, v1, :cond_8

    .line 1018
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;->width:I

    aput v2, v1, p0

    .line 1019
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    aget-object v0, v2, v0

    iget v0, v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;->height:I

    aput v0, v1, p0

    .line 1020
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPreviewSize returns camera: ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), crop: ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1032
    :cond_4
    :goto_2
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v0, v0, p0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v0, v0, p0

    if-nez v0, :cond_6

    .line 1034
    :cond_5
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput p1, v0, p0

    .line 1035
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput p2, v0, p0

    .line 1036
    const-string v0, "VideoCaptureRaw"

    const-string v1, "Cannot get supported preview size!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    :cond_6
    return-void

    .line 1001
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 1026
    :cond_8
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v5, v0, p0

    .line 1027
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, p0

    goto :goto_2
.end method

.method private initBackCameraSize()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 379
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initBackCameraSizeHardCoded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxWidth:I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxHeight:I

    invoke-static {v2, v0, v1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->selectCameraSizeForHTC(III)Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxWidth:I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxHeight:I

    invoke-static {v2, v0, v1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getPreviewSize(III)V

    .line 384
    :cond_0
    return-void
.end method

.method public static initBackCameraSizeHardCoded()Z
    .locals 7

    .prologue
    const/16 v6, 0x140

    const/16 v5, 0xa0

    const/4 v4, 0x1

    const/16 v2, 0xf0

    const/4 v3, 0x0

    .line 219
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Motorola"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROID"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v6, v0, v3

    .line 221
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v2, v0, v3

    .line 222
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v6, v0, v3

    .line 223
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v3

    move v0, v4

    .line 373
    :goto_0
    return v0

    .line 225
    :cond_0
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 230
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    :cond_1
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v6, v0, v3

    .line 232
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v2, v0, v3

    .line 233
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v6, v0, v3

    .line 234
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v3

    :goto_1
    move v0, v4

    .line 252
    goto :goto_0

    .line 235
    :cond_2
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-M820-BST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SCH-M828C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 236
    :cond_3
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 237
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 238
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 239
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    goto :goto_1

    .line 240
    :cond_4
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P1010"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 242
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v6, v0, v3

    .line 243
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v2, v0, v3

    .line 244
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v1, v1, v3

    aput v1, v0, v3

    .line 245
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aget v1, v1, v3

    int-to-float v1, v1

    const/high16 v2, 0x3fc0

    div-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v3

    goto :goto_1

    .line 247
    :cond_5
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x100

    aput v1, v0, v3

    .line 248
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 249
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 250
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    goto :goto_1

    .line 253
    :cond_6
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_8

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-D700"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T759"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 255
    :cond_7
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 256
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x90

    aput v1, v0, v3

    .line 257
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 258
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v3

    move v0, v4

    .line 259
    goto/16 :goto_0

    .line 260
    :cond_8
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_a

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 261
    :cond_9
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v6, v0, v3

    .line 262
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v2, v0, v3

    .line 263
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v6, v0, v3

    .line 264
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v3

    move v0, v4

    .line 265
    goto/16 :goto_0

    .line 266
    :cond_a
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_b

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9103"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 267
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 268
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x80

    aput v1, v0, v3

    .line 269
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 270
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v3

    move v0, v4

    .line 271
    goto/16 :goto_0

    .line 272
    :cond_b
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_d

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_d

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-D710"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SHW-M250S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 275
    :cond_c
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x280

    aput v1, v0, v3

    .line 276
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x1e0

    aput v1, v0, v3

    .line 277
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x240

    aput v1, v0, v3

    .line 278
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x180

    aput v1, v0, v3

    move v0, v4

    .line 279
    goto/16 :goto_0

    .line 280
    :cond_d
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_f

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T989"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I727"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 282
    :cond_e
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x1e0

    aput v1, v0, v3

    .line 283
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v6, v0, v3

    .line 284
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x1e0

    aput v1, v0, v3

    .line 285
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v6, v0, v3

    move v0, v4

    .line 286
    goto/16 :goto_0

    .line 287
    :cond_f
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_10

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P7510"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    :cond_10
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "asus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Transformer TF101"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 289
    :cond_11
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x1e0

    aput v1, v0, v3

    .line 290
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v6, v0, v3

    .line 291
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x1e0

    aput v1, v0, v3

    .line 292
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v6, v0, v3

    move v0, v4

    .line 293
    goto/16 :goto_0

    .line 294
    :cond_12
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "PC36100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 295
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 296
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 297
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 298
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    move v0, v4

    .line 299
    goto/16 :goto_0

    .line 300
    :cond_13
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "PG06100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 301
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 302
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 303
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 304
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    move v0, v4

    .line 305
    goto/16 :goto_0

    .line 306
    :cond_14
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "ADR6400L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 308
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 309
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 310
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 311
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    move v0, v4

    .line 312
    goto/16 :goto_0

    .line 313
    :cond_15
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Desire S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 314
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 315
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x90

    aput v1, v0, v3

    .line 316
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 317
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v3

    move v0, v4

    .line 318
    goto/16 :goto_0

    .line 319
    :cond_16
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-MS910"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 320
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 321
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 322
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 323
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    move v0, v4

    .line 324
    goto/16 :goto_0

    .line 325
    :cond_17
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_18

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "VS910 4G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 326
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 327
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 328
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 329
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    move v0, v4

    .line 330
    goto/16 :goto_0

    .line 331
    :cond_18
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_19

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P925"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 332
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v6, v0, v3

    .line 333
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v2, v0, v3

    .line 334
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v6, v0, v3

    .line 335
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v3

    move v0, v4

    .line 336
    goto/16 :goto_0

    .line 337
    :cond_19
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1a

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 338
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 339
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x80

    aput v1, v0, v3

    .line 340
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0xc0

    aput v1, v0, v3

    .line 341
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v3

    move v0, v4

    .line 342
    goto/16 :goto_0

    .line 343
    :cond_1a
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_1c

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1c

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "M886"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 344
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 345
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 346
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 347
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    .line 348
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1b

    .line 349
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x2

    aput v1, v0, v3

    :goto_2
    move v0, v4

    .line 352
    goto/16 :goto_0

    .line 351
    :cond_1b
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v4, v0, v3

    goto :goto_2

    .line 353
    :cond_1c
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_1d

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HUAWEI-M920"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 354
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 355
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 356
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 357
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    move v0, v4

    .line 358
    goto/16 :goto_0

    .line 359
    :cond_1d
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_1e

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HUAWEI-M921"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 360
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v2, v0, v3

    .line 361
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v3

    .line 362
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v2, v0, v3

    .line 363
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v3

    .line 364
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v4, v0, v3

    move v0, v4

    .line 365
    goto/16 :goto_0

    .line 366
    :cond_1e
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Galaxy Nexus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 367
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x280

    aput v1, v0, v3

    .line 368
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x1e0

    aput v1, v0, v3

    .line 369
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x240

    aput v1, v0, v3

    .line 370
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x180

    aput v1, v0, v3

    move v0, v4

    .line 371
    goto/16 :goto_0

    :cond_1f
    move v0, v3

    .line 373
    goto/16 :goto_0
.end method

.method private static initCameraIds()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 200
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isInitialized:Z

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    return-void

    .line 202
    :cond_1
    sput-boolean v4, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isInitialized:Z

    .line 203
    invoke-static {}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getNumberOfCameras()I

    move-result v0

    .line 204
    new-instance v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;

    invoke-direct {v1}, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;-><init>()V

    move v2, v5

    .line 205
    :goto_0
    if-ge v2, v0, :cond_0

    .line 206
    invoke-static {v2, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getCameraInfo(ILcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;)V

    .line 208
    sget-boolean v3, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasBackCamera:Z

    if-nez v3, :cond_3

    iget v3, v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    if-nez v3, :cond_3

    .line 209
    sget-object v3, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraIds:[I

    aput v2, v3, v5

    .line 210
    sput-boolean v4, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasBackCamera:Z

    .line 205
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 211
    :cond_3
    sget-boolean v3, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasFrontCamera:Z

    if-nez v3, :cond_2

    iget v3, v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    if-ne v3, v4, :cond_2

    .line 212
    sget-object v3, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraIds:[I

    aput v2, v3, v4

    .line 213
    sput-boolean v4, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasFrontCamera:Z

    goto :goto_1
.end method

.method private initCameraSize(II)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 859
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initCameraSize( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    iput p1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxWidth:I

    .line 862
    iput p2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxHeight:I

    .line 865
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasBackCamera:Z

    if-eqz v0, :cond_0

    .line 866
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initBackCameraSize()V

    .line 867
    :cond_0
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->hasFrontCamera:Z

    if-eqz v0, :cond_1

    .line 868
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initFrontCameraSize()V

    .line 871
    :cond_1
    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->userFrontCameraRotation:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 872
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    sget v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->userFrontCameraRotation:I

    aput v1, v0, v3

    .line 875
    :cond_2
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v0, v0, v4

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v1, v1, v4

    invoke-static {v4, v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->setCameraSize(III)V

    .line 878
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aget v0, v0, v3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aget v0, v0, v3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    :cond_3
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_4

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 880
    :cond_4
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v0, v0, v3

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v1, v1, v3

    invoke-static {v3, v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->setCameraSize(III)V

    .line 884
    :goto_0
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aget v0, v0, v4

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aget v1, v1, v4

    invoke-static {v4, v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->setCaptureSize(III)V

    .line 885
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aget v0, v0, v3

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aget v1, v1, v3

    if-le v0, v1, :cond_6

    .line 886
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aget v0, v0, v3

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aget v1, v1, v3

    invoke-static {v3, v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->setCaptureSize(III)V

    .line 890
    :goto_1
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[front] camera: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " crop: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rotation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " [back] camera: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " crop: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rotation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->updateLayout()V

    .line 904
    return-void

    .line 882
    :cond_5
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aget v0, v0, v3

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aget v1, v1, v3

    invoke-static {v3, v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->setCameraSize(III)V

    goto/16 :goto_0

    .line 888
    :cond_6
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aget v0, v0, v3

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aget v1, v1, v3

    invoke-static {v3, v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->setCaptureSize(III)V

    goto/16 :goto_1
.end method

.method public static initFPSRange(Landroid/hardware/Camera$Parameters;II)Ljava/util/List;
    .locals 11
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/hardware/Camera$Parameters;",
            "II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 686
    new-array v0, v9, [I

    fill-array-data v0, :array_0

    .line 689
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v1

    .line 690
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 692
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 694
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 695
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 696
    const-string v3, "VideoCaptureRaw"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSupportedPreviewFpsRange() returned:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 699
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 700
    new-array v0, v9, [I

    fill-array-data v0, :array_1

    move-object v2, v0

    .line 701
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 704
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 707
    aget v3, v0, v7

    aget v4, v0, v8

    mul-int/lit16 v5, p1, 0x3e8

    mul-int/lit16 v6, p2, 0x3e8

    invoke-static {v3, v4, v5, v6}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->rangesIntersect(IIII)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 709
    aget v1, v0, v7

    .line 710
    aget v0, v0, v8

    .line 754
    :goto_2
    :try_start_0
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 755
    const-string v2, "VideoCaptureRaw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPreviewFpsRange("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 761
    :goto_3
    new-array v2, v9, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 714
    :cond_1
    mul-int/lit16 v3, p2, 0x3e8

    aget v4, v0, v7

    if-ge v3, v4, :cond_3

    .line 717
    aget v1, v2, v8

    if-lez v1, :cond_2

    .line 719
    aget v0, v2, v7

    .line 720
    aget v1, v2, v8

    move v10, v1

    move v1, v0

    move v0, v10

    .line 721
    goto :goto_2

    .line 726
    :cond_2
    aget v1, v0, v7

    .line 727
    aget v0, v0, v8

    goto :goto_2

    .line 735
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v2, v0

    .line 736
    goto :goto_1

    .line 739
    :cond_4
    aget v1, v0, v7

    .line 740
    aget v0, v0, v8

    goto :goto_2

    .line 748
    :cond_5
    mul-int/lit16 v0, p1, 0x3e8

    .line 749
    mul-int/lit16 v1, p2, 0x3e8

    move v10, v1

    move v1, v0

    move v0, v10

    goto :goto_2

    .line 757
    :catch_0
    move-exception v2

    .line 758
    const-string v3, "VideoCaptureRaw"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception setPreviewFpsRange(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_6
    move v0, v7

    move v1, v7

    goto/16 :goto_2

    .line 686
    nop

    :array_0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 700
    :array_1
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public static initFrameRate(Landroid/hardware/Camera$Parameters;I)I
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 772
    :try_start_0
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFrameRates()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 780
    :goto_0
    if-nez v1, :cond_0

    move v0, v5

    .line 799
    :goto_1
    return v0

    .line 774
    :catch_0
    move-exception v0

    .line 776
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 777
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_0

    .line 782
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move v0, v5

    .line 783
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 784
    const-string v2, "VideoCaptureRaw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSupportedPreviewFrameRates "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    move v2, v5

    move v0, p1

    .line 787
    :goto_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 788
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 789
    if-lt v0, p1, :cond_3

    .line 793
    :cond_2
    :try_start_1
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    .line 794
    const-string v1, "VideoCaptureRaw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPreviewFrameRate("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 795
    :catch_1
    move-exception v1

    .line 796
    const-string v2, "VideoCaptureRaw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception setPreviewFrameRate(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 787
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method private initFrontCameraSize()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 626
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init front camera (manufacturer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " model="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " version="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initFrontCameraSizeHardCoded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 630
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxWidth:I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxHeight:I

    invoke-static {v3, v0, v1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->selectCameraSizeForHTC(III)Z

    move-result v0

    if-nez v0, :cond_2

    .line 632
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxWidth:I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->maxHeight:I

    invoke-static {v3, v0, v1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getPreviewSize(III)V

    .line 634
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_4

    .line 635
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Flyer P510e"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Glacier"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P990"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-SU660"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-SU760"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Incredible S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9001"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    .line 641
    :cond_1
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v4, v0, v3

    .line 646
    :goto_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Flyer P510e"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 647
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v3, v0, v3

    .line 654
    :cond_2
    :goto_1
    return-void

    .line 643
    :cond_3
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v3, v0, v3

    goto :goto_0

    .line 650
    :cond_4
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v4, v0, v3

    goto :goto_1
.end method

.method public static initFrontCameraSizeHardCoded()Z
    .locals 7

    .prologue
    const/16 v6, 0xa0

    const/16 v5, 0xc0

    const/16 v4, 0x140

    const/16 v3, 0xf0

    const/4 v2, 0x1

    .line 387
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Nexus S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 390
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 391
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v4, v0, v2

    .line 392
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v2

    .line 393
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 622
    :goto_0
    return v0

    .line 395
    :cond_0
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 396
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-M920"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T959V"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SCH-I510"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T839"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I997"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "YP-GB1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "YP-G1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 404
    :cond_1
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v5, v0, v2

    .line 405
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x90

    aput v1, v0, v2

    .line 406
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v5, v0, v2

    .line 407
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 408
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x2

    aput v1, v0, v2

    :goto_1
    move v0, v2

    .line 431
    goto :goto_0

    .line 409
    :cond_2
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 410
    :cond_3
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0xb0

    aput v1, v0, v2

    .line 411
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x90

    aput v1, v0, v2

    .line 412
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x60

    aput v1, v0, v2

    .line 413
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x90

    aput v1, v0, v2

    .line 414
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x3

    aput v1, v0, v2

    goto :goto_1

    .line 415
    :cond_4
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P1010"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 417
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 418
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 419
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v6, v0, v2

    .line 420
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v3, v0, v2

    .line 421
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x3

    aput v1, v0, v2

    goto :goto_1

    .line 425
    :cond_5
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x100

    aput v1, v0, v2

    .line 426
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v2

    .line 427
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 428
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v2

    .line 429
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x3

    aput v1, v0, v2

    goto :goto_1

    .line 432
    :cond_6
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P7510"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "asus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Transformer TF101"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 434
    :cond_8
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x1e0

    aput v1, v0, v2

    .line 435
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v4, v0, v2

    .line 436
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x1e0

    aput v1, v0, v2

    .line 437
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v4, v0, v2

    .line 438
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 439
    goto/16 :goto_0

    .line 440
    :cond_9
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "PC36100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 441
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_a

    .line 442
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v5, v0, v2

    .line 443
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x90

    aput v1, v0, v2

    .line 444
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v5, v0, v2

    .line 445
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 452
    :goto_2
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_b

    .line 453
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    :goto_3
    move v0, v2

    .line 456
    goto/16 :goto_0

    .line 447
    :cond_a
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v3, v0, v2

    .line 448
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v6, v0, v2

    .line 449
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v3, v0, v2

    .line 450
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v6, v0, v2

    goto :goto_2

    .line 455
    :cond_b
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x2

    aput v1, v0, v2

    goto :goto_3

    .line 457
    :cond_c
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "ADR6400L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 459
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v3, v0, v2

    .line 460
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v6, v0, v2

    .line 461
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v3, v0, v2

    .line 462
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v6, v0, v2

    .line 463
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    sget-boolean v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v1, :cond_d

    move v1, v2

    :goto_4
    aput v1, v0, v2

    move v0, v2

    .line 464
    goto/16 :goto_0

    .line 463
    :cond_d
    const/4 v1, 0x2

    goto :goto_4

    .line 465
    :cond_e
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Desire S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 466
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v5, v0, v2

    .line 467
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x90

    aput v1, v0, v2

    .line 468
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v5, v0, v2

    .line 469
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 470
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x2

    aput v1, v0, v2

    move v0, v2

    .line 471
    goto/16 :goto_0

    .line 472
    :cond_f
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-MS910"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 474
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v3, v0, v2

    .line 475
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v6, v0, v2

    .line 476
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v3, v0, v2

    .line 477
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v6, v0, v2

    .line 478
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    sget-boolean v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v1, :cond_10

    move v1, v2

    :goto_5
    aput v1, v0, v2

    move v0, v2

    .line 479
    goto/16 :goto_0

    .line 478
    :cond_10
    const/4 v1, 0x2

    goto :goto_5

    .line 480
    :cond_11
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Sony Ericsson"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "R800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 482
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 483
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 484
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v4, v0, v2

    .line 485
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v2

    .line 486
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 487
    goto/16 :goto_0

    .line 488
    :cond_12
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 491
    :cond_13
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x280

    aput v1, v0, v2

    .line 492
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x1e0

    aput v1, v0, v2

    .line 493
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v4, v0, v2

    .line 494
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x1e0

    aput v1, v0, v2

    .line 495
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x3

    aput v1, v0, v2

    move v0, v2

    .line 496
    goto/16 :goto_0

    .line 497
    :cond_14
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_17

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "VS910 4G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 498
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v3, v0, v2

    .line 499
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v6, v0, v2

    .line 500
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v5, v0, v2

    .line 501
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 502
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    sget-boolean v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v1, :cond_16

    move v1, v2

    :goto_6
    aput v1, v0, v2

    .line 622
    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 502
    :cond_16
    const/4 v1, 0x2

    goto :goto_6

    .line 503
    :cond_17
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_18

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P925"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_18

    .line 504
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 505
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 506
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v4, v0, v2

    .line 507
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v2

    .line 508
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x2

    aput v1, v0, v2

    move v0, v2

    .line 509
    goto/16 :goto_0

    .line 510
    :cond_18
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_19

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 511
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v5, v0, v2

    .line 512
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 513
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v5, v0, v2

    .line 514
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 515
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x2

    aput v1, v0, v2

    move v0, v2

    .line 516
    goto/16 :goto_0

    .line 517
    :cond_19
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_1b

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1b

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-D700"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T759"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 519
    :cond_1a
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v5, v0, v2

    .line 520
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x90

    aput v1, v0, v2

    .line 521
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v5, v0, v2

    .line 522
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 523
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 524
    goto/16 :goto_0

    .line 525
    :cond_1b
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_1d

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1d

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 526
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x100

    aput v1, v0, v2

    .line 527
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, v2

    .line 528
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1c

    .line 530
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v5, v0, v2

    .line 531
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 532
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    :goto_7
    move v0, v2

    .line 540
    goto/16 :goto_0

    .line 536
    :cond_1c
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 537
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, v2

    .line 538
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x3

    aput v1, v0, v2

    goto :goto_7

    .line 541
    :cond_1d
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_1e

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1e

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9103"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 542
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v5, v0, v2

    .line 543
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 544
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v5, v0, v2

    .line 545
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x80

    aput v1, v0, v2

    .line 546
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 547
    goto/16 :goto_0

    .line 548
    :cond_1e
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_20

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_20

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-D710"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SHW-M250S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 551
    :cond_1f
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x280

    aput v1, v0, v2

    .line 552
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x1e0

    aput v1, v0, v2

    .line 553
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x240

    aput v1, v0, v2

    .line 554
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x180

    aput v1, v0, v2

    .line 555
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 556
    goto/16 :goto_0

    .line 557
    :cond_20
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_22

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_22

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T989"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_21

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I727"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 559
    :cond_21
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x1e0

    aput v1, v0, v2

    .line 560
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v4, v0, v2

    .line 561
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x1e0

    aput v1, v0, v2

    .line 562
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v4, v0, v2

    .line 563
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 564
    goto/16 :goto_0

    .line 565
    :cond_22
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_24

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_24

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "M886"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 566
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 567
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 568
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_23

    .line 570
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    .line 571
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v3, v0, v2

    .line 572
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v6, v0, v2

    :goto_8
    move v0, v2

    .line 578
    goto/16 :goto_0

    .line 574
    :cond_23
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v6, v0, v2

    .line 575
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v3, v0, v2

    .line 576
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x3

    aput v1, v0, v2

    goto :goto_8

    .line 579
    :cond_24
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_25

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HUAWEI-M920"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 580
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 581
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 582
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v3, v0, v2

    .line 583
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v6, v0, v2

    .line 584
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 585
    goto/16 :goto_0

    .line 586
    :cond_25
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_26

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HUAWEI-M921"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 587
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 588
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 589
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v3, v0, v2

    .line 590
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v6, v0, v2

    .line 591
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 592
    goto/16 :goto_0

    .line 593
    :cond_26
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-nez v0, :cond_27

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "PANTECH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_27

    .line 594
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 595
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 596
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v6, v0, v2

    .line 597
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v3, v0, v2

    .line 598
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x3

    aput v1, v0, v2

    move v0, v2

    .line 599
    goto/16 :goto_0

    .line 600
    :cond_27
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Galaxy Nexus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 601
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x280

    aput v1, v0, v2

    .line 602
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x1e0

    aput v1, v0, v2

    .line 603
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x240

    aput v1, v0, v2

    .line 604
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x180

    aput v1, v0, v2

    .line 605
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 606
    goto/16 :goto_0

    .line 607
    :cond_28
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Lenovo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_29

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "A1_07"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 608
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 609
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 610
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v4, v0, v2

    .line 611
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v2

    .line 612
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x2

    aput v1, v0, v2

    move v0, v2

    .line 613
    goto/16 :goto_0

    .line 614
    :cond_29
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LT22i"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 615
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, v2

    .line 616
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v3, v0, v2

    .line 617
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v4, v0, v2

    .line 618
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xd6

    aput v1, v0, v2

    .line 619
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 620
    goto/16 :goto_0
.end method

.method private isBecameStable([F)Z
    .locals 6
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1657
    .line 1659
    aget v0, p1, v3

    const v1, 0x411cf5c3

    sub-float/2addr v0, v1

    aput v0, p1, v3

    .line 1660
    aget v0, p1, v4

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_now:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_x:F

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_prev:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_x:F

    .line 1661
    aget v0, p1, v5

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_now:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_y:F

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_prev:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_y:F

    .line 1662
    aget v0, p1, v3

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_now:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_z:F

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->weight_prev:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_z:F

    .line 1663
    aget v0, p1, v4

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_x:F

    sub-float/2addr v0, v1

    .line 1664
    aget v1, p1, v5

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_y:F

    sub-float/2addr v1, v2

    .line 1665
    aget v2, p1, v3

    iget v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->acc_z:F

    sub-float/2addr v2, v3

    .line 1666
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    .line 1669
    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->value_prev:F

    sub-float v1, v0, v1

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->threshold_high:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->value_peak:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 1670
    :cond_0
    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->value_peak:F

    .line 1671
    iput-boolean v5, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isFoundPeak:Z

    move v1, v4

    .line 1681
    :goto_0
    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->value_prev:F

    .line 1684
    return v1

    .line 1675
    :cond_1
    iget-boolean v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isFoundPeak:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->value_peak:F

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->threshold_low:F

    mul-float/2addr v1, v2

    cmpg-float v1, v0, v1

    if-gez v1, :cond_2

    .line 1676
    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->value_prev:F

    sub-float/2addr v1, v0

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->threshold_stable:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 1677
    iput-boolean v4, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isFoundPeak:Z

    move v1, v5

    .line 1678
    goto :goto_0

    :cond_2
    move v1, v4

    goto :goto_0
.end method

.method private isCameraThread()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1493
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRendererHasCapture:Z

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static rangesIntersect(IIII)Z
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 668
    if-le p0, p2, :cond_0

    move v0, p0

    .line 669
    :goto_0
    if-ge p1, p3, :cond_1

    move v1, p1

    .line 670
    :goto_1
    if-gt v0, v1, :cond_2

    .line 671
    const/4 v0, 0x1

    .line 673
    :goto_2
    return v0

    :cond_0
    move v0, p2

    .line 668
    goto :goto_0

    :cond_1
    move v1, p3

    .line 669
    goto :goto_1

    .line 673
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static native registerPrThread()V
.end method

.method private static selectCameraSizeForHTC(III)Z
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x140

    const/16 v4, 0x1e0

    const/16 v3, 0x180

    const/4 v2, 0x1

    .line 811
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Bliss"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Rhyme"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Runnymede"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 817
    :goto_0
    if-nez v0, :cond_2

    move v0, v6

    .line 851
    :goto_1
    return v0

    :cond_1
    move v0, v6

    .line 811
    goto :goto_0

    .line 822
    :cond_2
    const/16 v0, 0xc0

    if-gt p1, v0, :cond_3

    const/16 v0, 0x80

    if-gt p2, v0, :cond_3

    .line 824
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0xf0

    aput v1, v0, p0

    .line 825
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0xa0

    aput v1, v0, p0

    .line 826
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0xf0

    aput v1, v0, p0

    .line 827
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0xa0

    aput v1, v0, p0

    .line 849
    :goto_2
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    aput v2, v0, v2

    move v0, v2

    .line 851
    goto :goto_1

    .line 829
    :cond_3
    if-gt p1, v3, :cond_4

    const/16 v0, 0x100

    if-gt p2, v0, :cond_4

    .line 830
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v3, v0, p0

    .line 831
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    const/16 v1, 0x120

    aput v1, v0, p0

    .line 832
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v3, v0, p0

    .line 833
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    const/16 v1, 0x100

    aput v1, v0, p0

    goto :goto_2

    .line 835
    :cond_4
    if-gt p1, v4, :cond_5

    if-gt p2, v5, :cond_5

    .line 836
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    aput v4, v0, p0

    .line 837
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v5, v0, p0

    .line 838
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    aput v4, v0, p0

    .line 839
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v5, v0, p0

    goto :goto_2

    .line 843
    :cond_5
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    const/16 v1, 0x280

    aput v1, v0, p0

    .line 844
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    aput v4, v0, p0

    .line 845
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    const/16 v1, 0x240

    aput v1, v0, p0

    .line 846
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    aput v3, v0, p0

    goto :goto_2
.end method

.method public static setActivityHandler(Landroid/os/Handler;)V
    .locals 2
    .parameter

    .prologue
    .line 1141
    const-class v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    monitor-enter v0

    .line 1142
    :try_start_0
    sput-object p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->ui_handler:Landroid/os/Handler;

    .line 1143
    monitor-exit v0

    .line 1144
    return-void

    .line 1143
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private native setCallback(II)V
.end method

.method private setCurrentCamera(I)V
    .locals 0
    .parameter

    .prologue
    .line 1339
    sput p1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    .line 1340
    return-void
.end method

.method public static setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 2
    .parameter

    .prologue
    .line 1120
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_holder:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 1121
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_holder:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x1

    aput-object p0, v0, v1

    .line 1122
    return-void
.end method

.method private static setPreviewSize(II)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 942
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    array-length v0, v0

    .line 946
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    array-length v1, v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    :goto_0
    if-ltz v1, :cond_0

    .line 948
    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;->width:I

    if-lt p0, v2, :cond_1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->crop_size:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw$Size;->height:I

    if-lt p1, v2, :cond_1

    move v0, v1

    .line 955
    :cond_0
    return v0

    .line 946
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public static setUserFrontCamRotation(I)V
    .locals 1
    .parameter

    .prologue
    .line 660
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    if-ltz p0, :cond_1

    const/4 v0, 0x4

    if-gt p0, v0, :cond_1

    .line 661
    :cond_0
    sput p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->userFrontCameraRotation:I

    .line 662
    :cond_1
    return-void
.end method

.method private setupCameraParams(Lcom/sgiggle/VideoCapture/CameraWrapper;)Z
    .locals 10
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1504
    invoke-virtual {p1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 1506
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v0

    const/16 v2, 0x11

    if-eq v0, v2, :cond_0

    .line 1507
    const-string v0, "VideoCaptureRaw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not supported PixelFormat "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 1601
    :goto_0
    return v0

    .line 1515
    :cond_0
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    move v3, v7

    move v4, v7

    move v5, v7

    .line 1518
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 1519
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1520
    const-string v6, "auto"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v0, v8

    move v4, v5

    .line 1518
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v5, v4

    move v4, v0

    goto :goto_1

    .line 1522
    :cond_1
    const-string v6, "continuous-video"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v4

    move v4, v8

    .line 1523
    goto :goto_2

    .line 1526
    :cond_2
    iput-boolean v7, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSupportAutoFocus:Z

    .line 1527
    if-eqz v5, :cond_a

    .line 1528
    const-string v0, "continuous-video"

    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1536
    :cond_3
    :goto_3
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "LG-MS910"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "Sony Ericsson"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "R800"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    if-ne v0, v8, :cond_5

    .line 1538
    :cond_4
    const-string v0, "VideoCaptureRaw"

    const-string v2, "Disable Autofocus"

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1539
    iput-boolean v7, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSupportAutoFocus:Z

    .line 1542
    :cond_5
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "lge"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "VS910 4G"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1544
    const/16 v0, 0x500

    const/16 v2, 0x3c0

    invoke-virtual {v1, v0, v2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 1550
    :cond_6
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Nexus S"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "Sony Ericsson"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "R800"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Sensation"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Bliss"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Rhyme"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Runnymede"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Sensation XL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1560
    :cond_8
    const/16 v0, 0xf

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->frameRate:I

    .line 1567
    :cond_9
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 1568
    const-string v3, "getSupportedPreviewFpsRange"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1569
    :try_start_1
    const-string v3, "setPreviewFpsRange"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v2

    move-object v3, v0

    .line 1573
    :goto_4
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 1574
    const-string v5, "getSupportedPreviewFrameRates"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    .line 1577
    :goto_5
    if-eqz v3, :cond_b

    if-eqz v2, :cond_b

    .line 1578
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->frameRate:I

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->frameRate:I

    invoke-static {v1, v0, v2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initFPSRange(Landroid/hardware/Camera$Parameters;II)Ljava/util/List;

    .line 1586
    :goto_6
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v0, v0, v2

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    iget v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v2, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 1591
    :try_start_3
    invoke-virtual {p1, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    move v0, v8

    .line 1601
    goto/16 :goto_0

    .line 1530
    :cond_a
    if-eqz v4, :cond_3

    .line 1531
    const-string v0, "auto"

    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1532
    iput-boolean v8, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSupportAutoFocus:Z

    goto/16 :goto_3

    .line 1570
    :catch_0
    move-exception v0

    move-object v0, v9

    :goto_7
    move-object v2, v9

    move-object v3, v0

    goto :goto_4

    .line 1575
    :catch_1
    move-exception v0

    move-object v0, v9

    goto :goto_5

    .line 1579
    :cond_b
    if-eqz v0, :cond_c

    .line 1580
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->frameRate:I

    invoke-static {v1, v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initFrameRate(Landroid/hardware/Camera$Parameters;I)I

    goto :goto_6

    .line 1582
    :cond_c
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->frameRate:I

    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    goto :goto_6

    .line 1593
    :catch_2
    move-exception v0

    .line 1597
    const-string v2, "VideoCaptureRaw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setParameters called exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    const-string v0, "VideoCaptureRaw"

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->flatten()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 1599
    goto/16 :goto_0

    .line 1570
    :catch_3
    move-exception v2

    goto :goto_7

    :cond_d
    move v0, v4

    move v4, v5

    goto/16 :goto_2
.end method

.method private setupGLRenderer()V
    .locals 10

    .prologue
    const/high16 v9, 0x4387

    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 1605
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    iget v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v2, v2, v3

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    iget v4, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v3, v3, v4

    sget-object v4, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    iget v5, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v4, v4, v5

    sget-object v5, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    iget v6, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v5, v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/GLES20/GLRenderer;->setClip(IIIII)V

    .line 1606
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    if-eqz v0, :cond_2

    .line 1607
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    if-nez v0, :cond_1

    .line 1608
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    move v2, v8

    move v3, v7

    move v4, v1

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLRenderer;->setTransform(IFZZZZ)V

    .line 1633
    :cond_0
    :goto_0
    return-void

    .line 1609
    :cond_1
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    if-ne v0, v1, :cond_0

    .line 1610
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    move v2, v8

    move v3, v7

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLRenderer;->setTransform(IFZZZZ)V

    goto :goto_0

    .line 1613
    :cond_2
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1630
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown rotation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    iget v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1615
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    move v2, v8

    move v3, v7

    move v4, v7

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLRenderer;->setTransform(IFZZZZ)V

    goto :goto_0

    .line 1618
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    move v2, v8

    move v3, v1

    move v4, v7

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLRenderer;->setTransform(IFZZZZ)V

    goto :goto_0

    .line 1621
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    move v2, v8

    move v3, v7

    move v4, v7

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLRenderer;->setTransform(IFZZZZ)V

    goto :goto_0

    .line 1624
    :pswitch_3
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    move v2, v9

    move v3, v7

    move v4, v7

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLRenderer;->setTransform(IFZZZZ)V

    goto :goto_0

    .line 1627
    :pswitch_4
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    move v2, v9

    move v3, v1

    move v4, v7

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/GLES20/GLRenderer;->setTransform(IFZZZZ)V

    goto :goto_0

    .line 1613
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private startCameraRecording()Z
    .locals 8

    .prologue
    const/4 v2, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1370
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-nez v0, :cond_1

    .line 1371
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraIds:[I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v0, v0, v1

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->open(I)Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    .line 1372
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-nez v0, :cond_0

    .line 1373
    const-string v0, "VideoCaptureRaw"

    const-string v1, "no camera"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v6

    .line 1428
    :goto_0
    return v0

    .line 1377
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setupCameraParams(Lcom/sgiggle/VideoCapture/CameraWrapper;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1378
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->release()V

    .line 1379
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    move v0, v6

    .line 1380
    goto :goto_0

    .line 1384
    :cond_1
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRendererHasCapture:Z

    if-nez v0, :cond_2

    .line 1385
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->addCallbackBuffers()V

    .line 1388
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    if-nez v0, :cond_b

    .line 1389
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    if-ne v0, v5, :cond_7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_7

    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-eq v0, v2, :cond_3

    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    :cond_3
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_4

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1393
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    const/16 v1, 0x10e

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setDisplayOrientation(I)V

    .line 1404
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    if-eqz v0, :cond_c

    .line 1405
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getCamera()Landroid/hardware/Camera;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    iget v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v2, v2, v3

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    iget v4, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v3, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/sgiggle/GLES20/GLCapture;->setCamera(Landroid/hardware/Camera;II)V

    .line 1412
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->startPreview()V

    .line 1414
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sm:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSupportAutoFocus:Z

    if-ne v0, v5, :cond_6

    .line 1415
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sm:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->accSensor:Landroid/hardware/Sensor;

    .line 1416
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->accSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_6

    .line 1417
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->accSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_6
    move v0, v5

    .line 1428
    goto/16 :goto_0

    .line 1394
    :cond_7
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    if-ne v0, v5, :cond_9

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_9

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "A1_07"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Lenovo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 1395
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    const/16 v1, 0xb4

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setDisplayOrientation(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1419
    :catch_0
    move-exception v0

    .line 1420
    const-string v1, "VideoCaptureRaw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to start recording "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1422
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-eqz v0, :cond_8

    .line 1423
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->release()V

    .line 1424
    iput-object v7, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    :cond_8
    move v0, v6

    .line 1426
    goto/16 :goto_0

    .line 1396
    :cond_9
    :try_start_1
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSamsungTablet10Dot1:Z

    if-eqz v0, :cond_a

    .line 1397
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    const/16 v1, 0x5a

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setDisplayOrientation(I)V

    goto/16 :goto_1

    .line 1398
    :cond_a
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_5

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "M886"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_5

    .line 1399
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    const/16 v1, 0xb4

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setDisplayOrientation(I)V

    goto/16 :goto_1

    .line 1401
    :cond_b
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setupGLRenderer()V

    goto/16 :goto_1

    .line 1406
    :cond_c
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    if-eqz v0, :cond_d

    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isNotNeededPreviewDisplay()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1407
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto/16 :goto_2

    .line 1409
    :cond_d
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_holder:[Landroid/view/SurfaceHolder;

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method public static staticResumeRecording()V
    .locals 3

    .prologue
    .line 1179
    const-string v0, "VideoCaptureRaw"

    const-string v1, "staticResumeRecording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1181
    const-class v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    monitor-enter v0

    .line 1182
    :try_start_0
    sget v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 1183
    const-string v1, "VideoCaptureRaw"

    const-string v2, "staticResumeRecording: no active camera"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1184
    monitor-exit v0

    .line 1190
    :goto_0
    return-void

    .line 1186
    :cond_0
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sInstance:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    sget v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    aget-object v1, v1, v2

    if-eqz v1, :cond_1

    .line 1187
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sInstance:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    sget v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->resumeRecording()V

    .line 1189
    :cond_1
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static staticSuspendRecording()V
    .locals 3

    .prologue
    .line 1197
    const-string v0, "VideoCaptureRaw"

    const-string v1, "staticSuspendRecording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    const-class v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    monitor-enter v0

    .line 1200
    :try_start_0
    sget v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 1201
    const-string v1, "VideoCaptureRaw"

    const-string v2, "staticSuspendRecording: no active camera"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1202
    monitor-exit v0

    .line 1208
    :goto_0
    return-void

    .line 1204
    :cond_0
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sInstance:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    sget v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    aget-object v1, v1, v2

    if-eqz v1, :cond_1

    .line 1205
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sInstance:[Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    sget v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    aget-object v1, v1, v2

    invoke-direct {v1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->suspendRecording()V

    .line 1207
    :cond_1
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private stopCameraRecording()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1435
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->suspendCameraRecording()V

    .line 1436
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-eqz v0, :cond_0

    .line 1439
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1440
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->release()V

    .line 1441
    iput-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    .line 1443
    :cond_0
    return-void
.end method

.method private suspendRecording()V
    .locals 1

    .prologue
    .line 1329
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->suspendCameraRecording()V

    .line 1330
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_suspended:Z

    .line 1331
    return-void
.end method

.method public static updateContext(Landroid/content/Context;)V
    .locals 7
    .parameter

    .prologue
    const/16 v6, 0x500

    const/16 v5, 0x400

    const/16 v4, 0x320

    const/16 v3, 0x258

    const/4 v2, 0x1

    .line 186
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sm:Landroid/hardware/SensorManager;

    .line 187
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->wm:Landroid/view/WindowManager;

    .line 188
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->wm:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    sput v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenWidth:I

    .line 189
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->wm:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    sput v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenHeight:I

    .line 190
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenWidth:I

    if-ne v0, v5, :cond_0

    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenHeight:I

    if-eq v0, v3, :cond_1

    :cond_0
    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenHeight:I

    if-ne v0, v5, :cond_2

    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenWidth:I

    if-ne v0, v3, :cond_2

    .line 191
    :cond_1
    sput-boolean v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSamsungTablet7Inch:Z

    .line 192
    :cond_2
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenWidth:I

    if-ne v0, v6, :cond_3

    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenHeight:I

    if-eq v0, v4, :cond_4

    :cond_3
    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenHeight:I

    if-ne v0, v6, :cond_5

    sget v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->screenWidth:I

    if-ne v0, v4, :cond_5

    .line 193
    :cond_4
    sput-boolean v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSamsungTablet10Dot1:Z

    .line 194
    :cond_5
    return-void
.end method

.method private updateLayout()V
    .locals 3

    .prologue
    .line 1151
    const-class v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    monitor-enter v0

    .line 1152
    :try_start_0
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->ui_handler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 1153
    const-string v1, "VideoCaptureRaw"

    const-string v2, "updateLayout: sending message to ui_handler"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1154
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->ui_handler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1159
    :goto_0
    monitor-exit v0

    .line 1160
    return-void

    .line 1157
    :cond_0
    const-string v1, "VideoCaptureRaw"

    const-string v2, "updateLayout: no ui_handler"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1159
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method protected doStartRecording(II)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1264
    const-string v0, "VideoCaptureRaw"

    const-string v1, "doStartRecording() starts"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1266
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setCurrentCamera(I)V

    .line 1267
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setCallback(II)V

    .line 1269
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    if-eqz v0, :cond_0

    .line 1270
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->startCameraRecording()Z

    move-result v0

    .line 1283
    :goto_0
    return v0

    .line 1273
    :cond_0
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_holder:[Landroid/view/SurfaceHolder;

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    .line 1275
    const-string v0, "VideoCaptureRaw"

    const-string v1, "startRecording: no surface, setting SUSPENDED state"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1276
    iput-boolean v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_suspended:Z

    move v0, v3

    .line 1277
    goto :goto_0

    .line 1281
    :cond_1
    iput-boolean v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_suspended:Z

    .line 1282
    iput-boolean v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_stopped:Z

    .line 1283
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->startCameraRecording()Z

    move-result v0

    goto :goto_0
.end method

.method protected doStopRecording()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1292
    const-string v0, "VideoCaptureRaw"

    const-string v1, "doStopRecording() starts"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1293
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setCurrentCamera(I)V

    .line 1294
    iput-boolean v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_suspended:Z

    .line 1295
    invoke-direct {p0, v2, v2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setCallback(II)V

    .line 1296
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->stopCameraRecording()V

    .line 1297
    return-void
.end method

.method public getCameraHeight()I
    .locals 2

    .prologue
    .line 1084
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_height:[I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v0, v0, v1

    return v0
.end method

.method public getCameraWidth()I
    .locals 2

    .prologue
    .line 1075
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera_width:[I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v0, v0, v1

    return v0
.end method

.method protected getCameraWrapper()Lcom/sgiggle/VideoCapture/CameraWrapper;
    .locals 1

    .prologue
    .line 1701
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    return-object v0
.end method

.method public getCaptureHeight()I
    .locals 2

    .prologue
    .line 1102
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_height:[I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v0, v0, v1

    return v0
.end method

.method public getCaptureRotation()I
    .locals 2

    .prologue
    .line 1111
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_rotation:[I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v0, v0, v1

    return v0
.end method

.method public getCaptureWidth()I
    .locals 2

    .prologue
    .line 1093
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->capture_width:[I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->type:I

    aget v0, v0, v1

    return v0
.end method

.method protected getGlRenderer()Lcom/sgiggle/GLES20/GLRenderer;
    .locals 1

    .prologue
    .line 1694
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    if-eqz v0, :cond_0

    .line 1695
    const/4 v0, 0x0

    .line 1697
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1641
    return-void
.end method

.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1636
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isFocusing:Z

    .line 1637
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 1645
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_stopped:Z

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-nez v0, :cond_1

    .line 1654
    :cond_0
    :goto_0
    return-void

    .line 1647
    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isBecameStable([F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isFocusing:Z

    if-nez v0, :cond_0

    .line 1648
    iput-boolean v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isFocusing:Z

    .line 1649
    const-class v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    monitor-enter v0

    .line 1650
    :try_start_0
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    if-eqz v1, :cond_2

    .line 1651
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    invoke-virtual {v1, p0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 1652
    :cond_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected resumeRecording()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1306
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isCameraThread()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1307
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resumeRecording(): called from thread "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Executing synchronously on camera thread."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1308
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    if-eqz v0, :cond_0

    .line 1309
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->resumeRecording()V

    .line 1322
    :goto_0
    return-void

    .line 1311
    :cond_0
    const-string v0, "VideoCaptureRaw"

    const-string v1, "timestamper is null. Ignoring resumeRecording. Should not have called resumeRecording before startRecording."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1317
    :cond_1
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resumeRecording(): _suspended="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_suspended:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1319
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_suspended:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->startCameraRecording()Z

    .line 1320
    :cond_2
    iput-boolean v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_suspended:Z

    .line 1321
    iput-boolean v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_stopped:Z

    goto :goto_0
.end method

.method public setCameraDisplayOrientation()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1343
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->wm:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getOrientation()I

    move-result v0

    .line 1345
    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 1352
    :goto_0
    new-instance v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;

    invoke-direct {v1}, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;-><init>()V

    .line 1353
    sget-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->cameraIds:[I

    sget v3, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->current_camera:I

    aget v2, v2, v3

    invoke-static {v2, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getCameraInfo(ILcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;)V

    .line 1354
    iget v2, v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    if-nez v2, :cond_0

    .line 1355
    iget v1, v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    sub-int v0, v1, v0

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 1360
    :goto_1
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v1, v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setDisplayOrientation(I)V

    .line 1361
    return-void

    :pswitch_0
    move v0, v1

    .line 1346
    goto :goto_0

    .line 1347
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 1348
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 1349
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 1357
    :cond_0
    iget v1, v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    .line 1358
    const/16 v1, 0x168

    sub-int v0, v1, v0

    rem-int/lit16 v0, v0, 0x168

    goto :goto_1

    .line 1345
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setVideoFrameRate(I)V
    .locals 3
    .parameter

    .prologue
    .line 1170
    iput p1, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->frameRate:I

    .line 1171
    const-string v0, "VideoCaptureRaw"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVideoFrameRate() frameRate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->frameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    return-void
.end method

.method public startRecording(II)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 1218
    const-string v0, "VideoCaptureRaw"

    const-string v1, "startRecording called"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRendererHasCapture:Z

    if-eqz v0, :cond_0

    .line 1220
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->doStartRecording(II)Z

    move v0, v2

    .line 1230
    :goto_0
    return v0

    .line 1226
    :cond_0
    new-instance v0, Lcom/sgiggle/VideoCapture/PreprocessorThread;

    invoke-direct {v0, p0}, Lcom/sgiggle/VideoCapture/PreprocessorThread;-><init>(Lcom/sgiggle/VideoCapture/VideoCaptureRaw;)V

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->preprocessor:Lcom/sgiggle/VideoCapture/PreprocessorThread;

    .line 1227
    new-instance v0, Lcom/sgiggle/VideoCapture/TimestamperThread;

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->preprocessor:Lcom/sgiggle/VideoCapture/PreprocessorThread;

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/VideoCapture/TimestamperThread;-><init>(Lcom/sgiggle/VideoCapture/VideoCaptureRaw;Lcom/sgiggle/VideoCapture/PreprocessorThread;)V

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    .line 1229
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/VideoCapture/TimestamperThread;->startRecording(II)V

    move v0, v2

    .line 1230
    goto :goto_0
.end method

.method public stopRecording()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1238
    const-string v0, "VideoCaptureRaw"

    const-string v1, "stopRecording called"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1239
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRendererHasCapture:Z

    if-eqz v0, :cond_0

    .line 1240
    invoke-virtual {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->doStopRecording()V

    .line 1255
    :goto_0
    return-void

    .line 1244
    :cond_0
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->stopRecording()V

    .line 1246
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->stopThread()V

    .line 1247
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->preprocessor:Lcom/sgiggle/VideoCapture/PreprocessorThread;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/PreprocessorThread;->stopPreprocessor()V

    .line 1251
    const-class v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    monitor-enter v0

    .line 1252
    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    .line 1253
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1254
    sput-object v2, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->preprocessor:Lcom/sgiggle/VideoCapture/PreprocessorThread;

    goto :goto_0

    .line 1253
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected suspendCameraRecording()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1452
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isCameraThread()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1453
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    if-eqz v0, :cond_1

    .line 1454
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->timestamper:Lcom/sgiggle/VideoCapture/TimestamperThread;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/TimestamperThread;->suspendRecording()V

    .line 1490
    :cond_0
    :goto_0
    return-void

    .line 1456
    :cond_1
    const-string v0, "VideoCaptureRaw"

    const-string v1, "timestamper is null. Ignoring suspendCameraRecording. Should not have called suspendCameraRecord after stopRecording."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1461
    :cond_2
    iput-boolean v3, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_stopped:Z

    .line 1462
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sm:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->accSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_3

    .line 1463
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1465
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-eqz v0, :cond_0

    .line 1466
    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSupportAutoFocus:Z

    if-ne v0, v3, :cond_4

    .line 1467
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->cancelAutoFocus()V

    .line 1468
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->stopPreview()V

    .line 1471
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    if-eqz v0, :cond_5

    .line 1472
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    invoke-virtual {v0, v2, v1, v1}, Lcom/sgiggle/GLES20/GLCapture;->setCamera(Landroid/hardware/Camera;II)V

    .line 1481
    :cond_5
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Motorola"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROIDX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROID2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "A955"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_6
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I997"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isSamsungTablet7Inch:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->isGingerbread:Z

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "htc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "ADR6400L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1485
    :cond_8
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0, v2}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1486
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->release()V

    .line 1487
    iput-object v2, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->camera:Lcom/sgiggle/VideoCapture/CameraWrapper;

    goto/16 :goto_0
.end method

.method public unsetPreviewDisplay()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1130
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    if-nez v0, :cond_0

    .line 1131
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_holder:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    .line 1132
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->_holder:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x1

    aput-object v2, v0, v1

    .line 1134
    :cond_0
    return-void
.end method

.method public updateParam(III)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 177
    invoke-direct {p0, p2, p3}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initCameraSize(II)V

    .line 178
    return-void
.end method
