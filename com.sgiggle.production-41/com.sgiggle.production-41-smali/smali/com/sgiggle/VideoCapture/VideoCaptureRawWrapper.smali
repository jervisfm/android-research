.class public Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;
.super Ljava/lang/Object;
.source "VideoCaptureRawWrapper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoCaptureRawWrapper"


# instance fields
.field public camera_height:I

.field public camera_width:I

.field public capture_height:I

.field public capture_rotation:I

.field public capture_width:I

.field m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    .line 22
    return-void
.end method

.method public static checkCamera(I)Z
    .locals 1
    .parameter

    .prologue
    .line 89
    invoke-static {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->checkCamera(I)Z

    move-result v0

    return v0
.end method

.method public static isResoluionFixed(I)Z
    .locals 1
    .parameter

    .prologue
    .line 27
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    .line 28
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initBackCameraSizeHardCoded()Z

    move-result v0

    .line 30
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initFrontCameraSizeHardCoded()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public initialize(III)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    new-instance v0, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-direct {v0, p1, p2, p3}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;-><init>(III)V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    .line 39
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraWidth()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->camera_width:I

    .line 40
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraHeight()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->camera_height:I

    .line 41
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCaptureWidth()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->capture_width:I

    .line 42
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCaptureHeight()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->capture_height:I

    .line 43
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCaptureRotation()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->capture_rotation:I

    .line 44
    return-void
.end method

.method public setVideoFrameRate(I)V
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0, p1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setVideoFrameRate(I)V

    .line 65
    return-void
.end method

.method public startRecording(II)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->startRecording(II)Z

    move-result v0

    return v0
.end method

.method public stopRecording()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->stopRecording()V

    .line 82
    return-void
.end method

.method public unsetPreviewDisplay()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->unsetPreviewDisplay()V

    .line 94
    return-void
.end method

.method public updateParam(III)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->updateParam(III)V

    .line 52
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraWidth()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->camera_width:I

    .line 53
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraHeight()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->camera_height:I

    .line 54
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCaptureWidth()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->capture_width:I

    .line 55
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCaptureHeight()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->capture_height:I

    .line 56
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCaptureRaw;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCaptureRotation()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureRawWrapper;->capture_rotation:I

    .line 57
    return-void
.end method
