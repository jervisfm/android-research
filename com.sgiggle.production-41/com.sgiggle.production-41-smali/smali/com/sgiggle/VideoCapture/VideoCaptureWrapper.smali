.class public Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;
.super Ljava/lang/Object;
.source "VideoCaptureWrapper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoCaptureWrapper"


# instance fields
.field m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    .line 18
    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/sgiggle/VideoCapture/VideoCapture;

    invoke-direct {v0}, Lcom/sgiggle/VideoCapture/VideoCapture;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    .line 25
    return-void
.end method

.method public setVideoFormat(I)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    invoke-virtual {v0, p1}, Lcom/sgiggle/VideoCapture/VideoCapture;->setVideoFormat(I)V

    .line 54
    return-void
.end method

.method public setVideoFrameRate(I)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    invoke-virtual {v0, p1}, Lcom/sgiggle/VideoCapture/VideoCapture;->setVideoFrameRate(I)V

    .line 34
    return-void
.end method

.method public setVideoSize(I)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    invoke-virtual {v0, p1}, Lcom/sgiggle/VideoCapture/VideoCapture;->setVideoSize(I)V

    .line 44
    return-void
.end method

.method public startPreview()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCapture;->startPreview()V

    .line 62
    return-void
.end method

.method public startRecording(Ljava/lang/String;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    invoke-virtual {v0, p1}, Lcom/sgiggle/VideoCapture/VideoCapture;->startRecording(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public stopPreview()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCapture;->stopPreview()V

    .line 70
    return-void
.end method

.method public stopRecording()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoCaptureWrapper;->m_videoCapture:Lcom/sgiggle/VideoCapture/VideoCapture;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/VideoCapture;->stopRecording()V

    .line 85
    return-void
.end method
