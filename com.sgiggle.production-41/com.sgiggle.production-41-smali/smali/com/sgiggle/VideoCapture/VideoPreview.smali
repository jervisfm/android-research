.class public Lcom/sgiggle/VideoCapture/VideoPreview;
.super Ljava/lang/Object;
.source "VideoPreview.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoPreview"


# instance fields
.field private bb:Ljava/nio/ByteBuffer;

.field private bitmap:Landroid/graphics/Bitmap;

.field private canvas:Landroid/graphics/Canvas;

.field private dst:Landroid/graphics/Rect;

.field private dst_height:I

.field private dst_width:I

.field private holder:Landroid/view/SurfaceHolder;

.field private isStarted:Z

.field private src_height:I

.field private src_width:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 150
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    .line 151
    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    move v2, v1

    .line 153
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 154
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sgiggle.VideoCaptureTest"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 155
    const/4 v2, 0x1

    .line 153
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    :cond_1
    if-eqz v2, :cond_2

    .line 158
    const-string v0, "VideoCapture"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 159
    :cond_2
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method private native NV21toRGB565clip([BIILjava/nio/ByteBuffer;II)V
.end method

.method private clear(I)V
    .locals 4
    .parameter

    .prologue
    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->holder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->holder:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    .line 126
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->canvas:Landroid/graphics/Canvas;

    .line 127
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, p1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 128
    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 132
    const-string v1, "VideoPreview"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clear(), exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private native close()V
.end method

.method private native init(II)I
.end method

.method private renderBuffer(Ljava/nio/ByteBuffer;)V
    .locals 6
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->holder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->isStarted:Z

    if-nez v0, :cond_2

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->holder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_1

    .line 88
    const-string v0, "VideoPreview"

    const-string v1, "renderBuffer: _holder == null, can\'t render"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_1
    :goto_0
    return-void

    .line 93
    :cond_2
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 94
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p1}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 96
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->holder:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    .line 97
    if-nez v1, :cond_3

    .line 98
    const-string v0, "VideoPreview"

    const-string v1, "renderBuffer: getSurface() failed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    invoke-virtual {v0}, Landroid/view/Surface$OutOfResourcesException;->printStackTrace()V

    goto :goto_0

    .line 102
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->canvas:Landroid/graphics/Canvas;

    if-nez v0, :cond_4

    .line 103
    const-string v0, "VideoPreview"

    const-string v1, "renderBuffer: can\'t lock canvas"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 113
    :catch_1
    move-exception v0

    .line 115
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 108
    :cond_4
    :try_start_2
    iget-object v2, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->canvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->bitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    check-cast v0, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst:Landroid/graphics/Rect;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 109
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_2
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method


# virtual methods
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 140
    monitor-enter p0

    .line 141
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sgiggle/VideoCapture/VideoPreview;->render([B)V

    .line 142
    monitor-exit p0

    .line 143
    return-void

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public render([B)V
    .locals 7
    .parameter

    .prologue
    .line 79
    iget v2, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->src_width:I

    iget v3, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->src_height:I

    iget-object v4, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->bb:Ljava/nio/ByteBuffer;

    iget v5, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_width:I

    iget v6, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_height:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/VideoCapture/VideoPreview;->NV21toRGB565clip([BIILjava/nio/ByteBuffer;II)V

    .line 80
    iget-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->bb:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/VideoPreview;->renderBuffer(Ljava/nio/ByteBuffer;)V

    .line 81
    return-void
.end method

.method public setPreviewDisplay(Landroid/view/SurfaceHolder;IIII)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    const-string v0, "VideoPreview"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPreviewDisplay "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iput-object p1, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->holder:Landroid/view/SurfaceHolder;

    .line 46
    iput p2, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->src_width:I

    .line 47
    iput p3, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->src_height:I

    .line 48
    iput p4, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_width:I

    .line 49
    iput p5, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_height:I

    .line 50
    return-void
.end method

.method public startRenderer()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 53
    monitor-enter p0

    .line 54
    :try_start_0
    const-string v0, "VideoPreview"

    const-string v1, "startRenderer()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_width:I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_height:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->bb:Ljava/nio/ByteBuffer;

    .line 56
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_width:I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_height:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->bitmap:Landroid/graphics/Bitmap;

    .line 57
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->holder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst:Landroid/graphics/Rect;

    .line 58
    const/high16 v0, -0x100

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/VideoPreview;->clear(I)V

    .line 59
    const/high16 v0, -0x100

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoCapture/VideoPreview;->clear(I)V

    .line 60
    iget v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_width:I

    iget v1, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst_height:I

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/VideoCapture/VideoPreview;->init(II)I

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->isStarted:Z

    .line 62
    monitor-exit p0

    .line 63
    return v3

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stopRenderer()V
    .locals 2

    .prologue
    .line 67
    monitor-enter p0

    .line 68
    :try_start_0
    const-string v0, "VideoPreview"

    const-string v1, "stopRenderer()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->bb:Ljava/nio/ByteBuffer;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->bitmap:Landroid/graphics/Bitmap;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->dst:Landroid/graphics/Rect;

    .line 72
    invoke-direct {p0}, Lcom/sgiggle/VideoCapture/VideoPreview;->close()V

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/VideoCapture/VideoPreview;->isStarted:Z

    .line 74
    monitor-exit p0

    .line 75
    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
