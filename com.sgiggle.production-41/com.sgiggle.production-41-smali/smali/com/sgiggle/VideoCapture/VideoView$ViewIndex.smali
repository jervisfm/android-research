.class public interface abstract Lcom/sgiggle/VideoCapture/VideoView$ViewIndex;
.super Ljava/lang/Object;
.source "VideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/VideoCapture/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewIndex"
.end annotation


# static fields
.field public static final BLANK_FRAME_CAMERA_BACK_AUX0:I = 0x13

.field public static final BLANK_FRAME_CAMERA_BACK_AUX1:I = 0x14

.field public static final BLANK_FRAME_CAMERA_BACK_BOTTOM:I = 0x12

.field public static final BLANK_FRAME_CAMERA_BACK_LEFT:I = 0xf

.field public static final BLANK_FRAME_CAMERA_BACK_RIGHT:I = 0x10

.field public static final BLANK_FRAME_CAMERA_BACK_TOP:I = 0x11

.field public static final BLANK_FRAME_CAMERA_FRONT_AUX0:I = 0x19

.field public static final BLANK_FRAME_CAMERA_FRONT_AUX1:I = 0x1a

.field public static final BLANK_FRAME_CAMERA_FRONT_BOTTOM:I = 0x18

.field public static final BLANK_FRAME_CAMERA_FRONT_LEFT:I = 0x15

.field public static final BLANK_FRAME_CAMERA_FRONT_RIGHT:I = 0x16

.field public static final BLANK_FRAME_CAMERA_FRONT_TOP:I = 0x17

.field public static final BLANK_FRAME_RENDERER_BACK_AUX0:I = 0x1f

.field public static final BLANK_FRAME_RENDERER_BACK_AUX1:I = 0x20

.field public static final BLANK_FRAME_RENDERER_BACK_BOTTOM:I = 0x1e

.field public static final BLANK_FRAME_RENDERER_BACK_LEFT:I = 0x1b

.field public static final BLANK_FRAME_RENDERER_BACK_RIGHT:I = 0x1c

.field public static final BLANK_FRAME_RENDERER_BACK_TOP:I = 0x1d

.field public static final BLANK_FRAME_RENDERER_FRONT_AUX0:I = 0x25

.field public static final BLANK_FRAME_RENDERER_FRONT_AUX1:I = 0x26

.field public static final BLANK_FRAME_RENDERER_FRONT_BOTTOM:I = 0x24

.field public static final BLANK_FRAME_RENDERER_FRONT_LEFT:I = 0x21

.field public static final BLANK_FRAME_RENDERER_FRONT_RIGHT:I = 0x22

.field public static final BLANK_FRAME_RENDERER_FRONT_TOP:I = 0x23

.field public static final BORDER_CAMERA_BACK_SMALL:I = 0xc

.field public static final BORDER_CAMERA_FRONT_SMALL:I = 0xd

.field public static final BORDER_PREFERRED_SMALL:I = 0xb

.field public static final BORDER_RENDERER_SMALL:I = 0xe

.field public static final BUTTON:I = 0x2

.field public static final BUTTON_BACKGROUND_CAMERA_BACK:I = 0x27

.field public static final BUTTON_BACKGROUND_CAMERA_FRONT:I = 0x28

.field public static final BUTTON_BACKGROUND_RENDERER:I = 0x29

.field public static final BUTTON_SWITCH_CAMERA_BACK:I = 0x2a

.field public static final BUTTON_SWITCH_CAMERA_FRONT:I = 0x2b

.field public static final BUTTON_SWITCH_RENDERER:I = 0x2c

.field public static final CAMERA_BACK_BIG:I = 0x3

.field public static final CAMERA_BACK_SMALL:I = 0x7

.field public static final CAMERA_FRONT_BIG:I = 0x4

.field public static final CAMERA_FRONT_SMALL:I = 0x8

.field public static final PREFERRED_SMALL:I = 0x6

.field public static final RENDERER_BIG:I = 0x5

.field public static final RENDERER_SMALL:I = 0x9

.field public static final SCREEN:I = 0x0

.field public static final TRANSPARENT:I = 0xa

.field public static final VIRTUAL_SCREEN:I = 0x1
