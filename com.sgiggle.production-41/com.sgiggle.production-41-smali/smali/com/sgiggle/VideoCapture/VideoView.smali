.class public Lcom/sgiggle/VideoCapture/VideoView;
.super Ljava/lang/Object;
.source "VideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/VideoCapture/VideoView$ViewIndex;,
        Lcom/sgiggle/VideoCapture/VideoView$Orientation;,
        Lcom/sgiggle/VideoCapture/VideoView$CameraType;,
        Lcom/sgiggle/VideoCapture/VideoView$ViewSize;
    }
.end annotation


# static fields
.field private static final CAMERA_HEIGHT:F = 240.0f

.field private static final CAMERA_WIDTH:F = 320.0f

.field private static final CAPTURE_HEIGHT:F = 128.0f

.field private static final CAPTURE_WIDTH:F = 192.0f

.field public static final NUM_BLANK_FRAME:I = 0x6

.field public static final RATIO_133:F = 1.3333334f

.field public static final RATIO_15:F = 1.5f

.field private static final RATIO_BUTTON_WH:F = 0.16666667f

.field public static final RATIO_DEFAULT:F = 1.5f

.field private static final RATIO_GAP:F = 0.025f

.field private static final RATIO_LS:F = 4.3f

.field private static final RATIO_SWITCH_BUTTON:F = 0.3f

.field private static final RATIO_SWITCH_BUTTON_WH:F = 0.5555556f

.field private static final TAG:Ljava/lang/String; = "VideoView"

.field private static final THRESHOLD_ASPECT_RATIO:F = 0.03f

.field private static final VIEW_FRAME_WIDTH:I = 0x6

.field public static final ViewIndexMax:I = 0x2c

.field public static final bigViews:[I

.field public static final cameraBigViews:[I

.field private static cameraSize:[Landroid/graphics/Rect;

.field public static final cameraSmallViews:[I

.field private static captureSize:[Landroid/graphics/Rect;

.field private static isGingerbread:Z

.field private static isNeedUpdate:Z

.field private static isSamsungTablet7Inch:Z

.field private static m_capture_aspect_ratio:[[F

.field private static m_isH264Renderer:Z

.field private static m_preview_aspect_ratio:[[F

.field private static m_renderer_aspect_ratio:[F

.field private static m_screen_aspect_ratio:[F

.field private static orientations:[I

.field public static final rendererViews:[I

.field public static final smallCameraViews:[I

.field private static views:[[Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 100
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->bigViews:[I

    .line 101
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->smallCameraViews:[I

    .line 102
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->cameraBigViews:[I

    .line 103
    new-array v0, v2, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->cameraSmallViews:[I

    .line 104
    new-array v0, v2, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->rendererViews:[I

    .line 107
    new-array v0, v2, [[F

    new-array v1, v2, [F

    fill-array-data v1, :array_5

    aput-object v1, v0, v3

    new-array v1, v2, [F

    fill-array-data v1, :array_6

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    .line 110
    new-array v0, v2, [[F

    new-array v1, v5, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v3

    new-array v1, v5, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    .line 112
    new-array v0, v2, [F

    fill-array-data v0, :array_9

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    .line 113
    new-array v0, v2, [F

    fill-array-data v0, :array_a

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_screen_aspect_ratio:[F

    .line 119
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    move v0, v4

    :goto_0
    sput-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isGingerbread:Z

    .line 121
    sput-boolean v3, Lcom/sgiggle/VideoCapture/VideoView;->m_isH264Renderer:Z

    .line 123
    sput-boolean v3, Lcom/sgiggle/VideoCapture/VideoView;->isSamsungTablet7Inch:Z

    return-void

    :cond_0
    move v0, v3

    .line 119
    goto :goto_0

    .line 100
    nop

    :array_0
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    .line 101
    :array_1
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 102
    :array_2
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    .line 103
    :array_3
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 104
    :array_4
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
    .end array-data

    .line 107
    :array_5
    .array-data 0x4
        0x0t 0x0t 0xc0t 0x3ft
        0x0t 0x0t 0xc0t 0x3ft
    .end array-data

    :array_6
    .array-data 0x4
        0xabt 0xaat 0x2at 0x3ft
        0xabt 0xaat 0x2at 0x3ft
    .end array-data

    .line 110
    :array_7
    .array-data 0x4
        0x0t 0x0t 0xc0t 0x3ft
        0x0t 0x0t 0xc0t 0x3ft
        0x0t 0x0t 0xc0t 0x3ft
    .end array-data

    :array_8
    .array-data 0x4
        0xabt 0xaat 0x2at 0x3ft
        0xabt 0xaat 0x2at 0x3ft
        0xabt 0xaat 0x2at 0x3ft
    .end array-data

    .line 112
    :array_9
    .array-data 0x4
        0x0t 0x0t 0xc0t 0x3ft
        0xabt 0xaat 0x2at 0x3ft
    .end array-data

    .line 113
    :array_a
    .array-data 0x4
        0x0t 0x0t 0xc0t 0x3ft
        0xabt 0xaat 0x2at 0x3ft
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static calcBigCameraSize([Landroid/graphics/Rect;I)V
    .locals 13
    .parameter
    .parameter

    .prologue
    .line 572
    const/4 v0, 0x0

    aget-object v0, p0, v0

    .line 573
    invoke-static {p1}, Lcom/sgiggle/VideoCapture/VideoView;->getVirtualSize(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 575
    const/4 v2, 0x0

    :goto_0
    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->cameraBigViews:[I

    array-length v3, v3

    if-ge v2, v3, :cond_b

    .line 576
    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->cameraBigViews:[I

    aget v3, v3, v2

    .line 577
    new-instance v4, Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v4, p0, v3

    .line 578
    aget-object v4, p0, v3

    .line 582
    const/4 v5, 0x3

    if-ne v3, v5, :cond_5

    const/4 v3, 0x0

    .line 584
    :goto_1
    invoke-static {v3}, Lcom/sgiggle/VideoCapture/VideoView;->getCameraSize(I)Landroid/graphics/Rect;

    move-result-object v5

    .line 585
    invoke-static {v3}, Lcom/sgiggle/VideoCapture/VideoView;->getCaptureSize(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 587
    if-nez p1, :cond_6

    .line 589
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    .line 590
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 593
    iget v9, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v10

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float v8, v10, v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 594
    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v10, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 595
    const/4 v8, 0x0

    aget-object v8, p0, v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    iget v9, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 598
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    sget-object v9, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v9, v9, p1

    aget v9, v9, v3

    mul-float/2addr v8, v9

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v9, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 599
    iget v9, v1, Landroid/graphics/Rect;->left:I

    iput v9, v4, Landroid/graphics/Rect;->left:I

    .line 600
    iget v9, v1, Landroid/graphics/Rect;->right:I

    iget v10, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v10

    invoke-static {v9, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 602
    iget v8, v4, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x3f80

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v11

    int-to-float v11, v11

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    mul-float/2addr v9, v10

    const/high16 v10, 0x4000

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    .line 604
    const/4 v9, 0x0

    cmpg-float v9, v8, v9

    if-gez v9, :cond_0

    .line 605
    iget v9, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v10

    sub-int/2addr v9, v10

    mul-float/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    sub-int v8, v9, v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 606
    :cond_0
    iget v8, v4, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x3f80

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v6, v5

    sub-float v5, v10, v5

    mul-float/2addr v5, v9

    const/high16 v6, 0x4000

    div-float/2addr v5, v6

    sub-float v5, v8, v5

    .line 607
    iget v6, v0, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    cmpl-float v6, v5, v6

    if-lez v6, :cond_1

    .line 608
    iget v6, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v8

    add-int/2addr v6, v8

    iget v8, v0, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    sub-float/2addr v5, v8

    mul-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-int v5, v6, v5

    iput v5, v4, Landroid/graphics/Rect;->right:I

    .line 609
    :cond_1
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 610
    iget v5, v4, Landroid/graphics/Rect;->right:I

    const/4 v6, 0x1

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/graphics/Rect;->right:I

    .line 613
    :cond_2
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    sget-object v6, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v6, v6, p1

    aget v3, v6, v3

    div-float v3, v5, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 614
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    if-gt v3, v5, :cond_3

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    const/4 v6, 0x2

    sub-int/2addr v5, v6

    if-ge v3, v5, :cond_4

    .line 615
    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v5

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float v3, v5, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, v4, Landroid/graphics/Rect;->top:I

    .line 616
    const/4 v3, 0x0

    aget-object v3, p0, v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v5, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v5

    iput v3, v4, Landroid/graphics/Rect;->bottom:I

    .line 575
    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 582
    :cond_5
    const/4 v3, 0x1

    goto/16 :goto_1

    .line 621
    :cond_6
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    .line 622
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 625
    iget v9, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v10

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float v8, v10, v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 626
    iget v8, v1, Landroid/graphics/Rect;->right:I

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget v10, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 627
    const/4 v8, 0x0

    aget-object v8, p0, v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget v9, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 630
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    sget-object v9, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v9, v9, p1

    aget v9, v9, v3

    div-float/2addr v8, v9

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v9, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 631
    iget v9, v1, Landroid/graphics/Rect;->top:I

    iput v9, v4, Landroid/graphics/Rect;->top:I

    .line 632
    iget v9, v1, Landroid/graphics/Rect;->bottom:I

    iget v10, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v10

    invoke-static {v9, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 634
    iget v8, v4, Landroid/graphics/Rect;->top:I

    int-to-float v8, v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x3f80

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v11

    int-to-float v11, v11

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    mul-float/2addr v9, v10

    const/high16 v10, 0x4000

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    .line 636
    const/4 v9, 0x0

    cmpg-float v9, v8, v9

    if-gez v9, :cond_7

    .line 637
    iget v9, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v10

    sub-int/2addr v9, v10

    mul-float/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    sub-int v8, v9, v8

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 638
    :cond_7
    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x3f80

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v6, v5

    sub-float v5, v10, v5

    mul-float/2addr v5, v9

    const/high16 v6, 0x4000

    div-float/2addr v5, v6

    sub-float v5, v8, v5

    .line 639
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    cmpl-float v6, v5, v6

    if-lez v6, :cond_8

    .line 640
    iget v6, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    add-int/2addr v6, v8

    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    sub-float/2addr v5, v8

    mul-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-int v5, v6, v5

    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    .line 641
    :cond_8
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_9

    .line 642
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    const/4 v6, 0x1

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    .line 645
    :cond_9
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    sget-object v6, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v6, v6, p1

    aget v3, v6, v3

    mul-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 646
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    if-gt v3, v5, :cond_a

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    const/4 v6, 0x2

    sub-int/2addr v5, v6

    if-ge v3, v5, :cond_4

    .line 647
    :cond_a
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float v3, v5, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, v4, Landroid/graphics/Rect;->left:I

    .line 648
    const/4 v3, 0x0

    aget-object v3, p0, v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v5, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v5

    iput v3, v4, Landroid/graphics/Rect;->right:I

    goto/16 :goto_2

    .line 652
    :cond_b
    return-void
.end method

.method private static calcBigSize(FI)Landroid/graphics/Rect;
    .locals 6
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x4000

    const/4 v5, 0x0

    .line 715
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p1

    aget-object v0, v0, v5

    .line 716
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 718
    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_screen_aspect_ratio:[F

    aget v2, v2, p1

    cmpl-float v2, v2, p0

    if-lez v2, :cond_1

    .line 720
    int-to-float v2, v0

    mul-float/2addr v2, p0

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 721
    if-nez p1, :cond_0

    .line 722
    new-instance v1, Landroid/graphics/Rect;

    mul-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v5, v5, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v0, v1

    .line 731
    :goto_0
    return-object v0

    .line 724
    :cond_0
    new-instance v3, Landroid/graphics/Rect;

    div-int/lit8 v4, v1, 0x2

    sub-int/2addr v4, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    invoke-direct {v3, v4, v5, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v0, v3

    goto :goto_0

    .line 727
    :cond_1
    int-to-float v2, v1

    mul-float/2addr v3, p0

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 728
    if-nez p1, :cond_2

    .line 729
    new-instance v3, Landroid/graphics/Rect;

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v4, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    invoke-direct {v3, v5, v4, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v0, v3

    goto :goto_0

    .line 731
    :cond_2
    new-instance v0, Landroid/graphics/Rect;

    mul-int/lit8 v2, v2, 0x2

    invoke-direct {v0, v5, v5, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method private static calcBlankFrame([Landroid/graphics/Rect;I)V
    .locals 11
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v10, 0x0

    .line 795
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 796
    aget-object v1, p0, v10

    .line 797
    new-array v2, v2, [Landroid/graphics/Rect;

    .line 799
    const/4 v3, 0x1

    sget-object v4, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v4, v4, p1

    const/16 v5, 0xe

    aget-object v4, v4, v5

    aput-object v4, v2, v3

    aput-object v4, v2, v10

    .line 800
    sget-boolean v3, Lcom/sgiggle/VideoCapture/VideoView;->m_isH264Renderer:Z

    if-eqz v3, :cond_0

    .line 801
    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, p1

    const/16 v4, 0xc

    aget-object v3, v3, v4

    aput-object v3, v2, v6

    .line 802
    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, p1

    const/16 v4, 0xd

    aget-object v3, v3, v4

    aput-object v3, v2, v7

    .line 807
    :goto_0
    if-nez p1, :cond_5

    move v3, v10

    .line 808
    :goto_1
    array-length v4, v0

    if-ge v3, v4, :cond_a

    .line 809
    aget v4, v0, v3

    aget-object v4, p0, v4

    .line 811
    iget v5, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 812
    iget v5, v4, Landroid/graphics/Rect;->right:I

    aget-object v6, v2, v3

    iget v6, v6, Landroid/graphics/Rect;->right:I

    if-lt v5, v6, :cond_1

    .line 813
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0xf

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 814
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x10

    new-instance v6, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-direct {v6, v4, v10, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 815
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x11

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 816
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x12

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 808
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 805
    :cond_0
    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, p1

    const/16 v4, 0xb

    aget-object v3, v3, v4

    aput-object v3, v2, v7

    aput-object v3, v2, v6

    goto :goto_0

    .line 820
    :cond_1
    iget v5, v4, Landroid/graphics/Rect;->right:I

    aget-object v6, v2, v3

    iget v6, v6, Landroid/graphics/Rect;->left:I

    if-ge v5, v6, :cond_2

    .line 821
    aget-object v5, v2, v3

    iget v5, v5, Landroid/graphics/Rect;->left:I

    .line 822
    mul-int/lit8 v6, v3, 0x6

    add-int/lit8 v6, v6, 0xf

    new-instance v7, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget-object v8, v2, v3

    iget v8, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-direct {v7, v4, v10, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v7, p0, v6

    move v4, v5

    .line 828
    :goto_3
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x10

    new-instance v6, Landroid/graphics/Rect;

    aget-object v7, v2, v3

    iget v7, v7, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-direct {v6, v7, v10, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 829
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x11

    new-instance v6, Landroid/graphics/Rect;

    aget-object v7, v2, v3

    iget v7, v7, Landroid/graphics/Rect;->right:I

    aget-object v8, v2, v3

    iget v8, v8, Landroid/graphics/Rect;->top:I

    invoke-direct {v6, v4, v10, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 830
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x12

    new-instance v6, Landroid/graphics/Rect;

    aget-object v7, v2, v3

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    aget-object v8, v2, v3

    iget v8, v8, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-direct {v6, v4, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    goto :goto_2

    .line 825
    :cond_2
    iget v4, v4, Landroid/graphics/Rect;->right:I

    .line 826
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0xf

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    goto :goto_3

    .line 833
    :cond_3
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-ge v5, v6, :cond_4

    .line 834
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0xf

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 835
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x10

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 836
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x11

    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v7

    iget v8, v4, Landroid/graphics/Rect;->top:I

    invoke-direct {v6, v10, v10, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 837
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x12

    new-instance v6, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-direct {v6, v10, v4, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    goto/16 :goto_2

    .line 840
    :cond_4
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0xf

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 841
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x10

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 842
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x11

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 843
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x12

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    goto/16 :goto_2

    :cond_5
    move v3, v10

    .line 848
    :goto_4
    array-length v4, v0

    if-ge v3, v4, :cond_a

    .line 849
    aget v4, v0, v3

    aget-object v4, p0, v4

    .line 851
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-ge v5, v6, :cond_8

    .line 852
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    aget-object v6, v2, v3

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    if-lt v5, v6, :cond_6

    .line 853
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x11

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 854
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x12

    new-instance v6, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-direct {v6, v10, v4, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 855
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x10

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 856
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0xf

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 848
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 860
    :cond_6
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    aget-object v6, v2, v3

    iget v6, v6, Landroid/graphics/Rect;->top:I

    if-ge v5, v6, :cond_7

    .line 861
    aget-object v5, v2, v3

    iget v5, v5, Landroid/graphics/Rect;->top:I

    .line 862
    mul-int/lit8 v6, v3, 0x6

    add-int/lit8 v6, v6, 0x11

    new-instance v7, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v8

    aget-object v9, v2, v3

    iget v9, v9, Landroid/graphics/Rect;->top:I

    invoke-direct {v7, v10, v4, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v7, p0, v6

    move v4, v5

    .line 868
    :goto_6
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x12

    new-instance v6, Landroid/graphics/Rect;

    aget-object v7, v2, v3

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-direct {v6, v10, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 869
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x10

    new-instance v6, Landroid/graphics/Rect;

    aget-object v7, v2, v3

    iget v7, v7, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v8

    aget-object v9, v2, v3

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v6, v7, v4, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 870
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0xf

    new-instance v6, Landroid/graphics/Rect;

    aget-object v7, v2, v3

    iget v7, v7, Landroid/graphics/Rect;->left:I

    aget-object v8, v2, v3

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v6, v10, v4, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    goto :goto_5

    .line 865
    :cond_7
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    .line 866
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x11

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    goto :goto_6

    .line 873
    :cond_8
    iget v5, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-ge v5, v6, :cond_9

    .line 874
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x11

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 875
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x12

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 876
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0x10

    new-instance v6, Landroid/graphics/Rect;

    iget v7, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-direct {v6, v7, v10, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    .line 877
    mul-int/lit8 v5, v3, 0x6

    add-int/lit8 v5, v5, 0xf

    new-instance v6, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-direct {v6, v10, v10, v4, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, p0, v5

    goto/16 :goto_5

    .line 880
    :cond_9
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x11

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 881
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x12

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 882
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0x10

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    .line 883
    mul-int/lit8 v4, v3, 0x6

    add-int/lit8 v4, v4, 0xf

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v10, v10, v10, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v5, p0, v4

    goto/16 :goto_5

    .line 887
    :cond_a
    return-void

    .line 795
    :array_0
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private static calcBlankFrameAuxSize([Landroid/graphics/Rect;I)I
    .locals 10
    .parameter
    .parameter

    .prologue
    const/high16 v9, 0x4000

    const/16 v8, 0x1a

    const/16 v7, 0x19

    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 534
    const/16 v0, 0x13

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, p0, v0

    .line 535
    const/16 v0, 0x14

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, p0, v0

    .line 536
    const/16 v0, 0x1f

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, p0, v0

    .line 537
    const/16 v0, 0x20

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, p0, v0

    .line 538
    const/16 v0, 0x25

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, p0, v0

    .line 539
    const/16 v0, 0x26

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, p0, v0

    .line 542
    if-nez p1, :cond_2

    .line 543
    new-instance v0, Landroid/graphics/Rect;

    aget-object v1, p0, v5

    iget v1, v1, Landroid/graphics/Rect;->right:I

    aget-object v2, p0, v6

    iget v2, v2, Landroid/graphics/Rect;->right:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {v0, v5, v5, v1, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v0, p0, v7

    .line 544
    new-instance v0, Landroid/graphics/Rect;

    aget-object v1, p0, v5

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    aget-object v2, p0, v5

    iget v2, v2, Landroid/graphics/Rect;->right:I

    aget-object v3, p0, v6

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    aget-object v3, p0, v5

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v0, v5, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v0, p0, v8

    .line 545
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isSamsungTablet7Inch:Z

    if-eqz v0, :cond_3

    .line 546
    aget-object v0, p0, v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    aget-object v1, p0, v6

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v2, v2, p1

    const/4 v3, 0x1

    aget v2, v2, v3

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    div-float/2addr v0, v9

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 547
    if-lez v0, :cond_0

    .line 548
    aget-object v1, p0, v7

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 549
    aget-object v1, p0, v8

    aget-object v2, p0, v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 565
    :cond_0
    :goto_0
    if-gez v0, :cond_1

    move v0, v5

    .line 568
    :cond_1
    return v0

    .line 554
    :cond_2
    new-instance v0, Landroid/graphics/Rect;

    aget-object v1, p0, v5

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    aget-object v2, p0, v6

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {v0, v5, v5, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v0, p0, v7

    .line 555
    new-instance v0, Landroid/graphics/Rect;

    aget-object v1, p0, v5

    iget v1, v1, Landroid/graphics/Rect;->right:I

    aget-object v2, p0, v5

    iget v2, v2, Landroid/graphics/Rect;->right:I

    aget-object v3, p0, v5

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    aget-object v4, p0, v6

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-direct {v0, v1, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v0, p0, v8

    .line 556
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isSamsungTablet7Inch:Z

    if-eqz v0, :cond_3

    .line 557
    aget-object v0, p0, v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    aget-object v1, p0, v6

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v2, v2, p1

    const/4 v3, 0x1

    aget v2, v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    div-float/2addr v0, v9

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 558
    if-lez v0, :cond_0

    .line 559
    aget-object v1, p0, v7

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 560
    aget-object v1, p0, v8

    aget-object v2, p0, v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_0

    :cond_3
    move v0, v5

    goto :goto_0
.end method

.method private static calcButtonBackground(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Landroid/graphics/Rect;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 897
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p2

    aget-object v0, v0, v3

    .line 900
    if-nez p2, :cond_0

    .line 901
    iget v1, p0, Landroid/graphics/Rect;->right:I

    iget v2, p1, Landroid/graphics/Rect;->left:I

    if-le v1, v2, :cond_1

    .line 902
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 903
    iget v2, p0, Landroid/graphics/Rect;->top:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 904
    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v4, p0, Landroid/graphics/Rect;->right:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 905
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Landroid/graphics/Rect;->bottom:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v5, v3

    move v3, v1

    move v1, v5

    .line 915
    :goto_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v3, v2, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4

    .line 908
    :cond_0
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    if-le v1, v2, :cond_1

    .line 909
    iget v1, p0, Landroid/graphics/Rect;->left:I

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 910
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 911
    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v4, p0, Landroid/graphics/Rect;->right:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 912
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Landroid/graphics/Rect;->bottom:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v5, v3

    move v3, v1

    move v1, v5

    goto :goto_0

    :cond_1
    move v0, v3

    move v1, v3

    move v2, v3

    goto :goto_0
.end method

.method private static calcSmallSize(Landroid/graphics/Rect;FI)Landroid/graphics/Rect;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const v1, 0x4109999a

    .line 742
    if-nez p2, :cond_0

    .line 743
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 744
    int-to-float v1, v0

    div-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    move v4, v1

    move v1, v0

    move v0, v4

    .line 750
    :goto_0
    new-instance v2, Landroid/graphics/Rect;

    mul-int/lit8 v1, v1, 0x2

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v2, v3, v3, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v2

    .line 747
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 748
    int-to-float v1, v0

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_0
.end method

.method private static calcSmallViewFrame(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x3

    .line 782
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/lit8 v0, v0, 0x6

    .line 783
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/lit8 v1, v1, 0x6

    .line 784
    iget v2, p0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v4

    .line 785
    iget v3, p0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    .line 786
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-direct {v4, v2, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method

.method private static calcSmallViewPosition(Landroid/graphics/Rect;Landroid/graphics/Rect;I)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const v4, 0x3f79999a

    .line 758
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p2

    const/4 v1, 0x2

    aget-object v0, v0, v1

    .line 759
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, p2

    aget-object v1, v1, v5

    .line 762
    if-nez p2, :cond_0

    .line 763
    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, p2

    aget-object v2, v2, v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v0, v2

    .line 764
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Landroid/graphics/Rect;->top:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    move v6, v1

    move v1, v0

    move v0, v6

    .line 770
    :goto_0
    iget v2, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v1

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 771
    iget v2, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 772
    iget v1, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 773
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 774
    return-void

    .line 767
    :cond_0
    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, p0, Landroid/graphics/Rect;->right:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    sub-int v1, v2, v1

    .line 768
    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, p2

    aget-object v2, v2, v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method private static calcSwitchCameraButton(Landroid/graphics/Rect;I)Landroid/graphics/Rect;
    .locals 9
    .parameter
    .parameter

    .prologue
    const v7, 0x3f0e38e4

    const v6, 0x3e99999a

    const v4, 0x3ccccccd

    const/4 v5, 0x0

    .line 924
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p1

    aget-object v0, v0, v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 925
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, p1

    aget-object v1, v1, v5

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 926
    int-to-float v2, v0

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 927
    int-to-float v3, v1

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 930
    if-nez p1, :cond_1

    .line 931
    int-to-float v0, v1

    mul-float/2addr v0, v6

    float-to-int v0, v0

    .line 932
    int-to-float v1, v0

    mul-float/2addr v1, v7

    float-to-int v1, v1

    .line 940
    :goto_0
    iget v4, p0, Landroid/graphics/Rect;->top:I

    if-lez v4, :cond_0

    .line 941
    iget v4, p0, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    .line 943
    :cond_0
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v1, v2

    add-int/2addr v0, v3

    invoke-direct {v4, v2, v3, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4

    .line 935
    :cond_1
    int-to-float v0, v0

    mul-float/2addr v0, v6

    float-to-int v0, v0

    .line 936
    int-to-float v1, v0

    mul-float/2addr v1, v7

    float-to-int v1, v1

    .line 937
    sget-object v4, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v4, v4, p1

    aget-object v4, v4, v5

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget v5, p0, Landroid/graphics/Rect;->right:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    sub-int/2addr v4, v0

    sub-int v2, v4, v2

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_0
.end method

.method private static dump()V
    .locals 7

    .prologue
    const/16 v6, 0x2c

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 459
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BACK:  cameraSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->cameraSize:[Landroid/graphics/Rect;

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " captureSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->captureSize:[Landroid/graphics/Rect;

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->orientations:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FRONT: cameraSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->cameraSize:[Landroid/graphics/Rect;

    aget-object v2, v2, v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " captureSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->captureSize:[Landroid/graphics/Rect;

    aget-object v2, v2, v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->orientations:[I

    aget v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v4

    .line 462
    :goto_0
    if-gt v0, v6, :cond_1

    .line 463
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v4

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 464
    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, v4

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, v4

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, v4

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v4

    .line 466
    :goto_1
    if-gt v0, v6, :cond_3

    .line 467
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v5

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 468
    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, v5

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, v5

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v3, v3, v5

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 470
    :cond_3
    return-void
.end method

.method public static getBorderRect(II)Landroid/graphics/Rect;
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x3

    .line 397
    invoke-static {p0, p1}, Lcom/sgiggle/VideoCapture/VideoView;->getRect(II)Landroid/graphics/Rect;

    move-result-object v0

    .line 398
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/lit8 v1, v1, 0x6

    .line 399
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    .line 400
    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    .line 401
    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v4

    .line 402
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v1, v3

    add-int/2addr v2, v0

    invoke-direct {v4, v3, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method

.method public static getButton(I)Landroid/graphics/Rect;
    .locals 2
    .parameter

    .prologue
    .line 419
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static getCameraSize(I)Landroid/graphics/Rect;
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 436
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->cameraSize:[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    if-nez v0, :cond_0

    .line 437
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->cameraSize:[Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    const/16 v2, 0x140

    const/16 v3, 0xf0

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, v0, p0

    .line 438
    :cond_0
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->cameraSize:[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static getCaptureSize(I)Landroid/graphics/Rect;
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 447
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->captureSize:[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    if-nez v0, :cond_0

    .line 448
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->captureSize:[Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    const/16 v2, 0xc0

    const/16 v3, 0x80

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, v0, p0

    .line 449
    :cond_0
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->captureSize:[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static getOrientation(I)I
    .locals 1
    .parameter

    .prologue
    .line 427
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->orientations:[I

    aget v0, v0, p0

    return v0
.end method

.method public static getRect(II)Landroid/graphics/Rect;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 379
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    if-eqz v0, :cond_0

    .line 380
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoView;->initSize()V

    .line 381
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 383
    :cond_0
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public static getScreen(I)Landroid/graphics/Rect;
    .locals 2
    .parameter

    .prologue
    .line 411
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method private static getVirtualSize(I)Landroid/graphics/Rect;
    .locals 5
    .parameter

    .prologue
    const v4, 0x7fffffff

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 453
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    aget-object v0, v0, v2

    if-nez v0, :cond_0

    .line 454
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v3, v3, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, v0, v2

    .line 455
    :cond_0
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    aget-object v0, v0, v2

    return-object v0
.end method

.method public static init(II)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x320

    const/16 v4, 0x258

    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 152
    new-array v0, v1, [Landroid/graphics/Rect;

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->cameraSize:[Landroid/graphics/Rect;

    .line 153
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Rect;

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->captureSize:[Landroid/graphics/Rect;

    .line 154
    new-array v0, v1, [I

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->orientations:[I

    .line 157
    const/16 v0, 0x2d

    filled-new-array {v1, v0}, [I

    move-result-object v0

    const-class v1, Landroid/graphics/Rect;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Landroid/graphics/Rect;

    sput-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    .line 160
    if-le p0, p1, :cond_6

    .line 161
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, v3

    invoke-static {p0, p1, v0}, Lcom/sgiggle/VideoCapture/VideoView;->initLandscape(II[Landroid/graphics/Rect;)V

    .line 162
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, v2

    invoke-static {p1, p0, v0}, Lcom/sgiggle/VideoCapture/VideoView;->initPortrait(II[Landroid/graphics/Rect;)V

    .line 168
    :goto_0
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x400

    if-ne p0, v0, :cond_0

    if-eq p1, v4, :cond_1

    :cond_0
    const/16 v0, 0x400

    if-ne p1, v0, :cond_2

    if-ne p0, v4, :cond_2

    .line 169
    :cond_1
    sput-boolean v2, Lcom/sgiggle/VideoCapture/VideoView;->isSamsungTablet7Inch:Z

    .line 172
    :cond_2
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0x500

    if-ne p0, v0, :cond_3

    if-eq p1, v5, :cond_4

    :cond_3
    const/16 v0, 0x500

    if-ne p1, v0, :cond_5

    if-ne p0, v5, :cond_5

    .line 174
    :cond_4
    invoke-static {v3, v2}, Lcom/sgiggle/VideoCapture/VideoView;->setOrientation(II)V

    .line 175
    invoke-static {v2, v2}, Lcom/sgiggle/VideoCapture/VideoView;->setOrientation(II)V

    .line 178
    :cond_5
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoView;->initVirtualSize()V

    .line 180
    sput-boolean v2, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 181
    return-void

    .line 164
    :cond_6
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, v3

    invoke-static {p1, p0, v0}, Lcom/sgiggle/VideoCapture/VideoView;->initLandscape(II[Landroid/graphics/Rect;)V

    .line 165
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, v2

    invoke-static {p0, p1, v0}, Lcom/sgiggle/VideoCapture/VideoView;->initPortrait(II[Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method private static initLandscape(II[Landroid/graphics/Rect;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 185
    int-to-float v0, p1

    const v1, 0x3e2aaaab

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 186
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v3, v3, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, p2, v3

    .line 187
    const/4 v1, 0x2

    new-instance v2, Landroid/graphics/Rect;

    sub-int v0, p0, v0

    invoke-direct {v2, v0, v3, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v2, p2, v1

    .line 188
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 189
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_screen_aspect_ratio:[F

    int-to-float v1, p0

    int-to-float v2, p1

    div-float/2addr v1, v2

    aput v1, v0, v3

    .line 190
    :cond_0
    return-void
.end method

.method private static initPortrait(II[Landroid/graphics/Rect;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 194
    int-to-float v0, p0

    const v1, 0x3e2aaaab

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 195
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v3, v3, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, p2, v3

    .line 196
    const/4 v1, 0x2

    new-instance v2, Landroid/graphics/Rect;

    sub-int v0, p1, v0

    invoke-direct {v2, v3, v0, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v2, p2, v1

    .line 197
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 198
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_screen_aspect_ratio:[F

    const/4 v1, 0x1

    int-to-float v2, p0

    int-to-float v3, p1

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 199
    :cond_0
    return-void
.end method

.method private static initSize()V
    .locals 1

    .prologue
    .line 473
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoView;->initView(I)V

    .line 474
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoView;->initView(I)V

    .line 477
    return-void
.end method

.method private static initView(I)V
    .locals 9
    .parameter

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x6

    const/16 v4, 0x9

    .line 480
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    .line 483
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aget v1, v1, p0

    invoke-static {v1, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcBigSize(FI)Landroid/graphics/Rect;

    move-result-object v1

    aput-object v1, v0, v7

    .line 486
    invoke-static {v0, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcBigCameraSize([Landroid/graphics/Rect;I)V

    .line 488
    invoke-static {v0, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcBlankFrameAuxSize([Landroid/graphics/Rect;I)I

    move-result v1

    .line 491
    const/4 v2, 0x0

    aget-object v2, v0, v2

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v3, v3, p0

    aget v3, v3, v6

    invoke-static {v2, v3, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcSmallSize(Landroid/graphics/Rect;FI)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v5

    .line 492
    const/4 v2, 0x0

    aget-object v2, v0, v2

    sget-object v3, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aget v3, v3, p0

    invoke-static {v2, v3, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcSmallSize(Landroid/graphics/Rect;FI)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v4

    .line 495
    aget-object v2, v0, v7

    aget-object v3, v0, v5

    invoke-static {v2, v3, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcSmallViewPosition(Landroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 496
    aget-object v2, v0, v8

    aget-object v3, v0, v4

    invoke-static {v2, v3, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcSmallViewPosition(Landroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 497
    if-nez p0, :cond_0

    .line 498
    aget-object v2, v0, v4

    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 499
    aget-object v2, v0, v4

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v3, v1

    iput v1, v2, Landroid/graphics/Rect;->bottom:I

    .line 506
    :goto_0
    const/4 v1, 0x7

    new-instance v2, Landroid/graphics/Rect;

    aget-object v3, v0, v5

    invoke-direct {v2, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v2, v0, v1

    .line 507
    const/16 v1, 0x8

    new-instance v2, Landroid/graphics/Rect;

    aget-object v3, v0, v5

    invoke-direct {v2, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v2, v0, v1

    .line 508
    invoke-static {v0, p0}, Lcom/sgiggle/VideoCapture/VideoView;->layoutAdjustSmallCameraSize([Landroid/graphics/Rect;I)V

    .line 511
    const/16 v1, 0xa

    new-instance v2, Landroid/graphics/Rect;

    aget-object v3, v0, v5

    invoke-direct {v2, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v2, v0, v1

    .line 514
    const/16 v1, 0xb

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, p0

    aget-object v2, v2, v5

    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->calcSmallViewFrame(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 515
    const/16 v1, 0xc

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, p0

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->calcSmallViewFrame(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 516
    const/16 v1, 0xd

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, p0

    const/16 v3, 0x8

    aget-object v2, v2, v3

    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->calcSmallViewFrame(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 517
    const/16 v1, 0xe

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, p0

    aget-object v2, v2, v4

    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->calcSmallViewFrame(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 520
    invoke-static {v0, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcBlankFrame([Landroid/graphics/Rect;I)V

    .line 523
    const/16 v1, 0x27

    const/4 v2, 0x3

    aget-object v2, v0, v2

    aget-object v3, v0, v6

    invoke-static {v2, v3, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcButtonBackground(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 524
    const/16 v1, 0x28

    aget-object v2, v0, v8

    aget-object v3, v0, v6

    invoke-static {v2, v3, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcButtonBackground(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 525
    const/16 v1, 0x29

    aget-object v2, v0, v7

    aget-object v3, v0, v6

    invoke-static {v2, v3, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcButtonBackground(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 528
    const/16 v1, 0x2a

    const/4 v2, 0x3

    aget-object v2, v0, v2

    invoke-static {v2, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcSwitchCameraButton(Landroid/graphics/Rect;I)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 529
    const/16 v1, 0x2b

    aget-object v2, v0, v8

    invoke-static {v2, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcSwitchCameraButton(Landroid/graphics/Rect;I)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 530
    const/16 v1, 0x2c

    aget-object v2, v0, v7

    invoke-static {v2, p0}, Lcom/sgiggle/VideoCapture/VideoView;->calcSwitchCameraButton(Landroid/graphics/Rect;I)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v0, v1

    .line 531
    return-void

    .line 502
    :cond_0
    aget-object v2, v0, v4

    iget v3, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 503
    aget-object v2, v0, v4

    iget v3, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v3

    iput v1, v2, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0
.end method

.method private static initVirtualSize()V
    .locals 6

    .prologue
    const/16 v5, 0x400

    const/4 v4, 0x0

    const/16 v3, 0x320

    const/4 v2, 0x1

    .line 203
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isGingerbread:Z

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 204
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Nexus S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P1010"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 205
    :cond_0
    invoke-static {v4}, Lcom/sgiggle/VideoCapture/VideoView;->getScreen(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(ILandroid/graphics/Rect;)V

    .line 206
    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->getScreen(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(ILandroid/graphics/Rect;)V

    .line 265
    :cond_1
    :goto_0
    return-void

    .line 208
    :cond_2
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-M920"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T959V"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SCH-I510"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T839"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I997"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "YP-GB1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isSamsungTablet7Inch:Z

    if-eqz v0, :cond_3

    .line 218
    invoke-static {v4, v5, v5}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(III)V

    .line 219
    invoke-static {v2, v5, v5}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(III)V

    .line 229
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    .line 230
    invoke-static {v2, v2}, Lcom/sgiggle/VideoCapture/VideoView;->setOrientation(II)V

    goto :goto_0

    .line 221
    :cond_3
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 222
    :cond_4
    invoke-static {v4}, Lcom/sgiggle/VideoCapture/VideoView;->getScreen(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(ILandroid/graphics/Rect;)V

    .line 223
    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->getScreen(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(ILandroid/graphics/Rect;)V

    goto :goto_1

    .line 226
    :cond_5
    invoke-static {v4, v3, v3}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(III)V

    .line 227
    invoke-static {v2, v3, v3}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(III)V

    goto :goto_1

    .line 233
    :cond_6
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isGingerbread:Z

    if-eqz v0, :cond_7

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 234
    invoke-static {v4, v3, v3}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(III)V

    .line 235
    invoke-static {v2, v3, v3}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(III)V

    goto/16 :goto_0

    .line 237
    :cond_7
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Glacier"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_8
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isGingerbread:Z

    if-eqz v0, :cond_9

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_f

    :cond_9
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Dell Inc."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Dell Streak 7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_a
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "M886"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_b
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_c

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P990"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-SU660"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "VS910 4G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-MS910"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_c
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "ADR6400L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation XL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Bliss"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Rhyme"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Runnymede"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "PG86100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_d
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Motorola"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_f

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Sony Ericsson"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "R800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_e
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Lenovo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_10

    .line 259
    :cond_f
    invoke-static {v4}, Lcom/sgiggle/VideoCapture/VideoView;->getScreen(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(ILandroid/graphics/Rect;)V

    .line 260
    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->getScreen(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(ILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 261
    :cond_10
    sget-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isGingerbread:Z

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "PANTECH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 262
    invoke-static {v4, v3, v3}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(III)V

    .line 263
    invoke-static {v2, v3, v3}, Lcom/sgiggle/VideoCapture/VideoView;->setVirtualSize(III)V

    goto/16 :goto_0
.end method

.method private static isRectEqual(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 948
    iget v0, p0, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->top:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 953
    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    if-lt v0, v1, :cond_1

    iget v0, p0, Landroid/graphics/Rect;->top:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 963
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, v3

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 964
    invoke-virtual {p0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget v1, p0, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    if-ne v1, v2, :cond_2

    iget v1, p0, Landroid/graphics/Rect;->right:I

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    if-ne v1, v2, :cond_2

    iget v1, p0, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    sub-int v2, v0, v2

    if-ne v1, v2, :cond_2

    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    iget v2, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    if-ne v1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method private static isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 958
    iget v0, p0, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-le v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->top:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static layoutAdjustSmallCameraSize([Landroid/graphics/Rect;I)V
    .locals 12
    .parameter
    .parameter

    .prologue
    .line 655
    invoke-static {p1}, Lcom/sgiggle/VideoCapture/VideoView;->getVirtualSize(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 657
    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->cameraSmallViews:[I

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 658
    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->cameraSmallViews:[I

    aget v2, v2, v1

    .line 659
    aget-object v3, p0, v2

    .line 662
    invoke-virtual {v3}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v5

    float-to-int v5, v5

    .line 665
    const/4 v6, 0x7

    if-ne v2, v6, :cond_1

    const/4 v2, 0x0

    .line 667
    :goto_1
    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->getCameraSize(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 668
    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->getCaptureSize(I)Landroid/graphics/Rect;

    move-result-object v7

    .line 670
    if-nez p1, :cond_2

    .line 671
    iget v8, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v11

    mul-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 672
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    div-float/2addr v6, v7

    mul-float/2addr v6, v10

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static {v9, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v7, v8

    .line 678
    :goto_2
    int-to-float v8, v7

    int-to-float v9, v6

    sget-object v10, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v10, v10, p1

    aget v10, v10, v2

    mul-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_3

    .line 679
    int-to-float v7, v6

    sget-object v8, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v8, v8, p1

    aget v8, v8, v2

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 683
    :goto_3
    sub-int v8, v4, v7

    iput v8, v3, Landroid/graphics/Rect;->left:I

    .line 684
    add-int/2addr v7, v4

    iput v7, v3, Landroid/graphics/Rect;->right:I

    .line 685
    sub-int v7, v5, v6

    iput v7, v3, Landroid/graphics/Rect;->top:I

    .line 686
    add-int/2addr v6, v5

    iput v6, v3, Landroid/graphics/Rect;->bottom:I

    .line 689
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v6

    const/4 v7, 0x6

    aget-object v7, p0, v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    const/4 v8, 0x2

    sub-int/2addr v7, v8

    if-ge v6, v7, :cond_4

    .line 690
    const/4 v5, 0x6

    aget-object v5, p0, v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    .line 691
    int-to-float v6, v5

    sget-object v7, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v7, v7, p1

    aget v2, v7, v2

    div-float v2, v6, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 692
    sub-int v6, v4, v5

    iput v6, v3, Landroid/graphics/Rect;->left:I

    .line 693
    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 694
    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v4, v2

    iput v2, v3, Landroid/graphics/Rect;->top:I

    .line 657
    :cond_0
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 665
    :cond_1
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 675
    :cond_2
    iget v8, v0, Landroid/graphics/Rect;->left:I

    sub-int v8, v4, v8

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v11

    mul-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 676
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    div-float/2addr v6, v7

    mul-float/2addr v6, v10

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static {v9, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v7, v8

    goto/16 :goto_2

    .line 681
    :cond_3
    int-to-float v6, v7

    sget-object v8, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v8, v8, p1

    aget v8, v8, v2

    div-float/2addr v6, v8

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    goto/16 :goto_3

    .line 696
    :cond_4
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x6

    aget-object v6, p0, v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    const/4 v7, 0x2

    sub-int/2addr v6, v7

    if-ge v4, v6, :cond_0

    .line 697
    const/4 v4, 0x6

    aget-object v4, p0, v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 698
    int-to-float v6, v4

    sget-object v7, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v7, v7, p1

    aget v2, v7, v2

    mul-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 699
    if-nez p1, :cond_5

    .line 700
    iget v6, v3, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v6, v2

    iput v2, v3, Landroid/graphics/Rect;->left:I

    .line 705
    :goto_5
    sub-int v2, v5, v4

    iput v2, v3, Landroid/graphics/Rect;->top:I

    .line 706
    add-int v2, v5, v4

    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_4

    .line 703
    :cond_5
    iget v6, v3, Landroid/graphics/Rect;->left:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v6

    iput v2, v3, Landroid/graphics/Rect;->right:I

    goto :goto_5

    .line 709
    :cond_6
    return-void
.end method

.method public static setCameraSize(III)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 294
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->cameraSize:[Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v4, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, v0, p0

    .line 295
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 296
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v0, v0, v4

    int-to-float v1, p1

    int-to-float v2, p2

    div-float/2addr v1, v2

    aput v1, v0, p0

    .line 297
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v0, v0, v5

    int-to-float v1, p2

    int-to-float v2, p1

    div-float/2addr v1, v2

    aput v1, v0, p0

    .line 298
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v0, v0, v4

    aget v0, v0, p0

    const/high16 v1, 0x4000

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v0, v0, v4

    aget v0, v0, p0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    .line 299
    :cond_0
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_preview_aspect_ratio[Orientation.LANDSCAPE]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not in a reasonable range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v2, v2, v4

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    :cond_1
    :goto_0
    sput-boolean v5, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 304
    return-void

    .line 301
    :cond_2
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_preview_aspect_ratio[Orientation.LANDSCAPE]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_preview_aspect_ratio:[[F

    aget-object v2, v2, v4

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setCaptureSize(III)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 313
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->captureSize:[Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v4, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v1, v0, p0

    .line 314
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 315
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v0, v0, v4

    int-to-float v1, p1

    int-to-float v2, p2

    div-float/2addr v1, v2

    aput v1, v0, p0

    .line 316
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v0, v0, v5

    int-to-float v1, p2

    int-to-float v2, p1

    div-float/2addr v1, v2

    aput v1, v0, p0

    .line 317
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v0, v0, v4

    aget v0, v0, p0

    const/high16 v1, 0x4000

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v0, v0, v4

    aget v0, v0, p0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    .line 318
    :cond_0
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_capture_aspect_ratio[Orientation.LANDSCAPE]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not in a reasonable range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v2, v2, v4

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_1
    :goto_0
    sput-boolean v5, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 323
    return-void

    .line 320
    :cond_2
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_capture_aspect_ratio[Orientation.LANDSCAPE]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_capture_aspect_ratio:[[F

    aget-object v2, v2, v4

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setIsH264Renderer(Z)V
    .locals 1
    .parameter

    .prologue
    .line 272
    sput-boolean p0, Lcom/sgiggle/VideoCapture/VideoView;->m_isH264Renderer:Z

    .line 273
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 274
    return-void
.end method

.method public static setOrientation(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 282
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set orientation of camera"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->orientations:[I

    aput p1, v0, p0

    .line 284
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 285
    return-void
.end method

.method public static setRenderSize(II)Z
    .locals 6
    .parameter
    .parameter

    .prologue
    const v3, 0x3cf5c28f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 354
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 355
    :cond_0
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRenderSize() width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (cannot be zero!)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :cond_1
    int-to-float v0, p0

    int-to-float v1, p1

    div-float/2addr v0, v1

    .line 357
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aget v1, v1, v4

    sub-float/2addr v1, v0

    cmpl-float v1, v1, v3

    if-gtz v1, :cond_2

    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aget v1, v1, v4

    sub-float v1, v0, v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    .line 359
    :cond_2
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aput v0, v1, v4

    .line 360
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    int-to-float v1, p1

    int-to-float v2, p0

    div-float/2addr v1, v2

    aput v1, v0, v5

    .line 361
    sput-boolean v5, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 362
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aget v0, v0, v4

    const/high16 v1, 0x4000

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_3

    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aget v0, v0, v4

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4

    .line 363
    :cond_3
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_renderer_aspect_ratio is not in a reasonable range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    move v0, v5

    .line 369
    :goto_1
    return v0

    .line 365
    :cond_4
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_renderer_aspect_ratio[Orientation.LANDSCAPE]="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->m_renderer_aspect_ratio:[F

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    move v0, v4

    .line 369
    goto :goto_1
.end method

.method public static setSamsungTablet(Z)V
    .locals 3
    .parameter

    .prologue
    .line 127
    const-string v0, "VideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting samsung tablet to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    sput-boolean p0, Lcom/sgiggle/VideoCapture/VideoView;->isSamsungTablet7Inch:Z

    .line 129
    return-void
.end method

.method private static setVirtualSize(III)V
    .locals 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 332
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    aget-object v0, v0, v7

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    .line 333
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, p0

    aget-object v1, v1, v7

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    .line 334
    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, p0

    new-instance v3, Landroid/graphics/Rect;

    neg-int v4, v0

    neg-int v5, v1

    sget-object v6, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v6, v6, p0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/2addr v0, v6

    sget-object v6, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v6, v6, p0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    add-int/2addr v1, v6

    invoke-direct {v3, v4, v5, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v3, v2, v8

    .line 335
    sput-boolean v8, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 336
    return-void
.end method

.method private static setVirtualSize(ILandroid/graphics/Rect;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 343
    sget-object v0, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v0, v0, p0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v1, v0, v2

    .line 344
    sput-boolean v2, Lcom/sgiggle/VideoCapture/VideoView;->isNeedUpdate:Z

    .line 345
    return-void
.end method

.method private static testView()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    move v0, v9

    .line 970
    :goto_0
    if-gt v0, v10, :cond_2f

    .line 971
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    .line 973
    aget-object v2, v1, v10

    aget-object v3, v1, v9

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: VIRTUAL_SCREEN            doesn\'t include SCREEN"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    :cond_0
    aget-object v2, v1, v9

    aget-object v3, v1, v11

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: SCREEN                    doesn\'t include BUTTON"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    :cond_1
    aget-object v2, v1, v9

    const/16 v3, 0xb

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: SCREEN                    doesn\'t include BORDER_PREFERRED_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    :cond_2
    aget-object v2, v1, v9

    const/16 v3, 0xe

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: SCREEN                    doesn\'t include BORDER_RENDERER_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    :cond_3
    aget-object v2, v1, v10

    aget-object v3, v1, v13

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: VIRTUAL_SCREEN            doesn\'t include CAMERA_BACK_BIG"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    :cond_4
    aget-object v2, v1, v10

    aget-object v3, v1, v12

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: VIRTUAL_SCREEN            doesn\'t include CAMERA_FRONT_BIG"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    :cond_5
    sget-boolean v2, Lcom/sgiggle/VideoCapture/VideoView;->m_isH264Renderer:Z

    if-eqz v2, :cond_19

    .line 980
    aget-object v2, v1, v9

    const/16 v3, 0xc

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: SCREEN                    doesn\'t include BORDER_CAMERA_BACK_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 981
    :cond_6
    aget-object v2, v1, v9

    const/16 v3, 0xd

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: SCREEN                    doesn\'t include BORDER_CAMERA_FRONT_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    :cond_7
    :goto_1
    aget-object v2, v1, v9

    const/4 v3, 0x5

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: SCREEN                    doesn\'t include RENDERER_BIG"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    :cond_8
    const/4 v2, 0x7

    aget-object v2, v1, v2

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: CAMERA_BACK_SMALL         doesn\'t include PREFERRED_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    :cond_9
    const/16 v2, 0x8

    aget-object v2, v1, v2

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: CAMERA_FRONT_SMALL        doesn\'t include PREFERRED_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    :cond_a
    const/16 v2, 0xb

    aget-object v2, v1, v2

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BORDER_PREFERRED_SMALL    doesn\'t include PREFERRED_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    :cond_b
    const/16 v2, 0xe

    aget-object v2, v1, v2

    const/16 v3, 0x9

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_c

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BORDER_PREFERRED_SMALL    doesn\'t include RENDERER_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 992
    :cond_c
    const/16 v2, 0xc

    aget-object v2, v1, v2

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BORDER_CAMERA_BACK_SMALL  doesn\'t include CAMERA_BACK_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    :cond_d
    const/16 v2, 0xd

    aget-object v2, v1, v2

    const/16 v3, 0x8

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_e

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BORDER_CAMERA_FRONT_SMALL doesn\'t include CAMERA_FRONT_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    :cond_e
    aget-object v2, v1, v13

    const/16 v3, 0x2a

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_f

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: CAMERA_BACK_BIG           doesn\'t include BUTTON_SWITCH_CAMERA_BACK"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    :cond_f
    aget-object v2, v1, v12

    const/16 v3, 0x2b

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: CAMERA_FRONT_BIG          doesn\'t include BUTTON_SWITCH_CAMERA_FRONT"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    :cond_10
    const/4 v2, 0x5

    aget-object v2, v1, v2

    const/16 v3, 0x2c

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_11

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: RENDERER_BIG              doesn\'t include BUTTON_SWITCH_RENDERER"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 998
    :cond_11
    const/16 v2, 0x27

    aget-object v2, v1, v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1b

    .line 999
    aget-object v2, v1, v11

    const/16 v3, 0x27

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_12

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BUTTON                    doesn\'t include BUTTON_BACKGROUND_CAMERA_BACK"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    :cond_12
    aget-object v2, v1, v13

    const/16 v3, 0x27

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_13

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: CAMERA_BACK_BIG           doesn\'t include BUTTON_BACKGROUND_CAMERA_BACK"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    :cond_13
    :goto_2
    const/16 v2, 0x28

    aget-object v2, v1, v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1c

    .line 1006
    aget-object v2, v1, v11

    const/16 v3, 0x28

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_14

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BUTTON                    doesn\'t include BUTTON_BACKGROUND_CAMERA_FRONT"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    :cond_14
    aget-object v2, v1, v12

    const/16 v3, 0x28

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_15

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: CAMERA_FRONT_BIG          doesn\'t include BUTTON_BACKGROUND_CAMERA_FRONT"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    :cond_15
    :goto_3
    const/16 v2, 0x29

    aget-object v2, v1, v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 1013
    aget-object v2, v1, v11

    const/16 v3, 0x29

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_16

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BUTTON                    doesn\'t include BUTTON_BACKGROUND_RENDERER"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    :cond_16
    const/4 v2, 0x5

    aget-object v2, v1, v2

    const/16 v3, 0x29

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: RENDERER_BIG              doesn\'t include BUTTON_BACKGROUND_RENDERER"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    :cond_17
    :goto_4
    const/16 v2, 0xf

    :goto_5
    const/16 v3, 0x2c

    if-gt v2, v3, :cond_1e

    .line 1021
    aget-object v3, v1, v9

    aget-object v4, v1, v2

    invoke-static {v3, v4}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string v3, "VideoView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ERROR: views["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]: SCREEN                    doesn\'t include view "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    :cond_18
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 984
    :cond_19
    aget-object v2, v1, v10

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_1a

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: VIRTUAL_SCREEN            doesn\'t include CAMERA_BACK_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 985
    :cond_1a
    aget-object v2, v1, v10

    const/16 v3, 0x8

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: VIRTUAL_SCREEN            doesn\'t include CAMERA_FRONT_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1003
    :cond_1b
    aget-object v2, v1, v13

    aget-object v3, v1, v11

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_13

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: CAMERA_BACK_BIG           intersacts BUTTON"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1010
    :cond_1c
    aget-object v2, v1, v12

    aget-object v3, v1, v11

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_15

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: CAMERA_FRONT_BIG          intersacts BUTTON"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1017
    :cond_1d
    const/4 v2, 0x5

    aget-object v2, v1, v2

    aget-object v3, v1, v11

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: RENDERER_BIG              intersacts BUTTON"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1024
    :cond_1e
    const/16 v2, 0xf

    :goto_6
    const/16 v3, 0x26

    if-gt v2, v3, :cond_22

    move v3, v2

    .line 1025
    :goto_7
    add-int/lit8 v4, v2, 0x6

    if-ge v3, v4, :cond_21

    .line 1026
    add-int/lit8 v4, v3, 0x1

    :goto_8
    add-int/lit8 v5, v2, 0x6

    if-ge v4, v5, :cond_20

    .line 1027
    aget-object v5, v1, v3

    aget-object v6, v1, v4

    invoke-static {v5, v6}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v5

    if-nez v5, :cond_1f

    const-string v5, "VideoView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ERROR: views["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]: view "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " intersacts view "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    :cond_1f
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 1025
    :cond_20
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1024
    :cond_21
    add-int/lit8 v2, v2, 0x6

    goto :goto_6

    .line 1032
    :cond_22
    new-array v2, v12, [I

    fill-array-data v2, :array_0

    .line 1033
    new-array v3, v12, [Landroid/graphics/Rect;

    .line 1035
    sget-object v4, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v4, v4, v0

    const/16 v5, 0xe

    aget-object v4, v4, v5

    aput-object v4, v3, v10

    aput-object v4, v3, v9

    .line 1036
    sget-boolean v4, Lcom/sgiggle/VideoCapture/VideoView;->m_isH264Renderer:Z

    if-eqz v4, :cond_25

    .line 1037
    sget-object v4, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v4, v4, v0

    const/16 v5, 0xc

    aget-object v4, v4, v5

    aput-object v4, v3, v11

    .line 1038
    sget-object v4, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v4, v4, v0

    const/16 v5, 0xd

    aget-object v4, v4, v5

    aput-object v4, v3, v13

    :goto_9
    move v4, v9

    .line 1043
    :goto_a
    if-ge v4, v12, :cond_2b

    .line 1044
    mul-int/lit8 v5, v4, 0x6

    add-int/lit8 v5, v5, 0xf

    :goto_b
    mul-int/lit8 v6, v4, 0x6

    add-int/lit8 v6, v6, 0xf

    add-int/lit8 v6, v6, 0x4

    if-ge v5, v6, :cond_26

    .line 1045
    aget v6, v2, v4

    aget-object v6, v1, v6

    aget-object v7, v1, v5

    invoke-static {v6, v7}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    if-nez v6, :cond_23

    const-string v6, "VideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ERROR: views["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]: view "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v2, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " intersacts view "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    :cond_23
    aget-object v6, v3, v4

    aget-object v7, v1, v5

    invoke-static {v6, v7}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    if-nez v6, :cond_24

    const-string v6, "VideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ERROR: views["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]: border "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " intersacts view "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    :cond_24
    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    .line 1041
    :cond_25
    sget-object v4, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v4, v4, v0

    const/16 v5, 0xb

    aget-object v4, v4, v5

    aput-object v4, v3, v13

    aput-object v4, v3, v11

    goto/16 :goto_9

    .line 1048
    :cond_26
    aget v6, v2, v4

    aget-object v6, v1, v6

    aget-object v7, v1, v5

    invoke-static {v6, v7}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    if-nez v6, :cond_27

    const-string v6, "VideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ERROR: views["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]: view "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v2, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " doesn\'t include "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    :cond_27
    aget-object v6, v3, v4

    aget-object v7, v1, v5

    invoke-static {v6, v7}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    if-nez v6, :cond_28

    const-string v6, "VideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ERROR: views["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]: view "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " intersacts border "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    :cond_28
    aget v6, v2, v4

    aget-object v6, v1, v6

    add-int/lit8 v7, v5, 0x1

    aget-object v7, v1, v7

    invoke-static {v6, v7}, Lcom/sgiggle/VideoCapture/VideoView;->isRectInclude(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    if-nez v6, :cond_29

    const-string v6, "VideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ERROR: views["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]: view "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v2, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " doesn\'t include "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v8, v5, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    :cond_29
    aget-object v6, v3, v4

    add-int/lit8 v7, v5, 0x1

    aget-object v7, v1, v7

    invoke-static {v6, v7}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    if-nez v6, :cond_2a

    const-string v6, "VideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ERROR: views["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]: border "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " intersacts view "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    :cond_2a
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_a

    .line 1054
    :cond_2b
    aget-object v2, v1, v11

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_2c

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BUTTON                    intersacts      PREFERRED_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    :cond_2c
    aget-object v2, v1, v11

    const/16 v3, 0x9

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->isRectNoIntersaction(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_2d

    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: views["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: BUTTON                    intersacts      RENDERER_SMALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    :cond_2d
    const/4 v2, 0x6

    aget-object v2, v1, v2

    const/16 v3, 0xa

    aget-object v1, v1, v3

    invoke-static {v2, v1}, Lcom/sgiggle/VideoCapture/VideoView;->isRectEqual(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_2e

    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: PREFERRED_SMALL           doesn\'t equal   TRANSPARENT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    :cond_2e
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_2f
    move v0, v9

    .line 1060
    :goto_c
    const/16 v1, 0xa

    if-gt v0, v1, :cond_31

    .line 1061
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v9

    aget-object v1, v1, v0

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, v10

    aget-object v2, v2, v0

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoView;->isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_30

    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] doesn\'t equal views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1060
    :cond_30
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 1063
    :cond_31
    const/16 v0, 0x27

    :goto_d
    const/16 v1, 0x2c

    if-gt v0, v1, :cond_33

    .line 1064
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v9

    aget-object v1, v1, v0

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, v10

    aget-object v2, v2, v0

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoView;->isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_32

    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] doesn\'t equal views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1063
    :cond_32
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_33
    move v0, v9

    .line 1066
    :goto_e
    const/16 v1, 0x18

    if-ge v0, v1, :cond_3a

    .line 1067
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v9

    add-int/lit8 v2, v0, 0xf

    aget-object v1, v1, v2

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, v10

    add-int/lit8 v3, v0, 0x11

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoView;->isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_34

    .line 1068
    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0xf

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] doesn\'t equal views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x11

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    :cond_34
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v9

    add-int/lit8 v2, v0, 0x10

    aget-object v1, v1, v2

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, v10

    add-int/lit8 v3, v0, 0x12

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoView;->isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_35

    .line 1070
    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] doesn\'t equal views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x12

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    :cond_35
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v9

    add-int/lit8 v2, v0, 0x11

    aget-object v1, v1, v2

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, v10

    add-int/lit8 v3, v0, 0x10

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoView;->isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_36

    .line 1072
    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x11

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] doesn\'t equal views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1073
    :cond_36
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v9

    add-int/lit8 v2, v0, 0x12

    aget-object v1, v1, v2

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, v10

    add-int/lit8 v3, v0, 0xf

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoView;->isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_37

    .line 1074
    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x12

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] doesn\'t equal views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0xf

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    :cond_37
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v9

    add-int/lit8 v2, v0, 0x13

    aget-object v1, v1, v2

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, v10

    add-int/lit8 v3, v0, 0x14

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoView;->isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_38

    .line 1076
    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x13

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] doesn\'t equal views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x14

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    :cond_38
    sget-object v1, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v1, v1, v9

    add-int/lit8 v2, v0, 0x14

    aget-object v1, v1, v2

    sget-object v2, Lcom/sgiggle/VideoCapture/VideoView;->views:[[Landroid/graphics/Rect;

    aget-object v2, v2, v10

    add-int/lit8 v3, v0, 0x13

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoView;->isRectLEqualP(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_39

    .line 1078
    const-string v1, "VideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: views[L]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x14

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] doesn\'t equal views[P]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x13

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    :cond_39
    add-int/lit8 v0, v0, 0x6

    goto/16 :goto_e

    .line 1080
    :cond_3a
    return-void

    .line 1032
    nop

    :array_0
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public static updateContext(Landroid/content/Context;)V
    .locals 5
    .parameter

    .prologue
    .line 138
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 139
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 140
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 141
    const-string v2, "VideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateContext(): width "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " height "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-static {v1, v0}, Lcom/sgiggle/VideoCapture/VideoView;->init(II)V

    .line 143
    return-void
.end method
