.class public Lcom/sgiggle/VideoRenderer/VideoRenderer;
.super Ljava/lang/Object;
.source "VideoRenderer.java"


# static fields
.field public static final MESSAGE_RENDERER_SIZE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "VideoRenderer"

.field private static final THRESHOLD_ASPECT_RATIO:F = 0.03f

.field private static _holder:Landroid/view/SurfaceHolder;

.field private static _holderModified:Z

.field private static config:Landroid/graphics/Bitmap$Config;

.field private static isLandscape:Z

.field private static parentRect:Landroid/graphics/Rect;

.field private static sInstance:Lcom/sgiggle/VideoRenderer/VideoRenderer;

.field private static final s_lock:Ljava/util/concurrent/locks/ReentrantLock;

.field private static transparentRect:Landroid/graphics/Rect;

.field private static ui_handler:Landroid/os/Handler;


# instance fields
.field private bb:Ljava/nio/ByteBuffer;

.field private bitmap:Landroid/graphics/Bitmap;

.field private bpp:I

.field private canvas:Landroid/graphics/Canvas;

.field private current_aspect_ratio:F

.field private current_height:I

.field private current_parentRect:Landroid/graphics/Rect;

.field private current_transparentRect:Landroid/graphics/Rect;

.field private current_width:I

.field private dst:Landroid/graphics/Rect;

.field private expansion:I

.field private isNativeBitmap:Z

.field private isStarted:Z

.field private rotation:I

.field private small_height:I

.field private small_width:I

.field private small_x:I

.field private small_y:I

.field private transparent_region:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->sInstance:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    .line 25
    sput-boolean v4, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holderModified:Z

    .line 26
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    .line 51
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    .line 55
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v5}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 450
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    .line 451
    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    move v1, v4

    .line 453
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 454
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sgiggle.VideoCaptureTest"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v5

    .line 459
    :goto_1
    if-eqz v0, :cond_0

    .line 460
    const-string v0, "VideoCapture"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 461
    :cond_0
    return-void

    .line 453
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_parentRect:Landroid/graphics/Rect;

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_transparentRect:Landroid/graphics/Rect;

    .line 63
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 64
    sput-object p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->sInstance:Lcom/sgiggle/VideoRenderer/VideoRenderer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 71
    return-void

    .line 69
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private native NV21toRGBclip([BIILjava/nio/ByteBuffer;III)V
.end method

.method public static clear(I)V
    .locals 13
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 401
    invoke-static {}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isH264Renderer()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 437
    :cond_0
    return-void

    .line 404
    :cond_1
    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v10

    .line 405
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 406
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v7

    .line 407
    if-eqz v4, :cond_0

    if-eqz v7, :cond_0

    .line 410
    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v7, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 411
    invoke-virtual {v1, p0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 412
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v2, v5, :cond_2

    .line 413
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v8

    .line 414
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v11

    .line 415
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sget-object v5, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int v5, v2, v5

    .line 416
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sget-object v6, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v2, v6

    .line 417
    mul-int v2, v8, v11

    new-array v2, v2, [I

    .line 419
    sub-int v12, v4, v5

    if-le v8, v12, :cond_5

    .line 420
    sub-int/2addr v4, v5

    .line 422
    :goto_0
    sub-int v8, v7, v6

    if-le v11, v8, :cond_4

    .line 423
    sub-int/2addr v7, v6

    move v8, v7

    .line 424
    :goto_1
    const-string v7, "VideoRenderer"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "clear() ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v11}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    if-lez v4, :cond_2

    if-lez v8, :cond_2

    move v7, v4

    .line 426
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 428
    :cond_2
    :goto_2
    const/4 v2, 0x2

    if-ge v3, v2, :cond_0

    .line 429
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, v9}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v4

    .line 430
    if-eqz v4, :cond_3

    .line 431
    move-object v0, v9

    check-cast v0, Landroid/graphics/Rect;

    move-object v2, v0

    invoke-virtual {v4, v1, v2, v10, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 432
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, v4}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 428
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 434
    :cond_3
    const-string v2, "VideoRenderer"

    const-string v4, "clear: canvas is null"

    invoke-static {v2, v4}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_4
    move v8, v11

    goto :goto_1

    :cond_5
    move v4, v8

    goto :goto_0
.end method

.method private native convertColor(IIILjava/nio/ByteBuffer;)V
.end method

.method private native convertColorBitmap(IIILandroid/graphics/Bitmap;)V
.end method

.method public static native hasH264Renderer()Z
.end method

.method private init(II)Z
    .locals 11
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    const v5, 0x3cf5c28f

    const/4 v1, 0x0

    .line 162
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isStarted:Z

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 266
    :goto_0
    return v0

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_width:I

    if-ne p1, v0, :cond_3

    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_height:I

    if-ne p2, v0, :cond_3

    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_parentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 168
    :cond_3
    const-string v0, "VideoRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_parentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 172
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_parentRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 175
    :cond_4
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 176
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_transparentRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 179
    :cond_5
    iput p1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_width:I

    .line 180
    iput p2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_height:I

    .line 182
    if-eqz p1, :cond_6

    if-nez p2, :cond_7

    .line 183
    :cond_6
    const-string v0, "VideoRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init() width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (cannot be zero!)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_7
    int-to-float v0, p1

    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 185
    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_aspect_ratio:F

    sub-float/2addr v2, v0

    cmpl-float v2, v2, v5

    if-gtz v2, :cond_8

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_aspect_ratio:F

    sub-float v2, v0, v2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_9

    .line 188
    :cond_8
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->updateRendererSize(II)V

    .line 190
    iput v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->current_aspect_ratio:F

    .line 193
    :cond_9
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    if-eqz v2, :cond_e

    .line 194
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-eqz v2, :cond_a

    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-nez v2, :cond_b

    .line 195
    :cond_a
    const-string v2, "VideoRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init() parentRect.width="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " parentRect.height="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (cannot be zero!)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_b
    sget-boolean v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isLandscape:Z

    if-eqz v2, :cond_d

    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 197
    :goto_1
    sub-float v3, v2, v0

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_c

    sub-float/2addr v0, v2

    cmpl-float v0, v0, v5

    if-lez v0, :cond_e

    :cond_c
    move v0, v1

    .line 201
    goto/16 :goto_0

    .line 196
    :cond_d
    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    goto :goto_1

    .line 205
    :cond_e
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/2addr v0, p1

    iput v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    .line 206
    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    const/4 v2, 0x3

    if-lt v0, v2, :cond_14

    .line 207
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    .line 208
    iput v6, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    .line 212
    :goto_2
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v0, v2, :cond_15

    .line 213
    iput v6, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bpp:I

    .line 216
    :goto_3
    sget-boolean v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isLandscape:Z

    if-eqz v0, :cond_16

    .line 217
    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    mul-int/2addr v0, p1

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    mul-int/2addr v2, p2

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    .line 218
    iput v1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->rotation:I

    .line 223
    :goto_4
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bpp:I

    mul-int/2addr v2, v3

    if-eq v0, v2, :cond_f

    .line 224
    iput-boolean v1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isNativeBitmap:Z

    .line 225
    :cond_f
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v0, v2, :cond_17

    .line 226
    iget-boolean v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isNativeBitmap:Z

    if-nez v0, :cond_10

    .line 227
    mul-int v0, p1, p2

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bpp:I

    mul-int/2addr v0, v2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bb:Ljava/nio/ByteBuffer;

    .line 228
    :cond_10
    iget v4, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bpp:I

    iget v5, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->rotation:I

    iget v6, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    move-object v0, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->initColorConverter(IIIIII)V

    .line 229
    iput-object v10, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparent_region:[I

    .line 255
    :goto_5
    const-string v0, "VideoRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bitmap "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const-string v0, "VideoRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isNativeBitmap "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isNativeBitmap:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const-string v0, "VideoRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bpp "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bpp:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    const-string v0, "VideoRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rotation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->rotation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    const-string v0, "VideoRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "expansion "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const-string v0, "VideoRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "transparent_region ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_11
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->dst:Landroid/graphics/Rect;

    if-eqz v0, :cond_12

    sget-boolean v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holderModified:Z

    if-eqz v0, :cond_13

    .line 263
    :cond_12
    sput-boolean v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holderModified:Z

    .line 264
    new-instance v0, Landroid/graphics/Rect;

    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->dst:Landroid/graphics/Rect;

    :cond_13
    move v0, v9

    .line 266
    goto/16 :goto_0

    .line 210
    :cond_14
    iput v9, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    goto/16 :goto_2

    .line 215
    :cond_15
    const/4 v0, 0x4

    iput v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bpp:I

    goto/16 :goto_3

    .line 220
    :cond_16
    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    mul-int/2addr v0, p2

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    mul-int/2addr v2, p1

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    .line 221
    const/16 v0, 0x5a

    iput v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->rotation:I

    goto/16 :goto_4

    .line 231
    :cond_17
    iget-boolean v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isNativeBitmap:Z

    if-nez v0, :cond_18

    .line 232
    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    mul-int/2addr v0, p1

    mul-int/2addr v0, p2

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bpp:I

    mul-int/2addr v0, v2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bb:Ljava/nio/ByteBuffer;

    .line 233
    :cond_18
    iget v6, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bpp:I

    iget v7, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->rotation:I

    iget v8, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->expansion:I

    move-object v2, p0

    move v3, v9

    move v4, p1

    move v5, p2

    invoke-direct/range {v2 .. v8}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->initColorConverter(IIIIII)V

    .line 234
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 235
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sget-object v2, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 236
    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 237
    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_width:I

    .line 238
    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_height:I

    .line 239
    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sget-object v4, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_x:I

    .line 240
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sget-object v3, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_y:I

    .line 242
    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_width:I

    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_x:I

    sub-int/2addr v2, v3

    if-le v0, v2, :cond_19

    .line 243
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_x:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_width:I

    .line 245
    :cond_19
    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_height:I

    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_y:I

    sub-int/2addr v2, v3

    if-le v0, v2, :cond_1a

    .line 246
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_y:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_height:I

    .line 247
    :cond_1a
    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_width:I

    if-lez v0, :cond_1b

    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_height:I

    if-lez v0, :cond_1b

    .line 248
    iget v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_width:I

    iget v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_height:I

    mul-int/2addr v0, v2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparent_region:[I

    goto/16 :goto_5

    .line 250
    :cond_1b
    iput-object v10, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparent_region:[I

    goto/16 :goto_5

    .line 252
    :cond_1c
    iput-object v10, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparent_region:[I

    goto/16 :goto_5
.end method

.method private native initColorConverter(IIIIII)V
.end method

.method public static native isH264Renderer()Z
.end method

.method private renderBuffer(Landroid/graphics/Bitmap;)V
    .locals 10
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 381
    iget-object v1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparent_region:[I

    if-eqz v1, :cond_0

    .line 382
    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparent_region:[I

    const/4 v3, 0x0

    iget v4, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_width:I

    iget v5, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_x:I

    iget v6, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_y:I

    iget v7, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_width:I

    iget v8, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->small_height:I

    move-object v1, p1

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 384
    :cond_0
    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->canvas:Landroid/graphics/Canvas;

    .line 385
    iget-object v1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->canvas:Landroid/graphics/Canvas;

    if-eqz v1, :cond_1

    .line 386
    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->canvas:Landroid/graphics/Canvas;

    move-object v0, v9

    check-cast v0, Landroid/graphics/Rect;

    move-object v1, v0

    iget-object v3, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->dst:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, v1, v3, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 387
    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    iget-object v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->canvas:Landroid/graphics/Canvas;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 391
    :goto_0
    return-void

    .line 389
    :cond_1
    const-string v1, "VideoRenderer"

    const-string v2, "renderBuffer: canvas is null"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private renderBuffer(Ljava/nio/ByteBuffer;)V
    .locals 1
    .parameter

    .prologue
    .line 371
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p1}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 373
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->renderBuffer(Landroid/graphics/Bitmap;)V

    .line 374
    return-void
.end method

.method public static setActivityHandler(Landroid/os/Handler;)V
    .locals 2
    .parameter

    .prologue
    .line 275
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 276
    sput-object p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->ui_handler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 280
    return-void

    .line 278
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public static setBitmapConfig(Landroid/graphics/Bitmap$Config;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 95
    sput-object p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    .line 96
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->parentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 97
    if-eqz p2, :cond_0

    .line 98
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 101
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 102
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isLandscape:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :goto_1
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 108
    return-void

    .line 100
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->transparentRect:Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 104
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    sput-boolean v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isLandscape:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public static setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 3
    .parameter

    .prologue
    .line 80
    const-string v0, "VideoRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPreviewDisplay(holder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    sput-object p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    .line 83
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holderModified:Z

    .line 84
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->setSurface(Landroid/view/Surface;)V

    .line 85
    return-void

    .line 84
    :cond_0
    invoke-interface {p0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    goto :goto_0
.end method

.method private static native setSurface(Landroid/view/Surface;)V
.end method

.method public static staticStartRenderer()V
    .locals 2

    .prologue
    .line 115
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 116
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->sInstance:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->sInstance:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->startRenderer()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :cond_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 121
    return-void

    .line 119
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public static staticStopRenderer()V
    .locals 2

    .prologue
    .line 137
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 138
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->sInstance:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    if-eqz v0, :cond_0

    .line 139
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->sInstance:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->stopRenderer()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    :cond_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 143
    return-void

    .line 141
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private updateRendererSize(II)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 286
    const-string v0, "VideoRenderer"

    const-string v1, "updateRendererSize()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 290
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->ui_handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 291
    const-string v0, "VideoRenderer"

    const-string v1, "updateRendererSize: sending message to ui_handler"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->ui_handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-static {v0, v1, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    :goto_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 300
    return-void

    .line 295
    :cond_0
    :try_start_1
    const-string v0, "VideoRenderer"

    const-string v1, "updateLayout: no ui_handler"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 298
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method


# virtual methods
.method public render(III)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    :try_start_0
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 347
    invoke-direct {p0, p2, p3}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->init(II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 361
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 363
    :goto_0
    return-void

    .line 349
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isNativeBitmap:Z

    if-eqz v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->convertColorBitmap(IIILandroid/graphics/Bitmap;)V

    .line 353
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->renderBuffer(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361
    :goto_1
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 356
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bb:Ljava/nio/ByteBuffer;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->convertColor(IIILjava/nio/ByteBuffer;)V

    .line 358
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bb:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->renderBuffer(Ljava/nio/ByteBuffer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 361
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/sgiggle/VideoRenderer/VideoRenderer;->s_lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public render(Ljava/nio/ByteBuffer;II)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 328
    invoke-direct {p0, p2, p3}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->init(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    :goto_0
    return-void

    .line 330
    :cond_0
    invoke-direct {p0, p1}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->renderBuffer(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public render([BIIIII)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 313
    invoke-direct {p0, p4, p5}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->init(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v4, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bb:Ljava/nio/ByteBuffer;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->NV21toRGBclip([BIILjava/nio/ByteBuffer;III)V

    .line 317
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bb:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->renderBuffer(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public startRenderer()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 124
    sget-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->_holder:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_0

    .line 125
    const-string v0, "VideoRenderer"

    const-string v1, "no surface"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_0
    iput-boolean v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isStarted:Z

    .line 129
    return v2
.end method

.method public stopRenderer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 147
    iput-boolean v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isStarted:Z

    .line 148
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->config:Landroid/graphics/Bitmap$Config;

    .line 149
    iput-object v1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bitmap:Landroid/graphics/Bitmap;

    .line 150
    iput-object v1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->dst:Landroid/graphics/Rect;

    .line 151
    iput-object v1, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->bb:Ljava/nio/ByteBuffer;

    .line 152
    iput v2, p0, Lcom/sgiggle/VideoRenderer/VideoRenderer;->rotation:I

    .line 153
    return-void
.end method
