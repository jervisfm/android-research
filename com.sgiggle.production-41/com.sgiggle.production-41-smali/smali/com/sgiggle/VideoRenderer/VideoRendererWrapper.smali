.class public Lcom/sgiggle/VideoRenderer/VideoRendererWrapper;
.super Ljava/lang/Object;
.source "VideoRendererWrapper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoRendererWrapper"


# instance fields
.field m_videoRenderer:Lcom/sgiggle/VideoRenderer/VideoRenderer;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRendererWrapper;->m_videoRenderer:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    .line 23
    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/sgiggle/VideoRenderer/VideoRenderer;

    invoke-direct {v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRendererWrapper;->m_videoRenderer:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    .line 30
    return-void
.end method

.method public render(III)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRendererWrapper;->m_videoRenderer:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->render(III)V

    .line 51
    return-void
.end method

.method public startRenderer()Z
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRendererWrapper;->m_videoRenderer:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->startRenderer()Z

    move-result v0

    return v0
.end method

.method public stopRenderer()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sgiggle/VideoRenderer/VideoRendererWrapper;->m_videoRenderer:Lcom/sgiggle/VideoRenderer/VideoRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->stopRenderer()V

    .line 44
    return-void
.end method
