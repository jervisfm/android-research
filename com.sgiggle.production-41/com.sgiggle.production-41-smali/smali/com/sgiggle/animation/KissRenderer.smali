.class public Lcom/sgiggle/animation/KissRenderer;
.super Ljava/lang/Object;
.source "KissRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native nativeInit()V
.end method

.method private static native nativeRender()V
.end method

.method private static native nativeResize(II)V
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    invoke-static {}, Lcom/sgiggle/animation/KissRenderer;->nativeRender()V

    .line 25
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-static {p2, p3}, Lcom/sgiggle/animation/KissRenderer;->nativeResize(II)V

    .line 21
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-static {}, Lcom/sgiggle/animation/KissRenderer;->nativeInit()V

    .line 17
    return-void
.end method
