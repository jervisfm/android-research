.class public Lcom/sgiggle/cafe/CafePrivate;
.super Ljava/lang/Object;
.source "CafePrivate.java"


# static fields
.field private static mAssetMgr:Landroid/content/res/AssetManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static GetAssetMgr()Landroid/content/res/AssetManager;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sgiggle/cafe/CafePrivate;->mAssetMgr:Landroid/content/res/AssetManager;

    return-object v0
.end method

.method public static Init(Landroid/content/res/AssetManager;)V
    .locals 0
    .parameter

    .prologue
    .line 20
    sput-object p0, Lcom/sgiggle/cafe/CafePrivate;->mAssetMgr:Landroid/content/res/AssetManager;

    .line 21
    invoke-static {}, Lcom/sgiggle/cafe/CafePrivate;->LinkNative()V

    .line 22
    return-void
.end method

.method private static native LinkNative()V
.end method

.method public static OpenForReadFile(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3
    .parameter

    .prologue
    .line 28
    :try_start_0
    invoke-static {}, Lcom/sgiggle/cafe/CafePrivate;->GetAssetMgr()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :catch_0
    move-exception v0

    .line 32
    const-string v0, "CAFE-PRIVATE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LOG-FAILED-GetAssetMgr().open("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ReadFile(Ljava/io/InputStream;I)[B
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 43
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 46
    if-gez p1, :cond_2

    .line 48
    const/16 v1, 0x400

    new-array v1, v1, [B

    .line 50
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-eq v2, v5, :cond_0

    .line 52
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    .line 79
    const-string v0, "CAFE-PRIVATE"

    const-string v1, "ReadFile ERROR"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    new-array v0, v4, [B

    :goto_1
    return-object v0

    .line 55
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_1
    aput-byte v3, v1, v2

    .line 56
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 74
    :cond_1
    :goto_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto :goto_1

    .line 60
    :cond_2
    new-array v1, p1, [B

    .line 62
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-eq v2, v5, :cond_3

    .line 64
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 67
    :cond_3
    if-ge v2, p1, :cond_1

    .line 69
    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-byte v3, v1, v2

    .line 70
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
