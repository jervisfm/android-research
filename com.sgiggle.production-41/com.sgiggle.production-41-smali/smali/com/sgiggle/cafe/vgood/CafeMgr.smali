.class public Lcom/sgiggle/cafe/vgood/CafeMgr;
.super Ljava/lang/Object;
.source "CafeMgr.java"


# static fields
.field public static final DEFAULT_CINEMATIC:Ljava/lang/String; = "Surprise.Cafe"

.field public static final DEFAULT_CLIP:Ljava/lang/String; = ""

.field public static final MAIN_VIEW:I = 0x0

.field public static final VIEW_ALTERNATE:I = 0x1

.field public static final VIEW_MAIN:I

.field static sInitCouter:I

.field public static vgoodPurchased:Z

.field public static vgoodSupport:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    sput-boolean v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodPurchased:Z

    .line 28
    sput v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->sInitCouter:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native Free()V
.end method

.method public static FreeEngine()V
    .locals 2

    .prologue
    .line 39
    sget v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->sInitCouter:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    sput v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->sInitCouter:I

    .line 40
    sget v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->sInitCouter:I

    if-gtz v0, :cond_0

    .line 41
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Free()V

    .line 42
    const/4 v0, 0x0

    sput v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->sInitCouter:I

    .line 44
    :cond_0
    return-void
.end method

.method public static native FreeGraphics()V
.end method

.method private static native Init(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public static InitEngine(Landroid/content/Context;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 31
    sget v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->sInitCouter:I

    if-nez v0, :cond_0

    .line 32
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/cafe/CafePrivate;->Init(Landroid/content/res/AssetManager;)V

    .line 33
    const-string v0, "Media"

    const-string v1, "MediaZ"

    const-string v2, "Save"

    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getOpenGlEsVersion()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    move v3, v5

    :goto_0
    invoke-static {v5, v0, v1, v2, v3}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Init(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 35
    :cond_0
    sget v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->sInitCouter:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->sInitCouter:I

    .line 36
    return-void

    .line 33
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static native InvalidateRenderView(I)V
.end method

.method public static native LoadSurprise(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static native OnSurfaceCreated()V
.end method

.method public static native OnTouchBegan(IFF)V
.end method

.method public static native OnTouchEnded(IFF)V
.end method

.method public static native OnTouchMoved(IFF)V
.end method

.method public static native Pause()V
.end method

.method public static native PlayClip(ILjava/lang/String;Z)V
.end method

.method public static native Render()V
.end method

.method public static native RenderClear()V
.end method

.method public static native RenderMain()V
.end method

.method public static native RenderView(IZZZ)V
.end method

.method public static Reset()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 52
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Free()V

    .line 53
    const-string v0, "Media"

    const-string v1, "MediaZ"

    const-string v2, "Save"

    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getOpenGlEsVersion()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    move v3, v5

    :goto_0
    invoke-static {v5, v0, v1, v2, v3}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Init(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 54
    return-void

    .line 53
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static native Resume()V
.end method

.method public static native SetCallbacks()V
.end method

.method public static native SetGodMode(Z)V
.end method

.method public static native SetRenderClearColor(IFFFF)F
.end method

.method public static native SetRenderView(IIIIII)V
.end method

.method public static native SetSurpriseTrackVisibility(ILjava/lang/String;Z)V
.end method

.method public static native StartAvatarSurpriseForClip(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJZ)I
.end method

.method public static native StartSurprise(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)I
.end method

.method public static StartSurprise(Ljava/lang/String;JJZ)I
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    const/4 v0, 0x0

    const-string v2, "Surprise.Cafe"

    const-string v3, ""

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move v8, p5

    invoke-static/range {v0 .. v8}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StartSurprise(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)I

    move-result v0

    return v0
.end method

.method public static native StopAllSurprises()V
.end method

.method public static native StopSurprise(I)V
.end method

.method public static native forceUpdateAvatarTrack(Z)V
.end method

.method public static initVGoodStatus(Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 22
    sput-object p0, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodSupport:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 23
    sput-boolean p1, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodPurchased:Z

    .line 24
    return-void
.end method

.method public static native setAvatarSurpriseId(ZI)V
.end method

.method public static native updateAvatarTracksVisibility()V
.end method
