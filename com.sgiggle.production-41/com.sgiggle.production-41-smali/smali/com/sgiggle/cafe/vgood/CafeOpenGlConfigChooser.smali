.class public Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;
.super Ljava/lang/Object;
.source "CafeOpenGlConfigChooser.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$EGLConfigChooser;


# static fields
.field private static m_version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput v0, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->m_version:I

    .line 87
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->checkVersion()V

    .line 88
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkVersion()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 19
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    .line 20
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    .line 22
    new-array v2, v3, [I

    .line 23
    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    .line 25
    invoke-static {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getOpenGlEs2Config(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 26
    sput v3, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->m_version:I

    .line 31
    :goto_0
    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 32
    return-void

    .line 27
    :cond_0
    invoke-static {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getOpenGlEsConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 28
    const/4 v2, 0x1

    sput v2, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->m_version:I

    goto :goto_0

    .line 30
    :cond_1
    const/4 v2, 0x0

    sput v2, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->m_version:I

    goto :goto_0
.end method

.method private static getAnyConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 68
    new-array v0, v2, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 69
    new-array v1, v2, [I

    .line 70
    invoke-interface {p0, p1, v0, v2, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigs(Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 71
    aget v1, v1, v3

    if-lez v1, :cond_0

    .line 72
    aget-object v0, v0, v3

    .line 73
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getOpenGlEs2Config(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 35
    const/16 v0, 0xb

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    .line 43
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 44
    new-array v5, v4, [I

    move-object v0, p0

    move-object v1, p1

    .line 45
    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 46
    aget v0, v5, v6

    if-lez v0, :cond_0

    .line 47
    aget-object v0, v3, v6

    .line 48
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 35
    nop

    :array_0
    .array-data 0x4
        0x24t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x23t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x22t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x21t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x40t 0x30t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x38t 0x30t 0x0t 0x0t
    .end array-data
.end method

.method private static getOpenGlEsConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 52
    const/16 v0, 0x9

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    .line 59
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 60
    new-array v5, v4, [I

    move-object v0, p0

    move-object v1, p1

    .line 61
    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 62
    aget v0, v5, v6

    if-lez v0, :cond_0

    .line 63
    aget-object v0, v3, v6

    .line 64
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 52
    nop

    :array_0
    .array-data 0x4
        0x24t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x23t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x22t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x21t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x38t 0x30t 0x0t 0x0t
    .end array-data
.end method

.method public static getOpenGlEsVersion()I
    .locals 1

    .prologue
    .line 15
    sget v0, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->m_version:I

    return v0
.end method


# virtual methods
.method public chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 78
    sget v0, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->m_version:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 79
    invoke-static {p1, p2}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getOpenGlEs2Config(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 80
    :cond_0
    sget v0, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->m_version:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 81
    invoke-static {p1, p2}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getOpenGlEsConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_1
    invoke-static {p1, p2}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getAnyConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    goto :goto_0
.end method
