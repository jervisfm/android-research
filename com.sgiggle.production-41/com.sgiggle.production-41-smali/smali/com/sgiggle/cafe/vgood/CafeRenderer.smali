.class public Lcom/sgiggle/cafe/vgood/CafeRenderer;
.super Ljava/lang/Object;
.source "CafeRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "Cafe-UI"


# instance fields
.field mAutoAdjustRotation:Z

.field private mCafeViewRenderers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;",
            ">;"
        }
    .end annotation
.end field

.field mRotate:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mRotate:Z

    .line 19
    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mAutoAdjustRotation:Z

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    .line 30
    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mRotate:Z

    .line 19
    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mAutoAdjustRotation:Z

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    .line 32
    iput-boolean p1, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mRotate:Z

    .line 33
    iput-boolean p2, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mAutoAdjustRotation:Z

    .line 34
    return-void
.end method

.method public static setCafeMainView(IIZZ)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 94
    const-string v1, "Cafe-UI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setCafeMainView "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    if-eqz p3, :cond_0

    if-gt p0, p1, :cond_1

    :cond_0
    if-eqz p2, :cond_3

    .line 98
    :cond_1
    mul-int/lit8 v1, p1, 0x3

    div-int/lit8 v1, v1, 0x2

    .line 99
    if-le v1, p0, :cond_2

    .line 100
    new-instance v2, Landroid/graphics/Rect;

    sub-int v1, p0, v1

    invoke-direct {v2, v1, v0, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v1, v2

    .line 104
    :goto_0
    const/16 v2, 0x5a

    move-object v4, v1

    move v5, v2

    .line 114
    :goto_1
    iget v1, v4, Landroid/graphics/Rect;->left:I

    iget v2, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderView(IIIIII)V

    .line 115
    invoke-static {v0, v6, v6, v6, v6}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderClearColor(IFFFF)F

    .line 116
    return-void

    .line 102
    :cond_2
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v0, v0, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    .line 106
    :cond_3
    mul-int/lit8 v1, p0, 0x3

    div-int/lit8 v1, v1, 0x2

    .line 107
    if-le v1, p1, :cond_4

    .line 108
    new-instance v2, Landroid/graphics/Rect;

    sub-int v1, p1, v1

    invoke-direct {v2, v0, v1, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v1, v2

    :goto_2
    move-object v4, v1

    move v5, v0

    .line 112
    goto :goto_1

    .line 110
    :cond_4
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v0, v0, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_2
.end method


# virtual methods
.method public addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public clearCafeViewRenderers()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 45
    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 2
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 78
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;

    .line 80
    if-eqz v0, :cond_0

    .line 81
    invoke-interface {v0}, Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;->render()V

    .line 78
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_1
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->RenderMain()V

    .line 87
    :cond_2
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 62
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 63
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;

    .line 64
    if-eqz v0, :cond_0

    .line 65
    iget-boolean v2, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mRotate:Z

    iget-boolean v3, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mAutoAdjustRotation:Z

    invoke-interface {v0, p2, p3, v2, v3}, Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;->onSurfaceChanged(IIZZ)V

    .line 62
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 69
    :cond_1
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mRotate:Z

    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mAutoAdjustRotation:Z

    invoke-static {p2, p3, v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->setCafeMainView(IIZZ)V

    .line 71
    :cond_2
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->OnSurfaceCreated()V

    .line 51
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeRenderer;->mCafeViewRenderers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;

    .line 53
    if-eqz v0, :cond_0

    .line 54
    invoke-interface {v0}, Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;->onSurfaceCreated()V

    .line 51
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57
    :cond_1
    return-void
.end method
