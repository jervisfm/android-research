.class public Lcom/sgiggle/cafe/vgood/CafeView;
.super Landroid/opengl/GLSurfaceView;
.source "CafeView.java"


# instance fields
.field private m_deliverTouchToCafe:Z

.field private m_renderer:Lcom/sgiggle/cafe/vgood/CafeRenderer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/CafeView;->m_deliverTouchToCafe:Z

    .line 20
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/CafeView;->init()V

    .line 21
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getOpenGlEsVersion()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 25
    invoke-virtual {p0, v2}, Lcom/sgiggle/cafe/vgood/CafeView;->setEGLContextClientVersion(I)V

    .line 26
    :cond_0
    new-instance v0, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;

    invoke-direct {v0}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sgiggle/cafe/vgood/CafeView;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    .line 27
    new-instance v0, Lcom/sgiggle/cafe/vgood/CafeRenderer;

    invoke-direct {v0}, Lcom/sgiggle/cafe/vgood/CafeRenderer;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeView;->m_renderer:Lcom/sgiggle/cafe/vgood/CafeRenderer;

    .line 28
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeView;->m_renderer:Lcom/sgiggle/cafe/vgood/CafeRenderer;

    invoke-virtual {p0, v0}, Lcom/sgiggle/cafe/vgood/CafeView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 29
    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/CafeView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, -0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 30
    return-void
.end method


# virtual methods
.method public getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeView;->m_renderer:Lcom/sgiggle/cafe/vgood/CafeRenderer;

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 38
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/CafeView;->m_deliverTouchToCafe:Z

    if-nez v0, :cond_0

    .line 39
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 73
    :goto_0
    return v0

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/CafeView;->getHeight()I

    move-result v0

    .line 42
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 44
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 73
    goto :goto_0

    .line 48
    :pswitch_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_3

    .line 49
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .line 51
    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    int-to-float v0, v0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v2, v3, v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->OnTouchBegan(IFF)V

    move v0, v7

    .line 52
    goto :goto_0

    .line 58
    :pswitch_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_2

    .line 59
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .line 61
    :goto_2
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    int-to-float v0, v0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v2, v3, v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->OnTouchEnded(IFF)V

    move v0, v7

    .line 62
    goto :goto_0

    .line 66
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    .line 67
    :goto_3
    if-ge v2, v1, :cond_1

    .line 68
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    int-to-float v5, v0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    sub-float/2addr v5, v6

    invoke-static {v3, v4, v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->OnTouchMoved(IFF)V

    .line 67
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_1
    move v0, v7

    .line 70
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1

    .line 44
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDeliverTouchToCafe(Z)V
    .locals 0
    .parameter

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sgiggle/cafe/vgood/CafeView;->m_deliverTouchToCafe:Z

    .line 34
    return-void
.end method
