.class public Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;
.super Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
.source "CafeViewForCanvasRenderer.java"


# instance fields
.field private m_renderer:Lcom/sgiggle/cafe/vgood/CafeRenderer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->init(ZZ)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0, p1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;-><init>(Landroid/content/Context;)V

    .line 33
    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->init(ZZ)V

    .line 34
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->init(ZZ)V

    .line 29
    return-void
.end method

.method private init(ZZ)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x2

    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;->getOpenGlEsVersion()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 38
    invoke-virtual {p0, v2}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setEGLContextClientVersion(I)V

    .line 39
    :cond_0
    new-instance v0, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;

    invoke-direct {v0}, Lcom/sgiggle/cafe/vgood/CafeOpenGlConfigChooser;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    .line 40
    new-instance v0, Lcom/sgiggle/cafe/vgood/CafeRenderer;

    invoke-direct {v0, p1, p2}, Lcom/sgiggle/cafe/vgood/CafeRenderer;-><init>(ZZ)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->m_renderer:Lcom/sgiggle/cafe/vgood/CafeRenderer;

    .line 41
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->m_renderer:Lcom/sgiggle/cafe/vgood/CafeRenderer;

    invoke-virtual {p0, v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 42
    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, -0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 43
    return-void
.end method


# virtual methods
.method public getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->m_renderer:Lcom/sgiggle/cafe/vgood/CafeRenderer;

    return-object v0
.end method
