.class abstract Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;
.super Ljava/lang/Object;
.source "GLSurfaceViewForCanvasRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$EGLConfigChooser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BaseConfigChooser"
.end annotation


# instance fields
.field protected mConfigSpec:[I

.field final synthetic this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;


# direct methods
.method public constructor <init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;[I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 669
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670
    invoke-direct {p0, p2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;->filterConfigSpec([I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;->mConfigSpec:[I

    .line 671
    return-void
.end method

.method private filterConfigSpec([I)[I
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 705
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextClientVersion:I
    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$200(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    move-object v0, p1

    .line 717
    :goto_0
    return-object v0

    .line 711
    :cond_0
    array-length v0, p1

    .line 712
    add-int/lit8 v1, v0, 0x2

    new-array v1, v1, [I

    .line 713
    sub-int v2, v0, v4

    invoke-static {p1, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 714
    sub-int v2, v0, v4

    const/16 v3, 0x3040

    aput v3, v1, v2

    .line 715
    const/4 v2, 0x4

    aput v2, v1, v0

    .line 716
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x3038

    aput v2, v1, v0

    move-object v0, v1

    .line 717
    goto :goto_0
.end method


# virtual methods
.method public chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 674
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 675
    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;->mConfigSpec:[I

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 677
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 680
    :cond_0
    aget v4, v5, v4

    .line 682
    if-gtz v4, :cond_1

    .line 683
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No configs match configSpec"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 687
    :cond_1
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 688
    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;->mConfigSpec:[I

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 690
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig#2 failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 692
    :cond_2
    invoke-virtual {p0, p1, p2, v3}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;->chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    .line 693
    if-nez v0, :cond_3

    .line 694
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No config chosen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 696
    :cond_3
    return-object v0
.end method

.method abstract chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
.end method
