.class Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;
.super Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;
.source "GLSurfaceViewForCanvasRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComponentSizeChooser"
.end annotation


# instance fields
.field protected mAlphaSize:I

.field protected mBlueSize:I

.field protected mDepthSize:I

.field protected mGreenSize:I

.field protected mRedSize:I

.field protected mStencilSize:I

.field private mValue:[I

.field final synthetic this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;


# direct methods
.method public constructor <init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;IIIIII)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 727
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    .line 728
    const/16 v0, 0xd

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3024

    aput v2, v0, v1

    aput p2, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0x3023

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p3, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x3022

    aput v2, v0, v1

    const/4 v1, 0x5

    aput p4, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    aput p5, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    aput p6, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    aput p7, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3038

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;[I)V

    .line 736
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mValue:[I

    .line 737
    iput p2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mRedSize:I

    .line 738
    iput p3, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mGreenSize:I

    .line 739
    iput p4, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mBlueSize:I

    .line 740
    iput p5, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mAlphaSize:I

    .line 741
    iput p6, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mDepthSize:I

    .line 742
    iput p7, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mStencilSize:I

    .line 743
    return-void
.end method

.method private findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 774
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mValue:[I

    invoke-interface {p1, p2, p3, p4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mValue:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 777
    :goto_0
    return v0

    :cond_0
    move v0, p5

    goto :goto_0
.end method


# virtual methods
.method public chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 748
    array-length v6, p3

    move v7, v5

    :goto_0
    if-ge v7, v6, :cond_1

    aget-object v3, p3, v7

    .line 749
    const/16 v4, 0x3025

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    .line 751
    const/16 v4, 0x3026

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    .line 753
    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mDepthSize:I

    if-lt v8, v1, :cond_0

    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mStencilSize:I

    if-lt v0, v1, :cond_0

    .line 754
    const/16 v4, 0x3024

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    .line 756
    const/16 v4, 0x3023

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    .line 758
    const/16 v4, 0x3022

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v10

    .line 760
    const/16 v4, 0x3021

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    .line 762
    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mRedSize:I

    if-ne v8, v1, :cond_0

    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mGreenSize:I

    if-ne v9, v1, :cond_0

    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mBlueSize:I

    if-ne v10, v1, :cond_0

    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;->mAlphaSize:I

    if-ne v0, v1, :cond_0

    move-object v0, v3

    .line 768
    :goto_1
    return-object v0

    .line 748
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 768
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
