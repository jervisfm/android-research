.class Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;
.super Ljava/lang/Object;
.source "GLSurfaceViewForCanvasRenderer.java"

# interfaces
.implements Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultContextFactory"
.end annotation


# instance fields
.field private EGL_CONTEXT_CLIENT_VERSION:I

.field final synthetic this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;


# direct methods
.method private constructor <init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)V
    .locals 1
    .parameter

    .prologue
    .line 619
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620
    const/16 v0, 0x3098

    iput v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;->EGL_CONTEXT_CLIENT_VERSION:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 619
    invoke-direct {p0, p1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)V

    return-void
.end method


# virtual methods
.method public createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 623
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;->EGL_CONTEXT_CLIENT_VERSION:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextClientVersion:I
    invoke-static {v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$200(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x3038

    aput v2, v0, v1

    .line 626
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextClientVersion:I
    invoke-static {v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$200(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)I

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-interface {p1, p2, p3, v1, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public destroyContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 632
    invoke-interface {p1, p2, p3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 633
    const-string v0, "DefaultContextFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "display:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " context: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eglDestroyContext failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 639
    :cond_0
    return-void
.end method
