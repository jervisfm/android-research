.class Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;
.super Ljava/lang/Object;
.source "GLSurfaceViewForCanvasRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EglHelper"
.end annotation


# instance fields
.field mEgl:Ljavax/microedition/khronos/egl/EGL10;

.field mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

.field mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

.field mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

.field final synthetic this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;


# direct methods
.method public constructor <init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)V
    .locals 0
    .parameter

    .prologue
    .line 806
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 808
    return-void
.end method

.method private throwEglException(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 992
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->throwEglException(Ljava/lang/String;I)V

    .line 993
    return-void
.end method

.method private throwEglException(Ljava/lang/String;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 996
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1000
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public createSurface(Landroid/view/SurfaceHolder;)Ljavax/microedition/khronos/opengles/GL;
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 868
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v0, :cond_0

    .line 869
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "egl not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 871
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v0, :cond_1

    .line 872
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglDisplay not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 874
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v0, :cond_2

    .line 875
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mEglConfig not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 881
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_3

    .line 887
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 889
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLWindowSurfaceFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;
    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$500(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2, v3}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;->destroySurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    .line 895
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLWindowSurfaceFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;
    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$500(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;->createWindowSurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 898
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v0, v1, :cond_7

    .line 899
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 900
    const/16 v1, 0x300b

    if-ne v0, v1, :cond_6

    .line 901
    const-string v0, "EglHelper"

    const-string v1, "createWindowSurface returned EGL_BAD_NATIVE_WINDOW."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v5

    .line 931
    :cond_5
    :goto_0
    return-object v0

    .line 904
    :cond_6
    const-string v1, "createWindowSurface"

    invoke-direct {p0, v1, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->throwEglException(Ljava/lang/String;I)V

    .line 911
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 912
    const-string v0, "eglMakeCurrent"

    invoke-direct {p0, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->throwEglException(Ljava/lang/String;)V

    .line 915
    :cond_8
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v0

    .line 916
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLWrapper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;
    invoke-static {v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$600(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 917
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLWrapper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;
    invoke-static {v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$600(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;->wrap(Ljavax/microedition/khronos/opengles/GL;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v0

    .line 920
    :cond_9
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mDebugFlags:I
    invoke-static {v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$700(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)I

    move-result v1

    and-int/lit8 v1, v1, 0x3

    if-eqz v1, :cond_5

    .line 921
    const/4 v1, 0x0

    .line 923
    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mDebugFlags:I
    invoke-static {v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$700(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_a

    .line 924
    or-int/lit8 v1, v1, 0x1

    .line 926
    :cond_a
    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mDebugFlags:I
    invoke-static {v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$700(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_b

    .line 927
    new-instance v2, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$LogWriter;

    invoke-direct {v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$LogWriter;-><init>()V

    .line 929
    :goto_1
    invoke-static {v0, v1, v2}, Landroid/opengl/GLDebugHelper;->wrap(Ljavax/microedition/khronos/opengles/GL;ILjava/io/Writer;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v0

    goto :goto_0

    :cond_b
    move-object v2, v5

    goto :goto_1
.end method

.method public destroySurface()V
    .locals 5

    .prologue
    .line 968
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_0

    .line 969
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 972
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLWindowSurfaceFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;
    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$500(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2, v3}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;->destroySurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    .line 973
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 975
    :cond_0
    return-void
.end method

.method public finish()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 981
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_0

    .line 982
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;
    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$400(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;->destroyContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)V

    .line 983
    iput-object v4, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 985
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v0, :cond_1

    .line 986
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 987
    iput-object v4, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 989
    :cond_1
    return-void
.end method

.method public start()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 821
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    .line 826
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 828
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v0, v1, :cond_0

    .line 829
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglGetDisplay failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 835
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 836
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 837
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglInitialize failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 839
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView$EGLConfigChooser;
    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$300(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1, v2}, Landroid/opengl/GLSurfaceView$EGLConfigChooser;->chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 845
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;
    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$400(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v0, v1, v2, v3}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;->createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 846
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v0, v1, :cond_3

    .line 847
    :cond_2
    iput-object v4, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 848
    const-string v0, "createContext"

    invoke-direct {p0, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->throwEglException(Ljava/lang/String;)V

    .line 854
    :cond_3
    iput-object v4, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 855
    return-void
.end method

.method public swap()Z
    .locals 4

    .prologue
    .line 939
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 948
    packed-switch v0, :pswitch_data_0

    .line 958
    :pswitch_0
    const-string v1, "eglSwapBuffers"

    invoke-direct {p0, v1, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->throwEglException(Ljava/lang/String;I)V

    .line 961
    :cond_0
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 950
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_1

    .line 955
    :pswitch_2
    const-string v0, "EglHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eglSwapBuffers returned EGL_BAD_NATIVE_WINDOW. tid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 948
    nop

    :pswitch_data_0
    .packed-switch 0x300b
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
