.class public Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;
.super Ljava/lang/Thread;
.source "GLSurfaceViewForCanvasRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GLThread"
.end annotation


# instance fields
.field private mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

.field private mEventQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mExited:Z

.field private mHaveEglContext:Z

.field private mHaveEglSurface:Z

.field private mHeight:I

.field private mPaused:Z

.field private mRenderComplete:Z

.field private mRenderMode:I

.field private mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

.field private mRequestPaused:Z

.field private mRequestRender:Z

.field private mShouldExit:Z

.field private mShouldReleaseEglContext:Z

.field private mWaitingForSurface:Z

.field private mWidth:I

.field final synthetic this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;


# direct methods
.method constructor <init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Landroid/opengl/GLSurfaceView$Renderer;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1021
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    .line 1022
    const-string v0, "GLThreadJava"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 1492
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEventQueue:Ljava/util/ArrayList;

    .line 1023
    iput v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWidth:I

    .line 1024
    iput v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHeight:I

    .line 1025
    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestRender:Z

    .line 1026
    iput v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderMode:I

    .line 1027
    #setter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSizeChanged:Z
    invoke-static {p1, v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$802(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)Z

    .line 1028
    iput-object p2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    .line 1029
    return-void
.end method

.method static synthetic access$1102(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1020
    iput-boolean p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mExited:Z

    return p1
.end method

.method private guardedRun()V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1070
    new-instance v2, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    move-object v3, v0

    invoke-direct {v2, v3}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)V

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    .line 1071
    const/4 v2, 0x0

    move v0, v2

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglContext:Z

    .line 1072
    const/4 v2, 0x0

    move v0, v2

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    .line 1074
    const/4 v2, 0x0

    .line 1075
    const/4 v3, 0x0

    .line 1076
    const/4 v4, 0x0

    .line 1077
    const/4 v5, 0x0

    .line 1078
    const/4 v6, 0x0

    .line 1079
    const/4 v7, 0x0

    .line 1080
    const/4 v8, 0x0

    .line 1081
    const/4 v9, 0x0

    .line 1082
    const/4 v10, 0x0

    .line 1083
    const/4 v11, 0x0

    .line 1084
    const/4 v12, 0x0

    move-object/from16 v16, v12

    move-object v12, v2

    move-object/from16 v2, v16

    move/from16 v17, v10

    move v10, v4

    move/from16 v4, v17

    move/from16 v18, v8

    move v8, v6

    move/from16 v6, v18

    move/from16 v19, v5

    move v5, v9

    move/from16 v9, v19

    move/from16 v20, v3

    move v3, v11

    move/from16 v11, v20

    .line 1087
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v13

    monitor-enter v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1089
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mShouldExit:Z

    move v14, v0

    if-eqz v14, :cond_0

    .line 1090
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1301
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1302
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglSurfaceLocked()V

    .line 1303
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglContextLocked()V

    .line 1304
    monitor-exit v2

    .line 1306
    :goto_2
    return-void

    .line 1304
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 1093
    :cond_0
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object v14, v0

    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_1

    .line 1094
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object v2, v0

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    move-object/from16 v16, v2

    move v2, v10

    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move-object/from16 v3, v16

    .line 1244
    :goto_3
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1246
    if-eqz v3, :cond_10

    .line 1247
    :try_start_4
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1248
    const/4 v3, 0x0

    move-object/from16 v16, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v2

    move-object/from16 v2, v16

    .line 1249
    goto :goto_0

    .line 1099
    :cond_1
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mPaused:Z

    move v14, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestPaused:Z

    move v15, v0

    if-eq v14, v15, :cond_2

    .line 1100
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestPaused:Z

    move v14, v0

    move v0, v14

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mPaused:Z

    .line 1101
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    .line 1108
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mShouldReleaseEglContext:Z

    move v14, v0

    if-eqz v14, :cond_3

    .line 1112
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglSurfaceLocked()V

    .line 1113
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglContextLocked()V

    .line 1114
    const/4 v5, 0x0

    move v0, v5

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mShouldReleaseEglContext:Z

    .line 1115
    const/4 v5, 0x1

    .line 1119
    :cond_3
    if-eqz v9, :cond_4

    .line 1120
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglSurfaceLocked()V

    .line 1121
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglContextLocked()V

    .line 1122
    const/4 v9, 0x0

    .line 1126
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    move v14, v0

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mPaused:Z

    move v14, v0

    if-eqz v14, :cond_6

    .line 1130
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglSurfaceLocked()V

    .line 1131
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->shouldReleaseEGLContextWhenPausing()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1132
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglContextLocked()V

    .line 1137
    :cond_5
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->shouldTerminateEGLWhenPausing()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    move-object v14, v0

    invoke-virtual {v14}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->finish()V

    .line 1146
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    move-object v14, v0

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z
    invoke-static {v14}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$1000(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z

    move-result v14

    if-nez v14, :cond_8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWaitingForSurface:Z

    move v14, v0

    if-nez v14, :cond_8

    .line 1150
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    move v14, v0

    if-eqz v14, :cond_7

    .line 1151
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglSurfaceLocked()V

    .line 1153
    :cond_7
    const/4 v14, 0x1

    move v0, v14

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWaitingForSurface:Z

    .line 1154
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    .line 1158
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    move-object v14, v0

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z
    invoke-static {v14}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$1000(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z

    move-result v14

    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWaitingForSurface:Z

    move v14, v0

    if-eqz v14, :cond_9

    .line 1162
    const/4 v14, 0x0

    move v0, v14

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWaitingForSurface:Z

    .line 1163
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    .line 1166
    :cond_9
    if-eqz v6, :cond_a

    .line 1170
    const/4 v6, 0x0

    .line 1171
    const/4 v7, 0x0

    .line 1172
    const/4 v14, 0x1

    move v0, v14

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderComplete:Z

    .line 1173
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    move/from16 v16, v7

    move v7, v6

    move/from16 v6, v16

    .line 1177
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->readyToDraw()Z

    move-result v14

    if-eqz v14, :cond_f

    .line 1180
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglContext:Z

    move v14, v0

    if-nez v14, :cond_b

    .line 1181
    if-eqz v5, :cond_d

    .line 1182
    const/4 v5, 0x0

    .line 1197
    :cond_b
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglContext:Z

    move v14, v0

    if-eqz v14, :cond_c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    move v14, v0

    if-nez v14, :cond_c

    .line 1198
    const/4 v8, 0x1

    move v0, v8

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    .line 1199
    const/4 v8, 0x1

    .line 1200
    const/4 v10, 0x1

    move/from16 v16, v10

    move v10, v8

    move/from16 v8, v16

    .line 1203
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    move v14, v0

    if-eqz v14, :cond_f

    .line 1204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    move-object v14, v0

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSizeChanged:Z
    invoke-static {v14}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$800(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z

    move-result v14

    if-eqz v14, :cond_e

    .line 1205
    const/4 v3, 0x1

    .line 1206
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWidth:I

    move v4, v0

    .line 1207
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHeight:I

    move v7, v0

    .line 1208
    const/4 v8, 0x1

    .line 1220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    move-object v14, v0

    const/4 v15, 0x0

    #setter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSizeChanged:Z
    invoke-static {v14, v15}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$802(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)Z

    move/from16 v16, v7

    move v7, v8

    move v8, v3

    move/from16 v3, v16

    .line 1224
    :goto_5
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    move-object/from16 v16, v2

    move v2, v10

    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move-object/from16 v3, v16

    .line 1225
    goto/16 :goto_3

    .line 1183
    :cond_d
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    move-object v0, v14

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->tryAcquireEglContextLocked(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v14

    if-eqz v14, :cond_b

    .line 1185
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    move-object v11, v0

    invoke-virtual {v11}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->start()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0

    .line 1190
    const/4 v11, 0x1

    :try_start_7
    move v0, v11

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglContext:Z

    .line 1191
    const/4 v11, 0x1

    .line 1193
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    goto :goto_4

    .line 1244
    :catchall_1
    move-exception v2

    monitor-exit v13
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1301
    :catchall_2
    move-exception v2

    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v3

    monitor-enter v3

    .line 1302
    :try_start_9
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglSurfaceLocked()V

    .line 1303
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglContextLocked()V

    .line 1304
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v2

    .line 1186
    :catch_0
    move-exception v2

    .line 1187
    :try_start_a
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v3

    move-object v0, v3

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->releaseEglContextLocked(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V

    .line 1188
    throw v2

    .line 1222
    :cond_e
    const/4 v14, 0x0

    move v0, v14

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestRender:Z

    goto :goto_5

    .line 1242
    :cond_f
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    .line 1252
    :cond_10
    if-eqz v2, :cond_12

    .line 1256
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    move-object v2, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    move-object v12, v0

    invoke-virtual {v12}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->createSurface(Landroid/view/SurfaceHolder;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    check-cast v2, Ljavax/microedition/khronos/opengles/GL10;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 1257
    if-nez v2, :cond_11

    .line 1301
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1302
    :try_start_c
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglSurfaceLocked()V

    .line 1303
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->stopEglContextLocked()V

    .line 1304
    monitor-exit v2

    goto/16 :goto_2

    :catchall_3
    move-exception v3

    monitor-exit v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v3

    .line 1261
    :cond_11
    :try_start_d
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v12

    invoke-virtual {v12, v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1262
    const/4 v12, 0x0

    move/from16 v16, v12

    move-object v12, v2

    move/from16 v2, v16

    .line 1265
    :cond_12
    if-eqz v11, :cond_13

    .line 1269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    move-object v11, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    move-object v13, v0

    iget-object v13, v13, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v11, v12, v13}, Landroid/opengl/GLSurfaceView$Renderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1270
    const/4 v11, 0x0

    .line 1273
    :cond_13
    if-eqz v9, :cond_14

    .line 1277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    move-object v9, v0

    invoke-interface {v9, v12, v5, v4}, Landroid/opengl/GLSurfaceView$Renderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1278
    const/4 v9, 0x0

    .line 1284
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    move-object v13, v0

    invoke-interface {v13, v12}, Landroid/opengl/GLSurfaceView$Renderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    move-object v13, v0

    invoke-virtual {v13}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->swap()Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    move-result v13

    if-nez v13, :cond_15

    .line 1289
    const/4 v10, 0x1

    .line 1292
    :cond_15
    if-eqz v8, :cond_16

    .line 1293
    const/4 v7, 0x1

    move-object/from16 v16, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v2

    move-object/from16 v2, v16

    goto/16 :goto_0

    .line 1304
    :catchall_4
    move-exception v2

    :try_start_e
    monitor-exit v3
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    throw v2

    :cond_16
    move-object/from16 v16, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v2

    move-object/from16 v2, v16

    goto/16 :goto_0
.end method

.method private readyToDraw()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1313
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mPaused:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    #getter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z
    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$1000(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWidth:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHeight:I

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestRender:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderMode:I

    if-ne v0, v1, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private stopEglContextLocked()V
    .locals 1

    .prologue
    .line 1063
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    .line 1064
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->finish()V

    .line 1065
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglContext:Z

    .line 1066
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->releaseEglContextLocked(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V

    .line 1068
    :cond_0
    return-void
.end method

.method private stopEglSurfaceLocked()V
    .locals 1

    .prologue
    .line 1052
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    .line 1053
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    .line 1054
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEglHelper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;->destroySurface()V

    .line 1056
    :cond_0
    return-void
.end method


# virtual methods
.method public ableToDraw()Z
    .locals 1

    .prologue
    .line 1309
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->readyToDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRenderMode()I
    .locals 2

    .prologue
    .line 1329
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1330
    :try_start_0
    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderMode:I

    monitor-exit v0

    return v1

    .line 1331
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1376
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1380
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestPaused:Z

    .line 1381
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1382
    :goto_0
    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1387
    :try_start_1
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1388
    :catch_0
    move-exception v1

    .line 1389
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1392
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1393
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1396
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1400
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestPaused:Z

    .line 1401
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestRender:Z

    .line 1402
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderComplete:Z

    .line 1403
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1404
    :goto_0
    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mPaused:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1409
    :try_start_1
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1410
    :catch_0
    move-exception v1

    .line 1411
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1414
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1415
    return-void
.end method

.method public onWindowResize(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 1418
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1419
    :try_start_0
    iput p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWidth:I

    .line 1420
    iput p2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mHeight:I

    .line 1421
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    const/4 v2, 0x1

    #setter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSizeChanged:Z
    invoke-static {v1, v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$802(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)Z

    .line 1422
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestRender:Z

    .line 1423
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderComplete:Z

    .line 1424
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1428
    :goto_0
    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mPaused:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderComplete:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    iget-object v1, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    iget-object v1, v1, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->ableToDraw()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1433
    :try_start_1
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1434
    :catch_0
    move-exception v1

    .line 1435
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1438
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1439
    return-void
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 2
    .parameter

    .prologue
    .line 1467
    if-nez p1, :cond_0

    .line 1468
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1470
    :cond_0
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1471
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1472
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1473
    monitor-exit v0

    .line 1474
    return-void

    .line 1473
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public requestExitAndWait()V
    .locals 2

    .prologue
    .line 1444
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1445
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mShouldExit:Z

    .line 1446
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1447
    :goto_0
    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1449
    :try_start_1
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1450
    :catch_0
    move-exception v1

    .line 1451
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1454
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1455
    return-void
.end method

.method public requestReleaseEglContextLocked()V
    .locals 1

    .prologue
    .line 1458
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mShouldReleaseEglContext:Z

    .line 1459
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1460
    return-void
.end method

.method public requestRender()V
    .locals 2

    .prologue
    .line 1335
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1336
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRequestRender:Z

    .line 1337
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1338
    monitor-exit v0

    .line 1339
    return-void

    .line 1338
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public run()V
    .locals 3

    .prologue
    .line 1033
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GLThread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->setName(Ljava/lang/String;)V

    .line 1039
    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->guardedRun()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1043
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->threadExiting(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V

    .line 1045
    :goto_0
    return-void

    .line 1040
    :catch_0
    move-exception v0

    .line 1043
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->threadExiting(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->threadExiting(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V

    throw v0
.end method

.method public setRenderMode(I)V
    .locals 2
    .parameter

    .prologue
    .line 1319
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 1320
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1322
    :cond_1
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1323
    :try_start_0
    iput p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mRenderMode:I

    .line 1324
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1325
    monitor-exit v0

    .line 1326
    return-void

    .line 1325
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public surfaceCreated()V
    .locals 3

    .prologue
    .line 1342
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1346
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    const/4 v2, 0x1

    #setter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z
    invoke-static {v1, v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$1002(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)Z

    .line 1347
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1348
    :goto_0
    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWaitingForSurface:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1350
    :try_start_1
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1351
    :catch_0
    move-exception v1

    .line 1352
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1355
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1356
    return-void
.end method

.method public surfaceDestroyed()V
    .locals 3

    .prologue
    .line 1359
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1363
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->this$0:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;

    const/4 v2, 0x0

    #setter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z
    invoke-static {v1, v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$1002(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)Z

    .line 1364
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1365
    :goto_0
    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mWaitingForSurface:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1367
    :try_start_1
    invoke-static {}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1368
    :catch_0
    move-exception v1

    .line 1369
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1372
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1373
    return-void
.end method
