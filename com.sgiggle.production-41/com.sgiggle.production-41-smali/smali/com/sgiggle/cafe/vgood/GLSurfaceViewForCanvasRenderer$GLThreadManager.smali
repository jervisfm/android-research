.class Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
.super Ljava/lang/Object;
.source "GLSurfaceViewForCanvasRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GLThreadManager"
.end annotation


# static fields
.field private static TAG:Ljava/lang/String; = null

.field private static final kGLES_20:I = 0x20000

.field private static final kMSM7K_RENDERER_PREFIX:Ljava/lang/String; = "Q3Dimension MSM7500 "


# instance fields
.field private mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

.field private mGLESDriverCheckComplete:Z

.field private mGLESVersion:I

.field private mGLESVersionCheckComplete:Z

.field private mMultipleGLESContextsAllowed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1541
    const-string v0, "GLThreadManager"

    sput-object v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$1;)V
    .locals 0
    .parameter

    .prologue
    .line 1540
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;-><init>()V

    return-void
.end method

.method private checkGLESVersion()V
    .locals 1

    .prologue
    .line 1622
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mGLESVersionCheckComplete:Z

    if-nez v0, :cond_0

    .line 1633
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mGLESVersionCheckComplete:Z

    .line 1635
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 1605
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mGLESDriverCheckComplete:Z

    if-nez v0, :cond_1

    .line 1606
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->checkGLESVersion()V

    .line 1607
    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mGLESVersion:I

    const/high16 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 1608
    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    .line 1609
    const-string v1, "Q3Dimension MSM7500 "

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    .line 1615
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1617
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mGLESDriverCheckComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1619
    :cond_1
    monitor-exit p0

    return-void

    .line 1609
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1605
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public releaseEglContextLocked(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V
    .locals 1
    .parameter

    .prologue
    .line 1586
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-ne v0, p1, :cond_0

    .line 1587
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    .line 1589
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1590
    return-void
.end method

.method public declared-synchronized shouldReleaseEGLContextWhenPausing()Z
    .locals 1

    .prologue
    .line 1596
    monitor-enter p0

    const/4 v0, 0x1

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized shouldTerminateEGLWhenPausing()Z
    .locals 1

    .prologue
    .line 1600
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->checkGLESVersion()V

    .line 1601
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mMultipleGLESContextsAllowed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1600
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized threadExiting(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)V
    .locals 1
    .parameter

    .prologue
    .line 1547
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    #setter for: Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->mExited:Z
    invoke-static {p1, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->access$1102(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;Z)Z

    .line 1548
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-ne v0, p1, :cond_0

    .line 1549
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    .line 1551
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1552
    monitor-exit p0

    return-void

    .line 1547
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public tryAcquireEglContextLocked(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 1562
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-nez v0, :cond_1

    .line 1563
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    .line 1564
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    move v0, v1

    .line 1578
    :goto_0
    return v0

    .line 1567
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->checkGLESVersion()V

    .line 1568
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 1569
    goto :goto_0

    .line 1575
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v0, :cond_3

    .line 1576
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;->mEglOwner:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->requestReleaseEglContextLocked()V

    .line 1578
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
