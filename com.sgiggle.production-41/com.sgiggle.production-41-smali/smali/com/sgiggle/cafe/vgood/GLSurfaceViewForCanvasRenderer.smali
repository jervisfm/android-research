.class public Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;
.super Landroid/view/SurfaceView;
.source "GLSurfaceViewForCanvasRenderer.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$1;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$LogWriter;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EglHelper;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$SimpleEGLConfigChooser;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$BaseConfigChooser;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultWindowSurfaceFactory;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;,
        Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;
    }
.end annotation


# static fields
.field public static final DEBUG_CHECK_GL_ERROR:I = 0x1

.field public static final DEBUG_LOG_GL_CALLS:I = 0x2

.field private static final DRAW_TWICE_AFTER_SIZE_CHANGED:Z = true

.field private static final LOG_EGL:Z = false

.field private static final LOG_PAUSE_RESUME:Z = false

.field private static final LOG_RENDERER:Z = false

.field private static final LOG_RENDERER_DRAW_FRAME:Z = false

.field private static final LOG_SURFACE:Z = false

.field private static final LOG_THREADS:Z = false

.field public static final RENDERMODE_CONTINUOUSLY:I = 0x1

.field public static final RENDERMODE_WHEN_DIRTY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GLSurfaceView"

.field private static final sGLThreadManager:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;


# instance fields
.field private mDebugFlags:I

.field private mEGLConfigChooser:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

.field private mEGLContextClientVersion:I

.field private mEGLContextFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;

.field private mEGLWindowSurfaceFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;

.field public mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

.field private mGLWrapper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;

.field private mHasSurface:Z

.field private mRenderMode:I

.field private mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

.field private mSizeChanged:Z

.field private mSurfaceHeight:I

.field private mSurfaceWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1647
    new-instance v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$1;)V

    sput-object v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->sGLThreadManager:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 207
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 1648
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSizeChanged:Z

    .line 208
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->init()V

    .line 209
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 216
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1648
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSizeChanged:Z

    .line 217
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->init()V

    .line 218
    return-void
.end method

.method static synthetic access$1000(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z
    .locals 1
    .parameter

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 156
    iput-boolean p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)I
    .locals 1
    .parameter

    .prologue
    .line 156
    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextClientVersion:I

    return v0
.end method

.method static synthetic access$300(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Landroid/opengl/GLSurfaceView$EGLConfigChooser;
    .locals 1
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;
    .locals 1
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;
    .locals 1
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLWindowSurfaceFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;
    .locals 1
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLWrapper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)I
    .locals 1
    .parameter

    .prologue
    .line 156
    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mDebugFlags:I

    return v0
.end method

.method static synthetic access$800(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;)Z
    .locals 1
    .parameter

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSizeChanged:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 156
    iput-boolean p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSizeChanged:Z

    return p1
.end method

.method static synthetic access$900()Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->sGLThreadManager:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThreadManager;

    return-object v0
.end method

.method private checkRenderThreadState()V
    .locals 2

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v0, :cond_0

    .line 1535
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1538
    :cond_0
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 224
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 232
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 233
    const/4 v0, 0x1

    iput v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mRenderMode:I

    .line 234
    return-void
.end method


# virtual methods
.method public getDebugFlags()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mDebugFlags:I

    return v0
.end method

.method public getRenderMode()I
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mRenderMode:I

    return v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 1665
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->onPause()V

    .line 530
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->requestExitAndWait()V

    .line 531
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    .line 532
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    if-nez v0, :cond_0

    .line 541
    new-instance v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$SimpleEGLConfigChooser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$SimpleEGLConfigChooser;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->interrupt()V

    .line 550
    :cond_1
    new-instance v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Landroid/opengl/GLSurfaceView$Renderer;)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    .line 551
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->start()V

    .line 553
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mRenderMode:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->setRenderMode(I)V

    .line 554
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z

    if-eqz v0, :cond_2

    .line 555
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->surfaceCreated()V

    .line 557
    :cond_2
    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSurfaceWidth:I

    if-lez v0, :cond_3

    iget v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSurfaceHeight:I

    if-lez v0, :cond_3

    .line 558
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    iget v1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSurfaceWidth:I

    iget v2, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSurfaceHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->onWindowResize(II)V

    .line 560
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->onResume()V

    .line 561
    return-void
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 1
    .parameter

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->requestExitAndWait()V

    .line 571
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0, p1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->queueEvent(Ljava/lang/Runnable;)V

    .line 574
    :cond_0
    return-void
.end method

.method public requestRender()V
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->requestRender()V

    .line 484
    return-void
.end method

.method public setDebugFlags(I)V
    .locals 0
    .parameter

    .prologue
    .line 263
    iput p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mDebugFlags:I

    .line 264
    return-void
.end method

.method public setEGLConfigChooser(IIIIII)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 403
    new-instance v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$ComponentSizeChooser;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;IIIIII)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    .line 405
    return-void
.end method

.method public setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V
    .locals 2
    .parameter

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->checkRenderThreadState()V

    .line 364
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    .line 365
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    if-eqz v0, :cond_0

    .line 366
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 368
    :cond_0
    return-void
.end method

.method public setEGLConfigChooser(Z)V
    .locals 1
    .parameter

    .prologue
    .line 385
    new-instance v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$SimpleEGLConfigChooser;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$SimpleEGLConfigChooser;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    .line 386
    return-void
.end method

.method public setEGLContextClientVersion(I)V
    .locals 0
    .parameter

    .prologue
    .line 434
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->checkRenderThreadState()V

    .line 435
    iput p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextClientVersion:I

    .line 436
    return-void
.end method

.method public setEGLContextFactory(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;)V
    .locals 0
    .parameter

    .prologue
    .line 332
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->checkRenderThreadState()V

    .line 333
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;

    .line 334
    return-void
.end method

.method public setEGLWindowSurfaceFactory(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;)V
    .locals 0
    .parameter

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->checkRenderThreadState()V

    .line 347
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLWindowSurfaceFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;

    .line 348
    return-void
.end method

.method public setGLWrapper(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;)V
    .locals 0
    .parameter

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLWrapper:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLWrapper;

    .line 251
    return-void
.end method

.method public setRenderMode(I)V
    .locals 2
    .parameter

    .prologue
    .line 455
    iput p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mRenderMode:I

    .line 456
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0, p1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->setRenderMode(I)V

    .line 461
    :goto_0
    return-void

    .line 459
    :cond_0
    const-string v0, "GLSurfaceView"

    const-string v1, "NO mGLThread Running yet"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 301
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->checkRenderThreadState()V

    .line 302
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    if-nez v0, :cond_0

    .line 303
    new-instance v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$SimpleEGLConfigChooser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$SimpleEGLConfigChooser;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Z)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;

    if-nez v0, :cond_1

    .line 306
    new-instance v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;

    invoke-direct {v0, p0, v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultContextFactory;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$1;)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLContextFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLContextFactory;

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLWindowSurfaceFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;

    if-nez v0, :cond_2

    .line 309
    new-instance v0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultWindowSurfaceFactory;

    invoke-direct {v0, v2}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$DefaultWindowSurfaceFactory;-><init>(Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$1;)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mEGLWindowSurfaceFactory:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$EGLWindowSurfaceFactory;

    .line 312
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    if-eqz v0, :cond_3

    .line 313
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_3
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    .line 319
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 515
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0, p3, p4}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->onWindowResize(II)V

    .line 518
    :cond_0
    iput p3, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSurfaceWidth:I

    .line 519
    iput p4, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mSurfaceHeight:I

    .line 520
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .parameter

    .prologue
    .line 491
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->surfaceCreated()V

    .line 494
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z

    .line 495
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .parameter

    .prologue
    .line 503
    const-string v0, "GLSurfaceView"

    const-string v1, "surface get destroyed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mGLThread:Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer$GLThread;->surfaceDestroyed()V

    .line 507
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/GLSurfaceViewForCanvasRenderer;->mHasSurface:Z

    .line 508
    return-void
.end method
