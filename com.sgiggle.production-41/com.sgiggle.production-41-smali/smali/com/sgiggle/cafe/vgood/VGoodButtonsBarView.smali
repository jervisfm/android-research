.class public Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;
.super Landroid/widget/LinearLayout;
.source "VGoodButtonsBarView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;
    }
.end annotation


# static fields
.field public static final STATE_DISABLE:I = 0x1

.field public static final STATE_NORMAL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VGoodButtonsBarView"


# instance fields
.field private allData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end field

.field private currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

.field private mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

.field private mCurrentShowPos:I

.field private mHorizontalListview:Lcom/sgiggle/production/widget/HorizontalListView;

.field private mList:Landroid/widget/ListView;

.field private mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

.field private mSelector:Landroid/widget/AdapterView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/AdapterView",
            "<",
            "Landroid/widget/Adapter;",
            ">;"
        }
    .end annotation
.end field

.field private scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

.field private showingCurrentPlayButton:Z


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/VideoTwoWayActivity;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const v3, 0x7f0a003e

    const/4 v2, 0x1

    .line 57
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mList:Landroid/widget/ListView;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mCurrentShowPos:I

    .line 45
    iput-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showingCurrentPlayButton:Z

    .line 58
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

    .line 60
    new-instance v0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    .line 61
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEmptyCount(I)V

    .line 62
    new-instance v0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$1;-><init>(Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

    .line 74
    if-nez p2, :cond_0

    .line 75
    const-string v0, "VGoodButtonsBarView"

    const-string v1, "orientation is landscape"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03005e

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 77
    invoke-virtual {p0, v3}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mList:Landroid/widget/ListView;

    .line 78
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 79
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mList:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mSelector:Landroid/widget/AdapterView;

    .line 80
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setReversed(Z)V

    .line 81
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setRotated(Z)V

    .line 82
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mList:Landroid/widget/ListView;

    new-instance v1, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$2;-><init>(Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 115
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mSelector:Landroid/widget/AdapterView;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 116
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mSelector:Landroid/widget/AdapterView;

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 118
    return-void

    .line 108
    :cond_0
    const-string v0, "VGoodButtonsBarView"

    const-string v1, "orientation is portrait"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03005d

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 110
    invoke-virtual {p0, v3}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/HorizontalListView;

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mHorizontalListview:Lcom/sgiggle/production/widget/HorizontalListView;

    .line 111
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mHorizontalListview:Lcom/sgiggle/production/widget/HorizontalListView;

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mSelector:Landroid/widget/AdapterView;

    .line 112
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mHorizontalListview:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->setScrollListener(Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;)Lcom/sgiggle/production/VideoTwoWayActivity;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;)Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

    return-object v0
.end method

.method private addAvatar(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V
    .locals 3
    .parameter

    .prologue
    .line 317
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AddAvatarMessage;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$AddAvatarMessage;-><init>(J)V

    .line 318
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 319
    return-void
.end method

.method private bundleWithSameAssetId(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 341
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->hasCinematic()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->hasCinematic()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private findFilterPositionByIndex(J)I
    .locals 4
    .parameter

    .prologue
    .line 210
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mSelector:Landroid/widget/AdapterView;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 212
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    move v0, v1

    .line 216
    :goto_1
    return v0

    .line 210
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 216
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private onAvatarClicked(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V
    .locals 1
    .parameter

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-direct {p0, v0, p1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->bundleWithSameAssetId(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    invoke-direct {p0, p1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->removeAvatar(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V

    .line 314
    :goto_0
    return-void

    .line 312
    :cond_0
    invoke-direct {p0, p1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->addAvatar(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V

    goto :goto_0
.end method

.method private playVGood(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V
    .locals 3
    .parameter

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->restartControlBarTimer()V

    .line 328
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v0

    .line 329
    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$InitiateVGoodMessage;

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InitiateVGoodMessage;-><init>(Ljava/lang/Integer;)V

    .line 330
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 331
    return-void
.end method

.method private removeAvatar(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V
    .locals 3
    .parameter

    .prologue
    .line 322
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RemoveAvatarMessage;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$RemoveAvatarMessage;-><init>(J)V

    .line 323
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 324
    return-void
.end method

.method private showBubbleForLockedVgood()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 334
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

    const v2, 0x7f090116

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

    const v4, 0x7f090117

    invoke-virtual {v2, v4}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 337
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$LockedVGoodSelectedMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$LockedVGoodSelectedMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 338
    return-void
.end method


# virtual methods
.method public enableButtonByTypes(Ljava/util/List;Z)V
    .locals 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 248
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->allData:Ljava/util/List;

    if-nez v0, :cond_1

    .line 249
    const-string v0, "VGoodButtonsBarView"

    const-string v1, "enableButtonByTypes: setData has never been called"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move v2, v6

    move v3, v6

    move v4, v7

    .line 255
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->allData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 256
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->allData:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 257
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 258
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    iget-object v5, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-direct {p0, v5, v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->bundleWithSameAssetId(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v3

    .line 262
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v8, v3

    move v3, v0

    move v0, v8

    .line 255
    :goto_3
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    move v3, v0

    goto :goto_1

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v1, p2}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setData(Ljava/util/List;Z)V

    .line 266
    if-eq v4, v7, :cond_3

    .line 267
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v4}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setHightlightWithBackground(I)V

    .line 269
    :cond_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 270
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v6}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setShowEmptySlots(Z)V

    .line 274
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mHorizontalListview:Lcom/sgiggle/production/widget/HorizontalListView;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mHorizontalListview:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->reset()V

    goto :goto_0

    :cond_5
    move v0, v4

    goto :goto_2

    :cond_6
    move v0, v3

    move v3, v4

    goto :goto_3
.end method

.method public findBundleById(J)V
    .locals 4
    .parameter

    .prologue
    .line 193
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mCurrentShowPos:I

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 196
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mSelector:Landroid/widget/AdapterView;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 198
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->AVATAR:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    if-ne v2, v3, :cond_1

    .line 199
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 200
    iput v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mCurrentShowPos:I

    .line 201
    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 206
    :cond_0
    return-void

    .line 196
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public getButtonState()I
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hideCurrentPlayingAnimation()V
    .locals 3

    .prologue
    .line 298
    const-string v0, "VGoodButtonsBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hideCurrentPlayingAnimation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showingCurrentPlayButton:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showingCurrentPlayButton:Z

    if-nez v0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->clearHightlightWithBackground()V

    .line 303
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 304
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mCurrentShowPos:I

    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showingCurrentPlayButton:Z

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p2}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 160
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->hasControlBarAnimation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    if-ne v1, v2, :cond_0

    .line 166
    :cond_2
    const-string v1, "VGoodButtonsBarView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemClick:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isItemEnabled:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v3, p3}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isItemEnabled(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v1, p3}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isItemEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    if-eqz v0, :cond_5

    .line 171
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->AVATAR:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    if-ne v1, v2, :cond_3

    .line 172
    invoke-direct {p0, v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->onAvatarClicked(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V

    .line 178
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, p3}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setHighlight(I)V

    goto :goto_0

    .line 173
    :cond_3
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    if-ne v1, v2, :cond_4

    .line 174
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mParent:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v2

    long-to-int v0, v2

    invoke-virtual {v1, v0, p3}, Lcom/sgiggle/production/VideoTwoWayActivity;->setFilter(II)V

    goto :goto_1

    .line 176
    :cond_4
    invoke-direct {p0, v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->playVGood(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V

    goto :goto_1

    .line 180
    :cond_5
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showBubbleForLockedVgood()V

    goto/16 :goto_0
.end method

.method public reenableAllButtons(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 280
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->allData:Ljava/util/List;

    if-nez v0, :cond_1

    .line 281
    const-string v0, "VGoodButtonsBarView"

    const-string v1, "reenableAllButtons: setData has never been called"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->allData:Ljava/util/List;

    invoke-virtual {v0, v1, p1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setData(Ljava/util/List;Z)V

    .line 285
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setShowEmptySlots(Z)V

    .line 288
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mHorizontalListview:Lcom/sgiggle/production/widget/HorizontalListView;

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0
.end method

.method public refreshCurrentPlayingAnimation(J)V
    .locals 3
    .parameter

    .prologue
    .line 185
    const-string v0, "VGoodButtonsBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "refreshCurrentPlayingAnimation:currentShowPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mCurrentShowPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showingCurrentPlayButton:Z

    if-nez v0, :cond_0

    .line 190
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->hideCurrentPlayingAnimation()V

    .line 189
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showCurrentPlayingAnimation(J)V

    goto :goto_0
.end method

.method public setData(Ljava/util/List;Z)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 121
    if-nez p1, :cond_0

    .line 128
    :goto_0
    return-void

    .line 124
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->allData:Ljava/util/List;

    .line 125
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setData(Ljava/util/List;Z)V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mCurrentShowPos:I

    goto :goto_0
.end method

.method public setEmptySlots(I)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x4

    .line 147
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    if-le p1, v1, :cond_0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEmptyCount(I)V

    .line 148
    return-void

    :cond_0
    move v1, p1

    .line 147
    goto :goto_0
.end method

.method public setFilterHightlightWithBackground(I)V
    .locals 4
    .parameter

    .prologue
    .line 220
    int-to-long v0, p1

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->findFilterPositionByIndex(J)I

    move-result v0

    .line 221
    const-string v1, "VGoodButtonsBarView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFilterHightlightWithBackground filterIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 223
    iget-object v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setHightlightWithBackground(I)V

    .line 225
    :cond_0
    return-void
.end method

.method public setHightlightWithBackground(I)V
    .locals 1
    .parameter

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setHightlightWithBackground(I)V

    .line 295
    return-void
.end method

.method public showCurrentPlayingAnimation(J)V
    .locals 3
    .parameter

    .prologue
    .line 228
    const-string v0, "VGoodButtonsBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showCurrentPlayingAnimation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showingCurrentPlayButton:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showingCurrentPlayButton:Z

    if-eqz v0, :cond_0

    .line 245
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->findBundleById(J)V

    .line 235
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->currentShowBundle:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    if-nez v0, :cond_1

    .line 236
    const-string v0, "VGoodButtonsBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can not find avatar in selector "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 240
    :cond_1
    const-string v0, "VGoodButtonsBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showCurrentPlayingAnimation:currentShowPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mCurrentShowPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->clearHighlight()V

    .line 243
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    iget v1, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mCurrentShowPos:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setHightlightWithBackground(I)V

    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showingCurrentPlayButton:Z

    goto :goto_0
.end method

.method public updateButtons(I)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    if-nez p1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEnabled(Z)V

    .line 133
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->clearHighlight()V

    .line 134
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mSelector:Landroid/widget/AdapterView;

    invoke-virtual {v0, v2}, Landroid/widget/AdapterView;->setEnabled(Z)V

    .line 139
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mAdapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEnabled(Z)V

    .line 137
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->mSelector:Landroid/widget/AdapterView;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setEnabled(Z)V

    goto :goto_0
.end method
