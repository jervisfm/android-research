.class public Lcom/sgiggle/cafe/vgood/VGoodRenderer;
.super Ljava/lang/Object;
.source "VGoodRenderer.java"

# interfaces
.implements Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;


# static fields
.field private static final TAG:Ljava/lang/String; = "VGOOD_RENDERER"


# instance fields
.field private mAnimationEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

.field private mGLRendererMode:Z

.field private mSurfaceReady:Z

.field private volatile mSurpriseId:I


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mAnimationEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mSurpriseId:I

    .line 17
    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mSurfaceReady:Z

    .line 18
    iput-boolean v1, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mGLRendererMode:Z

    .line 21
    iput-boolean p1, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mGLRendererMode:Z

    .line 22
    return-void
.end method

.method private startAnimation()V
    .locals 6

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mGLRendererMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mSurfaceReady:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mAnimationEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    if-eqz v0, :cond_1

    .line 61
    const-string v0, "VGOOD_RENDERER"

    const-string v1, "startAnimation"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Pause()V

    .line 64
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Resume()V

    .line 65
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mAnimationEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getAssetId()J

    move-result-wide v3

    .line 66
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mAnimationEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getSeed()I

    move-result v0

    int-to-long v1, v0

    .line 67
    iget-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mAnimationEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getAssetPath()Ljava/lang/String;

    move-result-object v0

    .line 68
    const-string v5, "Surprise.Cafe"

    invoke-static {v0, v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->LoadSurprise(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StartSurprise(Ljava/lang/String;JJZ)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mSurpriseId:I

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mAnimationEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    .line 72
    :cond_1
    return-void
.end method


# virtual methods
.method public handleDisplayAnimationEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 52
    const-string v0, "VGOOD_RENDERER"

    const-string v1, "handleDisplayAnimationEvent"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iput-object p1, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mAnimationEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    .line 54
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->startAnimation()V

    .line 55
    return-void
.end method

.method public onSurfaceChanged(IIZZ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    const-string v0, "VGOOD_RENDERER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged w "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    iget-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mGLRendererMode:Z

    if-eqz v0, :cond_0

    .line 33
    const-string v0, "VGOOD_RENDERER"

    const-string v1, "GL2.0 should not call onSurfaceChanged "

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-static {p1, p2, p3, p4}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->setCafeMainView(IIZZ)V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mSurfaceReady:Z

    .line 42
    invoke-direct {p0}, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->startAnimation()V

    goto :goto_0
.end method

.method public onSurfaceCreated()V
    .locals 2

    .prologue
    .line 26
    const-string v0, "VGOOD_RENDERER"

    const-string v1, "onSurfaceCreated"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    return-void
.end method

.method public render()V
    .locals 0

    .prologue
    .line 48
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->RenderMain()V

    .line 49
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mSurpriseId:I

    if-lez v0, :cond_0

    .line 76
    iget v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mSurpriseId:I

    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopSurprise(I)V

    .line 77
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Pause()V

    .line 79
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->mSurpriseId:I

    .line 80
    return-void
.end method
