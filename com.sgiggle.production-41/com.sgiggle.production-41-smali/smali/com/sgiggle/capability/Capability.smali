.class public Lcom/sgiggle/capability/Capability;
.super Ljava/lang/Object;
.source "Capability.java"


# static fields
.field public static final FEATURE:I = 0x0

.field public static final HARDWARD:I = 0x1

.field public static final SOFTWARE:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static native getBool(ILjava/lang/String;)Z
.end method

.method public static native getInt(ILjava/lang/String;)I
.end method

.method public static native getLong(ILjava/lang/String;)J
.end method

.method public static native getString(ILjava/lang/String;)Ljava/lang/String;
.end method

.method public static native keys(I)[Ljava/lang/String;
.end method

.method public static native reset(ILjava/lang/String;)V
.end method

.method public static native set(ILjava/lang/String;I)V
.end method

.method public static native set(ILjava/lang/String;J)V
.end method

.method public static native set(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public static native set(ILjava/lang/String;Z)V
.end method
