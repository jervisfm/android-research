.class public Lcom/sgiggle/contacts/Contact;
.super Ljava/lang/Object;
.source "Contact.java"


# instance fields
.field public deviceContactId:J

.field public displayName:Ljava/lang/String;

.field public emailAddresses:[Ljava/lang/String;

.field public firstName:Ljava/lang/String;

.field public hasPicture:Z

.field public lastName:Ljava/lang/String;

.field public middleName:Ljava/lang/String;

.field public namePrefix:Ljava/lang/String;

.field public nameSuffix:Ljava/lang/String;

.field public nativeFavorite:Z

.field public phoneTypes:[I

.field public subscriberNumbers:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    .line 6
    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    const-string v6, ""

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/contacts/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sgiggle/contacts/Contact;->deviceContactId:J

    .line 11
    iput-object p1, p0, Lcom/sgiggle/contacts/Contact;->namePrefix:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/sgiggle/contacts/Contact;->firstName:Ljava/lang/String;

    .line 13
    iput-object p3, p0, Lcom/sgiggle/contacts/Contact;->middleName:Ljava/lang/String;

    .line 14
    iput-object p4, p0, Lcom/sgiggle/contacts/Contact;->lastName:Ljava/lang/String;

    .line 15
    iput-object p5, p0, Lcom/sgiggle/contacts/Contact;->nameSuffix:Ljava/lang/String;

    .line 16
    iput-object p6, p0, Lcom/sgiggle/contacts/Contact;->displayName:Ljava/lang/String;

    .line 17
    return-void
.end method
