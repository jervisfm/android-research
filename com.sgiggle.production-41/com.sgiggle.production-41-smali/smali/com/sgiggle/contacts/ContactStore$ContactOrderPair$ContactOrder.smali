.class public final enum Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
.super Ljava/lang/Enum;
.source "ContactStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContactOrder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

.field public static final enum ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

.field public static final enum PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;


# instance fields
.field private m_value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 722
    new-instance v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    const-string v1, "PRIMARY"

    invoke-direct {v0, v1, v3, v2}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 723
    new-instance v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    const-string v1, "ALTERNATIVE"

    invoke-direct {v0, v1, v2, v4}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 721
    new-array v0, v4, [Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    sget-object v1, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->$VALUES:[Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 726
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 727
    iput p3, p0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->m_value:I

    .line 728
    return-void
.end method

.method public static fromInt(I)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    .locals 1
    .parameter

    .prologue
    .line 735
    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    .locals 1
    .parameter

    .prologue
    .line 721
    const-class v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    .locals 1

    .prologue
    .line 721
    sget-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->$VALUES:[Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    invoke-virtual {v0}, [Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 731
    iget v0, p0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->m_value:I

    return v0
.end method
