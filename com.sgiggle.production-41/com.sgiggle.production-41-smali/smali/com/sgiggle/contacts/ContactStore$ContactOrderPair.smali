.class public Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;
.super Ljava/lang/Object;
.source "ContactStore.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/contacts/ContactStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContactOrderPair"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private m_displayOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

.field private m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;


# direct methods
.method private constructor <init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742
    sget-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    iput-object v0, p0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 747
    sget-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    iput-object v0, p0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->m_displayOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 750
    iput-object p1, p0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 751
    iput-object p2, p0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->m_displayOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 752
    return-void
.end method

.method public static getDefault()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;
    .locals 3

    .prologue
    .line 767
    new-instance v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    sget-object v1, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    sget-object v2, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V

    return-object v0
.end method

.method public static getFromPhone(Landroid/content/ContentResolver;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;
    .locals 9
    .parameter

    .prologue
    .line 787
    sget-object v6, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 789
    sget-object v7, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 791
    const-string v0, "HTC"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Nexus One"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 792
    const-string v0, "content://com.android.contacts/contacts/display/sort/order/query"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 794
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "sort_order"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "display_order"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 801
    if-eqz v0, :cond_6

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 804
    const-string v1, "Last Name"

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 805
    :goto_0
    :try_start_1
    const-string v2, "Last Name"

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    .line 808
    :goto_2
    if-eqz v0, :cond_0

    .line 809
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    move-object v0, v1

    move-object v1, v2

    .line 821
    :goto_3
    const-string v2, "Tango.ContactStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sort order is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    if-ne v1, v4, :cond_4

    const-string v4, "FIRST NAME"

    :goto_4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    const-string v2, "Tango.ContactStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Display order is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    if-ne v0, v4, :cond_5

    const-string v4, "FIRST NAME"

    :goto_5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    new-instance v2, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    invoke-direct {v2, v1, v0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V

    return-object v2

    .line 804
    :cond_1
    :try_start_3
    sget-object v1, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 805
    :cond_2
    :try_start_4
    sget-object v2, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 812
    :catch_0
    move-exception v0

    move-object v0, v7

    move-object v1, v6

    .line 814
    :goto_6
    const-string v2, "Tango.ContactStore"

    const-string v3, "Sort and Display order are not supported on this device. Using defaults."

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 817
    :cond_3
    const-string v0, "android.contacts.SORT_ORDER"

    sget-object v1, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    invoke-virtual {v1}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->getValue()I

    move-result v1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->fromInt(I)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v0

    .line 818
    const-string v1, "android.contacts.DISPLAY_ORDER"

    sget-object v2, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    invoke-virtual {v2}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->getValue()I

    move-result v2

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->fromInt(I)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    .line 821
    :cond_4
    const-string v4, "LAST NAME"

    goto :goto_4

    .line 822
    :cond_5
    const-string v4, "LAST NAME"

    goto :goto_5

    .line 812
    :catch_1
    move-exception v0

    move-object v0, v7

    goto :goto_6

    :catch_2
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_6

    :cond_6
    move-object v1, v7

    move-object v2, v6

    goto/16 :goto_2
.end method

.method public static getFromPhone(Landroid/content/Context;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;
    .locals 1
    .parameter

    .prologue
    .line 776
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/ContentResolver;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDisplayOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->m_displayOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    return-object v0
.end method

.method public getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    return-object v0
.end method
