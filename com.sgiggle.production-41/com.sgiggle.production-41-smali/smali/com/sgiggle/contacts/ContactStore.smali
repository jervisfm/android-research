.class public Lcom/sgiggle/contacts/ContactStore;
.super Ljava/lang/Object;
.source "ContactStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;
    }
.end annotation


# static fields
.field private static final PHONE_TYPE_GENERIC:I = 0x0

.field private static final PHONE_TYPE_HOME:I = 0x2

.field private static final PHONE_TYPE_MAIN:I = 0x4

.field private static final PHONE_TYPE_MOBILE:I = 0x1

.field private static final PHONE_TYPE_WORK:I = 0x3

.field private static final TAG:Ljava/lang/String; = "Tango.ContactStore"

.field private static s_contentResolver:Landroid/content/ContentResolver;

.field private static s_rawContactIdsFromTangoSync:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sgiggle/contacts/ContactStore;->s_rawContactIdsFromTangoSync:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 717
    return-void
.end method

.method private static addSubscriberNumbersToContact(Ljava/lang/String;Lcom/sgiggle/contacts/Contact;)V
    .locals 10
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 59
    const-string v0, "Tango.ContactStore"

    const-string v1, "addSubscriberNumbersToContact called"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "data1"

    aput-object v3, v2, v8

    const-string v3, "data2"

    aput-object v3, v2, v9

    const-string v3, "contact_id=? AND mimetype=\'vnd.android.cursor.item/phone_v2\'"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p0, v4, v8

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 67
    if-nez v0, :cond_0

    .line 68
    const-string v0, "Tango.ContactStore"

    const-string v1, "addSubscriberNumbersToContact(): ContentResolver failed to query."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 74
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 75
    new-array v2, v1, [Ljava/lang/String;

    .line 76
    new-array v3, v1, [I

    move v4, v8

    .line 77
    :goto_1
    if-ge v4, v1, :cond_1

    .line 78
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 79
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/sgiggle/contacts/ContactStore;->translatePhoneType(I)I

    move-result v5

    aput v5, v3, v4

    .line 80
    const-string v5, "Tango.ContactStore"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "number = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v3, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 77
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    move-object v1, v3

    .line 85
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 87
    iput-object v2, p1, Lcom/sgiggle/contacts/Contact;->subscriberNumbers:[Ljava/lang/String;

    .line 88
    iput-object v1, p1, Lcom/sgiggle/contacts/Contact;->phoneTypes:[I

    goto :goto_0

    :cond_2
    move-object v1, v5

    move-object v2, v5

    goto :goto_2
.end method

.method private static buildContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sgiggle/contacts/Contact;
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 130
    const-string v0, "Tango.ContactStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "buildContact: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    new-instance v0, Lcom/sgiggle/contacts/Contact;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/contacts/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-static {p0, v0}, Lcom/sgiggle/contacts/ContactStore;->addSubscriberNumbersToContact(Ljava/lang/String;Lcom/sgiggle/contacts/Contact;)V

    .line 133
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore;->getEmailAddresses(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sgiggle/contacts/Contact;->emailAddresses:[Ljava/lang/String;

    .line 134
    invoke-static {p0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/sgiggle/contacts/Contact;->deviceContactId:J

    .line 135
    iget-wide v1, v0, Lcom/sgiggle/contacts/Contact;->deviceContactId:J

    invoke-static {v1, v2}, Lcom/sgiggle/contacts/ContactStore;->hasPicture(J)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sgiggle/contacts/Contact;->hasPicture:Z

    .line 136
    iget-wide v1, v0, Lcom/sgiggle/contacts/Contact;->deviceContactId:J

    invoke-static {v1, v2}, Lcom/sgiggle/contacts/ContactStore;->nativeFavorite(J)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sgiggle/contacts/Contact;->nativeFavorite:Z

    .line 138
    return-object v0
.end method

.method private static checkThumbnails4Pictures(Landroid/content/ContentResolver;Ljava/util/HashMap;)V
    .locals 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sgiggle/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 360
    const-string v0, "Tango.ContactStore"

    const-string v1, "checkThumbnails4Pictures()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v0, "photo_id"

    aput-object v0, v2, v7

    .line 362
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "PHOTO_ID is not null"

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 363
    if-eqz v1, :cond_2

    .line 364
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/contacts/Contact;

    .line 366
    if-eqz v0, :cond_0

    .line 368
    iput-boolean v7, v0, Lcom/sgiggle/contacts/Contact;->hasPicture:Z

    goto :goto_0

    .line 371
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 373
    :cond_2
    return-void
.end method

.method public static getAllContacts()[Lcom/sgiggle/contacts/Contact;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 338
    const-string v0, "Tango.ContactStore"

    const-string v1, "getAllContacts()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 341
    const-string v0, "Tango.ContactStore"

    const-string v1, "getAllContacts(): s_contentResolver is null. Return NULL."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v4

    .line 353
    :goto_0
    return-object v0

    .line 345
    :cond_0
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/sgiggle/contacts/ContactStore;->loadInitialContactIds(Landroid/content/ContentResolver;)Ljava/util/HashMap;

    move-result-object v0

    .line 346
    sget-object v1, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v0}, Lcom/sgiggle/contacts/ContactStore;->loadAllNames(Landroid/content/ContentResolver;Ljava/util/HashMap;)V

    .line 347
    sget-object v1, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v0}, Lcom/sgiggle/contacts/ContactStore;->loadAllPhoneNumbers(Landroid/content/ContentResolver;Ljava/util/HashMap;)V

    .line 348
    sget-object v1, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v0}, Lcom/sgiggle/contacts/ContactStore;->loadAllEmails(Landroid/content/ContentResolver;Ljava/util/HashMap;)V

    .line 349
    sget-object v1, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v0}, Lcom/sgiggle/contacts/ContactStore;->checkThumbnails4Pictures(Landroid/content/ContentResolver;Ljava/util/HashMap;)V

    .line 350
    sget-object v1, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v0}, Lcom/sgiggle/contacts/ContactStore;->getNativeFavoriteContacts(Landroid/content/ContentResolver;Ljava/util/HashMap;)V

    .line 352
    const-string v1, "Tango.ContactStore"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAllContacts(): loaded count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    new-array v0, v0, [Lcom/sgiggle/contacts/Contact;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/contacts/Contact;

    goto :goto_0

    :cond_1
    move-object v0, v4

    goto :goto_0
.end method

.method private static getAlternativeDisplayName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 629
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 631
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 633
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 635
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 637
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 639
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 641
    :cond_1
    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 642
    const/4 v1, 0x1

    .line 645
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 647
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 649
    if-eqz v1, :cond_2

    .line 651
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move v1, v3

    .line 654
    :cond_2
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 656
    :cond_3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 659
    :cond_4
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 661
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_6

    .line 663
    if-eqz v1, :cond_5

    .line 665
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 667
    :cond_5
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 669
    :cond_6
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 672
    :cond_7
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 674
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 676
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 678
    :cond_8
    invoke-virtual {v0, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 681
    :cond_9
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_a
    move v1, v3

    goto :goto_0
.end method

.method public static getContactByNumber(Ljava/lang/String;)Lcom/sgiggle/contacts/Contact;
    .locals 11
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 148
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 149
    const-string v0, "Tango.ContactStore"

    const-string v1, "getContactByNumber(): s_contentResolver is null. Return NULL."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 239
    :goto_0
    return-object v0

    .line 153
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v3

    .line 154
    goto :goto_0

    .line 157
    :cond_2
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 160
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v6

    const-string v4, "display_name"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 164
    if-nez v0, :cond_3

    .line 165
    const-string v0, "Tango.ContactStore"

    const-string v1, "getContactByNumber(): ContentResolver failed to query."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 166
    goto :goto_0

    .line 171
    :cond_3
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 172
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 173
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 174
    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    const-string v9, ""

    invoke-static/range {v4 .. v10}, Lcom/sgiggle/contacts/ContactStore;->buildContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sgiggle/contacts/Contact;

    move-result-object v1

    .line 177
    sget-object v2, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    invoke-static {v2}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/ContentResolver;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v2

    .line 178
    invoke-virtual {v2}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDisplayOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v5

    sget-object v6, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    if-ne v5, v6, :cond_4

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_4

    .line 180
    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "contact_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "display_name_alt"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "data4"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "data2"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "data5"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "data3"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "data6"

    aput-object v7, v5, v6

    move-object v6, v5

    .line 198
    :goto_1
    const-string v7, "contact_id = ? AND mimetype = ?"

    .line 199
    const/4 v5, 0x2

    new-array v8, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v8, v5

    const/4 v4, 0x1

    const-string v5, "vnd.android.cursor.item/name"

    aput-object v5, v8, v4

    .line 202
    sget-object v4, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 204
    if-nez v4, :cond_5

    .line 205
    const-string v1, "Tango.ContactStore"

    const-string v2, "getContactByNumber(): ContentResolver failed to query."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v3

    goto/16 :goto_0

    .line 189
    :cond_4
    const/4 v5, 0x7

    :try_start_1
    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "contact_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "display_name"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "data4"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "data2"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "data5"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "data3"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "data6"

    aput-object v7, v5, v6

    move-object v6, v5

    goto :goto_1

    .line 211
    :cond_5
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 212
    if-eqz v4, :cond_5

    const/4 v3, 0x2

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x3

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x4

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x5

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x6

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 218
    :cond_6
    const/4 v3, 0x1

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sgiggle/contacts/Contact;->displayName:Ljava/lang/String;

    .line 219
    const/4 v3, 0x2

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sgiggle/contacts/Contact;->namePrefix:Ljava/lang/String;

    .line 220
    const/4 v3, 0x3

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sgiggle/contacts/Contact;->firstName:Ljava/lang/String;

    .line 221
    const/4 v3, 0x4

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sgiggle/contacts/Contact;->middleName:Ljava/lang/String;

    .line 222
    const/4 v3, 0x5

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sgiggle/contacts/Contact;->lastName:Ljava/lang/String;

    .line 223
    const/4 v3, 0x6

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sgiggle/contacts/Contact;->nameSuffix:Ljava/lang/String;

    .line 226
    invoke-virtual {v2}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDisplayOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    if-ne v2, v3, :cond_7

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_7

    .line 228
    iget-object v2, v1, Lcom/sgiggle/contacts/Contact;->namePrefix:Ljava/lang/String;

    iget-object v3, v1, Lcom/sgiggle/contacts/Contact;->firstName:Ljava/lang/String;

    iget-object v5, v1, Lcom/sgiggle/contacts/Contact;->middleName:Ljava/lang/String;

    iget-object v6, v1, Lcom/sgiggle/contacts/Contact;->lastName:Ljava/lang/String;

    iget-object v7, v1, Lcom/sgiggle/contacts/Contact;->nameSuffix:Ljava/lang/String;

    invoke-static {v2, v3, v5, v6, v7}, Lcom/sgiggle/contacts/ContactStore;->getAlternativeDisplayName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sgiggle/contacts/Contact;->displayName:Ljava/lang/String;

    .line 233
    :cond_7
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v1

    .line 239
    goto/16 :goto_0

    .line 236
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_8
    move-object v1, v3

    goto :goto_2
.end method

.method private static getContactUri(J)Landroid/net/Uri;
    .locals 2
    .parameter

    .prologue
    .line 243
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getEmailAddresses(Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 96
    .line 98
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "data1"

    aput-object v3, v2, v6

    const-string v3, "contact_id=? AND mimetype=\'vnd.android.cursor.item/email_v2\'"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p0, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 104
    if-nez v0, :cond_0

    .line 105
    const-string v0, "Tango.ContactStore"

    const-string v1, "getEmailAddresses(): ContentResolver failed to query."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v5

    .line 121
    :goto_0
    return-object v0

    .line 110
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 111
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 112
    new-array v2, v1, [Ljava/lang/String;

    move v3, v6

    .line 113
    :goto_1
    if-ge v3, v1, :cond_1

    .line 114
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 115
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 113
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move-object v1, v2

    .line 119
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v1

    .line 121
    goto :goto_0

    :cond_2
    move-object v1, v5

    goto :goto_2
.end method

.method private static getNativeFavoriteContacts(Landroid/content/ContentResolver;Ljava/util/HashMap;)V
    .locals 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sgiggle/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 379
    const-string v0, "Tango.ContactStore"

    const-string v1, "getNativeFavoriteContacts()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v0, "starred"

    aput-object v0, v2, v7

    .line 381
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "starred=?"

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v6

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 383
    if-eqz v1, :cond_2

    .line 385
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386
    const-string v0, "Tango.ContactStore"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found favorite contact: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/contacts/Contact;

    .line 388
    if-eqz v0, :cond_0

    .line 390
    iput-boolean v7, v0, Lcom/sgiggle/contacts/Contact;->nativeFavorite:Z

    goto :goto_0

    .line 393
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_2
    return-void
.end method

.method private static getNativeFavortieCursor4Contact(J[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 287
    invoke-static {p0, p1}, Lcom/sgiggle/contacts/ContactStore;->getContactUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 288
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v0, "starred"

    aput-object v0, v2, v4

    .line 289
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    const-string v3, "starred=?"

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v6

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 290
    return-object v0
.end method

.method public static getPhotoByContactId(J)Landroid/graphics/Bitmap;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 312
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 313
    const-string v0, "Tango.ContactStore"

    const-string v1, "getPhotoByContactId(): s_contentResolver is null. Return NULL."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 331
    :goto_0
    return-object v0

    .line 320
    :cond_0
    invoke-static {p0, p1}, Lcom/sgiggle/contacts/ContactStore;->openContactPhoto(J)Ljava/io/InputStream;

    move-result-object v0

    .line 321
    if-nez v0, :cond_1

    move-object v0, v2

    .line 322
    goto :goto_0

    .line 324
    :cond_1
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 326
    :try_start_0
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v0, v1

    .line 331
    goto :goto_0

    .line 328
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private static getThumbnailsCursor4Contact(J[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 265
    invoke-static {p0, p1}, Lcom/sgiggle/contacts/ContactStore;->getContactUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 266
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    const-string v3, "PHOTO_ID is not null"

    move-object v2, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 267
    return-object v0
.end method

.method private static hasPicture(J)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 274
    new-array v0, v2, [Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/sgiggle/contacts/ContactStore;->getThumbnailsCursor4Contact(J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 276
    if-eqz v0, :cond_0

    .line 277
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 278
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 279
    if-eqz v1, :cond_0

    .line 280
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private static loadAllEmails(Landroid/content/ContentResolver;Ljava/util/HashMap;)V
    .locals 10
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sgiggle/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 588
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllEmails()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v7

    const-string v0, "data1"

    aput-object v0, v2, v8

    const-string v0, "raw_contact_id"

    aput-object v0, v2, v9

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 594
    if-nez v2, :cond_0

    .line 595
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllEmails(): ContentResolver failed to query."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    :goto_0
    return-void

    .line 600
    :cond_0
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 601
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 602
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 603
    sget-object v4, Lcom/sgiggle/contacts/ContactStore;->s_rawContactIdsFromTangoSync:Ljava/util/Set;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 604
    const-string v4, "Tango.ContactStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Discard contact data from tango_sync: Email.RAW_CONTACT_ID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Email.DATA="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 609
    :cond_1
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/contacts/Contact;

    .line 610
    if-eqz v0, :cond_0

    .line 611
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 612
    iget-object v4, v0, Lcom/sgiggle/contacts/Contact;->emailAddresses:[Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/sgiggle/contacts/Contact;->emailAddresses:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_2

    .line 614
    iget-object v4, v0, Lcom/sgiggle/contacts/Contact;->emailAddresses:[Ljava/lang/String;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 616
    :cond_2
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/sgiggle/contacts/Contact;->emailAddresses:[Ljava/lang/String;

    goto :goto_1

    .line 620
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 622
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllEmails(): done loaded."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static loadAllNames(Landroid/content/ContentResolver;Ljava/util/HashMap;)V
    .locals 13
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sgiggle/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 480
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllNames()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/ContentResolver;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v6

    .line 482
    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v9

    const-string v0, "data4"

    aput-object v0, v2, v8

    const-string v0, "data2"

    aput-object v0, v2, v10

    const-string v0, "data5"

    aput-object v0, v2, v11

    const-string v0, "data3"

    aput-object v0, v2, v12

    const/4 v0, 0x5

    const-string v1, "data6"

    aput-object v1, v2, v0

    .line 488
    const-string v3, "mimetype = ?"

    .line 489
    new-array v4, v8, [Ljava/lang/String;

    const-string v0, "vnd.android.cursor.item/name"

    aput-object v0, v4, v9

    .line 490
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 491
    if-nez v1, :cond_0

    .line 492
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllNames(): ContentResolver failed to query."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :goto_0
    return-void

    .line 497
    :cond_0
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 498
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/contacts/Contact;

    .line 500
    if-eqz v0, :cond_0

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 506
    :cond_1
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sgiggle/contacts/Contact;->namePrefix:Ljava/lang/String;

    .line 507
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sgiggle/contacts/Contact;->firstName:Ljava/lang/String;

    .line 508
    invoke-interface {v1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sgiggle/contacts/Contact;->middleName:Ljava/lang/String;

    .line 509
    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sgiggle/contacts/Contact;->lastName:Ljava/lang/String;

    .line 510
    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sgiggle/contacts/Contact;->nameSuffix:Ljava/lang/String;

    .line 513
    invoke-virtual {v6}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDisplayOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    if-ne v2, v3, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    .line 515
    iget-object v2, v0, Lcom/sgiggle/contacts/Contact;->namePrefix:Ljava/lang/String;

    iget-object v3, v0, Lcom/sgiggle/contacts/Contact;->firstName:Ljava/lang/String;

    iget-object v4, v0, Lcom/sgiggle/contacts/Contact;->middleName:Ljava/lang/String;

    iget-object v5, v0, Lcom/sgiggle/contacts/Contact;->lastName:Ljava/lang/String;

    iget-object v7, v0, Lcom/sgiggle/contacts/Contact;->nameSuffix:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5, v7}, Lcom/sgiggle/contacts/ContactStore;->getAlternativeDisplayName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sgiggle/contacts/Contact;->displayName:Ljava/lang/String;

    goto/16 :goto_1

    .line 519
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 521
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllNames(): done loaded."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static loadAllPhoneNumbers(Landroid/content/ContentResolver;Ljava/util/HashMap;)V
    .locals 11
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sgiggle/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 528
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllPhoneNumbers()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v7

    const-string v0, "data1"

    aput-object v0, v2, v8

    const-string v0, "data2"

    aput-object v0, v2, v9

    const-string v0, "raw_contact_id"

    aput-object v0, v2, v10

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 538
    if-nez v2, :cond_0

    .line 539
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllPhoneNumbers(): ContentResolver failed to query."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    :goto_0
    return-void

    .line 544
    :cond_0
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 545
    invoke-interface {v2, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 546
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 547
    sget-object v4, Lcom/sgiggle/contacts/ContactStore;->s_rawContactIdsFromTangoSync:Ljava/util/Set;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 548
    const-string v4, "Tango.ContactStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Discard contact data from tango_sync: Phone.RAW_CONTACT_ID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Phone.NUMBER="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 552
    :cond_1
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/contacts/Contact;

    .line 553
    if-eqz v0, :cond_0

    .line 554
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 555
    iget-object v4, v0, Lcom/sgiggle/contacts/Contact;->subscriberNumbers:[Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/sgiggle/contacts/Contact;->subscriberNumbers:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_2

    .line 557
    iget-object v4, v0, Lcom/sgiggle/contacts/Contact;->subscriberNumbers:[Ljava/lang/String;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 559
    :cond_2
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 560
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/sgiggle/contacts/Contact;->subscriberNumbers:[Ljava/lang/String;

    .line 562
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 563
    iget-object v1, v0, Lcom/sgiggle/contacts/Contact;->phoneTypes:[I

    if-eqz v1, :cond_3

    .line 565
    iget-object v1, v0, Lcom/sgiggle/contacts/Contact;->phoneTypes:[I

    array-length v4, v1

    move v5, v7

    :goto_2
    if-ge v5, v4, :cond_3

    aget v6, v1, v5

    .line 567
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 570
    :cond_3
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sgiggle/contacts/ContactStore;->translatePhoneType(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 572
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/sgiggle/contacts/Contact;->phoneTypes:[I

    move v4, v7

    .line 573
    :goto_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_0

    .line 575
    iget-object v5, v0, Lcom/sgiggle/contacts/Contact;->phoneTypes:[I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v5, v4

    .line 573
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    .line 579
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 581
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadAllPhoneNumbers(): done loaded."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static loadInitialContactIds(Landroid/content/ContentResolver;)Ljava/util/HashMap;
    .locals 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sgiggle/contacts/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 403
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadInitialContactIds()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 405
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/ContentResolver;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    .line 407
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "SHARP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v3, v4

    .line 416
    :goto_0
    invoke-virtual {v0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDisplayOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->ALTERNATIVE:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    if-ne v0, v1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 418
    new-array v0, v13, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v11

    const-string v1, "display_name_alt"

    aput-object v1, v0, v12

    move-object v2, v0

    .line 426
    :goto_1
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 431
    if-nez v0, :cond_2

    .line 432
    const-string v0, "Tango.ContactStore"

    const-string v1, "loadInitialContactIds(): ContentResolver failed to query."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 473
    :goto_2
    return-object v0

    .line 412
    :cond_0
    const-string v1, "in_visible_group=1"

    move-object v3, v1

    goto :goto_0

    .line 422
    :cond_1
    new-array v0, v13, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v11

    const-string v1, "display_name"

    aput-object v1, v0, v12

    move-object v2, v0

    goto :goto_1

    .line 436
    :cond_2
    :goto_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 437
    new-instance v1, Lcom/sgiggle/contacts/Contact;

    invoke-direct {v1}, Lcom/sgiggle/contacts/Contact;-><init>()V

    .line 438
    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/sgiggle/contacts/Contact;->deviceContactId:J

    .line 439
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sgiggle/contacts/Contact;->displayName:Ljava/lang/String;

    .line 440
    iget-wide v2, v1, Lcom/sgiggle/contacts/Contact;->deviceContactId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 442
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 444
    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v3, v11

    const-string v0, "contact_id"

    aput-object v0, v3, v12

    const-string v0, "account_name"

    aput-object v0, v3, v13

    const-string v0, "account_type"

    aput-object v0, v3, v14

    .line 451
    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 454
    sget-object v1, Lcom/sgiggle/contacts/ContactStore;->s_rawContactIdsFromTangoSync:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 455
    :cond_4
    :goto_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 456
    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 457
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 458
    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 459
    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 460
    if-eqz v5, :cond_4

    if-eqz v6, :cond_4

    .line 463
    const-string v8, "com.sgiggle.app"

    invoke-virtual {v6, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 464
    sget-object v8, Lcom/sgiggle/contacts/ContactStore;->s_rawContactIdsFromTangoSync:Ljava/util/Set;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 465
    const-string v8, "Tango.ContactStore"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Add RawContact from tango_sync to black list: RawContacts._ID="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RawContacts.CONTACT_ID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RawContacts.ACCOUNT_NAME="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RawContacts.ACCOUNT_TYPE="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 472
    :cond_5
    const-string v0, "Tango.ContactStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadInitialContactIds(): loaded count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 473
    goto/16 :goto_2
.end method

.method private static nativeFavorite(J)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 294
    new-array v0, v2, [Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/sgiggle/contacts/ContactStore;->getNativeFavortieCursor4Contact(J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 296
    if-eqz v0, :cond_0

    .line 297
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 298
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 299
    if-eqz v1, :cond_0

    .line 300
    const/4 v0, 0x1

    .line 303
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private static openContactPhoto(J)Ljava/io/InputStream;
    .locals 2
    .parameter

    .prologue
    .line 258
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    invoke-static {p0, p1}, Lcom/sgiggle/contacts/ContactStore;->getContactUri(J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 261
    return-object v0
.end method

.method private static translatePhoneType(I)I
    .locals 1
    .parameter

    .prologue
    .line 703
    sparse-switch p0, :sswitch_data_0

    .line 708
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 704
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 705
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 706
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 707
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 703
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0xc -> :sswitch_3
    .end sparse-switch
.end method

.method private static translatePhoneTypes(Ljava/util/ArrayList;)[I
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 695
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 696
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    array-length v0, v1

    if-ge v2, v0, :cond_0

    .line 697
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/contacts/ContactStore;->translatePhoneType(I)I

    move-result v0

    aput v0, v1, v2

    .line 696
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 699
    :cond_0
    return-object v1
.end method

.method public static updateContext(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 47
    if-nez p0, :cond_1

    .line 48
    const-string v0, "Tango.ContactStore"

    const-string v1, "updateContext: context is unexpectedly null"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    .line 53
    sget-object v0, Lcom/sgiggle/contacts/ContactStore;->s_contentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 54
    const-string v0, "Tango.ContactStore"

    const-string v1, "updateContext: getContextResolver() returned null."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
