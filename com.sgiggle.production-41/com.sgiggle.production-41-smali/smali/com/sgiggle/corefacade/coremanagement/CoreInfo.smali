.class public Lcom/sgiggle/corefacade/coremanagement/CoreInfo;
.super Ljava/lang/Object;
.source "CoreInfo.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 43
    invoke-static {}, Lcom/sgiggle/corefacade/coremanagement/coremanagementJNI;->new_CoreInfo()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;-><init>(JZ)V

    .line 44
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->swigCPtr:J

    .line 18
    return-void
.end method

.method protected static getCPtr(Lcom/sgiggle/corefacade/coremanagement/CoreInfo;)J
    .locals 2
    .parameter

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->swigCPtr:J

    goto :goto_0
.end method

.method public static getVersionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/sgiggle/corefacade/coremanagement/coremanagementJNI;->CoreInfo_getVersionString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/sgiggle/corefacade/coremanagement/coremanagementJNI;->delete_CoreInfo(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->delete()V

    .line 26
    return-void
.end method
