.class public Lcom/sgiggle/dynamicconfig/DynamicConfigWrapper;
.super Ljava/lang/Object;
.source "DynamicConfigWrapper.java"


# static fields
.field public static final DYNCFG_AUDIO_HAS_NO_EARPIECE:Ljava/lang/String; = "noearpiece"

.field public static final DYNCFG_AUDIO_MODE:Ljava/lang/String; = "audio_mode"

.field public static final DYNCFG_AUDIO_SOURCE:Ljava/lang/String; = "audio_source"

.field public static final DYNCFG_CAMERA_AUTOFOCUS:Ljava/lang/String; = "fcam_autofocus"

.field public static final DYNCFG_CAMERA_BACK_CAPTURE_HEIGHT_KEY:Ljava/lang/String; = "bcam_captH"

.field public static final DYNCFG_CAMERA_BACK_CAPTURE_WIDTH_KEY:Ljava/lang/String; = "bcam_captW"

.field public static final DYNCFG_CAMERA_BACK_HEIGHT_KEY:Ljava/lang/String; = "bcam_H"

.field public static final DYNCFG_CAMERA_BACK_WIDTH_KEY:Ljava/lang/String; = "bcam_W"

.field public static final DYNCFG_CAMERA_FRAME_RATE:Ljava/lang/String; = "fcam_frate"

.field public static final DYNCFG_CAMERA_FRONT_CAPTURE_HEIGHT_KEY:Ljava/lang/String; = "fcam_captH"

.field public static final DYNCFG_CAMERA_FRONT_CAPTURE_WIDTH_KEY:Ljava/lang/String; = "fcam_captW"

.field public static final DYNCFG_CAMERA_FRONT_HEIGHT_KEY:Ljava/lang/String; = "fcam_H"

.field public static final DYNCFG_CAMERA_FRONT_ROTATION_KEY:Ljava/lang/String; = "fcam_rotation"

.field public static final DYNCFG_CAMERA_FRONT_WIDTH_KEY:Ljava/lang/String; = "fcam_W"

.field public static final DYNCFG_CAMERA_RELEASE_WHEN_SUSPEND:Ljava/lang/String; = "fcam_release"

.field public static final DYNCFG_CAMERA_USE_FPSRANGE_METHOD:Ljava/lang/String; = "fcam_usefpsrange"

.field public static final DYNCFG_CAMERA_VIRTUALSIZE_LANDSCAPE_KEY:Ljava/lang/String; = "fcam_vsize-land"

.field public static final DYNCFG_CAMERA_VIRTUALSIZE_PORTRAIT_KEY:Ljava/lang/String; = "fcam_vsize-port"

.field public static final DYNCFG_VIDEO_NOSWAP:Ljava/lang/String; = "noswap"

.field public static final DYNCFG_VIDEO_VSOFT_FLAG:Ljava/lang/String; = "vsoft_flag"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 38
    invoke-static {p0}, Lcom/sgiggle/dynamicconfig/DynamicConfigWrapper;->nativeGet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static native nativeGet(Ljava/lang/String;)Ljava/lang/String;
.end method
