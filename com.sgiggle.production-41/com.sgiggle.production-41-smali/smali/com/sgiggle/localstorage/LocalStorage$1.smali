.class final Lcom/sgiggle/localstorage/LocalStorage$1;
.super Landroid/content/BroadcastReceiver;
.source "LocalStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/localstorage/LocalStorage;->registerKiller(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$mountedOnStartup:Z

.field final synthetic val$storageDir:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sgiggle/localstorage/LocalStorage$1;->val$storageDir:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/sgiggle/localstorage/LocalStorage$1;->val$mountedOnStartup:Z

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 145
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 146
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 147
    const/4 v2, 0x0

    .line 148
    if-eqz v1, :cond_1

    .line 149
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 151
    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sgiggle/localstorage/LocalStorage$1;->val$storageDir:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/localstorage/LocalStorage$1;->val$mountedOnStartup:Z

    if-eq v0, v1, :cond_0

    .line 152
    const-string v0, "Tango"

    const-string v1, "External storage status changed, killing ourselfs."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 155
    :cond_0
    return-void

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method
