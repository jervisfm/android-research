.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AudioControlMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;",
        ">;"
    }
.end annotation


# static fields
.field public static final AUDIO_CONTROL_MUTE:I = 0x2

.field public static final AUDIO_CONTROL_SPEAKER:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1086
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1087
    return-void
.end method

.method protected constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;)V
    .locals 0
    .parameter

    .prologue
    .line 1098
    invoke-direct {p0, p1}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1099
    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1090
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7551

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setSpeakeron(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setMute(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1095
    return-void
.end method

.method public static createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    .line 1103
    const/16 v1, 0x7551

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    .line 1104
    const/4 v1, 0x1

    if-ne p0, v1, :cond_1

    .line 1105
    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setSpeakeron(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    .line 1110
    :cond_0
    :goto_0
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;)V

    return-object v1

    .line 1106
    :cond_1
    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    .line 1107
    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setMute(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    goto :goto_0
.end method
