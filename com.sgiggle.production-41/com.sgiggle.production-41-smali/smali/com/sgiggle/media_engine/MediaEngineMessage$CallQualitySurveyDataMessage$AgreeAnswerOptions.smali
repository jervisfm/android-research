.class public final enum Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;
.super Ljava/lang/Enum;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AgreeAnswerOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

.field public static final enum AGREE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

.field public static final enum DISAGREE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

.field public static final enum NOT_SURE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

.field public static final enum UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2074
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    const-string v1, "UNANSWERED"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    const-string v1, "DISAGREE"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->DISAGREE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    const-string v1, "AGREE"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->AGREE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    const-string v1, "NOT_SURE"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->NOT_SURE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    .line 2073
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->DISAGREE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->AGREE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->NOT_SURE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->$VALUES:[Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2073
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;
    .locals 1
    .parameter

    .prologue
    .line 2073
    const-class v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;
    .locals 1

    .prologue
    .line 2073
    sget-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->$VALUES:[Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    invoke-virtual {v0}, [Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    return-object v0
.end method
