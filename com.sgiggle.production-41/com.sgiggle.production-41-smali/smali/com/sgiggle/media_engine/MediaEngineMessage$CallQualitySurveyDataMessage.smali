.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallQualitySurveyDataMessage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(ILcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    const/16 v1, 0x7665

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setOverallRating(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setHeardEcho(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p3}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setHeardNoise(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p4}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setDelay(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p5}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setDistortedSpeech(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p6}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setPictureFreezing(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p7}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setPictureBlurry(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p8}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setPictureRotated(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p9}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setUnnaturalMovement(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-static {p10}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;->MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setBadLipsync(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p11}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setComments(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2115
    return-void
.end method

.method private static MsgEnum2PayloadEnum(Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 3
    .parameter

    .prologue
    .line 2079
    sget-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$1;->$SwitchMap$com$sgiggle$media_engine$MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions:[I

    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2089
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected value of AgreeAnswerOptions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2081
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    .line 2087
    :goto_0
    return-object v0

    .line 2083
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->DISAGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    .line 2085
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->AGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    .line 2087
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->NOT_SURE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    .line 2079
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
