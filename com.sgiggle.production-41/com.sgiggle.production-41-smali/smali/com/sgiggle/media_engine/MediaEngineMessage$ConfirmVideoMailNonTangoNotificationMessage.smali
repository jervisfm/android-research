.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$ConfirmVideoMailNonTangoNotificationMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConfirmVideoMailNonTangoNotificationMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1569
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1570
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1582
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75d3

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConfirmVideoMailNonTangoNotificationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->setSuccess(Z)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1589
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1573
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75d3

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConfirmVideoMailNonTangoNotificationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->setSuccess(Z)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1579
    return-void
.end method
