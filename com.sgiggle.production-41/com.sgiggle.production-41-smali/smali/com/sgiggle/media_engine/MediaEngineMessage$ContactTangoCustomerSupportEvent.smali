.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$ContactTangoCustomerSupportEvent;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactTangoCustomerSupportEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3365
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 3366
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 3369
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    const v1, 0x891f

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactTangoCustomerSupportEvent;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->setTangoemail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 3373
    return-void
.end method
