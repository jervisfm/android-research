.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteConversationMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$StringPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1782
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StringPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StringPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1783
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 1786
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StringPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StringPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7642

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StringPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StringPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StringPayload$Builder;->setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StringPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StringPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$StringPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1790
    return-void
.end method
