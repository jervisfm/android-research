.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteVideoMailMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteVideoMailMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1538
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75cb

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteVideoMailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->addVideoMails(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1543
    return-void
.end method
