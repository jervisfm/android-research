.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameCatalogMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayGameCatalogMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2639
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    const/16 v1, 0x765c

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameCatalogMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->addMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    const-string v1, "product.category.game"

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->addCategoryKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2644
    return-void
.end method
