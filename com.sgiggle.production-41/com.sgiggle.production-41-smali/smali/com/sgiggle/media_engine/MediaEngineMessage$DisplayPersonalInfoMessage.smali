.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPersonalInfoMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayPersonalInfoMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1070
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    const/16 v1, 0x754d

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPersonalInfoMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setAccessAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1074
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 1077
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    const/16 v1, 0x754d

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPersonalInfoMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setAccessAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1081
    return-void
.end method
