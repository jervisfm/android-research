.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplaySettingsMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 950
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7569

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 953
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V
    .locals 2
    .parameter

    .prologue
    .line 956
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7569

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->addAlerts2Dismiss(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 960
    return-void
.end method
