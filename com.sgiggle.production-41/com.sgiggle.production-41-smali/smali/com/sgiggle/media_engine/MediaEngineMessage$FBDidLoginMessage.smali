.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FBDidLoginMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3166
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 3167
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 3169
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7670

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload$Builder;->setAccessToken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload$Builder;->setExpirationTime(J)Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 3174
    return-void
.end method
