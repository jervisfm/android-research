.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMailMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FinishPlayVideoMailMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2150
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75e6

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;->setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;->setSuccess(Z)Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$FinishPlayVideoMailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2156
    return-void
.end method
