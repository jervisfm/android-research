.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageRequestMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ForwardMessageRequestMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V
    .locals 2
    .parameter

    .prologue
    .line 1872
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    const/16 v1, 0x7648

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageRequestMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1876
    return-void
.end method
