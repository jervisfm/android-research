.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardToPostCallContentMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ForwardToPostCallContentMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2185
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_NONE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    invoke-direct {p0, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardToPostCallContentMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;)V

    .line 2186
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;)V
    .locals 2
    .parameter

    .prologue
    .line 2188
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7605

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardToPostCallContentMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload$Builder;->setContentType(Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;)Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2192
    return-void
.end method
