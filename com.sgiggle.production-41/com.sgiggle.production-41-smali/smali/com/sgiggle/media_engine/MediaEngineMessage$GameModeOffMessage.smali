.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$GameModeOffMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GameModeOffMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .parameter

    .prologue
    .line 2691
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7663

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameModeOffMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->setOnBackground(Z)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2695
    return-void
.end method
