.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$InitiateVGoodMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InitiateVGoodMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 672
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 673
    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 3
    .parameter

    .prologue
    .line 675
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;

    move-result-object v0

    const/16 v1, 0x7557

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InitiateVGoodMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;->setVgoodId(J)Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 679
    return-void
.end method
