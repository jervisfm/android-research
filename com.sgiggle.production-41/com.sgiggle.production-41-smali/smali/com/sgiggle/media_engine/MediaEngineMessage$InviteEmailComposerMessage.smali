.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteEmailComposerMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 805
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 806
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 809
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    const/16 v1, 0x756d

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->addAllInvitee(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 813
    return-void
.end method
