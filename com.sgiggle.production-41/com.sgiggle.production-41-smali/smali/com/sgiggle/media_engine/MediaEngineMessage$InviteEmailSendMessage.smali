.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSendMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteEmailSendMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 784
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 785
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 788
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7565

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSendMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->addAllInvitee(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setSuccess(Z)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 793
    return-void
.end method
