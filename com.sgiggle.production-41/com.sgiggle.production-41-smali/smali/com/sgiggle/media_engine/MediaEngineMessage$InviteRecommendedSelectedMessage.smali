.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectedMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteRecommendedSelectedMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 906
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 907
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 910
    invoke-static {p1, p2}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectedMessage;->createPayload(Ljava/util/List;Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 911
    return-void
.end method

.method protected static createPayload(Ljava/util/List;Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;"
        }
    .end annotation

    .prologue
    .line 914
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    .line 915
    const/16 v1, 0x7575

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectedMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->addAllContact(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    .line 918
    if-eqz p1, :cond_0

    .line 919
    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->setRecommendationAlgorithm(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    .line 922
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method
