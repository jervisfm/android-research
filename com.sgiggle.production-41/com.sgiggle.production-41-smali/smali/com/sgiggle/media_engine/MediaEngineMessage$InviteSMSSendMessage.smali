.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSendMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteSMSSendMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 857
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 858
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 865
    invoke-static {p1, p2, p3}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSendMessage;->createPayload(Ljava/util/List;Ljava/lang/String;Z)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 866
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Z)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 861
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSendMessage;-><init>(Ljava/util/List;Ljava/lang/String;Z)V

    .line 862
    return-void
.end method

.method protected static createPayload(Ljava/util/List;Ljava/lang/String;Z)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 2
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;"
        }
    .end annotation

    .prologue
    .line 869
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    .line 870
    const/16 v1, 0x7576

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSendMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->addAllInvitee(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setSuccess(Z)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    .line 874
    if-eqz p1, :cond_0

    .line 875
    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setRecommendationAlgorithm(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    .line 878
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method
