.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$LeaveVideoMailMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LeaveVideoMailMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2004
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75d5

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LeaveVideoMailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setLeave(Z)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2008
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1990
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75d5

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LeaveVideoMailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setLeave(Z)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1995
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1997
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75d5

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LeaveVideoMailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setLeave(Z)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2002
    return-void
.end method
