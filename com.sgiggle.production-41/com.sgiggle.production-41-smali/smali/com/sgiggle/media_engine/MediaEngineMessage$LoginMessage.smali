.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$LoginMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoginMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 468
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7535

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setFromUI(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 472
    return-void
.end method
