.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoginNotificationMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 588
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 589
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 592
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7532

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setFromUI(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setUsername(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 597
    return-void
.end method
