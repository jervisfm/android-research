.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MakeCallMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 602
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 603
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 614
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 615
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 606
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7537

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setAccountId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setVideoMode(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 612
    return-void
.end method
