.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$MediaEngineMessageFactory;
.super Ljava/lang/Object;
.source "MediaEngineMessage.java"

# interfaces
.implements Lcom/sgiggle/messaging/MessageFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MediaEngineMessageFactory"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaEngineMessageFactory"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(I)Lcom/sgiggle/messaging/Message;
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 3924
    const/16 v0, 0x7530

    if-lt p1, v0, :cond_0

    const v0, 0x9c3f

    if-le p1, v0, :cond_1

    :cond_0
    move-object v0, v3

    .line 4216
    :goto_0
    return-object v0

    .line 3928
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 4212
    const-string v0, "MediaEngineMessageFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "create(): message-id = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 4216
    goto :goto_0

    .line 3932
    :sswitch_0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginMessage;-><init>()V

    goto :goto_0

    .line 3933
    :sswitch_1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$TestMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TestMessage;-><init>()V

    goto :goto_0

    .line 3934
    :sswitch_2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationUserAcceptedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationUserAcceptedMessage;-><init>()V

    goto :goto_0

    .line 3935
    :sswitch_3
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationMessage;-><init>()V

    goto :goto_0

    .line 3936
    :sswitch_4
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;-><init>()V

    goto :goto_0

    .line 3937
    :sswitch_5
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$MakePremiumCallMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$MakePremiumCallMessage;-><init>()V

    goto :goto_0

    .line 3938
    :sswitch_6
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInBackgroundMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInBackgroundMessage;-><init>()V

    goto :goto_0

    .line 3939
    :sswitch_7
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;-><init>()V

    goto :goto_0

    .line 3940
    :sswitch_8
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RejectCallMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RejectCallMessage;-><init>()V

    goto :goto_0

    .line 3941
    :sswitch_9
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;-><init>()V

    goto :goto_0

    .line 3942
    :sswitch_a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AcceptCallMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AcceptCallMessage;-><init>()V

    goto :goto_0

    .line 3943
    :sswitch_b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendSgiggleInvitationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendSgiggleInvitationMessage;-><init>()V

    goto :goto_0

    .line 3944
    :sswitch_c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelSgiggleInvitationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelSgiggleInvitationMessage;-><init>()V

    goto :goto_0

    .line 3945
    :sswitch_d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AddVideoMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AddVideoMessage;-><init>()V

    goto :goto_0

    .line 3946
    :sswitch_e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RemoveVideoMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RemoveVideoMessage;-><init>()V

    goto :goto_0

    .line 3947
    :sswitch_f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InitiateVGoodMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InitiateVGoodMessage;-><init>()V

    goto/16 :goto_0

    .line 3948
    :sswitch_10
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserMessage;-><init>()V

    goto/16 :goto_0

    .line 3949
    :sswitch_11
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;-><init>()V

    goto/16 :goto_0

    .line 3950
    :sswitch_12
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionMessage;-><init>()V

    goto/16 :goto_0

    .line 3951
    :sswitch_13
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSendMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSendMessage;-><init>()V

    goto/16 :goto_0

    .line 3952
    :sswitch_14
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionMessage;-><init>()V

    goto/16 :goto_0

    .line 3953
    :sswitch_15
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectedMessage;-><init>()V

    goto/16 :goto_0

    .line 3954
    :sswitch_16
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionMessage;-><init>()V

    goto/16 :goto_0

    .line 3955
    :sswitch_17
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectedMessage;-><init>()V

    goto/16 :goto_0

    .line 3956
    :sswitch_18
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactsDisplayMainMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactsDisplayMainMessage;-><init>()V

    goto/16 :goto_0

    .line 3957
    :sswitch_19
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;-><init>()V

    goto/16 :goto_0

    .line 3958
    :sswitch_1a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AllowAccessAddressBookMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AllowAccessAddressBookMessage;-><init>()V

    goto/16 :goto_0

    .line 3959
    :sswitch_1b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerMessage;-><init>()V

    goto/16 :goto_0

    .line 3960
    :sswitch_1c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SavePersonalInfoMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SavePersonalInfoMessage;-><init>()V

    goto/16 :goto_0

    .line 3961
    :sswitch_1d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;-><init>()V

    goto/16 :goto_0

    .line 3962
    :sswitch_1e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeCallErrorMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeCallErrorMessage;-><init>()V

    goto/16 :goto_0

    .line 3963
    :sswitch_1f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;-><init>()V

    goto/16 :goto_0

    .line 3964
    :sswitch_20
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPersonalInfoMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPersonalInfoMessage;-><init>()V

    goto/16 :goto_0

    .line 3965
    :sswitch_21
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequestMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequestMessage;-><init>()V

    goto/16 :goto_0

    .line 3966
    :sswitch_22
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestFAQMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestFAQMessage;-><init>()V

    goto/16 :goto_0

    .line 3967
    :sswitch_23
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendValidationCodeMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendValidationCodeMessage;-><init>()V

    goto/16 :goto_0

    .line 3968
    :sswitch_24
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SwitchCameraMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SwitchCameraMessage;-><init>()V

    goto/16 :goto_0

    .line 3969
    :sswitch_25
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;-><init>()V

    goto/16 :goto_0

    .line 3970
    :sswitch_26
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestFilterContactMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestFilterContactMessage;-><init>()V

    goto/16 :goto_0

    .line 3971
    :sswitch_27
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInForegroundMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInForegroundMessage;-><init>()V

    goto/16 :goto_0

    .line 3972
    :sswitch_28
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$KeepTangoPushNotificationAliveMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$KeepTangoPushNotificationAliveMessage;-><init>()V

    goto/16 :goto_0

    .line 3973
    :sswitch_29
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$TangoDeviceTokenMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TangoDeviceTokenMessage;-><init>()V

    goto/16 :goto_0

    .line 3974
    :sswitch_2a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteContactMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteContactMessage;-><init>()V

    goto/16 :goto_0

    .line 3975
    :sswitch_2b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestCallLogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestCallLogMessage;-><init>()V

    goto/16 :goto_0

    .line 3976
    :sswitch_2c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteCallLogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteCallLogMessage;-><init>()V

    goto/16 :goto_0

    .line 3977
    :sswitch_2d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeRegistrationErrorMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeRegistrationErrorMessage;-><init>()V

    goto/16 :goto_0

    .line 3978
    :sswitch_2e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LaunchLogReportEmailMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LaunchLogReportEmailMessage;-><init>()V

    goto/16 :goto_0

    .line 3979
    :sswitch_2f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactSearchRequestMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactSearchRequestMessage;-><init>()V

    goto/16 :goto_0

    .line 3980
    :sswitch_30
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AddContactRequestMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AddContactRequestMessage;-><init>()V

    goto/16 :goto_0

    .line 3981
    :sswitch_31
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OtherRegisteredDeviceMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$OtherRegisteredDeviceMessage;-><init>()V

    goto/16 :goto_0

    .line 3982
    :sswitch_32
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissVerificationWithOtherDeviceMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissVerificationWithOtherDeviceMessage;-><init>()V

    goto/16 :goto_0

    .line 3983
    :sswitch_33
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactTangoCustomerSupportMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactTangoCustomerSupportMessage;-><init>()V

    goto/16 :goto_0

    .line 3984
    :sswitch_34
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ShakeActionDetectedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShakeActionDetectedMessage;-><init>()V

    goto/16 :goto_0

    .line 3985
    :sswitch_35
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PhoneNumberContactSearchMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PhoneNumberContactSearchMessage;-><init>()V

    goto/16 :goto_0

    .line 3986
    :sswitch_36
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailContactSearchMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailContactSearchMessage;-><init>()V

    goto/16 :goto_0

    .line 3987
    :sswitch_37
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceiveMessageNotificationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceiveMessageNotificationMessage;-><init>()V

    goto/16 :goto_0

    .line 3988
    :sswitch_38
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestAppLogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestAppLogMessage;-><init>()V

    goto/16 :goto_0

    .line 3989
    :sswitch_39
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryUnreadMissedCallNumberMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryUnreadMissedCallNumberMessage;-><init>()V

    goto/16 :goto_0

    .line 3990
    :sswitch_3a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearUnreadMissedCallNumberMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearUnreadMissedCallNumberMessage;-><init>()V

    goto/16 :goto_0

    .line 3991
    :sswitch_3b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodAnimationCompleteMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodAnimationCompleteMessage;-><init>()V

    goto/16 :goto_0

    .line 3992
    :sswitch_3c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationUserNoCodeMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationUserNoCodeMessage;-><init>()V

    goto/16 :goto_0

    .line 3993
    :sswitch_3d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartRecordingVideoMailMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartRecordingVideoMailMessage;-><init>()V

    goto/16 :goto_0

    .line 3994
    :sswitch_3e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailMessage;-><init>()V

    goto/16 :goto_0

    .line 3995
    :sswitch_3f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ProductCatalogFinishedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ProductCatalogFinishedMessage;-><init>()V

    goto/16 :goto_0

    .line 3996
    :sswitch_40
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryProductCatalogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryProductCatalogMessage;-><init>()V

    goto/16 :goto_0

    .line 3997
    :sswitch_41
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseMessage;-><init>()V

    goto/16 :goto_0

    .line 3998
    :sswitch_42
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductDetailMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductDetailMessage;-><init>()V

    goto/16 :goto_0

    .line 3999
    :sswitch_43
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$BackToVGoodProductCatalogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$BackToVGoodProductCatalogMessage;-><init>()V

    goto/16 :goto_0

    .line 4000
    :sswitch_44
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseVGoodMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseVGoodMessage;-><init>()V

    goto/16 :goto_0

    .line 4001
    :sswitch_45
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVgoodPaymentMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVgoodPaymentMessage;-><init>()V

    goto/16 :goto_0

    .line 4002
    :sswitch_46
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelInAppPurchaseMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelInAppPurchaseMessage;-><init>()V

    goto/16 :goto_0

    .line 4003
    :sswitch_47
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LockedVGoodSelectedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LockedVGoodSelectedMessage;-><init>()V

    goto/16 :goto_0

    .line 4004
    :sswitch_48
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ToogleVideoViewButtonBarMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ToogleVideoViewButtonBarMessage;-><init>()V

    goto/16 :goto_0

    .line 4005
    :sswitch_49
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ProductInfoOkMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ProductInfoOkMessage;-><init>()V

    goto/16 :goto_0

    .line 4006
    :sswitch_4a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationCompleteMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationCompleteMessage;-><init>()V

    goto/16 :goto_0

    .line 4007
    :sswitch_4b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailReceiversMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailReceiversMessage;-><init>()V

    goto/16 :goto_0

    .line 4008
    :sswitch_4c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelVideoMailReceiversSelectionMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelVideoMailReceiversSelectionMessage;-><init>()V

    goto/16 :goto_0

    .line 4009
    :sswitch_4d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ConfirmVideoMailNonTangoNotificationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConfirmVideoMailNonTangoNotificationMessage;-><init>()V

    goto/16 :goto_0

    .line 4010
    :sswitch_4e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissUploadVideoMailFinishedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissUploadVideoMailFinishedMessage;-><init>()V

    goto/16 :goto_0

    .line 4011
    :sswitch_4f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$WelcomeGoToMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$WelcomeGoToMessage;-><init>()V

    goto/16 :goto_0

    .line 4012
    :sswitch_50
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAttemptMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAttemptMessage;-><init>()V

    goto/16 :goto_0

    .line 4013
    :sswitch_51
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginMessage;-><init>()V

    goto/16 :goto_0

    .line 4014
    :sswitch_52
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreMessage;-><init>()V

    goto/16 :goto_0

    .line 4015
    :sswitch_53
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissStoreBadgeMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissStoreBadgeMessage;-><init>()V

    goto/16 :goto_0

    .line 4018
    :sswitch_54
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarPaymentMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarPaymentMessage;-><init>()V

    goto/16 :goto_0

    .line 4019
    :sswitch_55
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAvatarMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAvatarMessage;-><init>()V

    goto/16 :goto_0

    .line 4020
    :sswitch_56
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailMessage;-><init>()V

    goto/16 :goto_0

    .line 4021
    :sswitch_57
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$BackToAvatarProductCatalogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$BackToAvatarProductCatalogMessage;-><init>()V

    goto/16 :goto_0

    .line 4024
    :sswitch_58
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;-><init>()V

    goto/16 :goto_0

    .line 4025
    :sswitch_59
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;-><init>()V

    goto/16 :goto_0

    .line 4026
    :sswitch_5a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CloseConversationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CloseConversationMessage;-><init>()V

    goto/16 :goto_0

    .line 4027
    :sswitch_5b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListMessage;-><init>()V

    goto/16 :goto_0

    .line 4028
    :sswitch_5c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationMessage;-><init>()V

    goto/16 :goto_0

    .line 4029
    :sswitch_5d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMessageMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMessageMessage;-><init>()V

    goto/16 :goto_0

    .line 4030
    :sswitch_5e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMessageMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMessageMessage;-><init>()V

    goto/16 :goto_0

    .line 4031
    :sswitch_5f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowMoreMessageMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowMoreMessageMessage;-><init>()V

    goto/16 :goto_0

    .line 4032
    :sswitch_60
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageNotificationReceivedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageNotificationReceivedMessage;-><init>()V

    goto/16 :goto_0

    .line 4033
    :sswitch_61
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactRequestMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactRequestMessage;-><init>()V

    goto/16 :goto_0

    .line 4034
    :sswitch_62
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactResultMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactResultMessage;-><init>()V

    goto/16 :goto_0

    .line 4035
    :sswitch_63
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelSelectContactMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelSelectContactMessage;-><init>()V

    goto/16 :goto_0

    .line 4036
    :sswitch_64
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteSingleConversationMessageMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteSingleConversationMessageMessage;-><init>()V

    goto/16 :goto_0

    .line 4037
    :sswitch_65
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AdvertisementShownMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AdvertisementShownMessage;-><init>()V

    goto/16 :goto_0

    .line 4038
    :sswitch_66
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AdvertisementClickedMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AdvertisementClickedMessage;-><init>()V

    goto/16 :goto_0

    .line 4040
    :sswitch_67
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$TakePictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TakePictureMessage;-><init>()V

    goto/16 :goto_0

    .line 4041
    :sswitch_68
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelTakePictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelTakePictureMessage;-><init>()V

    goto/16 :goto_0

    .line 4042
    :sswitch_69
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ChoosePictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ChoosePictureMessage;-><init>()V

    goto/16 :goto_0

    .line 4043
    :sswitch_6a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelChoosePictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelChoosePictureMessage;-><init>()V

    goto/16 :goto_0

    .line 4044
    :sswitch_6b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PostProcessPictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PostProcessPictureMessage;-><init>()V

    goto/16 :goto_0

    .line 4045
    :sswitch_6c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostProcessPictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostProcessPictureMessage;-><init>()V

    goto/16 :goto_0

    .line 4046
    :sswitch_6d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureMessage;-><init>()V

    goto/16 :goto_0

    .line 4047
    :sswitch_6e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelViewPictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelViewPictureMessage;-><init>()V

    goto/16 :goto_0

    .line 4053
    :sswitch_6f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginErrorEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginErrorEvent;-><init>()V

    goto/16 :goto_0

    .line 4054
    :sswitch_70
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$TestEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TestEvent;-><init>()V

    goto/16 :goto_0

    .line 4055
    :sswitch_71
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationUserAcceptedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationUserAcceptedEvent;-><init>()V

    goto/16 :goto_0

    .line 4056
    :sswitch_72
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginNotificationEvent;-><init>()V

    goto/16 :goto_0

    .line 4057
    :sswitch_73
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginEvent;-><init>()V

    goto/16 :goto_0

    .line 4058
    :sswitch_74
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$MakePremiumCallEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$MakePremiumCallEvent;-><init>()V

    goto/16 :goto_0

    .line 4059
    :sswitch_75
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ProcessNotificationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ProcessNotificationEvent;-><init>()V

    goto/16 :goto_0

    .line 4060
    :sswitch_76
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;-><init>()V

    goto/16 :goto_0

    .line 4061
    :sswitch_77
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;-><init>()V

    goto/16 :goto_0

    .line 4062
    :sswitch_78
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallReceivedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallReceivedEvent;-><init>()V

    goto/16 :goto_0

    .line 4063
    :sswitch_79
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;-><init>()V

    goto/16 :goto_0

    .line 4064
    :sswitch_7a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallAcceptedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallAcceptedEvent;-><init>()V

    goto/16 :goto_0

    .line 4065
    :sswitch_7b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;-><init>()V

    goto/16 :goto_0

    .line 4066
    :sswitch_7c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoInInitializationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoInInitializationEvent;-><init>()V

    goto/16 :goto_0

    .line 4067
    :sswitch_7d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideoInProgressEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideoInProgressEvent;-><init>()V

    goto/16 :goto_0

    .line 4068
    :sswitch_7e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayRegisterUserEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayRegisterUserEvent;-><init>()V

    goto/16 :goto_0

    .line 4069
    :sswitch_7f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayRegisterUserEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayRegisterUserEvent;-><init>()V

    goto/16 :goto_0

    .line 4070
    :sswitch_80
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;-><init>()V

    goto/16 :goto_0

    .line 4071
    :sswitch_81
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoUsersEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoUsersEvent;-><init>()V

    goto/16 :goto_0

    .line 4072
    :sswitch_82
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoAlertsEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoAlertsEvent;-><init>()V

    goto/16 :goto_0

    .line 4073
    :sswitch_83
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;-><init>()V

    goto/16 :goto_0

    .line 4074
    :sswitch_84
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionEvent;-><init>()V

    goto/16 :goto_0

    .line 4075
    :sswitch_85
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;-><init>()V

    goto/16 :goto_0

    .line 4076
    :sswitch_86
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionEvent;-><init>()V

    goto/16 :goto_0

    .line 4077
    :sswitch_87
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;-><init>()V

    goto/16 :goto_0

    .line 4078
    :sswitch_88
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;-><init>()V

    goto/16 :goto_0

    .line 4079
    :sswitch_89
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactsDisplayMainEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactsDisplayMainEvent;-><init>()V

    goto/16 :goto_0

    .line 4080
    :sswitch_8a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsEvent;-><init>()V

    goto/16 :goto_0

    .line 4081
    :sswitch_8b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequiredEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequiredEvent;-><init>()V

    goto/16 :goto_0

    .line 4082
    :sswitch_8c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailValidationRequiredEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailValidationRequiredEvent;-><init>()V

    goto/16 :goto_0

    .line 4083
    :sswitch_8d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PushValidationRequiredEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PushValidationRequiredEvent;-><init>()V

    goto/16 :goto_0

    .line 4084
    :sswitch_8e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SMSValidationRequiredEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SMSValidationRequiredEvent;-><init>()V

    goto/16 :goto_0

    .line 4085
    :sswitch_8f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayFAQEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayFAQEvent;-><init>()V

    goto/16 :goto_0

    .line 4086
    :sswitch_90
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideo2WayInProgressEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideo2WayInProgressEvent;-><init>()V

    goto/16 :goto_0

    .line 4087
    :sswitch_91
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInInitializationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInInitializationEvent;-><init>()V

    goto/16 :goto_0

    .line 4088
    :sswitch_92
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$MissedCallEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$MissedCallEvent;-><init>()V

    goto/16 :goto_0

    .line 4089
    :sswitch_93
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateRequiredEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateRequiredEvent;-><init>()V

    goto/16 :goto_0

    .line 4090
    :sswitch_94
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$NetworkLowBandwidthEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$NetworkLowBandwidthEvent;-><init>()V

    goto/16 :goto_0

    .line 4091
    :sswitch_95
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$NetworkHighBandwidthEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$NetworkHighBandwidthEvent;-><init>()V

    goto/16 :goto_0

    .line 4092
    :sswitch_96
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;-><init>()V

    goto/16 :goto_0

    .line 4093
    :sswitch_97
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioModeChangedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioModeChangedEvent;-><init>()V

    goto/16 :goto_0

    .line 4094
    :sswitch_98
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoModeChangedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoModeChangedEvent;-><init>()V

    goto/16 :goto_0

    .line 4095
    :sswitch_99
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallDisconnectingEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallDisconnectingEvent;-><init>()V

    goto/16 :goto_0

    .line 4096
    :sswitch_9a
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationResultEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationResultEvent;-><init>()V

    goto/16 :goto_0

    .line 4097
    :sswitch_9b
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationFailedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationFailedEvent;-><init>()V

    goto/16 :goto_0

    .line 4098
    :sswitch_9c
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserNoNetworkEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserNoNetworkEvent;-><init>()V

    goto/16 :goto_0

    .line 4099
    :sswitch_9d
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayInviteContactEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayInviteContactEvent;-><init>()V

    goto/16 :goto_0

    .line 4100
    :sswitch_9e
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayCallLogEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayCallLogEvent;-><init>()V

    goto/16 :goto_0

    .line 4101
    :sswitch_9f
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateCallLogEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateCallLogEvent;-><init>()V

    goto/16 :goto_0

    .line 4102
    :sswitch_a0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LaunchLogReportEmailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LaunchLogReportEmailEvent;-><init>()V

    goto/16 :goto_0

    .line 4103
    :sswitch_a1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SearchAndAddMainEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SearchAndAddMainEvent;-><init>()V

    goto/16 :goto_0

    .line 4104
    :sswitch_a2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryOtherRegisteredDeviceEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryOtherRegisteredDeviceEvent;-><init>()V

    goto/16 :goto_0

    .line 4105
    :sswitch_a3
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VerficationWithOtherDeviceEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VerficationWithOtherDeviceEvent;-><init>()V

    goto/16 :goto_0

    .line 4106
    :sswitch_a4
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ShakeModeChangeEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShakeModeChangeEvent;-><init>()V

    goto/16 :goto_0

    .line 4107
    :sswitch_a5
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactTangoCustomerSupportEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactTangoCustomerSupportEvent;-><init>()V

    goto/16 :goto_0

    .line 4108
    :sswitch_a6
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PhoneNumberContactSearchEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PhoneNumberContactSearchEvent;-><init>()V

    goto/16 :goto_0

    .line 4109
    :sswitch_a7
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailContactSearchEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailContactSearchEvent;-><init>()V

    goto/16 :goto_0

    .line 4110
    :sswitch_a8
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailContactSearchResultEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailContactSearchResultEvent;-><init>()V

    goto/16 :goto_0

    .line 4111
    :sswitch_a9
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PhoneNumberContactSearchResultEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PhoneNumberContactSearchResultEvent;-><init>()V

    goto/16 :goto_0

    .line 4112
    :sswitch_aa
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;-><init>()V

    goto/16 :goto_0

    .line 4113
    :sswitch_ab
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$EnterBackgroundEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$EnterBackgroundEvent;-><init>()V

    goto/16 :goto_0

    .line 4114
    :sswitch_ac
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;-><init>()V

    goto/16 :goto_0

    .line 4115
    :sswitch_ad
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SMSRateLimitedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SMSRateLimitedEvent;-><init>()V

    goto/16 :goto_0

    .line 4116
    :sswitch_ae
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAppLogEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAppLogEvent;-><init>()V

    goto/16 :goto_0

    .line 4117
    :sswitch_af
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAlertNumbersEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAlertNumbersEvent;-><init>()V

    goto/16 :goto_0

    .line 4118
    :sswitch_b0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySMSSentEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySMSSentEvent;-><init>()V

    goto/16 :goto_0

    .line 4119
    :sswitch_b1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailValidationWrongCodeEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailValidationWrongCodeEvent;-><init>()V

    goto/16 :goto_0

    .line 4120
    :sswitch_b2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySupportWebsiteEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySupportWebsiteEvent;-><init>()V

    goto/16 :goto_0

    .line 4121
    :sswitch_b3
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailResponseEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailResponseEvent;-><init>()V

    goto/16 :goto_0

    .line 4122
    :sswitch_b4
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMailEvent;-><init>()V

    goto/16 :goto_0

    .line 4123
    :sswitch_b5
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartPlayVideoMailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartPlayVideoMailEvent;-><init>()V

    goto/16 :goto_0

    .line 4124
    :sswitch_b6
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PausePlayVideoMailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PausePlayVideoMailEvent;-><init>()V

    goto/16 :goto_0

    .line 4125
    :sswitch_b7
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMailEvent;-><init>()V

    goto/16 :goto_0

    .line 4126
    :sswitch_b8
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailConfigurationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailConfigurationEvent;-><init>()V

    goto/16 :goto_0

    .line 4127
    :sswitch_b9
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailResultEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailResultEvent;-><init>()V

    goto/16 :goto_0

    .line 4128
    :sswitch_ba
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;-><init>()V

    goto/16 :goto_0

    .line 4129
    :sswitch_bb
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartRecordingVideoMailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartRecordingVideoMailEvent;-><init>()V

    goto/16 :goto_0

    .line 4130
    :sswitch_bc
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailEvent;-><init>()V

    goto/16 :goto_0

    .line 4131
    :sswitch_bd
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodAnimationCompleteEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodAnimationCompleteEvent;-><init>()V

    goto/16 :goto_0

    .line 4132
    :sswitch_be
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductCatalogEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductCatalogEvent;-><init>()V

    goto/16 :goto_0

    .line 4133
    :sswitch_bf
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;-><init>()V

    goto/16 :goto_0

    .line 4134
    :sswitch_c0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$WaitProductCatalogEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$WaitProductCatalogEvent;-><init>()V

    goto/16 :goto_0

    .line 4135
    :sswitch_c1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSnsComposeEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSnsComposeEvent;-><init>()V

    goto/16 :goto_0

    .line 4136
    :sswitch_c2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSnsPublishResultEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSnsPublishResultEvent;-><init>()V

    goto/16 :goto_0

    .line 4137
    :sswitch_c3
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsRequestAuthEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsRequestAuthEvent;-><init>()V

    goto/16 :goto_0

    .line 4138
    :sswitch_c4
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsProcessingEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsProcessingEvent;-><init>()V

    goto/16 :goto_0

    .line 4139
    :sswitch_c5
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthFailedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthFailedEvent;-><init>()V

    goto/16 :goto_0

    .line 4140
    :sswitch_c6
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StopVideoRingbackEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StopVideoRingbackEvent;-><init>()V

    goto/16 :goto_0

    .line 4141
    :sswitch_c7
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayCallQualitySurveyEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayCallQualitySurveyEvent;-><init>()V

    goto/16 :goto_0

    .line 4142
    :sswitch_c8
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;-><init>()V

    goto/16 :goto_0

    .line 4143
    :sswitch_c9
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAppStoreEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAppStoreEvent;-><init>()V

    goto/16 :goto_0

    .line 4144
    :sswitch_ca
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayFacebookLikeEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayFacebookLikeEvent;-><init>()V

    goto/16 :goto_0

    .line 4145
    :sswitch_cb
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;-><init>()V

    goto/16 :goto_0

    .line 4146
    :sswitch_cc
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVgoodEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVgoodEvent;-><init>()V

    goto/16 :goto_0

    .line 4147
    :sswitch_cd
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ToggleVideoViewButtonBarEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ToggleVideoViewButtonBarEvent;-><init>()V

    goto/16 :goto_0

    .line 4148
    :sswitch_ce
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductInfoEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductInfoEvent;-><init>()V

    goto/16 :goto_0

    .line 4149
    :sswitch_cf
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodErrorEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodErrorEvent;-><init>()V

    goto/16 :goto_0

    .line 4150
    :sswitch_d0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;-><init>()V

    goto/16 :goto_0

    .line 4151
    :sswitch_d1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodProductCatalogEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodProductCatalogEvent;-><init>()V

    goto/16 :goto_0

    .line 4152
    :sswitch_d2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;-><init>()V

    goto/16 :goto_0

    .line 4153
    :sswitch_d3
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameCatalogEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameCatalogEvent;-><init>()V

    goto/16 :goto_0

    .line 4154
    :sswitch_d4
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductDetailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductDetailEvent;-><init>()V

    goto/16 :goto_0

    .line 4155
    :sswitch_d5
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameDemoEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameDemoEvent;-><init>()V

    goto/16 :goto_0

    .line 4156
    :sswitch_d6
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameInCallEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameInCallEvent;-><init>()V

    goto/16 :goto_0

    .line 4157
    :sswitch_d7
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationCompleteEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationCompleteEvent;-><init>()V

    goto/16 :goto_0

    .line 4158
    :sswitch_d8
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailReceiversEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailReceiversEvent;-><init>()V

    goto/16 :goto_0

    .line 4159
    :sswitch_d9
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailNonTangoNotificationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailNonTangoNotificationEvent;-><init>()V

    goto/16 :goto_0

    .line 4160
    :sswitch_da
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailFinishedEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailFinishedEvent;-><init>()V

    goto/16 :goto_0

    .line 4161
    :sswitch_db
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginEvent;-><init>()V

    goto/16 :goto_0

    .line 4162
    :sswitch_dc
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$BadgeStoreEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$BadgeStoreEvent;-><init>()V

    goto/16 :goto_0

    .line 4164
    :sswitch_dd
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$BadgeInviteEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$BadgeInviteEvent;-><init>()V

    goto/16 :goto_0

    .line 4166
    :sswitch_de
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;-><init>()V

    goto/16 :goto_0

    .line 4167
    :sswitch_df
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayConversationMessageEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayConversationMessageEvent;-><init>()V

    goto/16 :goto_0

    .line 4168
    :sswitch_e0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteSingleConversationMessageEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteSingleConversationMessageEvent;-><init>()V

    goto/16 :goto_0

    .line 4170
    :sswitch_e1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;-><init>()V

    goto/16 :goto_0

    .line 4171
    :sswitch_e2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationSummaryEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationSummaryEvent;-><init>()V

    goto/16 :goto_0

    .line 4172
    :sswitch_e3
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationEvent;-><init>()V

    goto/16 :goto_0

    .line 4174
    :sswitch_e4
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMessageEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMessageEvent;-><init>()V

    goto/16 :goto_0

    .line 4176
    :sswitch_e5
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageSendStatusEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageSendStatusEvent;-><init>()V

    goto/16 :goto_0

    .line 4177
    :sswitch_e6
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$MessageUpdateReadStatusEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$MessageUpdateReadStatusEvent;-><init>()V

    goto/16 :goto_0

    .line 4178
    :sswitch_e7
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowMoreMessageEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowMoreMessageEvent;-><init>()V

    goto/16 :goto_0

    .line 4179
    :sswitch_e8
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayMessageErrorEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayMessageErrorEvent;-><init>()V

    goto/16 :goto_0

    .line 4180
    :sswitch_e9
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationMessageNotificationEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationMessageNotificationEvent;-><init>()V

    goto/16 :goto_0

    .line 4181
    :sswitch_ea
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryLeaveMessageEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryLeaveMessageEvent;-><init>()V

    goto/16 :goto_0

    .line 4182
    :sswitch_eb
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;-><init>()V

    goto/16 :goto_0

    .line 4183
    :sswitch_ec
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RetrieveOfflineMessageResultEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RetrieveOfflineMessageResultEvent;-><init>()V

    goto/16 :goto_0

    .line 4184
    :sswitch_ed
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;-><init>()V

    goto/16 :goto_0

    .line 4186
    :sswitch_ee
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactEvent;-><init>()V

    goto/16 :goto_0

    .line 4187
    :sswitch_ef
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowAdvertisementEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowAdvertisementEvent;-><init>()V

    goto/16 :goto_0

    .line 4189
    :sswitch_f0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$TakePictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TakePictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4190
    :sswitch_f1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelTakePictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelTakePictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4191
    :sswitch_f2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ChoosePictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ChoosePictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4192
    :sswitch_f3
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelChoosePictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelChoosePictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4193
    :sswitch_f4
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PostProcessPictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PostProcessPictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4194
    :sswitch_f5
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelChoosePictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelChoosePictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4195
    :sswitch_f6
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendPictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendPictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4196
    :sswitch_f7
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4197
    :sswitch_f8
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelViewPictureEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelViewPictureEvent;-><init>()V

    goto/16 :goto_0

    .line 4198
    :sswitch_f9
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;-><init>()V

    goto/16 :goto_0

    .line 4202
    :sswitch_fa
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarErrorEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarErrorEvent;-><init>()V

    goto/16 :goto_0

    .line 4203
    :sswitch_fb
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SwitchAvatarEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SwitchAvatarEvent;-><init>()V

    goto/16 :goto_0

    .line 4204
    :sswitch_fc
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioAvatarInProgressEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioAvatarInProgressEvent;-><init>()V

    goto/16 :goto_0

    .line 4205
    :sswitch_fd
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoAvatarInProgressEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoAvatarInProgressEvent;-><init>()V

    goto/16 :goto_0

    .line 4206
    :sswitch_fe
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$Audio2WayAvatarInProgressEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$Audio2WayAvatarInProgressEvent;-><init>()V

    goto/16 :goto_0

    .line 4207
    :sswitch_ff
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;-><init>()V

    goto/16 :goto_0

    .line 4208
    :sswitch_100
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;-><init>()V

    goto/16 :goto_0

    .line 4209
    :sswitch_101
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayDemoAvatarEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayDemoAvatarEvent;-><init>()V

    goto/16 :goto_0

    .line 4210
    :sswitch_102
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarEvent;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarEvent;-><init>()V

    goto/16 :goto_0

    .line 3928
    nop

    :sswitch_data_0
    .sparse-switch
        0x125 -> :sswitch_f3
        0x7531 -> :sswitch_2
        0x7532 -> :sswitch_3
        0x7533 -> :sswitch_5
        0x7534 -> :sswitch_4
        0x7535 -> :sswitch_0
        0x7536 -> :sswitch_6
        0x7537 -> :sswitch_7
        0x7539 -> :sswitch_8
        0x753b -> :sswitch_9
        0x753d -> :sswitch_d
        0x753f -> :sswitch_e
        0x7541 -> :sswitch_b
        0x7543 -> :sswitch_a
        0x7545 -> :sswitch_1c
        0x7547 -> :sswitch_c
        0x7549 -> :sswitch_1e
        0x754b -> :sswitch_1f
        0x754d -> :sswitch_20
        0x7551 -> :sswitch_1d
        0x7554 -> :sswitch_2e
        0x7557 -> :sswitch_f
        0x755b -> :sswitch_10
        0x7561 -> :sswitch_11
        0x7563 -> :sswitch_12
        0x7565 -> :sswitch_13
        0x7567 -> :sswitch_18
        0x7569 -> :sswitch_19
        0x756b -> :sswitch_1a
        0x756d -> :sswitch_1b
        0x756f -> :sswitch_21
        0x7571 -> :sswitch_14
        0x7573 -> :sswitch_15
        0x7574 -> :sswitch_16
        0x7575 -> :sswitch_17
        0x7577 -> :sswitch_22
        0x757d -> :sswitch_24
        0x757f -> :sswitch_25
        0x7581 -> :sswitch_26
        0x7583 -> :sswitch_23
        0x7586 -> :sswitch_27
        0x7587 -> :sswitch_28
        0x7588 -> :sswitch_29
        0x7589 -> :sswitch_2d
        0x758a -> :sswitch_2a
        0x758b -> :sswitch_2b
        0x758c -> :sswitch_2c
        0x758d -> :sswitch_2f
        0x758e -> :sswitch_30
        0x7593 -> :sswitch_31
        0x7594 -> :sswitch_1
        0x7595 -> :sswitch_32
        0x7596 -> :sswitch_33
        0x7597 -> :sswitch_34
        0x7598 -> :sswitch_36
        0x7599 -> :sswitch_35
        0x759a -> :sswitch_37
        0x75a2 -> :sswitch_38
        0x75a4 -> :sswitch_39
        0x75a5 -> :sswitch_3a
        0x75ac -> :sswitch_3c
        0x75d1 -> :sswitch_4b
        0x75d2 -> :sswitch_4c
        0x75d3 -> :sswitch_4d
        0x75d6 -> :sswitch_3d
        0x75d7 -> :sswitch_3e
        0x75dc -> :sswitch_4e
        0x75eb -> :sswitch_48
        0x75ed -> :sswitch_45
        0x75ef -> :sswitch_44
        0x75f3 -> :sswitch_49
        0x75f7 -> :sswitch_46
        0x75fb -> :sswitch_3b
        0x75fc -> :sswitch_47
        0x7602 -> :sswitch_41
        0x7603 -> :sswitch_40
        0x7604 -> :sswitch_3f
        0x7612 -> :sswitch_43
        0x7613 -> :sswitch_42
        0x7615 -> :sswitch_4a
        0x7617 -> :sswitch_4f
        0x7619 -> :sswitch_50
        0x7624 -> :sswitch_54
        0x7625 -> :sswitch_55
        0x7626 -> :sswitch_56
        0x7627 -> :sswitch_57
        0x7639 -> :sswitch_52
        0x763a -> :sswitch_53
        0x763e -> :sswitch_58
        0x763f -> :sswitch_59
        0x7640 -> :sswitch_5a
        0x7641 -> :sswitch_5b
        0x7642 -> :sswitch_5c
        0x7643 -> :sswitch_5d
        0x7644 -> :sswitch_5e
        0x7645 -> :sswitch_5f
        0x7646 -> :sswitch_60
        0x764b -> :sswitch_61
        0x764c -> :sswitch_62
        0x764d -> :sswitch_63
        0x764e -> :sswitch_64
        0x7650 -> :sswitch_65
        0x7651 -> :sswitch_66
        0x7652 -> :sswitch_67
        0x7653 -> :sswitch_68
        0x7654 -> :sswitch_69
        0x7655 -> :sswitch_6a
        0x7656 -> :sswitch_6b
        0x7657 -> :sswitch_6c
        0x7658 -> :sswitch_6d
        0x7659 -> :sswitch_6e
        0x7670 -> :sswitch_51
        0x88b9 -> :sswitch_6f
        0x88bb -> :sswitch_71
        0x88bc -> :sswitch_72
        0x88bd -> :sswitch_73
        0x88bf -> :sswitch_74
        0x88c1 -> :sswitch_75
        0x88c3 -> :sswitch_76
        0x88c7 -> :sswitch_77
        0x88c9 -> :sswitch_78
        0x88cb -> :sswitch_79
        0x88cd -> :sswitch_7a
        0x88cf -> :sswitch_7b
        0x88d1 -> :sswitch_7d
        0x88d3 -> :sswitch_7e
        0x88d5 -> :sswitch_7c
        0x88d7 -> :sswitch_7f
        0x88d9 -> :sswitch_80
        0x88dc -> :sswitch_a0
        0x88dd -> :sswitch_81
        0x88de -> :sswitch_82
        0x88df -> :sswitch_83
        0x88e1 -> :sswitch_84
        0x88e7 -> :sswitch_89
        0x88e9 -> :sswitch_8a
        0x88eb -> :sswitch_8b
        0x88ec -> :sswitch_8c
        0x88ee -> :sswitch_8d
        0x88f0 -> :sswitch_8e
        0x88f1 -> :sswitch_85
        0x88f3 -> :sswitch_86
        0x88f4 -> :sswitch_87
        0x88f5 -> :sswitch_88
        0x88f9 -> :sswitch_9b
        0x88fb -> :sswitch_8f
        0x88fd -> :sswitch_90
        0x88ff -> :sswitch_91
        0x8901 -> :sswitch_92
        0x8903 -> :sswitch_93
        0x8905 -> :sswitch_94
        0x8907 -> :sswitch_95
        0x8908 -> :sswitch_96
        0x890b -> :sswitch_97
        0x890d -> :sswitch_99
        0x8910 -> :sswitch_9a
        0x8911 -> :sswitch_98
        0x8912 -> :sswitch_9c
        0x8913 -> :sswitch_9d
        0x8914 -> :sswitch_9e
        0x8915 -> :sswitch_9f
        0x8916 -> :sswitch_a1
        0x891c -> :sswitch_a2
        0x891d -> :sswitch_a3
        0x891e -> :sswitch_a4
        0x891f -> :sswitch_a5
        0x8920 -> :sswitch_a7
        0x8921 -> :sswitch_a6
        0x8922 -> :sswitch_a8
        0x8923 -> :sswitch_a9
        0x8924 -> :sswitch_aa
        0x8928 -> :sswitch_ab
        0x8929 -> :sswitch_ac
        0x892a -> :sswitch_ad
        0x892c -> :sswitch_ae
        0x892e -> :sswitch_af
        0x892f -> :sswitch_b0
        0x8933 -> :sswitch_b1
        0x8934 -> :sswitch_b2
        0x8951 -> :sswitch_b8
        0x8953 -> :sswitch_b9
        0x8959 -> :sswitch_d8
        0x895a -> :sswitch_d9
        0x895e -> :sswitch_ba
        0x895f -> :sswitch_bb
        0x8960 -> :sswitch_bc
        0x8962 -> :sswitch_b3
        0x8965 -> :sswitch_da
        0x896c -> :sswitch_b4
        0x896d -> :sswitch_b5
        0x896e -> :sswitch_b6
        0x896f -> :sswitch_b7
        0x8973 -> :sswitch_cd
        0x8975 -> :sswitch_cc
        0x8977 -> :sswitch_c8
        0x8979 -> :sswitch_ce
        0x897a -> :sswitch_bd
        0x8980 -> :sswitch_bf
        0x8981 -> :sswitch_c0
        0x8982 -> :sswitch_be
        0x898e -> :sswitch_c9
        0x898f -> :sswitch_ca
        0x8990 -> :sswitch_cb
        0x8991 -> :sswitch_c1
        0x8992 -> :sswitch_c2
        0x8993 -> :sswitch_c3
        0x8994 -> :sswitch_c4
        0x8995 -> :sswitch_c5
        0x8996 -> :sswitch_c6
        0x8997 -> :sswitch_d1
        0x8998 -> :sswitch_d4
        0x8999 -> :sswitch_cf
        0x899a -> :sswitch_d0
        0x899d -> :sswitch_d7
        0x89a8 -> :sswitch_c7
        0x89aa -> :sswitch_fc
        0x89ab -> :sswitch_fd
        0x89ac -> :sswitch_fe
        0x89ad -> :sswitch_ff
        0x89ae -> :sswitch_d2
        0x89af -> :sswitch_100
        0x89b0 -> :sswitch_101
        0x89b1 -> :sswitch_102
        0x89b2 -> :sswitch_fa
        0x89b3 -> :sswitch_fb
        0x89c0 -> :sswitch_f9
        0x89c1 -> :sswitch_dc
        0x89c3 -> :sswitch_dd
        0x89c6 -> :sswitch_de
        0x89c7 -> :sswitch_df
        0x89c8 -> :sswitch_e1
        0x89c9 -> :sswitch_e2
        0x89ca -> :sswitch_e3
        0x89cb -> :sswitch_e4
        0x89cc -> :sswitch_e5
        0x89cd -> :sswitch_e7
        0x89ce -> :sswitch_e8
        0x89cf -> :sswitch_ea
        0x89d0 -> :sswitch_eb
        0x89d1 -> :sswitch_ed
        0x89d2 -> :sswitch_ee
        0x89d3 -> :sswitch_e9
        0x89d4 -> :sswitch_ec
        0x89d6 -> :sswitch_e0
        0x89d8 -> :sswitch_ef
        0x89da -> :sswitch_f0
        0x89db -> :sswitch_f1
        0x89dc -> :sswitch_f2
        0x89de -> :sswitch_f4
        0x89df -> :sswitch_f5
        0x89e0 -> :sswitch_f6
        0x89e1 -> :sswitch_f7
        0x89e2 -> :sswitch_f8
        0x89e4 -> :sswitch_d3
        0x89e5 -> :sswitch_d5
        0x89e6 -> :sswitch_d6
        0x89f8 -> :sswitch_db
        0x89f9 -> :sswitch_e6
        0x8a48 -> :sswitch_70
    .end sparse-switch
.end method
