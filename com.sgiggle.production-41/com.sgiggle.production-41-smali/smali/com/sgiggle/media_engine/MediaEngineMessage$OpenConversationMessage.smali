.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OpenConversationMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1632
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1633
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V
    .locals 2
    .parameter

    .prologue
    .line 1636
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    const/16 v1, 0x763e

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setPeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1640
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 1643
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    const/16 v1, 0x763e

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1647
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZZ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1651
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    const/16 v1, 0x763e

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setFromPushNotification(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setMessageFromPushAvailable(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setFallbackToConversationList(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1658
    return-void
.end method
