.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseVGoodMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseVGoodMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2451
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    const/16 v1, 0x75ef

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseVGoodMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2454
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2457
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    const/16 v1, 0x75ef

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseVGoodMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setExternalMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setReceipt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setTransactionId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p6, p7}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setTime(J)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p8}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setSignature(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p9}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setIsrestore(Z)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2469
    return-void
.end method
