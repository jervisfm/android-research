.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInForegroundMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PutAppInForegroundMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1217
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7586

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInForegroundMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->setDoLogin(Z)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1221
    return-void
.end method
