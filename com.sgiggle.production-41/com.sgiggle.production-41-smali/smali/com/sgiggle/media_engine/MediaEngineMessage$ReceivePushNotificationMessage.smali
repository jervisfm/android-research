.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReceivePushNotificationMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 479
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7534

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 482
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 484
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7534

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setUsernameFrom(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setCallid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSessionid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->ANDROID_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setPushType(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 491
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 493
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7534

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setUsernameFrom(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setCallid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSessionid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->ANDROID_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setPushType(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSwiftServerIp(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSwiftTcpPort(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSwiftUdpPort(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 503
    return-void
.end method
