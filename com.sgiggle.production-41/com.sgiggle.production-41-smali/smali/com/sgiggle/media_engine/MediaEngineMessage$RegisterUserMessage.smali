.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RegisterUserMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 698
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 699
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 717
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    const/16 v1, 0x755b

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setAccessAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p10}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setStoreAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p9}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setLocale(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v3

    invoke-virtual {v3, p4}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountrycodenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v3

    invoke-virtual {v3, p5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryisocc(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v3

    invoke-virtual {v3, p6}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v2

    invoke-virtual {v2, p7}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setSubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-virtual {v1, p8}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 743
    return-void
.end method
