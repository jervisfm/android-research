.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReportPurchaseMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2248
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    const/16 v1, 0x7602

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2251
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2254
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    const/16 v1, 0x7602

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setReceipt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setTransactionId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setTime(J)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setSignature(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2264
    return-void
.end method
