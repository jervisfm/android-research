.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactRequestMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectContactRequestMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1930
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1931
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)V
    .locals 2
    .parameter

    .prologue
    .line 1934
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    const/16 v1, 0x764b

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactRequestMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1938
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1941
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    const/16 v1, 0x764b

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactRequestMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->setMessageType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1946
    return-void
.end method
