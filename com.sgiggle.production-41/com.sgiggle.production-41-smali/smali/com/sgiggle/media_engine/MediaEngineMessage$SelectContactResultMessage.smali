.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactResultMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectContactResultMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1951
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1952
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;Ljava/util/List;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1956
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    const/16 v1, 0x764c

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactResultMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->addAllContacts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1961
    return-void
.end method
